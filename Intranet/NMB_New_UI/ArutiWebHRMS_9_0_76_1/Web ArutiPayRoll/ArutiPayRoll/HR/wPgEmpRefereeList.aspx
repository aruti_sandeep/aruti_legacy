﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgEmpRefereeList.aspx.vb"
    Inherits="HR_wPgEmpRefereeList" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Reference List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Pending Employee" AutoPostBack="true"
                                            Visible="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" Text="Employee" runat="server" CssClass="form-lable" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPostCountry" Text="Country" runat="server" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpCountry" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblIDNo" Text="Company" runat="server" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtCompany" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRefereeName" Text="Referee" runat="server" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="TxtReferee" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnNew" CssClass="btn btn-primary" runat="server" Text="New" Visible="False" />
                                <asp:Button ID="BtnSearch" CssClass="btn btn-default" runat="server" Text="Search" />
                                <asp:Button ID="BtnReset" CssClass="btn btn-default" runat="server" Text="Reset" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                    <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btn btn-primary" Text="View Detail" />
                                    <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" CssClass="label label-primary bg-pw"></asp:Label>
                                    <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" CssClass="label label-danger bg-lc"></asp:Label>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
                                            <asp:DataGrid ID="gvReferee" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgSelect" runat="server" ToolTip="Select" CommandName="Select">
                                                                    <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details"
                                                                    CommandName="View" Visible="false"><i class="fas fa-eye text-primary"></i> </asp:LinkButton>
                                                                <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                            </span>
                                                        </ItemTemplate>
                                                        <%--<0>--%>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                   <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <%--<1>--%>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="EmpName" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee">
                                                    </asp:BoundColumn>
                                                    <%--<2>--%>
                                                    <asp:BoundColumn DataField="RefName" HeaderText="Referee" ReadOnly="True" FooterText="colhRefreeName">
                                                    </asp:BoundColumn>
                                                    <%--<3>--%>
                                                    <asp:BoundColumn DataField="Country" HeaderText="Country" ReadOnly="True" FooterText="colhCountry">
                                                    </asp:BoundColumn>
                                                    <%--<4>--%>
                                                    <asp:BoundColumn DataField="Company" HeaderText="Company" ReadOnly="True" FooterText="colhIdNo">
                                                    </asp:BoundColumn>
                                                    <%--<5>--%>
                                                    <asp:BoundColumn DataField="Email" HeaderText="Email" ReadOnly="True" FooterText="colhEmail">
                                                    </asp:BoundColumn>
                                                    <%--<6>--%>
                                                    <asp:BoundColumn DataField="telephone_no" HeaderText="Tel. No" ReadOnly="True" FooterText="colhTelNo">
                                                    </asp:BoundColumn>
                                                    <%--<7>--%>
                                                    <asp:BoundColumn DataField="mobile_no" HeaderText="Mobile" ReadOnly="True" FooterText="colhMobile">
                                                    </asp:BoundColumn>
                                                    <%--<8>--%>
                                                    <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                                    </asp:BoundColumn>
                                                    <%--<9>--%>
                                                    <asp:BoundColumn DataField="EmpId" HeaderText="EmpId" Visible="false" FooterText="objdgcolhempid">
                                                    </asp:BoundColumn>
                                                    <%--<10>--%>
                                                    <%--'Gajanan [17-DEC-2018] -- Start--%>
                                                    <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                    <asp:BoundColumn DataField="RefreeTranId" HeaderText="RefreeTranId" Visible="false"
                                                        FooterText="objdgcolhRefreeTranId"></asp:BoundColumn>
                                                    <%--<11>--%>
                                                    <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" FooterText="colhOperationType">
                                                    </asp:BoundColumn>
                                                    <%--<12>--%>
                                                    <%--'Gajanan [17-DEC-2018] -- End--%>
                                                </Columns>
                                                <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are ou Sure You Want To delete?:" />
                <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
