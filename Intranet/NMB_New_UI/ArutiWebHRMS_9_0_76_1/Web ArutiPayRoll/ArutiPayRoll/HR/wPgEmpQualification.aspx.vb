﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports System.Threading
Imports System.IO

#End Region

Partial Class HR_wPgEmpQualification
    Inherits Basepage

#Region " Private Variable(s) "

    Private objEmpQualif As New clsEmp_Qualification_Tran
    Dim msg As New CommonCodes
    Dim clsuser As New User

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim dicNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 18 SEP 2012 ] -- END


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmQualifications"
    'Pinkal (06-May-2014) -- End


    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Private mdtQualificationDocument As DataTable
    Private objDocument As New clsScan_Attach_Documents
    'SHANI (20 JUN 2015) -- End 


    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data. 
    Private mintQualificationUnkid As Integer = -1

    Private mintTransactionId As Integer = 0
    Private objAQualificationTran As New clsEmp_Qualification_Approval_Tran

    Dim Arr() As String
    Dim QualificationApprovalFlowVal As String
    'Gajanan [17-DEC-2018] -- End

    'Gajanan [3-April-2019] -- Start
    Dim mblnisEmployeeApprove As Boolean = False
    'Gajanan [3-April-2019] -- End

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End

    'Gajanan [21-June-2019] -- Start      
    Private OldData As New clsEmp_Qualification_Tran
    'Gajanan [21-June-2019] -- End
    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Private mstrURLReferer As String = ""
    'Sohail (09 Nov 2020) -- End
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objQGMaster As New clsCommon_Master
        Dim objQMaster As New clsqualification_master
        Dim objInstitute As New clsinstitute_master
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, Not Aruti.Data.ConfigParameter._Object._IsIncludeInactiveEmp)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsCombos = objEmployee.GetEmployeeList("Employee", True, )
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .



                'dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                '                                    CInt(Session("UserId")), _
                '                                    CInt(Session("Fin_year")), _
                '                                    CInt(Session("CompanyUnkId")), _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                    CStr(Session("UserAccessModeSetting")), True, _
                '                                    CBool(Session("IsIncludeInactiveEmp")), "Employee", True)


                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmQualificationsList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If

                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), _
                                       mblnOnlyApproved, CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                       True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "employeename"
                    .DataSource = dsCombos.Tables("Employee")
                    .DataBind()
                    .SelectedValue = "0"
                End With
                'S.SANDEEP [20-JUN-2018] -- End

                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                'S.SANDEEP [ 16 JAN 2014 ] -- START
                'With cboEmployee
                '    .DataValueField = "employeeunkid"
                '    .DataTextField = "employeename"
                '    .DataSource = dsCombos.Tables("Employee")
                '    .DataBind()
                '    .SelectedValue = 0
                'End With


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .



                'If Session("ShowPending") IsNot Nothing Then

                '    'Shani(24-Aug-2015) -- Start
                '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '    'dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"), , , False)
                '    dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                '                                        CInt(Session("UserId")), _
                '                                        CInt(Session("Fin_year")), _
                '                                        CInt(Session("CompanyUnkId")), _
                '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                        CStr(Session("UserAccessModeSetting")), False, _
                '                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                '    'Shani(24-Aug-2015) -- End

                '    Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
                '    With cboEmployee
                '        .DataValueField = "employeeunkid"
                '        'Nilay (09-Aug-2016) -- Start
                '        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '        '.DataTextField = "employeename"
                '        .DataTextField = "EmpCodeName"
                '        'Nilay (09-Aug-2016) -- End
                '        .DataSource = dtTab
                '        .DataBind()
                '    End With
                'Else
                '    With cboEmployee
                '        .DataValueField = "employeeunkid"
                '        'Nilay (09-Aug-2016) -- Start
                '        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '        '.DataTextField = "employeename"
                '        .DataTextField = "EmpCodeName"
                '        'Nilay (09-Aug-2016) -- End
                '        .DataSource = dsCombos.Tables("Employee")
                '        .DataBind()
                '        .SelectedValue = CStr(0)
                '    End With
                'End If
                'S.SANDEEP [ 16 JAN 2014 ] -- END

                'S.SANDEEP [20-JUN-2018] -- End

            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If

            dsCombos = objQGMaster.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "List")
            With cboQualifGrp
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombos = objQMaster.GetComboList("List", True)
            With cboQualifcation
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            'Shani (22-Jul-2016) -- Start
            'Issue : Result Code Not set defualt value (Metthew)
            Call cboQualifcation_SelectedIndexChanged(cboQualifcation, Nothing)
            'Shani (22-Jul-2016) -- End

            dsCombos = objInstitute.getListForCombo(False, "List", True)
            With cboProvider
                .DataValueField = "instituteunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            Dim objCMaster As New clsCommon_Master
            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With
            'SHANI (20 JUN 2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Public Function b64decode(ByVal StrDecode As String) As String
        Dim decodedString As String = ""
        Try
            decodedString = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(StrDecode))
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        Return decodedString
    End Function

    Private Function IsFromQualifList() As Boolean
        Try
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If (Request.QueryString("ProcessId") <> "") Then
            'Gajanan [17-DEC-2018] -- End
            If mintQualificationUnkid > 0 Then
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data. 

                'Dim mintQualifId As Integer = -1
                'mintQualifId = CInt(Val(b64decode(Request.QueryString("ProcessId"))))
                'Me.ViewState.Add("QualifUnkid", mintQualifId)
                'objEmpQualif._Qualificationtranunkid = mintQualifId

                objEmpQualif._Qualificationtranunkid = mintQualificationUnkid
                'Gajanan [17-DEC-2018] -- End
                Call Fill_Info()

            Else
                Me.ViewState.Add("SkillUnkid", -1)
            End If

            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data. 
                'If Me.ViewState("QualifUnkid") Is Nothing Or CInt(Me.ViewState("QualifUnkid")) <= 0 Then
                If mintQualificationUnkid <= 0 Then
                    'Gajanan [17-DEC-2018] -- End   
                    btnSave.Visible = CBool(Session("AddEmployeeQualification"))
                Else
                    btnSave.Visible = CBool(Session("EditEmployeeQualification"))
                End If

                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .

                ''S.SANDEEP [ 16 JAN 2014 ] -- START
                'If Session("ShowPending") IsNot Nothing Then
                '    btnSave.Visible = False
                'End If
                ''S.SANDEEP [ 16 JAN 2014 ] -- END

                'S.SANDEEP [20-JUN-2018] -- End






            Else

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data. 


                'If Me.ViewState("QualifUnkid") Is Nothing Or CInt(Me.ViewState("QualifUnkid")) <= 0 Then
                If mintQualificationUnkid <= 0 Then
                    'Gajanan [17-DEC-2018] -- End
                    btnSave.Visible = CBool(Session("AllowAddQualifications"))
                Else
                    btnSave.Visible = CBool(Session("AllowEditQualifications"))
                End If
            End If

            'Pinkal (22-Nov-2012) -- End

            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") Is Nothing Then

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'mdtQualificationDocument = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.QUALIFICATIONS, CInt(cboQualifcation.SelectedValue))

                    'Shani(07-Jan-2016) -- Start
                    'Passing Qualificiation tranunkid for other qualification, reason the attachment is not coming for Other Qualification.
                    'mdtQualificationDocument = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.QUALIFICATIONS, CInt(cboQualifcation.SelectedValue), Session("Document_Path"))

                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data. 
                    'mdtQualificationDocument = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.QUALIFICATIONS, CInt(Me.ViewState("QualifUnkid")), CStr(Session("Document_Path")))

                    mdtQualificationDocument = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.QUALIFICATIONS, mintQualificationUnkid, CStr(Session("Document_Path")))

                    'Gajanan [17-DEC-2018] -- End


                    'Shani(07-Jan-2016) -- End

                    'Shani(20-Nov-2015) -- End

                    'Shani(07-Jan-2016) -- Start
                    'Passing Qualificiation tranunkid for other qualification, reason the attachment is not coming for Other Qualification.
                    'If CInt(cboQualifcation.SelectedValue) <= 0 Then
                    '    mdtQualificationDocument.Rows.Clear()
                    'End If
                    If CInt(cboQualifcation.SelectedValue) <= 0 AndAlso chkOtherQualification.Checked = False Then
                        mdtQualificationDocument.Rows.Clear()
                    End If
                    'Shani(07-Jan-2016) -- End
                End If
            Else

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'mdtQualificationDocument = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.QUALIFICATIONS, CInt(cboQualifcation.SelectedValue))

                'Shani(07-Jan-2016) -- Start
                'Passing Qualificiation tranunkid for other qualification, reason the attachment is not coming for Other Qualification.
                'mdtQualificationDocument = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.QUALIFICATIONS, CInt(cboQualifcation.SelectedValue), Session("Document_Path"))
                mdtQualificationDocument = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.QUALIFICATIONS, -1, CStr(Session("Document_Path")))
                'Shani(07-Jan-2016) -- End

                'Shani(20-Nov-2015) -- End

                If CInt(cboQualifcation.SelectedValue) <= 0 Then
                    mdtQualificationDocument.Rows.Clear()
                End If
            End If

            'SHANI (20 JUN 2015) -- End 

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub Fill_Info()
        Try
            cboEmployee.SelectedValue = objEmpQualif._Employeeunkid.ToString
            cboProvider.SelectedValue = objEmpQualif._Instituteunkid.ToString
            cboQualifGrp.SelectedValue = objEmpQualif._Qualificationgroupunkid.ToString
            cboQualifcation.SelectedValue = objEmpQualif._Qualificationunkid.ToString
            txtRefNo.Text = objEmpQualif._Reference_No
            txtRemark.Text = objEmpQualif._Remark
            dtAFromDate.SetDate = objEmpQualif._Award_Start_Date.Date
            dtAToDate.SetDate = objEmpQualif._Award_End_Date.Date
            dtQualifDate.SetDate = objEmpQualif._Transaction_Date.Date

            'Sohail (21 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            cboResultCode.SelectedValue = objEmpQualif._Resultunkid.ToString
            txtGPAcode.Text = objEmpQualif._GPAcode.ToString
            'Sohail (21 Mar 2012) -- End


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes

            If objEmpQualif._Qualificationgroupunkid <= 0 Then
                chkOtherQualification.Checked = True
                chkOtherQualification_CheckedChanged(New Object(), New EventArgs())
                txtOtherQualificationGrp.Text = objEmpQualif._Other_QualificationGrp
                txtOtherQualification.Text = objEmpQualif._Other_Qualification
                txtOtherResultCode.Text = objEmpQualif._other_ResultCode
                'S.SANDEEP [23 JUL 2016] -- START
                'CCK- OTHER QUALIFICATION CHANGE
                txtOtherInstitute.Text = objEmpQualif._Other_institute
                'S.SANDEEP [23 JUL 2016] -- END
            End If
            chkOtherQualification.Enabled = False
            'Pinkal (25-APR-2012) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try

            'Gajanan [3-April-2019] -- Start
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)
            mblnisEmployeeApprove = objEmployee._Isapproved
            'Gajanan [3-April-2019] -- End

            If QualificationApprovalFlowVal Is Nothing Then


                objAQualificationTran._Audittype = enAuditType.ADD
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    objAQualificationTran._Audituserunkid = CInt(Session("UserId"))
                Else
                    objAQualificationTran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                End If

                If dtAToDate.IsNull = False Then
                    objAQualificationTran._Award_End_Date = dtAToDate.GetDate.Date
                Else
                    objAQualificationTran._Award_End_Date = Nothing
                End If

                If dtAFromDate.IsNull = False Then
                    objAQualificationTran._Award_Start_Date = dtAFromDate.GetDate.Date
                Else
                    objAQualificationTran._Award_Start_Date = Nothing
                End If

                objAQualificationTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objAQualificationTran._Qualificationgroupunkid = CInt(cboQualifGrp.SelectedValue)
                objAQualificationTran._Qualificationunkid = CInt(cboQualifcation.SelectedValue)
                objAQualificationTran._Reference_No = txtRefNo.Text
                objAQualificationTran._Remark = txtRemark.Text
                objAQualificationTran._Instituteunkid = CInt(cboProvider.SelectedValue)


                If dtAFromDate.IsNull = False Then
                    objAQualificationTran._Certificatedate = dtQualifDate.GetDate.Date
                Else
                    objAQualificationTran._Certificatedate = Nothing
                End If


                objAQualificationTran._Isvoid = False
                objAQualificationTran._Tranguid = Guid.NewGuid.ToString()
                objAQualificationTran._Transactiondate = Now
                objAQualificationTran._Approvalremark = ""
                objAQualificationTran._Isweb = True
                objAQualificationTran._Isfinal = False
                objAQualificationTran._Ip = Session("IP_ADD").ToString()
                objAQualificationTran._Host = Session("HOST_NAME").ToString()
                objAQualificationTran._Form_Name = mstrModuleName
                objAQualificationTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                If mintQualificationUnkid <= 0 Then
                    objAQualificationTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.ADDED
                    objAQualificationTran._Qualificationtranunkid = -1
                Else
                    objAQualificationTran._Qualificationtranunkid = mintQualificationUnkid
                    objAQualificationTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.EDITED
                End If
                objAQualificationTran._Resultunkid = CInt(cboResultCode.SelectedValue)
                objAQualificationTran._Gpacode = CDec(IIf(txtGPAcode.Text.Trim.Length = 0, "0", txtGPAcode.Text.Trim))

                If chkOtherQualification.Checked Then
                    objAQualificationTran._Other_Qualificationgrp = txtOtherQualificationGrp.Text.Trim
                    objAQualificationTran._Other_Qualification = txtOtherQualification.Text.Trim
                    objAQualificationTran._Other_Resultcode = txtOtherResultCode.Text.Trim
                    objAQualificationTran._Qualificationgroupunkid = 0
                    objAQualificationTran._Qualificationunkid = 0
                    objAQualificationTran._Resultunkid = 0
                    objAQualificationTran._Other_Institute = txtOtherInstitute.Text.Trim
                Else
                    objAQualificationTran._Other_Qualificationgrp = ""
                    objAQualificationTran._Other_Qualification = ""
                    objAQualificationTran._Other_Resultcode = ""
                    objAQualificationTran._Other_Institute = ""
                End If


            Else
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data. 
                'If CInt(Me.ViewState("QualifUnkid")) <= 0 Then
                If mintQualificationUnkid <= 0 Then
                    'Gajanan [17-DEC-2018] -- End
                    objEmpQualif._Qualificationtranunkid = -1
                Else

                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data. 
                    'objEmpQualif._Qualificationtranunkid = CInt(Me.ViewState("QualifUnkid"))
                    objEmpQualif._Qualificationtranunkid = mintQualificationUnkid
                    'Gajanan [17-DEC-2018] -- End
                End If

                If dtAToDate.IsNull = False Then
                    objEmpQualif._Award_End_Date = dtAToDate.GetDate.Date
                Else
                    objEmpQualif._Award_End_Date = Nothing
                End If

                If dtAFromDate.IsNull = False Then
                    objEmpQualif._Award_Start_Date = dtAFromDate.GetDate.Date
                Else
                    objEmpQualif._Award_Start_Date = Nothing
                End If

                objEmpQualif._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEmpQualif._Instituteunkid = CInt(cboProvider.SelectedValue)
                objEmpQualif._Isvoid = False
                objEmpQualif._Qualificationgroupunkid = CInt(cboQualifGrp.SelectedValue)
                objEmpQualif._Qualificationunkid = CInt(cboQualifcation.SelectedValue)
                objEmpQualif._Reference_No = txtRefNo.Text
                objEmpQualif._Remark = txtRemark.Text


                'Pinkal (12-Nov-2012) -- Start
                'Enhancement : TRA Changes

                If dtQualifDate.IsNull = True Then
                    objEmpQualif._Transaction_Date = Nothing
                Else
                    objEmpQualif._Transaction_Date = dtQualifDate.GetDate.Date
                End If

                'Pinkal (12-Nov-2012) -- End

                'Sohail (21 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                'Sohail (02 May 2012) -- Start
                'TRA - ENHANCEMENT
                'objEmpQualif._Resultunkid = cboResultCode.SelectedValue
                objEmpQualif._Resultunkid = CInt(IIf(cboResultCode.SelectedValue = "", 0, cboResultCode.SelectedValue))
                'Sohail (02 May 2012) -- End
                objEmpQualif._GPAcode = CDec(IIf(txtGPAcode.Text.Trim.Length = 0, "0", txtGPAcode.Text.Trim))
                'Sohail (21 Mar 2012) -- End

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    objEmpQualif._Userunkid = CInt(Session("UserId"))
                Else
                    objEmpQualif._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                End If

                objEmpQualif._Voiddatetime = Nothing
                objEmpQualif._Voidreason = ""
                objEmpQualif._Voiduserunkid = -1



                'Pinkal (25-APR-2012) -- Start
                'Enhancement : TRA Changes
                If chkOtherQualification.Checked Then
                    objEmpQualif._Other_QualificationGrp = txtOtherQualificationGrp.Text.Trim
                    objEmpQualif._Other_Qualification = txtOtherQualification.Text.Trim
                    objEmpQualif._other_ResultCode = txtOtherResultCode.Text.Trim
                    objEmpQualif._Qualificationgroupunkid = 0
                    objEmpQualif._Qualificationunkid = 0
                    objEmpQualif._Resultunkid = 0
                    'S.SANDEEP [23 JUL 2016] -- START
                    'CCK- OTHER QUALIFICATION CHANGE
                    objEmpQualif._Other_institute = txtOtherInstitute.Text.Trim
                    'S.SANDEEP [23 JUL 2016] -- END
                Else
                    objEmpQualif._Other_QualificationGrp = ""
                    objEmpQualif._Other_Qualification = ""
                    objEmpQualif._other_ResultCode = ""
                    'S.SANDEEP [23 JUL 2016] -- START
                    'CCK- OTHER QUALIFICATION CHANGE
                    objEmpQualif._Other_institute = ""
                    'S.SANDEEP [23 JUL 2016] -- END
                End If
                'Pinkal (25-APR-2012) -- End

                'S.SANDEEP [ 04 JULY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Blank_ModuleName()
                objEmpQualif._WebFormName = "frmQualifications"
                StrModuleName2 = "mnuPersonnel"
                StrModuleName3 = "mnuEmployeeData"
                objEmpQualif._WebClientIP = CStr(Session("IP_ADD"))
                objEmpQualif._WebHostName = CStr(Session("HOST_NAME"))
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    objEmpQualif._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                End If
                'S.SANDEEP [ 04 JULY 2012 ] -- END

                'Hemant (11 Apr 2022) -- Start            
                'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
                objEmpQualif._Created_Date = ConfigParameter._Object._CurrentDateAndTime
                'Hemant (11 Apr 2022) -- End

            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearObject()
        Try
            txtRefNo.Text = "" : txtRemark.Text = "" : cboProvider.SelectedValue = CStr(0) : cboQualifcation.SelectedValue = CStr(0) : cboQualifGrp.SelectedValue = CStr(0)
            dtAFromDate.SetDate = Nothing : dtAToDate.SetDate = Nothing : dtQualifDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            'Sohail (21 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            cboResultCode.SelectedValue = CStr(0) : txtGPAcode.Text = ""
            'Sohail (21 Mar 2012) -- End

            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            txtOtherQualificationGrp.Text = ""
            txtOtherQualification.Text = ""
            txtOtherResultCode.Text = ""
            'Sohail (02 May 2012) -- End

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            cboDocumentType.SelectedValue = CStr(0)
            cboEmployee.Enabled = True
            cboQualifGrp.Enabled = True
            cboQualifcation.Enabled = True
            If mdtQualificationDocument IsNot Nothing AndAlso mdtQualificationDocument.Rows.Count > 0 Then
                mdtQualificationDocument.Rows.Clear()
            End If
            'SHANI (20 JUN 2015) -- End 

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'Gajanan [21-June-2019] -- Start      
    'Private Function IsDataValid() As Boolean
    Private Function IsDataValid(Optional ByVal IsFromAttachment As Boolean = False) As Boolean
        'Gajanan [21-June-2019] -- End

        Try


            'Gajanan [21-June-2019] -- Start      

            Dim blnIsChange As Boolean = True
            If IsFromAttachment = False AndAlso IsNothing(OldData) = False Then

                If (mintQualificationUnkid > 0) Then
                    objEmpQualif._Qualificationtranunkid = mintQualificationUnkid
                    OldData = objEmpQualif
                End If

                If OldData._Award_End_Date = dtAToDate.GetDate AndAlso _
                   OldData._Award_Start_Date = dtAFromDate.GetDate AndAlso _
                   OldData._Qualificationgroupunkid = CInt(cboQualifGrp.SelectedValue) AndAlso _
                   OldData._Qualificationunkid = CInt(cboQualifcation.SelectedValue) AndAlso _
                   OldData._Reference_No = txtRefNo.Text AndAlso _
                   OldData._Remark = txtRemark.Text AndAlso _
                   OldData._Instituteunkid = CInt(cboProvider.SelectedValue) AndAlso _
                   OldData._Transaction_Date = dtQualifDate.GetDate AndAlso _
                   OldData._Resultunkid = CInt(cboResultCode.SelectedValue) AndAlso _
                   OldData._GPAcode = CDec(IIf(CStr(txtGPAcode.Text) = "", 0, txtGPAcode.Text)) Then

                    blnIsChange = False
                End If



                If chkOtherQualification.Checked Then
                    If OldData._Other_QualificationGrp = txtOtherQualificationGrp.Text.Trim AndAlso _
                        OldData._Other_Qualification = txtOtherQualification.Text.Trim AndAlso _
                        OldData._other_ResultCode = txtOtherResultCode.Text.Trim AndAlso _
                        OldData._Qualificationgroupunkid = 0 AndAlso _
                        OldData._Qualificationunkid = 0 AndAlso _
                        OldData._Resultunkid = 0 AndAlso _
                        OldData._Other_institute = txtOtherInstitute.Text.Trim Then

                        blnIsChange = False

                    End If
                End If
            End If


            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            If CBool(Session("QualificationCertificateAttachmentMandatory")) AndAlso IsFromAttachment = False Then
                If mdtQualificationDocument Is Nothing OrElse mdtQualificationDocument.Select("AUD <> 'D'").Length <= 0 Then
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), Me)
                    Exit Function
                End If
            End If
            'Gajanan [5-Dec-2019] -- End


            If IsFromAttachment = False AndAlso blnIsChange = False AndAlso IsNothing(mdtQualificationDocument) = False AndAlso CInt(mdtQualificationDocument.Select("AUD <> '' ").Count) > 0 Then
                blnIsChange = True
            End If



            If blnIsChange = False Then
                If (mintQualificationUnkid > 0) Then
                    objEmpQualif = Nothing
                    OldData = Nothing
                    mdtQualificationDocument = Nothing
                    Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpQualificationList.aspx", False)
                    Return False
                End If
            End If
            'Gajanan [21-June-2019] -- End


            If CInt(cboEmployee.SelectedValue) <= 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), Me)
                'Pinkal (06-May-2014) -- End
                cboEmployee.Focus()
                Return False
            End If

            If chkOtherQualification.Checked Then

                If txtOtherQualificationGrp.Text.Trim.Length <= 0 Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Other Qualification Group cannot be blank.Other Qualification Group is compulsory information."), Me)
                    'Pinkal (06-May-2014) -- End
                    txtOtherQualificationGrp.Focus()
                    Return False
                End If

                If txtOtherQualification.Text.Trim.Length <= 0 Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Other Qualification cannot be blank.Other Qualification is compulsory information."), Me)
                    'Pinkal (06-May-2014) -- End
                    txtOtherQualification.Focus()
                    Return False
                End If

                If txtOtherResultCode.Text.Trim.Length <= 0 Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Other Result Code cannot be blank.Other Result Code is compulsory information."), Me)
                    'Pinkal (06-May-2014) -- End
                    txtOtherResultCode.Focus()
                    Return False
                End If

            Else

                If CInt(cboQualifGrp.SelectedValue) <= 0 Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Qualification Group is compulsory information. Please select Qualification Group to continue."), Me)
                    'Pinkal (06-May-2014) -- End
                    cboQualifGrp.Focus()
                    Return False
                End If

                If CInt(cboQualifcation.SelectedValue) <= 0 Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Qualification is compulsory information. Please select Qualification to continue."), Me)
                    'Pinkal (06-May-2014) -- End
                    cboQualifcation.Focus()
                    Return False
                End If

                'Sohail (21 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                If CInt(cboResultCode.SelectedValue) <= 0 Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Result Code is compulsory information. Please select Result Code to continue."), Me)
                    'Pinkal (06-May-2014) -- End
                    cboResultCode.Focus()
                    Return False
                End If
                'Sohail (21 Mar 2012) -- End

            End If

            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            If chkOtherQualification.Checked = False Then
                If CInt(cboProvider.SelectedValue) <= 0 Then
                    'Hemant (31 May 2019) -- Start
                    'ISSUE/ENHANCEMENT : UAT Changes
                    'msg.DisplayMessage("Provider is compulsory information. Please select Provider to continue.", Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2001, "Provider is compulsory information. Please select Provider to continue."), Me)
                    'Hemant (31 May 2019) -- End
                    cboProvider.Focus()
                    Return False
                End If
            Else
                If CInt(txtOtherInstitute.Text.Trim.Length) <= 0 Then
                    'Hemant (31 May 2019) -- Start
                    'ISSUE/ENHANCEMENT : UAT Changes
                    'msg.DisplayMessage("Provider is compulsory information. Please select Provider to continue.", Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2002, "Provider is compulsory information. Please select Provider to continue."), Me)
                    'Hemant (31 May 2019) -- End
                    txtOtherInstitute.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP [23 JUL 2016] -- END



            If dtAFromDate.IsNull = False AndAlso dtAToDate.IsNull = False Then
                If dtAToDate.GetDate.Date <= dtAFromDate.GetDate.Date Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "End date cannot be less or equal to start date."), Me)
                    'Pinkal (06-May-2014) -- End
                    Return False
                End If
            End If

            'Anjan [ 05 Jan 2012 ] -- Start
            'ENHANCEMENT : TRA CHANGES
            Dim mdec As Decimal
            Decimal.TryParse(txtGPAcode.Text, mdec)
            If mdec > 100 Then
                'Hemant (31 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                'msg.DisplayMessage("GPA code cannot be greater than 100.", Me)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2003, "GPA code cannot be greater than 100."), Me)
                'Hemant (31 May 2019) -- End
                txtGPAcode.Focus()
                Return False
            End If

            'Anjan [ 05 Jan 2012 ] -- End


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            If QualificationApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then

                If objApprovalData.IsApproverPresent(enScreenName.frmQualificationsList, CStr(Session("Database_Name")), _
                                                      CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                      CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                                                      CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(cboEmployee.SelectedValue.ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    msg.DisplayMessage(objApprovalData._Message, Me)
                    Return False
                End If


                If objEmpQualif.isExist(CInt(cboEmployee.SelectedValue), CInt(cboQualifcation.SelectedValue), txtRefNo.Text.Trim, CInt(cboProvider.SelectedValue), mintQualificationUnkid, txtOtherQualification.Text.Trim, Nothing) = True Then
                    eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsEmp_Qualification_Tran", 1, "This Qualification is already assigned to particular employee. Please assign new Qualification."), enMsgBoxStyle.Information)
                    Return False
                End If
                Dim intOperationType As Integer = 0 : Dim strMsg As String = ""
                If objAQualificationTran.isExist(CInt(cboEmployee.SelectedValue), CInt(cboQualifcation.SelectedValue), txtRefNo.Text, CInt(cboProvider.SelectedValue), Nothing, "", txtOtherQualification.Text.Trim, Nothing, intOperationType) = True Then
                    If intOperationType > 0 Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                If mintQualificationUnkid > 0 Then
                                    strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in edit mode.")
                                Else
                                    strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 30, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.DELETED
                                If mintQualificationUnkid > 0 Then
                                    strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in deleted mode.")
                                Else
                                    strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 32, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.ADDED
                                If mintQualificationUnkid > 0 Then
                                    strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 33, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in added mode.")
                                Else
                                    strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 34, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
                                End If
                        End Select
                    End If
                End If
                If strMsg.Trim.Length > 0 Then
                    eZeeMsgBox.Show(strMsg, enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'Gajanan [17-April-2019] -- End


            Return True

        Catch ex As Exception
            msg.DisplayError(ex, Me)
            Return False
        Finally
        End Try
    End Function

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    Private Function Set_Notification(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Dim objEmployee As New clsEmployee_Master
        Try

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data. 

            'objEmpQualif._Qualificationtranunkid = CInt(Me.ViewState("QualifUnkid"))
            objEmpQualif._Qualificationtranunkid = mintQualificationUnkid

            'Gajanan [17-DEC-2018] -- End


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)
            'Shani(20-Nov-2015) -- End

            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If Session("Notify_EmplData").ToString.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee : <b>" & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & "</b></span></p>")
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                If CInt(Session("UserId")) > 0 Then
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmployee._Employeecode & "</b>. Following information has been changed by user : <b>" & objUser._Firstname.Trim & " " & objUser._Lastname & "</b></span></p>")
                ElseIf CInt(Session("Employeeunkid")) > 0 Then
                    StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmployee._Employeecode & "</b>. Following information has been changed by user : <b>" & objEmployee._Firstname.Trim & " " & objEmployee._Othername & " " & objEmployee._Surname & "</b></span></p>")
                End If

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & Session("HOST_NAME").ToString & "</b> and IPAddress : <b>" & Session("IP_ADD").ToString & "</b></span></p>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:15%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Field" & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Old Value" & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "New Value" & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")
                For Each sId As String In Session("Notify_EmplData").ToString.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enEmployeeData.QUALIFICATIONS

                            If objEmpQualif._Qualificationgroupunkid <> CInt(cboQualifGrp.SelectedValue) Then
                                Dim objQGroup As New clsCommon_Master
                                objQGroup._Masterunkid = objEmpQualif._Qualificationgroupunkid
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Qualification Group" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objQGroup._Name.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboQualifGrp.SelectedValue) <= 0, "", cboQualifGrp.SelectedItem.Text.Trim)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            ElseIf objEmpQualif._Other_QualificationGrp.Trim.Length > 0 Then

                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Other Qualification Group" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objEmpQualif._Other_QualificationGrp.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtOtherQualificationGrp.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            End If

                            If objEmpQualif._Qualificationunkid <> CInt(cboQualifcation.SelectedValue) Then
                                Dim objQual As New clsqualification_master
                                objQual._Qualificationunkid = objEmpQualif._Qualificationunkid
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Qualification/Award" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objQual._Qualificationname.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboQualifcation.SelectedValue) <= 0, "", cboQualifcation.SelectedItem.Text.Trim)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            ElseIf objEmpQualif._Other_Qualification.Trim.Length > 0 Then

                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Other Qualification/Award" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objEmpQualif._Other_Qualification.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtOtherQualification.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            End If

                            If objEmpQualif._Resultunkid <> CInt(cboResultCode.SelectedValue) Then
                                Dim objresult As New clsresult_master
                                objresult._Resultunkid = objEmpQualif._Resultunkid
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Result" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objresult._Resultname.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboResultCode.SelectedValue) <= 0, "", cboResultCode.SelectedItem.Text.Trim)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            ElseIf objEmpQualif._other_ResultCode.Trim.Length > 0 Then

                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Other Result" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objEmpQualif._other_ResultCode.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtOtherResultCode.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            End If

                            'S.SANDEEP [ 22 NOV 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'If objEmpQualif._GPAcode <> txtGPAcode.Text.Trim Then
                            '    StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                            '    StrMessage.Append(vbCrLf)
                            '    StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "GPA" & "</span></TD>")
                            '    StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objEmpQualif._GPAcode.ToString("#0.00") & "</span></TD>")
                            '    StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtGPAcode.Text.Trim & "</span></TD>")
                            '    StrMessage.Append("</TR>" & vbCrLf)
                            '    blnFlag = True
                            'End If

                            If txtGPAcode.Text.Trim <> "" Then
                                If objEmpQualif._GPAcode <> CDec(txtGPAcode.Text.Trim) Then
                                    StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                    StrMessage.Append(vbCrLf)
                                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "GPA" & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objEmpQualif._GPAcode.ToString("#0.00") & "</span></TD>")
                                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtGPAcode.Text.Trim & "</span></TD>")
                                    StrMessage.Append("</TR>" & vbCrLf)
                                    blnFlag = True
                                End If
                            End If
                            'S.SANDEEP [ 22 NOV 2012 ] -- END

                            If objEmpQualif._Award_Start_Date.Date <> dtAFromDate.GetDate.Date Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Start Date" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(objEmpQualif._Award_Start_Date <> Nothing, objEmpQualif._Award_Start_Date.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(Not dtAFromDate.IsNull, dtAFromDate.GetDate.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objEmpQualif._Award_End_Date.Date <> dtAToDate.GetDate.Date Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Award Date" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(objEmpQualif._Award_End_Date <> Nothing, objEmpQualif._Award_End_Date.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(Not dtAToDate.IsNull, dtAToDate.GetDate.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objEmpQualif._Instituteunkid <> CInt(cboProvider.SelectedValue) Then
                                Dim objInstitute As New clsinstitute_master
                                objInstitute._Instituteunkid = objEmpQualif._Instituteunkid
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Provider" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objInstitute._Institute_Name.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(cboProvider.SelectedValue) <= 0, "", cboProvider.SelectedItem.Text.Trim)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                    End Select
                Next
            End If
            StrMessage.Append("</TABLE>")
            StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString
        Catch ex As Exception
            msg.DisplayError(ex, Me)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data. 

            'If CInt(Me.ViewState("QualifUnkid")) > 0 Then
            If mintQualificationUnkid > 0 Then
                'Gajanan [17-DEC-2018] -- End

                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = "Notification of Changes in Employee Qualification(s)."
                            objSendMail._Message = dicNotification(sKey)
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data. 
                'ElseIf CInt(Me.ViewState("QualifUnkid")) <= 0 Then
            ElseIf mintQualificationUnkid <= 0 Then
                'Gajanan [17-DEC-2018] -- End

                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = "Notifications to newly added Employee Qualification(s)."
                            Dim sMsg As String = dicNotification(sKey)

                            If CInt(Session("UserId")) > 0 Then
                                Dim objUser As New clsUserAddEdit
                                objUser._Userunkid = CInt(Session("UserId"))
                                Set_Notification(objUser._Firstname & " " & objUser._Lastname)
                            ElseIf CInt(Session("Employeeunkid")) > 0 Then
                                Dim objEmp As New clsEmployee_Master

                                'Shani(20-Nov-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'objEmp._Employeeunkid = CInt(Session("Employeeunkid"))
                                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
                                'Shani(20-Nov-2015) -- End

                                Set_Notification(objEmp._Firstname & "  " & objEmp._Surname)
                            End If

                            objSendMail._Message = sMsg
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP [ 18 SEP 2012 ] -- END

    'S.SANDEEP [ 18 SEP 2012 ] -- END


    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            'S.SANDEEP |08-FEB-2022| -- START
            'ISSUE : CODE OPTIMIZATION/ERROR SOLVING
            If mdtQualificationDocument Is Nothing Then Exit Sub
            'S.SANDEEP |08-FEB-2022| -- END

            Dim dtRow() As DataRow = mdtQualificationDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtQualificationDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Employee_Module
                dRow("scanattachrefid") = enScanAttactRefId.QUALIFICATIONS

                'Shani(07-Jan-2016) -- Start
                'Passing Qualificiation tranunkid for other qualification, reason the attachment is not coming for Other Qualification.
                'dRow("transactionunkid") = IIf(mvOtherQualification.Visible = True, -1, cboQualifcation.SelectedValue)

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data. 
                'If Me.ViewState("QualifUnkid") Is Nothing Or CInt(Me.ViewState("QualifUnkid")) <= 0 Then
                If mintQualificationUnkid <= 0 Then
                    'Gajanan [17-DEC-2018] -- End

                    dRow("transactionunkid") = -1
                Else

                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data. 
                    'dRow("transactionunkid") = Me.ViewState("QualifUnkid")
                    dRow("transactionunkid") = mintQualificationUnkid
                    'Gajanan [17-DEC-2018] -- End

                End If
                'Shani(07-Jan-2016) -- End
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = dtQualifDate.GetDate
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"

                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                dRow("filesize_kb") = f.Length / 1024
                'SHANI (16 JUL 2015) -- End 

                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                'S.SANDEEP |25-JAN-2019| -- END

            Else
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtQualificationDocument.Rows.Add(dRow)
            Call FillQualificationAttachment()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("AddDocumentAttachment :- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillQualificationAttachment()
        Dim dtView As DataView
        Try
            If mdtQualificationDocument Is Nothing Then Exit Sub

            dtView = New DataView(mdtQualificationDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvQualification.AutoGenerateColumns = False
            dgvQualification.DataSource = dtView
            dgvQualification.DataBind()

            If dgvQualification.Items.Count > 0 Then
                cboEmployee.Enabled = False
                cboQualifGrp.Enabled = False
                cboQualifcation.Enabled = False
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "FillQualificationAttachment", mstrModuleName)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub DeleteTempFile()
        Try
            If mdtQualificationDocument Is Nothing Then Exit Sub
            Dim xRow() As DataRow = mdtQualificationDocument.Select("localpath <> '' ")
            If xRow.Length > 0 Then
                For Each row As DataRow In xRow
                    If System.IO.File.Exists(row("localpath").ToString) Then
                        System.IO.File.Delete(row("localpath").ToString)
                    End If
                Next
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'SHANI (20 JUN 2015) -- End 
#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                'Anjan [20 February 2016] -- End
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            'Hemant (31 May 2019) -- Start
            'ISSUE/ENHANCEMENT : UAT Changes
            Call SetMessages()
            'Call Language._Object.SaveValue()
            'Hemant (31 May 2019) -- End
            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            chkOtherQualification_CheckedChanged(New Object, New EventArgs)
            'S.SANDEEP [23 JUL 2016] -- END


            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            QualificationApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmQualificationsList)))
            'Gajanan [17-DEC-2018] -- End


            If (Page.IsPostBack = False) Then
                clsuser = CType(Session("clsuser"), User)

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = Session("UserId")
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END


                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'btnSave.Visible = False
                    btnSave.Visible = CBool(Session("EditEmployeeQualification"))
                    'Anjan (30 May 2012)-End 

                    'S.SANDEEP [20-JUN-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow For NMB .

                    ''S.SANDEEP [ 16 JAN 2014 ] -- START
                    'If Session("ShowPending") IsNot Nothing Then
                    '    btnSave.Visible = False
                    'End If
                    ''S.SANDEEP [ 16 JAN 2014 ] -- END

                    'S.SANDEEP [20-JUN-2018] -- End


                Else
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = -1
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'btnSave.Visible = ConfigParameter._Object._AllowEditQualifications
                    btnSave.Visible = CBool(Session("AllowEditQualifications"))
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-End 
                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data. 

                    If IsNothing(ViewState("mintQualificationUnkid")) = False Then
                        mintQualificationUnkid = CInt(ViewState("mintQualificationUnkid").ToString)
                    End If



                    'Gajanan [17-DEC-2018] -- End

                    'Gajanan [3-April-2019] -- Start
                    If IsNothing(ViewState("mblnisEmployeeApprove")) = False Then
                        mblnisEmployeeApprove = CBool(ViewState("mblnisEmployeeApprove"))
                    End If
                    'Gajanan [3-April-2019] -- End

                End If

                Call FillCombo()
                dtAFromDate.SetDate = Nothing
                dtAToDate.SetDate = Nothing
                dtQualifDate.SetDate = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime.Date

                'Nilay (02-Mar-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                If Request.QueryString.Count <= 0 Then
                    If Session("qualificationEmpunkid") IsNot Nothing Then
                        cboEmployee.SelectedValue = CStr(CInt(Session("qualificationEmpunkid")))
                    End If

                    'SHANI (20 JUN 2015) -- Start
                    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                Else
                    cboEmployee.Enabled = False
                    cboQualifGrp.Enabled = False
                    cboQualifcation.Enabled = False
                    'SHANI (20 JUN 2015) -- End 
                End If
                'Nilay (02-Mar-2015) -- End

                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If (Request.QueryString("ProcessId") <> Nothing) Then
                    mintQualificationUnkid = CInt(Val(b64decode(Request.QueryString("ProcessId"))))
                End If

                'Gajanan [17-DEC-2018] -- End


                IsFromQualifList()

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data. 
                'If CInt(Me.ViewState("QualifUnkid")) > 0 Then
                If mintQualificationUnkid > 0 Then
                    'Gajanan [17-DEC-2018] -- End

                    Call cboQualifcation_SelectedIndexChanged(sender, e)
                    cboResultCode.SelectedValue = CStr(objEmpQualif._Resultunkid)
                End If
                Call FillQualificationAttachment()
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                If Request.UrlReferrer IsNot Nothing Then
                    mstrURLReferer = Request.UrlReferrer.AbsoluteUri
                Else
                    mstrURLReferer = Session("rootpath").ToString & "HR/wPgEmpQualificationList.aspx"
                End If
                'Sohail (09 Nov 2020) -- End
            Else
                mdtQualificationDocument = CType(Session("mdtQualificationDocument"), DataTable)
                mintQualificationUnkid = CInt(ViewState("mintQualificationUnkid"))

                'SHANI (20 JUN 2015) -- End 
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                mstrURLReferer = ViewState("mstrURLReferer").ToString
                'Sohail (09 Nov 2020) -- End
            End If

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END
        End Try
    End Sub

    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    'Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
    '    Try
    '        If Not IsPostBack Then
    '            IsFromQualifList()
    '            'Sohail (21 Mar 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If Me.ViewState("QualifUnkid") > 0 Then
    '                Call cboQualifcation_SelectedIndexChanged(sender, e)
    '                cboResultCode.SelectedValue = objEmpQualif._Resultunkid
    '            End If
    '            'Sohail (21 Mar 2012) -- End
    '        End If
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'SHANI (20 JUN 2015) -- End

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleSaveButton(False)
            'ToolbarEntry1.VisibleCancelButton(False)
            'ToolbarEntry1.VisibleNewButton(False)
            'SHANI [01 FEB 2015]--END
            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") Is Nothing Then
                    If Session("mdtQualificationDocument") Is Nothing Then
                        Session.Add("mdtQualificationDocument", mdtQualificationDocument)
                    Else
                        Session("mdtQualificationDocument") = mdtQualificationDocument
                    End If
                End If
            Else
                If Session("mdtQualificationDocument") Is Nothing Then
                    Session.Add("mdtQualificationDocument", mdtQualificationDocument)
                Else
                    Session("mdtQualificationDocument") = mdtQualificationDocument
                End If
            End If
            'SHANI (20 JUN 2015) -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data. 
            ViewState("mintQualificationUnkid") = mintQualificationUnkid
            'Gajanan [17-DEC-2018] -- End

            'Gajanan [3-April-2019] -- Start
            ViewState("mblnisEmployeeApprove") = mblnisEmployeeApprove
            'Gajanan [3-April-2019] -- End
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            ViewState("mstrURLReferer") = mstrURLReferer
            'Sohail (09 Nov 2020) -- End
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("IsFrom_AddEdit") = False
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)
            mblnisEmployeeApprove = objEmployee._Isapproved
            'Gajanan [17-April-2019] -- End


            'Gajanan [21-June-2019] -- Start      
            'If IsDataValid(False) = False Then Exit Sub
            If IsDataValid(False) = False Then Exit Sub
            'Gajanan [21-June-2019] -- End
            Dim blnFlag As Boolean = False


            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    




            'SHANI (27 JUL 2015) -- Start
            'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
            'If CBool(Session("QualificationCertificateAttachmentMandatory")) Then
            '    If mdtQualificationDocument Is Nothing OrElse mdtQualificationDocument.Select("AUD <> 'D'").Length <= 0 Then
            '        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 26, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), Me)
            '        Exit Sub
            '    End If
            'End If
            'SHANI (27 JUL 2015) -- End 
            'Gajanan [5-Dec-2019] -- End


            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dicNotification = New Dictionary(Of String, String)


            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data. 
            'If CInt(Me.ViewState("QualifUnkid")) > 0 Then
            If mintQualificationUnkid > 0 Then
                'Gajanan [17-DEC-2018] -- End

                If Session("Notify_EmplData").ToString.Trim.Length > 0 Then
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    For Each sId As String In Session("Notify_EmplData").ToString.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        If dicNotification.ContainsKey(objUsr._Email) = False Then
                            dicNotification.Add(objUsr._Email, StrMessage)
                        End If
                    Next
                    objUsr = Nothing
                End If


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data. 
                'ElseIf CInt(Me.ViewState("QualifUnkid")) <= 0 Then
            ElseIf mintQualificationUnkid <= 0 Then
                'Gajanan [17-DEC-2018] -- End

                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                If Session("Notify_EmplData").ToString.Trim.Length > 0 Then
                    For Each sId As String In Session("Notify_EmplData").ToString.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        'Gajanan [19-NOV-2019] -- Start 

                        If dicNotification.ContainsKey(objUsr._Email) = False Then
                            dicNotification.Add(objUsr._Email, StrMessage)
                        End If
                        'Gajanan [19-NOV-2019] -- End
                    Next
                End If
                objUsr = Nothing
            End If

            'S.SANDEEP [ 18 SEP 2012 ] -- END

            Call SetValue()

            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            Dim mdsDoc As DataSet
            Dim mstrFolderName As String = ""
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim strFileName As String = ""
            For Each dRow As DataRow In mdtQualificationDocument.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault

                If dRow("AUD").ToString = "A" AndAlso dRow("localpath").ToString <> "" Then
                    'Pinkal (20-Nov-2018) -- Start
                    'Enhancement - Working on P2P Integration for NMB.
                    'strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                    'Pinkal (20-Nov-2018) -- End
                    If File.Exists(CStr(dRow("localpath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(dRow("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRow("fileuniquename") = strFileName
                        dRow("filepath") = strPath
                        dRow.AcceptChanges()
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'msg.DisplayError(ex, Me)
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("File does not exist on localpath", Me)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2004, "File does not exist on localpath"), Me)
                        'Hemant (31 May 2019) -- End
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRow("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'msg.DisplayError(ex, Me)
                            'Hemant (31 May 2019) -- Start
                            'ISSUE/ENHANCEMENT : UAT Changes
                            'msg.DisplayMessage("File does not exist on localpath", Me)
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2004, "File does not exist on localpath"), Me)
                            'Hemant (31 May 2019) -- End
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'msg.DisplayError(ex, Me)
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("File does not exist on localpath", Me)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2004, "File does not exist on localpath"), Me)
                        'Hemant (31 May 2019) -- End
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                End If

            Next

            'SHANI (16 JUL 2015) -- End 

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim ExtraFilter As String = "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and instituteunkid = " & cboProvider.SelectedValue.ToString() & " "

            Dim ExtraFilterForQuery As String = "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and instituteunkid = " & cboProvider.SelectedValue.ToString() & " "

            If dtAFromDate.IsNull = False Then
                ExtraFilter &= " and Award_Start_Date_fc = '" + eZeeDate.convertDate(dtAFromDate.GetDate()).ToString() + "' "
                ExtraFilterForQuery &= " and convert(varchar(8), cast(Award_Start_Date as datetime), 112)   =  '" + eZeeDate.convertDate(dtAFromDate.GetDate()).ToString() + "' "
            End If

            If dtAToDate.IsNull = False Then
                ExtraFilter &= " and Award_End_Date_fc = '" + eZeeDate.convertDate(dtAToDate.GetDate).ToString() + "' "
                ExtraFilterForQuery &= " and convert(varchar(8), cast(Award_End_Date as datetime), 112)   =  '" + eZeeDate.convertDate(dtAToDate.GetDate).ToString() + "' "
            End If

            If txtOtherQualification.Text.Trim.Length > 0 Then
                ExtraFilter &= " and other_qualification = '" + txtOtherQualification.Text + "' "
                ExtraFilterForQuery &= " and other_qualification = '" + txtOtherQualification.Text + "' "
            End If


            Dim eLMode As Integer
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                eLMode = enLogin_Mode.MGR_SELF_SERVICE
            Else
                eLMode = enLogin_Mode.EMP_SELF_SERVICE
            End If

            'Gajanan [17-April-2019] -- End




            'If CInt(Me.ViewState("QualifUnkid")) <= 0 Then
            If CInt(mintQualificationUnkid) <= 0 Then


                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                'If objEmpQualif.Insert = False Then


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.





                'If objEmpQualif.Insert(mdtQualificationDocument) = False Then
                '    'SHANI (20 JUN 2015) -- End 
                '    msg.DisplayMessage(objEmpQualif._Message, Me)
                'Else
                '    msg.DisplayMessage("Qualification successfully added.", Me)
                '    'S.SANDEEP [ 18 SEP 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    If objEmpQualif._Message.Trim.Length <= 0 Then
                '        trd = New Thread(AddressOf Send_Notification)
                '        trd.IsBackground = True
                '        trd.Start()
                '    End If
                '    'S.SANDEEP [ 18 SEP 2012 ] -- END
                'End If





                'Gajanan [3-April-2019] -- Start

                'If QualificationApprovalFlowVal Is Nothing AndAlso mintQualificationUnkid <= 0 Then
                If QualificationApprovalFlowVal Is Nothing AndAlso mintQualificationUnkid <= 0 AndAlso mblnisEmployeeApprove = True Then
                    'Gajanan [3-April-2019] -- End
                    blnFlag = objAQualificationTran.Insert(CInt(Session("Companyunkid")), , , mdtQualificationDocument)
                    If blnFlag = False AndAlso objAQualificationTran._Message <> "" Then
                        msg.DisplayMessage(objAQualificationTran._Message, Me)
                        Exit Sub
                    Else
                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                                           ConfigParameter._Object._UserAccessModeSetting, _
                                                           Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                                           CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                                                           enScreenName.frmQualificationsList, ConfigParameter._Object._EmployeeAsOnDate, _
                                                           CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                           Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.EDITED, , cboEmployee.SelectedValue.ToString(), , , _
                                                           ExtraFilter, Nothing, False, clsEmployeeAddress_approval_tran.enAddressType.DOMICILE, ExtraFilterForQuery)

                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("Qualification successfully added.", Me)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2005, "Qualification successfully added."), Me)
                        'Hemant (31 May 2019) -- End
                    End If
                Else
                    blnFlag = objEmpQualif.Insert(mdtQualificationDocument)
                    If blnFlag = False Then
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'eZeeMsgBox.Show(objEmpQualif._Message, enMsgBoxStyle.Information)
                        msg.DisplayMessage(objEmpQualif._Message, Me)
                        'Sohail (23 Mar 2019) -- End

                    Else
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("Qualification successfully added.", Me)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2005, "Qualification successfully added."), Me)
                        'Hemant (31 May 2019) -- End
                        'S.SANDEEP [ 18 SEP 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        If objEmpQualif._Message.Trim.Length <= 0 Then
                            trd = New Thread(AddressOf Send_Notification)
                            trd.IsBackground = True
                            trd.Start()
                        End If
                    End If
                End If
                'Gajanan [17-DEC-2018] -- End


                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                Call DeleteTempFile()
                'SHANI (20 JUN 2015) -- End 

                Call ClearObject()

                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                Call FillQualificationAttachment()
                'SHANI (20 JUN 2015) -- End 

            Else

                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                'If objEmpQualif.Update = False Then

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If objEmpQualif.Update(mdtQualificationDocument) = False Then
                '    'SHANI (20 JUN 2015) -- End
                '    msg.DisplayMessage(objEmpQualif._Message, Me)
                'Else
                '    msg.DisplayMessage("Qualification successfully updated", Me)
                '    'S.SANDEEP [ 18 SEP 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    If objEmpQualif._Message.Trim.Length <= 0 Then
                '        trd = New Thread(AddressOf Send_Notification)
                '        trd.IsBackground = True
                '        trd.Start()
                '    End If
                '    'S.SANDEEP [ 18 SEP 2012 ] -- END

                '    'SHANI (20 JUN 2015) -- Start
                '    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                '    Call DeleteTempFile()
                '    'SHANI (20 JUN 2015) -- End 

                '    'Nilay (02-Mar-2015) -- Start
                '    'Enhancement - REDESIGN SELF SERVICE.
                '    'Response.Redirect("wPgEmpQualificationList.aspx")
                '    Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpQualificationList.aspx")
                '    'Nilay (02-Mar-2015) -- End
                'End If

                'Gajanan [3-April-2019] -- Start




                'If QualificationApprovalFlowVal Is Nothing Then
                If QualificationApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                    'Gajanan [3-April-2019] -- End
                    blnFlag = objAQualificationTran.Insert(CInt(Session("Companyunkid")), Nothing, False, mdtQualificationDocument)
                    If blnFlag = False AndAlso objAQualificationTran._Message <> "" Then
                        msg.DisplayMessage(objAQualificationTran._Message, Me)
                        Exit Sub

                    Else
                        objApprovalData.SendNotification(1, FinancialYear._Object._DatabaseName, _
                                     ConfigParameter._Object._UserAccessModeSetting, _
                                     Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, _
                                     CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                                     enScreenName.frmQualificationsList, ConfigParameter._Object._EmployeeAsOnDate, _
                                     CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                     Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.ADDED, , cboEmployee.SelectedValue.ToString(), , , _
                                     ExtraFilter, Nothing, False, clsEmployeeAddress_approval_tran.enAddressType.DOMICILE, ExtraFilterForQuery)
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("Qualification successfully updated", Me)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2006, "Qualification successfully updated"), Me)
                        'Hemant (31 May 2019) -- End
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpQualificationList.aspx", False)
                    End If
                Else
                    If objEmpQualif.Update(mdtQualificationDocument) = False Then
                        msg.DisplayMessage(objEmpQualif._Message, Me)
                    Else
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("Qualification successfully updated", Me)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2006, "Qualification successfully updated"), Me)
                        'Hemant (31 May 2019) -- End
                        If objEmpQualif._Message.Trim.Length <= 0 Then
                            trd = New Thread(AddressOf Send_Notification)
                            trd.IsBackground = True
                            trd.Start()
                        End If
                        Call DeleteTempFile()
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpQualificationList.aspx", False)
                    End If
                End If
                'Gajanan [17-DEC-2018] -- End


            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.

    'Nilay (02-Mar-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Nilay (02-Mar-2015) -- End
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            '        Response.Redirect("~\UserHome.aspx", False)


            'Gajanan [21-June-2019] -- Start      



            'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpQualificationList.aspx")
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpQualificationList.aspx", False)
            Response.Redirect(mstrURLReferer, False)
            'Sohail (09 Nov 2020) -- End
            'Gajanan [21-June-2019] -- End
            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnClose_Click Event : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub
    'SHANI [01 FEB 2015]--END

    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            'Gajanan [21-June-2019] -- Start      
            'If IsDataValid(True) = False Then Exit Sub
            If IsDataValid(True) = False Then Exit Sub
            'Gajanan [21-June-2019] -- End
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 35, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                mdtQualificationDocument = CType(Session("mdtQualificationDocument"), DataTable)
                AddDocumentAttachment(f, f.FullName)
                Call FillQualificationAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'SHANI (20 JUN 2015) -- End 


    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgvQualification.Items.Count <= 0 Then
                'Hemant (31 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                'msg.DisplayMessage("No Files to download.", Me)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2007, "No Files to download."), Me)
                'Hemant (31 May 2019) -- End
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & cboEmployee.SelectedItem.Text.Replace(" ", "") + ".zip", mdtQualificationDocument, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

#End Region

#Region " Control Event(s) "

    Protected Sub drpSkillCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboQualifGrp.SelectedIndexChanged
        Try
            Dim dsCombos As New DataSet
            Dim objQMaster As New clsqualification_master
            dsCombos = objQMaster.GetComboList("List", True, CInt(cboQualifGrp.SelectedValue))
            With cboQualifcation
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect("wPgEmpQualificationList.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Sohail (21 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    Protected Sub cboQualifcation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboQualifcation.SelectedIndexChanged
        Try
            Dim dsCombos As New DataSet
            Dim objQMaster As New clsqualification_master
            dsCombos = objQMaster.GetResultCodeFromQualification(cboQualifcation.SelectedValue, True)
            With cboResultCode
                .DataValueField = "resultunkid"
                .DataTextField = "resultname"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Sohail (21 Mar 2012) -- End

    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Protected Sub dgvQualification_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvQualification.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(5).Text) > 0 Then
                    xrow = mdtQualificationDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(5).Text) & "")
                Else
                    xrow = mdtQualificationDocument.Select("GUID = '" & e.Item.Cells(4).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 39, "Are you sure you want to delete this attachment?")
                        popup_YesNo.Show()
                        'SHANI (27 JUL 2015) -- Start
                        'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
                        'xrow(0).Item("AUD") = "D"
                        'Call FillQualificationAttachment()
                        Me.ViewState("DeleteAttRowIndex") = mdtQualificationDocument.Rows.IndexOf(xrow(0))
                        'SHANI (27 JUL 2015) -- End 
                    End If
                ElseIf e.CommandName = "imgdownload" Then
                    Dim xPath As String = ""
                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        'SHANI (16 JUL 2015) -- Start
                        'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                        'Upload Image folder to access those attachemts from Server and 
                        'all client machines if ArutiSelfService is installed otherwise save then on Document path
                        'xPath = xrow(0).Item("file_path").ToString
                        xPath = xrow(0).Item("filepath").ToString
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                        'SHANI (16 JUL 2015) -- End 
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If
                    If xPath.Trim <> "" Then
                        Dim fileInfo As New IO.FileInfo(xPath)
                        If fileInfo.Exists = False Then
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'msg.DisplayError(ex, Me)
                            'Hemant (31 May 2019) -- Start
                            'ISSUE/ENHANCEMENT : UAT Changes
                            'msg.DisplayMessage("File does not Exist...", Me)
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2008, "File does not Exist..."), Me)
                            'Hemant (31 May 2019) -- End
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                        fileInfo = Nothing
                        Dim strFile As String = xPath
                        Response.ContentType = "image/jpg/pdf"
                        Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(1).Text & """")
                        Response.Clear()
                        Response.TransmitFile(strFile)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvQualification_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvQualification.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                DirectCast(Master.FindControl("script1"), ScriptManager).RegisterPostBackControl(e.Item.Cells(3).FindControl("DownloadLink"))
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)       'Hemant (13 Aug 2020)
        End Try
    End Sub
    'SHANI (20 JUN 2015) -- End 

    'SHANI (27 JUL 2015) -- Start
    'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                Me.ViewState("DeleteAttRowIndex") = Nothing
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                mdtQualificationDocument.Rows(CInt(Me.ViewState("DeleteAttRowIndex")))("AUD") = "D"
                mdtQualificationDocument.AcceptChanges()
                Call FillQualificationAttachment()
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'SHANI (27 JUL 2015) -- End 

#End Region


    'Pinkal (25-APR-2012) -- Start
    'Enhancement : TRA Changes

    Protected Sub chkOtherQualification_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOtherQualification.CheckedChanged
        Try
            If chkOtherQualification.Checked = False Then
                mltView.ActiveViewIndex = 0
                'S.SANDEEP [23 JUL 2016] -- START
                'CCK- OTHER QUALIFICATION CHANGE
                pnlOtherInstitute.Visible = False
                cboProvider.Visible = True
                'S.SANDEEP [23 JUL 2016] -- END
            Else
                'S.SANDEEP [23 JUL 2016] -- START
                'CCK- OTHER QUALIFICATION CHANGE
                pnlOtherInstitute.Visible = True
                cboProvider.Visible = False
                'S.SANDEEP [23 JUL 2016] -- END
                mltView.ActiveViewIndex = 1
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (25-APR-2012) -- End


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            'SHANI [01 FEB 2015]--END

            Me.lblQualification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQualification.ID, Me.lblQualification.Text)
            Me.lblQualificationGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQualificationGroup.ID, Me.lblQualificationGroup.Text)
            Me.lblStartDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStartDate.ID, Me.lblStartDate.Text)
            Me.lblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRemark.ID, Me.lblRemark.Text)
            Me.lblInstitution.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInstitution.ID, Me.lblInstitution.Text)
            Me.lblReferenceNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReferenceNo.ID, Me.lblReferenceNo.Text)
            Me.lblAwardDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAwardDate.ID, Me.lblAwardDate.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblEndDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEndDate.ID, Me.lblEndDate.Text)
            Me.lblGPAcode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGPAcode.ID, Me.lblGPAcode.Text)
            Me.lblResultCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblResultCode.ID, Me.lblResultCode.Text)
            Me.lblOtherResultCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblOtherResultCode.ID, Me.lblOtherResultCode.Text)
            Me.lblOtherQualificationGrp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblOtherQualificationGrp.ID, Me.lblOtherQualificationGrp.Text)
            Me.lblOtherQualification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblOtherQualification.ID, Me.lblOtherQualification.Text)
            Me.chkOtherQualification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "lnkOtherQualification", Me.chkOtherQualification.Text)

            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            'Hemant (30 Nov 2018) -- Start
            'Enhancement : Including Language Settings For Scan/Attachment Button
            btnAddAttachment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAddAttachment.ID, Me.btnAddAttachment.Text).Replace("&", "")
            btnAddFile.Value = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAddAttachment.ID, Me.btnAddAttachment.Text).Replace("&", "")
            'Hemant (30 Nov 2018) -- End
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End


    End Sub

    'Pinkal (06-May-2014) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Qualification Group is compulsory information. Please select Qualification Group to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Qualification is compulsory information. Please select Qualification to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "End date cannot be less or equal to start date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Result Code is compulsory information. Please select Result Code to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Other Qualification Group cannot be blank.Other Qualification Group is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Other Qualification cannot be blank.Other Qualification is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Other Result Code cannot be blank.Other Result Code is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 26, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 29, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in edit mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 30, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 31, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in deleted mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 32, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 33, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in added mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 34, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 39, "Are you sure you want to delete this attachment?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2001, "Provider is compulsory information. Please select Provider to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2002, "Provider is compulsory information. Please select Provider to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2003, "GPA code cannot be greater than 100.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2004, "File does not exist on localpath")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2005, "Qualification successfully added.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2006, "Qualification successfully updated")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2007, "No Files to download.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2008, "File does not Exist...")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsEmp_Qualification_Tran", 1, "This Qualification is already assigned to particular employee. Please assign new Qualification.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Selected information is already present for particular employee.")

        Catch Ex As Exception
            msg.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
