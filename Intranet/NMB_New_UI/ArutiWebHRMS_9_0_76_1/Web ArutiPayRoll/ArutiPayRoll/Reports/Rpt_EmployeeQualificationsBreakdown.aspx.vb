﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Rpt_EmployeeQualificationsBreakdown
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objEmployeeQualificationsBreakdown As clsEmployeeQualificationsBreakdown

    Private ReadOnly mstrModuleName As String = "frmEmployeeQualificationsBreakdown"

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjCMaster As New clsCommon_Master
        Dim ObjQMaster As New clsqualification_master
        Dim ObjIMaster As New clsinstitute_master
        Dim dsList As New DataSet
        Try
            dsList = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                True, "Emp", True)

            With drpEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = ObjCMaster.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "QCAT")
            With drpQualiGrp
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("QCAT")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = ObjQMaster.GetComboList("QMast", True)
            With drpQualification
                .DataValueField = "Id"
                .DataTextField = "NAME"
                .DataSource = dsList.Tables("QMast")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = ObjIMaster.getListForCombo(False, "Institute", True)
            With drpAward
                .DataValueField = "instituteunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Institute")
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            ObjEmp = Nothing
            ObjCMaster = Nothing
            ObjQMaster = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            drpEmployee.SelectedValue = 0
            drpQualification.SelectedValue = 0
            drpQualiGrp.SelectedValue = 0
            drpAward.SelectedValue = 0
            objEmployeeQualificationsBreakdown.setDefaultOrderBy(0)
            chkInActiveEmp.Checked = False
            chkAppointmentDate.Checked = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEmployeeQualificationsBreakdown.SetDefaultValue()
            objEmployeeQualificationsBreakdown._EmployeeId = drpEmployee.SelectedValue
            objEmployeeQualificationsBreakdown._EmployeeName = drpEmployee.SelectedItem.Text
            objEmployeeQualificationsBreakdown._QualificationGrpId = drpQualiGrp.SelectedValue
            objEmployeeQualificationsBreakdown._QualificationGrpName = drpQualiGrp.SelectedItem.Text
            objEmployeeQualificationsBreakdown._QualificationId = drpQualification.SelectedValue
            objEmployeeQualificationsBreakdown._QualificationName = drpQualification.SelectedItem.Text
            objEmployeeQualificationsBreakdown._InstituteId = drpAward.SelectedValue
            objEmployeeQualificationsBreakdown._InstituteName = drpAward.SelectedItem.Text
            objEmployeeQualificationsBreakdown._IsActive = chkInActiveEmp.Checked
            objEmployeeQualificationsBreakdown._IsAppointmentDate = chkAppointmentDate.Checked

            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objEmployeeQualificationsBreakdown._IncludeAccessFilterQry = False
            End If

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            objEmployeeQualificationsBreakdown = New clsEmployeeQualificationsBreakdown(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            SetLanguage()

            If Not IsPostBack Then
                Call FillCombo()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objEmployeeQualificationsBreakdown.setDefaultOrderBy(0)


            Call SetDateFormat()
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim ExportReportPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            Dim OpenAfterExport As Boolean = False

            objEmployeeQualificationsBreakdown.Generate_DetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                    CInt(Session("CompanyUnkId")), _
                                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                    Session("UserAccessModeSetting").ToString(), True, False, ExportReportPath, _
                                                                    OpenAfterExport)


            If objEmployeeQualificationsBreakdown._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objEmployeeQualificationsBreakdown._FileNameAfterExported
                Export.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblInstitute.Text = Language._Object.getCaption(Me.lblInstitute.ID, Me.lblInstitute.Text)
            Me.lblQualification.Text = Language._Object.getCaption(Me.lblQualification.ID, Me.lblQualification.Text)
            Me.lblQualifyGroup.Text = Language._Object.getCaption(Me.lblQualifyGroup.ID, Me.lblQualifyGroup.Text)
            Me.chkInActiveEmp.Text = Language._Object.getCaption(Me.chkInActiveEmp.ID, Me.chkInActiveEmp.Text)
            Me.chkAppointmentDate.Text = Language._Object.getCaption(Me.chkAppointmentDate.ID, Me.chkAppointmentDate.Text)

            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.BtnReset.Text = Language._Object.getCaption(Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
            Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
End Class
