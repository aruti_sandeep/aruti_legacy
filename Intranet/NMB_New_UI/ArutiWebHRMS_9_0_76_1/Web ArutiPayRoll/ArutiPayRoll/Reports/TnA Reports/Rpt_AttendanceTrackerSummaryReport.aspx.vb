﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_TnA_Reports_Rpt_AttendanceTrackerSummaryReport
    Inherits Basepage

#Region " Private Variables "

    Private mstrModuleName As String = "frmAttendanceTrackerSummaryReport"
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mstrAdvanceFilter As String = ""
    Private objEmpAttTrkSummary As clsAttendanceTrackerSummaryReport
    Private DisplayMessage As New CommonCodes
    Private mdtExcuseLv As DataTable

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objFromPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Try
            dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Emp", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            dsList = objFromPeriod.getListForCombo(enModuleReference.Payroll, _
                                               CInt(Session("Fin_year")), _
                                               Session("Database_Name"), _
                                               Session("fin_startdate"), "From_Period", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With            

            dsList = objMaster.GetEAllocation_Notification("List", "", False, False)
            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("Id") = 0
            drRow("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 103, "Select")
            dsList.Tables(0).Rows.InsertAt(drRow, 0)
            With cboAllocation
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing : objFromPeriod = Nothing : dsList.Dispose() : objMaster = Nothing
        End Try
    End Sub

    Private Sub FillLeave()
        Dim objlvType As New clsleavetype_master
        Dim dsList As New DataSet
        Try
            dsList = objlvType.GetList("List", True, True, "ISNULL(IsExcuseLeave, 0) = 1 ")

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim iTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "leavetypeunkid", "leavename").Copy()
                Dim iCol As New DataColumn
                With iCol
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                    .ColumnName = "displayvalue"
                End With
                iTable.Columns.Add(iCol)

                For Each iRow As DataRow In iTable.Rows
                    iRow("displayvalue") = iRow("leavename")
                Next
                iTable.AcceptChanges()

                dgvLeave.AutoGenerateColumns = False

                mdtExcuseLv = iTable
                dgvLeave.DataSource = iTable
                dgvLeave.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetValue() As Boolean
        Try
            objEmpAttTrkSummary.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 100, "Sorry, Period is mandatory information. Please select period to continue."), Me)
                Return False
            End If

            If dgvLeave.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 200, "Sorry, Excuse Leave is not defined. Please defined excuse leave in order to continue."), Me)
                Return False
            End If

            objEmpAttTrkSummary._Advance_Filter = mstrAdvanceFilter
            objEmpAttTrkSummary._AllocationId = cboAllocation.SelectedValue
            objEmpAttTrkSummary._AllocationName = cboAllocation.SelectedItem.Text
            objEmpAttTrkSummary._ConsiderZeroAsValue = chkConsiderZeroFilter.Checked
            objEmpAttTrkSummary._DaysFrom = nudDayFrom.Text
            objEmpAttTrkSummary._DaysTo = nudDayTo.Text
            objEmpAttTrkSummary._DefaultWorkingDays = nudDaysWorked.Text
            objEmpAttTrkSummary._EmployeeID = cboEmployee.SelectedValue
            objEmpAttTrkSummary._EmployeeName = cboEmployee.SelectedItem.Text
            objEmpAttTrkSummary._EndDate = mdtEndDate
            Dim iCaptions As New Dictionary(Of Integer, String)
            iCaptions = dgvLeave.Rows.Cast(Of GridViewRow).AsEnumerable().ToDictionary(Function(x) CInt(dgvLeave.DataKeys(x.RowIndex)("leavetypeunkid")), Function(x) x.Cells(getColumnID_Griview(dgvLeave, "dgcolhCaption", False, True)).Text)
            objEmpAttTrkSummary._ExcuseCaption = iCaptions
            objEmpAttTrkSummary._PeriodId = cboPeriod.SelectedValue
            objEmpAttTrkSummary._PeriodName = cboPeriod.SelectedItem.Text
            objEmpAttTrkSummary._ShowOnlyLateArrivalHours = chkShowLateArrivalhours.Checked
            objEmpAttTrkSummary._StartDate = mdtStartDate


            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            Call cboPeriod_SelectedIndexChanged(New Object, New EventArgs)
            cboEmployee.SelectedValue = 0
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mstrAdvanceFilter = String.Empty
            nudDaysWorked.Text = 20
            nudDayFrom.Text = 0
            nudDayTo.Text = ""
            chkConsiderZeroFilter.Checked = False
            chkShowLateArrivalhours.Checked = False
            GetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValue()
        Dim dsValue As New DataSet
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            dsValue = objUserDefRMode.GetList("List", enArutiReport.Attendance_Tracker_Summary_Report)
            If dsValue.Tables.Count > 0 Then
                Dim ival() As DataRow = dsValue.Tables(0).Select("reporttypeid = -111")
                If ival IsNot Nothing AndAlso ival.Length > 0 Then
                    nudDaysWorked.Text = CInt(ival(0)("transactionheadid"))
                End If
            End If

            If dsValue.Tables.Count > 0 Then
                Dim ival() As DataRow = dsValue.Tables(0).Select("reporttypeid = -222")
                If ival IsNot Nothing AndAlso ival.Length > 0 Then
                    cboAllocation.SelectedValue = CInt(ival(0)("transactionheadid"))
                End If
            End If

            If dgvLeave.Rows.Count <= 0 Then Exit Sub
            If dsValue.Tables.Count > 0 Then
                If dsValue.Tables(0).Rows.Count > 0 Then
                    For Each sRow As DataRow In dsValue.Tables(0).Rows
                        Dim gRow As GridViewRow = dgvLeave.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CInt(dgvLeave.DataKeys(x.RowIndex)("leavetypeunkid")) = CInt(sRow("reporttypeid"))).Select(Function(x) x).FirstOrDefault()
                        If gRow IsNot Nothing Then dgvLeave.Rows(gRow.RowIndex).Cells(getColumnID_Griview(dgvLeave, "dgcolhCaption", False, True)).Text = sRow("transactionheadid")
                        If mdtExcuseLv IsNot Nothing AndAlso gRow IsNot Nothing Then
                            mdtExcuseLv.Rows(gRow.RowIndex)("displayvalue") = sRow("transactionheadid")
                            mdtExcuseLv.AcceptChanges()
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objEmpAttTrkSummary = New clsAttendanceTrackerSummaryReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                Call FillCombo()
                Call FillLeave()
                Call GetValue()
            Else
                mdtStartDate = Me.ViewState("mdtStartDate")
                mdtEndDate = Me.ViewState("mdtEndDate")
                mstrAdvanceFilter = Me.ViewState("mstrAdvanceFilter")
                mdtExcuseLv = Me.ViewState("mdtExcuseLv")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mdtStartDate") = mdtStartDate
            Me.ViewState("mdtEndDate") = mdtEndDate
            Me.ViewState("mstrAdvanceFilter") = mstrAdvanceFilter
            Me.ViewState("mdtExcuseLv") = mdtExcuseLv
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetValue() = False Then Exit Sub
            Call SetDateFormat()
            Dim strFileName As String = String.Empty
            objEmpAttTrkSummary.Generate_DetailReport(Session("Database_Name").ToString, _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             mdtStartDate, _
                                             mdtEndDate, _
                                             CStr(Session("UserAccessModeSetting")), True, _
                                             IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                             False, 0, enPrintAction.None, enExportAction.ExcelExtra)

            If strFileName.Trim.Length <= 0 Then strFileName = objEmpAttTrkSummary._FileNameAfterExported.Trim
            If strFileName.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName
                Export.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAdvFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdvFilter.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "AEM"
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Button "

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1
        Dim mblnFlag As Boolean = False
        Try
            objUserDefRMode._Reportunkid = enArutiReport.Attendance_Tracker_Summary_Report
            objUserDefRMode._Reporttypeid = -111
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 1
            objUserDefRMode._EarningTranHeadIds = nudDaysWorked.Text.ToString()
            intUnkid = objUserDefRMode.isExist(enArutiReport.Attendance_Tracker_Summary_Report, objUserDefRMode._Reporttypeid, 0, objUserDefRMode._Headtypeid)
            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                mblnFlag = objUserDefRMode.Insert()
            Else
                mblnFlag = objUserDefRMode.Update()
            End If

            objUserDefRMode._Reportunkid = enArutiReport.Attendance_Tracker_Summary_Report
            objUserDefRMode._Reporttypeid = -222
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 1
            objUserDefRMode._EarningTranHeadIds = CInt(cboAllocation.SelectedValue)
            intUnkid = objUserDefRMode.isExist(enArutiReport.Attendance_Tracker_Summary_Report, objUserDefRMode._Reporttypeid, 0, objUserDefRMode._Headtypeid)
            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                mblnFlag = objUserDefRMode.Insert()
            Else
                mblnFlag = objUserDefRMode.Update()
            End If

            For Each gRow As GridViewRow In dgvLeave.Rows
                objUserDefRMode._Reportunkid = enArutiReport.Attendance_Tracker_Summary_Report
                objUserDefRMode._Reporttypeid = CInt(dgvLeave.DataKeys(gRow.RowIndex)("leavetypeunkid"))
                objUserDefRMode._Reportmodeid = 0
                objUserDefRMode._Headtypeid = 1
                objUserDefRMode._EarningTranHeadIds = gRow.Cells(getColumnID_Griview(dgvLeave, "dgcolhCaption", False, True)).Text

                intUnkid = objUserDefRMode.isExist(enArutiReport.Attendance_Tracker_Summary_Report, objUserDefRMode._Reporttypeid, 0, objUserDefRMode._Headtypeid)

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    mblnFlag = objUserDefRMode.Insert()
                Else
                    mblnFlag = objUserDefRMode.Update()
                End If
            Next

            If mblnFlag Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Selection Saved Successfully."), Me)
            Else
                eZeeMsgBox.Show(objUserDefRMode._Message)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtStartDate = objPeriod._TnA_StartDate.Date
                mdtEndDate = objPeriod._TnA_EndDate.Date
                objlblPDate.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 101, "Date Range :") & " " & mdtStartDate.ToShortDateString() & " - " & mdtEndDate.ToShortDateString()
                nudDaysWorked.Max = objPeriod._Constant_Days
                nudDayFrom.Max = objPeriod._Constant_Days
                nudDayTo.Max = objPeriod._Constant_Days
            Else
                mdtStartDate = Nothing
                mdtEndDate = Nothing
                objlblPDate.Text = ""
                nudDaysWorked.Max = 31
                nudDayFrom.Max = 31
                nudDayTo.Max = 31
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Gridview Event(s) "

    Protected Sub dgvLeave_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgvLeave.RowEditing
        Try
            dgvLeave.EditIndex = e.NewEditIndex
            dgvLeave.DataSource = mdtExcuseLv
            dgvLeave.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgvLeave_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles dgvLeave.RowUpdating
        Try
            Dim row As GridViewRow = dgvLeave.Rows(e.RowIndex)
            mdtExcuseLv.Rows(e.RowIndex)("displayvalue") = CType(row.Cells(getColumnID_Griview(dgvLeave, "dgcolhCaption", False, True)).Controls(0), TextBox).Text
            mdtExcuseLv.AcceptChanges()
            'row.Cells(getColumnID_Griview(dgvLeave, "dgcolhCaption", False, True)).Text = CType(row.Cells(getColumnID_Griview(dgvLeave, "dgcolhCaption", False, True)).Controls(0), TextBox).Text
            dgvLeave.EditIndex = -1
            dgvLeave.DataSource = mdtExcuseLv
            dgvLeave.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgvLeave_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles dgvLeave.RowCancelingEdit
        Try
            dgvLeave.EditIndex = -1
            dgvLeave.DataSource = mdtExcuseLv
            dgvLeave.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Language "
    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)

            Me.LblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblPeriod.ID, Me.LblPeriod.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAllocation.ID, Me.lblAllocation.Text)
            Me.lblDayFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDayFrom.ID, Me.lblDayFrom.Text)
            Me.lblExpetedDays.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExpetedDays.ID, Me.lblExpetedDays.Text)
            Me.lblYTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblYTo.ID, Me.lblYTo.Text)

            dgvLeave.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvLeave.Columns(1).FooterText, dgvLeave.Columns(1).HeaderText)
            dgvLeave.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvLeave.Columns(2).FooterText, dgvLeave.Columns(2).HeaderText)

            Me.chkConsiderZeroFilter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkConsiderZeroFilter.ID, Me.chkConsiderZeroFilter.Text).Replace("&", "")
            Me.chkShowLateArrivalhours.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowLateArrivalhours.ID, Me.chkShowLateArrivalhours.Text).Replace("&", "")
            Me.btnAdvFilter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAdvFilter.ID, Me.btnAdvFilter.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

End Class
