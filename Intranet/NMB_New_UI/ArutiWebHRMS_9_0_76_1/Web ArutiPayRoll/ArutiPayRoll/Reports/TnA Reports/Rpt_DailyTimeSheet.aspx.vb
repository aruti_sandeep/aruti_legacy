﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.Globalization

#End Region

Partial Class Reports_TnA_Reports_Rpt_DailyTimeSheet
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private mstrModuleName As String = "frmDailyTimeSheet"
    Private objDailyTimeSheet As clsDailyTimeSheet
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Private Function "

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), _
                                            True, CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "employeename"
                    .DataSource = dsList.Tables(0)
                    .SelectedValue = "0"
                    .DataBind()
                End With
                objEmp = Nothing
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee.Copy
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                End With
            End If
            dsList = Nothing

            Dim objCMaster As New clsCommon_Master
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "List")
            With cboShiftType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = "0"
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Daily Timesheet"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Daily Timesheet On Duty"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Daily Timesheet Off Duty"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Manual Attendance Register"))
                .SelectedIndex = 0
                .DataBind()
            End With

            Dim objMData As New clsMasterData
            dsList = objMData.GetEAllocation_Notification("List")
            Dim drow As DataRow = dsList.Tables("List").NewRow
            drow.Item("Id") = 0
            drow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Select Allocation for Report")
            dsList.Tables("List").Rows.InsertAt(drow, 0)

            If Session("CompanyGroupName").ToString().ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                Dim iRowIdx As Integer = -1
                iRowIdx = dsList.Tables("List").Rows.IndexOf(dsList.Tables("List").Select("Id = " & enAllocation.DEPARTMENT)(0))
                dsList.Tables("List").Rows.RemoveAt(iRowIdx)
            End If
            With cboReportColumn
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtDate.SetDate = DateTime.Now
            cboEmployee.SelectedValue = "0"
            cboShift.SelectedValue = "0"
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            cboShiftType.SelectedValue = "0"
            mstrAdvanceFilter = ""
            chkInactiveemp.Checked = False
            cboReportType.SelectedIndex = 0
            If Session("CompanyGroupName").ToString().ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                If CInt(Session("SelectedAllocationForDailyTimeSheetReport_Voltamp")) = enAllocation.DEPARTMENT Then
                    cboReportColumn.SelectedValue = "0"
                Else
                    cboReportColumn.SelectedValue = CStr(Session("SelectedAllocationForDailyTimeSheetReport_Voltamp"))
                End If
            Else
                cboReportColumn.SelectedValue = CStr(Session("SelectedAllocationForDailyTimeSheetReport_Voltamp"))
            End If
            chkShowEmployeeStatus.Checked = CBool(Session("ShowEmployeeStatusForDailyTimeSheetReport_Voltamp"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objDailyTimeSheet.SetDefaultValue()

            If Session("CompanyGroupName").ToString().ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                If cboReportColumn.SelectedIndex <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry, Location is mandatory information. Please select location to continue."), Me)
                    Return False
                End If
            End If
            objDailyTimeSheet._Date = dtDate.GetDate
            objDailyTimeSheet._EmpId = CInt(cboEmployee.SelectedValue)
            objDailyTimeSheet._EmpName = cboEmployee.Text
            objDailyTimeSheet._WorkedHourFrom = ""
            objDailyTimeSheet._WorkedHourTo = ""
            objDailyTimeSheet._IsActive = chkInactiveemp.Checked
            objDailyTimeSheet._Shiftunkid = CInt(cboShift.SelectedValue)
            objDailyTimeSheet._ShiftName = cboShift.Text
            objDailyTimeSheet._ViewByIds = mstrStringIds
            objDailyTimeSheet._ViewIndex = mintViewIdx
            objDailyTimeSheet._ViewByName = mstrStringName
            objDailyTimeSheet._Analysis_Fields = mstrAnalysis_Fields
            objDailyTimeSheet._Analysis_Join = mstrAnalysis_Join
            objDailyTimeSheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            objDailyTimeSheet._Report_GroupName = mstrReport_GroupName
            objDailyTimeSheet._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objDailyTimeSheet._ReportTypeName = cboReportType.Text
            objDailyTimeSheet._SelectedAllocationId = CInt(cboReportColumn.SelectedValue)
            objDailyTimeSheet._AllocationName = cboReportColumn.Text
            objDailyTimeSheet._ShowEmployeeStatus = chkShowEmployeeStatus.Checked
            objDailyTimeSheet._ShiftTypeId = CInt(cboShiftType.SelectedValue)
            objDailyTimeSheet._ShiftTypeName = cboShiftType.Text
            objDailyTimeSheet._AdvanceFilter = mstrAdvanceFilter
            objDailyTimeSheet._GroupName = Session("CompanyGroupName").ToString()
            
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Page Event(S) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            objDailyTimeSheet = New clsDailyTimeSheet(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                Call SetLanguage()
                Call FillCombo()
                Call ResetValue()
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    chkInactiveemp.Visible = False
                    lnkSaveChanges.Visible = False
                    lnkSetAnalysis.Visible = False
                End If

                If Session("CompanyGroupName").ToString().ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                    lblAllocation.Visible = False
                    lblLocation.Visible = True
                    cboReportType.Enabled = False
                Else
                    lblLocation.Visible = False
                End If
            Else
                mstrStringIds = CStr(Me.ViewState("mstrStringIds"))
                mstrStringName = CStr(Me.ViewState("mstrStringName"))
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysis_Join = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysis_OrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReport_GroupName = CStr(Me.ViewState("mstrReport_GroupName "))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName ") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button Event(S) "

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()

            objDailyTimeSheet._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objDailyTimeSheet._UserUnkId = CInt(Session("UserId"))
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objDailyTimeSheet._ApplyAddUACFilter = False
                objDailyTimeSheet._UserName = Session("DisplayName").ToString()
            End If
            objDailyTimeSheet.generateReportNew(Session("Database_Name").ToString, _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                    CStr(Session("ExportReportPath")), _
                                                    CBool(Session("OpenAfterExport")), 0, enPrintAction.None, _
                                                    enExportAction.None, _
                                                    CInt(Session("Base_CurrencyId")))

            Session("objRpt") = objDailyTimeSheet._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrStringIds = popAnalysisby._ReportBy_Ids
            mstrStringName = popAnalysisby._ReportBy_Name
            mintViewIdx = popAnalysisby._ViewIndex
            mstrAnalysis_Fields = popAnalysisby._Analysis_Fields
            mstrAnalysis_Join = popAnalysisby._Analysis_Join
            mstrAnalysis_OrderBy = popAnalysisby._Analysis_OrderBy
            mstrReport_GroupName = popAnalysisby._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Language "

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lnkSetAnalysis.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)
            Me.LblShift.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblShift.ID, Me.LblShift.Text)
            Me.lblEmpName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.lblLocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLocation.ID, Me.lblLocation.Text)
            Me.lblAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAllocation.ID, Me.lblAllocation.Text)
            Me.lblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDate.ID, Me.lblDate.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)
            Me.lblShiftType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblShiftType.ID, Me.lblShiftType.Text)
            Me.lnkSaveChanges.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkSaveChanges.ID, Me.lnkSaveChanges.Text)
            Me.chkShowEmployeeStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowEmployeeStatus.ID, Me.chkShowEmployeeStatus.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class
