﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_TnA_Reports_Rpt_TRAAbsenteesExcuseReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Dim objTRAAbsenteesExcuseReport As clsTRAAbsenteesExcuseReport
    Private ReadOnly mstrModuleName As String = "frmTRAAbsenteesExcuseReport"
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If
            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            Session("UserAccessModeSetting").ToString, True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                            blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmp = Nothing


            Dim objLeaveType As New clsleavetype_master
            dsList = objLeaveType.getListForCombo("List", True, -1, HttpContext.Current.Session("Database_Name").ToString(), "lvleavetype_master.isexcuseleave = 1", False)
            With cboExcuse
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            objLeaveType = Nothing
            dsList = Nothing


            If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                dsList = Nothing
                Dim objMaster As New clsMasterData
                dsList = objMaster.GetEAllocation_Notification("List", "", False, False)
                Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id <> " & enAllocation.DEPARTMENT, "", DataViewRowState.CurrentRows).ToTable()
                Dim drRow As DataRow = dtTable.NewRow
                drRow("Id") = 0
                drRow("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Select")
                dtTable.Rows.InsertAt(drRow, 0)
                With cboLocation
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dtTable
                    .DataBind()
                End With
                objMaster = Nothing
                dtTable = Nothing
                dsList = Nothing
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpStartdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                cboEmployee.SelectedValue = CStr(0)
            End If
            cboExcuse.SelectedValue = "0"
            chkShowEachEmpOnNewPage.Checked = True
            mstrAdvanceFilter = ""
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mintViewIndex = -1

            Dim objUserDefRMode As New clsUserDef_ReportMode
            Dim dsList As DataSet = objUserDefRMode.GetList("List", enArutiReport.Employee_Absentees_With_Excuse_Report, 0, 0)
            If dsList.Tables("List").Rows.Count > 0 Then
                Dim ar() As String = dsList.Tables("List").Rows(0).Item("transactionheadid").ToString().Split(CChar("|"))
                If ar.Length > 0 Then
                    cboLocation.SelectedValue = ar(0).ToString()
                    chkShowEachEmpOnNewPage.Checked = CBool(ar(1).ToString())
                End If
            End If
            dsList = Nothing
            objUserDefRMode = Nothing

            objTRAAbsenteesExcuseReport.setDefaultOrderBy(0)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                If CInt(cboLocation.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Location is compulsory information.Please Select Location."), Me)
                    cboLocation.Focus()
                    Return False
                End If
            End If

            objTRAAbsenteesExcuseReport.SetDefaultValue()
            objTRAAbsenteesExcuseReport._YearId = FinancialYear._Object._YearUnkid
            objTRAAbsenteesExcuseReport._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objTRAAbsenteesExcuseReport._UserUnkId = CInt(Session("UserId"))
            objTRAAbsenteesExcuseReport._FromDate = dtpStartdate.GetDate.Date
            objTRAAbsenteesExcuseReport._ToDate = dtpToDate.GetDate.Date
            objTRAAbsenteesExcuseReport._EmpId = CInt(cboEmployee.SelectedValue)
            objTRAAbsenteesExcuseReport._EmpName = cboEmployee.SelectedItem.Text
            objTRAAbsenteesExcuseReport._ExcuseId = CInt(cboExcuse.SelectedValue)
            objTRAAbsenteesExcuseReport._Excuse = cboExcuse.SelectedItem.Text
            objTRAAbsenteesExcuseReport._LocationId = CInt(cboLocation.SelectedValue)
            objTRAAbsenteesExcuseReport._Location = cboLocation.SelectedItem.Text
            objTRAAbsenteesExcuseReport._ShowEachEmployeeOnNewPage = chkShowEachEmpOnNewPage.Checked
            objTRAAbsenteesExcuseReport._AdvanceFilter = mstrAdvanceFilter
            objTRAAbsenteesExcuseReport._ViewByIds = mstrViewByIds
            objTRAAbsenteesExcuseReport._ViewIndex = mintViewIndex
            objTRAAbsenteesExcuseReport._ViewByName = mstrViewByName
            objTRAAbsenteesExcuseReport._Analysis_Fields = mstrAnalysis_Fields
            objTRAAbsenteesExcuseReport._Analysis_Join = mstrAnalysis_Join
            objTRAAbsenteesExcuseReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objTRAAbsenteesExcuseReport._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objTRAAbsenteesExcuseReport._Report_GroupName = mstrReport_GroupName

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                objTRAAbsenteesExcuseReport._IncludeAccessFilterQry = False
                objTRAAbsenteesExcuseReport._UserName = Session("DisplayName").ToString()
            Else
                objTRAAbsenteesExcuseReport._IncludeAccessFilterQry = True
            End If

            SetDateFormat()

            objTRAAbsenteesExcuseReport.setDefaultOrderBy(0)

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function


#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            objTRAAbsenteesExcuseReport = New clsTRAAbsenteesExcuseReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                SetLanguage()
                SetMessages()
                Call FillCombo()
                dtpStartdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                ResetValue()
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    lnkSetAnalysis.Visible = True
                Else
                    lnkSetAnalysis.Visible = False
                    lnkSave.Visible = False
                    chkShowEachEmpOnNewPage.Visible = False
                End If
            Else
                mstrViewByIds = Me.ViewState("mstrStringIds").ToString()
                mstrViewByName = Me.ViewState("mstrStringName").ToString()
                mintViewIndex = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = Me.ViewState("mstrAnalysis_Fields").ToString()
                mstrAnalysis_Join = Me.ViewState("mstrAnalysis_Join").ToString()
                mstrAnalysis_OrderBy = Me.ViewState("mstrAnalysis_OrderBy").ToString()
                mstrAnalysis_OrderBy_GName = Me.ViewState("mstrAnalysis_OrderBy_GName").ToString()
                mstrReport_GroupName = Me.ViewState("mstrReport_GroupName ").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrViewByIds
            Me.ViewState("mstrStringName") = mstrViewByName
            Me.ViewState("mintViewIdx") = mintViewIndex
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrAnalysis_OrderBy_GName") = mstrAnalysis_OrderBy_GName
            Me.ViewState("mstrReport_GroupName ") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objTRAAbsenteesExcuseReport.generateReportNew(Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   dtpStartdate.GetDate.Date, _
                                                   dtpToDate.GetDate.Date, _
                                                   CStr(Session("UserAccessModeSetting")), True, _
                                                   CStr(Session("ExportReportPath")), _
                                                   CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)

            Session("objRpt") = objTRAAbsenteesExcuseReport._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrViewByIds = popAnalysisby._ReportBy_Ids
            mstrViewByName = popAnalysisby._ReportBy_Name
            mintViewIndex = popAnalysisby._ViewIndex
            mstrAnalysis_Fields = popAnalysisby._Analysis_Fields
            mstrAnalysis_Join = popAnalysisby._Analysis_Join
            mstrAnalysis_OrderBy = popAnalysisby._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = popAnalysisby._Analysis_OrderBy_GName
            mstrReport_GroupName = popAnalysisby._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkSave.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            objUserDefRMode = New clsUserDef_ReportMode()
            objUserDefRMode._Reportunkid = enArutiReport.Employee_Absentees_With_Excuse_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 0

            Dim intUnkid As Integer = -1

            objUserDefRMode._EarningTranHeadIds = cboLocation.SelectedValue & "|" & chkShowEachEmpOnNewPage.Checked
            intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_Absentees_With_Excuse_Report, 0, 0, 0)

            objUserDefRMode._Reportmodeunkid = intUnkid
            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If
            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Setting saved successfully."), Me)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Public Sub SetLanguage()
        Try

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objTRAAbsenteesExcuseReport._ReportName)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)

            Me.lnkSetAnalysis.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "lnkAnalysisBy", Me.lnkSetAnalysis.Text)
            Me.LblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFromDate.ID, Me.LblFromDate.Text)
            Me.LblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblToDate.ID, Me.LblToDate.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.LblExcuse.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblExcuse.ID, Me.LblExcuse.Text)
            Me.LblLocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLocation.ID, Me.LblLocation.Text)
            Me.lnkSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkSave.ID, Me.lnkSave.Text)
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Setting saved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Select.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Location is compulsory information.Please Select Location.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
