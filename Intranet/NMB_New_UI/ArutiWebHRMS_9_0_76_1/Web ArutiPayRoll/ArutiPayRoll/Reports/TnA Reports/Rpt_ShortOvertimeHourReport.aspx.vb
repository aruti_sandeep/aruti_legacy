﻿
#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_TnA_Reports_Rpt_ShortOvertimeHourReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Dim objEmployeeTimeSheet As clsEmployeeShort_OT_Report
    Private ReadOnly mstrModuleName As String = "frmEmployeeShort_OT_Report"
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtPYPeriodStartDate As Date = Nothing
    Private mdtPYPeriodEndDate As Date = Nothing
    Private mdtTnAPeriodStartDate As Date = Nothing
    Private mdtTnAPeriodEndDate As Date = Nothing
    Private mintReportTypeId As Integer = 0

#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Short Hours Report"))


            If CInt(Session("Compcountryid")) <> 162 AndAlso CBool(Session("PolicyManagementTNA")) = False Then  'Oman 
                pnlFromToDate.Visible = True
                pnlTnAPeriod.Visible = False
                cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Over Time Report"))

                'Pinkal (22-Dec-2022) -- Start
                '(A1X-349) TRA - Late arrival/Early departure report modification
                chkShowEachEmpOnNewPage.Visible = False
                'Pinkal (22-Dec-2022) -- End

            Else
                pnlFromToDate.Visible = False
                pnlTnAPeriod.Visible = True

                'Pinkal (22-Dec-2022) -- Start
                '(A1X-349) TRA - Late arrival/Early departure report modification
                chkShowEachEmpOnNewPage.Visible = True
                'Pinkal (22-Dec-2022) -- End

                Dim objPeriod As New clscommom_period_Tran
                Dim dsFill As DataSet = Nothing
                dsFill = objPeriod.GetList("Period", enModuleReference.Payroll, CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date, True)
                Dim drRow As DataRow = dsFill.Tables(0).NewRow
                drRow("periodunkid") = 0
                drRow("period_name") = "Select"
                dsFill.Tables(0).Rows.InsertAt(drRow, 0)

                cboTnAPeriod.DataValueField = "periodunkid"
                cboTnAPeriod.DataTextField = "period_name"
                cboTnAPeriod.DataSource = dsFill.Tables(0)
                cboTnAPeriod.DataBind()
            End If

            If mintReportTypeId = enArutiReport.Employee_ShortHourReport Then
                cboReportType.SelectedIndex = 0
            ElseIf mintReportTypeId = enArutiReport.Employee_OvertimeReport Then
                cboReportType.SelectedIndex = 1
            End If
            Me.Title = cboReportType.SelectedItem.Text
            Me.lblPageHeader.Text = cboReportType.SelectedItem.Text
            cboReportType.Enabled = False


            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                             Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            objEmp = Nothing
            dsList = Nothing

            Dim objShift As New clsNewshift_master
            dsList = objShift.getListForCombo("List", True)
            With cboShift
                .DataValueField = "shiftunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboTnAPeriod.SelectedValue = "0"
            chkShowEachEmpOnNewPage.Checked = True
            cboEmployee.SelectedValue = "0"
            cboShift.SelectedValue = "0"
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            txtFromhour.Text = ""
            txtTohour.Text = ""
            chkInactiveemp.Checked = False
            objEmployeeTimeSheet.setDefaultOrderBy(0)
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If CInt(Session("Compcountryid")) = 162 Then 'Oman
                If CInt(cboTnAPeriod.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Period is compulsory information.Please select period."), Me)
                    cboTnAPeriod.Focus()
                    Return False
                End If
            End If

            objEmployeeTimeSheet.SetDefaultValue()

            objEmployeeTimeSheet._ReportTypeId = cboReportType.SelectedIndex + 1
            objEmployeeTimeSheet._ReportTypeName = cboReportType.SelectedItem.Text
            objEmployeeTimeSheet._EmpId = CInt(cboEmployee.SelectedValue)
            objEmployeeTimeSheet._EmpName = cboEmployee.Text

            If Company._Object._Countryunkid <> 162 Then ' Oman 
                objEmployeeTimeSheet._FromDate = dtpFromDate.GetDate.Date
                objEmployeeTimeSheet._ToDate = dtpToDate.GetDate.Date
            Else
                objEmployeeTimeSheet._FromDate = mdtTnAPeriodStartDate.Date
                objEmployeeTimeSheet._ToDate = mdtTnAPeriodEndDate.Date
                objEmployeeTimeSheet._PYPeriodStartDate = mdtPYPeriodStartDate.Date
                objEmployeeTimeSheet._PYPeriodEndDate = mdtPYPeriodEndDate.Date
            End If

            If txtFromhour.Text <> "" Then
                Dim mintSec As Integer
                If txtFromhour.Text.Contains(".") Then
                    mintSec = CInt(txtFromhour.Text.Substring(0, txtFromhour.Text.IndexOf("."))) * 3600
                    mintSec += CInt(txtFromhour.Text.Substring(txtFromhour.Text.IndexOf(".") + 1)) * 60
                Else
                    mintSec = CInt(txtFromhour.Text) * 3600
                End If
                objEmployeeTimeSheet._Fromhrs = mintSec
            End If

            If txtTohour.Text <> "" Then
                Dim mintSec As Integer
                If txtTohour.Text.Contains(".") Then
                    mintSec = CInt(txtTohour.Text.Substring(0, txtTohour.Text.IndexOf("."))) * 3600
                    mintSec += CInt(txtTohour.Text.Substring(txtTohour.Text.IndexOf(".") + 1)) * 60
                Else
                    mintSec = CInt(txtTohour.Text) * 3600
                End If
                objEmployeeTimeSheet._Tohrs = mintSec
            End If


            objEmployeeTimeSheet._IsActive = chkInactiveemp.Checked
            objEmployeeTimeSheet._Shiftunkid = CInt(cboShift.SelectedValue)
            objEmployeeTimeSheet._ShiftName = cboShift.SelectedItem.Text
            objEmployeeTimeSheet._ViewByIds = mstrStringIds
            objEmployeeTimeSheet._ViewIndex = mintViewIdx
            objEmployeeTimeSheet._ViewByName = mstrStringName
            objEmployeeTimeSheet._Analysis_Fields = mstrAnalysis_Fields
            objEmployeeTimeSheet._Analysis_Join = mstrAnalysis_Join
            objEmployeeTimeSheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmployeeTimeSheet._Report_GroupName = mstrReport_GroupName
            objEmployeeTimeSheet._Advance_Filter = mstrAdvanceFilter

            objEmployeeTimeSheet._CountryID = CInt(Session("Compcountryid"))
            If pnlTnAPeriod.Visible Then
                objEmployeeTimeSheet._PeriodID = CInt(cboTnAPeriod.SelectedValue)
                objEmployeeTimeSheet._PeriodName = cboTnAPeriod.SelectedItem.Text
            End If
            objEmployeeTimeSheet._ShowEachEmployeeOnNewPage = chkShowEachEmpOnNewPage.Checked
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function


#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            objEmployeeTimeSheet = New clsEmployeeShort_OT_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then

                If Request.QueryString.Count <= 0 Then Exit Sub
                Try
                    mintReportTypeId = CInt(b64decode(Server.UrlDecode(Request.QueryString.ToString.Substring(3))))
                    objEmployeeTimeSheet.setReportData(mintReportTypeId, CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
                Catch
                    Session("clsuser") = Nothing
                    DisplayMessage.DisplayMessage("Invalid Page Address.", Me, "../../index.aspx")
                    Exit Sub
                End Try

                SetLanguage()
                SetMessages()
                Call FillCombo()
                dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                ResetValue()
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    lnkSetAnalysis.Visible = True
                Else
                    lnkSetAnalysis.Visible = False
                    chkShowEachEmpOnNewPage.Visible = False
                End If
            Else
                mdtPYPeriodStartDate = Me.ViewState("PYPeriodStartDate")
                mdtPYPeriodEndDate = Me.ViewState("PYPeriodEndDate")
                mdtTnAPeriodStartDate = Me.ViewState("TnAPeriodStartDate")
                mdtTnAPeriodEndDate = Me.ViewState("TnAPeriodEndDate")
                mstrStringIds = Me.ViewState("mstrStringIds").ToString()
                mstrStringName = Me.ViewState("mstrStringName").ToString()
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = Me.ViewState("mstrAnalysis_Fields").ToString()
                mstrAnalysis_Join = Me.ViewState("mstrAnalysis_Join").ToString()
                mstrAnalysis_OrderBy = Me.ViewState("mstrAnalysis_OrderBy").ToString()
                mstrReport_GroupName = Me.ViewState("mstrReport_GroupName ").ToString()
                mintReportTypeId = CInt(Me.ViewState("ReportTypeId"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("PYPeriodStartDate") = mdtPYPeriodStartDate
            Me.ViewState("PYPeriodEndDate") = mdtPYPeriodEndDate
            Me.ViewState("TnAPeriodStartDate") = mdtTnAPeriodStartDate
            Me.ViewState("TnAPeriodEndDate") = mdtTnAPeriodEndDate
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName ") = mstrReport_GroupName
            Me.ViewState("ReportTypeId") = mintReportTypeId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objEmployeeTimeSheet.generateReportNew(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                                                 , CInt(Session("CompanyUnkId")), dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date _
                                                                                                 , CStr(Session("UserAccessModeSetting")), True, Session("ExportReportPath").ToString() _
                                                                                                 , CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None, 0)

            Session("objRpt") = objEmployeeTimeSheet._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrStringIds = popAnalysisby._ReportBy_Ids
            mstrStringName = popAnalysisby._ReportBy_Name
            mintViewIdx = popAnalysisby._ViewIndex
            mstrAnalysis_Fields = popAnalysisby._Analysis_Fields
            mstrAnalysis_Join = popAnalysisby._Analysis_Join
            mstrAnalysis_OrderBy = popAnalysisby._Analysis_OrderBy
            mstrReport_GroupName = popAnalysisby._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboTnAPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTnAPeriod.SelectedIndexChanged
        Try
            If CInt(cboTnAPeriod.SelectedValue) <= 0 Then
                objTnAPeriod.Text = ""
                Exit Sub
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString()) = CInt(cboTnAPeriod.SelectedValue)

            mdtPYPeriodStartDate = objPeriod._Start_Date
            mdtPYPeriodEndDate = objPeriod._End_Date

            mdtTnAPeriodStartDate = objPeriod._TnA_StartDate.Date
            mdtTnAPeriodEndDate = objPeriod._TnA_EndDate.Date

            objTnAPeriod.Text = objPeriod._TnA_StartDate.ToShortDateString() & Space(3) & "To" & Space(3) & objPeriod._TnA_EndDate.ToShortDateString()
            objPeriod = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboTnAPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Public Sub SetLanguage()
        Try

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objEmployeeTimeSheet._ReportName)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lnkSetAnalysis.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "lnkAnalysisBy", Me.lnkSetAnalysis.Text)
            Me.LblReportType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblReportType.ID, Me.LblReportType.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToDate.ID, Me.lblToDate.Text)
            Me.LblTnAPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblTnAPeriod.ID, Me.LblTnAPeriod.Text)
            Me.LblDuration.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblDuration.ID, Me.LblDuration.Text)
            Me.lblFromHrs.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromHrs.ID, Me.lblFromHrs.Text)
            Me.lblToHour.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToHour.ID, Me.lblToHour.Text)
            Me.LblShift.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblShift.ID, Me.LblShift.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.chkShowEachEmpOnNewPage.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowEachEmpOnNewPage.ID, Me.chkShowEachEmpOnNewPage.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Short Hours Report")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Over Time Report")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Period is compulsory information.Please select period.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
