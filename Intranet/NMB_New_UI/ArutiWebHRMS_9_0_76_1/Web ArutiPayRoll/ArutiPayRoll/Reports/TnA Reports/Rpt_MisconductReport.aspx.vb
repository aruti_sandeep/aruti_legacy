﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_TnA_Reports_Rpt_MisconductReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objMisconductReport As clsMisconductReport
    Private Shared ReadOnly mstrModuleName As String = "frmMisconductReport"
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If
            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            Session("UserAccessModeSetting").ToString, True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                            blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmp = Nothing


            'If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
            dsList = Nothing
            Dim objMaster As New clsMasterData
            dsList = objMaster.GetEAllocation_Notification("List", "", False, False)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id <> " & enAllocation.DEPARTMENT, "", DataViewRowState.CurrentRows).ToTable()
            Dim drRow As DataRow = dtTable.NewRow
            drRow("Id") = 0
            drRow("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Select")
            dtTable.Rows.InsertAt(drRow, 0)
            With cboLocation
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
            End With
            objMaster = Nothing
            dtTable = Nothing
            dsList = Nothing
            'End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                cboEmployee.SelectedValue = CStr(0)
            End If
            mstrAdvanceFilter = ""
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mintViewIndex = -1

            Dim objUserDefRMode As New clsUserDef_ReportMode
            Dim dsList As DataSet = objUserDefRMode.GetList("List", enArutiReport.EmployeeMisconductReport, 0, 0)
            If dsList.Tables("List").Rows.Count > 0 Then

                cboLocation.SelectedValue = dsList.Tables("List").Rows(0).Item("transactionheadid").ToString()

            End If
            dsList = Nothing
            objUserDefRMode = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objMisconductReport.SetDefaultValue()

            If (DateDiff(DateInterval.Day, dtpFromDate.GetDate(), dtpToDate.GetDate()) + 1) > 31 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 103, "Sorry, Date range cannot be exceed more than 31 days. Please set correct days to generate report."), Me)
                Return False
            End If

            If dtpToDate.GetDate.Date < dtpFromDate.GetDate.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "Sorry, To Date cannot be less than from date. Please set correct days to generate report."), Me)
                Return False
            End If

            Dim sMsg As String = ""
            objMisconductReport.IsAbsentProcessDone(sMsg, dtpFromDate.GetDate.Date, dtpToDate.GetDate.Date, CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)))
            If sMsg.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Return False
            End If

            If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                If CInt(cboLocation.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 100, "Location is compulsory information.Please Select Location."), Me)
                    cboLocation.Focus()
                    Return False
                End If
            End If

            If CInt(Session("MisconductConsecutiveAbsentDays")) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 101, "Sorry, Misconduct days are not set under Aruti configuration -> Report Setting. Please set Misconduct days from Aruti configuration -> Report Setting."), Me)
                Return False
            End If

            objMisconductReport._DateFrom = dtpFromDate.GetDate()
            objMisconductReport._DateTo = dtpToDate.GetDate()

            objMisconductReport._EmployeeId = CInt(cboEmployee.SelectedValue)
            objMisconductReport._EmployeeName = cboEmployee.SelectedItem.Text

            objMisconductReport._ViewByIds = mstrViewByIds
            objMisconductReport._ViewIndex = mintViewIndex
            objMisconductReport._ViewByName = mstrViewByName
            objMisconductReport._Analysis_Fields = mstrAnalysis_Fields
            objMisconductReport._Analysis_Join = mstrAnalysis_Join
            objMisconductReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMisconductReport._Analysis_OrderBy = mstrAnalysis_OrderBy_GName
            objMisconductReport._Report_GroupName = mstrReport_GroupName
            objMisconductReport._MisconductDays = CInt(Session("MisconductConsecutiveAbsentDays"))
            objMisconductReport._LocationId = CInt(cboLocation.SelectedValue)
            objMisconductReport._Location = cboLocation.SelectedItem.Text

            SetDateFormat()

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            objMisconductReport = New clsMisconductReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                SetLanguage()
                SetMessages()
                Call FillCombo()
                dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                ResetValue()
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    lnkSetAnalysis.Visible = True
                Else
                    lnkSetAnalysis.Visible = False
                    lnkSave.Visible = False
                End If
            Else
                mstrViewByIds = Me.ViewState("mstrStringIds").ToString()
                mstrViewByName = Me.ViewState("mstrStringName").ToString()
                mintViewIndex = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = Me.ViewState("mstrAnalysis_Fields").ToString()
                mstrAnalysis_Join = Me.ViewState("mstrAnalysis_Join").ToString()
                mstrAnalysis_OrderBy = Me.ViewState("mstrAnalysis_OrderBy").ToString()
                mstrAnalysis_OrderBy_GName = Me.ViewState("mstrAnalysis_OrderBy_GName").ToString()
                mstrReport_GroupName = Me.ViewState("mstrReport_GroupName ").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrViewByIds
            Me.ViewState("mstrStringName") = mstrViewByName
            Me.ViewState("mintViewIdx") = mintViewIndex
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrAnalysis_OrderBy_GName") = mstrAnalysis_OrderBy_GName
            Me.ViewState("mstrReport_GroupName ") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If SetFilter() = False Then Exit Sub
            objMisconductReport._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMisconductReport._UserUnkId = CInt(Session("UserId"))
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objMisconductReport._ApplyAddUACFilter = False
                objMisconductReport._UserName = Session("DisplayName").ToString()
            End If
            objMisconductReport.generateReportNew(Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   dtpToDate.GetDate().Date, _
                                                   dtpToDate.GetDate().Date, _
                                                   CStr(Session("UserAccessModeSetting")), True, _
                                                   CStr(Session("ExportReportPath")), _
                                                   CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)

            Session("objRpt") = objMisconductReport._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrViewByIds = popAnalysisby._ReportBy_Ids
            mstrViewByName = popAnalysisby._ReportBy_Name
            mintViewIndex = popAnalysisby._ViewIndex
            mstrAnalysis_Fields = popAnalysisby._Analysis_Fields
            mstrAnalysis_Join = popAnalysisby._Analysis_Join
            mstrAnalysis_OrderBy = popAnalysisby._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = popAnalysisby._Analysis_OrderBy_GName
            mstrReport_GroupName = popAnalysisby._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkSave.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            objUserDefRMode = New clsUserDef_ReportMode()
            objUserDefRMode._Reportunkid = enArutiReport.Employee_WithOut_ClockOut_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 0

            Dim intUnkid As Integer = -1

            objUserDefRMode._EarningTranHeadIds = cboLocation.SelectedValue
            intUnkid = objUserDefRMode.isExist(enArutiReport.Employee_WithOut_ClockOut_Report, 0, 0, 0)

            objUserDefRMode._Reportmodeunkid = intUnkid
            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If
            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Setting saved successfully."), Me)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Public Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objMisconductReport._ReportName)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)

            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)
            Me.lnkSetAnalysis.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "lnkAnalysisBy", Me.lnkSetAnalysis.Text)
            Me.LblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFromDate.ID, Me.LblFromDate.Text)
            Me.LblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblToDate.ID, Me.LblToDate.Text)
            Me.lnkSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkSave.ID, Me.lnkSave.Text)
            Me.LblLocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLocation.ID, Me.LblLocation.Text)
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Public Shared Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 101, "Sorry, Misconduct days are not set under Aruti configuration -> Report Setting. Please set Misconduct days from Aruti configuration -> Report Setting.")
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
