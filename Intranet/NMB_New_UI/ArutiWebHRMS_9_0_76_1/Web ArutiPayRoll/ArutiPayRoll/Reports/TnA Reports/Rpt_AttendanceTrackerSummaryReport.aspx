﻿<%@ Page Title="Attendance Tracker Summary Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_AttendanceTrackerSummaryReport.aspx.vb"
    Inherits="Reports_TnA_Reports_Rpt_AttendanceTrackerSummaryReport" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nutx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Attendance Tracker Summary Report"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="objlblPDate" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkShowLateArrivalhours" runat="server" Text="Show Only Late Arrival hours" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="elActDaysNoOfWorked" runat="server" Text="Actual No. Of Days Worked"
                                                            CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Label ID="lblDayFrom" runat="server" Text="From" CssClass="form-label"></asp:Label>
                                                        <nutx:NumericText ID="nudDayFrom" runat="server" Type="Numeric" />
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Label ID="lblYTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                                        <nutx:NumericText ID="nudDayTo" runat="server" Type="Numeric" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkConsiderZeroFilter" runat="server" Text="Consider Zero As Value and Add in Filter" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbOtherSetting" runat="server" Text="Report Display Setting"></asp:Label>
                                                    <ul class="header-dropdown m-r--5">
                                                        <asp:LinkButton ID="lnkSaveSelection" runat="server" Text="Save Setting">                                             
                                                        </asp:LinkButton>
                                                    </ul>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Label ID="lblExpetedDays" runat="server" Text="Number Of Working Days" CssClass="form-label"></asp:Label>
                                                        <nutx:NumericText ID="nudDaysWorked" runat="server" Type="Numeric" Text="20" />
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Label ID="lblAllocation" runat="server" Text="Allocation" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboAllocation" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 244px;">
                                                            <asp:GridView ID="dgvLeave" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                CssClass="table table-hover table-bordered" DataKeyNames="leavetypeunkid">
                                                                <Columns>
                                                                    <asp:CommandField ShowEditButton="true" ItemStyle-HorizontalAlign="Center" ControlStyle-Font-Underline="false"
                                                                        ItemStyle-Width="60px" CausesValidation="false" />
                                                                    <asp:BoundField DataField="leavename" HeaderText="Leave Name" ReadOnly="True" FooterText="dgcolhOrgLeave" />
                                                                    <asp:BoundField DataField="displayvalue" HeaderText="Display Name" FooterText="dgcolhCaption" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnAdvFilter" runat="server" Text="Advance Filter" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                    <uc7:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </div>
                <uc1:Export runat="server" ID="Export" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
