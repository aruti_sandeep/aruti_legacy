﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.Globalization

#End Region

Partial Class Reports_TnA_Reports_Rpt_EmpBreakTimesheetReport
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private mstrModuleName As String = "frmEmpBreakTimesheetReport"
    Private objEmpBreakTimeSheet As clsEmpBreakTimesheetReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Private Function "

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            CStr(Session("UserAccessModeSetting")), _
                                                            True, CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "employeename"
                    .DataSource = dsList.Tables(0)
                    .SelectedValue = "0"
                    .DataBind()
                End With
                objEmp = Nothing
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee.Copy
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                End With
            End If
            
            dsList = Nothing

            Dim objShift As New clsNewshift_master
            dsList = objShift.getListForCombo("List", True)
            With cboShift
                .DataValueField = "shiftunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = "0"
                .DataBind()
            End With
            objShift = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEmployee.SelectedValue = "0"
            cboShift.SelectedValue = "0"
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            chkInactiveemp.Checked = False
            mstrAdvanceFilter = ""            
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objEmpBreakTimeSheet.SetDefaultValue()

            objEmpBreakTimeSheet._EmpId = CInt(cboEmployee.SelectedValue)
            objEmpBreakTimeSheet._EmpName = cboEmployee.Text

            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = CInt(cboEmployee.SelectedValue)
                objEmpBreakTimeSheet._Shiftunkid = objEmp._Shiftunkid
                objEmpBreakTimeSheet._EmpCode = objEmp._Employeecode
            End If

            objEmpBreakTimeSheet._FromDate = dtpFromDate.GetDate
            objEmpBreakTimeSheet._ToDate = dtpToDate.GetDate
            objEmpBreakTimeSheet._IsActive = chkInactiveemp.Checked
            objEmpBreakTimeSheet._Shiftunkid = CInt(cboShift.SelectedValue)
            objEmpBreakTimeSheet._ShiftName = cboShift.Text
            objEmpBreakTimeSheet._ViewByIds = mstrStringIds
            objEmpBreakTimeSheet._ViewIndex = mintViewIdx
            objEmpBreakTimeSheet._ViewByName = mstrStringName
            objEmpBreakTimeSheet._Analysis_Fields = mstrAnalysis_Fields
            objEmpBreakTimeSheet._Analysis_Join = mstrAnalysis_Join
            objEmpBreakTimeSheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpBreakTimeSheet._Report_GroupName = mstrReport_GroupName
            objEmpBreakTimeSheet._AdvanceFilter = mstrAdvanceFilter
            objEmpBreakTimeSheet._ShowEachEmployeeOnNewPage = chkShowEachEmpOnNewPage.Checked
            objEmpBreakTimeSheet._GroupName = Session("CompanyGroupName").ToString()
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Page Event(S) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            objEmpBreakTimeSheet = New clsEmpBreakTimesheetReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                Call SetLanguage()
                Call FillCombo()
                Call ResetValue()
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    chkInactiveemp.Visible = False
                    chkShowEachEmpOnNewPage.Visible = False
                    lnkSetAnalysis.Visible = False
                End If
            Else
                mstrStringIds = CStr(Me.ViewState("mstrStringIds"))
                mstrStringName = CStr(Me.ViewState("mstrStringName"))
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysis_Join = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysis_OrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReport_GroupName = CStr(Me.ViewState("mstrReport_GroupName "))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName ") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button Event(S) "

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()

            objEmpBreakTimeSheet._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objEmpBreakTimeSheet._UserUnkId = CInt(Session("UserId"))
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objEmpBreakTimeSheet._ApplyAddUACFilter = False
                objEmpBreakTimeSheet._UserName = Session("DisplayName").ToString()
            End If
            objEmpBreakTimeSheet.generateReportNew(Session("Database_Name").ToString, _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                    CStr(Session("ExportReportPath")), _
                                                    CBool(Session("OpenAfterExport")), 0, enPrintAction.None, _
                                                    enExportAction.None, _
                                                    CInt(Session("Base_CurrencyId")))

            Session("objRpt") = objEmpBreakTimeSheet._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrStringIds = popAnalysisby._ReportBy_Ids
            mstrStringName = popAnalysisby._ReportBy_Name
            mintViewIdx = popAnalysisby._ViewIndex
            mstrAnalysis_Fields = popAnalysisby._Analysis_Fields
            mstrAnalysis_Join = popAnalysisby._Analysis_Join
            mstrAnalysis_OrderBy = popAnalysisby._Analysis_OrderBy
            mstrReport_GroupName = popAnalysisby._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Language "

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lnkSetAnalysis.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)
            Me.LblShift.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblShift.ID, Me.LblShift.Text)
            Me.lblEmpName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)
            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToDate.ID, Me.lblToDate.Text)
            Me.chkShowEachEmpOnNewPage.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowEachEmpOnNewPage.ID, Me.chkShowEachEmpOnNewPage.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class
