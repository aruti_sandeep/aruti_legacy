﻿Option Strict On 'Shani(11-Feb-2016) 

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_TnA_Reports_Rpt_TRAEarlygoingReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Dim objLateArrialReport As clsLateArrialReport
    Private ReadOnly mstrModuleName As String = "frmLateArrial_EarlyGoingReport"
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

    'Pinkal (22-Dec-2022) -- Start
    '(A1X-349) TRA - Late arrival/Early departure report modification
    Dim mintReportTypeId As Integer = 0
    'Pinkal (22-Dec-2022) -- End

#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If
            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            Session("UserAccessModeSetting").ToString, True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                            blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmp = Nothing

            cboReportType.Items.Clear()
            'Pinkal (22-Dec-2022) -- Start
            '(A1X-349) TRA - Late arrival/Early departure report modification
            'cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Late Arrival Report "))
            'cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Early Departure Report"))
            'cboReportType.SelectedIndex = 0
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Late Arrival Report "))
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Early Departure Report"))
            cboReportType.SelectedIndex = CInt(IIf(mintReportTypeId = enArutiReport.Late_Arrival_Report, 0, 1))
            Me.Title = cboReportType.SelectedItem.Text
            Me.lblPageHeader.Text = cboReportType.SelectedItem.Text
            cboReportType.Enabled = False
            'Pinkal (22-Dec-2022) -- End


            cboReportType_SelectedIndexChanged(cboReportType, New EventArgs())


            If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                dsList = Nothing
                Dim objMaster As New clsMasterData
                dsList = objMaster.GetEAllocation_Notification("List", "", False, False)
                Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id <> " & enAllocation.DEPARTMENT, "", DataViewRowState.CurrentRows).ToTable()
                Dim drRow As DataRow = dtTable.NewRow
                drRow("Id") = 0
                drRow("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Select")
                dtTable.Rows.InsertAt(drRow, 0)
                With cboLocation
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dtTable
                    .DataBind()
                End With
                objMaster = Nothing
                dtTable = Nothing
                dsList = Nothing
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            dtpStartdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                cboEmployee.SelectedValue = CStr(0)
            End If
            chkShowEachEmpOnNewPage.Checked = True
            mstrAdvanceFilter = ""
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mintViewIndex = -1

            'objLateArrialReport.setDefaultOrderBy(CInt(cboReportType.SelectedIndex))

            If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                LblLocation.Visible = True
                cboLocation.Visible = True
                If cboLocation.Items.Count > 0 Then cboLocation.SelectedIndex = 0
                btnReport.Visible = True
                btnExport.Visible = False

                'Pinkal (28-Apr-2022) -- Start
                'Enhancement TRA : TnA Module Enhancement for TRA.
                'cboReportType.SelectedIndex = 1
                'cboReportType.Enabled = False
                'Pinkal (28-Apr-2022) -- End
                cboReportType_SelectedIndexChanged(cboReportType, New EventArgs())
                chkDonotconsiderLeave.Checked = False
                chkDonotconsiderLeave.Visible = False
                chkIgnoreBlankRows.Checked = False
                chkIgnoreBlankRows.Visible = False
                chkShowShiftName.Checked = False
                chkShowShiftName.Visible = False
                chkShowEachEmpOnNewPage.Visible = True
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                If CInt(cboLocation.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Location is compulsory information.Please Select Location."), Me)
                    cboLocation.Focus()
                    Return False
                End If
            End If

            objLateArrialReport.SetDefaultValue()
            'Pinkal (22-Dec-2022) -- Start
            '(A1X-349) TRA - Late arrival/Early departure report modification
            'objLateArrialReport._YearId = FinancialYear._Object._YearUnkid
            objLateArrialReport._YearId = CInt(Session("Fin_year"))
            'Pinkal (22-Dec-2022) -- End

            objLateArrialReport._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objLateArrialReport._UserUnkId = CInt(Session("UserId"))
            objLateArrialReport._FromDate = dtpStartdate.GetDate.Date
            objLateArrialReport._ToDate = dtpToDate.GetDate.Date
            objLateArrialReport._EmployeeID = CInt(cboEmployee.SelectedValue)
            objLateArrialReport._EmployeeName = cboEmployee.SelectedItem.Text
            objLateArrialReport._Advance_Filter = mstrAdvanceFilter
            objLateArrialReport._ViewByIds = mstrViewByIds
            objLateArrialReport._ViewIndex = mintViewIndex
            objLateArrialReport._ViewByName = mstrViewByName
            objLateArrialReport._Analysis_Fields = mstrAnalysis_Fields
            objLateArrialReport._Analysis_Join = mstrAnalysis_Join
            objLateArrialReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objLateArrialReport._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objLateArrialReport._Report_GroupName = mstrReport_GroupName
            objLateArrialReport._ReportId = CInt(cboReportType.SelectedIndex)
            objLateArrialReport._ReportTypeName = cboReportType.SelectedItem.Text
            objLateArrialReport._IgnoreBlankRows = chkIgnoreBlankRows.Checked
            objLateArrialReport._ShowShiftName = chkShowShiftName.Checked
            objLateArrialReport._DoNotConsiderLeave = chkDonotconsiderLeave.Checked

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                objLateArrialReport._IncludeAccessFilterQry = False
                objLateArrialReport._UserName = Session("DisplayName").ToString()
            Else
                objLateArrialReport._IncludeAccessFilterQry = True
            End If

            objLateArrialReport._GroupName = Session("CompanyGroupName").ToString()
            If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                objLateArrialReport._ReportName = cboReportType.SelectedItem.Text
                objLateArrialReport._ShowEachEmployeeOnNewPage = chkShowEachEmpOnNewPage.Checked
                objLateArrialReport._LocationId = CInt(cboLocation.SelectedValue)
                objLateArrialReport._Location = cboLocation.SelectedItem.Text
            End If

            SetDateFormat()

            objLateArrialReport.setDefaultOrderBy(CInt(cboReportType.SelectedIndex))

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function


#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            objLateArrialReport = New clsLateArrialReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then

                'Pinkal (22-Dec-2022) -- Start
                '(A1X-349) TRA - Late arrival/Early departure report modification

                If Request.QueryString.Count <= 0 Then Exit Sub
                Try
                    mintReportTypeId = CInt(b64decode(Server.UrlDecode(Request.QueryString.ToString.Substring(3))))
                    objLateArrialReport.setReportData(mintReportTypeId, CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
                Catch
                    Session("clsuser") = Nothing
                    DisplayMessage.DisplayMessage("Invalid Page Address.", Me, "../../index.aspx")
                    Exit Sub
                End Try

                SetLanguage()
                SetMessages()
                Call FillCombo()
                dtpStartdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                ResetValue()
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    lnkSetAnalysis.Visible = True
                Else
                    lnkSetAnalysis.Visible = False
                    lnkSave.Visible = False
                    chkShowEachEmpOnNewPage.Visible = False
                End If
                'Pinkal (22-Dec-2022) -- End
            Else
                mstrViewByIds = Me.ViewState("mstrStringIds").ToString()
                mstrViewByName = Me.ViewState("mstrStringName").ToString()
                mintViewIndex = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = Me.ViewState("mstrAnalysis_Fields").ToString()
                mstrAnalysis_Join = Me.ViewState("mstrAnalysis_Join").ToString()
                mstrAnalysis_OrderBy = Me.ViewState("mstrAnalysis_OrderBy").ToString()
                mstrAnalysis_OrderBy_GName = Me.ViewState("mstrAnalysis_OrderBy_GName").ToString()
                mstrReport_GroupName = Me.ViewState("mstrReport_GroupName ").ToString()
                'Pinkal (22-Dec-2022) -- Start
                '(A1X-349) TRA - Late arrival/Early departure report modification
                mintReportTypeId = CInt(Me.ViewState("ReportTypeId"))
                'Pinkal (22-Dec-2022) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrViewByIds
            Me.ViewState("mstrStringName") = mstrViewByName
            Me.ViewState("mintViewIdx") = mintViewIndex
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrAnalysis_OrderBy_GName") = mstrAnalysis_OrderBy_GName
            Me.ViewState("mstrReport_GroupName ") = mstrReport_GroupName
            'Pinkal (22-Dec-2022) -- Start
            '(A1X-349) TRA - Late arrival/Early departure report modification
            Me.ViewState("ReportTypeId") = mintReportTypeId
            'Pinkal (22-Dec-2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objLateArrialReport.Generate_LateArrival_Report(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                                             , CInt(Session("CompanyUnkId")), dtpToDate.GetDate.Date, CStr(Session("UserAccessModeSetting")) _
                                                                                             , True, CBool(Session("IsIncludeInactiveEmp")))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objLateArrialReport.generateReportNew(Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   dtpStartdate.GetDate.Date, _
                                                   dtpToDate.GetDate.Date, _
                                                   CStr(Session("UserAccessModeSetting")), True, _
                                                   CStr(Session("ExportReportPath")), _
                                                   CBool(Session("OpenAfterExport")), _
                                                   CInt(cboReportType.SelectedIndex), enPrintAction.None, enExportAction.None)

            Session("objRpt") = objLateArrialReport._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrViewByIds = popAnalysisby._ReportBy_Ids
            mstrViewByName = popAnalysisby._ReportBy_Name
            mintViewIndex = popAnalysisby._ViewIndex
            mstrAnalysis_Fields = popAnalysisby._Analysis_Fields
            mstrAnalysis_Join = popAnalysisby._Analysis_Join
            mstrAnalysis_OrderBy = popAnalysisby._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = popAnalysisby._Analysis_OrderBy_GName
            mstrReport_GroupName = popAnalysisby._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Dim objUserDefRMode As New clsUserDef_ReportMode
            Dim dsList As DataSet = Nothing

            'Pinkal (22-Dec-2022) -- Start
            '(A1X-349) TRA - Late arrival/Early departure report modification
            If mintReportTypeId = enArutiReport.Late_Arrival_Report Then
            dsList = objUserDefRMode.GetList("List", enArutiReport.Late_Arrival_Report, 0, cboReportType.SelectedIndex)
            ElseIf mintReportTypeId = enArutiReport.Early_Departure_Report Then
                dsList = objUserDefRMode.GetList("List", enArutiReport.Early_Departure_Report, 0, cboReportType.SelectedIndex)
            End If
            'Pinkal (22-Dec-2022) -- End


            If dsList.Tables("List").Rows.Count > 0 Then
                Dim ar() As String = dsList.Tables("List").Rows(0).Item("transactionheadid").ToString().Split(CChar("|"))
                If ar.Length > 0 Then
                    chkShowShiftName.Checked = CBool(ar(0).ToString())

                    If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                        If ar.Length > 3 Then
                            chkDonotconsiderLeave.Checked = CBool(ar(1).ToString())
                            cboLocation.SelectedValue = CStr(IIf(ar(2).ToString() = "", "0", ar(2).ToString()))
                            chkShowEachEmpOnNewPage.Checked = CBool(IIf(ar(3).ToString() = "", "False", ar(3).ToString()))
                        Else
                            chkDonotconsiderLeave.Checked = False
                        End If
                    Else
                        If ar.Length > 1 Then
                            chkDonotconsiderLeave.Checked = CBool(ar(1).ToString())
                        Else
                            chkDonotconsiderLeave.Checked = False
                        End If
                    End If

                Else
                    chkShowShiftName.Checked = False
                    chkDonotconsiderLeave.Checked = False
                End If
            End If

            If CInt(cboReportType.SelectedIndex) = 0 Then
                chkDonotconsiderLeave.Text = Language.getMessage(mstrModuleName, 3, "Do not count") & " " & Language.getMessage(mstrModuleName, 4, "Late Arrival") & " " & Language.getMessage(mstrModuleName, 5, "When Employee is on Leave")
            ElseIf CInt(cboReportType.SelectedIndex) = 1 Then
                chkDonotconsiderLeave.Text = Language.getMessage(mstrModuleName, 3, "Do not count") & " " & Language.getMessage(mstrModuleName, 6, "Early Departure") & " " & Language.getMessage(mstrModuleName, 5, "When Employee is on Leave")
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkSave.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            objUserDefRMode = New clsUserDef_ReportMode()

            'Pinkal (22-Dec-2022) -- Start
            '(A1X-349) TRA - Late arrival/Early departure report modification
            If mintReportTypeId = enArutiReport.Late_Arrival_Report Then
            objUserDefRMode._Reportunkid = enArutiReport.Late_Arrival_Report
            ElseIf mintReportTypeId = enArutiReport.Early_Departure_Report Then
                objUserDefRMode._Reportunkid = enArutiReport.Early_Departure_Report
            End If
            'Pinkal (22-Dec-2022) -- End

            objUserDefRMode._Reporttypeid = cboReportType.SelectedIndex
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = cboReportType.SelectedIndex
            Dim intUnkid As Integer = -1
            If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                objUserDefRMode._EarningTranHeadIds = chkShowShiftName.Checked & "|" & chkDonotconsiderLeave.Checked & "|" & cboLocation.SelectedValue.ToString() & "|" & chkShowEachEmpOnNewPage.Checked
            Else
                objUserDefRMode._EarningTranHeadIds = chkShowShiftName.Checked & "|" & chkDonotconsiderLeave.Checked
            End If
            intUnkid = objUserDefRMode.isExist(enArutiReport.Late_Arrival_Report, cboReportType.SelectedIndex, 0, cboReportType.SelectedIndex)

            objUserDefRMode._Reportmodeunkid = intUnkid
            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If
            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Setting saved successfully."), Me)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Public Sub SetLanguage()
        Try

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objLateArrialReport._ReportName)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)
            Me.lnkSetAnalysis.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "lnkAnalysisBy", Me.lnkSetAnalysis.Text)
            Me.LblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFromDate.ID, Me.LblFromDate.Text)
            Me.LblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblToDate.ID, Me.LblToDate.Text)
            Me.LblReportType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblReportType.ID, Me.LblReportType.Text)
            Me.chkIgnoreBlankRows.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkIgnoreBlankRows.ID, Me.chkIgnoreBlankRows.Text)
            Me.chkShowShiftName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowShiftName.ID, Me.chkShowShiftName.Text)
            Me.lnkSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkSave.ID, Me.lnkSave.Text)
            Me.chkDonotconsiderLeave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkDonotconsiderLeave.ID, Me.chkDonotconsiderLeave.Text)
            Me.LblLocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLocation.ID, Me.LblLocation.Text)
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Late Arrival Report")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Early Departure Report")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Do not count")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Late Arrival")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "When Employee is on Leave")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Early Departure")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Setting saved successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Select.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Location is compulsory information.Please Select Location.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
