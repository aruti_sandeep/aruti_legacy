﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region
Partial Class Reports_Assessment_Reports_Rpt_ProgressUpdateReport
    Inherits Basepage

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmProgressUpdateReport"
    Private objProgress As clsProgressUpdateReport
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private DisplayMessage As New CommonCodes
    Private mdtDisplayCol As DataTable
    Private mdtAllocation As DataTable

#End Region

#Region " Private Function "

    Private Sub Fill_Column_List()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objMData.GetEAllocation_Notification("List")
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 900
                dRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Grade Group")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 901
                dRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Grade")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 902
                dRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Grade Level")
                dsList.Tables(0).Rows.Add(dRow)

                mdtDisplayCol = dsList.Tables(0).Copy()

                Dim dCol As New DataColumn
                dCol.ColumnName = "ischeck"
                dCol.DataType = GetType(System.Boolean)
                dCol.DefaultValue = False
                mdtDisplayCol.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "objcolhSelectCol"
                dCol.DataType = GetType(System.String)
                dCol.DefaultValue = ""
                mdtDisplayCol.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "objcolhJoin"
                dCol.DataType = GetType(System.String)
                dCol.DefaultValue = ""
                mdtDisplayCol.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "objcolhDisplay"
                dCol.DataType = GetType(System.String)
                dCol.DefaultValue = ""
                mdtDisplayCol.Columns.Add(dCol)

                For Each dtRow As DataRow In mdtDisplayCol.Rows
                    If CInt(dtRow.Item("Id")) = enAllocation.JOBS Then Continue For
                    Select Case CInt(dtRow.Item("Id"))
                        Case enAllocation.BRANCH
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(estm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrstation_master AS estm ON estm.stationunkid = Alloc.stationunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.DEPARTMENT_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(edgm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrdepartment_group_master AS edgm ON edgm.deptgroupunkid = Alloc.deptgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.DEPARTMENT
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(edpt.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrdepartment_master AS edpt ON edpt.departmentunkid = Alloc.departmentunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.SECTION_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(esgm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrsectiongroup_master AS esgm ON esgm.sectiongroupunkid = Alloc.sectiongroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.SECTION
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(esec.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrsection_master AS esec ON esec.sectionunkid = Alloc.sectionunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.UNIT_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(eugm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrunitgroup_master AS eugm ON eugm.unitgroupunkid = Alloc.unitgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.UNIT
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(eutm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrunit_master AS eutm ON eutm.unitunkid = Alloc.unitunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.TEAM
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(etem.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrteam_master AS etem ON etem.teamunkid = Alloc.teamunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.JOB_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ejgm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrjobgroup_master AS ejgm ON ejgm.jobgroupunkid = Jobs.jobgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.JOBS
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ejbm.job_name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrjob_master AS ejbm ON ejbm.jobunkid = Jobs.jobunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.CLASS_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ecgm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrclassgroup_master AS ecgm ON ecgm.classgroupunkid = Alloc.classgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.CLASSES
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ecls.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrclasses_master AS ecls ON ecls.classesunkid = Alloc.classunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case 900
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(eggm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrgradegroup_master eggm ON grds.gradegroupunkid = eggm.gradegroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case 901
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(egdm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrgrade_master egdm ON grds.gradeunkid = egdm.gradeunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case 902
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(egdl.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrgradelevel_master egdl ON grds.gradelevelunkid = egdl.gradelevelunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.COST_CENTER
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ecct.costcentername,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN prcostcenter_master AS ecct ON ecct.costcenterunkid = CC.costcenterunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                    End Select
                Next

                lvDisplayCol.DataSource = mdtDisplayCol
                lvDisplayCol.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMData = Nothing : dsList = Nothing
        End Try
    End Sub

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objStatus As New clsProgressUpdateReport(0, 0)
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objStatus.GetProgressStatus("List", True)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            'S.SANDEEP |24-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : Sprint 2023-13
            With cboReportType
                .Items.Clear()
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 501, "Update Progress Summary Report"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 502, "Update Progress Detail Report"))               
                .DataBind()
                .SelectedIndex = 0
            End With
            cboReportType_SelectedIndexChanged(Nothing, Nothing)

            Dim objMData As New clsMasterData
            dsCombos = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsCombos.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = 1
            End With
            cboAllocations_SelectedIndexChanged(Nothing, Nothing)

            dsCombos = objMData.Get_Goal_Accomplishement_Status("List", True)
            With cboApprovalStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            objMData = Nothing
            'S.SANDEEP |24-JUN-2023| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            'S.SANDEEP |24-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : Sprint 2023-13
            cboReportType.SelectedIndex = 0
            Call cboReportType_SelectedIndexChanged(Nothing, Nothing)
            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'S.SANDEEP |24-JUN-2023| -- END
            cboPeriod.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboStatus.SelectedValue = 0
            mstrAdvanceFilter = ""
            Call Fill_Column_List()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objProgress.SetDefaultValue()
            If cboPeriod.SelectedValue <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), Me)
                cboPeriod.Focus()
                Return False
            End If

            'S.SANDEEP |24-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : Sprint 2023-13
            'If lvDisplayCol.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Count <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please select atleast one column to display in report."), Me)
            '    lvDisplayCol.Focus()
            '    Return False
            'End If

            'objProgress._PeriodId = cboPeriod.SelectedValue
            'objProgress._PeriodName = cboPeriod.SelectedItem.Text
            'Dim iCols, iJoins, iDisplay As String
            'iCols = String.Empty : iJoins = String.Empty : iDisplay = String.Empty
            'If lvDisplayCol.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Count > 0 Then
            '    iCols = String.Join(vbCrLf, lvDisplayCol.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvDisplayCol, "objcolhSelectCol", False, False)).Text.ToString()).ToArray())
            '    iJoins = String.Join(vbCrLf, lvDisplayCol.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvDisplayCol, "objcolhJoin", False, False)).Text.ToString()).ToArray())
            '    iDisplay = String.Join(vbCrLf, lvDisplayCol.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvDisplayCol, "objcolhDisplay", False, False)).Text.ToString()).ToArray())
            'End If
            Select Case cboReportType.SelectedIndex
                Case 0  'SUMMARY
            If lvDisplayCol.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please select atleast one column to display in report."), Me)
                lvDisplayCol.Focus()
                Return False
            End If
                Case 1  'DETAILS
                    If lvAllocation.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkabox"), CheckBox).Checked = True).Count <= 0 Then                        
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Please select atleast one allocation to export report."), Me)
                        lvAllocation.Focus()
                        Return False
                    End If
            End Select
            'S.SANDEEP |24-JUN-2023| -- END

            

            objProgress._PeriodId = cboPeriod.SelectedValue
            objProgress._PeriodName = cboPeriod.SelectedItem.Text
            Dim iCols, iJoins, iDisplay As String
            iCols = String.Empty : iJoins = String.Empty : iDisplay = String.Empty
            If lvDisplayCol.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Count > 0 Then
                iCols = String.Join(vbCrLf, lvDisplayCol.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvDisplayCol, "objcolhSelectCol", False, False)).Text.ToString()).ToArray())
                iJoins = String.Join(vbCrLf, lvDisplayCol.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvDisplayCol, "objcolhJoin", False, False)).Text.ToString()).ToArray())
                iDisplay = String.Join(vbCrLf, lvDisplayCol.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvDisplayCol, "objcolhDisplay", False, False)).Text.ToString()).ToArray())
            End If
            

            objProgress._SelectedCols = iCols
            objProgress._SelectedJoin = iJoins
            objProgress._DisplayCols = iDisplay
            objProgress._AdvanceFilter = mstrAdvanceFilter
            objProgress._EmployeeId = cboEmployee.SelectedValue
            objProgress._EmployeeName = cboEmployee.SelectedItem.Text
            objProgress._ViewByIds = mstrStringIds
            objProgress._ViewIndex = mintViewIdx
            objProgress._ViewByName = mstrStringName
            objProgress._Analysis_Fields = mstrAnalysis_Fields
            objProgress._Analysis_Join = mstrAnalysis_Join
            objProgress._Analysis_OrderBy = mstrAnalysis_OrderBy
            objProgress._Report_GroupName = mstrReport_GroupName
            objProgress._FirstNamethenSurname = Session("FirstNamethenSurname")
            objProgress._StatusId = CInt(cboStatus.SelectedValue)
            objProgress._StatusName = cboStatus.SelectedItem.Text

            'S.SANDEEP |24-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : Sprint 2023-13


            Dim StrStringIds As String = ""
            If lvAllocation.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkabox"), CheckBox).Checked = True).Count > 0 Then
                StrStringIds = String.Join(",", lvAllocation.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkabox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvAllocation, "Id", False, False)).Text.ToString()).ToArray())
            End If
            objProgress._Allocation = cboAllocations.SelectedItem.Text
            If StrStringIds.Length > 0 Then
                Select Case cboAllocations.SelectedValue
                    Case enAllocation.BRANCH
                        StrStringIds = "Alloc.stationunkid IN(" & StrStringIds & ") "
                    Case enAllocation.DEPARTMENT_GROUP
                        StrStringIds = "Alloc.deptgroupunkid IN(" & StrStringIds & ") "
                    Case enAllocation.DEPARTMENT
                        StrStringIds = "Alloc.departmentunkid IN(" & StrStringIds & ") "
                    Case enAllocation.SECTION_GROUP
                        StrStringIds = "Alloc.sectiongroupunkid IN(" & StrStringIds & ") "
                    Case enAllocation.SECTION
                        StrStringIds = "Alloc.sectionunkid IN(" & StrStringIds & ") "
                    Case enAllocation.UNIT_GROUP
                        StrStringIds = "Alloc.unitgroupunkid IN(" & StrStringIds & ") "
                    Case enAllocation.UNIT
                        StrStringIds = "Alloc.unitunkid IN(" & StrStringIds & ") "
                    Case enAllocation.TEAM
                        StrStringIds = "Alloc.teamunkid IN(" & StrStringIds & ") "
                    Case enAllocation.JOB_GROUP
                        StrStringIds = "Jobs.jobgroupunkid IN(" & StrStringIds & ") "
                    Case enAllocation.JOBS
                        StrStringIds = "Jobs.jobunkid IN(" & StrStringIds & ") "
                    Case enAllocation.CLASS_GROUP
                        StrStringIds = "Alloc.classgroupunkid IN(" & StrStringIds & ") "
                    Case enAllocation.CLASSES
                        StrStringIds = "Alloc.classunkid IN(" & StrStringIds & ") "
                End Select
            End If
            objProgress._AllocationIds = StrStringIds

            objProgress._ApprovalStatusId = CInt(cboApprovalStatus.SelectedValue)
            objProgress._ApprovalStatusName = cboApprovalStatus.SelectedItem.Text
            objProgress._ReportType_Id = cboReportType.SelectedIndex
            objProgress._ReportType_Name = cboReportType.SelectedItem.Text
            If dtpUpdateFrom.IsNull = False Then objProgress._UpdateDateFrom = dtpUpdateFrom.GetDate()
            If dtpUpdateTo.IsNull = False Then objProgress._UpdateDateTo = dtpUpdateTo.GetDate()
            If dtpApprovalFrom.IsNull = False Then objProgress._ApprovalDateFrom = dtpApprovalFrom.GetDate()
            If dtpApprovalTo.IsNull = False Then objProgress._ApprovalDateTo = dtpApprovalTo.GetDate()
            'S.SANDEEP |24-JUN-2023| -- END

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    'S.SANDEEP |24-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : Sprint 2023-13
    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case cboAllocations.SelectedValue
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            mdtAllocation = New DataTable("List")
            Dim dCol As New DataColumn
            dCol.ColumnName = "Id"
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtAllocation.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Name"
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtAllocation.Columns.Add(dCol)


            'Gajanan [17-SEP-2019] -- Start    
            'Enhancement:Working On Assesment Score report
            'dCol = New DataColumn
            'dCol.ColumnName = "ischeck"
            'dCol.DataType = GetType(System.Boolean)
            'dCol.DefaultValue = False
            'mdtAllocation.Columns.Add(dCol)
            'Gajanan [17-SEP-2019] -- End

            For Each dtRow As DataRow In dTable.Rows
                Dim drow As DataRow = mdtAllocation.NewRow()
                drow.Item("Id") = dtRow.Item(StrIdColName)
                drow.Item("Name") = dtRow.Item(StrDisColName)
                mdtAllocation.Rows.Add(drow)
            Next

            lvAllocation.Columns(1).HeaderText = cboAllocations.SelectedItem.Text
            lvAllocation.DataSource = mdtAllocation
            lvAllocation.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |24-JUN-2023| -- END

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objProgress = New clsProgressUpdateReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            If Not IsPostBack Then
                Call FillCombo()
                Call Fill_Column_List()
            Else
                mstrStringIds = CStr(Me.ViewState("mstrStringIds"))
                mstrStringName = CStr(Me.ViewState("mstrStringName"))
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysis_Join = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysis_OrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReport_GroupName = CStr(Me.ViewState("mstrReport_GroupName"))
                mstrAdvanceFilter = Me.ViewState("mstrAdvanceFilter").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
            Me.ViewState("mstrAdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Call SetDateFormat()
            Dim strFileName As String = String.Empty
            Select Case cboReportType.SelectedIndex
                Case 0  'SUMMARY
            objProgress.ExportProgressUpdate(Session("Database_Name").ToString, _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                             CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                             CStr(Session("UserAccessModeSetting")), True, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)
                Case 1  'DETAILS
                    objProgress.ExportProgressUpdateDetailReport(Session("Database_Name").ToString, _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                             CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                             CStr(Session("UserAccessModeSetting")), True, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)
            End Select
            
            If strFileName.Trim.Length <= 0 Then strFileName = objProgress._FileNameAfterExported.Trim
            If strFileName.Contains(".xls") = False Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName & ".xls"
            Else
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName
            End If
            Export.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAdvFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdvFilter.Click
        Try
            Select Case cboReportType.SelectedIndex
                Case 0  'SUMMARY
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
                Case 1  'DETAILS
                    popupAdvanceFilter._Hr_EmployeeTable_Alias = "EM"
            End Select
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'S.SANDEEP |24-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : Sprint 2023-13
#Region " Controls Event(s) "

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            Call Fill_Data()
            If Me.ViewState("mdtAllocation") IsNot Nothing Then
                Me.ViewState("mdtAllocation") = mdtAllocation
            Else
                Me.ViewState.Add("mdtAllocation", mdtAllocation)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub  

    Protected Sub txtFrmSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            Dim dView As DataView = CType(Me.ViewState("mdtAllocation"), DataTable).DefaultView
            Dim StrSearch As String = String.Empty
            If txtSearch.Text.Trim.Length > 0 Then
                StrSearch = "Name LIKE '%" & txtSearch.Text & "%' "
            End If
            dView.RowFilter = StrSearch
            lvAllocation.DataSource = dView
            lvAllocation.DataBind()
            lvAllocation.Focus()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Procedure txtFrmSearch_TextChanged : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case cboReportType.SelectedIndex
                Case 0 ' SUMMARY
                    pnlData.Enabled = False
                    cboAllocations.SelectedValue = 1
                    dtpApprovalFrom.SetDate = Nothing
                    dtpApprovalTo.SetDate = Nothing
                    dtpUpdateFrom.SetDate = Nothing
                    dtpUpdateTo.SetDate = Nothing
                    cboApprovalStatus.SelectedValue = 0
                    cboStatus.Enabled = True                    
                    txtSearch.Text = ""
                Case 1 ' DETAILS
                    pnlData.Enabled = True
                    cboStatus.SelectedIndex = 0
                    cboStatus.Enabled = False

                    Call Fill_Column_List()
                    If Me.ViewState("mdtDisplayCol") IsNot Nothing Then
                        Me.ViewState("mdtDisplayCol") = mdtDisplayCol
                    Else
                        Me.ViewState.Add("mdtDisplayCol", mdtDisplayCol)
                    End If
                    Call cboAllocations_SelectedIndexChanged(New Object(), New EventArgs())

            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    'S.SANDEEP |24-JUN-2023| -- END


#Region " Link Event(s) "

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            Select Case cboReportType.SelectedIndex
                Case 0  'SUMMARY
                    popupAnalysisBy._Hr_EmployeeTable_Alias = "hremployee_master"
                Case 1  'DETAILS
                    popupAnalysisBy._Hr_EmployeeTable_Alias = "EM"
            End Select

            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class
