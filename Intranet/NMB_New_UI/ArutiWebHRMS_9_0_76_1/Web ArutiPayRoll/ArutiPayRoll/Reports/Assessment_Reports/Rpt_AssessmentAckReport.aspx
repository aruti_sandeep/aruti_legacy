﻿<%@ Page Title="Assessment Acknowledgement Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_AssessmentAckReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_AssessmentAckReport" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtSearchEmp').val().length > 0) {
                $('#<%=dgvEmp.ClientID %> tbody tr').hide();
                $('#<%=dgvEmp.ClientID %> tbody tr:first').show();
                $('#<%=dgvEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchEmp').val() + '\')').parent().show();
            }
            else if ($('#dgvEmp').val().length == 0) {
                resetFromSearchValue();
            }

            if ($('#<%=dgvEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        $("body").on("click", "[id*=ChkAllSelectedEmp]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkSelectedEmp]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=ChkSelectedEmp]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAllSelectedEmp]", grid);
            debugger;
            if ($("[id*=ChkSelectedEmp]", grid).length == $("[id*=ChkSelectedEmp]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }

        });
    </script>

    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Assessment Acknowledgement Report"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboPeriod" runat="server" Style="width: 99%" AutoPostBack="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboEmployee" runat="server" Style="width: 99%" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <asp:Label ID="lblAckStatus" runat="server" Text="Ack. Status" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboAckStatus" runat="server" Style="width: 99%" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblAppointment" runat="server" Text="Appointment" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboAppointment" runat="server" Style="width: 99%" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <asp:Label ID="objlblCaption" runat="server" Text="" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <uc1:DateCtrl ID="dtpDate1" runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <asp:Label ID="lblTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <uc1:DateCtrl ID="dtpDate2" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:CheckBox ID="chkConsiderEmployeeTermination" runat="server" Text="Consider Employee Active Status On Period Date" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblReportMode" runat="server" Text="Report Mode" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboReportMode" runat="server" Style="width: 99%" AutoPostBack="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" id="txtSearchEmp" name="txtSearch" placeholder="type search text"
                                                            maxlength="50" class="form-control" style="height: 25px; font: 100; width: 94%"
                                                            onkeyup="FromSearching();" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="height: 193px">
                                                    <asp:GridView ID="dgvEmp" runat="server" AutoGenerateColumns="False" Width="99%"
                                                        CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="assessormasterunkid,assessorname">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="25">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="ChkAllSelectedEmp" runat="server" Enabled="true" Text=" " />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="ChkSelectedEmp" runat="server" Enabled="true" Text=" " />
                                                                    <asp:HiddenField ID="hfemployeeunkid" runat="server" Value='<%#eval("assessormasterunkid") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="assessorname" HeaderText="" ReadOnly="true" FooterText="colhEmp" />
                                                            <asp:BoundField DataField="assessormasterunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                Visible="false" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc9:Export runat="server" ID="Export" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
