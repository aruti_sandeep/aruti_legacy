﻿<%@ Page Title="Planning Report" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="Rpt_Assessment_Planning_Report.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_Assessment_Planning_Report" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="uc" TagPrefix="datectrl" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="popupAnalysisBy" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



<script type="text/javascript">

    $("body").on("click", "[id*=chkHeder1]", function() {
        var chkHeader = $(this);
        debugger;
        var grid = $(this).closest("table");
        $("[id*=chkbox1]").prop("checked", $(chkHeader).prop("checked"));
    });


    $("body").on("click", "[id*=chkbox1]", function() {
        var grid = $(this).closest("table");
        var chkHeader = $("[id*=chkHeder1]", grid);
        debugger;
        if ($("[id*=chkbox1]", grid).length == $("[id*=chkbox1]:checked", grid).length) {
            chkHeader.prop("checked", true);
        }
        else {
            chkHeader.prop("checked", false);
        }
    });
        
    </script>


    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Planning Report"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" ToolTip="Analysis By">
                                             <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReportPlanning" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlCalibrationScore" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblDisplayScore" runat="server" Text="Display Score" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboScoreOption" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlBSC" runat="server" Width="100%">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboEmployee" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboStatus" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblSDateFrom" runat="server" Text="Status From" CssClass="form-label"></asp:Label>
                                                    <datectrl:uc ID="dtpS_Dt_From" runat="server" />
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblSDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                                    <datectrl:uc ID="dtpS_Dt_To" runat="server" />
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:CheckBox ID="chkIncludeSummary" runat="server" Text="Display Summary Report"
                                                        Checked="true" />
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:CheckBox ID="chkExcludeInactive" runat="server" Text="Exclude Inactive Employee"
                                                        Checked="true" />
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblAppointment" runat="server" Text="Appointed" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboAppointment" runat="server" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="objlblCaption" runat="server" Text="#Value" CssClass="form-label"></asp:Label>
                                                    <datectrl:uc ID="dtpDate1" runat="server" />
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                                    <datectrl:uc ID="dtpDate2" runat="server" />
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:CheckBox ID="chkConsiderEmployeeTermination" runat="server" Text="Consider Employee Active Status On Period Date" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlEmp" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtSearchEmp" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="table-responsive" style="height: 300px">
                                                        <asp:DataGrid ID="dgvAEmployee" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyField = "employeeunkid">
                                                            <Columns>
                                                                <asp:TemplateColumn ItemStyle-Width="25">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" Text=" "/>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" Text=" " />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="EmpCodeName" HeaderText="Employee" FooterText = "dgcolhEmpCodeName" />
                                                                <asp:BoundColumn DataField="employeeunkid" HeaderText="employeeunkid" Visible="false" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-l-10 m-t-30">
                                                    <asp:CheckBox ID="chkShowComputedScore" runat="server" Text="Show Computed Score" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                    <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btn btn-primary" />
                                    <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:popupAnalysisBy ID="popupAnalysisBy" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
