﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Assessment_Reports_Rpt_ProgressUpdateNotAttemptedReport
    Inherits Basepage

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmProgressUpdateNotAttemptedReport"
    Private objProgress As clsProgressUpdateNotAttemptedReport
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objStatus As New clsProgressUpdateReport(0, 0)
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            'dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, CInt(Session("Fin_year")), Session("Database_Name"), Session("fin_startdate"), "List", True)
            'With cboPeriod
            '    .DataValueField = "periodunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombos.Tables("List")
            '    .DataBind()
            'End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GName = ""
            'cboPeriod.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            dtpFromDate.SetDate = Now.Date
            dtpToDate.SetDate = Now.Date
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objProgress.SetDefaultValue()
            'If cboPeriod.SelectedValue <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), Me)
            '    cboPeriod.Focus()
            '    Return False

            If dtpFromDate.IsNull OrElse dtpToDate.IsNull Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Dates are compulsory information.Please Select Dates."), Me)
                dtpFromDate.Focus()
                Return False

            ElseIf dtpFromDate.GetDate.Date > dtpToDate.GetDate.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "From Date must be less than or equal to to date."), Me)
                dtpFromDate.Focus()
                Return False

            End If

            'objProgress._PeriodId = cboPeriod.SelectedValue
            'objProgress._PeriodName = cboPeriod.SelectedItem.Text
            objProgress._FromDate = dtpFromDate.GetDate.Date
            objProgress._ToDate = dtpToDate.GetDate.Date
            objProgress._EmployeeId = cboEmployee.SelectedValue
            objProgress._EmployeeName = cboEmployee.SelectedItem.Text

            objProgress._AdvanceFilter = mstrAdvanceFilter
            objProgress._ViewByIds = mstrStringIds
            objProgress._ViewIndex = mintViewIdx
            objProgress._ViewByName = mstrStringName
            objProgress._Analysis_Fields = mstrAnalysis_Fields
            objProgress._Analysis_Join = mstrAnalysis_Join
            objProgress._Analysis_OrderBy = mstrAnalysis_OrderBy
            objProgress._Report_GroupName = mstrReport_GroupName
            objProgress._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objProgress._FirstNamethenSurname = Session("FirstNamethenSurname")

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objProgress = New clsProgressUpdateNotAttemptedReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            If Not IsPostBack Then
                Call FillCombo()
                ResetValue()
            Else
                mstrStringIds = CStr(Me.ViewState("mstrStringIds"))
                mstrStringName = CStr(Me.ViewState("mstrStringName"))
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysis_Join = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysis_OrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReport_GroupName = CStr(Me.ViewState("mstrReport_GroupName"))
                mstrAdvanceFilter = Me.ViewState("mstrAdvanceFilter").ToString()
                mstrAnalysis_OrderBy_GName = CStr(Me.ViewState("mstrAnalysis_OrderBy_GName"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
            Me.ViewState("mstrAdvanceFilter") = mstrAdvanceFilter
            Me.ViewState("mstrAnalysis_OrderBy_GName") = mstrAnalysis_OrderBy_GName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Call SetDateFormat()
            Dim strFileName As String = String.Empty


            objProgress.ProgressUpdatedNotAttemptedtReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                     , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                     , Session("UserAccessModeSetting").ToString(), True, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)

            If strFileName.Trim.Length <= 0 Then strFileName = objProgress._FileNameAfterExported.Trim
            If strFileName.Contains(".xls") = False Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName & ".xls"
            Else
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName
            End If
            Export.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdvanceFilter.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Controls Event(s) "

    'Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
    '    Dim objPeriod As New clscommom_period_Tran
    '    Try
    '        objPeriod._Periodunkid(Session("Database_Name").ToString()) = CInt(cboPeriod.SelectedValue)

    '        If CInt(cboPeriod.SelectedValue) > 0 Then
    '            dtpFromDate.SetDate = objPeriod._Start_Date.Date
    '            dtpToDate.SetDate = objPeriod._End_Date.Date
    '        Else
    '            dtpFromDate.SetDate = CDate(Session("fin_startdate")).Date
    '            dtpToDate.SetDate = CDate(Session("fin_enddate")).Date
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objPeriod = Nothing
    '    End Try
    'End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
            mstrAnalysis_OrderBy_GName = popupAnalysisBy._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy._Hr_EmployeeTable_Alias = "hremployee_master"
            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

   
End Class
