﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Assessment_Reports_Rpt_Assessment_Planning_Report
    Inherits Basepage

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmBSC_Planning_Report"
    Private objBSCPlanning As clsBSC_Planning_Report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing
    Private mdtEmployee As DataTable
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData
        Try
            dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Emp", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            Dim dCol As New DataColumn
            dCol.ColumnName = "ischeck"
            dCol.DataType = GetType(System.Boolean)
            dCol.DefaultValue = False

            mdtEmployee = dsList.Tables(0).Copy
            mdtEmployee.Columns.Add(dCol)

            Dim xr As DataRow() = mdtEmployee.Select("employeeunkid = 0")
            If xr.Length > 0 Then
                mdtEmployee.Rows.Remove(xr(0))
            End If
            mdtEmployee.AcceptChanges()
            dgvAEmployee.AutoGenerateColumns = False
            dgvAEmployee.DataSource = mdtEmployee


            'Pinkal (27-May-2021) -- Start
            'Enhancement NMB - Working on converting Assessment Report in New UI.
            'If Me.ViewState("mdtEmployee") Is Nothing Then
            '    Me.ViewState.Add("mdtEmployee", mdtEmployee)
            'Else
            '    Me.ViewState("mdtEmployee") = mdtEmployee
            'End If
            'Pinkal (27-May-2021) -- End

            dgvAEmployee.DataBind()

            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
            Dim itm As ListItem
            With cboStatus
                .Items.Clear()
                itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Select"), 0)
                cboStatus.Items.Add(itm)
                itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsBSC_Planning_Report", 1, "Submitted For Approval"), 1)
                cboStatus.Items.Add(itm)
                'S.SANDEEP |12-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {Action List Phase 2 - 61}
                'itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsBSC_Planning_Report", 2, "Final Saved"), 2)
                itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsBSC_Planning_Report", 100, "Final Approved"), 2)
                'S.SANDEEP |12-MAR-2019| -- END
                cboStatus.Items.Add(itm)
                itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsBSC_Planning_Report", 3, "Opened For Changes"), 3)
                cboStatus.Items.Add(itm)
                itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsBSC_Planning_Report", 4, "Not Submitted For Approval"), 4)
                cboStatus.Items.Add(itm)
                itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsBSC_Planning_Report", 5, "Not Planned"), 999)
                cboStatus.Items.Add(itm)
                .SelectedIndex = 0
            End With

            dsList = objMaster.GetAD_Parameter_List(False, "List")
            With cboAppointment
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 1
                .DataBind()
            End With
            Call cboAppointment_SelectedIndexChanged(New Object(), New EventArgs())

            With cboReportType
                .Items.Clear()
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "BSC Planning Report"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 102, "Employee Planning Report"))
                .SelectedIndex = 0
            End With
            Call cboReportType_SelectedIndexChanged(New Object(), New EventArgs())

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            Dim objCScoreMaster As New clsComputeScore_master
            dsList = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
            With cboScoreOption
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
                .DataBind()
            End With
            objCScoreMaster = Nothing
            'S.SANDEEP |27-MAY-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboStatus.SelectedIndex = 0

            dtpS_Dt_From.SetDate = Nothing
            dtpS_Dt_To.SetDate = Nothing

            cboStatus.SelectedIndex = 0
            chkIncludeSummary.Checked = True
            objBSCPlanning.setDefaultOrderBy(0)
            mstrAdvanceFilter = ""
            cboAppointment.SelectedValue = 1
            chkShowComputedScore.Checked = False

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            'S.SANDEEP [09-MAR-2018] -- START
            'ISSUE/ENHANCEMENT : {#Internal Issues}
            objBSCPlanning.SetDefaultValue()
            'S.SANDEEP [09-MAR-2018] -- END
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Please select Period."), Me)
                cboPeriod.Focus()
                Return False
            End If

            objBSCPlanning._PeriodId = cboPeriod.SelectedValue
            objBSCPlanning._PeriodName = cboPeriod.SelectedItem.Text
            objBSCPlanning._Period_St_Date = mdtPeriodStart
            objBSCPlanning._Period_Ed_Date = mdtPeriodEnd
            objBSCPlanning._ReportTypeId = cboReportType.SelectedIndex
            objBSCPlanning._ReportTypeName = cboReportType.SelectedItem.Text

            If cboReportType.SelectedIndex = 0 Then
                If cboAppointment.SelectedValue = enAD_Report_Parameter.APP_DATE_FROM Then
                    If dtpDate1.GetDate <> Nothing AndAlso dtpDate2.GetDate = Nothing Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date."), Me)
                        dtpDate2.Focus()
                        Return False
                    ElseIf dtpDate1.GetDate = Nothing AndAlso dtpDate2.GetDate <> Nothing Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date."), Me)
                        dtpDate1.Focus()
                        Return False
                    ElseIf dtpDate1.GetDate <> Nothing AndAlso dtpDate2.GetDate <> Nothing Then
                        If dtpDate2.GetDate.Date < dtpDate1.GetDate.Date Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Sorry, Appointment To Date cannot be less than Appointment From Date."), Me)
                            dtpDate2.Focus()
                            Return False
                        End If
                    End If
                End If
                'S.SANDEEP [09-MAR-2018] -- START
                'ISSUE/ENHANCEMENT : {#Internal Issues}
                'objBSCPlanning.SetDefaultValue()
                'S.SANDEEP [09-MAR-2018] -- END
                objBSCPlanning._EmployeeId = cboEmployee.SelectedValue
                objBSCPlanning._EmployeeName = cboEmployee.SelectedItem.Text
                If dtpS_Dt_From.GetDate <> Nothing AndAlso dtpS_Dt_To.GetDate <> Nothing Then
                    objBSCPlanning._FilterDate1 = dtpS_Dt_From.GetDate.Date
                    objBSCPlanning._FilterDate2 = dtpS_Dt_To.GetDate.Date
                End If
                objBSCPlanning._StatusId = CInt(CType(cboStatus.SelectedItem, ListItem).Value)
                objBSCPlanning._StatusName = cboStatus.SelectedItem.Text
                objBSCPlanning._IncludeSummary = chkIncludeSummary.Checked
                objBSCPlanning._ViewByIds = mstrStringIds
                objBSCPlanning._ViewIndex = mintViewIdx
                objBSCPlanning._ViewByName = mstrStringName
                objBSCPlanning._Analysis_Fields = mstrAnalysis_Fields
                objBSCPlanning._Analysis_Join = mstrAnalysis_Join
                objBSCPlanning._Analysis_OrderBy = mstrAnalysis_OrderBy
                objBSCPlanning._Report_GroupName = mstrReport_GroupName
                objBSCPlanning._Advance_Filter = mstrAdvanceFilter
                objBSCPlanning._ExcludeInactiveEmployee = chkExcludeInactive.Checked
                objBSCPlanning._AppointmentTypeId = cboAppointment.SelectedValue
                objBSCPlanning._AppointmentTypeName = cboAppointment.SelectedItem.Text
                If dtpDate1.GetDate <> Nothing Then
                    objBSCPlanning._Date1 = dtpDate1.GetDate.Date
                End If
                If dtpDate2.Visible = True Then
                    If dtpDate2.GetDate <> Nothing Then
                        objBSCPlanning._Date2 = dtpDate2.GetDate.Date
                    End If
                End If
                objBSCPlanning._ConsiderEmployeeTerminationOnPeriodDate = chkConsiderEmployeeTermination.Checked
                objBSCPlanning._Database_Start_Date = Session("fin_startdate")

            ElseIf cboReportType.SelectedIndex = 1 Then

                'Pinkal (27-May-2021) -- Start
                'Enhancement NMB - Working on converting Assessment Report in New UI.

                'mdtEmployee = Me.ViewState("mdtEmployee")
                'mdtEmployee.AcceptChanges()
                'Dim dtmp() As DataRow = Nothing
                'dtmp = mdtEmployee.Select("ischeck = true")
                'If dtmp.Length <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 103, "Sorry, Please check atleast one employee to continue."), Me)
                '    Return False
                'End If
                'objBSCPlanning._SeletedEmpids = dtmp.ToDictionary(Of Integer, String)(Function(row) row.Field(Of Integer)("employeeunkid"), Function(row) row.Field(Of String)("EmpCodeName"))

                Dim gRow As IEnumerable(Of DataGridItem) = Nothing
                gRow = dgvAEmployee.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 103, "Sorry, Please check atleast one employee to continue."), Me)
                    Return False
                End If

                objBSCPlanning._SeletedEmpids = gRow.ToDictionary(Of Integer, String)(Function(x) CInt(dgvAEmployee.DataKeys(x.ItemIndex)), Function(x) x.Cells(getColumnId_Datagrid(dgvAEmployee, "dgcolhEmpCodeName", False, True)).Text)

                'Pinkal (27-May-2021) -- End

                objBSCPlanning._CascadingTypeId = Session("CascadingTypeId")
                objBSCPlanning._ShowComputedScore = chkShowComputedScore.Checked
            End If

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            objBSCPlanning._IsCalibrationSettingActive = Session("IsCalibrationSettingActive")
            objBSCPlanning._DisplayScoreName = cboScoreOption.Text
            objBSCPlanning._DisplayScoreType = cboScoreOption.SelectedValue
            'S.SANDEEP |27-MAY-2019| -- END

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)

            objBSCPlanning = New clsBSC_Planning_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

        If Not IsPostBack Then
            Call FillCombo()
            'S.SANDEEP |17-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PM ERROR
        Else
            mstrStringIds = CStr(Me.ViewState("mstrStringIds"))
            mstrStringName = CStr(Me.ViewState("mstrStringName"))
            mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
            mstrAnalysis_Fields = CStr(Me.ViewState("mstrAnalysis_Fields"))
            mstrAnalysis_Join = CStr(Me.ViewState("mstrAnalysis_Join"))
            mstrAnalysis_OrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
            mstrReport_GroupName = CStr(Me.ViewState("mstrReport_GroupName"))
            'S.SANDEEP |17-MAR-2020| -- END
        End If


        'S.SANDEEP |27-MAY-2019| -- START
        'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
        If CBool(Session("IsCalibrationSettingActive")) Then
            pnlCalibrationScore.Visible = True
        Else
            pnlCalibrationScore.Visible = False
        End If
        'S.SANDEEP |27-MAY-2019| -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'S.SANDEEP |17-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : PM ERROR
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |17-MAR-2020| -- END

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()
            objBSCPlanning._UserUnkId = CInt(Session("UserId"))
            objBSCPlanning.generateReportNew(Session("Database_Name").ToString, _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                             CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                             CStr(Session("UserAccessModeSetting")), True, _
                                             CStr(Session("ExportReportPath")), _
                                             CBool(Session("OpenAfterExport")), cboReportType.SelectedIndex, enPrintAction.None, _
                                             enExportAction.None, _
                                             CInt(Session("Base_CurrencyId")))
            Session("objRpt") = objBSCPlanning._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Protected Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If cboReportType.SelectedIndex = 0 Then
                pnlBSC.Visible = True : pnlEmp.Visible = False : chkShowComputedScore.Visible = False
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                lnkAnalysisBy.Visible = True
                'S.SANDEEP |17-MAR-2020| -- END
            ElseIf cboReportType.SelectedIndex = 1 Then
                pnlEmp.Visible = True : pnlBSC.Visible = False : chkShowComputedScore.Visible = True
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                lnkAnalysisBy.Visible = False
                'S.SANDEEP |17-MAR-2020| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboAppointment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAppointment.SelectedIndexChanged
        Try
            objlblCaption.Text = cboAppointment.SelectedItem.Text
            Select Case CInt(cboAppointment.SelectedValue)
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If dtpDate2.Visible = False Then dtpDate2.Visible = True
                    If lblTo.Visible = False Then lblTo.Visible = True
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    dtpDate2.Visible = False : lblTo.Visible = False
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    dtpDate2.Visible = False : lblTo.Visible = False
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
    '    Dim objPeriod As New clscommom_period_Tran
    '    Try
    '        Dim objEmployee As New clsEmployee_Master
    '        Dim dsList As New DataSet
    '        Dim dtStartDate, dtEndDate As Date
    '        dtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
    '        dtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
    '        If CInt(cboPeriod.SelectedValue) > 0 Then
    '            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
    '            mdtPeriodStart = objPeriod._Start_Date
    '            mdtPeriodEnd = objPeriod._End_Date
    '            dtStartDate = mdtPeriodStart
    '            dtEndDate = mdtPeriodEnd
    '        Else
    '            mdtPeriodStart = Nothing
    '            mdtPeriodEnd = Nothing
    '            dtStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
    '            dtEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
    '        End If
    '        dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
    '                                            Session("UserId"), _
    '                                            Session("Fin_year"), _
    '                                            Session("CompanyUnkId"), _
    '                                            dtStartDate, _
    '                                            dtEndDate, _
    '                                            Session("UserAccessModeSetting"), True, _
    '                                            Session("IsIncludeInactiveEmp"), "Emp", True)
    '        With cboEmployee
    '            .DataValueField = "employeeunkid"
    '            .DataTextField = "employeename"
    '            .DataSource = dsList.Tables(0)
    '            .SelectedValue = 0
    '            .DataBind()
    '        End With
    '        objEmployee = Nothing
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub

#End Region

#Region " Checkbox Event(s) "

    'Pinkal (27-May-2021) -- Start
    'Enhancement NMB - Working on converting Assessment Report in New UI.

    'Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If dgvAEmployee.Items.Count <= 0 Then Exit Sub
    '        Dim j As Integer = 0
    '        Dim chkValue As CheckBox = CType(sender, CheckBox)
    '        Dim dvEmployee As DataView = CType(Me.ViewState("mdtEmployee"), DataTable).DefaultView
    '        For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
    '            Dim gvItem As DataGridItem = dgvAEmployee.Items(j)
    '            CType(gvItem.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
    '            Dim dRow() As DataRow = CType(Me.ViewState("mdtEmployee"), DataTable).Select("employeeunkid = '" & gvItem.Cells(2).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("ischeck") = cb.Checked
    '            End If
    '            dvEmployee.Table.AcceptChanges()
    '            j += 1
    '        Next
    '        Me.ViewState("mdtEmployee") = dvEmployee.ToTable
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkHeder1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvItem As DataGridItem = DirectCast(cb.NamingContainer, DataGridItem)
    '        If gvItem.Cells.Count > 0 Then
    '            Dim dRow() As DataRow = CType(Me.ViewState("mdtEmployee"), DataTable).Select("employeeunkid = '" & gvItem.Cells(2).Text & "'")
    '            dRow(0).Item("ischeck") = cb.Checked
    '            CType(Me.ViewState("mdtEmployee"), DataTable).AcceptChanges()
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkbox1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Pinkal (27-May-2021) -- End

#End Region

#Region " Textbox Event(s) "

    Protected Sub txtFrmSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Dim dView As DataView = CType(Me.ViewState("mdtEmployee"), DataTable).DefaultView
            Dim StrSearch As String = String.Empty
            If txtSearchEmp.Text.Trim.Length > 0 Then
                StrSearch = "EmpCodeName LIKE '%" & txtSearchEmp.Text & "%' "
            End If
            dView.RowFilter = StrSearch
            dgvAEmployee.AutoGenerateColumns = False
            dgvAEmployee.DataSource = dView
            dgvAEmployee.DataBind()
            txtSearchEmp.Focus()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Procedure txtFrmSearch_TextChanged : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'S.SANDEEP |17-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : PM ERROR
#Region " Link Event(s) "

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            'Dim objPeriod As New clscommom_period_Tran
            'objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |17-MAR-2020| -- END

End Class
