﻿<%@ Page Title="Progress Update Not Attempted Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_ProgressUpdateNotAttemptedReport.aspx.vb"
    Inherits="Reports_Assessment_Reports_Rpt_ProgressUpdateNotAttemptedReport" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="popupAnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Progress Update Not Attempted Report"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkSetAnalysis" runat="server" ToolTip="Analysis By">
                                                 <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <%-- <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack ="true" />
                                                </div>
                                            </div>
                                        </div>--%>
                                       <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="LblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                                <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="LblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                                <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboEmployee" runat="server" />
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnAdvanceFilter" runat="server" Text="Advance Filter" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                    <uc1:popupAnalysisBy ID="popupAnalysisBy" runat="server" />
                    <uc7:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </div>
                <uc9:Export runat="server" ID="Export" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
