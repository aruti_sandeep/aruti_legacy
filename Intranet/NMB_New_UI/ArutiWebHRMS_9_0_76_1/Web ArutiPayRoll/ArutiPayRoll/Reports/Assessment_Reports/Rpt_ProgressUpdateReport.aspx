﻿<%@ Page Title="Progress Update Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_ProgressUpdateReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_ProgressUpdateReport" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="popupAnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function checkRowCheckbox(control, chkboxID, headerchkboxID) {
            var grid = control.closest("table");
            var chkHeader = $("[id*=" + headerchkboxID + "]", grid);
            var row = control.closest("tr")[0];

            debugger;
            if (!control.is(":checked")) {
                var row = control.closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {
                if ($("[id*=" + chkboxID + "]", grid).length == $("[id*=" + chkboxID + "]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        }
        function checkHeader(control) {
            var chkHeader = control;
            var grid = control.closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    $(this).attr("checked", "checked");

                } else {
                    $(this).removeAttr("checked");
                }
            });
        }

        $("body").on("click", '[id*=chkHeder]', function() {
            checkHeader($(this));
        });

        //$("[id*=chkbox]").live("click", function () {
        $("body").on("click", '[id*=chkbox]', function() {
            checkRowCheckbox($(this), 'chkbox', 'chkHeder');
        });

        //$("[id*=chkaHeder]").live("click", function () {
        $("body").on("click", '[id*=chkaHeder]', function() {
            checkHeader($(this));
        });

        //$("[id*=chkabox]").live("click", function () {
        $("body").on("click", '[id*=chkabox]', function() {
            checkRowCheckbox($(this), 'chkabox', 'chkaHeder');
        });
    </script>

    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Progress Update Report"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" ToolTip="Analysis By">
                                                 <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 245px">
                                            <asp:DataGrid ID="lvDisplayCol" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="25">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkHeder" runat="server" Enabled="true" Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkbox" runat="server" Enabled="true" Text=" " />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Name" HeaderText="Display Column On Report" />
                                                    <asp:BoundColumn DataField="objcolhSelectCol" HeaderText="objcolhSelectCol" Visible="false" />
                                                    <asp:BoundColumn DataField="objcolhJoin" HeaderText="objcolhJoin" Visible="false" />
                                                    <asp:BoundColumn DataField="objcolhDisplay" HeaderText="objcolhDisplay" Visible="false" />
                                                    <asp:BoundColumn DataField="Id" HeaderText="Id" Visible="false" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Panel ID="pnlData" runat="server">
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblUpdateDateFrm" runat="server" Text="Update Date From" CssClass="form-label"></asp:Label>
                                                    <uc2:DateCtrl ID="dtpUpdateFrom" runat="server" AutoPostBack="false" />
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblUpdateDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                                    <uc2:DateCtrl ID="dtpUpdateTo" runat="server" AutoPostBack="false" />
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblApprovalDateFrm" runat="server" Text="Approval Date From" CssClass="form-label"></asp:Label>
                                                    <uc2:DateCtrl ID="dtpApprovalFrom" runat="server" AutoPostBack="false" />
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblApprovalDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                                    <uc2:DateCtrl ID="dtpApprovalTo" runat="server" AutoPostBack="false" />
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblApprovalStatus" runat="server" Text="Approval Status" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboApprovalStatus" runat="server" AutoPostBack="true" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblAllocation" runat="server" Text="Allocations" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboAllocations" runat="server" AutoPostBack="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <asp:Panel ID="pnlAllocation" runat="server" Width="100%">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true" class="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="table-responsive" style="height: 220px">
                                                        <asp:DataGrid ID="lvAllocation" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderStyle-Width="25">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkaHeder" runat="server" Enabled="true" Text=" " />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkabox" runat="server" Enabled="true" Text=" " />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="Name" />
                                                                <asp:BoundColumn DataField="Id" HeaderText="Id" Visible="false" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnAdvFilter" runat="server" Text="Advance Filter" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                    <uc1:popupAnalysisBy ID="popupAnalysisBy" runat="server" />
                    <uc7:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </div>
                <uc9:Export runat="server" ID="Export" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
