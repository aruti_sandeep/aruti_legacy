﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region
Partial Class Reports_Assessment_Reports_Rpt_Performance_AppraisalReport
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPerformance_AppraisalReport"
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private objPerformance As clsPerformance_ApprisalReport
    Private mdtGrades As DataTable
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objGradeGroup As New clsGradeGroup
        Try
            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "Emp", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objGradeGroup.GetList("List", True)
            mdtGrades = dsList.Tables("List").Copy()

            Dim dCol As New DataColumn
            dCol.ColumnName = "ischeck"
            dCol.DataType = GetType(System.Boolean)
            dCol.DefaultValue = False
            mdtGrades.Columns.Add(dCol)

            lvGradeGroup.DataSource = mdtGrades
            'If Me.ViewState("mdtGrades") Is Nothing Then
            '    Me.ViewState.Add("mdtGrades", mdtGrades)
            'Else
            '    Me.ViewState("mdtGrades") = mdtGrades
            'End If
            lvGradeGroup.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmp = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            chkIncludeInactiveEmp.Checked = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objPerformance.SetDefaultValue()

            Dim StrGradesIds, StrGradeNames As String
            StrGradesIds = String.Empty : StrGradeNames = String.Empty


            'Pinkal (27-May-2021) -- Start
            'Enhancement NMB - Working on converting Assessment Report in New UI.

            'If CType(Me.ViewState("mdtGrades"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() < 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please Check atleast one Grade Group to export report."), Me)
            '    Return False
            'End If

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = lvGradeGroup.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please Check atleast one Grade Group to export report."), Me)
                Return False
            End If

            'Pinkal (27-May-2021) -- End

            objPerformance._EmployeeName = cboEmployee.Text
            objPerformance._EmployeeUnkid = cboEmployee.SelectedValue


            'Pinkal (27-May-2021) -- Start
            'Enhancement NMB - Working on converting Assessment Report in New UI.
            'StrGradesIds = String.Join(",", CType(Me.ViewState("mdtGrades"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of Integer)("gradegroupunkid").ToString()).ToArray())
            'StrGradeNames = String.Join(",", CType(Me.ViewState("mdtGrades"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) y.Field(Of String)("name").ToString()).ToArray())

            StrGradesIds = String.Join(",", gRow.Select(Function(x) lvGradeGroup.DataKeys(x.ItemIndex).ToString()).ToArray())
            StrGradeNames = String.Join(",", gRow.Select(Function(x) lvGradeGroup.Items(x.ItemIndex).Cells(getColumnId_Datagrid(lvGradeGroup, "dgcolhName", False, True)).Text).ToArray())

            'Pinkal (27-May-2021) -- End

            objPerformance._GradesIds = StrGradesIds
            objPerformance._GradeNames = StrGradeNames
            objPerformance._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            objPerformance = New clsPerformance_ApprisalReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        If Not IsPostBack Then
            Call FillCombo()
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()
            Dim strExportFileName As String = String.Empty
            objPerformance._UserName = CInt(Session("UserId"))
            objPerformance.Export_Performance_Appraisal_Report(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                               CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                               CStr(Session("UserAccessModeSetting")), True, _
                                                               IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                               False, strExportFileName)
            If strExportFileName.Trim <> "" Then
                'Pinkal (27-May-2021) -- Start
                'Enhancement NMB - Working on converting Assessment Report in New UI.
                If strExportFileName.Contains(".xls") = False Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strExportFileName & ".xls"
                Else
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strExportFileName
                End If
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog('" & Session("rootpath").ToString & "');", True)
                Export.Show()
                'Pinkal (27-May-2021) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    'Pinkal (27-May-2021) -- Start
    'Enhancement NMB - Working on converting Assessment Report in New UI.

    'Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If lvGradeGroup.Items.Count <= 0 Then Exit Sub
    '        Dim j As Integer = 0
    '        Dim chkValue As CheckBox = CType(sender, CheckBox)
    '        Dim dvEmployee As DataView = CType(Me.ViewState("mdtGrades"), DataTable).DefaultView
    '        For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
    '            Dim gvItem As DataGridItem = lvGradeGroup.Items(j)
    '            CType(gvItem.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
    '            Dim dRow() As DataRow = CType(Me.ViewState("mdtGrades"), DataTable).Select("gradegroupunkid = '" & gvItem.Cells(2).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("ischeck") = cb.Checked
    '            End If
    '            dvEmployee.Table.AcceptChanges()
    '            j += 1
    '        Next
    '        Me.ViewState("mdtGrades") = dvEmployee.ToTable
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkHeder1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvItem As DataGridItem = DirectCast(cb.NamingContainer, DataGridItem)
    '        If gvItem.Cells.Count > 0 Then
    '            Dim dRow() As DataRow = CType(Me.ViewState("mdtGrades"), DataTable).Select("gradegroupunkid = '" & gvItem.Cells(2).Text & "'")
    '            dRow(0).Item("ischeck") = cb.Checked
    '            CType(Me.ViewState("mdtGrades"), DataTable).AcceptChanges()
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkbox1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Pinkal (27-May-2021) -- End

#End Region

End Class
