﻿<%@ Page Title="Group Training Cost Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_GroupTrainingCostReport.aspx.vb" Inherits="Reports_Training_Rpt_GroupTrainingCostReport" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="row clearfix d--f jc--c ai--c">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Group Training Cost Report"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                           <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblTrainingCalendar" runat="server" Text="Training Calendar"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboTrainingCalendar" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboStatus" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblTrainingName" runat="server" Text="Training Name"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboTrainingName" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                             
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblApplicationDateFrom" runat="server" Text="Application Date From" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpApplicationDateFrom" runat="server"></uc2:DateCtrl>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblApplicationDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpApplicationDateTo" runat="server"></uc2:DateCtrl>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblTrainingDateFrom" runat="server" Text="Training Date From" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpTrainingDateFrom" runat="server"></uc2:DateCtrl>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblTrainingDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpTrainingDateTo" runat="server"></uc2:DateCtrl>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
            <uc9:Export runat="server" ID="Export" />
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Export" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
