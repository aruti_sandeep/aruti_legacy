﻿#Region " Imports "

Option Strict On
Imports Aruti.Data
Imports System.Data
Imports ArutiReports

#End Region

Partial Class Reports_Training_Rpt_TrainingRequestBudgetReport
    Inherits Basepage

#Region " Private Variable(s) "
    Private DisplayMessage As New CommonCodes
    Private objTrainingBudgetReport As clsTrainingRequestBudgetReport
    Private Shared ReadOnly mstrModuleName As String = "frmTrainingRequestBudgetReport"
#End Region

#Region "Page Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objTrainingBudgetReport = New clsTrainingRequestBudgetReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
                Call ResetValue()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim dsList As DataSet = Nothing
        Dim intColType As Integer = 0
        Dim dtTable As DataTable
        Dim strAllocationName As String = ""

        Try

            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DataValueField = "calendarunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            Select Case CInt(Session("TrainingBudgetAllocationID"))

                Case enAllocation.BRANCH
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 430, "Branch")
                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    objBranch = Nothing

                Case enAllocation.DEPARTMENT_GROUP
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 429, "Department Group")
                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    objDeptGrp = Nothing

                Case enAllocation.DEPARTMENT
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 428, "Department")
                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    objDept = Nothing

                Case enAllocation.SECTION_GROUP
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 427, "Section Group")
                    Dim objSG As New clsSectionGroup
                    dsList = objSG.GetList("List", True)
                    objSG = Nothing

                Case enAllocation.SECTION
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 426, "Section")
                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    objSection = Nothing

                Case enAllocation.UNIT_GROUP
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 425, "Unit Group")
                    Dim objUG As New clsUnitGroup
                    dsList = objUG.GetList("List", True)
                    objUG = Nothing

                Case enAllocation.UNIT
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 424, "Unit")
                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    objUnit = Nothing

                Case enAllocation.TEAM
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 423, "Team")
                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    objTeam = Nothing

                Case enAllocation.JOB_GROUP
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 422, "Job Group")
                    Dim objjobGRP As New clsJobGroup
                    dsList = objjobGRP.GetList("List", True)
                    objjobGRP = Nothing

                Case enAllocation.JOBS
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 421, "Jobs")
                    Dim objJobs As New clsJobs
                    dsList = objJobs.GetList("List", True)
                    objJobs = Nothing

                    intColType = 1

                Case enAllocation.CLASS_GROUP
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 420, "Class Group")
                    Dim objClassGrp As New clsClassGroup
                    dsList = objClassGrp.GetList("List", True)
                    objClassGrp = Nothing

                Case enAllocation.CLASSES
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 419, "Classes")
                    Dim objClass As New clsClass
                    dsList = objClass.GetList("List", True)
                    objClass = Nothing

                Case enAllocation.COST_CENTER
                    strAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 586, "Cost Center")
                    Dim objConstCenter As New clscostcenter_master
                    dsList = objConstCenter.GetList("List", True)
                    objConstCenter = Nothing

                    intColType = 2

            End Select

            If dsList IsNot Nothing Then

                dtTable = New DataView(dsList.Tables(0)).ToTable

                If intColType = 0 Then
                    dtTable.Columns(0).ColumnName = "Id"
                    dtTable.Columns("code").ColumnName = "Code"
                    dtTable.Columns("name").ColumnName = "Name"
                ElseIf intColType = 1 Then
                    dtTable.Columns("jobunkid").ColumnName = "Id"
                    dtTable.Columns("Code").ColumnName = "Code"
                    dtTable.Columns("jobname").ColumnName = "Name"
                ElseIf intColType = 2 Then
                    dtTable.Columns(0).ColumnName = "Id"
                    dtTable.Columns("costcentercode").ColumnName = "Code"
                    dtTable.Columns("costcentername").ColumnName = "Name"
                ElseIf intColType = 3 Then
                    dtTable.Columns(1).ColumnName = "Id"
                    dtTable.Columns(2).ColumnName = "Code"
                    dtTable.Columns(0).ColumnName = "Name"
                ElseIf intColType = 4 Then
                    dtTable.Columns(0).ColumnName = "Id"
                    dtTable.Columns("country_code").ColumnName = "Code"
                    dtTable.Columns("country_name").ColumnName = "Name"
                ElseIf intColType = 5 Then
                    dtTable.Columns("employeeunkid").ColumnName = "Id"
                    dtTable.Columns("employeecode").ColumnName = "Code"
                    dtTable.Columns("employeename").ColumnName = "Name"
                End If

                dtTable = New DataView(dtTable, "", "Name", DataViewRowState.CurrentRows).ToTable

                Dim row As DataRow = dtTable.NewRow
                row.Item("id") = 0
                row.Item("code") = ""
                row.Item("name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, " Select")
                dtTable.Rows.InsertAt(row, 0)

                With cboAllocationTran
                    .DataTextField = "Name"
                    .DataValueField = "Id"
                    .DataSource = dtTable
                    .DataBind()
                    .SelectedValue = "0"
                End With

                lblAllocationTran.Text = strAllocationName
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            cboTrainingCalendar.SelectedIndex = 0
            cboAllocationTran.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboTrainingCalendar.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please Select Traininig Calendar."), Me)
                cboTrainingCalendar.Focus()
                Exit Function
            End If

            objTrainingBudgetReport.SetDefaultValue()

            objTrainingBudgetReport._CurrentPeriodUnkid = CInt(cboTrainingCalendar.SelectedValue)
            objTrainingBudgetReport._CurrentPeriodName = cboTrainingCalendar.SelectedItem.Text

            objTrainingBudgetReport._PreviousPeriodUnkid = 0
            objTrainingBudgetReport._PreviousPeriodName = ""

            objTrainingBudgetReport._TrainingNeedAllocationID = CInt(Session("TrainingNeedAllocationID"))
            objTrainingBudgetReport._BudgetAllocationID = CInt(Session("TrainingBudgetAllocationID"))
            objTrainingBudgetReport._TrainingCostCenterAllocationID = CInt(Session("TrainingCostCenterAllocationID"))
            objTrainingBudgetReport._TrainingRemainingBalanceBasedOnID = CInt(Session("TrainingRemainingBalanceBasedOnID"))

            If CInt(cboAllocationTran.SelectedValue) > 0 Then
                objTrainingBudgetReport._AllocationTranIDs = CInt(cboAllocationTran.SelectedValue).ToString
                objTrainingBudgetReport._AllocationTranNAMEs = cboAllocationTran.SelectedItem.Text
            Else
                objTrainingBudgetReport._AllocationTranIDs = ""
                objTrainingBudgetReport._AllocationTranNAMEs = ""
            End If

            Select Case CInt(Session("TrainingNeedAllocationID"))
                Case enAllocation.BRANCH
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 430, "Branch")
                Case enAllocation.DEPARTMENT_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 429, "Department Group")
                Case enAllocation.DEPARTMENT
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 428, "Department")
                Case enAllocation.SECTION_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 427, "Section Group")
                Case enAllocation.SECTION
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 426, "Section")
                Case enAllocation.UNIT_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 425, "Unit Group")
                Case enAllocation.UNIT
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 424, "Unit")
                Case enAllocation.TEAM
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 423, "Team")
                Case enAllocation.JOB_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 422, "Job Group")
                Case enAllocation.JOBS
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 421, "Jobs")
                Case enAllocation.CLASS_GROUP
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 420, "Class Group")
                Case enAllocation.CLASSES
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 419, "Classes")
                Case enAllocation.COST_CENTER
                    objTrainingBudgetReport._TrainingNeedAllocationName = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMasterData", 586, "Cost Center")
                Case Else
                    objTrainingBudgetReport._TrainingNeedAllocationName = ""
            End Select

            GUI.fmtCurrency = Session("fmtCurrency").ToString()

            objTrainingBudgetReport.setDefaultOrderBy(0)

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try

    End Function

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objTrainingBudgetReport.setDefaultOrderBy(0)

            Call SetDateFormat()
            Dim strFilePath As String = System.IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim ExportReportPath As String = System.IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            Dim OpenAfterExport As Boolean = False

            objTrainingBudgetReport.Generate_DetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                          CInt(Session("CompanyUnkId")), _
                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                          Session("UserAccessModeSetting").ToString(), True, False, ExportReportPath, _
                                                          OpenAfterExport)


            If objTrainingBudgetReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objTrainingBudgetReport._FileNameAfterExported
                Export.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try


    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


#Region " Language "
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblAllocationTran.ID, Me.lblAllocationTran.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnExport.ID, Me.btnExport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblAllocationTran.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAllocationTran.ID, Me.lblAllocationTran.Text)
            Me.lblTrainingCalendar.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Trim.Replace("&", "")
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text).Trim.Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Trim.Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Please Select Training Calendar.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, " Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 419, "Classes")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 420, "Class Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 421, "Jobs")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 422, "Job Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 423, "Team")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 424, "Unit")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 425, "Unit Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 426, "Section")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 427, "Section Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 428, "Department")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 429, "Department Group")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 430, "Branch")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMasterData", 586, "Cost Center")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
