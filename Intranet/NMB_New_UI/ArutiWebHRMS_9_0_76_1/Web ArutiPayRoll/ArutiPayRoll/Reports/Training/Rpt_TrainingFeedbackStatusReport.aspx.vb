﻿Option Strict On
Imports Aruti.Data
Imports System.Data
Imports ArutiReports
Partial Class Reports_Training_Rpt_TrainingFeedbackStatusReport
    Inherits Basepage

#Region " Private Variable(s) "

    Private DisplayMessage As New CommonCodes
    Private objTrainingFeedbackStatusReport As clsTrainingFeedbackStatusReport
    Private ReadOnly mstrModuleName As String = "frmTrainingFeedbackStatusReport"

#End Region

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objTrainingFeedbackStatusReport = New clsTrainingFeedbackStatusReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
                'Call ResetValue()              
            Else

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Function(s) & Method(s) "
    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objMasterData As New clsMasterData
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objCommon As New clsCommon_Master
        Dim objEmp As New clsEmployee_Master

        Try

            dsCombo = objMasterData.GetTrainingEvaluationFeedbackModeList(True, "List")
            With cboEvaluationCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingName
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            With cboFeedbackStatus
                .Items.Clear()
                'Language.setLanguage(mstrModuleName)
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Select"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Done"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Not Done"))
                .SelectedIndex = 0
            End With

            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DataValueField = "calendarunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              Session("UserAccessModeSetting").ToString, True, _
                                             True, "Emp", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMasterData = Nothing
            objCalendar = Nothing
            objCommon = Nothing
            objEmp = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEvaluationCategory.SelectedIndex = 0
            cboTrainingName.SelectedIndex = 0
            cboFeedbackStatus.SelectedIndex = 0
            cboTrainingCalendar.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            'Hemant (25 Oct 2021) -- Start
            'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
            chkInactiveemp.Checked = False
            'Hemant (25 Oct 2021) -- End
            objTrainingFeedbackStatusReport.setDefaultOrderBy(0)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboEvaluationCategory.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please Select Evaluation Category."), Me)
                cboEvaluationCategory.Focus()
                Exit Function
            End If

            If CInt(cboTrainingName.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please Select Training Name."), Me)
                cboTrainingName.Focus()
                Exit Function
            End If

            objTrainingFeedbackStatusReport.SetDefaultValue()

            objTrainingFeedbackStatusReport._FeedbackModeid = CInt(cboEvaluationCategory.SelectedValue)
            objTrainingFeedbackStatusReport._FeedbackModeName = cboEvaluationCategory.SelectedItem.Text
            objTrainingFeedbackStatusReport._TrainingUnkid = CInt(cboTrainingName.SelectedValue)
            objTrainingFeedbackStatusReport._TrainingName = cboTrainingName.SelectedItem.Text
            objTrainingFeedbackStatusReport._FeedbackStatusid = CInt(cboFeedbackStatus.SelectedIndex)
            objTrainingFeedbackStatusReport._FeedbackStatusName = cboFeedbackStatus.SelectedItem.Text
            objTrainingFeedbackStatusReport._CalendarUnkid = CInt(cboTrainingCalendar.SelectedValue)
            objTrainingFeedbackStatusReport._CalendarName = cboTrainingCalendar.SelectedItem.Text
            objTrainingFeedbackStatusReport._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            objTrainingFeedbackStatusReport._EmployeeName = cboEmployee.SelectedItem.Text
            'Hemant (25 Oct 2021) -- Start
            'ENHANCEMENT : OLD-496 - Enhancement of Training Reports to show Active Employee List by Default.
            objTrainingFeedbackStatusReport._IsActive = CBool(chkInactiveemp.Checked)
            'Hemant (25 Oct 2021) -- End
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try

    End Function
#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objTrainingFeedbackStatusReport.setDefaultOrderBy(0)


            Call SetDateFormat()
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim ExportReportPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            Dim OpenAfterExport As Boolean = False


            objTrainingFeedbackStatusReport.Generate_DetailReport(Session("Database_Name").ToString(), _
                                                                  CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                  CInt(Session("CompanyUnkId")), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                  Session("UserAccessModeSetting").ToString(), True, False, ExportReportPath, _
                                                                  OpenAfterExport)



            If objTrainingFeedbackStatusReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objTrainingFeedbackStatusReport._FileNameAfterExported
                Export.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Language "
    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEvaluationCategory.ID, Me.lblEvaluationCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTrainingName.ID, Me.lblTrainingName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblFeedbackStatus.ID, Me.lblFeedbackStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnExport.ID, Me.btnExport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblEvaluationCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEvaluationCategory.ID, Me.lblEvaluationCategory.Text)
            Me.lblTrainingName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTrainingName.ID, Me.lblTrainingName.Text)
            Me.lblFeedbackStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFeedbackStatus.ID, Me.lblFeedbackStatus.Text)
            Me.lblTrainingCalendar.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)

            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Please Select Evaluation Category.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Please Select Training Name.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Done")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Not Done")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
