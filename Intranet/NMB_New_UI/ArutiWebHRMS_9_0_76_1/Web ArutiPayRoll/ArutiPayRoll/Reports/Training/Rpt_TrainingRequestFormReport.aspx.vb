﻿Option Strict On
#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports ArutiReports

#End Region

Partial Class Reports_Training_Rpt_TrainingRequestFormReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmTrainingRequestFormReport"

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            GC.Collect()
            SetLanguage()

            If Not IsPostBack Then
                Call FillCombo()

                cboRequester_SelectedIndexChanged(New Object, New EventArgs())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objCommon As New clsCommon_Master
        Dim objTrainingRequest As New clstraining_request_master
        Dim dsList As New DataSet
        Try
            dsList = objTrainingRequest.getTrainingRequesterUserList("User", True, CInt(Session("CompanyUnkId")))
            With cboRequester
                .DataValueField = "userunkid"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("User")
                .SelectedValue = "0"
                .DataBind()
            End With


            dsList = objMaster.getComboListForTrainingRequestInsertForm(True, "List")
            With cboRequestType
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With


            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              Session("UserAccessModeSetting").ToString(), True, _
                                              CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                              blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)


            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            dsList = objMaster.getComboListForTrainingType(True, "List")
            With cboTrainingType
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

          
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objEmployee = Nothing
            objMaster = Nothing
            objCommon = Nothing
            objTrainingRequest = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboRequestType.SelectedIndex = 0
            cboRequester.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            cboTrainingType.SelectedIndex = 0
            cboTrainingName.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter(ByRef objTrainingRequestForm As clsTrainingRequestFormReport) As Boolean
        Try
            If CInt(cboRequestType.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please Select Request Type."), Me)
                cboRequestType.Focus()
                Exit Function
            End If

            If CInt(cboRequester.SelectedIndex) = 0 AndAlso CInt(cboRequestType.SelectedValue) = enTrainingFormType.Group_Training Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please Select Requester."), Me)
                cboRequester.Focus()
                Exit Function
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 AndAlso CInt(cboRequestType.SelectedValue) = enTrainingFormType.Individual_Training Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Please Select Employee."), Me)
                cboEmployee.Focus()
                Exit Function
            End If

            If CInt(cboTrainingType.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Please Select Training Type."), Me)
                cboTrainingType.Focus()
                Exit Function
            End If

            If CInt(cboTrainingName.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Training Name."), Me)
                cboTrainingName.Focus()
                Exit Function
            End If

            objTrainingRequestForm.SetDefaultValue()

            objTrainingRequestForm._RequestTypeId = CInt(cboRequestType.SelectedValue)
            objTrainingRequestForm._RequestTypeName = cboRequestType.SelectedItem.Text
            objTrainingRequestForm._RequesterId = CInt(cboRequester.SelectedValue)
            objTrainingRequestForm._RequesterName = cboRequester.SelectedItem.Text
            objTrainingRequestForm._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            objTrainingRequestForm._EmployeeName = cboEmployee.SelectedItem.Text
            objTrainingRequestForm._TrainingTypeId = CInt(cboTrainingType.SelectedValue)
            objTrainingRequestForm._TrainingTypeName = cboTrainingType.SelectedItem.Text
            objTrainingRequestForm._TrainingUnkid = CInt(cboTrainingName.SelectedValue)
            objTrainingRequestForm._TrainingName = cboTrainingName.SelectedItem.Text

            If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                Dim objUser As New clsUserAddEdit
                Dim strUserName As String = String.Empty
                objUser._Userunkid = CInt(Session("UserId"))
                strUserName = objUser._Firstname & " " & objUser._Lastname
                If strUserName.Trim.Length <= 0 Then strUserName = objUser._Username
                objTrainingRequestForm._User = strUserName
                objUser = Nothing
            Else
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date) = CInt(HttpContext.Current.Session("Employeeunkid"))
                objTrainingRequestForm._User = objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname
                objEmp = Nothing
            End If



            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function
#End Region

#Region " Button's Event(s) "
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click

        Dim objTrainingRequestForm As New clsTrainingRequestFormReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

        Try

            If SetFilter(objTrainingRequestForm) = False Then Exit Sub


            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objTrainingRequestForm._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objTrainingRequestForm._UserUnkId = CInt(Session("UserId"))


            If CInt(Session("Employeeunkid")) > 0 Then
                objTrainingRequestForm._UserName = Session("DisplayName").ToString()
            End If


            objTrainingRequestForm.generateReportNew(CStr(Session("Database_Name")), _
                                           CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                           CStr(Session("UserAccessModeSetting")), True, _
                                           CStr(Session("ExportReportPath")), _
                                           CBool(Session("OpenAfterExport")), _
                                           0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))

            Session("objRpt") = objTrainingRequestForm._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTrainingRequestForm = Nothing
        End Try
    End Sub
#End Region


#Region "ComboBox Event"

    Protected Sub cboRequestType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRequestType.SelectedIndexChanged
        Try

            cboEmployee.SelectedIndex = 0
            cboEmployee.Enabled = True
            If CInt(cboRequestType.SelectedValue) = enTrainingFormType.Group_Training Then
                cboEmployee.Enabled = False
            End If

            cboRequester_SelectedIndexChanged(New Object, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub cboRequester_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRequester.SelectedIndexChanged, _
                                                                                                                   cboEmployee.SelectedIndexChanged, _
                                                                                                                   cboTrainingType.SelectedIndexChanged
        Dim objTrainingRequestForm As New clsTrainingRequestFormReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

        Dim dsList As DataSet = Nothing
        Try

            dsList = objTrainingRequestForm.getTrainingList(CInt(cboRequestType.SelectedValue), CInt(cboRequester.SelectedValue), CInt(cboEmployee.SelectedValue), CInt(cboTrainingType.SelectedValue), True)

            cboTrainingName.DataValueField = "coursemasterunkid"
            cboTrainingName.DataTextField = "name"
            cboTrainingName.DataSource = dsList.Tables(0)
            cboTrainingName.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)


            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)

            Me.lblRequestType.Text = Language._Object.getCaption(Me.lblRequestType.ID, Me.lblRequestType.Text)
            Me.lblRequester.Text = Language._Object.getCaption(Me.lblRequester.ID, Me.lblRequester.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblTrainingType.Text = Language._Object.getCaption(Me.lblTrainingType.ID, Me.lblTrainingType.Text)
            Me.lblTrainingName.Text = Language._Object.getCaption(Me.lblTrainingName.ID, Me.lblTrainingName.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnReport.Text = Language._Object.getCaption(Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class
