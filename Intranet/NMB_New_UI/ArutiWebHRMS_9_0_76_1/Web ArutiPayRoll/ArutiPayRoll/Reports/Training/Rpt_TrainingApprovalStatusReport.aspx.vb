﻿Option Strict On
Imports Aruti.Data
Imports System.Data
Imports ArutiReports
Partial Class Reports_Training_Rpt_TrainingApprovalStatusReport
    Inherits Basepage

#Region " Private Variable(s) "
    Private DisplayMessage As New CommonCodes
    Private objTrainingApprovalStatusReport As clsTrainingApprovalStatusReport
    Private Shared ReadOnly mstrModuleName As String = "frmTrainingApprovalStatusReport"

#End Region

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objTrainingApprovalStatusReport = New clsTrainingApprovalStatusReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
                Call ResetValue()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Function(s) & Method(s) "
    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master
        Dim objMaster As New clsMasterData

        Try

            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DataValueField = "calendarunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objMaster.getComboListForTrainingRequestInsertForm(True, "List")
            With cboRequestType
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objMaster.getComboListForTrainingRequestStatus(True, "List")
            With cboStatus
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboTrainingName
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            Session("UserAccessModeSetting").ToString, True, _
                                           True, "Emp", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmp = Nothing
            objCalendar = Nothing
            objCommon = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboTrainingCalendar.SelectedIndex = 0
            cboTrainingName.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboTrainingCalendar.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please Select Traininig Calendar."), Me)
                cboTrainingCalendar.Focus()
                Exit Function
            End If

            If CInt(cboRequestType.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please Select Request Type."), Me)
                cboRequestType.Focus()
                Exit Function
            End If



            objTrainingApprovalStatusReport.SetDefaultValue()

            objTrainingApprovalStatusReport._CalendarUnkid = CInt(cboTrainingCalendar.SelectedValue)
            objTrainingApprovalStatusReport._CalendarName = cboTrainingCalendar.SelectedItem.Text
            objTrainingApprovalStatusReport._RequestTypeId = CInt(cboRequestType.SelectedValue)
            objTrainingApprovalStatusReport._RequestTypeName = cboRequestType.SelectedItem.Text
            objTrainingApprovalStatusReport._StatusUnkid = CInt(cboStatus.SelectedValue)
            objTrainingApprovalStatusReport._StatusName = cboStatus.SelectedItem.Text
            objTrainingApprovalStatusReport._TrainingUnkid = CInt(cboTrainingName.SelectedValue)
            objTrainingApprovalStatusReport._TrainingName = cboTrainingName.SelectedItem.Text
            objTrainingApprovalStatusReport._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            objTrainingApprovalStatusReport._EmployeeName = cboEmployee.SelectedItem.Text
            objTrainingApprovalStatusReport._fmtCurrency = Session("fmtCurrency").ToString()

            If dtpApplicationDateFrom.IsNull = False Then
                objTrainingApprovalStatusReport._ApplicationDateFrom = dtpApplicationDateFrom.GetDate.Date
            End If

            If dtpApplicationDateTo.IsNull = False Then
                objTrainingApprovalStatusReport._ApplicationDateTo = dtpApplicationDateTo.GetDate.Date
            End If

            If dtpTrainingDateFrom.IsNull = False Then
                objTrainingApprovalStatusReport._TrainingDateFrom = dtpTrainingDateFrom.GetDate.Date
            End If

            If dtpTrainingDateTo.IsNull = False Then
                objTrainingApprovalStatusReport._TrainingDateTo = dtpTrainingDateTo.GetDate.Date
            End If
            GUI.fmtCurrency = Session("fmtCurrency").ToString()

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try

    End Function
#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objTrainingApprovalStatusReport.setDefaultOrderBy(0)


            Call SetDateFormat()
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim ExportReportPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            Dim OpenAfterExport As Boolean = False

            objTrainingApprovalStatusReport.Generate_DetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                Session("UserAccessModeSetting").ToString(), True, False, ExportReportPath, _
                                                                OpenAfterExport)


            If objTrainingApprovalStatusReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objTrainingApprovalStatusReport._FileNameAfterExported
                Export.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Language "
    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Language._Object.setCaption(Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Language._Object.setCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Language._Object.setCaption(Me.lblTrainingName.ID, Me.lblTrainingName.Text)
            Language._Object.setCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)

            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text)
            Language._Object.setCaption(Me.btnExport.ID, Me.btnExport.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblTrainingCalendar.Text = Language._Object.getCaption(Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblTrainingName.Text = Language._Object.getCaption(Me.lblTrainingName.ID, Me.lblTrainingName.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select Traininig Calendar.")
            Language.setMessage(mstrModuleName, 2, "Please Select Request Type.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
