﻿Option Strict On
Imports Aruti.Data
Imports System.Data
Imports ArutiReports
Partial Class Reports_Training_Rpt_MonthlyTrainingRequestsReport
    Inherits Basepage

#Region " Private Variable(s) "

    Private DisplayMessage As New CommonCodes
    Private objMonthlyTrainingRequestsReport As clsMonthlyTrainingRequestsReport
    Private Shared ReadOnly mstrModuleName As String = "frmMonthlyTrainingRequestsReport"

#End Region

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objMonthlyTrainingRequestsReport = New clsMonthlyTrainingRequestsReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
                Call ResetValue()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCalendar As New clsTraining_Calendar_Master
        Dim objCommon As New clsCommon_Master
        Dim objMaster As New clsMasterData

        Try

            dsCombo = objCalendar.getListForCombo("List", True)
            With cboTrainingCalendar
                .DataValueField = "calendarunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            'dsCombo = objMaster.getComboListForTrainingRequestStatus(True, "List")
            'With cboStatus
            '    .DataValueField = "id"
            '    .DataTextField = "name"
            '    .DataSource = dsCombo.Tables("List")
            '    .SelectedValue = "0"
            '    .DataBind()
            'End With

            With cboStatus
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Pending"))
                .Items.Add(Language.getMessage(mstrModuleName, 7, "Approved"))
                .Items.Add(Language.getMessage(mstrModuleName, 8, "Completed"))
                .SelectedIndex = 0
            End With


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCalendar = Nothing
            objMaster = Nothing
            objCommon = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboTrainingCalendar.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            dtpTrainingRequestFrom.SetDate = Nothing
            dtpTrainingRequestTo.SetDate = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboTrainingCalendar.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please Select Training Calendar."), Me)
                cboTrainingCalendar.Focus()
                Exit Function
            End If

            If dtpTrainingRequestFrom.IsNull = True Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please Select Training Request From Date."), Me)
                dtpTrainingRequestFrom.Focus()
                Exit Function
            End If

            If dtpTrainingRequestTo.IsNull = True Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Please Select Training Request To Date."), Me)
                dtpTrainingRequestTo.Focus()
                Exit Function
            End If

            If dtpTrainingRequestFrom.GetDate.Date > dtpTrainingRequestTo.GetDate.Date Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Training Request From Date should not be greater than Training Request To Date."), Me)
                dtpTrainingRequestFrom.Focus()
                Exit Function
            End If


            objMonthlyTrainingRequestsReport.SetDefaultValue()

            objMonthlyTrainingRequestsReport._CalendarUnkid = CInt(cboTrainingCalendar.SelectedValue)
            objMonthlyTrainingRequestsReport._CalendarName = cboTrainingCalendar.SelectedItem.Text
            objMonthlyTrainingRequestsReport._StatusID = CInt(cboStatus.SelectedIndex)
            objMonthlyTrainingRequestsReport._StatusName = cboStatus.SelectedItem.Text
            objMonthlyTrainingRequestsReport._fmtCurrency = Session("fmtCurrency").ToString()

            If dtpTrainingRequestFrom.IsNull = False Then
                objMonthlyTrainingRequestsReport._ApplicationDateFrom = dtpTrainingRequestFrom.GetDate.Date
            End If

            If dtpTrainingRequestTo.IsNull = False Then
                objMonthlyTrainingRequestsReport._ApplicationDateTo = dtpTrainingRequestTo.GetDate.Date
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString()

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try

    End Function

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objMonthlyTrainingRequestsReport.setDefaultOrderBy(0)


            Call SetDateFormat()
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim ExportReportPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            Dim OpenAfterExport As Boolean = False

            objMonthlyTrainingRequestsReport.Generate_DetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                Session("UserAccessModeSetting").ToString(), True, False, ExportReportPath, _
                                                                OpenAfterExport)


            If objMonthlyTrainingRequestsReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objMonthlyTrainingRequestsReport._FileNameAfterExported
                Export.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Language "
    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Language._Object.setCaption(Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Language._Object.setCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Language._Object.setCaption(Me.lblTrainingRequestFrom.ID, Me.lblTrainingRequestFrom.Text)
            Language._Object.setCaption(Me.lblTrainingRequestTo.ID, Me.lblTrainingRequestTo.Text)

            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text)
            Language._Object.setCaption(Me.btnExport.ID, Me.btnExport.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblTrainingCalendar.Text = Language._Object.getCaption(Me.lblTrainingCalendar.ID, Me.lblTrainingCalendar.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblTrainingRequestFrom.Text = Language._Object.getCaption(Me.lblTrainingRequestFrom.ID, Me.lblTrainingRequestFrom.Text)
            Me.lblTrainingRequestTo.Text = Language._Object.getCaption(Me.lblTrainingRequestTo.ID, Me.lblTrainingRequestTo.Text)

            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select Training Calendar.")
            Language.setMessage(mstrModuleName, 2, "Please Select Training Request From Date.")
            Language.setMessage(mstrModuleName, 3, "Please Select Training Request To Date.")
            Language.setMessage(mstrModuleName, 4, "Training Request From Date should not be greater than Training Request To Date.")
            Language.setMessage(mstrModuleName, 5, "Select")
            Language.setMessage(mstrModuleName, 6, "Pending")
            Language.setMessage(mstrModuleName, 7, "Approved")
            Language.setMessage(mstrModuleName, 8, "Completed")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
