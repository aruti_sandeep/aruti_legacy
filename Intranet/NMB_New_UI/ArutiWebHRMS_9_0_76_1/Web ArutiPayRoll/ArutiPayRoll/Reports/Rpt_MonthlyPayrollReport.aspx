﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="Rpt_MonthlyPayrollReport.aspx.vb" Inherits="Reports_Rpt_MonthlyPayrollReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server" >
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Monthly Payroll Report"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboPeriod" runat="server" Width="99%">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboEmployee" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblMembership" runat="server" Text="Membership" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboMembership" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblStaffExitReason" runat="server" Text="Exit Reason" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboExitReason" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="Label1" runat="server" Text="Cat. Reason" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboCatReason" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                                                <asp:LinkButton ID="lnkSaveLeaveSelection" runat="server" Text="Save Selection" CssClass="btn btn-default"></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <asp:Panel ID="pnlLeaves" runat="server" CssClass="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div style="height: 100px;overflow:auto" class="border">
                                                    <asp:CheckBoxList ID="lvLeaveType" runat="server" RepeatDirection="Vertical">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:CheckBox ID="chkSkipAbsentTnA" runat="server" Text="Skip Absent From TnA" />
                                                <asp:CheckBox ID="chkIncludePendingApprovedforms" runat="server" Text="Include Pending/Approve Form(s)" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right m-t-10">
                                                <asp:LinkButton ID="lnkSaveHeadSelection" runat="server" Text="Save Selection" CssClass="btn btn-default"></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <asp:Panel ID="pnlCHeades" runat="server" CssClass="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                                <div style="height: 240px;overflow:auto" class="border">
                                                    <asp:CheckBoxList ID="lvCustomTranHead" runat="server" RepeatDirection="Vertical">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                                <asp:CheckBox ID="chkincludesystemretirement" runat="server" Text="Include Retirement Done By System" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="BtnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                                    <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                    <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <uc9:Export runat="server" ID="Export" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
