﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region


Partial Class Reports_Rpt_LeaveAbsence_DetailedReport
    Inherits Basepage


#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Dim objLvAbsenceDetail As clsLeaveAbsence_Detailed_Report
    'Pinkal (11-Sep-2020) -- End

    Private mstrModuleName As String = "frmLeaveAbsence_Detailed_Report"
    Private mstrViewByIds As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysisFields As String = String.Empty
    Private mstrAnalysisJoin As String = String.Empty
    Private mstrAnalysisOrderBy As String = String.Empty
    Private mstrAnalysisOrderByGName As String = String.Empty
    Private mstrReportGroupName As String = String.Empty

#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try

            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = -1
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                 blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmployee = Nothing


            Dim objStatus As New clsMasterData
            dsList = objStatus.getLeaveStatusList("Status", Session("ApplicableLeaveStatus").ToString(), True, False)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "statusunkid <> 6", "", DataViewRowState.CurrentRows).ToTable()
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "name"
                .DataSource = dtTable
                .DataBind()
            End With
            objStatus = Nothing

            Dim objLeaveType As New clsleavetype_master
            dsList = objLeaveType.getListForCombo("Leave", False, 1, Session("Database_Name").ToString, "", False)
            With chkLeaveType
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objLeaveType = Nothing


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpStartdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmployee.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            chkSelectAll.Checked = True
            chkSelectAll_CheckedChanged(chkSelectAll, New EventArgs())
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysisFields = ""
            mstrAnalysisJoin = ""
            mstrAnalysisOrderBy = ""
            mstrAnalysisOrderByGName = ""
            mstrReportGroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByRef objLvAbsenceDetail As clsLeaveAbsence_Detailed_Report) As Boolean
        'Pinkal (11-Sep-2020) -- End
        Try
            objLvAbsenceDetail.SetDefaultValue()

            objLvAbsenceDetail._FromDate = dtpStartdate.GetDate.Date
            objLvAbsenceDetail._ToDate = dtpToDate.GetDate.Date
            objLvAbsenceDetail._EmployeeID = CInt(cboEmployee.SelectedValue)
            objLvAbsenceDetail._EmployeeName = cboEmployee.SelectedItem.Text
            objLvAbsenceDetail._StatusId = CInt(cboStatus.SelectedValue)
            objLvAbsenceDetail._StatusName = cboStatus.SelectedItem.Text

            Dim mstrLeaveTypeName As String = ""
            objLvAbsenceDetail._LeaveIds = GetLeaveType(mstrLeaveTypeName)
            objLvAbsenceDetail._LeaveName = mstrLeaveTypeName


            objLvAbsenceDetail._ViewByIds = mstrViewByIds
            objLvAbsenceDetail._ViewIndex = mintViewIndex
            objLvAbsenceDetail._ViewByName = mstrViewByName
            objLvAbsenceDetail._Analysis_Fields = mstrAnalysisFields
            objLvAbsenceDetail._Analysis_Join = mstrAnalysisJoin
            objLvAbsenceDetail._Analysis_OrderBy = mstrAnalysisOrderBy
            objLvAbsenceDetail._Analysis_OrderBy_GName = mstrAnalysisOrderByGName
            objLvAbsenceDetail._Report_GroupName = mstrReportGroupName

            objLvAbsenceDetail._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objLvAbsenceDetail._OpenAfterExport = False
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objLvAbsenceDetail._CompanyUnkId = CInt(Session("CompanyUnkId"))

            objLvAbsenceDetail._UserUnkId = CInt(Session("UserId"))
            objLvAbsenceDetail._UserAccessFilter = CStr(Session("AccessLevelFilterString"))

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SetLeaveType(ByVal blnchecked As Boolean)
        Try
            If chkLeaveType.Items.Count > 0 Then
                For i As Integer = 0 To chkLeaveType.Items.Count - 1
                    chkLeaveType.Items(i).Selected = blnchecked
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function GetLeaveType(ByRef mstrLeaveTypeName As String) As String
        Dim mstrIDs As String = ""
        Try
            If chkLeaveType.Items.Count > 0 Then
                For i As Integer = 0 To chkLeaveType.Items.Count - 1
                    If chkLeaveType.Items(i).Selected = True Then
                        mstrIDs &= chkLeaveType.Items(i).Value & ","
                        mstrLeaveTypeName &= chkLeaveType.Items(i).Text & ","
                    End If
                Next
            End If
            If mstrIDs.Trim.Length > 0 Then
                mstrIDs = mstrIDs.Substring(0, mstrIDs.Trim.Length - 1)
                mstrLeaveTypeName = mstrLeaveTypeName.Substring(0, mstrLeaveTypeName.Trim.Length - 1)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mstrIDs
    End Function


#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objLvAbsenceDetail = New clsLeaveAbsence_Detailed_Report
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End



            If Not IsPostBack Then
                SetLanguage()
                Call FillCombo()
                ResetValue()
            Else
                mstrViewByIds = Me.ViewState("mstrViewByIds").ToString()
                mstrViewByName = Me.ViewState("mstrViewByName").ToString()
                mintViewIndex = CInt(Me.ViewState("mintViewIndex"))
                mstrAnalysisFields = Me.ViewState("mstrAnalysisFields").ToString()
                mstrAnalysisJoin = Me.ViewState("mstrAnalysisJoin").ToString()
                mstrAnalysisOrderBy = Me.ViewState("mstrAnalysisOrderBy").ToString()
                mstrReportGroupName = Me.ViewState("mstrReportGroupName").ToString()

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                mstrAnalysisOrderByGName = Me.ViewState("mstrAnalysisOrderByGName").ToString()
                'Pinkal (11-Sep-2020) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrViewByIds") = mstrViewByIds
            Me.ViewState("mstrViewByName") = mstrViewByName
            Me.ViewState("mintViewIndex") = mintViewIndex
            Me.ViewState("mstrAnalysisFields") = mstrAnalysisFields
            Me.ViewState("mstrAnalysisJoin") = mstrAnalysisJoin
            Me.ViewState("mstrAnalysisOrderBy") = mstrAnalysisOrderBy
            Me.ViewState("mstrReportGroupName") = mstrReportGroupName

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Me.ViewState("mstrAnalysisOrderByGName") = mstrAnalysisOrderByGName
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLvAbsenceDetail As New clsLeaveAbsence_Detailed_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (11-Sep-2020) -- End

        Try

            If chkLeaveType.SelectedItem Is Nothing Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please Select atleast one Leave Type."), Me)
                Exit Sub
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If SetFilter() = False Then Exit Sub
            If SetFilter(objLvAbsenceDetail) = False Then Exit Sub
            'Pinkal (11-Sep-2020) -- End


            SetDateFormat()
            objLvAbsenceDetail.Generate_DetailReport(Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  Session("EmployeeAsOnDate").ToString, _
                                                  Session("UserAccessModeSetting").ToString, True)

            If objLvAbsenceDetail._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objLvAbsenceDetail._FileNameAfterExported
                Export.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLvAbsenceDetail = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            SetLeaveType(chkSelectAll.Checked)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrViewByIds = popupAnalysisBy._ReportBy_Ids
            mstrViewByName = popupAnalysisBy._ReportBy_Name
            mintViewIndex = popupAnalysisBy._ViewIndex
            mstrAnalysisFields = popupAnalysisBy._Analysis_Fields
            mstrAnalysisJoin = popupAnalysisBy._Analysis_Join
            mstrAnalysisOrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReportGroupName = popupAnalysisBy._Report_GroupName

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            mstrAnalysisOrderByGName = popupAnalysisBy._Analysis_OrderBy_GName
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy._EffectiveDate = dtpToDate.GetDate.Date
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, objLvAbsenceDetail._ReportName)
            'Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, objLvAbsenceDetail._ReportName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Pinkal (11-Sep-2020) -- End


            Me.LblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFromDate.ID, Me.LblFromDate.Text)
            Me.LblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblToDate.ID, Me.LblToDate.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lnkAnalysisBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)
            Me.lnkAnalysisBy.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.ToolTip)
            'Gajanan [17-Sep-2020] -- End

            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblLeaveType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLeaveType.ID, Me.lblLeaveType.Text)
            Me.chkSelectAll.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkSelectAll.ID, Me.chkSelectAll.Text)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

End Class
