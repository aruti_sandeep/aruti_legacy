﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Disciplin_Report_Rpt_DisciplineAppealReport
    Inherits Basepage

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplineAppealReport"
    Private objAppeal As clsDisciplineAppealReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objCategory As New clsCommon_Master
        Dim objDisciplineAction As New clsAction_Reason
        Dim dsList As New DataSet
        Try
            dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "List", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            dsList = objCategory.getComboList(clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY, True, "list")
            With cboOffenceCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
            Call cboOffenceCategory_SelectedIndexChanged(New Object, New EventArgs())

            dsList = objDisciplineAction.getComboList("Action", True, True)
            With cboDisciplineAction
                .DataValueField = "actionreasonunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList.Dispose()
            objEmployee = Nothing
            objCategory = Nothing
            objDisciplineAction = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboDisciplineType.SelectedValue = 0
            cboOffenceCategory.SelectedValue = 0
            cboDisciplineAction.SelectedValue = 0
            dtpFromDate.SetDate = Nothing
            dtpToDate.SetDate = Nothing
            chkIncludeInactiveEmployee.Checked = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAppeal._EmployeeId = cboEmployee.SelectedValue
            objAppeal._EmployeeName = cboEmployee.SelectedItem.Text

            If dtpFromDate.IsNull = False AndAlso dtpToDate.IsNull = False Then
                objAppeal._ChargeDateFrom = dtpFromDate.GetDate
                objAppeal._ChargeDateTo = dtpToDate.GetDate
            End If
            objAppeal._OffenceCategory = cboOffenceCategory.SelectedItem.Text
            objAppeal._OffenceCategoryId = CInt(cboOffenceCategory.SelectedValue)
            objAppeal._OffenceDescrId = CInt(cboDisciplineType.SelectedValue)
            objAppeal._OffenceDescription = cboDisciplineType.SelectedItem.Text
            objAppeal._ViewByIds = mstrStringIds
            objAppeal._ViewIndex = mintViewIdx
            objAppeal._ViewByName = mstrStringName
            objAppeal._Analysis_Fields = mstrAnalysis_Fields
            objAppeal._Analysis_Join = mstrAnalysis_Join
            objAppeal._Analysis_OrderBy = mstrAnalysis_OrderBy
            objAppeal._Report_GroupName = mstrReport_GroupName
            objAppeal._ActionId = cboDisciplineAction.SelectedValue
            objAppeal._ActionName = cboDisciplineAction.SelectedItem.Text

            'S.SANDEEP |10-JUN-2020| -- START
            'ISSUE/ENHANCEMENT : DISICIPLINE REPORT {ALLOCATION DISPLAY BASED ON CHARGE DATE}
            objAppeal._ShowAllocationBasedOnChargeDate = chkDisplayAllocationBasedOnChargeDate.Checked
            objAppeal._ShowAllocationBasedOnChargeDateString = chkDisplayAllocationBasedOnChargeDate.Text
            'S.SANDEEP |10-JUN-2020| -- END
            objAppeal._IncludeInactiveEmployee = chkIncludeInactiveEmployee.Checked

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = objAppeal._ReportName
            Me.lblPageHeader.Text = objAppeal._ReportName
            Me.lblDisciplineAction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblDisciplineAction.ID, Me.lblDisciplineAction.Text)
            Me.lblOffenceCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblOffenceCategory.ID, Me.lblOffenceCategory.Text)
            Me.lblDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblDateFrom.ID, Me.lblDateFrom.Text)
            Me.lblTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblTo.ID, Me.lblTo.Text)
            Me.lnkSetAnalysis.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)
            Me.lblDisciplineType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblDisciplineType.ID, Me.lblDisciplineType.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Forms "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objAppeal = New clsDisciplineAppealReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                SetLanguage()
                Call FillCombo()
                Call ResetValue()
            Else
                mstrStringIds = Me.ViewState("mstrStringIds")
                mstrStringName = Me.ViewState("mstrStringName")
                mintViewIdx = Me.ViewState("mintViewIdx")
                mstrAnalysis_Fields = Me.ViewState("mstrAnalysis_Fields")
                mstrAnalysis_Join = Me.ViewState("mstrAnalysis_Join")
                mstrAnalysis_OrderBy = Me.ViewState("mstrAnalysis_OrderBy")
                mstrReport_GroupName = Me.ViewState("mstrReport_GroupName")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()
            objAppeal._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objAppeal._UserUnkId = CInt(Session("UserId"))
            objAppeal.generateReportNew(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CStr(Session("ExportReportPath")), _
                                                CBool(Session("OpenAfterExport")), 0, enPrintAction.None, _
                                                enExportAction.None, _
                                                CInt(Session("Base_CurrencyId")))
            Session("objRpt") = objAppeal._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboOffenceCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOffenceCategory.SelectedIndexChanged
        Dim objOffence As New clsDisciplineType
        Dim dsList As New DataSet
        Try
            dsList = objOffence.getComboList("List", True, CInt(cboOffenceCategory.SelectedValue))
            With cboDisciplineType
                .DataValueField = "disciplinetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objOffence = Nothing : dsList.Dispose()
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy._Hr_EmployeeTable_Alias = "EM"
            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
End Class
