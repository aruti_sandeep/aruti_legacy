﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_DisciplinaryChargesDetailedReport.aspx.vb"
    Inherits="Reports_Disciplin_Report_Rpt_DisciplinaryChargesDetailedReport" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="Date" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc7" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                <uc7:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <uc9:Export runat="server" ID="Export" />
                <div class="block-header">
                    <h2>
                    </h2>
                </div>
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Disciplinary Charges Detailed Report"
                                        CssClass="form-label"></asp:Label>
                                    <ul class="header-dropdown m-r--5">
                                        <li class="dropdown">
                                            <asp:LinkButton ID="lnkSetAnalysis" runat="server" Text="Analysis By" ToolTip="Analysis By">
                                                    <i class="fas fa-filter"></i> 
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" ToolTip="Advance Filter">
                                                    <i class="fas fa-sliders-h"></i>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDateFrom" runat="server" Text="Charge Date" CssClass="form-label"></asp:Label>
                                        <uc1:Date ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc1:Date ID="dtpToDate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOffenceCategory" runat="server" Text="Offence Category" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboOffenceCategory" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDisciplineType" runat="server" Text="Offence Decription" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboDisciplineType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblResponseDateFrom" runat="server" Text="Response Date" CssClass="form-label"></asp:Label>
                                        <uc1:Date ID="dtpResponseFromDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblResponseDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc1:Date ID="dtpResponseToDate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblResponseType" runat="server" Text="Response Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboResponsetype" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox runat="server" ID="chkDisplayAllocationBasedOnChargeDate" Checked="true"
                                            Text="Display Employee Allocation Based on Charge Date" />
                                    </div>
                                </div>
                                 <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox runat="server" ID="chkIncludeInactiveEmployee" Checked="false" Text="Include Inactive Employee" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-primary" />
                                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-default" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
