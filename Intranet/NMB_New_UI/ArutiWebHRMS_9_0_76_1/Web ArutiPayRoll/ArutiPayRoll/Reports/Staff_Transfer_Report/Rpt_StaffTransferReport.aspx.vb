﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Staff_Transfer_Report_Rpt_StaffTransferReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objTransfer As clsStaffTransfer_Report
    Private ReadOnly mstrModuleName As String = "frmStaffTransferReport"
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Employee")
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            objEmp = Nothing

            dsList = Nothing
            Dim objClassGrp As New clsClassGroup
            dsList = objClassGrp.getComboList("List", True)
            With cboFromClassGrp
                .DataValueField = "classgroupunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            With cboDestinationClassGrp
                .DataValueField = "classgroupunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            objClassGrp = Nothing

            dsList = Nothing
            Dim objJob As New clsJobs
            dsList = objJob.getComboList("List", True)
            With cboJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            objJob = Nothing

            dsList = Nothing
            Dim objCommon As New clsCommon_Master
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.STAFF_TRANSFER, True, "List")
            With cboReason
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            objEmp = Nothing

            dsList = Nothing
            Dim objStaffTransfer As New clstransfer_request_tran
            dsList = objStaffTransfer.GetTransferRequest_Status("Status", True, False)
            With cboStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsList.Tables("Status")
                .DataBind()
                .SelectedValue = "0"
            End With
            objStaffTransfer = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = CStr(0)
            dtpRequestFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpRequestToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboFromClassGrp.SelectedValue = CStr(0)
            cboDestinationClassGrp.SelectedValue = CStr(0)
            cboJob.SelectedValue = CStr(0)
            cboReason.SelectedValue = CStr(0)
            cboStatus.SelectedValue = CStr(0)
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region "Page Event(S)"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Hemant (24 May 2024) -- Start
            'ISSUE(NMB): Getting Error "Object reference not set to an instance of an object" while Generating Staff Transfer Report
            objTransfer = New clsStaffTransfer_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            'Hemant (24 May 2024) -- End

            If Not IsPostBack Then
                SetControlCaptions()
                SetLanguage()
                SetMessages()
                Call FillCombo()
                Call ResetValue()
            Else
                mstrStringIds = CStr(Me.ViewState("mstrStringIds"))
                mstrStringName = CStr(Me.ViewState("mstrStringName"))
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysis_Join = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysis_OrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReport_GroupName = CStr(Me.ViewState("mstrReport_GroupName "))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName ") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Event(S)"

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If Session("ExportReportPath") Is Nothing OrElse Session("ExportReportPath").ToString().Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), Me)
                Exit Sub
            End If

            objTransfer.SetDefaultValue()

            objTransfer._EmpId = CInt(cboEmployee.SelectedValue)
            objTransfer._EmpName = cboEmployee.SelectedItem.Text
            objTransfer._RequestFromDate = dtpRequestFromDate.GetDate.Date
            objTransfer._RequestToDate = dtpRequestToDate.GetDate.Date
            objTransfer._FromClassGroupId = CInt(cboFromClassGrp.SelectedValue)
            If CInt(cboFromClassGrp.SelectedValue) > 0 Then
                objTransfer._FromClassGroup = cboFromClassGrp.SelectedItem.Text
            Else
                objTransfer._FromClassGroup = ""
            End If

            objTransfer._ToClassGroupId = CInt(cboDestinationClassGrp.SelectedValue)
            If CInt(cboDestinationClassGrp.SelectedValue) > 0 Then
                objTransfer._ToClassGroup = cboDestinationClassGrp.SelectedItem.Text
            Else
                objTransfer._ToClassGroup = ""
            End If

            objTransfer._JobId = CInt(cboJob.SelectedValue)
            If CInt(cboJob.SelectedValue) > 0 Then
                objTransfer._Job = cboJob.SelectedItem.Text
            Else
                objTransfer._Job = ""
            End If


            objTransfer._ReasonId = CInt(cboReason.SelectedValue)
            If CInt(cboReason.SelectedValue) > 0 Then
                objTransfer._Reason = cboReason.SelectedItem.Text
            Else
                objTransfer._Reason = ""
            End If

            objTransfer._StatusId = CInt(cboStatus.SelectedValue)
            If CInt(cboStatus.SelectedValue) > 0 Then
                objTransfer._Status = cboStatus.SelectedItem.Text
            Else
                objTransfer._Status = ""
            End If

            objTransfer._ViewByIds = mstrStringIds
            objTransfer._ViewIndex = mintViewIdx
            objTransfer._ViewByName = mstrStringName
            objTransfer._Analysis_Fields = mstrAnalysis_Fields
            objTransfer._Analysis_Join = mstrAnalysis_Join
            objTransfer._Report_GroupName = mstrReport_GroupName
            objTransfer._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            SetDateFormat()

            objTransfer.Generate_StaffTransferReport(CStr(Session("Database_Name")), _
                                                                         CInt(Session("UserId")), _
                                                                         CInt(Session("Fin_year")), _
                                                                         CInt(Session("CompanyUnkId")), _
                                                                         dtpRequestFromDate.GetDate.Date, _
                                                                         dtpRequestToDate.GetDate.Date, _
                                                                         CStr(Session("UserAccessModeSetting")), True, _
                                                                         CStr(Session("ExportReportPath")), _
                                                                         CBool(Session("OpenAfterExport")))

            If objTransfer._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objTransfer._FileNameAfterExported
                Export.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Control Event(S)"

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.ToolTip)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmpName.ID, Me.lblEmpName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblRequestFromDate.ID, Me.lblRequestFromDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblRequestToDate.ID, Me.lblRequestToDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFromClassGrp.ID, Me.LblFromClassGrp.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblDestinationClassGrp.ID, Me.LblDestinationClassGrp.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblJob.ID, Me.LblJob.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblReason.ID, Me.LblReason.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblStatus.ID, Me.LblStatus.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnExport.ID, Me.btnExport.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Public Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objTransfer._ReportName)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objTransfer._ReportName)
            Me.lnkAnalysisBy.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.ToolTip)
            Me.lblEmpName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.lblRequestFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRequestFromDate.ID, Me.lblRequestFromDate.Text)
            Me.lblRequestToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRequestToDate.ID, Me.lblRequestToDate.Text)
            Me.LblFromClassGrp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFromClassGrp.ID, Me.LblFromClassGrp.Text)
            Me.LblDestinationClassGrp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblDestinationClassGrp.ID, Me.LblDestinationClassGrp.Text)
            Me.LblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblJob.ID, Me.LblJob.Text)
            Me.LblReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblReason.ID, Me.LblReason.Text)
            Me.LblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblStatus.ID, Me.LblStatus.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, Location is mandatory information. Please select location to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Selection Saved Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Select Allocation for Report")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

End Class
