﻿Option Strict On
#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports
Imports System

#End Region


Partial Class Reports_Staff_Transfer_Report_Rpt_TransferRequestReport
    Inherits Basepage


#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmStaffTransferRequestReport"
    Private objTransfer As clsStaffTransferRequest_Report

#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                 blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmployee = Nothing

            dsList = Nothing
            Dim objClassGrp As New clsClassGroup
            dsList = objClassGrp.getComboList("List", True)
            With cboFromClassGroup
                .DataValueField = "classgroupunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            With cboDestinationClassGroup
                .DataValueField = "classgroupunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            objClassGrp = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), Me)
                cboEmployee.Focus()
                Return False

            ElseIf CInt(cboRequstDate.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Request Date is compulsory information.Please Select Request Date."), Me)
                cboRequstDate.Focus()
                Return False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return True
    End Function

    Public Sub ResetValue()
        Try
            cboEmployee.SelectedIndex = 0
            cboRequstDate.SelectedIndex = 0
            cboFromClassGroup.SelectedIndex = 0
            cboDestinationClassGroup.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter(ByRef objTransferRequest As clsStaffTransferRequest_Report) As Boolean
        Try
            objTransferRequest.SetDefaultValue()
            objTransferRequest._EmployeeId = CInt(cboEmployee.SelectedValue)
            objTransferRequest._EmployeeName = cboEmployee.SelectedItem.Text

            If CInt(cboRequstDate.SelectedValue) > 0 Then
                objTransferRequest._TransferRequestId = CInt(cboRequstDate.SelectedValue)
                objTransferRequest._RequestDate = CDate(cboRequstDate.SelectedItem.Text)
            Else
                objTransferRequest._TransferRequestId = 0
                objTransferRequest._RequestDate = Nothing
            End If

            objTransferRequest._FromClassGroupId = CInt(cboFromClassGroup.SelectedValue)
            If CInt(cboFromClassGroup.SelectedValue) > 0 Then
                objTransferRequest._FromClassGroup = cboFromClassGroup.SelectedItem.Text
            End If

            objTransferRequest._DestinationClassGroupId = CInt(cboDestinationClassGroup.SelectedValue)
            If CInt(cboDestinationClassGroup.SelectedValue) > 0 Then
                objTransferRequest._DestincationClassGroup = cboDestinationClassGroup.SelectedItem.Text
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objTransfer = New clsStaffTransferRequest_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                SetControlCaptions()
                SetLanguage()
                SetMessages()
                Call FillCombo()
                cboEmployee_SelectedIndexChanged(New Object, New EventArgs())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Dim objTransferRequest As New clsStaffTransferRequest_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            If Validation() Then

                If SetFilter(objTransferRequest) = False Then Exit Sub

                objTransferRequest._CompanyUnkId = CInt(Session("CompanyUnkId"))
                objTransferRequest._UserUnkId = CInt(Session("UserId"))
                SetDateFormat()

                objTransferRequest.generateReportNew(CStr(Session("Database_Name")), _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               CStr(Session("UserAccessModeSetting")), True, _
                                               CStr(Session("ExportReportPath")), _
                                               CBool(Session("OpenAfterExport")), _
                                               0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))

                Session("objRpt") = objTransferRequest._Rpt

                If Session("objRpt") IsNot Nothing Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTransferRequest = Nothing
        End Try
    End Sub

#End Region

#Region "Dropdown Events"

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Dim objForm As New clstransfer_request_tran
            Dim dsList As DataSet = Nothing
            SetDateFormat()
            dsList = objForm.getListForRequestDate(CInt(cboEmployee.SelectedValue), 0, True)
            cboRequstDate.DataValueField = "transferrequestunkid"
            cboRequstDate.DataTextField = "name"
            cboRequstDate.DataSource = dsList.Tables(0)
            cboRequstDate.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region



    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblEmployee.ID, Me.LblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRequestDate.ID, Me.LblRequestDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFromClassGroup.ID, Me.LblFromClassGroup.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblDestinationClassGroup.ID, Me.LblDestinationClassGroup.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReport.ID, Me.btnReport.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Public Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objTransfer._ReportName)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objTransfer._ReportName)
            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.LblRequestDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRequestDate.ID, Me.LblRequestDate.Text)
            Me.LblFromClassGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFromClassGroup.ID, Me.LblFromClassGroup.Text)
            Me.LblDestinationClassGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblDestinationClassGroup.ID, Me.LblDestinationClassGroup.Text)

            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Employee is compulsory information.Please Select Employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Request Date is compulsory information.Please Select Request Date.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

End Class
