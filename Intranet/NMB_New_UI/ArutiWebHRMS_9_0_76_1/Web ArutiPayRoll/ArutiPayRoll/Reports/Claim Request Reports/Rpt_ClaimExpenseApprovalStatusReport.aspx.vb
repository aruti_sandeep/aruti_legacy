﻿Option Strict On
Imports Aruti.Data
Imports System.Data
Imports ArutiReports
Partial Class Reports_Claim_Request_Reports_Rpt_ClaimExpenseApprovalStatusReport
    Inherits Basepage

#Region " Private Variable(s) "

    Private DisplayMessage As New CommonCodes
    Private objClaimExpenseApprovalStatusReport As clsClaimExpenseApprovalStatusReport
    Private ReadOnly mstrModuleName As String = "frmClaimExpenseApprovalStatusReport"
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""

#End Region

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objClaimExpenseApprovalStatusReport = New clsClaimExpenseApprovalStatusReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call SetLanguage()

                Call FillCombo()
            Else
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
                mstrAnalysis_OrderBy_GroupName = ViewState("mstrAnalysis_OrderBy_GroupName").ToString
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrStringIds", mstrStringIds)
            Me.ViewState.Add("mstrStringName", mstrStringName)
            Me.ViewState.Add("mintViewIdx", mintViewIdx)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
            Me.ViewState.Add("mstrAnalysis_OrderBy_GroupName", mstrAnalysis_OrderBy_GroupName)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objMasterData As New clsMasterData
        Dim dsCombos As New DataSet

        Try

            dsCombos = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            Session("UserAccessModeSetting").ToString, True, _
                                           True, "Emp", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'dsCombos = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            Dim objExpenseCategory As New clsexpense_category_master
            dsCombos = objExpenseCategory.GetExpenseCategory(Session("Database_Name").ToString(), True, True, True, "List", True, True, True, True, True)
            objExpenseCategory = Nothing
            'Pinkal (24-Jun-2024) -- End


            With cboExpenseCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With
            cboExpenseCategory_SelectedIndexChanged(Nothing, Nothing)

            dsCombos = objMasterData.getLeaveStatusList("List", "")
            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombos.Tables(0), "statusunkid IN (0,1,2,3,6)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "name"
                .DataSource = dtab
                .SelectedValue = "0"
                .DataBind()
            End With


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmp = Nothing
            objMasterData = Nothing
            dsCombos = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEmployee.SelectedIndex = 0
            cboExpenseCategory.SelectedIndex = 0
            cboExpense.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboStatus.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Status is mandatory information. Please select Status to continue."), Me)
                cboStatus.Focus()
                Exit Function
            End If

            objClaimExpenseApprovalStatusReport.SetDefaultValue()

            objClaimExpenseApprovalStatusReport._EmployeeId = CInt(cboEmployee.SelectedValue)
            objClaimExpenseApprovalStatusReport._EmployeeName = cboEmployee.SelectedItem.Text
            objClaimExpenseApprovalStatusReport._ExpCateId = CInt(cboExpenseCategory.SelectedValue)
            objClaimExpenseApprovalStatusReport._ExpCateName = cboExpenseCategory.SelectedItem.Text.ToString()
            objClaimExpenseApprovalStatusReport._ExpenseID = CInt(cboExpense.SelectedValue)
            objClaimExpenseApprovalStatusReport._Expense = cboExpense.SelectedItem.Text
            objClaimExpenseApprovalStatusReport._StatusId = CInt(cboStatus.SelectedValue)
            objClaimExpenseApprovalStatusReport._StatusName = cboStatus.SelectedItem.Text.ToString
            objClaimExpenseApprovalStatusReport._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            objClaimExpenseApprovalStatusReport._ViewByIds = mstrStringIds
            objClaimExpenseApprovalStatusReport._ViewIndex = mintViewIdx
            objClaimExpenseApprovalStatusReport._ViewByName = mstrStringName
            objClaimExpenseApprovalStatusReport._Analysis_Fields = mstrAnalysis_Fields
            objClaimExpenseApprovalStatusReport._Analysis_Join = mstrAnalysis_Join
            objClaimExpenseApprovalStatusReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objClaimExpenseApprovalStatusReport._Report_GroupName = mstrReport_GroupName
            objClaimExpenseApprovalStatusReport._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            objClaimExpenseApprovalStatusReport._PaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))

            GUI.fmtCurrency = Session("fmtCurrency").ToString

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try

    End Function

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objClaimExpenseApprovalStatusReport.setDefaultOrderBy(0)


            Call SetDateFormat()
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim ExportReportPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            Dim OpenAfterExport As Boolean = False


            objClaimExpenseApprovalStatusReport.Generate_DetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                       CInt(Session("CompanyUnkId")), _
                                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                       Session("UserAccessModeSetting").ToString(), True, False, ExportReportPath, _
                                                                       OpenAfterExport)




            If objClaimExpenseApprovalStatusReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objClaimExpenseApprovalStatusReport._FileNameAfterExported
                Export.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Try
            Dim dsCombo As New DataSet
            Dim objExpense As New clsExpense_Master
            dsCombo = objExpense.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List")
            With cboExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = "0"
                .DataBind()
            End With
            objExpense = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboExpenseCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Controls Events "

    Protected Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
            mstrAnalysis_OrderBy_GroupName = popupAnalysisBy._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Language "
    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblExpenseCategory.ID, Me.lblExpenseCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblExpense.ID, Me.lblExpense.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnExport.ID, Me.btnExport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lnkSetAnalysis.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.ToolTip)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblExpenseCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExpenseCategory.ID, Me.lblExpenseCategory.Text)
            Me.lblExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExpense.ID, Me.lblExpense.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)


            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text.Trim.Replace("&", ""))
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text.Trim.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Trim.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Status is mandatory information. Please select Status to continue.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
