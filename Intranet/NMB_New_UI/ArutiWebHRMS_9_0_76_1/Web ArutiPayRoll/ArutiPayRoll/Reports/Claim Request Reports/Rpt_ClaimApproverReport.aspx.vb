﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Claim_Request_Reports_Rpt_ClaimApproverReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objClaimApprover As clsClaimApproverReport
    Private ReadOnly mstrModuleName As String = "frmClaimApproverReport"
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            Dim dtTable As DataTable = Nothing

            'Pinkal (24-Jun-2024) -- Start
            'NMB Enhancement : P2P & Expense Category Enhancements.
            'dsList = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            Dim objExpenseCategory As New clsexpense_category_master
            dsList = objExpenseCategory.GetExpenseCategory(Session("Database_Name").ToString(), True, True, True, "List", True, True, True, True, True)
            objExpenseCategory = Nothing
            'Pinkal (24-Jun-2024) -- End



            If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                dtTable = New DataView(dsList.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboExpenseCategory
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable.Copy
                .DataBind()
                .SelectedValue = "0"
            End With


            dsList = Nothing
            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If
            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            Session("UserAccessModeSetting").ToString, True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                            blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmp = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboExpenseCategory.SelectedValue = CStr(0)
            cboEmployee.SelectedValue = CStr(0)
            mstrAdvanceFilter = ""
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mintViewIndex = -1
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Expense Category is compulsory information.Please select Expense Category."), Me)
                cboExpenseCategory.Focus()
                Return False
            End If

            objClaimApprover.SetDefaultValue()
            objClaimApprover._ExpenseCategoryId = CInt(cboExpenseCategory.SelectedValue)
            objClaimApprover._ExpenseCategory = cboExpenseCategory.SelectedItem.Text
            objClaimApprover._EmployeeID = CInt(cboEmployee.SelectedValue)
            objClaimApprover._EmployeeName = cboEmployee.SelectedItem.Text
            objClaimApprover._ViewByIds = mstrViewByIds
            objClaimApprover._ViewIndex = mintViewIndex
            objClaimApprover._ViewByName = mstrViewByName
            objClaimApprover._Analysis_Fields = mstrAnalysis_Fields
            objClaimApprover._Analysis_Join = mstrAnalysis_Join
            objClaimApprover._Analysis_OrderBy = mstrAnalysis_OrderBy
            objClaimApprover._Report_GroupName = mstrReport_GroupName
            objClaimApprover._Advance_Filter = mstrAdvanceFilter
            objClaimApprover._UserName = Session("UserName").ToString()
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objClaimApprover = New clsClaimApproverReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                SetLanguage()
                SetMessages()
                Call FillCombo()
                ResetValue()
            Else
                mstrViewByIds = Me.ViewState("mstrStringIds").ToString()
                mstrViewByName = Me.ViewState("mstrStringName").ToString()
                mintViewIndex = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = Me.ViewState("mstrAnalysis_Fields").ToString()
                mstrAnalysis_Join = Me.ViewState("mstrAnalysis_Join").ToString()
                mstrAnalysis_OrderBy = Me.ViewState("mstrAnalysis_OrderBy").ToString()
                mstrAnalysis_OrderBy_GName = Me.ViewState("mstrAnalysis_OrderBy_GName").ToString()
                mstrReport_GroupName = Me.ViewState("mstrReport_GroupName").ToString()
                mstrAdvanceFilter = Me.ViewState("mstrAdvanceFilter").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrViewByIds
            Me.ViewState("mstrStringName") = mstrViewByName
            Me.ViewState("mintViewIdx") = mintViewIndex
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrAnalysis_OrderBy_GName") = mstrAnalysis_OrderBy_GName
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
            Me.ViewState("mstrAdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objClaimApprover.generateReportNew(Session("Database_Name").ToString(), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                    CStr(Session("ExportReportPath")), _
                                                    CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)

            Session("objRpt") = objClaimApprover._Rpt

            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrViewByIds = popAnalysisby._ReportBy_Ids
            mstrViewByName = popAnalysisby._ReportBy_Name
            mintViewIndex = popAnalysisby._ViewIndex
            mstrAnalysis_Fields = popAnalysisby._Analysis_Fields
            mstrAnalysis_Join = popAnalysisby._Analysis_Join
            mstrAnalysis_OrderBy = popAnalysisby._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = popAnalysisby._Analysis_OrderBy_GName
            mstrReport_GroupName = popAnalysisby._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Language & UI Settings "

    Public Sub SetLanguage()
        Try

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objClaimApprover._ReportName)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)

            Me.LblExpenseCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblExpenseCategory.ID, Me.LblExpenseCategory.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Expense Category is compulsory information.Please select Expense Category.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

#End Region

End Class
