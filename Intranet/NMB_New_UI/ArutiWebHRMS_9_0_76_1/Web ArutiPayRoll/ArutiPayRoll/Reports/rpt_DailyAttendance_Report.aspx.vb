﻿Option Strict On 'Shani(11-Feb-2016)

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_rpt_DailyAttendance_Report
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objattendance As clsDailyAttendance_Report
    Private ReadOnly mstrModuleName As String = "frmDailyAttendance_Report"
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = True Then
            '    dsList = objEmp.GetEmployeeList("Employee", True,  False)
            'Else
            '    dsList = objEmp.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            'End If
            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
            'Shani(20-Nov-2015) -- End
            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Employee")
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            objEmp = Nothing

            'Pinkal (22-Dec-2022) -- Start
            ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns
            If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                Dim objMData As New clsMasterData
                dsList = objMData.GetEAllocation_Notification("List")
                Dim drow As DataRow = dsList.Tables("List").NewRow
                drow.Item("Id") = 0
                drow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Select Allocation for Report")
                dsList.Tables("List").Rows.InsertAt(drow, 0)

                Dim iRowIdx As Integer = -1
                iRowIdx = dsList.Tables("List").Rows.IndexOf(dsList.Tables("List").Select("Id = " & enAllocation.DEPARTMENT)(0))
                dsList.Tables("List").Rows.RemoveAt(iRowIdx)

                With cboReportColumn
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                    .SelectedValue = "0"
                End With
                pnlColumns.Visible = True
            Else
                pnlColumns.Visible = False
            End If
            'Pinkal (22-Dec-2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmployee.SelectedValue = CStr(0)
            chkInactiveemp.Checked = False
            chkIgnoreEmptyColumns.Checked = False
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003634 {PAPAYE}.
            Call dtpFromDate_TextChanged(New Object(), New EventArgs())
            'S.SANDEEP |29-MAR-2019| -- END

            'Pinkal (22-Dec-2022) -- Start
            '(A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
            If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                GetValue()
            End If
            'Pinkal (22-Dec-2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (22-Dec-2022) -- Start
    '(A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Daily_Attendance_Checking_Report)
            If dsList.Tables("List").Rows.Count > 0 Then
                cboReportColumn.SelectedValue = dsList.Tables(0).Rows(0)("transactionheadid").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Pinkal (22-Dec-2022) -- End

#End Region

#Region "Page Event(S)"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            objattendance = New clsDailyAttendance_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                'Pinkal (22-Dec-2022) -- Start
                ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
                SetLanguage()
                SetMessages()
                'Pinkal (22-Dec-2022) -- End
                Call FillCombo()
                Call ResetValue()
            Else
                mstrStringIds = CStr(Me.ViewState("mstrStringIds"))
                mstrStringName = CStr(Me.ViewState("mstrStringName"))
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysis_Join = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysis_OrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReport_GroupName = CStr(Me.ViewState("mstrReport_GroupName "))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName ") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Event(S)"

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            'Pinkal (22-Dec-2022) -- Start
            '(A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.

            If Session("ExportReportPath") Is Nothing OrElse Session("ExportReportPath").ToString().Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), Me)
                Exit Sub
            End If

            If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                If cboReportColumn.SelectedIndex <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, Location is mandatory information. Please select location to continue."), Me)
                    Exit Sub
                End If
            End If
            'Pinkal (22-Dec-2022) -- End


            objattendance.SetDefaultValue()

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objattendance._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            'objattendance._OpenAfterExport = False
            'Shani(20-Nov-2015) -- End
            objattendance._EmployeeId = CInt(cboEmployee.SelectedValue)
            objattendance._EmployeeName = cboEmployee.Text
            objattendance._FromDate = dtpFromDate.GetDate.Date
            objattendance._ToDate = dtpToDate.GetDate.Date
            objattendance._IncludeInactive = chkInactiveemp.Checked
            'objattendance._Advance_Filter = mstrAdvanceFilter
            objattendance._ViewByIds = mstrStringIds
            objattendance._ViewIndex = mintViewIdx
            objattendance._ViewByName = mstrStringName
            objattendance._Analysis_Fields = mstrAnalysis_Fields
            objattendance._Analysis_Join = mstrAnalysis_Join
            objattendance._Report_GroupName = mstrReport_GroupName
            objattendance._IgnoreEmptyColumns = chkIgnoreEmptyColumns.Checked
            objattendance._UserAccessFilter = CStr(Session("AccessLevelFilterString"))

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            objattendance._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            'Shani(11-Feb-2016) -- End


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003634 {PAPAYE}.
            objattendance._LegendCode = cboLegends.SelectedValue
            objattendance._LegendName = cboLegends.SelectedItem.Text
            'S.SANDEEP |29-MAR-2019| -- END

            'Pinkal (22-Dec-2022) -- Start
            ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
            If Session("CompanyGroupName").ToString().ToUpper = "TANZANIA REVENUE AUTHORITY" Then
                objattendance._LocationId = CInt(cboReportColumn.SelectedValue)
                objattendance._Location = cboReportColumn.SelectedItem.Text
            Else
                objattendance._LocationId = 0
                objattendance._Location = ""
            End If
            'Pinkal (22-Dec-2022) -- End


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objattendance.Generate_DailyAttendanceReport()
            objattendance.Generate_DailyAttendanceReport(CStr(Session("Database_Name")), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         dtpFromDate.GetDate.Date, _
                                                         dtpToDate.GetDate.Date, _
                                                         CStr(Session("UserAccessModeSetting")), True, _
                                                         CStr(Session("ExportReportPath")), _
                                                         CBool(Session("OpenAfterExport")))
            'Shani(20-Nov-2015) -- End


            If objattendance._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objattendance._FileNameAfterExported
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)

                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Control Event(S)"

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (22-Dec-2022) -- Start
    ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
    Protected Sub lnkSaveChanges_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSaveChanges.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            objUserDefRMode = New clsUserDef_ReportMode()
            objUserDefRMode._Reportunkid = enArutiReport.Daily_Attendance_Checking_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 1
            objUserDefRMode._EarningTranHeadIds = cboReportColumn.SelectedValue.ToString()
            Dim intUnkid As Integer = objUserDefRMode.isExist(enArutiReport.Daily_Attendance_Checking_Report, 0, 0, objUserDefRMode._Headtypeid)

            objUserDefRMode._Reportmodeunkid = intUnkid
            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If
            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Setting saved successfully."), Me)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Pinkal (22-Dec-2022) -- End

#End Region

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003634 {PAPAYE}.
#Region " Date Event(s) "

    Protected Sub dtpFromDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFromDate.TextChanged, dtpToDate.TextChanged
        Try
            Dim dttable As DataTable = Nothing
            objattendance._FromDate = dtpFromDate.GetDate.Date
            objattendance._ToDate = dtpToDate.GetDate.Date
            dttable = objattendance.GetLegends()
            Dim DrRow As DataRow = dttable.NewRow
            DrRow("Code") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Select")
            DrRow("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Select")
            DrRow("uDisplay") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Select")
            DrRow("Color") = ""
            DrRow("uCode") = "0"
            dttable.Rows.InsertAt(DrRow, 0)
            With cboLegends
                .DataValueField = "uCode"
                .DataTextField = "uDisplay"
                .DataSource = dttable
                .SelectedValue = "0"
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |29-MAR-2019| -- END
    
    'Pinkal (22-Dec-2022) -- Start
    ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.

    Public Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objattendance._ReportName)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, objattendance._ReportName)
            Me.lnkAnalysisBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)
            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToDate.ID, Me.lblToDate.Text)
            Me.lblEmpName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.lblLegends.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLegends.ID, Me.lblLegends.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.chkIgnoreEmptyColumns.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkIgnoreEmptyColumns.ID, Me.chkIgnoreEmptyColumns.Text)
            Me.gbShowColumns.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbShowColumns.ID, Me.gbShowColumns.Text)
            Me.lblLocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLocation.ID, Me.lblLocation.Text)
            Me.lnkSaveChanges.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkSaveChanges.ID, Me.lnkSaveChanges.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, Location is mandatory information. Please select location to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Selection Saved Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Select Allocation for Report")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    'Pinkal (22-Dec-2022) -- End
    
End Class
