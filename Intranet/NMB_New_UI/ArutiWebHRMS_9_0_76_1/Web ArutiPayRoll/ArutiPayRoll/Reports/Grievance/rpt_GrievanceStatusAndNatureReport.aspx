﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="rpt_GrievanceStatusAndNatureReport.aspx.vb"
    Inherits="Reports_Grievance_rpt_GrievanceStatusAndNatureReport" Title="Grievance Summary Report" %>

<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
            <div class="row clearfix d--f jc--c ai--c">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="card">
                        <div class="header">
                            <h2>
                            <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Detail Report"></asp:Label>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <asp:LinkButton ID="lnkSetAnalysis" runat="server" ToolTip="Analysis By">
                                      <i class="fas fa-filter"></i>
                                    </asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblReportType" runat="server" Text="Report Type" Width="100%"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="objlbltitle" runat="server" Text="Status" Width="100%"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboType" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                    </div>
                                </div>
                        <div class="footer">
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-primary" />
                            <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-default" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc7:AnalysisBy ID="popupAnalysisBy" runat="server" />
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
</asp:Content>
