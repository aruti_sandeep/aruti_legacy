﻿Imports Aruti.Data
Imports System.Data
Imports ArutiReports

Partial Class Reports_Grievance_RptGrievanceDetailReport
    Inherits Basepage

#Region " Private Variable(s) "
    Private DisplayMessage As New CommonCodes

    'Gajanan [5-July-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance NMB Report
    'Private objGrievanceDetailReport As clsGrievanceDetailReport
    Private objGrievanceReport As clsGrievanceReport
    'Gajanan [5-July-2019] -- End


#End Region

#Region " Private Function(s) & Method(s) "
    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objcommonmaster As New clsCommon_Master
        Dim objclsMasterData As New clsMasterData

        Try
            dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

            With drpfromemployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
                .SelectedValue = 0
            End With

            'Hemant (27 Oct 2023) -- Start
            'ISSUE/ENHANCEMENT(LHRC): A1X-1455 - Grievance detail report enhancement for company level grievances 
            If CInt(Session("GrievanceApprovalSetting")) = enGrievanceApproval.ApproverEmpMapping Then
                Dim drEmployee() As DataRow = dsList.Tables(0).Select("employeeunkid = -999")
                If drEmployee.Length <= 0 Then
                    Dim drow As DataRow = dsList.Tables(0).NewRow()
                    drow.Item("employeeunkid") = -999
                    drow.Item("employeecode") = "000000"
                    drow.Item("employeename") = "Company"
                    drow.Item("EmpCodeName") = "000000 - Company "
                    dsList.Tables(0).Rows.Add(drow)
                End If
            End If
            'Hemant (27 Oct 2023) -- End

            With drpaginstemployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables("Employee").Copy()
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = Nothing
            dsList = objclsMasterData.getComboListGrievanceResponse()
            With drpgrestatus
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            drpfromemployee.SelectedValue = 0
            drpaginstemployee.SelectedValue = 0
            drpgrestatus.SelectedValue = 0
            dtfromdate.SetDate = Nothing
            dttodate.SetDate = Nothing
            txtrefno.Text = ""
            chkshowcommitee.Checked = False
            Me.ViewState("StringIds") = ""
            Me.ViewState("ViewIdx") = -1
            Me.ViewState("StringName") = ""
            Me.ViewState("Analysis_Fields") = ""
            Me.ViewState("Analysis_Join") = ""
            Me.ViewState("Analysis_OrderBy") = ""
            Me.ViewState("Report_GroupName") = ""
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    Public Function SetFilter() As Boolean
        Try

            objGrievanceReport.SetDefaultValue()

            objGrievanceReport._UserAccessFilter = ""
            objGrievanceReport._AdvanceFilter = ""
            objGrievanceReport._IncludeAccessFilterQry = True

            objGrievanceReport._Approvalsetting = CInt(Session("GrievanceApprovalSetting"))
            objGrievanceReport._FromGreviceDate = dtfromdate.GetDate
            objGrievanceReport._ToGreviceDate = dttodate.GetDate

            objGrievanceReport._FromEmployeeid = drpfromemployee.SelectedValue
            objGrievanceReport._FromEmployeename = drpfromemployee.SelectedItem.Text
            objGrievanceReport._AginstEmployeeid = drpaginstemployee.SelectedValue
            objGrievanceReport._AginstEmployeename = drpaginstemployee.SelectedItem.Text
            objGrievanceReport._Refno = txtrefno.Text
            objGrievanceReport._ShowCommiteeMembers = chkshowcommitee.Checked
            objGrievanceReport._CommiteeMembers = chkshowcommitee.Text
            objGrievanceReport._GrievcaneStatusid = drpgrestatus.SelectedValue
            objGrievanceReport._GrievcaneStatus = drpgrestatus.SelectedItem.Text

            objGrievanceReport._ViewByIds = Me.ViewState("StringIds").ToString
            objGrievanceReport._ViewIndex = CInt(Me.ViewState("ViewIdx"))
            objGrievanceReport._ViewByName = Me.ViewState("StringName").ToString()
            objGrievanceReport._Analysis_Fields = Me.ViewState("Analysis_Fields").ToString()
            objGrievanceReport._Analysis_Join = Me.ViewState("Analysis_Join").ToString()
            objGrievanceReport._Analysis_OrderBy = Me.ViewState("Analysis_OrderBy").ToString
            objGrievanceReport._Report_GroupName = Me.ViewState("Report_GroupName").ToString()
            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try

    End Function
#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objGrievanceReport.setDefaultOrderBy(0)

            objGrievanceReport._CompanyUnkid = Session("CompanyUnkId")
            objGrievanceReport._UserUnkid = Session("UserId")
            objGrievanceReport._UserAccessFilter = Session("AccessLevelFilterString")
            Call SetDateFormat()

            objGrievanceReport.generateReportNew(Session("Database_Name"), _
                                           Session("UserId"), _
                                           Session("Fin_year"), _
                                           Session("CompanyUnkId"), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           Session("UserAccessModeSetting"), True, _
                                           Session("ExportReportPath"), _
                                           Session("OpenAfterExport"), 0, _
                                           enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))

            Session("objRpt") = objGrievanceReport._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


#End Region

#Region "Control Event"

    'Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
    '    Try
    '        AnalysisBy.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("lnkAnalysisBy_Click :-" & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AnalysisBy.buttonApply_Click
    '    Try
    '        Me.ViewState("StringIds") = AnalysisBy._ReportBy_Ids
    '        Me.ViewState("StringName") = AnalysisBy._ReportBy_Name
    '        Me.ViewState("ViewIdx") = AnalysisBy._ViewIndex
    '        Me.ViewState("Analysis_Fields") = AnalysisBy._Analysis_Fields
    '        Me.ViewState("Analysis_Join") = AnalysisBy._Analysis_Join
    '        Me.ViewState("Analysis_OrderBy") = AnalysisBy._Analysis_OrderBy
    '        Me.ViewState("Report_GroupName") = AnalysisBy._Report_GroupName
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("popAnalysisby_buttonApply_Click :-" & ex.Message, Me)
    '    End Try
    'End Sub

#End Region

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objGrievanceReport = New clsGrievanceReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call FillCombo()
                Call ResetValue()
                Me.ViewState.Add("StringIds", "")
                Me.ViewState.Add("StringName", "")
                Me.ViewState.Add("ViewIdx", -1)
                Me.ViewState.Add("Analysis_Fields", "")
                Me.ViewState.Add("Analysis_Join", "")
                Me.ViewState.Add("Analysis_OrderBy", "")
                Me.ViewState.Add("Report_GroupName", "")
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

End Class
