﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Recruitment_Reports_AgencyWorkedReport_Rpt_AgencyWorkedReport
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAgencyWorkedReport"
    Private objAwf As clsAgencyWorkedReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objVacancy As New clsVacancy
        Dim objApplicantList As New clsApplicant_master
        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        Try

            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "Emp", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
                .DataBind()
            End With

            dsList = objApplicantList.GetApplicantList("list", True, , True)
            With cboApplicant
                .DataValueField = "applicantunkid"
                .DataTextField = "applicantname"
                .DataSource = dsList.Tables("list")
                .SelectedValue = 0
                .DataBind()
            End With

            dsList = objVacancy.getVacancyType
            With cboVacancyType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
            Call cboVacancyType_SelectedIndexChanged(Nothing, Nothing)

            With cboReportType
                .Items.Clear()
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 100, "Applicant Agency Worked For Report"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 101, "Employee Agency Worked For Report"))
                .SelectedIndex = 0
            End With
            cboReportType_SelectedIndexChanged(Nothing, Nothing)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmp = Nothing : dsList.Dispose() : objApplicantList = Nothing : objVacancy = Nothing : objMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            Call cboReportType_SelectedIndexChanged(Nothing, Nothing)
            cboVacancyType.SelectedValue = 0
            cboVacancyType_SelectedIndexChanged(Nothing, Nothing)
            cboApplicant.SelectedValue = 0
            cboEmployee.SelectedValue = 0           
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAwf.SetDefaultValue()

            objAwf._ReportTypeId = cboReportType.SelectedIndex
            objAwf._ReportTypeName = cboReportType.SelectedItem.Text

            objAwf._ApplicantId = cboApplicant.SelectedValue
            objAwf._ApplicantName = cboApplicant.SelectedItem.Text

            objAwf._VacancyID = cboVacancy.SelectedValue
            objAwf._VacancyName = cboVacancy.SelectedItem.Text

            objAwf._VacancyTypeID = cboVacancyType.SelectedValue
            objAwf._VacancyTypeName = cboVacancyType.SelectedItem.Text

            objAwf._EmployeeId = cboEmployee.SelectedValue
            objAwf._EmployeeName = cboEmployee.SelectedItem.Text

            objAwf._ViewByIds = mstrStringIds
            objAwf._ViewIndex = mintViewIdx
            objAwf._ViewByName = mstrStringName

            objAwf._Analysis_Fields = mstrAnalysis_Fields
            objAwf._Analysis_Join = mstrAnalysis_Join
            objAwf._Analysis_OrderBy = mstrAnalysis_OrderBy
            objAwf._Report_GroupName = mstrReport_GroupName

            objAwf._Advance_Filter = mstrAdvanceFilter

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objAwf = New clsAgencyWorkedReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            If Not IsPostBack Then
                Call FillCombo()
            Else
                mstrStringIds = CStr(Me.ViewState("mstrStringIds"))
                mstrStringName = CStr(Me.ViewState("mstrStringName"))
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysis_Join = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysis_OrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReport_GroupName = CStr(Me.ViewState("mstrReport_GroupName"))
                mstrAdvanceFilter = Me.ViewState("mstrAdvanceFilter").ToString()
                cboReportType_SelectedIndexChanged(Nothing, Nothing)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
            Me.ViewState("mstrAdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Call SetDateFormat()
            Dim strFileName As String = String.Empty

            objAwf.generateReportNew(Session("Database_Name").ToString, _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                             CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                             CStr(Session("UserAccessModeSetting")), True, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                             False, False, enPrintAction.None, enExportAction.ExcelExtra, 0)

            If strFileName.Trim.Length <= 0 Then strFileName = objAwf._FileNameAfterExported.Trim
            If strFileName.Contains(".xls") = False Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName & ".xls"
            Else
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName
            End If
            Export.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAdvFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdvFilter.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case cboReportType.SelectedIndex
                Case 0  'APPLICANT RESIDENCE REPORT

                    objpnlEmployee.Visible = False
                    objpnlApplicant.Visible = True
                    lnkAnalysisBy.Visible = False
                    btnAdvFilter.Visible = False

                    mstrStringIds = ""
                    mstrStringName = ""
                    mintViewIdx = 0
                    mstrAnalysis_Fields = ""
                    mstrAnalysis_Join = ""
                    mstrAnalysis_OrderBy = ""
                    mstrReport_GroupName = ""
                    mstrAdvanceFilter = ""

                    cboEmployee.SelectedValue = 0

                Case 1  'EMPLOYEE RESIDENCE REPORT
                    objpnlApplicant.Visible = False
                    objpnlEmployee.Visible = True
                    lnkAnalysisBy.Visible = True
                    btnAdvFilter.Visible = True

                    cboApplicant.SelectedValue = 0
                    cboVacancyType.SelectedValue = 0
                    Call cboVacancyType_SelectedIndexChanged(Nothing, Nothing)

            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboVacancyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboVacancyType.SelectedIndexChanged
        Try
            Dim objVacancy As New clsVacancy
            Dim dsCombo As New DataSet
            dsCombo = objVacancy.getComboList(ConfigParameter._Object._CurrentDateAndTime, True, "List", -1, CInt(cboVacancyType.SelectedValue))
            With cboVacancy
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class
