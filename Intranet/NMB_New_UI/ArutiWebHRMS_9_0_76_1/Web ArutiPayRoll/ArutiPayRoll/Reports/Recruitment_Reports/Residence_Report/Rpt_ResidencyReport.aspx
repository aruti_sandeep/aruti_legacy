﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="Rpt_ResidencyReport.aspx.vb" Inherits="Reports_Recruitment_Reports_Residence_Report_Rpt_ResidencyReport" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="popupAnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc7" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Residence Report"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" ToolTip="Analysis By">
                                                 <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReportType" runat="server" Text="Report Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="objpnlApplicant" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblApplicant" runat="server" Text="Applicant" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboApplicant" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblVacancyType" runat="server" Text="Vacancy Type" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboVacancyType" runat="server" AutoPostBack="true" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblVacancy" runat="server" Text="Vacancy" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboVacancy" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="objpnlEmployee" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboEmployee" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="objpnlResidency" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblResidencyType" runat="server" Text="Residency Type" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboResidencyType" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnAdvFilter" runat="server" Text="Advance Filter" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                    <uc1:popupAnalysisBy ID="popupAnalysisBy" runat="server" />
                    <uc7:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </div>
                <uc9:Export runat="server" ID="Export" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
