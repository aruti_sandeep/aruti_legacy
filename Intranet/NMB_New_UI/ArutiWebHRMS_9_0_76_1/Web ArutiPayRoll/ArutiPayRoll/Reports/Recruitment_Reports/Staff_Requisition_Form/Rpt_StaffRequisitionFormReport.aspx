﻿<%@ Page Title="Staff Requisition Form Report" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="Rpt_StaffRequisitionFormReport.aspx.vb" Inherits="Reports_Recruitment_Reports_Staff_Requisition_Form_Rpt_StaffRequisitionFormReport" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_mian" runat="server">
        <ContentTemplate>
            <div class="row clearfix d--f jc--c">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Staff Requisition Form Report"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblRequisitiontype" runat="server" Text="Requisition Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboRequisitionType" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboJob" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblWorkStartDateFrom" runat="server" Text="Work Date From" CssClass="form-label"></asp:Label>
                                            <uc2:DateCtrl ID="dtpWStartDateFrom" runat="server" AutoPostBack="false" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblWorkStartDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                            <uc2:DateCtrl ID="dtpWStartDateTo" runat="server" AutoPostBack="false" />
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPositionFrom" runat="server" Text="Position From" CssClass="form-label"></asp:Label>
                                            <uc6:NumericText ID="nudPositionFrom" runat="server" Max="100" Min="0" />
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPositionTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                            <uc6:NumericText ID="nudPositionTo" runat="server" Max="100" Min="0" />
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="txtSearchFormNo" name="txtSearchFormNo" placeholder="Search Form No."
                                                          maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchFormNo', '<%= dgvFormNo.ClientID %>');" />
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="max-height: 170px;">
                                                    <asp:GridView ID="dgvFormNo" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        AllowPaging="False" Width="99%" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                        RowStyle-Wrap="false" DataKeyNames="staffrequisitiontranunkid, formno">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="objchkFormNo" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'objdgcolhFCheck');" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="objdgcolhFCheck" runat="server" Text=" " CssClass="chk-sm" Checked='<%# Eval("IsCheck")%>'/>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="FormNoWithJobTitle" HeaderText="Staff Requisition Form No"
                                                                ReadOnly="true" FooterText="dgcolhFormNo" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblRequisitionBy" runat="server" Text="Requisition By" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboRequistionBy" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="txtSearchAllocation" name="txtSearchAllocation" placeholder="Search Allocation."
                                                          maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchAllocation', '<%= dgvAllocation.ClientID %>');" />
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="max-height: 245px;">
                                                    <asp:GridView ID="dgvAllocation" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        AllowPaging="False" Width="99%" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                        RowStyle-Wrap="false" DataKeyNames="id, name">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="objchkAllocation" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'objdgcolhACheck');" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="objdgcolhACheck" runat="server" Text=" " CssClass="chk-sm" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="name" HeaderText=" " ReadOnly="true" FooterText="objdgcolhAllocation" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                              <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">

        function CheckAll(chkbox, chkgvSelect) {
            var grid = $(chkbox).closest(".table-responsive");
            $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));
        }

        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };
        
        function FromSearching(txtID, gridClientID) {
            if (gridClientID.toString().includes('%') == true)
                gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();

            if ($('#' + txtID).val().length > 0) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
               
                  $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();

            }
            else if ($('#' + txtID).val().length == 0) {
                resetFromSearchValue(txtID, gridClientID,);
            }
            if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue(txtID, gridClientID);
            }
        }

        function resetFromSearchValue(txtID, gridClientID) {


            $('#' + txtID).val('');
              $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
              $('.norecords').remove();            
            $('#' + txtID).focus();
        }
       
    </script>

</asp:Content>
