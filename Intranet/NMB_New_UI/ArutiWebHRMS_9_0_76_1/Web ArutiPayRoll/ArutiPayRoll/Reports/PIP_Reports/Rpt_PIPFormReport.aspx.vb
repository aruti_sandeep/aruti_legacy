﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_PIP_Reports_Rpt_PIPFormReport
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private objPIPForm As clsPIPFormReport
    Private ReadOnly mstrModuleName As String = "frmPIPFormReport"

#End Region

#Region " Private Functions & Methods "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsList = ObjEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                CStr(Session("UserAccessModeSetting")), _
                                                True, CBool(Session("IsIncludeInactiveEmp")), _
                                                "EmpList")

                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("employeeunkid") = 0
                dRow.Item("EmpCodeName") = "Select"
                dsList.Tables(0).Rows.InsertAt(dRow, 0)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedValue = "0"
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                End With
            End If

            cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList.Dispose()
            ObjEmp = Nothing
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objPIPForm.SetDefaultValue()

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Emplyoee is mandatory information. Please select employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            ElseIf CInt(cboPIPFormNo.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "PIP Form No is mandatory information. Please select PIP Form No to continue."), Me)
                cboPIPFormNo.Focus()
                Return False
            End If

            objPIPForm._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            objPIPForm._EmployeeName = cboEmployee.SelectedItem.Text
            objPIPForm._PIPFormId = CInt(cboPIPFormNo.SelectedValue)
            objPIPForm._PIPFormNo = cboPIPFormNo.SelectedItem.Text
            objPIPForm._EmployeeAsOnDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date
            objPIPForm._CompanyUnkId = CInt(Session("CompanyUnkId"))

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objPIPForm._UserName = Session("DisplayName").ToString()
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objPIPForm._UserUnkId = CInt(Session("UserId"))
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedIndex = 0
            cboPIPFormNo.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objPIPForm = New clsPIPFormReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                Call SetLanguage()
                Call SetMessages()
                Call FillCombo()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            Call SetDateFormat()

            If Not SetFilter() Then Exit Sub

            objPIPForm.generateReportNew(Session("Database_Name"), _
                                         Session("UserId"), _
                                         Session("Fin_year"), _
                                         Session("CompanyUnkId"), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                         Session("UserAccessModeSetting"), True, _
                                         Session("ExportReportPath"), _
                                         Session("OpenAfterExport"), _
                                         0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))

            Session("objRpt") = objPIPForm._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim objPIPForm As New clspipform_master
        Dim dsList As DataSet = Nothing
        Try
            dsList = objPIPForm.getComboList(CInt(cboEmployee.SelectedValue), "List", True, True, "")
            With cboPIPFormNo
                .DataValueField = "pipformunkid"
                .DataTextField = "formno"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList.Dispose()
            dsList = Nothing
            objPIPForm = Nothing
        End Try
    End Sub

#End Region

#Region " Language "

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPIPFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPIPFormNo.ID, Me.lblPIPFormNo.Text)
            Me.BtnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReport.ID, Me.BtnReport.Text).Replace("&", "")
            Me.BtnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
            Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Emplyoee is mandatory information. Please select employee to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "PIP Form No is mandatory information. Please select PIP Form No to continue.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

#End Region

  
End Class
