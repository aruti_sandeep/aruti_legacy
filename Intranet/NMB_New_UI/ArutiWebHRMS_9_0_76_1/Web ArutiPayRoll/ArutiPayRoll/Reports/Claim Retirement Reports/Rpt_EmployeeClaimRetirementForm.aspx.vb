﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region
Partial Class Reports_Claim_Retirement_Reports_Rpt_EmployeeClaimRetirementForm
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmEmployeeClaimRetirementForm"
    Private dtTranHead As DataTable = Nothing
#End Region

#Region " Private Function "
    Public Sub FillCombo()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim dsList As DataSet

            Dim intEmployeeID As Integer = 0
            Dim blnSelect As Boolean = True
            Dim blnIncludeAccessFilter As Boolean = True

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                intEmployeeID = CInt(Session("Employeeunkid"))
                blnIncludeAccessFilter = False
                blnSelect = False
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                         CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                          Session("UserAccessModeSetting").ToString(), _
                                          True, CBool(Session("IsIncludeInactiveEmp")), "Emp", blnSelect, intEmployeeID, , , , , , , , , , , , , , , , blnIncludeAccessFilter)

            cboEmployee.DataValueField = "employeeunkid"
            cboEmployee.DataTextField = "EmpCodeName"
            cboEmployee.DataSource = dsList.Tables(0)
            cboEmployee.DataBind()
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                cboEmployee.SelectedValue = CStr(Session("Employeeunkid"))
            Else
                cboEmployee.SelectedValue = "0"
            End If
            objEmployee = Nothing

            Dim objExpenseCategory As New clsexpense_category_master
            dsList = objExpenseCategory.GetExpenseCategory(Session("Database_Name").ToString(), True, True, True, "List", True, True, True, True, True)
            objExpenseCategory = Nothing

            cboExpenseCategory.DataSource = dsList.Tables(0)
            cboExpenseCategory.DataValueField = "Id"
            cboExpenseCategory.DataTextField = "Name"
            cboExpenseCategory.SelectedValue = "0"
            cboExpenseCategory.DataBind()

            cboExpenseCategory_SelectedValueChanged(Nothing, Nothing)

            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpFromDate.SetDate = Nothing
            dtpToDate.SetDate = Nothing
            cboEmployee.SelectedIndex = 0
            cboExpenseCategory.SelectedIndex = 0
            cboRetirementForm.SelectedIndex = 0
            'txtFilterAllowances.Text = ""
            'chkShowEmpScale.Checked = False
            FillTranHead()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function Validation() As Boolean

        Dim mblnFlag As Boolean = False
        Try
            If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Expese Category is compulsory information.Please Select Expense Category."), Me)
                cboExpenseCategory.Focus()
                Return False


            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), Me)
                cboExpenseCategory.Focus()
                Return False

            ElseIf CInt(cboRetirementForm.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Claim Retirement Form is compulsory information.Please Select Claim Retirement Form."), Me)
                cboRetirementForm.Focus()
                Return False

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Public Function SetFilter(ByRef objClaimRetirementForm As clsEmployeeClaimRetirementForm) As Boolean
        Try
            objClaimRetirementForm.SetDefaultValue()

            objClaimRetirementForm._EmployeeId = CInt(cboEmployee.SelectedValue)
            objClaimRetirementForm._EmployeeName = cboEmployee.SelectedItem.Text
            objClaimRetirementForm._ExpenseCategoryId = CInt(cboExpenseCategory.SelectedValue)
            objClaimRetirementForm._ExpenseCategory = cboExpenseCategory.SelectedItem.Text
            objClaimRetirementForm._ClaimRetirementFormId = CInt(cboRetirementForm.SelectedValue)
            objClaimRetirementForm._ClaimRetirementFormNo = cboRetirementForm.SelectedItem.Text

            objClaimRetirementForm._YearId = CInt(Session("Fin_year"))
            objClaimRetirementForm._Fin_StartDate = FinancialYear._Object._Database_Start_Date.Date
            objClaimRetirementForm._Fin_Enddate = FinancialYear._Object._Database_End_Date.Date

            Dim objClaimRequest As New clsclaim_request_master
            objClaimRequest._Crmasterunkid = CInt(cboRetirementForm.SelectedValue)


            If Session("CompName").ToString().ToUpper() = "KADCO" Then
                If objClaimRequest._Statusunkid = 1 AndAlso objClaimRequest._IsPrinted = False Then
                    objClaimRetirementForm._IsPrinted = True
                    objClaimRetirementForm._PrintedDateTime = ConfigParameter._Object._CurrentDateAndTime
                    objClaimRetirementForm._PrintUserId = CInt(Session("UserId"))
                    objClaimRetirementForm._PrintedIp = CStr(Session("IP_ADD"))
                    objClaimRetirementForm._PrintedHost = CStr(Session("HOST_NAME"))
                    objClaimRetirementForm._IsPrintFromWeb = True
                Else
                    objClaimRetirementForm._IsPrinted = False
                    objClaimRetirementForm._PrintedDateTime = Nothing
                    objClaimRetirementForm._PrintUserId = -1
                    objClaimRetirementForm._PrintedIp = ""
                    objClaimRetirementForm._PrintedHost = ""
                    objClaimRetirementForm._IsPrintFromWeb = False
                End If
            End If


            Dim objMasterData As New clsMasterData
            Dim mintPeriodID As Integer = objMasterData.getCurrentTnAPeriodID(enModuleReference.Payroll, objClaimRequest._Transactiondate.Date)
            objMasterData = Nothing

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = mintPeriodID
            objClaimRetirementForm._PeiordID = mintPeriodID
            objClaimRetirementForm._PeriodStartDate = objPeriod._TnA_StartDate.Date
            objClaimRetirementForm._PeriodEndDate = objPeriod._TnA_EndDate.Date
            objPeriod = Nothing
            objClaimRequest = Nothing

            If CInt(cboExpenseCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                objClaimRetirementForm._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
                objClaimRetirementForm._LeaveAccrueTenureSetting = CInt(Session("LeaveAccrueTenureSetting"))
                objClaimRetirementForm._LeaveAccrueDaysAfterEachMonth = CInt(Session("LeaveAccrueDaysAfterEachMonth"))
            End If

            If dtTranHead IsNot Nothing AndAlso dtTranHead.Rows.Count > 0 Then
                objClaimRetirementForm._SpecialAllowanceIds = String.Join(",", dtTranHead.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischecked") = True).Select(Function(y) y.Field(Of Integer)("tranheadunkid").ToString()).ToArray())
            End If

            'objClaimForm._ShowEmployeeScale = chkShowEmpScale.Checked

            objClaimRetirementForm._UserUnkId = CInt(Session("UserId"))
            objClaimRetirementForm._CompanyUnkId = CInt(Session("Companyunkid"))
            objClaimRetirementForm._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = Session("fmtCurrency").ToString()


            If CInt(Session("Employeeunkid")) > 0 Then
                objClaimRetirementForm._UserName = Session("DisplayName").ToString()
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub FillTranHead()
        'Dim objTranHead As clsTransactionHead = Nothing
        'Try
        '    objTranHead = New clsTransactionHead
        '    Dim dsList As DataSet = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "List", False, 0, 0, -1, False, False, "typeof_id <> " & enTypeOf.Salary, False, False, False)

        '    Dim dcColumn As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
        '    dcColumn.DefaultValue = False
        '    dsList.Tables(0).Columns.Add(dcColumn)

        '    dtTranHead = dsList.Tables(0).Copy()
        '    dgTranHeads.DataSource = dtTranHead
        '    dgTranHeads.DataBind()

        '    If dsList IsNot Nothing Then dsList.Clear()
        '    dsList = Nothing
        'Catch ex As Exception
        '    DisplayMessage.DisplayError(ex, Me)
        'Finally
        '    objTranHead = Nothing
        'End Try
    End Sub
#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Not IsPostBack Then
                SetLanguage()
                GC.Collect()
                Call FillCombo()
                Call ResetValue()
                Call FillTranHead()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState.Add("dtTranHead", dtTranHead)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Dim objClaimRetirementForm As New clsEmployeeClaimRetirementForm(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            If Validation() = False Then Exit Sub

            If SetFilter(objClaimRetirementForm) = False Then Exit Sub

            Call SetDateFormat()


            objClaimRetirementForm.generateReportNew(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                              , CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                              , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, CStr(Session("UserAccessModeSetting")) _
                                                              , True, Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport")), 0, Aruti.Data.enPrintAction.None, enExportAction.None, 0)

            Session("objRpt") = objClaimRetirementForm._Rpt

            If objClaimRetirementForm._Rpt IsNot Nothing Then ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objClaimRetirementForm = Nothing
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ComboBox"

    Private Sub cboExpenseCategory_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            Dim objClaimRetirement As New clsclaim_retirement_master
            Dim dtList As DataTable
            If dtpFromDate.IsNull = False AndAlso dtpToDate.IsNull = False Then
                Dim mstrSearch = "CONVERT(CHAR(8),cmclaim_retirement_master.transactiondate,112) >= '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date).ToString() & "' AND CONVERT(CHAR(8),cmclaim_retirement_master.transactiondate,112) <= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date).ToString() & "'"
                dtList = objClaimRetirement.GetEmployeeClaimRetirementForm(CInt(cboEmployee.SelectedValue), CInt(cboExpenseCategory.SelectedValue), True, mstrSearch)
            Else
                dtList = objClaimRetirement.GetEmployeeClaimRetirementForm(CInt(cboEmployee.SelectedValue), CInt(cboExpenseCategory.SelectedValue), True)
            End If
            cboRetirementForm.DataSource = dtList
            cboRetirementForm.DataTextField = "claimretirementno"
            cboRetirementForm.DataValueField = "claimretirementunkid"
            cboRetirementForm.SelectedValue = "0"
            cboRetirementForm.DataBind()
            objClaimRetirement = Nothing

            If dtList IsNot Nothing Then dtList.Clear()
            dtList = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Try
        '    Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

        '    Dim dvTranHead As DataView = dtTranHead.DefaultView

        '    dvTranHead.RowFilter = "code like '%" & txtFilterAllowances.Text.Trim & "%' OR name like '%" & txtFilterAllowances.Text.Trim & "%' "

        '    For i As Integer = 0 To dvTranHead.ToTable.Rows.Count - 1
        '        Dim gvRow As DataGridItem = dgTranHeads.Items(i)
        '        CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
        '        Dim dRow() As DataRow = dtTranHead.Select("tranheadunkid = '" & gvRow.Cells(3).Text & "'")
        '        If dRow.Length > 0 Then
        '            dRow(0).Item("IsChecked") = chkSelectAll.Checked
        '        End If
        '        dvTranHead.Table.AcceptChanges()
        '    Next
        '    dtTranHead = dvTranHead.Table

        'Catch ex As Exception
        '    DisplayMessage.DisplayError(ex, Me)
        'End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Try
        '    Dim chkSelect As CheckBox = CType(sender, CheckBox)
        '    Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)

        '    Dim drRow() As DataRow = dtTranHead.Select("tranheadunkid = " & item.Cells(3).Text)
        '    If drRow.Length > 0 Then
        '        drRow(0).Item("IsChecked") = chkSelect.Checked
        '    End If
        '    dtTranHead.AcceptChanges()

        'Catch ex As Exception
        '    DisplayMessage.DisplayError(ex, Me)
        'End Try
    End Sub
#End Region

#Region " TextBox Events "

    Protected Sub txtFilterAllowances_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Try
        '    If dtTranHead IsNot Nothing Then
        '        Dim dvFilterAllowancesSearch As DataView = dtTranHead.DefaultView
        '        If dvFilterAllowancesSearch.Table.Rows.Count > 0 Then
        '            If txtFilterAllowances.Text.Trim.Length > 0 Then
        '                dvFilterAllowancesSearch.RowFilter = "code like '%" & txtFilterAllowances.Text.Trim & "%' OR name like '%" & txtFilterAllowances.Text.Trim & "%' "
        '            End If
        '            dgTranHeads.DataSource = dvFilterAllowancesSearch.ToTable
        '            dgTranHeads.DataBind()
        '        End If
        '    End If
        'Catch ex As Exception
        '    DisplayMessage.DisplayError(ex, Me)

        'End Try
    End Sub

#End Region

#Region "DatePicker Events"

    Private Sub dtpFromDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFromDate.TextChanged, dtpToDate.TextChanged
        Try
            If dtpFromDate.IsNull = False AndAlso dtpToDate.IsNull = False Then
                cboExpenseCategory_SelectedValueChanged(cboExpenseCategory, New EventArgs())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.LblRetirementform.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRetirementform.ID, Me.LblRetirementform.Text)
            Me.LblExpenseCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblExpenseCategory.ID, Me.LblExpenseCategory.Text)
            'Me.LblSpecialAllowance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblSpecialAllowance.ID, Me.LblSpecialAllowance.Text)
            'Me.chkShowEmpScale.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowEmpScale.ID, Me.chkShowEmpScale.Text)

            Me.LblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFromDate.ID, Me.LblFromDate.Text)
            Me.LblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblToDate.ID, Me.LblToDate.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

End Class
