﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_Employee_Listing.aspx.vb"
    Inherits="Reports_Rpt_Employee_Listing" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Employee Listing Report"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" ToolTip="Analysis By" CssClass="lnkhover">
                                        <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpemployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployeementType" runat="server" Text="Employement Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployeementType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkGeneratePeriod" runat="server" Text="Generate Based on Period"
                                            AutoPostBack="true" OnCheckedChanged="chkGeneratePeriod_CheckedChanged" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <%--'S.SANDEEP |06-Jan-2023| -- START--%>
                                <%--'ISSUE/ENHANCEMENT : A1X-554--%>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card">
                                            <div class="header">
                                                <h4>
                                                    <asp:Label ID="gbEmpDisplayName" runat="server" Text="Employee Name Display Setting"></asp:Label>
                                                </h4>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lblSaveSelection" runat="server" Text="Save Setting" ToolTip="Save Setting"
                                                            CssClass="lnkhover">                                                        
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:RadioButton AutoPostBack="true" runat="server" Text="Show Firstname, Surname & Othername  Separately"
                                                            ID="radShowAllSeparately" GroupName="NS" />
                                                        <asp:RadioButton AutoPostBack="true" runat="server" Text="Show in desired format set, Please use #FName#, #OName#, #SName#"
                                                            ID="radShowInCombination" GroupName="NS" />
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtNameSetting" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <asp:Label ID="lblExample" runat="server" Text="Sample Format : #FName#-#SName# Put '-' in between"
                                                            CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--'S.SANDEEP |06-Jan-2023| -- END--%>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblBirthdateFrom" runat="server" Text="Birthdate From" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpBirthdate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblBirthdateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpBirthdateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblAnniversaryFrom" runat="server" Text="Anniversary Date From" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpAnniversaryDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblAnniversaryDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpAnniversaryDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblFirstAppointDateFrom" runat="server" Text="First Appointment Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpFAppointDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblFirstAppointDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpFAppointDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblAppointDateFrom" runat="server" Text="Appointment Date From" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpAppointDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblAppointDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpAppointDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblConfirmationDateFrom" runat="server" Text="Confirmation Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpConfirmationDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblConfirmationDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpConfirmationDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblProbationStartDateFrom" runat="server" Text="Probation Start Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpProbationStartDateFrom" runat="server" AutoPostBack="false">
                                        </uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblProbationStartDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpProbationStartDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblProbationEndDateFrom" runat="server" Text="Probation End Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpProbationEndDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblProbationEndDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpProbationEndDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblSuspendedStartDateFrom" runat="server" Text="Suspended Start Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpSuspendedStartDateFrom" runat="server" AutoPostBack="false">
                                        </uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblSuspendedStartDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpSuspendedStartDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblSuspendedEndDateFrom" runat="server" Text="Suspended End Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpSuspendedEndDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblSuspendedEndDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpSuspendedEndDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblReinstatementDateFrom" runat="server" Text="Reinstatement Date From"
                                            CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpReinstatementFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblReinstatementDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpReinstatementTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblEOCDateFrom" runat="server" Text="End Of Contract Date From" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpEOCDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <asp:Label ID="LblEOCDateTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpEOCDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee"
                                            Checked="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowEmpScale" runat="server" Text="Show Employee Scale" Checked="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbColumns" runat="server" Text="Add/Remove Field(s)" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" id="txtSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                    maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching('txtSearch', '<%= dgvchkEmpfields.ClientID %>');" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 315px;">
                                                            <asp:GridView ID="dgvchkEmpfields" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" CssClass="table table-hover table-bordered"
                                                                RowStyle-Wrap="false" DataKeyNames="Id">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-CssClass="align-center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkSelectAll" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'chkSelect');" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelect" runat="server" Text=" " CssClass="chk-sm" Checked='<%# Eval("IsChecked")%>' />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="id" HeaderText="Id" ReadOnly="true" Visible="false"></asp:BoundField>
                                                                    <asp:BoundField DataField="name" HeaderText="Field" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                        FooterText="cohField">
                                                                        <HeaderStyle HorizontalAlign="Left" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <HeaderStyle Font-Bold="False" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnExport" runat="server" Text="Export" CssClass="btn btn-default" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                    <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                    <uc9:Export runat="server" ID="Export" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Export" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">

        function CheckAll(chkbox, chkgvSelect) {
            var grid = $(chkbox).closest(".table-responsive");
            $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));
        }

        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };
        
        function FromSearching(txtID, gridClientID) {
            if (gridClientID.toString().includes('%') == true)
                gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();

            if ($('#' + txtID).val().length > 0) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
               
                  $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();

            }
            else if ($('#' + txtID).val().length == 0) {
                resetFromSearchValue(txtID, gridClientID,);
            }
            if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue(txtID, gridClientID);
            }
        }

        function resetFromSearchValue(txtID, gridClientID) {


            $('#' + txtID).val('');
              $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
              $('.norecords').remove();            
            $('#' + txtID).focus();
        }
       
    </script>

</asp:Content>
