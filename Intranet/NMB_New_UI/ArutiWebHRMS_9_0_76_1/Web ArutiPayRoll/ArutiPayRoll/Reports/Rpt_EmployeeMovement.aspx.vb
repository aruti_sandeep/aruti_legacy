﻿
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region


Partial Class Reports_Rpt_EmployeeMovement
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objEmovementReport As clsEmployeeMovementReport


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeMovementReport"
    'Pinkal (06-May-2014) -- End

#End Region

#Region " Private Function "

    Public Function Validation() As Boolean
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If dtFromDate.IsNull And dtToDate.IsNull Then
            If dtFromDate.IsNull = True AndAlso dtToDate.IsNull = False Then
                'Shani(24-Aug-2015) -- End
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please select To Date. To Date is compulsory information."), Me)
                'Pinkal (06-May-2014) -- End
                dtToDate.Focus()
                Return False

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ElseIf dtFromDate.IsNull = False AndAlso dtToDate.IsNull = True Then
                'Shani(24-Aug-2015) -- End
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please select To Date. To Date is compulsory information."), Me)
                dtToDate.Focus()
                Return False
                'ElseIf dtFromDate.IsNull And dtToDate.IsNull Then
                '    'Pinkal (06-May-2014) -- Start
                '    'Enhancement : Language Changes 
                '    'Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please select From Date. From Date is compulsory information."), Me)
                '    'Pinkal (06-May-2014) -- End
                '    dtFromDate.Focus()
                '    Return False
                'Shani(24-Aug-2015) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim objCost As New clscostcenter_master
        Dim ObjBMaster As New clsStation
        Dim ObjDMaster As New clsDepartment
        Dim ObjSection As New clsSections
        Dim ObjJMaster As New clsJobs
        Dim ObjGradeGrp As New clsGradeGroup
        Dim dsList As New DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If
            dsList = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                               Session("UserId"), _
                                               Session("Fin_year"), _
                                               Session("CompanyUnkId"), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                               Session("UserAccessModeSetting"), True, _
                                               Session("IsIncludeInactiveEmp"), "Emp", True)
            'Shani(24-Aug-2015) -- End

            With drpemployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = 0

            End With

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objCost.getComboList("CostCenter", True)
            'With drpCostCenter
            '    .DataValueField = "costcenterunkid"
            '    .DataTextField = "costcentername"
            '    .DataSource = dsList.Tables("CostCenter")
            '    .DataBind()
            '    .SelectedValue = 0
            'End With

            'dsList = ObjBMaster.getComboList("Branch", True)
            'With drpBranch
            '    .DataValueField = "stationunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables("Branch")
            '    .DataBind()
            '    .SelectedValue = 0
            'End With

            'dsList = ObjDMaster.getComboList("Dept", True)
            'With drpDepartment
            '    .DataValueField = "departmentunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables("Dept")
            '    .DataBind()
            '    .SelectedValue = 0
            'End With

            'dsList = ObjJMaster.getComboList("Job", True)
            'With drpJob
            '    .DataValueField = "jobunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables("Job")
            '    .DataBind()
            '    .SelectedValue = 0
            'End With

            'dsList = ObjGradeGrp.getComboList("GradeGroup", True)
            'With drpGradeGrp
            '    .DataValueField = "gradegroupunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables("GradeGroup")
            '    .DataBind()
            '    .SelectedValue = 0
            'End With
            'Shani(24-Aug-2015) -- End

            'Pinkal (03-Nov-2014) -- Start
            'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            'cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Employee Movement Report"))
            'cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Employee Movement Report With Allocations"))
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Employee Movement Allocation Wise"))
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Employee Movement Re-categorization Wise"))
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Employee Movement Grades Wise"))
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Employee Movement Costcenter Wise"))
            cboReportType.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Employee Movement Summary"))
            'Shani(15-Feb-2016) -- End

            cboReportType.SelectedIndex = 0

            Dim objvoidReason As New clsVoidReason
            dsList = objvoidReason.getComboList(1, True, "List")
            With cboAllocationReason
                .DataValueField = "Id"
                .DataTextField = "NAME"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            'Pinkal (03-Nov-2014) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            ObjEmp = Nothing
            ObjBMaster = Nothing
            ObjDMaster = Nothing
            ObjJMaster = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            drpemployee.SelectedValue = 0

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'drpCostCenter.SelectedValue = 0
            'drpBranch.SelectedValue = 0
            'drpDepartment.SelectedValue = 0
            'drpSection.SelectedValue = 0
            'drpUnit.SelectedValue = 0
            'drpJob.SelectedValue = 0
            'drpGradeGrp.SelectedValue = 0
            'drpGrade.SelectedValue = 0
            'drpGradeLevel.SelectedValue = 0
            'Shani(24-Aug-2015) -- End

            TxtFromScale.Text = 0
            TxtToScale.Text = 0
            dtFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            chkInactiveemp.Checked = False

            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            chkShowEmpScale.Checked = False
            'Pinkal (24-Apr-2013) -- End

            'Pinkal (03-Nov-2014) -- Start
            'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
            cboAllocationReason.SelectedIndex = 0
            'Pinkal (03-Nov-2014) -- End



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function SetFilter() As Boolean
    '    Try
    '        objEmovementReport.SetDefaultValue()

    '        objEmovementReport._EmployeeId = drpemployee.SelectedValue

    '        'Pinkal (24-Apr-2013) -- Start
    '        'Enhancement : TRA Changes
    '        'objEmovementReport._EmployeeName = drpemployee.Text
    '        objEmovementReport._EmployeeName = drpemployee.SelectedItem.Text
    '        'Pinkal (24-Apr-2013) -- End

    'objEmovementReport._CostCenterId = drpCostCenter.SelectedValue

    ''Pinkal (24-Apr-2013) -- Start
    ''Enhancement : TRA Changes
    ''objEmovementReport._CostCenterName = drpCostCenter.Text
    'objEmovementReport._CostCenterName = drpCostCenter.SelectedItem.Text
    ''Pinkal (24-Apr-2013) -- End

    'objEmovementReport._BranchId = drpBranch.SelectedValue

    ''Pinkal (24-Apr-2013) -- Start
    ''Enhancement : TRA Changes
    ''objEmovementReport._BranchName = drpBranch.Text
    'objEmovementReport._BranchName = drpBranch.SelectedItem.Text
    ''Pinkal (24-Apr-2013) -- End

    'objEmovementReport._DepartmentId = drpDepartment.SelectedValue

    ''Pinkal (24-Apr-2013) -- Start
    ''Enhancement : TRA Changes
    ''objEmovementReport._DepartmentName = drpDepartment.Text
    'objEmovementReport._DepartmentName = drpDepartment.SelectedItem.Text
    ''Pinkal (24-Apr-2013) -- End

    'objEmovementReport._SectionId = drpSection.SelectedValue

    ''Pinkal (24-Apr-2013) -- Start
    ''Enhancement : TRA Changes
    ''objEmovementReport._SectionName = drpSection.Text
    'objEmovementReport._SectionName = drpSection.SelectedItem.Text
    ''Pinkal (24-Apr-2013) -- End

    'objEmovementReport._UnitId = drpUnit.SelectedValue

    ''Pinkal (24-Apr-2013) -- Start
    ''Enhancement : TRA Changes
    ''objEmovementReport._UnitName = drpUnit.Text
    'objEmovementReport._UnitName = drpUnit.SelectedItem.Text
    ''Pinkal (24-Apr-2013) -- End



    'objEmovementReport._JobId = drpJob.SelectedValue

    ''Pinkal (24-Apr-2013) -- Start
    ''Enhancement : TRA Changes
    ''objEmovementReport._JobName = drpJob.Text
    'objEmovementReport._JobName = drpJob.SelectedItem.Text
    ''Pinkal (24-Apr-2013) -- End

    'objEmovementReport._GradeGroupId = drpGradeGrp.SelectedValue

    ''Pinkal (24-Apr-2013) -- Start
    ''Enhancement : TRA Changes
    ''objEmovementReport._GradeGroupName = drpGradeGrp.Text
    'objEmovementReport._GradeGroupName = drpGradeGrp.SelectedItem.Text
    ''Pinkal (24-Apr-2013) -- End

    'objEmovementReport._GradeId = drpGrade.SelectedValue

    ''Pinkal (24-Apr-2013) -- Start
    ''Enhancement : TRA Changes
    ''objEmovementReport._GradeName = drpGrade.Text
    'objEmovementReport._GradeName = drpGrade.SelectedItem.Text
    ''Pinkal (24-Apr-2013) -- End

    'objEmovementReport._GradeLevelId = drpGradeLevel.SelectedValue

    ''Pinkal (24-Apr-2013) -- Start
    ''Enhancement : TRA Changes
    ''objEmovementReport._GradeLevelName = drpGradeLevel.Text
    'objEmovementReport._GradeLevelName = drpGradeLevel.SelectedItem.Text
    ''Pinkal (24-Apr-2013) -- End


    '        If dtFromDate.IsNull = False And dtToDate.IsNull = False Then
    '            objEmovementReport._FromDate = dtFromDate.GetDate.Date
    '            objEmovementReport._ToDate = dtToDate.GetDate.Date
    '        End If

    '        If TxtFromScale.Text.Trim = "" Then
    '            objEmovementReport._FromScale = 0
    '        Else
    '            objEmovementReport._FromScale = CDbl(TxtFromScale.Text.Trim)
    '        End If

    '        If TxtToScale.Text.Trim = "" Then
    '            objEmovementReport._ToScale = 0
    '        Else
    '            objEmovementReport._ToScale = CDbl(TxtToScale.Text.Trim)
    '        End If
    '        objEmovementReport._IsActive = chkInactiveemp.Checked


    '        'Pinkal (24-Apr-2013) -- Start
    '        'Enhancement : TRA Changes
    '        objEmovementReport._ShowEmployeeScale = chkShowEmpScale.Checked
    '        'Pinkal (24-Apr-2013) -- End


    '        'Pinkal (03-Nov-2014) -- Start
    '        'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
    '        objEmovementReport._ReportId = CInt(cboReportType.SelectedIndex)
    '        objEmovementReport._ReportTypeName = cboReportType.SelectedItem.Text
    '        objEmovementReport._AllocationReasonId = CInt(cboAllocationReason.SelectedValue)
    '        objEmovementReport._AllocationReason = IIf(cboAllocationReason.SelectedValue > 0, cboAllocationReason.SelectedItem.Text, "")
    '        'Pinkal (03-Nov-2014) -- End



    '        Return True

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
    '    End Try
    'End Function
    Private Function SetFilter() As Boolean
        Try
            objEmovementReport.SetDefaultValue()

            objEmovementReport._EmployeeId = drpemployee.SelectedValue
            objEmovementReport._EmployeeName = drpemployee.SelectedItem.Text

            If dtFromDate.IsNull = False AndAlso dtToDate.IsNull = False Then
                objEmovementReport._FromDate = dtFromDate.GetDate.Date
                objEmovementReport._ToDate = dtToDate.GetDate.Date
            End If

            If txtFromScale.Text.Trim = "" Then
                objEmovementReport._FromScale = 0
            Else
                objEmovementReport._FromScale = CDbl(txtFromScale.Text.Trim)
            End If

            If txtToScale.Text.Trim = "" Then
                objEmovementReport._ToScale = 0
            Else
                objEmovementReport._ToScale = CDbl(txtToScale.Text.Trim)
            End If

            objEmovementReport._IsActive = chkInactiveemp.Checked
            objEmovementReport._ShowEmployeeScale = chkShowEmpScale.Checked
            objEmovementReport._ReportId = CInt(cboReportType.SelectedIndex)
            objEmovementReport._ReportTypeName = cboReportType.SelectedItem.Text
            objEmovementReport._AllocationReasonId = CInt(cboAllocationReason.SelectedValue)
            objEmovementReport._AllocationReason = IIf(cboAllocationReason.SelectedValue > 0, cboAllocationReason.SelectedItem.Text, "")
            'objEmovementReport._Isweb = True

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            'objEmovementReport._IsWeb = True
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objEmovementReport._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End


            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try

            ConfigParameter._Object._Companyunkid = Session("CompanyUnkId")
            Company._Object._Companyunkid = Session("CompanyUnkId")
            Aruti.Data.User._Object._Userunkid = Session("UserId")

            'HEADER PART

            strBuilder.Append(" <TITLE>" & lblPageHeader.Text & " [ " & cboReportType.SelectedItem.Text & " ]" & "</TITLE> " & vbCrLf)

            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            ConfigParameter._Object.GetReportSettings(CInt(enArutiReport.EmployeeDistributionReport))

            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If

                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>Prepared By : </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Session("UserName") & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='60%' colspan=15 align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Session("CompName") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & "Date :" & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='60%' colspan=15  align='center' > " & vbCrLf)

            strBuilder.Append(" <FONT SIZE=3><b> " & lblPageHeader.Text & " [ " & cboReportType.SelectedItem.Text & " ]" & "</B></FONT> " & vbCrLf)

            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & objEmovementReport._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            For j As Integer = 0 To objDataReader.Columns.Count - 1
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE' BACKGROUND-COLOR='BLUE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next

            strBuilder.Append(" </TR> " & vbCrLf)

            'Data Part
            For i As Integer = 0 To objDataReader.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1
                    If k <= 1 Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    Else
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End If

                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)


            Dim StrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            If StrPath.Trim.Length > 0 Then
                SaveExcelfile(StrPath.Trim & "\" & flFileName, strBuilder)
                Session("ExFileName") = StrPath.Trim & "\" & flFileName

                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                Export.Show()
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                'Gajanan (3 Jan 2019) -- End

                Return True
            Else
                Return False
            End If
            Return blnFlag
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Export_to_Excel :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SaveExcelfile:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function
    'Shani(24-Aug-2015) -- End
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objEmovementReport = New clsEmployeeMovementReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End
                Call FillCombo()

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'drpDepartment_SelectedIndexChanged(sender, e)
                'drpSection_SelectedIndexChanged(sender, e)
                'drpGradeGrp_SelectedIndexChanged(sender, e)
                'drpGrade_SelectedIndexChanged(sender, e)
                'Shani(24-Aug-2015) -- End
            End If

            'Pinkal (30-Apr-2013) -- Start
            'Enhancement : TRA Changes
            chkShowEmpScale.Visible = Session("AllowToViewScale")
            'Pinkal (30-Apr-2013) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End


    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        Dim dtTable As DataTable = Nothing
        'Shani(24-Aug-2015) -- End

        Try
            If Validation() Then
                If Not SetFilter() Then Exit Sub
                objEmovementReport._CompanyUnkId = Session("CompanyUnkId")
                objEmovementReport._UserUnkId = Session("UserId")
                'S.SANDEEP [ 12 NOV 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                objEmovementReport._UserAccessFilter = Session("AccessLevelFilterString")
                'S.SANDEEP [ 12 NOV 2012 ] -- END


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                ''Pinkal (03-Nov-2014) -- Start
                ''Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

                'If CInt(cboReportType.SelectedIndex) = 0 Then
                '    objEmovementReport.generateReport(0, enPrintAction.None, enExportAction.None)
                '    Session("objRpt") = objEmovementReport._Rpt

                '    'Shani(24-Aug-2015) -- Start
                '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '    'Open New Window And Show Report Every Report
                '    'Response.Redirect("../Aruti Report Structure/Report.aspx")
                '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                '    'Shani(24-Aug-2015) -- End

                'ElseIf CInt(cboReportType.SelectedIndex) = 1 Then
                'objEmovementReport._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
                'objEmovementReport._OpenAfterExport = False
                '    objEmovementReport.Generate_MovementReportWithAllocations()
                '    If objEmovementReport._FileNameAfterExported.Trim <> "" Then
                '        Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objEmovementReport._FileNameAfterExported

                '        'Shani [ 10 DEC 2014 ] -- START
                '        'Issue : Chrome is not supporting ShowModalDialog option now."
                '        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                '        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                '        'Shani [ 10 DEC 2014 ] -- END
                '    End If
                'End If

                ''Pinkal (03-Nov-2014) -- End
                'Pinkal (03-Nov-2014) -- Start
                'Enhancement - ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
                Dim dtStartDate As Date = Nothing
                Dim dtEndDate As Date = Nothing
                If dtFromDate.IsNull = False AndAlso dtToDate.IsNull = False Then
                    dtStartDate = dtFromDate.GetDate.Date
                    dtEndDate = dtToDate.GetDate.Date
                Else
                    dtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                    dtEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                End If

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Call SetDateFormat()
                'Pinkal (16-Apr-2016) -- End

                objEmovementReport.generateReportNew(Session("Database_Name"), _
                                                     Session("UserId"), _
                                                     Session("Fin_year"), _
                                                     Session("CompanyUnkId"), _
                                                     dtStartDate, _
                                                     dtEndDate, _
                                                     Session("UserAccessModeSetting"), True, _
                                                     IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                     Session("OpenAfterExport"), _
                                                     cboReportType.SelectedIndex, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))

                If objEmovementReport._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objEmovementReport._FileNameAfterExported
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End

                End If
                'dtTable = objEmovementReport._dtTable
                'If dtTable IsNot Nothing Then
                '   Dim strFilePath As String = "Employee_Movement" & cboReportType.SelectedItem.Text.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss") & ".xls"
                'Export_to_Excel(strFilePath, Session("ExportReportPath"), dtTable)
                'End If
                'Shani(24-Aug-2015) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ComboBox Event"

    'Shani(15-Feb-2016) -- Start
    'Report not showing Data in ESS due to Access Level Filter
    Protected Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            cboAllocationReason.DataSource = Nothing
            If cboReportType.SelectedIndex <= 3 Then
                cboAllocationReason.Enabled = True
                Dim dsList As New DataSet : Dim objCMaster As New clsCommon_Master
                Select Case cboReportType.SelectedIndex
                    Case 0
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRANSFERS, True, "List")
                    Case 1
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RECATEGORIZE, True, "List")
                    Case 2
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "List")
                    Case 3
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COST_CENTER, True, "List")
                    Case Else
                        dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRANSFERS, True, "List")
                End Select
                With cboAllocationReason
                    .DataValueField = "masterunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables(0)
                    .SelectedValue = 0
                    .DataBind()
                End With
                dsList.Dispose()
                objCMaster = Nothing
            Else
                cboAllocationReason.Enabled = False
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub
    'Shani(15-Feb-2016) -- End


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


    'Protected Sub drpDepartment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpDepartment.SelectedIndexChanged
    '    Dim dsList As New DataSet
    '    Dim ObjSection As New clsSections
    '    Try
    '        If drpDepartment.SelectedValue IsNot Nothing Then
    '            dsList = ObjSection.getComboList("Section", True, CInt(drpDepartment.SelectedValue))
    '            With drpSection
    '                .DataValueField = "sectionunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsList.Tables("Section")
    '                .DataBind()
    '                .SelectedValue = 0
    '            End With
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("drpDepartment_SelectedIndexChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub drpSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpSection.SelectedIndexChanged
    '    Dim dsList As New DataSet
    '    Dim Objunit As New clsUnits
    '    Try
    '        If drpSection.SelectedValue IsNot Nothing Then
    '            dsList = Objunit.getComboList("Unit", True, CInt(drpSection.SelectedValue))
    '            With drpUnit
    '                .DataValueField = "unitunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsList.Tables("Unit")
    '                .DataBind()
    '                .SelectedValue = 0
    '            End With
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("drpSection_SelectedIndexChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub drpGradeGrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpGradeGrp.SelectedIndexChanged
    '    Dim dsList As New DataSet
    '    Dim ObjGrade As New clsGrade
    '    Try
    '        If drpGradeGrp.SelectedValue IsNot Nothing Then
    '            dsList = ObjGrade.getComboList("Grade", True, CInt(drpGradeGrp.SelectedValue))
    '            With drpGrade
    '                .DataValueField = "gradeunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsList.Tables("Grade")
    '                .DataBind()
    '                .SelectedValue = 0
    '            End With
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("drpGradeGrp_SelectedIndexChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub drpGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpGrade.SelectedIndexChanged
    '    Dim dsList As New DataSet
    '    Dim ObjGradeLevel As New clsGradeLevel
    '    Try
    '        If drpGrade.SelectedValue IsNot Nothing Then
    '            dsList = ObjGradeLevel.getComboList("GradeLevel", True, CInt(drpGrade.SelectedValue))
    '            With drpGradeLevel
    '                .DataValueField = "gradelevelunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsList.Tables("GradeLevel")
    '                .DataBind()
    '                .SelectedValue = 0
    '            End With
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("drpGrade_SelectedIndexChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Shani(24-Aug-2015) -- End
#End Region

    Protected Sub drpemployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpemployee.DataBound
        Try         'Hemant (13 Aug 2020)
            If drpemployee.Items.Count > 0 Then
                For Each lstItem As ListItem In drpemployee.Items
                    lstItem.Attributes.Add("title", lstItem.Text)
                Next
                drpemployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToDate.ID, Me.lblToDate.Text)
            Me.LblAllocationReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblAllocationReason.ID, Me.LblAllocationReason.Text)
            Me.lblFromscale.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromscale.ID, Me.lblFromscale.Text)
            Me.lblToScale.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToScale.ID, Me.lblToScale.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.chkShowEmpScale.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowEmpScale.ID, Me.chkShowEmpScale.Text)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- End
End Class
