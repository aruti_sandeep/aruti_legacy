﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Competencies_Reports_Rpt_Family_Competencies_Report
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmFamilyCompetenciesReport"
    Private objFamCmpts As clsFamilyCompetenciesReport
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objCMst As New clsCommon_Master
        Dim objMData As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, , , True, True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objCMst.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES, True, "List")
            With cboCmptCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objMData.GetEAllocation_Notification("List")
            Dim dr As DataRow = dsCombos.Tables(0).NewRow
            dr.Item("Id") = enAllocation.EMPLOYEE_GRADES
            dr.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmGroupList", 9, "Grades")
            dsCombos.Tables(0).Rows.Add(dr)
            With cboAllocation
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With
            cboAllocation_SelectedIndexChanged(New Object, New EventArgs)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCompetencies()
        Dim objCmpt As New clsassess_competencies_master
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim ds As New DataSet
                ds = objCmpt.GetList("List", True)
                Dim dt As DataTable = Nothing
                Dim strfilter As String = ""

                strfilter = "periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
                If CInt(cboCmptCategory.SelectedValue) > 0 Then
                    strfilter &= " AND competence_categoryunkid = '" & CInt(cboCmptCategory.SelectedValue) & "' "
                End If
                dt = New DataView(ds.Tables(0), strfilter, "", DataViewRowState.CurrentRows).ToTable

                lvCmpts.DataSource = dt
                lvCmpts.DataBind()
            Else
                lvCmpts.DataSource = Nothing
                lvCmpts.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillAllocations()
        Dim dsList As New DataSet
        Try
            Select Case CInt(cboAllocation.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dsList = objBranch.GetList("List", True)
                    dsList.Tables(0).Columns("stationunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("name").ColumnName = "name"
                    objBranch = Nothing

                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dsList = objDeptGrp.GetList("List", True)
                    dsList.Tables(0).Columns("deptgroupunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("name").ColumnName = "name"
                    objDeptGrp = Nothing

                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dsList = objDept.GetList("List", True)
                    dsList.Tables(0).Columns("departmentunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("name").ColumnName = "name"
                    objDept = Nothing

                Case enAllocation.SECTION_GROUP
                    Dim objSectionGrp As New clsSectionGroup
                    dsList = objSectionGrp.GetList("List", True)
                    dsList.Tables(0).Columns("sectiongroupunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("name").ColumnName = "name"
                    objSectionGrp = Nothing

                Case enAllocation.SECTION
                    Dim objSection As New clsSections
                    dsList = objSection.GetList("List", True)
                    dsList.Tables(0).Columns("sectionunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("name").ColumnName = "name"
                    objSection = Nothing

                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dsList = objUnitGrp.GetList("List", True)
                    dsList.Tables(0).Columns("unitgroupunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("name").ColumnName = "name"
                    objUnitGrp = Nothing

                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dsList = objUnit.GetList("List", True)
                    dsList.Tables(0).Columns("unitunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("name").ColumnName = "name"
                    objUnit = Nothing

                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dsList = objTeam.GetList("List", True)
                    dsList.Tables(0).Columns("teamunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("name").ColumnName = "name"
                    objTeam = Nothing

                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dsList = objJobGrp.GetList("List", True)
                    dsList.Tables(0).Columns("jobgroupunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("name").ColumnName = "name"
                    objJobGrp = Nothing

                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dsList = objJob.GetList("List", True)
                    dsList.Tables(0).Columns("jobunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("JobName").ColumnName = "name"
                    objJob = Nothing

                Case enAllocation.CLASS_GROUP
                    Dim objClassGrp As New clsClassGroup
                    dsList = objClassGrp.GetList("List", True)
                    dsList.Tables(0).Columns("classgroupunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("name").ColumnName = "name"

                    objClassGrp = Nothing

                Case enAllocation.CLASSES
                    Dim objClass As New clsClass
                    dsList = objClass.GetList("List", True)
                    dsList.Tables(0).Columns("classesunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("name").ColumnName = "name"

                    objClass = Nothing

                Case enAllocation.COST_CENTER
                    Dim objCC As New clscostcenter_master
                    dsList = objCC.GetList("List", True)
                    dsList.Tables(0).Columns("costcenterunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("costcentername").ColumnName = "name"

                    objCC = Nothing

                Case enAllocation.EMPLOYEE_GRADES
                    Dim objGrd As New clsGrade
                    dsList = objGrd.GetList("List", True)
                    dsList.Tables(0).Columns("gradeunkid").ColumnName = "Id"
                    dsList.Tables(0).Columns("name").ColumnName = "name"

                    objGrd = Nothing
            End Select
            lvAlloc.Columns(1).HeaderText = cboAllocation.SelectedItem.Text
            lvAlloc.DataSource = dsList.Tables(0)
            lvAlloc.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            cboCmptCategory.SelectedValue = 0
            cboAllocation.SelectedValue = CInt(enAllocation.BRANCH)
            lvCmpts.DataSource = Nothing
            lvCmpts.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter(ByRef objFamCmpts As clsFamilyCompetenciesReport) As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 100, "Sorry, Period is mandatory information. Please select period to continue."), Me)
                Return False
            End If

            If lvAlloc.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True).Count > 0 Then
                Dim strValues As String = ""
                strValues = String.Join(",", lvCmpts.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True).Select(Function(x) lvCmpts.DataKeys(x.RowIndex)("Id").ToString).ToArray())
                objFamCmpts._AllocUnkids = strValues

                strValues = ""
                strValues = String.Join(",", lvCmpts.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True).Select(Function(x) lvCmpts.DataKeys(x.RowIndex)("name").ToString).ToArray())
                objFamCmpts._AllocName = strValues
            End If
            objFamCmpts._AllocRefId = cboAllocation.SelectedValue
            objFamCmpts._AllocRefName = cboAllocation.SelectedItem.Text
            objFamCmpts._CompetenceGroup = cboCmptCategory.SelectedItem.Text
            objFamCmpts._CompetenceGroupId = cboCmptCategory.SelectedValue
            If lvCmpts.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("ChkSelectedEmp"), CheckBox).Checked = True).Count > 0 Then
                Dim strValues As String = ""
                strValues = String.Join(",", lvCmpts.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("ChkSelectedEmp"), CheckBox).Checked = True).Select(Function(x) lvCmpts.DataKeys(x.RowIndex)("competenciesunkid").ToString).ToArray())
                objFamCmpts._CompetenciesIds = strValues

                strValues = ""
                strValues = String.Join(",", lvCmpts.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("ChkSelectedEmp"), CheckBox).Checked = True).Select(Function(x) lvCmpts.DataKeys(x.RowIndex)("name").ToString).ToArray())
                objFamCmpts._CompetenciesNames = strValues
            End If
            objFamCmpts._PeriodId = cboPeriod.SelectedValue
            objFamCmpts._PeriodName = cboPeriod.SelectedItem.Text

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                GC.Collect()
                Call SetControlCaptions()
                Call SetMessages()
                'Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        objFamCmpts = New clsFamilyCompetenciesReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            If SetFilter(objFamCmpts) = False Then Exit Sub
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objFamCmpts.Export_FamilyGroupCompetenciesReport(Session("Database_Name").ToString, _
                                                             CInt(Session("UserId")), _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             strFilePath, False)

            If objFamCmpts._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objFamCmpts._FileNameAfterExported
                Export.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboCmptCategory.SelectedIndexChanged
        Try
            FillCompetencies()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Try
            FillAllocations()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblAllocRef.ID, Me.lblAllocRef.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPeriod.ID, Me.lblPeriod.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnExport.ID, Me.btnExport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lvCmpts.Columns(1).FooterText, lvCmpts.Columns(1).HeaderText)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblAllocRef.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAllocRef.ID, Me.lblAllocRef.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")

            lvCmpts.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lvCmpts.Columns(1).FooterText, lvCmpts.Columns(1).HeaderText)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "frmGroupList", 9, "Grades")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 100, "Sorry, Period is mandatory information. Please select period to continue.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
