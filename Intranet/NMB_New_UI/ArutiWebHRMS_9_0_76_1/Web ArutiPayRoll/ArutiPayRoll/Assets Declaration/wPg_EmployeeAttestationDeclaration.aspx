﻿<%@ Page Title="Annual Attestation Form" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_EmployeeAttestationDeclaration.aspx.vb"
    Inherits="Assets_Declaration_wPg_EmployeeAttestationDeclaration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Annual Attestation Form" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetailHeader" runat="server" Text="Annual Attestation Form" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                        <asp:Label ID="txtHeader" runat="server" Font-Bold="true" Font-Underline="true" Font-Size="Medium"
                                            Text="Annual Attestation of Declarations and Code of Conduct Recommitment" CssClass="form-label"></asp:Label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                    </div>
                                    <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtPara" runat="server" TextMode="MultiLine"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtNoPara1" runat="server" Text="1."></asp:Label>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtPara1" runat="server" TextMode="MultiLine"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtNoPara2" runat="server" Text="2."></asp:Label>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtPara2" runat="server" TextMode="MultiLine"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtNoPara2I" runat="server" Text="i."></asp:Label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtPara2I" runat="server" TextMode="MultiLine"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtNoPara2II" runat="server" Text="ii."></asp:Label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtPara2II" runat="server" TextMode="MultiLine"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtNoPara2III" runat="server" Text="iii."></asp:Label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtPara2III" runat="server" TextMode="MultiLine"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtNoPara2IV" runat="server" Text="iv."></asp:Label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtPara2IV" runat="server" TextMode="MultiLine"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtNoPara3" runat="server" Text="3."></asp:Label>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtPara3" runat="server" TextMode="MultiLine"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtNoPara4" runat="server" Text="4."></asp:Label>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtPara4" runat="server" TextMode="MultiLine"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtNoPara5" runat="server" Text="5."></asp:Label>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtPara5" runat="server" TextMode="MultiLine"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtNoPara6" runat="server" Text="6."></asp:Label>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtPara6" runat="server" TextMode="MultiLine"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtNoPara7" runat="server" Text="7."></asp:Label>
                                            </div>
                                            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                                                <asp:Label ID="txtPara7" runat="server" TextMode="MultiLine"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 p-l-0">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-0">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmpName" runat="server" Text="Name:"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="objlblEmpName" runat="server" Text="&nbsp;"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-l-0">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmpDeclareDate" runat="server" Text="Date:"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-9 col-md-3 col-sm-12 col-xs-12">
                                                        <asp:Label ID="objlblEmpDeclareDate" runat="server" Text="&nbsp;"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                <asp:Panel ID="pnlSign" runat="server" ToolTip="If this is not your signature, upload new signature">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblempsign" runat="server" Text="Employee Signature:" Font-Bold="true"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:Image ID="imgSignature" runat="server" ToolTip="If this is not your signature, upload new signature" />
                                                            <asp:Label ID="lblnosign" runat="server" Text="Signature Not Available" ForeColor="Red"
                                                                Font-Bold="true"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkconfirmSign" ToolTip="Confirm Signature" runat="server"
                                                            Text="I confirm Signatures" />
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            </div>
                                        </div>
                                        <div style="float: right">
                                            <asp:Button ID="btnConfirmSign" runat="server" Text="Confirm Signature" CssClass="btn btn-primary" />
                                            <asp:Button ID="btnAcknowledge" runat="server" Text="I Acknowledge" CssClass="btn btn-primary" />
                                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
