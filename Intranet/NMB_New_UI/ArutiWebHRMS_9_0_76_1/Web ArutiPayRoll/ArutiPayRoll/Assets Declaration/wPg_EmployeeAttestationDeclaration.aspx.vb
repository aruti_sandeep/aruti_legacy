﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports System.IO

#End Region

Partial Class Assets_Declaration_wPg_EmployeeAttestationDeclaration
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmEmployeeAttestationDeclaration"
    Dim mintAssetDeclarationT2Unkid As Integer = -1
    Private mdtFinStartDate As DateTime
    Private mdtFinEndDate As DateTime
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Me.IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call SetLanguage()
                Call FillImage()
                Call Set_AttestationDeclarations()
            Else
                mintAssetDeclarationT2Unkid = Me.ViewState("mintAssetDeclarationt2Unkid")
                mdtFinStartDate = Me.ViewState("mdtFinStartDate")
                mdtFinEndDate = Me.ViewState("mdtFinEndDate")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mintAssetDeclarationt2Unkid", mintAssetDeclarationT2Unkid)
            Me.ViewState.Add("mdtFinStartDate", mdtFinStartDate)
            Me.ViewState.Add("mdtFinEndDate", mdtFinEndDate)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Private Method Functions "

    Private Sub FillImage()
        Dim objEmployee As New clsEmployee_Master
        Try
            pnlSign.Visible = False
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(Session("Employeeunkid"))
            If objEmployee._EmpSignature IsNot Nothing Then
                imgSignature.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=3"
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Set_AttestationDeclarations()
        Try
            Dim strPara As String = String.Empty
            Dim strPara1 As String = String.Empty
            Dim strPara2 As String = String.Empty
            Dim strPara2I As String = String.Empty
            Dim strPara2II As String = String.Empty
            Dim strPara2III As String = String.Empty
            Dim strPara2IV As String = String.Empty
            Dim strPara3 As String = String.Empty
            Dim strPara4 As String = String.Empty
            Dim strPara5 As String = String.Empty
            Dim strPara6 As String = String.Empty
            Dim strPara7 As String = String.Empty
            Dim strEmployeeNameLine As String = String.Empty
            Dim strSignatureLine As String = String.Empty

            strPara = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "I, undersigned, an employee of #CompanyName# acknowledges and agrees that:")
            strPara = strPara.Replace("#CompanyName#", Session("CompName").ToString())
            txtPara.Text = strPara

            strPara1 = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "I have honestly provided accurate and complete information about my declaration of assets, liabilities, and interests.")
            txtPara1.Text = strPara1

            strPara2 = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "I understand that the under mentioned codes, and policies are applicable to me and that there is onus and responsibility on me to be aware of the contents thereof.")
            txtPara2.Text = strPara2

            strPara2I = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "#CompanyCode# Code of Conduct")
            strPara2I = strPara2I.Replace("#CompanyCode#", Session("CompanyCode").ToString())
            txtPara2I.Text = strPara2I

            strPara2II = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "#CompanyCode# Conflict of Interest Policy")
            strPara2II = strPara2II.Replace("#CompanyCode#", Session("CompanyCode").ToString())
            txtPara2II.Text = strPara2II

            strPara2III = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "#CompanyCode# Insider Trading Policy")
            strPara2III = strPara2III.Replace("#CompanyCode#", Session("CompanyCode").ToString())
            txtPara2III.Text = strPara2III

            strPara2IV = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Tanzania Bankers Association (TBA) Code of Conduct")
            txtPara2IV.Text = strPara2IV

            strPara3 = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "I undertake to abide by the above as well as amendments released from time to time.")
            txtPara3.Text = strPara3

            strPara4 = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "I acknowledge that I am fully aware of and understand the contents and implications of the above codes and policies.")
            txtPara4.Text = strPara4

            strPara5 = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "I understand that #CompanyName# may from time to time, amend or change the code of conduct, which amendments will be communicated by #CompanyName# to myself.")
            strPara5 = strPara5.Replace("#CompanyName#", Session("CompName").ToString())
            txtPara5.Text = strPara5

            strPara6 = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "I acknowledge that this attestation and recommitment to the code of conduct is a condition of my terms of employment with #CompanyName#.")
            strPara6 = strPara6.Replace("#CompanyName#", Session("CompName").ToString())
            txtPara6.Text = strPara6

            strPara7 = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "I undertake at all times to act within the spirit of the code of conduct.")
            strPara7 = strPara7.Replace("#CompanyName#", Session("CompName").ToString())
            txtPara7.Text = strPara7

            objlblEmpName.Text = Session("E_Firstname").ToString & " " & Session("E_Othername").ToString & " " & Session("E_Surname").ToString
            objlblEmpDeclareDate.Text = ConfigParameter._Object._CurrentDateAndTime.Date


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (20 Dec 2024) -- Start
    'ISSUE/ENHANCEMENT(NMB): A1X - 2905 :  Asset declaration form changes 
    Private Sub Send_Notification(ByVal lstWebEmail As List(Of clsEmailCollection))
        Try
            If lstWebEmail.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In lstWebEmail
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    Try
                        objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                    Catch ex As Exception

                    End Try
                Next
                lstWebEmail.Clear()
            End If
        Catch ex As Exception
            Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
        Finally
            If lstWebEmail.Count > 0 Then
                lstWebEmail.Clear()
            End If
        End Try
    End Sub
    'Hemant (20 Dec 2024) -- End


#End Region

#Region "Button's Event"

    Protected Sub btnConfirmSign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmSign.Click
        Try
            pnlSign.Visible = True
            If imgSignature.ImageUrl.ToString.Trim.Length > 0 Then
                imgSignature.Visible = True
                lblnosign.Visible = False
            Else
                imgSignature.Visible = False
                lblnosign.Visible = True
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "displaypanel", "displaysign();", True)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAcknowledge_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAcknowledge.Click
        Dim dsAssetList As DataSet
        Dim blnApplyAccessFilter As Boolean = True
        Try
            Dim mdtDeclarationStartDate As Date = Nothing
            Dim mdtDeclarationEndDate As Date = Nothing

            If (Session("AssetDeclarationFromDate") IsNot Nothing AndAlso Session("AssetDeclarationFromDate").ToString().Length > 0) Then
                mdtDeclarationStartDate = eZeeDate.convertDate(Session("AssetDeclarationFromDate").ToString()).Date
            End If
            If Session("AssetDeclarationToDate") IsNot Nothing AndAlso Session("AssetDeclarationToDate").ToString().Length > 0 Then
                mdtDeclarationEndDate = eZeeDate.convertDate(Session("AssetDeclarationToDate").ToString()).Date
            End If

            If CInt(Session("Employeeunkid")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Employee information not found."), Me)
                Exit Sub
            ElseIf chkconfirmSign.Checked = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Sorry, Please confirm your signature first."), Me)
                Exit Sub
            ElseIf mdtDeclarationEndDate <> Nothing AndAlso eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) < eZeeDate.convertDate(mdtDeclarationEndDate) Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Sorry, You cannot do declaration before") & " " & mdtDeclarationEndDate.ToString("dd-MMM-yyyy"), Me)
                Exit Sub
            End If


            Dim intYear As Integer
            If eZeeDate.convertDate(Session("AssetDeclarationFromDate")) <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                intYear = eZeeDate.convertDate(Session("AssetDeclarationFromDate")).Year - CDate(Session("fin_startdate")).Year
            Else
                intYear = -1
            End If

            mdtFinStartDate = CDate(Session("fin_startdate")).AddYears(intYear)
            mdtFinEndDate = CDate(Session("fin_enddate")).AddYears(intYear)

            If Session("LoginBy") = Global.User.en_loginby.User Then
                If CBool(Session("ViewAssetsDeclarationList")) = False Then Exit Sub

            Else
                blnApplyAccessFilter = False

            End If


            Dim dicDB As New Dictionary(Of String, String)
            Dim dicDBDate As New Dictionary(Of String, String)
            Dim dicDBYearId As New Dictionary(Of String, Integer)


            dicDB.Add(Session("Database_Name"), Session("FinancialYear_Name"))
            dicDBDate.Add(Session("Database_Name"), (eZeeDate.convertDate(Session("fin_startdate")) & "|" & eZeeDate.convertDate(Session("fin_enddate"))).ToString)
            dicDBYearId.Add(Session("Database_Name"), Session("Fin_year"))


            dsAssetList = clsAssetdeclaration_masterT2.GetList(Session("UserId"), _
                                                             Session("CompanyUnkId"), _
                                                             Session("UserAccessModeSetting"), True, _
                                                             True, "List", _
                                                             dicDB, dicDBDate, dicDBYearId, , _
                                                             CInt(Session("Employeeunkid")), , , , , , "A.employeename", blnApplyAccessFilter, _
                                                             mdtFinStartDate, mdtFinEndDate, False)

            If dsAssetList.Tables(0).Rows.Count > 0 Then
                mintAssetDeclarationT2Unkid = CInt(dsAssetList.Tables(0).Rows(0).Item("assetdeclarationt2unkid").ToString())
            End If

            Dim objAssetDeclare As New clsAssetdeclaration_masterT2

            If mintAssetDeclarationT2Unkid > 0 Then
                objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
                If objAssetDeclare._IsAttestation = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Sorry! Annual Attestation of Asset declaration for this employee is already exist for selected financial year."), Me)
                    Exit Sub
                End If
            Else
                If objAssetDeclare.isExist(CInt(Session("Employeeunkid")), 0, 0, Nothing, mdtFinStartDate, mdtFinEndDate) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Sorry! Asset declaration for this employee is not exist for selected financial year."), Me)
                    Exit Sub
                End If
            End If

            If imgSignature.ImageUrl = "" Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, You cannot do Annual Attestation of Asset declaration now. Please set your signature in the system."), Me)
                Exit Sub
            End If

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmpAssetDeclaration"
            StrModuleName2 = "mnuAssetDeclaration"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")


            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            objAssetDeclare._IsAttestation = True
            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare._Loginemployeeunkid = Session("Employeeunkid")
                objAssetDeclare._Userunkid = -1
            Else
                objAssetDeclare._Userunkid = Session("UserId")
                objAssetDeclare._Loginemployeeunkid = -1
            End If


            objAssetDeclare._AttestationDate = ConfigParameter._Object._CurrentDateAndTime

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetDeclare._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetDeclare._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetDeclare._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._ClientIp = CStr(Session("IP_ADD"))
            objAssetDeclare._HostName = CStr(Session("HOST_NAME"))
            objAssetDeclare._FormName = mstrModuleName
            objAssetDeclare._IsFromWeb = True

            If objAssetDeclare.Update(enAction.EDIT_ONE) = False AndAlso objAssetDeclare._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetDeclare._Message, Me)
            Else
                'Hemant (20 Dec 2024) -- Start
                'ISSUE/ENHANCEMENT(NMB): A1X - 2905 :  Asset declaration form changes 
                objAssetDeclare.Send_Notification_Employee_For_Attestation(objAssetDeclare._Employeeunkid, eZeeDate.convertDate(Session("EmployeeAsOnDate")), enLogin_Mode.EMP_SELF_SERVICE, Session("Employeeunkid"), CInt(Session("UserId")))
                Send_Notification(objAssetDeclare._WebEmailList)
                'Hemant (20 Dec 2024) -- End
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "This Annual Attestation Acknowledged Successfully."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDetailHeader.ID, Me.lblDetailHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.txtHeader.ID, Me.txtHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.txtNoPara1.ID, Me.txtNoPara1.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.txtNoPara2.ID, Me.txtNoPara2.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.txtNoPara2I.ID, Me.txtNoPara2I.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.txtNoPara2II.ID, Me.txtNoPara2II.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.txtNoPara2III.ID, Me.txtNoPara2III.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.txtNoPara2IV.ID, Me.txtNoPara2IV.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.txtNoPara3.ID, Me.txtNoPara3.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.txtNoPara4.ID, Me.txtNoPara4.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.txtNoPara5.ID, Me.txtNoPara5.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.txtNoPara6.ID, Me.txtNoPara6.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.txtNoPara7.ID, Me.txtNoPara7.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmpName.ID, Me.lblEmpName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmpDeclareDate.ID, Me.lblEmpDeclareDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblempsign.ID, Me.lblempsign.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblnosign.ID, Me.lblnosign.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkconfirmSign.ID, Me.chkconfirmSign.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnConfirmSign.ID, Me.btnConfirmSign.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnAcknowledge.ID, Me.btnAcknowledge.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            lblDetailHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDetailHeader.ID, Me.lblDetailHeader.Text)
            txtHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtHeader.ID, Me.txtHeader.Text)
            txtNoPara1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtNoPara1.ID, Me.txtNoPara1.Text)
            txtNoPara2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtNoPara2.ID, Me.txtNoPara2.Text)
            txtNoPara2I.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtNoPara2I.ID, Me.txtNoPara2I.Text)
            txtNoPara2II.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtNoPara2II.ID, Me.txtNoPara2II.Text)
            txtNoPara2III.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtNoPara2III.ID, Me.txtNoPara2III.Text)
            txtNoPara2IV.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtNoPara2IV.ID, Me.txtNoPara2IV.Text)
            txtNoPara3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtNoPara3.ID, Me.txtNoPara3.Text)
            txtNoPara4.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtNoPara4.ID, Me.txtNoPara4.Text)
            txtNoPara5.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtNoPara5.ID, Me.txtNoPara5.Text)
            txtNoPara6.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtNoPara6.ID, Me.txtNoPara6.Text)
            txtNoPara7.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.txtNoPara7.ID, Me.txtNoPara7.Text)
            lblEmpName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpName.ID, Me.lblEmpName.Text)
            lblEmpDeclareDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpDeclareDate.ID, Me.lblEmpDeclareDate.Text)
            lblempsign.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblempsign.ID, Me.lblempsign.Text)
            lblnosign.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblnosign.ID, Me.lblnosign.Text)
            chkconfirmSign.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkconfirmSign.ID, Me.chkconfirmSign.Text)

            Me.btnConfirmSign.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnConfirmSign.ID, Me.btnConfirmSign.Text).Replace("&", "")
            Me.btnAcknowledge.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnAcknowledge.ID, Me.btnAcknowledge.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "I, undersigned, an employee of #CompanyName# acknowledges and agrees that:")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "I have honestly provided accuracy and complete information about my declaration of assets, liabilities, and interests.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "I understand that the under mentioned codes, and policies are applicable to me and that there is onus and responsibility on me to be aware of the contents thereof.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "#CompanyCode# Code of Conduct")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "#CompanyCode# Conflict of Interest Policy")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "#CompanyCode# Insider Trading Policy")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Tanzania Bankers Association (TBA) Code of Conduct")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "I undertake to abide by the above as well as amendments released from time to time.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "I acknowledge that I am fully aware of and understand the contents and implications of the above codes and policies.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "I understand that #CompanyName# may from time to time, amend or change the code of conduct, which amendments will be communicated by #CompanyName# to myself.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "I acknowledge that this attestation and recommitment to the code of conduct is a condition of my terms of employment with #CompanyName#.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "I undertake at all times to act within the spirit of the code of conduct.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Sorry, Employee information not found.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Sorry, Please confirm your signature first.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Sorry, You cannot do declaration before")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Sorry! Annual Attestation of Asset declaration for this employee is already exist for selected financial year.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Sorry! Asset declaration for this employee is not exist for selected financial year.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Sorry, You cannot do Annual Attestation of Asset declaration now. Please set your signature in the system.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "This Annual Attestation Acknowledged Successfully.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
