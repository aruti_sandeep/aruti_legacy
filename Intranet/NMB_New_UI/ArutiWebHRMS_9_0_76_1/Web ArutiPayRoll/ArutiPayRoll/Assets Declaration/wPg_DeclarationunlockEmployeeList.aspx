﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_DeclarationunlockEmployeeList.aspx.vb"
    Inherits="Assets_Declaration_wPg_AssetDecunlockEmployee" Title="Unlock Employee" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc2:Confirmation ID="popup_YesNo" Title="Confirmation" runat="server" Message="" />
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <%--<asp:Label ID="lblPageHeader2" runat="server" Text="Unlock Employee" CssClass="form-label"></asp:Label>--%>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Unlock Employee" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 350px;">
                                            <asp:Panel ID="pnl_dgView" runat="server" ScrollBars="Auto">
                                                <asp:GridView ID="GvLockEmployeeList" DataKeyNames="adlockunkid" runat="server" AutoGenerateColumns="False"
                                                    Width="99%" AllowPaging="false" HeaderStyle-Font-Bold="false" CssClass="table table-hover table-bordered"
                                                    RowStyle-Wrap="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-CssClass="headerstyle" ItemStyle-CssClass="itemstyle"
                                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                                                    Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged"
                                                                    Text=" " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Employee" HeaderText="Employee" HeaderStyle-Width="70%"
                                                            FooterText="colhEmployee"></asp:BoundField>
                                                        <asp:BoundField DataField="lockunlockdatetime" HeaderText="Lock DateTime" HeaderStyle-Width="25%"
                                                            FooterText="colhLockDateTime"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnUnlock" runat="server" CssClass="btn btn-primary" Text="Unlock" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
