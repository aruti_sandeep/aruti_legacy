﻿Option Strict On

#Region " Imports "
Imports System.Data
Imports Aruti.Data
Imports Microsoft.VisualBasic
Imports ArutiReports 'Nilay (21 Mar 2017)
#End Region

Partial Class Budget_Timesheet_wPg_EmployeeTimeSheetApproval
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmTimesheetApproval"

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
    Private ReadOnly mstrModuleName1 As String = "frmProjectHoursDetails"
    'Pinkal (28-Mar-2018) -- End

    Private DisplayMessage As New CommonCodes
    Private objTimesheetApproval As clstsemptimesheet_approval
    Private mintPeriodID As Integer = 0
    Private mdtApprovalTable As DataTable = Nothing
    Private mblnIsApproved As Boolean = True
    Private mdic As Dictionary(Of String, Integer)
    'Nilay (10 Jan 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
    Private mintPriority As Integer = 0
    Private mintLoginTypeID As Integer = -1
    Private mintLoginEmployeeID As Integer = -1
    Private mintUserID As Integer = -1
    'Nilay (10 Jan 2017) -- End

    'Nilay (27 Feb 2017) -- Start
    Private mblnIsFromEmailLink As Boolean = False
    Private mintApproverID As Integer = -1
    'Nilay (27 Feb 2017) -- End

    'Nilay (21 Mar 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing
    'Nilay (21 Mar 2017) -- End

    'Pinkal (16-May-2018) -- Start
    'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
    Private mdtPStartDate As Date = Nothing
    Private mdtPEndDate As Date = Nothing
    'Pinkal (16-May-2018) -- End

#End Region

#Region " Page's Events "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objTimesheetApproval = New clstsemptimesheet_approval
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            mdic = dgvEmpTimesheet.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvEmpTimesheet.Columns.IndexOf(x))

            If IsPostBack = False Then
                Call SetLanguage()

                'Nilay (27 Feb 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                'If Session("IsFromEmailLink") IsNot Nothing Then
                '    mblnIsFromEmailLink = CBool(Session("IsFromEmailLink"))
                '    Session("IsFromEmailLink") = Nothing
                'End If

                'If mblnIsFromEmailLink = True Then
                '    If Session("ApproverID") IsNot Nothing Then
                '        mintApproverID = CInt(Session("ApproverID"))
                '        Session("ApproverID") = Nothing
                '    End If
                'End If

                If Request.QueryString.Count > 0 Then
                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                    If arr.Length > 0 Then
                        mblnIsFromEmailLink = CBool(arr(0))
                        mintApproverID = CInt(arr(1))
                    End If
                End If
                'Nilay (27 Feb 2017) -- End

                If Session("ApprovalTable") IsNot Nothing Then
                    mdtApprovalTable = CType(Session("ApprovalTable"), DataTable)
                    'Nilay (27 Feb 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                    If mblnIsFromEmailLink = False Then
                        Session("ApprovalTable") = Nothing
                    End If
                    'Nilay (27 Feb 2017) -- End
                    'Session("ApprovalTable") = Nothing
                End If

                If Session("PeriodID") IsNot Nothing Then
                    mintPeriodID = CInt(Session("PeriodID"))
                    'Nilay (27 Feb 2017) -- Start
                    If mblnIsFromEmailLink = False Then
                        Session("PeriodID") = Nothing
                    End If
                    'Nilay (27 Feb 2017) -- End
                End If

                'Nilay (10 Jan 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
                If Session("Priority") IsNot Nothing Then
                    mintPriority = CInt(Session("Priority"))
                    'Nilay (27 Feb 2017) -- Start
                    If mblnIsFromEmailLink = False Then
                        Session("Priority") = Nothing
                    End If
                    'Nilay (27 Feb 2017) -- End
                End If

                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    mintLoginTypeID = enLogin_Mode.MGR_SELF_SERVICE
                    mintLoginEmployeeID = 0
                    mintUserID = CInt(Session("UserId"))
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    mintLoginTypeID = enLogin_Mode.EMP_SELF_SERVICE
                    mintLoginEmployeeID = CInt(Session("Employeeunkid"))
                    mintUserID = 0
                End If
                'Nilay (10 Jan 2017) -- End

                Call FillCombo()
                GetValue()

                'Nilay (15 Feb 2017) -- Start
                'txtRemarks.Focus()
                dgvEmpTimesheet.Focus()
                'Nilay (15 Feb 2017) -- End

            Else
                mintPeriodID = CInt(Me.ViewState("PeriodID"))
                mdtApprovalTable = CType(Me.ViewState("ApprovalTable"), DataTable)
                mblnIsApproved = CBool(Me.ViewState("IsApproved"))
                'Nilay (10 Jan 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
                mintPriority = CInt(Me.ViewState("Priority"))
                mintLoginTypeID = CInt(Me.ViewState("LoginTypeID"))
                mintLoginEmployeeID = CInt(Me.ViewState("LoginEmployeeID"))
                mintUserID = CInt(Me.ViewState("UserID"))
                'Nilay (10 Jan 2017) -- End

                'Nilay (27 Feb 2017) -- Start
                mblnIsFromEmailLink = CBool(Me.ViewState("IsFromEmailLink"))
                mintApproverID = CInt(Me.ViewState("ApproverID"))
                'Nilay (27 Feb 2017) -- End

                'Nilay (21 Mar 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                mdtPeriodStart = CDate(Me.ViewState("PeriodStartDate")).Date
                mdtPeriodEnd = CDate(Me.ViewState("PeriodEndDate")).Date
                'Nilay (21 Mar 2017) -- End

                'Pinkal (16-May-2018) -- Start
                'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
                mdtPStartDate = CDate(Me.ViewState("mdtPStartDate")).Date
                mdtPEndDate = CDate(Me.ViewState("mdtPEndDate")).Date
                'Pinkal (16-May-2018) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("PeriodID") = mintPeriodID
            Me.ViewState("ApprovalTable") = mdtApprovalTable
            Me.ViewState("IsApproved") = mblnIsApproved
            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            Me.ViewState("Priority") = mintPriority
            Me.ViewState("LoginTypeID") = mintLoginTypeID
            Me.ViewState("LoginEmployeeID") = mintLoginEmployeeID
            Me.ViewState("UserID") = mintUserID
            'Nilay (10 Jan 2017) -- End

            'Nilay (27 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            Me.ViewState("IsFromEmailLink") = mblnIsFromEmailLink
            Me.ViewState("ApproverID") = mintApproverID
            'Nilay (27 Feb 2017) -- End

            'Nilay (21 Mar 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            Me.ViewState("PeriodStartDate") = mdtPeriodStart
            Me.ViewState("PeriodEndDate") = mdtPeriodEnd
            'Nilay (21 Mar 2017) -- End

            'Pinkal (16-May-2018) -- Start
            'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
            Me.ViewState("mdtPStartDate") = mdtPStartDate
            Me.ViewState("mdtPEndDate") = mdtPEndDate
            'Pinkal (16-May-2018) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing

            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
                                               CDate(Session("fin_startdate")).Date, "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = ""
            End With
            objPeriod = Nothing
            dsList = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'Nilay (21 Mar 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = mintPeriodID

            'Pinkal (05-Jun-2017) -- Start
            'Enhancement - Implemented TnA Period for PSI Malawi AS Per Suzan's Request .
            'mdtPeriodStart = objPeriod._Start_Date.Date
            'mdtPeriodEnd = objPeriod._End_Date.Date
            mdtPeriodStart = objPeriod._TnA_StartDate.Date
            mdtPeriodEnd = objPeriod._TnA_EndDate.Date
            'Pinkal (05-Jun-2017) -- End

            'Pinkal (16-May-2018) -- Start
            'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
            mdtPStartDate = objPeriod._TnA_StartDate.Date
            mdtPEndDate = objPeriod._TnA_EndDate.Date
            'Pinkal (16-May-2018) -- End
            'Nilay (21 Mar 2017) -- End


            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            If Session("CompanyGroupName").ToString().ToUpper() = "PSI MALAWI" Then
                dgvEmpTimesheet.Columns(getColumnId_Datagrid(dgvEmpTimesheet, "dgcolhCostCenter", False, True)).Visible = True
            End If
            'Pinkal (06-Jan-2023) -- End

            cboPeriod.SelectedValue = mintPeriodID.ToString
            If mdtApprovalTable Is Nothing Then
                dgvEmpTimesheet.DataSource = New List(Of String)
                dgvEmpTimesheet.DataBind()
            ElseIf mdtApprovalTable.Rows.Count > 0 Then

                dgvEmpTimesheet.DataSource = mdtApprovalTable

                Dim strColumnArray() As String = {"dgcolhActivityDate"}
                Dim dCol = dgvEmpTimesheet.Columns.Cast(Of DataGridColumn).Where(Function(x) strColumnArray.Contains(x.FooterText))
                If dCol.Count > 0 Then
                    For Each dc As DataGridColumn In dCol
                        CType(dgvEmpTimesheet.Columns(dgvEmpTimesheet.Columns.IndexOf(dc)), BoundColumn).DataFormatString = "{0:" & Session("DateFormat").ToString & "}"
                    Next
                End If

                dgvEmpTimesheet.DataBind()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Setvalue()
        Try
            objTimesheetApproval = New clstsemptimesheet_approval

            objTimesheetApproval._Periodunkid = CInt(cboPeriod.SelectedValue)
            objTimesheetApproval._Approvaldate = ConfigParameter._Object._CurrentDateAndTime.Date
            objTimesheetApproval._ApprovalData = New DataView(mdtApprovalTable, "IsGrp = 0", "", DataViewRowState.CurrentRows).ToTable
            objTimesheetApproval._Statusunkid = CInt(IIf(mblnIsApproved, 1, 3))
            objTimesheetApproval._Visibleunkid = CInt(IIf(mblnIsApproved, 1, 3))
            objTimesheetApproval._Description = txtRemarks.Text.Trim
            objTimesheetApproval._Userunkid = CInt(Session("UserId"))


            'Pinkal (13-Apr-2017) -- Start
            'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
            objTimesheetApproval._WebFormName = mstrModuleName
            objTimesheetApproval._WebClientIP = CStr(Session("IP_ADD"))
            objTimesheetApproval._WebHostName = CStr(Session("HOST_NAME"))
            'Pinkal (13-Apr-2017) -- End



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Setvalue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (07 Feb 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
    'Private Sub CalculateActivityHours(ByVal intHours As Integer, ByVal intMinutes As Integer, ByVal item As DataGridItem)
    '    Try
    '        Dim intHoursInMin As Integer = 0
    '        Dim intTotHoursInMin As Integer = 0
    '        Dim decActivityHours As Decimal = 0

    '        If intHours <= 0 AndAlso intMinutes <= 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Hours is compulsory information.Please enter activity hours."), Me)
    '            'mdtApprovalTable.Rows(item.ItemIndex).Item("activity_hrs") = intHours & ":" & intMinutes
    '            mdtApprovalTable.Rows(item.ItemIndex).Item("activity_hrsinMins") = intHours * 60 + intMinutes
    '            mdtApprovalTable.AcceptChanges()
    '            Exit Sub
    '        End If

    '        If intHours > 0 Then

    '            Dim strActivityHours As String = CStr(mdtApprovalTable.Rows(item.ItemIndex).Item("activity_hrs"))
    '            Dim strHours As String = strActivityHours.Trim.Substring(0, strActivityHours.Trim.IndexOf(":"))
    '            Dim strMinutes As String = strActivityHours.Trim.Substring(strActivityHours.Trim.IndexOf(":") + 1)

    '            intHoursInMin = intHours * 60 + intMinutes
    '            'mdtApprovalTable.Rows(item.ItemIndex).Item("activity_hrsinMins") = intHoursInMin
    '            'mdtApprovalTable.AcceptChanges()
    '            'intTotHoursInMin = CInt(mdtApprovalTable.Compute("SUM(activity_hrsinMins)", "budgetunkid <> -999"))
    '            'decActivityHours = CDec(CalculateTime(True, intTotHoursInMin * 60))
    '            decActivityHours = CDec(CalculateTime(True, intHoursInMin * 60))

    '            'Dim strActivityHours As String = item.Cells(mdic("dgcolhActivityHours")).Text
    '            'Dim strHours As String = strActivityHours.Trim.Substring(0, strActivityHours.Trim.IndexOf(":"))
    '            'Dim strMinutes As String = strActivityHours.Trim.Substring(strActivityHours.Trim.IndexOf(":") + 1)

    '            Dim objBudget As New clsBudget_MasterNew
    '            Dim intDefaultBudgetID As Integer = 0
    '            Dim dsDefaultBudgetID As DataSet = objBudget.GetComboList("List", False, True, "", 0)
    '            If dsDefaultBudgetID IsNot Nothing AndAlso dsDefaultBudgetID.Tables(0).Rows.Count > 0 Then
    '                intDefaultBudgetID = CInt(dsDefaultBudgetID.Tables(0).Rows(0)("budgetunkid"))
    '            End If

    '            Dim strAssignedHrs As String = ""
    '            Dim objBudgetCodes As New clsBudgetcodes_master
    '            Dim dsList As DataSet = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPeriod.SelectedValue), intDefaultBudgetID, _
    '                                                                                 CInt(mdtApprovalTable.Rows(item.ItemIndex).Item("activityunkid")), _
    '                                                                                 CInt(mdtApprovalTable.Rows(item.ItemIndex).Item("employeeunkid")))

    '            Dim objEmpshift As New clsEmployee_Shift_Tran
    '            Dim intShiftID As Integer = objEmpshift.GetEmployee_Current_ShiftId(CDate(mdtApprovalTable.Rows(item.ItemIndex).Item("activitydate")).Date, _
    '                                                                                 CInt(mdtApprovalTable.Rows(item.ItemIndex).Item("employeeunkid")))
    '            objEmpshift = Nothing

    '            Dim objShiftTran As New clsshift_tran

    '            objShiftTran.GetShiftTran(intShiftID)

    '            Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(CDate(mdtApprovalTable.Rows(item.ItemIndex).Item("activitydate")).Date, FirstDayOfWeek.Sunday) - 1)

    '            If drRow.Length > 0 AndAlso (dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0) Then

    '                'Dim mstrWorkingHrs As String = txtHours.Text
    '                'Dim mintHours As Integer = CInt(mstrWorkingHrs.Trim.Substring(0, mstrWorkingHrs.Trim.IndexOf(":")))
    '                'Dim mintMins As Integer = CInt(mstrWorkingHrs.Trim.Substring(mstrWorkingHrs.Trim.IndexOf(":") + 1))
    '                'mintActivityWorkingHrs = (mintHours * 60) + mintMins

    '                Dim intAssignShiftHrs As Integer = CInt(CInt(CInt(CInt(drRow(0)("workinghrsinsec")) * CDec(mdtApprovalTable.Rows(item.ItemIndex).Item("percentage"))) / 100) / 60)

    '                If CBool(Session("AllowOverTimeToEmpTimesheet")) = False Then
    '                    If intAssignShiftHrs < intHoursInMin Then
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "This employee does not allow to do this activity more than define activity percentage."), Me)
    '                        CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = strHours
    '                        CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = strMinutes
    '                        mdtApprovalTable.Rows(item.ItemIndex).Item("activity_hrsinMins") = CInt(strHours) * 60 + CInt(strMinutes)
    '                        Exit Sub
    '                    End If
    '                End If
    '            End If

    '            'If CBool(Session("AllowOverTimeToEmpTimesheet")) = False Then
    '            '    If CInt(mdtApprovalTable.Rows(item.ItemIndex).Item("AssignedActivityHrsInMin")) < intHoursInMin Then
    '            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "This employee does not allow to do this activity more than define activity percentage."), Me)
    '            '        CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = strHours
    '            '        CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = strMinutes
    '            '        Exit Sub
    '            '    End If
    '            'End If

    '            If CBool(Session("AllowOverTimeToEmpTimesheet")) = True Then
    '                If intHoursInMin >= (24 * 60) Then
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Invalid working hrs.Please enter valid working hrs."), Me)
    '                End If
    '            End If

    '            mdtApprovalTable.Rows(item.ItemIndex).Item("activity_hrs") = intHours.ToString("#00") & ":" & intMinutes.ToString("#00")
    '            mdtApprovalTable.Rows(item.ItemIndex).Item("activity_hrsinMins") = intHoursInMin

    '            dgvEmpTimesheet.DataSource = mdtApprovalTable
    '            dgvEmpTimesheet.DataBind()
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "CalculateActivityHours:- ", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub CalculateActivityHours(ByVal intHours As Integer, ByVal intMinutes As Integer, ByVal item As DataGridItem)
        Try
            Dim intActivityWorkingHrsInMin As Integer = 0
            Dim intTotHoursInMin As Integer = 0

            Dim txtHours As TextBox = CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("txtHours"), TextBox)
            Dim txtMinutes As TextBox = CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("txtMinutes"), TextBox)

            Dim strActivityHours As String = item.Cells(mdic("dgcolhHours")).Text
            Dim strHours As String = strActivityHours.Trim.Substring(0, strActivityHours.Trim.IndexOf(":"))
            Dim strMinutes As String = strActivityHours.Trim.Substring(strActivityHours.Trim.IndexOf(":") + 1)

            If intHours > 23 Then
                DisplayMessage.DisplayMessage("Invalid Activity Hours. Reason: Activity Hours cannot be greater than or equal to 24.", Me)
                txtHours.Text = strHours
                txtHours.Focus()
                Exit Sub
            ElseIf intMinutes > 59 Then
                DisplayMessage.DisplayMessage("Invalid Activity Minutes. Reason: Activity Minutes cannot be greater than or equal to 60.", Me)
                txtMinutes.Text = strMinutes
                txtMinutes.Focus()
                Exit Sub
            End If

            intActivityWorkingHrsInMin = intHours * 60 + intMinutes

            Dim objBudget As New clsBudget_MasterNew
            Dim intDefaultBudgetID As Integer = 0
            Dim dsDefaultBudgetID As DataSet = objBudget.GetComboList("List", False, True, "", 0)
            If dsDefaultBudgetID IsNot Nothing AndAlso dsDefaultBudgetID.Tables(0).Rows.Count > 0 Then
                intDefaultBudgetID = CInt(dsDefaultBudgetID.Tables(0).Rows(0)("budgetunkid"))
            End If

            Dim strAssignedHrs As String = ""
            Dim objBudgetCodes As New clsBudgetcodes_master
            Dim dsList As DataSet = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPeriod.SelectedValue), intDefaultBudgetID, _
                                                                                 CInt(mdtApprovalTable.Rows(item.ItemIndex).Item("activityunkid")), _
                                                                                 CInt(mdtApprovalTable.Rows(item.ItemIndex).Item("employeeunkid")))

            Dim strTotalActivityHours As String = CStr(item.Cells(mdic("dgcolhTotalActivityHours")).Text)
            Dim intEmployeeID As Integer = CInt(item.Cells(mdic("objdgcolhEmployeeID")).Text)
            Dim strdtActivityDate As String = eZeeDate.convertDate(CDate(item.Cells(mdic("dgcolhActivityDate")).Text).Date)
            Dim intOldActivityWorkingHrsInMin As Integer = CInt(item.Cells(mdic("objdgcolhActivityHrsInMins")).Text)
            Dim intTotalActivityHoursInMin As Integer = CInt(item.Cells(mdic("objdgcolhTotalActivityHoursInMin")).Text)

            Dim intComputeActivityHoursInMin As Integer = intTotalActivityHoursInMin - intOldActivityWorkingHrsInMin + intActivityWorkingHrsInMin

            If CBool(Session("AllowToExceedTimeAssignedToActivity")) = True Then
                If CBool(Session("AllowOverTimeToEmpTimesheet")) = False Then
                    If CInt(CInt(item.Cells(mdic("objdgcolhShiftHoursInSec")).Text) / 60) < intActivityWorkingHrsInMin Then 'workinghrsinsec=shifthours
                        'item.Cells(mdic("dgcolhTotalActivityHours")).Text = CalculateTime(True, intComputeActivityHoursInMin * 60).ToString("#00.00").Replace(".", ":")
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "This employee does not allow to do this activity hours more than define shift hours."), Me)
                        CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("txtHours"), TextBox).Text = strHours
                        CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("txtMinutes"), TextBox).Text = strMinutes
                        'item.Cells(mdic("dgcolhHours")).Text = CalculateTime(True, intOldActivityWorkingHrsInMin * 60).ToString("#00.00").Replace(".", ":")
                        Exit Sub
                    ElseIf intComputeActivityHoursInMin > CInt(CInt(item.Cells(mdic("objdgcolhShiftHoursInSec")).Text) / 60) Then
                        'item.Cells(mdic("dgcolhTotalActivityHours")).Text = CalculateTime(True, intComputeActivityHoursInMin * 60).ToString("#00.00").Replace(".", ":")
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Total activity hours cannot be greater than shift hours."), Me)
                        CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("txtHours"), TextBox).Text = strHours
                        CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("txtMinutes"), TextBox).Text = strMinutes
                        'item.Cells(mdic("dgcolhHours")).Text = CalculateTime(True, intOldActivityWorkingHrsInMin * 60).ToString("#00.00").Replace(".", ":")
                        Exit Sub
                    End If
                ElseIf CBool(Session("AllowOverTimeToEmpTimesheet")) = True Then
                    If intComputeActivityHoursInMin >= (24 * 60) Then
                        'item.Cells(mdic("dgcolhTotalActivityHours")).Text = CalculateTime(True, intComputeActivityHoursInMin * 60).ToString("#0.00").Replace(".", ":")
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Invalid Total Activity hours. Reason: Total Activity hours cannot greater than or equal to 24. "), Me)
                        CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("txtHours"), TextBox).Text = strHours
                        CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("txtMinutes"), TextBox).Text = strMinutes
                        'item.Cells(mdic("dgcolhHours")).Text = CalculateTime(True, intOldActivityWorkingHrsInMin * 60).ToString("#00.00").Replace(".", ":")
                        Exit Sub
                    End If
                End If
            Else
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim intAssignedShiftHrs As Integer = CInt(CInt(CInt(CInt(item.Cells(mdic("objdgcolhShiftHoursInSec")).Text) * CDec(item.Cells(mdic("objdgcolhActivityPercentage")).Text)) / 100) / 60)
                    If intAssignedShiftHrs < intActivityWorkingHrsInMin Then
                        'item.Cells(mdic("dgcolhTotalActivityHours")).Text = CalculateTime(True, intComputeActivityHoursInMin * 60).ToString("#00.00").Replace(".", ":")
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "This employee does not allow to do this activity more than define activity percentage."), Me)
                        CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("txtHours"), TextBox).Text = strHours
                        CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("txtMinutes"), TextBox).Text = strMinutes
                        'item.Cells(mdic("dgcolhHours")).Text = CalculateTime(True, intOldActivityWorkingHrsInMin * 60).ToString("#00.00").Replace(".", ":")
                        Exit Sub
                    End If
                End If
            End If

            mdtApprovalTable.Rows(item.ItemIndex).Item("activity_hrs") = intHours.ToString("#00") & ":" & intMinutes.ToString("#00")
            mdtApprovalTable.Rows(item.ItemIndex).Item("activity_hrsinMins") = intActivityWorkingHrsInMin
            'mdtApprovalTable.Rows(item.ItemIndex).Item("TotalActivityHoursInMin") = intComputeActivityHoursInMin
            'mdtApprovalTable.Rows(item.ItemIndex).Item("TotalActivityHours") = CalculateTime(True, intComputeActivityHoursInMin * 60).ToString("#00.00").Replace(".", ":")

            Dim lstRow As List(Of DataRow) = (From p In mdtApprovalTable Where (CBool(p.Item("IsGrp")) = False AndAlso CInt(p.Item("employeeunkid")) = intEmployeeID _
                                                                                AndAlso CDate(p.Item("activitydate")).Date = CDate(item.Cells(mdic("dgcolhActivityDate")).Text).Date) Select p).ToList
            For Each dsRow As DataRow In lstRow
                dsRow.Item("TotalActivityHours") = CalculateTime(True, intComputeActivityHoursInMin * 60).ToString("#00.00").Replace(".", ":")
                dsRow.Item("TotalActivityHoursInMin") = intComputeActivityHoursInMin
            Next
            mdtApprovalTable.AcceptChanges()

            dgvEmpTimesheet.DataSource = mdtApprovalTable
            dgvEmpTimesheet.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "CalculateActivityHours:- ", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (07 Feb 2017) -- End



    Private Function IsValidate() As Boolean
        Try
            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'If mblnIsApproved = False Then
            '    If txtRemarks.Text.Trim.Length <= 0 Then
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Remark cannot be blank. Remark is required information."), Me)
            '        txtRemarks.Focus()
            '        Return False
            '    End If
            'End If
            'Nilay (01 Apr 2017) -- End

            Dim dRow() As DataRow = mdtApprovalTable.Select("timesheetapprovalunkid > 0 AND activity_hrsinMins <=0")
            If dRow.Length > 0 Then
                DisplayMessage.DisplayMessage("Activity Hours is compulsory information.Please enter Activity Hours.", Me)
                Return False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

#End Region

#Region " Button's Events "

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            Dim objBudgetTimesheet As New clsBudgetEmp_timesheet
            Dim objPeriod As New clscommom_period_Tran
            Dim blnFlag As Boolean

            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)

            If CBool(Session("NotAllowIncompleteTimesheet")) = True Then


                'Pinkal (02-Jul-2018) -- Start
                'Enhancement - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.

                'blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtApprovalTable, CInt(objPeriod._Constant_Days), _
                '                                                          objPeriod._Start_Date.Date, objPeriod._End_Date.Date, _
                '                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.APPROVED, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()))


                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                'blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtApprovalTable, CInt(objPeriod._Constant_Days), _
                '                                                       objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                '                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.APPROVED, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()))

                blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtApprovalTable, CInt(objPeriod._Constant_Days), _
                                                                       objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                                                                                                            clsBudgetEmp_timesheet.enBudgetTimesheetStatus.APPROVED, _
                                                                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                                                            CBool(Session("AllowOverTimeToEmpTimesheet")))

                'Pinkal (28-Jul-2018) -- End


                'Pinkal (02-Jul-2018) -- End



                If objBudgetTimesheet._Message <> "" Then
                    DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                    Exit Sub
                Else
                    If blnFlag = False Then
                        'Pinkal (31-Oct-2017) -- Start
                        'Enhancement - Solve Budget Timesheet Issue As Per Suzan's Comment.
                        'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot perform Approve operation. Reason : On Configuration --> Options --> Payroll Settings --> 'Don't allow incomplete timesheet to Submit For Apprval/Approve/Reject/Cancel' is alraeady set."), Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot perform Approve operation. Reason : your timesheet is incomplete, please complete it."), Me)
                        'Pinkal (31-Oct-2017) -- End
                        Exit Sub
                    End If
                End If
            End If
            'Nilay (01 Apr 2017) -- End

            mblnIsApproved = True
            If IsValidate() = False Then Exit Sub

            Setvalue()

            If objTimesheetApproval.UpdateGlobalApproval(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), _
                                                         CInt(Session("CompanyUnkId")), CBool(Session("IsIncludeInactiveEmp"))) = False Then

                DisplayMessage.DisplayMessage(objTimesheetApproval._Message, Me)
                Exit Sub
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Employee timesheet approved successfully."), Me)

                'Nilay (10 Jan 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification

                'Nilay (01 Apr 2017) -- Start
                'Dim objBudgetEmpTs As New clsBudgetEmp_timesheet
                'Nilay (01 Apr 2017) -- End

                Dim strEmployeeIDs As String = String.Join(",", mdtApprovalTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)

                'Nilay (27 Feb 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                'objBudgetEmpTs.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                '                                         CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                         CBool(Session("IsIncludeInactiveEmp")), strEmployeeIDs, "", CInt(cboPeriod.SelectedValue), _
                '                                         Nothing, True, mintPriority, mintLoginTypeID, mintLoginEmployeeID, mintUserID, _
                '                                         CStr(Session("ArutiSelfServiceURL")))

                'Nilay (21 Mar 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                'objBudgetEmpTs.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                '                                         CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                         CBool(Session("IsIncludeInactiveEmp")), strEmployeeIDs, "", CInt(cboPeriod.SelectedValue), _
                '                                         mintPriority, mintLoginTypeID, mintLoginEmployeeID, mintUserID, _
                '                                         CStr(Session("ArutiSelfServiceURL")))
                objBudgetTimesheet.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                                                              CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                              CBool(Session("IsIncludeInactiveEmp")), strEmployeeIDs, CInt(cboPeriod.SelectedValue), _
                                                              mintPriority, mdtApprovalTable, mintLoginTypeID, mintLoginEmployeeID, mintUserID, _
                                                              CStr(Session("ArutiSelfServiceURL")), True)
                'Nilay (01 Apr 2017) -- [True]
                'Nilay (21 Mar 2017) -- End
                'Nilay (27 Feb 2017) -- End

                'Nilay (21 Mar 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                Dim objEmployee As New clsEmployee_Master
                Dim dsEmp As DataSet = Nothing
                Dim strFilter As String = "hremployee_master.employeeunkid IN(" & strEmployeeIDs & ")"
                dsEmp = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), mdtPeriodStart, mdtPeriodEnd, CStr(Session("UserAccessModeSetting")), _
                                                    True, CBool(Session("IsIncludeInactiveEmp")), "Emp", , , , , , , , , , , , , , , , strFilter)
                'Nilay (21 Mar 2017) -- End

                For Each strEmpID As String In strEmployeeIDs.Split(CChar(","))
                    Dim intEmployeeID As Integer = CInt(strEmpID)
                    Dim dtApproverList As DataTable = Nothing
                    Dim objApprover As New clstsapprover_master

                    dtApproverList = objApprover.GetEmployeeApprover(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                                                                     CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                     CBool(Session("IsIncludeInactiveEmp")), -1, strEmpID, , Nothing, , _
                                                                     "priority > " & mintPriority & " ")

                    If dtApproverList Is Nothing OrElse dtApproverList.Rows.Count > 0 Then Continue For

                    Dim strEmpTimesheetIDs As String = String.Join(",", mdtApprovalTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = intEmployeeID _
                                                                                                                  AndAlso x.Field(Of Boolean)("IsGrp") = False) _
                                                                   .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)

                    'Nilay (21 Mar 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                    Dim objEmpTimesheetReport As New clsEmpBudgetTimesheetReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

                    Dim strExportPath As String = My.Computer.FileSystem.SpecialDirectories.Temp


                    'Pinkal (22-Jul-2019) -- Start
                    'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
                    'objEmpTimesheetReport._EmployeeID = intEmployeeID
                    'Dim dRow() As DataRow = dsEmp.Tables("Emp").Select("employeeunkid=" & intEmployeeID)
                    'If dRow.Length > 0 Then
                    '    objEmpTimesheetReport._EmployeeName = dRow(0).Item("employeename").ToString
                    '    objEmpTimesheetReport._EmployeeCode = dRow(0).Item("employeecode").ToString
                    'End If
                    Dim dRow() As DataRow = dsEmp.Tables("Emp").Select("employeeunkid=" & intEmployeeID)
                    If dRow.Length > 0 Then
                        objEmpTimesheetReport._dtEmployee = dRow.ToList().CopyToDataTable()
                    End If
                    'Pinkal (22-Jul-2019) -- End

                    objEmpTimesheetReport._PeriodId = CInt(cboPeriod.SelectedValue)

                    'Pinkal (04-May-2018) -- Start
                    'Enhancement - Budget timesheet Enhancement for MST/THPS.
                    'objEmpTimesheetReport._PeriodName = cboPeriod.Text
                    objEmpTimesheetReport._PeriodName = cboPeriod.SelectedItem.Text
                    'Pinkal (04-May-2018) -- End
                    objEmpTimesheetReport._StartDate = mdtPeriodStart
                    objEmpTimesheetReport._EndDate = mdtPeriodEnd
                    objEmpTimesheetReport._ViewReportInHTML = True
                    objEmpTimesheetReport._OpenAfterExport = False
                    objEmpTimesheetReport._ExportReportPath = strExportPath

                    objEmpTimesheetReport.Generate_DetailReport(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString, _
                                                                CStr(Session("UserAccessModeSetting")), True, True)

                    If objEmpTimesheetReport._FileNameAfterExported <> "" Then

                        objBudgetTimesheet.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                                                                      CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                      CBool(Session("IsIncludeInactiveEmp")), CStr(intEmployeeID), strEmpTimesheetIDs, _
                                                                      mintPeriodID, clsBudgetEmp_timesheet.enBudgetTimesheetStatus.APPROVED, mintLoginTypeID, _
                                                                      mintLoginEmployeeID, mintUserID, CStr(Session("ArutiSelfServiceURL")), , _
                                                                      strExportPath, objEmpTimesheetReport._FileNameAfterExported, True)
                        'Nilay (01 Apr 2017) -- [CStr(intEmployeeID), True]
                    End If
                    'Nilay (21 Mar 2017) -- End
                Next
                'Nilay (10 Jan 2017) -- End

                'Nilay (01 Apr 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objBudgetTimesheet.StartSendingEmail()
                objBudgetTimesheet.StartSendingEmail(CInt(Session("CompanyUnkId")))
                'Sohail (30 Nov 2017) -- End
                'Nilay (01 Apr 2017) -- End

                'Nilay (27 Feb 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                If mblnIsFromEmailLink = True Then

                    'Pinkal (23-Feb-2024) -- Start
                    '(A1X-2461) NMB : R&D - Force manual user login on approval links..

                    Session.Clear()
                    Session.Abandon()
                    Session.RemoveAll()

                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                        Response.Cookies("ASP.NET_SessionId").Value = ""
                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                    End If

                    If Request.Cookies("AuthToken") IsNot Nothing Then
                        Response.Cookies("AuthToken").Value = ""
                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                    End If

                    'Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
                    Response.Redirect("~/Index.aspx", False)
                    'Pinkal (23-Feb-2024) -- End

                Else
                    Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_EmployeeTimeSheetApprovalList.aspx", False)
                End If
                'Nilay (27 Feb 2017) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnApprove_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            mblnIsApproved = False

            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            If txtRemarks.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Remark cannot be blank. Remark is required information."), Me)
                txtRemarks.Focus()
                Exit Sub
            End If

            Dim objBudgetTimesheet As New clsBudgetEmp_timesheet
            Dim objPeriod As New clscommom_period_Tran
            Dim blnFlag As Boolean

            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)

            If CBool(Session("NotAllowIncompleteTimesheet")) = True Then

                'Pinkal (02-Jul-2018) -- Start
                'Enhancement - Allow to include newly hired employees on budget codes screen and allow to map employees with planned employees.

                'blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtApprovalTable, CInt(objPeriod._Constant_Days), _
                '                                                          objPeriod._Start_Date.Date, objPeriod._End_Date.Date, _
                '                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.REJECTED, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()))


                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                'blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtApprovalTable, CInt(objPeriod._Constant_Days), _
                '                                                       objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                '                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.REJECTED, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()))

                blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtApprovalTable, CInt(objPeriod._Constant_Days), _
                                                                       objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                                                                                                           clsBudgetEmp_timesheet.enBudgetTimesheetStatus.REJECTED, _
                                                                                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                                                           CBool(Session("AllowOverTimeToEmpTimesheet")))

                'Pinkal (28-Jul-2018) -- End

                'Pinkal (02-Jul-2018) -- End


                If objBudgetTimesheet._Message <> "" Then
                    DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                    Exit Sub
                Else
                    If blnFlag = False Then
                        'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot perform Reject operation. Reason : On Configuration --> Options --> Payroll Settings --> 'Don't allow incomplete timesheet to Submit For Apprval/Approve/Reject/Cancel' is alraeady set."), Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot perform Reject operation. Reason : your timesheet is incomplete, please complete it."), Me)
                        Exit Sub
                    End If
                End If
            End If
            'Nilay (01 Apr 2017) -- End

            If IsValidate() = False Then Exit Sub

            Setvalue()

            If objTimesheetApproval.UpdateGlobalApproval(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), _
                                                         CInt(Session("CompanyUnkId")), CBool(Session("IsIncludeInactiveEmp"))) = False Then

                DisplayMessage.DisplayMessage(objTimesheetApproval._Message, Me)
                Exit Sub
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Employee timesheet rejected successfully."), Me)

                'Nilay (10 Jan 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification

                'Nilay (01 Apr 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                'Dim objBudgetEmpTs As New clsBudgetEmp_timesheet
                'Dim strEmployeeIDs As String = String.Join(",", mdtApprovalTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)
                'For Each strID As String In strEmployeeIDs.Split(CChar(","))
                '    Dim intEmployeeID As Integer = CInt(strID)
                '    Dim strEmpTimesheetIDs As String = String.Join(",", mdtApprovalTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = intEmployeeID) _
                '                                                   .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)

                '    objBudgetEmpTs.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                '                                              CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                              CBool(Session("IsIncludeInactiveEmp")), intEmployeeID, strEmpTimesheetIDs, mintPeriodID, _
                '                                              clsBudgetEmp_timesheet.enBudgetTimesheetStatus.REJECTED, mintLoginTypeID, mintLoginEmployeeID, _
                '                                              mintUserID, CStr(Session("ArutiSelfServiceURL")), txtRemarks.Text)

                Dim strEmployeeIDs As String = String.Join(",", mdtApprovalTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)
                Dim strEmpTimesheetIDs As String = String.Join(",", mdtApprovalTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False) _
                                                               .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)

                objBudgetTimesheet.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                                                              CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                              CBool(Session("IsIncludeInactiveEmp")), strEmployeeIDs, strEmpTimesheetIDs, mintPeriodID, _
                                                              clsBudgetEmp_timesheet.enBudgetTimesheetStatus.REJECTED, mintLoginTypeID, mintLoginEmployeeID, _
                                                              mintUserID, CStr(Session("ArutiSelfServiceURL")), txtRemarks.Text, , , False)
                'Next

                'Nilay (01 Apr 2017) -- End

                'Nilay (10 Jan 2017) -- End

                'Nilay (27 Feb 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                If mblnIsFromEmailLink = True Then

                    'Pinkal (23-Feb-2024) -- Start
                    '(A1X-2461) NMB : R&D - Force manual user login on approval links..

                    Session.Clear()
                    Session.Abandon()
                    Session.RemoveAll()

                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                        Response.Cookies("ASP.NET_SessionId").Value = ""
                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                    End If

                    If Request.Cookies("AuthToken") IsNot Nothing Then
                        Response.Cookies("AuthToken").Value = ""
                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                    End If

                    'Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
                    Response.Redirect("~/Index.aspx", False)
                    'Pinkal (23-Feb-2024) -- End

                Else
                    Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_EmployeeTimeSheetApprovalList.aspx", False)
                End If
                'Nilay (27 Feb 2017) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReject_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Nilay (27 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            If mblnIsFromEmailLink = True Then
                Dim strQueryString As String = HttpUtility.UrlEncode(clsCrypto.Encrypt(cboPeriod.SelectedValue.ToString & _
                                                                                       "|" & mintApproverID.ToString & _
                                                                                       "|" & Session("CompanyUnkId").ToString & _
                                                                                       "|" & Session("UserId").ToString))

                Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_EmployeeTimeSheetApprovalList.aspx?" & strQueryString, False)
            Else
                Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_EmployeeTimeSheetApprovalList.aspx", False)
            End If
            'Nilay (27 Feb 2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " DataGrid's Events "

    Protected Sub dgvEmpTimesheet_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvEmpTimesheet.ItemDataBound
        Try
            Call SetDateFormat()

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            dgvEmpTimesheet.Columns(0).Visible = CBool(Session("ShowBgTimesheetActivityHrsDetail"))
            dgvEmpTimesheet.Columns(mdic("dgcolhProject")).Visible = CBool(Session("ShowBgTimesheetActivityProject"))
            dgvEmpTimesheet.Columns(mdic("dgcolhActivity")).Visible = CBool(Session("ShowBgTimesheetActivity"))
            'Pinkal (28-Jul-2018) -- End


            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then

                Dim intVisible = dgvEmpTimesheet.Columns.Cast(Of DataGridColumn).Where(Function(x) x.Visible = True).Count

                If e.Item.ItemIndex >= 0 Then
                    If CBool(e.Item.Cells(mdic("objdgcolhIsGrp")).Text) = True Then

                        'Pinkal (28-Mar-2018) -- Start
                        'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                        CType(e.Item.Cells(mdic("objdgcolhShowDetails")).FindControl("imgShowDetails"), ImageButton).Visible = False
                        'Pinkal (28-Mar-2018) -- End

                        e.Item.Cells(mdic("dgcolhParticular")).ColumnSpan = intVisible
                        For i = mdic("dgcolhParticular") + 1 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).Visible = False
                        Next

                        'Gajanan [17-Sep-2020] -- Start
                        'New UI Change
                        'e.Item.Cells(mdic("dgcolhParticular")).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(mdic("dgcolhParticular")).CssClass = "group-header"
                        'Gajanan [17-Sep-2020] -- End

                        e.Item.Cells(mdic("dgcolhParticular")).Style.Add("text-align", "left")

                        'Pinkal (28-Mar-2018) -- Start
                        'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

                        'Gajanan [17-Sep-2020] -- Start
                        'New UI Change
                        'e.Item.Cells(mdic("objdgcolhShowDetails")).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(mdic("objdgcolhShowDetails")).CssClass = "group-header"
                        'Gajanan [17-Sep-2020] -- End

                        e.Item.Cells(mdic("objdgcolhShowDetails")).Style.Add("text-align", "left")
                        'Pinkal (28-Mar-2018) -- End

                    Else
                        Dim mstrHours As String = e.Item.Cells(mdic("dgcolhHours")).Text
                        If mstrHours.Trim.Length > 0 Then
                            CType(e.Item.Cells(mdic("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = mstrHours.Trim.Substring(0, mstrHours.Trim.IndexOf(":"))
                            CType(e.Item.Cells(mdic("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = mstrHours.Trim.Substring(mstrHours.Trim.IndexOf(":") + 1)
                        Else
                            CType(e.Item.Cells(mdic("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = "00"
                            CType(e.Item.Cells(mdic("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = "00"
                        End If

                        'Pinkal (13-Aug-2018) -- Start
                        'Enhancement - Changes For PACT [Ref #249,252]
                        If CBool(e.Item.Cells(mdic("objdgcolhIsHoliday")).Text) Then
                            For i As Integer = 0 To e.Item.Cells.Count - 1
                                e.Item.Cells(i).Style.Add("color", "red")
                            Next
                        End If
                        'Pinkal (13-Aug-2018) -- End

                    End If
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvEmpTimesheet_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

    Protected Sub dgvEmpTimesheet_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvEmpTimesheet.ItemCommand
        Try
            'Pinkal (16-May-2018) -- Start
            'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
            'GetProjectDetails(mdtPeriodStart.Date, mdtPeriodEnd.Date, CInt(e.Item.Cells(mdic("objdgcolhEmployeeID")).Text), e.Item.Cells(mdic("objdgcolhEmployeeName")).Text, e.Item.Cells(mdic("dgcolhProject")).Text _
            '                        , e.Item.Cells(mdic("dgcolhDonor")).Text, CInt(e.Item.Cells(mdic("objdgcolhActivityID")).Text), e.Item.Cells(mdic("dgcolhActivity")).Text _
            '                        , CDbl(e.Item.Cells(mdic("objdgcolhActivityPercentage")).Text))
            GetProjectDetails(mdtPeriodStart.Date, mdtPeriodEnd.Date, CInt(e.Item.Cells(mdic("objdgcolhEmployeeID")).Text), e.Item.Cells(mdic("objdgcolhEmployeeName")).Text, e.Item.Cells(mdic("dgcolhProject")).Text _
                                    , e.Item.Cells(mdic("dgcolhDonor")).Text, CInt(e.Item.Cells(mdic("objdgcolhActivityID")).Text), e.Item.Cells(mdic("dgcolhActivity")).Text _
                                  , CDbl(e.Item.Cells(mdic("objdgcolhActivityPercentage")).Text), mdtPStartDate, mdtPEndDate)
            'Pinkal (16-May-2018) -- End
            popupShowDetails.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvEmpTimesheet_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (28-Mar-2018) -- End

#End Region

#Region " TextBox's Events "
    Protected Sub txtHours_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtHours As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtHours.NamingContainer, DataGridItem)

            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
            'If CInt(txtHours.Text) > 23 Then
            '    DisplayMessage.DisplayMessage("Invalid Activity Hours. Reason: Activity Hours cannot be greater than or equal to 24.", Me)
            '    txtHours.Text = "00"
            '    txtHours.Focus()
            '    Exit Sub
            'ElseIf txtHours.Text.Trim.Length <= 0 Then
            '    txtHours.Text = "00"
            '    txtHours.Focus()
            '    Exit Sub
            'Else
            '    Call CalculateActivityHours(CInt(txtHours.Text), CInt(CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text), item)
            'End If
            If txtHours.Text.Trim.Length <= 0 Then
                txtHours.Text = "00"
            End If
            Call CalculateActivityHours(CInt(txtHours.Text.Trim), CInt(CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("txtMinutes"), TextBox).Text), item)
            'Nilay (07 Feb 2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtHours_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtMinutes_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtMinutes As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtMinutes.NamingContainer, DataGridItem)

            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
            'If CInt(txtMinutes.Text) > 59 Then
            '    DisplayMessage.DisplayMessage("Invalid Activity Minutes. Reason: Activity Minutes cannot be greater than or equal to 60.", Me)
            '    txtMinutes.Text = "00"
            '    txtMinutes.Focus()
            '    Exit Sub
            'ElseIf txtMinutes.Text.Trim.Length <= 0 Then
            '    txtMinutes.Text = "00"
            '    txtMinutes.Focus()
            '    Exit Sub
            'Else
            '    Call CalculateActivityHours(CInt(CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text), CInt(txtMinutes.Text), item)
            'End If
            If txtMinutes.Text.Trim.Length <= 0 Then
                txtMinutes.Text = "00"
            End If

            Call CalculateActivityHours(CInt(CType(item.Cells(mdic("dgcolhEmpHours")).FindControl("txtHours"), TextBox).Text), CInt(txtMinutes.Text.Trim), item)
            'Nilay (07 Feb 2017) -- End



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtMinutes_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region "Project Details"

#Region "Private Methods"

    Private Sub GetProjectDetails(ByVal mdtFromDate As Date, ByVal mdtToDate As Date, ByVal mintEmployeeId As Integer, ByVal mstrEmployee As String, ByVal mstrProject As String, ByVal mstrDonor As String _
                                              , ByVal mintActivityId As Integer, ByVal mstrActivity As String, ByVal mdblAllocatePencentage As Double, ByVal xPeriodStartDate As Date, ByVal xPeriodEndDate As Date)
        '  'Pinkal (16-May-2018) -- 'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.[ByVal xPeriodStartDate As Date, ByVal xPeriodEndDate As Date]

        Try

            objLblEmpVal.Text = mstrEmployee
            objLblPeriodVal.Text = cboPeriod.SelectedItem.Text
            objLblProjectVal.Text = mstrProject
            objLblDonorGrantVal.Text = mstrDonor
            objLblActivityVal.Text = mstrActivity

            Dim dtTable As New DataTable()
            dtTable.Columns.Add("Id", Type.GetType("System.Int32"))
            dtTable.Columns.Add("Particulars", Type.GetType("System.String"))
            dtTable.Columns.Add("Hours", Type.GetType("System.String"))

            Dim drRow As DataRow = dtTable.NewRow()
            drRow("Id") = 1
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 1, "Projected(targeted) total number of hours for the month")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 2
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 2, "Projected running total number of hours as per entries")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 3
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 3, "Projected running total number of hours as of date")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 4
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 4, "Total Actual(running) number of hours")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 5
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 5, "Balance of the remained hrs")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 6
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 6, "Employee % allocation of the projects")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            Dim RowIndex As Integer = 1


            ' START - (I) Projected(targeted) total number of hours for the month (weekend + holiday Excluded)
            Dim mdblProejectedMonthHrs As Double = 0
            'Pinkal (16-May-2018) -- Start
            'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
            'Dim mdblTotalWorkingHrs As Double = GetProjectedTotalHRs(mdtFromDate, mdtToDate, mintEmployeeId)
            Dim mdblTotalWorkingHrs As Double = GetProjectedTotalHRs(xPeriodStartDate, xPeriodEndDate, mintEmployeeId)
            'Pinkal (16-May-2018) -- End
            dtTable.Rows(RowIndex - 1)("Hours") = CalculateTime(True, CInt((mdblTotalWorkingHrs * mdblAllocatePencentage) / 100)).ToString("#0.00").Replace(".", ":")
            mdblProejectedMonthHrs = CDbl((mdblTotalWorkingHrs * mdblAllocatePencentage) / 100)
            ' END -  (I) Projected(targeted) total number of hours for the month (weekend + holiday Excluded)



            'START - (II) Projected running total number of hours as per entries.
            mdblTotalWorkingHrs = 0
            If mdtApprovalTable IsNot Nothing AndAlso mdtApprovalTable.Rows.Count > 0 Then
                Dim dtDate As Date = Nothing
                dtDate = CDate(mdtApprovalTable.Compute("Max(activitydate)", "IsGrp = 0 AND employeeunkid = " & mintEmployeeId & " AND   activityunkid = " & mintActivityId))
                mdblTotalWorkingHrs = GetProjectedTotalHRs(mdtFromDate, dtDate, mintEmployeeId)
                dtTable.Rows(RowIndex)("Hours") = CalculateTime(True, CInt((mdblTotalWorkingHrs * mdblAllocatePencentage) / 100)).ToString("#0.00").Replace(".", ":")
            Else
                dtTable.Rows(RowIndex)("Hours") = "00:00"
            End If
            'END  - (II)  Projected running total number of hours as per entries.


            ' START - (III) Projected running total number of hours as of date.
            mdblTotalWorkingHrs = 0
            If eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date < mdtToDate.Date Then
                mdtToDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date
            End If
            mdblTotalWorkingHrs = GetProjectedTotalHRs(mdtFromDate, mdtToDate, mintEmployeeId)
            dtTable.Rows(RowIndex + 1)("Hours") = CalculateTime(True, CInt((mdblTotalWorkingHrs * mdblAllocatePencentage) / 100)).ToString("#0.00").Replace(".", ":")
            ' END - (III) Projected running total number of hours as of date. 


            'START - (IV) Total Actual(running) number of hours (only Entries)
            mdblTotalWorkingHrs = 0
            Dim mdblActualHrs As Double = 0
            If mdtApprovalTable IsNot Nothing AndAlso mdtApprovalTable.Rows.Count > 0 Then
                mdblTotalWorkingHrs = CInt(mdtApprovalTable.Compute("SUM(activity_hrsinMins)", "IsGrp = 0 AND employeeunkid = " & mintEmployeeId & " AND activityunkid =" & mintActivityId)) * 60
                mdblActualHrs = mdblTotalWorkingHrs
                dtTable.Rows(RowIndex + 2)("Hours") = CalculateTime(True, CInt(mdblTotalWorkingHrs)).ToString("#0.00").Replace(".", ":")
            Else
                dtTable.Rows(RowIndex + 2)("Hours") = "00:00"
            End If

            'END - (IV) Total Actual(running) number of hours (only Entries)

            'START - (V) Balance of the remained hrs.
            mdblTotalWorkingHrs = 0
            If mdblActualHrs > mdblProejectedMonthHrs Then
                mdblTotalWorkingHrs = mdblActualHrs - mdblProejectedMonthHrs
            Else
                mdblTotalWorkingHrs = mdblProejectedMonthHrs - mdblActualHrs
            End If
            dtTable.Rows(RowIndex + 3)("Hours") = CalculateTime(True, CInt(mdblTotalWorkingHrs)).ToString("#0.00").Replace(".", ":")
            'End - (V) Balance of the remained hrs.

            'START - (VI)  Show employee % allocation of the projects 
            dtTable.Rows(dtTable.Rows.Count - 1)("Hours") = mdblAllocatePencentage.ToString("#0.00") & " % "
            'END - (VI) Show employee % allocation of the projects 

            mdblActualHrs = 0
            mdblProejectedMonthHrs = 0
            mdblTotalWorkingHrs = 0

            dgvProjectHrDetails.AutoGenerateColumns = False
            dgvProjectHrDetails.DataSource = dtTable
            dgvProjectHrDetails.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "GetProjectDetails", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function GetProjectedTotalHRs(ByVal xFromDate As Date, ByVal xToDate As Date, ByVal xEmployeeId As Integer) As Double
        Dim mintTotalWorkingDays As Integer = 0
        Dim mdblTotalWorkingHrs As Double = 0
        Dim objEmpShift As New clsEmployee_Shift_Tran
        Dim objEmpHoliday As New clsemployee_holiday
        Try

            objEmpShift.GetTotalWorkingHours(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                     , Session("UserAccessModeSetting").ToString(), xEmployeeId, xFromDate.Date, xToDate.Date _
                                                                                                     , mintTotalWorkingDays, mdblTotalWorkingHrs, False)


            Dim dsHoliday As DataSet = objEmpHoliday.GetList("List", Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                         , xFromDate.Date, xToDate.Date, Session("UserAccessModeSetting").ToString(), True, False, True, xEmployeeId, , _
                                                                                         , " AND convert(char(8),lvholiday_master.holidaydate,112) >= '" & eZeeDate.convertDate(xFromDate) & "' AND convert(char(8),lvholiday_master.holidaydate,112) <= '" & eZeeDate.convertDate(xToDate) & "'")

            If dsHoliday IsNot Nothing AndAlso dsHoliday.Tables(0).Rows.Count > 0 Then
                Dim objShiftTran As New clsshift_tran
                For Each dr As DataRow In dsHoliday.Tables(0).Rows
                    Dim mintShiftId As Integer = objEmpShift.GetEmployee_Current_ShiftId(eZeeDate.convertDate(dr("holidaydate").ToString()).Date, xEmployeeId)
                    objShiftTran.GetShiftTran(mintShiftId)
                    Dim dHLRow() As DataRow = objShiftTran._dtShiftday.Select("isweekend = 0 AND dayid = " & Weekday(eZeeDate.convertDate(dr("holidaydate").ToString()).Date, FirstDayOfWeek.Sunday) - 1)
                    If dHLRow.Length > 0 Then
                        mintTotalWorkingDays -= 1
                        mdblTotalWorkingHrs -= CDbl(dHLRow(0)("workinghrsinsec"))
                    End If
                Next
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "GetProjectedTotalHRs", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objEmpShift = Nothing
            objEmpHoliday = Nothing
        End Try
        Return mdblTotalWorkingHrs
    End Function

#End Region

#Region "Button's Event"

    Protected Sub btnShowDetailsClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowDetailsClose.Click
        Try
            popupShowDetails.Hide()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnShowDetailsClose_Click: " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)

            Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.ID, Me.LblPeriod.Text)
            Me.LblRemarks.Text = Language._Object.getCaption(Me.LblRemarks.ID, Me.LblRemarks.Text)

            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.ID, Me.btnApprove.Text).Replace("&", "")
            Me.btnReject.Text = Language._Object.getCaption(Me.btnReject.ID, Me.btnReject.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvEmpTimesheet.Columns(0).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(0).FooterText, dgvEmpTimesheet.Columns(0).HeaderText)
            Me.dgvEmpTimesheet.Columns(1).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(1).FooterText, dgvEmpTimesheet.Columns(1).HeaderText)
            Me.dgvEmpTimesheet.Columns(2).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(2).FooterText, dgvEmpTimesheet.Columns(2).HeaderText)
            Me.dgvEmpTimesheet.Columns(3).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(3).FooterText, dgvEmpTimesheet.Columns(3).HeaderText)
            Me.dgvEmpTimesheet.Columns(4).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(4).FooterText, dgvEmpTimesheet.Columns(4).HeaderText)
            Me.dgvEmpTimesheet.Columns(5).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(5).FooterText, dgvEmpTimesheet.Columns(5).HeaderText)
            Me.dgvEmpTimesheet.Columns(6).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(6).FooterText, dgvEmpTimesheet.Columns(6).HeaderText)
            Me.dgvEmpTimesheet.Columns(7).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(7).FooterText, dgvEmpTimesheet.Columns(7).HeaderText)
            Me.dgvEmpTimesheet.Columns(8).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(8).FooterText, dgvEmpTimesheet.Columns(8).HeaderText)
            Me.dgvEmpTimesheet.Columns(9).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(9).FooterText, dgvEmpTimesheet.Columns(9).HeaderText)
            Me.dgvEmpTimesheet.Columns(10).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(10).FooterText, dgvEmpTimesheet.Columns(10).HeaderText)
            Me.dgvEmpTimesheet.Columns(11).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(11).FooterText, dgvEmpTimesheet.Columns(11).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region 'Language & UI Settings
    '</Language>


End Class
