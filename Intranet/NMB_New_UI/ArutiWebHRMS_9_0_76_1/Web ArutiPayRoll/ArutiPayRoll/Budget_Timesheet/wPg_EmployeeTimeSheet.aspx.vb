﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports Microsoft.VisualBasic
#End Region

Partial Class Budget_Timesheet_wPg_EmployeeTimeSheet
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmBudgetEmp_Timesheet"
    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
    Private ReadOnly mstrModuleName1 As String = "frmProjectHoursDetails"
    'Pinkal (28-Mar-2018) -- End
    Private DisplayMessage As New CommonCodes
    Private objBudgetTimesheet As clsBudgetEmp_timesheet
    Private mdtPeriodStartDate As Date = Nothing
    Private mdtPeriodEndDate As Date = Nothing
    Private mintEmptimesheetunkid As Integer = 0
    Private mintShiftWorkingHrs As Integer = 0
    Private mintShiftID As Integer = 0
    Private mstrCommandName As String = ""
    Private mintEmployeeId As Integer = 0
    Private mdtActivityDate As Date = Nothing
    Private mdtList As New DataTable
    Private mdtActivity As New DataTable
    Dim dis As Dictionary(Of String, Integer)
    'Nilay (10 Jan 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
    Private mdtEmpTimesheet As DataTable = Nothing
    Dim mdicETS As Dictionary(Of String, Integer)
    'Nilay (10 Jan 2017) -- End

    'Nilay (10 Jan 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
    Private mintLoginTypeID As Integer = -1
    Private mintLoginEmployeeID As Integer = -1
    Private mintUserID As Integer = -1
    'Nilay (10 Jan 2017) -- End

    'Nilay (15 Feb 2017) -- Start
    Private mintApprovedActivityHours As Integer = -1
    'Nilay (15 Feb 2017) -- End

    'Nilay (21 Mar 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Private mintOldActivityWorkingHrsInMin As Integer = 0
    'Nilay (21 Mar 2017) -- End


    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
    Private xCurrentItemIndex As Integer = -1
    Private mstrOriginalHrs As String = "00:00"
    'Pinkal (28-Mar-2018) -- End


    'Pinkal (16-May-2018) -- Start
    'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
    Private mdtPStartDate As Date = Nothing
    Private mdtPEndDate As Date = Nothing
    'Pinkal (16-May-2018) -- End

    'Pinkal (06-Jan-2023) -- Start
    '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
    Dim mintEditedCostCenterID As Integer = 0
    'Pinkal (06-Jan-2023) -- End

#End Region

#Region " Page's Events "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            objBudgetTimesheet = New clsBudgetEmp_timesheet

            dis = dgvTimesheet.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvTimesheet.Columns.IndexOf(x))
            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            mdicETS = dgvEmpTimesheet.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvEmpTimesheet.Columns.IndexOf(x))
            'Nilay (10 Jan 2017) -- End

            If IsPostBack = False Then
                Call SetLanguage()

                'Pinkal (06-Jan-2023) -- Start
                '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                If Session("CompanyGroupName").ToString().ToUpper() = "PSI MALAWI" Then
                    dgvTimesheet.Columns(getColumnId_Datagrid(dgvTimesheet, "dgcolhCostCenter", False, True)).Visible = True
                End If
                'Pinkal (06-Jan-2023) -- End


                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    dgvEmpTimesheet.DataSource = New List(Of String)
                    dgvEmpTimesheet.DataBind()

                    dgvTimesheet.DataSource = New List(Of String)
                    dgvTimesheet.DataBind()

                    'Nilay (15 Feb 2017) -- Start
                    'dgvEmpTimesheet.Columns(2).Visible = True
                    'Nilay (15 Feb 2017) -- End

                    'Nilay (10 Jan 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
                    mintLoginTypeID = enLogin_Mode.MGR_SELF_SERVICE
                    mintLoginEmployeeID = 0
                    mintUserID = CInt(Session("UserId"))
                    'Nilay (10 Jan 2017) -- End

                    'Pinkal (03-May-2017) -- Start
                    'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
                    SetVisibility()
                    'Pinkal (03-May-2017) -- End


                ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then

                    'Nilay (01 Apr 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                    lnkGlobalCancelTimesheet.Visible = False
                    'Nilay (01 Apr 2017) -- End

                    'Nilay (15 Feb 2017) -- Start
                    'dgvEmpTimesheet.Columns(2).Visible = False

                    'Nilay (27 Feb 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                    'dgvEmpTimesheet.Columns(3).Visible = False
                    dgvEmpTimesheet.Columns(2).Visible = False
                    'Nilay (27 Feb 2017) -- End
                    'Nilay (15 Feb 2017) -- End

                    'Nilay (10 Jan 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
                    mintLoginTypeID = enLogin_Mode.EMP_SELF_SERVICE
                    mintLoginEmployeeID = CInt(Session("Employeeunkid"))
                    mintUserID = 0
                    'Nilay (10 Jan 2017) -- End
                End If

                Call FillCombo()
            Else
                mdtPeriodStartDate = CDate(Me.ViewState("PeriodStartDate")).Date
                mdtPeriodEndDate = CDate(Me.ViewState("PeriodEndDate")).Date
                mintEmptimesheetunkid = CInt(Me.ViewState("Emptimesheetunkid"))
                mintShiftWorkingHrs = CInt(Me.ViewState("ShiftWorkingHrs"))
                mstrCommandName = CStr(Me.ViewState("CommandName"))
                mintEmployeeId = CInt(Me.ViewState("EmployeeId"))
                mdtActivityDate = CDate(Me.ViewState("ActivityDate"))
                mdtList = CType(Me.ViewState("DataList"), DataTable)
                mdtActivity = CType(Me.ViewState("ActivityTable"), DataTable)
                mintShiftID = CInt(Me.ViewState("ShiftID"))
                'Nilay (10 Jan 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
                mdtEmpTimesheet = CType(Me.ViewState("EmpTimesheetTable"), DataTable)
                mintLoginTypeID = CInt(Me.ViewState("LoginTypeID"))
                mintLoginEmployeeID = CInt(Me.ViewState("LoginEmployeeID"))
                mintUserID = CInt(Me.ViewState("UserID"))
                'Nilay (10 Jan 2017) -- End

                'Nilay (15 Feb 2017) -- Start
                mintApprovedActivityHours = CInt(Me.ViewState("ApprovedActivityHours"))
                'Nilay (15 Feb 2017) -- End

                'Nilay (21 Mar 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                mintOldActivityWorkingHrsInMin = CInt(Me.ViewState("OldActivityWorkingHrsInMin"))
                'Nilay (21 Mar 2017) -- End

                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                xCurrentItemIndex = CInt(Me.ViewState("xCurrentItemIndex"))
                mstrOriginalHrs = Me.ViewState("mstrOriginalHrs").ToString()
                'Pinkal (28-Mar-2018) -- End


                'Pinkal (16-May-2018) -- Start
                'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
                mdtPStartDate = CDate(Me.ViewState("mdtPStartDate")).Date
                mdtPEndDate = CDate(Me.ViewState("mdtPEndDate")).Date
                'Pinkal (16-May-2018) -- End

                'Pinkal (06-Jan-2023) -- Start
                '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                mintEditedCostCenterID = CInt(Me.ViewState("EditedCostCenterID"))
                'Pinkal (06-Jan-2023) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("PeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("PeriodEndDate") = mdtPeriodEndDate
            Me.ViewState("Emptimesheetunkid") = mintEmptimesheetunkid
            Me.ViewState("ShiftWorkingHrs") = mintShiftWorkingHrs
            Me.ViewState("CommandName") = mstrCommandName
            Me.ViewState("EmployeeId") = mintEmployeeId
            Me.ViewState("DataList") = mdtList
            Me.ViewState("ActivityDate") = mdtActivityDate
            Me.ViewState("ActivityTable") = mdtActivity
            Me.ViewState("ShiftID") = mintShiftID
            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            Me.ViewState("EmpTimesheetTable") = mdtEmpTimesheet
            Me.ViewState("LoginTypeID") = mintLoginTypeID
            Me.ViewState("LoginEmployeeID") = mintLoginEmployeeID
            Me.ViewState("UserID") = mintUserID
            'Nilay (10 Jan 2017) -- End

            'Nilay (15 Feb 2017) -- Start
            Me.ViewState("ApprovedActivityHours") = mintApprovedActivityHours
            'Nilay (15 Feb 2017) -- End

            'Nilay (21 Mar 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            Me.ViewState("OldActivityWorkingHrsInMin") = mintOldActivityWorkingHrsInMin
            'Nilay (21 Mar 2017) -- End

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            Me.ViewState("xCurrentItemIndex") = xCurrentItemIndex
            Me.ViewState("mstrOriginalHrs") = mstrOriginalHrs
            'Pinkal (28-Mar-2018) -- End

            'Pinkal (16-May-2018) -- Start
            'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
            Me.ViewState("mdtPStartDate") = mdtPStartDate
            Me.ViewState("mdtPEndDate") = mdtPEndDate
            'Pinkal (16-May-2018) -- End

            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            Me.ViewState("EditedCostCenterID") = mintEditedCostCenterID
            'Pinkal (06-Jan-2023) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objPeriod As New clscommom_period_Tran
            Dim objFundCode As New clsFundSource_Master
            Dim objMaster As New clsMasterData
            Dim intFirstOpenPeriodID As Integer = 0

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
                                               CDate(Session("fin_startdate").ToString).Date, "Period", True)
            intFirstOpenPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), enStatusType.OPEN, False, True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Period")
                .DataBind()
                .SelectedValue = intFirstOpenPeriodID.ToString
            End With
            Call cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Try

            'Pinkal (03-May-2017) -- Start
            'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewEmployeeBudgetTimesheetList")) = False Then Exit Sub
            End If
            'Pinkal (03-May-2017) -- End


            Dim blnApplyUserAccessFilter As Boolean = True
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then blnApplyUserAccessFilter = False


            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

            'Dim dsList As DataSet = objBudgetTimesheet.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
            '                                                   CInt(Session("CompanyUnkId")), mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, _
            '                                                   Session("UserAccessModeSetting").ToString(), True, CBool(Session("IsIncludeInactiveEmp")), _
            '                                                   CInt(cboEmployee.SelectedValue), True, True, _
            '                                                   CInt(IIf(CInt(cboPeriod.SelectedValue) > 0, CInt(cboPeriod.SelectedValue), -1)), _
            '                                                   Nothing, "", blnApplyUserAccessFilter)

            Dim dsList As DataSet = objBudgetTimesheet.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, _
                                                             Session("UserAccessModeSetting").ToString(), True, CBool(Session("IsIncludeInactiveEmp")), _
                                                              CBool(Session("AllowOverTimeToEmpTimesheet")), CInt(cboEmployee.SelectedValue), True, True, _
                                                               CInt(IIf(CInt(cboPeriod.SelectedValue) > 0, CInt(cboPeriod.SelectedValue), -1)), _
                                                               Nothing, "", blnApplyUserAccessFilter)

            'Pinkal (28-Mar-2018) -- End


            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            If Session("CompanyGroupName").ToString().ToUpper() = "PSI MALAWI" Then
                dgvEmpTimesheet.Columns(getColumnId_Datagrid(dgvEmpTimesheet, "dgcolhCostCenterList", False, True)).Visible = True
            End If
            'Pinkal (06-Jan-2023) -- End


            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            'dgvEmpTimesheet.DataSource = dsList.Tables("List")
            mdtEmpTimesheet = dsList.Tables("List")
            dgvEmpTimesheet.DataSource = mdtEmpTimesheet
            'Nilay (10 Jan 2017) -- End

            Dim strColumnArray() As String = {"dgcolhDate"}
            Dim dCol = dgvEmpTimesheet.Columns.Cast(Of DataGridColumn).Where(Function(x) strColumnArray.Contains(x.FooterText))
            If dCol.Count > 0 Then
                For Each dc As DataGridColumn In dCol
                    CType(dgvEmpTimesheet.Columns(dgvEmpTimesheet.Columns.IndexOf(dc)), BoundColumn).DataFormatString = "{0:" & Session("DateFormat").ToString & "}"
                Next
            End If

            dgvEmpTimesheet.DataBind()

            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            If CBool(Session("NotAllowIncompleteTimesheet")) = True Then
                dgvEmpTimesheet.Columns(mdicETS("btnCancel")).Visible = False
            End If
            'Nilay (01 Apr 2017) -- End

            'If CInt(cboEmployee.SelectedValue) > 0 Then
            '    objLblValue.Text = Language.getMessage(mstrModuleName, 12, "Total Working Hrs:") & " " & _
            '                       CDec(CalculateTime(True, objBudgetTimesheet.GetEmpWorkingHrsForTheDay(CDate(dtpDate.GetDate).Date, CInt(cboEmployee.SelectedValue)) * 60)).ToString("#00.00").Replace(".", ":")
            'Else
            '    objLblValue.Text = Language.getMessage(mstrModuleName, 12, "Total Working Hrs:") & " 00:00"
            'End If

            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs

            'Nilay (27 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'Dim dRow As DataRow() = mdtEmpTimesheet.Select("IsGroup=0 AND isholiday=0 AND isLeave=0 AND isDayOff=0 AND issubmit_approval=0")
            'If dRow.Length <= 0 Then
            '    dgvEmpTimesheet.Columns(mdicETS("dgcolhSelect")).Visible = False
            'Else
            '    dgvEmpTimesheet.Columns(mdicETS("dgcolhSelect")).Visible = True
            'End If
            'Nilay (27 Feb 2017) -- End

            'Nilay (07 Feb 2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillEmpActivityList()
        Dim dsList As DataSet = Nothing
        Try
            Dim objEmpshift As New clsEmployee_Shift_Tran
            mintShiftID = objEmpshift.GetEmployee_Current_ShiftId(dtpDate.GetDate.Date, CInt(cboEmployee.SelectedValue))
            objEmpshift = Nothing

            Dim objShiftTran As New clsshift_tran
            objShiftTran.GetShiftTran(mintShiftID)
            Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(dtpDate.GetDate.Date, FirstDayOfWeek.Sunday) - 1)
            If drRow.Length > 0 Then
                mintShiftWorkingHrs = CInt(drRow(0)("workinghrsinsec"))
            End If
            objShiftTran = Nothing

            Dim objFundSource As New clsBudgetCodesfundsource_Tran
            'Nilay (21 Mar 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'dsList = objFundSource.GetEmployeeAssignActivities(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), mintShiftWorkingHrs, dtpDate.GetDate.Date, mintEmptimesheetunkid)

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            'dsList = objFundSource.GetEmployeeAssignActivities(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), mintShiftWorkingHrs, _
            '                                                   dtpDate.GetDate.Date, CBool(Session("AllowActivityHoursByPercentage")), _
            '                                                   mintEmptimesheetunkid)

            dsList = objFundSource.GetEmployeeAssignActivities(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), mintShiftWorkingHrs, _
                                                               dtpDate.GetDate.Date, dtpToDate.GetDate.Date, CBool(Session("AllowActivityHoursByPercentage")), _
                                                               mintEmptimesheetunkid)

            'Pinkal (13-Oct-2017) -- End


            'Nilay (21 Mar 2017) -- End

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim drTotalRow As DataRow = dsList.Tables(0).NewRow()
                drTotalRow("budgetunkid") = -999
                drTotalRow("periodunkid") = -1
                drTotalRow("Employeeunkid") = -1
                drTotalRow("fundsourceunkid") = -1
                drTotalRow("Donor") = ""
                drTotalRow("fundprojectcodeunkid") = -1
                drTotalRow("Project") = ""
                drTotalRow("fundactivityunkid") = -1


                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                If CBool(Session("ShowBgTimesheetActivity")) Then
                    drTotalRow("Activity") = Language.getMessage(mstrModuleName, 29, "Total Activity Hrs :")
                Else
                    drTotalRow("Donor") = Language.getMessage(mstrModuleName, 29, "Total Activity Hrs :")
                End If
                'Pinkal (28-Jul-2018) -- End



                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 200)  Working on An option to show/review % allocation of the projects..
                'drTotalRow("percentage") = 0
                drTotalRow("percentage") = CDec(dsList.Tables(0).Compute("SUM(percentage)", "1=1"))
                'Pinkal (28-Mar-2018) -- End

                drTotalRow("emptimesheetunkid") = -1
                Dim mintActivityMins As Integer = CInt(dsList.Tables(0).Compute("SUM(ActivityHoursInMin)", "1=1"))
                drTotalRow("ActivityHours") = CalculateTime(True, mintActivityMins * 60).ToString("#00.00").Replace(".", ":")
                drTotalRow("ActivityHoursInMin") = 0
                drTotalRow("AssignedActivityHrsInMin") = 0
                drTotalRow("Description") = ""

                'Pinkal (06-Jan-2023) -- Start
                '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                drTotalRow("costcenterunkid") = 0
                'Pinkal (06-Jan-2023) -- End

                dsList.Tables(0).Rows.Add(drTotalRow)
            End If

            mdtActivity = dsList.Tables(0)

            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            If dgvTimesheet.Columns(getColumnId_Datagrid(dgvTimesheet, "dgcolhCostCenter", False, True)).Visible AndAlso mintEditedCostCenterID > 0 Then
                If mdtActivity IsNot Nothing AndAlso mdtActivity.Rows.Count > 0 Then
                    mdtActivity.Rows(0)("costcenterunkid") = mintEditedCostCenterID
                    mdtActivity.AcceptChanges()
                End If
            End If
            mintEditedCostCenterID = 0
            'Pinkal (06-Jan-2023) -- End

            dgvTimesheet.DataSource = mdtActivity
            dgvTimesheet.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "FillEmpActivityList", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Function Validation() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Return False


            ElseIf (dtpDate.IsNull OrElse dtpToDate.IsNull) OrElse (dtpDate.GetDate.Date > dtpToDate.GetDate.Date) OrElse (dtpDate.GetDate.Date < mdtPeriodStartDate OrElse dtpToDate.GetDate.Date > mdtPeriodEndDate) Then
                DisplayMessage.DisplayMessage("Please Select proper Date Range.", Me)
                Return False


            ElseIf (dtpDate.GetDate.Date = dtpToDate.GetDate.Date) AndAlso (dtpDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date OrElse dtpToDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "You cannot assign activity hours for future date."), Me)
                dtpDate.Focus()
                Return False


            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select Employee."), Me)
                cboEmployee.Focus()
                Return False
            Else
                Dim objapprover As New clstsapprover_master
                Dim dtList As DataTable = objapprover.GetEmployeeApprover(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                          CBool(Session("IsIncludeInactiveEmp")), -1, CStr(cboEmployee.SelectedValue), _
                                                                          -1, Nothing, False, "")

                If dtList.Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Please Assign Approver to this employee and also map approver to system user."), Me)
                    Return False
                End If
                dtList.Clear()
                dtList = Nothing
                objapprover = Nothing

                If dgvTimesheet.Items.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 37, "There is no activity assign to this employee for this tenure.please assign activity to this employee."), Me)
                    Return False
                End If


                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
                'Dim objEHoliday As New clsemployee_holiday
                'If objEHoliday.GetEmployeeHoliday(CInt(cboEmployee.SelectedValue)).Rows.Count <= 0 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 39, "Sorry,you cannot add activity for this employee. Reason : Not a Single Holiday is assigned to this employee for current financial year."), Me)
                '    Return False
                'End If
                'objEHoliday = Nothing
                'Pinkal (28-Mar-2018) -- End



                If dtpDate.GetDate.Date = dtpToDate.GetDate.Date Then

                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    If CBool(Session("AllowOverTimeToEmpTimesheet")) = False Then
                        Dim objEmpHoliday As New clsemployee_holiday
                        Dim dtTable As DataTable = objEmpHoliday.GetEmployeeHoliday(CInt(cboEmployee.SelectedValue), dtpDate.GetDate.Date)
                        If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry,you cannot add activity for this employee for selected date. Reason : Holiday is assigned to this employee for this date."), Me)
                            Return False
                        End If
                        dtTable = Nothing
                        objEmpHoliday = Nothing
                    End If
                    'Pinkal (28-Jul-2018) -- End


                    Dim objEmpDayOFF As New clsemployee_dayoff_Tran
                    Dim dsList As DataSet = objEmpDayOFF.GetList("List", CStr(Session("Database_Name")), -1, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                 CStr(Session("UserAccessModeSetting")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 "", CInt(cboEmployee.SelectedValue), dtpDate.GetDate.Date, dtpDate.GetDate.Date)

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Sorry,you cannot add activity for this employee for selected date. Reason : Day Off is assigned to this employee for this date."), Me)
                        Return False
                    End If
                    dsList = Nothing
                    objEmpDayOFF = Nothing


                    If CBool(Session("AllowOverTimeToEmpTimesheet")) = False Then
                        Dim objShiftTran As New clsshift_tran
                        objShiftTran.GetShiftTran(mintShiftID)
                        Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(dtpDate.GetDate.Date, FirstDayOfWeek.Sunday) - 1 & " AND isweekend = 1")
                        If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry,you cannot add activity for this employee for selected date. Reason : Weekend is assigned to this employee for this date."), Me)
                            Return False
                        End If
                        drRow = Nothing
                        objShiftTran = Nothing
                    End If



                    'Pinkal (13-Aug-2019) -- Start
                    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                    Dim objLeaveIssue As New clsleaveissue_Tran
                    If objLeaveIssue.GetIssuedDayFractionForViewer(CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(dtpDate.GetDate.Date)) > 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 38, "Sorry,you cannot add activity for this employee for selected date. Reason : Leave is already taken on this date by this employee."), Me)
                        Return False
                    Else
                        'Pinkal (11-Sep-2019) -- Start
                        'Defect PACT - Cancel Days are not inserting in bugdet timesheet .
                        Dim objCancelform As New clsCancel_Leaveform
                        If objCancelform.GetCancelDayExistForEmployee(CInt(cboEmployee.SelectedValue), dtpDate.GetDate.Date, Nothing) = False Then
                            Dim objLeave As New clsleaveform
                            If objLeave.isDayExist(True, CInt(cboEmployee.SelectedValue), dtpDate.GetDate.Date, dtpToDate.GetDate.Date, -1) Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 46, "Sorry,you cannot add activity for this employee for selected date. Reason : Leave is already approved on this date by this employee."), Me)
                                Return False
                                'Pinkal (21-Oct-2019) -- Start
                                'Defect NMB - Worked On Employee Timesheet Id not set properly due to that it is creating problem.
                                'ElseIf objLeave.isDayExist(False, CInt(cboEmployee.SelectedValue), dtpDate.GetDate.Date, dtpToDate.GetDate.Date, -1) Then
                                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 38, "Sorry,you cannot add activity for this employee for selected date. Reason : Leave is already applied on this date by this employee."), Me)
                                '    Return False
                                'Pinkal (21-Oct-2019) -- End
                            End If
                            objLeave = Nothing
                        End If
                        objCancelform = Nothing
                        'Pinkal (11-Sep-2019) -- End
                    End If
                    objLeaveIssue = Nothing
                    'Pinkal (13-Aug-2019) -- End

                End If

            End If

            Dim intTotHoursInMin As Integer = CInt(mdtActivity.Compute("SUM(ActivityHoursInMin)", "budgetunkid <> -999"))

            If intTotHoursInMin >= (24 * 60) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Invalid Total working hrs.Reason:Total Working hrs cannot greater than or equal to 24. "), Me)
                Return False
            ElseIf intTotHoursInMin <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Total Activity Hours must be greater than 0. Please define hours for atleast one activity."), Me)
                Return False

            End If


            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            If Session("CompanyGroupName").ToString().ToUpper() = "PSI MALAWI" AndAlso dgvTimesheet.Columns(getColumnId_Datagrid(dgvTimesheet, "dgcolhCostCenter", False, True)).Visible Then
                Dim dRow() As DataRow = mdtActivity.Select("costcenterunkid <= 0 AND budgetunkid <> -999 ")
                If dRow.Length > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 47, "Cost Center is compulsory information.Please select cost center for each activity."), Me)
                    dgvTimesheet.Focus()
                    Return False
                End If
            End If
            'Pinkal (06-Jan-2023) -- End


            If CBool(Session("DescriptionMandatoryForActivity")) = True Then
                Dim dRow() As DataRow = mdtActivity.Select("ActivityHoursInMin > 0 AND (Description = '' OR Description IS NULL) AND budgetunkid <> -999 ")
                If dRow.Length > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 36, "Description is a mandatory information for each activity. Please mention description for each activity."), Me)
                    Return False
                End If
            End If


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Private Sub SetValue()
        Try
            Dim objApproverMst As New clstsapprover_master
            objBudgetTimesheet = New clsBudgetEmp_timesheet
            Dim dtList As New DataTable

            If mintEmptimesheetunkid > 0 Then objBudgetTimesheet._Emptimesheetunkid = mintEmptimesheetunkid

            objBudgetTimesheet._Periodunkid = CInt(cboPeriod.SelectedValue)
            objBudgetTimesheet._Activitydate = CDate(dtpDate.GetDate).Date
            objBudgetTimesheet._Employeeunkid = CInt(cboEmployee.SelectedValue)

            'objBudgetTimesheet._Activity_Hrs = mintActivityWorkingHrs
            objBudgetTimesheet._ApprovedActivity_Hrs = 0
            objBudgetTimesheet._Statusunkid = 2 'Pending


            'Pinkal (13-Apr-2017) -- Start
            'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objBudgetTimesheet._Userunkid = CInt(Session("UserId"))
                objBudgetTimesheet._LoginEmployeeunkid = 0
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objBudgetTimesheet._Userunkid = 0
                objBudgetTimesheet._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objBudgetTimesheet._WebFormName = mstrModuleName
            objBudgetTimesheet._WebClientIP = CStr(Session("IP_ADD"))
            objBudgetTimesheet._WebHostName = CStr(Session("HOST_NAME"))

            'Pinkal (13-Apr-2017) -- End




            'Nilay (10 Jan 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
            'dtList = objApproverMst.GetEmployeeApprover(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CBool(Session("IsIncludeInactiveEmp")), _
            '                                           -1, CStr(cboEmployee.SelectedValue), -1, Nothing)
            'objBudgetTimesheet._dtApproverList = dtList
            'Nilay (10 Jan 2017) -- End

            'Nilay (15 Feb 2017) -- Start
            'objBudgetTimesheet._dtEmployeeActivity = New DataView(mdtActivity, "budgetunkid > 0", "", DataViewRowState.CurrentRows).ToTable
            objBudgetTimesheet._dtEmployeeActivity = New DataView(mdtActivity, "budgetunkid > 0 AND ActivityHoursInMin > 0", "", DataViewRowState.CurrentRows).ToTable
            'Nilay (15 Feb 2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = "0"
            dtpDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                cboEmployee.SelectedValue = "0"
            End If

            mdtActivity = Nothing
            mdtActivityDate = Nothing
            mintEmptimesheetunkid = 0

            dgvTimesheet.DataSource = New List(Of String)
            dgvTimesheet.DataBind()

            dgvEmpTimesheet.DataSource = New List(Of String)
            dgvEmpTimesheet.DataBind()

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            'Pinkal (13-Oct-2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetEmployeeBudgetCodes()
        Try
            Dim objBudgetCodes As New clsBudgetcodes_master
            Dim dsList As New DataSet

            '            If CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboActivity.SelectedValue) > 0 AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
            '                dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPeriod.SelectedValue), mintBudgetMstID, CInt(cboActivity.SelectedValue), CInt(cboEmployee.SelectedValue))
            '                Dim objEmpshift As New clsEmployee_Shift_Tran
            '                Dim mintShiftID As Integer = objEmpshift.GetEmployee_Current_ShiftId(CDate(dtpDate.GetDate).Date, CInt(cboEmployee.SelectedValue))
            '                objEmpshift = Nothing
            '                Dim objShiftTran As New clsshift_tran
            '                objShiftTran.GetShiftTran(mintShiftID)
            '                Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(CDate(dtpDate.GetDate).Date, FirstDayOfWeek.Sunday) - 1)
            '                If drRow.Length > 0 AndAlso (dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0) Then
            '                    mintShiftWorkingHrs = CInt(drRow(0)("workinghrsinsec"))
            '                    mdecPercentageHrs = CDec(dsList.Tables(0).Rows(0)("percentage"))
            '                    Call GetWorkingMins(mintShiftWorkingHrs, mdecPercentageHrs, "")
            '                Else
            '                    mintShiftWorkingHrs = 0
            '                    mdecPercentageHrs = 0
            '                    mintActivityWorkingHrs = 0
            '                    txtHours.Text = ""
            '                    mstrDefaultWorkingHRs = "00:00"
            '                    GoTo ResetValue
            '                End If
            '            Else
            'ResetValue:
            '                mintShiftWorkingHrs = 0
            '                mdecPercentageHrs = 0
            '                txtHours.Text = ""

            '            End If
            objBudgetCodes = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetEmployeeBudgetCodes:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Private Sub GetWorkingMins(ByVal xShiftWorkingHrs As Integer, ByVal xPercentageHrs As Decimal, Optional ByVal mstrWorkingHrs As String = "")
    '    Try
    '        If mstrWorkingHrs.Trim.Length <= 0 Then
    '            Dim mintActivityHrs As Integer = CInt(mintShiftWorkingHrs * mdecPercentageHrs / 100)
    '            mstrDefaultWorkingHRs = CDec(CalculateTime(True, mintActivityHrs)).ToString("#00.00").Replace(".", ":")
    '            'txtHours.Text = mstrDefaultWorkingHRs
    '        Else
    '            Dim mintHours As Integer = CInt(mstrWorkingHrs.Trim.Substring(0, mstrWorkingHrs.Trim.IndexOf(":")))
    '            Dim mintMins As Integer = CInt(mstrWorkingHrs.Trim.Substring(mstrWorkingHrs.Trim.IndexOf(":") + 1))
    '            mintActivityWorkingHrs = (mintHours * 60) + mintMins
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("GetWorkingMins:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Private Sub TextBoxTimeValidation(ByVal txt As TextBox)
        Try
            If txt.Text.Trim = ":" Then
                txt.Text = "00:00"
            ElseIf CDec(txt.Text.Trim.Replace(":", ".")) > 24 Then
Isvalid:
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Invalid working hrs.Please enter valid working hrs."), Me)
                txt.Focus()
                Exit Sub

            ElseIf txt.Text.Trim.Contains(":") Then
                If txt.Text.Trim.Contains(":60") Then
calFromHr:          If CDec(CDec(txt.Text.Trim.Replace(":", ".")) + 0.4) <= 24 Then
                        txt.Text = CDec(CDec(txt.Text.Trim.Replace(":", ".")) + 0.4).ToString("#00.00").Replace(".", ":")
                    Else
                        GoTo Isvalid
                    End If
                ElseIf txt.Text <> "" And txt.Text <> ":" Then
                    If txt.Text.Trim.IndexOf(":") = 0 And txt.Text.Trim.Contains(":60") Then
                        GoTo calFromHr
                    ElseIf txt.Text.Substring(txt.Text.Trim.IndexOf(":"), txt.Text.Trim.Length - txt.Text.Trim.IndexOf(":")) = ":" Then
                        txt.Text = txt.Text & "00"
                    ElseIf CDec(IIf(txt.Text.Substring(txt.Text.Trim.IndexOf(":") + 1, txt.Text.Trim.Length - txt.Text.Trim.IndexOf(":") - 1) = "", "0", txt.Text.Substring(txt.Text.Trim.IndexOf(":") + 1, txt.Text.Trim.Length - txt.Text.Trim.IndexOf(":") - 1))) > 59 Then
                        GoTo calFromHr
                    End If
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "TextBoxTimeValidation:- ", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub CalculateActivityHours(ByVal intHours As Integer, ByVal intMinutes As Integer, ByVal item As DataGridItem)
        Try
            Dim intHoursInMin As Integer = 0
            Dim intTotHoursInMin As Integer = 0
            Dim decActivityHours As Decimal = 0


            'Pinkal (04-May-2018) -- Start
            'Enhancement - Budget timesheet Enhancement for MST/THPS.

            mdtActivity.Rows(item.ItemIndex).Item("ActivityHoursInMin") = intHoursInMin
            mdtActivity.Rows(item.ItemIndex).Item("ActivityHours") = intHours.ToString("#00") & ":" & intMinutes.ToString("#00")
            mdtActivity.Rows(item.ItemIndex).Item("ActivityHoursInMin") = (intHours * 60) + intMinutes
            mdtActivity.AcceptChanges()

            If intHours <= 0 AndAlso intMinutes <= 0 Then
                Exit Sub
            End If
            'Pinkal (04-May-2018) -- End


            Dim txtHours As TextBox = CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox)
            Dim txtMinutes As TextBox = CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox)

            Dim strActivityHours As String = item.Cells(dis("dgcolhActivityHours")).Text
            Dim strHours As String = strActivityHours.Trim.Substring(0, strActivityHours.Trim.IndexOf(":"))
            Dim strMinutes As String = strActivityHours.Trim.Substring(strActivityHours.Trim.IndexOf(":") + 1)

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            mstrOriginalHrs = strActivityHours
            'Pinkal (28-Mar-2018) -- End

            If intHours > 23 Then
                DisplayMessage.DisplayMessage("Invalid Activity Hours. Reason: Activity Hours cannot be greater than or equal to 24.", Me)
                txtHours.Text = strHours
                txtHours.Focus()
                Exit Sub
            ElseIf intMinutes > 59 Then
                DisplayMessage.DisplayMessage("Invalid Activity Minutes. Reason: Activity Minutes cannot be greater than or equal to 60.", Me)
                txtMinutes.Text = strMinutes
                txtMinutes.Focus()
                Exit Sub
            End If


            Dim objBudgetCodes As New clsBudgetcodes_master
            Dim dsList As DataSet = objBudgetCodes.GetEmployeeActivityPercentage(CInt(item.Cells(dis("objdgcolhEmpPeriodunkid")).Text), _
                                                                                 CInt(item.Cells(dis("objdgcolhEmpBudgetID")).Text), _
                                                                                 CInt(item.Cells(dis("objdgcolhEmpActivityunkid")).Text), _
                                                                                 CInt(cboEmployee.SelectedValue))

            Dim objShiftTran As New clsshift_tran
            objShiftTran.GetShiftTran(mintShiftID)
            Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(dtpDate.GetDate.Date, FirstDayOfWeek.Sunday) - 1)

            If drRow.Length > 0 Then
                mintOldActivityWorkingHrsInMin = CInt(item.Cells(dis("objdgcolhEmpHourInMin")).Text)

                If isTimesheetExists(item) = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage("clsBudgetEmp_timesheet", 1, "This Activity is already defined of this employee for same date. Please define different activity for this date."), Me)
                    CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = strHours
                    CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = strMinutes
                    Exit Sub
                End If

                intHoursInMin = intHours * 60 + intMinutes

                intTotHoursInMin = CInt(mdtActivity.Compute("SUM(ActivityHoursInMin)", "budgetunkid <> -999"))
                decActivityHours = CDec(CalculateTime(True, intTotHoursInMin * 60))

                Dim objBudgetTsApproval As New clstsemptimesheet_approval


                Dim intTotalActivityHoursInMin As Integer = objBudgetTsApproval.ComputeActivityHours(CInt(cboEmployee.SelectedValue), _
                                                                                                     CDate(dtpDate.GetDate).Date, _
                                                                                                     CInt(cboPeriod.SelectedValue))
                Dim intComputeActivityHoursInMin As Integer = 0

                If intTotalActivityHoursInMin <= 0 Then
                    If mintEmptimesheetunkid > 0 Then
                        intTotalActivityHoursInMin = CInt(mdtActivity.Compute("SUM(ActivityHoursInMin)", "budgetunkid <> -999"))
                    Else

                        'Pinkal (02-Jun-2017) -- Start
                        'Enhancement - Problem Resolved for AKFK .
                        If IsDBNull(mdtActivity.Compute("SUM(ActivityHoursInMin)", _
                                                                              "fundactivityunkid <> " & CInt(item.Cells(dis("objdgcolhEmpActivityunkid")).Text) & _
                                                                              " AND  budgetunkid <> -999")) = False Then

                            intTotalActivityHoursInMin = CInt(mdtActivity.Compute("SUM(ActivityHoursInMin)", _
                                                                                  "fundactivityunkid <> " & CInt(item.Cells(dis("objdgcolhEmpActivityunkid")).Text) & _
                                                                                  " AND  budgetunkid <> -999"))
                        End If

                        'Pinkal (02-Jun-2017) -- End

                    End If
                End If

                If mintEmptimesheetunkid > 0 Then
                    intComputeActivityHoursInMin = intTotalActivityHoursInMin - mintOldActivityWorkingHrsInMin + intHoursInMin
                Else
                    intComputeActivityHoursInMin = intTotalActivityHoursInMin + intHoursInMin
                End If

                If CBool(Session("AllowToExceedTimeAssignedToActivity")) = True Then

                    If CBool(Session("AllowOverTimeToEmpTimesheet")) = False Then

                        'Pinkal (13-Aug-2019) -- Start
                        'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.

                        'If CInt(CInt(drRow(0)("workinghrsinsec")) / 60) < intHoursInMin Then 'workinghrsinsec=shifthours
                        '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "This employee does not allow to do this activity hours more than define shift hours."), Me)
                        '    CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = strHours
                        '    CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = strMinutes
                        '    Exit Sub


                        'ElseIf CInt(mdtActivity.Rows(item.ItemIndex).Item("AssignedActivityHrsInMin")) < intHoursInMin AndAlso CInt(CInt(drRow(0)("workinghrsinsec")) / 60) >= intHoursInMin Then

                        If CInt(mdtActivity.Rows(item.ItemIndex).Item("AssignedActivityHrsInMin")) < intHoursInMin AndAlso CInt(CInt(drRow(0)("workinghrsinsec")) / 60) >= intHoursInMin Then

                            'If intComputeActivityHoursInMin > CInt(CInt(drRow(0)("workinghrsinsec")) / 60) Then
                            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 37, "Total activity hours cannot be greater than shift hours."), Me)
                            '    CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = strHours
                            '    CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = strMinutes
                            '    Exit Sub
                            'End If


                            If CBool(Session("AllowEmpAssignedProjectExceedTime")) Then
                                Dim objExemptEmp As New clstsexemptemployee_tran
                                If objExemptEmp.isExist(CInt(cboEmployee.SelectedValue), -1) Then
                                    SetTotalActivityHours(mdtActivity, intHoursInMin, intHours, intMinutes, item)
                                    dgvTimesheet.DataSource = mdtActivity
                                    dgvTimesheet.DataBind()
                                    objExemptEmp = Nothing
                                    Exit Sub
                                End If
                                objExemptEmp = Nothing
                            Else
                                If ValidateProjectHrs(item.ItemIndex, intHoursInMin) = False Then
                                    If btnSave.Visible Then btnSave.Enabled = False
                                    'Pinkal (30-Aug-2019) -- Start
                                    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                                    If btnEdit.Visible Then btnEdit.Enabled = False
                                    'Pinkal (30-Aug-2019) -- End
                                    Exit Sub
                                End If
                            End If




                            'ElseIf intComputeActivityHoursInMin > CInt(CInt(drRow(0)("workinghrsinsec")) / 60) Then
                            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 37, "Total activity hours cannot be greater than shift hours."), Me)
                            '    CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = strHours
                            '    CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = strMinutes
                            '    Exit Sub


                        End If

                        'Pinkal (13-Aug-2019) -- End

                        If btnSave.Enabled = False Then btnSave.Enabled = True

                        'Pinkal (30-Aug-2019) -- Start
                        'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                        If btnEdit.Enabled = False Then btnEdit.Enabled = True
                        'Pinkal (30-Aug-2019) -- End

                    End If
                ElseIf CBool(Session("AllowToExceedTimeAssignedToActivity")) = False Then

                    'Pinkal (13-Aug-2019) -- Start
                    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                    If CBool(Session("AllowEmpAssignedProjectExceedTime")) Then
                        Dim objExemptEmp As New clstsexemptemployee_tran
                        If objExemptEmp.isExist(CInt(cboEmployee.SelectedValue), -1) Then
                            SetTotalActivityHours(mdtActivity, intHoursInMin, intHours, intMinutes, item)
                            dgvTimesheet.DataSource = mdtActivity
                            dgvTimesheet.DataBind()
                            objExemptEmp = Nothing
                            Exit Sub
                            'Pinkal (30-Aug-2019) -- Start
                            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                        Else
                            If CInt(mdtActivity.Rows(item.ItemIndex).Item("AssignedActivityHrsInMin")) < intHoursInMin Then 'workinghrsinsec=shifthours
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "This employee does not allow to do this activity more than define activity percentage."), Me)
                                CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = strHours
                                CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = strMinutes
                                Exit Sub
                            ElseIf ValidateProjectHrs(item.ItemIndex, intHoursInMin) = False Then
                                If btnSave.Visible Then btnSave.Enabled = False
                                If btnEdit.Visible Then btnEdit.Enabled = False
                                Exit Sub
                            End If
                            If btnSave.Enabled = False Then btnSave.Enabled = True
                            If btnEdit.Enabled = False Then btnEdit.Enabled = True
                            'Pinkal (30-Aug-2019) -- End
                        End If
                        objExemptEmp = Nothing
                    End If

                    'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    '    If CInt(mdtActivity.Rows(item.ItemIndex).Item("AssignedActivityHrsInMin")) < intHoursInMin Then
                    '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "This employee does not allow to do this activity more than define activity percentage."), Me)
                    '        CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = strHours
                    '        CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = strMinutes
                    '        Exit Sub
                    '    End If
                    'End If

                    'Pinkal (13-Aug-2019) -- End


                End If

            End If


            'Pinkal (13-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.

            'mdtActivity.Rows(item.ItemIndex).Item("ActivityHoursInMin") = intHoursInMin
            'mdtActivity.AcceptChanges()
            'intTotHoursInMin = CInt(mdtActivity.Compute("SUM(ActivityHoursInMin)", "budgetunkid <> -999"))
            'decActivityHours = CDec(CalculateTime(True, intTotHoursInMin * 60))

            'If intTotHoursInMin >= (24 * 60) Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Invalid Total working hrs.Reason:Total Working hrs cannot greater than or equal to 24. "), Me)
            '    CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Focus()
            '    Exit Sub
            'End If

            'mdtActivity.Rows(item.ItemIndex).Item("ActivityHours") = intHours.ToString("#00") & ":" & intMinutes.ToString("#00")
            'mdtActivity.Rows(item.ItemIndex).Item("ActivityHoursInMin") = intHoursInMin
            'Dim dRow As DataRow() = mdtActivity.Select("budgetunkid = -999")
            'If dRow.Length > 0 Then
            '    dRow(0).Item("ActivityHours") = decActivityHours.ToString("#00.00").Replace(".", ":")
            'End If
            'dRow(0).AcceptChanges()
            SetTotalActivityHours(mdtActivity, intHoursInMin, intHours, intMinutes, item)
            'Pinkal (13-Aug-2019) -- End


            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            If CBool(Session("AllowOverTimeToEmpTimesheet")) Then

                'Pinkal (16-May-2018) -- Start
                'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
                'Dim dtTable As DataTable = GetProjectDetails(CInt(cboEmployee.SelectedValue), mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, mdtActivity.Rows(item.ItemIndex)("Project").ToString() _
                '                                                           , mdtActivity.Rows(item.ItemIndex)("Donor").ToString(), CInt(mdtActivity.Rows(item.ItemIndex)("fundactivityunkid")), mdtActivity.Rows(item.ItemIndex)("Activity").ToString() _
                '                                                           , CDbl(mdtActivity.Rows(item.ItemIndex)("percentage")))


                Dim dtTable As DataTable = GetProjectDetails(CInt(cboEmployee.SelectedValue), mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, mdtActivity.Rows(item.ItemIndex)("Project").ToString() _
                                                                                           , mdtActivity.Rows(item.ItemIndex)("Donor").ToString(), CInt(mdtActivity.Rows(item.ItemIndex)("fundactivityunkid")), mdtActivity.Rows(item.ItemIndex)("Activity").ToString() _
                                                                           , CDbl(mdtActivity.Rows(item.ItemIndex)("percentage")), mdtPStartDate, mdtPEndDate)
                'Pinkal (16-May-2018) -- End


                Dim mstrProjectedTotalHrs As String = "00:00"
                Dim mstrRemainingBalance As String = "00:00"
                Dim mintRemainingBalance As Integer = 0
                Dim mblnOvertime As Boolean = False
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    mstrProjectedTotalHrs = dtTable.Rows(0)("Hours").ToString()
                    mintRemainingBalance = CalculateSecondsFromHrs(dtTable.Rows(4)("Hours").ToString())
                    Dim mintCurrentBalance = CalculateSecondsFromHrs(CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text & ":" & CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text)
                    If CBool(dtTable.Rows(dtTable.Rows.Count - 1)("Hours")) Then
                        mintRemainingBalance = mintRemainingBalance + mintCurrentBalance
                    Else
                        If mintRemainingBalance < mintCurrentBalance Then
                            mblnOvertime = True
                        End If
                        mintRemainingBalance = mintCurrentBalance - mintRemainingBalance
                    End If
                    mstrRemainingBalance = CalculateTime(True, mintRemainingBalance).ToString().Replace(".", ":")
                End If

                If CBool(dtTable.Rows(dtTable.Rows.Count - 1)("Hours")) OrElse mblnOvertime Then
                    Language.setLanguage(mstrModuleName)
                    popupConfirmYesNo.Title = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
                    popupConfirmYesNo.Message = Language.getMessage(mstrModuleName, 42, "Are you sure want to exceed total projected activity hrs,") & vbCrLf & Language.getMessage(mstrModuleName, 43, "Projected hours will be") & " " & mstrProjectedTotalHrs _
                                                 & " " & Language.getMessage(mstrModuleName, 44, "hours.") & vbCrLf & Language.getMessage(mstrModuleName, 45, "Acummulated Overtime hours will be") & " " & mstrRemainingBalance & " " & Language.getMessage(mstrModuleName, 44, "hours.")
                    mstrCommandName = "OverTimeConfirmation"
                    xCurrentItemIndex = item.ItemIndex
                    popupConfirmYesNo.Show()
                End If

            End If

            'Pinkal (28-Mar-2018) -- End

            dgvTimesheet.DataSource = mdtActivity
            dgvTimesheet.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function isTimesheetExists(ByVal item As DataGridItem) As Boolean
        Try
            objBudgetTimesheet = New clsBudgetEmp_timesheet

            Return objBudgetTimesheet.isExist(CInt(cboEmployee.SelectedValue), _
                                              CInt(item.Cells(dis("objdgcolhEmpActivityunkid")).Text), _
                                              dtpDate.GetDate.Date, Nothing, mintEmptimesheetunkid)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SetVisibility()
        Try
            btnSave.Enabled = CBool(Session("AllowToAddEmployeeBudgetTimesheet"))
            btnEdit.Enabled = CBool(Session("AllowToEditEmployeeBudgetTimesheet"))

            Dim dt = dgvEmpTimesheet.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvEmpTimesheet.Columns.IndexOf(x))
            dgvEmpTimesheet.Columns(dt("btnEdit")).Visible = CBool(Session("AllowToEditEmployeeBudgetTimesheet"))
            dgvEmpTimesheet.Columns(dt("btnDelete")).Visible = CBool(Session("AllowToDeleteEmployeeBudgetTimesheet"))
            dgvEmpTimesheet.Columns(dt("btnCancel")).Visible = CBool(Session("AllowToCancelEmployeeBudgetTimesheet"))
            dt = Nothing

            lnkViewPendingSubmitApproval.Enabled = CBool(Session("AllowToViewPendingEmpBudgetTimesheetSubmitForApproval"))
            lnkViewCompletedSubmitApproval.Enabled = CBool(Session("AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval"))
            lnkGlobalDeleteTimesheet.Enabled = CBool(Session("AllowToDeleteEmployeeBudgetTimesheet"))
            lnkGlobalCancelTimesheet.Enabled = CBool(Session("AllowToCancelEmployeeBudgetTimesheet"))

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetVisibility:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    'Pinkal (04-May-2018) -- Start
    'Enhancement - Budget timesheet Enhancement for MST/THPS.

    Private Function ValidateProjectHrs(ByVal xRowIndex As Integer, ByVal xActivityHrs As Integer) As Boolean
        Try

            'Pinkal (16-May-2018) -- Start
            'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.

            'Dim dtTable As DataTable = GetProjectDetails(CInt(cboEmployee.SelectedValue), mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, mdtActivity.Rows(xRowIndex)("Project").ToString() _
            '                                                                                  , mdtActivity.Rows(xRowIndex)("Donor").ToString(), CInt(mdtActivity.Rows(xRowIndex)("fundactivityunkid")), mdtActivity.Rows(xRowIndex)("Activity").ToString() _
            '                                                                                  , CDbl(mdtActivity.Rows(xRowIndex)("percentage")))


            Dim dtTable As DataTable = GetProjectDetails(CInt(cboEmployee.SelectedValue), mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, mdtActivity.Rows(xRowIndex)("Project").ToString() _
                                                                                              , mdtActivity.Rows(xRowIndex)("Donor").ToString(), CInt(mdtActivity.Rows(xRowIndex)("fundactivityunkid")), mdtActivity.Rows(xRowIndex)("Activity").ToString() _
                                                                                              , CDbl(mdtActivity.Rows(xRowIndex)("percentage")), mdtPStartDate, mdtPEndDate)
            'Pinkal (16-May-2018) -- End



            Dim mstrMonthlyProjectHrs As String = "00:00"
            Dim mstrRemainingBalance As String = "00:00"
            Dim mstrTotalHrsEntries As String = "00:00"
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mstrMonthlyProjectHrs = dtTable.Rows(0)("Hours").ToString()  'Remaining Balance
                mstrRemainingBalance = dtTable.Rows(4)("Hours").ToString()  'Remaining Balance
                mstrTotalHrsEntries = dtTable.Rows(3)("Hours").ToString()  'Total Actual(running) number of hours (only Entries)
            End If

            Dim mintWorkingDays As Integer = 0
            Dim mdblWorkingHours As Double = 0
            Dim objEmpshiftTran As New clsEmployee_Shift_Tran

            objEmpshiftTran.GetTotalWorkingHours(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                    , Session("UserAccessModeSetting").ToString(), CInt(cboEmployee.SelectedValue), dtpDate.GetDate.Date, dtpToDate.GetDate.Date _
                                                                                                    , mintWorkingDays, mdblWorkingHours, False)

            objEmpshiftTran = Nothing


            'Pinkal (30-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
            If mintEmptimesheetunkid <= 0 Then
                If CalculateSecondsFromHrs(mstrMonthlyProjectHrs) < (mintWorkingDays * xActivityHrs * 60) + CalculateSecondsFromHrs(mstrTotalHrsEntries) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 40, "This Project Activity has only") & " " & mstrRemainingBalance & " " & Language.getMessage(mstrModuleName, 41, "Hours remaining."), Me)
                    Return False
                End If
            Else
                If CalculateSecondsFromHrs(mstrMonthlyProjectHrs) < (mintWorkingDays * xActivityHrs * 60) + (CalculateSecondsFromHrs(mstrTotalHrsEntries) - (CInt(mdtActivity.Rows(xRowIndex)("AssignedActivityHrsInMin")) * 60)) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 40, "This Project Activity has only") & " " & mstrRemainingBalance & " " & Language.getMessage(mstrModuleName, 41, "Hours remaining."), Me)
                    Return False
                End If
            End If
            'Pinkal (30-Aug-2019) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (04-May-2018) -- End


    'Pinkal (13-Aug-2019) -- Start
    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
    Private Sub SetTotalActivityHours(ByVal mdtActivity As DataTable, ByVal intHoursInMin As Integer, ByVal intHours As Integer, ByVal intMinutes As Integer, ByVal item As DataGridItem)
        Dim intTotHoursInMin As Integer = 0
        Dim decActivityHours As Decimal = 0
        Try
            mdtActivity.Rows(item.ItemIndex).Item("ActivityHoursInMin") = intHoursInMin
            mdtActivity.AcceptChanges()
            intTotHoursInMin = CInt(mdtActivity.Compute("SUM(ActivityHoursInMin)", "budgetunkid <> -999"))
            decActivityHours = CDec(CalculateTime(True, intTotHoursInMin * 60))

            If intTotHoursInMin >= (24 * 60) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Invalid Total working hrs.Reason:Total Working hrs cannot greater than or equal to 24. "), Me)
                CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Focus()
                Exit Sub
            End If

            mdtActivity.Rows(item.ItemIndex).Item("ActivityHours") = intHours.ToString("#00") & ":" & intMinutes.ToString("#00")
            mdtActivity.Rows(item.ItemIndex).Item("ActivityHoursInMin") = intHoursInMin
            Dim dRow As DataRow() = mdtActivity.Select("budgetunkid = -999")
            If dRow.Length > 0 Then
                dRow(0).Item("ActivityHours") = decActivityHours.ToString("#00.00").Replace(".", ":")
            End If
            dRow(0).AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (13-Aug-2019) -- End



#End Region

#Region " ComboBox's Events "

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Dim objPeriod As New clscommom_period_Tran

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
                'Pinkal (05-Jun-2017) -- Start
                'Enhancement - Implemented TnA Period for PSI Malawi AS Per Suzan's Request .
                'dtpDate.SetDate = CDate(objPeriod._Start_Date)
                'mdtPeriodStartDate = CDate(objPeriod._Start_Date).Date
                'mdtPeriodEndDate = CDate(objPeriod._End_Date).Date
                dtpDate.SetDate = CDate(objPeriod._TnA_StartDate).Date

                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.

                'Pinkal (05-Dec-2017) -- Start
                'Enhancement - In Budget Timesheet, Employee/User can't able to apply for future dates.
                'dtpToDate.SetDate = CDate(objPeriod._TnA_EndDate).Date
                'Pinkal (13-Oct-2017) -- End

                'mdtPeriodStartDate = CDate(objPeriod._TnA_StartDate).Date
                'mdtPeriodEndDate = CDate(objPeriod._TnA_EndDate).Date
                'Pinkal (05-Jun-2017) -- End

                Dim mdtEndDate As DateTime = ConfigParameter._Object._CurrentDateAndTime.Date

                If mdtEndDate.Date >= CDate(objPeriod._TnA_EndDate).Date Then
                    mdtEndDate = CDate(objPeriod._TnA_EndDate).Date
                End If

                dtpToDate.SetDate = mdtEndDate.Date
                mdtPeriodStartDate = CDate(objPeriod._TnA_StartDate).Date
                mdtPeriodEndDate = mdtEndDate.Date
                'Pinkal (05-Dec-2017) -- End

                'Pinkal (16-May-2018) -- Start
                'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
                mdtPStartDate = CDate(objPeriod._TnA_StartDate).Date
                mdtPEndDate = CDate(objPeriod._TnA_EndDate).Date
                'Pinkal (16-May-2018) -- End


            Else
                dtpDate.SetDate = ConfigParameter._Object._CurrentDateAndTime

                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
                'Pinkal (13-Oct-2017) -- End

                mdtPeriodStartDate = CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)).Date
                mdtPeriodEndDate = CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)).Date

                'Pinkal (16-May-2018) -- Start
                'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
                mdtPStartDate = CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)).Date
                mdtPEndDate = CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)).Date
                FillEmpActivityList()
                FillList()
                'Pinkal (16-May-2018) -- End


            End If

            'Pinkal (31-Oct-2017) -- Start
            'Enhancement - Solve Budget Timesheet Issue.
            Dim objEmployee As New clsEmployee_Master
            Dim dsList As DataSet = Nothing

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), mdtPeriodStartDate, mdtPeriodEndDate, _
                                                     CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", True, _
                                                     , , , , , , , , , , , , , , , , , )

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                    .SelectedValue = "0"
                End With

            ElseIf (CInt(Session("LoginBy"))) = Global.User.en_loginby.Employee Then
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()

                Call cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())
            End If

            'Pinkal (31-Oct-2017) -- End


            dtpDate_TextChanged(dtpDate, New EventArgs())

            Call GetEmployeeBudgetCodes()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try

            'Pinkal (31-Oct-2017) -- Start
            'Enhancement - Solve Budget Timesheet Issue.
            Dim mdtDate As Date = Nothing
            Dim mdtToDate As Date = Nothing
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
                mdtDate = CDate(objPeriod._TnA_StartDate).Date

                'Pinkal (05-Dec-2017) -- Start
                'Enhancement - In Budget Timesheet, Employee/User can't able to apply for future dates.
                'mdtToDate = CDate(objPeriod._TnA_EndDate).Date
                mdtToDate = ConfigParameter._Object._CurrentDateAndTime.Date
                If mdtToDate >= CDate(objPeriod._TnA_EndDate).Date Then
                    mdtToDate = CDate(objPeriod._TnA_EndDate).Date
                End If
                'Pinkal (05-Dec-2017) -- End

                dtpDate.SetDate = mdtDate.Date
                dtpToDate.SetDate = mdtToDate.Date

                'Pinkal (16-May-2018) -- Start
                'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
                mdtPStartDate = CDate(objPeriod._TnA_StartDate).Date
                mdtPEndDate = CDate(objPeriod._TnA_EndDate).Date
                'Pinkal (16-May-2018) -- End

                objPeriod = Nothing

            Else

                'Pinkal (16-May-2018) -- Start
                'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
                mdtDate = CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)).Date
                mdtToDate = CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)).Date
                'Pinkal (16-May-2018) -- End
            End If
            'Pinkal (31-Oct-2017) -- End

            If CInt(cboEmployee.SelectedValue) > 0 Then


                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.

                Dim objemployee As New clsEmployee_Master

                objemployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

                'Pinkal (31-Oct-2017) -- Start
                'Enhancement - Solve Budget Timesheet Issue.

                If objemployee._Reinstatementdate <> Nothing Then

                    If objemployee._Appointeddate.Date < objemployee._Reinstatementdate.Date AndAlso (objemployee._Reinstatementdate.Date >= dtpDate.GetDate.Date AndAlso objemployee._Reinstatementdate.Date <= dtpToDate.GetDate.Date) Then
                        mdtDate = objemployee._Reinstatementdate.Date
                    Else
                        mdtDate = dtpDate.GetDate.Date
                    End If

                ElseIf (objemployee._Appointeddate.Date >= dtpDate.GetDate.Date AndAlso objemployee._Appointeddate.Date <= dtpToDate.GetDate.Date) Then
                    mdtDate = objemployee._Appointeddate.Date
                    'If objemployee._Appointeddate.Date > dtpDate.GetDate.Date Then
                    '    mdtDate = objemployee._Appointeddate.Date
                    'ElseIf objemployee._Reinstatementdate.Date > dtpDate.GetDate.Date Then
                    '    mdtDate = objemployee._Reinstatementdate.Date
                    'Else
                    '    mdtDate = dtpDate.GetDate.Date
                    'End If
                    'Else
                End If

                If objemployee._Termination_From_Date.Date <> Nothing OrElse objemployee._Empl_Enddate.Date <> Nothing Then
                    If objemployee._Termination_From_Date.Date <> Nothing AndAlso (objemployee._Termination_From_Date.Date >= dtpDate.GetDate.Date AndAlso objemployee._Termination_From_Date.Date <= dtpToDate.GetDate.Date) Then
                        mdtToDate = objemployee._Termination_From_Date.Date
                    ElseIf objemployee._Empl_Enddate.Date <> Nothing AndAlso (objemployee._Empl_Enddate.Date >= dtpDate.GetDate.Date AndAlso objemployee._Empl_Enddate.Date <= dtpToDate.GetDate.Date) Then
                        mdtToDate = objemployee._Empl_Enddate.Date
                    End If
                ElseIf (objemployee._Termination_To_Date.Date >= dtpDate.GetDate.Date AndAlso objemployee._Termination_To_Date.Date <= dtpToDate.GetDate.Date) Then
                    mdtToDate = objemployee._Termination_To_Date.Date
                End If


                dtpDate.SetDate = mdtDate

                'Pinkal (31-Oct-2017) -- Start
                'Enhancement - Solve Budget Timesheet Issue.
                dtpToDate.SetDate = mdtToDate
                'Pinkal (31-Oct-2017) -- End


                mdtPeriodStartDate = dtpDate.GetDate.Date
                mdtPeriodEndDate = dtpToDate.GetDate.Date

                objemployee = Nothing

                'Pinkal (13-Oct-2017) -- End

                FillList()
                FillEmpActivityList()
            Else
                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                If CInt(cboPeriod.SelectedValue) > 0 Then
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
                    mdtPeriodStartDate = CDate(objPeriod._TnA_StartDate).Date

                    'Pinkal (05-Dec-2017) -- Start
                    'Enhancement - In Budget Timesheet, Employee/User can't able to apply for future dates.
                    'mdtPeriodEndDate = CDate(objPeriod._TnA_EndDate).Date
                    mdtToDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    If mdtToDate >= CDate(objPeriod._TnA_EndDate).Date Then
                        mdtToDate = CDate(objPeriod._TnA_EndDate).Date
                    End If
                    mdtPeriodEndDate = mdtToDate.Date
                    'Pinkal (05-Dec-2017) -- End

                    'Pinkal (16-May-2018) -- Start
                    'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
                    mdtPStartDate = CDate(objPeriod._TnA_StartDate).Date
                    mdtPEndDate = CDate(objPeriod._TnA_EndDate).Date
                    'Pinkal (16-May-2018) -- End

                    objPeriod = Nothing
                End If


                dgvEmpTimesheet.DataSource = New List(Of String)
                dgvEmpTimesheet.DataBind()

                'Pinkal (13-Oct-2017) -- End
                dgvTimesheet.DataSource = New List(Of String)
                dgvTimesheet.DataBind()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (06-Jan-2023) -- Start
    '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
    Protected Sub cboCostcenter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cboCostcenter As DropDownList = CType(sender, DropDownList)
            Dim item As DataGridItem = CType(cboCostcenter.NamingContainer, DataGridItem)
            mdtActivity.Rows(item.ItemIndex).Item("costcenterunkid") = cboCostcenter.SelectedValue
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (06-Jan-2023) -- End


#End Region

#Region " DatePicker's Events "

    Protected Sub dtpDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDate.TextChanged, dtpToDate.TextChanged
        Try
            Dim dsList As New DataSet
            Dim objEmployee As New clsEmployee_Master
            Dim objBudgetTimesheet As New clsBudgetEmp_timesheet


            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            'If CInt(cboPeriod.SelectedValue) > 0 AndAlso CDate(dtpDate.GetDate).Date <> Nothing Then
            'If CDate(dtpDate.GetDate).Date < mdtPeriodStartDate OrElse CDate(dtpDate.GetDate).Date > mdtPeriodEndDate Then
            If CInt(cboPeriod.SelectedValue) > 0 AndAlso (dtpDate.GetDate.Date <> Nothing AndAlso dtpToDate.GetDate.Date <> Nothing) Then
                If (dtpDate.GetDate.Date < mdtPeriodStartDate OrElse dtpDate.GetDate.Date > mdtPeriodEndDate) OrElse (dtpToDate.GetDate.Date < mdtPeriodStartDate OrElse dtpToDate.GetDate.Date > mdtPeriodEndDate) Then
                    'Pinkal (13-Oct-2017) -- End
                    DisplayMessage.DisplayMessage("Date must be in the range from " & mdtPeriodStartDate & " to " & mdtPeriodEndDate, Me)

                    'Pinkal (13-Oct-2017) -- Start
                    'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                    'dtpDate.SetDate = Nothing
                    'dtpToDate.SetDate = Nothing
                    'Pinkal (13-Oct-2017) -- End

                    dgvEmpTimesheet.DataSource = New List(Of String)
                    dgvEmpTimesheet.DataBind()
                    Exit Sub
                Else
                    ''Pinkal (31-Oct-2017) -- Start
                    ''Enhancement - Solve Budget Timesheet Issue.
                    'dgvTimesheet.DataSource = New List(Of String)
                    'dgvTimesheet.DataBind()
                    ''Pinkal (31-Oct-2017) -- End
                End If
            End If

            ''Pinkal (31-Oct-2017) -- Start
            ''Enhancement - Solve Budget Timesheet Issue.
            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
            '    dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
            '                                         CInt(Session("CompanyUnkId")), mdtPeriodStartDate, mdtPeriodEndDate, _
            '                                         CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", True, _
            '                                         , , , , , , , , , , , , , , , , , )

            '    With cboEmployee
            '        .DataValueField = "employeeunkid"
            '        .DataTextField = "EmpCodeName" 'Nilay (10 Jan 2017)
            '        .DataSource = dsList.Tables("List")
            '        .DataBind()
            '        .SelectedValue = "0"
            '    End With

            'ElseIf (CInt(Session("LoginBy"))) = Global.User.en_loginby.Employee Then
            '    Dim objglobalassess = New GlobalAccess
            '    objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
            '    cboEmployee.DataSource = objglobalassess.ListOfEmployee
            '    cboEmployee.DataTextField = "loginname"
            '    cboEmployee.DataValueField = "employeeunkid"
            '    cboEmployee.DataBind()

            '    Call cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())
            'End If

            ''Pinkal (31-Oct-2017) -- End

            If CInt(cboEmployee.SelectedValue) > 0 Then
                'objLblValue.Text = Language.getMessage(mstrModuleName, 12, "Total Working Hrs:") & " " & _
                'CDec(CalculateTime(True, objBudgetTimesheet.GetEmpWorkingHrsForTheDay(CDate(dtpDate.GetDate).Date, CInt(cboEmployee.SelectedValue)) * 60)).ToString("#00.00").Replace(".", ":")
                'Call cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())
                FillEmpActivityList()
                'Pinkal (04-May-2018) -- Start
                'Enhancement - Budget timesheet Enhancement for MST/THPS.
                If btnSave.Enabled = False Then btnSave.Enabled = True
                'Pinkal (04-May-2018) -- End
            Else
                'objLblValue.Text = Language.getMessage(mstrModuleName, 12, "Total Working Hrs:") & " 00:00"
                'dgvEmpTimesheet.DataSource = New List(Of String)
                'dgvEmpTimesheet.DataBind()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dtpDate_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region " TextBox's Events "

    Protected Sub TxtHours_TextChanged1(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtHours As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtHours.NamingContainer, DataGridItem)

            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
            'If txtHours.Text.Trim.Length > 0 AndAlso CInt(txtHours.Text) > 23 Then 'Nilay (07 Feb 2017) -- [txtHours.Text.Trim.Length > 0]
            '    DisplayMessage.DisplayMessage("Invalid Activity Hours. Reason: Activity Hours cannot be greater than or equal to 24.", Me)
            '    txtHours.Text = "00"
            '    txtHours.Focus()
            '    Exit Sub
            'ElseIf txtHours.Text.Trim.Length <= 0 Then
            '    txtHours.Text = "00"
            '    txtHours.Focus()
            '    Exit Sub
            'Else
            '    Call CalculateActivityHours(CInt(txtHours.Text), CInt(CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text), item)
            'End If
            If txtHours.Text.Trim.Length <= 0 Then
                txtHours.Text = "00"
            End If

            Call CalculateActivityHours(CInt(txtHours.Text.Trim), CInt(CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text), item)
            'Nilay (07 Feb 2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("TxtHours_TextChanged1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub TxtMinutes_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtMinutes As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtMinutes.NamingContainer, DataGridItem)

            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
            'If txtMinutes.Text.Trim.Length > 0 AndAlso CInt(txtMinutes.Text) > 59 Then 'Nilay (07 Feb 2017) -- [txtMinutes.Text.Trim.Length > 0]
            '    DisplayMessage.DisplayMessage("Invalid Activity Minutes. Reason: Activity Minutes cannot be greater than or equal to 60.", Me)
            '    txtMinutes.Text = "00"
            '    txtMinutes.Focus()
            '    Exit Sub
            'ElseIf txtMinutes.Text.Trim.Length <= 0 Then
            '    txtMinutes.Text = "00"
            '    txtMinutes.Focus()
            '    Exit Sub
            'Else
            '    Call CalculateActivityHours(CInt(CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text), CInt(txtMinutes.Text), item)
            'End If
            If txtMinutes.Text.Trim.Length <= 0 Then
                txtMinutes.Text = "00"
            End If

            Call CalculateActivityHours(CInt(CType(item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text), CInt(txtMinutes.Text.Trim), item)
            'Nilay (07 Feb 2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("TxtMinutes_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub TxtDescription_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtDescription As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtDescription.NamingContainer, DataGridItem)

            mdtActivity.Rows(item.ItemIndex).Item("description") = txtDescription.Text
            mdtActivity.AcceptChanges()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("TxtDescription_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Protected Sub txtHours_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try

    '        If CType(sender, TextBox).ID.ToUpper() = "TXTHOURS" Then

    '            If CInt(CType(sender, TextBox).Text) > 23 Then
    '                DisplayMessage.DisplayMessage("Invalid Total working hrs. Reason: Total Working hrs cannot greater than or equal to 24.", Me)
    '                CType(sender, TextBox).Text = "00"
    '                CType(sender, TextBox).Focus()
    '                Exit Sub
    '            End If

    '        ElseIf CType(sender, TextBox).ID.ToUpper() = "TXTMINUTES" Then

    '            If CInt(CType(sender, TextBox).Text) > 59 Then
    '                DisplayMessage.DisplayMessage("Invalid Total working Mins. Reason: Working Mins cannot greater than 59.", Me)
    '                CType(sender, TextBox).Text = "00"
    '                CType(sender, TextBox).Focus()
    '                Exit Sub
    '            End If

    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("txtHours_TextChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

#End Region

#Region " DataGrid's Events "

    Protected Sub dgvEmpTimesheet_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvEmpTimesheet.ItemCommand
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                Dim dt = dgvEmpTimesheet.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvEmpTimesheet.Columns.IndexOf(x))

                mstrCommandName = e.CommandName.ToUpper

                If e.CommandName.ToUpper = "EDIT" Then


                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    If e.Item.Cells(dt("objdgcolhEmpTimesheetID")).Text <> "&nbsp;" AndAlso IsDBNull(e.Item.Cells(dt("objdgcolhEmpTimesheetID")).Text) = False Then
                        mintEmptimesheetunkid = CInt(e.Item.Cells(dt("objdgcolhEmpTimesheetID")).Text)
                    End If

                    mdtActivityDate = CDate(e.Item.Cells(dt("dgcolhDate")).Text)

                    If CInt(e.Item.Cells(dt("objdgcolhActivityHoursInMin")).Text) > 0 Then

                        If CBool(e.Item.Cells(dt("objdgcolhIsSubmitForApproval")).Text) = True AndAlso CInt(e.Item.Cells(dt("objdgcolhStatusId")).Text) = 2 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 30, "You can't Edit this timesheet. Reason: This timesheet is already submitted for approval."), Me)
                            Exit Sub
                        End If

                        If CInt(e.Item.Cells(dt("objdgcolhStatusId")).Text) = 1 Then 'APPROVED
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "You can't Edit this timesheet. Reason: This timesheet is already approved."), Me)
                            Exit Sub
                        ElseIf CInt(e.Item.Cells(dt("objdgcolhStatusId")).Text) = 3 Then 'REJECTED
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "You can't Edit this timesheet. Reason: This timesheet is already rejected."), Me)
                            Exit Sub
                        ElseIf CInt(e.Item.Cells(dt("objdgcolhStatusId")).Text) = 2 Then 'PENDING
                            Dim objApproval As New clstsemptimesheet_approval

                            'Pinkal (13-Aug-2018) -- Start
                            'Enhancement - Changes For PACT [Ref #249,252]
                            'Dim dsPendingList As DataSet = objApproval.GetList("List", CStr(Session("Database_Name")), mdtActivityDate, CStr(Session("DateFormat")), _
                            '                                                  CInt(Session("UserId")), False, Nothing, True, mintEmptimesheetunkid, False, _
                            '                                                  "tsemptimesheet_approval.statusunkid <> 2 ")

                            Dim dsPendingList As DataSet = objApproval.GetList("List", CStr(Session("Database_Name")), mdtActivityDate, CStr(Session("DateFormat")), _
                                                                                  CInt(Session("UserId")), False, CBool(Session("AllowOverTimeToEmpTimesheet")), Nothing, True, mintEmptimesheetunkid, False, _
                                                                               "tsemptimesheet_approval.statusunkid <> 2 ")
                            'Pinkal (13-Aug-2018) -- End


                            If dsPendingList IsNot Nothing AndAlso dsPendingList.Tables(0).Rows.Count > 0 Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "You can't edit this timesheet. Reason: This timesheet is already approved by some of the approver(s) of this employee."), Me)
                                Exit Sub
                            End If
                            dsPendingList = Nothing
                            objApproval = Nothing
                        End If

                    End If

                    'Pinkal (28-Jul-2018) -- End

                    cboPeriod.Enabled = False
                    dtpDate.Enabled = False
                    cboEmployee.Enabled = False
                    btnEdit.Visible = True
                    btnSave.Visible = False
                    dtpToDate.Enabled = False


                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    If e.Item.Cells(dt("objdgcolhPeriodID")).Text <> "&nbsp;" AndAlso CInt(e.Item.Cells(dt("objdgcolhPeriodID")).Text) > 0 Then
                        cboPeriod.SelectedValue = CStr(CInt(e.Item.Cells(dt("objdgcolhPeriodID")).Text))
                    End If
                    'Pinkal (28-Jul-2018) -- End

                    dtpDate.SetDate = CDate(e.Item.Cells(dt("dgcolhDate")).Text)
                    dtpToDate.SetDate = CDate(e.Item.Cells(dt("dgcolhDate")).Text)
                    cboEmployee.SelectedValue = CStr(CInt(e.Item.Cells(dt("objdgcolhEmployeeID")).Text))

                    'Pinkal (06-Jan-2023) -- Start
                    '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                    If dgvEmpTimesheet.Columns(getColumnId_Datagrid(dgvEmpTimesheet, "dgcolhCostCenterList", False, True)).Visible AndAlso CInt(dgvEmpTimesheet.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhCostCenterIdList", False, True)).Text) > 0 Then   'AndAlso CInt(dgvEmpTimesheetList.Rows(e.RowIndex).Cells(objdgcolhCostCenterIdList.Index).Value) > 0 Then
                        mintEditedCostCenterID = CInt(dgvEmpTimesheet.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgvEmpTimesheet, "objdgcolhCostCenterIdList", False, True)).Text)
                    End If
                    'Pinkal (06-Jan-2023) -- End


                    Call FillEmpActivityList()

                    ScriptManager.RegisterStartupScript(Page, GetType(Page), "ScrollToADiv", "setTimeout(scrollToDiv, 1);", True)

                ElseIf e.CommandName.ToUpper = "DELETE" Then

                    mintEmptimesheetunkid = CInt(e.Item.Cells(dt("objdgcolhEmpTimesheetID")).Text)
                    mdtActivityDate = CDate(e.Item.Cells(dt("dgcolhDate")).Text)

                    'Nilay (10 Jan 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification
                    mintEmployeeId = CInt(e.Item.Cells(dt("objdgcolhEmployeeID")).Text)

                    'Pinkal (13-Oct-2017) -- Start
                    'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                    If CBool(e.Item.Cells(dt("objdgcolhIsSubmitForApproval")).Text) = True AndAlso CInt(e.Item.Cells(dt("objdgcolhStatusId")).Text) = 2 Then
                        'If CBool(e.Item.Cells(dt("objdgcolhIsSubmitForApproval")).Text) = True Then
                        'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 34, "You can't Delete this timesheet. Reason: This timesheet is already submitted for approval."), Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "You can't Delete this timesheet. Reason: This timesheet is already submitted for approval."), Me)
                        Exit Sub
                    End If
                    'Pinkal (13-Oct-2017) -- End

                    'Nilay (10 Jan 2017) -- End

                    If CInt(e.Item.Cells(dt("objdgcolhStatusId")).Text) = 1 Then 'APPROVED
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "You can't delete this timesheet. Reason: This timesheet is already approved."), Me)
                        Exit Sub
                    ElseIf CInt(e.Item.Cells(dt("objdgcolhStatusId")).Text) = 3 Then 'REJECTED
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "You can't delete this timesheet. Reason: This timesheet is already rejected."), Me)
                        Exit Sub
                    ElseIf CInt(e.Item.Cells(dt("objdgcolhStatusId")).Text) = 2 Then 'PENDING
                        Dim objApproval As New clstsemptimesheet_approval


                        'Pinkal (03-Aug-2018) -- Start
                        'Enhancement - Changes For B5 [Ref #289]

                        'Dim dsPendingList As DataSet = objApproval.GetList("List", CStr(Session("Database_Name")), mdtActivityDate, CStr(Session("DateFormat")), _
                        '                                                   CInt(Session("UserId")), False, Nothing, True, mintEmptimesheetunkid, False, _
                        '                                                   "tsemptimesheet_approval.statusunkid <> 2 ")

                        Dim dsPendingList As DataSet = objApproval.GetList("List", CStr(Session("Database_Name")), mdtActivityDate, CStr(Session("DateFormat")), _
                                                                          CInt(Session("UserId")), False, CBool(Session("AllowOverTimeToEmpTimesheet")), Nothing, True, mintEmptimesheetunkid, False, _
                                                                           "tsemptimesheet_approval.statusunkid <> 2 ")

                        'Pinkal (03-Aug-2018) -- End

                        If dsPendingList IsNot Nothing AndAlso dsPendingList.Tables(0).Rows.Count > 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "You can't delete this timesheet. Reason: This timesheet is already approved by some of the approver(s) of this employee."), Me)
                            Exit Sub
                        End If
                        dsPendingList = Nothing
                        objApproval = Nothing
                    End If

                    'Call ResetValue()

                    Language.setLanguage(mstrModuleName)
                    popupConfirmYesNo.Title = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
                    popupConfirmYesNo.Message = Language.getMessage(mstrModuleName, 11, "Are you sure you want to delete this employee timesheet?")
                    popupConfirmYesNo.Show()

                ElseIf e.CommandName.ToUpper = "CANCEL" Then

                    mintEmployeeId = CInt(e.Item.Cells(dt("objdgcolhEmployeeID")).Text)
                    mintEmptimesheetunkid = CInt(e.Item.Cells(dt("objdgcolhEmpTimesheetID")).Text)
                    mdtActivityDate = CDate(e.Item.Cells(dt("dgcolhDate")).Text).Date

                    'Nilay (15 Feb 2017) -- Start
                    mintApprovedActivityHours = CInt(e.Item.Cells(dt("objdgcolhApprovedActivityHoursInMin")).Text)
                    'Nilay (15 Feb 2017) -- End

                    If CInt(e.Item.Cells(dt("objdgcolhStatusId")).Text) = 2 Then 'PENDING
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "You can't cancel this timesheet. Reason: This timesheet is in pending status."), Me)
                        Exit Sub
                    ElseIf CInt(e.Item.Cells(dt("objdgcolhStatusId")).Text) = 3 Then 'REJECTED
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "You can't cancel this timesheet. Reason: This timesheet is already rejected."), Me)
                        Exit Sub
                    End If

                    Language.setLanguage(mstrModuleName)
                    popupConfirmYesNo.Title = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
                    popupConfirmYesNo.Message = Language.getMessage(mstrModuleName, 20, "Are you sure you want to cancel this employee timesheet?")
                    popupConfirmYesNo.Show()

                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvEmpTimesheet_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvEmpTimesheet_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvEmpTimesheet.ItemDataBound
        Try
            Call SetDateFormat()

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            dgvEmpTimesheet.Columns(mdicETS("dgcolhProject")).Visible = CBool(Session("ShowBgTimesheetActivityProject"))
            dgvEmpTimesheet.Columns(mdicETS("dgcolhActivity")).Visible = CBool(Session("ShowBgTimesheetActivity"))
            'Pinkal (28-Jul-2018) -- End

            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim intVisible = dgvEmpTimesheet.Columns.Cast(Of DataGridColumn).Where(Function(x) x.Visible = True).Count

                If e.Item.ItemIndex >= 0 Then
                    If CBool(dr("IsGrp")) = True Then

                        If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then

                            e.Item.Cells(mdicETS("btnEdit")).FindControl("imgEdit").Visible = False
                            e.Item.Cells(mdicETS("btnDelete")).FindControl("ImgDelete").Visible = False
                            e.Item.Cells(mdicETS("btnCancel")).FindControl("ImgCancel").Visible = False

                            e.Item.Cells(mdicETS("dgcolhEmployee")).ColumnSpan = intVisible

                            For i = mdicETS("dgcolhEmployee") + 1 To e.Item.Cells.Count - 1
                                e.Item.Cells(i).Visible = False
                            Next

                            'Gajanan [17-Sep-2020] -- Start
                            'New UI Change
                            'e.Item.Cells(mdicETS("btnEdit")).CssClass = "GroupHeaderStyleBorderLeft"
                            'e.Item.Cells(mdicETS("btnDelete")).CssClass = "GroupHeaderStyleBorderLeft"
                            'e.Item.Cells(mdicETS("btnCancel")).CssClass = "GroupHeaderStyleBorderLeft"
                            'e.Item.Cells(mdicETS("dgcolhEmployee")).CssClass = "GroupHeaderStyleBorderLeft"

                            e.Item.Cells(mdicETS("btnEdit")).CssClass = "group-header"
                            e.Item.Cells(mdicETS("btnDelete")).CssClass = "group-header"
                            e.Item.Cells(mdicETS("btnCancel")).CssClass = "group-header"
                            e.Item.Cells(mdicETS("dgcolhEmployee")).CssClass = "group-header"
                            'Gajanan [17-Sep-2020] -- End
                            
                            e.Item.Cells(mdicETS("dgcolhEmployee")).Style.Add("text-align", "left")

                        ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then

                            e.Item.Cells(mdicETS("btnEdit")).FindControl("imgEdit").Visible = False
                            e.Item.Cells(mdicETS("btnDelete")).FindControl("ImgDelete").Visible = False
                            e.Item.Cells(mdicETS("btnCancel")).FindControl("ImgCancel").Visible = False
                            e.Item.Cells(mdicETS("dgcolhEmployee")).ColumnSpan = intVisible

                            For i = mdicETS("dgcolhEmployee") + 1 To e.Item.Cells.Count - 1
                                e.Item.Cells(i).Visible = False
                            Next

                            'Gajanan [17-Sep-2020] -- Start
                            'New UI Change
                            'e.Item.Cells(mdicETS("btnEdit")).CssClass = "GroupHeaderStyleBorderLeft"
                            'e.Item.Cells(mdicETS("btnDelete")).CssClass = "GroupHeaderStyleBorderLeft"
                            'e.Item.Cells(mdicETS("btnCancel")).CssClass = "GroupHeaderStyleBorderLeft"
                            'e.Item.Cells(mdicETS("dgcolhEmployee")).CssClass = "GroupHeaderStyleBorderLeft"

                            e.Item.Cells(mdicETS("btnEdit")).CssClass = "group-header"
                            e.Item.Cells(mdicETS("btnDelete")).CssClass = "group-header"
                            e.Item.Cells(mdicETS("btnCancel")).CssClass = "group-header"
                            e.Item.Cells(mdicETS("dgcolhEmployee")).CssClass = "group-header"
                            'Gajanan [17-Sep-2020] -- End
                            
                            e.Item.Cells(mdicETS("dgcolhEmployee")).Style.Add("text-align", "left")
                        End If
                    Else

                        'Pinkal (28-Jul-2018) -- Start
                        'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                        'If CBool(dr("isDayOff")) = True OrElse CBool(dr("isLeave")) OrElse CBool(dr("isholiday")) = True Then
                        '    e.Item.Cells(mdicETS("btnEdit")).FindControl("imgEdit").Visible = False
                        '    e.Item.Cells(mdicETS("btnDelete")).FindControl("ImgDelete").Visible = False
                        '    e.Item.Cells(mdicETS("btnCancel")).FindControl("ImgCancel").Visible = False
                        '    For i As Integer = 0 To intVisible
                        '        e.Item.Cells(i).Style.Add("color", "red")
                        '    Next
                        'Else
                        '    e.Item.Cells(mdicETS("btnEdit")).FindControl("imgEdit").Visible = True
                        '    e.Item.Cells(mdicETS("btnDelete")).FindControl("ImgDelete").Visible = True
                        '    e.Item.Cells(mdicETS("btnCancel")).FindControl("ImgCancel").Visible = True
                        'End If

                        If CBool(dr("isDayOff")) = True OrElse CBool(dr("isLeave")) OrElse CBool(dr("isholiday")) Then
                            For i As Integer = 0 To intVisible
                                e.Item.Cells(i).Style.Add("color", "red")
                            Next
                        End If

                        If CBool(dr("isDayOff")) = True OrElse CBool(dr("isLeave")) OrElse (CBool(dr("isholiday")) AndAlso CBool(Session("AllowOverTimeToEmpTimesheet")) = False) Then
                            e.Item.Cells(mdicETS("btnEdit")).FindControl("imgEdit").Visible = False
                        End If

                        If CInt(dr("ActivityHoursInMin")) <= 0 Then
                            e.Item.Cells(mdicETS("btnDelete")).FindControl("ImgDelete").Visible = False
                            e.Item.Cells(mdicETS("btnCancel")).FindControl("ImgCancel").Visible = False
                        End If

                        'Pinkal (28-Jul-2018) -- End

                    End If
                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvEmpTimesheet_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvTimesheet_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvTimesheet.ItemDataBound
        Try

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            dgvTimesheet.Columns(0).Visible = CBool(Session("ShowBgTimesheetActivityHrsDetail"))
            dgvTimesheet.Columns(dis("dgcolhEmpProject")).Visible = CBool(Session("ShowBgTimesheetActivityProject"))
            dgvTimesheet.Columns(dis("dgcolhEmpActivity")).Visible = CBool(Session("ShowBgTimesheetActivity"))
            dgvTimesheet.Columns(dis("dgcolhEmpPercentage")).Visible = CBool(Session("ShowBgTimesheetAssignedActivityPercentage"))
            'Pinkal (28-Jul-2018) -- End

            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then

                Dim mstrHours As String = e.Item.Cells(dis("dgcolhActivityHours")).Text
                If mstrHours.Trim.Length > 0 Then
                    CType(e.Item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = mstrHours.Trim.Substring(0, mstrHours.Trim.IndexOf(":"))
                    CType(e.Item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = mstrHours.Trim.Substring(mstrHours.Trim.IndexOf(":") + 1)
                Else
                    CType(e.Item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = "00"
                    CType(e.Item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = "00"
                End If

                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 200)  Working on An option to show/review % allocation of the projects..
                e.Item.Cells(dis("dgcolhEmpPercentage")).Text = CDec(e.Item.Cells(dis("dgcolhEmpPercentage")).Text).ToString("#0.00")
                'Pinkal (28-Mar-2018) -- End


                'Pinkal (06-Jan-2023) -- Start
                '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                Dim cboCostcenter As DropDownList = CType(e.Item.FindControl("cboCostcenter"), DropDownList)

                Dim objcostCenter As New clscostcenter_master
                Dim dsList As DataSet = objcostCenter.getComboList("List", True, "", "")
                Dim drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("costcenterunkid") <= 0)
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    drRow(0)("costcentername") = ""
                    drRow(0).AcceptChanges()
                End If
                With cboCostcenter
                    .DataValueField = "costcenterunkid"
                    .DataTextField = "costcentername"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedValue = e.Item.Cells(dis("objdgcolhcostcenterunkid")).Text
                End With
                'Pinkal (06-Jan-2023) -- End

                If e.Item.Cells(dis("objdgcolhEmpBudgetID")).Text = "-999" Then

                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    'CType(e.Item.Cells(dis("dgcolhEmpDescription")).FindControl("TxtDescription"), TextBox).Visible = False
                    CType(e.Item.Cells(dis("dgcolhEmpDescription")).FindControl("pnlDescription"), Panel).Visible = False
                    'Gajanan [17-Sep-2020] -- End

                    CType(e.Item.Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).ReadOnly = True
                    CType(e.Item.Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).ReadOnly = True

                    'Pinkal (06-Jan-2023) -- Start
                    '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                    If Session("CompanyGroupName").ToString().ToUpper() = "PSI MALAWI" Then
                        CType(e.Item.Cells(dis("dgcolhCostCenter")).FindControl("cboCostcenter"), DropDownList).Visible = False
                    End If
                    'Pinkal (06-Jan-2023) -- End


                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                    If dgvTimesheet.Columns(0).Visible AndAlso dgvTimesheet.Columns(dis("dgcolhEmpProject")).Visible AndAlso dgvTimesheet.Columns(dis("dgcolhEmpActivity")).Visible AndAlso dgvTimesheet.Columns(dis("dgcolhEmpPercentage")).Visible Then

                        'Pinkal (28-Mar-2018) -- Start
                        'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                        e.Item.Cells(0).Visible = False
                        'Pinkal (28-Mar-2018) -- End

                        e.Item.Cells(dis("dgcolhEmpDonor")).Visible = False
                        e.Item.Cells(dis("dgcolhEmpProject")).Visible = False

                        'Pinkal (28-Mar-2018) -- Start
                        'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                        'e.Item.Cells(dis("dgcolhEmpActivity")).ColumnSpan = 3
                        e.Item.Cells(dis("dgcolhEmpActivity")).ColumnSpan = 4
                        'Pinkal (28-Mar-2018) -- End
                        e.Item.Cells(dis("dgcolhEmpActivity")).Style.Add("text-align", "right")
                        'e.Item.Cells(dis("dgcolhEmpHours")).Style.Add("text-align", "right")

                        For i = CInt(dis("dgcolhEmpActivity")) To e.Item.Cells.Count - 1
                            e.Item.Cells(i).CssClass = "GroupHeaderStyleBorderLeft"
                        Next

                    Else
                        e.Item.Cells(0).FindControl("imgShowDetails").Visible = False

                        For i As Integer = 0 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).CssClass = "GroupHeaderStyleBorderLeft"
                        Next

                    End If
                    'Pinkal (28-Jul-2018) -- End

                Else
                    Dim txtDescription As TextBox = CType(e.Item.Cells(dis("dgcolhEmpDescription")).FindControl("TxtDescription"), TextBox)
                    txtDescription.Text = txtDescription.Text.ToString.Trim
                End If

            End If


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvTimesheet_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

    Protected Sub dgvTimesheet_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvTimesheet.ItemCommand
        Try

            'Pinkal (16-May-2018) -- Start
            'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
            'Dim dtTable As DataTable = GetProjectDetails(CInt(cboEmployee.SelectedValue), mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, e.Item.Cells(dis("dgcolhEmpProject")).Text _
            '                         , e.Item.Cells(dis("dgcolhEmpDonor")).Text, CInt(e.Item.Cells(dis("objdgcolhEmpActivityunkid")).Text), e.Item.Cells(dis("dgcolhEmpActivity")).Text _
            '                         , CDbl(e.Item.Cells(dis("dgcolhEmpPercentage")).Text))

            Dim dtTable As DataTable = GetProjectDetails(CInt(cboEmployee.SelectedValue), mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, e.Item.Cells(dis("dgcolhEmpProject")).Text _
                                     , e.Item.Cells(dis("dgcolhEmpDonor")).Text, CInt(e.Item.Cells(dis("objdgcolhEmpActivityunkid")).Text), e.Item.Cells(dis("dgcolhEmpActivity")).Text _
                                  , CDbl(e.Item.Cells(dis("dgcolhEmpPercentage")).Text), mdtPStartDate, mdtPEndDate)

            'Pinkal (16-May-2018) -- End


            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                dtTable.Rows.RemoveAt(dtTable.Rows.Count - 1)
            End If

            dgvProjectHrDetails.AutoGenerateColumns = False
            dgvProjectHrDetails.DataSource = dtTable
            dgvProjectHrDetails.DataBind()

            popupShowDetails.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvTimesheet_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (28-Mar-2018) -- End


#End Region

#Region " Button's Events "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If Validation() = False Then Exit Sub

            SetValue()


            'Pinkal (13-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.

            'If objBudgetTimesheet.Insert(Session("Database_Name").ToString() _
            '                                         , CInt(Session("UserId")) _
            '                                         , CInt(Session("Fin_year")) _
            '                                         , CInt(Session("CompanyUnkId")) _
            '                                         , Session("EmployeeAsOnDate").ToString _
            '                                         , Session("UserAccessModeSetting").ToString _
            '                                         , True, CBool(Session("IsIncludeInactiveEmp")) _
            '                                         , dtpDate.GetDate.Date, dtpToDate.GetDate.Date _
            '                                         , CBool(Session("AllowOverTimeToEmpTimesheet")) _
            '                                        , CBool(Session("AllowActivityHoursByPercentage")) _
            '                                        , CBool(Session("AllowToExceedTimeAssignedToActivity"))) = False Then

            Dim mstrExceededDates As String = ""

            If objBudgetTimesheet.Insert(Session("Database_Name").ToString() _
                                                     , CInt(Session("UserId")) _
                                                     , CInt(Session("Fin_year")) _
                                                     , CInt(Session("CompanyUnkId")) _
                                                     , Session("EmployeeAsOnDate").ToString _
                                                     , Session("UserAccessModeSetting").ToString _
                                                     , True, CBool(Session("IsIncludeInactiveEmp")) _
                                                     , dtpDate.GetDate.Date, dtpToDate.GetDate.Date _
                                                     , CBool(Session("AllowOverTimeToEmpTimesheet")) _
                                                    , CBool(Session("AllowActivityHoursByPercentage")) _
                                                      , CBool(Session("AllowToExceedTimeAssignedToActivity")) _
                                                      , CBool(Session("AllowEmpAssignedProjectExceedTime")), mstrExceededDates) = False Then

                'Pinkal (13-Aug-2019) -- End

                DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
            Else


                'Pinkal (13-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                If mstrExceededDates.Trim.Length > 0 Then
                    mstrExceededDates = mstrExceededDates.Trim.Substring(0, mstrExceededDates.Trim().Length - 1)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 47, "Timesheet was not able to be inserted on the following dates:") & "\n\n" & _
                                                                     mstrExceededDates & "\n\n" & Language.getMessage(mstrModuleName, 48, "Reason: Allocated hours are greater than shift hours."), Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Timesheet inserted Successfully."), Me)
                End If
                'Pinkal (13-Aug-2019) -- End
                FillList()

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    FillEmpActivityList()
                Else
                    dgvTimesheet.DataSource = New List(Of String)
                    dgvTimesheet.DataBind()
                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            objBudgetTimesheet = New clsBudgetEmp_timesheet

            If Validation() = False Then Exit Sub

            Call SetValue()


            'Pinkal (13-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
            Dim mstrExceededDates As String = ""
            'Pinkal (13-Aug-2019) -- End


            Dim mblnFlag As Boolean = False
            If mintEmptimesheetunkid > 0 Then


                'Pinkal (30-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                'mblnFlag = objBudgetTimesheet.Update(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), CInt(Session("CompanyUnkId")), _
                '                                                           CDate(dtpDate.GetDate).Date, CBool(Session("IsIncludeInactiveEmp")), False, CBool(Session("AllowOverTimeToEmpTimesheet")))
                mblnFlag = objBudgetTimesheet.Update(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), CInt(Session("CompanyUnkId")), _
                                                                          CDate(dtpDate.GetDate).Date, CBool(Session("IsIncludeInactiveEmp")), False, CBool(Session("AllowOverTimeToEmpTimesheet")), _
                                                                          Nothing, False, mstrExceededDates)

                'Pinkal (30-Aug-2019) -- End

            Else

                'Pinkal (13-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.

                'mblnFlag = objBudgetTimesheet.Insert(Session("Database_Name").ToString() _
                '                                     , CInt(Session("UserId")) _
                '                                     , CInt(Session("Fin_year")) _
                '                                     , CInt(Session("CompanyUnkId")) _
                '                                     , Session("EmployeeAsOnDate").ToString _
                '                                     , Session("UserAccessModeSetting").ToString _
                '                                     , True, CBool(Session("IsIncludeInactiveEmp")) _
                '                                     , dtpDate.GetDate.Date, dtpToDate.GetDate.Date _
                '                                     , CBool(Session("AllowOverTimeToEmpTimesheet")) _
                '                                    , CBool(Session("AllowActivityHoursByPercentage")) _
                '                                    , CBool(Session("AllowToExceedTimeAssignedToActivity")))

                mblnFlag = objBudgetTimesheet.Insert(Session("Database_Name").ToString() _
                                                     , CInt(Session("UserId")) _
                                                     , CInt(Session("Fin_year")) _
                                                     , CInt(Session("CompanyUnkId")) _
                                                     , Session("EmployeeAsOnDate").ToString _
                                                     , Session("UserAccessModeSetting").ToString _
                                                     , True, CBool(Session("IsIncludeInactiveEmp")) _
                                                     , dtpDate.GetDate.Date, dtpToDate.GetDate.Date _
                                                     , CBool(Session("AllowOverTimeToEmpTimesheet")) _
                                                    , CBool(Session("AllowActivityHoursByPercentage")) _
                                                  , CBool(Session("AllowToExceedTimeAssignedToActivity")) _
                                                  , CBool(Session("AllowEmpAssignedProjectExceedTime")), mstrExceededDates)

                'Pinkal (13-Aug-2019) -- End
            End If


            If mblnFlag = False Then
                DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                Exit Sub
            Else
                'Pinkal (13-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                If mstrExceededDates.Trim.Length > 0 Then
                    mstrExceededDates = mstrExceededDates.Trim.Substring(0, mstrExceededDates.Trim().Length - 1)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 47, "Timesheet was not able to be inserted on the following dates:") & "\n\n" & _
                                                                      mstrExceededDates & "\n\n" & Language.getMessage(mstrModuleName, 48, "Reason: Allocated hours are greater than shift hours."), Me)

                    'Pinkal (30-Aug-2019) -- Start
                    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                    If mintEmptimesheetunkid > 0 Then Exit Sub
                    'Pinkal (30-Aug-2019) -- End
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Timesheet inserted Successfully."), Me)
                End If
                'Pinkal (13-Aug-2019) -- End
                cboPeriod.Enabled = True
                dtpDate.Enabled = True
                cboEmployee.Enabled = True
                btnSave.Visible = True
                btnEdit.Visible = False
                dtpToDate.Enabled = True
                cboPeriod.SelectedValue = "0"
                cboPeriod.SelectedValue = objBudgetTimesheet._Periodunkid.ToString()
                cboPeriod_SelectedIndexChanged(New Object, New EventArgs())
                cboEmployee_SelectedIndexChanged(New Object, New EventArgs())
                objBudgetTimesheet = New clsBudgetEmp_timesheet
                dgvTimesheet.DataSource = New List(Of String)
                dgvTimesheet.DataBind()
                mintEmptimesheetunkid = 0
            End If

            'Pinkal (28-Jul-2018) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnEdit_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (10 Jan 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Email Notification

    'Nilay (27 Feb 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    'Protected Sub btnSubmitForAproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitForAproval.Click
    '    Try
    '        objBudgetTimesheet = New clsBudgetEmp_timesheet

    '        mdtEmpTimesheet = New DataView(mdtEmpTimesheet, "IsChecked=1 AND IsGroup=0", "", DataViewRowState.CurrentRows).ToTable

    '        If mdtEmpTimesheet.Rows.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 30, "Please tick atleast one record for further process."), Me)
    '            Exit Sub
    '        End If

    '        Dim dR As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGroup=0 AND issubmit_approval=1")
    '        If dR.Length > 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "Some of the checked employee activity(s) is already sent for approval."), Me)
    '            Exit Sub
    '        End If

    '        Dim objApproverMst As New clstsapprover_master
    '        Dim dtList As DataTable = objApproverMst.GetEmployeeApprover(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
    '                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                                                     CBool(Session("IsIncludeInactiveEmp")), -1, _
    '                                                                     CStr(cboEmployee.SelectedValue), -1, Nothing)

    '        objBudgetTimesheet._dtApproverList = dtList
    '        objBudgetTimesheet._dtEmployeeActivity = mdtEmpTimesheet
    '        objBudgetTimesheet._Employeeunkid = CInt(cboEmployee.SelectedValue)
    '        objBudgetTimesheet._Periodunkid = CInt(cboPeriod.SelectedValue)
    '        objBudgetTimesheet._Userunkid = CInt(Session("UserId"))

    '        If objBudgetTimesheet.Update(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), _
    '                                     CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                     CBool(Session("IsIncludeInactiveEmp")), True) = False Then

    '            DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
    '            Exit Sub
    '        Else
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Employee Timesheet activity(s) submitted for approval successfully."), Me)

    '            Dim strEmpTimesheetIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(cboEmployee.SelectedValue)) _
    '                                                               .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)


    '            objBudgetTimesheet.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
    '                                                          CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                                          CBool(Session("IsIncludeInactiveEmp")), CStr(cboEmployee.SelectedValue), _
    '                                                          strEmpTimesheetIDs, CInt(cboPeriod.SelectedValue), Nothing, False, -1, mintLoginTypeID, _
    '                                                          mintLoginEmployeeID, CInt(Session("UserId")), CStr(Session("ArutiSelfServiceURL")))

    '            CType(dgvEmpTimesheet.Controls(mdicETS("dgcolhSelect")).Controls(mdicETS("dgcolhSelect")).FindControl("chkSelectAll"), CheckBox).Checked = False
    '            Call FillList()
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("btnSubmitForAproval_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (27 Feb 2017) -- End


    'Nilay (10 Jan 2017) -- End

    Protected Sub popupConfirmYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmYesNo.buttonYes_Click
        Try
            If mstrCommandName = "DELETE" Then
                Language.setLanguage(mstrModuleName)
                popupDeleteReason.Title = "Delete Reason"
                popupDeleteReason.Reason = ""
                popupDeleteReason.Show()

            ElseIf mstrCommandName = "CANCEL" Then

                Language.setLanguage(mstrModuleName)
                popupDeleteReason.Title = Language.getMessage(mstrModuleName, 21, "Cancel Reason")
                popupDeleteReason.Reason = ""
                popupDeleteReason.Show()

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupConfirmYesNo_buttonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

    Protected Sub popupConfirmYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmYesNo.buttonNo_Click
        Try
            If mstrCommandName = "OverTimeConfirmation" Then
                Dim strHours As String = mstrOriginalHrs.Trim.Substring(0, mstrOriginalHrs.Trim.IndexOf(":"))
                Dim strMinutes As String = mstrOriginalHrs.Trim.Substring(mstrOriginalHrs.Trim.IndexOf(":") + 1)
                CType(dgvEmpTimesheet.Items(xCurrentItemIndex).Cells(dis("dgcolhEmpHours")).FindControl("TxtHours"), TextBox).Text = strHours
                CType(dgvEmpTimesheet.Items(xCurrentItemIndex).Cells(dis("dgcolhEmpHours")).FindControl("TxtMinutes"), TextBox).Text = strMinutes
                mstrCommandName = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (28-Mar-2018) -- End


    Protected Sub popupDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteReason.buttonDelReasonYes_Click
        Try
            objBudgetTimesheet = New clsBudgetEmp_timesheet

            Call SetDateFormat()

            If mstrCommandName = "DELETE" Then

                objBudgetTimesheet._Voidreason = popupDeleteReason.Reason
                objBudgetTimesheet._Voiduserunkid = CInt(Session("UserId"))
                objBudgetTimesheet._LoginEmployeeunkid = -1
                objBudgetTimesheet._WebFormName = mstrModuleName
                objBudgetTimesheet._WebClientIP = CStr(Session("IP_ADD"))
                objBudgetTimesheet._WebHostName = CStr(Session("HOST_NAME"))

                If objBudgetTimesheet.Delete(CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             mintEmptimesheetunkid) = False Then
                    DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                    Exit Sub
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Timesheet deleted Successfully."), Me)
                    objBudgetTimesheet.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                  CBool(Session("IsIncludeInactiveEmp")), _
                                                                  CStr(mintEmployeeId), CStr(mintEmptimesheetunkid), CInt(cboPeriod.SelectedValue), _
                                                                  clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED, mintLoginTypeID, _
                                                                  mintLoginEmployeeID, mintUserID, CStr(Session("ArutiSelfServiceURL")), _
                                                                  popupDeleteReason.Reason)
                    cboPeriod.Enabled = True
                    dtpDate.Enabled = True
                    cboEmployee.Enabled = True
                    mintEmptimesheetunkid = 0
                    btnEdit.Visible = True
                    btnSave.Visible = True
                    'ResetValue()
                    FillList()
                End If

            ElseIf mstrCommandName = "CANCEL" Then

                objBudgetTimesheet._Userunkid = CInt(Session("UserId"))
                objBudgetTimesheet._WebFormName = mstrModuleName
                objBudgetTimesheet._WebClientIP = CStr(Session("IP_ADD"))
                objBudgetTimesheet._WebHostName = CStr(Session("HOST_NAME"))


                If objBudgetTimesheet.CancelTimesheet(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), _
                                                      CInt(Session("CompanyUnkId")), CBool(Session("IsIncludeInactiveEmp")), _
                                                      CStr(Session("UserAccessModeSetting")), popupDeleteReason.Reason, _
                                                      CStr(mintEmptimesheetunkid), Nothing) = False Then

                    DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                Else
                    FillList()
                    objBudgetTimesheet.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                  CBool(Session("IsIncludeInactiveEmp")), CStr(mintEmployeeId), CStr(mintEmptimesheetunkid), _
                                                                  CInt(cboPeriod.SelectedValue), clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED, _
                                                                  mintLoginTypeID, mintLoginEmployeeID, mintUserID, CStr(Session("ArutiSelfServiceURL")), _
                                                                  popupDeleteReason.Reason, , , False)
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupDeleteReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Link Button's Events"
    Protected Sub lnkViewPendingSubmitApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewPendingSubmitApproval.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            Session("PeriodID") = CInt(cboPeriod.SelectedValue)
            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'Session("IsFromPendingSubmitForApproval") = True
            Session("OperationID") = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL
            'Nilay (01 Apr 2017) -- End

            Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_SubmitForApproval.aspx", False)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkViewPendingSubmitApproval_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkViewCompletedSubmitApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewCompletedSubmitApproval.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            Session("PeriodID") = CInt(cboPeriod.SelectedValue)
            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'Session("IsFromPendingSubmitForApproval") = False
            Session("OperationID") = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED
            'Nilay (01 Apr 2017) -- End

            Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_SubmitForApproval.aspx", False)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkViewCompletedSubmitApproval_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01 Apr 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Protected Sub lnkGlobalCancelTimesheet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGlobalCancelTimesheet.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            Session("PeriodID") = CInt(cboPeriod.SelectedValue)
            Session("OperationID") = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED

            Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_SubmitForApproval.aspx", False)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkGlobalCancelTimesheet_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkGlobalDeleteTimesheet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGlobalDeleteTimesheet.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            Session("PeriodID") = CInt(cboPeriod.SelectedValue)
            Session("OperationID") = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED

            Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_SubmitForApproval.aspx", False)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkGlobalDeleteTimesheet_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01 Apr 2017) -- End

#End Region

#Region " CheckBox's Events "
    'Nilay (27 Feb 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    'Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If dgvEmpTimesheet.Items.Count <= 0 Then Exit Sub

    '        Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

    '        If mdtEmpTimesheet.Rows.Count > 0 Then
    '            For Each item As DataGridItem In dgvEmpTimesheet.Items
    '                CType(item.Cells(mdicETS("dgcolhSelect")).FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
    '            Next
    '            For Each dRow As DataRow In mdtEmpTimesheet.Select("IsGroup=0 AND isholiday=0 AND isLeave=0 AND isDayOff=0 AND issubmit_approval=0")
    '                dRow.Item("IsChecked") = chkSelectAll.Checked
    '            Next
    '            mdtEmpTimesheet.AcceptChanges()
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If dgvEmpTimesheet.Items.Count <= 0 Then Exit Sub

    '        Dim chkSelect As CheckBox = CType(sender, CheckBox)
    '        Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)

    '        If mdtEmpTimesheet.Rows.Count > 0 Then
    '            Dim dRow As DataRow() = mdtEmpTimesheet.Select("IsGroup=0 AND isholiday=0 AND isLeave=0 AND isDayOff=0 AND emptimesheetunkid=" & CInt(item.Cells(mdicETS("objdgcolhEmpTimesheetID")).Text))
    '            If dRow.Length > 0 Then
    '                For Each dR In dRow
    '                    dR.Item("IsChecked") = chkSelect.Checked
    '                Next
    '            End If
    '            mdtEmpTimesheet.AcceptChanges()

    '            Dim dRC As DataRow() = mdtEmpTimesheet.Select("IsGroup=0 AND isholiday=0 AND isLeave=0 AND isDayOff=0 AND issubmit_approval=0")
    '            Dim xCount As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGroup=0 AND isholiday=0 AND isLeave=0 AND isDayOff=0")

    '            If xCount.Length = dRC.Length Then
    '                CType(dgvEmpTimesheet.Controls(mdicETS("dgcolhSelect")).Controls(mdicETS("dgcolhSelect")).FindControl("chkSelectAll"), CheckBox).Checked = True
    '            Else
    '                CType(dgvEmpTimesheet.Controls(mdicETS("dgcolhSelect")).Controls(mdicETS("dgcolhSelect")).FindControl("chkSelectAll"), CheckBox).Checked = False
    '            End If
    '            mdtEmpTimesheet.AcceptChanges()
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("chkSelect_CheckedChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (27 Feb 2017) -- End
#End Region

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

#Region "Project Details"

#Region "Private Methods"

    Private Function GetProjectDetails(ByVal xEmployeeId As Integer, ByVal mdtFromDate As Date, ByVal mdtToDate As Date, ByVal mstrProject As String, ByVal mstrDonor As String _
                                              , ByVal mintActivityId As Integer, ByVal mstrActivity As String, ByVal mdblAllocatePencentage As Double, ByVal xPeriodStartDate As Date, ByVal xPeriodEndDate As Date) As DataTable

        '  'Pinkal (16-May-2018) -- 'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.[ByVal xPeriodStartDate As Date, ByVal xPeriodEndDate As Date]

        Dim dtTable As DataTable = Nothing
        Try

            objLblEmpVal.Text = cboEmployee.SelectedItem.Text
            objLblPeriodVal.Text = cboPeriod.SelectedItem.Text
            objLblProjectVal.Text = mstrProject
            objLblDonorGrantVal.Text = mstrDonor
            objLblActivityVal.Text = mstrActivity

            dtTable = New DataTable()
            dtTable.Columns.Add("Id", Type.GetType("System.Int32"))
            dtTable.Columns.Add("Particulars", Type.GetType("System.String"))
            dtTable.Columns.Add("Hours", Type.GetType("System.String"))

            Dim drRow As DataRow = dtTable.NewRow()
            drRow("Id") = 1
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 1, "Projected(targeted) total number of hours for the month")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 2
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 2, "Projected running total number of hours as per entries")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 3
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 3, "Projected running total number of hours as of date")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 4
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 4, "Total Actual(running) number of hours")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 5
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 5, "Balance of the remained hrs")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            drRow = dtTable.NewRow()
            drRow("Id") = 6
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 6, "Employee % allocation of the projects")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)


            drRow = dtTable.NewRow()
            drRow("Id") = 7
            drRow("Particulars") = Language.getMessage(mstrModuleName1, 7, "OverTime")
            drRow("Hours") = ""
            dtTable.Rows.Add(drRow)

            Dim RowIndex As Integer = 1


            ' START - (I) Projected(targeted) total number of hours for the month (weekend + holiday Excluded)
            Dim mdblProejectedMonthHrs As Double = 0

            'Pinkal (16-May-2018) -- Start
            'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.
            'Dim mdblTotalWorkingHrs As Double = GetProjectedTotalHRs(mdtFromDate, mdtToDate)
            Dim mdblTotalWorkingHrs As Double = GetProjectedTotalHRs(xPeriodStartDate, xPeriodEndDate)
            'Pinkal (16-May-2018) -- End

            dtTable.Rows(RowIndex - 1)("Hours") = CalculateTime(True, CInt((mdblTotalWorkingHrs * mdblAllocatePencentage) / 100)).ToString("#00.00").Replace(".", ":")
            mdblProejectedMonthHrs = CDbl((mdblTotalWorkingHrs * mdblAllocatePencentage) / 100)
            ' END -  (I) Projected(targeted) total number of hours for the month (weekend + holiday Excluded)



            'START - (II) Projected running total number of hours as per entries.
            mdblTotalWorkingHrs = 0
            If mdtEmpTimesheet IsNot Nothing AndAlso mdtEmpTimesheet.Rows.Count > 0 Then
                Dim dtDate As Date = Nothing
                'Pinkal (04-May-2018) -- Start
                'Enhancement - Budget timesheet Enhancement for MST/THPS.
                If IsDBNull(mdtEmpTimesheet.Compute("Max(activitydate)", "fundactivityunkid = " & mintActivityId)) = False Then
                    dtDate = CDate(mdtEmpTimesheet.Compute("Max(activitydate)", "fundactivityunkid = " & mintActivityId))
                Else
                    dtTable.Rows(RowIndex)("Hours") = "00:00"
                End If

                If dtDate <> Nothing Then
                    mdblTotalWorkingHrs = GetProjectedTotalHRs(mdtFromDate, dtDate)
                    dtTable.Rows(RowIndex)("Hours") = CalculateTime(True, CInt((mdblTotalWorkingHrs * mdblAllocatePencentage) / 100)).ToString("#00.00").Replace(".", ":")
                End If
                'Pinkal (04-May-2018) -- End

            Else
                dtTable.Rows(RowIndex)("Hours") = "00:00"
            End If
            'END  - (II)  Projected running total number of hours as per entries.


            ' START - (III) Projected running total number of hours as of date.
            mdblTotalWorkingHrs = 0
            If eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date < mdtToDate.Date Then
                mdtToDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date
            End If
            mdblTotalWorkingHrs = GetProjectedTotalHRs(mdtFromDate, mdtToDate)
            dtTable.Rows(RowIndex + 1)("Hours") = CalculateTime(True, CInt((mdblTotalWorkingHrs * mdblAllocatePencentage) / 100)).ToString("#00.00").Replace(".", ":")
            ' END - (III) Projected running total number of hours as of date. 


            'START - (IV) Total Actual(running) number of hours (only Entries)
            mdblTotalWorkingHrs = 0
            Dim mdblActualHrs As Double = 0
            If mdtEmpTimesheet IsNot Nothing AndAlso mdtEmpTimesheet.Rows.Count > 0 Then
                'Pinkal (04-May-2018) -- Start
                'Enhancement - Budget timesheet Enhancement for MST/THPS.
                If IsDBNull(mdtEmpTimesheet.Compute("SUM(ActivityHoursInMin)", "fundactivityunkid = " & mintActivityId)) = False Then
                    mdblTotalWorkingHrs = CInt(mdtEmpTimesheet.Compute("SUM(ActivityHoursInMin)", "fundactivityunkid = " & mintActivityId)) * 60
                End If
                'Pinkal (04-May-2018) -- End
                mdblActualHrs = mdblTotalWorkingHrs
                dtTable.Rows(RowIndex + 2)("Hours") = CalculateTime(True, CInt(mdblTotalWorkingHrs)).ToString("#0.00").Replace(".", ":")
            Else
                dtTable.Rows(RowIndex + 2)("Hours") = "00:00"
            End If

            'END - (IV) Total Actual(running) number of hours (only Entries)

            'START - (V) Balance of the remained hrs.
            Dim mblnIsOvertime As Boolean = False
            mdblTotalWorkingHrs = 0
            If mdblActualHrs > mdblProejectedMonthHrs Then
                mdblTotalWorkingHrs = mdblActualHrs - mdblProejectedMonthHrs
                mblnIsOvertime = True
            Else
                mdblTotalWorkingHrs = mdblProejectedMonthHrs - mdblActualHrs
                mblnIsOvertime = False
            End If
            dtTable.Rows(RowIndex + 3)("Hours") = CalculateTime(True, CInt(mdblTotalWorkingHrs)).ToString("#00.00").Replace(".", ":")
            'End - (V) Balance of the remained hrs.

            'START - (VI)  Show employee % allocation of the projects 
            dtTable.Rows(RowIndex + 4)("Hours") = mdblAllocatePencentage.ToString("#00.00") & " % "
            'END - (VI) Show employee % allocation of the projects 


            'START (VII) Show Overtime 
            dtTable.Rows(dtTable.Rows.Count - 1)("Hours") = mblnIsOvertime
            'End (VII) Show Overtime 

            mdblActualHrs = 0
            mdblProejectedMonthHrs = 0
            mdblTotalWorkingHrs = 0

            'dgvProjectHrDetails.AutoGenerateColumns = False
            'dgvProjectHrDetails.DataSource = dtTable
            'dgvProjectHrDetails.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return dtTable
    End Function

    Private Function GetProjectedTotalHRs(ByVal xFromDate As Date, ByVal xToDate As Date) As Double
        Dim mintTotalWorkingDays As Integer = 0
        Dim mdblTotalWorkingHrs As Double = 0
        Dim objEmpShift As New clsEmployee_Shift_Tran
        Dim objEmpHoliday As New clsemployee_holiday
        Try

            objEmpShift.GetTotalWorkingHours(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                     , Session("UserAccessModeSetting").ToString(), CInt(cboEmployee.SelectedValue), xFromDate.Date, xToDate.Date _
                                                                                                     , mintTotalWorkingDays, mdblTotalWorkingHrs, False)


            Dim dsHoliday As DataSet = objEmpHoliday.GetList("List", Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                         , xFromDate.Date, xToDate.Date, Session("UserAccessModeSetting").ToString(), True, False, True, CInt(cboEmployee.SelectedValue), , _
                                                                                         , " AND convert(char(8),lvholiday_master.holidaydate,112) >= '" & eZeeDate.convertDate(xFromDate) & "' AND convert(char(8),lvholiday_master.holidaydate,112) <= '" & eZeeDate.convertDate(xToDate) & "'")

            If dsHoliday IsNot Nothing AndAlso dsHoliday.Tables(0).Rows.Count > 0 Then
                Dim objShiftTran As New clsshift_tran
                For Each dr As DataRow In dsHoliday.Tables(0).Rows
                    Dim mintShiftId As Integer = objEmpShift.GetEmployee_Current_ShiftId(eZeeDate.convertDate(dr("holidaydate").ToString()).Date, CInt(cboEmployee.SelectedValue))
                    objShiftTran.GetShiftTran(mintShiftId)
                    Dim dHLRow() As DataRow = objShiftTran._dtShiftday.Select("isweekend = 0 AND dayid = " & Weekday(eZeeDate.convertDate(dr("holidaydate").ToString()).Date, FirstDayOfWeek.Sunday) - 1)
                    If dHLRow.Length > 0 Then
                        mintTotalWorkingDays -= 1
                        mdblTotalWorkingHrs -= CDbl(dHLRow(0)("workinghrsinsec"))
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmpShift = Nothing
            objEmpHoliday = Nothing
        End Try
        Return mdblTotalWorkingHrs
    End Function

#End Region

#Region "Button's Event"

    Protected Sub btnShowDetailsClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowDetailsClose.Click
        Try
            popupShowDetails.Hide()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnShowDetailsClose_Click: " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#End Region

    'Pinkal (28-Mar-2018) -- End




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)

            Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.ID, Me.LblPeriod.Text)
            Me.LblDate.Text = Language._Object.getCaption(Me.LblDate.ID, Me.LblDate.Text)

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.ID, Me.LblToDate.Text)
            'Pinkal (13-Oct-2017) -- End

            Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.ID, Me.LblEmployee.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.ID, Me.btnOperation.Text).Replace("&", "")

            'Nilay (01 Apr 2017) -- Start
            lnkViewPendingSubmitApproval.Text = Language._Object.getCaption("mnuViewPendingSubmitApproval", lnkViewPendingSubmitApproval.Text).Replace("&", "")
            lnkViewCompletedSubmitApproval.Text = Language._Object.getCaption("mnuViewCompletedSubmitApproval", lnkViewCompletedSubmitApproval.Text).Replace("&", "")
            lnkGlobalCancelTimesheet.Text = Language._Object.getCaption("mnuGlobalCancelTimesheet", lnkGlobalCancelTimesheet.Text).Replace("&", "")
            lnkGlobalDeleteTimesheet.Text = Language._Object.getCaption("mnuGlobalDeleteTimesheet", lnkGlobalDeleteTimesheet.Text).Replace("&", "")
            'Nilay (01 Apr 2017) -- End

            Me.dgvTimesheet.Columns(0).HeaderText = Language._Object.getCaption(dgvTimesheet.Columns(0).FooterText, dgvTimesheet.Columns(0).HeaderText)
            Me.dgvTimesheet.Columns(1).HeaderText = Language._Object.getCaption(dgvTimesheet.Columns(1).FooterText, dgvTimesheet.Columns(1).HeaderText)
            Me.dgvTimesheet.Columns(2).HeaderText = Language._Object.getCaption(dgvTimesheet.Columns(2).FooterText, dgvTimesheet.Columns(2).HeaderText)
            Me.dgvTimesheet.Columns(3).HeaderText = Language._Object.getCaption(dgvTimesheet.Columns(3).FooterText, dgvTimesheet.Columns(3).HeaderText)
            Me.dgvTimesheet.Columns(4).HeaderText = Language._Object.getCaption(dgvTimesheet.Columns(4).FooterText, dgvTimesheet.Columns(4).HeaderText)
            Me.dgvTimesheet.Columns(5).HeaderText = Language._Object.getCaption(dgvTimesheet.Columns(5).FooterText, dgvTimesheet.Columns(5).HeaderText)
            Me.dgvTimesheet.Columns(6).HeaderText = Language._Object.getCaption(dgvTimesheet.Columns(6).FooterText, dgvTimesheet.Columns(6).HeaderText)
            Me.dgvTimesheet.Columns(7).HeaderText = Language._Object.getCaption(dgvTimesheet.Columns(7).FooterText, dgvTimesheet.Columns(7).HeaderText)

            Me.dgvEmpTimesheet.Columns(3).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(3).FooterText, dgvEmpTimesheet.Columns(3).HeaderText)
            Me.dgvEmpTimesheet.Columns(4).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(4).FooterText, dgvEmpTimesheet.Columns(4).HeaderText)
            Me.dgvEmpTimesheet.Columns(5).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(5).FooterText, dgvEmpTimesheet.Columns(5).HeaderText)
            Me.dgvEmpTimesheet.Columns(6).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(6).FooterText, dgvEmpTimesheet.Columns(6).HeaderText)
            Me.dgvEmpTimesheet.Columns(7).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(7).FooterText, dgvEmpTimesheet.Columns(7).HeaderText)
            Me.dgvEmpTimesheet.Columns(8).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(8).FooterText, dgvEmpTimesheet.Columns(8).HeaderText)
            Me.dgvEmpTimesheet.Columns(9).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(9).FooterText, dgvEmpTimesheet.Columns(9).HeaderText)
            Me.dgvEmpTimesheet.Columns(10).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(10).FooterText, dgvEmpTimesheet.Columns(10).HeaderText)
            Me.dgvEmpTimesheet.Columns(11).HeaderText = Language._Object.getCaption(dgvEmpTimesheet.Columns(11).FooterText, dgvEmpTimesheet.Columns(11).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Language & UI Settings
    '</Language>

   
   
    
End Class
