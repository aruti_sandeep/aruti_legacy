﻿<%@ Page Title="Employee TimeSheet Approval List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_EmployeeTimeSheetApprovalList.aspx.vb"
    Inherits="Budget_Timesheet_wPg_EmployeeTimeSheetApprovalList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Advance_Filter.ascx" TagName="AdvanceFilter" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }


        $("body").on("click", "[id*=chkAllSelect]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkSelect]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=chkSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkAllSelect]", grid);
            debugger;
            if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });
        
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Timesheet Approval List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                <ul class="header-dropdown">
                                    <asp:LinkButton ID="lnkAllocation" runat="server" ToolTip="Allocations">
                                              <i class="fas fa-sliders-h"></i>
                                    </asp:LinkButton>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                                        <uc2:DateCtrl ID="dtpDate" runat="server" />
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDonor" runat="server" Text="Donor/Grant" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDonor" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblProject" runat="server" Text="Project" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboProject" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboApprover" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblActivity" runat="server" Text="Activity" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboActivity" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblApproverStatus" runat="server" Text="Approver Status" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboApproverStatus" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblStatus" runat="server" Text="TimeSheet Status" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkMyApprovals" runat="server" Checked="true" Visible="false" Text="My Approvals"
                                                            AutoPostBack="true" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 300px">
                                            <asp:DataGrid ID="dgvEmpTimesheet" runat="server" AutoGenerateColumns="False" AllowPaging="false" Width= "200%"
                                                CssClass="table table-hover table-bordered">
                                                <ItemStyle CssClass="griviewitem" />
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="dgcolhIsCheck">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAllSelect" runat="server" CssClass="filled-in" Text=" " /> <%--AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged"--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="filled-in" Text=" " />  <%--AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged"--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn FooterText="objdgcolhShowDetails">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgShowDetails" runat="server" ImageUrl="~/images/Info_icons.png"
                                                                CommandName="Show Details" ToolTip="Show Project Hour Details" />
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Particulars" HeaderText="Particular" ReadOnly="true"
                                                        HeaderStyle-Width="50px" ItemStyle-Width="50px" FooterText="dgcolhParticular">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Approver" HeaderText="Approver"  ReadOnly="true" FooterText="dgcolhApprover" ItemStyle-Width = "400px" HeaderStyle-Width = "400px"> </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="approvaldate" HeaderText="Approval Date" HeaderStyle-Wrap = "false" ReadOnly="true" FooterText="dgcolhApprovalDate" ItemStyle-Width = "150px" HeaderStyle-Width = "150px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundname" HeaderText="Donor/Grant" ReadOnly="true" FooterText="dgcolhDonor"  ItemStyle-Width = "150px" HeaderStyle-Width = "150px" ></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundprojectname" HeaderText="Project Code"  ReadOnly="true" FooterText="dgcolhProject"  ItemStyle-Width = "150px" HeaderStyle-Width = "150px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="activity_name" HeaderText="Activity Code" ReadOnly="true" FooterText="dgcolhActivity"  ItemStyle-Width = "150px" HeaderStyle-Width = "150px"></asp:BoundColumn>
                                                    
                                                    <asp:BoundColumn DataField="costcentername" FooterText="dgcolhCostCenter" HeaderText="Cost Center"
                                                        ReadOnly="true" Visible = "false"  ItemStyle-Width = "150px" HeaderStyle-Width = "150px"></asp:BoundColumn>
                                                    
                                                    <asp:BoundColumn DataField="activity_hrs" HeaderText="Activity Hours"  ItemStyle-Wrap= "false" ReadOnly="true" FooterText="dgcolhHours"  ItemStyle-Width = "100px" HeaderStyle-Width = "100px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="description" HeaderText="Description" ReadOnly="true" FooterText="dgcolhEmpDescription"  ItemStyle-Width = "150px" HeaderStyle-Width = "200px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="submission_remark" FooterText="dgcolhSubmissionRemark" HeaderText="Submission Remark" ReadOnly="true"  ItemStyle-Width = "200px" HeaderStyle-Width = "200px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TotalActivityHours" HeaderText="Tot. Activity Hrs" HeaderStyle-Wrap = "false" ReadOnly="true" FooterText="dgcolhTotalActivityHours"  ItemStyle-Width = "100px" HeaderStyle-Width = "100px" />
                                                    <asp:BoundColumn DataField="status" HeaderText="Status"  ReadOnly="true" FooterText="dgcolhStatus"  ItemStyle-Width = "100px" HeaderStyle-Width = "100px"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="periodunkid" HeaderText="" FooterText="objdgcolhPeriodID"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="" FooterText="objdgcolhEmployeeID"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundsourceunkid" HeaderText="" FooterText="objdgcolhFundSourceID"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="projectcodeunkid" HeaderText="" FooterText="objdgcolhProjectID"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="activityunkid" HeaderText="" FooterText="objdgcolhActivityID"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IsGrp" HeaderText="" FooterText="objdgcolhIsGrp" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="mapuserunkid" HeaderText="" FooterText="objdgcolhMappedUserId"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ApprovalStatusId" HeaderText="" FooterText="objdgcolhStatusID"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Employee" HeaderText="" FooterText="objdgcolhEmployeeName"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="approveremployeeunkid" HeaderText="" FooterText="objdgcolhApproverEmployeeID"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="activitydate" HeaderText="" FooterText="objdgcolhActivityDate"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="priority" HeaderText="" FooterText="objdgcolhPriority"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="emptimesheetunkid" HeaderText="" FooterText="objdgcolhEmpTimesheetID"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="timesheetapprovalunkid" HeaderText="" FooterText="objdgcolhTimesheetApprovalID"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isexternalapprover" HeaderText="" FooterText="objdgcolhIsExternalApprover"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="tsapproverunkid" HeaderText="" FooterText="objdgcolhApproverunkid"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ischecked" HeaderText="" FooterText="objdgcolhIsCheck"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ShiftHours" HeaderText="" FooterText="objdgcolhShiftHours"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ShiftHoursInSec" HeaderText="" FooterText="objdgcolhShiftHoursInSec"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="TotalActivityHoursInMin" HeaderText="" FooterText="objdgcolhTotalActivityHoursInMin"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="percentage" HeaderText="" FooterText="objdgcolhpercentage"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isholiday" HeaderText="" FooterText="objdgcolhIsHoliday"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnChangeStatus" runat="server" Text="Change Status" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupShowDetails" runat="server" BackgroundCssClass="modal-backdrop"
                    TargetControlID="hdf_Details" PopupControlID="pnlShowDetails" CancelControlID="hdf_Details">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlShowDetails" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblpopupHeader" runat="server" Text="Project & Activity Hours Details"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <asp:Label ID="LblShowDetailsEmployee" runat="server" Text="Employee" CssClass="form-label" />
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="objLblEmpVal" runat="server" Text="#Employee" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <asp:Label ID="LblShowDetailsPeriod" runat="server" Text="Period" CssClass="form-label" />
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="objLblPeriodVal" runat="server" Text="#Period" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <asp:Label ID="Label1" runat="server" Text="Project" CssClass="form-label" />
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="objLblProjectVal" runat="server" Text="#Project" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <asp:Label ID="LblDonorGrant" runat="server" Text="Donor/Grant" CssClass="form-label" />
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="objLblDonorGrantVal" runat="server" Text="#Donor/Grant" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <asp:Label ID="Label2" runat="server" Text="Activity" CssClass="form-label" />
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="objLblActivityVal" runat="server" Text="#Activity" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 300px;">
                                    <asp:DataGrid ID="dgvProjectHrDetails" runat="server" AutoGenerateColumns="False"
                                        AllowPaging="false" CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:BoundColumn DataField="Particulars" HeaderText="Particulars" ReadOnly="true"
                                                FooterText="dgcolhParticulars"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Hours" HeaderText="Hours" ReadOnly="true" FooterText="dgcolhHours">
                                            </asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnShowDetailsClose" runat="server" CssClass=" btn btn-primary" Text="Close" />
                        <asp:HiddenField ID="hdf_Details" runat="server" />
                    </div>
                </asp:Panel>
                <uc1:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
