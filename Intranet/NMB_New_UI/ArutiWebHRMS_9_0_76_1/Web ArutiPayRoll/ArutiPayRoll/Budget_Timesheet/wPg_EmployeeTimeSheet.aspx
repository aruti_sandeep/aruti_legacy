<%@ Page Title="Employee Timesheet" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_EmployeeTimeSheet.aspx.vb" Inherits="Budget_Timesheet_wPg_EmployeeTimeSheet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/OperationButton.ascx" TagName="OperationButton" TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }

        function onlyNumbers(txtBox, e) {
            //        var e = event || evt; // for trans-browser compatibility
            //        var charCode = e.which || e.keyCode;
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)

                if (charCode == 1)
                return false;

            if (charCode > 31 && (charCode <= 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                var hdf = '#<%= hdf_SetScroll.ClientID %>'
                if ($(hdf).val() == "1") {
                    $(hdf).val("0");
                } else {
                    $(window).scrollTop(nbodyY);
                }
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
        function SethiddenVal() {
            var hdf = '#<%= hdf_SetScroll.ClientID %>'
            $(hdf).val("1");
            return true
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Timesheet"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                                        <uc3:DateCtrl ID="dtpDate" runat="server" />
                                                    </div>
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                                        <uc3:DateCtrl ID="dtpToDate" runat="server" />
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 330px">
                                                            <asp:DataGrid ID="dgvTimesheet" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                                CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:TemplateColumn ItemStyle-Width="30px">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgShowDetails" runat="server" ImageUrl="~/images/Info_icons.png"
                                                                                CommandName="Show Details" ToolTip="Show Project Hour Details" />
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="Donor" HeaderText="Donor/Grant" ReadOnly="true" FooterText="dgcolhEmpDonor"
                                                                        ItemStyle-Width="200px"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Project" HeaderText="Project" ReadOnly="true" FooterText="dgcolhEmpProject"
                                                                        ItemStyle-Width="200px"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="Activity" HeaderText="Activity" ReadOnly="true" FooterText="dgcolhEmpActivity">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="percentage" HeaderText="Assigned Activity(%)" ReadOnly="true"
                                                                        HeaderStyle-Width="100px" ItemStyle-Width="100px" FooterText="dgcolhEmpPercentage"
                                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                                        
                                                                    <asp:TemplateColumn FooterText="dgcolhCostCenter" HeaderText="Cost Center" HeaderStyle-Wrap="false" Visible = "false"
                                                                        HeaderStyle-Width="250px" ItemStyle-Width="250px">
                                                                        <ItemTemplate>
                                                                            <div class="form-group">
                                                                                <asp:DropDownList ID="cboCostcenter" runat="server" AutoPostBack = "true" OnSelectedIndexChanged = "cboCostcenter_SelectedIndexChanged" />
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    
                                                                    <asp:TemplateColumn FooterText="dgcolhEmpHours" HeaderText="Hours : Mins" HeaderStyle-Wrap="false"
                                                                        HeaderStyle-Width="260px" ItemStyle-Width="260px">
                                                                        <ItemTemplate>
                                                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                                                <div class="form-group">
                                                                                    <div class="form-line">
                                                                                        <asp:TextBox ID="TxtHours" runat="server" AutoPostBack="true" Width="100%" onKeypress="return onlyNumbers(this, event);"
                                                                                            MaxLength="2" Style="text-align: right;" OnTextChanged="TxtHours_TextChanged1"
                                                                                            CssClass="form-control" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 m-t-15">
                                                                                <asp:Label ID="LblColon" runat="server" Text=":" CssClass="form-label" />
                                                                            </div>
                                                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                                                <div class="form-group">
                                                                                    <div class="form-line">
                                                                                        <asp:TextBox ID="TxtMinutes" runat="server" AutoPostBack="true" Width="100%" onKeypress="return onlyNumbers(this, event);"
                                                                                            MaxLength="2" Style="text-align: right" OnTextChanged="TxtMinutes_TextChanged"
                                                                                            CssClass="form-control" />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            </span>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn FooterText="dgcolhEmpDescription" HeaderText="Description" HeaderStyle-Width="200px"
                                                                        ItemStyle-Width="200px">
                                                                        <ItemTemplate>
                                                                            <asp:Panel ID="pnlDescription" runat="server">
                                                                                <div class="form-group">
                                                                                    <div class="form-line">
                                                                                        <asp:TextBox ID="TxtDescription" runat="server" Text='<%# Eval("description") %>'
                                                                                            Width="100%" AutoPostBack="true" OnTextChanged="TxtDescription_TextChanged" CssClass="form-control"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </asp:Panel>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="ActivityHoursInMin" HeaderText="HourInSec" ReadOnly="true"
                                                                        Visible="false" FooterText="objdgcolhEmpHourInMin" />
                                                                    <asp:BoundColumn DataField="AssignedActivityHrsInMin" HeaderText="AssignedActivityHrsInMin"
                                                                        ReadOnly="true" Visible="false" FooterText="objdgcolhEmpAssignedActivityHrsInMin" />
                                                                    <asp:BoundColumn DataField="periodunkid" HeaderText="Periodunkid" ReadOnly="true"
                                                                        Visible="false" FooterText="objdgcolhEmpPeriodunkid" />
                                                                    <asp:BoundColumn DataField="fundsourceunkid" HeaderText="DonorID" ReadOnly="true"
                                                                        Visible="false" FooterText="objdgcolhEmpDonorID" />
                                                                    <asp:BoundColumn DataField="fundprojectcodeunkid" HeaderText="ProjectID" ReadOnly="true"
                                                                        Visible="false" FooterText="objdgcolhEmpProjectunkid" />
                                                                    <asp:BoundColumn DataField="fundactivityunkid" HeaderText="ActivityID" ReadOnly="true"
                                                                        Visible="false" FooterText="objdgcolhEmpActivityunkid" />
                                                                    <asp:BoundColumn DataField="budgetunkid" HeaderText="BudgetID" ReadOnly="true" Visible="false"
                                                                        FooterText="objdgcolhEmpBudgetID" />
                                                                    <asp:BoundColumn DataField="ActivityHours" ReadOnly="true" Visible="false" FooterText="dgcolhActivityHours">
                                                                    </asp:BoundColumn>
                                                                      <asp:BoundColumn DataField="costcenterunkid" ReadOnly="true" Visible="false" FooterText="objdgcolhcostcenterunkid">
                                                                    </asp:BoundColumn>
                                                                </Columns>
                                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                            </asp:DataGrid>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <div class="btn-group pull-left">
                                                    <asp:LinkButton ID="btnOperation" runat="server" class="btn btn-primary dropdown-toggle"
                                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Operations<span class="caret"></span></asp:LinkButton>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <asp:LinkButton ID="lnkViewPendingSubmitApproval" runat="server" Text="View Pending Submit For Approval"></asp:LinkButton>
                                                        </li>
                                                        <li>
                                                            <asp:LinkButton ID="lnkViewCompletedSubmitApproval" runat="server" Text="View Completed Submit For Approval"></asp:LinkButton>
                                                        </li>
                                                        <li>
                                                            <asp:LinkButton ID="lnkGlobalCancelTimesheet" runat="server" Text="Gloabl Cancel Timesheet"></asp:LinkButton>
                                                        </li>
                                                        <li>
                                                            <asp:LinkButton ID="lnkGlobalDeleteTimesheet" runat="server" Text="Gloabl Delete Timesheet"></asp:LinkButton>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <asp:Button ID="btnSave" runat="server" ValidationGroup="MKE" Text="Save" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnEdit" runat="server" ValidationGroup="MKE" Text="Edit" Visible="false"
                                                    CssClass="btn btn-primary" />
                                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                                <input type="hidden" id="hdf_SetScroll" value="0" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:DataGrid ID="dgvEmpTimesheet" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                AllowPaging="false">
                                                <ItemStyle CssClass="griviewitem" />
                                                <Columns>
                                                    <asp:TemplateColumn FooterText="btnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgEdit" runat="server" CommandName="Edit" CssClass="gridedit"
                                                                    ToolTip="Edit" OnClientClick="return SethiddenVal()">
                                                                        <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" CssClass="griddelete"
                                                                    Style="text-align: center;" ToolTip="Delete">
                                                                       <i class="fas fa-trash text-danger"></i>  
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn FooterText="btnCancel" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgCancel" runat="server" CommandName="Cancel" CssClass="gridicon"
                                                                    ToolTip="Cancel">
                                                                        <i class="fas fa-ban text-danger style="font-size:20px;color:Red"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Particular" FooterText="dgcolhEmployee" HeaderText="Employee"
                                                        ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="activitydate" FooterText="dgcolhDate" HeaderText="Activity Date"
                                                        ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundname" FooterText="dgcolhDonor" HeaderText="Donor/Grant"
                                                        ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundprojectname" FooterText="dgcolhProject" HeaderText="Project Code"
                                                        ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="activity_name" FooterText="dgcolhActivity" HeaderText="Activity Code"
                                                        ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="costcentername" FooterText="dgcolhCostCenterList" HeaderText="Cost Center"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="activity_hrs" FooterText="dgcolhHours" HeaderStyle-Width="100px"
                                                        HeaderText="Activity Hours" ItemStyle-Width="100px" ReadOnly="true">
                                                        <HeaderStyle Width="100px" />
                                                        <ItemStyle Width="100px" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="approvedactivity_hrs" FooterText="dgcolhApprovedActHrs"
                                                        HeaderStyle-Width="175px" HeaderText="Approved Activity Hrs." ItemStyle-Width="175px"
                                                        ReadOnly="true">
                                                        <HeaderStyle Width="175px" />
                                                        <ItemStyle Width="175px" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="description" FooterText="dgcolhDescription" HeaderText="Description"
                                                        ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="status" FooterText="dgcolhStatus" HeaderText="Status"
                                                        ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="emptimesheetunkid" FooterText="objdgcolhEmpTimesheetID"
                                                        HeaderText="" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="periodunkid" FooterText="objdgcolhPeriodID" HeaderText=""
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmployeeID" HeaderText=""
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundsourceunkid" FooterText="objdgcolhFundSourceID" HeaderText=""
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundprojectcodeunkid" FooterText="objdgcolhProjectID"
                                                        HeaderText="" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundactivityunkid" FooterText="objdgcolhActivityID" HeaderText=""
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="statusunkid" FooterText="objdgcolhStatusId" HeaderText=""
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrp" HeaderText="" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isholiday" FooterText="objdgcolhIsHoliday" HeaderText=""
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isDayOff" FooterText="objdgcolhIsDayOFF" HeaderText=""
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isLeave" FooterText="objdgcolhIsLeave" HeaderText=""
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="issubmit_approval" FooterText="objdgcolhIsSubmitForApproval"
                                                        HeaderText="" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IsChecked" FooterText="objdgcolhSelect" HeaderText=""
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ApprovedActivityHoursInMin" FooterText="objdgcolhApprovedActivityHoursInMin"
                                                        HeaderText="" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ActivityHoursInMin" FooterText="objdgcolhActivityHoursInMin"
                                                        HeaderText="" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="costcenterunkid" FooterText="objdgcolhCostCenterIdList"
                                                        HeaderText="CostCenterIdList" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupShowDetails" runat="server" BackgroundCssClass="modal-backdrop"
                    TargetControlID="hdf_Details" PopupControlID="pnlShowDetails" CancelControlID="hdf_Details">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlShowDetails" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblpopupHeader" runat="server" Text="Project & Activity Hours Details"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <asp:Label ID="LblShowDetailsEmployee" runat="server" Text="Employee" CssClass="form-label" />
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="objLblEmpVal" runat="server" Text="#Employee" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <asp:Label ID="LblShowDetailsPeriod" runat="server" Text="Period" CssClass="form-label" />
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="objLblPeriodVal" runat="server" Text="#Period" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <asp:Label ID="LblProject" runat="server" Text="Project" CssClass="form-label" />
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="objLblProjectVal" runat="server" Text="#Project" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <asp:Label ID="LblDonorGrant" runat="server" Text="Donor/Grant" CssClass="form-label" />
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="objLblDonorGrantVal" runat="server" Text="#Donor/Grant" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <asp:Label ID="LblActivity" runat="server" Text="Activity" CssClass="form-label" />
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="objLblActivityVal" runat="server" Text="#Activity" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 300px;">
                                    <asp:DataGrid ID="dgvProjectHrDetails" runat="server" AutoGenerateColumns="False"
                                        AllowPaging="false" CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:BoundColumn DataField="Particulars" HeaderText="Particulars" ReadOnly="true"
                                                FooterText="dgcolhParticulars"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Hours" HeaderText="Hours" ReadOnly="true" FooterText="dgcolhHours">
                                            </asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnShowDetailsClose" runat="server" CssClass="btn btn-primary" Text="Close" />
                        <asp:HiddenField ID="hdf_Details" runat="server" />
                    </div>
                </asp:Panel>
                <uc1:DeleteReason ID="popupDeleteReason" runat="server" />
                <uc2:ConfirmYesNo ID="popupConfirmYesNo" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
