﻿<%@ Page Title="Submit For Approval" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_SubmitForApproval.aspx.vb" Inherits="Budget_Timesheet_wPg_SubmitForApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }

        $("body").on("click", "[id*=chkAllSelect]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkSelect]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=chkSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkAllSelect]", grid);
            debugger;
            if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });
        
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Submit For Approval"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpDate" runat="server" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlSubmissionRemark" runat="server">
                                            <asp:Label ID="LblSubmissionRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtSubmissionRemark" runat="server" TextMode="MultiLine" Rows="2"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:DataGrid ID="dgvEmpTimesheetList" runat="server" AutoGenerateColumns="False"
                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <ItemStyle CssClass="griviewitem" />
                                                <Columns>
                                                    <asp:TemplateColumn FooterText="objdgcolhSelect" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Width="30px" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAllSelect" runat="server" CssClass="filled-in" Text=" " />
                                                            <%--AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged"--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="filled-in" Text=" " /><%-- AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged"--%>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="IsChecked" FooterText="dgcolhSelect" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Particular" FooterText="dgcolhEmployee" HeaderText="Employee"
                                                        ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="activitydate" FooterText="dgcolhDate" HeaderText="Activity Date"
                                                        HeaderStyle-Wrap="false" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundname" FooterText="dgcolhDonor" HeaderText="Donor/Grant"
                                                        ItemStyle-Width="200px" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundprojectname" FooterText="dgcolhProject" HeaderText="Project Code"
                                                        ItemStyle-Width="200px" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="activity_name" FooterText="dgcolhActivity" HeaderText="Activity Name"
                                                        ReadOnly="true"></asp:BoundColumn>
                                                        
                                                    <asp:BoundColumn DataField="costcentername" FooterText="dgcolhCostCenter" HeaderText="Cost Center"
                                                        ReadOnly="true" Visible = "false"></asp:BoundColumn>    
                                                        
                                                    <asp:BoundColumn DataField="activity_hrs" FooterText="dgcolhHours" HeaderText="Activity Hours"
                                                        HeaderStyle-Wrap="false" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="approvedactivity_hrs" FooterText="dgcolhApprovedActHrs"
                                                        HeaderStyle-Wrap="false" HeaderText="Approved Activity Hours" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="description" FooterText="dgcolhDescription" HeaderText="Description"
                                                        ItemStyle-Width="300px" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="submission_remark" FooterText="dgcolhSubmissionRemark"
                                                        ItemStyle-Width="300px" HeaderText="Submission Remark" ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="status" FooterText="dgcolhStatus" HeaderText="Status"
                                                        ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="emptimesheetunkid" FooterText="objdgcolhEmpTimesheetID"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="periodunkid" FooterText="objdgcolhPeriodID" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmployeeID" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundsourceunkid" FooterText="objdgcolhFundSourceID" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundprojectcodeunkid" FooterText="objdgcolhProjectID"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fundactivityunkid" FooterText="objdgcolhActivityID" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="statusunkid" FooterText="objdgcolhStatusId" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrp" ReadOnly="true" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isholiday" FooterText="objdgcolhIsHoliday" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isDayOff" FooterText="objdgcolhIsDayOFF" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isLeave" FooterText="objdgcolhIsLeave" ReadOnly="true"
                                                        Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ApprovedActivityHoursInMin" FooterText="objdgcolhApprovedActivityHoursInMin"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="issubmit_approval" FooterText="objdgcolhIsSubmitForApproval"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ADate" FooterText="objdgcolhADate" ReadOnly="true" Visible="false">
                                                    </asp:BoundColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-primary" />
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSubmitForApproval" runat="server" Text="Submit For Approval" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc2:DeleteReason ID="popupDeleteReason" runat="server" />
                <uc3:ConfirmYesNo ID="popupConfirmYesNo" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
