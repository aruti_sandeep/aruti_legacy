﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports Microsoft.VisualBasic
#End Region
Partial Class Budget_Timesheet_wPg_SubmitForApproval
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSubmitForApproval"
    Private DisplayMessage As New CommonCodes
    Private objBudgetTimesheet As clsBudgetEmp_timesheet

    Private mdicETS As Dictionary(Of String, Integer)
    Private mintPeriodID As Integer = -1
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mdtEmpTimesheet As DataTable = Nothing
    Private mintConstantDays As Integer = 0
    Private mintLoginTypeID As Integer = -1
    Private mintLoginEmployeeID As Integer = -1
    Private mintUserID As Integer = -1
    Private menOperation As clsBudgetEmp_timesheet.enBudgetTimesheetStatus

#End Region

#Region " Page's Events "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objBudgetTimesheet = New clsBudgetEmp_timesheet

            mdicETS = dgvEmpTimesheetList.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvEmpTimesheetList.Columns.IndexOf(x))

            If IsPostBack = False Then

                If Session("PeriodID") IsNot Nothing Then
                    mintPeriodID = CInt(Session("PeriodID"))
                End If

                If Session("OperationID") IsNot Nothing Then
                    menOperation = CType(Session("OperationID"), clsBudgetEmp_timesheet.enBudgetTimesheetStatus)
                End If

                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    mintLoginTypeID = enLogin_Mode.MGR_SELF_SERVICE
                    mintLoginEmployeeID = 0
                    mintUserID = CInt(Session("UserId"))
                    SetVisibility()
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    mintLoginTypeID = enLogin_Mode.EMP_SELF_SERVICE
                    mintLoginEmployeeID = CInt(Session("Employeeunkid"))
                    mintUserID = 0
                End If
                Call FillCombo()

            Else
                mintPeriodID = CInt(Me.ViewState("PeriodID"))
                mdtStartDate = CDate(Me.ViewState("StartDate")).Date
                mdtEndDate = CDate(Me.ViewState("EndDate")).Date
                mdtEmpTimesheet = CType(Me.ViewState("EmpTimesheetTable"), DataTable)
                mintConstantDays = CInt(Me.ViewState("ConstantDays"))
                mintLoginTypeID = CInt(Me.ViewState("LoginTypeID"))
                mintLoginEmployeeID = CInt(Me.ViewState("LoginEmployeeID"))
                mintUserID = CInt(Me.ViewState("UserID"))
                menOperation = CType(Me.ViewState("OperationID"), clsBudgetEmp_timesheet.enBudgetTimesheetStatus)
            End If


            Call SetControlVisibility()
            Call SetLanguage()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("PeriodID") = mintPeriodID
            Me.ViewState("StartDate") = mdtStartDate
            Me.ViewState("EndDate") = mdtEndDate
            Me.ViewState("EmpTimesheetTable") = mdtEmpTimesheet
            Me.ViewState("ConstantDays") = mintConstantDays
            Me.ViewState("LoginTypeID") = mintLoginTypeID
            Me.ViewState("LoginEmployeeID") = mintLoginEmployeeID
            Me.ViewState("UserID") = mintUserID
            Me.ViewState("OperationID") = menOperation
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
                                               CDate(Session("fin_startdate")).Date, "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = mintPeriodID.ToString
            End With
            objPeriod = Nothing
            dsList = Nothing
            Call cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())

            Select Case menOperation
                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED


                    'Pinkal (03-Jan-2020) -- Start
                    'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
                    'Dim dtTable As DataTable = objMaster.getLeaveStatusList("Status", False, False).Tables(0).Select("statusunkid IN (0,1,2)").CopyToDataTable  'statusunkid = 4 = Rescheduled
                    Dim dtTable As DataTable = objMaster.getLeaveStatusList("Status", "", False, False).Tables(0).Select("statusunkid IN (0,1,2)").CopyToDataTable  'statusunkid = 4 = Rescheduled
                    'Pinkal (03-Jan-2020) -- End


                    With cboStatus
                        .DataValueField = "statusunkid"
                        .DataTextField = "name"
                        .DataSource = dtTable
                        .DataBind()
                        .SelectedValue = "0"
                    End With
            End Select

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Try
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED Then
                    If CBool(Session("AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval")) = False Then Exit Sub
                ElseIf menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL Then
                    If CBool(Session("AllowToViewPendingEmpBudgetTimesheetSubmitForApproval")) = False Then Exit Sub
                End If
            End If

            Dim dsList As DataSet = Nothing
            Dim strSearch As String = ""

            If dtpDate.GetDate <> Nothing Then
                strSearch &= "AND ltbemployee_timesheet.activitydate='" & eZeeDate.convertDate(dtpDate.GetDate) & "' "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND ltbemployee_timesheet.employeeunkid=" & CInt(cboEmployee.SelectedValue) & " "
            End If

            Select Case menOperation
                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL, clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED
                    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=0 AND ltbemployee_timesheet.statusunkid=2 "

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED
                    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=1 "
                    If CInt(cboStatus.SelectedValue) > 0 Then
                        strSearch &= "AND ltbemployee_timesheet.statusunkid=" & CInt(cboStatus.SelectedValue) & " "
                    End If

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED
                    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=1 AND ltbemployee_timesheet.statusunkid=1 "

            End Select

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            Dim mblnApplyAccessFilter As Boolean = True
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                mblnApplyAccessFilter = False
            End If

            dsList = objBudgetTimesheet.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), _
                                             True, CBool(Session("IsIncludeInactiveEmp")), CBool(Session("AllowOverTimeToEmpTimesheet")), , True, True, _
                                             CInt(cboPeriod.SelectedValue), , strSearch, mblnApplyAccessFilter, True)

        
            If CBool(Session("AllowOverTimeToEmpTimesheet")) Then

                Dim objEmpHoliday As New clsemployee_holiday
                Dim dsHolidayList As DataSet = objEmpHoliday.GetList("Holiday", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                  , mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True _
                                                                                                  , CBool(Session("IsIncludeInactiveEmp")), mblnApplyAccessFilter, -1, Nothing, _
                                                                                                  , " AND  CONVERT(char(8),lvholiday_master.holidaydate,112) >= '" & eZeeDate.convertDate(mdtStartDate) & "'  AND CONVERT(char(8),lvholiday_master.holidaydate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "'")

                objEmpHoliday = Nothing

                For Each drRow As DataRow In dsHolidayList.Tables(0).Rows
                    Dim dRow() As DataRow = dsList.Tables(0).Select("ADate = '" & drRow("holidaydate").ToString() & "' AND employeeunkid = " & CInt(drRow("employeeunkid")))
                    If dRow.Length > 0 Then
                        For i As Integer = 0 To dRow.Length - 1
                            dRow(i)("isholiday") = True
                            dRow(i).AcceptChanges()
                        Next
                    End If
                Next

                dsList.AcceptChanges()
            End If


            dgvEmpTimesheetList.AutoGenerateColumns = False

            If menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED AndAlso CInt(cboStatus.SelectedValue) > 0 Then
                dgvEmpTimesheetList.Columns(mdicETS("dgcolhApprovedActHrs")).Visible = True

            ElseIf menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED Then
                dgvEmpTimesheetList.Columns(mdicETS("dgcolhApprovedActHrs")).Visible = True
            End If


            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            If Session("CompanyGroupName").ToString().ToUpper() = "PSI MALAWI" Then
                dgvEmpTimesheetList.Columns(getColumnId_Datagrid(dgvEmpTimesheetList, "dgcolhCostCenter", False, True)).Visible = True
            End If
            'Pinkal (06-Jan-2023) -- End


            mdtEmpTimesheet = dsList.Tables("List")
            dgvEmpTimesheetList.DataSource = mdtEmpTimesheet

            Dim strColumnArray() As String = {"dgcolhDate"}
            Dim dCol = dgvEmpTimesheetList.Columns.Cast(Of DataGridColumn).Where(Function(x) strColumnArray.Contains(x.FooterText))
            If dCol.Count > 0 Then
                For Each dc As DataGridColumn In dCol
                    CType(dgvEmpTimesheetList.Columns(dgvEmpTimesheetList.Columns.IndexOf(dc)), BoundColumn).DataFormatString = "{0:" & Session("DateFormat").ToString & "}"
                Next
            End If

            dgvEmpTimesheetList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetControlVisibility()
        Try
            Select Case menOperation
                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL
                    lblPageHeader.Text = Language.getMessage(mstrModuleName, 6, "Pending Submit For Approval")
                    btnSubmitForApproval.Visible = True
                    btnCancel.Visible = False
                    btnDelete.Visible = False
                    lblStatus.Visible = False
                    cboStatus.Visible = False
                    dgvEmpTimesheetList.Columns(mdicETS("objdgcolhSelect")).Visible = True
                    dgvEmpTimesheetList.Columns(mdicETS("dgcolhStatus")).Visible = False
                    lblStatus.Visible = False
                    cboStatus.Visible = False
                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    'LblSubmissionRemark.Visible = True
                    'txtSubmissionRemark.Visible = True
                    pnlSubmissionRemark.Visible = True
                    'Gajanan [17-Sep-2020] -- End

                    'Pinkal (28-Jul-2018) -- End


                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED
                    lblPageHeader.Text = Language.getMessage(mstrModuleName, 7, "Completed Submit For Approval")
                    btnSubmitForApproval.Visible = False
                    btnCancel.Visible = False
                    btnDelete.Visible = False
                    lblStatus.Visible = True
                    cboStatus.Visible = True
                    dgvEmpTimesheetList.Columns(mdicETS("objdgcolhSelect")).Visible = False
                    dgvEmpTimesheetList.Columns(mdicETS("dgcolhStatus")).Visible = True
                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    'LblSubmissionRemark.Visible = False
                    'txtSubmissionRemark.Visible = False
                    pnlSubmissionRemark.Visible = False
                    'Gajanan [17-Sep-2020] -- End

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED
                    lblPageHeader.Text = Language.getMessage(mstrModuleName, 8, "Global Cancel Timesheet")
                    btnCancel.Visible = True
                    btnSubmitForApproval.Visible = False
                    btnDelete.Visible = False
                    lblStatus.Visible = False
                    cboStatus.Visible = False

                    dgvEmpTimesheetList.Columns(mdicETS("objdgcolhSelect")).Visible = True
                    dgvEmpTimesheetList.Columns(mdicETS("dgcolhStatus")).Visible = False
                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    'LblSubmissionRemark.Visible = False
                    'txtSubmissionRemark.Visible = False
                    pnlSubmissionRemark.Visible = False
                    'Gajanan [17-Sep-2020] -- End

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED
                    lblPageHeader.Text = Language.getMessage(mstrModuleName, 9, "Global Delete Timesheet")
                    btnDelete.Visible = True
                    btnSubmitForApproval.Visible = False
                    btnCancel.Visible = False
                    dgvEmpTimesheetList.Columns(mdicETS("objdgcolhSelect")).Visible = True
                    dgvEmpTimesheetList.Columns(mdicETS("dgcolhStatus")).Visible = False
                    lblStatus.Visible = False
                    cboStatus.Visible = False
                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    'LblSubmissionRemark.Visible = False
                    'txtSubmissionRemark.Visible = False
                    pnlSubmissionRemark.Visible = False
                    'Gajanan [17-Sep-2020] -- End
            End Select

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnSubmitForApproval.Enabled = CBool(Session("AllowToSubmitForApprovalForPendingEmpBudgetTimesheet"))
            btnDelete.Enabled = CBool(Session("AllowToDeleteEmployeeBudgetTimesheet"))
            btnCancel.Enabled = CBool(Session("AllowToCancelEmployeeBudgetTimesheet"))

            If menOperation <> clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL Then
                dgvEmpTimesheetList.Columns(mdicETS("dgcolhSubmissionRemark")).Visible = True
            Else
                dgvEmpTimesheetList.Columns(mdicETS("dgcolhSubmissionRemark")).Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region " ComboBox's Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
                mdtStartDate = CDate(objPeriod._TnA_StartDate).Date
                mdtEndDate = CDate(objPeriod._TnA_EndDate).Date
                mintConstantDays = Convert.ToInt32(DateDiff(DateInterval.Day, CDate(objPeriod._TnA_StartDate).Date, CDate(CDate(objPeriod._TnA_EndDate).Date)) + 1)

                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    Dim objEmployee As New clsEmployee_Master
                    Dim dsList As DataSet = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                     CInt(Session("CompanyUnkId")), mdtStartDate, mdtEndDate, _
                                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                                        CBool(Session("IsIncludeInactiveEmp")), "List", True)


                    With cboEmployee
                        .DataValueField = "employeeunkid"
                        .DataTextField = "EmpCodeName"
                        .DataSource = dsList.Tables("List")
                        .DataBind()
                        .SelectedValue = "0"
                    End With
                    objEmployee = Nothing
                    dsList = Nothing
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    Dim objglobalassess = New GlobalAccess
                    objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                    cboEmployee.DataSource = objglobalassess.ListOfEmployee
                    cboEmployee.DataTextField = "loginname"
                    cboEmployee.DataValueField = "employeeunkid"
                    cboEmployee.DataBind()
                End If

            Else
                dtpDate.SetDate = Nothing
                mdtStartDate = Nothing : mdtEndDate = Nothing
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory information. Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            Call FillList()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboPeriod.SelectedValue = "0"
            dtpDate.SetDate = Nothing
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then cboEmployee.SelectedValue = "0"
            cboStatus.SelectedValue = "0"

            dgvEmpTimesheetList.DataSource = New List(Of String)
            dgvEmpTimesheetList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSubmitForApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitForApproval.Click
        Try
            Dim blnFlag As Boolean = False
            objBudgetTimesheet = New clsBudgetEmp_timesheet


            If CBool(Session("RemarkMandatoryForTimesheetSubmission")) AndAlso txtSubmissionRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Submission Remark is compulsory information.Please enter submission remark."), Me)
                txtSubmissionRemark.Focus()
                Exit Sub
            End If


            If mdtEmpTimesheet Is Nothing Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvEmpTimesheetList.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True)

            If gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
                Exit Sub
            End If

            gRow = Nothing
            gRow = dgvEmpTimesheetList.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CBool(x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhIsGrp", False, True)).Text) = False And CInt(x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhIsSubmitForApproval", False, True)).Text) = 1)
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Some of the checked employee activity(s) is already sent for approval."), Me)
                Exit Sub
            End If
            gRow = Nothing

            gRow = dgvEmpTimesheetList.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CBool(x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhIsGrp", False, True)).Text) = False)
            If gRow IsNot Nothing AndAlso gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
                Exit Sub
            End If

            gRow = Nothing
            gRow = dgvEmpTimesheetList.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CBool(x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhIsGrp", False, True)).Text) = False And CInt(x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhIsSubmitForApproval", False, True)).Text) = 0)
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                For i As Integer = 0 To gRow.Count - 1
                    mdtEmpTimesheet.Rows(gRow(i).ItemIndex)("IsChecked") = True
                    mdtEmpTimesheet.Rows(gRow(i).ItemIndex)("submission_remark") = txtSubmissionRemark.Text.Trim()
                Next
                mdtEmpTimesheet.AcceptChanges()
            End If


            'dR = dtEmpTimeSheet.Select("IsChecked=1 AND IsGrp=0 AND issubmit_approval = 0")
            'If dR.Length > 0 Then
            '    For i As Integer = 0 To dR.Length - 1
            '        dR(i)("submission_remark") = txtSubmissionRemark.Text.Trim()
            '    Next
            '    dtEmpTimeSheet.AcceptChanges()
            'End If


            'Dim dtEmpTimeSheet As DataTable = New DataView(mdtEmpTimesheet, "IsChecked=1 AND IsGrp=0", "", DataViewRowState.CurrentRows).ToTable 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]

            'If dtEmpTimeSheet.Rows.Count <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
            '    Exit Sub
            'End If

            'Dim dR As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0 AND issubmit_approval=1") 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
            'If dR.Length > 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Some of the checked employee activity(s) is already sent for approval."), Me)
            '    Exit Sub
            'End If

            'If CBool(Session("RemarkMandatoryForTimesheetSubmission")) AndAlso txtSubmissionRemark.Text.Trim.Length <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Submission Remark is compulsory information.Please enter submission remark."), Me)
            '    txtSubmissionRemark.Focus()
            '    Exit Sub
            'End If

            'dR = Nothing
            'dR = dtEmpTimeSheet.Select("IsChecked=1 AND IsGrp=0 AND issubmit_approval = 0")
            'If dR.Length > 0 Then
            '    For i As Integer = 0 To dR.Length - 1
            '        dR(i)("submission_remark") = txtSubmissionRemark.Text.Trim()
            '    Next
            '    dtEmpTimeSheet.AcceptChanges()
            'End If

            'objBudgetTimesheet._dtEmployeeActivity = dtEmpTimeSheet

            Dim drRow = mdtEmpTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsChecked") = True And x.Field(Of Boolean)("IsGrp") = False And x.Field(Of Integer)("issubmit_approval") = 0)

            If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                objBudgetTimesheet._dtEmployeeActivity = drRow.CopyToDataTable()
            End If

            objBudgetTimesheet._Periodunkid = CInt(cboPeriod.SelectedValue)

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objBudgetTimesheet._Userunkid = CInt(Session("UserId"))
                objBudgetTimesheet._LoginEmployeeunkid = 0
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objBudgetTimesheet._Userunkid = CInt(Session("UserId"))
                objBudgetTimesheet._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objBudgetTimesheet._WebFormName = mstrModuleName
            objBudgetTimesheet._WebClientIP = CStr(Session("IP_ADD"))
            objBudgetTimesheet._WebHostName = CStr(Session("HOST_NAME"))

            objBudgetTimesheet._IsSubmitForApproval = True

            If CBool(Session("NotAllowIncompleteTimesheet")) = True Then


                blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtEmpTimesheet, mintConstantDays, mdtStartDate, mdtEndDate, _
                                                                                                           clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL, _
                                                                                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                                                           CBool(Session("AllowOverTimeToEmpTimesheet")))

                If objBudgetTimesheet._Message <> "" Then
                    DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                    Exit Sub
                Else
                    If blnFlag = False Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot perform Submit For Approval. Reason : your timesheet is incomplete, please complete it and submit again."), Me)
                        Exit Sub
                    End If
                End If
            End If


            If objBudgetTimesheet.Update(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), _
                                         CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                       CBool(Session("IsIncludeInactiveEmp")), True, CBool(Session("AllowOverTimeToEmpTimesheet"))) = False Then
                DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                Exit Sub
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Employee Timesheet activity(s) submitted for approval successfully."), Me)

                'Dim strEmployeeIDs As String = String.Join(",", dtEmpTimeSheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)
                'Dim dtSelectedData As DataTable = New DataView(mdtEmpTimesheet, "IsChecked=1", "", DataViewRowState.CurrentRows).ToTable


                'objBudgetTimesheet.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                '                                          CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                          CBool(Session("IsIncludeInactiveEmp")), strEmployeeIDs, CInt(cboPeriod.SelectedValue), _
                '                                          -1, dtSelectedData, mintLoginTypeID, mintLoginEmployeeID, mintUserID, _
                '                                          CStr(Session("ArutiSelfServiceURL")), False)

                Dim strEmployeeIDs As String = String.Join(",", drRow.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)
                Dim dRow = mdtEmpTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsChecked") = True)

                objBudgetTimesheet.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                                                          CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                          CBool(Session("IsIncludeInactiveEmp")), strEmployeeIDs, CInt(cboPeriod.SelectedValue), _
                                                          -1, dRow.DefaultIfEmpty().CopyToDataTable(), mintLoginTypeID, mintLoginEmployeeID, mintUserID, _
                                                          CStr(Session("ArutiSelfServiceURL")), False)
            
                txtSubmissionRemark.Text = ""
            End If

            Call FillList()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_EmployeeTimeSheet.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If dgvEmpTimesheetList.Items.Count <= 0 Then Exit Sub

            'Dim dRow As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0")
            'If dRow.Length <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
            '    Exit Sub
            'End If
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvEmpTimesheetList.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CBool(x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhIsGrp", False, True)).Text) = False)
            If gRow IsNot Nothing AndAlso gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
                Exit Sub
            End If

            'Pinkal (02-Sep-2022) -- Start
            'Enhancement Baylor Malawi : Baylor - Put Setting on confirguration for timesheet approval notification to include holiday and weekend entries.
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                For i As Integer = 0 To gRow.Count - 1
                    mdtEmpTimesheet.Rows(gRow(i).ItemIndex)("IsChecked") = True
                Next
                mdtEmpTimesheet.AcceptChanges()
            End If
            'Pinkal (02-Sep-2022) -- End

            Language.setLanguage(mstrModuleName)
            popupConfirmYesNo.Title = Language.getMessage(mstrModuleName, 8, "Global Cancel Timesheet")
            popupConfirmYesNo.Message = Language.getMessage(mstrModuleName, 12, "Are you sure you want to cancel this employee timesheet?")
            popupConfirmYesNo.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If dgvEmpTimesheetList.Items.Count <= 0 Then Exit Sub

            'Dim dRow As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0")
            'If dRow.Length <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
            '    Exit Sub
            'End If
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvEmpTimesheetList.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CBool(x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhIsGrp", False, True)).Text) = False)
            If gRow IsNot Nothing AndAlso gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
                Exit Sub
            End If

            Language.setLanguage(mstrModuleName)
            popupConfirmYesNo.Title = Language.getMessage(mstrModuleName, 9, "Global Delete Timesheet")
            popupConfirmYesNo.Message = Language.getMessage(mstrModuleName, 10, "Are you sure you want to delete this employee timesheet?")
            popupConfirmYesNo.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupConfirmYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmYesNo.buttonYes_Click
        Try
            Select Case menOperation
                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED
                    Language.setLanguage(mstrModuleName)
                    popupDeleteReason.Title = Language.getMessage(mstrModuleName, 21, "Cancel Reason")
                    popupDeleteReason.Reason = ""
                    popupDeleteReason.Show()

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED
                    Language.setLanguage(mstrModuleName)
                    popupDeleteReason.Title = "Delete Reason"
                    popupDeleteReason.Reason = ""
                    popupDeleteReason.Show()

            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteReason.buttonDelReasonYes_Click
        Try
            objBudgetTimesheet = New clsBudgetEmp_timesheet

            Call SetDateFormat()

            Select Case menOperation

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED

                    Dim blnFlag As Boolean
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)

                    If CBool(Session("NotAllowIncompleteTimesheet")) = True Then

                        blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtEmpTimesheet, CInt(objPeriod._Constant_Days), _
                                                                                                                   objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                                                                                                                   clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED, _
                                                                                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                                                                   CBool(Session("AllowOverTimeToEmpTimesheet")))

                        If objBudgetTimesheet._Message <> "" Then
                            DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                            Exit Sub
                        Else
                            If blnFlag = False Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, you cannot perform Cancel operation. Reason : your timesheet is incomplete, please complete it."), Me)
                                Exit Sub
                            End If
                        End If
                    End If

                    'Dim strEmployeeIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)

                    'Dim strEmpTimesheetIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False AndAlso x.Field(Of Boolean)("ischecked") = True) _
                    '                                               .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)

                    Dim strEmployeeIDs As String = String.Join(",", dgvEmpTimesheetList.Items.Cast(Of DataGridItem).Select(Function(x) x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhEmployeeID", False, True)).Text).Distinct.ToArray())

                    Dim strEmpTimesheetIDs As String = String.Join(",", dgvEmpTimesheetList.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True _
                                                                                                AndAlso CBool(x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhIsGrp", False, True)).Text) = False) _
                                                                                                .Select(Function(x) x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhEmpTimesheetID", False, True)).Text).Distinct.ToArray())

                    objBudgetTimesheet._Userunkid = CInt(Session("UserId"))
                    objBudgetTimesheet._WebFormName = mstrModuleName
                    objBudgetTimesheet._WebClientIP = CStr(Session("IP_ADD"))
                    objBudgetTimesheet._WebHostName = CStr(Session("HOST_NAME"))

                    If objBudgetTimesheet.CancelTimesheet(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), _
                                                          CInt(Session("CompanyUnkId")), CBool(Session("IsIncludeInactiveEmp")), _
                                                      CStr(Session("UserAccessModeSetting")), popupDeleteReason.Reason, strEmpTimesheetIDs, CBool(Session("AllowOverTimeToEmpTimesheet")), Nothing) = False Then

                        DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                        Exit Sub
                    Else
                        objBudgetTimesheet.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                      CBool(Session("IsIncludeInactiveEmp")), strEmployeeIDs, strEmpTimesheetIDs, _
                                                                      CInt(cboPeriod.SelectedValue), clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED, _
                                                                      mintLoginTypeID, mintLoginEmployeeID, mintUserID, CStr(Session("ArutiSelfServiceURL")), _
                                                                      popupDeleteReason.Reason, , , False)
                    End If

                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Timesheet Cancelled Successfully."), Me)

                    Call FillList()

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED

                    'For Each row As DataRow In mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0")
                    '    objBudgetTimesheet._Voiduserunkid = CInt(Session("UserId"))
                    '    objBudgetTimesheet._LoginEmployeeunkid = -1
                    '    objBudgetTimesheet._Voidreason = popupDeleteReason.Reason
                    '    objBudgetTimesheet._WebFormName = mstrModuleName
                    '    objBudgetTimesheet._WebClientIP = CStr(Session("IP_ADD"))
                    '    objBudgetTimesheet._WebHostName = CStr(Session("HOST_NAME"))

                    '    If objBudgetTimesheet.Delete(CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                    '                                 CInt(row.Item("emptimesheetunkid"))) = False Then
                    '        DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                    '        Exit Sub
                    '    End If
                    'Next

                    'Dim strEmployeeIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)
                    'Dim strEmpTimesheetIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False) _
                    '                                               .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)

                    Dim gRow As IEnumerable(Of DataGridItem) = Nothing
                    gRow = dgvEmpTimesheetList.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelect"), CheckBox).Checked = True And CBool(x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhIsGrp", False, True)).Text) = False)
                    If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                        For Each row As DataGridItem In gRow
                            objBudgetTimesheet._Voiduserunkid = CInt(Session("UserId"))
                            objBudgetTimesheet._LoginEmployeeunkid = -1
                            objBudgetTimesheet._Voidreason = popupDeleteReason.Reason
                            objBudgetTimesheet._WebFormName = mstrModuleName
                            objBudgetTimesheet._WebClientIP = CStr(Session("IP_ADD"))
                            objBudgetTimesheet._WebHostName = CStr(Session("HOST_NAME"))

                            If objBudgetTimesheet.Delete(CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         CInt(row.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhEmpTimesheetID", False, True)).Text)) = False Then
                                DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                                Exit Sub
                            End If
                        Next

                    End If

                    Dim strEmployeeIDs As String = String.Join(",", dgvEmpTimesheetList.Items.Cast(Of DataGridItem).Select(Function(x) x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhEmployeeID", False, True)).Text).Distinct.ToArray())
                    Dim strEmpTimesheetIDs As String = String.Join(",", dgvEmpTimesheetList.Items.Cast(Of DataGridItem).Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhIsGrp", False, True)).Text) = False) _
                                                                                                .Select(Function(x) x.Cells(getColumnId_Datagrid(dgvEmpTimesheetList, "objdgcolhEmpTimesheetID", False, True)).Text).Distinct.ToArray())

                    objBudgetTimesheet.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                  CBool(Session("IsIncludeInactiveEmp")), strEmployeeIDs, strEmpTimesheetIDs, _
                                                                  CInt(cboPeriod.SelectedValue), clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED, _
                                                                  mintLoginTypeID, mintLoginEmployeeID, mintUserID, CStr(Session("ArutiSelfServiceURL")), _
                                                                  popupDeleteReason.Reason, , , False)

                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Timesheet deleted Successfully."), Me)

                    Call FillList()

            End Select

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " DataGrid's Events "

    Protected Sub dgvEmpTimesheetList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvEmpTimesheetList.ItemDataBound
        Try
            Call SetDateFormat()

            dgvEmpTimesheetList.Columns(mdicETS("dgcolhActivity")).Visible = CBool(Session("ShowBgTimesheetActivity"))
            dgvEmpTimesheetList.Columns(mdicETS("dgcolhProject")).Visible = CBool(Session("ShowBgTimesheetActivityProject"))

            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)

                Dim intVisible = dgvEmpTimesheetList.Columns.Cast(Of DataGridColumn).Where(Function(x) x.Visible = True).Count

                If e.Item.ItemIndex >= 0 Then

                    If CBool(dr("IsGrp")) = True Then 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]

                        e.Item.Cells(mdicETS("dgcolhEmployee")).ColumnSpan = intVisible

                        For i = mdicETS("dgcolhEmployee") + 1 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).Visible = False
                        Next

                        'Gajanan [17-Sep-2020] -- Start
                        'New UI Change
                        'e.Item.Cells(mdicETS("dgcolhEmployee")).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(mdicETS("dgcolhEmployee")).CssClass = "group-header"
                        'Gajanan [17-Sep-2020] -- End

                        e.Item.Cells(mdicETS("dgcolhEmployee")).Style.Add("text-align", "left")

                        Select Case menOperation
                            Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL, clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED, _
                                 clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED

                                CType(e.Item.Cells(mdicETS("objdgcolhSelect")).FindControl("chkSelect"), CheckBox).Visible = False
                                'Gajanan [17-Sep-2020] -- Start
                                'New UI Change
                                'e.Item.Cells(mdicETS("objdgcolhSelect")).CssClass = "GroupHeaderStyleBorderLeft"
                                e.Item.Cells(mdicETS("objdgcolhSelect")).CssClass = "group-header"
                                'Gajanan [17-Sep-2020] -- End

                            Case Else
                                e.Item.Cells(mdicETS("dgcolhEmployee")).Style.Add("padding-left", "10px")
                        End Select

                    Else
                        If CBool(dr("isholiday")) Then
                            For i As Integer = 0 To e.Item.Cells.Count - 1
                                e.Item.Cells(i).Style.Add("color", "red")
                            Next
                        End If
                    End If

                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    'Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chkSelectAll As CheckBox = CType(sender, CheckBox)


    '        'Pinkal (05-Sep-2020) -- Start
    '        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    '        If mdtEmpTimesheet Is Nothing Then Exit Sub
    '        'Pinkal (05-Sep-2020) -- End
    '        If mdtEmpTimesheet.Rows.Count <= 0 Then Exit Sub



    '        For Each item As DataGridItem In dgvEmpTimesheetList.Items
    '            CType(item.Cells(mdicETS("objdgcolhSelect")).FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
    '            Dim dRow As DataRow() = mdtEmpTimesheet.Select("emptimesheetunkid=" & CInt(item.Cells(mdicETS("objdgcolhEmpTimesheetID")).Text))
    '            If dRow.Length > 0 Then
    '                For Each dR In dRow
    '                    dR.Item("IsChecked") = chkSelectAll.Checked
    '                Next
    '            End If
    '        Next
    '        mdtEmpTimesheet.AcceptChanges()
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If dgvEmpTimesheetList.Items.Count <= 0 Then Exit Sub

    '        Dim chkSelect As CheckBox = CType(sender, CheckBox)
    '        Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)

    '        Dim dsRow As DataRow() = mdtEmpTimesheet.Select("emptimesheetunkid=" & CInt(item.Cells(mdicETS("objdgcolhEmpTimesheetID")).Text))
    '        If dsRow.Length > 0 Then
    '            dsRow(0).Item("IsChecked") = chkSelect.Checked
    '        End If
    '        mdtEmpTimesheet.AcceptChanges()

    '        Dim drRow As DataRow() = mdtEmpTimesheet.Select("IsGrp=0 AND employeeunkid=" & CInt(item.Cells(mdicETS("objdgcolhEmployeeID")).Text)) 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
    '        Dim dRow As DataRow() = mdtEmpTimesheet.Select("IsGrp=0 AND IsChecked=0 AND employeeunkid=" & CInt(item.Cells(mdicETS("objdgcolhEmployeeID")).Text)) 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
    '        Dim gRow As DataRow() = mdtEmpTimesheet.Select("IsGrp=1 AND employeeunkid=" & CInt(item.Cells(mdicETS("objdgcolhEmployeeID")).Text)) 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]

    '        If gRow.Length <= 0 Then Exit Sub

    '        If drRow.Length = dRow.Length Then
    '            gRow(0).Item("IsChecked") = False
    '        Else
    '            gRow(0).Item("IsChecked") = True
    '        End If
    '        gRow(0).AcceptChanges()

    '        Dim xCount As DataRow() = mdtEmpTimesheet.Select("IsChecked=True")
    '        If xCount.Length = dgvEmpTimesheetList.Items.Count Then
    '            CType(dgvEmpTimesheetList.Controls(mdicETS("objdgcolhSelect")).Controls(mdicETS("objdgcolhSelect")).FindControl("chkSelectAll"), CheckBox).Checked = True
    '        Else
    '            CType(dgvEmpTimesheetList.Controls(mdicETS("objdgcolhSelect")).Controls(mdicETS("objdgcolhSelect")).FindControl("chkSelectAll"), CheckBox).Checked = False
    '        End If

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkSelect_CheckedChanged:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

#End Region

#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            If menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL Then
                Me.Title = Language._Object.getCaption("mnuViewPendingSubmitApproval", Me.lblPageHeader.Text)
                Me.lblPageHeader.Text = Language._Object.getCaption("mnuViewPendingSubmitApproval", Me.lblPageHeader.Text)

            ElseIf menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED Then
                Me.Title = Language._Object.getCaption("mnuViewCompletedSubmitApproval", Me.lblPageHeader.Text)
                Me.lblPageHeader.Text = Language._Object.getCaption("mnuViewCompletedSubmitApproval", Me.lblPageHeader.Text)

            ElseIf menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED Then
                Me.Title = Language._Object.getCaption("mnuViewCompletedSubmitApproval", Me.lblPageHeader.Text)
                Me.lblPageHeader.Text = Language._Object.getCaption("mnuViewCompletedSubmitApproval", Me.lblPageHeader.Text)

            ElseIf menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED Then
                Me.Title = Language._Object.getCaption("mnuGlobalDeleteTimesheet", Me.lblPageHeader.Text)
                Me.lblPageHeader.Text = Language._Object.getCaption("mnuGlobalDeleteTimesheet", Me.lblPageHeader.Text)
            End If

            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)

            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)

            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnSubmitForApproval.Text = Language._Object.getCaption(Me.btnSubmitForApproval.ID, Me.btnSubmitForApproval.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.ID, Me.btnCancel.Text).Replace("&", "")
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.ID, Me.btnDelete.Text).Replace("&", "")

            Me.dgvEmpTimesheetList.Columns(2).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(2).FooterText, dgvEmpTimesheetList.Columns(2).HeaderText)
            Me.dgvEmpTimesheetList.Columns(3).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(3).FooterText, dgvEmpTimesheetList.Columns(3).HeaderText)
            Me.dgvEmpTimesheetList.Columns(4).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(4).FooterText, dgvEmpTimesheetList.Columns(4).HeaderText)
            Me.dgvEmpTimesheetList.Columns(5).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(5).FooterText, dgvEmpTimesheetList.Columns(5).HeaderText)
            Me.dgvEmpTimesheetList.Columns(6).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(6).FooterText, dgvEmpTimesheetList.Columns(6).HeaderText)
            Me.dgvEmpTimesheetList.Columns(7).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(7).FooterText, dgvEmpTimesheetList.Columns(7).HeaderText)
            Me.dgvEmpTimesheetList.Columns(8).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(8).FooterText, dgvEmpTimesheetList.Columns(8).HeaderText)
            Me.dgvEmpTimesheetList.Columns(9).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(9).FooterText, dgvEmpTimesheetList.Columns(9).HeaderText)
            Me.dgvEmpTimesheetList.Columns(10).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(10).FooterText, dgvEmpTimesheetList.Columns(10).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

#End Region

End Class
