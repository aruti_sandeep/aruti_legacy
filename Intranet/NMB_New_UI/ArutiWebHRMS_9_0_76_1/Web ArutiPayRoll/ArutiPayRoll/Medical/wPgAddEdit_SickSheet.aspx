﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPgAddEdit_SickSheet.aspx.vb"
    Inherits="Medical_wPgAddEdit_SickSheet" MasterPageFile="~/Home1.master" Title="Sick Sheet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
            
            <div class="row clearfix d--f fd--c ai--c">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Sick Sheet Form"></asp:Label>
                                </h2>
                            </div>
                             <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                                <div class="header">
                                                    <h2>
                                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Sick Sheet Add/Edit"></asp:Label>
                                                    </h2>
                                                 </div>
                                                 <div class="body">
                                                          <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                    <asp:Label ID="lblSheetNo" runat="server" Text="Sick Sheet No " CssClass="form-label" />
                                                                    <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtSickSheetNo" runat="server" CssClass="form-control" />
                                                                             </div>
                                                                    </div>
                                                            </div>
                                                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                     <asp:Label ID="lblDate" runat="server" Text="Date"  CssClass="form-label" />
                                                                     <uc2:DateCtrl ID="dtSheetdate" runat="server" AutoPostBack="false" />
                                                             </div>
                                                        </div>
                                                          <div class="row clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                         <asp:Label ID="lblProvider" runat="server" Text="Provider" CssClass="form-label" />
                                                                         <div class="form-group">
                                                                                <asp:DropDownList ID="drpProvider" runat="server" />
                                                                          </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                         <asp:Label ID="lblEmployeecode" runat="server" Text="Employee Code" CssClass="form-label" />
                                                                         <div class="form-group">
                                                                                <div class="form-line">
                                                                                        <asp:TextBox ID="txtEmployeeCode" runat="server" AutoPostBack="true" CssClass="form-control" />
                                                                                </div>
                                                                           </div>     
                                                                </div>
                                                                
                                                         </div>
                                                          <div class="row clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="lblEmployeeName" runat="server" Text="Employee Name" CssClass="form-label" />
                                                                        <div class="form-group">
                                                                                <asp:DropDownList ID="drpEmployeeName" runat="server" AutoPostBack="True" />
                                                                        </div>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="lblJob" runat="server" Text="Job / Position" CssClass="form-label" />
                                                                         <div class="form-group">
                                                                                <div class="form-line">
                                                                                        <asp:TextBox ID="txtJob" runat="server" ReadOnly="true" CssClass="form-control" />
                                                                                </div>
                                                                         </div>       
                                                                </div>
                                                          </div>
                                                          <div class="row clearfix">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="lblCover" runat="server" Text="Medical Cover" CssClass="form-label" />
                                                                        <div class="form-group">
                                                                                <asp:DropDownList ID="drpMedicalCover" runat="server" />
                                                                        </div>
                                                                </div>
                                                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="LblSickSheetAllocation" runat="server" Text="#Allocation" CssClass="form-label" />
                                                                         <div class="form-group">
                                                                                <asp:DropDownList ID="cboSickSheetAllocation" runat="server" />
                                                                         </div>
                                                                </div>        
                                                          </div>
                                                 </div>           
                                        </div>
                                   </div>
                              </div>
                                <div class="row clearfix">
                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="height: 300px">
                                                                <asp:DataGrid ID="gvdependants" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                                    CssClass="table table-hover table-bordered" >
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkAllDependant" runat="server" OnCheckedChanged="chkAllDependance_CheckedChanged"
                                                                                    AutoPostBack="true" CssClass="filled-in" Text=" "  />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkDependant" runat="server" CssClass="filled-in" Text=" " />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="dependants" HeaderText="Dependant" ReadOnly="true" FooterText="colhEmpName"
                                                                            HeaderStyle-HorizontalAlign="Left" />
                                                                        <asp:BoundColumn DataField="gender" HeaderText="Gender" ReadOnly="true" FooterText="colhGender"
                                                                            HeaderStyle-HorizontalAlign="Left" />
                                                                        <asp:BoundColumn DataField="age" HeaderText="Age" ReadOnly="true" ItemStyle-HorizontalAlign="Right"
                                                                            HeaderStyle-HorizontalAlign="Left" FooterText="colhAge" />
                                                                        <asp:BoundColumn DataField="Months" HeaderText="Month" ReadOnly="true" ItemStyle-HorizontalAlign="Right"
                                                                            HeaderStyle-HorizontalAlign="Left" FooterText="colhRelation" />
                                                                        <asp:BoundColumn DataField="relation" HeaderText="Relation" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" />
                                                                        <asp:BoundColumn DataField="dependent_Id" Visible="false"></asp:BoundColumn>
                                                                    </Columns>
                                                                    <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                                                </asp:DataGrid>
                                                 </div>
                                      </div>                  
                               </div>
                            </div>
                            <div class="footer">
                                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />
                                    <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                    <asp:HiddenField ID="hdpopup" runat="server" />
                            </div>
                       </div>
                    </div>
               </div>                         
            <%--
                <div class="panel-primary">
                    <div class="panel-heading">
                        
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 15%">
                                            
                                        </td>
                                        <td style="width: 35%">
                                           
                                        </td>
                                        <td style="width: 15%">
                                           
                                        </td>
                                        <td style="width: 35%">
                                            
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 15%">
                                           
                                        </td>
                                        <td style="width: 35%">
                                           
                                        </td>
                                        <td style="width: 15%">
                                           
                                        </td>
                                        <td style="width: 35%">
                                            
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 15%">
                                            
                                        </td>
                                        <td style="width: 35%">
                                            
                                        </td>
                                        <td style="width: 15%">
                                           
                                        </td>
                                        <td style="width: 35%">
                                            
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 15%">
                                            
                                        </td>
                                        <td style="width: 35%">
                                            
                                        </td>
                                        <td style="width: 15%">
                                        </td>
                                        <td style="width: 35%">
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 15%">
                                            
                                        </td>
                                        <td style="width: 25%">
                                            
                                        </td>
                                        <td style="width: 15%">
                                        </td>
                                        <td style="width: 35%">
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 100%" colspan="4">
                                            <asp:Panel ID="pnl_gvdependants" Style="margin-top: 5px" ScrollBars="Auto" Height="300px"
                                                runat="server">
                                                
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                                <div id="btnfixedbottom" class="btn-default">
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <uc8:ConfirmYesNo ID="popYesNo" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
