﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgTrancheApproverLevel.aspx.vb" Inherits="Loan_Savings_New_Loan_Tranche_Loan_Approver_Level_wPgTrancheApproverLevel" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>



    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Loan Tranche Approver Level List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader2" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
                                            <asp:DataGrid ID="dgApprLevelList" DataKeyNames="stlevelunkid" runat="server" AutoGenerateColumns="False"
                                                Width="99%" CssClass="table table-hover table-bordered" AllowPaging="false">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="5%" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderText="Edit" FooterText="colhedit">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkedit" runat="server" OnClick="lnkedit_Click" CommandArgument='<%#Eval("lnlevelunkid") %>' ToolTip = "Edit">
                                                                <i class="fas fa-pencil-alt text-primary"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="5%" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center" 
                                                        HeaderText="Delete" FooterText="colhdelete">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" CommandArgument='<%#Eval("lnlevelunkid") %>' ToolTip = "Delete"> 
                                                                    <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="lnlevelname" HeaderText="Level Name" HeaderStyle-Width="25%"
                                                        FooterText="colhlevelname"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="minrange" HeaderText="Min. Range" HeaderStyle-Width="25%" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                                                        FooterText="colhMinRange"></asp:BoundColumn>
                                                     <asp:BoundColumn DataField="maxrange" HeaderText="Max. Range" HeaderStyle-Width="25%" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                                                        FooterText="colhMaxRange"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="priority" HeaderText="Priority" HeaderStyle-Width="15%"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterText="colhpriority">
                                                    </asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnnew" runat="server" CssClass="btn btn-primary" Text="New" OnClick="btnnew_Click" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupLoanTrancheApproverLevel" BackgroundCssClass="modal-backdrop"
                    TargetControlID="txtlevelname" runat="server" PopupControlID="pnlLoanTrancheApproverLevel"
                    DropShadow="false" CancelControlID="LblLoanTranche">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlLoanTrancheApproverLevel" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblLoanTranche" Text="Loan Tranche Approver Level Add/ Edit" runat="server" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lbllevelname" runat="server" Text="Level Name" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtlevelname" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="LblMinRange" runat="server" Text="Min. Range" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtMinRange" runat="server" CssClass="form-control" style="text-align:right" onKeypress="return onlyNumbers(this, event);" Text = "0.00"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="LblMaxRange" runat="server" Text="Max. Range" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtMaxRange" runat="server" CssClass="form-control" style="text-align:right" onKeypress="return onlyNumbers(this, event);" Text = "0.00" ></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <asp:Label ID="lbllevelpriority" runat="server" Text="Priority" CssClass="form-label" />
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtlevelpriority" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        <cc1:NumericUpDownExtender ID="nudYear" runat="server" Width="100" Minimum="0" TargetControlID="txtlevelpriority">
                                        </cc1:NumericUpDownExtender>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSaveLoanTrancheApproverLevel" runat="server" CssClass="btn btn-primary"
                            Text="Save" />
                        <asp:Button ID="btnCloseLoanTrancheApproverLevel" runat="server" CssClass="btn btn-default"
                            Text="Close" />
                    </div>
                </asp:Panel>
                <uc1:Confirmation ID="popup_YesNo" Title="Confirmation" runat="server" Message="" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
