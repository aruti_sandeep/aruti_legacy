<%@ Page Language="VB"   MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_FlexcubeLoanBalance.aspx.vb"  Inherits="Loan_Savings_New_Loan_Flexcube_LoanBalance_wPg_FlexcubeLoanBalance" Title="Flexcube Loan Balance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0"); witnes.feliLox
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
             <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Flexcube Loan Balance" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <asp:DropDownList ID="cboEmployee" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgvFlexLoanBalance" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                CssClass="table table-hover table-bordered" AllowPaging="false" Width="100%">
                                                <Columns>
                                                        <asp:BoundColumn DataField="CUSTOMER_NO" HeaderText="Customer No" FooterText="dgcolhCustomerNo" />
                                                        <asp:BoundColumn DataField="ACCOUNT_NUMBER" HeaderText="Loan Account No" FooterText="dgcolhLoanAccountNo" />
                                                       <asp:BoundColumn DataField="PRODUCT_CODE" HeaderText="Product Code" FooterText="dgcolhProductcode" />
                                                       <asp:BoundColumn DataField="INSTALMENT_AMOUNT" HeaderText="Number Of service Installments" FooterText="dgcolhInstallmentAmt" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign= "Right"  />
                                                       <asp:BoundColumn DataField="CURRENT_OUTSTANDING_INTEREST" HeaderText="Current Outstanding Interest" FooterText="dgcolhCurrentOutstandingInterest" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign= "Right"   />
                                                       <asp:BoundColumn DataField="PRINCIPALOUTSTANDING" HeaderText="Principal Outstanding" FooterText="dgcolhPrincipalOutstanding" ItemStyle-HorizontalAlign="Right"  HeaderStyle-HorizontalAlign= "Right"  />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
