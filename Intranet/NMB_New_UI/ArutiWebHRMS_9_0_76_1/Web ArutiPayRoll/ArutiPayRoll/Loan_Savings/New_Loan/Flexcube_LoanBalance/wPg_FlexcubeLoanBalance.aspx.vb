﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
#End Region

Partial Class Loan_Savings_New_Loan_Flexcube_LoanBalance_wPg_FlexcubeLoanBalance
    Inherits Basepage

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmFlexcubeLoanBalance"
    Private DisplayMessage As New CommonCodes
    Private mstrFilterTitle As String = String.Empty
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mintEmployeeunkid As Integer = -1
    Private mintLoanAdvanceunkid As Integer = -1
#End Region

#Region "Page's Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                SetMessages()
                GetControlCaptions()
                Call FillCombo()

                If dgvFlexLoanBalance.Items.Count <= 0 Then
                    dgvFlexLoanBalance.DataSource = New List(Of String)
                    dgvFlexLoanBalance.DataBind()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objEmployee As New clsEmployee_Master
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran
            Dim objOtherOpApproval As New clsloanotherop_approval_tran

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     Session("UserAccessModeSetting").ToString, True, False, _
                                                     "Employee", True)
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables("Employee")
                    .DataBind()
                    .SelectedValue = "0"
                End With

            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If
            objEmployee = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList(ByVal mstrBankAccountNo As String, ByVal strEmployeeCode As String)
        'Hemant (29 Mar 2024) -- [strEmployeeCode]
        Dim strSearch As String = String.Empty
        Dim objloanAdvance As New clsLoan_Advance
        Try

            Dim dsFlexLoanData As DataSet = objloanAdvance.GetFlexcubeLoanData(Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString _
                                                                                    , Session("OracleUserPassword").ToString, mstrBankAccountNo, strEmployeeCode, "", False)
            'Hemant (29 Mar 2024) -- [strEmployeeCode]


            dgvFlexLoanBalance.AutoGenerateColumns = False
            dgvFlexLoanBalance.DataSource = dsFlexLoanData.Tables(0)
            dgvFlexLoanBalance.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objloanAdvance = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Events"

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedIndex = 0
            dgvFlexLoanBalance.DataSource = New List(Of String)
            dgvFlexLoanBalance.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Exit Sub
            End If

            Dim objEmpBankTran As New clsEmployeeBanks
            Dim xPeriodStart As Date = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date
            Dim xPeriodEnd As Date = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date

            Dim dsEmpBank As DataSet = objEmpBankTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) _
                                                                                          , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), "EmployeeBank" _
                                                                                          , False, , CStr(cboEmployee.SelectedValue), xPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

            If dsEmpBank IsNot Nothing AndAlso dsEmpBank.Tables(0).Rows.Count > 0 Then
                Call FillList(dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString)
                'Hemant (29 Mar 2024) -- [dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString]
            Else
                dgvFlexLoanBalance.DataSource = New List(Of String)
                dgvFlexLoanBalance.DataBind()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DataGrid Events"
    Protected Sub dgvFlexLoanBalance_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvFlexLoanBalance.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                e.Item.Cells(getColumnId_Datagrid(dgvFlexLoanBalance, "dgcolhInstallmentAmt", False, True)).Text = Format(CDec(getColumnId_Datagrid(dgvFlexLoanBalance, "dgcolhInstallmentAmt", False, True)), Session("fmtCurrency").ToString)
                e.Item.Cells(getColumnId_Datagrid(dgvFlexLoanBalance, "dgcolhCurrentOutstandingInterest", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgvFlexLoanBalance, "dgcolhCurrentOutstandingInterest", False, True)).Text), Session("fmtCurrency").ToString)
                e.Item.Cells(getColumnId_Datagrid(dgvFlexLoanBalance, "dgcolhPrincipalOutstanding", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgvFlexLoanBalance, "dgcolhPrincipalOutstanding", False, True)).Text), Session("fmtCurrency").ToString)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblDetialHeader.ID, Me.lblDetialHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvFlexLoanBalance.Columns(0).FooterText, Me.dgvFlexLoanBalance.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvFlexLoanBalance.Columns(1).FooterText, Me.dgvFlexLoanBalance.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvFlexLoanBalance.Columns(2).FooterText, Me.dgvFlexLoanBalance.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvFlexLoanBalance.Columns(3).FooterText, Me.dgvFlexLoanBalance.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvFlexLoanBalance.Columns(4).FooterText, Me.dgvFlexLoanBalance.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvFlexLoanBalance.Columns(5).FooterText, Me.dgvFlexLoanBalance.Columns(5).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnSearch.ID, btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnReset.ID, btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnClose.ID, btnClose.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDetialHeader.ID, Me.lblDetialHeader.Text)

            Me.dgvFlexLoanBalance.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvFlexLoanBalance.Columns(0).FooterText, Me.dgvFlexLoanBalance.Columns(0).HeaderText)
            Me.dgvFlexLoanBalance.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvFlexLoanBalance.Columns(1).FooterText, Me.dgvFlexLoanBalance.Columns(1).HeaderText)
            Me.dgvFlexLoanBalance.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvFlexLoanBalance.Columns(2).FooterText, Me.dgvFlexLoanBalance.Columns(2).HeaderText)
            Me.dgvFlexLoanBalance.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvFlexLoanBalance.Columns(3).FooterText, Me.dgvFlexLoanBalance.Columns(3).HeaderText)
            Me.dgvFlexLoanBalance.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvFlexLoanBalance.Columns(4).FooterText, Me.dgvFlexLoanBalance.Columns(4).HeaderText)
            Me.dgvFlexLoanBalance.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvFlexLoanBalance.Columns(5).FooterText, Me.dgvFlexLoanBalance.Columns(5).HeaderText)

            btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnSearch.ID, btnSearch.Text)
            btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnReset.ID, btnReset.Text)
            btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnClose.ID, btnClose.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class
