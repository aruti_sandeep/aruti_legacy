﻿<%@ Page Title="Add/Edit Loan Application" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_AddEditFlexcubeLoanApplication.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Application_wPg_AddEditFlexcubeLoanApplication" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="nut" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, event) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, evemt) {
            $("#endreq").val("1");
        }
        function IsValidAttach() {
            debugger;
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');

            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            return true;
        }    

        function IsValidOtherAttach() {
            debugger;
            var cbodoctype = $('#<%= cboOtherDocumentType.ClientID %>');

            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            return true;
        }    

    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <script type="text/javascript">
        function pageLoad(sender, args) { $("select").searchable(); } </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>
    
    <script type="text/javascript">
        function displaysign() {
            var img = document.getElementById('<%=pnlSign.ClientID %>');
            img.style.display = "block";
        }
     

        function FileUploadChangeEvent() {
            var cnt = $('.flupload').length;
            for (i = 0; i < cnt; i++) {
                var fupld = $('.flupload')[i].id;
                if (fupld != null)
                    fileUpLoadChange($('.flupload')[i].id);
            }
        }

        function countdown(minutes, seconds) {
            var element, endTime, mins, msLeft, time;

            function twoDigits(n) {
                return (n <= 9 ? "0" + n : n);
            }

            function updateTimer() {
                msLeft = endTime - (+new Date);

                if (msLeft < 1000) {
                    element.innerHTML = '00:00';
                    document.getElementById('<%=txtVerifyOTP.ClientID %>').readOnly = true;
                    document.getElementById('<%=btnVerify.ClientID %>').disabled = "disabled";
                    document.getElementById('<%=btnSendCodeAgain.ClientID %>').disabled = "";
                    document.getElementById('<%=btnTOTPClose.ClientID %>').disabled = "";
                } else {
                    document.getElementById('<%=btnSendCodeAgain.ClientID %>').disabled = "disabled";
                    document.getElementById('<%=btnTOTPClose.ClientID %>').disabled = "disabled";
                    time = new Date(msLeft);
                    mins = time.getUTCMinutes();
                    element.innerHTML = (twoDigits(mins)) + ':' + twoDigits(time.getUTCSeconds());
                    timerid = setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
                }
                if (msLeft <= 0) {
                    return;
                }
                document.getElementById('<%=hdf_TOTP.ClientID %>').value = twoDigits(time.getUTCSeconds());
            }
            element = document.getElementById('<%=LblSeconds.ClientID %>');
            endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
            updateTimer();
        }

        function VerifyOnClientClick() {
            var val = document.getElementById('<%=hdf_TOTP.ClientID %>').value;
            if (val !== undefined) {
                countdown(0, val, false);
            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc4:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!-- Task Info -->
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Loan Application" CssClass="form-label"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="card inner-card">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblEmpName" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboEmpName" runat="server" AutoPostBack="true" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                                    <asp:CheckBox ID="chkMakeExceptionalApplication" runat="server" Text="Request for Exceptional Approval?"
                                                        AutoPostBack="true" CssClass="filled-in" />
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboLoanScheme" runat="server" AutoPostBack="True" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblLoanSchemeCategory" runat="server" Text="Loan Category" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboLoanSchemeCategory" runat="server" data-live-search="true"
                                                            Enabled="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblRepaymentDays" runat="server" Text="Repayment every (Days)" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtRepaymentDays" runat="server" ReadOnly="true" Style="text-align: right"
                                                                Width="99%" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblMaxLoanAmount" runat="server" Text="Max Principal Amount" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtMaxLoanAmount" AutoPostBack="true" Text="0.0" Style="text-align: right"
                                                                runat="server" onKeypress="return onlyNumbers(this, event);" ReadOnly="true"
                                                                CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblMinLoanAmount" runat="server" Text="Min Principal Amount" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtMinLoanAmount" AutoPostBack="true" Text="0.0" Style="text-align: right"
                                                                runat="server" onKeypress="return onlyNumbers(this, event);" ReadOnly="true"
                                                                CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblMaxInstallmentAmt" runat="server" Text="Max Installment Amt" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtMaxInstallmentAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblMaxInstallment" runat="server" Text="Max no of Installment" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtMaxInstallment" AutoPostBack="true" Text="1" Style="text-align: right"
                                                                runat="server" onKeypress="return onlyNumbers(this, event);" ReadOnly="true"
                                                                CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblLoanInterest" runat="server" Text="Loan Rate(%)" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtLoanRate" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblInsuranceRate" runat="server" Text="Insurance Rate(%)" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtInsuranceRate" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card inner-card">
                                                <div class="body">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblLoanCalcType" runat="server" Text="Loan Calc. Type" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboLoanCalcType" runat="server" Enabled="False" AutoPostBack="true"
                                                                    data-live-search="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblInterestCalcType" runat="server" Text="Int. Calc. Type" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboInterestCalcType" runat="server" AutoPostBack="true" Enabled="False"
                                                                    data-live-search="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="row clearfix" style="display: none;">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAmt" runat="server" Text="Applied Principal Amt" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtAppliedUptoPrincipalAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                        Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card inner-card">
                                        <div class="header">
                                            <h2>
                                                    <asp:Label ID="elEmployeeDetails" runat="server" Text="Employee Details" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPayPeriodsEOC" runat="server" Text="Pay Periods" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="nudPayPeriodEOC" AutoPostBack="true" Text="1" Style="text-align: right"
                                                                    runat="server" onKeypress="return onlyNumbers(this, event);" CssClass="form-control"
                                                                    Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmplDate" runat="server" Text="Til EOC Date" CssClass="form-label"></asp:Label>
                                                        <uc1:DateCtrl ID="dtpEndEmplDate" runat="server" AutoPostBack="false" Enabled="false" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                <asp:Label ID="elLoanAmountCalculation" runat="server" Text="Loan Amount Calculation"
                                                    CssClass="form-label"></asp:Label>
                                            </h2>
                                        </div>
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblEMIInstallments" runat="server" Text="Loan Tenure (In Months)"
                                                        CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtEMIInstallments" AutoPostBack="true" Text="1" Style="text-align: right"
                                                                runat="server" onKeypress="return onlyNumbers(this, event);" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblPrincipalAmt" runat="server" Text="Principal Amt." CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtLoanAmt" AutoPostBack="true" Text="0.0" Style="text-align: right"
                                                                runat="server" onKeypress="return onlyNumbers(this, event);" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        </div>
                                                    </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-25">
                                                    <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true" data-live-search="true"
                                                        Enabled="false">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="display: none;">
                                                    <asp:Label ID="lblIntAmt" runat="server" Text="Interest Amt." CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtIntAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblEMIAmount" runat="server" Text="Installment Amt" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtInstallmentAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <asp:Panel ID="pnlOutstandingInfo" runat="server">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblOutStandingPrincipalAmt" runat="server" Text="Outstanding Principal Amt."
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtOutStandingPrincipalAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                    Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblOutStandingInterestAmt" runat="server" Text="Outstanding Interest Amt."
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtOutStandingInterestAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                    Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblInstallmentPaid" runat="server" Text="No of Installment Paid" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtInstallmentPaid" runat="server" ReadOnly="true" Style="text-align: right"
                                                                        Text="0" Width="99%" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblInsuranceAmt" runat="server" Text="Insurance Amt." CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtInsuranceAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblTakeHome" runat="server" Text="Take Home" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtTakeHome" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30" style="text-align:right;">
                                                        <asp:CheckBox ID="chkSalaryAdvanceLiquidation" runat="server" Text="Salary Advance Liquidation"
                                                            AutoPostBack="true" CssClass="filled-in" />
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPurposeOfCredit" runat="server" Text="Purpose of Credit" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtPurposeOfCredit" TextMode="MultiLine" Rows="3" runat="server"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <asp:Panel ID="Panel1" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPlotNo" runat="server" Text="Plot No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPlotNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblBlockNo" runat="server" Text="Block No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtBlockNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblStreet" runat="server" Text="Street" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtStreet" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTownCity" runat="server" Text="Town/City" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtTownCity" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblMarketValue" runat="server" Text="Market Value" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtMarketValue" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                    Style="text-align: right" Text="0.0"  CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCTNo" runat="server" Text="CT No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtCTNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLONo" runat="server" Text="LO No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtLONo" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblFSV" runat="server" Text="FSV" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtFSV" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                    Style="text-align: right" Text="0.0"  CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblTitleIssueDate" runat="server" Text="Title Issue Date" CssClass="form-label"></asp:Label>
                                                            <uc1:DateCtrl ID="dtpTitleIssueDate" runat="server" AutoPostBack="true" />
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblTitleValidity" runat="server" Text="Title Validity" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="nudTitleValidity" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                        Style="text-align: right" Text="0.0" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblTitleExpiryDate" runat="server" Text="Title Expiry Date" CssClass="form-label"></asp:Label>
                                                            <uc1:DateCtrl ID="dtpTitleExpiryDate" runat="server" Enabled = "false" />
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                           <asp:Label ID="LblBOQ" runat="server" Text="Bill Of Quantity (BOQ)" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtBOQ" runat="server" Text = "" CssClass="form-control" ></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                        </div>
                            </asp:Panel>
                            <asp:Panel ID="Panel2" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblModel" runat="server" Text="Model" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtModel" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblYOM" runat="server" Text="YOM" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtYOM" runat="server" MaxLength="4" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblChassisNo" runat="server" Text="Chassis No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtChassisNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblColour" runat="server" Text="Colour" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtColour" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblSupplier" runat="server" Text="Supplier" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtSupplier" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    <asp:Panel ID="pnlSecureLoanDetails1" runat="server" Visible="false">
                                                <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblTotalCIF" runat="server" Text="Total CIF" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtTotalCIF" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                            Style="text-align: right" Text="0.0" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblEstimatedTax" runat="server" Text="Estimated Tax" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtEstimatedTax" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                            Style="text-align: right" Text="0.0" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblInvoiceValue" runat="server" Text="Invoice Value" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtInvoiceValue" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                            Style="text-align: right" Text="0.0" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblModelNo" runat="server" Text="Model No" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtModelNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblEngineNo" runat="server" Text="Engine No" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtEngineNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblEngineCapacity" runat="server" Text="Engine Capacity" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                        <asp:TextBox ID="txtEngineCapacity" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                            Style="text-align: right" Text="0.0" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    </div>
                                                </div>
                                                    </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <div class="row clearfix">
                                <asp:Panel ID="pnlAttachment" runat="server">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblAttachmentInfo" runat="server" Text="Attachment Info" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <asp:Panel ID="pnlScanAttachment" runat="server">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblDocumentType" runat="server" Text="Document Type(.PDF and Images only)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDocumentType" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-20  ">
                                                            <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                                <div id="fileuploader">
                                                                    <input type="button" id="btnAddFile" runat="server" onclick="return IsValidAttach()"
                                                                        value="Browse" class="btn btn-primary" />
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  p-l-0">
                                                            <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary" />
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ">
                                                            <asp:Button ID="btnAddAttachment" runat="server" Style="display: none" Text="Browse" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive" style="max-height: 350px;">
                                                        <asp:DataGrid ID="dgvQualification" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                            Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                            HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:TemplateColumn FooterText="objcohDelete">
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                        <asp:BoundColumn HeaderText="Document Type" DataField="attachment_type" FooterText="colhDocType" />
                                                                <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                                <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center"
                                                                    ItemStyle-Font-Size="22px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Download">
                                                                                                            <i class="fas fa-download"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                    Visible="false" />
                                                                <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                    Visible="false" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                                </asp:Panel>
                                                <asp:Panel ID="pnlOtherScanAttachment" runat="server">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblOtherDocumentType" runat="server" Text="Other Document Type(.PDF and Images only)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboOtherDocumentType" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-20  ">
                                                            <asp:Panel ID="pnl_OtherImageAdd" runat="server">
                                                                    <div id="otherfileuploader">
                                                                    <input type="button" id="btnOtherAddFile" runat="server" onclick="return IsValidOtherAttach()"
                                                                        value="Browse" class="btn btn-primary" />
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  p-l-0">
                                                            <asp:Button ID="btnOtherDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary" />
                                                        </div>
                                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ">
                                                            <asp:Button ID="btnAddOtherAttachment" runat="server" Style="display: none" Text="Browse" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive" style="max-height: 350px;">
                                                        <asp:DataGrid ID="dgvOtherQualification" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                            Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                            HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:TemplateColumn FooterText="objcohDelete">
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                        <asp:BoundColumn HeaderText="Document Type" DataField="attachment_type" FooterText="colhOtherDocType" />
                                                                <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                                <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center"
                                                                    ItemStyle-Font-Size="22px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Download">
                                                                                                            <i class="fas fa-download"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                    Visible="false" />
                                                                <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                    Visible="false" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlSign" runat="server">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblempsign" runat="server" Text="Employee Signature:" Font-Bold="true"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Image ID="imgSignature" runat="server" ToolTip="If this is not your signature, upload new signature" />
                                                                <asp:Label ID="lblnosign" runat="server" Text="Signature Not Available" ForeColor="Red"
                                                                    Font-Bold="true"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <%-- <div class="row clearfix">
                                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                                                <asp:UpdatePanel ID="UPUploadSig" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <uc9:FileUpload ID="flUploadsig" runat="server" />
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>--%>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:CheckBox ID="chkconfirmSign" ToolTip="Confirm Signature" runat="server" Text="I confirm Signatures" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </asp:Panel>
                        </div>
                        </div>
                        <div class="footer">
                            <div class="btn-group m-l-0" style="float: left">
                                <asp:Label ID="objvalPeriodDuration" runat="server" Text="#periodDuration" Style="display: block;"
                                    Visible="false"></asp:Label>
                                <asp:Label ID="objlblExRate" runat="server" Style="float: left;" Text="" CssClass="form-label"></asp:Label>
                            </div>
                            <asp:Button ID="btnConfirmSign" runat="server" Text="Confirm Signature" CssClass="btn btn-primary"
                                Visible="false" />
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
                </div>
                <cc1:ModalPopupExtender ID="popup_TOTP" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="hdf_TOTP" PopupControlID="pnl_TOTP" TargetControlID="hdf_TOTP">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_TOTP" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblVerifyOTP" runat="server" Text="Verify OTP"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="LblOTP" runat="server" Text="Enter OTP" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtVerifyOTP" runat="server" CssClass="form-control" MaxLength="6"
                                            onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="LblSeconds" runat="server" Text="00:00" CssClass="form-label" Font-Bold="true"
                                    ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSendCodeAgain" runat="server" Text="Resend OTP" CssClass="btn btn-primary" />
                        <asp:Button ID="btnVerify" runat="server" Text="Verify" CssClass="btn btn-primary" />
                        <asp:Button ID="btnTOTPClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdf_TOTP" runat="server" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
             <Triggers>
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
                <asp:PostBackTrigger ControlID="dgvQualification" />
                <asp:PostBackTrigger ControlID="btnOtherDownloadAll" />
                <asp:PostBackTrigger ControlID="dgvOtherQualification" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
    
      <script type="text/javascript">

        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            debugger;
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                url: "wPg_AddEditFlexcubeLoanApplication.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
                    dragDropStr: "",
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnAddAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });

                $("#otherfileuploader").uploadFile({
                    url: "wPg_AddEditFlexcubeLoanApplication.aspx?uploadotherimage=mSEfU19VPc4=",
                    method: "POST",
                    dragDropStr: "",
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    showDone: false,
                    fileName: "myotherfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnAddOtherAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });

            }
        }

        //$('input[type=file]').live("click", function() {
        //$("body").on("click", 'input[type=file]', function() {
        $("body").on("click", 'input[name*=myfile]', function() {
            return IsValidAttach();
        });

        $("body").on("click", 'input[name*=myotherfile]', function() {
            return IsValidOtherAttach();
        });
        
    </script>

</asp:Content>
