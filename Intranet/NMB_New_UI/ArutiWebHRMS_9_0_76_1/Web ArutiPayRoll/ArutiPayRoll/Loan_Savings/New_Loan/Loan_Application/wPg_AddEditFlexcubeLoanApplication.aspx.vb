﻿Option Strict On

#Region " Imports "
Imports System.Data
Imports Aruti.Data
Imports System.IO

#End Region


Partial Class Loan_Savings_New_Loan_Loan_Application_wPg_AddEditFlexcubeLoanApplication
    Inherits Basepage

#Region "Private Variables"
    Private Shared ReadOnly mstrModuleName As String = "frmFlexcubeLoanApplication_AddEdit"
    Dim DisplayMessage As New CommonCodes
    Private objProcesspendingloan As New clsProcess_pending_loan
    Private mintProcesspendingloanunkid As Integer = 0
    Private mdecInstallmentAmt As Decimal = 0
    Private mdecEMIInstallments As Decimal = 0
    Private mblnIsSendEmailNotification As Boolean = False
    Private mstrMaxInstallmentHeadFormula As String = String.Empty
    Private mdecMaxLoanAmountDefined As Decimal = 0
    Private mdecEstimateMaxInstallmentAmt As Decimal = 0
    Private mintMaxNoOfInstallmentLoanScheme As Integer = 1
    Private mintMaxLoanCalcTypeId As Integer = 0
    Private mstrMaxLoanAmountHeadFormula As String = String.Empty
    Private mintDeductionPeriodUnkId As Integer = 0
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    Private mdtApplcationDate As Date
    Private decInstallmentAmount As Decimal = 0
    Private decInstrAmount As Decimal = 0
    Private decTotIntrstAmount As Decimal = 0
    Private decTotInstallmentAmount As Decimal = 0
    Private decNetAmount As Decimal = 0
    Private mblnIsAttachmentRequired As Boolean = False
    Private mstrDocumentTypeIDs As String = String.Empty
    Private mdtLoanApplicationDocument As DataTable
    Private objDocument As New clsScan_Attach_Documents
    Private blnIsPnlSignVisible As Boolean = False
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mdecMaxLoanAmount As Decimal = 0
    Private mstrApplicationNo As String = String.Empty
    Private mdecMinLoanAmount As Decimal = 0

    'Pinkal (20-Sep-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mblnRequiredReportingToApproval As Boolean = False
    'Pinkal (20-Sep-2022) -- End

    'Pinkal (12-Oct-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mblnSkipApprovalFlow As Boolean = False
    'Pinkal (12-Oct-2022) -- End
    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    'Private mintOtherDocumentunkid As Integer = -1
    Private mstrOtherDocumentIds As String = ""
    'Pinkal (23-Nov-2022) -- End


    Private mdtFinalLoanApplicationDocument As DataTable
    Private mdtOtherLoanApplicationDocument As DataTable
    'Hemant (27 Oct 2022) -- End

    'Pinkal (10-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mblnLoanApproval_DailyReminder As Boolean
    Private mintEscalationDays As Integer = 0
    'Pinkal (10-Nov-2022) -- End

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mdecOtherLoanOutStandingPrincipalAmt As Decimal = 0
    'Pinkal (23-Nov-2022) -- End
    'Hemant (22 Dec 2022) -- Start
    Private mintCurrentFlexcubeLoanCount As Integer
    'Hemant (22 Dec 2022) -- End
    'Hemant (03 Feb 2023) -- Start
    'ISSUE/EHANCEMENT(NMB) : A1X-619 -  New formula for calculating Maximum Installment on loan application and approval pages
    Private mstrLoanSchemeCode As String = ""
    'Hemant (03 Feb 2023) -- End  


    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .
    Dim mstrSecretKey As String = ""
    Private blnShowTOTPPopup As Boolean = False
    Dim Counter As Integer = 0
    Dim mstrEmpMobileNo As String = ""
    Dim mstrEmployeeName As String = ""
    Dim mstrEmployeeEmail As String = ""
    'Pinkal (02-Jun-2023) -- End

    'Pinkal (11-Mar-2024) -- Start
    '(A1X-2505) NMB - Credit card integration.
    Dim mdecCreditCardAmountOnExposure As Decimal = 0
    'Pinkal (11-Mar-2024) -- End
    'Hemant (18 Mar 2024) -- Start
    'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
    Dim mstrCompanyGroupName As String = ""
    'Hemant (18 Mar 2024) -- End
    'Hemant (22 Nov 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
    Dim mdecActualSalaryAdvancePendingPrincipalAmt As Decimal = 0
    Dim mdecFinalSalaryAdvancePendingPrincipalAmt As Decimal = 0
    'Hemant (22 Nov 2024) -- End
#End Region

#Region "Page's Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call GetControlCaptions()
                Call FillCombo()
                Call FillPeriod(False)
                Call FillDocumentTypeCombo()
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                Call FillOtherDocumentTypeCombo()
                'Hemant (27 Oct 2022) -- End
                Panel1.Visible = False
                Panel2.Visible = False
                pnlScanAttachment.Visible = False

                mdtApplcationDate = ConfigParameter._Object._CurrentDateAndTime.Date
                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                Dim objGroup As New clsGroup_Master
                objGroup._Groupunkid = 1
                mstrCompanyGroupName = objGroup._Groupname.ToString().ToUpper()
                pnlSecureLoanDetails1.Visible = False
                txtRepaymentDays.ReadOnly = True
                If mstrCompanyGroupName = "TADB" Then
                    txtRepaymentDays.ReadOnly = False
                    pnlSecureLoanDetails1.Visible = True
                End If
                objGroup = Nothing
                'Hemant (29 Mar 2024) -- End

                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromDesktopMSS"))
                ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromESS"))
                End If

                If Session("ProcessPendingLoanunkid") IsNot Nothing Then
                    mintProcesspendingloanunkid = CInt(Session("ProcessPendingLoanunkid"))
                    objProcesspendingloan._Processpendingloanunkid = mintProcesspendingloanunkid
                    Session("ProcessPendingLoanunkid") = Nothing
                    If mintProcesspendingloanunkid > 0 Then chkMakeExceptionalApplication.Enabled = False
                    Call GetValue()
                    cboEmpName.Focus()
                    mdecEMIInstallments = objProcesspendingloan._NoOfInstallment
                    mdecInstallmentAmt = objProcesspendingloan._InstallmentAmount
                    mdecInstallmentAmt = objProcesspendingloan._InstallmentAmount
                End If


                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                'If IsNothing(ViewState("mdtLoanApplicationDocument")) = False Then
                '    mdtLoanApplicationDocument = CType(ViewState("mdtLoanApplicationDocument"), DataTable).Copy()
                'Else
                '    mdtLoanApplicationDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.LOAN_ADVANCE, mintProcesspendingloanunkid, CStr(Session("Document_Path")))                

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'mdtFinalLoanApplicationDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.LOAN_ADVANCE, mintProcesspendingloanunkid, CStr(Session("Document_Path")))
                mdtFinalLoanApplicationDocument = objDocument.GetQulificationAttachment(CInt(cboEmpName.SelectedValue), enScanAttactRefId.LOAN_ADVANCE, mintProcesspendingloanunkid, CStr(Session("Document_Path")))
                If Session("mdtLoanApplicationDocument") Is Nothing Then
                mdtLoanApplicationDocument = mdtFinalLoanApplicationDocument.Clone()
                mdtOtherLoanApplicationDocument = mdtFinalLoanApplicationDocument.Clone()
                End If
                'Pinkal (04-Aug-2023) -- End

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                'Dim drLoanApplicationDocument() As DataRow = mdtFinalLoanApplicationDocument.Copy.Select("documentunkid <> " & mintOtherDocumentunkid & "")
                'If drLoanApplicationDocument.Length > 0 Then
                '    mdtLoanApplicationDocument = drLoanApplicationDocument.CopyToDataTable()
                'End If
                'Dim drOtherLoanApplicationDocument() As DataRow = mdtFinalLoanApplicationDocument.Copy.Select("documentunkid = " & mintOtherDocumentunkid & "")
                'If drOtherLoanApplicationDocument.Length > 0 Then
                '    mdtOtherLoanApplicationDocument = drOtherLoanApplicationDocument.CopyToDataTable()
                'End If
                If mstrOtherDocumentIds.Trim.Length > 0 Then
                    Dim drLoanApplicationDocument() As DataRow = mdtFinalLoanApplicationDocument.Copy.Select("documentunkid NOT IN (" & mstrOtherDocumentIds & ")")
                If drLoanApplicationDocument.Length > 0 Then
                    mdtLoanApplicationDocument = drLoanApplicationDocument.CopyToDataTable()
                End If
                    Dim drOtherLoanApplicationDocument() As DataRow = mdtFinalLoanApplicationDocument.Copy.Select("documentunkid IN (" & mstrOtherDocumentIds & ")")
                If drOtherLoanApplicationDocument.Length > 0 Then
                    mdtOtherLoanApplicationDocument = drOtherLoanApplicationDocument.CopyToDataTable()
                End If
                    'Pinkal (04-Aug-2023) -- Start
                    '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                Else
                    mdtLoanApplicationDocument = mdtFinalLoanApplicationDocument.Copy()
                    'Pinkal (04-Aug-2023) -- End
                End If
                'Pinkal (23-Nov-2022) -- End


                    If mintProcesspendingloanunkid <= 0 Then
                        If mdtLoanApplicationDocument IsNot Nothing Then mdtLoanApplicationDocument.Rows.Clear()
                    'Hemant (27 Oct 2022) -- Start
                    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                    If mdtOtherLoanApplicationDocument IsNot Nothing Then mdtOtherLoanApplicationDocument.Rows.Clear()
                    'Hemant (27 Oct 2022) -- End
                    End If

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                If Session("mdtLoanApplicationDocument") Is Nothing Then
                    Session("mdtLoanApplicationDocument") = mdtLoanApplicationDocument
                Else
                    mdtLoanApplicationDocument = CType(Session("mdtLoanApplicationDocument"), DataTable)
                End If

                If Session("mdtOtherLoanApplicationDocument") Is Nothing Then
                    Session("mdtOtherLoanApplicationDocument") = mdtOtherLoanApplicationDocument
                Else
                    mdtOtherLoanApplicationDocument = CType(Session("mdtOtherLoanApplicationDocument"), DataTable)
                End If

                If mdtFinalLoanApplicationDocument IsNot Nothing Then mdtFinalLoanApplicationDocument.Rows.Clear()

                If Session("mdtFinalLoanApplicationDocument") Is Nothing Then
                    Session("mdtFinalLoanApplicationDocument") = mdtFinalLoanApplicationDocument
                Else
                    mdtFinalLoanApplicationDocument = CType(Session("mdtFinalLoanApplicationDocument"), DataTable)
                End If
                'Pinkal (04-Aug-2023) -- End

                Call FillLoanApplicationAttachment()
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                Call FillLoanApplicationOtherAttachment()
                If chkMakeExceptionalApplication.Checked = True Then
                    pnlOtherScanAttachment.Visible = True
                Else
                    pnlOtherScanAttachment.Visible = False
                End If

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                'If (chkMakeExceptionalApplication.Checked = True AndAlso mintOtherDocumentunkid > 0) OrElse mblnIsAttachmentRequired = True Then
                If (chkMakeExceptionalApplication.Checked = True AndAlso mstrOtherDocumentIds.Trim.Length > 0) OrElse mblnIsAttachmentRequired = True Then
                    'Pinkal (23-Nov-2022) -- End
                    pnlAttachment.Visible = True
                Else
                    pnlAttachment.Visible = False
            End If
                'Hemant (27 Oct 2022) -- End                


                If mintProcesspendingloanunkid <= 0 AndAlso CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    cboEmpName_SelectedIndexChanged(cboEmpName, New System.EventArgs())
                End If


                If imgSignature.ImageUrl.ToString.Trim.Length > 0 Then
                    btnConfirmSign.Visible = True
                Else
                    btnConfirmSign.Visible = False
                End If
                pnlSign.Attributes.Add("style", "display:none")
               
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1003 - As a user, I want all the mandatory fields on loan application to be highlighted in asterisks. (Purpose of credit, model, YOM, Chassis number, supplier, Colour, Plot No, Block No, Street, Town/city, Market Value, CT no, LO no, FSV, Document Type)
                lblEmpName.Text = lblEmpName.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblLoanScheme.Text = lblLoanScheme.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblPrincipalAmt.Text = lblPrincipalAmt.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblEMIInstallments.Text = lblEMIInstallments.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblPurposeOfCredit.Text = lblPurposeOfCredit.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblPlotNo.Text = lblPlotNo.Text & "<span style='color: Red; font-size: medium'>*</span>"

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                'lblBlockNo.Text = lblBlockNo.Text & "<span style='color: Red; font-size: medium'>*</span>"
                If CInt(cboLoanScheme.SelectedValue) > 0 Then
                    Dim objLoanScheme As New clsLoan_Scheme
                    objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                    If objLoanScheme._MarketValueMandatory Then
                        lblMarketValue.Text = lblMarketValue.Text & "<span style='color: Red; font-size: medium'>*</span>"
                    End If
                    If objLoanScheme._FSVValueMandatory Then
                        lblFSV.Text = lblFSV.Text & "<span style='color: Red; font-size: medium'>*</span>"
                    End If
                    If objLoanScheme._BOQMandatory Then
                        LblBOQ.Text = LblBOQ.Text & "<span style='color: Red; font-size: medium'>*</span>"
                    End If
                    objLoanScheme = Nothing
                Else
                    lblMarketValue.Text = lblMarketValue.Text & "<span style='color: Red; font-size: medium'>*</span>"
                    lblFSV.Text = lblFSV.Text & "<span style='color: Red; font-size: medium'>*</span>"
                    LblBOQ.Text = LblBOQ.Text & "<span style='color: Red; font-size: medium'>*</span>"
                End If
                'Pinkal (17-May-2024) -- End


                lblStreet.Text = lblStreet.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblTownCity.Text = lblTownCity.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblCTNo.Text = lblCTNo.Text & "<span style='color: Red; font-size: medium'>*</span>"

                'Pinkal (21-Mar-2024) -- Start
                'NMB - Mortgage UAT Enhancements.
                'lblLONo.Text = lblLONo.Text & "<span style='color: Red; font-size: medium'>*</span>"
                'Pinkal (21-Mar-2024) -- End


                lblModel.Text = lblModel.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblYOM.Text = lblYOM.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblChassisNo.Text = lblChassisNo.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblSupplier.Text = lblSupplier.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblColour.Text = lblColour.Text & "<span style='color: Red; font-size: medium'>*</span>"
                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                lblTotalCIF.Text = lblTotalCIF.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblestimatedtax.Text = lblestimatedtax.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblinvoicevalue.Text = lblinvoicevalue.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblmodelno.Text = lblmodelno.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblengineno.Text = lblengineno.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblenginecapacity.Text = lblenginecapacity.Text & "<span style='color: Red; font-size: medium'>*</span>"
                'Hemant (29 Mar 2024) -- End
                lblDocumentType.Text = lblDocumentType.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblOtherDocumentType.Text = lblOtherDocumentType.Text & "<span style='color: Red; font-size: medium'>*</span>"
                'Hemant (27 Oct 2022) -- End
                'Hemant (07 Jul 2023) -- Start
                'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
                lblTitleIssueDate.Text = lblTitleIssueDate.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblTitleValidity.Text = lblTitleValidity.Text & "<span style='color: Red; font-size: medium'>*</span>"
                lblTitleExpiryDate.Text = lblTitleExpiryDate.Text & "<span style='color: Red; font-size: medium'>*</span>"
                'Hemant (07 July 2023) -- End
            Else
            mdecInstallmentAmt = CDec(Me.ViewState("InstallmentAmt"))
            mdecEMIInstallments = CDec(Me.ViewState("EMIInstallments"))
            mintProcesspendingloanunkid = CInt(Me.ViewState("Processpendingloanunkid"))
            mblnIsSendEmailNotification = CBool(Me.ViewState("SendEmailNotification"))
            mdtPeriodStart = CDate(ViewState("mdtPeriodStart"))
            mdtPeriodEnd = CDate(ViewState("mdtPeriodEnd"))
            mdecMaxLoanAmountDefined = CDec(Me.ViewState("MaxLoanAmountDefined"))
            mintMaxNoOfInstallmentLoanScheme = CInt(Me.ViewState("MaxNoOfInstallmentLoanScheme"))
            mstrMaxInstallmentHeadFormula = CStr(Me.ViewState("MaxInstallmentHeadFormula"))
            mintMaxLoanCalcTypeId = CInt(Me.ViewState("MaxLoanCalcTypeId"))
            mstrMaxLoanAmountHeadFormula = CStr(Me.ViewState("MaxLoanAmountHeadFormula"))
            mdtApplcationDate = CDate(Me.ViewState("ApplcationDate"))
            mblnIsSendEmailNotification = CBool(Me.ViewState("SendEmailNotification"))
            decInstrAmount = CDec(Me.ViewState("decInstrAmount"))
            decInstallmentAmount = CDec(Me.ViewState("decInstallmentAmount"))
            decTotIntrstAmount = CDec(Me.ViewState("decTotIntrstAmount"))
            decTotInstallmentAmount = CDec(Me.ViewState("decTotInstallmentAmount"))
            decNetAmount = CDec(Me.ViewState("decNetAmount"))
            mintDeductionPeriodUnkId = CInt(Me.ViewState("DeductionPeriodUnkId"))
            mblnIsAttachmentRequired = CBool(Me.ViewState("IsAttachmentRequired"))
            mstrDocumentTypeIDs = CStr(Me.ViewState("DocumentTypeIDs"))

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'mdtLoanApplicationDocument = CType(ViewState("mdtLoanApplicationDocument"), DataTable)
                mdtLoanApplicationDocument = CType(Session("mdtLoanApplicationDocument"), DataTable)
                'Pinkal (04-Aug-2023) -- End

                blnIsPnlSignVisible = CBool(Me.ViewState("blnIsPnlSignVisible"))
                If blnIsPnlSignVisible Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "displaypanel", "displaysign();", True)
                End If
                mstrBaseCurrSign = ViewState("mstrBaseCurrSign").ToString()
                mintBaseCurrId = CInt(ViewState("mintBaseCurrId"))
                mintPaidCurrId = CInt(ViewState("mintPaidCurrId"))
                mdecBaseExRate = CDec(ViewState("mdecBaseExRate"))
                mdecPaidExRate = CDec(ViewState("mdecPaidExRate"))
                mdecMaxLoanAmount = CDec(Me.ViewState("MaxLoanAmount"))
                mdecMinLoanAmount = CDec(Me.ViewState("MinLoanAmount"))
                'Pinkal (20-Sep-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnRequiredReportingToApproval = CBool(Me.ViewState("RequiredReportingToApproval"))
                'Pinkal (20-Sep-2022) -- End

                'Pinkal (12-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnSkipApprovalFlow = CBool(Me.ViewState("SkipApprovalFlow"))
                'Pinkal (12-Oct-2022) -- End
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                'mintOtherDocumentunkid = CInt(ViewState("mintOtherDocumentunkid"))
                mstrOtherDocumentIds = ViewState("OtherDocumentIds").ToString()
                'Pinkal (23-Nov-2022) -- End

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'mdtOtherLoanApplicationDocument = CType(ViewState("mdtOtherLoanApplicationDocument"), DataTable)
                'mdtFinalLoanApplicationDocument = CType(ViewState("mdtFinalLoanApplicationDocument"), DataTable)
                mdtOtherLoanApplicationDocument = CType(Session("mdtOtherLoanApplicationDocument"), DataTable)
                mdtFinalLoanApplicationDocument = CType(Session("mdtFinalLoanApplicationDocument"), DataTable)
                'Pinkal (04-Aug-2023) -- End


                'Hemant (27 Oct 2022) -- End

                'Pinkal (10-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnLoanApproval_DailyReminder = CBool(Me.ViewState("LoanApproval_DailyReminder"))
                mintEscalationDays = CInt(Me.ViewState("EscalationDays"))
                'Pinkal (10-Nov-2022) -- End

                mdecOtherLoanOutStandingPrincipalAmt = CDec(Me.ViewState("OtherLoanOutStandingPrincipalAmt"))
                'Hemant (22 Dec 2022) -- Start
                mintCurrentFlexcubeLoanCount = CInt(Me.ViewState("mintCurrentFlexcubeLoanCount"))
                'Hemant (22 Dec 2022) -- End
                'Hemant (03 Feb 2023) -- Start
                'ISSUE/EHANCEMENT(NMB) : A1X-619 -  New formula for calculating Maximum Installment on loan application and approval pages
                mstrLoanSchemeCode = CStr(Me.ViewState("mstrLoanSchemeCode"))
                'Hemant (03 Feb 2023) -- End  


                'Pinkal (02-Jun-2023) -- Start
                'NMB Enhancement : TOTP Related Changes .
                mstrSecretKey = CStr(Me.ViewState("mstrSecretKey"))
                Counter = CInt(Me.ViewState("Counter"))
                blnShowTOTPPopup = CBool(Me.ViewState("blnShowTOTPPopup"))
                mstrEmployeeName = CStr(Me.ViewState("EmployeeName"))
                mstrEmployeeEmail = CStr(Me.ViewState("EmployeeEmail"))
                mstrEmpMobileNo = CStr(Me.ViewState("EmployeeMobileNo"))
                'Pinkal (02-Jun-2023) -- End

                'Pinkal (11-Mar-2024) -- Start
                '(A1X-2505) NMB - Credit card integration.
                mdecCreditCardAmountOnExposure = CDec(Me.ViewState("CreditCardAmountOnExposure"))
                'Pinkal (11-Mar-2024) -- End
                'Hemant (18 Mar 2024) -- Start
                'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
                mstrCompanyGroupName = CStr(Me.ViewState("CompanyGroupName"))
                'Hemant (18 Mar 2024) -- End

                'Hemant (22 Nov 2024) -- Start
                'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                mdecActualSalaryAdvancePendingPrincipalAmt = CDec(ViewState("mdecActualSalaryAdvancePendingPrincipalAmt"))
                mdecFinalSalaryAdvancePendingPrincipalAmt = CDec(ViewState("mdecFinalSalaryAdvancePendingPrincipalAmt"))
                'Hemant (22 Nov 2024) -- End


            End If

            If Request.QueryString("uploadimage") IsNot Nothing Then
                If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                    Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                    postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                    Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                End If
            End If

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            If Request.QueryString("uploadotherimage") IsNot Nothing Then
                If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadotherimage"))) = True Then
                    Dim postedFile As HttpPostedFile = Context.Request.Files("myotherfile")
                    postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                    Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                End If
            End If
            'Hemant (27 Oct 2022) -- End


            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            If blnShowTOTPPopup Then
                popup_TOTP.Show()
            End If
            'Pinkal (02-Jun-2023) -- End
          

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("InstallmentAmt") = mdecInstallmentAmt
            Me.ViewState("EMIInstallments") = mdecEMIInstallments
            Me.ViewState("Processpendingloanunkid") = mintProcesspendingloanunkid
            Me.ViewState("SendEmailNotification") = mblnIsSendEmailNotification
            Me.ViewState("mdtPeriodStart") = mdtPeriodStart
            Me.ViewState("mdtPeriodEnd") = mdtPeriodEnd
            Me.ViewState("MaxLoanAmountDefined") = mdecMaxLoanAmountDefined
            Me.ViewState("MaxNoOfInstallmentLoanScheme") = mintMaxNoOfInstallmentLoanScheme
            Me.ViewState("MaxInstallmentHeadFormula") = mstrMaxInstallmentHeadFormula
            Me.ViewState("MaxLoanCalcTypeId") = mintMaxLoanCalcTypeId
            Me.ViewState("MaxLoanAmountHeadFormula") = mstrMaxLoanAmountHeadFormula
            Me.ViewState("ApplcationDate") = mdtApplcationDate
            Me.ViewState("decInstrAmount") = decInstrAmount
            Me.ViewState("decInstallmentAmount") = decInstallmentAmount
            Me.ViewState("decTotIntrstAmount") = decTotIntrstAmount
            Me.ViewState("decTotInstallmentAmount") = decTotInstallmentAmount
            Me.ViewState("decNetAmount") = decNetAmount
            Me.ViewState("DeductionPeriodUnkId") = mintDeductionPeriodUnkId
            Me.ViewState("IsAttachmentRequired") = mblnIsAttachmentRequired
            Me.ViewState("DocumentTypeIDs") = mstrDocumentTypeIDs

            'Pinkal (20-Sep-2022) -- Start
            'NMB Loan Module Enhancement.
            Me.ViewState("RequiredReportingToApproval") = mblnRequiredReportingToApproval
            'Pinkal (20-Sep-2022) -- End

            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            Me.ViewState("SkipApprovalFlow") = mblnSkipApprovalFlow
            'Pinkal (12-Oct-2022) -- End

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            'If Request.QueryString("uploadimage") Is Nothing Then
            '    If ViewState("mdtLoanApplicationDocument") Is Nothing Then
            '        ViewState.Add("mdtLoanApplicationDocument", mdtLoanApplicationDocument)
            '    Else
            '        ViewState("mdtLoanApplicationDocument") = mdtLoanApplicationDocument
            '    End If
            'Else
            '    If ViewState("mdtLoanApplicationDocument") Is Nothing Then
            '        ViewState.Add("mdtLoanApplicationDocument", mdtLoanApplicationDocument)
            '    Else
            '        ViewState("mdtLoanApplicationDocument") = mdtLoanApplicationDocument
            '    End If
            'End If
            Session("mdtLoanApplicationDocument") = mdtLoanApplicationDocument
            'Pinkal (04-Aug-2023) -- End


            Me.ViewState("blnIsPnlSignVisible") = blnIsPnlSignVisible
            Me.ViewState("mstrBaseCurrSign") = mstrBaseCurrSign
            Me.ViewState("mintBaseCurrId") = mintBaseCurrId
            Me.ViewState("mintPaidCurrId") = mintPaidCurrId
            Me.ViewState("mdecBaseExRate") = mdecBaseExRate
            Me.ViewState("mdecPaidExRate") = mdecPaidExRate
            Me.ViewState("MaxLoanAmount") = mdecMaxLoanAmount
            Me.ViewState("ApplicationNo") = mstrApplicationNo
            Me.ViewState("MinLoanAmount") = mdecMinLoanAmount

            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            Me.ViewState("LoanApproval_DailyReminder") = mblnLoanApproval_DailyReminder
            Me.ViewState("EscalationDays") = mintEscalationDays
            'Pinkal (10-Nov-2022) -- End

            Me.ViewState("OtherLoanOutStandingPrincipalAmt") = mdecOtherLoanOutStandingPrincipalAmt

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            'Me.ViewState("mintOtherDocumentunkid") = mintOtherDocumentunkid
            Me.ViewState("OtherDocumentIds") = mstrOtherDocumentIds
            'Pinkal (23-Nov-2022) -- End

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            'If Request.QueryString("uploadimage") Is Nothing Then
            '    If ViewState("mdtOtherLoanApplicationDocument") Is Nothing Then
            '        ViewState.Add("mdtOtherLoanApplicationDocument", mdtOtherLoanApplicationDocument)
            '    Else
            '        ViewState("mdtOtherLoanApplicationDocument") = mdtOtherLoanApplicationDocument
            '    End If
            'Else
            '    If ViewState("mdtOtherLoanApplicationDocument") Is Nothing Then
            '        ViewState.Add("mdtOtherLoanApplicationDocument", mdtOtherLoanApplicationDocument)
            '    Else
            '        ViewState("mdtOtherLoanApplicationDocument") = mdtOtherLoanApplicationDocument
            '    End If
            'End If
            '  ViewState("mdtFinalLoanApplicationDocument") = mdtFinalLoanApplicationDocument
            Session("mdtOtherLoanApplicationDocument") = mdtOtherLoanApplicationDocument
            Session("mdtFinalLoanApplicationDocument") = mdtFinalLoanApplicationDocument
            'Pinkal (04-Aug-2023) -- End

            'Hemant (27 Oct 2022) -- End
            'Hemant (22 Dec 2022) -- Start
            Me.ViewState("mintCurrentFlexcubeLoanCount") = mintCurrentFlexcubeLoanCount
            'Hemant (22 Dec 2022) -- End
            'Hemant (03 Feb 2023) -- Start
            'ISSUE/EHANCEMENT(NMB) : A1X-619 -  New formula for calculating Maximum Installment on loan application and approval pages
            Me.ViewState("mstrLoanSchemeCode") = mstrLoanSchemeCode
            'Hemant (03 Feb 2023) -- End  


            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Me.ViewState("Counter") = Counter
            Me.ViewState("mstrSecretKey") = mstrSecretKey
            Me.ViewState("blnShowTOTPPopup") = blnShowTOTPPopup
            Me.ViewState("EmployeeName") = mstrEmployeeName
            Me.ViewState("EmployeeEmail") = mstrEmployeeEmail
            Me.ViewState("EmployeeMobileNo") = mstrEmpMobileNo
            'Pinkal (02-Jun-2023) -- End

            'Pinkal (11-Mar-2024) -- Start
            '(A1X-2505) NMB - Credit card integration.
            Me.ViewState("CreditCardAmountOnExposure") = mdecCreditCardAmountOnExposure
            'Pinkal (11-Mar-2024) -- End
            'Hemant (18 Mar 2024) -- Start
            'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
            Me.ViewState("CompanyGroupName") = mstrCompanyGroupName
            'Hemant (18 Mar 2024) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            Me.ViewState("mdecActualSalaryAdvancePendingPrincipalAmt") = mdecActualSalaryAdvancePendingPrincipalAmt
            Me.ViewState("mdecFinalSalaryAdvancePendingPrincipalAmt") = mdecFinalSalaryAdvancePendingPrincipalAmt
            'Hemant (22 Nov 2024) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objLoan_Advance As New clsLoan_Advance
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     Session("UserAccessModeSetting").ToString, _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), _
                                                     "List", True)

                With cboEmpName
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedValue = "0"
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmpName.DataSource = objglobalassess.ListOfEmployee
                cboEmpName.DataTextField = "loginname"
                cboEmpName.DataValueField = "employeeunkid"
                cboEmpName.DataBind()
            End If

            Dim mblnSchemeShowOnEss As Boolean = True
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, " ISNULL(ispostingtoflexcube,0) = 1 ", mblnSchemeShowOnEss)

            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With
            cboLoanScheme_SelectedIndexChanged(Nothing, Nothing)

            Dim objCurrency As New clsExchangeRate
            dsList = objCurrency.getComboList("Currency", True)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsList.Tables("Currency")
                .DataBind()
                Dim drRow() As DataRow = dsList.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    Call cboCurrency_SelectedIndexChanged(cboCurrency, New System.EventArgs())
                End If
                drRow = Nothing
            End With

            dsList = objLoan_Advance.GetLoan_Scheme_Categories("List")

            With cboLoanSchemeCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objLoan_Advance.GetLoanCalculationTypeList("List", True)
            With cboLoanCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsList = objLoan_Advance.GetLoan_Interest_Calculation_Type("List", True)
            With cboInterestCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objLoanScheme = Nothing
            objLoan_Advance = Nothing
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Dim objEmployee As New clsEmployee_Master
        Dim objEmployeeDates As New clsemployee_dates_tran
        Dim objDiscFileMaster As New clsDiscipline_file_master
        Dim objProceedingMaster As New clsdiscipline_proceeding_master
        Dim objLoanCategoryMapping As New clsLoanCategoryMapping
        'Hemant (27 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
        Dim objLoanScheme As New clsLoan_Scheme
        'Hemant (27 Oct 2022) -- End
        Dim strFilter As String = String.Empty
        Dim dsData As New DataSet
        Try
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmpName.SelectedValue)


            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            mstrEmployeeName = objEmployee._Firstname & " " & objEmployee._Surname
            mstrEmployeeEmail = objEmployee._Email


            'Hemant (29 Mar 2024) -- Start
            'ENHANCEMENT(TADB): New loan application form
            If CBool(Session("ApplyTOTPForLoanApplicationApproval")) Then
                'Hemant (29 Mar 2024) -- End
            If CInt(Session("SMSGatewayEmailType")) = CInt(clsEmployee_Master.EmpColEnum.Col_Present_Mobile) Then
                mstrEmpMobileNo = objEmployee._Present_Mobile.ToString()
            ElseIf CInt(Session("SMSGatewayEmailType")) = CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Mobile) Then
                mstrEmpMobileNo = objEmployee._Domicile_Mobile.ToString()
            End If

            If mstrEmpMobileNo.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 60, "Mobile No is compulsory information. Please set Employee's Mobile No to send TOTP."), Me)
                Return False
            End If
            End If 'Hemant (29 Mar 2024)

            'Pinkal (02-Jun-2023) -- End


            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
            objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            'Hemant (27 Oct 2022) -- End
            If CInt(cboEmpName.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), Me)
                cboEmpName.Focus()
                Return False
            End If

            If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), Me)
                cboLoanScheme.Focus()
                Return False
            End If

            If CDec(txtLoanAmt.Text) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Applied Amount cannot be blank. Applied Amount is compulsory information."), Me)
                txtLoanAmt.Focus()
                Return False
            End If

            If CInt(cboCurrency.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Currency is compulsory information.Please select currency."), Me)
                cboCurrency.Focus()
                Return False
            End If

            If CDec(txtInstallmentAmt.Text) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Installment Amount cannot be 0.Please define installment amount greater than 0."), Me)
                txtInstallmentAmt.Focus()
                Return False
            End If



            If CDec(txtInstallmentAmt.Text) > CDec(txtLoanAmt.Text) AndAlso CInt(txtEMIInstallments.Text) > 1 Then
                'Hemant (07 Jul 2023) -- [nudDuration.Value > 1]
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Installment Amount cannot be greater than Loan Amount."), Me)
                txtInstallmentAmt.Focus()
                Return False
            End If

            'Hemant (18 Mar 2024) -- Start
            'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
            If mstrCompanyGroupName = "TADB" And txtRepaymentDays.Enabled = True AndAlso (txtRepaymentDays.Text.Trim.Length <= 0 OrElse CInt(txtRepaymentDays.Text) <= 0) Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 74, "No of Repayment Days cannot be 0.Please define No of Repayment Days greater than 0."), Me)
                txtRepaymentDays.Focus()
                Return False
            End If
            'Hemant (18 Mar 2024) -- End




            'Pinkal (28-Oct-2024) -- Start
            'NMB Enhancement : (A1X-2834) NMB - Credit card loan enhancements.
            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.CREDIT_CARD Then
                Dim mstrBankACNo As String = ""
                Dim objEmpBankTran As New clsEmployeeBanks

                Dim dsEmpBank As DataSet = objEmpBankTran.GetList(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString() _
                                                                                              , True, CBool(Session("IsIncludeInactiveEmp")), "EmployeeBank", , , CInt(cboEmpName.SelectedValue).ToString(), mdtPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

                If dsEmpBank IsNot Nothing AndAlso dsEmpBank.Tables(0).Rows.Count > 0 Then

                    If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then

                        Dim objCreditCardIntegration As New clsCreditCardIntegration
                        Dim dsList As DataSet = objCreditCardIntegration.GetList("List")

                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                            Dim dRRow As DataRow() = dsList.Tables(0).Select("directdebitaccno = '" & dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim() & "'")

                            If dRRow IsNot Nothing AndAlso dRRow.Length > 0 Then

                                Dim mstrDayssPastDue As String = "0.00"
                                Dim mstrWalletCreditLimit As String = "0.00"
                                Dim mstrProgramName As String = ""


                                If IsDBNull(dRRow(0)("programname")) = False AndAlso dRRow(0)("programname").ToString().Trim().Length > 0 Then
                                    Dim ar() As String = dRRow(0)("programname").ToString().Trim().Split(CChar("-"))
                                    If ar IsNot Nothing AndAlso ar.Length > 0 Then
                                        mstrProgramName = ar(0).ToString().Trim()
                                    End If
                                End If

                                If IsDBNull(dRRow(0)("dayspastdue")) = False AndAlso dRRow(0)("dayspastdue").ToString().Trim().Length > 0 Then
                                    If dRRow(0)("dayspastdue").ToString().Trim() = "-" Then
                                        mstrDayssPastDue = "0.00"
                                    ElseIf dRRow(0)("dayspastdue").ToString().Trim() = "" Then
                                        mstrDayssPastDue = "0.00"
                                    ElseIf dRRow(0)("dayspastdue").ToString().Trim() = " " Then
                                        mstrDayssPastDue = "0.00"
                                    Else
                                        mstrDayssPastDue = dRRow(0)("dayspastdue").ToString().Trim()
                                    End If
                                End If

                                If IsDBNull(dRRow(0)("walletcreditlimit")) = False AndAlso dRRow(0)("walletcreditlimit").ToString().Trim().Length > 0 Then
                                    If dRRow(0)("walletcreditlimit").ToString().Trim() = "-" Then
                                        mstrWalletCreditLimit = "0.00"
                                    ElseIf dRRow(0)("walletcreditlimit").ToString().Trim() = "" Then
                                        mstrWalletCreditLimit = "0.00"
                                    ElseIf dRRow(0)("walletcreditlimit").ToString().Trim() = " " Then
                                        mstrWalletCreditLimit = "0.00"
                                    Else
                                        mstrWalletCreditLimit = dRRow(0)("walletcreditlimit").ToString().Trim()
                                    End If
                                End If


                                If IsNumeric(mstrDayssPastDue) AndAlso CDec(mstrDayssPastDue) > 0 Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 77, "You cannnot apply for this loan application.Reason : you have already past dues days."), Me)
                                    Return False

                                ElseIf IsNumeric(mstrDayssPastDue) AndAlso CInt(mstrDayssPastDue) <= 0 AndAlso mstrProgramName.Trim.Length > 0 AndAlso mstrProgramName.Trim().ToUpper() = objLoanScheme._Code.Trim().ToUpper() Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 80, "You cannnot apply for this loan application.Reason : you have already past dues days entry for this loan scheme."), Me)
                                    Return False

                                ElseIf IsNumeric(mstrWalletCreditLimit) AndAlso CDec(mstrWalletCreditLimit) < CDec(txtLoanAmt.Text) Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 78, "You cannnot apply for this loan application.Reason : you have already exceeded with your credit card limit."), Me)
                                    txtLoanAmt.Focus()
                                    Return False

                                End If 'If CInt(dRRow(0)("dayspastdue")) > 0 Then

                            End If  'If dRRow IsNot Nothing AndAlso dRRow.Length > 0 Then

                            dRRow = Nothing
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 79, "You cannnot apply for this loan application.Reason : There is no record(s) related to credit card."), Me)
                            Return False

                        End If  'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                        objCreditCardIntegration = Nothing

                    End If ' If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then

                End If 'If dsEmpBank.Tables(0).Rows.Count > 0 Then

                objEmpBankTran = Nothing

            End If  ' If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.CREDIT_CARD Then

            'Pinkal (28-Oct-2024) -- End


            If chkMakeExceptionalApplication.Checked = False Then


                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                If objLoanScheme._MinNoOfInstallment > 0 AndAlso CInt(txtEMIInstallments.Text) < objLoanScheme._MinNoOfInstallment Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 56, "Installment months cannot be less than") & " " & objLoanScheme._MinNoOfInstallment & " " & Language.getMessage(mstrModuleName, 33, "for") & " " & objLoanScheme._Name & Language.getMessage(mstrModuleName, 34, " Scheme."), Me)
                    txtEMIInstallments.Focus()
                    Return False
                End If
                'Pinkal (23-Nov-2022) -- End


                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
                'Dim objLoanScheme As New clsLoan_Scheme
                'objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                'Hemant (27 Oct 2022) -- End
                'Hemant (02 Feb 2024) -- Start
                'ISSUE/ENHANCEMENT(NMB): To exclude one month installment on application because during application, the system considers the first month in installment payments but in practice Approvers selects the 2nd month as deduction period. 
                'If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(txtEMIInstallments.Text) > objLoanScheme._MaxNoOfInstallment Then
                If CInt(txtMaxInstallment.Text) > 0 AndAlso CInt(txtEMIInstallments.Text) > CInt(txtMaxInstallment.Text) Then
                    'Hemant (02 Feb 2024) -- End
                Language.setLanguage(mstrModuleName)
                    'Hemant (02 Feb 2024) -- Start
                    'ISSUE/ENHANCEMENT(NMB): To exclude one month installment on application because during application, the system considers the first month in installment payments but in practice Approvers selects the 2nd month as deduction period. 
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Installment months cannot be greater than ") & " " & objLoanScheme._MaxNoOfInstallment & Language.getMessage(mstrModuleName, 33, "for") & " " & objLoanScheme._Name & Language.getMessage(mstrModuleName, 34, " Scheme."), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Installment months cannot be greater than ") & " " & txtMaxInstallment.Text & " " & Language.getMessage(mstrModuleName, 33, "for") & " " & objLoanScheme._Name & Language.getMessage(mstrModuleName, 34, " Scheme."), Me)
                    'Hemant (02 Feb 2024) -- End
                txtEMIInstallments.Focus()
                Return False
            End If

                If CDec(txtLoanAmt.Text) > CDec(txtMaxLoanAmount.Text) Then
                Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, you can not apply for this loan scheme: Reason, Principal amount  is exceeding the Max Principal Amt") & " [" & Format(CDec(txtMaxLoanAmount.Text), Session("fmtCurrency").ToString) & "] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 42, " set for selected scheme."), Me)
                    txtLoanAmt.Focus()
                Return False
            End If

                If CDec(txtInstallmentAmt.Text) > CDec(txtMaxInstallmentAmt.Text) Then
                Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, you can not apply for this loan scheme: Reason, Installment amount  is exceeding the Max Installment amount") & " [" & Format(CDec(txtMaxInstallmentAmt.Text), Session("fmtCurrency").ToString) & "] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 42, " set for selected scheme."), Me)
                txtLoanAmt.Focus()
                Return False
            End If

                If CDec(txtMinLoanAmount.Text) > CDec(txtLoanAmt.Text) Then
                Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, you can not apply for this loan scheme: Reason, Principal amount  should be greater than the Minimum Principal amount") & " [" & Format(CDec(txtMinLoanAmount.Text), Session("fmtCurrency").ToString) & "] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 42, " set for selected scheme."), Me)
                txtLoanAmt.Focus()
                Return False
            End If

                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-946 - As a user, I want to have an option (check box) on the loan scheme to indicate which loan schemes are eligible for top-up. If a loan scheme doesn’t have this option checked, user cannot do top-up on it   
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
                'If CBool(objLoanScheme._IsEligibleForTopUp) = False AndAlso (CDec(txtOutStandingPrincipalAmt.Text) > 0 OrElse CDec(txtOutStandingInterestAmt.Text) > 0) Then
                '    Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 46, "Sorry, you can not apply for Topup for this loan scheme: Reason, TopUp is not allowed ") & Language.getMessage(mstrModuleName, 42, " set for selected scheme."), Me)
                '    Return False
                'End If
                'Hemant (27 Oct 2022) -- End
                'Hemant (12 Oct 2022) -- End

                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-950 - As a user, I don’t want the application to save if the number of paid installments is less than number of installments configured on loan scheme for top-up
                'Hemant (13 Dec 2022) -- Start
                'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - Top-up applications do not check the top-up condition is the number of paid installments in flex = 0
                'If CBool(objLoanScheme._IsPostingToFlexcube) = True AndAlso CInt(txtInstallmentPaid.Text) > 0 AndAlso CInt(txtInstallmentPaid.Text) <= CInt(objLoanScheme._MinOfInstallmentPaid) Then
                If CBool(objLoanScheme._IsPostingToFlexcube) = True AndAlso mintCurrentFlexcubeLoanCount > 0 AndAlso CInt(txtInstallmentPaid.Text) >= 0 AndAlso CInt(txtInstallmentPaid.Text) < CInt(objLoanScheme._MinOfInstallmentPaid) Then
                    'Hemant (13 Dec 2022) -- End
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 47, "Sorry, you can not apply for Topup for this loan scheme: Reason, No of Installment Paid should be greater than minimum No of Installment Paid ") & " [" & objLoanScheme._MinOfInstallmentPaid & "] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 42, " set for selected scheme."), Me)
                    Return False
                End If
                'Hemant (12 Oct 2022) -- End

                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
                'objLoanScheme = Nothing
                'Hemant (27 Oct 2022) -- End
            End If

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
            If CBool(objLoanScheme._IsPostingToFlexcube) = True AndAlso CBool(objLoanScheme._IsEligibleForTopUp) = False AndAlso (CDec(txtOutStandingPrincipalAmt.Text) > 0 OrElse CDec(txtOutStandingInterestAmt.Text) > 0) Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 46, "Sorry, you can not apply for Topup for this loan scheme: Reason, TopUp is not allowed ") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 42, " set for selected scheme."), Me)
                Return False
            End If
            'Hemant (27 Oct 2022) -- End

            If CInt(cboLoanCalcType.SelectedValue) = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI) Then
                Dim decValue As Decimal
                Decimal.TryParse(txtAppliedUptoPrincipalAmt.Text, decValue)
                If decValue <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Principal Amount cannot be 0. Please enter Principal amount."), Me)
                    txtAppliedUptoPrincipalAmt.Focus()
                    Return False
                End If
            End If

                If txtPurposeOfCredit.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Purpose Of Credit is compulsory information. Please enter Purpose Of Credit to continue."), Me)
                    txtPurposeOfCredit.Focus()
                    Return False
                End If

            'Hemant (13 Dec 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - For top-up loan applications, system should not allow saving the application if The Take Home amount is 0 or negative amount.
            If CDec(txtTakeHome.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 57, "Sorry, you do not qualify for a top-up at the moment. The take home amount should be greater than 0"), Me)
                txtLoanAmt.Focus()
                Return False
            End If
            'Hemant (13 Dec 2022) -- End

            'Hemant (17 Mar 2023) -- Start
            'ISSUE/EHANCEMENT(NMB) : Please keep it for exceptional. Its causing us trouble in CRB
            Dim dsSchemeCategoryMappring As DataSet = objLoanCategoryMapping.GetList("List", CInt(cboLoanSchemeCategory.SelectedValue))
            If dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 AndAlso CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("identitytypeunkid")) > 0 Then
                Dim strIdentityTypesIds As String = String.Join(",", dsSchemeCategoryMappring.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("identitytypeunkid").ToString).ToArray())
                Dim objIdentityTran As New clsIdentity_tran
                objIdentityTran._EmployeeUnkid = CInt(cboEmpName.SelectedValue)
                Dim dtIdentityType As DataTable = objIdentityTran._DataList
                Dim drIdentityType() As DataRow = dtIdentityType.Select("idtypeunkid IN (" & strIdentityTypesIds & ")")
                If drIdentityType.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "Sorry, NIDA number is missing or in pending state"), Me)
                    Return False
                End If
                objIdentityTran = Nothing
            End If
            'Hemant (17 Mar 2023) -- End 

            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then

                If txtPlotNo.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Plot No is compulsory information. Please enter Plot No to continue."), Me)
                    txtPlotNo.Focus()
                    Return False
                End If

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                'If txtBlockNo.Text.Trim.Length <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Block No is compulsory information. Please enter Block No to continue."), Me)
                '    txtBlockNo.Focus()
                '    Return False
                'End If
                'Pinkal (17-May-2024) -- End


                If txtStreet.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Street is compulsory information. Please enter Street to continue."), Me)
                    txtStreet.Focus()
                    Return False
                End If

                If txtTownCity.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Town/City is compulsory information. Please enter Town/City to continue."), Me)
                    txtTownCity.Focus()
                    Return False
                End If

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                If objLoanScheme._MarketValueMandatory AndAlso CDec(IIf(txtMarketValue.Text.Trim.Length <= 0, 0, txtMarketValue.Text.Trim)) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Market Value cannot be 0. Please enter Market Value."), Me)
                    txtMarketValue.Focus()
                    Return False
                End If
                'Pinkal (17-May-2024) -- End

                If txtCTNo.Text.Trim.Length <= 0 AndAlso objLoanScheme._Name.ToString.ToUpper.Contains("SEMI-FINISH MORTGAGE") = False Then
                    'Hemant (17 Jan 2025) -- [AndAlso objLoanScheme._Name.ToString.ToUpper.Contains("SEMI-FINISH MORTGAGE") = False]
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "CT No is compulsory information. Please enter CT No to continue."), Me)
                    txtCTNo.Focus()
                    Return False
                End If

                'Pinkal (21-Mar-2024) -- Start
                'NMB - Mortgage UAT Enhancements.
                'If txtLONo.Text.Trim.Length <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "LO No is compulsory information. Please enter LO No to continue."), Me)
                '    txtLONo.Focus()
                '    Return False
                'End If
                'Pinkal (21-Mar-2024) -- End

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                If objLoanScheme._FSVValueMandatory AndAlso CDec(IIf(txtFSV.Text.Trim.Length <= 0, 0, txtFSV.Text.Trim)) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "FSV cannot be 0.Please enter FSV."), Me)
                    txtFSV.Focus()
                    Return False
                End If

                If objLoanScheme._BOQMandatory AndAlso txtBOQ.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 75, "Bill of Quantity(BOQ) cannot be blank.Please enter Bill of Quantity(BOQ)."), Me)
                    txtBOQ.Focus()
                    Return False
                End If
                'Pinkal (17-May-2024) -- End


                'Hemant (07 Jul 2023) -- Start
                'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
                If dtpTitleIssueDate.GetDate = Nothing Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 64, "Title Issue Date is compulsory information. Please enter Title Issue Date to continue."), Me)
                    dtpTitleIssueDate.Focus()
                    Return False
                End If

                If CInt(nudTitleValidity.Text) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 65, "Title Validity cannot be 0.Please enter Title Validity."), Me)
                    nudTitleValidity.Focus()
                    Return False
                End If

                If dtpTitleExpiryDate.GetDate = Nothing Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 66, "Title Expriry Date is compulsory information. Please enter Title Expriry Date to continue."), Me)
                    dtpTitleExpiryDate.Focus()
                    Return False
                End If
                'Hemant (07 July 2023) -- End
                'Hemant (07 Jul 2023) -- Start
                'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
                If dtpTitleExpiryDate.GetDate.Date < eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date.AddDays(CInt(objLoanScheme._NoOfDaysBefereTitleExpiry)) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 67, "Sorry, you can not apply for this Mortgage Loan: Reason, Title Expiry Date should not be less than Date :" & eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date.AddDays(CInt(objLoanScheme._NoOfDaysBefereTitleExpiry)).ToShortDateString), Me)
                    dtpTitleExpiryDate.Focus()
                    Return False
                End If
                'Hemant (07 July 2023) -- End
            End If

            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.SECURED Then

                If txtModel.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Model is compulsory information. Please enter Model to continue."), Me)
                    txtModel.Focus()
                    Return False
                End If

                If txtYOM.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "YOM is compulsory information. Please enter YOM to continue."), Me)
                    txtYOM.Focus()
                    Return False
                End If

                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-998 - As a user, I want to have additional fields on application and approval pages where loan category = Secured. The below are the fields and they should be mandatory(Chassis number,Supplier,Colour)
                If txtChassisNo.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 49, "Chassis No is compulsory information. Please enter Chassis No to continue."), Me)
                    txtChassisNo.Focus()
                    Return False
                End If

                If txtSupplier.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 50, "Supplier is compulsory information. Please enter Supplier to continue."), Me)
                    txtSupplier.Focus()
                    Return False
                End If

                If txtColour.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 51, "Colour is compulsory information. Please enter Colour to continue."), Me)
                    txtColour.Focus()
                    Return False
                End If
                'Hemant (27 Oct 2022) -- End

                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                If mstrCompanyGroupName = "TADB" Then
                    If CDec(txtTotalCIF.Text) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 68, "Total CIF cannot be 0.Please enter Total CIF."), Me)
                        txtTotalCIF.Focus()
                        Return False
                    End If

                    If CDec(txtEstimatedTax.Text) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 69, "Estimated Tax cannot be 0.Please enter Estimated Tax."), Me)
                        txtEstimatedTax.Focus()
                        Return False
                    End If

                    If CDec(txtInvoiceValue.Text) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 70, "Invoice Value cannot be 0.Please enter Invoice Value."), Me)
                        txtInvoiceValue.Focus()
                        Return False
                    End If

                    If txtModelNo.Text.Trim.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 71, "Model No is compulsory information. Please enter Model No to continue."), Me)
                        txtModelNo.Focus()
                        Return False
                    End If

                    If txtEngineNo.Text.Trim.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 72, "Engine No is compulsory information. Please enter Engine No to continue."), Me)
                        txtEngineNo.Focus()
                        Return False
                    End If

                    If CDec(txtEngineCapacity.Text) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 73, "Engine Capacity cannot be 0.Please enter Engine Capacity."), Me)
                        txtEngineCapacity.Focus()
                        Return False
                    End If
                End If
                'Hemant (29 Mar 2024) -- End

            End If

            If chkMakeExceptionalApplication.Checked = False Then

                If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then

                    'Pinkal (17-May-2024) -- Start
                    'NMB Enhancement For Mortgage Loan.
                    'Hemant (17 Jan 2025) -- Start
                    'ISSUE/ENHANCEMENT(NMB): A1X - 2968 :  Semi finished mortgage loan changes
                    If objLoanScheme._Name.ToString.ToUpper.Contains("SEMI-FINISH MORTGAGE") = False Then
                        'Hemant (17 Jan 2025) -- End
                    'Hemant (13 Dec 2022) -- Start
                    'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - For loan applications where loan category = mortgage, they don’t want the application to save if FSV < Market Value .
                    If (objLoanScheme._FSVValueMandatory OrElse CDec(IIf(txtFSV.Text.Trim.Length <= 0, 0, txtFSV.Text.Trim)) > 0) AndAlso _
                       (objLoanScheme._MarketValueMandatory OrElse CDec(IIf(txtMarketValue.Text.Trim.Length <= 0, 0, txtMarketValue.Text.Trim)) > 0) AndAlso _
                       CDec(IIf(txtFSV.Text.Trim.Length <= 0, 0, txtFSV.Text.Trim)) > CDec(IIf(txtMarketValue.Text.Trim.Length <= 0, 0, txtMarketValue.Text.Trim)) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 76, "FSV cannot be more than the Market Value."), Me)
                        txtFSV.Focus()
                        Return False
                    End If
                    'Hemant (13 Dec 2022) -- End
                    'Hemant (13 Dec 2022) -- Start
                    'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - For loan applications where loan category = mortgage, they don’t want system to save the application if the Principle Amount is greater than the FSV.
                    If (objLoanScheme._FSVValueMandatory OrElse CDec(IIf(txtFSV.Text.Trim.Length <= 0, 0, txtFSV.Text.Trim)) > 0) AndAlso CDec(txtLoanAmt.Text) > CDec(IIf(txtFSV.Text.Trim.Length <= 0, 0, txtFSV.Text.Trim)) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 59, "Principle amount cannot be greater than the FSV"), Me)
                        txtLoanAmt.Focus()
                        Return False
                    End If
                    'Hemant (13 Dec 2022) -- End
                    End If 'Hemant (17 Jan 2025)
                    'Pinkal (17-May-2024) -- End

                End If
                If objLoanScheme._IsDisableEmployeeConfirm = False AndAlso objEmployee._Confirmation_Date >= eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) Then
                    'Hemant (22 Nov 2024) -- [objLoanScheme._IsDisableEmployeeConfirm = False]
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Sorry, you are not confirmed yet. Please contact HR support"), Me)
                Return False
            End If

            dsData = objEmployeeDates.GetList("List", enEmp_Dates_Transaction.DT_TERMINATION, _
                                                CDate(Session("fin_startdate").ToString), _
                                                 CInt(cboEmpName.SelectedValue))

            If dsData IsNot Nothing AndAlso dsData.Tables("List").Rows.Count > 0 Then
                If DateDiff(DateInterval.Day, CDate(dsData.Tables("List").Rows(0).Item("effectivedate")), CDate(dsData.Tables("List").Rows(0).Item("date1"))) <= 180 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Sorry, your contract duration is less than 6 months. Please contact HR support"), Me)
                    Return False
                End If
            End If

                'Hemant (17 Mar 2023) -- Start
                'ISSUE/EHANCEMENT(NMB) : Please keep it for exceptional. Its causing us trouble in CRB
                'Dim dsSchemeCategoryMappring As DataSet = objLoanCategoryMapping.GetList("List", CInt(cboLoanSchemeCategory.SelectedValue))
                'If dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 AndAlso CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("identitytypeunkid")) > 0 Then
                '    Dim objIdentityTran As New clsIdentity_tran
                '    objIdentityTran._EmployeeUnkid = CInt(cboEmpName.SelectedValue)
                '    Dim dtIdentityType As DataTable = objIdentityTran._DataList
                '    Dim drIdentityType() As DataRow = dtIdentityType.Select("idtypeunkid=" & CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("identitytypeunkid")))
                '    If drIdentityType.Length <= 0 Then
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "Sorry, NIDA number is missing or in pending state"), Me)
                '        Return False
                '    End If
                '    objIdentityTran = Nothing
                'End If
                'Hemant (17 Mar 2023) -- End 

            Dim mintUserID As Integer
            If CInt(Session("UserId")) <= 0 Then
                Dim objUser As New clsUserAddEdit
                mintUserID = objUser.Return_UserId(CInt(cboEmpName.SelectedValue), CInt(Session("CompanyUnkId")))
                objUser = Nothing
            Else
                mintUserID = CInt(Session("UserId"))
            End If
            If dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 AndAlso CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("chargestatusunkid")) > 0 Then
                Dim dsDisplinaryFile As New DataSet
                    dsDisplinaryFile = objDiscFileMaster.GetList(Session("Database_Name").ToString, mintUserID, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", , " hrdiscipline_file_master.involved_employeeunkid = '" & CInt(cboEmpName.SelectedValue) & "' AND A.StatusId = " & CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("chargestatusunkid")) & " ", False)
                If dsDisplinaryFile.Tables(0).Rows.Count > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Sorry, you have a pending disciplinary issue. Kindly contact HR ER."), Me)
                    Return False
                End If
            End If

            Dim dsProceedingMaster As New DataSet
            dsProceedingMaster = objProceedingMaster.GetList(Session("Database_Name").ToString, mintUserID, CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("UserAccessModeSetting")), _
                                                 True, CBool(Session("IsIncludeInactiveEmp")), "List", " hrdiscipline_file_master.involved_employeeunkid = " & CInt(cboEmpName.SelectedValue) & "", CBool(Session("AddProceedingAgainstEachCount")))
            Dim drProceedingMaster() As DataRow = dsProceedingMaster.Tables(0).Select("penalty_expirydate >= '" & ConfigParameter._Object._CurrentDateAndTime.Date & "' ")
            If drProceedingMaster.Length > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Sorry, you have a pending disciplinary issue. Kindly contact HR ER."), Me)
                Return False
            End If


            Dim intNoOfInstallment As Integer
            Integer.TryParse(txtEMIInstallments.Text, intNoOfInstallment)
            If intNoOfInstallment > 1 Then

                    If IsDBNull(objEmployee._Empl_Enddate) = False AndAlso objEmployee._Empl_Enddate <> Nothing Then
                Dim dtLoanEnd As Date = mdtPeriodStart.AddMonths(intNoOfInstallment).AddDays(-1)
                        'Hemant (27 Oct 2023) -- Start
                        'ISSUE(NMB): Showing Message "Sorry, installments cannot go beyond the end of contract date" While Approving Flexcube Loan
                        'Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, mdtPeriodStart, objEmployee._Empl_Enddate.Date.AddDays(1)))
                        Dim intEmpTenure As Integer = Math.Abs((mdtPeriodStart.Month - objEmployee._Empl_Enddate.Date.Month) + 12 * (mdtPeriodStart.Year - objEmployee._Empl_Enddate.Date.Year)) + 1
                        'Hemant (27 Oct 2023) -- End
                    If intNoOfInstallment > intEmpTenure Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Sorry, installments cannot go beyond the end of contract date"), Me)
                        If txtEMIInstallments.Enabled = True Then txtEMIInstallments.Focus()
                        Return False
                    End If
                End If

                    If IsDBNull(objEmployee._Termination_To_Date) = False AndAlso objEmployee._Termination_To_Date <> Nothing Then
                    Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, mdtPeriodStart, objEmployee._Termination_To_Date.Date.AddMonths(-3).AddDays(1)))
                    If intNoOfInstallment > intEmpTenure Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Sorry, no of installments selected cannot go beyond the retirement date"), Me)
                        If txtEMIInstallments.Enabled = True Then txtEMIInstallments.Focus()
                        Return False

                    End If
                End If
            End If
            End If

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-996 - As a user, I want to have only one pending loan application at a time. System should not allow application of another loan if there’s a pending entry in the system regardless of the loan scheme
            Dim dsProcessPendingLoan As New DataSet
            strFilter = " lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmpName.SelectedValue) & " AND lnloan_process_pending_loan.loan_statusunkid = " & enLoanApplicationStatus.PENDING & ""
            If mintProcesspendingloanunkid > 0 Then
                strFilter &= " AND lnloan_process_pending_loan.processpendingloanunkid <> " & mintProcesspendingloanunkid & " "
            End If

            dsProcessPendingLoan = objProcesspendingloan.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 CStr(Session("UserAccessModeSetting")), _
                                                                 True, _
                                                                 CBool(Session("IsIncludeInactiveEmp")), _
                                                                 "Loan", , strFilter, , _
                                                                 CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))
            If dsProcessPendingLoan IsNot Nothing AndAlso dsProcessPendingLoan.Tables(0).Rows.Count > 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 48, "Sorry, you can not apply for this loan scheme: Reason, Approval for Another Loan is still in Pending Stage."), Me)
                Return False
            End If
            dsProcessPendingLoan = Nothing
            strFilter = ""
            'Hemant (27 Oct 2022) -- End

            If mstrCompanyGroupName = "NMB PLC" AndAlso objEmployee._EmpSignature Is Nothing Then
                'Hemant (18 Mar 2024) -- [mstrCompanyGroupName = "NMB PLC"]
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 30, "Sorry, signature missing. Please update signature from your profile"), Me)
                Return False
            End If

            If mstrCompanyGroupName = "NMB PLC" AndAlso chkconfirmSign.Checked = False Then
                'Hemant (18 Mar 2024) -- [mstrCompanyGroupName = "NMB PLC"]
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, "Sorry, Please confirm your signature first."), Me)
                Return False
            End If


            'Pinkal (20-Sep-2022) -- Start 
            'NMB Loan Module Enhancement.
            If CInt(Session("LoanIntegration")) = enLoanIntegration.FlexCube AndAlso CBool(Session("RoleBasedLoanApproval")) Then
                Dim objRoleMapping As New clsloanscheme_role_mapping

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'Dim dsList As DataSet = objRoleMapping.GetList("List", True, " AND lnloanscheme_role_mapping.isexceptional = " & CInt(IIf(chkMakeExceptionalApplication.Checked, 1, 0)) & " AND lnloanscheme_role_mapping.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue), Nothing)
                Dim dsList As DataSet = objRoleMapping.GetList("List", True, False, " AND lnloanscheme_role_mapping.isexceptional = " & CInt(IIf(chkMakeExceptionalApplication.Checked, 1, 0)) & " AND lnloanscheme_role_mapping.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue), Nothing)
                'Pinkal (04-Aug-2023) -- End

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "Loan Scheme is not mapped with Role and Level.please map Loan scheme with role and level."), Me)
                    objRoleMapping = Nothing
                    Return False
                End If
                objRoleMapping = Nothing

                'Pinkal (12-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                Dim objLoanSchemeRoleMapping As New clsloanscheme_role_mapping

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'Dim dtTable As DataTable = objLoanSchemeRoleMapping.GetNextApprovers(Session("Database_Name").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")) _
                '                                                                                            , enUserPriviledge.AllowToApproveLoan, mdtApplcationDate.Date, CInt(Session("UserId")), CInt(cboEmpName.SelectedValue), CInt(cboLoanScheme.SelectedValue) _
                '                                                                                            , chkMakeExceptionalApplication.Checked, CDec(txtOutStandingPrincipalAmt.Text) + CDec(txtLoanAmt.Text), Nothing, "")

                Dim dtTable As DataTable = objLoanSchemeRoleMapping.GetNextApprovers(Session("Database_Name").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")) _
                                                                                                            , enUserPriviledge.AllowToApproveLoan, mdtApplcationDate.Date, CInt(Session("UserId")), CInt(cboEmpName.SelectedValue), CInt(cboLoanScheme.SelectedValue) _
                                                                                                            , chkMakeExceptionalApplication.Checked, CDec(txtOutStandingPrincipalAmt.Text) + CDec(txtLoanAmt.Text), False, Nothing, "")
                'Pinkal (04-Aug-2023) -- End


                If mblnRequiredReportingToApproval = False AndAlso mblnSkipApprovalFlow = False AndAlso dtTable IsNot Nothing AndAlso dtTable.Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 55, "You cannot apply for this loan application.Reason : Loan application's loan amount is not match with the approver level's minimum and maximum range. "), Me)
                    objLoanSchemeRoleMapping = Nothing
                    Return False
                End If
                objLoanSchemeRoleMapping = Nothing
                'Pinkal (12-Oct-2022) -- End

            End If
            'Pinkal (20-Sep-2022) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            If mstrCompanyGroupName = "TADB" Then
                Dim intPrevPeriod As Integer = (New clsMasterData).getCurrentPeriodID(CInt(enModuleReference.Payroll), mdtPeriodStart.AddDays(-1), 0, 0, True, False, Nothing, False)
                Dim dtStart As Date = mdtPeriodStart
                Dim dtEnd As Date = mdtPeriodEnd
                Dim objPeriod As New clscommom_period_Tran
                Dim objCompany As New clsCompany_Master
                objPeriod._Periodunkid(Session("Database_Name").ToString) = intPrevPeriod
                Dim intyearid As Integer = objPeriod._Yearunkid
                Dim dsCompany As DataSet = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), -1, "List", intyearid)

                Dim strDBName As String = Session("Database_Name").ToString
                If dsCompany IsNot Nothing AndAlso dsCompany.Tables(0).Rows.Count > 0 Then
                    strDBName = dsCompany.Tables(0).Rows(0).Item("database_name").ToString
                End If

                objPeriod = Nothing
                objCompany = Nothing

                Dim dsPayrollData As DataSet = (New clsPayrollProcessTran).GetList(strDBName, CInt(Session("UserId")), intyearid, CInt(Session("CompanyUnkId")), dtStart, dtEnd, CStr(Session("UserAccessModeSetting")), True, False, "List", CInt(cboEmpName.SelectedValue), intPrevPeriod)

                Dim decGrossPay As Decimal = 0

                Dim drGrossPay() As DataRow = dsPayrollData.Tables(0).Select(" trnheadcode = '061' ")

                If drGrossPay.Length > 0 Then
                    decGrossPay = CDec(drGrossPay.CopyToDataTable.Compute("SUM(amount)", ""))
                End If

                Dim decTotalDeduction As Decimal = 0
                Dim drTotalDeduction() As DataRow = dsPayrollData.Tables(0).Select(" trnheadcode = '060' ")
                If drTotalDeduction.Length > 0 Then
                    decTotalDeduction = CDec(drTotalDeduction.CopyToDataTable.Compute("SUM(amount)", ""))
                End If

                Dim decExcludeDeduction As Decimal = 0
                If objLoanScheme._Name.Trim.ToString() = "Personal Loan" AndAlso (CDec(txtOutStandingPrincipalAmt.Text) > 0 OrElse CDec(txtOutStandingInterestAmt.Text) > 0) Then
                    Dim decPersonalLoanInstallmentAmt As Decimal = 0
                    Dim drPersonalLoan() As DataRow = dsPayrollData.Tables(0).Select(" trnheadname = 'Personal Loan INSTALLMENT' ")
                    If drPersonalLoan.Length > 0 Then
                        decPersonalLoanInstallmentAmt = CDec(drPersonalLoan.CopyToDataTable.Compute("SUM(amount)", ""))
                    End If
                    decExcludeDeduction = decExcludeDeduction + decPersonalLoanInstallmentAmt

                    If CBool(chkSalaryAdvanceLiquidation.Checked) = True Then
                        Dim decSalaryAdvanceInstallmentAmt As Decimal = 0
                        Dim drSalaryAdvance() As DataRow = dsPayrollData.Tables(0).Select(" trnheadname = 'Salary Advance INSTALLMENT' ")
                        If drSalaryAdvance.Length > 0 Then
                            decSalaryAdvanceInstallmentAmt = CDec(drSalaryAdvance.CopyToDataTable.Compute("SUM(amount)", ""))
                        End If
                        decExcludeDeduction = decExcludeDeduction + decSalaryAdvanceInstallmentAmt
                    End If

                End If

                decTotalDeduction = decTotalDeduction + CDec(txtInstallmentAmt.Text) - decExcludeDeduction

                Dim decMonthlyDedPerc As Decimal = 0
                If decGrossPay > 0 Then
                    decMonthlyDedPerc = decTotalDeduction * 100 / decGrossPay
                End If

                If decMonthlyDedPerc > 66.67 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 81, "You cannot apply for this loan application.Reason : Percentage(%) of total monthly deduction to gross pay exceed 66.67%."), Me)
                    Return False
                End If
            End If
            'Hemant (22 Nov 2024) -- End



            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objEmployeeDates = Nothing
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
            objLoanScheme = Nothing
            'Hemant (27 Oct 2022) -- End
        End Try
    End Function

    Private Sub FillPeriod(ByVal blnOnlyFirstPeriod As Boolean)
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombos As DataSet
        Dim mdtTable As DataTable
        Try


            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                 CInt(Session("Fin_year")), _
                                                 Session("Database_Name").ToString, _
                                                 CDate(Session("fin_startdate")), _
                                                 "List", False, enStatusType.OPEN)

            If dsCombos.Tables(0).Rows.Count > 0 Then
                mintDeductionPeriodUnkId = CInt(dsCombos.Tables(0).Rows(0).Item("periodunkid"))
            End If

            cboDeductionPeriod_SelectedIndexChanged()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub ResetMax()
        Try
            If CInt(cboEmpName.SelectedValue) > 0 AndAlso CInt(cboLoanScheme.SelectedValue) > 0 Then
                Dim objLScheme As New clsLoan_Scheme
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : - On loan scheme master, if a loan scheme has posting to flexcube enabled, as a user, I want to have the option of specifying the number of installments that need to be paid before a top-up can be done. 
                Dim objEmpBankTran As New clsEmployeeBanks
                Dim objloanAdvance As New clsLoan_Advance
                'Hemant (12 Oct 2022) -- End
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                Dim objLoanCategoryMapping As New clsLoanCategoryMapping
                'Hemant (27 Oct 2022) -- End

                objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)

                'Pinkal (20-Sep-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnRequiredReportingToApproval = objLScheme._RequiredReportingToApproval
                'Pinkal (20-Sep-2022) -- End

                'Pinkal (12-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnSkipApprovalFlow = objLScheme._IsSkipApproval
                'Pinkal (12-Oct-2022) -- End

                'Pinkal (10-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnLoanApproval_DailyReminder = objLScheme._LoanApproval_DailyReminder
                mintEscalationDays = objLScheme._EscalationDays
                'Pinkal (10-Nov-2022) -- End

                mdecMaxLoanAmountDefined = objLScheme._MaxLoanAmountLimit
                'Hemant (03 Feb 2023) -- Start
                'ISSUE/EHANCEMENT(NMB) : A1X-619 -  New formula for calculating Maximum Installment on loan application and approval pages
                mstrLoanSchemeCode = objLScheme._Code
                'Hemant (03 Feb 2023) -- End  
                txtLoanRate.Text = CStr(objLScheme._InterestRate)
                mintMaxLoanCalcTypeId = CInt(objLScheme._MaxLoanAmountCalcTypeId)
                mstrMaxLoanAmountHeadFormula = CStr(objLScheme._MaxLoanAmountHeadFormula)
                cboLoanCalcType.SelectedValue = CStr(objLScheme._LoanCalcTypeId)
                cboInterestCalcType.SelectedValue = CStr(objLScheme._InterestCalctypeId)
                'Call cboLoanCalcType_SelectedIndexChanged(sender, e)
                cboLoanSchemeCategory.SelectedValue = CStr(objLScheme._LoanSchemeCategoryId)

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                If objLScheme._MarketValueMandatory = False Then
                    If lblMarketValue.Text.Trim.IndexOf("<") >= 0 Then
                        lblMarketValue.Text = lblMarketValue.Text.Trim().Substring(0, lblMarketValue.Text.Trim.IndexOf("<")).Replace("<span style='color: White;font-size: medium'>*</span>", "")
                    End If
                End If
                If objLScheme._FSVValueMandatory = False Then
                    If lblFSV.Text.Trim.IndexOf("<") >= 0 Then
                        lblFSV.Text = lblFSV.Text.Trim().Substring(0, lblFSV.Text.Trim.IndexOf("<")).Replace("<span style='color: White;font-size: medium'>*</span>", "")
                    End If
                End If
                If objLScheme._BOQMandatory = False Then
                    If LblBOQ.Text.Trim.IndexOf("<") >= 0 Then
                        LblBOQ.Text = LblBOQ.Text.Trim().Substring(0, LblBOQ.Text.Trim.IndexOf("<")).Replace("<span style='color: White;font-size: medium'>*</span>", "")
                    End If
                End If
                'Pinkal (17-May-2024) -- End

                'Hemant (17 Jan 2025) -- Start
                'ISSUE/ENHANCEMENT(NMB): A1X - 2968 :  Semi finished mortgage loan changes
                If objLScheme._Name.ToString.ToUpper.Contains("SEMI-FINISH MORTGAGE") = True Then
                    If lblCTNo.Text.Trim.IndexOf("<") >= 0 Then
                        lblCTNo.Text = lblCTNo.Text.Trim().Substring(0, lblCTNo.Text.Trim.IndexOf("<")).Replace("<span style='color: White;font-size: medium'>*</span>", "")
                    End If
                End If
                'Hemant (17 Jan 2025) -- End



                'Hemant (18 Mar 2024) -- Start
                'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
                If mstrCompanyGroupName = "NMB PLC" Then
                    'Hemant (18 Mar 2024) -- END
                txtRepaymentDays.Text = CStr(objLScheme._RepaymentDays)
                End If 'Hemant (18 Mar 2024)

                'Hemant (07 Jul 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-1099 - Changes in maximum principal amount calculation to factor in remaining months to applicant's last employment date
                If CInt(nudPayPeriodEOC.Text) < CInt(objLScheme._MaxNoOfInstallment) Then
                    mintMaxNoOfInstallmentLoanScheme = CInt(nudPayPeriodEOC.Text)
                Else
                    'Hemant (07 Jul 2023) -- End
                mintMaxNoOfInstallmentLoanScheme = CInt(objLScheme._MaxNoOfInstallment)
                End If 'Hemant (07 Jul 2023)
                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                If mstrCompanyGroupName = "NMB PLC" Then
                    'Hemant (29 Mar 2024) -- End
                If IsNotMortgageLoanExist() = True Then 'Hemant (07 Jul 2023)
                mstrMaxInstallmentHeadFormula = objLScheme._MaxInstallmentHeadFormula
                    'Hemant (07 Jul 2023) -- Start
                    'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
                Else
                    mstrMaxInstallmentHeadFormula = objLScheme._MaxInstallmentHeadFormulaForMortgage
                    'Hemant (07 Jul 2023) -- End
                End If
                    'Hemant (29 Mar 2024) -- Start
                    'ENHANCEMENT(TADB): New loan application form
                Else
                    mstrMaxInstallmentHeadFormula = objLScheme._MaxInstallmentHeadFormula
                End If
                'Hemant (29 Mar 2024) -- End

                If mintMaxLoanCalcTypeId = 2 Then
                    mdecMaxLoanAmount = GetAmountByHeadFormula(mstrMaxLoanAmountHeadFormula)
                Else
                    mdecMaxLoanAmount = mdecMaxLoanAmountDefined
                End If
                If mstrMaxInstallmentHeadFormula.Trim.Length <= 0 Then
                    'txtMaxLoanAmount.Text = Format(GetMaxLoanAmountDefinedLoanScheme(decMaxLoanAmount), Session("fmtCurrency").ToString)
                    txtMaxLoanAmount.Text = Format(mdecMaxLoanAmount, Session("fmtCurrency").ToString)
                Else
                    mdecEstimateMaxInstallmentAmt = GetAmountByHeadFormula(mstrMaxInstallmentHeadFormula)
                    'Hemant (22 Nov 2024) -- Start
                    'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                    If mstrCompanyGroupName = "TADB" Then
                        txtMaxLoanAmount.Text = Format(mdecMaxLoanAmount, Session("fmtCurrency").ToString)
                    Else
                        'Hemant (22 Nov 2024) -- End
                    txtMaxLoanAmount.Text = Format(IIf(mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme > mdecMaxLoanAmount, mdecMaxLoanAmount, mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme), Session("fmtCurrency").ToString)
                    End If  'Hemant (22 Nov 2024) -- End

                    'Hemant (22 Dec 2023) -- Start
                    mdecMaxLoanAmount = CDec(txtMaxLoanAmount.Text)
                    'Hemant (22 Dec 2023) -- End
                End If

                mdecMinLoanAmount = objLScheme._MinLoanAmountLimit


                txtMinLoanAmount.Text = Format(mdecMinLoanAmount, Session("fmtCurrency").ToString)

                Panel1.Visible = False
                Panel2.Visible = False
                If CInt(objLScheme._LoanSchemeCategoryId) = enLoanSchemeCategories.SECURED Then
                    Panel2.Visible = True
                ElseIf CInt(objLScheme._LoanSchemeCategoryId) = enLoanSchemeCategories.MORTGAGE Then
                    Panel1.Visible = True
                End If

                'Hemant (07 Jul 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-1099 - Changes in maximum principal amount calculation to factor in remaining months to applicant's last employment date
                If CInt(nudPayPeriodEOC.Text) < CInt(objLScheme._MaxNoOfInstallment) Then
                    txtMaxInstallment.Text = nudPayPeriodEOC.Text
                Else
                    'Hemant (07 Jul 2023) -- End
                txtMaxInstallment.Text = CStr(objLScheme._MaxNoOfInstallment)
                End If 'Hemant (07 Jul 2023)

                txtInsuranceRate.Text = CStr(objLScheme._InsuranceRate)

                txtMaxInstallmentAmt.Text = Format(mdecEstimateMaxInstallmentAmt, Session("fmtCurrency").ToString)

                'If mintProcesspendingloanunkid <= 0 Then
                RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
                txtLoanAmt.Text = Format(mdecMaxLoanAmount, Session("fmtCurrency").ToString)
                AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged

                RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                txtEMIInstallments.Text = txtMaxInstallment.Text
                AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged

                'Hemant (22 Nov 2024) -- Start
                'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                If mstrCompanyGroupName = "TADB" Then
                    txtMaxLoanAmount.Text = Format(mdecMaxLoanAmount, Session("fmtCurrency").ToString)
                    txtLoanAmt_TextChanged(txtLoanAmt, New EventArgs())
                Else
                    'Hemant (22 Nov 2024) -- End
                txtInstallmentAmt.Text = Format(mdecEstimateMaxInstallmentAmt, Session("fmtCurrency").ToString)
                Call txtInstallmentAmt_TextChanged(txtInstallmentAmt, New System.EventArgs())
                txtMaxLoanAmount.Text = Format(IIf(CDec(txtLoanAmt.Text) > mdecMaxLoanAmount, mdecMaxLoanAmount, CDec(txtLoanAmt.Text)), Session("fmtCurrency").ToString)
                End If  'Hemant (22 Nov 2024)                
    '        End If

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                'A1X-2587 NMB mortgage - Insurance deduction: The system should deduct insurance for the first year only and for the following year it will be done yearly. 
                If objLScheme._LoanSchemeCategoryId = enLoanSchemeCategories.MORTGAGE Then
                    If CInt(txtEMIInstallments.Text) > 12 Then
                        txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100), Session("fmtCurrency").ToString)
                    Else
                        txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100) * (CInt(txtEMIInstallments.Text) / 12), Session("fmtCurrency").ToString)
                    End If
                Else
                    txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100) * (CInt(txtEMIInstallments.Text) / 12), Session("fmtCurrency").ToString)
                End If
                'Pinkal (17-May-2024) -- End


                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-949 - As a user, on the application screen, if the loan scheme selected is eligible for top-up, my Take home should be calculated as Principal Amount – (Outstanding Principle + Outstanding Interest + Insurance Amount)
                'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - CDec(txtInsuranceAmt.Text), Session("fmtCurrency").ToString)
                Dim dsEmpBank As New DataSet
                Dim decOutStandingPrincipalAmt As Decimal = 0
                Dim decOutStandingInterestAmt As Decimal = 0
                Dim intNoOfInstallmentPaid As Integer = 0
                'Hemant (11 Oct 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2822 :  Allow to apply another loan if employee has 1 installment left to clear
                Dim intTotalNoOfInstallment As Integer = 0
                'Hemant (11 Oct 2024) -- End
                Dim xPrevPeriodStart As Date
                Dim xPrevPeriodEnd As Date
                If CInt(mintDeductionPeriodUnkId) > 0 Then
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(mintDeductionPeriodUnkId)
                    xPrevPeriodStart = objPeriod._Start_Date
                    xPrevPeriodEnd = objPeriod._End_Date
                    objPeriod = Nothing
                Else
                    xPrevPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    xPrevPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                End If
                dsEmpBank = objEmpBankTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), "EmployeeBank", False, , CStr(cboEmpName.SelectedValue), xPrevPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

                'call oracle function to get list of loan emis by passing bank account no.
                If dsEmpBank.Tables(0).Rows.Count > 0 Then
                    If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then
                        Dim dsFlexLoanData As DataSet = objloanAdvance.GetFlexcubeLoanData(Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString, Session("OracleUserPassword").ToString, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString, objLScheme._Code, , False)
                        'Hemant (29 Mar 2024) -- [dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString]
                        'Hemant (01 Sep 2023) -- [blnUseOldView := False]
                        If dsFlexLoanData IsNot Nothing AndAlso dsFlexLoanData.Tables(0).Rows.Count > 0 Then
                            decOutStandingPrincipalAmt = (From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("PRINCIPALOUTSTANDING")))).Sum()
                            decOutStandingInterestAmt = (From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("CURRENT_OUTSTANDING_INTEREST")))).Sum()
                            intNoOfInstallmentPaid = CInt((From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("PAIDNOOFINSTALMENT")))).Sum())
                            'Hemant (11 Oct 2024) -- Start
                            'ENHANCEMENT(NMB): A1X - 2822 :  Allow to apply another loan if employee has 1 installment left to clear
                            If objLScheme._Code.ToString.Trim = "CL27" Then
                                intTotalNoOfInstallment = CInt((From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("INSTALMENTCOUNT")))).Sum())
                                If intTotalNoOfInstallment - intNoOfInstallmentPaid <= 1 Then
                                    decOutStandingPrincipalAmt = 0
                                    decOutStandingInterestAmt = 0
                                End If
                            End If
                            'Hemant (11 Oct 2024) -- End
                            'Hemant (22 Dec 2022) -- Start
                            mintCurrentFlexcubeLoanCount = dsFlexLoanData.Tables(0).Rows.Count
                        Else
                            mintCurrentFlexcubeLoanCount = 0
                            'Hemant (22 Dec 2022) -- End
                        End If

                        'Pinkal (23-Nov-2022) -- Start
                        'NMB Loan Module Enhancement. [GETTING OUTSTANDING PRINCIPAL AMOUNT FROM OTHER LOAN SCHEME ALSO TO DEFINE THE APPROVER FLOW]
                        dsFlexLoanData = Nothing
                        dsFlexLoanData = objloanAdvance.GetFlexcubeLoanData(Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString, Session("OracleUserPassword").ToString, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString, "", False, False)
                        'Hemant (29 Mar 2024) -- [dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString]
                        'Hemant (01 Sep 2023) -- [blnUseOldView := False]
                        If dsFlexLoanData IsNot Nothing AndAlso dsFlexLoanData.Tables(0).Rows.Count > 0 Then
                            Dim decOtherLoanOutStandingPrincipalAmt As Decimal = (From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("PRINCIPALOUTSTANDING")))).DefaultIfEmpty.Sum()
                            If decOutStandingPrincipalAmt <= decOtherLoanOutStandingPrincipalAmt Then
                                mdecOtherLoanOutStandingPrincipalAmt = decOtherLoanOutStandingPrincipalAmt - decOutStandingPrincipalAmt
                            Else
                                mdecOtherLoanOutStandingPrincipalAmt = decOutStandingPrincipalAmt - decOtherLoanOutStandingPrincipalAmt
                    End If

                        End If ' If dsFlexLoanData IsNot Nothing AndAlso dsFlexLoanData.Tables(0).Rows.Count > 0 Then
                        'Pinkal (23-Nov-2022) -- End

                        'Hemant (22 Nov 2024) -- Start
                        'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                        If mstrCompanyGroupName = "TADB" AndAlso objLScheme._Code.Trim().ToString() = "56" AndAlso (decOutStandingPrincipalAmt > 0 OrElse decOutStandingInterestAmt > 0) Then
                            Dim dsSalaryAdvanceFlexcubeloan As DataSet = objloanAdvance.GetFlexcubeLoanData(Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString, Session("OracleUserPassword").ToString, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString, "58", , False)
                            If dsSalaryAdvanceFlexcubeloan IsNot Nothing AndAlso dsSalaryAdvanceFlexcubeloan.Tables(0).Rows.Count > 0 Then
                                mdecActualSalaryAdvancePendingPrincipalAmt = (From p In dsSalaryAdvanceFlexcubeloan.Tables(0).AsEnumerable() Select (CDec(p.Item("PRINCIPALOUTSTANDING")))).DefaultIfEmpty.Sum()
                            End If
                            If mdecActualSalaryAdvancePendingPrincipalAmt > 0 Then
                                chkSalaryAdvanceLiquidation.Visible = True
                            Else
                                chkSalaryAdvanceLiquidation.Visible = False
                            End If

                        Else
                            chkSalaryAdvanceLiquidation.Visible = False
                            chkSalaryAdvanceLiquidation.Checked = False
                            mdecActualSalaryAdvancePendingPrincipalAmt = 0
                            mdecFinalSalaryAdvancePendingPrincipalAmt = 0
                        End If
                        'Hemant (22 Nov 2024) -- End

                    End If 'If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then

                End If '  If dsEmpBank.Tables(0).Rows.Count > 0 Then

                txtOutStandingPrincipalAmt.Text = Format(decOutStandingPrincipalAmt, Session("fmtCurrency").ToString)
                txtOutStandingInterestAmt.Text = Format(decOutStandingInterestAmt, Session("fmtCurrency").ToString)
                txtInstallmentPaid.Text = intNoOfInstallmentPaid.ToString
                dsEmpBank = Nothing
                'If CDec(txtOutStandingPrincipalAmt.Text) > 0 OrElse CDec(txtOutStandingInterestAmt.Text) > 0 Then
                '    pnlOutstandingInfo.Visible = True
                'Else
                '    pnlOutstandingInfo.Visible = False
                'End If
                'Hemant (10 Nov 2022) -- Start
                'ISSUE/ENHANCEMENT(NMB) : For the Take home field in top-up loans. Please return it to calculate as normal loan only.
                'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(txtOutStandingPrincipalAmt.Text) + CDec(txtOutStandingInterestAmt.Text) + CDec(txtInsuranceAmt.Text)), Session("fmtCurrency").ToString)
                'Hemant (13 Dec 2022) -- Start
                'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - For top-up loans, they want the Take Home formula to be as below : Take Home = Principle – (Current Outstanding Principle + Current Outstanding Interest + Insurance Amount).
                'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - CDec(txtInsuranceAmt.Text), Session("fmtCurrency").ToString)
                txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(txtOutStandingPrincipalAmt.Text) + CDec(txtOutStandingInterestAmt.Text) + CDec(txtInsuranceAmt.Text) - mdecFinalSalaryAdvancePendingPrincipalAmt), Session("fmtCurrency").ToString)
                'Hemant (22 Nov 2024) -- [- mdecFinalSalaryAdvancePendingPrincipalAmt]
                'Hemant (13 Dec 2022) -- End
                'Hemant (10 Nov 2022) -- End
                'Hemant (12 Oct 2022) -- End

                mblnIsAttachmentRequired = objLScheme._IsAttachmentRequired
                mstrDocumentTypeIDs = CStr(objLScheme._DocumentTypeIDs)

                If mblnIsAttachmentRequired = True Then
                    pnlScanAttachment.Visible = True
                Else
                    pnlScanAttachment.Visible = False
                End If

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.

                'mintOtherDocumentunkid = 0
                'Dim dsSchemeCategoryMappring As DataSet = objLoanCategoryMapping.GetList("List", CInt(cboLoanSchemeCategory.SelectedValue))
                'If CInt(cboLoanSchemeCategory.SelectedValue) > 0 AndAlso dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 AndAlso CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("otherdocumentunkid")) > 0 Then
                '    mintOtherDocumentunkid = CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("otherdocumentunkid"))
                '    Dim lstDocumentTypeIDs As List(Of String) = mstrDocumentTypeIDs.Split(","c).ToList()
                '    lstDocumentTypeIDs.Remove(mintOtherDocumentunkid.ToString)
                '    mstrDocumentTypeIDs = String.Join(",", lstDocumentTypeIDs.ToArray())
                'End If
                'dsSchemeCategoryMappring = Nothing

                'If chkMakeExceptionalApplication.Checked = True AndAlso mintOtherDocumentunkid > 0 Then
                '    pnlOtherScanAttachment.Visible = True
                'Else
                '    pnlOtherScanAttachment.Visible = False
                'End If

                'If (chkMakeExceptionalApplication.Checked = True AndAlso mintOtherDocumentunkid > 0) OrElse mblnIsAttachmentRequired = True Then
                '    pnlAttachment.Visible = True
                'Else
                '    pnlAttachment.Visible = False
                'End If
                mstrOtherDocumentIds = ""
                Dim dsSchemeCategoryMappring As DataSet = objLoanCategoryMapping.GetList("List", CInt(cboLoanSchemeCategory.SelectedValue))
                If CInt(cboLoanSchemeCategory.SelectedValue) > 0 AndAlso dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 Then
                    mstrOtherDocumentIds = String.Join(",", dsSchemeCategoryMappring.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("otherdocumentunkid") > 0).Select(Function(x) x.Field(Of Integer)("otherdocumentunkid").ToString()).ToArray())
                    Dim lstDocumentTypeIDs As List(Of String) = mstrDocumentTypeIDs.Split(","c).ToList()
                    If mstrOtherDocumentIds.Trim.Length > 0 Then
                        Dim arOtherDocIds() As String = mstrOtherDocumentIds.Trim.Split(CChar(","))
                        For i As Integer = 0 To arOtherDocIds.Length - 1
                            lstDocumentTypeIDs.Remove(arOtherDocIds(i))
                        Next
                        arOtherDocIds = Nothing
                    End If
                    mstrDocumentTypeIDs = String.Join(",", lstDocumentTypeIDs.ToArray())
                End If
                dsSchemeCategoryMappring = Nothing

                If chkMakeExceptionalApplication.Checked = True AndAlso mstrOtherDocumentIds.Trim.Length > 0 Then
                    pnlOtherScanAttachment.Visible = True
                Else
                    pnlOtherScanAttachment.Visible = False
                End If

                If (chkMakeExceptionalApplication.Checked = True AndAlso mstrOtherDocumentIds.Trim.Length > 0) OrElse mblnIsAttachmentRequired = True Then
                    pnlAttachment.Visible = True
                Else
                    pnlAttachment.Visible = False
                End If


                'Pinkal (23-Nov-2022) -- End

               

                Call FillDocumentTypeCombo()
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                Call FillOtherDocumentTypeCombo()
                'Hemant (27 Oct 2022) -- End
                objLScheme = Nothing
                objEmpBankTran = Nothing
                objloanAdvance = Nothing
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                objLoanCategoryMapping = Nothing
                'Hemant (27 Oct 2022) -- End
            Else
                txtLoanRate.Text = Format(0, Session("fmtCurrency").ToString)
                cboLoanCalcType.SelectedValue = CStr(0)
                cboInterestCalcType.SelectedValue = CStr(0)
                cboLoanSchemeCategory.SelectedValue = CStr(0)
                txtRepaymentDays.Text = CStr(0)
                txtMaxInstallmentAmt.Text = Format(0, Session("fmtCurrency").ToString)
                txtMaxLoanAmount.Text = Format(0, Session("fmtCurrency").ToString)

                txtMinLoanAmount.Text = Format(0, Session("fmtCurrency").ToString)

                Panel1.Visible = False
                Panel2.Visible = False

                txtMaxInstallment.Text = CStr(0)

                txtInsuranceRate.Text = CStr(0)

                RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
                txtLoanAmt.Text = Format(0, Session("fmtCurrency").ToString)
                AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged

                RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                txtEMIInstallments.Text = "1"
                AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged

                txtInsuranceAmt.Text = Format(0, Session("fmtCurrency").ToString)
                txtTakeHome.Text = Format(0, Session("fmtCurrency").ToString)

                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                txtInstallmentAmt.Text = Format(0, Session("fmtCurrency").ToString)
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

                pnlScanAttachment.Visible = False
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                pnlOtherScanAttachment.Visible = False
                'Hemant (27 Oct 2022) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Function GetAmountByHeadFormula(ByVal strHeadFormula As String) As Decimal
        Dim objMaster As New clsMasterData
        Dim objPayrollProcess As New clsPayrollProcessTran
        Dim decAmount As Decimal = 0
        Try

            If CInt(cboEmpName.SelectedValue) > 0 AndAlso strHeadFormula.Trim.Length > 0 Then
                Dim intPrevPeriod As Integer = objMaster.getCurrentPeriodID(CInt(enModuleReference.Payroll), mdtPeriodStart.AddDays(-1), 0, 0, True, False, Nothing, False)

                'Hemant (22 Dec 2023) -- Start
                Dim objPeriod As New clscommom_period_Tran
                Dim objCompany As New clsCompany_Master
                objPeriod._Periodunkid(Session("Database_Name").ToString) = intPrevPeriod
                Dim intyearid As Integer = objPeriod._Yearunkid
                Dim dsCompany As DataSet = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), -1, "List", intyearid)

                Dim strDatabaseName As String = Session("Database_Name").ToString
                If dsCompany IsNot Nothing AndAlso dsCompany.Tables(0).Rows.Count > 0 Then
                    strDatabaseName = dsCompany.Tables(0).Rows(0).Item("database_name").ToString
                End If

                objPeriod = Nothing
                objCompany = Nothing
                'Hemant (22 Dec 2023) -- End

                Dim mintUserID As Integer
                If CInt(Session("UserId")) <= 0 Then
                    'Dim objUser As New clsUserAddEdit
                    'mintUserID = objUser.Return_UserId(CInt(cboEmpName.SelectedValue), CInt(Session("CompanyUnkId")))
                    'objUser = Nothing
                    mintUserID = 1
                Else
                    mintUserID = CInt(Session("UserId"))
                End If


                'Pinkal (11-Mar-2024) -- Start
                '(A1X-2505) NMB - Credit card integration.
                If strHeadFormula.Trim.Length > 0 AndAlso strHeadFormula.Contains("#creditcardamountondsr#") Then
                    Dim mstrBankACNo As String = ""
                    Dim objEmpBankTran As New clsEmployeeBanks
                    Dim dsEmpBank As DataSet = objEmpBankTran.GetList(strDatabaseName, mintUserID, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString() _
                                                                                                  , True, CBool(Session("IsIncludeInactiveEmp")), "EmployeeBank", , , CInt(cboEmpName.SelectedValue).ToString(), mdtPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

                    If dsEmpBank IsNot Nothing AndAlso dsEmpBank.Tables(0).Rows.Count > 0 Then
                        If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then
                            Dim objCreditCardIntegration As New clsCreditCardIntegration
                            objCreditCardIntegration.GetEmpCreditCardAmount(dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim(), mstrLoanSchemeCode, mdecCreditCardAmountOnExposure, True)
                            objCreditCardIntegration = Nothing
                        End If
                    End If 'If dsEmpBank.Tables(0).Rows.Count > 0 Then
                    objEmpBankTran = Nothing
                End If
                'Pinkal (11-Mar-2024) -- End



                Dim dt As DataTable = objPayrollProcess.GetFormulaBasePayslipAmount(strDatabaseName, mintUserID, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString, True, False, "List", intPrevPeriod, CStr(cboEmpName.SelectedValue), strHeadFormula, "", _
                                                                                    Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString, Session("OracleUserPassword").ToString, mstrLoanSchemeCode)
                'Hemant (22 Dec 2023) -- [ Session("Database_Name").ToString --> strDatabaseName]
                'Hemant (03 Feb 2023) -- [mstrLoanSchemeCode]
                If dt.Rows.Count > 0 Then
                    decAmount = CDec(dt.Rows(0).Item("amount"))
                End If
            End If
            Return decAmount
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "GetAmountByHeadFormula", mstrModuleName)
        Finally
            objMaster = Nothing
            objPayrollProcess = Nothing
        End Try
    End Function

    Private Sub GetValue()
        Try

            If Not (objProcesspendingloan._Application_Date = Nothing) Then
                mdtApplcationDate = objProcesspendingloan._Application_Date
            End If

            If Not (objProcesspendingloan._Application_No = Nothing) Then
                mstrApplicationNo = objProcesspendingloan._Application_No
            End If

            cboEmpName.SelectedValue = CStr(objProcesspendingloan._Employeeunkid)
            cboEmpName_SelectedIndexChanged(cboEmpName, New EventArgs())
            cboLoanScheme.SelectedValue = CStr(objProcesspendingloan._Loanschemeunkid)
            Call cboLoanScheme_SelectedIndexChanged(cboLoanScheme, Nothing)
            txtLoanAmt.Text = Format(CDec(objProcesspendingloan._Loan_Amount), Session("fmtCurrency").ToString)
            If mintProcesspendingloanunkid > 0 Then
            cboCurrency.SelectedValue = CStr(objProcesspendingloan._Countryunkid)
            End If


            'RemoveHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            'txtAppliedUptoPrincipalAmt.Text = Format(objProcesspendingloan._InstallmentAmount, Session("fmtCurrency").ToString)
            'AddHandler txtPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            'RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
            txtEMIInstallments.Text = CStr(IIf(objProcesspendingloan._NoOfInstallment <= 0, 1, objProcesspendingloan._NoOfInstallment))
            mintDeductionPeriodUnkId = CInt(objProcesspendingloan._DeductionPeriodunkid)
            txtPurposeOfCredit.Text = objProcesspendingloan._Emp_Remark
            'AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
            'RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            'RemoveHandler cboLoanCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
            'txtLoanAmt.Text = Format(objProcesspendingloan._Loan_Amount, Session("fmtCurrency").ToString)
            'AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            'AddHandler cboLoanCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
            'Call cboDeductionPeriod_SelectedIndexChanged()

            objProcesspendingloan._Isvoid = objProcesspendingloan._Isvoid
            objProcesspendingloan._Voiddatetime = objProcesspendingloan._Voiddatetime
            objProcesspendingloan._Voiduserunkid = objProcesspendingloan._Voiduserunkid

            Call cboCurrency_SelectedIndexChanged(cboCurrency, New System.EventArgs())

            'RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            txtInstallmentAmt.Text = Format(CDec(objProcesspendingloan._InstallmentAmount), Session("fmtCurrency").ToString)
            'AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            'Call txtEMIInstallments_TextChanged(txtEMIInstallments, New System.EventArgs)

            txtPlotNo.Text = objProcesspendingloan._PlotNo
            txtBlockNo.Text = objProcesspendingloan._BlockNo
            txtStreet.Text = objProcesspendingloan._Street
            txtTownCity.Text = objProcesspendingloan._TownCity
            txtMarketValue.Text = Format(objProcesspendingloan._MarketValue, Session("fmtCurrency").ToString)
            txtCTNo.Text = objProcesspendingloan._CTNo
            txtLONo.Text = objProcesspendingloan._LONo
            txtFSV.Text = Format(objProcesspendingloan._FSV, Session("fmtCurrency").ToString)

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            txtBOQ.Text = objProcesspendingloan._BillOfQty.ToString()
            'Pinkal (17-May-2024) -- End


            txtModel.Text = objProcesspendingloan._Model
            txtYOM.Text = objProcesspendingloan._YOM

            chkMakeExceptionalApplication.Checked = CBool(objProcesspendingloan._IsExceptionalApplication)

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-998 - As a user, I want to have additional fields on application and approval pages where loan category = Secured. The below are the fields and they should be mandatory(Chassis number,Supplier,Colour)
            txtChassisNo.Text = objProcesspendingloan._ChassisNo
            txtSupplier.Text = objProcesspendingloan._Supplier
            txtColour.Text = objProcesspendingloan._Colour
            'Hemant (27 Oct 2022) -- End

            'Hemant (29 Mar 2024) -- Start
            'ENHANCEMENT(TADB): New loan application form
            txtTotalCIF.Text = Format(objProcesspendingloan._TotalCIF, Session("fmtCurrency").ToString)
            txtEstimatedTax.Text = Format(objProcesspendingloan._EstimatedTax, Session("fmtCurrency").ToString)
            txtInvoiceValue.Text = Format(objProcesspendingloan._InvoiceValue, Session("fmtCurrency").ToString)
            txtModelNo.Text = objProcesspendingloan._ModelNo
            txtEngineNo.Text = objProcesspendingloan._EngineNo
            txtEngineCapacity.Text = Format(objProcesspendingloan._EngineCapacity, Session("fmtCurrency").ToString)
            'Hemant (29 Mar 2024) -- End

            'Hemant (18 Mar 2024) -- Start
            'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
            txtRepaymentDays.Text = CStr(objProcesspendingloan._RepaymentDays)
            'Hemant (18 Mar 2024) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            chkSalaryAdvanceLiquidation.Checked = CBool(objProcesspendingloan._IsLiquidate)
            Call chkSalaryAdvanceLiquidation_CheckedChanged(chkSalaryAdvanceLiquidation, New System.EventArgs())
            'Hemant (22 Nov 2024) -- End

            'Hemant (07 Jul 2023) -- Start
            'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
            dtpTitleIssueDate.SetDate = CDate(objProcesspendingloan._TitleIssueDate)
            nudTitleValidity.Text = CStr(objProcesspendingloan._TitleValidity)
            dtpTitleExpiryDate.SetDate = CDate(objProcesspendingloan._TitleExpiryDate)
            'Hemant (07 July 2023) -- End

            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmpName.SelectedValue)
            If objEmployee._EmpSignature IsNot Nothing Then
                imgSignature.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=3"
            End If
            'Hemant (07 Jul 2023) -- Start
            'Enhancement : NMB : Show the remaining number of pay periods & Dates to the end of applicant's contract, Retirement on loan application and approval screens
            'Dim dtEOCDate As Date = Nothing
            'If objEmployee._Termination_To_Date <> Nothing AndAlso IsDBNull(objEmployee._Termination_To_Date) = False Then
            '    dtEOCDate = CDate(objEmployee._Termination_To_Date)
            '    lblEmplDate.Text = "Till Retr. Date"
            'End If

            'If objEmployee._Empl_Enddate <> Nothing AndAlso IsDBNull(objEmployee._Empl_Enddate) = False Then
            '    If dtEOCDate < CDate(objEmployee._Empl_Enddate) Then
            '    Else
            '        dtEOCDate = CDate(objEmployee._Empl_Enddate)
            '    End If
            '    lblEmplDate.Text = "Till EOC Date"
            'End If
            'If dtEOCDate <> Nothing Then
            '    dtpEndEmplDate.SetDate = dtEOCDate
            '    nudPayPeriodEOC.Text = CStr(Math.Abs((mdtPeriodStart.Month - dtEOCDate.Month) + 12 * (mdtPeriodStart.Year - dtEOCDate.Year)) + 1)
            '    lblPayPeriodsEOC.Visible = True
            '    nudPayPeriodEOC.Visible = True
            '    lblEmplDate.Visible = True
            '    dtpEndEmplDate.Visible = True
            'Else
            '    lblPayPeriodsEOC.Visible = False
            '    nudPayPeriodEOC.Visible = False
            '    lblEmplDate.Visible = False
            '    dtpEndEmplDate.Visible = False
            'End If
            'Hemant (07 July 2023) -- End
            objEmployee = Nothing

            Call txtEMIInstallments_TextChanged(txtEMIInstallments, New System.EventArgs)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue()
        Try

            objProcesspendingloan._Isloan = True
            If mintProcesspendingloanunkid > 0 Then
                objProcesspendingloan._Processpendingloanunkid = mintProcesspendingloanunkid
            End If

            'objProcesspendingloan._Application_No = txtApplicationNo.Text
            objProcesspendingloan._Application_Date = mdtApplcationDate
            objProcesspendingloan._Employeeunkid = CInt(cboEmpName.SelectedValue)
            objProcesspendingloan._Loan_Amount = CDec(txtLoanAmt.Text)
            objProcesspendingloan._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            objProcesspendingloan._Countryunkid = CInt(cboCurrency.SelectedValue)
            objProcesspendingloan._DeductionPeriodunkid = mintDeductionPeriodUnkId
            objProcesspendingloan._Emp_Remark = txtPurposeOfCredit.Text

            objProcesspendingloan._InstallmentAmount = CDec(txtInstallmentAmt.Text)
            objProcesspendingloan._NoOfInstallment = CInt(txtEMIInstallments.Text)


            objProcesspendingloan._Loan_Statusunkid = 1
            objProcesspendingloan._Userunkid = CInt(Session("UserId").ToString)
            objProcesspendingloan._Isvoid = False
            objProcesspendingloan._Voiddatetime = Nothing
            objProcesspendingloan._Voiduserunkid = -1
            objProcesspendingloan._IsLoanApprover_ForLoanScheme = CBool(Session("LoanApprover_ForLoanScheme"))
            Blank_ModuleName()
            StrModuleName2 = "mnuLoan_Advance_Savings"
            objProcesspendingloan._WebClientIP = Session("IP_ADD").ToString
            objProcesspendingloan._WebFormName = "frmLoanApplication_AddEdit"
            objProcesspendingloan._WebHostName = Session("HOST_NAME").ToString
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objProcesspendingloan._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objProcesspendingloan._PlotNo = ""
            objProcesspendingloan._BlockNo = ""
            objProcesspendingloan._Street = ""
            objProcesspendingloan._TownCity = ""
            objProcesspendingloan._MarketValue = CDec(0)
            objProcesspendingloan._CTNo = ""
            objProcesspendingloan._LONo = ""
            objProcesspendingloan._FSV = CDec(0)
            objProcesspendingloan._Model = ""
            objProcesspendingloan._YOM = ""
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-998 - As a user, I want to have additional fields on application and approval pages where loan category = Secured. The below are the fields and they should be mandatory(Chassis number,Supplier,Colour)
            objProcesspendingloan._ChassisNo = ""
            objProcesspendingloan._Supplier = ""
            objProcesspendingloan._Colour = ""
            objProcesspendingloan._IsFlexcube = True
            'Hemant (27 Oct 2022) -- End
            'Hemant (29 Mar 2024) -- Start
            'ENHANCEMENT(TADB): New loan application form
            objProcesspendingloan._TotalCIF = CDec(0)
            objProcesspendingloan._EstimatedTax = CDec(0)
            objProcesspendingloan._InvoiceValue = CDec(0)
            objProcesspendingloan._ModelNo = ""
            objProcesspendingloan._EngineNo = ""
            objProcesspendingloan._EngineCapacity = CDec(0)
            'Hemant (29 Mar 2024) -- End
            'Hemant (07 Jul 2023) -- Start
            'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
            objProcesspendingloan._TitleIssueDate = Nothing
            objProcesspendingloan._TitleValidity = 0
            objProcesspendingloan._TitleExpiryDate = Nothing
            'Hemant (07 July 2023) -- End
            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then
                objProcesspendingloan._PlotNo = txtPlotNo.Text
                objProcesspendingloan._BlockNo = txtBlockNo.Text
                objProcesspendingloan._Street = txtStreet.Text
                objProcesspendingloan._TownCity = txtTownCity.Text
                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                objProcesspendingloan._MarketValue = CDec(IIf(txtMarketValue.Text.Trim.Length <= 0, 0, txtMarketValue.Text.Trim))
                'Pinkal (17-May-2024) -- End
                objProcesspendingloan._CTNo = txtCTNo.Text
                objProcesspendingloan._LONo = txtLONo.Text

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                objProcesspendingloan._FSV = CDec(IIf(txtFSV.Text.Trim.Length <= 0, 0, txtFSV.Text.Trim))
                'Pinkal (17-May-2024) -- End

                'Hemant (07 Jul 2023) -- Start
                'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
                objProcesspendingloan._TitleIssueDate = CDate(dtpTitleIssueDate.GetDate)
                objProcesspendingloan._TitleValidity = CInt(nudTitleValidity.Text)
                objProcesspendingloan._TitleExpiryDate = CDate(dtpTitleExpiryDate.GetDate)
                'Hemant (07 July 2023) -- End
            ElseIf CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.SECURED Then
                objProcesspendingloan._Model = txtModel.Text
                objProcesspendingloan._YOM = txtYOM.Text
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-998 - As a user, I want to have additional fields on application and approval pages where loan category = Secured. The below are the fields and they should be mandatory(Chassis number,Supplier,Colour)
                objProcesspendingloan._ChassisNo = txtChassisNo.Text
                objProcesspendingloan._Supplier = txtSupplier.Text
                objProcesspendingloan._Colour = txtColour.Text
                'Hemant (27 Oct 2022) -- End
                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                objProcesspendingloan._TotalCIF = CDec(txtTotalCIF.Text)
                objProcesspendingloan._EstimatedTax = CDec(txtEstimatedTax.Text)
                objProcesspendingloan._InvoiceValue = CDec(txtInvoiceValue.Text)
                objProcesspendingloan._ModelNo = txtModelNo.Text
                objProcesspendingloan._EngineNo = txtEngineNo.Text
                objProcesspendingloan._EngineCapacity = CDec(txtEngineCapacity.Text)
                'Hemant (29 Mar 2024) -- End
            End If

            objProcesspendingloan._IsExceptionalApplication = chkMakeExceptionalApplication.Checked

            'Pinkal (20-Sep-2022) -- Start
            'NMB Loan Module Enhancement.
            objProcesspendingloan._RequiredReportingToApproval = mblnRequiredReportingToApproval
            'Pinkal (20-Sep-2022) -- End

            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            objProcesspendingloan._SkipApproverFlow = mblnSkipApprovalFlow
            'Pinkal (12-Oct-2022) -- End

            'Hemant (12 Oct 2022) -- Start 
            'ENHANCEMENT(NMB) :  AC2-949 - As a user, on the application screen, if the loan scheme selected is eligible for top-up, my Take home should be calculated as Principal Amount – (Outstanding Principle + Outstanding Interest + Insurance Amount)
            objProcesspendingloan._OutstandingPrincipalAmount = CDec(txtOutStandingPrincipalAmt.Text)
            objProcesspendingloan._OutstandingInterestAmount = CDec(txtOutStandingInterestAmt.Text)
            objProcesspendingloan._NoOfInstallmentPaid = CInt(txtInstallmentPaid.Text)
            If CDec(txtOutStandingPrincipalAmt.Text) > 0 OrElse CDec(txtOutStandingInterestAmt.Text) > 0 Then
                objProcesspendingloan._IsTopUp = True
            Else
                objProcesspendingloan._IsTopUp = False
            End If

            objProcesspendingloan._TakeHomeAmount = CDec(txtTakeHome.Text)
            'Hemant (12 Oct 2022) -- End

            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objProcesspendingloan._LoanApproval_DailyReminder = mblnLoanApproval_DailyReminder
            objProcesspendingloan._IssubmitApproval = True
            objProcesspendingloan._EscalationDays = mintEscalationDays
            'Pinkal (10-Nov-2022) -- End

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objProcesspendingloan._OtherLoanOutstandingPrincipalAmount = mdecOtherLoanOutStandingPrincipalAmt
            objProcesspendingloan._PostedData = ""
            objProcesspendingloan._ReponseData = ""
            objProcesspendingloan._IsPostedError = False
            'Pinkal (23-Nov-2022) -- End

            'Hemant (25 Nov 2022) -- Start
            'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
            objProcesspendingloan._InterestRate = CDec(txtLoanRate.Text)
            'Hemant (25 Nov 2022) -- End

            'Pinkal (21-Jul-2023) -- Start
            '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
            objProcesspendingloan._NTFsendtitleexpiryemp = False
            objProcesspendingloan._NTFsendtitleexpiryusers = False
            'Pinkal (21-Jul-2023) -- End

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objProcesspendingloan._SendEmpReminderForNextTrance = Nothing
            objProcesspendingloan._NtfSendEmpForNextTranche = False
            'Pinkal (04-Aug-2023) -- End

            'Pinkal (11-Mar-2024) -- Start
            '(A1X-2505) NMB - Credit card integration.
            objProcesspendingloan._CreditCardAmountOnExposure = mdecCreditCardAmountOnExposure
            'Pinkal (11-Mar-2024) -- End
            'Hemant (18 Mar 2024) -- Start
            'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
            objProcesspendingloan._RepaymentDays = CInt(txtRepaymentDays.Text)
            'Hemant (18 Mar 2024) -- End

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            objProcesspendingloan._BillOfQty = txtBOQ.Text.Trim
            'Pinkal (17-May-2024) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            If chkSalaryAdvanceLiquidation.Visible = True Then
                objProcesspendingloan._IsLiquidate = CBool(chkSalaryAdvanceLiquidation.Checked)
            Else
                objProcesspendingloan._IsLiquidate = False
            End If
            objProcesspendingloan._SalaryAdvancePrincipalAmount = mdecFinalSalaryAdvancePendingPrincipalAmt
            'Hemant (22 Nov 2024) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Calculate_Projected_Loan_Principal(ByVal eLnCalcType As enLoanCalcId _
                                                 , ByVal eIntRateCalcTypeID As enLoanInterestCalcType _
                                                 , Optional ByVal blnIsDurationChange As Boolean = False _
                                                 )
        Try
            Dim objLoan_Advance As New clsLoan_Advance
            Dim decPrincipalAmount As Decimal = 0
            Dim dtEndDate As Date = DateAdd(DateInterval.Month, CInt(txtEMIInstallments.Text), Now.Date)
            Dim dtFPeriodStart As Date = dtEndDate
            Dim dtFPeriodEnd As Date = dtEndDate
            If mintDeductionPeriodUnkId > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = mintDeductionPeriodUnkId
                dtFPeriodStart = objPeriod._Start_Date
                dtFPeriodEnd = objPeriod._End_Date
            End If
            objLoan_Advance.Calulate_Projected_Loan_Principal(CDec(txtMaxLoanAmount.Text) _
                                                            , CDec(txtMaxInstallmentAmt.Text) _
                                                            , CInt(DateDiff(DateInterval.Day, Now.Date, dtEndDate.Date)) _
                                                            , CDec(txtLoanRate.Text) _
                                                            , eLnCalcType _
                                                            , eIntRateCalcTypeID _
                                                            , CInt(txtMaxInstallment.Text) _
                                                            , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
                                                            , Convert.ToDecimal(txtInstallmentAmt.Text) _
                                                            , decInstrAmount _
                                                            , decPrincipalAmount _
                                                            , decTotIntrstAmount _
                                                            , decTotInstallmentAmount _
                                                            )

            'RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            'txtInstallmentAmt.Text = Format(decInstallmentAmount, Session("fmtCurrency").ToString)
            'AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            RemoveHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            txtAppliedUptoPrincipalAmt.Text = Format(decInstallmentAmount - decInstrAmount, Session("fmtCurrency").ToString)
            If decPrincipalAmount > CDec(txtMaxLoanAmount.Text) Then
                txtAppliedUptoPrincipalAmt.Text = Format(CDec(txtMaxLoanAmount.Text), Session("fmtCurrency").ToString)
            Else
                If Math.Abs(decPrincipalAmount - CDec(txtMaxLoanAmount.Text)) > 2 Then
                    txtAppliedUptoPrincipalAmt.Text = Format(decPrincipalAmount, Session("fmtCurrency").ToString)
                End If
            End If
            AddHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged

            RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            If decPrincipalAmount > CDec(txtMaxLoanAmount.Text) Then
                txtLoanAmt.Text = Format(CDec(txtMaxLoanAmount.Text), Session("fmtCurrency").ToString)
            Else
                If Math.Abs(decPrincipalAmount - CDec(txtMaxLoanAmount.Text)) > 2 Then
                    txtLoanAmt.Text = Format(decPrincipalAmount, Session("fmtCurrency").ToString)
                End If
            End If
            If Math.Abs(decPrincipalAmount - CDec(txtMaxLoanAmount.Text)) > 2 Then
                Call txtLoanAmt_TextChanged(txtLoanAmt, New System.EventArgs)
            End If
            AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged

            txtIntAmt.Text = Format(decInstrAmount, Session("fmtCurrency").ToString)

            objLoan_Advance = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Calculate_Projected_Loan_Balance(ByVal eLnCalcType As enLoanCalcId _
                                                 , ByVal eIntRateCalcTypeID As enLoanInterestCalcType _
                                                 , Optional ByVal blnIsDurationChange As Boolean = False _
                                                 )
        Try
            Dim objLoan_Advance As New clsLoan_Advance
            Dim dtEndDate As Date = DateAdd(DateInterval.Month, CInt(txtEMIInstallments.Text), Now.Date)
            Dim dtFPeriodStart As Date = dtEndDate
            Dim dtFPeriodEnd As Date = dtEndDate
            If mintDeductionPeriodUnkId > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = mintDeductionPeriodUnkId
                dtFPeriodStart = objPeriod._Start_Date
                dtFPeriodEnd = objPeriod._End_Date
            End If
            objLoan_Advance.Calulate_Projected_Loan_Balance(CDec(txtLoanAmt.Text) _
                                                            , CInt(DateDiff(DateInterval.Day, Now.Date, dtEndDate.Date)) _
                                                            , CDec(txtLoanRate.Text) _
                                                            , eLnCalcType _
                                                            , eIntRateCalcTypeID _
                                                            , CInt(txtEMIInstallments.Text) _
                                                            , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
                                                            , Convert.ToDecimal(txtInstallmentAmt.Text) _
                                                            , decInstrAmount _
                                                            , decInstallmentAmount _
                                                            , decTotIntrstAmount _
                                                            , decTotInstallmentAmount _
                                                            )

            RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            txtInstallmentAmt.Text = Format(decInstallmentAmount, Session("fmtCurrency").ToString)
            AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            txtIntAmt.Text = Format(decInstrAmount, Session("fmtCurrency").ToString)
            objLoan_Advance = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillDocumentTypeCombo()
        Dim objCommonMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Dim dt As New DataTable
        Dim strFilter As String = String.Empty
        Try
            dsCombos = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            If mstrDocumentTypeIDs.Trim.Length > 0 Then
                strFilter = "masterunkid in ( 0," & mstrDocumentTypeIDs & " )"
            Else
                strFilter = "masterunkid in ( 0 )"
            End If
            Dim drRow() As DataRow = dsCombos.Tables(0).Select(strFilter)
            If drRow.Length > 0 Then
                dt = drRow.CopyToDataTable
            End If
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dt
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDocumentTypeCombo", mstrModuleName)
        Finally
            objCommonMaster = Nothing
        End Try
    End Sub

    Private Sub FillLoanApplicationAttachment()
        Dim dtView As DataView
        Try
            If mdtLoanApplicationDocument Is Nothing Then
                dgvQualification.DataSource = Nothing
                dgvQualification.DataBind()
                Exit Sub
            End If

            dtView = New DataView(mdtLoanApplicationDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvQualification.AutoGenerateColumns = False
            dgvQualification.DataSource = dtView
            dgvQualification.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtLoanApplicationDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtLoanApplicationDocument.NewRow
                dRow("scanattachtranunkid") = -1

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'dRow("employeeunkid") = -1
                dRow("employeeunkid") = CInt(cboEmpName.SelectedValue)
                'Pinkal (04-Aug-2023) -- End

                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Loan_Balance
                dRow("scanattachrefid") = enScanAttactRefId.LOAN_ADVANCE
                dRow("form_name") = mstrModuleName
                dRow("transactionunkid") = mintProcesspendingloanunkid

                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = DateTime.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                dRow("attachment_type") = cboDocumentType.SelectedItem.Text
                'Pinkal (17-May-2024) -- End



                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                'dRow("file_data") = xDocumentData
                Dim bytes() As Byte
                bytes = Encoding.ASCII.GetBytes(strfullpath)
                Dim xDocumentData As Byte() = bytes
                Try
                    dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Catch ex As FormatException
                    CommonCodes.LogErrorOnly(ex)
                End Try
                Dim objFile As New FileInfo(strfullpath)
                dRow("fileextension") = objFile.Extension.ToString().Replace(".", "")
                objFile = Nothing
                dRow("file_data") = xDocumentData
                'Pinkal (04-Aug-2023) -- End
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 32, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtLoanApplicationDocument.Rows.Add(dRow)

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            Session("mdtLoanApplicationDocument") = mdtLoanApplicationDocument
            'Pinkal (04-Aug-2023) -- End

            Call FillLoanApplicationAttachment()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
    Private Sub FillOtherDocumentTypeCombo()
        Dim objCommonMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Dim dt As New DataTable
        Dim strFilter As String = String.Empty
        Try
            dsCombos = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            'If mintOtherDocumentunkid > 0 Then
            '    strFilter = "masterunkid in ( 0," & mintOtherDocumentunkid.ToString & " )"
            'Else
            '    strFilter = "masterunkid in ( 0 )"
            'End If
            If mstrOtherDocumentIds.Trim.Length > 0 Then
                strFilter = "masterunkid in ( 0," & mstrOtherDocumentIds.Trim & " )"
            Else
                strFilter = "masterunkid in ( 0 )"
            End If
            'Pinkal (23-Nov-2022) -- End


            Dim drRow() As DataRow = dsCombos.Tables(0).Select(strFilter)
            If drRow.Length > 0 Then
                dt = drRow.CopyToDataTable
            End If
            With cboOtherDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dt
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillOtherDocumentTypeCombo", mstrModuleName)
        Finally
            objCommonMaster = Nothing
        End Try
    End Sub

    Private Sub FillLoanApplicationOtherAttachment()
        Dim dtView As DataView
        Try
            If mdtOtherLoanApplicationDocument Is Nothing Then
                dgvOtherQualification.DataSource = Nothing
                dgvOtherQualification.DataBind()
                Exit Sub
            End If

            dtView = New DataView(mdtOtherLoanApplicationDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvOtherQualification.AutoGenerateColumns = False
            dgvOtherQualification.DataSource = dtView
            dgvOtherQualification.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddDocumentOtherAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            'Pinkal (27-Sep-2024) -- Start
            'NMB Enhancement : (A1X-2787) NMB - Credit report development.
            If mdtOtherLoanApplicationDocument Is Nothing OrElse IsDBNull(mdtOtherLoanApplicationDocument) Then Exit Sub
            'Pinkal (27-Sep-2024) -- End

            Dim dtRow() As DataRow = mdtOtherLoanApplicationDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtOtherLoanApplicationDocument.NewRow
                dRow("scanattachtranunkid") = -1

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'dRow("employeeunkid") = -1
                dRow("employeeunkid") = CInt(cboEmpName.SelectedValue)
                'Pinkal (04-Aug-2023) -- End

                dRow("documentunkid") = CInt(cboOtherDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Loan_Balance
                dRow("scanattachrefid") = enScanAttactRefId.LOAN_ADVANCE
                dRow("form_name") = mstrModuleName
                dRow("transactionunkid") = mintProcesspendingloanunkid

                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = DateTime.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024


                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                dRow("attachment_type") = cboOtherDocumentType.SelectedItem.Text
                'Pinkal (17-May-2024) -- End

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                'dRow("file_data") = xDocumentData
                Dim bytes() As Byte
                bytes = Encoding.ASCII.GetBytes(strfullpath)
                Dim xDocumentData As Byte() = bytes
                Try
                    dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Catch ex As FormatException
                    CommonCodes.LogErrorOnly(ex)
                End Try
                Dim objFile As New FileInfo(strfullpath)
                dRow("fileextension") = objFile.Extension.ToString().Replace(".", "")
                objFile = Nothing
                dRow("file_data") = xDocumentData
                'Pinkal (04-Aug-2023) -- End
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 32, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtOtherLoanApplicationDocument.Rows.Add(dRow)

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            Session("mdtOtherLoanApplicationDocument") = mdtOtherLoanApplicationDocument
            'Pinkal (04-Aug-2023) -- End

            Call FillLoanApplicationOtherAttachment()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (27 Oct 2022) -- End


    'Hemant (25 Nov 2022) -- Start
    'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
    Private Sub GetEmployeeHistoryData(ByVal intEmployeeId As Integer, ByRef intCategorizationTranunkid As Integer, ByRef intTransferunkid As Integer, ByRef intAtEmployeeunkid As Integer)
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim objETaransfer As New clsemployee_transfer_tran
        Dim objEmployee As New clsEmployee_Master
        Dim dsCategorize As New DataSet
        Dim dsTransfer As New DataSet
        Dim dsMovement As New DataSet
        Try

            dsCategorize = objCategorize.Get_Current_Job(ConfigParameter._Object._CurrentDateAndTime.Date, intEmployeeId)
            If dsCategorize.Tables(0).Rows.Count > 0 Then
                intCategorizationTranunkid = CInt(dsCategorize.Tables(0).Rows(0).Item("categorizationtranunkid"))
            Else
                intCategorizationTranunkid = -1
            End If
            dsTransfer = objETaransfer.Get_Current_Allocation(ConfigParameter._Object._CurrentDateAndTime.Date, intEmployeeId)
            If dsTransfer.Tables(0).Rows.Count > 0 Then
                intTransferunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("transferunkid"))
            Else
                intTransferunkid = -1
            End If

            Dim dtMovement As New DataTable
            dsMovement = objEmployee.GetMovement_Log(intEmployeeId, "Log")
            dtMovement = dsMovement.Tables(0).Copy
            dtMovement.DefaultView.Sort = "Date DESC, atemployeeunkid DESC"
            dtMovement = dtMovement.DefaultView.ToTable()
            If dtMovement.Rows.Count > 0 Then
                intAtEmployeeunkid = CInt(dtMovement.Rows(0).Item("atemployeeunkid"))
            Else
                intAtEmployeeunkid = -1
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCategorize = Nothing
            objETaransfer = Nothing
            objEmployee = Nothing
        End Try
    End Sub
    'Hemant (25 Nov 2022) -- End

    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .
    Private Sub Save()
        Try
            Dim btnFlag As Boolean = False

            Call SetValue()

            If mblnIsAttachmentRequired = True Then
                If mdtLoanApplicationDocument Is Nothing OrElse mdtLoanApplicationDocument.Select("AUD <> 'D'").Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), Me)
                    Exit Sub
                End If

                Dim arrDocumentTypeID() As String = mstrDocumentTypeIDs.Split(CChar(","))
                For Each intDocumentTypeID As Integer In arrDocumentTypeID
                    'Pinkal (17-May-2024) -- Start
                    'NMB Enhancement For Mortgage Loan.
                    Dim mblnOptionalAttachmentForFlexcubeLoan As Boolean = False
                    If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then
                        Dim objCommon As New clsCommon_Master
                        objCommon._Masterunkid = intDocumentTypeID
                        mblnOptionalAttachmentForFlexcubeLoan = objCommon._OptionalAttachmentForFlexcubeLoanApplication
                        objCommon = Nothing
                    End If

                    If mblnOptionalAttachmentForFlexcubeLoan = False Then
                    Dim drDocumentRow() As DataRow = mdtLoanApplicationDocument.Select("AUD <> 'D' AND documentunkid = " & intDocumentTypeID & "")
                    If drDocumentRow.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 38, "All Scan/Document(s) are mandatory information. Please attach all Scan/Document(s) in order to perform operation."), Me)
                        Exit Sub
                    End If
                    End If
                    'Pinkal (17-May-2024) -- End
                Next
            End If

            If chkMakeExceptionalApplication.Checked = True AndAlso mstrOtherDocumentIds.Trim.Length > 0 Then
                If mdtOtherLoanApplicationDocument Is Nothing OrElse mdtOtherLoanApplicationDocument.Select("AUD <> 'D'").Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation."), Me)
                    Exit Sub
                End If
            End If

            Dim intCategorizationTranunkid As Integer
            Dim intTransferunkid As Integer
            Dim intAtEmployeeunkid As Integer
            GetEmployeeHistoryData(CInt(cboEmpName.SelectedValue), intCategorizationTranunkid, intTransferunkid, intAtEmployeeunkid)
            objProcesspendingloan._CategorizationTranunkid = intCategorizationTranunkid
            objProcesspendingloan._Transferunkid = intTransferunkid
            objProcesspendingloan._AtEmployeeunkid = intAtEmployeeunkid

            'Pinkal (27-Sep-2024) -- Start
            'NMB Enhancement : (A1X-2787) NMB - Credit report development.
            If mdtFinalLoanApplicationDocument IsNot Nothing Then
            mdtFinalLoanApplicationDocument.Rows.Clear()
                If mdtLoanApplicationDocument IsNot Nothing Then mdtFinalLoanApplicationDocument.Merge(mdtLoanApplicationDocument)
                If mdtOtherLoanApplicationDocument IsNot Nothing Then mdtFinalLoanApplicationDocument.Merge(mdtOtherLoanApplicationDocument)
            End If
            'Pinkal (27-Sep-2024) -- End


            Dim mdsDoc As DataSet
            Dim mstrFolderName As String = ""
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim strFileName As String = ""
            For Each dRowAttachment As DataRow In mdtFinalLoanApplicationDocument.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRowAttachment("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault

                If dRowAttachment("AUD").ToString = "A" AndAlso dRowAttachment("localpath").ToString <> "" Then
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRowAttachment("localpath")))
                    If File.Exists(CStr(dRowAttachment("localpath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(dRowAttachment("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRowAttachment("fileuniquename") = strFileName
                        dRowAttachment("filepath") = strPath
                        dRowAttachment.AcceptChanges()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If
                ElseIf dRowAttachment("AUD").ToString = "D" AndAlso dRowAttachment("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRowAttachment("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "File does not exist on localpath"), Me)
                            Exit Sub
                        End If
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If
                End If

            Next

            If mintProcesspendingloanunkid > 0 Then
                btnFlag = objProcesspendingloan.Update(Session("Database_Name").ToString, CInt(Session("UserId")) _
                                                                           , CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                           , mdtPeriodStart, mdtPeriodEnd, CStr(Session("UserAccessModeSetting")) _
                                                                           , True, CBool(Session("IsIncludeInactiveEmp")), True, Nothing, mdtFinalLoanApplicationDocument)

            Else
                btnFlag = objProcesspendingloan.Insert(Session("Database_Name").ToString, _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       CInt(Session("LoanApplicationNoType")), _
                                                       CStr(Session("LoanApplicationPrifix")), _
                                                       , mdtFinalLoanApplicationDocument)
            End If

            If btnFlag = False And objProcesspendingloan._Message <> "" Then
                DisplayMessage.DisplayMessage(objProcesspendingloan._Message, Me)
            Else
                Dim enLoginMode As New enLogin_Mode
                Dim intLoginByEmployeeId As Integer = 0
                Dim intUserId As Integer = 0

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    enLoginMode = enLogin_Mode.EMP_SELF_SERVICE
                    intLoginByEmployeeId = CInt(Session("Employeeunkid"))
                    intUserId = 0
                Else
                    enLoginMode = enLogin_Mode.MGR_SELF_SERVICE
                    intUserId = CInt(Session("UserId"))
                    intLoginByEmployeeId = 0
                End If

                If mblnSkipApprovalFlow = False Then
                    objProcesspendingloan.Send_NotificationFlexCube_Employee(CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid, objProcesspendingloan._Application_No, _
                                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("CompanyUnkId")), cboLoanScheme.SelectedItem.Text, _
                                                                                                objProcesspendingloan._Loan_Statusunkid, "", enLoginMode, intLoginByEmployeeId, intUserId, True, Nothing)
                    'Pinkal (21-Mar-2024) -- Start
                    'NMB - Mortgage UAT Enhancements.
                    'objProcesspendingloan.Send_NotificationFlexCube_Approver(Session("Database_Name").ToString, CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, objProcesspendingloan._Application_Date.Date, _
                    '                                                                      CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid.ToString(), _
                    '                                                                      Session("ArutiSelfServiceURL").ToString(), CInt(Session("CompanyUnkId")), enLoginMode, intLoginByEmployeeId, intUserId, True, Nothing)

                    objProcesspendingloan.Send_NotificationFlexCube_Approver(Session("Database_Name").ToString, CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, objProcesspendingloan._Application_Date.Date, _
                                                                                          CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid.ToString(), _
                                                                      Session("ArutiSelfServiceURL").ToString(), CInt(Session("CompanyUnkId")), objProcesspendingloan._Loan_Statusunkid, enLoginMode, intLoginByEmployeeId, intUserId, True, Nothing, -999, "")

                    'Pinkal (21-Mar-2024) -- End

                End If
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 54, "Loan Application Submitted successfully."), Me, Convert.ToString(Session("rootpath")) & "Loan_Savings/New_Loan/Loan_Application/wPg_LoanApplicationList.aspx")

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                If mdtLoanApplicationDocument IsNot Nothing Then mdtLoanApplicationDocument.Clear()
                mdtLoanApplicationDocument = Nothing
                Session("mdtLoanApplicationDocument") = Nothing
                Session.Remove("mdtLoanApplicationDocument")

                If mdtOtherLoanApplicationDocument IsNot Nothing Then mdtOtherLoanApplicationDocument.Clear()
                mdtOtherLoanApplicationDocument = Nothing
                Session("mdtOtherLoanApplicationDocument") = Nothing
                Session.Remove("mdtOtherLoanApplicationDocument")

                If mdtFinalLoanApplicationDocument IsNot Nothing Then mdtFinalLoanApplicationDocument.Clear()
                mdtFinalLoanApplicationDocument = Nothing
                Session("mdtFinalLoanApplicationDocument") = Nothing
                Session.Remove("mdtFinalLoanApplicationDocument")
                'Pinkal (04-Aug-2023) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SendOTP(ByVal mstrTOTPCode As String) As String
        Dim objLoan As New clsProcess_pending_loan
        Dim objSendMail As New clsSendMail
        Dim mstrError As String = ""
        Try
            objSendMail._ToEmail = Session("SMSGatewayEmail").ToString()
            'objSendMail._ToEmail = mstrEmployeeEmail
            objSendMail._Subject = mstrEmpMobileNo
            objSendMail._Form_Name = mstrModuleName

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objSendMail._LogEmployeeUnkid = CInt(Session("Employeeunkid"))
                objSendMail._UserUnkid = -1
                objSendMail._OperationModeId = enLogin_Mode.EMP_SELF_SERVICE
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objSendMail._LogEmployeeUnkid = -1
                objSendMail._UserUnkid = CInt(Session("UserId"))
                objSendMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
            End If

            objSendMail._SenderAddress = CStr(IIf(mstrEmployeeEmail.Trim.Length <= 0, mstrEmployeeName, mstrEmployeeEmail.Trim))
            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
            objSendMail._Message = objLoan.SetSMSForLoanApplication(mstrEmployeeName, mstrTOTPCode, Counter, False)
            mstrError = objSendMail.SendMail(CInt(Session("CompanyUnkId")))
        Catch ex As Exception
            mstrError = ex.Message.ToString()
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objSendMail = Nothing
            objLoan = Nothing
        End Try
        Return mstrError
    End Function

    Private Sub InsertAttempts(ByRef mblnprompt As Boolean)
        Dim objAttempts As New clsloantotpattempts_tran
        Dim objConfig As New clsConfigOptions
        Try
            Dim xAttempts As Integer = 0
            Dim xAttemptsunkid As Integer = 0
            Dim xOTPRetrivaltime As DateTime = Nothing
            Dim dsAttempts As DataSet = Nothing

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, CInt(Session("UserId")), 0)
            ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, 0, CInt(Session("Employeeunkid")))
            End If

            If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                xAttempts = CInt(dsAttempts.Tables(0).Rows(0)("attempts"))
                xAttemptsunkid = CInt(dsAttempts.Tables(0).Rows(0)("attemptsunkid"))

                If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                    xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))

                    If xOTPRetrivaltime > objConfig._CurrentDateAndTime Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 62, "You cannot save this loan application.Reason :  As You reached Maximum unsuccessful attempts.Please try after") & " " & CInt(Session("TOTPRetryAfterMins")) & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 63, "mintues") & ".", Me)
                        objAttempts = Nothing
                        mblnprompt = True
                        btnVerify.Enabled = False
                        LblSeconds.Text = "00:00"
                        LblSeconds.Visible = False
                        blnShowTOTPPopup = False
                        popup_TOTP.Hide()
                        Exit Sub
                    End If
                End If

                If xAttempts >= CInt(Session("MaxTOTPUnSuccessfulAttempts")) Then
                    xAttempts = 1
                    xAttemptsunkid = 0
                Else
                    xAttempts = xAttempts + 1
                End If
            Else
                xAttempts = 1
            End If


            objAttempts._Attemptsunkid = xAttemptsunkid
            objAttempts._Attempts = xAttempts
            objAttempts._Attemptdate = objConfig._CurrentDateAndTime
            objAttempts._Otpretrivaltime = Nothing

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                objAttempts._loginemployeeunkid = -1
                objAttempts._Userunkid = CInt(Session("UserId"))
            ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                objAttempts._loginemployeeunkid = CInt(Session("Employeeunkid"))
                objAttempts._Userunkid = -1
            End If

            objAttempts._Ip = Session("IP_ADD").ToString
            objAttempts._Machine_Name = Session("HOST_NAME").ToString
            objAttempts._FormName = mstrModuleName
            objAttempts._Isweb = True

            If xAttemptsunkid <= 0 Then
                If objAttempts.Insert() = False Then
                    DisplayMessage.DisplayMessage(objAttempts._Message, Me)
                    Exit Sub
                End If
            Else
                If xAttempts >= CInt(Session("MaxTOTPUnSuccessfulAttempts")) Then
                    objAttempts._Otpretrivaltime = objConfig._CurrentDateAndTime.AddMinutes(CInt(Session("TOTPRetryAfterMins")))
                    blnShowTOTPPopup = False
                    popup_TOTP.Hide()
                End If
                If objAttempts.Update() = False Then
                    DisplayMessage.DisplayMessage(objAttempts._Message, Me)
                    Exit Sub
                End If
              
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objConfig = Nothing
            objAttempts = Nothing
        End Try
    End Sub

    'Pinkal (02-Jun-2023) -- End
    'Hemant (07 Jul 2023) -- Start
    'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
    Private Function IsNotMortgageLoanExist() As Boolean
        Dim objEmpBankTran As New clsEmployeeBanks
        Dim objloanAdvance As New clsLoan_Advance
        Dim objLoanScheme As New clsLoan_Scheme
        Try
            Dim dsEmpBank As New DataSet
            Dim xPrevPeriodStart As Date
            Dim xPrevPeriodEnd As Date
            If CInt(mintDeductionPeriodUnkId) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(mintDeductionPeriodUnkId)
                xPrevPeriodStart = objPeriod._Start_Date
                xPrevPeriodEnd = objPeriod._End_Date
                objPeriod = Nothing
            Else
                xPrevPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                xPrevPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            dsEmpBank = objEmpBankTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), "EmployeeBank", False, , CStr(cboEmpName.SelectedValue), xPrevPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

            'call oracle function to get list of loan emis by passing bank account no.
            If dsEmpBank.Tables(0).Rows.Count > 0 Then
                Dim dsFlexLoanData As DataSet = objloanAdvance.GetFlexcubeLoanData(Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString, Session("OracleUserPassword").ToString, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString, "CL28", , False)
                'Hemant (29 Mar 2024) -- [dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString]
                'Hemant (01 Sep 2023) -- [blnUseOldView := False]
                If dsFlexLoanData IsNot Nothing AndAlso dsFlexLoanData.Tables(0).Rows.Count > 0 Then
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsMortgageLoanExist", mstrModuleName)
        Finally
            objEmpBankTran = Nothing
            objloanAdvance = Nothing
            objLoanScheme = Nothing
        End Try
    End Function
    'Hemant (07 Jul 2023) -- End
#End Region

#Region "Button's Events"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            If mdtLoanApplicationDocument IsNot Nothing Then mdtLoanApplicationDocument.Clear()
            mdtLoanApplicationDocument = Nothing
            Session("mdtLoanApplicationDocument") = Nothing
            Session.Remove("mdtLoanApplicationDocument")

            If mdtOtherLoanApplicationDocument IsNot Nothing Then mdtOtherLoanApplicationDocument.Clear()
            mdtOtherLoanApplicationDocument = Nothing
            Session("mdtOtherLoanApplicationDocument") = Nothing
            Session.Remove("mdtOtherLoanApplicationDocument")

            If mdtFinalLoanApplicationDocument IsNot Nothing Then mdtFinalLoanApplicationDocument.Clear()
            mdtFinalLoanApplicationDocument = Nothing
            Session("mdtFinalLoanApplicationDocument") = Nothing
            Session.Remove("mdtFinalLoanApplicationDocument")
            'Pinkal (04-Aug-2023) -- End
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Application/wPg_LoanApplicationList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim btnFlag As Boolean = False

            If IsValidate() = False Then Exit Sub

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .

            If CBool(Session("ApplyTOTPForLoanApplicationApproval")) Then


                '/* START CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP
                Dim objConfig As New clsConfigOptions
                Dim objAttempts As New clsloantotpattempts_tran
                Dim dsAttempts As DataSet = Nothing

                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, CInt(Session("UserId")), 0)
                ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, 0, CInt(Session("Employeeunkid")))
                End If

                Dim xOTPRetrivaltime As DateTime = Nothing
                If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                    If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                        xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))
                        If xOTPRetrivaltime > objConfig._CurrentDateAndTime Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 62, "You cannot save this loan application.Reason : As You reached Maximum unsuccessful attempts.Please try after") & " " & CInt(Session("TOTPRetryAfterMins")) & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 63, "minutes") & ".", Me)
                            objConfig = Nothing
                            objAttempts = Nothing
                            Exit Sub
                        End If
                    End If
                End If
                objConfig = Nothing
                objAttempts = Nothing
                '/* END CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP


                If Session("SMSGatewayEmail").ToString().Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 61, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan application."), Me)
                    Exit Sub
                End If

                Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))

                Dim objUser As New clsUserAddEdit
                Dim xUserId As Integer = objUser.Return_UserId(CInt(cboEmpName.SelectedValue), CInt(Session("CompanyUnkId")))
                objUser._Userunkid = xUserId
                txtVerifyOTP.Text = ""
                txtVerifyOTP.ReadOnly = False
                btnVerify.Enabled = True
                btnSendCodeAgain.Enabled = False
                LblSeconds.Visible = True
                btnTOTPClose.Enabled = False

                If objUser._TOTPSecurityKey.Trim.Length > 0 Then
                    mstrSecretKey = objUser._TOTPSecurityKey
                Else
                    mstrSecretKey = objTOTPHelper.GenerateSecretKey()
                    objUser._TOTPSecurityKey = mstrSecretKey
                    If objUser.Update(False) = False Then
                        DisplayMessage.DisplayMessage(objUser._Message, Me)
                        Exit Sub
                    End If
                End If
                objUser = Nothing

                Counter = objTOTPHelper._TimeStepSeconds

                Dim mstrTOTPCode As String = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
                Dim mstrError As String = SendOTP(mstrTOTPCode)
                If mstrError.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    blnShowTOTPPopup = False
                    popup_TOTP.Hide()
                    Exit Sub
                End If

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:countdown(0," & Counter & "); ", True)
                blnShowTOTPPopup = True
                popup_TOTP.Show()
            Else
                Save()
                End If
            'Pinkal (02-Jun-2023) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAttachment.Click
        Try
            'Hemant (13 Dec 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - When selecting document attachments on loan application page, the system loads for a long time before accepting the attachment.
            'If IsValidate() = False Then Exit Sub
            'Hemant (13 Dec 2022) -- End

            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1003 - As a user, I want all the mandatory fields on loan application to be highlighted in asterisks. (Purpose of credit, model, YOM, Chassis number, supplier, Colour, Plot No, Block No, Street, Town/city, Market Value, CT no, LO no, FSV, Document Type)
                'If f.Extension.ToString.ToUpper() <> ".PDF" Then
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1066, "Please select proper PDF file."), Me)
                Dim arrValidFileExtension() As String = {".PDF", ".JPG", ".JPEG", ".PNG", ".GIF", ".BMP", ".TIF"}
                If arrValidFileExtension.Contains(f.Extension.ToString.ToUpper()) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 52, "Please select PDF or Image file."), Me)
                    'Hemant (27 Oct 2022) -- End
                    Exit Sub
                End If

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'mdtLoanApplicationDocument = CType(ViewState("mdtLoanApplicationDocument"), DataTable)
                mdtLoanApplicationDocument = CType(Session("mdtLoanApplicationDocument"), DataTable)
                'Pinkal (04-Aug-2023) -- End
                AddDocumentAttachment(f, f.FullName)
                Call FillLoanApplicationAttachment()
            End If
            Session.Remove("Imagepath")
            cboDocumentType.SelectedValue = CStr(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgvQualification.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 53, "No Files to download."), Me)
                Exit Sub
            End If

            'If txtFormNo.Text.Trim.Length > 0 Then
            '    strMsg = DownloadAllDocument("file" & txtFormNo.Text.Replace(" ", "") + ".zip", mdtLoanApplicationDocument, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
            'Else
            '    strMsg = DownloadAllDocument("StaffRequisition" + eZeeDate.convertDate(DateTime.Now) + ".zip", mdtLoanApplicationDocument, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnConfirmSign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmSign.Click
        Try
            pnlSign.Visible = False
            blnIsPnlSignVisible = False
            If imgSignature.ImageUrl.ToString.Trim.Length > 0 Then
                pnlSign.Visible = True
                blnIsPnlSignVisible = True
                imgSignature.Visible = True
                lblnosign.Visible = False
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "displaypanel", "displaysign();", True)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
    Protected Sub btnAddOtherAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddOtherAttachment.Click
        Try
            'Hemant (13 Dec 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - When selecting document attachments on loan application page, the system loads for a long time before accepting the attachment.
            'If IsValidate() = False Then Exit Sub
            'Hemant (13 Dec 2022) -- End

            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                Dim arrValidFileExtension() As String = {".PDF", ".JPG", ".JPEG", ".PNG", ".GIF", ".BMP", ".TIF"}
                If arrValidFileExtension.Contains(f.Extension.ToString.ToUpper()) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 52, "Please select PDF or Image file."), Me)
                    Exit Sub
                End If

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'mdtOtherLoanApplicationDocument = CType(ViewState("mdtOtherLoanApplicationDocument"), DataTable)
                mdtOtherLoanApplicationDocument = CType(Session("mdtOtherLoanApplicationDocument"), DataTable)
                'Pinkal (04-Aug-2023) -- End
                AddDocumentOtherAttachment(f, f.FullName)
                Call FillLoanApplicationOtherAttachment()
            End If
            Session.Remove("Imagepath")
            cboOtherDocumentType.SelectedValue = CStr(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (27 Oct 2022) -- End


    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .

    Protected Sub btnVerify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerify.Click
        Try
            If txtVerifyOTP.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmOTPValidator", 1, "OTP is compulsory information.Please Enter OTP."), Me)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                txtVerifyOTP.Focus()
                popup_TOTP.Show()
                Exit Sub
            End If

            Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))
            If objTOTPHelper.ValidateTotpCode(mstrSecretKey, txtVerifyOTP.Text.Trim) = False Then
                Dim mblnprompt As Boolean = False
                InsertAttempts(mblnprompt)
                If mblnprompt = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmOTPValidator", 2, "Invalid OTP Code. Please try again."), Me)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                    objTOTPHelper = Nothing
                    Exit Sub
                End If
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                blnShowTOTPPopup = False
                popup_TOTP.Hide()
                Save()
            End If
            objTOTPHelper = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnSendCodeAgain_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendCodeAgain.Click
        Try
            btnSendCodeAgain.Enabled = False
            btnTOTPClose.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True

            Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))
            Dim mstrTOTPCode As String = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
            Counter = objTOTPHelper._TimeStepSeconds
            Dim mstrError As String = SendOTP(mstrTOTPCode)
            If mstrError.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(mstrError, Me)
                blnShowTOTPPopup = False
                popup_TOTP.Hide()
                Exit Sub
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:countdown(0," & Counter & "); ", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_TOTP.Show()
        End Try
    End Sub

    Protected Sub btnTOTPClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTOTPClose.Click
        Try
            btnSendCodeAgain.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True
            blnShowTOTPPopup = False
            popup_TOTP.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (02-Jun-2023) -- End

#End Region

#Region "ComboBox Events"

    Protected Sub cboEmpName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpName.SelectedIndexChanged
        Dim objEmployee As New clsEmployee_Master
        Try
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-949 - As a user, on the application screen, if the loan scheme selected is eligible for top-up, my Take home should be calculated as Principal Amount – (Outstanding Principle + Outstanding Interest + Insurance Amount)
            'Dim decMaxLoanAmount As Decimal = 0
            'If mintMaxLoanCalcTypeId = 2 Then
            '    decMaxLoanAmount = GetAmountByHeadFormula(mstrMaxLoanAmountHeadFormula)
            'Else
            '    decMaxLoanAmount = mdecMaxLoanAmountDefined
            'End If

            'If mstrMaxInstallmentHeadFormula.Trim.Length <= 0 Then
            '    'txtMaxLoanAmount.Text = Format(GetMaxLoanAmountDefinedLoanScheme(decMaxLoanAmount), Session("fmtCurrency").ToString)
            '    txtMaxLoanAmount.Text = Format(decMaxLoanAmount, Session("fmtCurrency").ToString)
            'Else
            '    mdecEstimateMaxInstallmentAmt = GetAmountByHeadFormula(mstrMaxInstallmentHeadFormula)
            '    txtMaxLoanAmount.Text = Format(IIf(mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme > decMaxLoanAmount, decMaxLoanAmount, mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme), Session("fmtCurrency").ToString)
            'End If
            'Hemant (12 Oct 2022) -- End

            btnConfirmSign.Visible = False
            pnlSign.Visible = False
            blnIsPnlSignVisible = False
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmpName.SelectedValue)
            If objEmployee._EmpSignature IsNot Nothing Then
                imgSignature.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=3"
                btnConfirmSign.Visible = True
            End If
            'Hemant (07 Jul 2023) -- Start
            'Enhancement : NMB : Show the remaining number of pay periods & Dates to the end of applicant's contract, Retirement on loan application and approval screens
            Dim dtEOCDate As Date = Nothing
            'Hemant (02 Feb 2024) -- Start
            'ISSUE/ENHANCEMENT(NMB): To exclude one month installment on application because during application, the system considers the first month in installment payments but in practice Approvers selects the 2nd month as deduction period. 
            Dim blnIsEOCDate As Boolean = False
            'Hemant (02 Feb 2024) -- End
            If objEmployee._Termination_To_Date <> Nothing AndAlso IsDBNull(objEmployee._Termination_To_Date) = False Then
                dtEOCDate = CDate(objEmployee._Termination_To_Date)
                lblEmplDate.Text = "Till Retr. Date"
            End If

            If objEmployee._Empl_Enddate <> Nothing AndAlso IsDBNull(objEmployee._Empl_Enddate) = False Then
                If dtEOCDate < CDate(objEmployee._Empl_Enddate) Then
                Else
                    dtEOCDate = CDate(objEmployee._Empl_Enddate)
                End If
                lblEmplDate.Text = "Till EOC Date"
                'Hemant (02 Feb 2024) -- Start
                'ISSUE/ENHANCEMENT(NMB): To exclude one month installment on application because during application, the system considers the first month in installment payments but in practice Approvers selects the 2nd month as deduction period. 
                blnIsEOCDate = True
                'Hemant (02 Feb 2024) -- End
            End If
            If dtEOCDate <> Nothing Then
                dtpEndEmplDate.SetDate = dtEOCDate
                'Hemant (02 Feb 2024) -- Start
                'ISSUE/ENHANCEMENT(NMB): To exclude one month installment on application because during application, the system considers the first month in installment payments but in practice Approvers selects the 2nd month as deduction period. 
                If blnIsEOCDate = True Then
                    nudPayPeriodEOC.Text = CStr(Math.Abs((mdtPeriodStart.Month - dtEOCDate.Month) + 12 * (mdtPeriodStart.Year - dtEOCDate.Year)))
                Else
                    'Hemant (02 Feb 2024) -- End
                nudPayPeriodEOC.Text = CStr(Math.Abs((mdtPeriodStart.Month - dtEOCDate.Month) + 12 * (mdtPeriodStart.Year - dtEOCDate.Year)) + 1)
                End If 'Hemant (02 Feb 2024)
                lblPayPeriodsEOC.Visible = True
                nudPayPeriodEOC.Visible = True
                lblEmplDate.Visible = True
                dtpEndEmplDate.Visible = True
            Else
                lblPayPeriodsEOC.Visible = False
                nudPayPeriodEOC.Visible = False
                lblEmplDate.Visible = False
                dtpEndEmplDate.Visible = False
            End If
            'Hemant (07 July 2023) -- End
            ResetMax()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Protected Sub cboDeductionPeriod_SelectedIndexChanged()
        Try
            Call SetDateFormat()

            Dim objPeriod As New clscommom_period_Tran
            objvalPeriodDuration.Visible = True
            If mintDeductionPeriodUnkId > 0 Then
                objPeriod._Periodunkid(Session("Database_Name").ToString) = mintDeductionPeriodUnkId
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date

                Language.setLanguage(mstrModuleName)
                objvalPeriodDuration.Text = Language.getMessage(mstrModuleName, 39, "Period Duration From :") & " " & objPeriod._Start_Date.Date & " " & Language.getMessage(mstrModuleName, 40, "To") & " " & objPeriod._End_Date.Date
                objPeriod = Nothing
            Else
                objvalPeriodDuration.Text = ""
                objvalPeriodDuration.Visible = False
                mdtPeriodStart = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
                mdtPeriodEnd = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged
        Try
            txtRepaymentDays.Text = "0"
            cboLoanSchemeCategory.SelectedValue = CStr(0)

            'Pinkal (20-Sep-2022) -- Start
            'NMB Loan Module Enhancement.
            mblnRequiredReportingToApproval = False
            'Pinkal (20-Sep-2022) -- End

            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            mblnSkipApprovalFlow = False
            'Pinkal (12-Oct-2022) -- End

            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            mblnLoanApproval_DailyReminder = False
            mintEscalationDays = 0
            'Pinkal (10-Nov-2022) -- End

            Call ResetMax()
            'If cboLoanScheme.SelectedValue IsNot Nothing AndAlso CInt(cboLoanScheme.SelectedValue) > 0 Then
            '    Dim objLScheme As New clsLoan_Scheme
            '    Dim decMaxLoanAmount As Decimal = 0

            '    objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            '    mdecMaxLoanAmountDefined = objLScheme._MaxLoanAmountLimit
            '    txtLoanRate.Text = CStr(objLScheme._InterestRate)
            '    mintMaxLoanCalcTypeId = CInt(objLScheme._MaxLoanAmountCalcTypeId)
            '    mstrMaxLoanAmountHeadFormula = CStr(objLScheme._MaxLoanAmountHeadFormula)
            '    cboLoanCalcType.SelectedValue = CStr(objLScheme._LoanCalcTypeId)
            '    cboInterestCalcType.SelectedValue = CStr(objLScheme._InterestCalctypeId)
            '    'Call cboLoanCalcType_SelectedIndexChanged(sender, e)
            '    cboLoanSchemeCategory.SelectedValue = CStr(objLScheme._LoanSchemeCategoryId)
            '    txtRepaymentDays.Text = CStr(objLScheme._RepaymentDays)

            '    mintMaxNoOfInstallmentLoanScheme = CInt(objLScheme._MaxNoOfInstallment)
            '    mstrMaxInstallmentHeadFormula = objLScheme._MaxInstallmentHeadFormula
            '    If mintMaxLoanCalcTypeId = 2 Then
            '        decMaxLoanAmount = GetAmountByHeadFormula(mstrMaxLoanAmountHeadFormula)
            '    Else
            '        decMaxLoanAmount = mdecMaxLoanAmountDefined
            '    End If
            '    If mstrMaxInstallmentHeadFormula.Trim.Length <= 0 Then
            '        'txtMaxLoanAmount.Text = Format(GetMaxLoanAmountDefinedLoanScheme(decMaxLoanAmount), Session("fmtCurrency").ToString)
            '        txtMaxLoanAmount.Text = Format(decMaxLoanAmount, Session("fmtCurrency").ToString)
            '    Else
            '        mdecEstimateMaxInstallmentAmt = GetAmountByHeadFormula(mstrMaxInstallmentHeadFormula)
            '        txtMaxLoanAmount.Text = Format(IIf(mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme > decMaxLoanAmount, decMaxLoanAmount, mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme), Session("fmtCurrency").ToString)
            '    End If

            '    Panel1.Visible = False
            '    Panel2.Visible = False
            '    If CInt(objLScheme._LoanSchemeCategoryId) = enLoanSchemeCategories.SECURED Then
            '        Panel2.Visible = True
            '    ElseIf CInt(objLScheme._LoanSchemeCategoryId) = enLoanSchemeCategories.MORTGAGE Then
            '        Panel1.Visible = True
            '    End If

            '    txtMaxInstallment.Text = CStr(objLScheme._MaxNoOfInstallment)

            '    txtInsuranceRate.Text = CStr(objLScheme._InsuranceRate)

            '    If mintProcesspendingloanunkid <= 0 Then
            '        txtLoanAmt.Text = txtMaxLoanAmount.Text
            '        txtEMIInstallments.Text = txtMaxInstallment.Text
            '        txtInstallmentAmt.Text = Format(CDec(txtLoanAmt.Text) / CInt(txtEMIInstallments.Text), Session("fmtCurrency").ToString)
            '    End If

            '    txtMaxInstallmentAmt.Text = Format(CDec(txtMaxLoanAmount.Text) / CInt(txtEMIInstallments.Text), Session("fmtCurrency").ToString)

            '    txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100) * (CInt(txtEMIInstallments.Text) / 12), Session("fmtCurrency").ToString)
            '    txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - CDec(txtInsuranceAmt.Text), Session("fmtCurrency").ToString)

            '    mblnIsAttachmentRequired = objLScheme._IsAttachmentRequired
            '    mstrDocumentTypeIDs = CStr(objLScheme._DocumentTypeIDs)

            '    If mblnIsAttachmentRequired = True Then
            '        pnlScanAttachment.Visible = True
            '    Else
            '        pnlScanAttachment.Visible = False
            '    End If
            '    Call FillDocumentTypeCombo()

            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExRate.Text = ""
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, mdtPeriodEnd, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, Session("fmtCurrency").ToString()) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "TextBox Events"

    Private Sub txtPrincipalAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAppliedUptoPrincipalAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.No_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Dim decLoanAmt, decPrinc As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtAppliedUptoPrincipalAmt.Text, decPrinc)
                Dim decValue As Decimal = 0
                decValue = decLoanAmt / decPrinc
                RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                txtEMIInstallments.Text = CStr(IIf(decValue <= 0, 1, CDec(Math.Ceiling(decValue))))
                AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.No_Interest_With_Mapped_Head, enIntCalcType)

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub txtInstallmentAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then

                If CDbl(txtInstallmentAmt.Text) > 0 Then
                    RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                    Dim dblvalue As Double = 0
                    dblvalue = CDbl(txtLoanAmt.Text) / CDbl(txtInstallmentAmt.Text)
                    txtEMIInstallments.Text = CStr(IIf(dblvalue <= 0, 1, Math.Ceiling(dblvalue).ToString()))
                    AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged

                    RemoveHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtAppliedUptoPrincipalAmt.Text = Format(CDbl(txtInstallmentAmt.Text), Session("fmtCurrency").ToString)
                    AddHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtIntAmt.Text = Format(0, Session("fmtCurrency").ToString)
                    decNetAmount = CDec(txtInstallmentAmt.Text)

                End If

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then

                If CDbl(txtInstallmentAmt.Text) > 0 Then
                    RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                    Dim dblvalue As Double = 0
                    dblvalue = CDbl(txtLoanAmt.Text) / CDbl(txtInstallmentAmt.Text)
                    txtEMIInstallments.Text = CStr(IIf(dblvalue <= 0, 1, Math.Ceiling(dblvalue).ToString()))

                    AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                    RemoveHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtAppliedUptoPrincipalAmt.Text = Format(CDbl(txtInstallmentAmt.Text), Session("fmtCurrency").ToString)
                    AddHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtIntAmt.Text = Format(0, Session("fmtCurrency").ToString)
                    decNetAmount = CDec(txtInstallmentAmt.Text)
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub txtLoanAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                Dim decLoanAmt, decPrinc As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtAppliedUptoPrincipalAmt.Text, decPrinc)
                Dim decValue As Decimal = 0
                If decPrinc > 0 Then
                    decValue = decLoanAmt / decPrinc
                End If
                txtEMIInstallments.Text = CStr(IIf(decValue <= 0, 1, CDec(Math.Ceiling(decValue))))
                AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                Call txtEMIInstallments_TextChanged(txtEMIInstallments, New System.EventArgs)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Call txtEMIInstallments_TextChanged(txtEMIInstallments, New System.EventArgs)

            End If

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            'A1X-2587 NMB mortgage - Insurance deduction: The system should deduct insurance for the first year only and for the following year it will be done yearly. 
            Dim objLScheme As New clsLoan_Scheme
            objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            If objLScheme._LoanSchemeCategoryId = enLoanSchemeCategories.MORTGAGE Then
                If CInt(txtEMIInstallments.Text) > 12 Then
                    txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100), Session("fmtCurrency").ToString)
                Else
            txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100) * (CInt(txtEMIInstallments.Text) / 12), Session("fmtCurrency").ToString)
                End If
            Else
                txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100) * (CInt(txtEMIInstallments.Text) / 12), Session("fmtCurrency").ToString)
            End If
            objLScheme = Nothing
            'Pinkal (17-May-2024) -- End


            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-949 - As a user, on the application screen, if the loan scheme selected is eligible for top-up, my Take home should be calculated as Principal Amount – (Outstanding Principle + Outstanding Interest + Insurance Amount)
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - CDec(txtInsuranceAmt.Text), Session("fmtCurrency").ToString)
            'Hemant (10 Nov 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : For the Take home field in top-up loans. Please return it to calculate as normal loan only.
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(txtOutStandingPrincipalAmt.Text) + CDec(txtOutStandingInterestAmt.Text) + CDec(txtInsuranceAmt.Text)), Session("fmtCurrency").ToString)
            'Hemant (13 Dec 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - For top-up loans, they want the Take Home formula to be as below : Take Home = Principle – (Current Outstanding Principle + Current Outstanding Interest + Insurance Amount).
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - CDec(txtInsuranceAmt.Text), Session("fmtCurrency").ToString)
            txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(txtOutStandingPrincipalAmt.Text) + CDec(txtOutStandingInterestAmt.Text) + CDec(txtInsuranceAmt.Text)) - mdecFinalSalaryAdvancePendingPrincipalAmt, Session("fmtCurrency").ToString)
            'Hemant (22 Nov 2024) -- [- mdecFinalSalaryAdvancePendingPrincipalAmt]
            'Hemant (13 Dec 2022) -- End
            'Hemant (10 Nov 2022) -- End
            'Hemant (12 Oct 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub txtLoanRate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanRate.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub txtEMIInstallments_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEMIInstallments.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                Dim decLoanAmt, decEMIInst As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtEMIInstallments.Text, decEMIInst)
                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                txtInstallmentAmt.Text = Format(decLoanAmt / decEMIInst, Session("fmtCurrency").ToString)
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                txtAppliedUptoPrincipalAmt.ReadOnly = False
                Dim decLoanAmt, decEMIInst As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtEMIInstallments.Text, decEMIInst)
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Dim decLoanAmt, decEMIInst As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtEMIInstallments.Text, decEMIInst)
                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                txtInstallmentAmt.Text = Format(decLoanAmt / decEMIInst, Session("fmtCurrency").ToString)
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            End If

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            'A1X-2587 NMB mortgage - Insurance deduction: The system should deduct insurance for the first year only and for the following year it will be done yearly. 
            Dim objLScheme As New clsLoan_Scheme
            objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            If objLScheme._LoanSchemeCategoryId = enLoanSchemeCategories.MORTGAGE Then
                If CInt(txtEMIInstallments.Text) > 12 Then
                    txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100), Session("fmtCurrency").ToString)
                Else
            txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100) * (CInt(txtEMIInstallments.Text) / 12), Session("fmtCurrency").ToString)
                End If
            Else
                txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100) * (CInt(txtEMIInstallments.Text) / 12), Session("fmtCurrency").ToString)
            End If
            objLScheme = Nothing
            'Pinkal (17-May-2024) -- End

            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-949 - As a user, on the application screen, if the loan scheme selected is eligible for top-up, my Take home should be calculated as Principal Amount – (Outstanding Principle + Outstanding Interest + Insurance Amount)
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - CDec(txtInsuranceAmt.Text), Session("fmtCurrency").ToString)
            'Hemant (10 Nov 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : For the Take home field in top-up loans. Please return it to calculate as normal loan only.
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(txtOutStandingPrincipalAmt.Text) + CDec(txtOutStandingInterestAmt.Text) + CDec(txtInsuranceAmt.Text)), Session("fmtCurrency").ToString)
            'Hemant (13 Dec 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - For top-up loans, they want the Take Home formula to be as below : Take Home = Principle – (Current Outstanding Principle + Current Outstanding Interest + Insurance Amount).
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - CDec(txtInsuranceAmt.Text), Session("fmtCurrency").ToString)
            txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(txtOutStandingPrincipalAmt.Text) + CDec(txtOutStandingInterestAmt.Text) + CDec(txtInsuranceAmt.Text)) - mdecFinalSalaryAdvancePendingPrincipalAmt, Session("fmtCurrency").ToString)
            'Hemant (22 Nov 2024) -- [- mdecFinalSalaryAdvancePendingPrincipalAmt]
            'Hemant (13 Dec 2022) -- End
            'Hemant (10 Nov 2022) -- End
            'Hemant (12 Oct 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Hemant (07 Jul 2023) -- Start
    'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
    Private Sub nudTitleValidity_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudTitleValidity.TextChanged
        Try
            nudTitleValidity.Text = Math.Floor(CDec(nudTitleValidity.Text)).ToString
            dtpTitleExpiryDate.SetDate = dtpTitleIssueDate.GetDate.Date.AddYears(CInt(nudTitleValidity.Text))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (07 July 2023) -- End

#End Region

#Region "Gridview Event"
    Protected Sub dgvQualification_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvQualification.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvQualification, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtLoanApplicationDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvQualification, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtLoanApplicationDocument.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvQualification, "objcolhGUID", False, True)).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 36, "Are you sure you want to delete this attachment?")
                        popup_YesNo.Show()
                        Me.ViewState("DeleteAttRowIndex") = mdtLoanApplicationDocument.Rows.IndexOf(xrow(0))
                    End If
                ElseIf e.CommandName = "imgdownload" Then
                    Dim xPath As String = ""
                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("filepath").ToString
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If
                    If xPath.Trim <> "" Then
                        Dim fileInfo As New IO.FileInfo(xPath)
                        If fileInfo.Exists = False Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "File does not exist on localpath"), Me)
                            Exit Sub
                        End If
                        fileInfo = Nothing
                        Dim strFile As String = xPath
                        Response.ContentType = "image/jpg/pdf"
                        Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvQualification, "colhName", False, True)).Text & """")
                        Response.Clear()
                        Response.TransmitFile(strFile)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
    Protected Sub dgvOtherQualification_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvOtherQualification.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvOtherQualification, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtOtherLoanApplicationDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvOtherQualification, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtOtherLoanApplicationDocument.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvOtherQualification, "objcolhGUID", False, True)).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 36, "Are you sure you want to delete this attachment?")
                        popup_YesNo.Show()
                        Me.ViewState("DeleteOtherAttRowIndex") = mdtOtherLoanApplicationDocument.Rows.IndexOf(xrow(0))
                    End If
                ElseIf e.CommandName = "imgdownload" Then
                    Dim xPath As String = ""
                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("filepath").ToString
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If
                    If xPath.Trim <> "" Then
                        Dim fileInfo As New IO.FileInfo(xPath)
                        If fileInfo.Exists = False Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "File does not exist on localpath"), Me)
                            Exit Sub
                        End If
                        fileInfo = Nothing
                        Dim strFile As String = xPath
                        Response.ContentType = "image/jpg/pdf"
                        Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvOtherQualification, "colhName", False, True)).Text & """")
                        Response.Clear()
                        Response.TransmitFile(strFile)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (27 Oct 2022) -- End

#End Region

#Region "Controls Event(S)"
    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                Me.ViewState("DeleteAttRowIndex") = Nothing
            End If

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            If Me.ViewState("DeleteOtherAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteOtherAttRowIndex")) < 0 Then
                Me.ViewState("DeleteOtherAttRowIndex") = Nothing
            End If
            'Hemant (27 Oct 2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                mdtLoanApplicationDocument.Rows(CInt(Me.ViewState("DeleteAttRowIndex")))("AUD") = "D"
                mdtLoanApplicationDocument.AcceptChanges()
                Call FillLoanApplicationAttachment()
                Me.ViewState("DeleteAttRowIndex") = Nothing
            End If

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            If Me.ViewState("DeleteOtherAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteOtherAttRowIndex")) < 0 Then
                mdtOtherLoanApplicationDocument.Rows(CInt(Me.ViewState("DeleteOtherAttRowIndex")))("AUD") = "D"
                mdtOtherLoanApplicationDocument.AcceptChanges()
                Call FillLoanApplicationOtherAttachment()
                Me.ViewState("DeleteOtherAttRowIndex") = Nothing
            End If
            'Hemant (27 Oct 2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Checkbox Events"

    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
    Protected Sub chkMakeExceptionalApplication_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMakeExceptionalApplication.CheckedChanged
        Try
            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            '  If chkMakeExceptionalApplication.Checked = True AndAlso mintOtherDocumentunkid > 0 Then
            If chkMakeExceptionalApplication.Checked = True AndAlso mstrOtherDocumentIds.Trim.Length > 0 Then
                'Pinkal (23-Nov-2022) -- End
                pnlOtherScanAttachment.Visible = True
            Else
                pnlOtherScanAttachment.Visible = False
            End If

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            'If (chkMakeExceptionalApplication.Checked = True AndAlso mintOtherDocumentunkid > 0) OrElse mblnIsAttachmentRequired = True Then
            If (chkMakeExceptionalApplication.Checked = True AndAlso mstrOtherDocumentIds.Trim.Length > 0) OrElse mblnIsAttachmentRequired = True Then
                'Pinkal (23-Nov-2022) -- End
                pnlAttachment.Visible = True
            Else
                pnlAttachment.Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (27 Oct 2022) -- End

    'Hemant (22 Nov 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
    Private Sub chkSalaryAdvanceLiquidation_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSalaryAdvanceLiquidation.CheckedChanged
        Try
            If chkSalaryAdvanceLiquidation.Checked = True Then
                mdecFinalSalaryAdvancePendingPrincipalAmt = mdecActualSalaryAdvancePendingPrincipalAmt
            Else
                mdecFinalSalaryAdvancePendingPrincipalAmt = 0
            End If

            txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(txtOutStandingPrincipalAmt.Text) + CDec(txtOutStandingInterestAmt.Text) + CDec(txtInsuranceAmt.Text)) - mdecFinalSalaryAdvancePendingPrincipalAmt, GUI.fmtCurrency)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (22 Nov 2024) -- End

#End Region

#Region "Timer Events"


    'Public Sub timer_tick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    ts.Enabled = False
    '    Try
    '        Dim t As TimeSpan = TimeSpan.FromSeconds(Counter)
    '        LblSeconds.Text = t.Minutes.ToString("#00") & ":" & t.Seconds.ToString("#00")
    '        If Counter < 0 Then
    '            txtVerifyOTP.ReadOnly = True
    '            LnkSendCodeAgain.Enabled = True
    '            ts.Enabled = False
    '            Counter = 0
    '            t = TimeSpan.FromSeconds(Counter)
    '            LblSeconds.Text = t.Minutes.ToString("#00") & ":" & t.Seconds.ToString("#00")
    '            btnVerify.Enabled = False
    '            Exit Sub
    '            'Else
    '            '    If objTOTPHelper.ValidateTotpCode(mstrSecretKey, txtVerifyOTP.Text.Trim) Then
    '            '        LblMessage.Text = "Verify Successful."
    '            '        LblMessage.ForeColor = Color.Green
    '            '    Else
    '            '        LblMessage.Text = "Invalid Code."
    '            '        LblMessage.ForeColor = Color.Red
    '            '        Counter = 0
    '            '        t = TimeSpan.FromSeconds(Counter)
    '            '        LblSeconds.Text = t.Minutes.ToString("#00") & ":" & t.Seconds.ToString("#00")
    '            '        ts.Enabled = False
    '            '        LnkSendCodeAgain.Enabled = True
    '            '        Exit Sub
    '            '    End If
    '        End If
    '        Counter -= 1
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "timer_Elapsed", mstrModuleName)
    '    Finally
    '        If ts.Enabled = False Then ts.Enabled = True
    '        popup_TOTP.Show()
    '    End Try
    'End Sub

#End Region

#Region "DateTimePicker Events"

    'Hemant (07 Jul 2023) -- Start
    'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
    Private Sub dtpTitleIssueDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTitleIssueDate.TextChanged
        Try
            dtpTitleExpiryDate.SetDate = dtpTitleIssueDate.GetDate.Date.AddYears(CInt(nudTitleValidity.Text))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (07 July 2023) -- End

#End Region


    Private Sub SetControlCaptions()
        Try

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPageHeader.ID, Me.Title)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEmpName.ID, lblEmpName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblLoanScheme.ID, lblLoanScheme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblLoanSchemeCategory.ID, lblLoanSchemeCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblRepaymentDays.ID, lblRepaymentDays.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblMaxLoanAmount.ID, lblMaxLoanAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblMinLoanAmount.ID, lblMinLoanAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblMaxInstallmentAmt.ID, lblMaxInstallmentAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblMaxInstallment.ID, lblMaxInstallment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblLoanInterest.ID, lblLoanInterest.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblInsuranceRate.ID, lblInsuranceRate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblLoanCalcType.ID, lblLoanCalcType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblInterestCalcType.ID, lblInterestCalcType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblAmt.ID, lblAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, elLoanAmountCalculation.ID, elLoanAmountCalculation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEMIInstallments.ID, lblEMIInstallments.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPrincipalAmt.ID, lblPrincipalAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblIntAmt.ID, lblIntAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEMIAmount.ID, lblEMIAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblInsuranceAmt.ID, lblInsuranceAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTakeHome.ID, lblTakeHome.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPurposeOfCredit.ID, lblPurposeOfCredit.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblPlotNo.ID, lblPlotNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblBlockNo.ID, lblBlockNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblStreet.ID, lblStreet.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTownCity.ID, lblTownCity.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblMarketValue.ID, lblMarketValue.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblCTNo.ID, lblCTNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblLONo.ID, lblLONo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblFSV.ID, lblFSV.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblModel.ID, lblModel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblYOM.ID, lblYOM.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblDocumentType.ID, lblDocumentType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblempsign.ID, lblempsign.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblnosign.ID, lblnosign.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblOutStandingPrincipalAmt.ID, lblOutStandingPrincipalAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblOutStandingInterestAmt.ID, lblOutStandingInterestAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblInstallmentPaid.ID, lblInstallmentPaid.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblChassisNo.ID, lblChassisNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblSupplier.ID, lblSupplier.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblColour.ID, lblColour.Text)
            'Hemant (29 Mar 2024) -- Start
            'ENHANCEMENT(TADB): New loan application form
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTotalCIF.ID, lblTotalCIF.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEstimatedTax.ID, lblEstimatedTax.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblInvoiceValue.ID, lblInvoiceValue.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblModelNo.ID, lblModelNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEngineNo.ID, lblEngineNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblEngineCapacity.ID, lblEngineCapacity.Text)
            'Hemant (29 Mar 2024) -- End
            'Hemant (07 Jul 2023) -- Start
            'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTitleIssueDate.ID, lblTitleIssueDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTitleValidity.ID, lblTitleValidity.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, lblTitleExpiryDate.ID, lblTitleExpiryDate.Text)
            'Hemant (07 July 2023) -- End

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblBOQ.ID, Me.LblBOQ.Text)
            'Pinkal (17-May-2024) -- End


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, chkMakeExceptionalApplication.ID, chkMakeExceptionalApplication.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, chkconfirmSign.ID, chkconfirmSign.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnSave.ID, btnSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnClose.ID, btnClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnDownloadAll.ID, btnDownloadAll.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, btnConfirmSign.ID, btnConfirmSign.Text)

          
            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblVerifyOTP.ID, Me.lblVerifyOTP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblOTP.ID, Me.LblOTP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSendCodeAgain.ID, Me.btnSendCodeAgain.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnVerify.ID, Me.btnVerify.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnTOTPClose.ID, Me.btnTOTPClose.Text)
            'Pinkal (02-Jun-2023) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPageHeader.ID, Me.Title)

            Me.lblEmpName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEmpName.ID, lblEmpName.Text)
            Me.lblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblLoanScheme.ID, lblLoanScheme.Text)
            Me.lblLoanSchemeCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblLoanSchemeCategory.ID, lblLoanSchemeCategory.Text)
            Me.lblRepaymentDays.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblRepaymentDays.ID, lblRepaymentDays.Text)
            Me.lblMaxLoanAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblMaxLoanAmount.ID, lblMaxLoanAmount.Text)
            Me.lblMinLoanAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblMinLoanAmount.ID, lblMinLoanAmount.Text)
            Me.lblMaxInstallmentAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblMaxInstallmentAmt.ID, lblMaxInstallmentAmt.Text)
            Me.lblMaxInstallment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblMaxInstallment.ID, lblMaxInstallment.Text)
            Me.lblLoanInterest.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblLoanInterest.ID, lblLoanInterest.Text)
            Me.lblInsuranceRate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblInsuranceRate.ID, lblInsuranceRate.Text)
            Me.lblLoanCalcType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblLoanCalcType.ID, lblLoanCalcType.Text)
            Me.lblInterestCalcType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblInterestCalcType.ID, lblInterestCalcType.Text)
            Me.lblAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblAmt.ID, lblAmt.Text)
            Me.elLoanAmountCalculation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), elLoanAmountCalculation.ID, elLoanAmountCalculation.Text)
            Me.lblEMIInstallments.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEMIInstallments.ID, lblEMIInstallments.Text)
            Me.lblPrincipalAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPrincipalAmt.ID, lblPrincipalAmt.Text)
            Me.lblIntAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblIntAmt.ID, lblIntAmt.Text)
            Me.lblEMIAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEMIAmount.ID, lblEMIAmount.Text)
            Me.lblInsuranceAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblInsuranceAmt.ID, lblInsuranceAmt.Text)
            Me.lblTakeHome.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTakeHome.ID, lblTakeHome.Text)
            Me.lblPurposeOfCredit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPurposeOfCredit.ID, lblPurposeOfCredit.Text)
            Me.lblPlotNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblPlotNo.ID, lblPlotNo.Text)
            Me.lblBlockNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblBlockNo.ID, lblBlockNo.Text)
            Me.lblStreet.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblStreet.ID, lblStreet.Text)
            Me.lblTownCity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTownCity.ID, lblTownCity.Text)
            Me.lblMarketValue.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblMarketValue.ID, lblMarketValue.Text)
            Me.lblCTNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblCTNo.ID, lblCTNo.Text)
            Me.lblLONo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblLONo.ID, lblLONo.Text)
            Me.lblFSV.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblFSV.ID, lblFSV.Text)
            Me.lblModel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblModel.ID, lblModel.Text)
            Me.lblYOM.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblYOM.ID, lblYOM.Text)
            Me.lblDocumentType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblDocumentType.ID, lblDocumentType.Text)
            Me.lblempsign.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblempsign.ID, lblempsign.Text)
            Me.lblnosign.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblnosign.ID, lblnosign.Text)
            Me.lblOutStandingPrincipalAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblOutStandingPrincipalAmt.ID, lblOutStandingPrincipalAmt.Text)
            Me.lblOutStandingInterestAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblOutStandingInterestAmt.ID, lblOutStandingInterestAmt.Text)
            Me.lblInstallmentPaid.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblInstallmentPaid.ID, lblInstallmentPaid.Text)
            Me.lblChassisNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblChassisNo.ID, lblChassisNo.Text)
            Me.lblSupplier.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblSupplier.ID, lblSupplier.Text)
            Me.lblColour.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblColour.ID, lblColour.Text)
            'Hemant (29 Mar 2024) -- Start
            'ENHANCEMENT(TADB): New loan application form
            Me.lblTotalCIF.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTotalCIF.ID, lblTotalCIF.Text)
            Me.lblEstimatedTax.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEstimatedTax.ID, lblEstimatedTax.Text)
            Me.lblInvoiceValue.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblInvoiceValue.ID, lblInvoiceValue.Text)
            Me.lblModelNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblModelNo.ID, lblModelNo.Text)
            Me.lblEngineNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEngineNo.ID, lblEngineNo.Text)
            Me.lblEngineCapacity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblEngineCapacity.ID, lblEngineCapacity.Text)
            'Hemant (29 Mar 2024) -- End
            'Hemant (07 Jul 2023) -- Start
            'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
            Me.lblTitleIssueDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTitleIssueDate.ID, lblTitleIssueDate.Text)
            Me.lblTitleValidity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTitleValidity.ID, lblTitleValidity.Text)
            Me.lblTitleExpiryDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), lblTitleExpiryDate.ID, lblTitleExpiryDate.Text)
            'Hemant (07 July 2023) -- End

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            Me.LblBOQ.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), LblBOQ.ID, LblBOQ.Text)
            'Pinkal (17-May-2024) -- End

            Me.chkMakeExceptionalApplication.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), chkMakeExceptionalApplication.ID, chkMakeExceptionalApplication.Text)
            Me.chkconfirmSign.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), chkconfirmSign.ID, chkconfirmSign.Text)

            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnSave.ID, btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnClose.ID, btnClose.Text).Replace("&", "")
            Me.btnDownloadAll.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnDownloadAll.ID, btnDownloadAll.Text).Replace("&", "")
            Me.btnConfirmSign.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnConfirmSign.ID, btnConfirmSign.Text).Replace("&", "")


            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Me.lblVerifyOTP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVerifyOTP.ID, Me.lblVerifyOTP.Text)
            Me.LblOTP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblOTP.ID, Me.LblOTP.Text)
            Me.btnSendCodeAgain.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSendCodeAgain.ID, Me.btnSendCodeAgain.Text)
            Me.btnVerify.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnVerify.ID, Me.btnVerify.Text)
            Me.btnTOTPClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnTOTPClose.ID, Me.btnTOTPClose.Text)
            'Pinkal (02-Jun-2023) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Applied Amount cannot be blank. Applied Amount is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Currency is compulsory information.Please select currency.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Installment Amount cannot be 0.Please define installment amount greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Installment Amount cannot be greater than Loan Amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Installment months cannot be greater than")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Sorry, you can not apply for this loan scheme: Reason, Principal amount  is exceeding the Max Principal Amt")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Sorry, you can not apply for this loan scheme: Reason, Installment amount  is exceeding the Max Installment amount")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Sorry, you can not apply for this loan scheme: Reason, Principal amount  should be greater than the Minimum Principal amount")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Principal Amount cannot be 0. Please enter Principal amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Purpose Of Credit is compulsory information. Please enter Purpose Of Credit to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Plot No is compulsory information. Please enter Plot No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Block No is compulsory information. Please enter Block No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Street is compulsory information. Please enter Street to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Town/City is compulsory information. Please enter Town/City to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Market Value cannot be 0. Please enter Market Value.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "CT No is compulsory information. Please enter CT No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "LO No is compulsory information. Please enter LO No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "FSV cannot be 0.Please enter FSV.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "Model is compulsory information. Please enter Model to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, "YOM is compulsory information. Please enter YOM to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 23, "Sorry, you are not confirmed yet. Please contact HR support")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 24, "Sorry, your contract duration is less than 6 months. Please contact HR support")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 25, "Sorry, NIDA number is missing or in pending state")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 26, "Sorry, you have a pending disciplinary issue. Kindly contact HR ER.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 29, "Sorry, no of installments selected cannot go beyond the retirement date")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 30, "Sorry, signature missing. Please update signature from your profile")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 31, "Sorry, Please confirm your signature first.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 33, "for")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 34, " Scheme.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 39, "Period Duration From :")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 40, "To")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 42, " set for selected scheme.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 44, "File does not exist on localpath")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 45, "Loan Scheme is not mapped with Role and Level.please map Loan scheme with role and level.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 46, "Sorry, you can not apply for Topup for this loan scheme: Reason, TopUp is not allowed")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 47, "Sorry, you can not apply for Topup for this loan scheme: Reason, No of Installment Paid should be greater than minimum No of Installment Paid")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 48, "You cannot apply for this loan application.Reason : Loan application's loan amount is not match with the approver level's minimum and maximum range. ")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 49, "Chassis No is compulsory information. Please enter Chassis No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 50, "Supplier is compulsory information. Please enter Supplier to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 51, "Colour is compulsory information. Please enter Colour to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 52, "Please select PDF or Image file.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 53, "No Files to download.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 54, "Loan Application Submitted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 55, "You cannot apply for this loan application.Reason : Loan application's loan amount is not match with the approver level's minimum and maximum range. ")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 56, "Installment months cannot be less than")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 57, "Sorry, you do not qualify for a top-up at the moment. The take home amount should be greater than 0")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 59, "Principle amount cannot be greater than the FSV")

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 60, "Mobile No is compulsory information. Please set Employee's Mobile No to send TOTP.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 61, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan application.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 62, "You cannot save this loan application.Reason :  As You reached Maximum unsuccessful attempts.Please try after")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 63, "minutes")
            'Pinkal (02-Jun-2023) -- End
            'Hemant (07 Jul 2023) -- Start
            'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 65, "Title Validity cannot be 0.Please enter Title Validity.")
            'Hemant (07 July 2023) -- End
            'Hemant (29 Mar 2024) -- Start
            'ENHANCEMENT(TADB): New loan application form
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 68, "Total CIF cannot be 0.Please enter Total CIF.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 69, "Estimated Tax cannot be 0.Please enter Estimated Tax.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 70, "Invoice Value cannot be 0.Please enter Invoice Value.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 71, "Model No is compulsory information. Please enter Model No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 72, "Engine No is compulsory information. Please enter Engine No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 73, "Engine Capacity cannot be 0.Please enter Engine Capacity.")
            'Hemant (29 Mar 2024) -- End

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 74, "No of Repayment Days cannot be 0.Please define No of Repayment Days greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 75, "Bill of Quantity(BOQ) cannot be blank.Please enter Bill of Quantity(BOQ).")
            'Pinkal (17-May-2024) -- End

            'Hemant (16 Aug 2024) -- Start
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 76, "FSV cannot be more than the Market Value.")
            'Hemant (16 Aug 2024) -- End


            'Pinkal (28-Oct-2024) -- Start
            'NMB Enhancement : (A1X-2834) NMB - Credit card loan enhancements.
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 77, "You cannnot apply for this loan application.Reason : you have already past dues days.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 78, "You cannnot apply for this loan application.Reason : you have already exceeded with your credit card limit.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 79, "You cannnot apply for this loan application.Reason : There is no record(s) related to credit card.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),  mstrModuleName, 80, "You cannnot apply for this loan application.Reason : you have already past dues days entry for this loan scheme.")
            'Pinkal (28-Oct-2024) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 81, "You cannot apply for this loan application.Reason : Percentage(%) of total monthly deduction to gross pay exceed 66.67%.")
            'Hemant (22 Nov 2024) -- End


        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

  

End Class
