﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_NewLoanAdvance_AddEdit.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Assignment_wPg_NewLoanAdvance_AddEdit" Title="Add/Edit Loan And Advance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>
    <%--<script type="text/javascript">
        function pageLoad(sender, args) {
            alert('hi');
        }
         
    </script>--%>
    <%-- <script language="javascript" type="text/javascript">
        $(window).load(function() 
        {   alert("load");
//            var numud = $find('#<%=NumericUpDownExtender1.ClientID %>');
//            numud.add_currentChanged(onChanged);
        });  
//        function onChanged(sender, e)
//         {       
//            __doPostBack('<%# txtDuration.ClientID %>', 'true');
//         } 
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, event) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, evemt) {
            $('.panel-36').slideToggle(10);
            $("#endreq").val("1");
            //SetDesign()
        }
        function ShowDiv(result) {
            var strarry = result.split(",");
            for (i = 0; i < strarry.length; i++) {
                $("#" + strarry[i]).children(".panel-36").slideToggle(10);
                $("#" + strarry[i]).children(".toggler-36").toggleClass("active");
                $("#" + strarry[i]).children(".toggler-36").children('div').children('i').toggleClass("fa-chevron-circle-down");
            }
        }
    </script>

    <%--<script type="text/javascript">
    function SetDesign(){
        $("#<%= txtDuration.ClientID %>").next().next().css("top","0px");
        $("#<%= txtDuration.ClientID %>").next().css("top","-12px");
    }
    </script>--%>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            var charCode;

            if (window.event)
                charCode = window.event.keyCode;       // IE
            else
                charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <%--<script>
    $("#<%=txtDuration.ClientID %>_bUp").live("click",function(){
        CountInstallment();
    });
    function CountInstallment()
    {
    alert('hiii'
        $("#<%=txtDuration.ClientID %>").click()
    }
</script>--%>
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc2:DeleteReason ID="popup_DeleteReason" runat="server" />
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <%--<asp:Label ID="lblDetialHeader" runat="server" Text="Loan/Advance General Information"
                                        CssClass="form-label"></asp:Label>--%>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Loan And Advance" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Panel ID="fpnlType" runat="server">
                                            <div style="float: right">
                                                <asp:RadioButton ID="radLoan" runat="server" Checked="true" GroupName="LoanAdvance"
                                                    Text="Loan" />
                                                <asp:RadioButton ID="radAdvance" runat="server" GroupName="LoanAdvance" Text="Advance" />
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblVoucherNo" runat="server" Text="Voucher No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtVoucherNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpDate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpName" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmpName" runat="server" Enabled="false" AutoPostBack="true"
                                                data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprovedBy" runat="server" Text="Approved By" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApprovedBy" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLoanScheme" runat="server" Enabled="false" AutoPostBack="true"
                                                data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDeductionPeriod" runat="server" Text="Deduction Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboDeductionPeriod" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblloanamt" runat="server" Text="Loan Amount" CssClass="form-label"></asp:Label>
                                        <asp:Label ID="lblAdvanceAmt" runat="server" Text="Advance Amount" Visible="false"
                                            CssClass="form-label"></asp:Label>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 p-r-0">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtLoanAmt" Style="text-align: right" ReadOnly="true" Text="0" onKeypress="return onlyNumbers(this, event);"
                                                        runat="server" CssClass="form-control"></asp:TextBox>
                                                    <asp:TextBox ID="txtAdvanceAmt" Style="text-align: right" Text="0" runat="server"
                                                        ReadOnly="true" onKeypress="return onlyNumbers(this, event);" Visible="false"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-10">
                                            <asp:Label ID="objlblExRate" Style="margin-left: 5px" runat="server" Text="" CssClass="form-label"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLoanCalcType" runat="server" Text="Loan Calc. Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLoanCalcType" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblInterestCalcType" runat="server" Text="Interest Calc. Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboInterestCalcType" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMappedHead" runat="server" Text="Mapped Head" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboMappedHead" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlProjectedAmt" runat="server">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lnProjectedAmount" runat="server" Text="Projected Amount" CssClass="form-label"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblInterestAmt" runat="server" Text="Interest Ammount" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtInterestAmt" runat="server" Text="0" ReadOnly="true" onKeypress="return onlyNumbers(this, event);"
                                                        Style="text-align: right" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblNetAmount" runat="server" Text="Net Ammount" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtNetAmount" runat="server" Text="0" ReadOnly="true" onKeypress="return onlyNumbers(this, event);"
                                                        Style="text-align: right" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="pnlInformation" runat="server" Style="margin-top: 5px">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="elProjectedINSTLAmt" runat="server" Text="Projected Installment Amount (First Installment)"
                                                CssClass="form-label"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblLoanInterest" runat="server" Text="Rate (%)" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtLoanRate" AutoPostBack="true" runat="server" Style="text-align: right"
                                                        Text="0" onKeypress="return onlyNumbers(this, event);" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblDuration" runat="server" Text="Installment (In Months)" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtDuration" AutoPostBack="true" Text="1" Style="text-align: right"
                                                        onclick="txtDuration_TextChanged" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPrincipalAmt" runat="server" Text="Principal Amt." CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtPrincipalAmt" AutoPostBack="true" runat="server" Style="text-align: right"
                                                        ReadOnly="true" Text="0" onKeypress="return onlyNumbers(this, event);" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblIntAmt" runat="server" Text="Interest Amt." CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtIntAmt" AutoPostBack="true" Text="0" Style="text-align: right"
                                                        ReadOnly="true" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEMIAmount" runat="server" Text="Installment Amt." CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtInstallmentAmt" runat="server" AutoPostBack="true" ReadOnly="true"
                                                        Text="0" onKeypress="return onlyNumbers(this, event);" Style="text-align: right"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPurpose" runat="server" Text="Loan Purpose" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtPurpose" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                        <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                                            <asp:Panel ID="pnl_LoanInterestInfo" runat="server" Visible="true" CssClass="panel panel-primary">
                                                <div class="panel-heading" role="tab" id="headingOne_1">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion_1" 
                                                        href="#collapseOne_1"     aria-expanded="true" aria-controls="collapseOne_1">
                                                            <asp:Label ID="lblLoanInterestInfo" runat="server" Text="Loan Interest Information"
                                                                CssClass="form-label"></asp:Label>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_1">
                                                    <div class="panel-body">
                                                        <asp:Panel ID="pnlGridInterestHistory" ScrollBars="Auto" Style="max-height: 300px"
                                                            runat="server">
                                                            <asp:DataGrid ID="dgvInterestHistory" runat="server" AutoGenerateColumns="false"
                                                                CssClass="table table-hover table-bordered" AllowPaging="false" Width="99%">
                                                                <Columns>
                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnAdd">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="imgAdd" runat="server" ToolTip="Add" CommandName="AddInterest">
                                                                                            <div style="font-size: 20px; color: Green;">
                                                                                                <i class="fa fa-plus"></i>
                                                                                            </div>
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="brnEdit">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="EditInterest"
                                                                                    CssClass="gridedit"></asp:LinkButton></span></ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                                    CommandName="DeleteInterest" CssClass="griddelete"></asp:LinkButton></span></ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="dperiod" HeaderText="Period" ReadOnly="true" FooterText="dgcolhInterestPeriod">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="ddate" HeaderText="Effective Date" ReadOnly="true" FooterText="dgcolhInterestEffectiveDate">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="interest_rate" HeaderText="Interest(%)" ReadOnly="true"
                                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterText="dgcolhInterestRate">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="lninteresttranunkid" HeaderText="" ReadOnly="true" Visible="false"
                                                                        FooterText="objdgcolhlninteresttranunkid"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="pstatusid" HeaderText="" ReadOnly="true" Visible="false"
                                                                        FooterText="objdgcolhIPStatusId"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnl_LoanEMIInfo" runat="server" Visible="true" CssClass="panel panel-primary">
                                                <div class="panel-heading" role="tab" id="headingTwo_1">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1"
                                                            href="#collapseTwo_1" aria-expanded="false" aria-controls="collapseTwo_1">
                                                            <asp:Label ID="lblLoanEMIInfo" runat="server" Text="Loan EMI Information" CssClass="form-label"></asp:Label>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_1">
                                                    <div class="panel-body">
                                                        <asp:Panel ID="pnlGriddgvEMIHistory" ScrollBars="Auto" Style="max-height: 300px"
                                                            runat="server">
                                                            <asp:DataGrid ID="dgvEMIHistory" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                                CssClass="table table-hover table-bordered" Width="99%">
                                                                <Columns>
                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnAdd">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="imgAdd" runat="server" ToolTip="Add" CommandName="AddEMI">
                                                                                                <div style="font-size: 20px; color: Green;"><i class="fa fa-plus"></i></div>
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="brnEdit">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="EditEMI"
                                                                                    CssClass="gridedit"></asp:LinkButton></span></ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                                    CommandName="DeleteEMI" CssClass="griddelete"></asp:LinkButton></span></ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="dperiod" HeaderText="Period" ReadOnly="true" FooterText="dgcolhEMIPeriod">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="ddate" HeaderText="Effective Date" ReadOnly="true" FooterText="dgcolhEMIeffectivedate">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="loan_duration" HeaderText="Loan Duration" ReadOnly="true"
                                                                        Visible="false" FooterText="dgcolhloan_duration" HeaderStyle-HorizontalAlign="Right"
                                                                        ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="emi_amount" HeaderText="Installment Amt." ReadOnly="true"
                                                                        FooterText="dgcolhInstallmentAmt" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="emi_tenure" HeaderText="No. of Installments" ReadOnly="true"
                                                                        FooterText="dgcolhEMINoofInstallment" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="lnemitranunkid" HeaderText="" ReadOnly="true" Visible="false"
                                                                        FooterText="objdgcolhlnemitranunkid"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="pstatusid" HeaderText="" ReadOnly="true" Visible="false"
                                                                        FooterText="dgcolhEPstatusid"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnl_LoanTopupInfo" runat="server" Visible="true" CssClass="panel panel-primary">
                                                <div class="panel-heading" role="tab" id="headingThree_1">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1"
                                                            href="#collapseThree_1" aria-expanded="false" aria-controls="collapseThree_1">
                                                            <asp:Label ID="lblLoanTopupInfo" runat="server" Text="Loan Topup Information" CssClass="form-label"></asp:Label>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_1">
                                                    <div class="panel-body">
                                                        <asp:Panel ID="pnlGriddgvTopupHistory" ScrollBars="Auto" Style="max-height: 300px"
                                                            runat="server">
                                                            <asp:DataGrid ID="dgvTopupHistory" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                                CssClass="table table-hover table-bordered" Width="99%">
                                                                <Columns>
                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnAdd">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="imgAdd" runat="server" ToolTip="Add" CommandName="AddEMI">
                                                                                                <div style="font-size: 20px; color: Green;"><i class="fa fa-plus"></i></div>
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="brnEdit">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="EditTopup"
                                                                                    CssClass="gridedit"></asp:LinkButton></span></ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                                    CommandName="DeleteTopup" CssClass="griddelete"></asp:LinkButton></span></ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="dperiod" HeaderText="Period" ReadOnly="true" FooterText="dgcolhTopupPeriod">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="ddate" HeaderText="Effective Date" ReadOnly="true" FooterText="dgColhTopupEffectiveDate">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="topup_amount" HeaderText="Topup Amount" ReadOnly="true"
                                                                        FooterText="dgcolhTopupAmount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="interest_amount" HeaderText="Topup Interest" ReadOnly="true"
                                                                        Visible="false" FooterText="dgcolhTopupInterest"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="lntopuptranunkid" HeaderText="" ReadOnly="true" Visible="false"
                                                                        FooterText="objdgcolhlntopuptranunkid"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="pstatusid" HeaderText="" ReadOnly="true" Visible="false"
                                                                        FooterText="objdgcolhTPstatusid"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
        $(document).ready(function() {
            $('.panel-36').slideToggle(1200);
        });

        //$('.toggler-36').live("click", function() {
        $("body").on("click", '.toggler-36', function() {
            $(this).next('.panel-36').slideToggle(500);
            $(this).toggleClass("active");
            $(this).children('div').children('i').toggleClass("fa-chevron-circle-down");
            if ($(this).next('.panel-36').css("height") == "1px") {
                PageMethods.DivIdArray($(this).next('.panel-36').parent().attr("id"), true);
            } else {
                PageMethods.DivIdArray($(this).next('.panel-36').parent().attr("id"), false);
            }
        }); 
    </script>

</asp:Content>
