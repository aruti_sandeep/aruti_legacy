﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Net.Dns
#End Region

Partial Class Loan_Savings_New_Loan_Loan_Assignment_wPg_LoanAdvanceOperationList
    Inherits Basepage

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmLoanAdvanceOperationList"
    Private ReadOnly mstrModuleName1 As String = "frmLoanParameter"
    Private DisplayMessage As New CommonCodes

    Private objLnAdv As clsLoan_Advance
    Private objLoanRate As New clslnloan_interest_tran
    Private objLoanEMI As New clslnloan_emitenure_tran
    Private objLoanTopup As New clslnloan_topup_tran
    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private objlnOpApprovaltran As clsloanotherop_approval_tran
    'Nilay (01-Apr-2016) -- End

    'Nilay (06-Aug-2016) -- Start
    'CHANGES : Replace Query String with Session and ViewState
    'Private mintVocNo As Integer = -1
    'Private mstrEmployee As String = ""
    'Private mstrLoanScheme As String = ""
    'Nilay (06-Aug-2016) -- END

    Private mintLoanAdvanceTranunkid As Integer = -1
    Private mintEmployeeId As Integer = -1
    Private mdtLoanEffectiveDate As String = ""
    Private mintItemIndex As Integer = -1
    Private mintTabUnkid As Integer = -1
    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private mintlnOtherOpTranunkid As Integer = 0
    'Nilay (01-Apr-2016) -- End


    '=============================Loan Paramteres========================='
    Private mintParameterId As enParameterMode
    Private mintCalcTypeId As enLoanCalcId
    Private mintInterestCalcTypeId As enLoanInterestCalcType
    'Private mblnDoNotAllowChangePeriod As Boolean = False 'Nilay (01-Apr-2016)
    Private mblnPopupShowHide As Boolean = False
    Private mintCountryUnkid As Integer = -1
    Private mintPaidCurrId As Integer = 0
    Private mdecPrincipalAmnt As Decimal = 0
    Private mdecInterestAmnt As Decimal = 0
    Private mdecInstallmentAmnt As Decimal = 0
    Private mdecTopupAmnt As Decimal = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mstrBaseCurrSign As String = ""
    Private mdsLoanInfo As New DataSet
    Private mdtBalanceEndDate As Date = Nothing
    'Nilay (25-Mar-2016) -- Start
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    'Nilay (25-Mar-2016) -- End

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private mblnAllowChangeStatus As Boolean = False
    Private mdecInterestRate As Decimal = 0
    Private mintNoOfInstl As Integer = 1
    Private mintLoanSchemeunkid As Integer = 0
    'Nilay (01-Apr-2016) -- End

    'Shani (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification 
    Private objCONN As SqlConnection
    Private mintlnOptranId As Integer = 0
    'Shani (21-Jul-2016) -- End

    'Nilay (13-Sept-2016) -- Start
    'Enhancement : Enable Default Parameter Edit & other fixes
    Private mActionMode As enAction
    'Nilay (13-Sept-2016) -- End

    'Nilay (13-Sept-2016) -- Start
    'Enhancement : Enable Default Parameter Edit & other fixes
    Private mblnIsDefault As Boolean = True
    'Nilay (13-Sept-2016) -- End

    'Nilay (10-Dec-2016) -- Start
    'Issue #26: Setting to be included on configuration for Loan flow Approval process
    Private mblnIsSendEmailNotification As Boolean = False
    'Nilay (10-Dec-2016) -- End

#End Region

#Region "Page's Events"
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                'Sohail (02 Apr 2019) -- Start
                'NMB Issue - 74.1 - Error "On Load Event !! Bad Data" on clicking any page with link after session get expired.
                If Request.QueryString.Count <= 0 Then Exit Sub
                'Sohail (02 Apr 2019) -- End
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                txtEmployeeName.Text = arr(0).ToString
                txtLoanScheme.Text = arr(1).ToString
                txtVocNo.Text = arr(2).ToString
                mintLoanAdvanceTranunkid = CInt(arr(3))
                mintlnOptranId = CInt(arr(4))
                If arr.Length > 0 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try
                    Blank_ModuleName()
                    clsCommonATLog._WebFormName = "frmPerformanceEvaluation"
                    StrModuleName2 = "mnuAssessment"
                    StrModuleName3 = "mnuPerformaceEvaluation"
                    clsCommonATLog._WebClientIP = Session("IP_ADD").ToString
                    clsCommonATLog._WebHostName = Session("HOST_NAME").ToString
                    Me.ViewState.Add("IsDirect", True)

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(5))
                    HttpContext.Current.Session("UserId") = CInt(arr(6))

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ''ArtLic._Object = New ArutiLic(False)
                    ''If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                    ''    Dim objGroupMaster As New clsGroup_Master
                    ''    objGroupMaster._Groupunkid = 1
                    ''    ArtLic._Object.HotelName = objGroupMaster._Groupname
                    ''End If

                    ''If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                    ''    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                    ''    Exit Sub
                    ''End If

                    ''If ConfigParameter._Object._IsArutiDemo Then
                    ''    If ConfigParameter._Object._IsExpire Then
                    ''        DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                    ''        Exit Try
                    ''    Else
                    ''        If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                    ''            DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                    ''            Exit Try
                    ''        End If
                    ''    End If
                    ''End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If
                    Session("LoanApprover_ForLoanScheme") = clsConfig._IsLoanApprover_ForLoanScheme
                    Session("_LoanVocNoType") = clsConfig._LoanVocNoType

                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))

                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If
                    'Dim objUserPrivilege As New clsUserPrivilege
                    'objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                    'Session("AllowtoAddReviewerEvaluation") = objUserPrivilege._AllowtoAddReviewerEvaluation
                    objlnOpApprovaltran = New clsloanotherop_approval_tran
                    objlnOpApprovaltran._LoanOtherOptranunkid = mintlnOptranId
                    If objlnOpApprovaltran._Visibleid <> 1 Then
                        If objlnOpApprovaltran._Visibleid = 2 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 17, "You can't Edit this Loan detail. Reason: This Loan is already Approved."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        ElseIf objlnOpApprovaltran._Visibleid = 3 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 18, "You can't Edit this Loan detail. Reason: This Loan is already Rejected."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 19, "You can't Edit this Loan detail. Reason: This Loan is already Approved/Rejected."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If
                    End If
                    objlnOpApprovaltran = Nothing
                    GoTo Link
                End If

            End If
            'Shani (21-Jul-2016) -- End

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
Link:
            If IsPostBack = False Then

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromDesktopMSS"))

                    'Varsha Rana (12-Sept-2017) -- Start
                    'Enhancement - Loan Topup in ESS
                    dgPendingOp.Columns(0).Visible = True
                    lvData.Columns(0).Visible = True
                    ' Varsha Rana (12-Sept-2017) -- End
                ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromESS"))
                    'Varsha Rana (12-Sept-2017) -- Start
                    'Enhancement - Loan Topup in ESS
                    dgPendingOp.Columns(0).Visible = False
                    lvData.Columns(0).Visible = False
                    lvData.Columns(1).Visible = False
                    chkMyApproval.Checked = False
                    chkMyApproval.Visible = False
                    radInterestRate.Visible = False
                    radInstallment.Visible = False
                    ' Varsha Rana (12-Sept-2017) -- End
                End If
                'Nilay (10-Dec-2016) -- End

                Call SetLanguage()

                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'If Request.QueryString.Count > 0 Then
                '    Dim array() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                '    If array.Length > 0 Then
                '        mstrEmployee = CStr(array(0))
                '        mstrLoanScheme = CStr(array(1))
                '        mintVocNo = CInt(array(2))
                '        mintLoanAdvanceTranunkid = CInt(array(3))
                '    End If
                '    txtEmployeeName.Text = mstrEmployee
                '    txtLoanScheme.Text = mstrLoanScheme
                '    txtVocNo.Text = mintVocNo.ToString
                If Session("VocNo") IsNot Nothing Then
                    txtVocNo.Text = Session("VocNo").ToString
                    Session("VocNo") = Nothing
                End If
                If Session("Employee") IsNot Nothing Then
                    txtEmployeeName.Text = Session("Employee").ToString
                    Session("Employee") = Nothing
                End If
                If Session("LoanScheme") IsNot Nothing Then
                    txtLoanScheme.Text = Session("LoanScheme").ToString
                    Session("LoanScheme") = Nothing
                End If
                If Session("LoanAdvanceTranunkid") IsNot Nothing Then
                    mintLoanAdvanceTranunkid = CInt(Session("LoanAdvanceTranunkid"))
                    Session("LoanAdvanceTranunkid") = Nothing
                End If
                'End If

                'Nilay (06-Aug-2016) -- END

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Call SetDateFormat()
                'Pinkal (16-Apr-2016) -- End

                Dim objLnAdv As New clsLoan_Advance
                objLnAdv._Loanadvancetranunkid = mintLoanAdvanceTranunkid
                mintEmployeeId = objLnAdv._Employeeunkid
                mdtLoanEffectiveDate = CStr(objLnAdv._Effective_Date)

                objlblEffDate.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Loan Start Date :") & " " & objLnAdv._Effective_Date.ToShortDateString

                objLnAdv = Nothing

                If lvData.Items.Count <= 0 Then
                    lvData.DataSource = New List(Of String)
                    lvData.DataBind()
                End If

                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                'Call Fill_List()
                If dgPendingOp.Items.Count <= 0 Then
                    dgPendingOp.DataSource = New List(Of String)
                    dgPendingOp.DataBind()
                End If

                Call FillComboOpList()
                Call Fill_List()
                Call FillPendingOpList()
                'Nilay (01-Apr-2016) -- End

                'Call Load_LoanParameters()

                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                If Request.QueryString.Count > 0 Then
                    For Each dgItem As DataGridItem In dgPendingOp.Items
                        If CInt(dgItem.Cells(18).Text) = mintlnOptranId Then
                            Dim lnk As LinkButton = CType(dgItem.Cells(0).FindControl("lnkChangeStatus"), LinkButton)
                            Call dgPendingOp_ItemCommand(lnk, New DataGridCommandEventArgs(dgItem, lnk.CommandName, New CommandEventArgs(lnk.CommandName, lnk.CommandArgument)))
                        End If
                    Next
                End If
                'Shani (21-Jul-2016) -- End
            Else
                mintLoanAdvanceTranunkid = CInt(Me.ViewState("LoanAdvanceTranunkid"))
                mintEmployeeId = CInt(Me.ViewState("EmployeeId"))
                mdtLoanEffectiveDate = Me.ViewState("LoanEffectiveDate").ToString
                mintItemIndex = CInt(Me.ViewState("ItemIndex"))
                mintTabUnkid = CInt(Me.ViewState("TableUnkid"))
                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                mintlnOtherOpTranunkid = CInt(Me.ViewState("LoanOtherOpTranunkid"))
                'Nilay (01-Apr-2016) -- End


                '=============================Loan Paramteres========================='

                mintParameterId = CType(Me.ViewState("ParameterId"), enParameterMode)
                mintCalcTypeId = CType(Me.ViewState("CalcTypeId"), enLoanCalcId)
                mintInterestCalcTypeId = CType(Me.ViewState("InterestCalcTypeId"), enLoanInterestCalcType)
                mintCountryUnkid = CInt(Me.ViewState("CountryUnkid"))
                mintPaidCurrId = CInt(Me.ViewState("PaidCurrId"))
                mdecPrincipalAmnt = CDec(Me.ViewState("PrincipalAmnt"))
                mdecInterestAmnt = CDec(Me.ViewState("InterestAmnt"))
                mdecBaseExRate = CDec(Me.ViewState("BaseExRate"))
                mdecPaidExRate = CDec(Me.ViewState("PaidExRate"))
                mdecInstallmentAmnt = CDec(Me.ViewState("InstallmentAmount"))
                mdecTopupAmnt = CDec(Me.ViewState("TopupAmount"))
                mstrBaseCurrSign = CStr(Me.ViewState("BaseCurrSign"))
                mblnPopupShowHide = CBool(Me.ViewState("PopupShowHide"))
                'mblnDoNotAllowChangePeriod = CBool(Me.ViewState("DoNotAllowChangePeriod")) 'Nilay (01-Apr-2016)
                mdtBalanceEndDate = CDate(Me.ViewState("BalanceEndDate"))
                mdsLoanInfo = CType(Me.Session("LoanInfo"), DataSet)
                'Nilay (25-Mar-2016) -- Start
                mdtStartDate = CDate(Me.ViewState("PeriodStartDate"))
                mdtEndDate = CDate(Me.ViewState("PeriodEndDate"))
                'Nilay (25-Mar-2016) -- End

                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                mblnAllowChangeStatus = CBool(Me.ViewState("AllowChangeStatus"))
                mdecInterestRate = CDec(Me.ViewState("InterestRate"))
                mintNoOfInstl = CInt(Me.ViewState("NoOfInstallment"))
                mintLoanSchemeunkid = CInt(Me.ViewState("LoanSchemeunkid"))
                'Nilay (01-Apr-2016) -- End

                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                mActionMode = CType(Me.ViewState("Action"), enAction)
                'Nilay (13-Sept-2016) -- End

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                mblnIsSendEmailNotification = CBool(Me.ViewState("SendEmailNotification"))
                'Nilay (10-Dec-2016) -- End

                If mblnPopupShowHide = True Then
                    popupLoanParameter.Show()
                Else
                    popupLoanParameter.Hide()
                End If

                Call FillPendingOpList() 'Nilay (01-Apr-2016)

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("LoanAdvanceTranunkid") = mintLoanAdvanceTranunkid
            Me.ViewState("EmployeeId") = mintEmployeeId
            Me.ViewState("LoanEffectiveDate") = mdtLoanEffectiveDate
            Me.ViewState("ItemIndex") = mintItemIndex
            Me.ViewState("TableUnkid") = mintTabUnkid
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            Me.ViewState("LoanOtherOpTranunkid") = mintlnOtherOpTranunkid
            'Nilay (01-Apr-2016) -- End

            '=============================Loan Paramteres========================='
            Me.ViewState("ParameterId") = mintParameterId
            Me.ViewState("CalcTypeId") = mintCalcTypeId
            Me.ViewState("InterestCalcTypeId") = mintInterestCalcTypeId
            Me.ViewState("CountryUnkid") = mintCountryUnkid
            Me.ViewState("PaidCurrId") = mintPaidCurrId
            Me.ViewState("PrincipalAmnt") = mdecPrincipalAmnt
            Me.ViewState("InterestAmnt") = mdecInterestAmnt
            Me.ViewState("BaseExRate") = mdecBaseExRate
            Me.ViewState("PaidExRate") = mdecPaidExRate
            Me.ViewState("InstallmentAmount") = mdecInstallmentAmnt
            Me.ViewState("TopupAmount") = mdecTopupAmnt
            Me.ViewState("BaseCurrSign") = mstrBaseCurrSign
            Me.ViewState("PopupShowHide") = mblnPopupShowHide
            'Me.ViewState("DoNotAllowChangePeriod") = mblnDoNotAllowChangePeriod 'Nilay (01-Apr-2016)
            Me.ViewState("BalanceEndDate") = mdtBalanceEndDate
            Me.Session("LoanInfo") = mdsLoanInfo
            'Nilay (25-Mar-2016) -- Start
            Me.ViewState("PeriodStartDate") = mdtStartDate
            Me.ViewState("PeriodEndDate") = mdtEndDate
            'Nilay (25-Mar-2016) -- End

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            Me.ViewState("AllowChangeStatus") = mblnAllowChangeStatus
            Me.ViewState("InterestRate") = mdecInterestRate
            Me.ViewState("NoOfInstallment") = mintNoOfInstl
            Me.ViewState("LoanSchemeunkid") = mintLoanSchemeunkid
            'Nilay (01-Apr-2016) -- End

            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            Me.ViewState("Action") = mActionMode
            'Nilay (13-Sept-2016) -- End

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            Me.ViewState("IsDefault") = mblnIsDefault
            'Nilay (20-Sept-2016) -- End

            'Nilay (10-Dec-2016) -- Start
            'Issue #26: Setting to be included on configuration for Loan flow Approval process
            Me.ViewState("SendEmailNotification") = mblnIsSendEmailNotification
            'Nilay (10-Dec-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Shani (21-Jul-2016) -- Start
        'Enhancement - Create New Loan Notification 
        'Me.IsLoginRequired = True
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
        'Shani (21-Jul-2016) -- End
    End Sub

#End Region

#Region "Private Methods"

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private Sub FillComboOpList()
        Try
            Dim dsCombo As DataSet = Nothing
            Dim objLoanApplication As New clsProcess_pending_loan

            dsCombo = objLoanApplication.GetLoan_Status("List", False, False)
            With cboStatusSearch
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillComboOpList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01-Apr-2016) -- End

    Private Sub Fill_List()
        Dim dsList As New DataSet
        objLnAdv = New clsLoan_Advance
        Try
            dsList = objLnAdv.GetLoanOperationList(mintLoanAdvanceTranunkid, "List")

            lvData.AutoGenerateColumns = False

            If dsList.Tables(0).Rows.Count > 0 Then

                Dim dtTable As DataTable = dsList.Tables(0).Clone
                Dim mDicAdded As New Dictionary(Of Integer, Integer)

                Dim dtRow As DataRow = Nothing
                For Each xRow As DataRow In dsList.Tables(0).Rows
                    If mDicAdded.ContainsKey(CInt(xRow("periodunkid"))) = False Then
                        dtRow = dtTable.NewRow
                        dtRow("PeriodName") = xRow("PeriodName")
                        dtRow("EffDate") = xRow("PeriodName")
                        dtRow("periodunkid") = xRow("periodunkid")
                        dtRow("IdType") = 0
                        dtRow("isdefault") = True
                        dtTable.Rows.Add(dtRow)
                        mDicAdded.Add(CInt(xRow("periodunkid")), CInt(xRow("periodunkid")))

                        Dim dFilter As DataTable = New DataView(dsList.Tables(0), "periodunkid = '" & CInt(xRow("periodunkid")) & "'", "", DataViewRowState.CurrentRows).ToTable
                        For Each frow As DataRow In dFilter.Rows
                            'Nilay (01-Apr-2016) -- Start
                            'ENHANCEMENT - Approval Process in Loan Other Operations
                            If IsDBNull(frow("EffDate")) = False AndAlso frow("EffDate").ToString.Trim <> "" Then
                                'frow("EffDate") = CDate(frow("EffDate")).ToString("M/d/yyyy", New System.Globalization.CultureInfo(""))
                                frow("EffDate") = CDate(frow("EffDate")).ToString(Session("DateFormat").ToString, New System.Globalization.CultureInfo(""))
                            End If
                            'Nilay (01-Apr-2016) -- End
                            dtTable.ImportRow(frow)
                        Next
                    End If
                Next


                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                'Dim row = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isdefault") = False)
                'If row.Count > 0 Then
                '    mblnIsDefault = False
                'End If
                'Nilay (13-Sept-2016) -- End
                Dim row = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isdefault") = False)
                If row.Count > 0 Then
                    mblnIsDefault = False
                    lvData.Columns(0).Visible = False
                    lvData.Columns(1).Visible = True
                Else
                    mblnIsDefault = True
                End If

                'Varsha Rana (12-Sept-2017) -- Start
                'Enhancement - Loan Topup in ESS
                'If mblnIsDefault = True Then
                If mblnIsDefault = True AndAlso CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ' Varsha Rana (12-Sept-2017) -- End
                    lvData.Columns(0).Visible = True
                    lvData.Columns(1).Visible = False
                End If
                'Nilay (20-Sept-2016) -- End

                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                'Dim dsBal As New DataSet
                'objLnAdv = New clsLoan_Advance
                'dsBal = objLnAdv.GetLastLoanBalance("List", mintLoanAdvanceTranunkid)
                'If dsBal.Tables(0).Rows.Count > 0 Then
                '    lvData.Columns(0).Visible = False
                'Else
                '    lvData.Columns(0).Visible = True
                'End If
                'Nilay (01-Apr-2016) -- End

                lvData.DataSource = dtTable 'dsList.Tables("List")
                lvData.DataBind()
            Else
                lvData.DataSource = New List(Of String)
                lvData.DataBind()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_List:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private Sub FillPendingOpList()
        Try
            Dim strSearchFilter As String = String.Empty
            Dim dsList As DataSet = Nothing
            objlnOpApprovaltran = New clsloanotherop_approval_tran

            If CInt(cboStatusSearch.SelectedValue) > 0 Then
                strSearchFilter = " lnloanotherop_approval_tran.final_status = " & CInt(cboStatusSearch.SelectedValue)
            End If

            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            If Request.QueryString.Count > 0 Then
                If mintlnOptranId > 0 Then
                    strSearchFilter &= " AND lnloanotherop_approval_tran.lnotheroptranunkid = '" & mintlnOptranId & "' "
                End If
            End If
            'Shani (21-Jul-2016) -- End

            dsList = objlnOpApprovaltran.getPendingOpList(mintLoanAdvanceTranunkid, "List", strSearchFilter)

            Dim mintloanadvancetranceunkid As Integer = 0
            Dim mintLoanOperationtypeId As Integer = 0
            Dim mstrIdentifyGuid As String = String.Empty
            Dim dtList As DataTable = Nothing
            Dim mstrStatus As String = String.Empty

            If dsList.Tables("List") IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then
                For Each drRow As DataRow In dsList.Tables("List").Rows
                    If CInt(drRow("lnotheroptranunkid")) <= 0 Then Continue For
                    mstrStatus = ""
                    If mintloanadvancetranceunkid <> CInt(drRow("loanadvancetranunkid")) OrElse mintLoanOperationtypeId <> CInt(drRow("lnoperationtypeid")) _
                                    OrElse mstrIdentifyGuid <> CStr(drRow("identify_guid")) Then
                        dtList = New DataView(dsList.Tables("List"), "employeeunkid= " & CInt(drRow("employeeunkid")) & _
                                              " AND loanadvancetranunkid= " & CInt(drRow("loanadvancetranunkid")) & _
                                              " AND lnoperationtypeid = " & CInt(drRow("lnoperationtypeid")) & _
                                              " AND identify_guid = '" & CStr(drRow("identify_guid")) & "' ", "", DataViewRowState.CurrentRows).ToTable
                        mintloanadvancetranceunkid = CInt(drRow("loanadvancetranunkid"))
                        mintLoanOperationtypeId = CInt(drRow("lnoperationtypeid"))
                        mstrIdentifyGuid = CStr(drRow("identify_guid"))
                    End If
                    If dtList IsNot Nothing AndAlso dtList.Rows.Count > 0 Then
                        Dim dR As DataRow() = dtList.Select("priority >= " & CInt(drRow("priority")))
                        If dR.Length > 0 Then
                            For i As Integer = 0 To dR.Length - 1
                                If CInt(drRow("statusunkid")) = 2 Then
                                    mstrStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Approved By :-  ") & drRow("ApproverName").ToString()
                                    Exit For
                                ElseIf CInt(drRow("statusunkid")) = 1 Then
                                    If CInt(dR(i)("statusunkid")) = 2 Then
                                        mstrStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Approved By :-  ") & dR(i)("ApproverName").ToString()
                                        Exit For
                                    ElseIf CInt(dR(i)("statusunkid")) = 3 Then
                                        mstrStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Rejected By :-  ") & dR(i)("ApproverName").ToString()
                                        Exit For
                                    End If
                                ElseIf CInt(drRow("statusunkid")) = 3 Then
                                    mstrStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Rejected By :-  ") & dR(i)("ApproverName").ToString()
                                    Exit For
                                End If
                            Next
                        End If
                    End If
                    If mstrStatus <> "" Then
                        drRow("Status") = mstrStatus.Trim
                    End If
                Next
            End If

            Dim dtTable As New DataTable
            If chkMyApproval.Checked Then
                dtTable = New DataView(dsList.Tables(0), "MappedUserId = " & CInt(Session("UserId")) & " OR MappedUserId <= 0", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            If dtTable.Rows.Count = 1 AndAlso CInt(dtTable.Rows(0).Item("lnotheroptranunkid")) <= 0 Then
                dgPendingOp.DataSource = New List(Of String)
                dgPendingOp.DataBind()
                Exit Sub
            End If
            'Nilay (21-Jul-2016) -- End

            Dim dtTran As DataTable = dtTable.Clone

            Dim dCol As New DataColumn
            dCol.DataType = GetType(Integer)
            dCol.DefaultValue = 0
            dCol.ColumnName = "grpRow"
            dtTable.Columns.Add(dCol)

            Dim strGuid As String = ""
            Dim isgrp As Boolean = False

            'Dim dRow As DataRow = Nothing

            '            For Each dtRow As DataRow In dtTable.Rows
            '                If CBool(dtRow.Item("isgrp")) = True Then
            '                    isgrp = True
            '                    GoTo AddRow
            '                End If

            '                If (dtRow.Item("identify_guid").ToString <> strGuid) Then
            '                    If isgrp = True Then
            '                        isgrp = CBool(dtRow.Item("isgrp"))
            '                        strGuid = dtRow.Item("identify_guid").ToString
            '                        GoTo AddRow
            '                    End If
            '                    dRow = dtTran.NewRow
            '                    strGuid = dtRow.Item("identify_guid").ToString
            '                    dRow.Item("grpRow") = 1
            '                    dRow.Item("isgrp") = False
            '                    dtTran.Rows.Add(dRow)
            '                End If
            'AddRow:
            '                dRow = dtTran.NewRow
            '                dRow.Item("grpRow") = 0
            '                For Each dtCol As DataColumn In dtTable.Columns
            '                    dRow.Item(dtCol.ColumnName) = dtRow.Item(dtCol.ColumnName)
            '                Next
            '                dtTran.Rows.Add(dRow)
            '            Next

            For i As Integer = 0 To dtTable.Rows.Count - 1
                If CBool(dtTable.Rows(i).Item("isgrp")) = True Then
                    isgrp = True
                    Continue For
                End If
                If (dtTable.Rows(i).Item("identify_guid").ToString <> strGuid) Then
                    If isgrp = True Then
                        isgrp = CBool(dtTable.Rows(i).Item("isgrp"))
                        strGuid = dtTable.Rows(i).Item("identify_guid").ToString
                        Continue For
                    End If
                    strGuid = dtTable.Rows(i).Item("identify_guid").ToString
                    dtTable.Rows.InsertAt(dtTable.NewRow, i)
                    dtTable.Rows(i).Item("grpRow") = 1
                    dtTable.Rows(i).Item("isgrp") = False
                End If
            Next

            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'If CInt(cboStatusSearch.SelectedValue) = 1 Then 'Pending


            'Varsha Rana (12-Sept-2017) -- Start
            'Enhancement - Loan Topup in ESS
            'If CInt(cboStatusSearch.SelectedValue) = enLoanApplicationStatus.PENDING Then
            If CInt(cboStatusSearch.SelectedValue) = enLoanApplicationStatus.PENDING AndAlso CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                ' Varsha Rana (12-Sept-2017) -- End
                'Nilay (20-Sept-2016) -- End
                dgPendingOp.Columns(0).Visible = True
            Else
                dgPendingOp.Columns(0).Visible = False
            End If
            'Nilay (21-Jul-2016) -- End

            dgPendingOp.DataSource = dtTable
            dgPendingOp.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "FillPendingOpList", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01-Apr-2016) -- End

    Private Function IsEditAllowed(ByVal xColumnToMatch As String, ByVal xColumnUnkid As Integer) As Boolean
        Try
            Dim objLnAdv As New clsLoan_Advance
            Dim ds As DataSet = objLnAdv.GetLastLoanBalance("List", mintLoanAdvanceTranunkid)
            Dim strMsg As String = String.Empty

            If Not (ds.Tables("List").Rows.Count > 0 AndAlso CInt(ds.Tables("List").Rows(0).Item(xColumnToMatch)) = xColumnUnkid) Then
                If ds.Tables("List").Rows.Count > 0 Then
                    If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                        'Language.setLanguage(mstrModuleName)
                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clslnloan_interest_tran", 3, " Process Payroll.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
                        'Language.setLanguage(mstrModuleName)
                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clslnloan_interest_tran", 4, " Loan Payment List.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
                        'Language.setLanguage(mstrModuleName)
                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clslnloan_interest_tran", 5, " Receipt Payment List.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                        'Language.setLanguage(mstrModuleName)
                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clslnloan_interest_tran", 6, " Loan Installment Change.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                        'Language.setLanguage(mstrModuleName)
                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clslnloan_interest_tran", 7, " Loan Topup Added.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                        'Language.setLanguage(mstrModuleName)
                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clslnloan_interest_tran", 8, " Loan Rate Change.")
                    End If
                End If
            End If
            objLnAdv = Nothing
            If strMsg.Trim.Length > 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Sorry, You cannot edit this transaction. Please delete last transaction from the screen of") & strMsg, Me)
                Return False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsEditAllowed:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function


#End Region

#Region "DataGrid's Event"

    Protected Sub lvData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvData.ItemCommand
        Try
            mintItemIndex = e.Item.ItemIndex

            If e.CommandName.ToUpper = "EDIT" Then

                Select Case mintItemIndex
                    Case enParameterMode.LN_RATE
                        If IsEditAllowed("lninteresttranunkid", CInt(e.Item.Cells(13).Text)) = False Then Exit Sub
                    Case enParameterMode.LN_EMI
                        If IsEditAllowed("lnemitranunkid", CInt(e.Item.Cells(13).Text)) = False Then Exit Sub
                    Case enParameterMode.LN_TOPUP
                        If IsEditAllowed("lntopuptranunkid", CInt(e.Item.Cells(13).Text)) = False Then Exit Sub
                End Select

                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                Dim dsList As DataSet = Nothing
                Dim strFilter As String = ""
                objlnOpApprovaltran = New clsloanotherop_approval_tran

                strFilter = " lnloanotherop_approval_tran.final_status = 1 AND lnloanotherop_approval_tran.statusunkid = 2 AND lnloanotherop_approval_tran.final_approved = 0 "

                dsList = objlnOpApprovaltran.getPendingOpList(mintLoanAdvanceTranunkid, "List", strFilter)
                Dim dRow As DataRow() = dsList.Tables("List").Select("isgrp=0")
                If dRow.Length > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Sorry, you cannot edit default operation. Reason: Some of the pending operation transaction(s) in approval process."), Me)
                    Exit Sub
                End If
                'Nilay (13-Sept-2016) -- End

                mintTabUnkid = CInt(e.Item.Cells(13).Text)
                mintParameterId = CType(CInt(e.Item.Cells(10).Text), enParameterMode)
                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                mblnAllowChangeStatus = False
                mActionMode = enAction.EDIT_ONE
                'Nilay (13-Sept-2016) -- End

                Call Load_LoanParameters()
                popupLoanParameter.Show()
                mblnPopupShowHide = True

            End If

            If e.CommandName.ToUpper = "DELETE" Then
                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                'If eZeeDate.convertDate(mdtLoanEffectiveDate) = eZeeDate.convertDate(e.Item.Cells(12).Text) OrElse CInt(e.Item.Cells(11).Text) = 0 Then
                If CInt(e.Item.Cells(11).Text) = 2 Then
                    'Nilay (01-Apr-2016) -- End
                    DisplayMessage.DisplayMessage("Sorry, you cannot delete this transaction. Reason: Transaction period is already closed.", Me)
                Else
                    Dim objTnaLeaveTran As New clsTnALeaveTran
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(e.Item.Cells(9).Text)
                    mdtStartDate = objPeriod._Start_Date
                    mdtEndDate = objPeriod._End_Date

                    If objTnaLeaveTran.IsPayrollProcessDone(CInt(e.Item.Cells(9).Text), mintEmployeeId.ToString, CDate(objPeriod._End_Date)) Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Sorry, you cannot delete this entry. Reason : Payroll is already processed for this selected employee and period."), Me)
                        objPeriod = Nothing
                        objTnaLeaveTran = Nothing
                        Exit Sub
                    End If
                    objTnaLeaveTran = Nothing
                    objPeriod = Nothing

                    mintTabUnkid = CInt(e.Item.Cells(13).Text)
                    mintParameterId = CType(CInt(e.Item.Cells(10).Text), enParameterMode)

                    popupDeleteReason.Reason = ""
                    popupDeleteReason.Show()

                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lvData_ItemCommand" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles lvData.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Varsha Rana (12-Sept-2017) -- Start
            'Enhancement - Loan Topup in ESS
            'If e.Item.ItemIndex >= 0 Then

            '    If CInt(e.Item.Cells(10).Text) = 0 Then
            '        CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
            '        CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
            '        e.Item.Cells(0).Visible = False
            '        e.Item.Cells(1).Visible = False
            '        e.Item.Cells(2).ColumnSpan = e.Item.Cells.Count - 1

            '        For i = 3 To e.Item.Cells.Count - 1
            '            e.Item.Cells(i).Visible = False
            '        Next

            '        e.Item.Cells(2).CssClass = "GroupHeaderStyleBorderLeft"
            '        e.Item.Cells(2).Style.Add("text-align", "left")

            '        'Nilay (01-Apr-2016) -- Start
            '        'ENHANCEMENT - Approval Process in Loan Other Operations
            '    Else
            '        'Nilay (13-Sept-2016) -- Start
            '        'Enhancement : Enable Default Parameter Edit & other fixes
            '        'If CBool(e.Item.Cells(14).Text) = True Then
            '        '    CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
            '        '    For i = 2 To e.Item.Cells.Count - 1
            '        '        e.Item.Cells(i).Style.Add("color", "blue")
            '        '    Next
            '        'End If
            '        If CBool(e.Item.Cells(14).Text) = True Then
            '            'Dim row = CType(lvData.DataSource, DataTable).Rows.Cast(Of DataRow).AsEnumerable().Where(Function(x) x.Field(Of Integer)("IdType") <> 0 AndAlso x.Field(Of Boolean)("isdefault") = False)
            '            CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = mblnIsDefault
            '            CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
            '            For i = 2 To e.Item.Cells.Count - 1
            '                e.Item.Cells(i).Style.Add("color", "blue")
            '            Next
            '        Else
            '            CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
            '            'Nilay (20-Sept-2016) -- Start
            '            'Enhancement : Cancel feature for approved but not assigned loan application
            '            CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = Not mblnIsDefault
            '            'Nilay (20-Sept-2016) -- End
            '        End If
            '        'Nilay (13-Sept-2016) -- End

            '        'If e.Item.Cells(2).Text.Trim <> "" AndAlso e.Item.Cells(2).Text <> "&nbsp;" Then
            '        '    e.Item.Cells(2).Text = CDate(eZeeDate.convertDate(e.Item.Cells(2).Text)).ToString(Session("DateFormat").ToString)
            '        'End If
            '        'Nilay (01-Apr-2016) -- End

            '    End If
            'End If
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                ' e.Item.Cells(0).Visible = False
                e.Item.Cells(1).Visible = False
                If e.Item.ItemIndex >= 0 Then
                    If CInt(e.Item.Cells(10).Text) = 0 Then
                        CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                        CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
                        e.Item.Cells(0).Visible = False
                        e.Item.Cells(1).Visible = False
                        e.Item.Cells(2).ColumnSpan = e.Item.Cells.Count - 1

                        For i = 3 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).Visible = False
                        Next

                        e.Item.Cells(2).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(2).Style.Add("text-align", "left")

                    Else
                        If CBool(e.Item.Cells(14).Text) = True Then

                            CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                            CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
                            For i = 2 To e.Item.Cells.Count - 1
                                e.Item.Cells(i).Style.Add("color", "blue")
                            Next
                        Else
                            CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                            CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False

                        End If
                    End If
                End If

            Else
                If e.Item.ItemIndex >= 0 Then

                    If CInt(e.Item.Cells(10).Text) = 0 Then
                        CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                        CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
                        e.Item.Cells(0).Visible = False
                        e.Item.Cells(1).Visible = False
                        e.Item.Cells(2).ColumnSpan = e.Item.Cells.Count - 1
                        For i = 3 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).Visible = False
                        Next

                        e.Item.Cells(2).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(2).Style.Add("text-align", "left")

                    Else
                        If CBool(e.Item.Cells(14).Text) = True Then
                            CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = mblnIsDefault
                            CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
                            For i = 2 To e.Item.Cells.Count - 1
                                e.Item.Cells(i).Style.Add("color", "blue")
                            Next
                        Else
                            CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                            CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = Not mblnIsDefault
                        End If
                    End If
                End If
            End If
            ' Varsha Rana (12-Sept-2017) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lvData_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Protected Sub dgPendingOp_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgPendingOp.ItemCommand
        Try
            If e.CommandName.ToUpper = "CHANGESTATUS" Then

                Dim mstrFilter As String = ""
                Dim dsList As DataSet = Nothing
                objlnOpApprovaltran = New clsloanotherop_approval_tran

                mintlnOtherOpTranunkid = CInt(e.Item.Cells(18).Text)
                mintLoanAdvanceTranunkid = CInt(e.Item.Cells(22).Text)
                mintParameterId = CType(CInt(e.Item.Cells(12).Text), enParameterMode)
                mblnAllowChangeStatus = True

                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                mActionMode = enAction.EDIT_ONE
                'Nilay (13-Sept-2016) -- End

                If CBool(objlnOpApprovaltran.IsAllowedPendingOpChangeStatus(CStr(e.Item.Cells(19).Text), _
                                                                            CDate(e.Item.Cells(2).Text).Date, _
                                                                            mintLoanAdvanceTranunkid, _
                                                                            CInt(e.Item.Cells(14).Text)) = False) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Sorry, you cannot change status. Reason: Previous operation is not final approved yet."), Me)
                    Exit Sub
                End If

                If CInt(Session("UserId")) <> CInt(e.Item.Cells(13).Text) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "You cannot change status of this operation. Reason: You are logged in into another user login."), Me)
                    Exit Sub
                End If

                If CInt(e.Item.Cells(21).Text) = 2 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                    Exit Sub
                ElseIf CInt(e.Item.Cells(21).Text) = 3 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                    Exit Sub
                End If

                mstrFilter &= " lnloanotherop_approval_tran.approverempunkid <> " & CInt(e.Item.Cells(16).Text)

                dsList = objlnOpApprovaltran.getOtherOpApprovaltranList("List", _
                                                                        CInt(e.Item.Cells(14).Text), _
                                                                        CInt(e.Item.Cells(12).Text), _
                                                                        CStr(e.Item.Cells(19).Text), _
                                                                        mstrFilter)

                If dsList IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then

                    For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1

                        If CInt(e.Item.Cells(17).Text) > CInt(dsList.Tables("List").Rows(i)("priority")) Then

                            Dim dtList As DataTable = New DataView(dsList.Tables("List"), "lnlevelunkid = " & CInt(dsList.Tables("List").Rows(i)("lnlevelunkid")) & " AND statusunkid = 2 ", "", DataViewRowState.CurrentRows).ToTable
                            If dtList.Rows.Count > 0 Then Continue For

                            If CInt(dsList.Tables("List").Rows(i)("statusunkid")) = 2 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                                Exit Sub
                            ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                                Exit Sub
                            End If

                        ElseIf CInt(e.Item.Cells(17).Text) <= CInt(dsList.Tables("List").Rows(i)("priority")) Then

                            If CInt(dsList.Tables("List").Rows(i)("statusunkid")) = 2 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                                Exit Sub
                            ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = 3 Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                                Exit Sub
                            End If
                        End If
                    Next
                End If

                'Nilay (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                txtRemark.Text = ""
                'Nilay (21-Jul-2016) -- End

                Call Load_LoanParameters()
                popupLoanParameter.Show()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgPendingOp_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgPendingOp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPendingOp.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                If CBool(e.Item.Cells(11).Text) = True Then
                    If CInt(e.Item.Cells(20).Text) <> 1 Then
                        CType(e.Item.Cells(0).FindControl("lnkChangeStatus"), LinkButton).Visible = False
                        e.Item.Cells(0).Visible = False
                        e.Item.Cells(2).Text = e.Item.Cells(1).Text
                        e.Item.Cells(1).Visible = False
                        e.Item.Cells(2).ColumnSpan = e.Item.Cells.Count - 1
                        For i = 3 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).Visible = False
                        Next
                        e.Item.Cells(2).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(2).Style.Add("text-align", "left")
                    End If
                Else
                    If CInt(e.Item.Cells(20).Text) = 1 Then
                        CType(e.Item.Cells(0).FindControl("lnkChangeStatus"), LinkButton).Visible = False
                        e.Item.Cells(0).ColumnSpan = e.Item.Cells.Count - 1
                        For i As Integer = 1 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).Visible = False
                        Next
                        e.Item.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(0).Style.Add("height", "17px")
                    Else
                        If e.Item.Cells(2).Text.Trim <> "" AndAlso e.Item.Cells(2).Text <> "&nbsp;" Then
                            e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).Date.ToString(Session("DateFormat").ToString, New System.Globalization.CultureInfo(""))
                        End If
                        If e.Item.Cells(6).Text.Trim <> "" AndAlso e.Item.Cells(6).Text <> "&nbsp;" Then
                            e.Item.Cells(6).Text = Format(CDec(e.Item.Cells(6).Text), Session("fmtCurrency").ToString)
                        End If
                        If e.Item.Cells(7).Text.Trim <> "" AndAlso e.Item.Cells(7).Text <> "&nbsp;" Then
                            e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), Session("fmtCurrency").ToString)
                        End If
                        Select Case CInt(e.Item.Cells(12).Text)
                            Case enParameterMode.LN_RATE
                                e.Item.Cells(5).Text = ""
                                e.Item.Cells(6).Text = ""
                                e.Item.Cells(7).Text = ""
                            Case enParameterMode.LN_EMI
                                e.Item.Cells(4).Text = ""
                                e.Item.Cells(7).Text = ""
                            Case enParameterMode.LN_TOPUP
                                e.Item.Cells(4).Text = ""
                                e.Item.Cells(5).Text = ""
                                e.Item.Cells(6).Text = ""
                        End Select
                    End If
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgPendingOp_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01-Apr-2016) -- End

#End Region

#Region "Button's Events"
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Call ClearParameters()
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvanceList.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Call ClearParameters()
            mblnAllowChangeStatus = False
            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            mActionMode = enAction.ADD_ONE
            'Nilay (13-Sept-2016) -- End

            Call Load_LoanParameters()
            'pnl_ChangeStatus.Visible = False

            'Varsha Rana (12-Sept-2017) -- Start
            'Enhancement - Loan Topup in ESS
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                radTopup.Checked = True
                Call radInstallment_CheckedChanged(radTopup, New System.EventArgs)
            End If
            ' Varsha Rana (12-Sept-2017) -- End
            popupLoanParameter.Show()
            mblnPopupShowHide = True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Select Case mintParameterId

                Case enParameterMode.LN_RATE
                    objLoanRate._Loanadvancetranunkid = mintLoanAdvanceTranunkid
                    objLoanRate._Isvoid = True
                    objLoanRate._Voiduserunkid = CInt(Session("UserId"))
                    objLoanRate._Voiddatetime = DateAndTime.Now.Date
                    objLoanRate._Voidreason = popupDeleteReason.Reason

                    'Nilay (05-May-2016) -- Start
                    Blank_ModuleName()
                    StrModuleName2 = "mnuLoan_Advance_Savings"
                    objLoanRate._WebIP = Session("IP_ADD").ToString
                    objLoanRate._WebFormName = "frmLoanAdvanceOperationList"
                    objLoanRate._WebHost = Session("HOST_NAME").ToString
                    'Nilay (05-May-2016) -- End

                    'Nilay (25-Mar-2016) -- Start
                    'blnFlag = objLoanRate.Delete(mintTabUnkid, True)

                    'Nilay (01-Apr-2016) -- Start
                    'ENHANCEMENT - Approval Process in Loan Other Operations
                    'blnFlag = objLoanRate.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                    '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                    '                             CStr(Session("UserAccessModeSetting")), True, mintTabUnkid, True)

                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'blnFlag = objLoanRate.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                    '                             mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True, mintTabUnkid, True)
                    blnFlag = objLoanRate.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                 mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), _
                                                 True, mintTabUnkid, True, False)
                    'Nilay (20-Sept-2016) -- End

                    'Nilay (01-Apr-2016) -- End

                    'Nilay (25-Mar-2016) -- End

                    If blnFlag = False AndAlso objLoanRate._Message <> "" Then
                        DisplayMessage.DisplayMessage(objLoanRate._Message, Me)
                        Exit Select
                    End If

                Case enParameterMode.LN_EMI
                    objLoanEMI._Loanadvancetranunkid = mintLoanAdvanceTranunkid
                    objLoanEMI._Isvoid = True
                    objLoanEMI._Voiduserunkid = CInt(Session("UserId"))
                    objLoanEMI._Voiddatetime = DateAndTime.Now.Date
                    objLoanEMI._Voidreason = popupDeleteReason.Reason

                    'Nilay (05-May-2016) -- Start
                    Blank_ModuleName()
                    StrModuleName2 = "mnuLoan_Advance_Savings"
                    objLoanEMI._WebIP = Session("IP_ADD").ToString
                    objLoanEMI._WebFormName = "frmLoanAdvanceOperationList"
                    objLoanEMI._WebHost = Session("HOST_NAME").ToString
                    'Nilay (05-May-2016) -- End

                    'Nilay (25-Mar-2016) -- Start
                    'blnFlag = objLoanEMI.Delete(mintTabUnkid, True)

                    'Nilay (01-Apr-2016) -- Start
                    'ENHANCEMENT - Approval Process in Loan Other Operations
                    'blnFlag = objLoanEMI.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                    '                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                    '                          CStr(Session("UserAccessModeSetting")), True, mintTabUnkid, True)

                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'blnFlag = objLoanEMI.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                    '                          mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True, mintTabUnkid, True)
                    blnFlag = objLoanEMI.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                              mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), _
                                              True, mintTabUnkid, True, False)
                    'Nilay (20-Sept-2016) -- End


                    'Nilay (01-Apr-2016) -- End

                    'Nilay (25-Mar-2016) -- End

                    If blnFlag = False AndAlso objLoanEMI._Message <> "" Then
                        DisplayMessage.DisplayMessage(objLoanEMI._Message, Me)
                        Exit Select
                    End If

                Case enParameterMode.LN_TOPUP
                    objLoanTopup._Loanadvancetranunkid = mintLoanAdvanceTranunkid
                    objLoanTopup._Isvoid = True
                    objLoanTopup._Voiduserunkid = CInt(Session("UserId"))
                    objLoanTopup._Voiddatetime = DateAndTime.Now.Date
                    objLoanTopup._Voidreason = popupDeleteReason.Reason

                    'Nilay (05-May-2016) -- Start
                    Blank_ModuleName()
                    StrModuleName2 = "mnuLoan_Advance_Savings"
                    objLoanTopup._WebIP = Session("IP_ADD").ToString
                    objLoanTopup._WebFormName = "frmLoanAdvanceOperationList"
                    objLoanTopup._WebHost = Session("HOST_NAME").ToString
                    'Nilay (05-May-2016) -- End

                    'Nilay (25-Mar-2016) -- Start
                    'blnFlag = objLoanTopup.Delete(mintTabUnkid, True)

                    'Nilay (01-Apr-2016) -- Start
                    'ENHANCEMENT - Approval Process in Loan Other Operations
                    'blnFlag = objLoanTopup.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                    '                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                    '                              CStr(Session("UserAccessModeSetting")), True, mintTabUnkid, True)

                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'blnFlag = objLoanTopup.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                    '                              mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True, mintTabUnkid, True)
                    blnFlag = objLoanTopup.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                  mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), _
                                                  True, mintTabUnkid, True, False)
                    'Nilay (20-Sept-2016) -- End

                    'Nilay (01-Apr-2016) -- End

                    'Nilay (25-Mar-2016) -- End

                    If blnFlag = False AndAlso objLoanTopup._Message <> "" Then
                        DisplayMessage.DisplayMessage(objLoanTopup._Message, Me)
                        Exit Select
                    End If

            End Select
            If blnFlag = True Then
                Call Fill_List()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupDeleteReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
#Region "CheckBox Events"
    Protected Sub chkMyApproval_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMyApproval.CheckedChanged
        Try
            Call FillPendingOpList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkMyApproval_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region "ComboBox Events"
    Protected Sub cboStatusSearch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatusSearch.SelectedIndexChanged
        Try
            Call FillPendingOpList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboStatusSearch_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region
    'Nilay (01-Apr-2016) -- End


#Region "Loan Parameters"

#Region "Private Methods"

    Private Sub Load_LoanParameters()
        Try
            Dim objLnAdv As New clsLoan_Advance
            objLnAdv._Loanadvancetranunkid = mintLoanAdvanceTranunkid

            mintCalcTypeId = CType(objLnAdv._Calctype_Id, enLoanCalcId)
            mintInterestCalcTypeId = CType(objLnAdv._Interest_Calctype_Id, enLoanInterestCalcType)
            mintCountryUnkid = objLnAdv._CountryUnkid
            mdtLoanEffectiveDate = CStr(objLnAdv._Effective_Date)
            mintEmployeeId = objLnAdv._Employeeunkid
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            mintLoanSchemeunkid = objLnAdv._Loanschemeunkid
            'Nilay (01-Apr-2016) -- End

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If mintCalcTypeId = enLoanCalcId.No_Interest Then
            If mintCalcTypeId = enLoanCalcId.No_Interest OrElse mintCalcTypeId = enLoanCalcId.No_Interest_With_Mapped_Head Then
                'Sohail (29 Apr 2019) -- End
                radInterestRate.Enabled = False
            End If
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If mintCalcTypeId = enLoanCalcId.No_Interest_With_Mapped_Head Then
                radInstallment.Enabled = False
            End If
            'Sohail (29 Apr 2019) -- End

            'txtInstallmentAmt.ReadOnly = False

            radInterestRate.Checked = False
            radInstallment.Checked = False
            radTopup.Checked = False

            If mintCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                txtPrincipalAmt.Enabled = True
            Else
                txtPrincipalAmt.Enabled = False
            End If

            Call SetFormControls()

            Call FillCombo()
            Call cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'If mintTabUnkid <= 0 Then
            '    pnlControls.Enabled = False
            '    pnlradOpr.Enabled = True
            'Else
            '    pnlControls.Enabled = True
            '    pnlradOpr.Enabled = False
            '    'mblnDoNotAllowChangePeriod = False

            '    Select Case mintParameterId
            '        Case enParameterMode.LN_RATE
            '            objLoanRate._Lninteresttranunkid = mintTabUnkid
            '        Case enParameterMode.LN_EMI
            '            objLoanEMI._Lnemitranunkid = mintTabUnkid
            '        Case enParameterMode.LN_TOPUP
            '            objLoanTopup._Lntopuptranunkid = mintTabUnkid
            '    End Select

            'End If
            'Nilay (01-Apr-2016) -- End

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'Call GetValue()
            cboPeriod.Enabled = True
            dtpEffectiveDate.Enabled = True
            'Nilay (01-Apr-2016) -- End

            Select Case mintParameterId
                Case enParameterMode.LN_EMI, enParameterMode.LN_TOPUP
                    dtpEffectiveDate.Enabled = False
            End Select

            'Call ResetLoanPara()

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'If mintTabUnkid <= 0 Then
            'Nilay (01-Apr-2016) -- End

            'Varsha Rana (12-Sept-2017) -- Start
            'Enhancement - Loan Topup in ESS
            'Dim dsList As DataSet = objLnAdv.GetList(Session("Database_Name").ToString, _
            '                                        CInt(Session("UserId")), CInt(Session("Fin_year")), _
            '                                        CInt(Session("CompanyUnkId")), True, _
            '                                        CDate(mdtLoanEffectiveDate), _
            '                                        CDate(Session("fin_enddate")), _
            '                                        CDate(Session("fin_enddate")), _
            '                                        Session("UserAccessModeSetting").ToString, _
            '                                        "List", "LN.loanadvancetranunkid = " & mintLoanAdvanceTranunkid & "", _
            '                                        mintEmployeeId)

            Dim dsList As DataSet = objLnAdv.GetList(Session("Database_Name").ToString, _
                                                     CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), True, _
                                                     CDate(mdtLoanEffectiveDate), _
                                                     CDate(Session("fin_enddate")), _
                                                     CDate(Session("fin_enddate")), _
                                                     Session("UserAccessModeSetting").ToString, _
                                                     "List", "LN.loanadvancetranunkid = " & mintLoanAdvanceTranunkid & "", _
                                                mintEmployeeId, CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

            ' Varsha Rana (12-Sept-2017) -- End

            If dsList.Tables("List").Rows.Count > 0 Then
                txtLoanInterest.Text = CDec(Format(CDec(dsList.Tables("List").Rows(0).Item("interest_rate")), Session("fmtCurrency").ToString)).ToString
                txtDuration.Text = CInt(dsList.Tables("List").Rows(0).Item("installments")).ToString
                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                mdecInterestRate = CDec(Format(CDec(dsList.Tables("List").Rows(0).Item("interest_rate")), Session("fmtCurrency").ToString))
                mintNoOfInstl = CInt(dsList.Tables("List").Rows(0).Item("installments"))
                'Nilay (01-Apr-2016) -- End
                Call txtDuration_TextChanged(txtDuration, New System.EventArgs)
            End If
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'End If 
            'Nilay (01-Apr-2016) -- End

            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            If mActionMode = enAction.EDIT_ONE Then
                Select Case mintParameterId
                    Case enParameterMode.LN_RATE
                        objLoanRate._Lninteresttranunkid = mintTabUnkid
                        radInterestRate.Checked = True
                    Case enParameterMode.LN_EMI
                        objLoanEMI._Lnemitranunkid = mintTabUnkid
                        radInstallment.Checked = True
                    Case enParameterMode.LN_TOPUP
                        objLoanTopup._Lntopuptranunkid = mintTabUnkid
                        radTopup.Checked = True
                End Select
            End If
            'Nilay (13-Sept-2016) -- End

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            If mblnAllowChangeStatus = True Then
                pnl_ChangeStatus.Visible = True
                objlnOpApprovaltran._LoanOtherOptranunkid = mintlnOtherOpTranunkid
                pnlradOpr.Enabled = False
                Call GetValuePendingParameter()
            Else
                pnl_ChangeStatus.Visible = False
                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                'pnlradOpr.Enabled = True
                'Call GetValueApprovedParameter()
                If mActionMode = enAction.EDIT_ONE Then
                    dtpEffectiveDate.Enabled = False
                    pnlradOpr.Enabled = False
                    Call GetValueApprovedParameter()
                Else
                    pnlradOpr.Enabled = True
                End If
                'Nilay (13-Sept-2016) -- End
            End If
            'Nilay (01-Apr-2016) -- End

            If mintCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Call txtPrincipalAmt_TextChanged(txtPrincipalAmt, New System.EventArgs)
            End If

            objLnAdv = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Load_LoanParameters:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    'Private Sub SetFormControls()
    '    Try
    '        Select Case mintParameterId
    '            Case enParameterMode.LN_RATE
    '                cboPeriod.Enabled = True
    '                If mintInterestCalcTypeId = enLoanInterestCalcType.DAILY Then
    '                    dtpEffectiveDate.Enabled = True
    '                Else
    '                    dtpEffectiveDate.Enabled = False
    '                End If
    '                txtPrincipalAmt.Enabled = False
    '                txtInterestAmt.Enabled = False
    '                txtInstallmentAmt.Enabled = False
    '                txtTopupAmt.Enabled = False
    '                txtLoanInterest.Enabled = True
    '                txtDuration.Enabled = False
    '                'dtpEffectiveDate.Enabled = True
    '                txtInstallmentAmt.Visible = True
    '                lblEMIAmount.Visible = True
    '                txtTopupAmt.Visible = False
    '                lblTopupAmt.Visible = False

    '            Case enParameterMode.LN_EMI

    '                cboPeriod.Enabled = True
    '                txtInterestAmt.Enabled = False
    '                txtLoanInterest.Enabled = False
    '                txtDuration.Enabled = True
    '                txtPrincipalAmt.Enabled = False
    '                lblEMIAmount.Visible = True
    '                txtInstallmentAmt.Enabled = False
    '                txtInstallmentAmt.Visible = True
    '                lblTopupAmt.Visible = False
    '                txtTopupAmt.Enabled = False
    '                txtTopupAmt.Visible = False
    '                dtpEffectiveDate.Enabled = False

    '                If mintCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
    '                    txtPrincipalAmt.Enabled = True
    '                ElseIf mintCalcTypeId = enLoanCalcId.No_Interest Then
    '                    txtDuration.Enabled = False
    '                    txtInstallmentAmt.Enabled = True
    '                End If

    '            Case enParameterMode.LN_TOPUP

    '                cboPeriod.Enabled = True
    '                txtPrincipalAmt.Enabled = False
    '                txtInterestAmt.Enabled = False
    '                txtInstallmentAmt.Enabled = False
    '                txtTopupAmt.Enabled = True
    '                txtDuration.Enabled = False
    '                txtLoanInterest.Enabled = False
    '                lblEMIAmount.Visible = False
    '                txtInstallmentAmt.Visible = False
    '                txtTopupAmt.Visible = True
    '                lblTopupAmt.Visible = True
    '                dtpEffectiveDate.Enabled = False

    '        End Select

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("SetFormControls:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Private Sub SetFormControls()
        Try
            Select Case mintParameterId
                Case enParameterMode.LN_RATE
                    lblEMIAmount.Visible = True
                    txtInstallmentAmt.Visible = True
                    txtInstallmentAmt.Enabled = True
                    txtInstallmentAmt.ReadOnly = True
                    lblTopupAmt.Visible = False
                    txtTopupAmt.Visible = False
                    txtTopupAmt.Enabled = False
                    txtLoanInterest.Enabled = True
                    txtDuration.Enabled = False
                    'Nilay (13-Sept-2016) -- Start
                    'Enhancement : Enable Default Parameter Edit & other fixes
                    'If mintInterestCalcTypeId = enLoanInterestCalcType.DAILY Then
                    '    dtpEffectiveDate.Enabled = True
                    'Else
                    '    dtpEffectiveDate.Enabled = False
                    'End If
                    If mActionMode <> enAction.EDIT_ONE Then
                        If mintInterestCalcTypeId = enLoanInterestCalcType.DAILY Then
                            dtpEffectiveDate.Enabled = True
                        Else
                            dtpEffectiveDate.Enabled = False
                        End If
                    ElseIf mActionMode = enAction.EDIT_ONE Then
                        If mblnAllowChangeStatus = True Then
                            dtpEffectiveDate.Enabled = True
                        Else
                            dtpEffectiveDate.Enabled = False
                        End If
                    End If
                    'Nilay (13-Sept-2016) -- End
                    txtPrincipalAmt.Enabled = False
                    txtInterestAmt.Enabled = True

                Case enParameterMode.LN_EMI
                    If mintCalcTypeId = enLoanCalcId.No_Interest Then
                        txtInstallmentAmt.ReadOnly = False
                        txtDuration.Enabled = False
                    Else
                        txtInstallmentAmt.ReadOnly = True
                        txtDuration.Enabled = True
                    End If
                    txtInstallmentAmt.Enabled = True
                    txtTopupAmt.Enabled = False
                    txtLoanInterest.Enabled = False
                    lblEMIAmount.Visible = True
                    txtInstallmentAmt.Visible = True
                    txtTopupAmt.Visible = False
                    lblTopupAmt.Visible = False
                    dtpEffectiveDate.Enabled = False
                    txtPrincipalAmt.Enabled = True
                    txtInterestAmt.Enabled = False

                Case enParameterMode.LN_TOPUP
                    txtInstallmentAmt.Enabled = False
                    txtTopupAmt.Enabled = True
                    txtDuration.Enabled = False
                    txtLoanInterest.Enabled = False
                    lblEMIAmount.Visible = False
                    txtInstallmentAmt.Visible = False
                    txtTopupAmt.Visible = True
                    lblTopupAmt.Visible = True
                    dtpEffectiveDate.Enabled = False
                    txtPrincipalAmt.Enabled = False
                    txtInterestAmt.Enabled = False

            End Select

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFormControls:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01-Apr-2016) -- End



    Private Sub FillCombo()

        Dim dsList As New DataSet
        Dim objPrd As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim mdtTable As DataTable = Nothing

        Try
            dsList = objPrd.getListForCombo(enModuleReference.Payroll, _
                                            CInt(Session("Fin_year")), _
                                            Session("Database_Name").ToString, _
                                            CDate(Session("fin_startdate")), _
                                            "Period", True, 1)

            Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, _
                                                                         CInt(Session("Fin_year")), 1)
            If intFirstPeriodId > 0 Then
                mdtTable = New DataView(dsList.Tables("Period"), "periodunkid = " & intFirstPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                mdtTable = New DataView(dsList.Tables("List"), "1=2", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboPeriod
                .DataTextField = "name"
                .DataValueField = "periodunkid"
                .DataSource = mdtTable
                .DataBind()
                If .Items.Count > 0 Then
                    .SelectedIndex = 0
                End If
            End With

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            Dim objLoan As New clsProcess_pending_loan

            'Nilay (09-Aug-2016) -- Start
            'dsList = objLoan.GetLoan_Status("Status", False, False)
            dsList = objLoan.GetLoan_Status("Status", True, False)
            Dim dtView As DataView = New DataView(dsList.Tables("Status"), "Id <> 1", "", DataViewRowState.CurrentRows)
            'Nilay (09-Aug-2016) -- End
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "NAME"
                'Nilay (09-Aug-2016) -- Start
                '.DataSource = dsList.Tables("Status")
                .DataSource = dtView
                .SelectedIndex = 0
                'Nilay (09-Aug-2016) -- End
                .DataBind()
            End With
            objLoan = Nothing
            'Nilay (01-Apr-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objPrd = Nothing
            objMaster = Nothing
        End Try
    End Sub

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    'Private Sub GetValue()
    Private Sub GetValueApprovedParameter()
        'Nilay (01-Apr-2016) -- End
        Try
            Select Case mintParameterId
                Case enParameterMode.LN_RATE
                    txtLoanInterest.Text = CDec(objLoanRate._Interest_Rate).ToString
                    radInterestRate.Checked = True
                    Call radInstallment_CheckedChanged(radInterestRate, New EventArgs())
                    If objLoanRate._Effectivedate <> Nothing Then
                        dtpEffectiveDate.SetDate = CDate(objLoanRate._Effectivedate).Date
                    End If

                Case enParameterMode.LN_EMI
                    txtDuration.Text = CStr(objLoanEMI._Emi_Tenure)
                    mdecInstallmentAmnt = objLoanEMI._Emi_Amount
                    txtInstallmentAmt.Text = Format(objLoanEMI._Emi_Amount, Session("fmtCurrency").ToString)
                    radInstallment.Checked = True
                    Call radInstallment_CheckedChanged(radInstallment, New EventArgs())
                    If objLoanEMI._Effectivedate <> Nothing Then
                        dtpEffectiveDate.SetDate = CDate(objLoanEMI._Effectivedate).Date
                    End If

                    objlblEndDate.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Loan Stop Date :") & " " & DateAdd(DateInterval.Month, CDec(txtDuration.Text), CDate(mdtLoanEffectiveDate)).AddDays(-1)

                Case enParameterMode.LN_TOPUP
                    mdecTopupAmnt = objLoanTopup._Topup_Amount
                    txtTopupAmt.Text = Format(objLoanTopup._Topup_Amount, Session("fmtCurrency").ToString)
                    radTopup.Checked = True
                    Call radInstallment_CheckedChanged(radTopup, New EventArgs())
                    If objLoanTopup._Effectivedate <> Nothing Then
                        dtpEffectiveDate.SetDate = CDate(objLoanTopup._Effectivedate).Date
                    End If

            End Select
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValueApprovedParameter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private Sub GetValuePendingParameter()
        Dim dsList As DataSet = Nothing
        Try
            Select Case mintParameterId
                Case enParameterMode.LN_RATE
                    radInterestRate.Checked = True
                    Call radInstallment_CheckedChanged(radInterestRate, New EventArgs)
                    txtLoanInterest.Text = Format(objlnOpApprovaltran._InterestRate, Session("fmtCurrency").ToString)
                    txtDuration.Text = mintNoOfInstl.ToString
                    Call txtDuration_TextChanged(txtDuration, New System.EventArgs)
                    If objlnOpApprovaltran._EffectiveDate <> Nothing Then
                        dtpEffectiveDate.SetDate = CDate(objlnOpApprovaltran._EffectiveDate).Date
                    End If

                Case enParameterMode.LN_EMI
                    radInstallment.Checked = True
                    Call radInstallment_CheckedChanged(radInterestRate, New EventArgs)
                    txtLoanInterest.Text = mdecInterestRate.ToString
                    txtDuration.Text = CStr(objlnOpApprovaltran._EmiTenure)
                    Call txtDuration_TextChanged(txtDuration, New System.EventArgs)
                    mdecInstallmentAmnt = objlnOpApprovaltran._EmiAmount
                    txtInstallmentAmt.Text = Format(objlnOpApprovaltran._EmiAmount, Session("fmtCurrency").ToString)
                    txtPrincipalAmt.Text = Format(objlnOpApprovaltran._PrincipalAmount, Session("fmtCurrency").ToString)
                    txtInterestAmt.Text = Format(objlnOpApprovaltran._InterestAmount, Session("fmtCurrency").ToString)
                    If objlnOpApprovaltran._EffectiveDate <> Nothing Then
                        dtpEffectiveDate.SetDate = CDate(objlnOpApprovaltran._EffectiveDate).Date
                    End If
                Case enParameterMode.LN_TOPUP
                    radTopup.Checked = True
                    Call radInstallment_CheckedChanged(radInterestRate, New EventArgs)
                    txtLoanInterest.Text = mdecInterestRate.ToString
                    txtDuration.Text = mintNoOfInstl.ToString
                    Call txtDuration_TextChanged(txtDuration, New System.EventArgs)
                    mdecTopupAmnt = objlnOpApprovaltran._TopupAmount
                    txtTopupAmt.Text = Format(objlnOpApprovaltran._TopupAmount, Session("fmtCurrency").ToString)
                    If objlnOpApprovaltran._EffectiveDate <> Nothing Then
                        dtpEffectiveDate.SetDate = CDate(objlnOpApprovaltran._EffectiveDate).Date
                    End If
            End Select

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValuePendingParameter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01-Apr-2016) -- End

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private Sub SetValue()
        Try
            Dim strGuidValue As String

            If mblnAllowChangeStatus = True Then
                objlnOpApprovaltran._LoanOtherOptranunkid = mintlnOtherOpTranunkid
            Else
                strGuidValue = Guid.NewGuid.ToString()
                objlnOpApprovaltran._Identify_Guid = strGuidValue
            End If

            Select Case mintParameterId
                Case enParameterMode.LN_RATE
                    objlnOpApprovaltran._LoanAdvancetranunkid = mintLoanAdvanceTranunkid
                    objlnOpApprovaltran._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objlnOpApprovaltran._LoanOperationTypeId = enParameterMode.LN_RATE
                    objlnOpApprovaltran._EffectiveDate = dtpEffectiveDate.GetDate
                    objlnOpApprovaltran._InterestRate = CDbl(txtLoanInterest.Text)

                    objlnOpApprovaltran._Userunkid = CInt(Session("UserId"))
                    objlnOpApprovaltran._Isvoid = False
                    objlnOpApprovaltran._Voiduserunkid = -1
                    objlnOpApprovaltran._VoidDateTime = Nothing
                    objlnOpApprovaltran._VoidReason = ""
                    objlnOpApprovaltran._FinalStatus = 1 'Pending

                    'Nilay (05-May-2016) -- Start
                    Call setWebATParameters()
                    'Nilay (05-May-2016) -- End

                    'objlnOpApprovaltran._Identify_Guid = strGuidValue
                    'If mActionMode = enAction.EDIT_ONE Then
                    '    objlnOpApprovaltran._ApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                    'End If

                Case enParameterMode.LN_EMI
                    objlnOpApprovaltran._LoanAdvancetranunkid = mintLoanAdvanceTranunkid
                    objlnOpApprovaltran._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objlnOpApprovaltran._LoanOperationTypeId = enParameterMode.LN_EMI
                    objlnOpApprovaltran._EffectiveDate = dtpEffectiveDate.GetDate
                    objlnOpApprovaltran._EmiTenure = CInt(txtDuration.Text)
                    objlnOpApprovaltran._EmiAmount = CDec(txtInstallmentAmt.Text)
                    objlnOpApprovaltran._LoanDuration = CInt(txtDuration.Text)
                    'Hemant (01 Dec 2021) -- Start
                    'ISSUE/ENHANCEMENT : OLD-525(Aflife) - No. of Installments not changing after installment amount change.
                    'objlnOpApprovaltran._EndDate = DateAdd(DateInterval.Month, CDbl(txtDuration.Text), CDate(mdtLoanEffectiveDate).Date).AddDays(-1)
                    objlnOpApprovaltran._EndDate = DateAdd(DateInterval.Month, CDbl(txtDuration.Text), dtpEffectiveDate.GetDate).AddDays(-1)
                    'Hemant (01 Dec 2021) -- End
                    objlnOpApprovaltran._PrincipalAmount = CDec(txtPrincipalAmt.Text)
                    objlnOpApprovaltran._InterestAmount = CDec(txtInterestAmt.Text)
                    objlnOpApprovaltran._ExchangeRate = mdecPaidExRate
                    objlnOpApprovaltran._BaseCurrencyAmount = CDec(txtInstallmentAmt.Text) / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))

                    objlnOpApprovaltran._Userunkid = CInt(Session("UserId"))
                    objlnOpApprovaltran._Isvoid = False
                    objlnOpApprovaltran._Voiduserunkid = -1
                    objlnOpApprovaltran._VoidDateTime = Nothing
                    objlnOpApprovaltran._VoidReason = ""
                    objlnOpApprovaltran._FinalStatus = 1 'Pending

                    'Nilay (05-May-2016) -- Start
                    Call setWebATParameters()
                    'Nilay (05-May-2016) -- End

                    'objlnOpApprovaltran._Identify_Guid = strGuidValue
                    'If mActionMode = enAction.EDIT_ONE Then
                    '    objlnOpApprovaltran._ApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                    'End If

                Case enParameterMode.LN_TOPUP
                    objlnOpApprovaltran._LoanAdvancetranunkid = mintLoanAdvanceTranunkid
                    objlnOpApprovaltran._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objlnOpApprovaltran._LoanOperationTypeId = enParameterMode.LN_TOPUP
                    objlnOpApprovaltran._EffectiveDate = dtpEffectiveDate.GetDate
                    objlnOpApprovaltran._TopupAmount = CDec(txtTopupAmt.Text)
                    objlnOpApprovaltran._ExchangeRate = mdecPaidExRate
                    objlnOpApprovaltran._BaseCurrencyAmount = CDec(txtTopupAmt.Text) / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))

                    objlnOpApprovaltran._Userunkid = CInt(Session("UserId"))
                    objlnOpApprovaltran._Isvoid = False
                    objlnOpApprovaltran._Voiduserunkid = -1
                    objlnOpApprovaltran._VoidDateTime = Nothing
                    objlnOpApprovaltran._VoidReason = ""
                    objlnOpApprovaltran._FinalStatus = 1 'Pending

                    'Varsha Rana (12-Sept-2017) -- Start
                    'Enhancement - Loan Topup in ESS
                    If mActionMode = enAction.ADD_ONE Then

                        If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                            objlnOpApprovaltran._Isfromess = False
                        Else
                            objlnOpApprovaltran._Isfromess = True
                        End If

                    End If
                    ' Varsha Rana (12-Sept-2017) -- End

                    'Nilay (05-May-2016) -- Start
                    Call setWebATParameters()
                    'Nilay (05-May-2016) -- End

                    'objlnOpApprovaltran._Identify_Guid = strGuidValue
                    'If mActionMode = enAction.EDIT_ONE Then
                    '    objlnOpApprovaltran._ApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                    'End If

            End Select

            If mblnAllowChangeStatus = True Then
                objlnOpApprovaltran._Employeeunkid = mintEmployeeId
                objlnOpApprovaltran._ApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                objlnOpApprovaltran._Statusunkid = CInt(cboStatus.SelectedValue)
                objlnOpApprovaltran._Remark = txtRemark.Text
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'Private Sub SetValue()
    '    Try
    '        Select Case mintParameterId
    '            Case enParameterMode.LN_RATE
    '                objLoanRate._Effectivedate = dtpEffectiveDate.GetDate.Date
    '                objLoanRate._Interest_Rate = CDec(txtLoanInterest.Text)
    '                objLoanRate._Isvoid = False
    '                objLoanRate._Loanadvancetranunkid = mintLoanAdvanceTranunkid
    '                objLoanRate._Periodunkid = CInt(cboPeriod.SelectedValue)
    '                objLoanRate._Userunkid = CInt(Session("UserId"))
    '                objLoanRate._Voiddatetime = Nothing
    '                objLoanRate._Voidreason = ""
    '                objLoanRate._Voiduserunkid = -1

    '            Case enParameterMode.LN_EMI
    '                objLoanEMI._Effectivedate = dtpEffectiveDate.GetDate.Date
    '                objLoanEMI._Emi_Tenure = CInt(txtDuration.Text)
    '                objLoanEMI._End_Date = DateAdd(DateInterval.Month, CDec(txtDuration.Text), CDate(mdtLoanEffectiveDate)).AddDays(-1)
    '                objLoanEMI._Emi_Amount = CDec(txtInstallmentAmt.Text)
    '                objLoanEMI._Exchange_rate = mdecPaidExRate
    '                objLoanEMI._Basecurrency_amount = CDec(txtInstallmentAmt.Text) / mdecPaidExRate
    '                objLoanEMI._Loan_Duration = CInt(txtDuration.Text)
    '                objLoanEMI._Isvoid = False
    '                objLoanEMI._Voiddatetime = Nothing
    '                objLoanEMI._Voidreason = ""
    '                objLoanEMI._Voiduserunkid = -1
    '                objLoanEMI._Userunkid = CInt(Session("UserId"))
    '                objLoanEMI._Loanadvancetranunkid = mintLoanAdvanceTranunkid
    '                objLoanEMI._Periodunkid = CInt(cboPeriod.SelectedValue)
    '                objLoanEMI._Principal_amount = CDec(txtPrincipalAmt.Text)
    '                objLoanEMI._Interest_amount = CDec(txtInterestAmt.Text)

    '            Case enParameterMode.LN_TOPUP
    '                objLoanTopup._Effectivedate = dtpEffectiveDate.GetDate.Date
    '                objLoanTopup._Exchange_rate = mdecPaidExRate
    '                objLoanTopup._Isvoid = False
    '                objLoanTopup._Loanadvancetranunkid = mintLoanAdvanceTranunkid
    '                objLoanTopup._Periodunkid = CInt(cboPeriod.SelectedValue)
    '                objLoanTopup._Topup_Amount = CDec(txtTopupAmt.Text)
    '                objLoanTopup._Userunkid = CInt(Session("UserId"))
    '                objLoanTopup._Voiddatetime = Nothing
    '                objLoanTopup._Voidreason = ""
    '                objLoanTopup._Voiduserunkid = -1
    '                objLoanTopup._Basecurrency_amount = CDec(txtTopupAmt.Text) / mdecPaidExRate

    '        End Select
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
    '    Finally
    '    End Try
    'End Sub
    'Nilay (01-Apr-2016) -- End



    Private Sub SetDateValue()
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim dtEndDate As Date = Nothing

                If mintParameterId = enParameterMode.LN_RATE Then
                    Dim dsBal As New DataSet
                    Dim objLoanAdvance As New clsLoan_Advance
                    dsBal = objLoanAdvance.GetLastLoanBalance("List", mintLoanAdvanceTranunkid)
                    objLoanAdvance = Nothing
                    If dsBal.Tables(0).Rows.Count > 0 Then
                        dtEndDate = eZeeDate.convertDate(dsBal.Tables(0).Rows(0).Item("end_date").ToString).Date.AddDays(1)
                    End If
                End If

                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
                'Nilay (25-Mar-2016) -- Start
                mdtStartDate = objPeriod._Start_Date
                mdtEndDate = objPeriod._End_Date
                'Nilay (25-Mar-2016) -- End

                If dtEndDate <> Nothing Then
                    dtpEffectiveDate.SetDate = dtEndDate
                Else
                    dtpEffectiveDate.SetDate = CDate(objPeriod._Start_Date)
                End If

                objPeriod = Nothing

                Dim objExRate As New clsExchangeRate
                Dim dsList As New DataSet
                objlblExRate.Text = ""
                dsList = objExRate.GetList("ExRate", True, , , mintCountryUnkid, True, dtpEffectiveDate.GetDate.Date, True)
                If dsList.Tables("ExRate").Rows.Count > 0 Then
                    mdecBaseExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                    mintPaidCurrId = CInt(dsList.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
                    objlblExRate.Text = Format(mdecBaseExRate, Session("fmtCurrency").ToString) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dsList.Tables("ExRate").Rows(0).Item("currency_sign").ToString & " "
                Else
                    mdecBaseExRate = 0
                    mdecPaidExRate = 0
                End If
                objExRate = Nothing

                Dim objLnAdv As New clsLoan_Advance
                'Nilay (25-Mar-2016) -- Start
                'mdsLoanInfo = objLnAdv.Calculate_LoanBalanceInfo("List", dtpEffectiveDate.GetDate.Date, "", mintLoanAdvanceTranunkid, , , True)


                'Varsha Rana (12-Sept-2017) -- Start
                'Enhancement - Loan Topup in ESS
                'mdsLoanInfo = objLnAdv.Calculate_LoanBalanceInfo(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                '                                               mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True, _
                '                                               "List", dtpEffectiveDate.GetDate.Date, "", mintLoanAdvanceTranunkid, , , True)
                mdsLoanInfo = objLnAdv.Calculate_LoanBalanceInfo(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                 mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True, _
                                                               "List", dtpEffectiveDate.GetDate.Date, "", mintLoanAdvanceTranunkid, , , True, , , , , , , , , , , CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))
                ' Varsha Rana (12-Sept-2017) -- End


                'Nilay (25-Mar-2016) -- End


                dsList = New DataSet
                dsList = objLnAdv.GetLastLoanBalance("List", mintLoanAdvanceTranunkid)
                If dsList.Tables(0).Rows.Count > 0 Then
                    mdtBalanceEndDate = CDate(eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("end_date").ToString))
                End If
                dsList.Dispose()
                objLnAdv = Nothing

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetDateValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Calculate_Loan_Installment_Amount(ByVal eLnCalcType As enLoanCalcId, _
                                                  ByVal eIntRateCalcTypeID As enLoanInterestCalcType, _
                                                  Optional ByVal blnIsDurationChange As Boolean = False)
        Try
            objLnAdv = New clsLoan_Advance

            If mdsLoanInfo IsNot Nothing AndAlso mdsLoanInfo.Tables(0).Rows.Count > 0 Then

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'If eLnCalcType <> enLoanCalcId.No_Interest Then
                If eLnCalcType <> enLoanCalcId.No_Interest AndAlso eLnCalcType <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                    'Sohail (29 Apr 2019) -- End
                    Dim decInstlmntAmnt As Decimal = 0
                    Dim decInterestRateAmnt As Decimal = 0
                    Dim decNetAmount As Decimal = 0
                    Dim decTotInstallmentAmount As Decimal = 0
                    Dim decTotIntrstAmount As Decimal = 0

                    Dim dtEndDate As Date = DateAdd(DateInterval.Month, CDec(txtDuration.Text), CDate(mdtLoanEffectiveDate).Date).AddDays(-1)

                    Dim dtFPeriodStart As Date = dtpEffectiveDate.GetDate.Date
                    Dim dtFPeriodEnd As Date = dtEndDate
                    If CInt(cboPeriod.SelectedValue) > 0 Then
                        Dim objPeriod As New clscommom_period_Tran
                        objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
                        'Nilay (25-Mar-2016) -- Start
                        dtFPeriodStart = objPeriod._Start_Date
                        'Nilay (25-Mar-2016) -- End
                        dtFPeriodEnd = objPeriod._End_Date
                    End If

                    Dim decOrigInstAmt As Decimal = CDec(mdsLoanInfo.Tables(0).Rows(0).Item("TotPrincipalAmount"))
                    If mintCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                        decOrigInstAmt = CDec(txtPrincipalAmt.Text)
                    End If

                    'Nilay (25-Mar-2016) -- Start
                    'Dim dsLoan As DataSet = objLnAdv.Calculate_LoanBalanceInfo("List", dtpEffectiveDate.GetDate.Date, mintEmployeeId.ToString, mintLoanAdvanceTranunkid)
                    Dim dsLoan As DataSet = objLnAdv.Calculate_LoanBalanceInfo(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                               CInt(Session("CompanyUnkId")), dtFPeriodStart, dtFPeriodEnd, CStr(Session("UserAccessModeSetting")), _
                                                                               True, "List", dtpEffectiveDate.GetDate.Date, mintEmployeeId.ToString, mintLoanAdvanceTranunkid)
                    'Nilay (25-Mar-2016) -- End

                    Dim decDueBalanceAmt As Decimal = CDec(mdsLoanInfo.Tables(0).Rows(0).Item("BalanceAmount"))
                    If dsLoan.Tables("List").Rows.Count > 0 Then
                        decDueBalanceAmt = CDec(dsLoan.Tables("List").Rows(0).Item("LastProjectedBalance"))
                    End If

                    'Sohail / Varsha (01 Nov 2017) -- Start
                    'PACRA Issue - 70.1 - The loan which calculation type is under simple interest rate and interest calculation type in by  tenure is not bring the right results when the number of installment is changed i.e if initially the number of installment set to 10 installment and changed to 5 instalment the results on this is not correct.
                    'objLnAdv.Calulate_Projected_Loan_Balance(CDec(Format(CDec(decDueBalanceAmt * mdecPaidExRate), Session("fmtCurrency").ToString)), _
                    '                                         CInt(DateDiff(DateInterval.Day, dtpEffectiveDate.GetDate.Date, dtEndDate.AddDays(1))), _
                    '                                         CDec(txtLoanInterest.Text), _
                    '                                         eLnCalcType, _
                    '                                         eIntRateCalcTypeID, _
                    '                                         CInt(txtDuration.Text), _
                    '                                         CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))), _
                    '                                         decOrigInstAmt, _
                    '                                         decInterestRateAmnt, _
                    '                                         decInstlmntAmnt, _
                    '                                         decTotIntrstAmount, _
                    '                                         decTotInstallmentAmount)
                    objLnAdv.Calulate_Projected_Loan_Balance(CDec(Format(CDec(decDueBalanceAmt * mdecPaidExRate), Session("fmtCurrency").ToString)), _
                                                             CInt(DateDiff(DateInterval.Day, dtpEffectiveDate.GetDate.Date, dtEndDate.AddDays(1))), _
                                                             CDec(txtLoanInterest.Text), _
                                                             eLnCalcType, _
                                                             eIntRateCalcTypeID, _
                                                            CInt(DateDiff(DateInterval.Month, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))), _
                                                             CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))), _
                                                             decOrigInstAmt, _
                                                             decInterestRateAmnt, _
                                                             decInstlmntAmnt, _
                                                             decTotIntrstAmount, _
                                                             decTotInstallmentAmount)
                    'Sohail / Varsha (01 Nov 2017) -- End

                    decNetAmount = CDec(Format(CDec(CDec(mdsLoanInfo.Tables(0).Rows(0).Item("PrincipalBalance")) * mdecPaidExRate), Session("fmtCurrency").ToString)) + decInterestRateAmnt

                    mdecInstallmentAmnt = decInstlmntAmnt 'mdecInstallmentAmnt
                    txtInstallmentAmt.Text = Format(decInstlmntAmnt, Session("fmtCurrency").ToString)

                    txtPrincipalAmt.Text = Format(decInstlmntAmnt - decInterestRateAmnt, Session("fmtCurrency").ToString)
                    mdecPrincipalAmnt = decInstlmntAmnt - decInterestRateAmnt
                    txtInterestAmt.Text = Format(decInterestRateAmnt, Session("fmtCurrency").ToString)
                    mdecInterestAmnt = decInterestRateAmnt

                Else
                    'Dim intNoOfEMI As Integer = 1
                    'If blnIsDurationChange = False Then
                    '    If CDec(txtInstallmentAmt.Text) > 0 Then
                    '        intNoOfEMI = CInt(IIf(CDec(txtInstallmentAmt.Text) = 0, 0, CDec(CDec(mdsLoanInfo.Tables(0).Rows(0).Item("PrincipalBalance")) * mdecPaidExRate) / txtInstallmentAmt.Text))
                    '    Else
                    '        intNoOfEMI = 1
                    '    End If

                    '    If CInt(CDec(IIf(CDec(txtInstallmentAmt.Text) = 0, 0, CDec(CDec(mdsLoanInfo.Tables(0).Rows(0).Item("PrincipalBalance")) * mdecPaidExRate) / txtInstallmentAmt.Text))) > intNoOfEMI Then
                    '        intNoOfEMI += 1
                    '    End If

                    '    'RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                    '    txtDuration.Text = intNoOfEMI
                    '    'Call txtDuration_TextChanged(Nothing, Nothing)
                    '    'AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged

                    'Else
                    '    Dim decInstallmentAmt As Decimal
                    '    If intNoOfEMI > 0 Then
                    '        decInstallmentAmt = CDec(IIf(intNoOfEMI = 0, 0, CDec(CDec(mdsLoanInfo.Tables(0).Rows(0).Item("PrincipalBalance")) * mdecPaidExRate) / txtDuration.Text))
                    '    Else
                    '        decInstallmentAmt = 0
                    '    End If

                    '    'RemoveHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
                    '    mdecInstallmentAmnt = decInstallmentAmt
                    '    txtInstallmentAmt.Text = CDec(Format(decInstallmentAmt, Session("fmtCurrency")))
                    '    'Call txtInstallmentAmt_TextChanged(Nothing, Nothing)
                    '    'AddHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged

                    'End If

                    If blnIsDurationChange = False Then
                        txtPrincipalAmt.Text = txtInstallmentAmt.Text
                        mdecPrincipalAmnt = mdecInstallmentAmnt

                    Else
                        mdecInstallmentAmnt = CDec(mdsLoanInfo.Tables(0).Rows(0).Item("emi_amountPaidCurrency"))
                        txtInstallmentAmt.Text = Format(mdsLoanInfo.Tables(0).Rows(0).Item("emi_amountPaidCurrency"), Session("fmtCurrency").ToString)
                        txtPrincipalAmt.Text = Format(CDec(mdsLoanInfo.Tables(0).Rows(0).Item("emi_amountPaidCurrency")), Session("fmtCurrency").ToString)
                        mdecPrincipalAmnt = CDec(mdsLoanInfo.Tables(0).Rows(0).Item("emi_amountPaidCurrency"))
                        txtInterestAmt.Text = Format(0, Session("fmtCurrency").ToString).ToString
                        mdecInterestAmnt = 0
                    End If
                End If
            End If
            objLnAdv = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "Calculate_Projected_Loan_Balance", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally

        End Try
    End Sub

    Private Function Valid_Data() As Boolean

        Try
            If mintCalcTypeId = enLoanCalcId.No_Interest Then
                If radInstallment.Checked = False AndAlso radTopup.Checked = False Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Sorry, Please select atleast one operation type to continue."), Me)
                    Return False 'Sohail (29 Apr 2019)
                End If
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf mintCalcTypeId = enLoanCalcId.No_Interest_With_Mapped_Head Then
                If radTopup.Checked = False Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Please select atleast one operation type to continue."), Me)
                    Return False
                End If
                'Sohail (29 Apr 2019) -- End
            Else
                If radInterestRate.Checked = False AndAlso radInstallment.Checked = False AndAlso radTopup.Checked = False Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Sorry, Please select atleast one operation type to continue."), Me)
                    Return False 'Sohail (29 Apr 2019)
                End If
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                'Varsha (25 Nov 2017) -- Start
                'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                'Language.setLanguage(mstrModuleName)
                'Varsha (25 Nov 2017) -- End
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Sorry, Period is mandatory information. Please select Period to continue."), Me)
                cboPeriod.Focus()
                Return False
            End If

            'Nilay (09-Aug-2016) -- Start
            If mblnAllowChangeStatus Then
                If CInt(cboStatus.SelectedValue) = 0 Then
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    'Language.setLanguage(mstrModuleName)
                    'Varsha (25 Nov 2017) -- End
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Please select atleast one Status from the list to perform further operation."), Me)
                    cboStatus.Focus()
                    Return False
                End If
            End If
            'Nilay (09-Aug-2016) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If dtpEffectiveDate.GetDate.Date <> Nothing Then
                If dtpEffectiveDate.GetDate.Date > CDate(objPeriod._End_Date) Then
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    'Language.setLanguage(mstrModuleName)
                    'Varsha (25 Nov 2017) -- End
                    DisplayMessage.DisplayMessage("Sorry, You cannot set Effective Date greater than Period End Date" & " " & Format(objPeriod._End_Date, "dd-MMM-yyyy") & ".", Me)
                    dtpEffectiveDate.SetDate = CDate(objPeriod._Start_Date)
                    Call dtpEffectiveDate_TextChanged(dtpEffectiveDate, New EventArgs())
                    dtpEffectiveDate.Focus()
                    Return False
                End If
            End If

            Dim objTnaLeaveTran As New clsTnALeaveTran

            If objTnaLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), mintEmployeeId.ToString, CDate(objPeriod._End_Date)) Then
                Select Case mintParameterId
                    Case enParameterMode.LN_RATE
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 10, "Sorry, you cannot add this Interest Rate Information from this employee loan.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
                    Case enParameterMode.LN_EMI
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 11, "Sorry, you cannot add this Installment Information from this employee loan.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
                    Case enParameterMode.LN_TOPUP
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 12, "Sorry, you cannot add this Topup Information from this employee loan.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
                End Select
                objPeriod = Nothing
                objTnaLeaveTran = Nothing
                Return False
            End If

            objTnaLeaveTran = Nothing
            objPeriod = Nothing

            Select Case mintParameterId
                Case enParameterMode.LN_RATE
                    If CDec(txtLoanInterest.Text) <= 0 Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "Sorry, Interest Rate is mandatory information. Please set Interest Rate to continue."), Me)
                        txtLoanInterest.Focus()
                        Return False
                    End If
                    'Nilay (13-Sept-2016) -- Start
                    'Enhancement : Enable Default Parameter Edit & other fixes
                    'Dim strMsg As String = ""
                    'strMsg = objLoanRate.IsValidInterestRate(mintLoanAdvanceTranunkid, dtpEffectiveDate.GetDate.Date, CDec(txtLoanInterest.Text))
                    'If strMsg <> "" Then
                    '    DisplayMessage.DisplayMessage(strMsg, Me)
                    '    txtLoanInterest.Focus()
                    '    Return False
                    'End If
                    If mActionMode <> enAction.EDIT_ONE Then
                        Dim strMsg As String = ""
                        strMsg = objLoanRate.IsValidInterestRate(mintLoanAdvanceTranunkid, dtpEffectiveDate.GetDate.Date, CDec(txtLoanInterest.Text))
                        If strMsg <> "" Then
                            'Varsha (25 Nov 2017) -- Start
                            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                            'Language.setLanguage(mstrModuleName)
                            'Varsha (25 Nov 2017) -- End
                            DisplayMessage.DisplayMessage(strMsg, Me)
                            txtLoanInterest.Focus()
                            Return False
                        End If
                    End If
                    'Nilay (13-Sept-2016) -- End

                Case enParameterMode.LN_EMI
                    If mintCalcTypeId = enLoanCalcId.No_Interest Then
                        If CDec(txtInstallmentAmt.Text) <= 0 Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 4, "Sorry, Installment amount is mandatory information. Please set Installment amount to continue."), Me)
                            txtInstallmentAmt.Focus()
                            Return False
                        End If
                    End If

                    If mintCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                        If CDec(txtPrincipalAmt.Text) <= 0 Then
                            'Varsha (25 Nov 2017) -- Start
                            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                            'Language.setLanguage(mstrModuleName)
                            'Varsha (25 Nov 2017) -- End
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 14, "Sorry, Principal amount is mandatory information. Please set Principal amount to continue."), Me)
                            txtPrincipalAmt.Focus()
                            Return False
                        End If
                    End If

                    'Sohail (26 Mar 2018) -- Start
                    'Le Grand Casino and Plam Beach Hotel and Casino Issue - Support Issue Id # 0002139 - 70.2 - Unable to change loan installment for no interest loan due to loan end date doesnt change when topups were added in previous periods.
                    'If DateAdd(DateInterval.Month, CDec(txtDuration.Text), CDate(mdtLoanEffectiveDate)).AddDays(-1).Date <= mdtBalanceEndDate.Date Then
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    'If Not (mintCalcTypeId = CInt(enLoanCalcId.No_Interest) OrElse mintCalcTypeId = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI)) AndAlso DateAdd(DateInterval.Month, CDec(txtDuration.Text), CDate(mdtLoanEffectiveDate)).AddDays(-1).Date <= mdtBalanceEndDate.Date Then
                    If Not (mintCalcTypeId = CInt(enLoanCalcId.No_Interest) OrElse mintCalcTypeId = CInt(enLoanCalcId.No_Interest_With_Mapped_Head) OrElse mintCalcTypeId = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI)) AndAlso DateAdd(DateInterval.Month, CDec(txtDuration.Text), CDate(mdtLoanEffectiveDate)).AddDays(-1).Date <= mdtBalanceEndDate.Date Then
                        'Sohail (29 Apr 2019) -- End
                        'Sohail (26 Mar 2018) -- End
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 5, "Sorry, Loan Stop Date should be greater than") & " " & mdtBalanceEndDate.ToShortDateString, Me)
                        txtInstallmentAmt.Focus()
                        Return False
                    End If
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    Dim objLoanScheme As New clsLoan_Scheme
                    objLoanScheme._Loanschemeunkid = mintLoanSchemeunkid
                    If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(txtDuration.Text) > objLoanScheme._MaxNoOfInstallment Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, " for ") & objLoanScheme._Name & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, " Scheme."), Me)
                        txtDuration.Focus()
                        Exit Function
                    End If
                    'Varsha (25 Nov 2017) -- End
                Case enParameterMode.LN_TOPUP
                    If CDec(txtTopupAmt.Text) <= 0 Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 6, "Sorry, Topup amount is mandatory information. Please set Totup amount to continue."), Me)
                        txtTopupAmt.Focus()
                        Return False
                    End If

            End Select

            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations

            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            'If CInt(cboStatus.SelectedValue) = 3 AndAlso txtRemark.Text.Trim.Length <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 14, "Remark cannot be blank. Remark is compulsory information."), Me)
            '    txtRemark.Focus()
            '    Return False
            'End If
            If mblnAllowChangeStatus = True Then
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'If CInt(cboStatus.SelectedValue) = 3 AndAlso txtRemark.Text.Trim.Length <= 0 Then
                If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED AndAlso txtRemark.Text.Trim.Length <= 0 Then
                    'Nilay (20-Sept-2016) -- End
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Remark cannot be blank. Remark is compulsory information."), Me)
                    txtRemark.Focus()
                    Return False
                End If
            End If
            'Nilay (13-Sept-2016) -- End

            'Nilay (01-Apr-2016) -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Valid_Data:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Function IsSaveAllowed(ByVal xColumnToMatch As String, ByVal xColumnUnkid As Integer) As Boolean
        Try
            Dim objLnAdv As New clsLoan_Advance
            Dim ds As DataSet = objLnAdv.GetLastLoanBalance("List", mintLoanAdvanceTranunkid)
            objLnAdv = Nothing

            If mintTabUnkid <= 0 Then
                If ds.Tables("List").Rows.Count > 0 Then
                    If ds.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(dtpEffectiveDate.GetDate.Date) Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 13, "Sorry, Effective date should not less than last transactoin date") & " " & Format(eZeeDate.convertDate(ds.Tables("List").Rows(0).Item("end_date").ToString).AddDays(1), "dd-MMM-yyyy") & ".", Me)
                        Return False
                    End If
                End If

            Else
                Dim strMsg As String = String.Empty
                If Not (ds.Tables("List").Rows.Count > 0 AndAlso CInt(ds.Tables("List").Rows(0).Item(xColumnToMatch)) = xColumnUnkid) Then
                    If ds.Tables("List").Rows.Count > 0 Then
                        If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                            'Language.setLanguage(mstrModuleName)
                            strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clslnloan_interest_tran", 3, " Process Payroll.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
                            'Language.setLanguage(mstrModuleName)
                            strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clslnloan_interest_tran", 4, " Loan Payment List.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
                            'Language.setLanguage(mstrModuleName)
                            strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clslnloan_interest_tran", 5, " Receipt Payment List.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                            'Language.setLanguage(mstrModuleName)
                            strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clslnloan_interest_tran", 6, " Installment Change.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                            'Language.setLanguage(mstrModuleName)
                            strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clslnloan_interest_tran", 7, " Topup Added.")
                        ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                            'Language.setLanguage(mstrModuleName)
                            strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clslnloan_interest_tran", 8, " Rate Change.")
                        End If
                    End If
                End If
                If strMsg.Trim.Length > 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 9, "Sorry, You cannot save this transaction. Please delete last transaction from the screen of") & strMsg, Me)
                    Return False
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsSaveAllowed:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Private Sub ClearParameters()
        Try
            pnlControls.Enabled = False
            pnlradOpr.Enabled = True

            radInterestRate.Checked = False
            radInstallment.Checked = False
            radTopup.Checked = False

            'cboPeriod.Enabled = False 'Nilay (01-Apr-2016)
            dtpEffectiveDate.Enabled = False
            'objlblExRate.Text = ""
            txtLoanInterest.Text = "0"
            txtDuration.Text = "1"
            txtInstallmentAmt.Text = "0"
            txtTopupAmt.Text = "0"

            lblTopupAmt.Visible = False
            txtTopupAmt.Visible = False

            lblEMIAmount.Visible = True
            txtInstallmentAmt.Visible = True
            txtInstallmentAmt.Enabled = False

            txtLoanInterest.Enabled = False
            txtDuration.Enabled = False
            txtTopupAmt.Enabled = False

            objlblEndDate.Text = ""

            Me.ViewState("DoNotAllowChangePeriod") = Nothing
            Me.ViewState("ParameterId") = Nothing
            Me.ViewState("PopupShowHide") = Nothing
            Me.ViewState("CalcTypeId") = Nothing
            Me.ViewState("CountryUnkid") = Nothing
            Me.ViewState("BaseExRate") = Nothing
            Me.ViewState("PaidExRate") = Nothing
            Me.ViewState("PaidCurrId") = Nothing
            Me.ViewState("BaseCurrSign") = Nothing
            Me.ViewState("BalanceEndDate") = Nothing
            Me.ViewState("InstallmentAmount") = Nothing
            Me.ViewState("TopupAmount") = Nothing
            Me.ViewState("TableUnkid") = Nothing

            Me.Session("LoanInfo") = Nothing

            'mblnDoNotAllowChangePeriod = CBool(Me.ViewState("DoNotAllowChangePeriod")) 'Nilay (01-Apr-2016)
            mintParameterId = CType(Me.ViewState("ParameterId"), enParameterMode)
            mblnPopupShowHide = CBool(Me.ViewState("PopupShowHide"))
            mintCalcTypeId = CType(Me.ViewState("CalcTypeId"), enLoanCalcId)
            mintCountryUnkid = CInt(Me.ViewState("CountryUnkid"))
            mdecBaseExRate = CDec(Me.ViewState("BaseExRate"))
            mdecPaidExRate = CDec(Me.ViewState("PaidExRate"))
            mintPaidCurrId = CInt(Me.ViewState("PaidCurrId"))
            mstrBaseCurrSign = CStr(Me.ViewState("BaseCurrSign"))
            mdtBalanceEndDate = CDate(Me.ViewState("BalanceEndDate"))
            mdecInstallmentAmnt = CDec(Me.ViewState("InstallmentAmount"))
            mdecTopupAmnt = CDec(Me.ViewState("TopupAmount"))
            mintTabUnkid = CInt(Me.ViewState("TableUnkid"))
            mdsLoanInfo = CType(Me.Session("LoanInfo"), DataSet)

            'radInstallment.Checked = True
            'radInstallment_CheckedChanged(radInstallment, New EventArgs())
            'radInstallment.Checked = False

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ClearParameters:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private Sub ResetLoanPara()
        Try
            txtLoanInterest.Text = mdecInterestRate.ToString
            txtDuration.Text = mintNoOfInstl.ToString
            Call txtDuration_TextChanged(txtDuration, New EventArgs)
            txtTopupAmt.Text = "0"

            'pnlradOpr.Enabled = True
            'pnlControls.Enabled = False
            'cboPeriod.Enabled = False
            'dtpEffectiveDate.SetDate = Nothing
            'txtLoanInterest.Enabled = False
            'txtDuration.Enabled = False
            'lblEMIAmount.Visible = True
            'txtInstallmentAmt.Visible = True
            'txtInstallmentAmt.Enabled = False
            'lblTopupAmt.Visible = False
            'txtTopupAmt.Enabled = False

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetLoanPara:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01-Apr-2016) -- End

    'Nilay (05-May-2016) -- Start
    Private Sub setWebATParameters()
        Try
            Blank_ModuleName()
            StrModuleName2 = "mnuLoan_Advance_Savings"
            objlnOpApprovaltran._WebClientIP = Session("IP_ADD").ToString
            objlnOpApprovaltran._WebFormName = "frmLoanParameter"
            objlnOpApprovaltran._WebHostName = Session("HOST_NAME").ToString

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("setWebATParameters:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (05-May-2016) -- End

#End Region

#Region "Button's Event"

    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Protected Sub btnSavelnPara_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavelnPara.Click
        Dim blnFlag As Boolean = False
        Try
            If Valid_Data() = False Then Exit Sub
            Call SetValue()

            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            'If mblnAllowChangeStatus = False Then
            If mActionMode = enAction.ADD_ONE Then
                'Nilay (13-Sept-2016) -- End

                'Nilay (08-Dec-2016) -- Start
                'Enhancements: Same user is the approver and posting loan, loan should by pass Approval process
                'blnFlag = objlnOpApprovaltran.InsertApprovalTran(CStr(Session("Database_Name")), _
                '                                                 CInt(Session("UserId")), _
                '                                                 CInt(Session("Fin_year")), _
                '                                                 CInt(Session("CompanyUnkId")), _
                '                                                 mintEmployeeId, _
                '                                                 mintLoanSchemeunkid, _
                '                                                 CBool(Session("LoanApprover_ForLoanScheme")))
                blnFlag = objlnOpApprovaltran.InsertApprovalTran(CStr(Session("Database_Name")), _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 mdtStartDate, mdtEndDate, _
                                                                 CStr(Session("UserAccessModeSetting")), True, _
                                                                 mintEmployeeId, _
                                                                 mintLoanSchemeunkid, _
                                                                 CBool(Session("LoanApprover_ForLoanScheme")), _
                                                                 ConfigParameter._Object._CurrentDateAndTime)
                'Nilay (08-Dec-2016) -- End

                If blnFlag = False AndAlso objlnOpApprovaltran._Message <> "" Then
                    DisplayMessage.DisplayMessage(objlnOpApprovaltran._Message, Me)
                    Exit Sub
                    'Shani (21-Jul-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                Else
                    Dim objProcessLoan As New clsProcess_pending_loan
                    Dim enLoginmd As enLogin_Mode
                    Dim intUserUnkid As Integer = 0
                    Dim intEmpUnkId As Integer = 0
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        enLoginmd = enLogin_Mode.EMP_SELF_SERVICE
                        intEmpUnkId = CInt(Session("Employeeunkid"))
                    Else
                        enLoginmd = enLogin_Mode.MGR_SELF_SERVICE

                        'Varsha Rana (12-Sept-2017) -- Start
                        'Enhancement - Loan Topup in ESS
                        'intEmpUnkId = CInt(Session("UserId"))
                        intEmpUnkId = 0
                        intUserUnkid = CInt(Session("UserId"))
                        ' Varsha Rana (12-Sept-2017) -- End


                    End If

                    'Varsha Rana (12-Sept-2017) -- Start
                    'Enhancement - Loan Topup in ESS
                    Blank_ModuleName()
                    StrModuleName2 = "mnuLoan_Advance_Savings"
                    objProcessLoan._WebClientIP = Session("IP_ADD").ToString
                    objProcessLoan._WebFormName = "frmLoanParameter"
                    objProcessLoan._WebHostName = Session("HOST_NAME").ToString
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                        objProcessLoan._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                    End If
                    ' Varsha Rana (12-Sept-2017) -- End

                    'Nilay (10-Dec-2016) -- Start
                    'Issue #26: Setting to be included on configuration for Loan flow Approval process
                    If mblnIsSendEmailNotification = True Then
                        'Nilay (27-Dec-2016) -- Start
                        If objlnOpApprovaltran._FinalStatus = 1 Then
                            Select Case mintParameterId
                                Case enParameterMode.LN_RATE
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                    '                                                  mintLoanSchemeunkid, mintEmployeeId, _
                                    '                                                  CInt(IIf(objlnOpApprovaltran._MinApprovedPriority > 0, objlnOpApprovaltran._MinApprovedPriority, -1)), _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Rate, _
                                    '                                          False, objlnOpApprovaltran._Identify_Guid, _
                                    '                                          Session("ArutiSelfServiceURL").ToString, False, enLoginmd, intEmpUnkId, intUserUnkid)
                                    objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                                                                     mintLoanSchemeunkid, mintEmployeeId, _
                                                                                     CInt(IIf(objlnOpApprovaltran._MinApprovedPriority > 0, objlnOpApprovaltran._MinApprovedPriority, -1)), _
                                                                             clsProcess_pending_loan.enApproverEmailType.Loan_Rate, _
                                                                             False, objlnOpApprovaltran._Identify_Guid, _
                                                                             Session("ArutiSelfServiceURL").ToString, CInt(Session("CompanyUnkId")), False, enLoginmd, intEmpUnkId, intUserUnkid)
                                    'Sohail (30 Nov 2017) -- End
                                Case enParameterMode.LN_EMI
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                    '                                                  mintLoanSchemeunkid, mintEmployeeId, _
                                    '                                                  CInt(IIf(objlnOpApprovaltran._MinApprovedPriority > 0, objlnOpApprovaltran._MinApprovedPriority, -1)), _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Installment, _
                                    '                                          False, objlnOpApprovaltran._Identify_Guid, _
                                    '                                          Session("ArutiSelfServiceURL").ToString, False, enLoginmd, intEmpUnkId, intUserUnkid)
                                    objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                                                                      mintLoanSchemeunkid, mintEmployeeId, _
                                                                                      CInt(IIf(objlnOpApprovaltran._MinApprovedPriority > 0, objlnOpApprovaltran._MinApprovedPriority, -1)), _
                                                                              clsProcess_pending_loan.enApproverEmailType.Loan_Installment, _
                                                                              False, objlnOpApprovaltran._Identify_Guid, _
                                                                              Session("ArutiSelfServiceURL").ToString, CInt(Session("CompanyUnkId")), False, enLoginmd, intEmpUnkId, intUserUnkid)
                                    'Sohail (30 Nov 2017) -- End
                                Case enParameterMode.LN_TOPUP
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                    '                                                  mintLoanSchemeunkid, mintEmployeeId, _
                                    '                                                  CInt(IIf(objlnOpApprovaltran._MinApprovedPriority > 0, objlnOpApprovaltran._MinApprovedPriority, -1)), _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Topup, _
                                    '                                          False, objlnOpApprovaltran._Identify_Guid, _
                                    '                                          Session("ArutiSelfServiceURL").ToString, False, enLoginmd, intEmpUnkId, intUserUnkid)
                                    objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                                                                    mintLoanSchemeunkid, mintEmployeeId, _
                                                                                    CInt(IIf(objlnOpApprovaltran._MinApprovedPriority > 0, objlnOpApprovaltran._MinApprovedPriority, -1)), _
                                                                            clsProcess_pending_loan.enApproverEmailType.Loan_Topup, _
                                                                            False, objlnOpApprovaltran._Identify_Guid, _
                                                                            Session("ArutiSelfServiceURL").ToString, CInt(Session("CompanyUnkId")), False, enLoginmd, intEmpUnkId, intUserUnkid)
                                    'Sohail (30 Nov 2017) -- End
                            End Select

                        ElseIf objlnOpApprovaltran._FinalStatus = 2 Then
                            Select Case mintParameterId
                                Case enParameterMode.LN_RATE
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, _
                                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Rate, _
                                    '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                                    '                                          enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                    objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, _
                                                                              clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                              clsProcess_pending_loan.enApproverEmailType.Loan_Rate, _
                                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(Session("CompanyUnkId")), , _
                                                                              enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                    'Sohail (30 Nov 2017) -- End
                                Case enParameterMode.LN_EMI
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, _
                                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Installment, _
                                    '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                                    '                                          enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                    objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, _
                                                                            clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                            clsProcess_pending_loan.enApproverEmailType.Loan_Installment, _
                                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(Session("CompanyUnkId")), , _
                                                                            enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                    'Sohail (30 Nov 2017) -- End
                                Case enParameterMode.LN_TOPUP
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, _
                                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Topup, _
                                    '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                                    '                                          enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                    objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, _
                                                                              clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                              clsProcess_pending_loan.enApproverEmailType.Loan_Topup, _
                                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(Session("CompanyUnkId")), , _
                                                                              enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                    'Sohail (30 Nov 2017) -- End
                            End Select
                        End If
                        'Nilay (27-Dec-2016) -- End
                    End If
                    'Nilay (10-Dec-2016) -- End

                    'Shani (21-Jul-2016) -- End

                End If

                'Nilay (13-Sept-2016) -- Start
                'Enhancement : Enable Default Parameter Edit & other fixes
                'Else
            ElseIf mActionMode = enAction.EDIT_ONE Then

                If mblnAllowChangeStatus = True Then
                    'Nilay (13-Sept-2016) -- End
                    blnFlag = objlnOpApprovaltran.UpdatePendingOpChangeStatus(CStr(Session("Database_Name")), _
                                                                              CInt(Session("UserId")), _
                                                                              CInt(Session("Fin_year")), _
                                                                              CInt(Session("CompanyUnkId")), _
                                                                              mdtStartDate, mdtEndDate, _
                                                                              CStr(Session("UserAccessModeSetting")), _
                                                                              True, _
                                                                              mintLoanSchemeunkid, _
                                                                              CBool(Session("LoanApprover_ForLoanScheme")))

                    If blnFlag = False And objlnOpApprovaltran._Message <> "" Then
                        DisplayMessage.DisplayMessage(objlnOpApprovaltran._Message, Me)
                        Exit Sub
                        'Shani (21-Jul-2016) -- Start
                        'Enhancement - Create New Loan Notification 
                    Else
                        'Nilay (10-Dec-2016) -- Start
                        'Issue #26: Setting to be included on configuration for Loan flow Approval process
                        If mblnIsSendEmailNotification = True Then

                            Dim objProcessLoan As New clsProcess_pending_loan
                            'Nilay (20-Sept-2016) -- Start
                            'Enhancement : Cancel feature for approved but not assigned loan application
                            'If CInt(cboStatus.SelectedValue) = 2 Then
                            If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.APPROVED Then
                                'Nilay (20-Sept-2016) -- End
                                objlnOpApprovaltran._LoanOtherOptranunkid = mintlnOtherOpTranunkid
                                If objlnOpApprovaltran._FinalStatus = 1 Then
                                    Select Case mintParameterId
                                        Case enParameterMode.LN_RATE
                                            'Sohail (30 Nov 2017) -- Start
                                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                            'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                            '                                          mintLoanSchemeunkid, _
                                            '                                          mintEmployeeId, _
                                            '                                          objlnOpApprovaltran._Priority, _
                                            '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Rate, _
                                            '                                          False, objlnOpApprovaltran._Identify_Guid, _
                                            '                                          ConfigParameter._Object._ArutiSelfServiceURL, False, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                            objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                                      mintLoanSchemeunkid, _
                                                                                      mintEmployeeId, _
                                                                                      objlnOpApprovaltran._Priority, _
                                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Rate, _
                                                                                      False, objlnOpApprovaltran._Identify_Guid, _
                                                                                      ConfigParameter._Object._ArutiSelfServiceURL, CInt(Session("CompanyUnkId")), False, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                            'Sohail (30 Nov 2017) -- End
                                        Case enParameterMode.LN_EMI
                                            'Sohail (30 Nov 2017) -- Start
                                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                            'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                            '                                          mintLoanSchemeunkid, _
                                            '                                          mintEmployeeId, _
                                            '                                          objlnOpApprovaltran._Priority, _
                                            '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Installment, _
                                            '                                          False, objlnOpApprovaltran._Identify_Guid, _
                                            '                                          ConfigParameter._Object._ArutiSelfServiceURL, False, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                            objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                                      mintLoanSchemeunkid, _
                                                                                      mintEmployeeId, _
                                                                                      objlnOpApprovaltran._Priority, _
                                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Installment, _
                                                                                      False, objlnOpApprovaltran._Identify_Guid, _
                                                                                      ConfigParameter._Object._ArutiSelfServiceURL, CInt(Session("CompanyUnkId")), False, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                            'Sohail (30 Nov 2017) -- End
                                        Case enParameterMode.LN_TOPUP
                                            'Sohail (30 Nov 2017) -- Start
                                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                            'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                            '                                          mintLoanSchemeunkid, _
                                            '                                          mintEmployeeId, _
                                            '                                          objlnOpApprovaltran._Priority, _
                                            '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Topup, _
                                            '                                          False, objlnOpApprovaltran._Identify_Guid, _
                                            '                                          ConfigParameter._Object._ArutiSelfServiceURL, False, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                            objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                                      mintLoanSchemeunkid, _
                                                                                      mintEmployeeId, _
                                                                                      objlnOpApprovaltran._Priority, _
                                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Topup, _
                                                                                      False, objlnOpApprovaltran._Identify_Guid, _
                                                                                      ConfigParameter._Object._ArutiSelfServiceURL, CInt(Session("CompanyUnkId")), False, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                            'Sohail (30 Nov 2017) -- End
                                    End Select
                                ElseIf objlnOpApprovaltran._FinalStatus = 2 Then
                                    Select Case mintParameterId
                                        Case enParameterMode.LN_RATE
                                            'Sohail (30 Nov 2017) -- Start
                                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                            'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, clsProcess_pending_loan.enApproverEmailType.Loan_Rate, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                            objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, clsProcess_pending_loan.enApproverEmailType.Loan_Rate, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(Session("CompanyUnkId")), , enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                            'Sohail (30 Nov 2017) -- End
                                        Case enParameterMode.LN_EMI
                                            'Sohail (30 Nov 2017) -- Start
                                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                            'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, clsProcess_pending_loan.enApproverEmailType.Loan_Installment, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                            objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, clsProcess_pending_loan.enApproverEmailType.Loan_Installment, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(Session("CompanyUnkId")), , enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                            'Sohail (30 Nov 2017) -- End 
                                        Case enParameterMode.LN_TOPUP
                                            'Sohail (30 Nov 2017) -- Start
                                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                            'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, clsProcess_pending_loan.enApproverEmailType.Loan_Topup, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                            objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, clsProcess_pending_loan.enApproverEmailType.Loan_Topup, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(Session("CompanyUnkId")), , enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                            'Sohail (30 Nov 2017) -- End
                                    End Select
                                End If
                                'Nilay (20-Sept-2016) -- Start
                                'Enhancement : Cancel feature for approved but not assigned loan application
                                'ElseIf CInt(cboStatus.SelectedValue) = 3 Then
                            ElseIf CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED Then
                                'Nilay (20-Sept-2016) -- End
                                Select Case mintParameterId
                                    Case enParameterMode.LN_RATE
                                        'Sohail (30 Nov 2017) -- Start
                                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                        'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, clsProcess_pending_loan.enNoticationLoanStatus.REJECT, clsProcess_pending_loan.enApproverEmailType.Loan_Rate, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), txtRemark.Text, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, clsProcess_pending_loan.enNoticationLoanStatus.REJECT, clsProcess_pending_loan.enApproverEmailType.Loan_Rate, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(Session("CompanyUnkId")), txtRemark.Text, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        'Sohail (30 Nov 2017) -- End
                                    Case enParameterMode.LN_EMI
                                        'Sohail (30 Nov 2017) -- Start
                                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                        'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, clsProcess_pending_loan.enNoticationLoanStatus.REJECT, clsProcess_pending_loan.enApproverEmailType.Loan_Installment, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), txtRemark.Text, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, clsProcess_pending_loan.enNoticationLoanStatus.REJECT, clsProcess_pending_loan.enApproverEmailType.Loan_Installment, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(Session("CompanyUnkId")), txtRemark.Text, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        'Sohail (30 Nov 2017) -- End
                                    Case enParameterMode.LN_TOPUP
                                        'Sohail (30 Nov 2017) -- Start
                                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                        'objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, clsProcess_pending_loan.enNoticationLoanStatus.REJECT, clsProcess_pending_loan.enApproverEmailType.Loan_Topup, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), txtRemark.Text, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        objProcessLoan.Send_Notification_Employee(mintEmployeeId, mintLoanAdvanceTranunkid, clsProcess_pending_loan.enNoticationLoanStatus.REJECT, clsProcess_pending_loan.enApproverEmailType.Loan_Topup, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(Session("CompanyUnkId")), txtRemark.Text, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                        'Sohail (30 Nov 2017) -- End
                                End Select
                            End If

                            'Shani (21-Jul-2016) -- End
                        End If
                        'Nilay (10-Dec-2016) -- End
                    End If

                    'Nilay (13-Sept-2016) -- Start
                    'Enhancement : Enable Default Parameter Edit & other fixes
                ElseIf mblnAllowChangeStatus = False Then

                    Select Case mintParameterId
                        Case enParameterMode.LN_RATE
                            If IsSaveAllowed("lninteresttranunkid", mintTabUnkid) = False Then Exit Sub

                            Dim objLoan_Advance As New clsLoan_Advance
                            objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceTranunkid
                            objLoan_Advance._Interest_Rate = CDec(txtLoanInterest.Text)
                            objLoan_Advance._Userunkid = CInt(Session("UserId"))
                            blnFlag = objLoan_Advance.Update()

                            If blnFlag Then
                                objLoanRate._Lninteresttranunkid = mintTabUnkid
                                objLoanRate._Interest_Rate = CDec(txtLoanInterest.Text)
                                objLoanRate._Userunkid = CInt(Session("UserId"))

                                blnFlag = objLoanRate.Update()
                                If blnFlag = False AndAlso objLoanRate._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objLoanRate._Message, Me)
                                    Exit Sub
                                End If
                            Else
                                If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objLoan_Advance._Message, Me)
                                    objLoan_Advance = Nothing
                                    Exit Sub
                                End If
                            End If
                            objLoan_Advance = Nothing

                        Case enParameterMode.LN_EMI
                            If IsSaveAllowed("lnemitranunkid", mintTabUnkid) = False Then Exit Sub

                            Dim objLoan_Advance As New clsLoan_Advance
                            objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceTranunkid
                            objLoan_Advance._Loan_Duration = CInt(txtDuration.Text)
                            objLoan_Advance._Emi_Tenure = CInt(txtDuration.Text)
                            objLoan_Advance._Emi_Amount = CDec(txtInstallmentAmt.Text) / mdecPaidExRate
                            objLoan_Advance._Userunkid = CInt(Session("UserId"))
                            blnFlag = objLoan_Advance.Update()

                            If blnFlag Then
                                objLoanEMI._Lnemitranunkid = mintTabUnkid
                                objLoanEMI._Emi_Tenure = CInt(txtDuration.Text)
                                objLoanEMI._Emi_Amount = CDec(txtInstallmentAmt.Text) / mdecPaidExRate
                                objLoanEMI._Loan_Duration = CInt(txtDuration.Text)
                                objLoanEMI._Userunkid = CInt(Session("UserId"))

                                blnFlag = objLoanEMI.Update()
                                If blnFlag = False AndAlso objLoanEMI._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objLoanEMI._Message, Me)
                                    Exit Sub
                                End If
                            Else
                                If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objLoan_Advance._Message, Me)
                                    objLoan_Advance = Nothing
                                    Exit Sub
                                End If
                            End If

                        Case enParameterMode.LN_TOPUP
                            If IsSaveAllowed("lntopuptranunkid", mintTabUnkid) = False Then Exit Sub
                            objLoanTopup._Lntopuptranunkid = mintTabUnkid
                            objLoanTopup._Topup_Amount = CDec(txtTopupAmt.Text) / mdecPaidExRate

                            blnFlag = objLoanTopup.Update()
                            If blnFlag = False AndAlso objLoanTopup._Message <> "" Then
                                DisplayMessage.DisplayMessage(objLoanTopup._Message, Me)
                                Exit Sub
                            End If
                    End Select
                End If
                'Nilay (13-Sept-2016) -- End

            End If

            Call ClearParameters()

            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            If Request.QueryString.Count > 0 Then
                Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
            End If
            'Shani (21-Jul-2016) -- End
            Call Fill_List()
            Call FillPendingOpList()
            mblnPopupShowHide = False
            popupLoanParameter.Hide()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSavelnPara_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Protected Sub btnSavelnPara_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavelnPara.Click
    '    Dim blnFlag As Boolean = False
    '    Try
    '        If Valid_Data() = False Then Exit Sub
    '        Call SetValue()

    '        If mintTabUnkid <= 0 Then
    '            Select Case mintParameterId
    '                Case enParameterMode.LN_RATE
    '                    If IsSaveAllowed("lninteresttranunkid", mintTabUnkid) = False Then Exit Sub
    '                    'Nilay (25-Mar-2016) -- Start
    '                    'blnFlag = objLoanRate.Insert(True, CInt(Session("Fin_year")))
    '                    blnFlag = objLoanRate.Insert(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
    '                                                 mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True, True)
    '                    'Nilay (25-Mar-2016) -- End
    '                    If blnFlag = False AndAlso objLoanRate._Message <> "" Then
    '                        DisplayMessage.DisplayMessage(objLoanRate._Message, Me)
    '                        Exit Sub
    '                    End If
    '                Case enParameterMode.LN_EMI
    '                    If IsSaveAllowed("lnemitranunkid", mintTabUnkid) = False Then Exit Sub
    '                    'Nilay (25-Mar-2016) -- Start
    '                    'blnFlag = objLoanEMI.Insert(True, CInt(Session("Fin_year")))
    '                    blnFlag = objLoanEMI.Insert(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
    '                                                mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True, True)
    '                    'Nilay (25-Mar-2016) -- End
    '                    If blnFlag = False AndAlso objLoanEMI._Message <> "" Then
    '                        DisplayMessage.DisplayMessage(objLoanEMI._Message, Me)
    '                        Exit Sub
    '                    End If
    '                Case enParameterMode.LN_TOPUP
    '                    If IsSaveAllowed("lntopuptranunkid", mintTabUnkid) = False Then Exit Sub
    '                    'Nilay (25-Mar-2016) -- Start
    '                    'blnFlag = objLoanTopup.Insert(True, CInt(Session("Fin_year")))
    '                    blnFlag = objLoanTopup.Insert(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
    '                                                  mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True, True)
    '                    'Nilay (25-Mar-2016) -- End
    '                    If blnFlag = False AndAlso objLoanTopup._Message <> "" Then
    '                        DisplayMessage.DisplayMessage(objLoanTopup._Message, Me)
    '                        Exit Sub
    '                    End If
    '            End Select
    '        Else
    '            Select Case mintParameterId
    '                Case enParameterMode.LN_RATE
    '                    If IsSaveAllowed("lninteresttranunkid", mintTabUnkid) = False Then Exit Sub

    '                    Dim objLoan_Advance As New clsLoan_Advance
    '                    objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceTranunkid
    '                    objLoan_Advance._Interest_Rate = CDec(txtLoanInterest.Text)
    '                    objLoan_Advance._Userunkid = CInt(Session("UserId"))
    '                    blnFlag = objLoan_Advance.Update(CInt(Session("UserId")))

    '                    If blnFlag Then
    '                        blnFlag = objLoanRate.Update()
    '                        If blnFlag = False AndAlso objLoanRate._Message <> "" Then
    '                            DisplayMessage.DisplayMessage(objLoanRate._Message, Me)
    '                            Exit Sub
    '                        End If
    '                    Else
    '                        If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
    '                            DisplayMessage.DisplayMessage(objLoan_Advance._Message, Me)
    '                            objLoan_Advance = Nothing
    '                            Exit Sub
    '                        End If
    '                    End If
    '                    objLoan_Advance = Nothing

    '                Case enParameterMode.LN_EMI
    '                    If IsSaveAllowed("lnemitranunkid", mintTabUnkid) = False Then Exit Sub

    '                    Dim objLoan_Advance As New clsLoan_Advance
    '                    objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceTranunkid
    '                    objLoan_Advance._Loan_Duration = CInt(txtDuration.Text)
    '                    objLoan_Advance._Emi_Tenure = CInt(txtDuration.Text)
    '                    objLoan_Advance._Emi_Amount = mdecInstallmentAmnt / mdecPaidExRate
    '                    objLoan_Advance._Userunkid = CInt(Session("UserId"))
    '                    blnFlag = objLoan_Advance.Update(CInt(Session("UserId")))

    '                    If blnFlag Then
    '                        blnFlag = objLoanEMI.Update()
    '                        If blnFlag = False AndAlso objLoanEMI._Message <> "" Then
    '                            DisplayMessage.DisplayMessage(objLoanEMI._Message, Me)
    '                            Exit Sub
    '                        End If
    '                    Else
    '                        If blnFlag = False AndAlso objLoan_Advance._Message <> "" Then
    '                            DisplayMessage.DisplayMessage(objLoan_Advance._Message, Me)
    '                            objLoan_Advance = Nothing
    '                            Exit Sub
    '                        End If
    '                    End If

    '                Case enParameterMode.LN_TOPUP
    '                    If IsSaveAllowed("lntopuptranunkid", mintTabUnkid) = False Then Exit Sub
    '                    blnFlag = objLoanTopup.Update()
    '                    If blnFlag = False AndAlso objLoanTopup._Message <> "" Then
    '                        DisplayMessage.DisplayMessage(objLoanTopup._Message, Me)
    '                        Exit Sub
    '                    End If

    '            End Select
    '        End If

    '        Call ClearParameters()
    '        Call Fill_List()
    '        mblnPopupShowHide = False
    '        popupLoanParameter.Hide()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("btnSavelnPara_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Apr-2016) -- End



    Protected Sub btnCloselnPara_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloselnPara.Click
        Try
            Call ClearParameters()
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            If Request.QueryString.Count > 0 Then
                Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
            End If
            'Shani (21-Jul-2016) -- End
            popupLoanParameter.Hide()
            mblnPopupShowHide = False

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnCloselnPara_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region "RadioButton Events"
    Protected Sub radInstallment_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radInterestRate.CheckedChanged, _
                                                                                                             radInstallment.CheckedChanged, _
                                                                                                             radTopup.CheckedChanged
        Try
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            'Select Case CType(sender, RadioButton).ID.ToUpper
            '    Case radInterestRate.ID.ToUpper
            '        If CType(sender, RadioButton).Checked = True Then
            '            mintParameterId = enParameterMode.LN_RATE
            '            If pnlControls.Enabled = False Then pnlControls.Enabled = True

            '            'Call SetFormControls()
            '            Call SetDateValue()

            '            'If mintTabUnkid <= 0 Then
            '            '    radInstallment.Enabled = True
            '            '    radInstallment.Checked = False
            '            '    radTopup.Enabled = True
            '            '    radTopup.Checked = False
            '            'End If

            '        End If
            '    Case radInstallment.ID.ToUpper
            '        If CType(sender, RadioButton).Checked = True Then
            '            mintParameterId = enParameterMode.LN_EMI
            '            If pnlControls.Enabled = False Then pnlControls.Enabled = True

            '            'Call SetFormControls()
            '            Call SetDateValue()

            '            'If mintTabUnkid <= 0 Then
            '            '    pnlradOpr.Enabled = True
            '            '    radInterestRate.Enabled = True
            '            '    radInterestRate.Checked = False
            '            '    radTopup.Enabled = True
            '            '    radTopup.Checked = False
            '            'End If

            '        End If
            '    Case radTopup.ID.ToUpper
            '        If CType(sender, RadioButton).Checked = True Then
            '            mintParameterId = enParameterMode.LN_TOPUP
            '            If pnlControls.Enabled = False Then pnlControls.Enabled = True

            '            'Call SetFormControls()
            '            Call SetDateValue()

            '            'If mintTabUnkid <= 0 Then
            '            '    pnlradOpr.Enabled = True
            '            '    radInterestRate.Enabled = True
            '            '    radInterestRate.Checked = False
            '            '    radInstallment.Enabled = True
            '            '    radInstallment.Checked = False
            '            'End If

            '        End If
            'End Select
            'Call SetFormControls()
            'Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId)

            Call ResetLoanPara()

            Select Case CType(sender, RadioButton).ID.ToUpper
                Case radInterestRate.ID.ToUpper
                    If CType(sender, RadioButton).Checked = True Then
                        mintParameterId = enParameterMode.LN_RATE
                    End If
                Case radInstallment.ID.ToUpper
                    If CType(sender, RadioButton).Checked = True Then
                        mintParameterId = enParameterMode.LN_EMI
                    End If
                Case radTopup.ID.ToUpper
                    If CType(sender, RadioButton).Checked = True Then
                        mintParameterId = enParameterMode.LN_TOPUP
                    End If
            End Select

            If pnlControls.Enabled = False Then pnlControls.Enabled = True
            'Nilay (13-Sept-2016) -- Start
            'Enhancement : Enable Default Parameter Edit & other fixes
            'Call SetDateValue()
            If mActionMode <> enAction.EDIT_ONE Then
                Call SetDateValue()
            End If
            'Nilay (13-Sept-2016) -- End
            Call SetFormControls()
            Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId)
            'Nilay (01-Apr-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("RadioButton CheckChanged Event:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region "TextBox's Events"

    Protected Sub txtDuration_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDuration.TextChanged
        Try
            If CType(sender, TextBox).ID = txtDuration.ID Then

                Dim dtEndDate As Date = DateAdd(DateInterval.Month, CDbl(txtDuration.Text), CDate(mdtLoanEffectiveDate).Date).AddDays(-1)
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
                'Sohail (26 Mar 2018) -- Start
                'Le Grand Casino and Plam Beach Hotel and Casino Issue - Support Issue Id # 0002139 - 70.2 - Unable to change loan installment for no interest loan due to loan end date doesnt change when topups were added in previous periods.
                'If dtEndDate < objPeriod._End_Date Then
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'If Not (mintCalcTypeId = CInt(enLoanCalcId.No_Interest) OrElse mintCalcTypeId = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI)) AndAlso dtEndDate < objPeriod._End_Date Then
                If Not (mintCalcTypeId = CInt(enLoanCalcId.No_Interest) OrElse mintCalcTypeId = CInt(enLoanCalcId.No_Interest_With_Mapped_Head) OrElse mintCalcTypeId = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI)) AndAlso dtEndDate < objPeriod._End_Date Then
                    'Sohail (29 Apr 2019) -- End
                    'Sohail (26 Mar 2018) -- End
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 15, "Loan stop date cannot be less than current open period end date") & " " & Format(eZeeDate.convertDate(objPeriod._End_Date), "dd-MMM-yyyy"), Me)
                    Exit Try
                End If

                If CInt(txtDuration.Text) > 0 Then
                    objlblEndDate.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Loan Stop Date :") & " " & DateAdd(DateInterval.Month, CDec(txtDuration.Text), CDate(mdtLoanEffectiveDate)).AddDays(-1)
                Else
                    objlblEndDate.Text = ""
                End If
            End If

            Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId, True)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtDuration_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtLoanInterest_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLoanInterest.TextChanged
        Try
            If radInterestRate.Checked = True Then
                Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtLoanInterest_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtPrincipalAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPrincipalAmt.TextChanged
        Try
            If mintCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                If CDec(txtPrincipalAmt.Text) = 0 Then
                    Dim objEmi As New clslnloan_emitenure_tran
                    Dim dsEmi As DataSet = objEmi.GetLastEMI("List", mintLoanAdvanceTranunkid, CDate(mdtLoanEffectiveDate).Date)
                    If dsEmi IsNot Nothing AndAlso dsEmi.Tables("List").Rows.Count > 0 Then
                        txtPrincipalAmt.Text = Format(CDec(dsEmi.Tables("List").Rows(0).Item("principal_amount")), Session("fmtCurrency").ToString)
                    End If
                End If
                Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtPrincipalAmt_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtInstallmentAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtInstallmentAmt_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ComboBox's Events"

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Call SetDateValue()
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            mblnPopupShowHide = True
            'Nilay (01-Apr-2016) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "DateControl's Event"
    Protected Sub dtpEffectiveDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpEffectiveDate.TextChanged
        Try
            Call Calculate_Loan_Installment_Amount(mintCalcTypeId, mintInterestCalcTypeId)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dtpEffectiveDate_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbFilterCriteria", Me.lblDetialHeader.Text)

            'Hemant (17 Sep 2020) -- Start
            'New UI Change
            'Me.lblpopupHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, Me.lblPageHeader.Text)
            'Hemant (17 Sep 2020) -- End
            Me.lblPopupDetailHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbFilterCriteria", Me.lblDetialHeader.Text)

            Me.lblEmpName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.lblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblVocNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVocNo.ID, Me.lblVocNo.Text)

            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.lblDuration.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDuration.ID, Me.lblDuration.Text)
            Me.lblLoanInterest.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanInterest.ID, Me.lblLoanInterest.Text)
            Me.lblEMIAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEMIAmount.ID, Me.lblEMIAmount.Text)
            Me.btnSavelnPara.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSavelnPara.ID, Me.btnSavelnPara.Text)
            Me.btnCloselnPara.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCloselnPara.ID, Me.btnCloselnPara.Text)
            Me.lblTopupAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTopupAmt.ID, Me.lblTopupAmt.Text)
            Me.lblEffectiveDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEffectiveDate.ID, Me.lblEffectiveDate.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.elOperation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.elOperation.ID, Me.elOperation.Text)
            Me.radTopup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.radTopup.ID, Me.radTopup.Text)
            Me.radInstallment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.radInstallment.ID, Me.radInstallment.Text)
            Me.radInterestRate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.radInterestRate.ID, Me.radInterestRate.Text)
            Me.lblPrincipalAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPrincipalAmt.ID, Me.lblPrincipalAmt.Text)
            Me.lblInterestAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInterestAmt.ID, Me.lblInterestAmt.Text)

            Me.lvData.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvData.Columns(2).FooterText, Me.lvData.Columns(2).HeaderText)
            Me.lvData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvData.Columns(3).FooterText, Me.lvData.Columns(3).HeaderText)
            Me.lvData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvData.Columns(4).FooterText, Me.lvData.Columns(4).HeaderText)
            Me.lvData.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvData.Columns(5).FooterText, Me.lvData.Columns(5).HeaderText)
            Me.lvData.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvData.Columns(6).FooterText, Me.lvData.Columns(6).HeaderText)
            Me.lvData.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvData.Columns(7).FooterText, Me.lvData.Columns(7).HeaderText)


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
