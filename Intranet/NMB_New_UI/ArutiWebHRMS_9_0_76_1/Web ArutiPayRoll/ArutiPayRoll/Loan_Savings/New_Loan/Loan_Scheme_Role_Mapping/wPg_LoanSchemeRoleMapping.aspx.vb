﻿Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing

Partial Class Loan_Savings_New_Loan_Loan_Scheme_Role_Mapping_wPg_LoanSchemeRoleMapping
    Inherits Basepage


#Region "Private Variable"

    Private DisplayMessage As New CommonCodes
    Private mdicETS As Dictionary(Of String, Integer)
    Private mstrModuleName As String = "frmLoanSchemeRoleMapping"
    Private mintMappingunkid As Integer
    Private currentId As String = ""
    Private Index As Integer
    Private mblnShowpopupLoanSchemeMapping As Boolean = False

    'Pinkal (04-Aug-2023) -- Start
    '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
    Private mblnLoanTranche As Boolean = False
    'Pinkal (04-Aug-2023) -- End

#End Region

#Region "Form Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If


            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            'Pinkal (20-Jan-2023) -- Start
            'Solved NMB Issue for Randamely Access of URL in Loan module.
            If Session("UserId") Is Nothing OrElse CInt(Session("UserId")) <= 0 Then
                DisplayMessage.DisplayMessage("you are not authorized to access this page.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Pinkal (20-Jan-2023) -- End

            mdicETS = GvLoanSchemeRoleList.Columns.Cast(Of DataControlField).ToDictionary(Function(x) x.FooterText, Function(x) GvLoanSchemeRoleList.Columns.IndexOf(x))

            If IsPostBack = False Then
                GC.Collect()
                Call SetControlCaptions()
                SetMessages()
                Call SetLanguage()

                If Request.QueryString.Count <= 0 Then Exit Sub
                Dim mstrRequset As String = ""
                Try
                    mstrRequset = b64decode(Server.UrlDecode(Request.QueryString.ToString.Substring(3)))
                Catch
                    Session("clsuser") = Nothing
                    DisplayMessage.DisplayMessage("Invalid Page Address.", Me, "../../index.aspx")
                    Exit Sub
                End Try

                If mstrRequset.Trim.Length > 0 Then
                    mblnLoanTranche = CBool(mstrRequset)
                End If

                ListFillCombo()
                FillList(True)
                SetVisibility()
            Else
                mintMappingunkid = CType(ViewState("mintMappingunkid"), Integer)
                mblnShowpopupLoanSchemeMapping = Convert.ToBoolean(ViewState("ShowpopupLoanSchemeMapping").ToString())
                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                mblnLoanTranche = CBool(ViewState("mblnLoanTranche"))
                'Pinkal (04-Aug-2023) -- End

                If mblnShowpopupLoanSchemeMapping Then
                    popupLoanSchemeMapping.Show()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintMappingunkid") = mintMappingunkid
            ViewState("ShowpopupLoanSchemeMapping") = mblnShowpopupLoanSchemeMapping
            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            ViewState("mblnLoanTranche") = mblnLoanTranche
            'Pinkal (04-Aug-2023) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (06-Jan-2023) -- Start
    '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (06-Jan-2023) -- End

#End Region

#Region "   For List"

#Region "   Private Function"

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsLoanScheme As DataSet = Nothing
        Dim dtLevel As DataTable = Nothing
        Dim strSearching As String = ""
        Dim objLoanSchemeRoleMapping As New clsloanscheme_role_mapping

        Try

            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            If CBool(Session("AllowToViewLoanSchemeRoleMapping")) = False Then Exit Sub
            'Pinkal (06-Jan-2023) -- End

            If isblank Then
                strSearching = "AND 1 = 2 "
            End If

            If CInt(cboLoanSchemeList.SelectedValue) > 0 Then
                strSearching &= "AND lnloanscheme_role_mapping.loanschemeunkid = " & CInt(cboLoanSchemeList.SelectedValue) & " "
            End If

            If CInt(cboRoleList.SelectedValue) > 0 Then
                strSearching &= "AND lnloanscheme_role_mapping.roleunkid = " & CInt(cboRoleList.SelectedValue) & " "
            End If

            If CInt(cboLevelList.SelectedValue) > 0 Then
                strSearching &= "AND lnloanscheme_role_mapping.levelunkid = " & CInt(cboLevelList.SelectedValue) & " "
            End If


            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            'dsLoanScheme = objLoanSchemeRoleMapping.GetList("List", True, strSearching)
            dsLoanScheme = objLoanSchemeRoleMapping.GetList("List", True, mblnLoanTranche, strSearching)
            'Pinkal (04-Aug-2023) -- End


            If dsLoanScheme.Tables(0).Rows.Count <= 0 Then
                isblank = True
                Dim drRow As DataRow = dsLoanScheme.Tables(0).NewRow()
                drRow("Level") = ""
                drRow("levelunkid") = 0
                drRow("mappingunkid") = 0
                drRow("isexceptional") = False
                dsLoanScheme.Tables(0).Rows.Add(drRow)
            End If

            If dsLoanScheme IsNot Nothing Then
                dtLevel = New DataView(dsLoanScheme.Tables(0), "", "priority,Loan_Scheme", DataViewRowState.CurrentRows).ToTable()

                Dim strLevelName As String = ""
                Dim dtTable As DataTable = dtLevel.Clone
                dtTable.Columns.Add("IsGrp", Type.GetType("System.Boolean"))
                Dim dtRow As DataRow = Nothing
                For Each drow As DataRow In dtLevel.Rows
                    If CStr(drow("Level")).Trim <> strLevelName.Trim Then
                        dtRow = dtTable.NewRow
                        dtRow("IsGrp") = True
                        dtRow("mappingunkid") = drow("mappingunkid")
                        dtRow("Level") = drow("Level")
                        strLevelName = drow("Level").ToString()
                        dtRow("isexceptional") = CBool(drow("isexceptional"))
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In dtLevel.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next

                dtTable.AcceptChanges()
                GvLoanSchemeRoleList.DataSource = dtTable
                GvLoanSchemeRoleList.DataBind()

                If isblank = True Then
                    GvLoanSchemeRoleList.Rows(0).Visible = False
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanSchemeRoleMapping = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnnew.Enabled = CBool(Session("AllowToaddLoanSchemeRoleMapping"))
            GvLoanSchemeRoleList.Columns(getColumnID_Griview(GvLoanSchemeRoleList, "dgcolhedit", False, True)).Visible = CBool(Session("AllowToEditLoanSchemeRoleMapping"))
            GvLoanSchemeRoleList.Columns(getColumnID_Griview(GvLoanSchemeRoleList, "dgcolhdelete", False, True)).Visible = Session("AllowToDeleteLoanSchemeRoleMapping")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ListFillCombo()
        Dim objLoanScheme As clsLoan_Scheme
        Dim objRole As clsUserRole_Master
        Dim objLevel As clslnapproverlevel_master
        Try

            objLoanScheme = New clsLoan_Scheme

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            Dim mstrFilter As String = ""

            If mblnLoanTranche Then
                mstrFilter = "loanschemecategory_id = " & enLoanSchemeCategories.MORTGAGE
            End If
            'Dim dsList As DataSet = objLoanScheme.getComboList(True, "List", -1, "", False, False, "")
            Dim dsList As DataSet = objLoanScheme.getComboList(True, "List", -1, mstrFilter, False, False, "")
            'Pinkal (04-Aug-2023) -- End

            With cboLoanSchemeList
                .DataTextField = "name"
                .DataValueField = "loanschemeunkid"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With

            With cboLoanScheme
                .DataTextField = "name"
                .DataValueField = "loanschemeunkid"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With


            dsList.Clear()
            dsList = Nothing

            objRole = New clsUserRole_Master
            dsList = objRole.getComboList("List", True)
            With cboRoleList
                .DataTextField = "name"
                .DataValueField = "roleunkid"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With

            With cboRole
                .DataTextField = "name"
                .DataValueField = "roleunkid"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With

            dsList.Clear()
            dsList = Nothing

            objLevel = New clslnapproverlevel_master

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            'dsList = objLevel.getListForCombo("List", True)
            dsList = objLevel.getListForCombo(mblnLoanTranche, "List", True)
            'Pinkal (04-Aug-2023) -- End
            With cboLevelList
                .DataTextField = "name"
                .DataValueField = "lnlevelunkid"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
            End With

            With cboLevel
                .DataTextField = "name"
                .DataValueField = "lnlevelunkid"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
            End With

            dsList.Clear()
            dsList = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRole = Nothing
            objLevel = Nothing
        End Try
    End Sub

    Private Sub SetAtValue(ByVal xFormName As String, ByVal objLoanSchemeMapping As clsloanscheme_role_mapping)
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objLoanSchemeMapping._AuditUserId = CInt(Session("UserId"))
            End If
            objLoanSchemeMapping._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objLoanSchemeMapping._ClientIP = CStr(Session("IP_ADD"))
            objLoanSchemeMapping._HostName = CStr(Session("HOST_NAME"))
            objLoanSchemeMapping._FormName = xFormName
            objLoanSchemeMapping._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "    Button Event"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            Reset()
            popupLoanSchemeMapping.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboLoanSchemeList.SelectedValue = "0"
            cboRoleList.SelectedValue = "0"
            cboLevelList.SelectedValue = "0"
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objLoanSchemeMapping As clsloanscheme_role_mapping

        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            objLoanSchemeMapping = New clsloanscheme_role_mapping
            objLoanSchemeMapping._Mappingunkid = lnkedit.CommandArgument.ToString()
            mintMappingunkid = lnkedit.CommandArgument.ToString()
            GetValue()
            mblnShowpopupLoanSchemeMapping = True
            popupLoanSchemeMapping.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanSchemeMapping = Nothing
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim objLoanScheme As clsloanscheme_role_mapping
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            objLoanScheme = New clsloanscheme_role_mapping

            objLoanScheme._Mappingunkid = CInt(lnkdelete.CommandArgument.ToString())
            mintMappingunkid = CInt(lnkdelete.CommandArgument.ToString())

            If objLoanScheme.isUsed(CInt(lnkdelete.CommandArgument.ToString())) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, You cannot delete this mapping of loan scheme with role . Reason: This Mapping is in use."), Me)
                Exit Sub
            End If

            popup_YesNo.Show()
            popup_YesNo.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Confirmation")
            popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Are you sure you want to delete ?")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanScheme = Nothing
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Dim objLoanScheme As clsloanscheme_role_mapping
        Try

            objLoanScheme = New clsloanscheme_role_mapping

            SetAtValue(mstrModuleName, objLoanScheme)

            objLoanScheme._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objLoanScheme._FormName = mstrModuleName

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objLoanScheme._Voiduserunkid = CInt(Session("UserId"))
            End If

            If objLoanScheme.Delete(mintMappingunkid) = False Then
                DisplayMessage.DisplayMessage(objLoanScheme._Message, Me)
                Exit Sub
            End If

            mintMappingunkid = 0

            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanScheme = Nothing
        End Try
    End Sub

#End Region

#Region "  Gridview Events"

    Protected Sub GvLoanSchemeRoleList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvLoanSchemeRoleList.RowDataBound
        Dim oldid As String
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                If Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then

                    Dim lnkEdit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                    lnkEdit.Visible = False

                    Dim lnkDelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                    lnkDelete.Visible = False

                    e.Row.Cells(3).Text = DataBinder.Eval(e.Row.DataItem, "Level").ToString
                    e.Row.Cells(3).ColumnSpan = e.Row.Cells.Count - 1
                    e.Row.BackColor = ColorTranslator.FromHtml("#ECECEC")
                    e.Row.Font.Bold = True

                    For i As Integer = 4 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next
                Else
                    'Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)

                    'lnkdelete.ToolTip = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Delete")

                    'If CBool(Session("AllowToDeleteOTRequisitionApprover")) Then
                    '    lnkdelete.Visible = True
                    'Else
                    '    lnkdelete.Visible = False
                    'End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub

#End Region

#End Region

#Region "   For Add/Edit"

#Region "   Private Function"

    Private Function Validation() As Boolean
        Try
            If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Loan Scheme is  compulsory information.Please select Loan Scheme."), Me)
                cboLoanScheme.Focus()
                Return False
            ElseIf CInt(cboRole.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Role is  compulsory information.Please select Role."), Me)
                cboRole.Focus()
                Return False
            ElseIf CInt(cboLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Level is  compulsory information.Please select Level."), Me)
                cboLevel.Focus()
                Return False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub GetValue()
        Dim objLoanSchemeMapping As clsloanscheme_role_mapping
        Try
            objLoanSchemeMapping = New clsloanscheme_role_mapping
            If mintMappingunkid > 0 Then
                objLoanSchemeMapping._Mappingunkid = mintMappingunkid
                cboLoanScheme.Enabled = False
            End If
            cboLoanScheme.SelectedValue = objLoanSchemeMapping._Loanschemeunkid.ToString()
            cboRole.SelectedValue = objLoanSchemeMapping._Roleunkid.ToString()
            cboLevel.SelectedValue = objLoanSchemeMapping._Levelunkid.ToString()
            chkExceptional.Checked = objLoanSchemeMapping._IsExceptional
            chkExceptional.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanSchemeMapping = Nothing
        End Try
    End Sub

    Private Sub SetValue(ByVal objLoanSchemeMapping As clsloanscheme_role_mapping)
        Try
            If mintMappingunkid > 0 Then objLoanSchemeMapping._Mappingunkid = mintMappingunkid
            objLoanSchemeMapping._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            objLoanSchemeMapping._IsExceptional = chkExceptional.Checked

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objLoanSchemeMapping._IsLoanTranche = mblnLoanTranche
            'Pinkal (04-Aug-2023) -- End

            objLoanSchemeMapping._Roleunkid = CInt(cboRole.SelectedValue)
            objLoanSchemeMapping._Levelunkid = CInt(cboLevel.SelectedValue)
            objLoanSchemeMapping._Userunkid = CInt(Session("UserId"))
            SetAtValue(mstrModuleName, objLoanSchemeMapping)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub Reset()
        Try
            cboLoanScheme.Enabled = True
            cboLoanScheme.SelectedValue = "0"
            cboRole.SelectedValue = "0"
            cboLevel.SelectedValue = "0"
            chkExceptional.Checked = False
            chkExceptional.Enabled = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "    Button Event"

    Protected Sub btnSaveLoanSchemeRoleMapping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveLoanSchemeRoleMapping.Click
        Dim objLoanSchemeMapping As New clsloanscheme_role_mapping
        Try

            If Validation() = False Then
                mblnShowpopupLoanSchemeMapping = True
                popupLoanSchemeMapping.Show()
                Exit Sub
            End If


            SetValue(objLoanSchemeMapping)

            Dim blnFlag As Boolean = False
            If mintMappingunkid > 0 Then
                blnFlag = objLoanSchemeMapping.Update()
            ElseIf mintMappingunkid <= 0 Then
                blnFlag = objLoanSchemeMapping.Insert()
            End If

            If blnFlag = False And objLoanSchemeMapping._Message <> "" Then
                DisplayMessage.DisplayMessage(objLoanSchemeMapping._Message, Me)
                mblnShowpopupLoanSchemeMapping = True
                popupLoanSchemeMapping.Show()
                Exit Sub
            Else
                FillList(False)
                Reset()
                mintMappingunkid = 0
                ViewState("mintMappingunkid") = mintMappingunkid
                mblnShowpopupLoanSchemeMapping = False
                popupLoanSchemeMapping.Hide()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanSchemeMapping = Nothing
        End Try
    End Sub

    Protected Sub btnCloseLoanSchemeRoleMapping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseLoanSchemeRoleMapping.Click
        Try
            Reset()
            mblnShowpopupLoanSchemeMapping = False
            popupLoanSchemeMapping.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblAddEditPageHeader.ID, Me.LblAddEditPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCaption.ID, Me.lblCaption.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanSchemeList.ID, Me.LblLoanSchemeList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRoleList.ID, Me.LblRoleList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLevelList.ID, Me.LblLevelList.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanScheme.ID, Me.LblLoanScheme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRole.ID, Me.LblRole.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLevel.ID, Me.LblLevel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkExceptional.ID, Me.chkExceptional.Text)


            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLoanSchemeRoleList.Columns(2).FooterText, Me.GvLoanSchemeRoleList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLoanSchemeRoleList.Columns(3).FooterText, Me.GvLoanSchemeRoleList.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLoanSchemeRoleList.Columns(4).FooterText, Me.GvLoanSchemeRoleList.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.GvLoanSchemeRoleList.Columns(5).FooterText, Me.GvLoanSchemeRoleList.Columns(5).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnnew.ID, Me.btnnew.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSaveLoanSchemeRoleMapping.ID, Me.btnSaveLoanSchemeRoleMapping.Text.Replace("&", ""))
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCloseLoanSchemeRoleMapping.ID, Me.btnCloseLoanSchemeRoleMapping.Text.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.LblAddEditPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblAddEditPageHeader.ID, Me.LblAddEditPageHeader.Text)
            Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCaption.ID, Me.lblCaption.Text)
            Me.LblLoanSchemeList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanSchemeList.ID, Me.LblLoanSchemeList.Text)
            Me.LblRoleList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRoleList.ID, Me.LblRoleList.Text)
            Me.LblLevelList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLevelList.ID, Me.LblLevelList.Text)
            Me.LblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanScheme.ID, Me.LblLoanScheme.Text)
            Me.LblRole.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRole.ID, Me.LblRole.Text)
            Me.LblLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLevel.ID, Me.LblLevel.Text)
            Me.chkExceptional.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkExceptional.ID, Me.chkExceptional.Text)

            Me.GvLoanSchemeRoleList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLoanSchemeRoleList.Columns(2).FooterText, Me.GvLoanSchemeRoleList.Columns(2).HeaderText)
            Me.GvLoanSchemeRoleList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLoanSchemeRoleList.Columns(3).FooterText, Me.GvLoanSchemeRoleList.Columns(3).HeaderText)
            Me.GvLoanSchemeRoleList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLoanSchemeRoleList.Columns(4).FooterText, Me.GvLoanSchemeRoleList.Columns(4).HeaderText)
            Me.GvLoanSchemeRoleList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.GvLoanSchemeRoleList.Columns(5).FooterText, Me.GvLoanSchemeRoleList.Columns(5).HeaderText)

            Me.btnnew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnnew.ID, Me.btnnew.Text.Replace("&", ""))
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text.Replace("&", ""))
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text.Replace("&", ""))
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))
            Me.btnSaveLoanSchemeRoleMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveLoanSchemeRoleMapping.ID, Me.btnSaveLoanSchemeRoleMapping.Text.Replace("&", ""))
            Me.btnCloseLoanSchemeRoleMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCloseLoanSchemeRoleMapping.ID, Me.btnCloseLoanSchemeRoleMapping.Text.Replace("&", ""))


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Loan Scheme is  compulsory information.Please select Loan Scheme.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Role is  compulsory information.Please select Role.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Level is  compulsory information.Please select Level.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, You cannot delete this mapping of loan scheme with role . Reason: This Mapping is in use.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Confirmation")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Are you sure you want to delete ?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings


End Class
