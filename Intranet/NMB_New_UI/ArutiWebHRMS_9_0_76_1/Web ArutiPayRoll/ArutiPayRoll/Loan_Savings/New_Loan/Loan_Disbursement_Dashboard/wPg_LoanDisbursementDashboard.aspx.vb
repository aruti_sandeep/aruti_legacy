﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Globalization
#End Region

Partial Class Loan_Savings_New_Loan_Loan_Disbursement_Dashboard_wPg_LoanDisbursementDashboard
    Inherits Basepage

#Region "Private Variables"

    Private Shared ReadOnly mstrModuleName As String = "frmLoanDisbursementDashboard"
    Dim DisplayMessage As New CommonCodes
    Private objRoleApprovaltran As New clsProcess_pending_loan
    Private mstrAdvanceFilter As String = ""

    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .
    Dim mstrSecretKey As String = ""
    Private blnShowTOTPPopup As Boolean = False
    Dim Counter As Integer = 0
    Dim mstrEmpMobileNo As String = ""
    Dim mstrEmployeeName As String = ""
    Dim mstrEmployeeEmail As String = ""
    'Pinkal (02-Jun-2023) -- End

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            'Pinkal (20-Jan-2023) -- Start
            'Solved NMB Issue for Randamely Access of URL in Loan module.
            If Session("UserId") Is Nothing OrElse CInt(Session("UserId")) <= 0 Then
                DisplayMessage.DisplayMessage("you are not authorized to access this page.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Pinkal (20-Jan-2023) -- End

            If IsPostBack = False Then
                Call SetControlCaptions()
                'Pinkal (14-Dec-2022) -- Start
                'NMB Loan Module Enhancement.
                SetMessages()
                'Pinkal (14-Dec-2022) -- End
                Call SetLanguage()
                Call FillCombo()

                dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date

                If CInt(cboStatus.SelectedIndex) <= 1 Then
                    btnPost.Enabled = False
                Else
                    btnPost.Enabled = True
                End If

                If dgLoanApplication.Items.Count <= 0 Then
                    dgLoanApplication.DataSource = New List(Of String)
                    dgLoanApplication.DataBind()
                End If
            Else
                mstrAdvanceFilter = CStr(Me.ViewState("AdvanceFilter"))
                'Pinkal (02-Jun-2023) -- Start
                'NMB Enhancement : TOTP Related Changes .
                mstrSecretKey = CStr(Me.ViewState("mstrSecretKey"))
                Counter = CInt(Me.ViewState("Counter"))
                blnShowTOTPPopup = CBool(Me.ViewState("blnShowTOTPPopup"))
                mstrEmployeeName = CStr(Me.ViewState("EmployeeName"))
                mstrEmployeeEmail = CStr(Me.ViewState("EmployeeEmail"))
                mstrEmpMobileNo = CStr(Me.ViewState("EmployeeMobileNo"))

                If blnShowTOTPPopup Then
                    popup_TOTP.Show()
                End If

                'Pinkal (02-Jun-2023) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Me.ViewState("Counter") = Counter
            Me.ViewState("mstrSecretKey") = mstrSecretKey
            Me.ViewState("blnShowTOTPPopup") = blnShowTOTPPopup
            Me.ViewState("EmployeeName") = mstrEmployeeName
            Me.ViewState("EmployeeEmail") = mstrEmployeeEmail
            Me.ViewState("EmployeeMobileNo") = mstrEmpMobileNo
            'Pinkal (02-Jun-2023) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            Dim objEmployee As New clsEmployee_Master
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objLoanApplication As New clsProcess_pending_loan
            Dim objMasterData As New clsMasterData
            'Pinkal (20-Jan-2023) -- Start
            'Solved NMB Issue for Randamely Access of URL in Loan module.

            'Dim objLevel As New clslnapproverlevel_master
            'Dim objLoanApprover As New clsLoanApprover_master

            'dsList = objLoanApprover.GetEmployeeFromUser(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                             CStr(Session("UserAccessModeSetting")), _
            '                                             True, _
            '                                             CBool(Session("IsIncludeInactiveEmp")), _
            '                                             "List")

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                    CInt(Session("UserId")), _
                                    CInt(Session("Fin_year")), _
                                    CInt(Session("CompanyUnkId")), _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                    Session("UserAccessModeSetting").ToString(), True, _
                                    False, "List", True)

            'Pinkal (20-Jan-2023) -- End

            With cboEmployee
                .DataTextField = "EmpCodeName"
                .DataValueField = "employeeunkid"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With


            Dim mblnSchemeShowOnEss As Boolean = False
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then mblnSchemeShowOnEss = True
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", mblnSchemeShowOnEss)

            With cboLoanScheme
                .DataTextField = "name"
                .DataValueField = "loanschemeunkid"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            cboStatus.Items.Clear()
            cboStatus.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Select"))
            cboStatus.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Success"))
            cboStatus.Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Fail"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Dim objLoanApplication As New clsProcess_pending_loan
        Try
            Dim strSearch As String = ""
            Dim dsList As DataSet = Nothing


            'Pinkal (20-Jan-2023) -- Start
            'Solved NMB Issue for Randamely Access of URL in Loan module.
            If CBool(Session("AllowToAccessLoanDisbursementDashboard")) = False Then Exit Sub
            'Pinkal (20-Jan-2023) -- End

            If dtpFromDate.IsNull = False Then
                strSearch &= "AND lnloan_process_pending_loan.application_date >= '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date) & "' "
            End If

            If dtpToDate.IsNull = False Then
                strSearch &= "AND lnloan_process_pending_loan.application_date <= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                strSearch &= "AND lnloan_process_pending_loan.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedIndex) > 0 Then
                strSearch &= "AND lnloan_process_pending_loan.isfxcube_error = " & CInt(IIf(cboStatus.SelectedIndex = 1, 0, 1)) & " "
            End If

            strSearch &= "AND lnloan_process_pending_loan.loan_statusunkid = " & enLoanApplicationStatus.APPROVED & " "

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsList = objLoanApplication.GetList(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) _
                                                                                            , CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", 0, strSearch, False, True)

            dgLoanApplication.AutoGenerateColumns = False
            dgLoanApplication.DataSource = dsList.Tables(0)
            dgLoanApplication.DataBind()

            If CInt(cboStatus.SelectedIndex) <= 1 Then
                btnPost.Enabled = False
            Else
                btnPost.Enabled = True
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanApplication = Nothing
        End Try
    End Sub

    'Pinkal (14-Dec-2022) -- End

    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .

    Private Sub Post(ByVal gRow As IEnumerable(Of DataGridItem))
        Dim objconfig As New clsConfigOptions
        Dim objRoleLoanApproval As New clsroleloanapproval_process_Tran
        Try

            Dim mstrOracleHostName As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "OracleHostName", Nothing)
            Dim mstrOraclePortNo As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "OraclePortNo", Nothing)
            Dim mstrOracleServiceName As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "OracleServiceName", Nothing)
            Dim mstrOracleUserName As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "OracleUserName", Nothing)
            Dim mstrOracleUserPassword As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "OracleUserPassword", Nothing)
            Dim mstrNewLoanRequestFlexcubeURL As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "NewLoanRequestFlexcubeURL", Nothing)
            Dim mstrTopupRequestFlexcubeURL As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "TopupRequestFlexcubeURL", Nothing)

            'Pinkal (15-Sep-2023) -- Start
            '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.
            Dim mstrCarLoanRequestFlexcubeURL As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "CarLoanRequestFlexcubeURL", Nothing)
            Dim mstrMortgageLoanRequestFlexcubeURL As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "MortgageRequestFlexcubeURL", Nothing)
            Dim mstrPrepaidLoanRequestFlexcubeURL As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "PrepaidRequestFlexcubeURL", Nothing)
            'Pinkal (15-Sep-2023) -- End
            

            If objconfig.IsKeyExist(CInt(Session("CompanyUnkId")), "Advance_CostCenterunkid", Nothing) = True Then
                mstrOracleUserPassword = clsSecurity.Decrypt(mstrOracleUserPassword, "ezee")
            End If
            objconfig = Nothing

            objRoleLoanApproval._ClientIP = CStr(Session("IP_ADD"))
            objRoleLoanApproval._HostName = CStr(Session("HOST_NAME"))
            objRoleLoanApproval._FormName = mstrModuleName
            objRoleLoanApproval._IsFromWeb = True

            Dim objPeriod As New clscommom_period_Tran
            Dim mblnFlag As Boolean = False

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            Dim objLoanApplication As New clsProcess_pending_loan
            'Pinkal (16-Nov-2023) -- End

            For i As Integer = 0 To gRow.Count - 1

                Dim mdtStartDate As Date = Nothing
                Dim mdtEndDate As Date = Nothing
                Dim objloanAdvance As New clsLoan_Advance
                Dim objEmpBankTran As New clsEmployeeBanks
                Dim decOutStandingPrincipalAmt As Decimal = 0
                Dim decOutStandingInterestAmt As Decimal = 0
                Dim intNoOfInstallmentPaid As Integer = 0
                Dim mstrBankAcNo As String = ""


                'Pinkal (16-Nov-2023) -- Start
                '(A1X-1489) NMB - Mortgage tranche disbursement API.
                objLoanApplication._Processpendingloanunkid = CInt(dgLoanApplication.Items(gRow(i).ItemIndex).Cells(getColumnId_Datagrid(dgLoanApplication, "objdgcolhprocesspendingloanunkid", False, True)).Text)

                Dim mstrSearch As String = "lna.processpendingloanunkid = " & objLoanApplication._Processpendingloanunkid & " AND lna.statusunkid = " & enLoanApplicationStatus.APPROVED & _
                                                          "AND ln.loan_statusunkid = " & enLoanApplicationStatus.APPROVED & " AND lna.mapuserunkid = " & objLoanApplication._Approverunkid


                Dim dsApproval As DataSet = objRoleLoanApproval.GetRoleBasedApprovalList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                                                         , objLoanApplication._Application_Date.Date, objLoanApplication._Application_Date.Date, Session("UserAccessModeSetting").ToString() _
                                                                                                                                         , True, CBool(Session("IsIncludeInactiveEmp")), "List", mstrSearch, "", False, Nothing)


                If dsApproval IsNot Nothing AndAlso dsApproval.Tables(0).Rows.Count > 0 Then
                    objRoleLoanApproval._Pendingloanaprovalunkid = CInt(dsApproval.Tables(0).Rows(0)("pendingloanaprovalunkid"))
                End If

                'Pinkal (16-Nov-2023) -- End



                objPeriod._Periodunkid(Session("Database_Name").ToString()) = CInt(dgLoanApplication.Items(gRow(i).ItemIndex).Cells(getColumnId_Datagrid(dgLoanApplication, "objdgcolhdeductionperiodunkid", False, True)).Text)
                mdtStartDate = objPeriod._Start_Date.Date
                mdtEndDate = objPeriod._End_Date.Date

                'Pinkal (02-Jun-2023) -- Start
                'NMB Enhancement : TOTP Related Changes .
                'Dim dsEmpBank As DataSet = objEmpBankTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                '                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Session("UserAccessModeSetting").ToString _
                '                                                                            , True, CBool(Session("IsIncludeInactiveEmp")), "EmployeeBank", False, "", dgLoanApplication.Items(gRow(i).ItemIndex).Cells(getColumnId_Datagrid(dgLoanApplication, "objdgcolhEmployeeID", False, True)).Text _
                '                                                                            , mdtEndDate, "BankGrp, BranchName, EmpName, end_date DESC", "")

                Dim dsEmpBank As DataSet = objEmpBankTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                         , mdtStartDate.Date, mdtEndDate.Date, Session("UserAccessModeSetting").ToString _
                                                                                            , True, CBool(Session("IsIncludeInactiveEmp")), "EmployeeBank", False, "", dgLoanApplication.Items(gRow(i).ItemIndex).Cells(getColumnId_Datagrid(dgLoanApplication, "objdgcolhEmployeeID", False, True)).Text _
                                                                                            , mdtEndDate, "BankGrp, BranchName, EmpName, end_date DESC", "")
                'Pinkal (02-Jun-2023) -- End


                If dsEmpBank IsNot Nothing AndAlso dsEmpBank.Tables(0).Rows.Count > 0 AndAlso dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then

                    mstrBankAcNo = dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString

                    Dim dsFlexLoanData As DataSet = objloanAdvance.GetFlexcubeLoanData(Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString, Session("OracleUserPassword").ToString _
                                                                                                                          , mstrBankAcNo, "", dgLoanApplication.Items(gRow(i).ItemIndex).Cells(getColumnId_Datagrid(dgLoanApplication, "objdgcolhLoanSchemeCode", False, True)).Text, False)

                    If dsFlexLoanData IsNot Nothing AndAlso dsFlexLoanData.Tables(0).Rows.Count > 0 Then
                        decOutStandingPrincipalAmt = (From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("PRINCIPALOUTSTANDING")))).Sum()
                        decOutStandingInterestAmt = (From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("CURRENT_OUTSTANDING_INTEREST")))).Sum()
                        intNoOfInstallmentPaid = CInt((From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("PAIDNOOFINSTALMENT")))).Sum())
                    End If

                End If

                'Pinkal (15-Sep-2023) -- Start
                '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.
                Select Case dgLoanApplication.Items(gRow(i).ItemIndex).Cells(getColumnId_Datagrid(dgLoanApplication, "objdgcolhLoanSchemeCode", False, True)).Text.Trim().ToUpper
                    Case "CL17"    'Car Loan
                        mstrNewLoanRequestFlexcubeURL = mstrCarLoanRequestFlexcubeURL

                    Case "CL25", "CL27"  'Annual Loan , Prepaid Loan
                        mstrNewLoanRequestFlexcubeURL = mstrPrepaidLoanRequestFlexcubeURL

                    Case "CL28"   'Mortgage Loan
                        mstrNewLoanRequestFlexcubeURL = mstrMortgageLoanRequestFlexcubeURL
                End Select
                'Pinkal (15-Sep-2023) -- End


                If objRoleLoanApproval.SendFlexcubeRequest(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrNewLoanRequestFlexcubeURL, mstrTopupRequestFlexcubeURL _
                                                                                , Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtStartDate.Date, mdtEndDate.Date, CStr(Session("UserAccessModeSetting")) _
                                                                                , True, CBool(Session("IsIncludeInactiveEmp")), decOutStandingPrincipalAmt, decOutStandingInterestAmt, mstrBankAcNo _
                                                                                , dgLoanApplication.Items(gRow(i).ItemIndex).Cells(getColumnId_Datagrid(dgLoanApplication, "objdgcolhLoanSchemeCode", False, True)).Text _
                                                                                , CInt(dgLoanApplication.Items(gRow(i).ItemIndex).Cells(getColumnId_Datagrid(dgLoanApplication, "objdgcolhprocesspendingloanunkid", False, True)).Text), "") = False Then

                    mblnFlag = True
                    Continue For

                End If

            Next
            objPeriod = Nothing

            If mblnFlag = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Some of the selected Loan Application(s) did not posted successfully."), Me)
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Selected Loan Application(s) posted successfully."), Me)
                FillList()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRoleLoanApproval = Nothing
            objconfig = Nothing
        End Try
    End Sub

    Private Function SendOTP(ByVal mstrTOTPCode As String) As String
        Dim objLoan As New clsProcess_pending_loan
        Dim objSendMail As New clsSendMail
        Dim mstrError As String = ""
        Try
            objSendMail._ToEmail = Session("SMSGatewayEmail").ToString()
            'objSendMail._ToEmail = mstrEmployeeEmail
            objSendMail._Subject = mstrEmpMobileNo
            objSendMail._Form_Name = mstrModuleName
            objSendMail._LogEmployeeUnkid = -1
            objSendMail._UserUnkid = CInt(Session("UserId"))
            objSendMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
            objSendMail._SenderAddress = CStr(IIf(mstrEmployeeEmail.Trim.Length <= 0, mstrEmployeeName, mstrEmployeeEmail.Trim))
            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
            objSendMail._Message = objLoan.SetSMSForLoanApplication(mstrEmployeeName, mstrTOTPCode, Counter, True)
            mstrError = objSendMail.SendMail(CInt(Session("CompanyUnkId")))
        Catch ex As Exception
            mstrError = ex.Message.ToString()
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objSendMail = Nothing
            objLoan = Nothing
        End Try
        Return mstrError
    End Function

    Private Sub InsertAttempts(ByRef mblnprompt As Boolean)
        Dim objAttempts As New clsloantotpattempts_tran
        Dim objConfig As New clsConfigOptions
        Try
            Dim xAttempts As Integer = 0
            Dim xAttemptsunkid As Integer = 0
            Dim xOTPRetrivaltime As DateTime = Nothing
            Dim dsAttempts As DataSet = Nothing

            dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, CInt(Session("UserId")), 0)

            If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                xAttempts = CInt(dsAttempts.Tables(0).Rows(0)("attempts"))
                xAttemptsunkid = CInt(dsAttempts.Tables(0).Rows(0)("attemptsunkid"))

                If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                    xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))

                    If xOTPRetrivaltime > objConfig._CurrentDateAndTime Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "You cannot save this loan application.Reason :  As You reached Maximum unsuccessful attempts.Please try after") & " " & CInt(Session("TOTPRetryAfterMins")) & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "mintues") & ".", Me)
                        objAttempts = Nothing
                        mblnprompt = True
                        btnVerify.Enabled = False
                        LblSeconds.Text = "00:00"
                        LblSeconds.Visible = False
                        blnShowTOTPPopup = False
                        popup_TOTP.Hide()
                        Exit Sub
                    End If
                End If

                If xAttempts >= CInt(Session("MaxTOTPUnSuccessfulAttempts")) Then
                    xAttempts = 1
                    xAttemptsunkid = 0
                Else
                    xAttempts = xAttempts + 1
                End If
            Else
                xAttempts = 1
            End If

            objAttempts._Attemptsunkid = xAttemptsunkid
            objAttempts._Attempts = xAttempts
            objAttempts._Attemptdate = objConfig._CurrentDateAndTime
            objAttempts._Otpretrivaltime = Nothing
            objAttempts._loginemployeeunkid = -1
            objAttempts._Userunkid = CInt(Session("UserId"))
            objAttempts._Ip = Session("IP_ADD").ToString
            objAttempts._Machine_Name = Session("HOST_NAME").ToString
            objAttempts._FormName = mstrModuleName
            objAttempts._Isweb = True

            If xAttemptsunkid <= 0 Then
                If objAttempts.Insert() = False Then
                    DisplayMessage.DisplayMessage(objAttempts._Message, Me)
                    Exit Sub
                End If
            Else
                If xAttempts >= CInt(Session("MaxTOTPUnSuccessfulAttempts")) Then
                    objAttempts._Otpretrivaltime = objConfig._CurrentDateAndTime.AddMinutes(CInt(Session("TOTPRetryAfterMins")))
                    blnShowTOTPPopup = False
                    popup_TOTP.Hide()
                End If
                If objAttempts.Update() = False Then
                    DisplayMessage.DisplayMessage(objAttempts._Message, Me)
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objConfig = Nothing
            objAttempts = Nothing
        End Try
    End Sub

    'Pinkal (02-Jun-2023) -- End

#End Region

#Region "Button's Events"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If cboStatus.SelectedIndex <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Flexcube Status is compulsory information. Please select Flexcube Status to continue."), Me)
                cboStatus.Focus()
                Exit Sub
            End If
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            dgLoanApplication.DataSource = New List(Of String)
            dgLoanApplication.DataBind()

            cboEmployee.SelectedValue = "0"
            cboLoanScheme.SelectedValue = "0"
            cboStatus.SelectedIndex = 0
            mstrAdvanceFilter = ""
            dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date

            If CInt(cboStatus.SelectedIndex) <= 1 Then
                btnPost.Enabled = False
            Else
                btnPost.Enabled = True
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnPost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPost.Click
        Try

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgLoanApplication.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelectApp"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please select atleast one Loan Application to do further operation on it."), Me)
                Exit Sub
            End If

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            If CBool(Session("ApplyTOTPForLoanApplicationApproval")) Then


                '/* START CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP
                Dim objConfig As New clsConfigOptions
                Dim objAttempts As New clsloantotpattempts_tran
                Dim dsAttempts As DataSet = Nothing

                dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, CInt(Session("UserId")), 0)

                Dim xOTPRetrivaltime As DateTime = Nothing
                If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                    If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                        xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))
                        If xOTPRetrivaltime > objConfig._CurrentDateAndTime Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "You cannot save this loan application.Reason : As You reached Maximum unsuccessful attempts.Please try after") & " " & CInt(Session("TOTPRetryAfterMins")) & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "minutes") & ".", Me)
                            objConfig = Nothing
                            objAttempts = Nothing
                            Exit Sub
                        End If
                    End If
                End If
                objConfig = Nothing
                objAttempts = Nothing
                '/* END CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP

                If Session("SMSGatewayEmail").ToString().Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan Posting."), Me)
                    Exit Sub
                End If


                Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(Session("UserId"))
                txtVerifyOTP.Text = ""
                txtVerifyOTP.ReadOnly = False
                btnVerify.Enabled = True
                btnTOTPClose.Enabled = False

                If objUser._TOTPSecurityKey.Trim.Length > 0 Then
                    mstrSecretKey = objUser._TOTPSecurityKey
                Else
                    mstrSecretKey = objTOTPHelper.GenerateSecretKey()
                    objUser._TOTPSecurityKey = mstrSecretKey
                    If objUser.Update(False) = False Then
                        DisplayMessage.DisplayMessage(objUser._Message, Me)
                        Exit Sub
                    End If
                End If

                If objUser._EmployeeUnkid > 0 Then

                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objUser._EmployeeUnkid

                    mstrEmployeeName = objEmployee._Firstname + " " + objEmployee._Surname
                    mstrEmployeeEmail = objEmployee._Email

                    If CInt(Session("SMSGatewayEmailType")) = CInt(clsEmployee_Master.EmpColEnum.Col_Present_Mobile) Then
                        mstrEmpMobileNo = objEmployee._Present_Mobile.ToString()
                    ElseIf CInt(Session("SMSGatewayEmailType")) = CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Mobile) Then
                        mstrEmpMobileNo = objEmployee._Domicile_Mobile.ToString()
                    End If
                    objEmployee = Nothing
                Else
                    mstrEmployeeName = objUser._Firstname + " " + objUser._Lastname
                    mstrEmployeeEmail = objUser._Email
                    mstrEmpMobileNo = objUser._Phone
                End If
                objUser = Nothing

                If mstrEmpMobileNo.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Mobile No is compulsory information. Please set Approver's Mobile No to send TOTP."), Me)
                    Exit Sub
                End If

                Counter = objTOTPHelper._TimeStepSeconds

                Dim mstrTOTPCode As String = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
                Dim mstrError As String = SendOTP(mstrTOTPCode)
                If mstrError.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    blnShowTOTPPopup = False
                    popup_TOTP.Hide()
                    Exit Sub
                End If

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:countdown(0," & Counter & "); ", True)
                blnShowTOTPPopup = True
                popup_TOTP.Show()
            Else
                Post(gRow)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If cboStatus.SelectedIndex <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Flexcube Status is compulsory information. Please select Flexcube Status to continue."), Me)
                cboStatus.Focus()
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgLoanApplication.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelectApp"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please select atleast one Loan Application to do further operation on it."), Me)
                Exit Sub
            End If

            Dim dt As New DataTable()

            For i As Integer = 1 To dgLoanApplication.Columns.Count - 1
                If dgLoanApplication.Columns(i).Visible Then
                    dt.Columns.Add(dgLoanApplication.Columns(i).HeaderText)
                End If
            Next

            For Each row As DataGridItem In gRow
                Dim dr As DataRow = dt.NewRow
                dr(dgLoanApplication.Columns(getColumnId_Datagrid(dgLoanApplication, "dgcolhApplicationNo", False, True)).HeaderText) = row.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhApplicationNo", False, True)).Text
                dr(dgLoanApplication.Columns(getColumnId_Datagrid(dgLoanApplication, "dgcolhApplicationDate", False, True)).HeaderText) = row.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhApplicationDate", False, True)).Text
                dr(dgLoanApplication.Columns(getColumnId_Datagrid(dgLoanApplication, "dgcolhEmployee", False, True)).HeaderText) = row.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhEmployee", False, True)).Text
                dr(dgLoanApplication.Columns(getColumnId_Datagrid(dgLoanApplication, "dgcolhLoanScheme", False, True)).HeaderText) = row.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhLoanScheme", False, True)).Text
                dr(dgLoanApplication.Columns(getColumnId_Datagrid(dgLoanApplication, "dgcolhRequestedAmt", False, True)).HeaderText) = row.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhRequestedAmt", False, True)).Text
                dr(dgLoanApplication.Columns(getColumnId_Datagrid(dgLoanApplication, "dgcolhApprovedAmt", False, True)).HeaderText) = row.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhApprovedAmt", False, True)).Text
                dr(dgLoanApplication.Columns(getColumnId_Datagrid(dgLoanApplication, "dgcolhMessage", False, True)).HeaderText) = row.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhMessage", False, True)).Text
                dr(dgLoanApplication.Columns(getColumnId_Datagrid(dgLoanApplication, "dgcolhStatus", False, True)).HeaderText) = row.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhStatus", False, True)).Text
                dt.Rows.Add(dr)
            Next
    
            Dim dg As New DataGrid
            dg.HeaderStyle.Font.Bold = True
            dg.AllowPaging = False
            dg.DataSource = dt
            dg.DataBind()

            Dim file As System.IO.FileInfo = New System.IO.FileInfo(IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & "Loan_Disbursement_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss") & ".xls")
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=" & file.Name)
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"

            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            dg.RenderControl(hw)

            Response.Output.Write(sw.ToString())
            Response.Flush()
            Response.[End]()

            If dt IsNot Nothing Then dt.Clear()
            dt = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .

    Protected Sub btnVerify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerify.Click
        Try
            If txtVerifyOTP.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmOTPValidator", 1, "OTP is compulsory information.Please Enter OTP."), Me)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                txtVerifyOTP.Focus()
                popup_TOTP.Show()
                Exit Sub
            End If
            Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))
            If objTOTPHelper.ValidateTotpCode(mstrSecretKey, txtVerifyOTP.Text.Trim) = False Then
                Dim mblnprompt As Boolean = False
                InsertAttempts(mblnprompt)
                If mblnprompt = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmOTPValidator", 2, "Invalid OTP Code. Please try again."), Me)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                    objTOTPHelper = Nothing
                    Exit Sub
                End If
            Else
                blnShowTOTPPopup = False
                popup_TOTP.Hide()
                Dim gRow As IEnumerable(Of DataGridItem) = Nothing
                gRow = dgLoanApplication.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelectApp"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please select atleast one Loan Application to do further operation on it."), Me)
                    Exit Sub
                End If

                Post(gRow)
                gRow = Nothing
            End If
            objTOTPHelper = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnSendCodeAgain_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendCodeAgain.Click
        Try
            btnSendCodeAgain.Enabled = False
            btnTOTPClose.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True

            Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))
            Dim mstrTOTPCode As String = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
            Counter = objTOTPHelper._TimeStepSeconds
            Dim mstrError As String = SendOTP(mstrTOTPCode)
            If mstrError.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(mstrError, Me)
                blnShowTOTPPopup = False
                popup_TOTP.Hide()
                Exit Sub
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:countdown(0," & Counter & "); ", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_TOTP.Show()
        End Try
    End Sub

    Protected Sub btnTOTPClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTOTPClose.Click
        Try
            btnSendCodeAgain.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True
            blnShowTOTPPopup = False
            popup_TOTP.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (02-Jun-2023) -- End

#End Region

#Region "DataGrid Events"

    Protected Sub dgLoanApplication_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgLoanApplication.ItemDataBound
        Try
            SetDateFormat()
            If e.Item.ItemIndex >= 0 Then

                If e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhEmployee", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhEmployee", False, True)).Text <> "&nbsp;" Then
                    e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhEmployee", False, True)).Text = e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "objdgcolhEmpCode", False, True)).Text + " - " + e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhEmployee", False, True)).Text
                End If

                If e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhLoanScheme", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhLoanScheme", False, True)).Text <> "&nbsp;" Then
                    e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhLoanScheme", False, True)).Text = e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "objdgcolhLoanSchemeCode", False, True)).Text + " - " + e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhLoanScheme", False, True)).Text
                End If

                e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhApplicationDate", False, True)).Text = CDate(e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhApplicationDate", False, True)).Text).ToShortDateString()

                If e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhRequestedAmt", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhRequestedAmt", False, True)).Text <> "&nbsp;" Then
                    e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhRequestedAmt", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhRequestedAmt", False, True)).Text), Session("fmtCurrency").ToString)
                End If

                If e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhApprovedAmt", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhApprovedAmt", False, True)).Text <> "&nbsp;" Then
                    e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhApprovedAmt", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgLoanApplication, "dgcolhApprovedAmt", False, True)).Text), Session("fmtCurrency").ToString)
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        ' Verifies that the control is rendered
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, "gbFilterCriteria", Me.lblDetialHeader.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblFromDate.ID, Me.lblFromDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblToDate.ID, Me.lblToDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnPost.ID, Me.btnPost.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApplication.Columns(1).FooterText, Me.dgLoanApplication.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApplication.Columns(2).FooterText, Me.dgLoanApplication.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApplication.Columns(3).FooterText, Me.dgLoanApplication.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApplication.Columns(4).FooterText, Me.dgLoanApplication.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApplication.Columns(5).FooterText, Me.dgLoanApplication.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApplication.Columns(6).FooterText, Me.dgLoanApplication.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApplication.Columns(7).FooterText, Me.dgLoanApplication.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApplication.Columns(8).FooterText, Me.dgLoanApplication.Columns(8).HeaderText)

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblVerifyOTP.ID, Me.lblVerifyOTP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblOTP.ID, Me.LblOTP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSendCodeAgain.ID, Me.btnSendCodeAgain.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnVerify.ID, Me.btnVerify.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnTOTPClose.ID, Me.btnTOTPClose.Text)
            'Pinkal (02-Jun-2023) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbFilterCriteria", Me.lblDetialHeader.Text)
            'Me.lnkAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToDate.ID, Me.lblToDate.Text)
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnPost.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnPost.ID, Me.btnPost.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgLoanApplication.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApplication.Columns(1).FooterText, Me.dgLoanApplication.Columns(1).HeaderText)
            Me.dgLoanApplication.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApplication.Columns(2).FooterText, Me.dgLoanApplication.Columns(2).HeaderText)
            Me.dgLoanApplication.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApplication.Columns(3).FooterText, Me.dgLoanApplication.Columns(3).HeaderText)
            Me.dgLoanApplication.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApplication.Columns(4).FooterText, Me.dgLoanApplication.Columns(4).HeaderText)
            Me.dgLoanApplication.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApplication.Columns(5).FooterText, Me.dgLoanApplication.Columns(5).HeaderText)
            Me.dgLoanApplication.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApplication.Columns(6).FooterText, Me.dgLoanApplication.Columns(6).HeaderText)
            Me.dgLoanApplication.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApplication.Columns(7).FooterText, Me.dgLoanApplication.Columns(7).HeaderText)
            Me.dgLoanApplication.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApplication.Columns(8).FooterText, Me.dgLoanApplication.Columns(8).HeaderText)



            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Me.lblVerifyOTP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVerifyOTP.ID, Me.lblVerifyOTP.Text)
            Me.LblOTP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblOTP.ID, Me.LblOTP.Text)
            Me.btnSendCodeAgain.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSendCodeAgain.ID, Me.btnSendCodeAgain.Text)
            Me.btnVerify.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnVerify.ID, Me.btnVerify.Text)
            Me.btnTOTPClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnTOTPClose.ID, Me.btnTOTPClose.Text)
            'Pinkal (02-Jun-2023) -- End

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub


#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Select")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Success")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Fail")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Flexcube Status is compulsory information. Please select Flexcube Status to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Please select atleast one Loan Application to do further operation on it.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Some of the selected Loan Application(s) did not posted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Selected Loan Application(s) posted successfully.")

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan Posting.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Mobile No is compulsory information. Please set Approver's Mobile No to send TOTP.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "You cannot save this loan application.Reason :  As You reached Maximum unsuccessful attempts.Please try after")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "mintues")
            'Pinkal (02-Jun-2023) -- End

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings



  
End Class
