﻿<%@ Page Language="VB" Title="Loan Disbursement Dashboard" AutoEventWireup="false"
    EnableEventValidation="false" MasterPageFile="~/Home1.master" CodeFile="wPg_LoanDisbursementDashboard.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Disbursement_Dashboard_wPg_LoanDisbursementDashboard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        $("body").on("click", "[id*=chkAllSelectApp]", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("[id*=chkSelectApp]").prop("checked", $(chkHeader).prop("checked"));

        });

        $("body").on("click", "[id*=chkSelectApp]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkAllSelectApp]", grid);

            if ($("[id*=chkSelectApp]", grid).length == $("[id*=chkSelectApp]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }

        });

        function countdown(minutes, seconds) {
            var element, endTime, mins, msLeft, time;

            function twoDigits(n) {
                return (n <= 9 ? "0" + n : n);
            }

            function updateTimer() {
                msLeft = endTime - (+new Date);

                if (msLeft < 1000) {
                    element.innerHTML = '00:00';
                    document.getElementById('<%=txtVerifyOTP.ClientID %>').readOnly = true;
                    document.getElementById('<%=btnVerify.ClientID %>').disabled = "disabled";
                    document.getElementById('<%=btnSendCodeAgain.ClientID %>').disabled = "";
                    document.getElementById('<%=btnTOTPClose.ClientID %>').disabled = "";
                } else {
                    document.getElementById('<%=btnSendCodeAgain.ClientID %>').disabled = "disabled";
                    document.getElementById('<%=btnTOTPClose.ClientID %>').disabled = "disabled";
                    time = new Date(msLeft);
                    mins = time.getUTCMinutes();
                    element.innerHTML = (twoDigits(mins)) + ':' + twoDigits(time.getUTCSeconds());
                    setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
                }
                if (msLeft <= 0) {
                    return;
                }
                document.getElementById('<%=hdf_TOTP.ClientID %>').value = twoDigits(time.getUTCSeconds());
            }
            element = document.getElementById('<%=LblSeconds.ClientID %>');
            endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
            updateTimer();
        }

        function VerifyOnClientClick() {
            var val = document.getElementById('<%=hdf_TOTP.ClientID %>').value;
            if (val !== undefined) {
                countdown(0, val, false);
            }
        }

        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }

    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <%--<uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />--%>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Loan Disbursement Dashboard" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                    <%--<ul class="header-dropdown m-r--5">
                                        <li class="dropdown">
                                            <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations">
                                                    <i class="fas fa-sliders-h"></i>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>--%>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblFromDate" runat="server" Text="From Date" CssClass="form-label">
                                        </asp:Label>
                                        <uc1:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblStatus" runat="server" Text="Flexcube Status" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="false" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="form-label">
                                        </asp:Label>
                                        <uc1:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLoanScheme" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgLoanApplication" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                CssClass="table table-hover table-bordered" Width="150%">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="objdgcolhSelect">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAllSelectApp" runat="server" CssClass="filled-in" Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelectApp" runat="server" CssClass="filled-in" Text=" " />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Application_No" HeaderText="Application No" FooterText="dgcolhApplicationNo" />
                                                    <asp:BoundColumn DataField="applicationdate" HeaderText="Application Date" FooterText="dgcolhApplicationDate" />
                                                    <asp:BoundColumn DataField="EmpName" HeaderText="Employee" FooterText="dgcolhEmployee" />
                                                    <asp:BoundColumn DataField="LoanScheme" HeaderText="Loan Scheme" FooterText="dgcolhLoanScheme" />
                                                    <asp:BoundColumn DataField="Amount" HeaderText="Requested Amount" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" FooterText="dgcolhRequestedAmt" />
                                                    <asp:BoundColumn DataField="Approved_Amount" HeaderText="Approved Amount" FooterText="dgcolhApprovedAmt"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="fxcube_responsedata" HeaderText="Message" FooterText="dgcolhMessage" />                                                        
                                                    <asp:BoundColumn DataField="FlexcubeStatus" HeaderText="Status" FooterText="dgcolhStatus" />
                                                    <asp:BoundColumn DataField="LoanSchemeCode" HeaderText="objdgcolhLoanSchemeCode"
                                                        Visible="false" FooterText="objdgcolhLoanSchemeCode" />
                                                        <asp:BoundColumn DataField="EmpCode" HeaderText="objdgcolhEmpCode" Visible="false"
                                                        FooterText="objdgcolhEmpCode" />
                                                     <asp:BoundColumn DataField="deductionperiodunkid" HeaderText="objdgcolhdeductionperiodunkid"
                                                        Visible="false" FooterText="objdgcolhdeductionperiodunkid" />
                                                     <asp:BoundColumn DataField="employeeunkid" HeaderText="objdgcolhEmployeeID" Visible="false"
                                                        FooterText="objdgcolhEmployeeID" />
                                                    <asp:BoundColumn DataField="processpendingloanunkid" HeaderText="objdgcolhprocesspendingloanunkid"
                                                        Visible="false" FooterText="objdgcolhprocesspendingloanunkid" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnPost" runat="server" Text="Post" CssClass="btn btn-primary" />
                                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popup_TOTP" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="hdf_TOTP" PopupControlID="pnl_TOTP" TargetControlID="hdf_TOTP">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_TOTP" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblVerifyOTP" runat="server" Text="Verify OTP"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="LblOTP" runat="server" Text="Enter OTP" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtVerifyOTP" runat="server" CssClass="form-control" MaxLength="6"
                                            onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="LblSeconds" runat="server" Text="00:00" CssClass="form-label" Font-Bold="true"
                                    ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSendCodeAgain" runat="server" Text="Resend OTP" CssClass="btn btn-primary" />
                        <asp:Button ID="btnVerify" runat="server" Text="Verify" CssClass="btn btn-primary" />
                        <asp:Button ID="btnTOTPClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdf_TOTP" runat="server" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID = "btnExport"  />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
