﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_AddEditLoanTranche.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Tranche_Application_wPg_AddEditLoanTranche"
    Title="Add/Edit Loan Tranche Application" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">


        function IsValidAttach() {
            debugger;
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');

            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            return true;
        }

        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }

        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }



        function countdown(minutes, seconds) {
            var element, endTime, mins, msLeft, time;

            function twoDigits(n) {
                return (n <= 9 ? "0" + n : n);
            }

            function updateTimer() {
                msLeft = endTime - (+new Date);

                if (msLeft < 1000) {
                    element.innerHTML = '00:00';
                    document.getElementById('<%=txtVerifyOTP.ClientID %>').readOnly = true;
                    document.getElementById('<%=btnVerify.ClientID %>').disabled = "disabled";
                    document.getElementById('<%=btnSendCodeAgain.ClientID %>').disabled = "";
                    document.getElementById('<%=btnTOTPClose.ClientID %>').disabled = "";
                } else {
                    document.getElementById('<%=btnSendCodeAgain.ClientID %>').disabled = "disabled";
                    document.getElementById('<%=btnTOTPClose.ClientID %>').disabled = "disabled";
                    time = new Date(msLeft);
                    mins = time.getUTCMinutes();
                    element.innerHTML = (twoDigits(mins)) + ':' + twoDigits(time.getUTCSeconds());
                    timerid = setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
                }
                if (msLeft <= 0) {
                    return;
                }
                document.getElementById('<%=hdf_TOTP.ClientID %>').value = twoDigits(time.getUTCSeconds());
            }
            element = document.getElementById('<%=LblSeconds.ClientID %>');
            endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
            updateTimer();
        }

        function VerifyOnClientClick() {
            var val = document.getElementById('<%=hdf_TOTP.ClientID %>').value;
            if (val !== undefined) {
                countdown(0, val, false);
            }
        }
  
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc2:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                <div class="row clearfix  d--f jc--c ai--c ">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Loan Tranche Application"
                                        CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="LblTrancheApplicationNo" runat="server" Text="Tranche Application No"
                                                    CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtTrancheLoanApplicationNo" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="LblTrancheDate" runat="server" Text="Tranche Date" CssClass="form-label"></asp:Label>
                                                <uc1:DateCtrl ID="dtpTrancheDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="LblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="LblLoanApplication" runat="server" Text="Loan Application No" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboLoanApplicationNo" runat="server" AutoPostBack="true" data-live-search="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 m-t-30">
                                                <asp:LinkButton ID="lnkLoanDetails" runat="server" Text="Loan Details" Font-Underline="false"></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="LblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboLoanscheme" runat="server" Enabled="false" data-live-search="true"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label ID="LblTrancheAmount" runat="server" Text="Tranche Amount" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="TxtTrancheAmount" runat="server" CssClass="form-control" Text="0.00"
                                                            Style="text-align: right"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                                <asp:Label ID="LblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true" data-live-search="true"
                                                        Enabled="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="LblRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="TxtRemark" runat="server" CssClass="form-control" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <asp:Panel ID="pnlAttachment" runat="server">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card inner-card">
                                                        <div class="header">
                                                            <h2>
                                                                <asp:Label ID="lblAttachmentInfo" runat="server" Text="Attachment Info" CssClass="form-label"></asp:Label>
                                                            </h2>
                                                        </div>
                                                        <div class="body">
                                                            <asp:Panel ID="pnlScanAttachment" runat="server">
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                        <asp:Label ID="lblDocumentType" runat="server" Text="Document Type(.PDF and Images only)"
                                                                            CssClass="form-label"></asp:Label>
                                                                        <div class="form-group">
                                                                            <asp:DropDownList ID="cboDocumentType" runat="server">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-25">
                                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-l-20  ">
                                                                            <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                                                <div id="fileuploader">
                                                                                    <input type="button" id="btnAddFile" runat="server" onclick="return IsValidAttach()"
                                                                                        value="Browse" class="btn btn-primary" />
                                                                                </div>
                                                                            </asp:Panel>
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                                                                            <asp:Button ID="btnAddAttachment" runat="server" Style="display: none" Text="Browse" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row clearfix">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive" style="max-height: 350px;">
                                                                        <asp:DataGrid ID="dgAttachment" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                            Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                                            HeaderStyle-Font-Bold="false">
                                                                            <Columns>
                                                                                <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-HorizontalAlign="Center"
                                                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50px">
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                        </span>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                 <asp:BoundColumn HeaderText="Document Type" DataField="attachment_type" FooterText="colhDocType" />
                                                                                <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                                <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                                                <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                                    Visible="false" />
                                                                                <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                                    Visible="false" />
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="objvalPeriodDuration" runat="server" Text="#periodDuration" Style="float: left;"
                                                    CssClass="form-label"></asp:Label>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="objlblExRate" runat="server" Style="float: left;" Text="" CssClass="form-label"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupLoanDetails" runat="server" BackgroundCssClass="ModalPopupBG2"
                        CancelControlID="hdfLoanDetails" PopupControlID="pnlLoanDetails" TargetControlID="hdfLoanDetails">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlLoanDetails" runat="server" CssClass="card modal-dialog" Style="display: none;">
                        <div class="header">
                            <h2>
                                <asp:Label ID="LblLoanDetails" runat="server" Text="Loan Details"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblLoanApplicationNo" runat="server" Text="Application No" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="TxtLoanApplicationNo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblApplicationDate" runat="server" Text="Application Date" CssClass="form-label"></asp:Label>
                                    <uc1:DateCtrl ID="dtpApplicationDate" runat="server" AutoPostBack="false" Enabled="false" />
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblLoanEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="TxtLoanEmployee" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblLoanschme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="TxtLoanscheme" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblApprovedLoanAmt" runat="server" Text="Approved Loan Amount" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="TxtApprovedLoanAmt" runat="server" CssClass="form-control" ReadOnly="true"
                                                Style="text-align: right"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblFirstTranche" runat="server" Text="First Tranche" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="TxtFirstTranche" runat="server" CssClass="form-control" ReadOnly="true"
                                                Style="text-align: right"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblFirstTrancheDate" runat="server" Text="First Tranche Date " CssClass="form-label"></asp:Label>
                                    <uc1:DateCtrl ID="dtpFirstTrancheDate" runat="server" AutoPostBack="false" Enabled="false" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblSecodTranche" runat="server" Text="Second Tranche" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="TxtSecondTranche" runat="server" CssClass="form-control" ReadOnly="true"
                                                Style="text-align: right"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblSecondTrancheDate" runat="server" Text="Second Tranche Date " CssClass="form-label"></asp:Label>
                                    <uc1:DateCtrl ID="dtpSecondTrancheDate" runat="server" AutoPostBack="false" Enabled="false" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblThirdTranche" runat="server" Text="Third Tranche" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtThirdTranche" runat="server" CssClass="form-control" ReadOnly="true"
                                                Style="text-align: right"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblThirdTrancheDate" runat="server" Text="Third Tranche Date " CssClass="form-label"></asp:Label>
                                    <uc1:DateCtrl ID="dtpThirdTrancheDate" runat="server" AutoPostBack="false" Enabled="false" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblFourthTranche" runat="server" Text="Fourth Tranche" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="TxtFourthTranche" runat="server" CssClass="form-control" ReadOnly="true"
                                                Style="text-align: right"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblFourthTrancheDate" runat="server" Text="Fourth Tranche Date " CssClass="form-label"></asp:Label>
                                    <uc1:DateCtrl ID="dtpFourthTranchDate" runat="server" AutoPostBack="false" Enabled="false" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblFifthTranche" runat="server" Text="Fifth Tranche" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="TxtFifthTranche" runat="server" CssClass="form-control" ReadOnly="true"
                                                Style="text-align: right"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblFifthTrancheDate" runat="server" Text="Fifth Tranche Date " CssClass="form-label"></asp:Label>
                                    <uc1:DateCtrl ID="dtpFifthTrancheDate" runat="server" AutoPostBack="false" Enabled="false" />
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnLoanDetailClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                            <asp:HiddenField ID="hdfLoanDetails" runat="server" />
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popup_TOTP" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="hdf_TOTP" PopupControlID="pnl_TOTP" TargetControlID="hdf_TOTP">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_TOTP" runat="server" CssClass="card modal-dialog" Style="display: none;">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblVerifyOTP" runat="server" Text="Verify OTP"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblOTP" runat="server" Text="Enter OTP" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtVerifyOTP" runat="server" CssClass="form-control" MaxLength="6"
                                                onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                    <asp:Label ID="LblSeconds" runat="server" Text="00:00" CssClass="form-label" Font-Bold="true"
                                        ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnSendCodeAgain" runat="server" Text="Resend OTP" CssClass="btn btn-primary" />
                            <asp:Button ID="btnVerify" runat="server" Text="Verify" CssClass="btn btn-primary" />
                            <asp:Button ID="btnTOTPClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                            <asp:HiddenField ID="hdf_TOTP" runat="server" />
                        </div>
                    </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgAttachment" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">

        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });

        function ImageLoad() {
            debugger;
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPg_AddEditLoanTranche.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
                    dragDropStr: "",
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnAddAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $("body").on("click", 'input[name*=myfile]', function() {
            return IsValidAttach();
        });
    
    </script>

</asp:Content>
