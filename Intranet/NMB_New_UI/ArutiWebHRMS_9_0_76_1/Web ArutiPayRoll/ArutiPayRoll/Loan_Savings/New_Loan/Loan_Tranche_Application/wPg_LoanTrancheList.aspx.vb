﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Globalization

#End Region

Partial Class Loan_Savings_New_Loan_Loan_Tranche_Application_wPg_LoanTrancheList
    Inherits Basepage

#Region "Private Variables"
    Private Shared ReadOnly mstrModuleName As String = "frmLoanTrancheList"
    Dim DisplayMessage As New CommonCodes
    Private mintLoanTrancheRequestId As Integer = 0

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                SetControlCaptions()
                SetMessages()
                Call SetLanguage()

                Call FillCombo()
                Call SetVisibility()

                If dgvLoanTrancheApplicationList.Items.Count <= 0 Then
                    dgvLoanTrancheApplicationList.DataSource = New List(Of String)
                    dgvLoanTrancheApplicationList.DataBind()
                End If

                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    dgvLoanTrancheApplicationList.Columns(2).Visible = False
                End If
            Else
                mintLoanTrancheRequestId = CInt(ViewState("LoanTrancheRequestId"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("LoanTrancheRequestId") = mintLoanTrancheRequestId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objLoanApplication As New clsProcess_pending_loan
        Dim objLoanTrancheApplication As New clsloanTranche_request_Tran
        Try

            Dim objMaster As New clsMasterData
            Dim dsList As DataSet = Nothing

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     CStr(Session("UserAccessModeSetting")), _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), _
                                                     "EmpList")

                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("employeeunkid") = 0
                dRow.Item("EmpCodeName") = "Select"
                dsList.Tables(0).Rows.InsertAt(dRow, 0)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedValue = "0"
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If

            Dim mblnSchemeShowOnEss As Boolean = True
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "loanschemecategory_id = " & enLoanSchemeCategories.MORTGAGE & " AND  ISNULL(ispostingtoflexcube,0) = 1 ", mblnSchemeShowOnEss)
            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim dtTable As DataTable = objLoanTrancheApplication.GetMortgageLoanApplicationNo(CInt(cboEmployee.SelectedValue), True)
            With cboLoanApplicationNo
                .DataValueField = "processpendingloanunkid"
                .DataTextField = "application_no"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objLoanApplication.GetLoan_Status("Status", True, False)
            With cboStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsList.Tables("Status")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objLoanScheme = Nothing
            objLoanApplication = Nothing
            objLoanTrancheApplication = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objLoanTranche As New clsloanTranche_request_Tran
        Try
            Dim dsLoanTrancheList As New DataSet
            Dim objExchangeRate As New clsExchangeRate
            Dim StrSearching As String = String.Empty


            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND lnloantranche_request_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanApplicationNo.SelectedValue) > 0 Then
                StrSearching &= "AND lnloantranche_request_tran.processpendingloanunkid = " & CInt(cboLoanApplicationNo.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                StrSearching &= "AND lnloantranche_request_tran.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearching &= "AND lnloantranche_request_tran.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            If dtpAppDate.IsNull = False Then
                StrSearching &= "AND lnloantranche_request_tran.tranchedate = '" & eZeeDate.convertDate(dtpAppDate.GetDate.Date) & "' "
            End If

            If txtApplicationNo.Text.Trim <> "" Then
                StrSearching &= "AND lnloantranche_request_tran.applicationno like '" & txtApplicationNo.Text.Trim & "' "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            dsLoanTrancheList = objLoanTranche.GetList("List", True, True, StrSearching, Nothing)

            dgvLoanTrancheApplicationList.AutoGenerateColumns = False
            dgvLoanTrancheApplicationList.DataSource = dsLoanTrancheList.Tables(0)
            dgvLoanTrancheApplicationList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanTranche = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

#End Region

#Region "Button's Events"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Tranche_Application/wPg_AddEditLoanTranche.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedIndex = 0
            cboLoanApplicationNo.SelectedIndex = 0
            dtpAppDate.SetDate = Nothing
            txtApplicationNo.Text = ""
            cboLoanScheme.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            dgvLoanTrancheApplicationList.DataSource = New List(Of String)
            dgvLoanTrancheApplicationList.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popDeleteReason_buttonDelReasonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popDeleteReason.buttonDelReasonNo_Click
        Try
            Session("LoanTrancheRequestId") = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popDeleteReason.buttonDelReasonYes_Click
        Dim objLoanTranche As New clsloanTranche_request_Tran
        Try
            objLoanTranche._Loantrancherequestunkid = mintLoanTrancheRequestId
            objLoanTranche._Voidreason = popDeleteReason.Reason
            objLoanTranche._Isvoid = True
            objLoanTranche._Voiddatetime = DateAndTime.Now.Date
            objLoanTranche._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))

            objLoanTranche._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            objLoanTranche._ClientIP = CStr(Session("IP_ADD"))
            objLoanTranche._HostName = CStr(Session("HOST_NAME"))
            objLoanTranche._FormName = mstrModuleName
            objLoanTranche._IsFromWeb = True

            Dim objScanAttach As New clsScan_Attach_Documents
            objLoanTranche._Document = objScanAttach.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.LOAN_TRANCHE, mintLoanTrancheRequestId, Session("Document_Path").ToString())
            objScanAttach = Nothing

            If objLoanTranche.Delete(CInt(Session("CompanyUnkId")), mintLoanTrancheRequestId) = False Then
                DisplayMessage.DisplayMessage(objLoanTranche._Message, Me)
                Exit Sub
            End If
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanTranche = Nothing
        End Try
    End Sub

#End Region

#Region "DataGrid Events"

    Protected Sub dgvLoanTrancheApplicationList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvLoanTrancheApplicationList.ItemCommand
        Try

            If CInt(e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "objdgcolhstatusunkid", False, True)).Text) <> enLoanApplicationStatus.PENDING Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "You cannot Edit this loan tranche application. Reason: This loan tranche application is in") & " " & _
                                                             e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhStatus", False, True)).Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "status") & ".", Me)
                Exit Sub
            End If

            If e.CommandName.ToUpper = "EDIT" Then

                Dim objLoanTrancheApproval As New clsloanTranche_approval_Tran
                If CBool(objLoanTrancheApproval.IsPendingLoanTrancheApplication(CInt(e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "objdgcolhloantrancherequestunkid", False, True)).Text))) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "You cannot Edit this loan tranche application. Reason: It is already in approval process."), Me)
                    objLoanTrancheApproval = Nothing
                    Exit Sub
                End If
                objLoanTrancheApproval = Nothing

                Session("LoanTrancheRequestId") = e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "objdgcolhloantrancherequestunkid", False, True)).Text

                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Tranche_Application/wPg_AddEditLoanTranche.aspx", False)

            ElseIf e.CommandName.ToUpper = "DELETE" Then

                Dim objLoanTrancheApproval As New clsloanTranche_approval_Tran
                If CBool(objLoanTrancheApproval.IsPendingLoanTrancheApplication(CInt(e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "objdgcolhloantrancherequestunkid", False, True)).Text))) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You cannot Delete this loan tranche application. Reason: It is already in approval process."), Me)
                    objLoanTrancheApproval = Nothing
                    Exit Sub
                End If
                objLoanTrancheApproval = Nothing

                mintLoanTrancheRequestId = CInt(e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "objdgcolhloantrancherequestunkid", False, True)).Text)

                popDeleteReason.Reason = ""
                popDeleteReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Are you sure you want to delete this Loan Tranche Application?")
                popDeleteReason.Show()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvLoanTrancheApplicationList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvLoanTrancheApplicationList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then
                Call SetDateFormat()
                e.Item.Font.Bold = True
            End If

            If e.Item.ItemIndex >= 0 Then

                If CBool(e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "objdgcolhIsGrp", False, True)).Text) = True Then

                    CType(e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhEdit", False, True)).FindControl("lnkEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhDelete", False, True)).FindControl("lnkDelete"), LinkButton).Visible = False

                    e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhEdit", False, True)).Visible = False
                    e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhDelete", False, True)).Visible = False

                    e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).ColumnSpan = e.Item.Cells.Count - 7
                    e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).Text = e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApplicationNo", False, True)).Text
                    e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).Font.Bold = True

                    For i = 4 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next

                    e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).CssClass = "group-header"
                    e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).Style.Add("text-align", "left")

                Else
                    If e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).Text.ToString().Trim <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).Text = CDate(e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).Text).Date.ToShortDateString
                    End If

                    e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanTrancheAmount", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanTrancheAmount", False, True)).Text), Session("fmtCurrency").ToString)
                    e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApprovedTrancheAmount", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApprovedTrancheAmount", False, True)).Text), Session("fmtCurrency").ToString)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblPageHeader.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblAppNo.ID, Me.lblAppNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblAppDate.ID, Me.lblAppDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApplicationNo", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApplicationNo", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanApplicationNo", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanApplicationNo", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanScheme", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanScheme", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanTrancheAmount", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanTrancheAmount", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApprovedTrancheAmount", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApprovedTrancheAmount", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhStatus", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhStatus", False, True)).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)

            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.LblLoanApplicationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)
            Me.lblAppNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAppNo.ID, Me.lblAppNo.Text)
            Me.lblAppDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAppDate.ID, Me.lblAppDate.Text)
            Me.lblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)

            Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApplicationNo", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApplicationNo", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApplicationNo", False, True)).HeaderText)
            Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhAppDate", False, True)).HeaderText)
            Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanApplicationNo", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanApplicationNo", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanApplicationNo", False, True)).HeaderText)
            Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanScheme", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanScheme", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanScheme", False, True)).HeaderText)
            Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanTrancheAmount", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanTrancheAmount", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhLoanTrancheAmount", False, True)).HeaderText)
            Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApprovedTrancheAmount", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApprovedTrancheAmount", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhApprovedTrancheAmount", False, True)).HeaderText)
            Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhStatus", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhStatus", False, True)).FooterText, Me.dgvLoanTrancheApplicationList.Columns(getColumnId_Datagrid(dgvLoanTrancheApplicationList, "dgcolhStatus", False, True)).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, Offer Letter is not available for Selected Loan Scheme")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Are you sure you want to delete this Loan Tranche Application?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
