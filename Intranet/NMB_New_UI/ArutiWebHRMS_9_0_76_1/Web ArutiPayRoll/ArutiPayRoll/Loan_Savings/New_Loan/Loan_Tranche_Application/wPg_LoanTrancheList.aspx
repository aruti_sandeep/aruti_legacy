﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_LoanTrancheList.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Tranche_Application_wPg_LoanTrancheList"
    MaintainScrollPositionOnPostback="true" Title="Loan Tranche Application List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/OperationButton.ascx" TagName="OperationButton" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc2:DeleteReason ID="popDeleteReason" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Loan Tranche Application List"
                            CssClass="form-label"> </asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblAppDate" runat="server" Text="App. Date" CssClass="form-label">
                                        </asp:Label>
                                        <uc1:DateCtrl ID="dtpAppDate" runat="server" CssClass="form-control" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLoanScheme" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="LblLoanApplicationNo" runat="server" Text="Loan Application No" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLoanApplicationNo" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblAppNo" runat="server" Text="Application No." CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApplicationNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgvLoanTrancheApplicationList" runat="server" AutoGenerateColumns="False"
                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="dgcolhEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkEdit" runat="server" ToolTip="Edit" CommandName="Edit">
                                                                <i class="fas fa-pencil-alt"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="dgcolhDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                     <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="applicationno" HeaderText="Application No" ReadOnly="true" FooterText="dgcolhApplicationNo" />
                                                    <asp:BoundColumn DataField="tranchedate" HeaderText="Application Date" FooterText="dgcolhAppDate" />
                                                     <asp:BoundColumn DataField="LoanApplicationNo" HeaderText="Loan Application No" ReadOnly="true" FooterText="dgcolhLoanApplicationNo" />
                                                    <asp:BoundColumn DataField="LoanScheme" HeaderText="Loan Scheme" ReadOnly="true" FooterText="dgcolhLoanScheme" />
                                                    <asp:BoundColumn DataField="trancheamt" HeaderText="Loan Tranche Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ReadOnly="true" FooterText="dgcolhLoanTrancheAmount" />
                                                    <asp:BoundColumn DataField="approved_trancheamt" HeaderText="Approved Tranche Amount" HeaderStyle-HorizontalAlign="Right"  ItemStyle-HorizontalAlign="Right" ReadOnly="true" FooterText="dgcolhApprovedTrancheAmount" />
                                                    <asp:BoundColumn DataField="Status" HeaderText="Status" ReadOnly="true" FooterText="dgcolhStatus" />
                                                    <asp:BoundColumn DataField="IsGrp" HeaderText="EmpID" ReadOnly="true" Visible="false" FooterText="objdgcolhIsGrp" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmpID" ReadOnly="true" Visible="false" FooterText="objdgcolhEmpId" />
                                                    <asp:BoundColumn DataField="loanschemeunkid" HeaderText="SchemeID" ReadOnly="true" Visible="false" FooterText="objdgcolhSchemeID" />
                                                    <asp:BoundColumn DataField="finalapproverunkid" HeaderText="Approver" ReadOnly="true" Visible="false" FooterText="objdgcolhfinalApprover" />
                                                     <asp:BoundColumn DataField="statusunkid" HeaderText="Statusunkid" ReadOnly="true" Visible="false" FooterText="objdgcolhstatusunkid" />
                                                    <asp:BoundColumn DataField="processpendingloanunkid" HeaderText="" ReadOnly="true" Visible="false" FooterText="objdgcolhprocesspendingloanunkid" />
                                                    <asp:BoundColumn DataField="loantrancherequestunkid" HeaderText="" ReadOnly="true"  Visible="false" FooterText="objdgcolhloantrancherequestunkid" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
