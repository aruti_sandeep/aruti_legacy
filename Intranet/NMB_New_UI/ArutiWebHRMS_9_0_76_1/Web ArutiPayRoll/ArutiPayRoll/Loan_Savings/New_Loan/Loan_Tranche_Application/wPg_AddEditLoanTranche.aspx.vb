﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports System.Globalization
Imports System.IO

Partial Class Loan_Savings_New_Loan_Loan_Tranche_Application_wPg_AddEditLoanTranche
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmAddEditLoanTranche"
    Private mintLoanTrancheRequestId As Integer = 0
    Private mintDeleteRowIndex As Integer = -1
    Private mdtLoanTrancheApplicationDocument As DataTable = Nothing

    'Pinkal (17-May-2024) -- Start
    'NMB Enhancement For Mortgage Loan.
    'Private mblnValidTrancheDate As Boolean = False
    Private mblnValidTrancheDate As Boolean = True
    'Pinkal (17-May-2024) -- End

    Private mdecOriginalTrancheAmount As Decimal = 0
    Private mintDeductionPeriodUnkId As Integer = 0
    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mintTrancheNo As Integer = 1

    'Pinkal (16-Nov-2023) -- Start
    '(A1X-1489) NMB - Mortgage tranche disbursement API.
    Dim mstrSecretKey As String = ""
    Private blnShowTOTPPopup As Boolean = False
    Dim Counter As Integer = 0
    Dim mstrEmpMobileNo As String = ""
    Dim mstrEmployeeName As String = ""
    Dim mstrEmployeeEmail As String = ""
    'Pinkal (16-Nov-2023) -- End

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                SetMessages()
                Call SetLanguage()

                FillCombo()
                FillPeriod(False)
                SetVisibility()

                If Session("LoanTrancheRequestId") IsNot Nothing Then
                    mintLoanTrancheRequestId = CInt(Session("LoanTrancheRequestId"))
                End If
                GetValue()
            Else
                mintLoanTrancheRequestId = CInt(ViewState("LoanTrancheRequestId"))
                mintTrancheNo = CInt(ViewState("TrancheNo"))
                mdtLoanTrancheApplicationDocument = CType(Session("LoanTrancheApplicationDocument"), DataTable)
                mintDeleteRowIndex = CInt(ViewState("DeleteRowIndex"))
                mblnValidTrancheDate = CBool(ViewState("ValidTranchDate"))
                mdecOriginalTrancheAmount = CDec(ViewState("OriginalTrancheAmount"))
                mintDeductionPeriodUnkId = CInt(Me.ViewState("DeductionPeriodUnkId"))
                mdtPeriodStart = CDate(ViewState("mdtPeriodStart"))
                mdtPeriodEnd = CDate(ViewState("mdtPeriodEnd"))
                mstrBaseCurrSign = ViewState("mstrBaseCurrSign").ToString()
                mintBaseCurrId = CInt(ViewState("mintBaseCurrId"))
                mintPaidCurrId = CInt(ViewState("mintPaidCurrId"))
                mdecBaseExRate = CDec(ViewState("mdecBaseExRate"))
                mdecPaidExRate = CDec(ViewState("mdecPaidExRate"))


                'Pinkal (16-Nov-2023) -- Start
                '(A1X-1489) NMB - Mortgage tranche disbursement API.
                mstrSecretKey = CStr(Me.ViewState("mstrSecretKey"))
                Counter = CInt(Me.ViewState("Counter"))
                blnShowTOTPPopup = CBool(Me.ViewState("blnShowTOTPPopup"))
                mstrEmployeeName = CStr(Me.ViewState("EmployeeName"))
                mstrEmployeeEmail = CStr(Me.ViewState("EmployeeEmail"))
                mstrEmpMobileNo = CStr(Me.ViewState("EmployeeMobileNo"))
                'Pinkal (16-Nov-2023) -- End


            End If

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If


            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            If blnShowTOTPPopup Then
                popup_TOTP.Show()
            End If
            'Pinkal (16-Nov-2023) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("LoanTrancheRequestId") = mintLoanTrancheRequestId
            Me.ViewState("TrancheNo") = mintTrancheNo
            Me.ViewState("DeleteRowIndex") = mintDeleteRowIndex
            Me.ViewState("ValidTranchDate") = mblnValidTrancheDate
            Me.ViewState("OriginalTrancheAmount") = mdecOriginalTrancheAmount
            Me.ViewState("DeductionPeriodUnkId") = mintDeductionPeriodUnkId
            Me.ViewState("mdtPeriodStart") = mdtPeriodStart
            Me.ViewState("mdtPeriodEnd") = mdtPeriodEnd
            Me.ViewState("mstrBaseCurrSign") = mstrBaseCurrSign
            Me.ViewState("mintBaseCurrId") = mintBaseCurrId
            Me.ViewState("mintPaidCurrId") = mintPaidCurrId
            Me.ViewState("mdecBaseExRate") = mdecBaseExRate
            Me.ViewState("mdecPaidExRate") = mdecPaidExRate

            Session("LoanTrancheApplicationDocument") = mdtLoanTrancheApplicationDocument


            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            Me.ViewState("Counter") = Counter
            Me.ViewState("mstrSecretKey") = mstrSecretKey
            Me.ViewState("blnShowTOTPPopup") = blnShowTOTPPopup
            Me.ViewState("EmployeeName") = mstrEmployeeName
            Me.ViewState("EmployeeEmail") = mstrEmployeeEmail
            Me.ViewState("EmployeeMobileNo") = mstrEmpMobileNo
            'Pinkal (16-Nov-2023) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetVisibility()
        Try
            pnlAttachment.Visible = CBool(Session("MandatoryAttachmentsForLoanTrancheApplication"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objLoanApplication As New clsloanTranche_request_Tran
        Try

            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If

            dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                        CInt(Session("UserId")), _
                                        CInt(Session("Fin_year")), _
                                        CInt(Session("CompanyUnkId")), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                        CStr(Session("UserAccessModeSetting")), True, _
                                        False, "Employee", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedIndex = 0
            End With


            Dim mblnSchemeShowOnEss As Boolean = True
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "loanschemecategory_id = " & enLoanSchemeCategories.MORTGAGE & " AND  ISNULL(ispostingtoflexcube,0) = 1 ", mblnSchemeShowOnEss)

            With cboLoanscheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim dtTable As DataTable = objLoanApplication.GetMortgageLoanApplicationNo(CInt(cboEmployee.SelectedValue), True)
            With cboLoanApplicationNo
                .DataValueField = "processpendingloanunkid"
                .DataTextField = "application_no"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim objCurrency As New clsExchangeRate
            dsList = objCurrency.getComboList("Currency", True)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsList.Tables("Currency")
                .DataBind()
                Dim drRow() As DataRow = dsList.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    Call cboCurrency_SelectedIndexChanged(cboCurrency, New System.EventArgs())
                End If
                drRow = Nothing
            End With

            FillDocumentTypeCombo()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanApplication = Nothing
            objLoanScheme = Nothing
        End Try
    End Sub

    Private Sub FillDocumentTypeCombo(Optional ByVal mstrDocumentIds As String = "")
        Dim objCommonMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Dim dt As New DataTable
        Dim strFilter As String = String.Empty
        Dim dtTable As DataTable = Nothing
        Try

            dsCombos = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")

            If mstrDocumentIds.Trim.Length > 0 Then
                dtTable = New DataView(dsCombos.Tables(0), "masterunkid in (0," & mstrDocumentIds & ")", "", DataViewRowState.CurrentRows).ToTable()
            Else
                dtTable = dsCombos.Tables(0)
            End If

            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dtTable.Copy
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsCombos IsNot Nothing Then dsCombos.Clear()
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            dsCombos = Nothing
            objCommonMaster = Nothing
        End Try
    End Sub

    Private Sub FillPeriod(ByVal blnOnlyFirstPeriod As Boolean)
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombos As DataSet
        Try


            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                 CInt(Session("Fin_year")), _
                                                 Session("Database_Name").ToString, _
                                                 CDate(Session("fin_startdate")), _
                                                 "List", False, enStatusType.OPEN)

            If dsCombos.Tables(0).Rows.Count > 0 Then
                mintDeductionPeriodUnkId = CInt(dsCombos.Tables(0).Rows(0).Item("periodunkid"))
            End If

            cboDeductionPeriod_SelectedIndexChanged()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub GetValue()
        Dim objLoanTranche As New clsloanTranche_request_Tran
        Try
            If mintLoanTrancheRequestId > 0 Then
                objLoanTranche._Loantrancherequestunkid = mintLoanTrancheRequestId
                txtTrancheLoanApplicationNo.Text = objLoanTranche._Applicationno
            End If

            If objLoanTranche._Tranchedate <> Nothing Then
                dtpTrancheDate.SetDate = objLoanTranche._Tranchedate.Date
            Else
                dtpTrancheDate.SetDate = Basepage.GetCurrentDateTime.Date
            End If
            dtpTrancheDate.Enabled = False

            If objLoanTranche._Processpendingloanunkid > 0 Then
                cboLoanApplicationNo.SelectedValue = objLoanTranche._Processpendingloanunkid.ToString()
                cboLoanApplicationNo.Enabled = False
                cboLoanApplicationNo_SelectedIndexChanged(cboLoanApplicationNo, New EventArgs())
            End If

            If objLoanTranche._Loanschemeunkid > 0 Then
                cboLoanscheme.SelectedValue = objLoanTranche._Loanschemeunkid.ToString()
                cboLoanscheme_SelectedIndexChanged(cboLoanscheme, New EventArgs())
            End If

            If objLoanTranche._Trancheamt > 0 Then
                TxtTrancheAmount.Text = Format(objLoanTranche._Trancheamt, Session("fmtcurrency").ToString())
            End If

            If objLoanTranche._Remark.Trim.Length > 0 Then
                TxtRemark.Text = objLoanTranche._Remark.Trim()
            End If

            If Session("LoanTrancheApplicationDocument") Is Nothing Then
                Dim objAttchement As New clsScan_Attach_Documents
                'Call objAttchement.GetList(Session("Document_Path").ToString(), "List ", "", CInt(Session("Employeeunkid")), , , , "hrdocuments_tran.transactionunkid = " & mintLoanTrancheRequestId, CBool(IIf(mintLoanTrancheRequestId > 0, False, True)), , enScanAttactRefId.LOAN_TRANCHE, 0, True, "")
                '                mdtLoanTrancheApplicationDocument = objAttchement._Datatable.Copy()
                mdtLoanTrancheApplicationDocument = objAttchement.GetQulificationAttachment(CInt(Session("Employeeunkid")), enScanAttactRefId.LOAN_TRANCHE, mintLoanTrancheRequestId, Session("Document_Path").ToString())
                objAttchement = Nothing

                If mintLoanTrancheRequestId <= 0 Then
                    mdtLoanTrancheApplicationDocument.Clear()
                End If

                Session("LoanTrancheApplicationDocument") = mdtLoanTrancheApplicationDocument
            Else
                mdtLoanTrancheApplicationDocument = CType(Session("LoanTrancheApplicationDocument"), DataTable)
            End If
            Call FillLoanTrancheRequestAttachment()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanTranche = Nothing
        End Try
    End Sub

    Private Sub FillLoanTrancheRequestAttachment()
        Dim dtView As DataView
        Try
            If mdtLoanTrancheApplicationDocument Is Nothing Then
                dgAttachment.DataSource = Nothing
                dgAttachment.DataBind()
                Exit Sub
            End If

            dtView = New DataView(mdtLoanTrancheApplicationDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgAttachment.AutoGenerateColumns = False
            dgAttachment.DataSource = dtView
            dgAttachment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try

            If mdtLoanTrancheApplicationDocument Is Nothing Then
                mdtLoanTrancheApplicationDocument = CType(Session("LoanTrancheApplicationDocument"), DataTable)
            End If

            Dim objDocument As New clsScan_Attach_Documents
            Dim dtRow() As DataRow = mdtLoanTrancheApplicationDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtLoanTrancheApplicationDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(Session("Employeeunkid"))
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Loan_Balance
                dRow("scanattachrefid") = enScanAttactRefId.LOAN_TRANCHE
                dRow("transactionunkid") = -1
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = DateTime.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024

                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))
                dRow("Documentype") = GetMimeType(strfullpath)

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                dRow("attachment_type") = cboDocumentType.SelectedItem.Text
                'Pinkal (17-May-2024) -- End

                Dim bytes() As Byte
                bytes = Encoding.ASCII.GetBytes(strfullpath)
                Dim xDocumentData As Byte() = bytes
                Try
                    dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Catch ex As FormatException
                    CommonCodes.LogErrorOnly(ex)
                End Try

                Dim objFile As New FileInfo(strfullpath)
                dRow("fileextension") = objFile.Extension.ToString().Replace(".", "")
                objFile = Nothing
                dRow("file_data") = xDocumentData
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtLoanTrancheApplicationDocument.Rows.Add(dRow)

            Session("LoanTrancheApplicationDocument") = mdtLoanTrancheApplicationDocument

            Call FillLoanTrancheRequestAttachment()

            objDocument = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue(ByVal objLoanTranche As clsloanTranche_request_Tran)
        Try
            If mintLoanTrancheRequestId > 0 Then
                objLoanTranche._Loantrancherequestunkid = mintLoanTrancheRequestId
            End If

            objLoanTranche._Applicationno = txtTrancheLoanApplicationNo.Text.Trim
            objLoanTranche._Processpendingloanunkid = CInt(cboLoanApplicationNo.SelectedValue)
            objLoanTranche._Employeeunkid = CInt(Session("Employeeunkid"))
            objLoanTranche._Loanschemeunkid = CInt(cboLoanscheme.SelectedValue)
            objLoanTranche._Tranchedate = dtpTrancheDate.GetDate.Date
            objLoanTranche._Trancheamt = CDec(TxtTrancheAmount.Text)
            objLoanTranche._Statusunkid = enLoanApplicationStatus.PENDING
            objLoanTranche._Countryunkid = CInt(cboCurrency.SelectedValue)
            objLoanTranche._DeductionPeriodunkid = mintDeductionPeriodUnkId
            objLoanTranche._FinalDeductionPeriodunkid = 0
            objLoanTranche._Finalapproverunkid = 0
            objLoanTranche._Approved_Trancheamt = 0
            objLoanTranche._SendEmpReminderForNextTrance = Nothing

            Dim objLoanApplication As New clsProcess_pending_loan
            Dim dtTranche As DataTable = objLoanApplication.GetFinalLoanTranchesFromLoanApplication(CInt(cboLoanApplicationNo.SelectedValue))
            objLoanApplication = Nothing

            If dtTranche IsNot Nothing AndAlso dtTranche.Rows.Count > 0 Then
                If mintTrancheNo = 2 Then
                    If IsDBNull(dtTranche.Rows(0)("third_tranchedate")) = False AndAlso dtTranche.Rows(0)("third_tranchedate") IsNot Nothing Then
                        objLoanTranche._SendEmpReminderForNextTrance = DateAdd(DateInterval.Day, CInt(Session("EmpNotificationTrancheDateBeforeDays")) * -1, CDate(dtTranche.Rows(0)("third_tranchedate")).Date)
                    Else
                        objLoanTranche._SendEmpReminderForNextTrance = Nothing
                    End If
                    objLoanTranche._NtfSendEmpForNextTranche = False
                ElseIf mintTrancheNo = 3 Then
                    If IsDBNull(dtTranche.Rows(0)("fourth_tranchedate")) = False AndAlso dtTranche.Rows(0)("fourth_tranchedate") IsNot Nothing Then
                        objLoanTranche._SendEmpReminderForNextTrance = DateAdd(DateInterval.Day, CInt(Session("EmpNotificationTrancheDateBeforeDays")) * -1, CDate(dtTranche.Rows(0)("fourth_tranchedate")).Date)
                    Else
                        objLoanTranche._SendEmpReminderForNextTrance = Nothing
                    End If
                    objLoanTranche._NtfSendEmpForNextTranche = False
                ElseIf mintTrancheNo = 4 Then
                    If IsDBNull(dtTranche.Rows(0)("fifth_tranchedate")) = False AndAlso dtTranche.Rows(0)("fifth_tranchedate") IsNot Nothing Then
                        objLoanTranche._SendEmpReminderForNextTrance = DateAdd(DateInterval.Day, CInt(Session("EmpNotificationTrancheDateBeforeDays")) * -1, CDate(dtTranche.Rows(0)("fifth_tranchedate")).Date)
                    Else
                        objLoanTranche._SendEmpReminderForNextTrance = Nothing
                    End If
                    objLoanTranche._NtfSendEmpForNextTranche = False
                ElseIf mintTrancheNo > 4 Then
                    objLoanTranche._SendEmpReminderForNextTrance = Nothing
                    objLoanTranche._NtfSendEmpForNextTranche = True
                End If
            End If  'If dtTranche IsNot Nothing AndAlso dtTranche.Rows.Count > 0 Then
            dtTranche.Clear()
            dtTranche.Dispose()
            dtTranche = Nothing


            objLoanTranche._Remark = TxtRemark.Text.Trim
            objLoanTranche._Document = mdtLoanTrancheApplicationDocument.Copy()
            objLoanTranche._Isvoid = False
            objLoanTranche._Voiduserunkid = -1
            objLoanTranche._Voiddatetime = Nothing
            objLoanTranche._Voidreason = ""
            objLoanTranche._Userunkid = -1

            objLoanTranche._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            objLoanTranche._ClientIP = CStr(Session("IP_ADD"))
            objLoanTranche._HostName = CStr(Session("HOST_NAME"))
            objLoanTranche._FormName = mstrModuleName
            objLoanTranche._IsFromWeb = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Dim mblnFlag As Boolean = True
        Try
            If CInt(cboLoanApplicationNo.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Loan Application is compulsory information.Please Select Loan Application No."), Me)
                cboLoanApplicationNo.Focus()
                mblnFlag = False

            ElseIf TxtTrancheAmount.Text.Trim.Length <= 0 OrElse CDec(TxtTrancheAmount.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Loan Tranche Amount should be greater than 0.Please enter valid Loan Tranche Amount."), Me)
                TxtTrancheAmount.Focus()
                mblnFlag = False

            ElseIf mintTrancheNo > 5 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "You can not apply for this Loan Tranche application.Reason : All Traches are applied for this loan application."), Me)
                mblnFlag = False

            ElseIf mblnValidTrancheDate = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "You can not apply for this Loan Tranche application.Reason : You have to wait for the eligible date for the next tranche."), Me)
                mblnFlag = False

            ElseIf TxtTrancheAmount.Text.Trim.Length > 0 AndAlso mdecOriginalTrancheAmount < CDec(TxtTrancheAmount.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Loan Trache Amount should be less than or equal to") & " " & Format(mdecOriginalTrancheAmount, Session("fmtcurrency").ToString()) & ".", Me)
                TxtTrancheAmount.Focus()
                mblnFlag = False
            End If

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            If CBool(Session("ApplyTOTPForLoanApplicationApproval")) Then
                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)

                mstrEmployeeName = objEmployee._Firstname & " " & objEmployee._Surname
                mstrEmployeeEmail = objEmployee._Email

                If CInt(Session("SMSGatewayEmailType")) = CInt(clsEmployee_Master.EmpColEnum.Col_Present_Mobile) Then
                    mstrEmpMobileNo = objEmployee._Present_Mobile.ToString()
                ElseIf CInt(Session("SMSGatewayEmailType")) = CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Mobile) Then
                    mstrEmpMobileNo = objEmployee._Domicile_Mobile.ToString()
                End If

                If mstrEmpMobileNo.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Mobile No is compulsory information. Please set Employee's Mobile No to send TOTP."), Me)
                    mblnFlag = False
                End If
                objEmployee = Nothing
            End If
            'Pinkal (16-Nov-2023) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            mblnFlag = False
        End Try
        Return mblnFlag
    End Function

    'Pinkal (16-Nov-2023) -- Start
    '(A1X-1489) NMB - Mortgage tranche disbursement API.

    Private Sub Save()
        Dim blnFlag As Boolean = False
        Dim objLoanTranche As New clsloanTranche_request_Tran
        Try
            Dim mdsDoc As DataSet
            Dim mstrFolderName As String = ""
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim strFileName As String = ""
            For Each dRowAttachment As DataRow In mdtLoanTrancheApplicationDocument.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRowAttachment("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault

                If dRowAttachment("AUD").ToString = "A" AndAlso dRowAttachment("localpath").ToString <> "" Then
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRowAttachment("localpath")))
                    If File.Exists(CStr(dRowAttachment("localpath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(dRowAttachment("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRowAttachment("fileuniquename") = strFileName
                        dRowAttachment("filepath") = strPath
                        dRowAttachment.AcceptChanges()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "File does not exist on localpath."), Me)
                        Exit Sub
                    End If
                ElseIf dRowAttachment("AUD").ToString = "D" AndAlso dRowAttachment("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRowAttachment("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "File does not exist on localpath."), Me)
                            Exit Sub
                        End If
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "File does not exist on localpath."), Me)
                        Exit Sub
                    End If
                End If

            Next

            SetValue(objLoanTranche)

            If mintLoanTrancheRequestId > 0 Then
                blnFlag = objLoanTranche.Update(CInt(Session("CompanyUnkId")))
            Else
                blnFlag = objLoanTranche.Insert(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")))
            End If

            If blnFlag = False Then
                DisplayMessage.DisplayMessage(objLoanTranche._Message, Me)
                Exit Sub
            Else

                If mdtLoanTrancheApplicationDocument IsNot Nothing Then mdtLoanTrancheApplicationDocument.Clear()
                mdtLoanTrancheApplicationDocument = Nothing
                Session("LoanTrancheApplicationDocument") = Nothing
                Session.Remove("LoanTrancheApplicationDocument")

                Session("LoanTrancheRequestId") = Nothing
                Session.Remove("LoanTrancheRequestId")

                Dim enLoginMode As New enLogin_Mode
                Dim intLoginByEmployeeId As Integer = 0
                Dim intUserId As Integer = 0

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    enLoginMode = enLogin_Mode.EMP_SELF_SERVICE
                    intLoginByEmployeeId = CInt(Session("Employeeunkid"))
                    intUserId = 0
                Else
                    enLoginMode = enLogin_Mode.MGR_SELF_SERVICE
                    intUserId = CInt(Session("UserId"))
                    intLoginByEmployeeId = 0
                End If

                objLoanTranche.Send_NotificationLoanTranche_Approver(Session("Database_Name").ToString, CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, objLoanTranche._Tranchedate.Date, _
                                                                                        CInt(cboLoanscheme.SelectedValue), CInt(cboEmployee.SelectedValue), objLoanTranche._Loantrancherequestunkid.ToString(), _
                                                                                       Session("ArutiSelfServiceURL").ToString(), CInt(Session("CompanyUnkId")), enLoginMode, intLoginByEmployeeId, intUserId)

                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Loan Tranche application submitted successfully."), Me, Convert.ToString(Session("rootpath")) & "Loan_Savings/New_Loan/Loan_Tranche_Application/wPg_LoanTrancheList.aspx")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SendOTP(ByVal mstrTOTPCode As String) As String
        Dim objLoanTranche As New clsloanTranche_request_Tran
        Dim objSendMail As New clsSendMail
        Dim mstrError As String = ""
        Try
            objSendMail._ToEmail = Session("SMSGatewayEmail").ToString()
            objSendMail._Subject = mstrEmpMobileNo
            objSendMail._Form_Name = mstrModuleName

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objSendMail._LogEmployeeUnkid = CInt(Session("Employeeunkid"))
                objSendMail._UserUnkid = -1
                objSendMail._OperationModeId = enLogin_Mode.EMP_SELF_SERVICE
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objSendMail._LogEmployeeUnkid = -1
                objSendMail._UserUnkid = CInt(Session("UserId"))
                objSendMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
            End If

            objSendMail._SenderAddress = CStr(IIf(mstrEmployeeEmail.Trim.Length <= 0, mstrEmployeeName, mstrEmployeeEmail.Trim))
            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
            objSendMail._Message = objLoanTranche.SetSMSForLoanTrancheApplication(mstrEmployeeName, mstrTOTPCode, Counter, False)
            mstrError = objSendMail.SendMail(CInt(Session("CompanyUnkId")))
        Catch ex As Exception
            mstrError = ex.Message.ToString()
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objSendMail = Nothing
            objLoanTranche = Nothing
        End Try
        Return mstrError
    End Function

    Private Sub InsertAttempts(ByRef mblnprompt As Boolean)
        Dim objAttempts As New clsloantotpattempts_tran
        Dim objConfig As New clsConfigOptions
        Try
            Dim xAttempts As Integer = 0
            Dim xAttemptsunkid As Integer = 0
            Dim xOTPRetrivaltime As DateTime = Nothing
            Dim dsAttempts As DataSet = Nothing

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, CInt(Session("UserId")), 0)
            ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, 0, CInt(Session("Employeeunkid")))
            End If

            If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                xAttempts = CInt(dsAttempts.Tables(0).Rows(0)("attempts"))
                xAttemptsunkid = CInt(dsAttempts.Tables(0).Rows(0)("attemptsunkid"))

                If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                    xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))

                    If xOTPRetrivaltime > objConfig._CurrentDateAndTime Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "You cannot save this loan tranche application.Reason :  As You reached Maximum unsuccessful attempts.Please try after") & " " & CInt(Session("TOTPRetryAfterMins")) & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "mintues") & ".", Me)
                        objAttempts = Nothing
                        mblnprompt = True
                        btnVerify.Enabled = False
                        LblSeconds.Text = "00:00"
                        LblSeconds.Visible = False
                        blnShowTOTPPopup = False
                        popup_TOTP.Hide()
                        Exit Sub
                    End If
                End If

                If xAttempts >= CInt(Session("MaxTOTPUnSuccessfulAttempts")) Then
                    xAttempts = 1
                    xAttemptsunkid = 0
                Else
                    xAttempts = xAttempts + 1
                End If
            Else
                xAttempts = 1
            End If


            objAttempts._Attemptsunkid = xAttemptsunkid
            objAttempts._Attempts = xAttempts
            objAttempts._Attemptdate = objConfig._CurrentDateAndTime
            objAttempts._Otpretrivaltime = Nothing

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                objAttempts._loginemployeeunkid = -1
                objAttempts._Userunkid = CInt(Session("UserId"))
            ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                objAttempts._loginemployeeunkid = CInt(Session("Employeeunkid"))
                objAttempts._Userunkid = -1
            End If

            objAttempts._Ip = Session("IP_ADD").ToString
            objAttempts._Machine_Name = Session("HOST_NAME").ToString
            objAttempts._FormName = mstrModuleName
            objAttempts._Isweb = True

            If xAttemptsunkid <= 0 Then
                If objAttempts.Insert() = False Then
                    DisplayMessage.DisplayMessage(objAttempts._Message, Me)
                    Exit Sub
                End If
            Else
                If xAttempts >= CInt(Session("MaxTOTPUnSuccessfulAttempts")) Then
                    objAttempts._Otpretrivaltime = objConfig._CurrentDateAndTime.AddMinutes(CInt(Session("TOTPRetryAfterMins")))
                    blnShowTOTPPopup = False
                    popup_TOTP.Hide()
                End If
                If objAttempts.Update() = False Then
                    DisplayMessage.DisplayMessage(objAttempts._Message, Me)
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objConfig = Nothing
            objAttempts = Nothing
        End Try
    End Sub

    'Pinkal (16-Nov-2023) -- End

#End Region

#Region " Buttons Methods "

    Protected Sub btnAddAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAttachment.Click
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                Dim arrValidFileExtension() As String = {".PDF", ".JPG", ".JPEG", ".PNG", ".GIF", ".BMP", ".TIF"}
                If arrValidFileExtension.Contains(f.Extension.ToString.ToUpper()) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please select PDF or Image file."), Me)
                    Exit Sub
                End If
                AddDocumentAttachment(f, f.FullName)
                Call FillLoanTrancheRequestAttachment()
            End If
            Session.Remove("Imagepath")
            cboDocumentType.SelectedValue = CStr(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            mintDeleteRowIndex = -1
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If mintDeleteRowIndex >= 0 Then
                mdtLoanTrancheApplicationDocument.Rows(mintDeleteRowIndex)("AUD") = "D"
                mdtLoanTrancheApplicationDocument.AcceptChanges()
                Call FillLoanTrancheRequestAttachment()
                mintDeleteRowIndex = -1
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Validation() = False Then Exit Sub

            If CBool(Session("ApplyTOTPForLoanApplicationApproval")) Then


                '/* START CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP
                Dim objConfig As New clsConfigOptions
                Dim objAttempts As New clsloantotpattempts_tran
                Dim dsAttempts As DataSet = Nothing

                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, CInt(Session("UserId")), 0)
                ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, 0, CInt(Session("Employeeunkid")))
                End If

                Dim xOTPRetrivaltime As DateTime = Nothing
                If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                    If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                        xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))
                        If xOTPRetrivaltime > objConfig._CurrentDateAndTime Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "You cannot save this loan tranche application.Reason : As You reached Maximum unsuccessful attempts.Please try after") & " " & CInt(Session("TOTPRetryAfterMins")) & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "minutes") & ".", Me)
                            objConfig = Nothing
                            objAttempts = Nothing
                            Exit Sub
                        End If
                    End If
                End If
                objConfig = Nothing
                objAttempts = Nothing
                '/* END CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP

                If Session("SMSGatewayEmail").ToString().Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan tranche application."), Me)
                    Exit Sub
                End If

                Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))

                Dim objUser As New clsUserAddEdit
                Dim xUserId As Integer = objUser.Return_UserId(CInt(cboEmployee.SelectedValue), CInt(Session("CompanyUnkId")))
                objUser._Userunkid = xUserId
                txtVerifyOTP.Text = ""
                txtVerifyOTP.ReadOnly = False
                btnVerify.Enabled = True
                btnSendCodeAgain.Enabled = False
                LblSeconds.Visible = True
                btnTOTPClose.Enabled = False

                If objUser._TOTPSecurityKey.Trim.Length > 0 Then
                    mstrSecretKey = objUser._TOTPSecurityKey
                Else
                    mstrSecretKey = objTOTPHelper.GenerateSecretKey()
                    objUser._TOTPSecurityKey = mstrSecretKey
                    If objUser.Update(False) = False Then
                        DisplayMessage.DisplayMessage(objUser._Message, Me)
                        Exit Sub
                    End If
                End If
                objUser = Nothing

                Counter = objTOTPHelper._TimeStepSeconds

                Dim mstrTOTPCode As String = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
                Dim mstrError As String = SendOTP(mstrTOTPCode)
                If mstrError.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    blnShowTOTPPopup = False
                    popup_TOTP.Hide()
                    Exit Sub
                End If

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:countdown(0," & Counter & "); ", True)
                blnShowTOTPPopup = True
                popup_TOTP.Show()

            Else
                Save()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If mdtLoanTrancheApplicationDocument IsNot Nothing Then mdtLoanTrancheApplicationDocument.Clear()
            mdtLoanTrancheApplicationDocument = Nothing
            Session("LoanTrancheApplicationDocument") = Nothing
            Session.Remove("LoanTrancheApplicationDocument")

            Session("LoanTrancheRequestId") = Nothing
            Session.Remove("LoanTrancheRequestId")
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Tranche_Application/wPg_LoanTrancheList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (16-Nov-2023) -- Start
    '(A1X-1489) NMB - Mortgage tranche disbursement API.

    Protected Sub btnVerify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerify.Click
        Try
            If txtVerifyOTP.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmOTPValidator", 1, "OTP is compulsory information.Please Enter OTP."), Me)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                txtVerifyOTP.Focus()
                popup_TOTP.Show()
                Exit Sub
            End If

            Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))
            If objTOTPHelper.ValidateTotpCode(mstrSecretKey, txtVerifyOTP.Text.Trim) = False Then
                Dim mblnprompt As Boolean = False
                InsertAttempts(mblnprompt)
                If mblnprompt = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmOTPValidator", 2, "Invalid OTP Code. Please try again."), Me)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                    objTOTPHelper = Nothing
                    Exit Sub
                End If
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                blnShowTOTPPopup = False
                popup_TOTP.Hide()
                Save()
            End If
            objTOTPHelper = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnSendCodeAgain_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendCodeAgain.Click
        Try
            btnSendCodeAgain.Enabled = False
            btnTOTPClose.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True

            Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))
            Dim mstrTOTPCode As String = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
            Counter = objTOTPHelper._TimeStepSeconds
            Dim mstrError As String = SendOTP(mstrTOTPCode)
            If mstrError.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(mstrError, Me)
                blnShowTOTPPopup = False
                popup_TOTP.Hide()
                Exit Sub
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:countdown(0," & Counter & "); ", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_TOTP.Show()
        End Try
    End Sub

    Protected Sub btnTOTPClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTOTPClose.Click
        Try
            btnSendCodeAgain.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True
            blnShowTOTPPopup = False
            popup_TOTP.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (16-Nov-2023) -- End

#End Region

#Region "Dropdown Events"

    Protected Sub cboLoanApplicationNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanApplicationNo.SelectedIndexChanged
        Dim objLoanApplication As New clsloanTranche_request_Tran
        Dim mintFinalLoanApplicationApproveId As Integer = 0
        Try
            If CInt(cboLoanApplicationNo.SelectedValue) <= 0 Then
                cboLoanscheme.SelectedValue = "0"
                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                mdecOriginalTrancheAmount = 0
                TxtTrancheAmount.Text = Format(mdecOriginalTrancheAmount, Session("fmtcurrency").ToString())
                'Pinkal (17-May-2024) -- End
                Exit Sub
            End If

            Dim dtTable As DataTable = objLoanApplication.GetMortgageLoanApplicationNo(CInt(cboEmployee.SelectedValue), False, "lnloan_process_pending_loan.processpendingloanunkid = " & CInt(cboLoanApplicationNo.SelectedValue))
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim xLoanSchemeId As Integer = dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("loanschemeunkid")).First()
                cboLoanscheme.SelectedValue = xLoanSchemeId.ToString()
                cboLoanscheme_SelectedIndexChanged(cboLoanscheme, New EventArgs())
            End If
            Dim objLoan As New clsProcess_pending_loan

            objLoan._Processpendingloanunkid = CInt(cboLoanApplicationNo.SelectedValue)
            mintFinalLoanApplicationApproveId = objLoan._Approverunkid

            TxtLoanApplicationNo.Text = objLoan._Application_No
            dtpApplicationDate.SetDate = objLoan._Application_Date.Date
            TxtLoanscheme.Text = cboLoanscheme.SelectedItem.Text
            TxtLoanEmployee.Text = cboEmployee.SelectedItem.Text
            TxtApprovedLoanAmt.Text = Format(objLoan._Approved_Amount, Session("fmtcurrency").ToString())

            Dim mstrFilter As String = "ln.processpendingloanunkid = " & objLoan._Processpendingloanunkid & " AND lna.mapuserunkid = " & objLoan._Approverunkid
            Dim objLoanApproval As New clsroleloanapproval_process_Tran
            Dim dsList As DataSet = objLoanApproval.GetRoleBasedApprovalList(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                             , dtpApplicationDate.GetDate.Date, dtpApplicationDate.GetDate.Date, Session("UserAccessModeSetting").ToString() _
                                                                                                             , True, False, "List", mstrFilter, "", False, Nothing)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                TxtFirstTranche.Text = Format(CDec(dsList.Tables(0).Rows(0)("first_tranche")), Session("fmtcurrency").ToString())
                TxtSecondTranche.Text = Format(CDec(dsList.Tables(0).Rows(0)("second_tranche")), Session("fmtcurrency").ToString())
                txtThirdTranche.Text = Format(CDec(dsList.Tables(0).Rows(0)("third_tranche")), Session("fmtcurrency").ToString())
                TxtFourthTranche.Text = Format(CDec(dsList.Tables(0).Rows(0)("fourth_tranche")), Session("fmtcurrency").ToString())
                TxtFifthTranche.Text = Format(CDec(dsList.Tables(0).Rows(0)("fifth_tranche")), Session("fmtcurrency").ToString())

                If IsDBNull(dsList.Tables(0).Rows(0)("first_tranchedate")) = False Then
                    dtpFirstTrancheDate.SetDate = CDate(dsList.Tables(0).Rows(0)("first_tranchedate")).Date
                End If
                If IsDBNull(dsList.Tables(0).Rows(0)("second_tranchedate")) = False Then
                    dtpSecondTrancheDate.SetDate = CDate(dsList.Tables(0).Rows(0)("second_tranchedate")).Date
                End If
                If IsDBNull(dsList.Tables(0).Rows(0)("third_tranchedate")) = False Then
                    dtpThirdTrancheDate.SetDate = CDate(dsList.Tables(0).Rows(0)("third_tranchedate")).Date
                End If
                If IsDBNull(dsList.Tables(0).Rows(0)("fourth_tranchedate")) = False Then
                    dtpFourthTranchDate.SetDate = CDate(dsList.Tables(0).Rows(0)("fourth_tranchedate")).Date
                End If
                If IsDBNull(dsList.Tables(0).Rows(0)("fifth_tranchedate")) = False Then
                    dtpFifthTrancheDate.SetDate = CDate(dsList.Tables(0).Rows(0)("fifth_tranchedate")).Date
                End If
            End If

            'Dim mintTrancheNo As Integer = 1
            Dim dtTotalTranche As DataTable = objLoanApplication.GetLoanTracheNo(CInt(cboLoanApplicationNo.SelectedValue))

            'If mintLoanTrancheRequestId > 0 Then
            mintTrancheNo = dtTotalTranche.Rows.Count
            'Else
            'mintTrancheNo = dtTotalTranche.Rows.Count + 1
            'End If

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.

            'If mintTrancheNo <= 1 AndAlso (dtpSecondTrancheDate.IsNull = False AndAlso dtpTrancheDate.GetDate.Date >= dtpSecondTrancheDate.GetDate.Date) Then
            '    mintTrancheNo += 1
            '    mblnValidTrancheDate = True

            'ElseIf mintTrancheNo <= 2 AndAlso (dtpThirdTrancheDate.IsNull = False AndAlso dtpTrancheDate.GetDate.Date >= dtpThirdTrancheDate.GetDate.Date) Then
            '    mintTrancheNo += 1
            '    mblnValidTrancheDate = True

            'ElseIf mintTrancheNo <= 3 AndAlso (dtpFourthTranchDate.IsNull = False AndAlso dtpTrancheDate.GetDate.Date >= dtpFourthTranchDate.GetDate.Date) Then
            '    mintTrancheNo += 1
            '    mblnValidTrancheDate = True

            'ElseIf mintTrancheNo <= 4 AndAlso dtpFifthTrancheDate.IsNull = False AndAlso dtpTrancheDate.GetDate.Date >= dtpFifthTrancheDate.GetDate.Date Then
            '    mintTrancheNo += 1
            '    mblnValidTrancheDate = True

            'ElseIf mintTrancheNo >= 5 Then
            '    mintTrancheNo += 1
            '    mblnValidTrancheDate = False
            'End If

            If mintTrancheNo <= 1 Then
                mintTrancheNo += 1

            ElseIf mintTrancheNo <= 2 Then
                mintTrancheNo += 1

            ElseIf mintTrancheNo <= 3 Then
                mintTrancheNo += 1

            ElseIf mintTrancheNo <= 4 Then
                mintTrancheNo += 1

            ElseIf mintTrancheNo >= 5 Then
                mintTrancheNo += 1
            End If

            'Pinkal (17-May-2024) -- End


            Dim dtTrancheAmount As DataTable = objLoanApplication.GetLoanTracheAmount(CInt(cboLoanApplicationNo.SelectedValue), mintFinalLoanApplicationApproveId, mintTrancheNo)
            If dtTrancheAmount IsNot Nothing AndAlso dtTrancheAmount.Rows.Count > 0 Then

                If mintTrancheNo = 1 Then
                    mdecOriginalTrancheAmount = CDec(dtTrancheAmount.Rows(0)("first_tranche"))
                ElseIf mintTrancheNo = 2 Then
                    mdecOriginalTrancheAmount = CDec(dtTrancheAmount.Rows(0)("second_tranche"))
                ElseIf mintTrancheNo = 3 Then
                    mdecOriginalTrancheAmount = CDec(dtTrancheAmount.Rows(0)("third_tranche"))
                ElseIf mintTrancheNo = 4 Then
                    mdecOriginalTrancheAmount = CDec(dtTrancheAmount.Rows(0)("fourth_tranche"))
                ElseIf mintTrancheNo = 5 Then
                    mdecOriginalTrancheAmount = CDec(dtTrancheAmount.Rows(0)("fifth_tranche"))
                Else
                    mdecOriginalTrancheAmount = 0
                End If
                If mintLoanTrancheRequestId <= 0 Then
                    TxtTrancheAmount.Text = Format(mdecOriginalTrancheAmount, Session("fmtcurrency").ToString())
                End If
            End If
            dtTrancheAmount.Clear()
            dtTrancheAmount = Nothing

            objLoanApproval = Nothing
            objLoan = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanApplication = Nothing
        End Try
    End Sub

    Protected Sub cboLoanscheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanscheme.SelectedIndexChanged
        Dim mstrDocumentTypeIds As String = ""
        Try
            If CInt(cboLoanscheme.SelectedValue) > 0 Then
                Dim objLoanScheme As New clsLoan_Scheme
                objLoanScheme._Loanschemeunkid = CInt(cboLoanscheme.SelectedValue)
                mstrDocumentTypeIds = objLoanScheme._LoanTrancheDocumentTypeIDs
                objLoanScheme = Nothing
            End If
            FillDocumentTypeCombo(mstrDocumentTypeIds)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExRate.Text = ""
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, mdtPeriodEnd, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, Session("fmtCurrency").ToString()) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboDeductionPeriod_SelectedIndexChanged()
        Try
            Call SetDateFormat()

            Dim objPeriod As New clscommom_period_Tran
            objvalPeriodDuration.Visible = True
            If mintDeductionPeriodUnkId > 0 Then
                objPeriod._Periodunkid(Session("Database_Name").ToString) = mintDeductionPeriodUnkId
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
                objvalPeriodDuration.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Period Duration From :") & " " & objPeriod._Start_Date.Date & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "To") & " " & objPeriod._End_Date.Date
                objPeriod = Nothing
            Else
                objvalPeriodDuration.Text = ""
                objvalPeriodDuration.Visible = False
                mdtPeriodStart = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
                mdtPeriodEnd = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Gridview Events "

    Protected Sub dgAttachment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAttachment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(getColumnId_Datagrid(dgAttachment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtLoanTrancheApplicationDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(getColumnId_Datagrid(dgAttachment, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtLoanTrancheApplicationDocument.Select("GUID = '" & e.Item.Cells(getColumnId_Datagrid(dgAttachment, "objcolhGUID", False, True)).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 36, "Are you sure you want to delete this attachment?")
                        popup_YesNo.Show()
                        mintDeleteRowIndex = mdtLoanTrancheApplicationDocument.Rows.IndexOf(xrow(0))
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Linkbutton Events"

    Protected Sub lnkLoanDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLoanDetails.Click
        Try
            If CInt(cboLoanApplicationNo.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 1, "Loan Application is compulsory information.Please Select Loan Application."), Me)
                cboLoanApplicationNo.Focus()
                Exit Sub
            End If
            popupLoanDetails.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblTrancheApplicationNo.ID, Me.LblTrancheApplicationNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblTrancheDate.ID, Me.LblTrancheDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblEmployee.ID, Me.LblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanScheme.ID, Me.LblLoanScheme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRemark.ID, Me.LblRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkLoanDetails.ID, Me.lnkLoanDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblTrancheAmount.ID, Me.LblTrancheAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSave.ID, Me.btnSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanDetails.ID, Me.LblLoanDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblApplicationDate.ID, Me.LblApplicationDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanEmployee.ID, Me.LblLoanEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanschme.ID, Me.LblLoanschme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblApprovedLoanAmt.ID, Me.LblApprovedLoanAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFirstTranche.ID, Me.LblFirstTranche.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblSecodTranche.ID, Me.LblSecodTranche.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblThirdTranche.ID, Me.LblThirdTranche.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFourthTranche.ID, Me.LblFourthTranche.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFifthTranche.ID, Me.LblFifthTranche.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFirstTrancheDate.ID, Me.LblFirstTrancheDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblSecondTrancheDate.ID, Me.LblSecondTrancheDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblThirdTrancheDate.ID, Me.LblThirdTrancheDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFourthTrancheDate.ID, Me.LblFourthTrancheDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFifthTrancheDate.ID, Me.LblFifthTrancheDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnLoanDetailClose.ID, Me.btnLoanDetailClose.Text)

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblVerifyOTP.ID, Me.lblVerifyOTP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblOTP.ID, Me.LblOTP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSendCodeAgain.ID, Me.btnSendCodeAgain.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnVerify.ID, Me.btnVerify.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnTOTPClose.ID, Me.btnTOTPClose.Text)
            'Pinkal (16-Nov-2023) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.LblTrancheApplicationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblTrancheApplicationNo.ID, Me.LblTrancheApplicationNo.Text)
            Me.LblTrancheDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblTrancheDate.ID, Me.LblTrancheDate.Text)
            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.LblLoanApplicationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)
            Me.LblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanScheme.ID, Me.LblLoanScheme.Text)
            Me.LblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRemark.ID, Me.LblRemark.Text)
            Me.lnkLoanDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkLoanDetails.ID, Me.lnkLoanDetails.Text)
            Me.LblTrancheAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblTrancheAmount.ID, Me.LblTrancheAmount.Text)
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)

            Me.LblLoanDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanDetails.ID, Me.LblLoanDetails.Text)
            Me.LblLoanApplicationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)
            Me.LblApplicationDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblApplicationDate.ID, Me.LblApplicationDate.Text)
            Me.LblLoanEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanEmployee.ID, Me.LblLoanEmployee.Text)
            Me.LblLoanschme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanschme.ID, Me.LblLoanschme.Text)
            Me.LblApprovedLoanAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblApprovedLoanAmt.ID, Me.LblApprovedLoanAmt.Text)
            Me.LblFirstTranche.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFirstTranche.ID, Me.LblFirstTranche.Text)
            Me.LblSecodTranche.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblSecodTranche.ID, Me.LblSecodTranche.Text)
            Me.LblThirdTranche.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblThirdTranche.ID, Me.LblThirdTranche.Text)
            Me.LblFourthTranche.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFourthTranche.ID, Me.LblFourthTranche.Text)
            Me.LblFifthTranche.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFifthTranche.ID, Me.LblFifthTranche.Text)
            Me.LblFirstTrancheDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFirstTrancheDate.ID, Me.LblFirstTrancheDate.Text)
            Me.LblSecondTrancheDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblSecondTrancheDate.ID, Me.LblSecondTrancheDate.Text)
            Me.LblThirdTrancheDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblThirdTrancheDate.ID, Me.LblThirdTrancheDate.Text)
            Me.LblFourthTrancheDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFourthTrancheDate.ID, Me.LblFourthTrancheDate.Text)
            Me.LblFifthTrancheDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFifthTrancheDate.ID, Me.LblFifthTrancheDate.Text)
            Me.btnLoanDetailClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnLoanDetailClose.ID, Me.btnLoanDetailClose.Text)

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            Me.lblVerifyOTP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVerifyOTP.ID, Me.lblVerifyOTP.Text)
            Me.LblOTP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblOTP.ID, Me.LblOTP.Text)
            Me.btnSendCodeAgain.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSendCodeAgain.ID, Me.btnSendCodeAgain.Text)
            Me.btnVerify.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnVerify.ID, Me.btnVerify.Text)
            Me.btnTOTPClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnTOTPClose.ID, Me.btnTOTPClose.Text)
            'Pinkal (16-Nov-2023) -- End

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub


#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Loan Application is compulsory information.Please Select Loan Application No.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Loan Tranche Amount should be greater than 0.Please enter valid Loan Tranche Amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "You can not apply for this Loan Tranche application.Reason : You have to wait for the eligible date for the next tranche.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Selected information is already present for particular employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Loan Tranche application submitted successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Loan Trache Amount should be less than or equal to")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "File does not exist on localpath.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Period Duration From :")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "To")

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Mobile No is compulsory information. Please set Employee's Mobile No to send TOTP.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "You cannot save this loan tranche application.Reason :  As You reached Maximum unsuccessful attempts.Please try after")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "mintues")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan tranche application.")
            'Pinkal (16-Nov-2023) -- End

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "You can not apply for this Loan Tranche application.Reason : All Traches are applied for this loan application.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings

    
    
End Class
