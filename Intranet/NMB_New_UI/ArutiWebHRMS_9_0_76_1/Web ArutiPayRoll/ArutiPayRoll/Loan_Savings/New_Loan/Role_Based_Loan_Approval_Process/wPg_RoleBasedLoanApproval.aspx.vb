﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Net.Dns
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

#End Region

Partial Class Loan_Savings_New_Loan_Role_Based_Loan_Approval_Process_wPg_RoleBasedLoanApproval
    Inherits Basepage

#Region "Private Variables"

    Private Shared ReadOnly mstrModuleName As String = "frmRoleBasedLoanApproval"
    Dim DisplayMessage As New CommonCodes
    Private objCONN As SqlConnection
    Private objProcesspendingloan As New clsProcess_pending_loan
    Private mintProcesspendingloanunkid As Integer = -1
    Private mdecInstallmentAmt As Decimal = 0
    Private mdecEMIInstallments As Decimal = 0
    Private mstrMaxInstallmentHeadFormula As String = String.Empty
    Private mdecMaxLoanAmountDefined As Decimal = 0
    Private mdecEstimateMaxInstallmentAmt As Decimal = 0
    Private mintMaxNoOfInstallmentLoanScheme As Integer = 1
    Private mintMaxLoanCalcTypeId As Integer = 0
    Private mstrMaxLoanAmountHeadFormula As String = String.Empty
    Private mintDeductionPeriodUnkId As Integer = 0
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    Private mdtApplcationDate As Date
    Private decInstallmentAmount As Decimal = 0
    Private decInstrAmount As Decimal = 0
    Private decTotIntrstAmount As Decimal = 0
    Private decTotInstallmentAmount As Decimal = 0
    Private decNetAmount As Decimal = 0
    Private mblnIsAttachmentRequired As Boolean = False
    Private mstrDocumentTypeIDs As String = String.Empty
    Private mdtLoanApplicationDocument As DataTable
    Private objDocument As New clsScan_Attach_Documents

    Private mintPendingLoanAprovalunkid As Integer = -1
    Private mintMapUserId As Integer = -1
    Private mdecMaxLoanAmount As Decimal = 0
    Private mdecMinLoanAmount As Decimal = 0
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mstrApplicationNo As String = String.Empty

    'Pinkal (12-Oct-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mblnEligibleForTopUp As Boolean = False
    Private mblnPostingToFlexcube As Boolean = False
    Private mintInstallmentPaidForFlexcube As Integer = 0
    'Pinkal (12-Oct-2022) -- End
    Private objRoleBasedApproval As New clsroleloanapproval_process_Tran

    'Pinkal (27-Oct-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mdtOtherDocument As DataTable = Nothing
    Private mblnRequiredReportingToApproval As Boolean = False
    Private mintOtherDocumentunkid As Integer = 0
    'Pinkal (27-Oct-2022) -- End

    'Pinkal (10-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mblnLoanApproval_DailyReminder As Boolean
    Private mintEscalationDays As Integer = 0
    'Pinkal (10-Nov-2022) -- End

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mintDeleteRowIndex As Integer = -1
    Private mstrLoanSchemeCode As String = ""
    Private blnpopupCRBData As Boolean = False
    Private mdtGetCRBData As DataTable = Nothing
    Private mdtFinalCRBData As DataTable = Nothing
    Private mdtCRBDocument As DataTable
    Private LstToken As New List(Of String)
    'Pinkal (23-Nov-2022) -- End


    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .
    Dim mstrSecretKey As String = ""
    Private blnShowTOTPPopup As Boolean = False
    Dim Counter As Integer = 0
    Dim mstrEmpMobileNo As String = ""
    Dim mstrEmployeeName As String = ""
    Dim mstrEmployeeEmail As String = ""
    Dim xStatusId As Integer = enLoanApplicationStatus.PENDING
    'Pinkal (02-Jun-2023) -- End

    'Pinkal (21-Mar-2024) -- Start
    'NMB - Mortgage UAT Enhancements.
    Dim mdecCreditCardAmountOnExposure As Decimal = 0
    Dim xCurrentPriority As Integer = -999
    'Pinkal (21-Mar-2024) -- End
    'Hemant (18 Mar 2024) -- Start
    'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
    Dim mstrCompanyGroupName As String = ""
    'Hemant (18 Mar 2024) -- End
    'Hemant (10 May 2024) -- Start
    'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
    Private mdtApproverDocument As DataTable = Nothing
    'Hemant (10 May 2024) -- End
    'Hemant (22 Nov 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
    Dim mdecActualSalaryAdvancePendingPrincipalAmt As Decimal = 0
    Dim mdecFinalSalaryAdvancePendingPrincipalAmt As Decimal = 0
    'Hemant (22 Nov 2024) -- End
#End Region

#Region "Page's Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub
                KillIdleSQLSessions()
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If

                If Request.QueryString("uploadimage") Is Nothing AndAlso Request.QueryString("uploadapproverimage") Is Nothing Then
                    'Hemant (10 May 2024) -- [AndAlso Request.QueryString("uploadapproverimage") Is Nothing]

                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))


                If arr.Length = 4 Then
                mintPendingLoanAprovalunkid = CInt(arr(0))
                mintMapUserId = CInt(arr(1))
                mintProcesspendingloanunkid = CInt(arr(2))
                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(3))
                    HttpContext.Current.Session("UserId") = mintMapUserId

                        'Pinkal (23-Feb-2024) -- Start
                        '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                        Dim objConfig As New clsConfigOptions
                        Dim mblnATLoginRequiredToApproveApplications As Boolean = CBool(objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "LoginRequiredToApproveApplications", Nothing))
                        objConfig = Nothing

                        If mblnATLoginRequiredToApproveApplications = False Then
                            Dim objBasePage As New Basepage
                            objBasePage.GenerateAuthentication()
                            objBasePage = Nothing
                        End If


                        If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                            If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then
                                Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                                Session("ApproverUserId") = CInt(Session("UserId"))
                                DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                                Exit Sub
                            Else

                                If mblnATLoginRequiredToApproveApplications = False Then

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")

                    gobjConfigOptions = New clsConfigOptions
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))

                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)


                    If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", ConfigParameter._Object._ArutiSelfServiceURL)
                    End If
                    Session("DateFormat") = ConfigParameter._Object._CompanyDateFormat
                    Session("DateSeparator") = ConfigParameter._Object._CompanyDateSeparator

                    Call SetDateFormat()

                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))
                    objUser = Nothing


                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If


                                End If '  If mblnATLoginRequiredToApproveApplications = False Then

                            End If ' If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                        Else
                            Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                            Session("ApproverUserId") = CInt(Session("UserId"))
                            DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                            Exit Sub

                        End If  ' If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then



                    Dim dsList As DataSet = Nothing
                    Dim mstrSearch As String = "lna.processpendingloanunkid = " & mintProcesspendingloanunkid & " AND lna.pendingloanaprovalunkid = " & mintPendingLoanAprovalunkid

                        'Pinkal (15-Feb-2023) -- Start
                        'When Employee Deleted the loan application and approver again access that link at that time we can give this message.
                        'dsList = objRoleBasedApproval.GetRoleBasedApprovalList(Session("Database_Name").ToString, _
                        '                                             CInt(Session("UserId")), _
                        '                                             CInt(Session("Fin_year")), _
                        '                                             CInt(Session("CompanyUnkId")), _
                        '                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                        '                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                        '                                             CStr(Session("UserAccessModeSetting")), True, _
                        '                                             CBool(Session("IsIncludeInactiveEmp")), "List", mstrSearch, "", Nothing)

                    dsList = objRoleBasedApproval.GetRoleBasedApprovalList(Session("Database_Name").ToString, _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 CStr(Session("UserAccessModeSetting")), True, _
                                                                 CBool(Session("IsIncludeInactiveEmp")), "List", mstrSearch, "", False, Nothing)

                        'Pinkal (15-Feb-2023) -- End


                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            'Pinkal (21-Jul-2023) -- Start
                            '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
                            'If CInt(dsList.Tables(0).Rows(0)("statusunkid")) <> enLoanApplicationStatus.PENDING Then
                            If CInt(dsList.Tables(0).Rows(0)("visibleunkid")) <> enLoanApplicationStatus.PENDING Then
                                'Pinkal (21-Jul-2023) -- End

                            If CInt(dsList.Tables(0).Rows(0)("statusunkid")) = enLoanApplicationStatus.REJECTED Then

                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 34, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me.Page, Session("rootpath").ToString & "Index.aspx")

                                    'Pinkal (23-Feb-2024) -- Start
                                    '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                                    Session("ApprovalLink") = Nothing
                                    Session("ApproverUserId") = Nothing

                                    If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                        If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                            Session.Abandon()
                                            If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                                Response.Cookies("ASP.NET_SessionId").Value = ""
                                                Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                                Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                            End If

                                            If Request.Cookies("AuthToken") IsNot Nothing Then
                                                Response.Cookies("AuthToken").Value = ""
                                                Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                            End If

                                        End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                Exit Sub

                                    'Pinkal (23-Feb-2024) -- End

                            Else
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 35, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me.Page, Session("rootpath").ToString & "Index.aspx")

                                    'Pinkal (23-Feb-2024) -- Start
                                    '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                                    Session("ApprovalLink") = Nothing
                                    Session("ApproverUserId") = Nothing

                                    If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                        If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                            Session.Abandon()
                                            If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                                Response.Cookies("ASP.NET_SessionId").Value = ""
                                                Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                                Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                            End If

                                            If Request.Cookies("AuthToken") IsNot Nothing Then
                                                Response.Cookies("AuthToken").Value = ""
                                                Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                        End If

                                        End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                    Exit Sub

                                    'Pinkal (23-Feb-2024) -- End

                                End If '   If CInt(dsList.Tables(0).Rows(0)("statusunkid")) = enLoanApplicationStatus.REJECTED Then

                            End If '       If CInt(dsList.Tables(0).Rows(0)("visibleunkid")) <> enLoanApplicationStatus.PENDING Then

                            'Pinkal (15-Feb-2023) -- Start
                            'When Employee Deleted the loan application and approver again access that link at that time we can give this message.
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 92, "Sorry, there is no pending loan application data."), Me.Page, Session("rootpath").ToString & "Index.aspx")

                            'Pinkal (23-Feb-2024) -- Start
                            '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                            Session("ApprovalLink") = Nothing
                            Session("ApproverUserId") = Nothing

                            If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    Session.Abandon()
                                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                        Response.Cookies("ASP.NET_SessionId").Value = ""
                                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                    End If

                                    If Request.Cookies("AuthToken") IsNot Nothing Then
                                        Response.Cookies("AuthToken").Value = ""
                                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                    End If

                                End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                            End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                            Exit Sub

                            'Pinkal (23-Feb-2024) -- End

                            'Pinkal (15-Feb-2023) -- End
                        End If  ' If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                    Dim dtRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") >= CInt(dsList.Tables(0).Rows(0)("priority")) AndAlso x.Field(Of Integer)("statusunkid") <> enLoanApplicationStatus.PENDING)
                    If dtRow.Count > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 36, "You can't Edit this Loan detail. Reason: This Loan is already approved/reject."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If
                    GoTo Link
                End If

                    'Pinkal (23-Nov-2022) -- Start
                    'NMB Loan Module Enhancement.
                End If  ' If Request.QueryString("uploadimage") Is Nothing Then
                'Pinkal (23-Nov-2022) -- End

            End If

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
Link:
            If IsPostBack = False Then

                SetControlCaptions()
                SetLanguage()
                'Pinkal (14-Dec-2022) -- Start
                'NMB Loan Module Enhancement.
                Call SetMessages()
                'Pinkal (14-Dec-2022) -- End
                If Session("pendingloanaprovalunkid") IsNot Nothing Then
                    mintPendingLoanAprovalunkid = CInt(Session("pendingloanaprovalunkid"))
                End If

                If Session("Mapuserunkid") IsNot Nothing Then
                    mintMapUserId = CInt(Session("Mapuserunkid"))
                End If

                If Session("ProcessPendingLoanunkid") IsNot Nothing Then
                    mintProcesspendingloanunkid = CInt(Session("ProcessPendingLoanunkid"))
                End If

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                SetVisibility()
                'Pinkal (23-Nov-2022) -- End
                Call FillCombo()
                FillPeriod(False)
                FillDocumentTypeCombo()
                'Hemant (10 May 2024) -- Start
                'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
                FillApproverDocumentTypeCombo()
                'Hemant (10 May 2024) -- End
                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                Dim objGroup As New clsGroup_Master
                objGroup._Groupunkid = 1
                mstrCompanyGroupName = objGroup._Groupname.ToString().ToUpper()
                objGroup = Nothing
                'Hemant (29 Mar 2024) -- End
                Call GetValue()

                'Hemant (27 Oct 2023) -- Start
                'ENHANCEMENT(NMB): A1X-1443 - Show comments of the previous loan approver to the next level approver
                Call SetPreviousApproverComment(mintProcesspendingloanunkid, mintPendingLoanAprovalunkid)
                'Hemant (27 Oct 2023) -- End

                'Hemant (10 May 2024) -- Start
                'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
                'pnlApproverScanAttachment.Visible = False
                'If mstrCompanyGroupName = "TADB" Then
                '    pnlApproverScanAttachment.Visible = True
                '    btnAddApproverFile.Visible = CBool(Session("AllowToAttachDocumentOnRolebaseLoanApproval"))
                '    dgvApproverAttachement.Columns(0).Visible = CBool(Session("AllowToAttachDocumentOnRolebaseLoanApproval"))
                'End If
                'Hemant (10 May 2024) -- End

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                    pnlApproverScanAttachment.Visible = True
                cboDocumentType.Visible = CBool(Session("AllowToAttachDocumentOnRolebaseLoanApproval"))
                    btnAddApproverFile.Visible = CBool(Session("AllowToAttachDocumentOnRolebaseLoanApproval"))
                    dgvApproverAttachement.Columns(0).Visible = CBool(Session("AllowToAttachDocumentOnRolebaseLoanApproval"))
                btnOfferLetter.Visible = CBool(Session("AllowToPreviewOfferLetterOnLoanApproval"))
                'Pinkal (17-May-2024) -- End

            Else
                mintPendingLoanAprovalunkid = CInt(Me.ViewState("pendingloanaprovalunkid"))
                mintMapUserId = CInt(Me.ViewState("Mapuserunkid"))
                mintProcesspendingloanunkid = CInt(Me.ViewState("ProcessPendingLoanunkid"))
                mstrBaseCurrSign = ViewState("mstrBaseCurrSign").ToString()
                mdtPeriodStart = CDate(Me.ViewState("mdtPeriodStart"))
                mdtPeriodEnd = CDate(Me.ViewState("mdtPeriodEnd"))
                mintBaseCurrId = CInt(ViewState("mintBaseCurrId"))
                mintPaidCurrId = CInt(ViewState("mintPaidCurrId"))
                mdecBaseExRate = CDec(ViewState("mdecBaseExRate"))
                mdecPaidExRate = CDec(ViewState("mdecPaidExRate"))
                mdecInstallmentAmt = CDec(Me.ViewState("InstallmentAmt"))
                mdtLoanApplicationDocument = CType(ViewState("mdtLoanApplicationDocument"), DataTable)
                'Pinkal (12-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                mintDeductionPeriodUnkId = CInt(Me.ViewState("DeductionPeriodUnkId"))
                mblnEligibleForTopUp = CBool(Me.ViewState("EligibleForTopUp"))
                mblnPostingToFlexcube = CBool(Me.ViewState("PostingToFlexcube"))
                mintInstallmentPaidForFlexcube = CInt(Me.ViewState("InstallmentPaidForFlexcube"))
                'Pinkal (12-Oct-2022) -- End

                'Pinkal (27-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnRequiredReportingToApproval = CBool(Me.ViewState("RequiredReportingToApproval"))
                mdtOtherDocument = CType(ViewState("OtherLoanApplicationDocument"), DataTable)
                mintOtherDocumentunkid = CInt(Me.ViewState("OtherDocumentunkid"))
                'Pinkal (27-Oct-2022) -- End

                'Pinkal (10-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnLoanApproval_DailyReminder = CBool(Me.ViewState("LoanApproval_DailyReminder"))
                mintEscalationDays = CInt(Me.ViewState("EscalationDays"))
                'Pinkal (10-Nov-2022) -- End

                'Pinkal (21-Mar-2024) -- Start
                'NMB - Mortgage UAT Enhancements.
                mdecCreditCardAmountOnExposure = CDec(Me.ViewState("CreditCardAmountOnExposure"))
                xCurrentPriority = CInt(Me.ViewState("CurrentPriority"))
                'Pinkal (21-Mar-2024) -- End
                'Hemant (18 Mar 2024) -- Start
                'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
                mstrCompanyGroupName = CStr(Me.ViewState("CompanyGroupName"))
                'Hemant (18 Mar 2024) -- End
                'Hemant (10 May 2024) -- Start
                'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
                mdtApproverDocument = CType(ViewState("ApproverLoanApplicationDocument"), DataTable)
                'Hemant (10 May 2024) -- End

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                If ViewState("blnpopupCRBData") IsNot Nothing Then
                    blnpopupCRBData = Convert.ToBoolean(ViewState("blnpopupCRBData").ToString())
                End If
                mstrLoanSchemeCode = Me.ViewState("LoanSchemeCode").ToString()
                mdtGetCRBData = CType(Me.ViewState("GetCRBData"), DataTable)
                mdtFinalCRBData = CType(Me.ViewState("FinalCRBData"), DataTable)
                mdtCRBDocument = CType(ViewState("CRBDocument"), DataTable)
                mintDeleteRowIndex = CInt(Me.ViewState("DeleteRowIndex"))
                If blnpopupCRBData Then
                    popupCRBData.Show()
                End If

                'Pinkal (23-Nov-2022) -- End

                'Pinkal (02-Jun-2023) -- Start
                'NMB Enhancement : TOTP Related Changes .
                mstrSecretKey = CStr(Me.ViewState("mstrSecretKey"))
                Counter = CInt(Me.ViewState("Counter"))
                blnShowTOTPPopup = CBool(Me.ViewState("blnShowTOTPPopup"))
                mstrEmployeeName = CStr(Me.ViewState("EmployeeName"))
                mstrEmployeeEmail = CStr(Me.ViewState("EmployeeEmail"))
                mstrEmpMobileNo = CStr(Me.ViewState("EmployeeMobileNo"))
                xStatusId = CInt(Me.ViewState("xStatusId"))
                'Hemant (22 Nov 2024) -- Start
                'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                mdecActualSalaryAdvancePendingPrincipalAmt = CDec(ViewState("mdecActualSalaryAdvancePendingPrincipalAmt"))
                mdecFinalSalaryAdvancePendingPrincipalAmt = CDec(ViewState("mdecFinalSalaryAdvancePendingPrincipalAmt"))
                'Hemant (22 Nov 2024) -- End

                If blnShowTOTPPopup Then
                    popup_TOTP.Show()
                End If
                'Pinkal (02-Jun-2023) -- End



            End If

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            If Request.QueryString("uploadimage") IsNot Nothing Then
                If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                    Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                    postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                    Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                End If
            End If
            'Pinkal (23-Nov-2022) -- End

            'Hemant (10 May 2024) -- Start
            'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
            If Request.QueryString("uploadapproverimage") IsNot Nothing Then
                If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadapproverimage"))) = True Then
                    Dim postedFile As HttpPostedFile = Context.Request.Files("myapproverfile")
                    postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                    Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                End If
            End If
            'Hemant (10 May 2024) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("pendingloanaprovalunkid") = mintPendingLoanAprovalunkid
            Me.ViewState("ProcessPendingLoanunkid") = mintProcesspendingloanunkid
            Me.ViewState("Mapuserunkid") = mintMapUserId
            Me.ViewState("InstallmentAmt") = mdecInstallmentAmt
            Me.ViewState("mdtPeriodStart") = mdtPeriodStart
            Me.ViewState("mdtPeriodEnd") = mdtPeriodEnd
            Me.ViewState("mstrBaseCurrSign") = mstrBaseCurrSign
            Me.ViewState("mintBaseCurrId") = mintBaseCurrId
            Me.ViewState("mintPaidCurrId") = mintPaidCurrId
            Me.ViewState("mdecBaseExRate") = mdecBaseExRate
            Me.ViewState("mdecPaidExRate") = mdecPaidExRate
            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            Me.ViewState("DeductionPeriodUnkId") = mintDeductionPeriodUnkId
            Me.ViewState("EligibleForTopUp") = mblnEligibleForTopUp
            Me.ViewState("PostingToFlexcube") = mblnPostingToFlexcube
            Me.ViewState("InstallmentPaidForFlexcube") = mintInstallmentPaidForFlexcube
            'Pinkal (12-Oct-2022) -- End

            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            Me.ViewState("LoanApproval_DailyReminder") = mblnLoanApproval_DailyReminder
            Me.ViewState("EscalationDays") = mintEscalationDays
            'Pinkal (10-Nov-2022) -- End

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            Me.ViewState("LoanSchemeCode") = mstrLoanSchemeCode
            ViewState("blnpopupCRBData") = blnpopupCRBData
            Me.ViewState("GetCRBData") = mdtGetCRBData
            Me.ViewState("FinalCRBData") = mdtFinalCRBData
            Me.ViewState("DeleteRowIndex") = mintDeleteRowIndex
            If Request.QueryString("uploadimage") Is Nothing Then
                Me.ViewState("CRBDocument") = mdtCRBDocument
            End If
            'Pinkal (23-Nov-2022) -- End


            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            Me.ViewState("CreditCardAmountOnExposure") = mdecCreditCardAmountOnExposure
            Me.ViewState("CurrentPriority") = xCurrentPriority
            'Pinkal (21-Mar-2024) -- End


            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            Me.ViewState("RequiredReportingToApproval") = mblnRequiredReportingToApproval
            Me.ViewState("OtherDocumentunkid") = mintOtherDocumentunkid

                If ViewState("mdtLoanApplicationDocument") Is Nothing Then
                    ViewState.Add("mdtLoanApplicationDocument", mdtLoanApplicationDocument)
                Else
                    ViewState("mdtLoanApplicationDocument") = mdtLoanApplicationDocument
                End If

            If ViewState("OtherLoanApplicationDocument") Is Nothing Then
                ViewState.Add("OtherLoanApplicationDocument", mdtOtherDocument)
            Else
                ViewState("OtherLoanApplicationDocument") = mdtOtherDocument
                End If
            'Pinkal (27-Oct-2022) -- End

            'Hemant (10 May 2024) -- Start
            'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
            If ViewState("ApproverLoanApplicationDocument") Is Nothing Then
                ViewState.Add("ApproverLoanApplicationDocument", mdtApproverDocument)
            Else
                ViewState("ApproverLoanApplicationDocument") = mdtApproverDocument
            End If
            'Hemant (10 May 2024) -- End


            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Me.ViewState("Counter") = Counter
            Me.ViewState("mstrSecretKey") = mstrSecretKey
            Me.ViewState("blnShowTOTPPopup") = blnShowTOTPPopup
            Me.ViewState("EmployeeName") = mstrEmployeeName
            Me.ViewState("EmployeeEmail") = mstrEmployeeEmail
            Me.ViewState("EmployeeMobileNo") = mstrEmpMobileNo
            Me.ViewState("xStatusId") = xStatusId
            'Pinkal (02-Jun-2023) -- End
            'Hemant (18 Mar 2024) -- Start
            'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
            Me.ViewState("CompanyGroupName") = mstrCompanyGroupName
            'Hemant (18 Mar 2024) -- End
            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            Me.ViewState("mdecActualSalaryAdvancePendingPrincipalAmt") = mdecActualSalaryAdvancePendingPrincipalAmt
            Me.ViewState("mdecFinalSalaryAdvancePendingPrincipalAmt") = mdecFinalSalaryAdvancePendingPrincipalAmt
            'Hemant (22 Nov 2024) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Pinkal (27-Oct-2022) -- Start
        'NMB Loan Module Enhancement.
        If Request.QueryString.Count <= 0 Then
        Me.IsLoginRequired = True
        End If
        'Pinkal (27-Oct-2022) -- End
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objLoan_Advance As New clsLoan_Advance
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     Session("UserAccessModeSetting").ToString, _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), _
                                                     "List", True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, False, False, "", "", "", False, False, "")

                With cboEmpName
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                    .SelectedValue = "0"
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmpName.DataSource = objglobalassess.ListOfEmployee
                cboEmpName.DataTextField = "loginname"
                cboEmpName.DataValueField = "employeeunkid"
                cboEmpName.DataBind()
            End If

            Dim mblnSchemeShowOnEss As Boolean = True
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", mblnSchemeShowOnEss)

            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With
            cboLoanScheme_SelectedIndexChanged(Nothing, Nothing)

            Dim objCurrency As New clsExchangeRate
            dsList = objCurrency.getComboList("Currency", True)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsList.Tables("Currency")
                .DataBind()
                .SelectedIndex = 0
            End With

            dsList = objLoan_Advance.GetLoan_Scheme_Categories("List")
            With cboLoanSchemeCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objLoan_Advance.GetLoanCalculationTypeList("List", True)
            With cboLoanCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsList = objLoan_Advance.GetLoan_Interest_Calculation_Type("List", True)
            With cboInterestCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
            objLoanScheme = Nothing
            objLoan_Advance = Nothing
        End Try
    End Sub

    Private Sub FillPeriod(ByVal blnOnlyFirstPeriod As Boolean)
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombos As DataSet = Nothing
        Try

            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            Dim mblnFlag As Boolean = CBool(Session("AllowToChangeDeductionPeriodOnLoanApproval"))


            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                 CInt(Session("Fin_year")), _
                                                 Session("Database_Name").ToString, _
                                                 CDate(Session("fin_startdate")), _
                                                 "List", mblnFlag, enStatusType.OPEN)

            With cboDeductionPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .DataBind()
            End With

            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            'cboDeductionPeriod_SelectedIndexChanged(cboDeductionPeriod, New EventArgs())
            'Pinkal (27-Oct-2022) -- End


            LblDeductionPeriod.Visible = CBool(Session("AllowToChangeDeductionPeriodOnLoanApproval"))
            cboDeductionPeriod.Visible = CBool(Session("AllowToChangeDeductionPeriodOnLoanApproval"))

            'Pinkal (27-Oct-2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsCombos IsNot Nothing Then dsCombos.Clear()
            dsCombos = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Function GetAmountByHeadFormula(ByVal strHeadFormula As String) As Decimal
        Dim objMaster As New clsMasterData
        Dim objPayrollProcess As New clsPayrollProcessTran
        Dim decAmount As Decimal = 0
        Try

            If CInt(cboEmpName.SelectedValue) > 0 AndAlso strHeadFormula.Trim.Length > 0 Then
                Dim intPrevPeriod As Integer = objMaster.getCurrentPeriodID(CInt(enModuleReference.Payroll), mdtPeriodStart.AddDays(-1), 0, 0, True, False, Nothing, False)
                'Hemant (22 Dec 2023) -- Start
                Dim objPeriod As New clscommom_period_Tran
                Dim objCompany As New clsCompany_Master
                objPeriod._Periodunkid(Session("Database_Name").ToString) = intPrevPeriod
                Dim intyearid As Integer = objPeriod._Yearunkid
                Dim dsCompany As DataSet = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), -1, "List", intyearid)

                Dim strDatabaseName As String = Session("Database_Name").ToString
                If dsCompany IsNot Nothing AndAlso dsCompany.Tables(0).Rows.Count > 0 Then
                    strDatabaseName = dsCompany.Tables(0).Rows(0).Item("database_name").ToString
                End If

                objPeriod = Nothing
                objCompany = Nothing

                'Hemant (22 Dec 2023) -- Start
                Dim mintUserID As Integer
                If CInt(Session("UserId")) <= 0 Then
                    mintUserID = 1
                Else
                    mintUserID = CInt(Session("UserId"))
                End If
                'Hemant (22 Dec 2023) -- End


                'Pinkal (21-Mar-2024) -- Start
                'NMB - Mortgage UAT Enhancements.
                If strHeadFormula.Trim.Length > 0 AndAlso strHeadFormula.Contains("#creditcardamountondsr#") Then
                    Dim mstrBankACNo As String = ""
                    Dim objEmpBankTran As New clsEmployeeBanks
                    Dim dsEmpBank As DataSet = objEmpBankTran.GetList(strDatabaseName, mintUserID, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString() _
                                                                                                  , True, CBool(Session("IsIncludeInactiveEmp")), "EmployeeBank", , , CInt(cboEmpName.SelectedValue).ToString(), mdtPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

                    If dsEmpBank IsNot Nothing AndAlso dsEmpBank.Tables(0).Rows.Count > 0 Then
                        If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then
                            Dim objCreditCardIntegration As New clsCreditCardIntegration
                            objCreditCardIntegration.GetEmpCreditCardAmount(dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim(), mstrLoanSchemeCode, mdecCreditCardAmountOnExposure, True)
                            objCreditCardIntegration = Nothing
                        End If
                    End If 'If dsEmpBank.Tables(0).Rows.Count > 0 Then
                    objEmpBankTran = Nothing
                End If
                'Pinkal (21-Mar-2024) -- End



                'Hemant (22 Dec 2023) -- End
                Dim dt As DataTable = objPayrollProcess.GetFormulaBasePayslipAmount(strDatabaseName, mintUserID, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStart, mdtPeriodEnd, Session("UserAccessModeSetting").ToString, True, False, "List", intPrevPeriod, CStr(cboEmpName.SelectedValue), strHeadFormula, "", _
                                                                                    Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString, Session("OracleUserPassword").ToString, mstrLoanSchemeCode)

                'Hemant (22 Nov 2024) -- [ CInt(Session("UserId")) --> mintUserID]        
                'Hemant (22 Dec 2023) -- [ Session("Database_Name").ToString --> strDatabaseName]
                'Pinkal (15-Feb-2023) -- [mstrLoanSchemeCode]
                If dt.Rows.Count > 0 Then
                    decAmount = CDec(dt.Rows(0).Item("amount"))
                End If
            End If
            Return decAmount
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "GetAmountByHeadFormula", mstrModuleName)
        Finally
            objMaster = Nothing
            objPayrollProcess = Nothing
        End Try
    End Function

    Private Sub GetValue()
        Try
            Dim objLoanapplication As New clsProcess_pending_loan
            objLoanapplication._Processpendingloanunkid = mintProcesspendingloanunkid

            txtLoanApplicationNo.Text = objLoanapplication._Application_No
            If Not (objLoanapplication._Application_Date = Nothing) Then
                dtpApplicationDate.SetDate = objLoanapplication._Application_Date.Date
            End If

            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            chkMakeExceptionalApplication.Checked = objLoanapplication._IsExceptionalApplication
            cboDeductionPeriod.SelectedValue = objLoanapplication._DeductionPeriodunkid.ToString()
            cboDeductionPeriod_SelectedIndexChanged(cboDeductionPeriod, New EventArgs())
            'Pinkal (27-Oct-2022) -- End

            cboEmpName.SelectedValue = CStr(objLoanapplication._Employeeunkid)
            cboEmpName_SelectedIndexChanged(cboEmpName, New EventArgs())
            cboEmpName.Enabled = False
            cboLoanScheme.SelectedValue = CStr(objLoanapplication._Loanschemeunkid)
            cboLoanScheme.Enabled = False
            cboLoanScheme_SelectedIndexChanged(cboLoanScheme, New EventArgs())
            cboCurrency.SelectedValue = objLoanapplication._Countryunkid.ToString()
            Call cboCurrency_SelectedIndexChanged(cboCurrency, New System.EventArgs())

            txtPlotNo.Text = objLoanapplication._PlotNo
            txtBlockNo.Text = objLoanapplication._BlockNo
            txtStreet.Text = objLoanapplication._Street
            txtTownCity.Text = objLoanapplication._TownCity
            txtMarketValue.Text = Format(objLoanapplication._MarketValue, Session("fmtCurrency").ToString)
            txtCTNo.Text = objLoanapplication._CTNo
            txtLONo.Text = objLoanapplication._LONo
            txtFSV.Text = Format(objLoanapplication._FSV, Session("fmtCurrency").ToString)
            txtModel.Text = objLoanapplication._Model
            txtYOM.Text = objLoanapplication._YOM
            txtPurposeOfCredit.Text = objLoanapplication._Emp_Remark
            txtInstallmentAmt.Text = Format(CDec(objLoanapplication._InstallmentAmount), Session("fmtCurrency").ToString)

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            txtBOQ.Text = objLoanapplication._BillOfQty.ToString()
            'Pinkal (17-May-2024) -- End


            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            txtChassisNo.Text = objLoanapplication._ChassisNo
            txtSupplier.Text = objLoanapplication._Supplier
            txtColour.Text = objLoanapplication._Colour
            'Pinkal (27-Oct-2022) -- End

            If mintPendingLoanAprovalunkid > 0 Then
                objRoleBasedApproval._Pendingloanaprovalunkid = mintPendingLoanAprovalunkid
                txtLoanAmt.Text = Format(CDec(objRoleBasedApproval._Loan_Amount), Session("fmtCurrency").ToString)
                txtLoanAmt_TextChanged(txtLoanAmt, New EventArgs())

                'Pinkal (27-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                cboDeductionPeriod.SelectedValue = objRoleBasedApproval._Deductionperiodunkid.ToString()
                'cboDeductionPeriod_SelectedIndexChanged(cboDeductionPeriod, New EventArgs())
                If CBool(Session("AllowToChangeDeductionPeriodOnLoanApproval")) Then cboDeductionPeriod.SelectedValue = "0"
                'Pinkal (27-Oct-2022) -- End
                txtInstallmentAmt.Text = Format(CDec(objRoleBasedApproval._Installmentamt), Session("fmtCurrency").ToString)
                mdecInstallmentAmt = CDec(objRoleBasedApproval._Installmentamt)
                txtEMIInstallments.Text = objRoleBasedApproval._Noofinstallment.ToString()

                'Pinkal (21-Mar-2024) -- Start
                'NMB - Mortgage UAT Enhancements.
                xCurrentPriority = objRoleBasedApproval._Priority
                'Pinkal (21-Mar-2024) -- End
            End If

            Call txtEMIInstallments_TextChanged(txtEMIInstallments, New System.EventArgs)

            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.

            If ViewState("OtherLoanApplicationDocument") IsNot Nothing Then
                mdtOtherDocument = CType(ViewState("OtherLoanApplicationDocument"), DataTable)
            Else
                mdtOtherDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.LOAN_ADVANCE, mintProcesspendingloanunkid, CStr(Session("Document_Path")))
                If mintProcesspendingloanunkid <= 0 Then
                    If mdtOtherDocument IsNot Nothing Then mdtOtherDocument.Rows.Clear()
                End If
                mdtOtherDocument = New DataView(mdtOtherDocument, "documentunkid = " & mintOtherDocumentunkid, "", DataViewRowState.CurrentRows).ToTable()
            End If
            FillLoanApplicationOtherAttachment()

            If ViewState("mdtLoanApplicationDocument") IsNot Nothing Then
                mdtLoanApplicationDocument = CType(ViewState("mdtLoanApplicationDocument"), DataTable).Copy()
            Else
                mdtLoanApplicationDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.LOAN_ADVANCE, mintProcesspendingloanunkid, CStr(Session("Document_Path")))
                If mintProcesspendingloanunkid <= 0 Then
                    If mdtLoanApplicationDocument IsNot Nothing Then mdtLoanApplicationDocument.Rows.Clear()
                End If
                mdtLoanApplicationDocument = New DataView(mdtLoanApplicationDocument, "documentunkid <> " & mintOtherDocumentunkid, "", DataViewRowState.CurrentRows).ToTable()
            End If
            Call FillLoanApplicationAttachment()
            'Pinkal (27-Oct-2022) -- End

            'Hemant (10 May 2024) -- Start
            'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
            If ViewState("ApproverLoanApplicationDocument") IsNot Nothing Then
                mdtApproverDocument = CType(ViewState("ApproverLoanApplicationDocument"), DataTable)
            Else
                If mdtApproverDocument IsNot Nothing Then mdtApproverDocument.Rows.Clear()
                Dim dsList As DataSet = Nothing
                Dim mstrSearch As String = "lna.processpendingloanunkid = " & mintProcesspendingloanunkid & " AND ( lna.pendingloanaprovalunkid = " & mintPendingLoanAprovalunkid & " OR lna.statusunkid <> " & enLoanApplicationStatus.PENDING & " ) "


                dsList = objRoleBasedApproval.GetRoleBasedApprovalList(Session("Database_Name").ToString, _
                                                                    CInt(Session("UserId")), _
                                                                    CInt(Session("Fin_year")), _
                                                                    CInt(Session("CompanyUnkId")), _
                                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                                    CBool(Session("IsIncludeInactiveEmp")), "List", mstrSearch, "", False, Nothing)
                For Each drRow As DataRow In dsList.Tables(0).Rows
                    Dim xTable As DataTable = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.ROLEBASE_LOAN_APPROVAL, CInt(drRow.Item("pendingloanaprovalunkid").ToString), CStr(Session("Document_Path")))
                    If mdtApproverDocument IsNot Nothing Then
                        mdtApproverDocument.Merge(xTable)
                    Else
                        mdtApproverDocument = xTable
                    End If

                Next

                If mintProcesspendingloanunkid <= 0 Then
                    If mdtApproverDocument IsNot Nothing Then mdtApproverDocument.Rows.Clear()
                End If
            End If
            FillLoanApplicationApproverAttachment()
            'Hemant (10 May 2024) -- End


            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            If CType(ViewState("CRBDocument"), DataTable) IsNot Nothing Then
                mdtCRBDocument = CType(ViewState("CRBDocument"), DataTable)
            Else
                mdtCRBDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.LOAN_APPROVAL, mintPendingLoanAprovalunkid, CStr(Session("Document_Path")))
            End If
            FillCRBAttachment()
            'Pinkal (23-Nov-2022) -- End

            'Pinkal (14-Dec-2022) -- Start
            'NMB Loan Module Enhancement.
            TxtFirstTranche.Text = Format(CDec(objRoleBasedApproval._FirstTranche), Session("fmtCurrency").ToString)
            TxtSecondTranche.Text = Format(CDec(objRoleBasedApproval._SecondTranche), Session("fmtCurrency").ToString)
            TxtThirdTranche.Text = Format(CDec(objRoleBasedApproval._ThirdTranche), Session("fmtCurrency").ToString)
            TxtFourthTranche.Text = Format(CDec(objRoleBasedApproval._FourTranche), Session("fmtCurrency").ToString)
            'Pinkal (14-Dec-2022) -- End

            'Hemant (07 Jul 2023) -- Start
            'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
            dtpTitleIssueDate.SetDate = CDate(objLoanapplication._TitleIssueDate)
            nudTitleValidity.Text = CStr(objLoanapplication._TitleValidity)
            dtpTitleExpiryDate.SetDate = CDate(objLoanapplication._TitleExpiryDate)
            'Hemant (07 July 2023) -- End
            'Hemant (07 Jul 2023) -- Start
            'Enhancement : NMB : Show the remaining number of pay periods & Dates to the end of applicant's contract, Retirement on loan application and approval screens
            'Dim objEmployee As New clsEmployee_Master
            'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmpName.SelectedValue)
            'Dim dtEOCDate As Date = Nothing
            'If objEmployee._Termination_To_Date <> Nothing AndAlso IsDBNull(objEmployee._Termination_To_Date) = False Then
            '    dtEOCDate = CDate(objEmployee._Termination_To_Date)
            '    lblEmplDate.Text = "Till Retr. Date"
            'End If

            'If objEmployee._Empl_Enddate <> Nothing AndAlso IsDBNull(objEmployee._Empl_Enddate) = False Then
            '    If dtEOCDate < CDate(objEmployee._Empl_Enddate) Then
            '    Else
            '        dtEOCDate = CDate(objEmployee._Empl_Enddate)
            '    End If
            '    lblEmplDate.Text = "Till EOC Date"
            'End If
            'If dtEOCDate <> Nothing Then
            '    dtpEndEmplDate.SetDate = dtEOCDate
            '    nudPayPeriodEOC.Text = CStr(Math.Abs((mdtPeriodStart.Month - dtEOCDate.Month) + 12 * (mdtPeriodStart.Year - dtEOCDate.Year)) + 1)
            '    lblPayPeriodsEOC.Visible = True
            '    nudPayPeriodEOC.Visible = True
            '    lblEmplDate.Visible = True
            '    dtpEndEmplDate.Visible = True
            'Else
            '    lblPayPeriodsEOC.Visible = False
            '    nudPayPeriodEOC.Visible = False
            '    lblEmplDate.Visible = False
            '    dtpEndEmplDate.Visible = False
            'End If
            'objEmployee = Nothing
            'Hemant (07 July 2023) -- End


            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            TxtFifthTranche.Text = Format(CDec(objRoleBasedApproval._FifthTranche), Session("fmtCurrency").ToString)

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then
                'Pinkal (21-Mar-2024) -- Start
                'NMB - Mortgage UAT Enhancements.
                If CBool(Session("AllowToSetDisbursementTranches")) Then
                dtpFirstTranche.SetDate = Now.Date
                dtpFirstTranche.Enabled = False
            Else
                    dtpFirstTranche.SetDate = Nothing
                End If
                'Pinkal (21-Mar-2024) -- End
            Else
                dtpFirstTranche.SetDate = objRoleBasedApproval._FirstrancheDate.Date
            End If
            'Pinkal (16-Nov-2023) -- End
          
            dtpSecondTranche.SetDate = objRoleBasedApproval._SecondtrancheDate.Date
            dtpThirdTranche.SetDate = objRoleBasedApproval._ThirdtrancheDate.Date
            dtpFourthTranche.SetDate = objRoleBasedApproval._FourthtrancheDate.Date
            dtpFifthTranche.SetDate = objRoleBasedApproval._FifthtrancheDate.Date
            'Pinkal (04-Aug-2023) -- End

            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            txtFirstTranche_Remark.Text = objRoleBasedApproval._FirstTranche_Remark.Trim
            txtSecondTranche_Remark.Text = objRoleBasedApproval._SecondTranche_Remark.Trim
            txtThirdTranche_Remark.Text = objRoleBasedApproval._ThirdTranche_Remark.Trim
            txtFourthTranche_Remark.Text = objRoleBasedApproval._FourthTranche_Remark.Trim
            txtFifthTranche_Remark.Text = objRoleBasedApproval._FifthTranche_Remark.Trim
            'Pinkal (21-Mar-2024) -- End


            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            txtPrecedentRemarks.Text = objRoleBasedApproval._Precedent_Remark.Trim
            txtSubsequentRemarks.Text = objRoleBasedApproval._Subsequent_Remark.Trim
            'Pinkal (17-May-2024) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            chkSalaryAdvanceLiquidation.Checked = CBool(objLoanapplication._IsLiquidate)
            chkSalaryAdvanceLiquidation_CheckedChanged(chkSalaryAdvanceLiquidation, New EventArgs())
            'Hemant (22 Nov 2024) -- End
            'Hemant (22 Nov 2024) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Calculate_Projected_Loan_Principal(ByVal eLnCalcType As enLoanCalcId _
                                               , ByVal eIntRateCalcTypeID As enLoanInterestCalcType _
                                               , Optional ByVal blnIsDurationChange As Boolean = False _
                                               )
        Try
            Dim objLoan_Advance As New clsLoan_Advance
            Dim decPrincipalAmount As Decimal = 0
            Dim dtEndDate As Date = DateAdd(DateInterval.Month, CInt(txtEMIInstallments.Text), Now.Date)
            Dim dtFPeriodStart As Date = dtEndDate
            Dim dtFPeriodEnd As Date = dtEndDate
            If mintDeductionPeriodUnkId > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = mintDeductionPeriodUnkId
                dtFPeriodStart = objPeriod._Start_Date
                dtFPeriodEnd = objPeriod._End_Date
            End If
            objLoan_Advance.Calulate_Projected_Loan_Principal(CDec(txtMaxLoanAmount.Text) _
                                                            , CDec(txtMaxInstallmentAmt.Text) _
                                                            , CInt(DateDiff(DateInterval.Day, Now.Date, dtEndDate.Date)) _
                                                            , CDec(txtLoanRate.Text) _
                                                            , eLnCalcType _
                                                            , eIntRateCalcTypeID _
                                                            , CInt(txtMaxInstallment.Text) _
                                                            , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
                                                            , Convert.ToDecimal(txtInstallmentAmt.Text) _
                                                            , decInstrAmount _
                                                            , decPrincipalAmount _
                                                            , decTotIntrstAmount _
                                                            , decTotInstallmentAmount _
                                                            )

            'RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            'txtInstallmentAmt.Text = Format(decInstallmentAmount, Session("fmtCurrency").ToString)
            'AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            RemoveHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            txtAppliedUptoPrincipalAmt.Text = Format(decInstallmentAmount - decInstrAmount, Session("fmtCurrency").ToString)
            If decPrincipalAmount > CDec(txtMaxLoanAmount.Text) Then
                txtAppliedUptoPrincipalAmt.Text = Format(CDec(txtMaxLoanAmount.Text), Session("fmtCurrency").ToString)
            Else
                If Math.Abs(decPrincipalAmount - CDec(txtMaxLoanAmount.Text)) > 2 Then
                    txtAppliedUptoPrincipalAmt.Text = Format(decPrincipalAmount, Session("fmtCurrency").ToString)
                End If
            End If
            AddHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged

            RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            If decPrincipalAmount > CDec(txtMaxLoanAmount.Text) Then
                txtLoanAmt.Text = Format(CDec(txtMaxLoanAmount.Text), Session("fmtCurrency").ToString)
            Else
                If Math.Abs(decPrincipalAmount - CDec(txtMaxLoanAmount.Text)) > 2 Then
                    txtLoanAmt.Text = Format(decPrincipalAmount, Session("fmtCurrency").ToString)
                End If
            End If
            If Math.Abs(decPrincipalAmount - CDec(txtMaxLoanAmount.Text)) > 2 Then
                Call txtLoanAmt_TextChanged(txtLoanAmt, New System.EventArgs)
            End If
            AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged

            txtIntAmt.Text = Format(decInstrAmount, Session("fmtCurrency").ToString)

            objLoan_Advance = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Calculate_Projected_Loan_Balance(ByVal eLnCalcType As enLoanCalcId _
                                                 , ByVal eIntRateCalcTypeID As enLoanInterestCalcType _
                                                 , Optional ByVal blnIsDurationChange As Boolean = False _
                                                 )
        Try
            Dim objLoan_Advance As New clsLoan_Advance
            Dim dtEndDate As Date = DateAdd(DateInterval.Month, CInt(txtEMIInstallments.Text), Now.Date)
            Dim dtFPeriodStart As Date = dtEndDate
            Dim dtFPeriodEnd As Date = dtEndDate
            If mintDeductionPeriodUnkId > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = mintDeductionPeriodUnkId
                dtFPeriodStart = objPeriod._Start_Date
                dtFPeriodEnd = objPeriod._End_Date
            End If
            objLoan_Advance.Calulate_Projected_Loan_Balance(CDec(txtLoanAmt.Text) _
                                                            , CInt(DateDiff(DateInterval.Day, Now.Date, dtEndDate.Date)) _
                                                            , CDec(txtLoanRate.Text) _
                                                            , eLnCalcType _
                                                            , eIntRateCalcTypeID _
                                                            , CInt(txtEMIInstallments.Text) _
                                                            , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
                                                            , Convert.ToDecimal(txtInstallmentAmt.Text) _
                                                            , decInstrAmount _
                                                            , decInstallmentAmount _
                                                            , decTotIntrstAmount _
                                                            , decTotInstallmentAmount _
                                                            )

            RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            txtInstallmentAmt.Text = Format(decInstallmentAmount, Session("fmtCurrency").ToString)
            AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            txtIntAmt.Text = Format(decInstrAmount, Session("fmtCurrency").ToString)
            objLoan_Advance = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillDocumentTypeCombo()
        Dim objCommonMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Dim dt As New DataTable
        Dim strFilter As String = String.Empty
        Try
            dsCombos = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            If mstrDocumentTypeIDs.Trim.Length > 0 Then
                strFilter = "masterunkid in ( 0," & mstrDocumentTypeIDs & " )"
            Else
                strFilter = "masterunkid in ( 0 )"
            End If
            Dim drRow() As DataRow = dsCombos.Tables(0).Select(strFilter)
            If drRow.Length > 0 Then
                dt = drRow.CopyToDataTable
            End If
            'With cboDocumentType
            '    .DataValueField = "masterunkid"
            '    .DataTextField = "name"
            '    .DataSource = dt
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDocumentTypeCombo", mstrModuleName)
        Finally
            objCommonMaster = Nothing
        End Try
    End Sub

    Private Sub FillLoanApplicationAttachment()
        Try
            If mdtLoanApplicationDocument Is Nothing Then
                dgvAttachement.DataSource = Nothing
                dgvAttachement.DataBind()
                Exit Sub
            End If
            dgvAttachement.AutoGenerateColumns = False
            dgvAttachement.DataSource = mdtLoanApplicationDocument
            dgvAttachement.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (27-Oct-2022) -- Start
    'NMB Loan Module Enhancement.
    Private Sub FillLoanApplicationOtherAttachment()
        Dim dtView As DataView
        Try
            If mdtOtherDocument Is Nothing Then
                dgvOtherAttachement.DataSource = Nothing
                dgvOtherAttachement.DataBind()
                Exit Sub
            End If

            dtView = New DataView(mdtOtherDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvOtherAttachement.AutoGenerateColumns = False
            dgvOtherAttachement.DataSource = dtView
            dgvOtherAttachement.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (27-Oct-2022) -- End


    Private Function Validation(ByVal xStatusId As Integer) As Boolean
        Dim mblnFlag As Boolean = True
        Dim objEmployee As New clsEmployee_Master
        'Pinkal (23-Nov-2022) -- Start
        'NMB Loan Module Enhancement.
        Dim objLoanScheme As New clsLoan_Scheme
        'Pinkal (23-Nov-2022) -- End
        Try

            'Hemant (07 Jul 2023) -- Start
            'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
            objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            'Hemant (07 July 2023) -- End
            If CInt(cboEmpName.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), Me)
                cboEmpName.Focus()
                Return False
            End If

            If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), Me)
                cboLoanScheme.Focus()
                Return False
            End If

            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            If xStatusId = enLoanApplicationStatus.APPROVED AndAlso CInt(cboDeductionPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 40, "Deduction Period is compulsory information.Please Select Deduction Period to continue."), Me)
                cboDeductionPeriod.Focus()
                Return False
            End If
            'Pinkal (27-Oct-2022) -- End

            If CDec(txtLoanAmt.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Applied Amount cannot be blank. Applied Amount is compulsory information."), Me)
                txtLoanAmt.Focus()
                Return False
            End If

            If CInt(cboCurrency.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Currency is compulsory information.Please select currency."), Me)
                cboCurrency.Focus()
                Return False
            End If

            If CDec(txtInstallmentAmt.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Installment Amount cannot be 0.Please define installment amount greater than 0."), Me)
                txtInstallmentAmt.Focus()
                Return False
            End If

            If CDec(txtInstallmentAmt.Text) > CDec(txtLoanAmt.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Installment Amount cannot be greater than Loan Amount."), Me)
                txtInstallmentAmt.Focus()
                Return False
            End If

            If CInt(cboLoanCalcType.SelectedValue) = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI) Then
                Dim decValue As Decimal
                Decimal.TryParse(txtAppliedUptoPrincipalAmt.Text, decValue)
                If decValue <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Principal Amount cannot be 0. Please enter Principal amount."), Me)
                    txtAppliedUptoPrincipalAmt.Focus()
                    Return False
                End If
            End If  ' If CInt(cboLoanCalcType.SelectedValue) = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI) Then

            'Hemant (17 Mar 2023) -- Start
            'ISSUE/EHANCEMENT(NMB) : Please keep it for exceptional. Its causing us trouble in CRB
            Dim objLoanCategoryMapping As New clsLoanCategoryMapping
            Dim dsSchemeCategoryMappring As DataSet = objLoanCategoryMapping.GetList("List", CInt(cboLoanSchemeCategory.SelectedValue))
            If dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 AndAlso CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("identitytypeunkid")) > 0 Then
                Dim strIdentityTypesIds As String = String.Join(",", dsSchemeCategoryMappring.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("identitytypeunkid").ToString).ToArray())
                Dim objIdentityTran As New clsIdentity_tran
                objIdentityTran._EmployeeUnkid = CInt(cboEmpName.SelectedValue)
                Dim dtIdentityType As DataTable = objIdentityTran._DataList
                If dtIdentityType IsNot Nothing AndAlso dtIdentityType.Rows.Count > 0 Then
                    Dim drIdentityType() As DataRow = dtIdentityType.Select("idtypeunkid IN (" & strIdentityTypesIds & ")")
                    If drIdentityType.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Sorry, NIDA number is missing or in pending state."), Me)
                        objIdentityTran = Nothing
                        objLoanCategoryMapping = Nothing
                        Return False
                    End If
                End If
                objIdentityTran = Nothing
            End If
            objLoanCategoryMapping = Nothing
            'Hemant (17 Mar 2023) -- End 

            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then

                If txtPlotNo.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Plot No is compulsory information. Please enter Plot No to continue."), Me)
                    txtPlotNo.Focus()
                    Return False
                End If

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                'If txtBlockNo.Text.Trim.Length <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Block No is compulsory information. Please enter Block No to continue."), Me)
                '    txtBlockNo.Focus()
                '    Return False
                'End If
                'Pinkal (17-May-2024) -- End



                If txtStreet.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Street is compulsory information. Please enter Street to continue."), Me)
                    txtStreet.Focus()
                    Return False
                End If

                If txtTownCity.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Town/City is compulsory information. Please enter Town/City to continue."), Me)
                    txtTownCity.Focus()
                    Return False
                End If

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                If objLoanScheme._MarketValueMandatory AndAlso CDec(txtMarketValue.Text) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Market Value cannot be 0. Please enter Market Value."), Me)
                    txtMarketValue.Focus()
                    Return False
                End If
                'Pinkal (17-May-2024) -- End


                If txtCTNo.Text.Trim.Length <= 0 AndAlso objLoanScheme._Name.ToString.ToUpper.Contains("SEMI-FINISH MORTGAGE") = False Then
                    'Hemant (17 Jan 2025) -- [AndAlso objLoanScheme._Name.ToString.ToUpper.Contains("SEMI-FINISH MORTGAGE") = False]
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "CT No is compulsory information. Please enter CT No to continue."), Me)
                    txtCTNo.Focus()
                    Return False
                End If

                'Pinkal (21-Mar-2024) -- Start
                'NMB - Mortgage UAT Enhancements.
                'If txtLONo.Text.Trim.Length <= 0 Then
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "LO No is compulsory information. Please enter LO No to continue."), Me)
                '    txtLONo.Focus()
                '    Return False
                'End If
                'Pinkal (21-Mar-2024) -- End


                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                If objLoanScheme._FSVValueMandatory AndAlso CDec(txtFSV.Text) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "FSV cannot be 0.Please enter FSV."), Me)
                    txtFSV.Focus()
                    Return False
                End If

                If objLoanScheme._BOQMandatory AndAlso txtBOQ.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 107, "Bill of Quantity(BOQ) cannot be blank.Please enter Bill of Quantity(BOQ)."), Me)
                    txtBOQ.Focus()
                    Return False
                End If

                'If pnlPrecedentSubsequentRemark.Visible AndAlso CBool(Session("AllowToAddPrecedentSubsequentRemarks")) AndAlso xStatusId = enLoanApplicationStatus.APPROVED Then
                '    If txtPrecedentRemarks.Text.Trim.Length <= 0 Then
                '        txtPrecedentRemarks.Focus()
                '        Return False
                '    ElseIf txtSubsequentRemarks.Text.Trim.Length <= 0 Then
                '        txtSubsequentRemarks.Focus()
                '        Return False
                '    End If
                'End If

                'Pinkal (17-May-2024) -- End

                'Hemant (07 Jul 2023) -- Start
                'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
                If dtpTitleIssueDate.GetDate = Nothing Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 100, "Title Issue Date is compulsory information. Please enter Title Issue Date to continue."), Me)
                    dtpTitleIssueDate.Focus()
                    Return False
                End If

                If CInt(nudTitleValidity.Text) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 101, "Title Validity cannot be 0.Please enter Title Validity."), Me)
                    nudTitleValidity.Focus()
                    Return False
                End If

                If dtpTitleExpiryDate.GetDate = Nothing Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 102, "Title Expriry Date is compulsory information. Please enter Title Expriry Date to continue."), Me)
                    dtpTitleExpiryDate.Focus()
                    Return False
                End If
                'Hemant (07 July 2023) -- End
                'Hemant (07 Jul 2023) -- Start
                'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
                If dtpTitleExpiryDate.GetDate.Date < eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date.AddDays(CInt(objLoanScheme._NoOfDaysBefereTitleExpiry)) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 103, "Sorry, you can not apply for this Mortgage Loan: Reason, Title Expiry Date should not be less than Date :" & eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date.AddDays(CInt(objLoanScheme._NoOfDaysBefereTitleExpiry)).ToShortDateString), Me)
                    dtpTitleExpiryDate.Focus()
                    Return False
                End If
                'Hemant (07 July 2023) -- End

            End If  'If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then

            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.SECURED Then
                If txtModel.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Model is compulsory information. Please enter Model to continue."), Me)
                    txtModel.Focus()
                    Return False
                End If

                If txtYOM.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "YOM is compulsory information. Please enter YOM to continue."), Me)
                    txtYOM.Focus()
                    Return False
                End If

                'Pinkal (27-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                If txtChassisNo.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 41, "Chassis No is compulsory information. Please enter Chassis No to continue."), Me)
                    txtChassisNo.Focus()
                    Return False
                End If

                If txtSupplier.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 42, "Supplier is compulsory information. Please enter Supplier to continue."), Me)
                    txtSupplier.Focus()
                    Return False
                End If

                If txtColour.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 43, "Colour is compulsory information. Please enter Colour to continue."), Me)
                    txtColour.Focus()
                    Return False
                End If
                'Pinkal (27-Oct-2022) -- End

            End If  ' If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.SECURED Then

            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmpName.SelectedValue)

            If chkMakeExceptionalApplication.Checked = False AndAlso xStatusId = enLoanApplicationStatus.APPROVED Then

                'Hemant (07 Jul 2023) -- Start
                'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
                'objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                'Hemant (07 July 2023) -- End


                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.

                If objLoanScheme._MinNoOfInstallment > 0 AndAlso CInt(txtEMIInstallments.Text) < objLoanScheme._MinNoOfInstallment Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 46, "Installment months cannot be less than") & " " & objLoanScheme._MinNoOfInstallment & " " & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "for") & " " & objLoanScheme._Name & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, " Scheme."), Me)
                    txtEMIInstallments.Focus()                    
                    objLoanScheme = Nothing
                    Return False
                End If

                If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(txtEMIInstallments.Text) > objLoanScheme._MaxNoOfInstallment Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Installment months cannot be greater than") & " " & objLoanScheme._MaxNoOfInstallment & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "for") & " " & objLoanScheme._Name & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, " Scheme."), Me)
                    txtEMIInstallments.Focus()                    
                    objLoanScheme = Nothing
                    Return False
                End If
                'objLoanScheme = Nothing
                'Pinkal (23-Nov-2022) -- End

                If CDec(txtLoanAmt.Text) > CDec(txtMaxLoanAmount.Text) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Sorry, you can not apply for this loan scheme: Reason, Principal amount  is exceeding the Max Principal Amt") & _
                                                                   " [" & Format(CDec(txtMaxLoanAmount.Text), Session("fmtCurrency").ToString) & "] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, " set for selected scheme."), Me)
                    txtLoanAmt.Focus()
                    Return False
                End If

                If CDec(txtInstallmentAmt.Text) > CDec(txtMaxInstallmentAmt.Text) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Sorry, you can not apply for this loan scheme: Reason, Installment amount  is exceeding the Max Installment amount") & _
                                                                " [" & Format(CDec(txtMaxInstallmentAmt.Text), Session("fmtCurrency").ToString) & "] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, " set for selected scheme."), Me)
                    txtLoanAmt.Focus()
                    Return False
                End If

                If CDec(txtMinLoanAmount.Text) > CDec(txtLoanAmt.Text) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Sorry, you can not apply for this loan scheme: Reason, Principal amount  should be greater than the Minimum Principal amount") & _
                                                            " [" & Format(CDec(txtMinLoanAmount.Text), GUI.fmtCurrency) & "] " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, " set for selected scheme."), Me)
                    txtLoanAmt.Focus()
                    Return False
                End If

                'Pinkal (12-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                If mblnEligibleForTopUp = False AndAlso (CDec(TxtOutStandingPrinciple.Text) > 0 OrElse CDec(TxtOutstandingInterest.Text) > 0) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 37, "Sorry, you can not apply for Topup for this loan scheme: Reason, TopUp is not allowed") & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 38, "set for selected scheme."), Me)
                    Return False
                End If

                If mblnPostingToFlexcube = True AndAlso CDec(TxtPaidInstallments.Text) > 0 AndAlso CDec(TxtPaidInstallments.Text) < mintInstallmentPaidForFlexcube Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 39, "Sorry, you can not apply for Topup for this loan scheme: Reason, No of Installment Paid should be greater than minimum No of Installment Paid") & " [" & objLoanScheme._MinOfInstallmentPaid & "] " & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 38, "set for selected scheme."), Me)
                    Return False
                End If
                'Pinkal (12-Oct-2022) -- End

                If objLoanScheme._IsDisableEmployeeConfirm = False AndAlso objEmployee._Confirmation_Date >= eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) Then
                    'Hemant (22 Nov 2024) -- [objLoanScheme._IsDisableEmployeeConfirm = False]
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 25, "Sorry, you are not confirmed yet. Please contact HR support."), Me)
                    Return False
                End If

                If mstrCompanyGroupName = "NMB PLC" AndAlso objEmployee._EmpSignature Is Nothing Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Sorry, signature missing. Please update signature from your profile."), Me)
                    Return False
                End If

                Dim objEmployeeDates As New clsemployee_dates_tran
                Dim dsData As DataSet = objEmployeeDates.GetList("List", enEmp_Dates_Transaction.DT_TERMINATION, _
                                                                                        CDate(Session("fin_startdate").ToString), _
                                                                                         CInt(cboEmpName.SelectedValue))

                If dsData IsNot Nothing AndAlso dsData.Tables("List").Rows.Count > 0 Then
                    If DateDiff(DateInterval.Day, CDate(dsData.Tables("List").Rows(0).Item("effectivedate")), CDate(dsData.Tables("List").Rows(0).Item("date1"))) <= 180 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "Sorry, your contract duration is less than 6 months. Please contact HR support."), Me)
                        objEmployeeDates = Nothing
                        Return False
                    End If
                End If
                objEmployeeDates = Nothing

                'Hemant (17 Mar 2023) -- Start
                'ISSUE/EHANCEMENT(NMB) : Please keep it for exceptional. Its causing us trouble in CRB
                'Dim objLoanCategoryMapping As New clsLoanCategoryMapping
                'Dim dsSchemeCategoryMappring As DataSet = objLoanCategoryMapping.GetList("List", CInt(cboLoanSchemeCategory.SelectedValue))
                'If dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 AndAlso CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("identitytypeunkid")) > 0 Then
                '    Dim objIdentityTran As New clsIdentity_tran
                '    objIdentityTran._EmployeeUnkid = CInt(cboEmpName.SelectedValue)
                '    Dim dtIdentityType As DataTable = objIdentityTran._DataList
                '    If dtIdentityType IsNot Nothing AndAlso dtIdentityType.Rows.Count > 0 Then
                '        Dim drIdentityType() As DataRow = dtIdentityType.Select("idtypeunkid=" & CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("identitytypeunkid")))
                '        If drIdentityType.Length <= 0 Then
                '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Sorry, NIDA number is missing or in pending state."), Me)
                '            objIdentityTran = Nothing
                '            objLoanCategoryMapping = Nothing
                '            Return False
                '        End If
                '    End If
                '    objIdentityTran = Nothing
                'End If
                'objLoanCategoryMapping = Nothing
                'Hemant (17 Mar 2023) -- End 


                Dim objDiscFileMaster As New clsDiscipline_file_master
                Dim mintUserID As Integer
                If CInt(Session("UserId")) <= 0 Then
                    Dim objUser As New clsUserAddEdit
                    mintUserID = objUser.Return_UserId(CInt(cboEmpName.SelectedValue), CInt(Session("CompanyUnkId")))
                    objUser = Nothing
                Else
                    mintUserID = CInt(Session("UserId"))
                End If


                If dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 AndAlso CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("chargestatusunkid")) > 0 Then
                    Dim dsDisplinaryFile As New DataSet
                    dsDisplinaryFile = objDiscFileMaster.GetList(Session("Database_Name").ToString, mintUserID, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", , " hrdiscipline_file_master.involved_employeeunkid = '" & CInt(cboEmpName.SelectedValue) & "' AND A.statusid = " & CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("chargestatusunkid")) & " ")
                    'Pinkal (10-Nov-2022) -- Start
                    'NMB Loan Module Enhancement.
                    If dsDisplinaryFile IsNot Nothing AndAlso dsDisplinaryFile.Tables(0).Rows.Count > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Sorry, you have a pending disciplinary issue. Kindly contact HR ER."), Me)
                        objDiscFileMaster = Nothing
                        Return False
                    End If
                    'Pinkal (10-Nov-2022) -- End
                End If
                objDiscFileMaster = Nothing

                Dim objProceedingMaster As New clsdiscipline_proceeding_master
                Dim dsProceedingMaster As New DataSet
                dsProceedingMaster = objProceedingMaster.GetList(Session("Database_Name").ToString, mintUserID, CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("UserAccessModeSetting")), _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), "List", " hrdiscipline_file_master.involved_employeeunkid = " & CInt(cboEmpName.SelectedValue) & "", CBool(Session("AddProceedingAgainstEachCount")))

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                If dsProceedingMaster IsNot Nothing AndAlso dsProceedingMaster.Tables(0).Rows.Count > 0 Then
                    Dim drProceedingMaster() As DataRow = dsProceedingMaster.Tables(0).Select("penalty_expirydate >= '" & ConfigParameter._Object._CurrentDateAndTime.Date & "' ")
                    If drProceedingMaster.Length > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Sorry, you have a pending disciplinary issue. Kindly contact HR ER."), Me)
                        objProceedingMaster = Nothing
                        Return False
                    End If
                End If
                'Pinkal (23-Nov-2022) -- End
                objProceedingMaster = Nothing


                Dim intNoOfInstallment As Integer
                Integer.TryParse(txtEMIInstallments.Text, intNoOfInstallment)
                If intNoOfInstallment > 1 Then

                    If IsDBNull(objEmployee._Empl_Enddate) = False AndAlso objEmployee._Empl_Enddate <> Nothing Then
                        Dim dtLoanEndDate As Date = mdtPeriodStart.AddMonths(intNoOfInstallment).AddDays(-1)
                        'Hemant (27 Oct 2023) -- Start
                        'ISSUE(NMB): Showing Message "Sorry, installments cannot go beyond the end of contract date" While Approving Flexcube Loan
                        'Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, mdtPeriodStart, objEmployee._Empl_Enddate.Date.AddDays(-1)))
                        Dim intEmpTenure As Integer = Math.Abs((mdtPeriodStart.Month - objEmployee._Empl_Enddate.Date.Month) + 12 * (mdtPeriodStart.Year - objEmployee._Empl_Enddate.Date.Year)) + 1
                        'Hemant (27 Oct 2023) -- End
                        If intNoOfInstallment > intEmpTenure Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 30, "Sorry, installments cannot go beyond the end of contract date."), Me)
                            If txtEMIInstallments.Enabled = True Then txtEMIInstallments.Focus()
                            Return False
                        End If
                    End If


                    If IsDBNull(objEmployee._Termination_To_Date) = False AndAlso objEmployee._Termination_To_Date <> Nothing Then
                        Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, mdtPeriodStart, objEmployee._Termination_To_Date.Date.AddMonths(-3).AddDays(1)))
                        If intNoOfInstallment > intEmpTenure Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, "Sorry, no of installments selected cannot go beyond the retirement date."), Me)
                            If txtEMIInstallments.Enabled = True Then txtEMIInstallments.Focus()
                            Return False
                        End If
                    End If
                End If

            End If   'If chkMakeExceptionalApplication.Checked = False Then

            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            If (xStatusId = enLoanApplicationStatus.REJECTED OrElse xStatusId = enLoanApplicationStatus.RETURNTOAPPLICANT OrElse xStatusId = enLoanApplicationStatus.RETURNTOPREVIOUSAPPROVER) AndAlso TxtApproverRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 32, "Remark cannot be blank.Remark is compulsory information."), Me)
                TxtApproverRemark.Focus()
                Return False
            End If

            If xStatusId <> enLoanApplicationStatus.RETURNTOAPPLICANT AndAlso xStatusId <> enLoanApplicationStatus.RETURNTOPREVIOUSAPPROVER Then
            If CBool(Session("AllowToGetCRBData")) Then
                Dim xCount As Integer = 0
                If mdtCRBDocument IsNot Nothing Then
                    xCount = mdtCRBDocument.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Count
                End If

                If (mdtFinalCRBData Is Nothing OrElse mdtFinalCRBData.Rows.Count <= 0) AndAlso (mdtCRBDocument Is Nothing OrElse xCount <= 0) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 47, "Please get atleast one identity data from CRB system or attach CRB Report."), Me)
                    Return False
                End If
            End If
            End If
            'Pinkal (21-Mar-2024) -- End


            'Pinkal (14-Dec-2022) -- Start
            'NMB Loan Module Enhancement.


            'Hemant (17 Jan 2025) -- Start
            'ISSUE/ENHANCEMENT(NMB): A1X - 2968 :  Semi finished mortgage loan changes
            If objLoanScheme._Name.ToString.ToUpper.Contains("SEMI-FINISH MORTGAGE") = False Then
                'Hemant (17 Jan 2025) -- End
            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            If xStatusId = enLoanApplicationStatus.APPROVED AndAlso CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE AndAlso (objLoanScheme._FSVValueMandatory OrElse CDec(IIf(txtFSV.Text.Trim.Length <= 0, 0, txtFSV.Text.Trim)) > 0) _
                AndAlso (objLoanScheme._MarketValueMandatory OrElse CDec(IIf(txtMarketValue.Text.Trim.Length <= 0, 0, txtMarketValue.Text.Trim)) > 0) AndAlso CDec(IIf(txtFSV.Text.Trim.Length <= 0, 0, txtFSV.Text.Trim)) > CDec(IIf(txtMarketValue.Text.Trim.Length <= 0, 0, txtMarketValue.Text.Trim)) Then

                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 108, "FSV cannot be more than the Market Value.Please enter proper FSV value."), Me)
                txtFSV.Focus()
                Return False
            ElseIf xStatusId = enLoanApplicationStatus.APPROVED AndAlso CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE AndAlso (objLoanScheme._FSVValueMandatory OrElse CDec(IIf(txtFSV.Text.Trim.Length <= 0, 0, txtFSV.Text.Trim)) > 0) _
                    AndAlso CDec(IIf(txtLoanAmt.Text.Trim.Length <= 0, 0, txtLoanAmt.Text.Trim)) > CDec(IIf(txtFSV.Text.Trim.Length <= 0, 0, txtFSV.Text.Trim)) Then

                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 89, "Principal amount cannot be greather than the FSV Value."), Me)
                txtFSV.Focus()
                Return False
            End If

            'Pinkal (17-May-2024) -- End
            End If 'Hemant (17 Jan 2025)

            'Pinkal (28-Apr-2023) -- Start
            'Resolve the Issue for NMB as User wants to reject the application but system didn't allow it.
            If xStatusId = enLoanApplicationStatus.APPROVED AndAlso CDec(IIf(txtTakeHome.Text.Trim.Length <= 0, 0, txtTakeHome.Text.Trim)) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 90, "Sorry, you do not qualify for a top-up at the moment. The take home amount should be greater than 0."), Me)
                txtTakeHome.Focus()
                Return False
            End If
            'Pinkal (28-Apr-2023) -- End

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.

            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE AndAlso xStatusId = enLoanApplicationStatus.APPROVED AndAlso pnlDisbursementfunds.Visible AndAlso CBool(Session("AllowToSetDisbursementTranches")) Then
                If CDec(IIf(TxtFirstTranche.Text.Trim.Length <= 0, 0, TxtFirstTranche.Text.Trim)) <= 0 AndAlso CDec(IIf(TxtSecondTranche.Text.Trim.Length <= 0, 0, TxtSecondTranche.Text.Trim)) <= 0 AndAlso _
                   CDec(IIf(TxtThirdTranche.Text.Trim.Length <= 0, 0, TxtThirdTranche.Text.Trim)) <= 0 AndAlso CDec(IIf(TxtFourthTranche.Text.Trim.Length <= 0, 0, TxtFourthTranche.Text.Trim)) <= 0 AndAlso _
                   CDec(IIf(TxtFifthTranche.Text.Trim.Length <= 0, 0, TxtFifthTranche.Text.Trim)) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 85, "Please enter any one tranche to complete the disbursement funds."), Me)
                    TxtFirstTranche.Focus()
                    Return False
                End If

                If CDec(IIf(TxtFirstTranche.Text.Trim.Length <= 0, 0, TxtFirstTranche.Text.Trim)) > 0 AndAlso dtpFirstTranche.IsNull Then
                    DisplayMessage.DisplayMessage(LblFirstTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "is compulsory information.Please set") & " " & LblFirstTrancheDate.Text & ".", Me)
                    dtpFirstTranche.Focus()
                    Return False

                ElseIf CDec(IIf(TxtFirstTranche.Text.Trim.Length <= 0, 0, TxtFirstTranche.Text.Trim)) <= 0 AndAlso dtpFirstTranche.IsNull = False Then
                    DisplayMessage.DisplayMessage(LblFirstTranche.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "is compulsory information.Please set") & " " & LblFirstTranche.Text & ".", Me)
                    TxtFirstTranche.Focus()
                    Return False

                ElseIf CDec(IIf(TxtSecondTranche.Text.Trim.Length <= 0, 0, TxtSecondTranche.Text.Trim)) > 0 AndAlso dtpSecondTranche.IsNull Then
                    DisplayMessage.DisplayMessage(LblSecondTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "is compulsory information.Please set") & " " & LblSecondTrancheDate.Text & ".", Me)
                    dtpSecondTranche.Focus()
                    Return False

                ElseIf CDec(IIf(TxtSecondTranche.Text.Trim.Length <= 0, 0, TxtSecondTranche.Text.Trim)) <= 0 AndAlso dtpSecondTranche.IsNull = False Then
                    DisplayMessage.DisplayMessage(LblSecondTranche.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "is compulsory information.Please set") & " " & LblSecondTranche.Text & ".", Me)
                    TxtSecondTranche.Focus()
                    Return False

                ElseIf CDec(IIf(TxtThirdTranche.Text.Trim.Length <= 0, 0, TxtThirdTranche.Text.Trim)) > 0 AndAlso dtpThirdTranche.IsNull Then
                    DisplayMessage.DisplayMessage(LblThirdTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "is compulsory information.Please set") & " " & LblThirdTrancheDate.Text & ".", Me)
                    dtpThirdTranche.Focus()
                    Return False

                ElseIf CDec(IIf(TxtThirdTranche.Text.Trim.Length <= 0, 0, TxtThirdTranche.Text.Trim)) <= 0 AndAlso dtpThirdTranche.IsNull = False Then
                    DisplayMessage.DisplayMessage(LblThirdTranche.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "is compulsory information.Please set") & " " & LblThirdTranche.Text & ".", Me)
                    TxtThirdTranche.Focus()
                    Return False

                ElseIf CDec(IIf(TxtFourthTranche.Text.Trim.Length <= 0, 0, TxtFourthTranche.Text.Trim)) > 0 AndAlso dtpFourthTranche.IsNull Then
                    DisplayMessage.DisplayMessage(LblFourthTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "is compulsory information.Please set") & " " & LblFourthTrancheDate.Text & ".", Me)
                    dtpFourthTranche.Focus()
                    Return False

                ElseIf CDec(IIf(TxtFourthTranche.Text.Trim.Length <= 0, 0, TxtFourthTranche.Text.Trim)) <= 0 AndAlso dtpFourthTranche.IsNull = False Then
                    DisplayMessage.DisplayMessage(LblFourthTranche.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "is compulsory information.Please set") & " " & LblFourthTranche.Text & ".", Me)
                    TxtFourthTranche.Focus()
                    Return False

                ElseIf CDec(IIf(TxtFifthTranche.Text.Trim.Length <= 0, 0, TxtFifthTranche.Text.Trim)) > 0 AndAlso dtpFifthTranche.IsNull Then
                    DisplayMessage.DisplayMessage(LblFifthTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "is compulsory information.Please set") & " " & LblFifthTrancheDate.Text & ".", Me)
                    dtpFifthTranche.Focus()
                    Return False

                ElseIf CDec(IIf(TxtFifthTranche.Text.Trim.Length <= 0, 0, TxtFifthTranche.Text.Trim)) <= 0 AndAlso dtpFifthTranche.IsNull = False Then
                    DisplayMessage.DisplayMessage(LblFifthTranche.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 104, "is compulsory information.Please set") & " " & LblFifthTranche.Text & ".", Me)
                    TxtFifthTranche.Focus()
                    Return False

                ElseIf LoanTrancheDateValidation() = False Then
                    Return False
                End If

            End If

            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE AndAlso xStatusId = enLoanApplicationStatus.APPROVED AndAlso pnlDisbursementfunds.Visible AndAlso (CDec(txtLoanAmt.Text) < CDec(IIf(TxtFirstTranche.Text.Trim.Length <= 0, 0, TxtFirstTranche.Text.Trim)) + CDec(IIf(TxtSecondTranche.Text.Trim.Length <= 0, 0, TxtSecondTranche.Text.Trim)) + _
              CDec(IIf(TxtThirdTranche.Text.Trim.Length <= 0, 0, TxtThirdTranche.Text.Trim)) + CDec(IIf(TxtFourthTranche.Text.Trim.Length <= 0, 0, TxtFourthTranche.Text.Trim)) + CDec(IIf(TxtFifthTranche.Text.Trim.Length <= 0, 0, TxtFifthTranche.Text.Trim))) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 86, "Sum of All Disbursement tranches should not be greater than principal amount."), Me)
                TxtFirstTranche.Focus()
                Return False
            ElseIf CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE AndAlso xStatusId = enLoanApplicationStatus.APPROVED AndAlso pnlDisbursementfunds.Visible AndAlso (CDec(txtLoanAmt.Text) > CDec(IIf(TxtFirstTranche.Text.Trim.Length <= 0, 0, TxtFirstTranche.Text.Trim)) + CDec(IIf(TxtSecondTranche.Text.Trim.Length <= 0, 0, TxtSecondTranche.Text.Trim)) + _
                  CDec(IIf(TxtThirdTranche.Text.Trim.Length <= 0, 0, TxtThirdTranche.Text.Trim)) + CDec(IIf(TxtFourthTranche.Text.Trim.Length <= 0, 0, TxtFourthTranche.Text.Trim)) + CDec(IIf(TxtFifthTranche.Text.Trim.Length <= 0, 0, TxtFifthTranche.Text.Trim))) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 91, "Sum of All Disbursement tranches must be equal to principal amount."), Me)
                TxtFirstTranche.Focus()
                Return False
            End If
            'Pinkal (04-Aug-2023) -- End

            'Hemant (10 May 2024) -- Start
            'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            If xStatusId = enLoanApplicationStatus.APPROVED AndAlso CBool(Session("AllowToAttachDocumentOnRolebaseLoanApproval")) AndAlso mdtApproverDocument.Select("transactionunkid = " & mintPendingLoanAprovalunkid & " AND AUD <> 'D' ").Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 106, "Approver Scan/Document(s) is mandatory information.Please attach Approver Scan/Document(s) in order to perform operation. "), Me)
                Return False
            End If
            'Pinkal (17-May-2024) -- End

            'Hemant (10 May 2024) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            mblnFlag = False
        Finally
            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objLoanScheme = Nothing
            'Pinkal (23-Nov-2022) -- End
            objEmployee = Nothing
        End Try
        Return mblnFlag
    End Function


    'Pinkal (04-Aug-2023) -- Start
    '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
    Private Function LoanTrancheDateValidation() As Boolean
        Dim mblnFlag As Boolean = True
        Try
            If (dtpFirstTranche.IsNull = False AndAlso dtpSecondTranche.IsNull = False) AndAlso (dtpFirstTranche.GetDate.Date >= dtpSecondTranche.GetDate.Date) Then
                DisplayMessage.DisplayMessage(LblFirstTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 105, "should be less than") & " " & LblSecondTrancheDate.Text & ".", Me)
                mblnFlag = False

            ElseIf (dtpFirstTranche.IsNull = False AndAlso dtpThirdTranche.IsNull = False) AndAlso (dtpFirstTranche.GetDate.Date >= dtpThirdTranche.GetDate.Date) Then
                DisplayMessage.DisplayMessage(LblFirstTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 105, "should be less than") & " " & LblThirdTrancheDate.Text & ".", Me)
                mblnFlag = False

            ElseIf (dtpFirstTranche.IsNull = False AndAlso dtpFourthTranche.IsNull = False) AndAlso (dtpFirstTranche.GetDate.Date >= dtpFourthTranche.GetDate.Date) Then
                DisplayMessage.DisplayMessage(LblFirstTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 105, "should be less than") & " " & LblFourthTrancheDate.Text & ".", Me)
                mblnFlag = False

            ElseIf (dtpFirstTranche.IsNull = False AndAlso dtpFifthTranche.IsNull = False) AndAlso (dtpFirstTranche.GetDate.Date >= dtpFifthTranche.GetDate.Date) Then
                DisplayMessage.DisplayMessage(LblFirstTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 105, "should be less than") & " " & LblFifthTrancheDate.Text & ".", Me)
                mblnFlag = False

            ElseIf (dtpSecondTranche.IsNull = False AndAlso dtpThirdTranche.IsNull = False) AndAlso (dtpSecondTranche.GetDate.Date >= dtpThirdTranche.GetDate.Date) Then
                DisplayMessage.DisplayMessage(LblSecondTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 105, "should be less than") & " " & LblThirdTrancheDate.Text & ".", Me)
                mblnFlag = False

            ElseIf (dtpSecondTranche.IsNull = False AndAlso dtpFourthTranche.IsNull = False) AndAlso (dtpSecondTranche.GetDate.Date >= dtpFourthTranche.GetDate.Date) Then
                DisplayMessage.DisplayMessage(LblSecondTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 105, "should be less than") & " " & LblFourthTrancheDate.Text & ".", Me)
                mblnFlag = False

            ElseIf (dtpSecondTranche.IsNull = False AndAlso dtpFifthTranche.IsNull = False) AndAlso (dtpSecondTranche.GetDate.Date >= dtpFifthTranche.GetDate.Date) Then
                DisplayMessage.DisplayMessage(LblSecondTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 105, "should be less than") & " " & LblFifthTrancheDate.Text & ".", Me)
                mblnFlag = False

            ElseIf (dtpThirdTranche.IsNull = False AndAlso dtpFourthTranche.IsNull = False) AndAlso (dtpThirdTranche.GetDate.Date >= dtpFourthTranche.GetDate.Date) Then
                DisplayMessage.DisplayMessage(LblThirdTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 105, "should be less than") & " " & LblFourthTrancheDate.Text & ".", Me)
                mblnFlag = False

            ElseIf (dtpThirdTranche.IsNull = False AndAlso dtpFifthTranche.IsNull = False) AndAlso (dtpThirdTranche.GetDate.Date >= dtpFifthTranche.GetDate.Date) Then
                DisplayMessage.DisplayMessage(LblThirdTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 105, "should be less than") & " " & LblFifthTrancheDate.Text & ".", Me)
                mblnFlag = False

            ElseIf (dtpFourthTranche.IsNull = False AndAlso dtpFifthTranche.IsNull = False) AndAlso (dtpFourthTranche.GetDate.Date >= dtpFifthTranche.GetDate.Date) Then
                DisplayMessage.DisplayMessage(LblFourthTrancheDate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 105, "should be less than") & " " & LblFifthTrancheDate.Text & ".", Me)
                mblnFlag = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            mblnFlag = False
        End Try
        Return mblnFlag
    End Function
    'Pinkal (04-Aug-2023) -- End


    Private Sub SaveApproval(ByVal xStatusId As Integer)
        Dim objRoleBasedApproval As New clsroleloanapproval_process_Tran
        'Pinkal (14-Dec-2022) -- Start
        'NMB Loan Module Enhancement.
        Dim mblnFinalApproval As Boolean = False
        'Pinkal (14-Dec-2022) -- End

        Try
            objRoleBasedApproval._Pendingloanaprovalunkid = mintPendingLoanAprovalunkid
            objRoleBasedApproval._Processpendingloanunkid = mintProcesspendingloanunkid
            objRoleBasedApproval._Employeeunkid = CInt(cboEmpName.SelectedValue)
            objRoleBasedApproval._Approvaldate = ConfigParameter._Object._CurrentDateAndTime
            objRoleBasedApproval._Deductionperiodunkid = CInt(cboDeductionPeriod.SelectedValue)
            objRoleBasedApproval._Statusunkid = xStatusId
            objRoleBasedApproval._Visibleunkid = xStatusId
            objRoleBasedApproval._Remark = TxtApproverRemark.Text.Trim
            objRoleBasedApproval._Userunkid = CInt(Session("UserId"))
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRoleBasedApproval._AuditUserId = CInt(Session("UserId"))
            End If
            objRoleBasedApproval._ClientIP = CStr(Session("IP_ADD"))
            objRoleBasedApproval._HostName = CStr(Session("HOST_NAME"))
            objRoleBasedApproval._FormName = mstrModuleName
            objRoleBasedApproval._IsFromWeb = True

            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            objRoleBasedApproval._OutStanding_PrincipalAmt = CDec(IIf(TxtOutStandingPrinciple.Text.Trim.Length <= 0, 0, TxtOutStandingPrinciple.Text.Trim))
            objRoleBasedApproval._OutStanding_InterestAmt = CDec(IIf(TxtOutstandingInterest.Text.Trim.Length <= 0, 0, TxtOutstandingInterest.Text.Trim))
            objRoleBasedApproval._PaidInstallments = CInt(IIf(TxtPaidInstallments.Text.Trim.Length <= 0, 0, TxtPaidInstallments.Text.Trim))
            If CDec(TxtOutStandingPrinciple.Text) > 0 OrElse CDec(TxtOutstandingInterest.Text) > 0 Then
                objRoleBasedApproval._IsTopup = True
            Else
                objRoleBasedApproval._IsTopup = False
            End If
            objRoleBasedApproval._TakeHomeAmount = CDec(IIf(txtTakeHome.Text.Trim.Length <= 0, 0, txtTakeHome.Text.Trim))
            'Pinkal (12-Oct-2022) -- End


            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            objRoleBasedApproval._RequiredReportingToApproval = mblnRequiredReportingToApproval
            If xStatusId = enLoanApplicationStatus.RETURNTOAPPLICANT Then
                objRoleBasedApproval._Voiduserunkid = CInt(Session("UserId"))
                objRoleBasedApproval._Voidreason = TxtApproverRemark.Text.Trim
            End If
            'Pinkal (27-Oct-2022) -- End

            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objRoleBasedApproval._IsApprovalDailyReminder = mblnLoanApproval_DailyReminder
            objRoleBasedApproval._EscalationDays = mintEscalationDays
            'Pinkal (10-Nov-2022) -- End

            'Hemant (25 Nov 2022) -- Start
            'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
            Dim intFinalApproverCategorizationTranunkid, intCentralizedApproverCategorizationTranunkid As Integer
            Dim intFinalApproverTransferunkid, intCentralizedApproverTransferunkid As Integer
            Dim intFinalApproverAtEmployeeunkid, intCentralizedApproverAtEmployeeunkid As Integer
            Dim objUser As New clsUserAddEdit
            objUser._Userunkid = CInt(Session("UserId"))
            GetEmployeeHistoryData(CInt(objUser._EmployeeUnkid), intFinalApproverCategorizationTranunkid, intFinalApproverTransferunkid, intFinalApproverAtEmployeeunkid)
            GetEmployeeHistoryData(CInt(Session("LoanCentralizedApproverId")), intCentralizedApproverCategorizationTranunkid, intCentralizedApproverTransferunkid, intCentralizedApproverAtEmployeeunkid)

            objRoleBasedApproval._FinalApproverCategorizationTranunkid = intFinalApproverCategorizationTranunkid
            objRoleBasedApproval._FinalApproverTransferunkid = intFinalApproverTransferunkid
            objRoleBasedApproval._FinalApproverAtEmployeeunkid = intFinalApproverAtEmployeeunkid

            objRoleBasedApproval._CentralizedCategorizationTranunkid = intCentralizedApproverCategorizationTranunkid
            objRoleBasedApproval._CentralizedTransferunkid = intCentralizedApproverTransferunkid
            objRoleBasedApproval._CentralizedAtEmployeeunkid = intCentralizedApproverAtEmployeeunkid
           
            objUser = Nothing
            'Hemant (25 Nov 2022) -- End


            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objRoleBasedApproval._LoanSchemeCode = mstrLoanSchemeCode
            objRoleBasedApproval._LoanScheme = cboLoanScheme.SelectedItem.Text
            objRoleBasedApproval._CRBData = mdtFinalCRBData
            objRoleBasedApproval._CRBAttachment = mdtCRBDocument
            'Hemant (10 May 2024) -- Start
            'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
            objRoleBasedApproval._ApproverAttachment = mdtApproverDocument
            'Hemant (10 May 2024) -- End


            Dim mdsDoc As DataSet
            Dim mstrFolderName As String = ""
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim strFileName As String = ""
            For Each dRowAttachment As DataRow In mdtCRBDocument.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRowAttachment("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault

                If dRowAttachment("AUD").ToString = "A" AndAlso dRowAttachment("localpath").ToString <> "" Then
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRowAttachment("localpath")))
                    If File.Exists(CStr(dRowAttachment("localpath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(dRowAttachment("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRowAttachment("fileuniquename") = strFileName
                        dRowAttachment("filepath") = strPath
                        dRowAttachment.AcceptChanges()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 48, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If
                ElseIf dRowAttachment("AUD").ToString = "D" AndAlso dRowAttachment("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRowAttachment("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 48, "File does not exist on localpath"), Me)
                            Exit Sub
                        End If
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 48, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If
                End If

            Next
            'Pinkal (23-Nov-2022) -- End

            'Hemant (10 May 2024) -- Start
            'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
            strFileName = ""
            For Each drApproverAttachment As DataRow In mdtApproverDocument.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(drApproverAttachment("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault

                If drApproverAttachment("AUD").ToString = "A" AndAlso drApproverAttachment("localpath").ToString <> "" Then
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(drApproverAttachment("localpath")))
                    If File.Exists(CStr(drApproverAttachment("localpath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(drApproverAttachment("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        drApproverAttachment("fileuniquename") = strFileName
                        drApproverAttachment("filepath") = strPath
                        drApproverAttachment.AcceptChanges()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 48, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If
                ElseIf drApproverAttachment("AUD").ToString = "D" AndAlso drApproverAttachment("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = drApproverAttachment("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 48, "File does not exist on localpath"), Me)
                            Exit Sub
                        End If
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 48, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If
                End If

            Next
            'Hemant (10 May 2024) -- End


            'Pinkal (14-Dec-2022) -- Start
            'NMB Loan Module Enhancement.
            objRoleBasedApproval._FirstTranche = CDec(IIf(TxtFirstTranche.Text.Trim.Length <= 0, 0, TxtFirstTranche.Text.Trim))
            objRoleBasedApproval._SecondTranche = CDec(IIf(TxtSecondTranche.Text.Trim.Length <= 0, 0, TxtSecondTranche.Text.Trim))
            objRoleBasedApproval._ThirdTranche = CDec(IIf(TxtThirdTranche.Text.Trim.Length <= 0, 0, TxtThirdTranche.Text.Trim))
            objRoleBasedApproval._FourTranche = CDec(IIf(TxtFourthTranche.Text.Trim.Length <= 0, 0, TxtFourthTranche.Text.Trim))
            'Pinkal (14-Dec-2022) -- End


            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objRoleBasedApproval._FifthTranche = CDec(IIf(TxtFifthTranche.Text.Trim.Length <= 0, 0, TxtFifthTranche.Text.Trim))

            If dtpFirstTranche.IsNull = False Then
                objRoleBasedApproval._FirstrancheDate = dtpFirstTranche.GetDate.Date
            End If

            If dtpSecondTranche.IsNull = False Then
                objRoleBasedApproval._SecondtrancheDate = dtpSecondTranche.GetDate.Date
            End If

            If dtpThirdTranche.IsNull = False Then
                objRoleBasedApproval._ThirdtrancheDate = dtpThirdTranche.GetDate.Date
            End If

            If dtpFourthTranche.IsNull = False Then
                objRoleBasedApproval._FourthtrancheDate = dtpFourthTranche.GetDate.Date
            End If

            If dtpFifthTranche.IsNull = False Then
                objRoleBasedApproval._FifthtrancheDate = dtpFifthTranche.GetDate.Date
            End If
            'Pinkal (04-Aug-2023) -- End

            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            objRoleBasedApproval._FirstTranche_Remark = txtFirstTranche_Remark.Text.Trim
            objRoleBasedApproval._SecondTranche_Remark = txtSecondTranche_Remark.Text.Trim
            objRoleBasedApproval._ThirdTranche_Remark = txtThirdTranche_Remark.Text.Trim
            objRoleBasedApproval._FourthTranche_Remark = txtFourthTranche_Remark.Text.Trim
            objRoleBasedApproval._FifthTranche_Remark = txtFifthTranche_Remark.Text.Trim
            'Pinkal (21-Mar-2024) -- End


            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            objRoleBasedApproval._CreditCardAmountOnExposure = mdecCreditCardAmountOnExposure
            'Pinkal (21-Mar-2024) -- End


            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            objRoleBasedApproval._Precedent_Remark = txtPrecedentRemarks.Text.Trim
            objRoleBasedApproval._Subsequent_Remark = txtSubsequentRemarks.Text.Trim
            'Pinkal (17-May-2024) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            objRoleBasedApproval._SalaryAdvancePrincipalAmount = mdecFinalSalaryAdvancePendingPrincipalAmt
            'Hemant (22 Nov 2024) -- End



            Dim blnFlag As Boolean = False
            If mintPendingLoanAprovalunkid > 0 Then
                'Pinkal (27-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                'blnFlag = objRoleBasedApproval.Update(Session("Database_Name").ToString, CInt(Session("UserId")) _
                '                                                          , CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                '                                                          , mdtPeriodStart, mdtPeriodEnd, CStr(Session("UserAccessModeSetting")) _
                '                                                          , True, CBool(Session("IsIncludeInactiveEmp")), Nothing)

                blnFlag = objRoleBasedApproval.Update(Session("Database_Name").ToString, CInt(Session("UserId")) _
                                                                           , CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                           , mdtPeriodStart, mdtPeriodEnd, CStr(Session("UserAccessModeSetting")) _
                                                                          , True, CBool(Session("IsIncludeInactiveEmp")), Nothing, dtpApplicationDate.GetDate.Date, CInt(cboLoanScheme.SelectedValue))
                'Pinkal (27-Oct-2022) -- End
               
            End If

            If blnFlag = False And objRoleBasedApproval._Message <> "" Then
                DisplayMessage.DisplayMessage(objRoleBasedApproval._Message, Me)
                Exit Sub
            Else

                'Pinkal (14-Dec-2022) -- Start
                'NMB Loan Module Enhancement.
                SendNotifications(xStatusId, mblnFinalApproval)
                'Pinkal (14-Dec-2022) -- End

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE AndAlso xStatusId = enLoanApplicationStatus.APPROVED Then
                    Dim objLoanScheme As New clsLoan_Scheme
                    objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                    If objLoanScheme._SeniorApproverLevelId = objRoleBasedApproval._Levelunkid Then
                        Dim objLevel As New clslnapproverlevel_master
                        objLevel._lnLevelunkid = objRoleBasedApproval._Levelunkid
                        objRoleBasedApproval.SendMortgageLoanApproverLevelNotification(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                                         , mdtPeriodStart, mdtPeriodEnd, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")) _
                                                                                                                         , objLoanScheme._ApproverLevelAlertIds, objRoleBasedApproval._Processpendingloanunkid, Session("UserName").ToString().Trim() & " - " & objLevel._lnLevelname)
                        objLevel = Nothing
                    End If
                    objLoanScheme = Nothing
                End If
                'Pinkal (17-May-2024) -- End


                Session("ProcessPendingLoanunkid") = Nothing
                Session("pendingloanaprovalunkid") = Nothing
                Session("Mapuserunkid") = Nothing
                mintPendingLoanAprovalunkid = 0
                mintProcesspendingloanunkid = 0
                mintMapUserId = 0


                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                Dim mstrStatus As String = ""
                If xStatusId = enLoanApplicationStatus.APPROVED Then
                    mstrStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsProcess_pending_loan", 6, "Approved")
                    'Pinkal (14-Dec-2022) -- Start
                    'NMB Loan Module Enhancement.
                    If mblnFinalApproval Then mstrStatus &= " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 87, "and disbursed")
                    'Pinkal (14-Dec-2022) -- End
                ElseIf xStatusId = enLoanApplicationStatus.REJECTED Then
                    mstrStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsProcess_pending_loan", 7, "Rejected")
                ElseIf xStatusId = enLoanApplicationStatus.RETURNTOAPPLICANT Then
                    mstrStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsProcess_pending_loan", 112, "Return To Applicant")
                    'Pinkal (21-Mar-2024) -- Start
                    'NMB - Mortgage UAT Enhancements.
                ElseIf xStatusId = enLoanApplicationStatus.RETURNTOPREVIOUSAPPROVER Then
                    mstrStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsProcess_pending_loan", 148, "Return To Previous Approver")
                    'Pinkal (21-Mar-2024) -- End
                End If
                ''Pinkal (27-Oct-2022) -- Start
                ''NMB Loan Module Enhancement.
                'If Request.QueryString.Count > 0 Then
                '    Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
                'Else
                '    Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Role_Based_Loan_Approval_Process/wPg_RoleBasedLoanApprovalList.aspx", False)
                'End If
                ''Pinkal (27-Oct-2022) -- End

                If Request.QueryString.Count > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "Loan Application") & " " & mstrStatus & " " & _
                                                             Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 45, "successfully") & ".", Me, Session("rootpath").ToString & "Index.aspx")

                    'Pinkal (23-Feb-2024) -- Start
                    '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                    Session.Clear()
                    Session.Abandon()
                    Session.RemoveAll()

                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                        Response.Cookies("ASP.NET_SessionId").Value = ""
                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                    End If

                    If Request.Cookies("AuthToken") IsNot Nothing Then
                        Response.Cookies("AuthToken").Value = ""
                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                    End If

                    'Pinkal (23-Feb-2024) -- End

                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "Loan Application") & " " & mstrStatus & " " & _
                                                            Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 45, "successfully") & ".", Me, Session("rootpath").ToString() & "Loan_Savings/New_Loan/Role_Based_Loan_Approval_Process/wPg_RoleBasedLoanApprovalList.aspx")

                    If mdtGetCRBData IsNot Nothing Then
                        mdtGetCRBData.Rows.Clear()
                        mdtGetCRBData = Nothing
                    End If

                    If mdtFinalCRBData IsNot Nothing Then
                        mdtFinalCRBData.Rows.Clear()
                        mdtFinalCRBData = Nothing
                    End If

            End If
                'Pinkal (23-Nov-2022) -- End

            End If

        Catch ex As Exception
            'Pinkal (14-Dec-2022) -- Start
            'NMB Loan Module Enhancement.
            SendNotifications(xStatusId, mblnFinalApproval)
            'Pinkal (14-Dec-2022) -- End
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            popupCRBData.Hide()
            'Pinkal (23-Nov-2022) -- End
            objRoleBasedApproval = Nothing
        End Try
    End Sub


    'Hemant (25 Nov 2022) -- Start
    'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
    Private Sub GetEmployeeHistoryData(ByVal intEmployeeId As Integer, ByRef intCategorizationTranunkid As Integer, ByRef intTransferunkid As Integer, ByRef intAtEmployeeunkid As Integer)
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim objETaransfer As New clsemployee_transfer_tran
        Dim objEmployee As New clsEmployee_Master
        Dim dsCategorize As New DataSet
        Dim dsTransfer As New DataSet
        Dim dsMovement As New DataSet
        Try

            dsCategorize = objCategorize.Get_Current_Job(ConfigParameter._Object._CurrentDateAndTime.Date, intEmployeeId)
            If dsCategorize.Tables(0).Rows.Count > 0 Then
                intCategorizationTranunkid = CInt(dsCategorize.Tables(0).Rows(0).Item("categorizationtranunkid"))
            Else
                intCategorizationTranunkid = -1
            End If
            dsTransfer = objETaransfer.Get_Current_Allocation(ConfigParameter._Object._CurrentDateAndTime.Date, intEmployeeId)
            If dsTransfer.Tables(0).Rows.Count > 0 Then
                intTransferunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("transferunkid"))
            Else
                intTransferunkid = -1
            End If

            Dim dtMovement As New DataTable
            dsMovement = objEmployee.GetMovement_Log(intEmployeeId, "Log")
            dtMovement = dsMovement.Tables(0).Copy
            dtMovement.DefaultView.Sort = "Date DESC, atemployeeunkid DESC"
            dtMovement = dtMovement.DefaultView.ToTable()
            If dtMovement.Rows.Count > 0 Then
                intAtEmployeeunkid = CInt(dtMovement.Rows(0).Item("atemployeeunkid"))
            Else
                intAtEmployeeunkid = -1
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCategorize = Nothing
            objETaransfer = Nothing
            objEmployee = Nothing
        End Try
    End Sub
    'Hemant (25 Nov 2022) -- End

    Private Sub ResetMax()
        Try
            If CInt(cboEmpName.SelectedValue) > 0 AndAlso CInt(cboLoanScheme.SelectedValue) > 0 Then
                Dim objLScheme As New clsLoan_Scheme

                objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                mdecMaxLoanAmountDefined = objLScheme._MaxLoanAmountLimit
                txtLoanRate.Text = CStr(objLScheme._InterestRate)
                mintMaxLoanCalcTypeId = CInt(objLScheme._MaxLoanAmountCalcTypeId)
                mstrMaxLoanAmountHeadFormula = CStr(objLScheme._MaxLoanAmountHeadFormula)
                cboLoanCalcType.SelectedValue = CStr(objLScheme._LoanCalcTypeId)
                cboInterestCalcType.SelectedValue = CStr(objLScheme._InterestCalctypeId)
                cboLoanSchemeCategory.SelectedValue = CStr(objLScheme._LoanSchemeCategoryId)
                txtRepaymentDays.Text = CStr(objLScheme._RepaymentDays)

                'Hemant (07 Jul 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-1099 - Changes in maximum principal amount calculation to factor in remaining months to applicant's last employment date
                If CInt(nudPayPeriodEOC.Text) < CInt(objLScheme._MaxNoOfInstallment) Then
                    mintMaxNoOfInstallmentLoanScheme = CInt(nudPayPeriodEOC.Text)
                Else
                    'Hemant (07 Jul 2023) -- End
                mintMaxNoOfInstallmentLoanScheme = CInt(objLScheme._MaxNoOfInstallment)
                End If 'Hemant (07 Jul 2023)
                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                If mstrCompanyGroupName = "NMB PLC" Then
                If IsNotMortgageLoanExist() = True Then 'Hemant (07 Jul 2023)
                mstrMaxInstallmentHeadFormula = objLScheme._MaxInstallmentHeadFormula
                    'Hemant (07 Jul 2023) -- Start
                    'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
                Else
                    mstrMaxInstallmentHeadFormula = objLScheme._MaxInstallmentHeadFormulaForMortgage
                    'Hemant (07 Jul 2023) -- End
                    End If 'Hemant (29 Mar 2024) -- Start
                    'ENHANCEMENT(TADB): New loan application form
                Else
                    mstrMaxInstallmentHeadFormula = objLScheme._MaxInstallmentHeadFormula
                End If
                'Hemant (29 Mar 2024) -- End

                'Pinkal (12-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                pnlTopupDetails.Visible = objLScheme._IsEligibleForTopUp
                mblnEligibleForTopUp = objLScheme._IsEligibleForTopUp
                mblnPostingToFlexcube = objLScheme._IsPostingToFlexcube
                mintInstallmentPaidForFlexcube = objLScheme._MinOfInstallmentPaid
                'Pinkal (12-Oct-2022) -- End

                'Pinkal (10-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnLoanApproval_DailyReminder = objLScheme._LoanApproval_DailyReminder
                mintEscalationDays = objLScheme._EscalationDays
                'Pinkal (10-Nov-2022) -- End

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                mstrLoanSchemeCode = objLScheme._Code
                'Pinkal (23-Nov-2022) -- End


                'Pinkal (27-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnRequiredReportingToApproval = objLScheme._RequiredReportingToApproval

                Dim objLoanCategoryMapping As New clsLoanCategoryMapping
                Dim dsSchemeCategoryMappring As DataSet = objLoanCategoryMapping.GetList("List", CInt(cboLoanSchemeCategory.SelectedValue))
                If CInt(cboLoanSchemeCategory.SelectedValue) > 0 AndAlso dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 AndAlso CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("otherdocumentunkid")) > 0 Then
                    mintOtherDocumentunkid = CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("otherdocumentunkid"))
                End If
                dsSchemeCategoryMappring = Nothing
                objLoanCategoryMapping = Nothing

                If chkMakeExceptionalApplication.Checked = True AndAlso mintOtherDocumentunkid > 0 Then
                    pnlOtherScanAttachment.Visible = True
                Else
                    pnlOtherScanAttachment.Visible = False
                End If

                'Pinkal (27-Oct-2022) -- End

                If mintMaxLoanCalcTypeId = 2 Then
                    mdecMaxLoanAmount = GetAmountByHeadFormula(mstrMaxLoanAmountHeadFormula)
                Else
                    mdecMaxLoanAmount = mdecMaxLoanAmountDefined
                End If

                If mstrMaxInstallmentHeadFormula.Trim.Length <= 0 Then
                    txtMaxLoanAmount.Text = Format(mdecMaxLoanAmount, Session("fmtCurrency").ToString)
                Else
                    mdecEstimateMaxInstallmentAmt = GetAmountByHeadFormula(mstrMaxInstallmentHeadFormula)
                    'Hemant (22 Nov 2024) -- Start
                    'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                    If mstrCompanyGroupName = "TADB" Then
                        txtMaxLoanAmount.Text = Format(mdecMaxLoanAmount, Session("fmtCurrency").ToString)
                    Else
                        'Hemant (22 Nov 2024) -- End
                    txtMaxLoanAmount.Text = Format(IIf(mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme > mdecMaxLoanAmount, mdecMaxLoanAmount, mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme), Session("fmtCurrency").ToString)
                    End If  'Hemant (22 Nov 2024) -- End
                    'Hemant (22 Dec 2023) -- Start
                    mdecMaxLoanAmount = CDec(txtMaxLoanAmount.Text)
                    'Hemant (22 Dec 2023) -- End
                End If

                mdecMinLoanAmount = objLScheme._MinLoanAmountLimit

                txtMinLoanAmount.Text = Format(mdecMinLoanAmount, Session("fmtCurrency").ToString)

                Panel1.Visible = False
                Panel2.Visible = False
                If CInt(objLScheme._LoanSchemeCategoryId) = enLoanSchemeCategories.SECURED Then
                    Panel2.Visible = True
                ElseIf CInt(objLScheme._LoanSchemeCategoryId) = enLoanSchemeCategories.MORTGAGE Then
                    Panel1.Visible = True
                End If

                'Pinkal (14-Dec-2022) -- Start
                'NMB Loan Module Enhancement.
                If CInt(objLScheme._LoanSchemeCategoryId) = enLoanSchemeCategories.MORTGAGE Then
                    'Pinkal (16-Nov-2023) -- Start
                    '(A1X-1489) NMB - Mortgage tranche disbursement API.
                    'If CBool(Session("AllowToSetDisbursementTranches")) AndAlso (objLScheme._Name.ToString().ToUpper() = "CONSTRUCTION MORTGAGE" OrElse objLScheme._Name.ToString().ToUpper() = "PURCHASE MORTGAGE") Then
                    If CBool(Session("AllowToSetDisbursementTranches")) Then
                        pnlDisbursementfunds.Visible = True
                        TxtFirstTranche.ReadOnly = False
                        TxtSecondTranche.ReadOnly = False
                        TxtThirdTranche.ReadOnly = False
                        TxtFourthTranche.ReadOnly = False
                        'Pinkal (04-Aug-2023) -- Start
                        '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                        TxtFifthTranche.ReadOnly = False
                        dtpFirstTranche.Enabled = True
                        dtpSecondTranche.Enabled = True
                        dtpThirdTranche.Enabled = True
                        dtpFourthTranche.Enabled = True
                        dtpFifthTranche.Enabled = True
                        'Pinkal (04-Aug-2023) -- End
                    Else
                        'Pinkal (21-Mar-2024) -- Start
                        'NMB - Mortgage UAT Enhancements.
                        'pnlDisbursementfunds.Visible = True
                        pnlDisbursementfunds.Visible = False
                        'Pinkal (21-Mar-2024) -- End

                        TxtFirstTranche.ReadOnly = True
                        TxtSecondTranche.ReadOnly = True
                        TxtThirdTranche.ReadOnly = True
                        TxtFourthTranche.ReadOnly = True
                        'Pinkal (04-Aug-2023) -- Start
                        '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                        TxtFifthTranche.ReadOnly = True
                        dtpFirstTranche.Enabled = False
                        dtpSecondTranche.Enabled = False
                        dtpThirdTranche.Enabled = False
                        dtpFourthTranche.Enabled = False
                        dtpFifthTranche.Enabled = False
                        'Pinkal (04-Aug-2023) -- End
                    End If
                    'Pinkal (16-Nov-2023) -- End

                    'Pinkal (17-May-2024) -- Start
                    'NMB Enhancement For Mortgage Loan.
                    pnlPrecedentSubsequentRemark.Visible = CBool(Session("AllowToAddPrecedentSubsequentRemarks"))
                    'Pinkal (17-May-2024) -- End
                Else
                    pnlDisbursementfunds.Visible = False
                    'Pinkal (17-May-2024) -- Start
                    'NMB Enhancement For Mortgage Loan.
                    pnlPrecedentSubsequentRemark.Visible = False
                    'Pinkal (17-May-2024) -- End
                End If
                'Pinkal (14-Dec-2022) -- End

                'Hemant (07 Jul 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-1099 - Changes in maximum principal amount calculation to factor in remaining months to applicant's last employment date
                If CInt(nudPayPeriodEOC.Text) < CInt(objLScheme._MaxNoOfInstallment) Then
                    txtMaxInstallment.Text = nudPayPeriodEOC.Text
                Else
                    'Hemant (07 Jul 2023) -- End
                txtMaxInstallment.Text = CStr(objLScheme._MaxNoOfInstallment)
                End If 'Hemant (07 Jul 2023))

                txtInsuranceRate.Text = CStr(objLScheme._InsuranceRate)

                txtMaxInstallmentAmt.Text = Format(mdecEstimateMaxInstallmentAmt, Session("fmtCurrency").ToString)

                RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
                txtLoanAmt.Text = Format(mdecMaxLoanAmount, Session("fmtCurrency").ToString)
                AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged

                RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                txtEMIInstallments.Text = txtMaxInstallment.Text
                AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged

                'Hemant (22 Nov 2024) -- Start
                'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                If mstrCompanyGroupName = "TADB" Then
                    txtMaxLoanAmount.Text = Format(mdecMaxLoanAmount, Session("fmtCurrency").ToString)
                    txtLoanAmt_TextChanged(txtLoanAmt, New EventArgs())
                Else
                    'Hemant (22 Nov 2024) -- End
                txtInstallmentAmt.Text = Format(mdecEstimateMaxInstallmentAmt, Session("fmtCurrency").ToString)
                Call txtInstallmentAmt_TextChanged(txtInstallmentAmt, New System.EventArgs())
                txtMaxLoanAmount.Text = Format(IIf(CDec(txtLoanAmt.Text) > mdecMaxLoanAmount, mdecMaxLoanAmount, CDec(txtLoanAmt.Text)), Session("fmtCurrency").ToString)
                End If  'Hemant (22 Nov 2024)          
                'End If
                txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100) * (CInt(txtEMIInstallments.Text) / 12), Session("fmtCurrency").ToString)


                'Pinkal (12-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                Dim objEmpBankTran As New clsEmployeeBanks
                Dim objloanAdvance As New clsLoan_Advance
                Dim dsEmpBank As New DataSet
                Dim decOutStandingPrincipalAmt As Decimal = 0
                Dim decOutStandingInterestAmt As Decimal = 0
                Dim intNoOfInstallmentPaid As Integer = 0
                'Hemant (11 Oct 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2822 :  Allow to apply another loan if employee has 1 installment left to clear
                Dim intTotalNoOfInstallment As Integer = 0
                'Hemant (11 Oct 2024) -- End
                Dim xPrevPeriodStart As Date
                Dim xPrevPeriodEnd As Date
                If CInt(mintDeductionPeriodUnkId) > 0 Then
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(mintDeductionPeriodUnkId)
                    xPrevPeriodStart = objPeriod._Start_Date
                    xPrevPeriodEnd = objPeriod._End_Date
                    objPeriod = Nothing
                Else
                    xPrevPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    xPrevPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                End If
                dsEmpBank = objEmpBankTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), "EmployeeBank", False, , CStr(cboEmpName.SelectedValue), xPrevPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

                'call oracle function to get list of loan emis by passing bank account no.
                If dsEmpBank.Tables(0).Rows.Count > 0 Then
                    If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then

                        'Pinkal (20-Feb-2023) -- Start
                        'NMB - On Loan Approval They want exclude product code CL27 in loan approval.
                        'Dim dsFlexLoanData As DataSet = objloanAdvance.GetFlexcubeLoanData(Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString, Session("OracleUserPassword").ToString, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, objLScheme._Code)
                        Dim dsFlexLoanData As DataSet = objloanAdvance.GetFlexcubeLoanData(Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString, Session("OracleUserPassword").ToString, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString, objLScheme._Code, True, False) 'EXCLUDE CL27 PRODUCT CODE 
                        'Hemant (29 Mar 2024) -- [dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString]
                        'Hemant (01 Sep 2023) -- [blnUseOldView := False]
                        'Pinkal (20-Feb-2023) -- End
                        If dsFlexLoanData IsNot Nothing AndAlso dsFlexLoanData.Tables(0).Rows.Count > 0 Then
                            decOutStandingPrincipalAmt = (From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("PRINCIPALOUTSTANDING")))).Sum()
                            decOutStandingInterestAmt = (From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("CURRENT_OUTSTANDING_INTEREST")))).Sum()
                            intNoOfInstallmentPaid = CInt((From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("PAIDNOOFINSTALMENT")))).Sum())
                            'Hemant (11 Oct 2024) -- Start
                            'ENHANCEMENT(NMB): A1X - 2822 :  Allow to apply another loan if employee has 1 installment left to clear
                            If objLScheme._Code.ToString.Trim = "CL27" Then
                                intTotalNoOfInstallment = CInt((From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("INSTALMENTCOUNT")))).Sum())
                                If intTotalNoOfInstallment - intNoOfInstallmentPaid <= 1 Then
                                    decOutStandingPrincipalAmt = 0
                                    decOutStandingInterestAmt = 0
                                End If
                            End If
                            'Hemant (11 Oct 2024) -- End
                        End If
                        'Hemant (22 Nov 2024) -- Start
                        'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                        If mstrCompanyGroupName = "TADB" AndAlso objLScheme._Code.Trim().ToString() = "56" AndAlso (decOutStandingPrincipalAmt > 0 OrElse decOutStandingInterestAmt > 0) Then
                            Dim dsSalaryAdvanceFlexcubeloan As DataSet = objloanAdvance.GetFlexcubeLoanData(Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString, Session("OracleUserPassword").ToString, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString, "58", , False)
                            If dsSalaryAdvanceFlexcubeloan IsNot Nothing AndAlso dsSalaryAdvanceFlexcubeloan.Tables(0).Rows.Count > 0 Then
                                mdecActualSalaryAdvancePendingPrincipalAmt = (From p In dsSalaryAdvanceFlexcubeloan.Tables(0).AsEnumerable() Select (CDec(p.Item("PRINCIPALOUTSTANDING")))).DefaultIfEmpty.Sum()
                            End If
                            If mdecActualSalaryAdvancePendingPrincipalAmt > 0 Then
                                chkSalaryAdvanceLiquidation.Visible = True
                            Else
                                chkSalaryAdvanceLiquidation.Visible = False
                            End If

                        Else
                            chkSalaryAdvanceLiquidation.Visible = False
                            chkSalaryAdvanceLiquidation.Checked = False
                            mdecActualSalaryAdvancePendingPrincipalAmt = 0
                            mdecFinalSalaryAdvancePendingPrincipalAmt = 0
                        End If
                        'Hemant (22 Nov 2024) -- End
                    End If '  If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then
                End If ' If dsEmpBank.Tables(0).Rows.Count > 0 Then

                TxtOutStandingPrinciple.Text = Format(decOutStandingPrincipalAmt, Session("fmtCurrency").ToString)
                TxtOutstandingInterest.Text = Format(decOutStandingInterestAmt, Session("fmtCurrency").ToString)
                TxtPaidInstallments.Text = intNoOfInstallmentPaid.ToString
                dsEmpBank = Nothing


                'Pinkal (10-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                ' txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(TxtOutStandingPrinciple.Text) + CDec(TxtOutstandingInterest.Text) + CDec(txtInsuranceAmt.Text)), Session("fmtCurrency").ToString)

                'Pinkal (14-Dec-2022) -- Start
                'NMB Loan Module Enhancement.[AS PER MATTHEW'S UAT DOCUMENT CHANGES ]
                'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - CDec(txtInsuranceAmt.Text), Session("fmtCurrency").ToString) '
                txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(TxtOutStandingPrinciple.Text) + CDec(TxtOutstandingInterest.Text) + CDec(txtInsuranceAmt.Text)) - mdecFinalSalaryAdvancePendingPrincipalAmt, Session("fmtCurrency").ToString)
                'Hemant (22 Nov 2024) -- [- mdecFinalSalaryAdvancePendingPrincipalAmt]
                'Pinkal (14-Dec-2022) -- End

                'Pinkal (10-Nov-2022) -- End


                objEmpBankTran = Nothing
                objloanAdvance = Nothing
                'Pinkal (12-Oct-2022) -- End

                mblnIsAttachmentRequired = objLScheme._IsAttachmentRequired
                mstrDocumentTypeIDs = CStr(objLScheme._DocumentTypeIDs)

                If mblnIsAttachmentRequired = True Then
                    pnlScanAttachment.Visible = True
                Else
                    pnlScanAttachment.Visible = False
                End If
                Call FillDocumentTypeCombo()
                objLScheme = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private Sub SetVisibility()
        Try
            btnGetCRBData.Visible = CBool(Session("AllowToGetCRBData"))
            'Pinkal (20-Feb-2023) -- Start
            'NMB - Loan Approval Screen Enhancements.
            btnViewExposure.Visible = CBool(Session("AllowToViewLoanExposuresOnLoanApproval"))
            'Pinkal (20-Feb-2023) -- End

            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            pnlDisbursementfunds.Visible = CBool(Session("AllowToSetDisbursementTranches"))
            'Pinkal (21-Mar-2024) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddCRBDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtCRBDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtCRBDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmpName.SelectedValue)
                dRow("documentunkid") = CInt(cboCRBDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Loan_Balance
                dRow("scanattachrefid") = enScanAttactRefId.LOAN_APPROVAL
                dRow("form_name") = mstrModuleName
                dRow("transactionunkid") = mintPendingLoanAprovalunkid
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = DateTime.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 49, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtCRBDocument.Rows.Add(dRow)
            Call FillCRBAttachment()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCRBAttachment()
        Dim dtView As DataView
        Try
            If mdtCRBDocument Is Nothing Then
                dgCRBDocument.DataSource = Nothing
                dgCRBDocument.DataBind()
                Exit Sub
            End If

            dtView = New DataView(mdtCRBDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgCRBDocument.AutoGenerateColumns = False
            dgCRBDocument.DataSource = dtView
            dgCRBDocument.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    'Pinkal (23-Nov-2022) -- End

    'Pinkal (14-Dec-2022) -- Start
    'NMB Loan Module Enhancement.
    Private Sub SendNotifications(ByVal xStatusId As Integer, ByRef mblnFinalApproval As Boolean)
        Try
            Dim objProcesspendingloan As New clsProcess_pending_loan
            objProcesspendingloan._Processpendingloanunkid = mintProcesspendingloanunkid
    
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objProcesspendingloan._Userunkid = CInt(Session("UserId"))
            End If
            objProcesspendingloan._WebClientIP = CStr(Session("IP_ADD"))
            objProcesspendingloan._WebHostName = CStr(Session("HOST_NAME"))
            objProcesspendingloan._WebFormName = mstrModuleName


            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            'If objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.PENDING AndAlso xStatusId <> enLoanApplicationStatus.RETURNTOAPPLICANT Then
            '    objProcesspendingloan.Send_NotificationFlexCube_Approver(Session("Database_Name").ToString, CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, objProcesspendingloan._Application_Date.Date, _
            '                                                                          CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid.ToString(), _
            '                                                                          Session("ArutiSelfServiceURL").ToString(), CInt(Session("CompanyUnkId")), enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), True, Nothing)

            'End If

            If (objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.PENDING OrElse xStatusId = enLoanApplicationStatus.RETURNTOPREVIOUSAPPROVER) AndAlso xStatusId <> enLoanApplicationStatus.RETURNTOAPPLICANT Then
                objProcesspendingloan.Send_NotificationFlexCube_Approver(Session("Database_Name").ToString, CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, objProcesspendingloan._Application_Date.Date, _
                                                                                      CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid.ToString(), _
                                                                                      Session("ArutiSelfServiceURL").ToString(), CInt(Session("CompanyUnkId")), xStatusId, _
                                                                                      enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), True, Nothing, xCurrentPriority, TxtApproverRemark.Text.Trim)

            End If

            'Pinkal (21-Mar-2024) -- End



            If objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED OrElse objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.REJECTED OrElse xStatusId = enLoanApplicationStatus.RETURNTOAPPLICANT Then

                If objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED Then mblnFinalApproval = True

                objProcesspendingloan.Send_NotificationFlexCube_Employee(CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid, objProcesspendingloan._Application_No, _
                                                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("CompanyUnkId")), cboLoanScheme.SelectedItem.Text, _
                                                                                       CInt(IIf(xStatusId = enLoanApplicationStatus.RETURNTOAPPLICANT, xStatusId, objProcesspendingloan._Loan_Statusunkid)), TxtApproverRemark.Text.Trim, _
                                                                                       enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), True, Nothing)


                'Pinkal (24-Oct-2024) -- Start
                '(A1X-2825) - TADB - Notify specific users when a Flexcube loan is final approved.
                'Hemant (06 Dec 2024) -- Start
                'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
                'If objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED AndAlso Session("LoanFlexcubeSuccessfulNotificationUserIds") IsNot Nothing AndAlso Session("LoanFlexcubeSuccessfulNotificationUserIds").ToString().Length > 0 Then
                '    objProcesspendingloan.SendLoanFlexcubeSuccessfulNotificationToUsers(CInt(Session("CompanyUnkId")), Session("LoanFlexcubeSuccessfulNotificationUserIds").ToString(), cboEmpName.SelectedItem.Text, txtLoanApplicationNo.Text, cboLoanScheme.SelectedItem.Text)
                'End If
                Dim objLoanScheme As New clsLoan_Scheme
                objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                If objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED AndAlso objLoanScheme._IsFinalApprovalNotify = True AndAlso objLoanScheme._FinalApprovalNotifyEmployeeIds IsNot Nothing AndAlso objLoanScheme._FinalApprovalNotifyEmployeeIds.ToString().Trim.Length > 0 Then
                    objProcesspendingloan.SendLoanFlexcubeSuccessfulNotificationToUsers(CInt(Session("CompanyUnkId")), objLoanScheme._FinalApprovalNotifyEmployeeIds.ToString().Trim, cboEmpName.SelectedItem.Text, txtLoanApplicationNo.Text, cboLoanScheme.SelectedItem.Text _
                                                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("UserId")))
                End If
                objLoanScheme = Nothing
                'Hemant (06 Dec 2024) -- End               
                'Pinkal (24-Oct-2024) -- End

            End If
            objProcesspendingloan = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (14-Dec-2022) -- End

    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .
    Private Function SendOTP(ByVal mstrTOTPCode As String) As String
        Dim objLoan As New clsProcess_pending_loan
        Dim objSendMail As New clsSendMail
        Dim mstrError As String = ""
        Try
            objSendMail._ToEmail = Session("SMSGatewayEmail").ToString()
            'objSendMail._ToEmail = mstrEmployeeEmail
            objSendMail._Subject = mstrEmpMobileNo
            objSendMail._Form_Name = mstrModuleName
            objSendMail._LogEmployeeUnkid = -1
            objSendMail._UserUnkid = CInt(Session("UserId"))
            objSendMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
            objSendMail._SenderAddress = CStr(IIf(mstrEmployeeEmail.Trim.Length <= 0, mstrEmployeeName, mstrEmployeeEmail.Trim))
            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
            objSendMail._Message = objLoan.SetSMSForLoanApplication(mstrEmployeeName, mstrTOTPCode, Counter, True)
            mstrError = objSendMail.SendMail(CInt(Session("CompanyUnkId")))
        Catch ex As Exception
            mstrError = ex.Message.ToString()
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objSendMail = Nothing
            objLoan = Nothing
        End Try
        Return mstrError
    End Function

    Private Sub InsertAttempts(ByRef mblnprompt As Boolean)
        Dim objAttempts As New clsloantotpattempts_tran
        Dim objConfig As New clsConfigOptions
        Try
            Dim xAttempts As Integer = 0
            Dim xAttemptsunkid As Integer = 0
            Dim xOTPRetrivaltime As DateTime = Nothing
            Dim dsAttempts As DataSet = Nothing

            dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, CInt(Session("UserId")), 0)

            If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                xAttempts = CInt(dsAttempts.Tables(0).Rows(0)("attempts"))
                xAttemptsunkid = CInt(dsAttempts.Tables(0).Rows(0)("attemptsunkid"))

                If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                    xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))

                    If xOTPRetrivaltime > objConfig._CurrentDateAndTime Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 62, "You cannot save this loan application.Reason :  As You reached Maximum unsuccessful attempts.Please try after") & " " & CInt(Session("TOTPRetryAfterMins")) & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 63, "mintues") & ".", Me)
                        objAttempts = Nothing
                        mblnprompt = True
                        btnVerify.Enabled = False
                        LblSeconds.Text = "00:00"
                        LblSeconds.Visible = False
                        blnShowTOTPPopup = False
                        popup_TOTP.Hide()
                        Exit Sub
                    End If
                End If

                If xAttempts >= CInt(Session("MaxTOTPUnSuccessfulAttempts")) Then
                    xAttempts = 1
                    xAttemptsunkid = 0
                Else
                    xAttempts = xAttempts + 1
                End If
            Else
                xAttempts = 1
            End If

            objAttempts._Attemptsunkid = xAttemptsunkid
            objAttempts._Attempts = xAttempts
            objAttempts._Attemptdate = objConfig._CurrentDateAndTime
            objAttempts._Otpretrivaltime = Nothing
            objAttempts._loginemployeeunkid = -1
            objAttempts._Userunkid = CInt(Session("UserId"))
            objAttempts._Ip = Session("IP_ADD").ToString
            objAttempts._Machine_Name = Session("HOST_NAME").ToString
            objAttempts._FormName = mstrModuleName
            objAttempts._Isweb = True

            If xAttemptsunkid <= 0 Then
                If objAttempts.Insert() = False Then
                    DisplayMessage.DisplayMessage(objAttempts._Message, Me)
                    Exit Sub
                End If
            Else
                If xAttempts >= CInt(Session("MaxTOTPUnSuccessfulAttempts")) Then
                    objAttempts._Otpretrivaltime = objConfig._CurrentDateAndTime.AddMinutes(CInt(Session("TOTPRetryAfterMins")))
                    blnShowTOTPPopup = False
                    popup_TOTP.Hide()
                End If
                If objAttempts.Update() = False Then
                    DisplayMessage.DisplayMessage(objAttempts._Message, Me)
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objConfig = Nothing
            objAttempts = Nothing
        End Try
    End Sub
    'Pinkal (02-Jun-2023) -- End

    'Hemant (07 Jul 2023) -- Start
    'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
    Private Function IsNotMortgageLoanExist() As Boolean
        Dim objEmpBankTran As New clsEmployeeBanks
        Dim objloanAdvance As New clsLoan_Advance
        Dim objLoanScheme As New clsLoan_Scheme
        Try
            Dim dsEmpBank As New DataSet
            Dim xPrevPeriodStart As Date
            Dim xPrevPeriodEnd As Date
            If CInt(mintDeductionPeriodUnkId) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(mintDeductionPeriodUnkId)
                xPrevPeriodStart = objPeriod._Start_Date
                xPrevPeriodEnd = objPeriod._End_Date
                objPeriod = Nothing
            Else
                xPrevPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                xPrevPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            dsEmpBank = objEmpBankTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), "EmployeeBank", False, , CStr(cboEmpName.SelectedValue), xPrevPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

            'call oracle function to get list of loan emis by passing bank account no.
            If dsEmpBank.Tables(0).Rows.Count > 0 Then
                Dim dsFlexLoanData As DataSet = objloanAdvance.GetFlexcubeLoanData(Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString, Session("OracleUserPassword").ToString, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString, "CL28", False)
                'Hemant (29 Mar 2024) -- [dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString]
                'Hemant (01 Sep 2023) -- [blnUseOldView := False]
                If dsFlexLoanData IsNot Nothing AndAlso dsFlexLoanData.Tables(0).Rows.Count > 0 Then
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmpBankTran = Nothing
            objloanAdvance = Nothing
            objLoanScheme = Nothing
        End Try
    End Function
    'Hemant (07 Jul 2023) -- End

    'Hemant (27 Oct 2023) -- Start
    'ENHANCEMENT(NMB): A1X-1443 - Show comments of the previous loan approver to the next level approver
    Private Sub SetPreviousApproverComment(ByVal intProcessPendingLoanUnkid As Integer, ByVal intPendingLoanAprovalUnkid As Integer)
        Try

            btnPreviousApproverComment.Visible = False
            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            btnReturnPreviousApprover.Visible = False
            'Pinkal (21-Mar-2024) -- End
            Dim dsList As DataSet = Nothing
            Dim mstrSearch As String = "lna.processpendingloanunkid = " & intProcessPendingLoanUnkid


            dsList = objRoleBasedApproval.GetRoleBasedApprovalList(Session("Database_Name").ToString, _
                                                                CInt(Session("UserId")), _
                                                                CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                CStr(Session("UserAccessModeSetting")), True, _
                                                                CBool(Session("IsIncludeInactiveEmp")), "List", mstrSearch, "", False, Nothing)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim drList() As DataRow = dsList.Tables(0).Select(" pendingloanaprovalunkid = " & intPendingLoanAprovalUnkid & "")
                If drList.Length > 0 Then
                    Dim intCurrentPriority As Integer = CInt(drList(0).Item("priority"))

                    'Pinkal (17-May-2024) -- Start
                    'NMB Enhancement For Mortgage Loan.
                    'Dim drPrevApprover() As DataRow = dsList.Tables(0).Select("priority < " & intCurrentPriority & " AND statusunkid <>  " & CInt(enLoanApplicationStatus.PENDING) & " ", "priority DESC")
                    Dim drPrevApprover() As DataRow = dsList.Tables(0).Select("priority < " & intCurrentPriority & " AND statusunkid =  " & CInt(enLoanApplicationStatus.APPROVED) & " ", "priority DESC")
                    'Pinkal (17-May-2024) -- End

                    If drPrevApprover.Length > 0 Then

                        'Pinkal (17-May-2024) -- Start
                        'NMB Enhancement For Mortgage Loan.
                        'txtApproverName.Text = drPrevApprover(0).Item("UserName").ToString
                        'txtApproverLevel.Text = drPrevApprover(0).Item("Level").ToString
                        'txtApproverRole.Text = drPrevApprover(0).Item("Role").ToString
                        'txtApprRejectRemark.Text = drPrevApprover(0).Item("Remark").ToString

                        gvPreviousApproverRemarks.DataSource = drPrevApprover.ToList().CopyToDataTable()
                        gvPreviousApproverRemarks.DataBind()

                        btnPreviousApproverComment.Visible = True
                        'Pinkal (21-Mar-2024) -- Start
                        'NMB - Mortgage UAT Enhancements.
                        btnReturnPreviousApprover.Visible = True
                        'Pinkal (21-Mar-2024) -- End

                        'Pinkal (17-May-2024) -- End
                    End If
                End If
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (27 Oct 2023) -- End

    'Hemant (10 May 2024) -- Start
    'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
    Private Sub FillApproverDocumentTypeCombo()
        Dim objCommonMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Dim dt As New DataTable
        Dim strFilter As String = String.Empty
        Try
            dsCombos = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            strFilter = "1=1"
            Dim drRow() As DataRow = dsCombos.Tables(0).Select(strFilter)
            If drRow.Length > 0 Then
                dt = drRow.CopyToDataTable
            End If
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dt
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDocumentTypeCombo", mstrModuleName)
        Finally
            objCommonMaster = Nothing
        End Try
    End Sub

    Private Sub FillLoanApplicationApproverAttachment()
        Dim dtView As DataView
        Try
            If mdtApproverDocument Is Nothing Then
                dgvApproverAttachement.DataSource = Nothing
                dgvApproverAttachement.DataBind()
                Exit Sub
            End If

            dtView = New DataView(mdtApproverDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvApproverAttachement.AutoGenerateColumns = False
            dgvApproverAttachement.DataSource = dtView
            dgvApproverAttachement.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddApproverDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtApproverDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtApproverDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmpName.SelectedValue)
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Loan_Balance
                dRow("scanattachrefid") = enScanAttactRefId.ROLEBASE_LOAN_APPROVAL
                dRow("form_name") = mstrModuleName
                dRow("transactionunkid") = mintPendingLoanAprovalunkid
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = DateTime.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                dRow("attachment_type") = cboDocumentType.SelectedItem.Text
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 49, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtApproverDocument.Rows.Add(dRow)
            Call FillLoanApplicationApproverAttachment()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (10 May 2024) -- End



#End Region

#Region "ComboBox Events"

    Protected Sub cboEmpName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpName.SelectedIndexChanged
        Dim objEmployee As New clsEmployee_Master
        Try
            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            'Dim decMaxLoanAmount As Decimal = 0
            'If mintMaxLoanCalcTypeId = 2 Then
            '    decMaxLoanAmount = GetAmountByHeadFormula(mstrMaxLoanAmountHeadFormula)
            'Else
            '    decMaxLoanAmount = mdecMaxLoanAmountDefined
            'End If

            'If mstrMaxInstallmentHeadFormula.Trim.Length <= 0 Then
            '    'txtMaxLoanAmount.Text = Format(GetMaxLoanAmountDefinedLoanScheme(decMaxLoanAmount), Session("fmtCurrency").ToString)
            '    txtMaxLoanAmount.Text = Format(decMaxLoanAmount, Session("fmtCurrency").ToString)
            'Else
            '    mdecEstimateMaxInstallmentAmt = GetAmountByHeadFormula(mstrMaxInstallmentHeadFormula)
            '    txtMaxLoanAmount.Text = Format(IIf(mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme > decMaxLoanAmount, decMaxLoanAmount, mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme), Session("fmtCurrency").ToString)
            'End If
            'Hemant (07 Jul 2023) -- Start
            'Enhancement : NMB : Show the remaining number of pay periods & Dates to the end of applicant's contract, Retirement on loan application and approval screens
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmpName.SelectedValue)
            Dim dtEOCDate As Date = Nothing
            If objEmployee._Termination_To_Date <> Nothing AndAlso IsDBNull(objEmployee._Termination_To_Date) = False Then
                dtEOCDate = CDate(objEmployee._Termination_To_Date)
                lblEmplDate.Text = "Till Retr. Date"
            End If

            If objEmployee._Empl_Enddate <> Nothing AndAlso IsDBNull(objEmployee._Empl_Enddate) = False Then
                If dtEOCDate < CDate(objEmployee._Empl_Enddate) Then
                Else
                    dtEOCDate = CDate(objEmployee._Empl_Enddate)
                End If
                lblEmplDate.Text = "Till EOC Date"
            End If
            If dtEOCDate <> Nothing Then
                dtpEndEmplDate.SetDate = dtEOCDate
                nudPayPeriodEOC.Text = CStr(Math.Abs((mdtPeriodStart.Month - dtEOCDate.Month) + 12 * (mdtPeriodStart.Year - dtEOCDate.Year)) + 1)
                lblPayPeriodsEOC.Visible = True
                nudPayPeriodEOC.Visible = True
                lblEmplDate.Visible = True
                dtpEndEmplDate.Visible = True
            Else
                lblPayPeriodsEOC.Visible = False
                nudPayPeriodEOC.Visible = False
                lblEmplDate.Visible = False
                dtpEndEmplDate.Visible = False
            End If
            'Hemant (07 July 2023) -- End
            ResetMax()
            'Pinkal (12-Oct-2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Protected Sub cboDeductionPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDeductionPeriod.SelectedIndexChanged
        Try
            Call SetDateFormat()

            Dim objPeriod As New clscommom_period_Tran
            'objvalPeriodDuration.Visible = True
            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboDeductionPeriod.SelectedValue)
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
                'objvalPeriodDuration.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Period Duration From :") & " " & objPeriod._Start_Date.Date & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "To") & " " & objPeriod._End_Date.Date
                objPeriod = Nothing
            Else
                'objvalPeriodDuration.Text = ""
                'objvalPeriodDuration.Visible = False
                mdtPeriodStart = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
                mdtPeriodEnd = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
            End If
            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            mintDeductionPeriodUnkId = CInt(cboDeductionPeriod.SelectedValue)
            'Pinkal (12-Oct-2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged
        Try
            ResetMax()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExRate.Text = ""
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, mdtPeriodEnd, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                'objlblExRate.Text = Format(mdecBaseExRate, Session("fmtCurrency").ToString()) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "TextBox Events"

    Private Sub txtPrincipalAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAppliedUptoPrincipalAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.No_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Dim decLoanAmt, decPrinc As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtAppliedUptoPrincipalAmt.Text, decPrinc)
                Dim decValue As Decimal = 0
                decValue = decLoanAmt / decPrinc
                RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                txtEMIInstallments.Text = CStr(IIf(decValue <= 0, 1, CDec(Math.Ceiling(decValue))))
                AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.No_Interest_With_Mapped_Head, enIntCalcType)

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub txtInstallmentAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then

                If CDbl(txtInstallmentAmt.Text) > 0 Then
                    RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                    Dim dblvalue As Double = 0
                    dblvalue = CDbl(txtLoanAmt.Text) / CDbl(txtInstallmentAmt.Text)
                    txtEMIInstallments.Text = CStr(IIf(dblvalue <= 0, 1, Math.Ceiling(dblvalue).ToString()))
                    AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged

                    RemoveHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtAppliedUptoPrincipalAmt.Text = Format(CDbl(txtInstallmentAmt.Text), Session("fmtCurrency").ToString)
                    AddHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtIntAmt.Text = Format(0, Session("fmtCurrency").ToString)
                    decNetAmount = CDec(txtInstallmentAmt.Text)

                End If

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then

                If CDbl(txtInstallmentAmt.Text) > 0 Then
                    RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                    Dim dblvalue As Double = 0
                    dblvalue = CDbl(txtLoanAmt.Text) / CDbl(txtInstallmentAmt.Text)
                    txtEMIInstallments.Text = CStr(IIf(dblvalue <= 0, 1, Math.Ceiling(dblvalue).ToString()))

                    AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                    RemoveHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtAppliedUptoPrincipalAmt.Text = Format(CDbl(txtInstallmentAmt.Text), Session("fmtCurrency").ToString)
                    AddHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtIntAmt.Text = Format(0, Session("fmtCurrency").ToString)
                    decNetAmount = CDec(txtInstallmentAmt.Text)
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub txtLoanAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                Dim decLoanAmt, decPrinc As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtAppliedUptoPrincipalAmt.Text, decPrinc)
                Dim decValue As Decimal = 0
                If decPrinc > 0 Then
                    decValue = decLoanAmt / decPrinc
                End If
                txtEMIInstallments.Text = CStr(IIf(decValue <= 0, 1, CDec(Math.Ceiling(decValue))))
                AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                Call txtEMIInstallments_TextChanged(txtEMIInstallments, New System.EventArgs)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Call txtEMIInstallments_TextChanged(txtEMIInstallments, New System.EventArgs)

            End If
            txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100) * (CInt(txtEMIInstallments.Text) / 12), Session("fmtCurrency").ToString)

            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - CDec(txtInsuranceAmt.Text), Session("fmtCurrency").ToString)
            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(TxtOutStandingPrinciple.Text) + CDec(TxtOutstandingInterest.Text) + CDec(txtInsuranceAmt.Text)), Session("fmtCurrency").ToString)

            'Pinkal (14-Dec-2022) -- Start
            'NMB Loan Module Enhancement.[AS PER MATTHEW'S UAT DOCUMENT CHANGES ]
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - CDec(txtInsuranceAmt.Text), Session("fmtCurrency").ToString)
            txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(TxtOutStandingPrinciple.Text) + CDec(TxtOutstandingInterest.Text) + CDec(txtInsuranceAmt.Text)) - mdecFinalSalaryAdvancePendingPrincipalAmt, Session("fmtCurrency").ToString)
            'Hemant (22 Nov 2024) -- [- mdecFinalSalaryAdvancePendingPrincipalAmt]
            'Pinkal (14-Dec-2022) -- End

            'Pinkal (10-Nov-2022) -- End
            'Pinkal (12-Oct-2022) -- End
           
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub txtLoanRate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanRate.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub txtEMIInstallments_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEMIInstallments.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                Dim decLoanAmt, decEMIInst As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtEMIInstallments.Text, decEMIInst)
                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                txtInstallmentAmt.Text = Format(decLoanAmt / decEMIInst, Session("fmtCurrency").ToString)
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                txtAppliedUptoPrincipalAmt.ReadOnly = False
                Dim decLoanAmt, decEMIInst As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtEMIInstallments.Text, decEMIInst)
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Dim decLoanAmt, decEMIInst As Decimal
                Decimal.TryParse(txtLoanAmt.Text, decLoanAmt)
                Decimal.TryParse(txtEMIInstallments.Text, decEMIInst)
                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                txtInstallmentAmt.Text = Format(decLoanAmt / decEMIInst, Session("fmtCurrency").ToString)
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            End If


            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            'A1X-2587 NMB mortgage - Insurance deduction: The system should deduct insurance for the first year only and for the following year it will be done yearly. 
            Dim objLScheme As New clsLoan_Scheme
            objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            If objLScheme._LoanSchemeCategoryId = enLoanSchemeCategories.MORTGAGE Then
                If CInt(txtEMIInstallments.Text) > 12 Then
                    txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100), Session("fmtCurrency").ToString)
                Else
                    txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100) * (CInt(txtEMIInstallments.Text) / 12), Session("fmtCurrency").ToString)
                End If
            Else
            txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Text) * (CDec(txtInsuranceRate.Text) / 100) * (CInt(txtEMIInstallments.Text) / 12), Session("fmtCurrency").ToString)
            End If
            objLScheme = Nothing
            'Pinkal (17-May-2024) -- End


            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - CDec(txtInsuranceAmt.Text), Session("fmtCurrency").ToString)
            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(TxtOutStandingPrinciple.Text) + CDec(TxtOutstandingInterest.Text) + CDec(txtInsuranceAmt.Text)), Session("fmtCurrency").ToString)

            'Pinkal (14-Dec-2022) -- Start
            'NMB Loan Module Enhancement.[AS PER MATTHEW'S UAT DOCUMENT CHANGES ]
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - CDec(txtInsuranceAmt.Text), Session("fmtCurrency").ToString)
            txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(TxtOutStandingPrinciple.Text) + CDec(TxtOutstandingInterest.Text) + CDec(txtInsuranceAmt.Text)) - mdecFinalSalaryAdvancePendingPrincipalAmt, Session("fmtCurrency").ToString)
            'Hemant (22 Nov 2024) -- [- mdecFinalSalaryAdvancePendingPrincipalAmt]
            'Pinkal (14-Dec-2022) -- End

            'Pinkal (10-Nov-2022) -- End
            'Pinkal (12-Oct-2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnReject.Click _
                                                                                                                                            , btnReturnApplicant.Click, btnReturnPreviousApprover.Click

        'Pinkal (21-Mar-2024) -- NMB - Mortgage UAT Enhancements.[btnReturnApprover.Click]
        'Pinkal (27-Oct-2022) -- NMB Loan Module Enhancement.[btnReturnApplicant.Click]
        Try

            If CType(sender, Button).ID.ToUpper() = btnApprove.ID.ToUpper() Then
                xStatusId = enLoanApplicationStatus.APPROVED
            ElseIf CType(sender, Button).ID.ToUpper() = btnReject.ID.ToUpper() Then
                xStatusId = enLoanApplicationStatus.REJECTED
                'Pinkal (27-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
            ElseIf CType(sender, Button).ID.ToUpper() = btnReturnApplicant.ID.ToUpper() Then
                xStatusId = enLoanApplicationStatus.RETURNTOAPPLICANT
                'Pinkal (27-Oct-2022) -- End

                'Pinkal (21-Mar-2024) -- Start
                'NMB - Mortgage UAT Enhancements.
            ElseIf CType(sender, Button).ID.ToUpper() = btnReturnPreviousApprover.ID.ToUpper() Then
                xStatusId = enLoanApplicationStatus.RETURNTOPREVIOUSAPPROVER
                'Pinkal (21-Mar-2024) -- End
            End If

            If Validation(xStatusId) = False Then Exit Sub


            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            If CBool(Session("ApplyTOTPForLoanApplicationApproval")) Then


                '/* START CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP
                Dim objConfig As New clsConfigOptions
                Dim objAttempts As New clsloantotpattempts_tran
                Dim dsAttempts As DataSet = Nothing

                dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, CInt(Session("UserId")), 0)

                Dim xOTPRetrivaltime As DateTime = Nothing
                If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                    If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                        xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))
                        If xOTPRetrivaltime > objConfig._CurrentDateAndTime Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 62, "You cannot save this loan application.Reason : As You reached Maximum unsuccessful attempts.Please try after") & " " & CInt(Session("TOTPRetryAfterMins")) & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 63, "minutes") & ".", Me)
                            objConfig = Nothing
                            objAttempts = Nothing
                            Exit Sub
                        End If
                    End If
                End If
                objConfig = Nothing
                objAttempts = Nothing
                '/* END CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP


                If Session("SMSGatewayEmail").ToString().Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 96, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan approval."), Me)
                    Exit Sub
                End If

                Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(Session("UserId"))
                txtVerifyOTP.Text = ""
                txtVerifyOTP.ReadOnly = False
                btnVerify.Enabled = True
                btnSendCodeAgain.Enabled = False
                LblSeconds.Visible = True
                btnTOTPClose.Enabled = False

                If objUser._TOTPSecurityKey.Trim.Length > 0 Then
                    mstrSecretKey = objUser._TOTPSecurityKey
                Else
                    mstrSecretKey = objTOTPHelper.GenerateSecretKey()
                    objUser._TOTPSecurityKey = mstrSecretKey
                    If objUser.Update(False) = False Then
                        DisplayMessage.DisplayMessage(objUser._Message, Me)
                        Exit Sub
                    End If
                End If

                If objUser._EmployeeUnkid > 0 Then

                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objUser._EmployeeUnkid

                    mstrEmployeeName = objEmployee._Firstname + " " + objEmployee._Surname
                    mstrEmployeeEmail = objEmployee._Email

                    If CInt(Session("SMSGatewayEmailType")) = CInt(clsEmployee_Master.EmpColEnum.Col_Present_Mobile) Then
                        mstrEmpMobileNo = objEmployee._Present_Mobile.ToString()
                    ElseIf CInt(Session("SMSGatewayEmailType")) = CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Mobile) Then
                        mstrEmpMobileNo = objEmployee._Domicile_Mobile.ToString()
                    End If
                    objEmployee = Nothing
                Else
                    mstrEmployeeName = objUser._Firstname + " " + objUser._Lastname
                    mstrEmployeeEmail = objUser._Email
                    mstrEmpMobileNo = objUser._Phone
                End If
                objUser = Nothing

                If mstrEmpMobileNo.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 97, "Mobile No is compulsory information. Please set Approver's Mobile No to send TOTP."), Me)
                    Exit Sub
                End If

                Counter = objTOTPHelper._TimeStepSeconds

                Dim mstrTOTPCode As String = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
                Dim mstrError As String = SendOTP(mstrTOTPCode)
                If mstrError.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    blnShowTOTPPopup = False
                    popup_TOTP.Hide()
                    Exit Sub
                End If

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:countdown(0," & Counter & "); ", True)
                blnShowTOTPPopup = True
                popup_TOTP.Show()
            Else
                SaveApproval(xStatusId)
            End If
            'Pinkal (02-Jun-2023) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Protected Sub btnGetCRBData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetCRBData.Click
        Dim objLoanCategory As New clsLoanCategoryMapping
        Try
            Dim dsList As DataSet = objLoanCategory.GetList("List", CInt(cboLoanSchemeCategory.SelectedValue), Nothing)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim mstrIdTypeIds As String = String.Join(",", dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("identitytypeunkid").ToString()).ToArray())

                If mstrIdTypeIds.Trim.Length > 0 Then
                    Dim objEmpIdentity As New clsIdentity_tran
                    objEmpIdentity._EmployeeUnkid = CInt(cboEmpName.SelectedValue)
                    Dim dtTable As DataTable = objEmpIdentity._DataList.Copy

                    If dtTable.Columns.Contains("crb_status") = False Then
                        Dim dcCol As New DataColumn("crb_status")
                        dcCol.DataType = Type.GetType("System.String")
                        dcCol.DefaultValue = ""
                        dtTable.Columns.Add(dcCol)
                    End If

                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        dtTable = New DataView(dtTable, "idtypeunkid in (" & mstrIdTypeIds & ")", "", DataViewRowState.CurrentRows).ToTable()

                        dgEmpIdentity.DataSource = dtTable
                        dgEmpIdentity.DataBind()

                        Dim mstrIdentityTypeIds As String = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("idtypeunkid").ToString()).ToArray())
                        If mstrIdentityTypeIds.Trim.Length > 0 Then
                            Dim dtCRBDoc As DataTable = New DataView(dsList.Tables(0), "identitytypeunkid in (" & mstrIdentityTypeIds & ")", "", DataViewRowState.CurrentRows).ToTable()

                            Dim drRow As DataRow = dtCRBDoc.NewRow
                            drRow("crbdocumentunkid") = "0"
                            drRow("crbdocument") = "Select"
                            dtCRBDoc.Rows.InsertAt(drRow, 0)
                            dtCRBDoc.AcceptChanges()

                            With cboCRBDocumentType
                                .DataValueField = "crbdocumentunkid"
                                .DataTextField = "crbdocument"
                                .DataSource = dtCRBDoc
                                .DataBind()
                                .SelectedValue = "0"
                            End With
                        End If

                    End If ' If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                    objEmpIdentity = Nothing

                    'Me.RegisterPostBackControl()

                End If ' If mstrIdTypeIds.Trim.Length > 0 Then

                'Hemant (10 May 2024) -- Start
                'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
                blnpopupCRBData = True
                'Hemant (10 May 2024) -- End
                popupCRBData.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanCategory = Nothing
        End Try
    End Sub

    Protected Sub btnAddCRBAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCRBAttachment.Click
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                Dim arrValidFileExtension() As String = {".PDF", ".JPG", ".JPEG", ".PNG", ".GIF", ".BMP", ".TIF"}
                If arrValidFileExtension.Contains(f.Extension.ToString.ToUpper()) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 52, "Please select PDF or Image file."), Me)
                    Exit Sub
                End If
                AddCRBDocumentAttachment(f, f.FullName)
                Call FillCRBAttachment()
            End If
            Session.Remove("Imagepath")
            cboCRBDocumentType.SelectedValue = CStr(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupCRBData.Show()
        End Try
    End Sub

    Protected Sub btnSaveCRBIdentityData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCRBIdentityData.Click
        Try

            Dim xCount As Integer = 0
            If mdtCRBDocument IsNot Nothing Then
                xCount = mdtCRBDocument.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Count
            End If

            If (mdtGetCRBData Is Nothing OrElse mdtGetCRBData.Rows.Count <= 0) AndAlso (mdtCRBDocument Is Nothing OrElse xCount <= 0) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 47, "Please get atleast one identity data from CRB system or attach CRB Report."), Me)
                blnpopupCRBData = True
                popupCRBData.Show()
                Exit Sub
            End If

            If mdtGetCRBData IsNot Nothing Then
                mdtFinalCRBData = mdtGetCRBData.Copy
                mdtFinalCRBData.AcceptChanges()
            End If

            blnpopupCRBData = False
            popupCRBData.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCloseCRBIdentityData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseCRBIdentityData.Click
        Try
            blnpopupCRBData = False
            popupCRBData.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If mintDeleteRowIndex >= 0 Then
                mdtCRBDocument.Rows(mintDeleteRowIndex)("AUD") = "D"
                mdtCRBDocument.AcceptChanges()
                Call FillCRBAttachment()
                mintDeleteRowIndex = -1
            End If

            'Hemant (10 May 2024) -- Start
            'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
            If Me.ViewState("DeleteApproverAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteApproverAttRowIndex")) < 0 Then
                mdtApproverDocument.Rows(CInt(Me.ViewState("DeleteApproverAttRowIndex")))("AUD") = "D"
                mdtApproverDocument.AcceptChanges()
                Call FillLoanApplicationApproverAttachment()
                Me.ViewState("DeleteApproverAttRowIndex") = Nothing
            End If
            'Hemant (10 May 2024) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If blnpopupCRBData = True Then popupCRBData.Show()
        End Try
    End Sub
    'Pinkal (23-Nov-2022) -- End

    'Pinkal (20-Feb-2023) -- Start
    'NMB - Loan Approval Screen Enhancements.
    Protected Sub btnViewExposure_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewExposure.Click
        Dim objEmpBankTran As New clsEmployeeBanks
        Dim objloanAdvance As New clsLoan_Advance
        Dim dsEmpBank As New DataSet
        Try

            Dim xPrevPeriodStart As Date
            Dim xPrevPeriodEnd As Date
            If CInt(mintDeductionPeriodUnkId) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(mintDeductionPeriodUnkId)
                xPrevPeriodStart = objPeriod._Start_Date
                xPrevPeriodEnd = objPeriod._End_Date
                objPeriod = Nothing
            Else
                xPrevPeriodStart = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
                xPrevPeriodEnd = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
            End If
            dsEmpBank = objEmpBankTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), "EmployeeBank", False, , CStr(cboEmpName.SelectedValue), xPrevPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

            If dsEmpBank.Tables(0).Rows.Count > 0 Then
                If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then
                    Dim dsExposures As DataSet = objloanAdvance.GetFlexcubeLoanData(Session("OracleHostName").ToString, Session("OraclePortNo").ToString, Session("OracleServiceName").ToString, Session("OracleUserName").ToString, Session("OracleUserPassword").ToString, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString, "", False, False)
                    'Hemant (29 Mar 2024) -- [dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString]
                    'Hemant (01 Sep 2023) -- [blnUseOldView := False]
                    If dsExposures IsNot Nothing AndAlso dsExposures.Tables(0).Rows.Count > 0 Then
                        Dim drRow As DataRow = dsExposures.Tables(0).NewRow
                        drRow("PRODUCT_CODE") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 93, "Total")
                        drRow("PRINCIPALOUTSTANDING") = (From p In dsExposures.Tables(0).AsEnumerable() Select (CDec(p.Item("PRINCIPALOUTSTANDING")))).Sum()
                        drRow("CURRENT_OUTSTANDING_INTEREST") = (From p In dsExposures.Tables(0).AsEnumerable() Select (CDec(p.Item("CURRENT_OUTSTANDING_INTEREST")))).Sum()
                        drRow("INSTALMENT_AMOUNT") = CInt((From p In dsExposures.Tables(0).AsEnumerable() Select (CDec(p.Item("INSTALMENT_AMOUNT")))).Sum())
                        dsExposures.Tables(0).Rows.InsertAt(drRow, dsExposures.Tables(0).Rows.Count)
                    End If

                    dgViewExposures.AutoGenerateColumns = False
                    dgViewExposures.DataSource = dsExposures.Tables(0)
                    dgViewExposures.DataBind()

                    'Pinkal (21-Mar-2024) -- Start
                    'NMB - Mortgage UAT Enhancements.
                    Dim objCreditCardIntegration As New clsCreditCardIntegration
                    Dim dsCCList As DataSet = objCreditCardIntegration.GetList("List", "lncreditcarddetails.directdebitaccno ='" & dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString() & "'")
                    objCreditCardIntegration = Nothing

                    dgCreditCardExposures.AutoGenerateColumns = False
                    dgCreditCardExposures.DataSource = dsCCList.Tables(0)
                    dgCreditCardExposures.DataBind()
                    'Pinkal (21-Mar-2024) -- End
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 94, "This employee doesn't have any bank account no.Please contact administrator."), Me)
                    Exit Sub
                End If
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 95, "This employee doesn't have any bank details.Please contact administrator."), Me)
                Exit Sub
            End If
            popupViewExposures.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsEmpBank IsNot Nothing Then
                dsEmpBank.Clear()
                dsEmpBank.Dispose()
            End If
            dsEmpBank = Nothing
            objloanAdvance = Nothing
            objEmpBankTran = Nothing
        End Try
    End Sub
    'Pinkal (20-Feb-2023) -- End

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Me.Request.QueryString.Count > 0 Then
                'Pinkal (23-Feb-2024) -- Start
                '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                Session.Clear()
                Session.Abandon()
                Session.RemoveAll()

                If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                    Response.Cookies("ASP.NET_SessionId").Value = ""
                    Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                    Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                End If

                If Request.Cookies("AuthToken") IsNot Nothing Then
                    Response.Cookies("AuthToken").Value = ""
                    Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                End If

                'Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
                Response.Redirect("~/Index.aspx", False)
                'Pinkal (23-Feb-2024) -- End
            Else
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Role_Based_Loan_Approval_Process/wPg_RoleBasedLoanApprovalList.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .

    Protected Sub btnVerify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerify.Click
        Try
            If txtVerifyOTP.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmOTPValidator", 1, "OTP is compulsory information.Please Enter OTP."), Me)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                txtVerifyOTP.Focus()
                popup_TOTP.Show()
                Exit Sub
            End If
            Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))
            If objTOTPHelper.ValidateTotpCode(mstrSecretKey, txtVerifyOTP.Text.Trim) = False Then
                Dim mblnprompt As Boolean = False
                InsertAttempts(mblnprompt)
                If mblnprompt = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmOTPValidator", 2, "Invalid OTP Code. Please try again."), Me)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                    objTOTPHelper = Nothing
                    Exit Sub
                End If
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                blnShowTOTPPopup = False
                popup_TOTP.Hide()
                SaveApproval(xStatusId)
            End If
            objTOTPHelper = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnSendCodeAgain_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendCodeAgain.Click
        Try
            btnSendCodeAgain.Enabled = False
            btnTOTPClose.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True

            Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))
            Dim mstrTOTPCode As String = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
            Counter = objTOTPHelper._TimeStepSeconds
            Dim mstrError As String = SendOTP(mstrTOTPCode)
            If mstrError.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(mstrError, Me)
                blnShowTOTPPopup = False
                popup_TOTP.Hide()
                Exit Sub
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:countdown(0," & Counter & "); ", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_TOTP.Show()
        End Try
    End Sub

    Protected Sub btnTOTPClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTOTPClose.Click
        Try
            btnSendCodeAgain.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True
            blnShowTOTPPopup = False
            popup_TOTP.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (02-Jun-2023) -- End

    'Hemant (27 Oct 2023) -- Start
    'ENHANCEMENT(NMB): A1X-1443 - Show comments of the previous loan approver to the next level approver
    Protected Sub btnPreviousApproverComment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreviousApproverComment.Click
        Try
            popupPreviousApprover.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (27 Oct 2023) -- End

    'Hemant (10 May 2024) -- Start
    'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
    Protected Sub btnAddApproverAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddApproverAttachment.Click
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                Dim arrValidFileExtension() As String = {".PDF", ".JPG", ".JPEG", ".PNG", ".GIF", ".BMP", ".TIF"}
                If arrValidFileExtension.Contains(f.Extension.ToString.ToUpper()) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 52, "Please select PDF or Image file."), Me)
                    Exit Sub
                End If
                AddApproverDocumentAttachment(f, f.FullName)
                Call FillLoanApplicationApproverAttachment()
            End If
            Session.Remove("Imagepath")
            cboDocumentType.SelectedValue = CStr(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub
    'Hemant (10 May 2024) -- End

    'Pinkal (17-May-2024) -- Start
    'NMB Enhancement For Mortgage Loan.
    Protected Sub btnOfferLetter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOfferLetter.Click
        Dim mdicLoanType As New Dictionary(Of Integer, String)
        Dim objFlexcubeLoanOfferLetter As New ArutiReports.clsFlexcubeLoanOfferLetter(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            Dim intLoanTypeId As Integer

            mdicLoanType.Add(1, "GENERAL")
            mdicLoanType.Add(2, "CAR")
            mdicLoanType.Add(3, "REFINANCE MORTGAGE")
            mdicLoanType.Add(4, "PURCHASE MORTGAGE")
            mdicLoanType.Add(5, "CONSTRUCTION MORTGAGE")

            For Each key In mdicLoanType
                If cboLoanScheme.SelectedItem.Text.ToUpper.Contains(key.Value) Then
                    Select Case key.Key
                        Case 1
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.General_Loan
                            Exit For
                        Case 2
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.Car_Loan
                            Exit For
                        Case 3
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.Refinancing_Mortgage_Loan
                            Exit For
                        Case 4
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.Purchase_Mortgage_Loan
                            Exit For
                        Case 5
                            intLoanTypeId = clsFlexcubeLoanOfferLetter.enLoanType.Construction_Mortgage_Loan
                            Exit For
                    End Select
                End If
            Next

            If intLoanTypeId <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Offer Letter is not available for Selected Loan Scheme"), Me)
                Exit Sub
            End If

            objFlexcubeLoanOfferLetter._EmployeeUnkId = -1
            objFlexcubeLoanOfferLetter._LoanSchemeUnkId = -1
            objFlexcubeLoanOfferLetter._ProcessPendingLoanUnkid = mintProcesspendingloanunkid
            objFlexcubeLoanOfferLetter._LoanTypeId = intLoanTypeId

            GUI.fmtCurrency = CStr(Session("fmtCurrency"))

            objFlexcubeLoanOfferLetter.generateReportNew(CStr(Session("Database_Name")), _
                                         CInt(Session("UserId")), _
                                         CInt(Session("Fin_year")), _
                                         CInt(Session("CompanyUnkId")), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                         Session("UserAccessModeSetting").ToString, True, _
                                         Session("ExportReportPath").ToString, _
                                         CBool(Session("OpenAfterExport")), _
                                         0, enPrintAction.None, enExportAction.None, _
                                         CInt(Session("Base_CurrencyId")))
            Session("objRpt") = objFlexcubeLoanOfferLetter._Rpt
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mdicLoanType IsNot Nothing Then mdicLoanType.Clear()
            mdicLoanType = Nothing
            objFlexcubeLoanOfferLetter = Nothing
        End Try
    End Sub
    'Pinkal (17-May-2024) -- End

#End Region

#Region "Gridview Event"

    Protected Sub dgvAttachement_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvAttachement.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(getColumnId_Datagrid(dgvAttachement, "objcolhScanUnkId", False, True)).Text) > 0 Then

                    xrow = mdtLoanApplicationDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(getColumnId_Datagrid(dgvAttachement, "objcolhScanUnkId", False, True)).Text) & "")

                    If e.CommandName = "imgdownload" Then
                        Dim xPath As String = ""
                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("localpath").ToString
                        End If
                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 33, "File does not Exist..."), Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(getColumnId_Datagrid(dgvAttachement, "colhName", False, True)).Text & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (27-Oct-2022) -- Start
    'NMB Loan Module Enhancement.

    Protected Sub dgvOtherAttachement_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvOtherAttachement.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(getColumnId_Datagrid(dgvOtherAttachement, "objcolhOtherScanUnkId", False, True)).Text) > 0 Then

                    xrow = mdtOtherDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(getColumnId_Datagrid(dgvOtherAttachement, "objcolhOtherScanUnkId", False, True)).Text) & "")

                    If e.CommandName = "imgotherdownload" Then
                        Dim xPath As String = ""
                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("localpath").ToString
                        End If
                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 33, "File does not Exist..."), Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(getColumnId_Datagrid(dgvOtherAttachement, "colhotherName", False, True)).Text & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (27-Oct-2022) -- End

    'Hemant (10 May 2024) -- Start
    'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
    Protected Sub dgvApproverAttachement_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvApproverAttachement.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(getColumnId_Datagrid(dgvApproverAttachement, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtApproverDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(getColumnId_Datagrid(dgvApproverAttachement, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtApproverDocument.Select("GUID = '" & e.Item.Cells(getColumnId_Datagrid(dgvApproverAttachement, "objcolhGUID", False, True)).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 36, "Are you sure you want to delete this attachment?")
                        popup_YesNo.Show()
                        Me.ViewState("DeleteApproverAttRowIndex") = mdtApproverDocument.Rows.IndexOf(xrow(0))
                    End If
                ElseIf e.CommandName = "imgdownload" Then
                    Dim xPath As String = ""
                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("filepath").ToString
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If
                    If xPath.Trim <> "" Then
                        Dim fileInfo As New IO.FileInfo(xPath)
                        If fileInfo.Exists = False Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 44, "File does not exist on localpath"), Me)
                            Exit Sub
                        End If
                        fileInfo = Nothing
                        Dim strFile As String = xPath
                        Response.ContentType = "image/jpg/pdf"
                        Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(getColumnId_Datagrid(dgvApproverAttachement, "colhName", False, True)).Text & """")
                        Response.Clear()
                        Response.TransmitFile(strFile)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (10 May 2024) -- End


    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.


    Private Sub GetProperties(ByVal o As JObject, Optional ByVal mstrFilter As String = "")
        Try
            Dim drFilter As JObject = Nothing

            If mstrFilter.Trim.Length > 0 Then
                drFilter = CType(o.AsJEnumerable().Where(Function(x) x.Equals(mstrFilter)), JObject)
            Else
                drFilter = o
            End If

            For Each prop As KeyValuePair(Of String, JToken) In drFilter
                If (prop.Value.Type = JTokenType.Object) Then
                    GetProperties(CType(prop.Value, JObject), "value")
                Else
                    LstToken.Add(prop.Key.ToString() & " - " & prop.Value.ToString())
                End If
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetKeys(ByVal obj As JObject)
        Try
            For Each child In obj.Children()
                If (child.Type = JTokenType.Object) Then
                    GetKeys(JObject.Parse(CStr(child)))
                Else
                    GetProperties(obj)
                End If
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgEmpIdentity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmpIdentity.ItemCommand
        Try
            If e.CommandName.ToUpper = "GETDATA" Then


                Dim objconfig As New clsConfigOptions
                Dim mstrOracleUserName As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "OracleUserName", Nothing)
                Dim mstrOracleUserPassword As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "OracleUserPassword", Nothing)
                Dim mstrCRBRequestFlexcubeURL As String = objconfig.GetKeyValue(CInt(Session("CompanyUnkId")), "CRBRequestFlexcubeURL", Nothing)


                If objconfig.IsKeyExist(CInt(Session("CompanyUnkId")), "Advance_CostCenterunkid", Nothing) = True Then
                    mstrOracleUserPassword = clsSecurity.Decrypt(mstrOracleUserPassword, "ezee")
                End If
                objconfig = Nothing


                Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(mstrOracleUserName.Trim & ":" & mstrOracleUserPassword)
                Dim mstrBase64Credential As String = Convert.ToBase64String(byt)


                Dim objMasterData As New clsMasterData
                Dim objCRB As New clsCRBLoanIntegration
                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, Nothing) = CInt(cboEmpName.SelectedValue)


                objCRB.dateOfBirth = objEmployee._Birthdate.ToString("yyyy-MM-ddT00:00:00")
                'Pinkal (15-Sep-2023) -- Start
                '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.
                'objCRB.firstName = objEmployee._Firstname.Trim
                'objCRB.fullName = objEmployee._Firstname & " " & CStr(IIf(objEmployee._Othername.Trim.Length > 0, objEmployee._Othername.Trim & " ", "")) & objEmployee._Surname.Trim
                'objCRB.presentSurname = objEmployee._Surname.Trim
                objCRB.firstName = objEmployee._Firstname.Trim.Replace("`", "''").Replace("'", "''")
                objCRB.fullName = objEmployee._Firstname.Replace("`", "''").Replace("'", "''") & " " & CStr(IIf(objEmployee._Othername.Trim.Length > 0, objEmployee._Othername.Trim.Replace("`", "''").Replace("'", "''") & " ", "")) & objEmployee._Surname.Trim.Replace("`", "''").Replace("'", "''")
                objCRB.presentSurname = objEmployee._Surname.Trim.Replace("`", "''").Replace("'", "''")
                'Pinkal (15-Sep-2023) --End
                objCRB.phoneNumbers.string = "+" & objEmployee._Present_Mobile

                objCRB.idNumbers.idNumberPairIndividual.idNumberType = e.Item.Cells(getColumnId_Datagrid(dgEmpIdentity, "dgcolhIdentityType", False, True)).Text
                objCRB.idNumbers.idNumberPairIndividual.idNumber = e.Item.Cells(getColumnId_Datagrid(dgEmpIdentity, "dgcolhIdentityNo", False, True)).Text

                Dim mstrError As String = ""
                Dim mstrPostedData As String = ""
                Dim mstrReponseData As String = ""

                If objMasterData.GetSetFlexCubeLoanRequest(mstrCRBRequestFlexcubeURL, mstrBase64Credential, objCRB, mstrError, mstrPostedData, mstrReponseData, True) = False Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    Exit Sub
                End If

                'mstrPostedData = "{""dateOfBirth"":""1988-05-14T00:00:00"",""firstName"":""Prisca"",""fullName"":""Prisca Godwell Mking`I"",""idNumbers"":{""idNumberPairIndividual"":{""idNumber"":""19880514-14121-00001-11"",""idNumberType"":""NationalID""}},""phoneNumbers"":{""string"":""+255717582593""},""presentSurname"":""Mking`I""}"

                'mstrReponseData = "{""statusCode"":""600"",""statusDesc"":""Success"",""body"":{""status"":""ok"",""hitcount"":1,""infomsg"":""ReportGenerated"",""currency"":""TZS"",""generalInformation"":{""subjectIDNumber"":""19880514-14121-00001-11"",""requestDate"":""2023-09-26T21:00:00Z"",""referenceNumber"":""42326427-68361172"",""recommendedDecision"":""Approve"",""brokenRules"":0},""personalInformation"":{""fullName"":""Prisca GODWEL Mking`i"",""dateOfBirth"":""1988-05-13T21:00:00Z"",""age"":35,""gender"":""Female"",""maritalStatus"":""Single"",""employmentStatus"":""Other""},""scoringAnalysis"":{""mobileScore"":624,""mobileScoreRiskGrade"":""C1"",""conclusion"":""<Conclusion xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR4""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR5""}]},""cipscore"":726,""cipriskGrade"":""A3""},""inquiriesAnalysis"":{""totalLast7Days"":3,""totalLast1Month"":3,""nonBankingLast1Month"":0,""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ1""},{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ2""},{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ3""}]},""conclusion"":""<Conclusion xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>""},""currentContracts"":{""currentBanking"":{""positive"":1,""negative"":0,""balance"":65294510.12,""balanceAtRisk"":0},""currentNonBanking"":{""positive"":0,""negative"":0,""balance"":0,""balanceAtRisk"":0},""total"":{""positive"":1,""negative"":0,""balance"":65294510.12,""balanceAtRisk"":0}},""pastDueInformation"":{""totalCurrentPastDue"":0,""totalCurrentDaysPastDue"":0,""worstCurrentPastDue"":0.0,""worstCurrentDaysPastDue"":0,""worstPastDueLast12Months"":0.0,""worstPastDueDaysLast12Months"":0,""monthsWithoutArrearsLast12Months"":9,""totalMonthsWithHistoryLast12Months"":12,""percMonthsWithoutArrearsLast12Months"":75},""repaymentInformation"":{""totalMonthlyPayment"":0,""nextContractMaturesInMonths"":89,""allContractsMatureInMonths"":89,""lastContractOpened"":1674766800000,""closedContracts"":2},""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK4""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK5""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK6""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK7""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK8""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK9""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK10""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK11""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST12""}]},""tzaCb5Data"":{""bouncedCheques"":{},""branches"":{""numberOfBranches"":0},""cip"":{""recordList"":{""record"":[{""date"":""2023-09-27T11:05:32.2821706Z"",""grade"":""A3"",""probabilityOfDefault"":1.07,""score"":726,""trend"":""NoChange""},{""date"":""2023-08-30T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.14,""score"":723,""trend"":""Up""},{""date"":""2023-07-30T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.4,""reasonsList"":{""reason"":[{""code"":""NCS3""}]},""score"":714,""trend"":""NoChange""},{""date"":""2023-06-29T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.4,""reasonsList"":{""reason"":[{""code"":""NCS3""}]},""score"":714,""trend"":""NoChange""},{""date"":""2023-05-30T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.4,""reasonsList"":{""reason"":[{""code"":""NCS3""}]},""score"":714,""trend"":""NoChange""},{""date"":""2023-04-29T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.4,""reasonsList"":{""reason"":[{""code"":""NCS3""}]},""score"":714,""trend"":""NoChange""},{""date"":""2023-03-30T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.4,""reasonsList"":{""reason"":[{""code"":""NCS3""}]},""score"":714,""trend"":""NoChange""},{""date"":""2023-02-27T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.4,""reasonsList"":{""reason"":[{""code"":""NCS3""}]},""score"":714,""trend"":""Down""},{""date"":""2023-01-30T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.14,""score"":723,""trend"":""Up""},{""date"":""2022-12-30T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.4,""reasonsList"":{""reason"":[{""code"":""NCS3""}]},""score"":714,""trend"":""NoChange""},{""date"":""2022-11-29T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.31,""reasonsList"":{""reason"":[{""code"":""NCS3""}]},""score"":717,""trend"":""NoChange""},{""date"":""2022-10-30T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.31,""reasonsList"":{""reason"":[{""code"":""NCS3""}]},""score"":717,""trend"":""NoChange""},{""date"":""2022-09-29T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.31,""reasonsList"":{""reason"":[{""code"":""NCS3""}]},""score"":717,""trend"":""Up""}]}},""ciq"":{""detail"":{""lostStolenRecordsFound"":0,""numberOfCancelledClosedContracts"":0,""numberOfSubscribersMadeInquiriesLast14Days"":1,""numberOfSubscribersMadeInquiriesLast2Days"":1},""summary"":{""numberOfFraudAlertsPrimarySubject"":0,""numberOfFraudAlertsThirdParty"":0}},""contractOverview"":{""contractList"":{""contract"":[{""contractStatus"":""GrantedAndActivated"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":65294510.12,""value"":65294510.12},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""phaseOfContract"":""Open"",""roleOfClient"":""MainDebtor"",""sector"":""Banks"",""startDate"":""2023-01-26T21:00:00Z"",""totalAmount"":{""currency"":""TZS"",""localValue"":54600000.0,""value"":54600000.0},""typeOfContract"":""Installment""},{""contractStatus"":""SettledInAdvance"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""phaseOfContract"":""Closed"",""roleOfClient"":""MainDebtor"",""sector"":""Banks"",""startDate"":""2018-08-01T21:00:00Z"",""totalAmount"":{""currency"":""TZS"",""localValue"":20240000.0,""value"":20240000.0},""typeOfContract"":""Installment""},{""contractStatus"":""SettledOnTime"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""phaseOfContract"":""Closed"",""roleOfClient"":""MainDebtor"",""sector"":""Banks"",""startDate"":""2021-05-11T21:00:00Z"",""totalAmount"":{""currency"":""TZS"",""localValue"":1000000.0,""value"":1000000.0},""typeOfContract"":""Installment""}]}},""contractSummary"":{""affordabilityHistoryList"":{""affordabilityHistory"":[{""month"":9,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2023},{""month"":8,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2023},{""month"":7,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2023},{""month"":6,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2023},{""month"":5,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2023},{""month"":4,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2023},{""month"":3,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2023},{""month"":2,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2023},{""month"":1,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2023},{""month"":12,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":11,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":10,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022}]},""affordabilitySummary"":{""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0}},""debtor"":{""closedContracts"":2,""openContracts"":1,""outstandingAmountSum"":{""currency"":""TZS"",""localValue"":65294510.12,""value"":65294510.12},""pastDueAmountSum"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""totalAmountSum"":{""currency"":""TZS"",""localValue"":54600000.0,""value"":54600000.0}},""guarantor"":{""closedContracts"":0,""openContracts"":0},""overall"":{""maxDueInstallmentsClosedContracts"":0,""maxDueInstallmentsOpenContracts"":0,""worstPastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""worstPastDueDays"":0},""paymentCalendarList"":{""paymentCalendar"":[{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2023-09-29T21:00:00Z""},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2023-08-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":65294510.12,""value"":65294510.12},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2023-07-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":66012032.21,""value"":66012032.21},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2023-06-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":66729554.3,""value"":66729554.3},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2023-05-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":67447076.39,""value"":67447076.39},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2023-04-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":68164598.48,""value"":68164598.48},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2023-03-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":68882120.57,""value"":68882120.57},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2023-02-27T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":68882120.57,""value"":68882120.57},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2023-01-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":62684622.12,""value"":62684622.12},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-12-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":63381117.92,""value"":63381117.92},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-11-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":64077613.72,""value"":64077613.72},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-10-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":64774109.52,""value"":64774109.52},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-09-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":65470605.32,""value"":65470605.32},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-08-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":66167101.12,""value"":66167101.12},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-07-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":66863596.92,""value"":66863596.92},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":2,""date"":""2022-06-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":66863596.92,""value"":66863596.92},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":2,""date"":""2022-05-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":49712991.13,""value"":49712991.13},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-04-29T21:00:00Z""},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-03-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":50328667.03,""value"":50328667.03},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":2,""date"":""2022-02-27T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":41741960.27,""value"":41741960.27},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-01-30T21:00:00Z""},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-12-30T21:00:00Z""},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-11-29T21:00:00Z""},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2021-10-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":43918240.67,""value"":43918240.67},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":2,""date"":""2021-09-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":45295644.12,""value"":45295644.12},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":2,""date"":""2021-08-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":46006380.89,""value"":46006380.89},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":2,""date"":""2021-07-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":33275680.92,""value"":33275680.92},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":2,""date"":""2021-06-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":33856845.24,""value"":33856845.24},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2021-05-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":33354676.23,""value"":33354676.23},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-04-29T21:00:00Z""},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2021-03-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":34350338.21,""value"":34350338.21},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2021-02-27T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":34848169.2,""value"":34848169.2},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2021-01-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":35346000.19,""value"":35346000.19},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-12-30T21:00:00Z""},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2020-11-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":35843831.18,""value"":35843831.18},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-10-30T21:00:00Z""}]},""sectorInfoList"":{""sectorInfo"":{""debtorClosedContracts"":2,""debtorOpenContracts"":1,""debtorOutstandingAmountSum"":{""currency"":""TZS"",""localValue"":65294510.12,""value"":65294510.12},""debtorPastDueAmountSum"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""debtorTotalAmountSum"":{""currency"":""TZS"",""localValue"":54600000.0,""value"":54600000.0},""guarantorClosedContracts"":0,""guarantorOpenContracts"":0,""sector"":""Banks""}}},""contracts"":{""contractList"":{""contract"":[{""contractStatus"":""GrantedAndActivated"",""contractSubtype"":""NotSpecified"",""currencyOfContract"":""TZS"",""disputes"":{""closedDisputes"":0,""falseDisputes"":0},""expectedEndDate"":""2031-02-24T21:00:00Z"",""methodOfPayment"":""NotSpecified"",""negativeStatusOfContract"":""NoNegativeStatus"",""numberOfDueInstallments"":0,""outstandingAmount"":{""currency"":""TZS"",""localValue"":65294510.12,""value"":65294510.12},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""paymentCalendarList"":{""calendarItem"":[{""date"":""2023-09-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2023-08-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":65294510.12,""value"":65294510.12},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2023-07-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":66012032.21,""value"":66012032.21},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2023-06-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":66729554.3,""value"":66729554.3},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2023-05-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":67447076.39,""value"":67447076.39},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2023-04-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":68164598.48,""value"":68164598.48},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2023-03-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":68882120.57,""value"":68882120.57},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2023-02-27T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":68882120.57,""value"":68882120.57},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2023-01-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":62684622.12,""value"":62684622.12},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-12-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":63381117.92,""value"":63381117.92},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-11-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":64077613.72,""value"":64077613.72},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-10-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":64774109.52,""value"":64774109.52},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-09-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":65470605.32,""value"":65470605.32},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-08-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":66167101.12,""value"":66167101.12},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-07-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":66863596.92,""value"":66863596.92},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-06-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":66863596.92,""value"":66863596.92},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-05-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":49629657.76,""value"":49629657.76},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-04-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2022-03-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":50328667.03,""value"":50328667.03},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-02-27T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":41408626.91,""value"":41408626.91},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-01-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2021-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2021-11-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2021-10-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":43918240.67,""value"":43918240.67},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-09-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":44545644.11,""value"":44545644.11},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-08-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":45173047.55,""value"":45173047.55},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-07-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":32359014.25,""value"":32359014.25},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-06-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":32856845.24,""value"":32856845.24},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-05-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":33354676.23,""value"":33354676.23},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-04-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2021-03-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":34350338.21,""value"":34350338.21},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-02-27T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":34848169.2,""value"":34848169.2},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-01-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":35346000.19,""value"":35346000.19},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2020-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2020-11-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":35843831.18,""value"":35843831.18},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2020-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""}]},""paymentPeriodicity"":""Days30"",""phaseOfContract"":""Open"",""purposeOfFinancing"":""Other"",""roleOfClient"":""MainDebtor"",""startDate"":""2023-01-26T21:00:00Z"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC"",""subscriberType"":""Banks"",""totalAmount"":{""currency"":""TZS"",""localValue"":54600000.0,""value"":54600000.0},""typeOfContract"":""Installment"",""worstPastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0}},{""contractStatus"":""SettledInAdvance"",""contractSubtype"":""ConsumerLoan"",""currencyOfContract"":""TZS"",""disputes"":{""closedDisputes"":0,""falseDisputes"":0},""expectedEndDate"":""2024-08-24T21:00:00Z"",""methodOfPayment"":""DirectRemittance"",""negativeStatusOfContract"":""NoNegativeStatus"",""numberOfDueInstallments"":0,""outstandingAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""paymentCalendarList"":{""calendarItem"":[{""date"":""2019-05-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":23349559.87,""value"":23349559.87},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2019-04-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":23714396.74,""value"":23714396.74},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2019-03-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":24079233.61,""value"":24079233.61},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2019-02-27T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":24444070.48,""value"":24444070.48},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2019-01-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":24808907.35,""value"":24808907.35},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2018-12-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":25173744.22,""value"":25173744.22},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2018-11-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":25538581.09,""value"":25538581.09},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2018-10-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":25903417.96,""value"":25903417.96},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2018-09-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":26268254.83,""value"":26268254.83},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2018-08-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":15353431.65,""value"":15353431.65},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2018-07-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":15597136.91,""value"":15597136.91},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2018-06-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":15840842.17,""value"":15840842.17},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2018-05-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":16084547.43,""value"":16084547.43},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2018-04-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":16328252.69,""value"":16328252.69},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2018-03-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":16571957.95,""value"":16571957.95},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2018-02-27T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2018-01-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":17059368.47,""value"":17059368.47},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2017-12-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":17303073.73,""value"":17303073.73},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2017-11-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":17546778.99,""value"":17546778.99},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2017-10-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":17546778.99,""value"":17546778.99},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2017-09-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":9896019.8,""value"":9896019.8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2017-08-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2017-07-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":10291860.6,""value"":10291860.6},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2017-06-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":10489781.0,""value"":10489781.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2017-05-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2017-04-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":10885621.8,""value"":10885621.8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2017-03-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":11083542.2,""value"":11083542.2},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2017-02-27T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2017-01-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":11479383.0,""value"":11479383.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2016-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2016-11-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2016-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2016-09-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2016-08-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2016-07-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2016-06-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable""}]},""paymentPeriodicity"":""Days30"",""phaseOfContract"":""Closed"",""purposeOfFinancing"":""Other"",""roleOfClient"":""MainDebtor"",""startDate"":""2018-08-01T21:00:00Z"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC"",""subscriberType"":""Banks"",""totalAmount"":{""currency"":""TZS"",""localValue"":20240000.0,""value"":20240000.0},""typeOfContract"":""Installment"",""worstPastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0}},{""contractStatus"":""SettledOnTime"",""contractSubtype"":""NotSpecified"",""currencyOfContract"":""TZS"",""disputes"":{""closedDisputes"":0,""falseDisputes"":0},""expectedEndDate"":""2022-05-24T21:00:00Z"",""methodOfPayment"":""NotSpecified"",""negativeStatusOfContract"":""NoNegativeStatus"",""numberOfDueInstallments"":0,""outstandingAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""paymentCalendarList"":{""calendarItem"":[{""date"":""2022-05-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":83333.37,""value"":83333.37},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-04-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2022-03-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2022-02-27T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":333333.36,""value"":333333.36},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-01-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2021-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2021-11-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2021-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable""},{""date"":""2021-09-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":750000.01,""value"":750000.01},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-08-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":833333.34,""value"":833333.34},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-07-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":916666.67,""value"":916666.67},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-06-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1000000.0,""value"":1000000.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0}]},""paymentPeriodicity"":""Days30"",""phaseOfContract"":""Closed"",""purposeOfFinancing"":""Other"",""roleOfClient"":""MainDebtor"",""startDate"":""2021-05-11T21:00:00Z"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC"",""subscriberType"":""Banks"",""totalAmount"":{""currency"":""TZS"",""localValue"":1000000.0,""value"":1000000.0},""typeOfContract"":""Installment"",""worstPastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0}}]}},""currentRelations"":{},""dashboard"":{""cip"":{""grade"":""A3"",""score"":726},""ciq"":{""fraudAlerts"":0,""fraudAlertsThirdParty"":0},""collaterals"":{""highestCollateralValue"":{""currency"":""TZS"",""localValue"":0,""value"":0},""highestCollateralValueType"":""NotSpecified"",""numberOfCollaterals"":0,""totalCollateralValue"":{""currency"":""TZS"",""localValue"":0,""value"":0}},""disputes"":{""activeContractDisputes"":0,""activeSubjectDisputes"":0,""closedContractDisputes"":0,""closedSubjectDisputes"":0,""falseDisputes"":0},""inquiries"":{""inquiriesForLast12Months"":3},""paymentsProfile"":{""numberOfDifferentSubscribers"":1,""pastDueAmountSum"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""worstPastDueDaysCurrent"":0,""worstPastDueDaysForLast12Months"":0},""relations"":{""numberOfInvolvements"":0,""numberOfRelations"":0}},""disputes"":{""summary"":{""numberOfActiveDisputesContracts"":0,""numberOfActiveDisputesInCourt"":0,""numberOfActiveDisputesSubjectData"":0,""numberOfClosedDisputesContracts"":0,""numberOfClosedDisputesSubjectData"":0,""numberOfFalseDisputes"":0}},""individual"":{""contact"":{""mobilePhone"":""+255717582593""},""general"":{""birthSurname"":""Mking`i"",""citizenship"":""NotSpecified"",""classificationOfIndividual"":""Individual"",""countryOfBirth"":""NotSpecified"",""dateOfBirth"":""1988-05-13T21:00:00Z"",""education"":""NotSpecified"",""employment"":""Other"",""firstName"":""Prisca"",""fullName"":""Prisca GODWEL Mking`i"",""gender"":""Female"",""maritalStatus"":""Single"",""nationality"":""TZ"",""negativeStatus"":""NotSpecified""},""identifications"":{""passportIssuerCountry"":""NotSpecified"",""wardID"":0},""mainAddress"":{""country"":""TZ""},""secondaryAddress"":{""country"":""TZ""}},""inquiries"":{""inquiryList"":{""inquiry"":[{""dateOfInquiry"":""2023-09-26T08:01:00.437Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC (MOBILE MICROLENDING)""},{""dateOfInquiry"":""2023-09-26T07:52:31.71Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC (MOBILE MICROLENDING)""},{""dateOfInquiry"":""2023-09-26T07:41:59.557Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC (MOBILE MICROLENDING)""},{""dateOfInquiry"":""2022-06-21T09:01:04.477Z"",""product"":""Quick Check"",""reason"":""NotSpecified"",""sector"":""Banks"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC""}]},""summary"":{""numberOfInquiriesLast12Months"":3,""numberOfInquiriesLast1Month"":3,""numberOfInquiriesLast24Months"":4,""numberOfInquiriesLast3Months"":3,""numberOfInquiriesLast6Months"":3}},""managers"":{""numberOfManagers"":0},""parameters"":{""consent"":true,""inquiryReason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""reportDate"":""2023-09-27T11:05:32.2821706Z"",""sections"":{""string"":""CreditinfoReportPlus""},""subjectType"":""Individual"",""idnumber"":68361172,""idnumberType"":""CreditinfoId""},""paymentIncidentList"":{""summary"":{""dueAmountSummary"":{""currency"":""TZS"",""localValue"":0,""value"":0},""outstandingAmountSummary"":{""currency"":""TZS"",""localValue"":0,""value"":0}}},""reportInfo"":{""created"":""2023-09-27T11:05:32.5800222Z"",""referenceNumber"":""42326427-68361172"",""reportStatus"":""ReportGenerated"",""requestedBy"":""Emmanuel Robert"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC (MOBILE MICROLENDING)"",""version"":573},""shareholders"":{""numberOfShareholders"":0},""subjectInfoHistory"":{""addressList"":""<AddressList xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""><Address><Item>MainAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2017-01-30T21:00:00Z</ValidFrom><ValidTo>2017-03-30T21:00:00Z</ValidTo><Value>Mufindi, CityTownVillage.Mufindi, Iringa, Mufindi, TZ</Value></Address><Address><Item>MainAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2017-03-30T21:00:00Z</ValidFrom><ValidTo>2020-05-30T21:00:00Z</ValidTo><Value>MUFINDI, CityTownVillage.MUFINDI, Iringa, Mufindi, TZ</Value></Address><Address><Item>MainAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2020-05-30T21:00:00Z</ValidFrom><ValidTo>2020-08-30T21:00:00Z</ValidTo><Value>MUFINDI, CityTownVillage.MUFINDI, TZ</Value></Address><Address><Item>MainAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2020-08-30T21:00:00Z</ValidFrom><ValidTo>2020-11-29T21:00:00Z</ValidTo><Value>MUFINDI, CityTownVillage.MUFINDI, Iringa, Mufindi, TZ</Value></Address><Address><Item>SecondaryAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2017-01-30T21:00:00Z</ValidFrom><ValidTo>2017-03-30T21:00:00Z</ValidTo><Value>Kinondoni, CityTownVillage.Kinondoni, Dar Es Salaam, Kinondoni, TZ</Value></Address><Address><Item>SecondaryAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2017-03-30T21:00:00Z</ValidFrom><ValidTo>2020-05-30T21:00:00Z</ValidTo><Value>MBEZI BEACH, CityTownVillage.TANKI BOVU, Dar Es Salaam, Kinondoni, TZ</Value></Address><Address><Item>SecondaryAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2020-05-30T21:00:00Z</ValidFrom><ValidTo>2020-08-30T21:00:00Z</ValidTo><Value>MBEZI BEACH, CityTownVillage.TANKI BOVU, TZ</Value></Address><Address><Item>SecondaryAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2020-08-30T21:00:00Z</ValidFrom><ValidTo>2020-11-29T21:00:00Z</ValidTo><Value>MBEZI BEACH, CityTownVillage.TANKI BOVU, Dar Es Salaam, Kinondoni, TZ</Value></Address></AddressList>"",""generalList"":""<GeneralList xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""><General><Item>Names</Item><Subscriber>B01</Subscriber><ValidFrom>2017-01-30T21:00:00Z</ValidFrom><ValidTo>2017-11-29T21:00:00Z</ValidTo><Value>PRISCA GODWEL MKING'I</Value></General><General><Item>FullName</Item><Subscriber>B01</Subscriber><ValidFrom>2017-01-30T21:00:00Z</ValidFrom><ValidTo>2017-11-29T21:00:00Z</ValidTo><Value>PRISCA GODWEL MKING'I</Value></General><General><Item>BirthSurname</Item><Subscriber>B01</Subscriber><ValidFrom>2017-01-30T21:00:00Z</ValidFrom><ValidTo>2017-11-29T21:00:00Z</ValidTo><Value>MKING'I</Value></General></GeneralList>""}},""extract"":{""nationalId"":0,""date"":""2023-09-27T11:05:32.7199764Z"",""decision"":""Approve"",""mobileScore"":624,""mobileGrade"":""C1"",""age"":35,""dateOfBirth"":""1988-05-13T21:00:00Z"",""mobileTotalBalance"":0,""apd1DPD"":0,""allContacts"":""255717582593255717582593"",""mobilePhone"":""+255717582593"",""scr2Result"":""NotEvaluated"",""scr2Parameter"":530,""scr2Value"":726,""scr4Result"":""NotEvaluated"",""scr4Parameter"":548,""inq2Result"":""NotEvaluated"",""inq2Parameter"":5,""inq2Value"":1,""rsk3Result"":""NotEvaluated"",""rsk3Parameter"":2,""rsk3Value"":0,""rsk4Result"":""NotEvaluated"",""rsk4Parameter"":1,""rsk4Value"":0,""rsk6Result"":""NotEvaluated"",""rsk6Parameter"":3,""rsk6Value"":1,""rsk7Result"":""NotEvaluated"",""rsk7Parameter"":1.1,""rsk10Result"":""NotEvaluated"",""cst1Result"":""NotEvaluated"",""cst1Parameter"":23,""cst1Value"":35,""cst2Result"":""NotEvaluated"",""cst2Parameter"":1000,""cst2Value"":0,""cst3Result"":""NotEvaluated"",""cst3Parameter"":700000,""cst3Value"":0,""cst12Result"":""NotEvaluated"",""cipscore"":726,""cipgrade"":""A3""},""strategy"":{""id"":""438bb263-fb48-470b-b0f1-7e4efa81220d"",""name"":""Mobile Loan Strategy - NMBPLC"",""beeStrategy"":""MobileLoanStrategy"",""templateName"":""TZA_IDM"",""preferEmbedded"":false,""returnOutputDataInSteps"":true}}}"

                Dim mblnIsCRBError As Boolean = False
                Dim dtTable As DataTable = JsonStringToDataTable(mstrReponseData)
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    If CInt(dtTable.Rows(0)("statusCode")) <> 600 Then
                        mblnIsCRBError = True
                    Else
                        mblnIsCRBError = False
                    End If
                End If


                If mdtGetCRBData Is Nothing Then
                    Dim objCRBData As New clsloanapproval_crbdata
                    mdtGetCRBData = objCRBData._DtCRBData.Clone
                    objCRBData = Nothing
                End If


                Dim drExist = mdtGetCRBData.AsEnumerable().Where(Function(x) x.Field(Of Integer)("idtypeunkid") = CInt(dgEmpIdentity.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgEmpIdentity, "objdgcolhIdentityId", False, True)).Text) _
                                                                                            And x.Field(Of Integer)("pendingloanapprovalunkid") = mintPendingLoanAprovalunkid)

                If drExist IsNot Nothing AndAlso drExist.Count <= 0 Then
                    Dim drRow As DataRow = mdtGetCRBData.NewRow
                    drRow("lnloancrbunkid") = -1
                    drRow("pendingloanapprovalunkid") = mintPendingLoanAprovalunkid
                    drRow("processpendingloanunkid") = mintProcesspendingloanunkid
                    drRow("employeeunkid") = CInt(cboEmpName.SelectedValue)
                    drRow("mapuserunkid") = mintMapUserId
                    drRow("idtypeunkid") = CInt(dgEmpIdentity.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgEmpIdentity, "objdgcolhIdentityId", False, True)).Text)
                    drRow("crb_posteddata") = mstrPostedData
                    drRow("crb_responsedata") = mstrReponseData
                    drRow("iscrb_error") = mblnIsCRBError
                    drRow("crb_status") = IIf(mblnIsCRBError, "Error", "Success")
                    drRow("isvoid") = False
                    drRow("voiddatetime") = DBNull.Value
                    drRow("voiduserunkid") = -1
                    drRow("voidreason") = ""
                    mdtGetCRBData.Rows.Add(drRow)
                End If


                If mblnIsCRBError Then
                    dgEmpIdentity.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgEmpIdentity, "dgcolhCRBStatus", False, True)).Text = "Error"
                    dgEmpIdentity.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgEmpIdentity, "dgcolhCRBStatus", False, True)).ForeColor = Drawing.Color.Red
                Else
                    dgEmpIdentity.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgEmpIdentity, "dgcolhCRBStatus", False, True)).Text = "Success"
                    dgEmpIdentity.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgEmpIdentity, "dgcolhCRBStatus", False, True)).ForeColor = Drawing.Color.Green
                End If

                objEmployee = Nothing
                objMasterData = Nothing


            ElseIf e.CommandName.ToUpper = "GETREPORT" Then

                If dgEmpIdentity.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgEmpIdentity, "dgcolhCRBStatus", False, True)).Text = "Error" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 51, "You can not generate this report.Reason : This identity type got error status to get CRB response."), Me)
                    Exit Sub
                ElseIf dgEmpIdentity.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgEmpIdentity, "dgcolhCRBStatus", False, True)).Text = "&nbsp;" OrElse dgEmpIdentity.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgEmpIdentity, "dgcolhCRBStatus", False, True)).Text = "" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 52, "You can not generate this report.Reason : Before Generate Report you have to get CRB response for this identity type."), Me)
                    Exit Sub
                End If


                Dim mstrPostedData As String = ""
                Dim mstrReponseData As String = ""
                Dim strBuilder As New StringBuilder

                If mdtGetCRBData IsNot Nothing AndAlso mdtGetCRBData.Rows.Count > 0 Then

                    Dim drRow() = mdtGetCRBData.Select("idtypeunkid = " & CInt(dgEmpIdentity.Items(e.Item.ItemIndex).Cells(getColumnId_Datagrid(dgEmpIdentity, "objdgcolhIdentityId", False, True)).Text))
                    If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                        mstrPostedData = drRow(0)("crb_posteddata").ToString()
                        mstrReponseData = drRow(0)("crb_responsedata").ToString()
                    End If

                    'DisplayMessage.DisplayMessage(mstrReponseData, Me)

                    'mstrReponseData = "{""statusCode"":""600"",""statusDesc"":""Success"",""body"":{""status"":""ok"",""hitcount"":1,""infomsg"":""ReportGenerated"",""currency"":""TZS"",""generalInformation"":{""subjectIDNumber"":""19900926121010000126"",""requestDate"":""2022-12-04T21:00:00Z"",""referenceNumber"":""2603132-34677137"",""recommendedDecision"":""Approve"",""brokenRules"":0},""personalInformation"":{""fullName"":""Daniel A Bungela"",""dateOfBirth"":""1980-09-26T21:00:00Z"",""age"":42,""gender"":""NotSpecified"",""maritalStatus"":""NotSpecified"",""employmentStatus"":""NotSpecified""},""scoringAnalysis"":{""mobileScore"":250,""mobileScoreRiskGrade"":""E3"",""conclusion"":""<Conclusion xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR4""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR5""}]},""cipscore"":999,""cipriskGrade"":""XX""},""inquiriesAnalysis"":{""totalLast7Days"":5,""totalLast1Month"":41,""nonBankingLast1Month"":0,""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ1""},{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ2""},{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ3""}]},""conclusion"":""<Conclusion xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>""},""currentContracts"":{""currentBanking"":{""positive"":1,""negative"":0,""balance"":0,""balanceAtRisk"":0},""currentNonBanking"":{""positive"":0,""negative"":1,""balance"":0,""balanceAtRisk"":0},""total"":{""positive"":1,""negative"":1,""balance"":0,""balanceAtRisk"":0}},""pastDueInformation"":{""totalCurrentPastDue"":30000,""totalCurrentDaysPastDue"":63,""worstCurrentPastDue"":30000.0,""worstCurrentDaysPastDue"":63,""worstPastDueLast12Months"":0,""worstPastDueDaysLast12Months"":0,""monthsWithoutArrearsLast12Months"":0,""totalMonthsWithHistoryLast12Months"":0,""percMonthsWithoutArrearsLast12Months"":0},""repaymentInformation"":{""totalMonthlyPayment"":0,""nextContractMaturesInMonths"":0,""allContractsMatureInMonths"":0,""lastContractOpened"":1471467600000,""closedContracts"":0},""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK4""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK5""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK6""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK7""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK8""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK9""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK10""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK11""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST12""}]},""tzaCb5Data"":{""bouncedCheques"":{""chequeList"":null},""branches"":{""branchList"":null,""numberOfBranches"":0},""cip"":{""recordList"":{""record"":[{""date"":""2022-12-05T12:25:11.6105252Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XDAT"",""description"":""Not enough data on borrower contracts to calculate the score""},{""code"":""MSM5"",""description"":""3 or more instalments overdue more than 1 year ago""},{""code"":""NQS2"",""description"":""3 or more subscribers inquired the report on this person""}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-11-29T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XDAT"",""description"":null},{""code"":""MSM5"",""description"":null},{""code"":""NQS2"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-10-30T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100.0,""reasonsList"":{""reason"":[{""code"":""XDAT"",""description"":null},{""code"":""MSM5"",""description"":null},{""code"":""NQS2"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-09-29T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100.0,""reasonsList"":{""reason"":[{""code"":""XDAT"",""description"":null},{""code"":""MSM5"",""description"":null},{""code"":""NQS2"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-08-30T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100.0,""reasonsList"":{""reason"":[{""code"":""XDAT"",""description"":null},{""code"":""MSM5"",""description"":null},{""code"":""NQS2"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-07-30T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100.0,""reasonsList"":{""reason"":[{""code"":""XDAT"",""description"":null},{""code"":""MSM5"",""description"":null},{""code"":""NQS2"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-06-29T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100.0,""reasonsList"":{""reason"":[{""code"":""XDAT"",""description"":null},{""code"":""MSM5"",""description"":null},{""code"":""NQS2"",""description"":null}]},""score"":999,""trend"":""Up""},{""date"":""2022-05-30T21:00:00Z"",""grade"":""D3"",""probabilityOfDefault"":26.57,""reasonsList"":{""reason"":[{""code"":""HMP2"",""description"":null},{""code"":""MSM4"",""description"":null},{""code"":""LWM2"",""description"":null},{""code"":""NQS2"",""description"":null}]},""score"":574,""trend"":""NoChange""},{""date"":""2022-04-29T21:00:00Z"",""grade"":""D3"",""probabilityOfDefault"":26.57,""reasonsList"":{""reason"":[{""code"":""HMP2"",""description"":null},{""code"":""MSM4"",""description"":null},{""code"":""LWM2"",""description"":null},{""code"":""NQS3"",""description"":null}]},""score"":574,""trend"":""NoChange""},{""date"":""2022-03-30T21:00:00Z"",""grade"":""D3"",""probabilityOfDefault"":26.57,""reasonsList"":{""reason"":[{""code"":""HMP2"",""description"":null},{""code"":""MSM4"",""description"":null},{""code"":""LWM2"",""description"":null},{""code"":""NQS3"",""description"":null}]},""score"":574,""trend"":""NoChange""},{""date"":""2022-02-27T21:00:00Z"",""grade"":""D3"",""probabilityOfDefault"":26.57,""reasonsList"":{""reason"":[{""code"":""HMP2"",""description"":null},{""code"":""MSM4"",""description"":null},{""code"":""LWM2"",""description"":null}]},""score"":574,""trend"":""NoChange""},{""date"":""2022-01-30T21:00:00Z"",""grade"":""D3"",""probabilityOfDefault"":26.57,""reasonsList"":{""reason"":[{""code"":""HMP2"",""description"":null},{""code"":""MSM4"",""description"":null},{""code"":""LWM2"",""description"":null}]},""score"":574,""trend"":""NoChange""},{""date"":""2021-12-30T21:00:00Z"",""grade"":""D3"",""probabilityOfDefault"":26.57,""reasonsList"":{""reason"":[{""code"":""HMP2"",""description"":null},{""code"":""MSM4"",""description"":null},{""code"":""LWM2"",""description"":null},{""code"":""NQS3"",""description"":null}]},""score"":574,""trend"":""Up""}]}},""ciq"":{""detail"":{""lostStolenRecordsFound"":0,""numberOfCancelledClosedContracts"":0,""numberOfSubscribersMadeInquiriesLast14Days"":3,""numberOfSubscribersMadeInquiriesLast2Days"":0},""summary"":{""numberOfFraudAlertsPrimarySubject"":0,""numberOfFraudAlertsThirdParty"":0}},""contractOverview"":{""contractList"":{""contract"":[{""contractStatus"":""GrantedAndActivated"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":30000.0,""value"":30000.0},""pastDueDays"":63,""phaseOfContract"":""Open"",""roleOfClient"":""MainDebtor"",""sector"":""Leasing"",""startDate"":""2016-08-17T21:00:00Z"",""totalAmount"":{""currency"":""TZS"",""localValue"":50000.0,""value"":50000.0},""typeOfContract"":""NotSpecified""},{""contractStatus"":""GrantedAndActivated"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""phaseOfContract"":""Open"",""roleOfClient"":""MainDebtor"",""sector"":""Banks"",""startDate"":""2016-08-17T21:00:00Z"",""totalAmount"":{""currency"":""TZS"",""localValue"":4444100.0,""value"":4444100.0},""typeOfContract"":""NonInstallment""}]}},""contractSummary"":{""affordabilityHistoryList"":{""affordabilityHistory"":[{""month"":12,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":11,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":10,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":9,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":8,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":7,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":6,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":5,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":4,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":3,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":2,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":1,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022}]},""affordabilitySummary"":{""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0}},""debtor"":{""closedContracts"":0,""openContracts"":2,""outstandingAmountSum"":null,""pastDueAmountSum"":{""currency"":""TZS"",""localValue"":30000.0,""value"":30000.0},""totalAmountSum"":{""currency"":""TZS"",""localValue"":4494100.0,""value"":4494100.0}},""guarantor"":{""closedContracts"":0,""openContracts"":0},""overall"":{""lastDelinquency90PlusDays"":null,""maxDueInstallmentsClosedContracts"":0,""maxDueInstallmentsOpenContracts"":0,""worstPastDueAmount"":{""currency"":""TZS"",""localValue"":34858.0,""value"":34858.0},""worstPastDueDays"":63},""paymentCalendarList"":{""paymentCalendar"":[{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-12-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-11-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-10-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-09-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-08-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-07-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-06-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-05-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-04-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-03-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-02-27T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-01-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-12-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-11-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-10-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-09-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-08-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-07-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-06-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""SubStandard"",""contractsSubmitted"":1,""date"":""2021-05-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":30000.0,""value"":30000.0},""pastDueDays"":63,""pastDueInstallments"":2},{""classification"":""Watch"",""contractsSubmitted"":2,""date"":""2021-04-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":10000.0,""value"":10000.0},""pastDueDays"":32,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2021-03-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2021-02-27T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2021-01-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-12-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-11-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-10-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-09-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-08-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-07-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-06-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-05-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-04-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-03-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-02-28T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-01-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null}]},""sectorInfoList"":{""sectorInfo"":{""debtorClosedContracts"":0,""debtorOpenContracts"":1,""debtorOutstandingAmountSum"":null,""debtorPastDueAmountSum"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""debtorTotalAmountSum"":{""currency"":""TZS"",""localValue"":4444100.0,""value"":4444100.0},""guarantorClosedContracts"":0,""guarantorOpenContracts"":0,""sector"":""Banks""}}},""contracts"":{""contractList"":{""contract"":[{""collateralsList"":null,""contractStatus"":""GrantedAndActivated"",""contractSubtype"":""NotSpecified"",""currencyOfContract"":""TZS"",""disputes"":{""closedDisputes"":0,""disputeList"":null,""falseDisputes"":0},""expectedEndDate"":null,""methodOfPayment"":""NotSpecified"",""negativeStatusOfContract"":""NotSpecified"",""numberOfDueInstallments"":2,""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":30000.0,""value"":30000.0},""pastDueDays"":63,""paymentCalendarList"":{""calendarItem"":[{""date"":""2022-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-11-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-09-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-08-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-07-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-06-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-05-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-04-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-03-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-02-27T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-01-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-11-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-09-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-08-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-07-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-06-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-05-30T21:00:00Z"",""delinquencyStatus"":""Warning"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":30000.0,""value"":30000.0},""pastDueDays"":63,""pastDueInstallments"":2},{""date"":""2021-04-29T21:00:00Z"",""delinquencyStatus"":""Warning"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":10000.0,""value"":10000.0},""pastDueDays"":32,""pastDueInstallments"":0},{""date"":""2021-03-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-02-27T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-01-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2020-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-11-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-09-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-08-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-07-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-06-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-05-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-04-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-03-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-02-28T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-01-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null}]},""paymentIncidentList"":null,""paymentPeriodicity"":""Days30"",""phaseOfContract"":""Open"",""purposeOfFinancing"":""NotSpecified"",""relatedSubjectsList"":null,""roleOfClient"":""MainDebtor"",""startDate"":""2016-08-17T21:00:00Z"",""subscriber"":""MOBISOL UK LIMITED"",""subscriberType"":""Leasing"",""totalAmount"":{""currency"":""TZS"",""localValue"":50000.0,""value"":50000.0},""typeOfContract"":""NotSpecified"",""worstPastDueAmount"":{""currency"":""TZS"",""localValue"":34858.0,""value"":34858.0}},{""collateralsList"":null,""contractStatus"":""GrantedAndActivated"",""contractSubtype"":""NotSpecified"",""currencyOfContract"":""TZS"",""disputes"":{""closedDisputes"":0,""disputeList"":null,""falseDisputes"":0},""expectedEndDate"":null,""methodOfPayment"":""NotSpecified"",""negativeStatusOfContract"":""NotSpecified"",""numberOfDueInstallments"":0,""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""paymentCalendarList"":{""calendarItem"":[{""date"":""2022-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-11-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-09-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-08-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-07-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-06-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-05-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-04-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-03-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-02-27T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-01-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-11-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-09-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-08-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-07-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-06-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-05-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-04-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":null,""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-03-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-02-27T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-01-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null}]},""paymentIncidentList"":null,""paymentPeriodicity"":""Days30"",""phaseOfContract"":""Open"",""purposeOfFinancing"":""NotSpecified"",""relatedSubjectsList"":null,""roleOfClient"":""MainDebtor"",""startDate"":""2016-08-17T21:00:00Z"",""subscriber"":""JUMO LIMITED"",""subscriberType"":""Banks"",""totalAmount"":{""currency"":""TZS"",""localValue"":4444100.0,""value"":4444100.0},""typeOfContract"":""NonInstallment"",""worstPastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0}}]}},""currentRelations"":{""contractRelationList"":null,""involvementList"":null,""relatedPartyList"":null},""dashboard"":{""cip"":{""grade"":""XX"",""score"":999},""ciq"":{""fraudAlerts"":0,""fraudAlertsThirdParty"":0},""collaterals"":{""highestCollateralValue"":{""currency"":""TZS"",""localValue"":0,""value"":0},""highestCollateralValueType"":""NotSpecified"",""numberOfCollaterals"":0,""totalCollateralValue"":{""currency"":""TZS"",""localValue"":0,""value"":0}},""disputes"":{""activeContractDisputes"":0,""activeSubjectDisputes"":0,""closedContractDisputes"":0,""closedSubjectDisputes"":0,""falseDisputes"":0},""inquiries"":{""inquiriesForLast12Months"":134},""paymentsProfile"":{""numberOfDifferentSubscribers"":2,""pastDueAmountSum"":{""currency"":""TZS"",""localValue"":30000.0,""value"":30000.0},""worstPastDueDaysCurrent"":63,""worstPastDueDaysForLast12Months"":0},""relations"":{""numberOfInvolvements"":0,""numberOfRelations"":0}},""disputes"":{""disputeList"":null,""summary"":{""numberOfActiveDisputesContracts"":0,""numberOfActiveDisputesInCourt"":0,""numberOfActiveDisputesSubjectData"":0,""numberOfClosedDisputesContracts"":0,""numberOfClosedDisputesSubjectData"":0,""numberOfFalseDisputes"":0}},""individual"":{""contact"":{""mobilePhone"":""+255745038444""},""general"":{""birthSurname"":null,""citizenship"":""NotSpecified"",""classificationOfIndividual"":""NotSpecified"",""countryOfBirth"":""TZ"",""dateOfBirth"":""1980-09-26T21:00:00Z"",""education"":""NotSpecified"",""employment"":""NotSpecified"",""firstName"":null,""fullName"":""Daniel A Bungela"",""gender"":""NotSpecified"",""maritalStatus"":""NotSpecified"",""nationality"":""NotSpecified"",""negativeStatus"":""NotSpecified""},""identifications"":{""passportIssuerCountry"":""NotSpecified"",""wardID"":0},""mainAddress"":{""country"":""NotSpecified""},""secondaryAddress"":{""country"":""NotSpecified""}},""inquiries"":{""inquiryList"":{""inquiry"":[{""dateOfInquiry"":""2022-12-02T12:27:21.833Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""JUMO LIMITED""},{""dateOfInquiry"":""2022-12-02T09:10:00.803Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""JUMO LIMITED""},{""dateOfInquiry"":""2022-12-02T08:31:31.007Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""JUMO LIMITED""},{""dateOfInquiry"":""2022-12-02T08:14:53.45Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""JUMO LIMITED""},{""dateOfInquiry"":""2022-12-02T08:09:46.02Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""JUMO LIMITED""}]},""summary"":{""numberOfInquiriesLast12Months"":134,""numberOfInquiriesLast1Month"":41,""numberOfInquiriesLast24Months"":137,""numberOfInquiriesLast3Months"":101,""numberOfInquiriesLast6Months"":113}},""managers"":{""managerList"":null,""numberOfManagers"":0},""parameters"":{""consent"":true,""inquiryReason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""reportDate"":""2022-12-05T12:25:11.6105252Z"",""sections"":{""string"":""CreditinfoReportPlus""},""subjectType"":""Individual"",""idnumber"":34677137,""idnumberType"":""CreditinfoId""},""paymentIncidentList"":{""paymentIncidentList"":null,""summary"":{""dueAmountSummary"":{""currency"":""TZS"",""localValue"":0,""value"":0},""outstandingAmountSummary"":{""currency"":""TZS"",""localValue"":0,""value"":0}}},""reportInfo"":{""created"":""2022-12-05T12:25:11.938657Z"",""referenceNumber"":""2603132-34677137"",""reportStatus"":""ReportGenerated"",""requestedBy"":""Nmb Idm"",""subscriber"":""NMBPLC"",""version"":573},""shareholders"":{""numberOfShareholders"":0,""shareholderList"":null},""subjectInfoHistory"":{""addressList"":""<AddressList xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""><Address><Item>MainAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2019-03-30T21:00:00Z</ValidFrom><ValidTo>2019-04-29T21:00:00Z</ValidTo><Value>Mkula/ Busega/ Simiyu</Value></Address><Address><Item>MainAddressLine</Item><Subscriber>B01</Subscriber><ValidFrom>2019-03-30T21:00:00Z</ValidFrom><ValidTo>2019-04-29T21:00:00Z</ValidTo><Value>Mkula/ Busega/ Simiyu</Value></Address></AddressList>"",""contactList"":null,""generalList"":""<GeneralList xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""><General><Item>Banking.CountryOfBirth</Item><Subscriber>B01</Subscriber><ValidFrom>2019-03-30T21:00:00Z</ValidFrom><ValidTo>2021-01-30T21:00:00Z</ValidTo><Value>TZ</Value></General></GeneralList>"",""identificationsList"":null}},""extract"":{""nationalId"":0,""date"":""2022-12-05T12:25:12.7780646Z"",""decision"":""Approve"",""mobileScore"":250,""mobileGrade"":""E3"",""age"":42,""dateOfBirth"":""1980-09-26T21:00:00Z"",""mobileTotalBalance"":0,""apd1DPD"":0,""allContacts"":255745038444,""mobilePhone"":""+255745038444"",""scr2Result"":""NotEvaluated"",""scr2Parameter"":530,""scr2Value"":999,""scr4Result"":""NotEvaluated"",""scr4Parameter"":548,""inq2Result"":""NotEvaluated"",""inq2Parameter"":5,""inq2Value"":1,""rsk3Result"":""NotEvaluated"",""rsk3Parameter"":2,""rsk3Value"":0,""rsk4Result"":""NotEvaluated"",""rsk4Parameter"":1,""rsk4Value"":0,""rsk6Result"":""NotEvaluated"",""rsk6Parameter"":3,""rsk6Value"":0,""rsk7Result"":""NotEvaluated"",""rsk7Parameter"":1.1,""rsk10Result"":""NotEvaluated"",""cst1Result"":""NotEvaluated"",""cst1Parameter"":23,""cst1Value"":42,""cst2Result"":""NotEvaluated"",""cst2Parameter"":1000,""cst2Value"":0,""cst3Result"":""NotEvaluated"",""cst3Parameter"":700000,""cst3Value"":0,""cst12Result"":""NotEvaluated"",""cipgrade"":""XX"",""cipscore"":999},""strategy"":{""id"":""8c9e41cd-b781-4763-86d5-7825cb7fd10a"",""name"":""JUMO Mobile Loan Strategy"",""beeStrategy"":""Z_MobileLoanStrategy"",""templateName"":""TZA_IDM_NEW_STANDARD"",""preferEmbedded"":false,""returnOutputDataInSteps"":false}}}"
                    'mstrReponseData = "{""statusCode"":""600"",""statusDesc"":""Success"",""body"":{""status"":""ok"",""hitcount"":0,""infomsg"":"""",""currency"":""TZS"",""generalInformation"":{""subjectIDNumber"":""emptyID"",""requestDate"":""2022-12-12T21:00:00Z"",""referenceNumber"":"""",""recommendedDecision"":""Approve"",""brokenRules"":0},""personalInformation"":{""fullName"":null,""dateOfBirth"":null,""age"":0,""gender"":null,""maritalStatus"":null,""employmentStatus"":null},""scoringAnalysis"":{""mobileScore"":999,""mobileScoreRiskGrade"":""XX"",""conclusion"":""<Conclusion xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR4""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR5""}]},""cipscore"":999,""cipriskGrade"":""XX""},""inquiriesAnalysis"":{""totalLast7Days"":0,""totalLast1Month"":0,""nonBankingLast1Month"":0,""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ1""},{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ2""},{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ3""}]},""conclusion"":""<Conclusion xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>""},""currentContracts"":{""currentBanking"":{""positive"":0,""negative"":0,""balance"":0,""balanceAtRisk"":0},""currentNonBanking"":{""positive"":0,""negative"":0,""balance"":0,""balanceAtRisk"":0},""total"":{""positive"":0,""negative"":0,""balance"":0,""balanceAtRisk"":0}},""pastDueInformation"":{""totalCurrentPastDue"":0,""totalCurrentDaysPastDue"":0,""worstCurrentPastDue"":0,""worstCurrentDaysPastDue"":0,""worstPastDueLast12Months"":0,""worstPastDueDaysLast12Months"":0,""monthsWithoutArrearsLast12Months"":0,""totalMonthsWithHistoryLast12Months"":0,""percMonthsWithoutArrearsLast12Months"":0},""repaymentInformation"":{""totalMonthlyPayment"":0,""nextContractMaturesInMonths"":0,""allContractsMatureInMonths"":0,""lastContractOpened"":null,""closedContracts"":0},""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK4""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK5""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK6""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK7""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK8""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK9""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK10""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK11""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST12""}]},""tzaCb5Data"":null,""extract"":{""nationalId"":0,""date"":""2022-12-13T12:49:55.2403611Z"",""decision"":""Approve"",""mobileScore"":999,""mobileGrade"":""XX"",""age"":0,""dateOfBirth"":null,""mobileTotalBalance"":0,""apd1DPD"":0,""allContacts"":null,""mobilePhone"":""+0767170204"",""scr2Result"":""NotEvaluated"",""scr2Parameter"":530,""scr2Value"":999,""scr4Result"":""NotEvaluated"",""scr4Parameter"":548,""inq2Result"":""NotEvaluated"",""inq2Parameter"":5,""inq2Value"":0,""rsk3Result"":""NotEvaluated"",""rsk3Parameter"":2,""rsk3Value"":0,""rsk4Result"":""NotEvaluated"",""rsk4Parameter"":1,""rsk4Value"":0,""rsk6Result"":""NotEvaluated"",""rsk6Parameter"":3,""rsk6Value"":0,""rsk7Result"":""NotEvaluated"",""rsk7Parameter"":1.1,""rsk10Result"":""NotEvaluated"",""cst1Result"":""NotEvaluated"",""cst1Parameter"":23,""cst1Value"":0,""cst2Result"":""NotEvaluated"",""cst2Parameter"":1000,""cst2Value"":0,""cst3Result"":""NotEvaluated"",""cst3Parameter"":700000,""cst3Value"":0,""cst12Result"":""NotEvaluated"",""cipgrade"":""XX"",""cipscore"":999},""strategy"":{""id"":""8c9e41cd-b781-4763-86d5-7825cb7fd10a"",""name"":""JUMO Mobile Loan Strategy"",""beeStrategy"":""Z_MobileLoanStrategy"",""templateName"":""TZA_IDM_NEW_STANDARD"",""preferEmbedded"":false,""returnOutputDataInSteps"":false}}}"
                    'mstrReponseData = "{""statusCode"":""600"",""statusDesc"":""Success"",""body"":{""status"":""ok"",""hitcount"":1,""infomsg"":""ReportGenerated"",""currency"":""TZS"",""generalInformation"":{""subjectIDNumber"":""0000"",""requestDate"":""2022-12-12T21:00:00Z"",""referenceNumber"":""2621543-81343625"",""recommendedDecision"":""Approve"",""brokenRules"":0},""personalInformation"":{""fullName"":""ZUBERI LUTOZI"",""dateOfBirth"":""1967-12-31T21:00:00Z"",""age"":54,""gender"":""NotSpecified"",""maritalStatus"":""NotSpecified"",""employmentStatus"":""NotSpecified""},""scoringAnalysis"":{""mobileScore"":999,""mobileScoreRiskGrade"":""XX"",""conclusion"":""<Conclusion xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR4""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR5""}]},""cipscore"":999,""cipriskGrade"":""XX""},""inquiriesAnalysis"":{""totalLast7Days"":5,""totalLast1Month"":9,""nonBankingLast1Month"":0,""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ1""},{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ2""},{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ3""}]},""conclusion"":""<Conclusion xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>""},""currentContracts"":{""currentBanking"":{""positive"":0,""negative"":0,""balance"":0,""balanceAtRisk"":0},""currentNonBanking"":{""positive"":0,""negative"":0,""balance"":0,""balanceAtRisk"":0},""total"":{""positive"":0,""negative"":0,""balance"":0,""balanceAtRisk"":0}},""pastDueInformation"":{""totalCurrentPastDue"":0,""totalCurrentDaysPastDue"":0,""worstCurrentPastDue"":0,""worstCurrentDaysPastDue"":0,""worstPastDueLast12Months"":0,""worstPastDueDaysLast12Months"":0,""monthsWithoutArrearsLast12Months"":0,""totalMonthsWithHistoryLast12Months"":0,""percMonthsWithoutArrearsLast12Months"":0},""repaymentInformation"":{""totalMonthlyPayment"":0,""nextContractMaturesInMonths"":0,""allContractsMatureInMonths"":0,""lastContractOpened"":null,""closedContracts"":0},""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK4""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK5""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK6""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK7""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK8""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK9""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK10""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK11""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST12""}]},""tzaCb5Data"":{""bouncedCheques"":{""chequeList"":null},""branches"":{""branchList"":null,""numberOfBranches"":0},""cip"":{""recordList"":{""record"":[{""date"":""2022-12-13T15:33:10.4948448Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":""Subject is not found in database""},{""code"":""XNOC"",""description"":""No contract in the database""},{""code"":""XDAT"",""description"":""Not enough data on borrower contracts to calculate the score""}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-11-29T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":null},{""code"":""XNOC"",""description"":null},{""code"":""XDAT"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-10-30T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":null},{""code"":""XNOC"",""description"":null},{""code"":""XDAT"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-09-29T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":null},{""code"":""XNOC"",""description"":null},{""code"":""XDAT"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-08-30T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":null},{""code"":""XNOC"",""description"":null},{""code"":""XDAT"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-07-30T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":null},{""code"":""XNOC"",""description"":null},{""code"":""XDAT"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-06-29T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":null},{""code"":""XNOC"",""description"":null},{""code"":""XDAT"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-05-30T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":null},{""code"":""XNOC"",""description"":null},{""code"":""XDAT"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-04-29T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":null},{""code"":""XNOC"",""description"":null},{""code"":""XDAT"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-03-30T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":null},{""code"":""XNOC"",""description"":null},{""code"":""XDAT"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-02-27T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":null},{""code"":""XNOC"",""description"":null},{""code"":""XDAT"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-01-30T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":null},{""code"":""XNOC"",""description"":null},{""code"":""XDAT"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2021-12-30T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XNOF"",""description"":null},{""code"":""XNOC"",""description"":null},{""code"":""XDAT"",""description"":null}]},""score"":999,""trend"":""Up""}]}},""ciq"":{""detail"":{""lostStolenRecordsFound"":0,""numberOfCancelledClosedContracts"":0,""numberOfSubscribersMadeInquiriesLast14Days"":1,""numberOfSubscribersMadeInquiriesLast2Days"":1},""summary"":{""numberOfFraudAlertsPrimarySubject"":0,""numberOfFraudAlertsThirdParty"":0}},""contractOverview"":{""contractList"":null},""contractSummary"":{""affordabilityHistoryList"":{""affordabilityHistory"":[{""month"":12,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":11,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":10,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":9,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":8,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":7,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":6,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":5,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":4,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":3,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":2,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":1,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022}]},""affordabilitySummary"":{""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0}},""debtor"":{""closedContracts"":0,""openContracts"":0,""outstandingAmountSum"":null,""pastDueAmountSum"":null,""totalAmountSum"":null},""guarantor"":{""closedContracts"":0,""openContracts"":0},""overall"":{""lastDelinquency90PlusDays"":null,""maxDueInstallmentsClosedContracts"":0,""maxDueInstallmentsOpenContracts"":0,""worstPastDueAmount"":{""currency"":""TZS"",""localValue"":0,""value"":0},""worstPastDueDays"":0},""paymentCalendarList"":null,""sectorInfoList"":null},""contracts"":{""contractList"":null},""currentRelations"":{""contractRelationList"":null,""involvementList"":null,""relatedPartyList"":null},""dashboard"":{""cip"":{""grade"":""XX"",""score"":999},""ciq"":{""fraudAlerts"":0,""fraudAlertsThirdParty"":0},""collaterals"":{""highestCollateralValue"":null,""highestCollateralValueType"":""NotSpecified"",""numberOfCollaterals"":0,""totalCollateralValue"":null},""disputes"":{""activeContractDisputes"":0,""activeSubjectDisputes"":0,""closedContractDisputes"":0,""closedSubjectDisputes"":0,""falseDisputes"":0},""inquiries"":{""inquiriesForLast12Months"":9},""paymentsProfile"":{""numberOfDifferentSubscribers"":0,""pastDueAmountSum"":null,""worstPastDueDaysCurrent"":0,""worstPastDueDaysForLast12Months"":0},""relations"":{""numberOfInvolvements"":0,""numberOfRelations"":0}},""disputes"":{""disputeList"":null,""summary"":{""numberOfActiveDisputesContracts"":0,""numberOfActiveDisputesInCourt"":0,""numberOfActiveDisputesSubjectData"":0,""numberOfClosedDisputesContracts"":0,""numberOfClosedDisputesSubjectData"":0,""numberOfFalseDisputes"":0}},""individual"":{""contact"":{""mobilePhone"":null},""general"":{""birthSurname"":null,""citizenship"":""NotSpecified"",""classificationOfIndividual"":""NotSpecified"",""countryOfBirth"":""NotSpecified"",""dateOfBirth"":""1967-12-31T21:00:00Z"",""education"":""NotSpecified"",""employment"":""NotSpecified"",""firstName"":""ZUBERI"",""fullName"":""ZUBERI LUTOZI"",""gender"":""NotSpecified"",""maritalStatus"":""NotSpecified"",""nationality"":""NotSpecified"",""negativeStatus"":""NotSpecified""},""identifications"":{""passportIssuerCountry"":""NotSpecified"",""wardID"":0},""mainAddress"":{""country"":""NotSpecified""},""secondaryAddress"":{""country"":""NotSpecified""}},""inquiries"":{""inquiryList"":{""inquiry"":[{""dateOfInquiry"":""2022-12-13T15:20:26.59Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""NMBPLC""},{""dateOfInquiry"":""2022-12-13T15:10:22.177Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""NMBPLC""},{""dateOfInquiry"":""2022-12-13T14:58:06.303Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""NMBPLC""},{""dateOfInquiry"":""2022-12-13T14:45:46.667Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""NMBPLC""},{""dateOfInquiry"":""2022-12-13T13:14:50.397Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""NMBPLC""}]},""summary"":{""numberOfInquiriesLast12Months"":9,""numberOfInquiriesLast1Month"":9,""numberOfInquiriesLast24Months"":9,""numberOfInquiriesLast3Months"":9,""numberOfInquiriesLast6Months"":9}},""managers"":{""managerList"":null,""numberOfManagers"":0},""parameters"":{""consent"":true,""inquiryReason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""reportDate"":""2022-12-13T15:33:10.4948448Z"",""sections"":{""string"":""CreditinfoReportPlus""},""subjectType"":""Individual"",""idnumber"":81343625,""idnumberType"":""CreditinfoId""},""paymentIncidentList"":{""paymentIncidentList"":null,""summary"":{""dueAmountSummary"":{""currency"":""TZS"",""localValue"":0,""value"":0},""outstandingAmountSummary"":{""currency"":""TZS"",""localValue"":0,""value"":0}}},""reportInfo"":{""created"":""2022-12-13T15:33:11.8864853Z"",""referenceNumber"":""2621543-81343625"",""reportStatus"":""ReportGenerated"",""requestedBy"":""Nmb Idm"",""subscriber"":""NMBPLC"",""version"":573},""shareholders"":{""numberOfShareholders"":0,""shareholderList"":null},""subjectInfoHistory"":{""addressList"":null,""contactList"":null,""generalList"":null,""identificationsList"":null}},""extract"":{""nationalId"":0,""date"":""2022-12-13T15:33:11.9991403Z"",""decision"":""Approve"",""mobileScore"":999,""mobileGrade"":""XX"",""age"":54,""dateOfBirth"":""1967-12-31T21:00:00Z"",""mobileTotalBalance"":0,""apd1DPD"":0,""allContacts"":255753105223,""mobilePhone"":""+255767170204"",""scr2Result"":""NotEvaluated"",""scr2Parameter"":530,""scr2Value"":999,""scr4Result"":""NotEvaluated"",""scr4Parameter"":548,""inq2Result"":""NotEvaluated"",""inq2Parameter"":5,""inq2Value"":1,""rsk3Result"":""NotEvaluated"",""rsk3Parameter"":2,""rsk3Value"":0,""rsk4Result"":""NotEvaluated"",""rsk4Parameter"":1,""rsk4Value"":0,""rsk6Result"":""NotEvaluated"",""rsk6Parameter"":3,""rsk6Value"":0,""rsk7Result"":""NotEvaluated"",""rsk7Parameter"":1.1,""rsk10Result"":""NotEvaluated"",""cst1Result"":""NotEvaluated"",""cst1Parameter"":23,""cst1Value"":54,""cst2Result"":""NotEvaluated"",""cst2Parameter"":1000,""cst2Value"":0,""cst3Result"":""NotEvaluated"",""cst3Parameter"":700000,""cst3Value"":0,""cst12Result"":""NotEvaluated"",""cipgrade"":""XX"",""cipscore"":999},""strategy"":{""id"":""8c9e41cd-b781-4763-86d5-7825cb7fd10a"",""name"":""JUMO Mobile Loan Strategy"",""beeStrategy"":""Z_MobileLoanStrategy"",""templateName"":""TZA_IDM_NEW_STANDARD"",""preferEmbedded"":false,""returnOutputDataInSteps"":false}}}"
                    'mstrReponseData = "{""statusCode"":""600"",""statusDesc"":""Success"",""body"":{""status"":""ok"",""hitcount"":1,""infomsg"":""ReportGenerated"",""currency"":""TZS"",""generalInformation"":{""subjectIDNumber"":""19800520303170000111"",""requestDate"":""2023-03-09T21:00:00Z"",""referenceNumber"":""28695908-676610"",""recommendedDecision"":""Approve"",""brokenRules"":0},""personalInformation"":{""fullName"":""Elizabeth JOHN Kanjagaile"",""dateOfBirth"":""1980-05-19T21:00:00Z"",""age"":42,""gender"":""Female"",""maritalStatus"":""Single"",""employmentStatus"":""Banker""},""scoringAnalysis"":{""mobileScore"":625,""mobileScoreRiskGrade"":""C1"",""conclusion"":""<Conclusion xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR4""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""SCR5""}]},""cipscore"":726,""cipriskGrade"":""A3""},""inquiriesAnalysis"":{""totalLast7Days"":4,""totalLast1Month"":4,""nonBankingLast1Month"":0,""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ1""},{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ2""},{""result"":""NotEvaluated"",""description"":""<Description xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>"",""id"":""INQ3""}]},""conclusion"":""<Conclusion xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""/>""},""currentContracts"":{""currentBanking"":{""positive"":1,""negative"":0,""balance"":1.5535746214E8,""balanceAtRisk"":0},""currentNonBanking"":{""positive"":0,""negative"":0,""balance"":0,""balanceAtRisk"":0},""total"":{""positive"":1,""negative"":0,""balance"":1.5535746214E8,""balanceAtRisk"":0}},""pastDueInformation"":{""totalCurrentPastDue"":0,""totalCurrentDaysPastDue"":0,""worstCurrentPastDue"":0.0,""worstCurrentDaysPastDue"":0,""worstPastDueLast12Months"":0.0,""worstPastDueDaysLast12Months"":0,""monthsWithoutArrearsLast12Months"":8,""totalMonthsWithHistoryLast12Months"":10,""percMonthsWithoutArrearsLast12Months"":80},""repaymentInformation"":{""totalMonthlyPayment"":0,""nextContractMaturesInMonths"":-8,""allContractsMatureInMonths"":88,""lastContractOpened"":1657054800000,""closedContracts"":1},""policyRules"":{""rule"":[{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK4""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK5""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK6""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK7""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK8""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK9""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK10""},{""result"":""NotEvaluated"",""description"":""Rule is disabled. "",""id"":""RSK11""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST1""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST2""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST3""},{""result"":""NotEvaluated"",""description"":""Rule is disabled."",""id"":""CST12""}]},""tzaCb5Data"":{""bouncedCheques"":{""chequeList"":null},""branches"":{""branchList"":null,""numberOfBranches"":0},""cip"":{""recordList"":{""record"":[{""date"":""2023-03-10T07:30:37.4793329Z"",""grade"":""A3"",""probabilityOfDefault"":1.07,""reasonsList"":null,""score"":726,""trend"":""NoChange""},{""date"":""2023-02-27T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.14,""reasonsList"":null,""score"":723,""trend"":""Up""},{""date"":""2023-01-30T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.4,""reasonsList"":{""reason"":[{""code"":""NCS3"",""description"":null}]},""score"":714,""trend"":""NoChange""},{""date"":""2022-12-30T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.4,""reasonsList"":{""reason"":[{""code"":""NCS3"",""description"":null}]},""score"":714,""trend"":""NoChange""},{""date"":""2022-11-29T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.4,""reasonsList"":{""reason"":[{""code"":""NCS3"",""description"":null}]},""score"":714,""trend"":""NoChange""},{""date"":""2022-10-30T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.31,""reasonsList"":{""reason"":[{""code"":""NCS3"",""description"":null}]},""score"":717,""trend"":""NoChange""},{""date"":""2022-09-29T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.31,""reasonsList"":{""reason"":[{""code"":""NCS3"",""description"":null}]},""score"":717,""trend"":""NoChange""},{""date"":""2022-08-30T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.31,""reasonsList"":{""reason"":[{""code"":""NCS3"",""description"":null}]},""score"":717,""trend"":""NoChange""},{""date"":""2022-07-30T21:00:00Z"",""grade"":""A3"",""probabilityOfDefault"":1.31,""reasonsList"":{""reason"":[{""code"":""NCS3"",""description"":null}]},""score"":717,""trend"":""Down""},{""date"":""2022-06-29T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XDAT"",""description"":null},{""code"":""NCS3"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-05-30T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XDAT"",""description"":null},{""code"":""NBC4"",""description"":null},{""code"":""NCS3"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-04-29T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XDAT"",""description"":null},{""code"":""NBC4"",""description"":null}]},""score"":999,""trend"":""NoChange""},{""date"":""2022-03-30T21:00:00Z"",""grade"":""XX"",""probabilityOfDefault"":100,""reasonsList"":{""reason"":[{""code"":""XDAT"",""description"":null},{""code"":""NBC4"",""description"":null}]},""score"":999,""trend"":""Up""}]}},""ciq"":{""detail"":{""lostStolenRecordsFound"":0,""numberOfCancelledClosedContracts"":0,""numberOfSubscribersMadeInquiriesLast14Days"":1,""numberOfSubscribersMadeInquiriesLast2Days"":1},""summary"":{""numberOfFraudAlertsPrimarySubject"":0,""numberOfFraudAlertsThirdParty"":0}},""contractOverview"":{""contractList"":{""contract"":[{""contractStatus"":""GrantedAndActivated"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.5493746214E8,""value"":1.5493746214E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""phaseOfContract"":""Open"",""roleOfClient"":""MainDebtor"",""sector"":""Banks"",""startDate"":""2022-07-05T21:00:00Z"",""totalAmount"":{""currency"":""TZS"",""localValue"":1.31E8,""value"":1.31E8},""typeOfContract"":""Installment""},{""contractStatus"":""GrantedAndActivated"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":420000.0,""value"":420000.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""phaseOfContract"":""Open"",""roleOfClient"":""MainDebtor"",""sector"":""Banks"",""startDate"":""2021-06-30T21:00:00Z"",""totalAmount"":{""currency"":""TZS"",""localValue"":840000.0,""value"":840000.0},""typeOfContract"":""Installment""},{""contractStatus"":""SettledInAdvance"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""phaseOfContract"":""Closed"",""roleOfClient"":""MainDebtor"",""sector"":""Banks"",""startDate"":""2020-01-21T21:00:00Z"",""totalAmount"":{""currency"":""TZS"",""localValue"":1000000.0,""value"":1000000.0},""typeOfContract"":""Installment""}]}},""contractSummary"":{""affordabilityHistoryList"":{""affordabilityHistory"":[{""month"":3,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2023},{""month"":2,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2023},{""month"":1,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2023},{""month"":12,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":11,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":10,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":9,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":8,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":7,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":6,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":5,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022},{""month"":4,""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0},""year"":2022}]},""affordabilitySummary"":{""monthlyAffordability"":{""currency"":""TZS"",""localValue"":0,""value"":0}},""debtor"":{""closedContracts"":1,""openContracts"":2,""outstandingAmountSum"":{""currency"":""TZS"",""localValue"":1.5535746214E8,""value"":1.5535746214E8},""pastDueAmountSum"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""totalAmountSum"":{""currency"":""TZS"",""localValue"":1.3184E8,""value"":1.3184E8}},""guarantor"":{""closedContracts"":0,""openContracts"":0},""overall"":{""lastDelinquency90PlusDays"":null,""maxDueInstallmentsClosedContracts"":0,""maxDueInstallmentsOpenContracts"":0,""worstPastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""worstPastDueDays"":0},""paymentCalendarList"":{""paymentCalendar"":[{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2023-03-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2023-02-27T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.5493746214E8,""value"":1.5493746214E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2023-01-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.566589895E8,""value"":1.566589895E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-12-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.5838051686E8,""value"":1.5838051686E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-11-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.6010204422E8,""value"":1.6010204422E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-10-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-09-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.6354509894E8,""value"":1.6354509894E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-08-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.652666263E8,""value"":1.652666263E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-07-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.652666263E8,""value"":1.652666263E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-06-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.2152531813E8,""value"":1.2152531813E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2022-05-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.2152531813E8,""value"":1.2152531813E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-04-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-03-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""Standard"",""contractsSubmitted"":2,""date"":""2022-02-27T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.0138421627E8,""value"":1.0138421627E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2022-01-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-12-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-11-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-10-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""Standard"",""contractsSubmitted"":2,""date"":""2021-09-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.0974724932E8,""value"":1.0974724932E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":2,""date"":""2021-08-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.1141985593E8,""value"":1.1141985593E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":2,""date"":""2021-07-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.1302246254E8,""value"":1.1302246254E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2021-06-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.1378506915E8,""value"":1.1378506915E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2021-05-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.1538767576E8,""value"":1.1538767576E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2021-04-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":9.001855884E7,""value"":9.001855884E7},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2021-03-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":9.151886816E7,""value"":9.151886816E7},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-02-27T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2021-01-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2020-12-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":9.601979612E7,""value"":9.601979612E7},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2020-11-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":9.752010544E7,""value"":9.752010544E7},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2020-10-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":9.902041476E7,""value"":9.902041476E7},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-09-29T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2020-08-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.020210334E8,""value"":1.020210334E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2020-07-30T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.0352134272E8,""value"":1.0352134272E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2020-06-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.0502165204E8,""value"":1.0502165204E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""classification"":""NotAvailable"",""contractsSubmitted"":0,""date"":""2020-05-30T21:00:00Z"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""classification"":""Standard"",""contractsSubmitted"":1,""date"":""2020-04-29T21:00:00Z"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.0802227068E8,""value"":1.0802227068E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0}]},""sectorInfoList"":{""sectorInfo"":{""debtorClosedContracts"":1,""debtorOpenContracts"":2,""debtorOutstandingAmountSum"":{""currency"":""TZS"",""localValue"":1.5535746214E8,""value"":1.5535746214E8},""debtorPastDueAmountSum"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""debtorTotalAmountSum"":{""currency"":""TZS"",""localValue"":1.3184E8,""value"":1.3184E8},""guarantorClosedContracts"":0,""guarantorOpenContracts"":0,""sector"":""Banks""}}},""contracts"":{""contractList"":{""contract"":[{""collateralsList"":null,""contractStatus"":""GrantedAndActivated"",""contractSubtype"":""NotSpecified"",""currencyOfContract"":""TZS"",""disputes"":{""closedDisputes"":0,""disputeList"":null,""falseDisputes"":0},""expectedEndDate"":""2030-07-24T21:00:00Z"",""methodOfPayment"":""NotSpecified"",""negativeStatusOfContract"":""NoNegativeStatus"",""numberOfDueInstallments"":0,""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.5493746214E8,""value"":1.5493746214E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""paymentCalendarList"":{""calendarItem"":[{""date"":""2023-03-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2023-02-27T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.5493746214E8,""value"":1.5493746214E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2023-01-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.566589895E8,""value"":1.566589895E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-12-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.5838051686E8,""value"":1.5838051686E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-11-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.6010204422E8,""value"":1.6010204422E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-09-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.6354509894E8,""value"":1.6354509894E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-08-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.652666263E8,""value"":1.652666263E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-07-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.652666263E8,""value"":1.652666263E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-06-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.2152531813E8,""value"":1.2152531813E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-05-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.2152531813E8,""value"":1.2152531813E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-04-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-03-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-02-27T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.0096421627E8,""value"":1.0096421627E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-01-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-11-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-09-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.0897724932E8,""value"":1.0897724932E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-08-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.1057985593E8,""value"":1.1057985593E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-07-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.1218246254E8,""value"":1.1218246254E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-06-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.1378506915E8,""value"":1.1378506915E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-05-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.1538767576E8,""value"":1.1538767576E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-04-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":9.001855884E7,""value"":9.001855884E7},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-03-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":9.151886816E7,""value"":9.151886816E7},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-02-27T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-01-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-12-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":9.601979612E7,""value"":9.601979612E7},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2020-11-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":9.752010544E7,""value"":9.752010544E7},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2020-10-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":9.902041476E7,""value"":9.902041476E7},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2020-09-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-08-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.020210334E8,""value"":1.020210334E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2020-07-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.0352134272E8,""value"":1.0352134272E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2020-06-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.0502165204E8,""value"":1.0502165204E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2020-05-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2020-04-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1.0802227068E8,""value"":1.0802227068E8},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0}]},""paymentIncidentList"":null,""paymentPeriodicity"":""Days30"",""phaseOfContract"":""Open"",""purposeOfFinancing"":""Other"",""relatedSubjectsList"":null,""roleOfClient"":""MainDebtor"",""startDate"":""2022-07-05T21:00:00Z"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC"",""subscriberType"":""Banks"",""totalAmount"":{""currency"":""TZS"",""localValue"":1.31E8,""value"":1.31E8},""typeOfContract"":""Installment"",""worstPastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0}},{""collateralsList"":null,""contractStatus"":""GrantedAndActivated"",""contractSubtype"":""NotSpecified"",""currencyOfContract"":""TZS"",""disputes"":{""closedDisputes"":0,""disputeList"":null,""falseDisputes"":0},""expectedEndDate"":""2022-07-24T21:00:00Z"",""methodOfPayment"":""NotSpecified"",""negativeStatusOfContract"":""NoNegativeStatus"",""numberOfDueInstallments"":0,""outstandingAmount"":{""currency"":""TZS"",""localValue"":420000.0,""value"":420000.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""paymentCalendarList"":{""calendarItem"":[{""date"":""2023-03-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2023-02-27T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2023-01-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-11-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-09-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-08-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-07-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-06-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-05-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-04-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-03-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2022-02-27T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":420000.0,""value"":420000.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2022-01-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-11-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-09-29T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":770000.0,""value"":770000.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-08-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":840000.0,""value"":840000.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-07-30T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":840000.0,""value"":840000.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2021-06-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-05-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2021-04-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null}]},""paymentIncidentList"":null,""paymentPeriodicity"":""Days30"",""phaseOfContract"":""Open"",""purposeOfFinancing"":""Other"",""relatedSubjectsList"":null,""roleOfClient"":""MainDebtor"",""startDate"":""2021-06-30T21:00:00Z"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC"",""subscriberType"":""Banks"",""totalAmount"":{""currency"":""TZS"",""localValue"":840000.0,""value"":840000.0},""typeOfContract"":""Installment"",""worstPastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0}},{""collateralsList"":null,""contractStatus"":""SettledInAdvance"",""contractSubtype"":""NotSpecified"",""currencyOfContract"":""TZS"",""disputes"":{""closedDisputes"":0,""disputeList"":null,""falseDisputes"":0},""expectedEndDate"":""2021-01-24T21:00:00Z"",""methodOfPayment"":""NotSpecified"",""negativeStatusOfContract"":""NotSpecified"",""numberOfDueInstallments"":0,""outstandingAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""paymentCalendarList"":{""calendarItem"":[{""date"":""2020-02-28T21:00:00Z"",""delinquencyStatus"":""Ok"",""outstandingAmount"":{""currency"":""TZS"",""localValue"":1000000.0,""value"":1000000.0},""pastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""pastDueDays"":0,""pastDueInstallments"":0},{""date"":""2020-01-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2019-12-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2019-11-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2019-10-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2019-09-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2019-08-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2019-07-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2019-06-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2019-05-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2019-04-29T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null},{""date"":""2019-03-30T21:00:00Z"",""delinquencyStatus"":""NotAvailable"",""outstandingAmount"":null,""pastDueAmount"":null,""pastDueDays"":null,""pastDueInstallments"":null}]},""paymentIncidentList"":null,""paymentPeriodicity"":""Days30"",""phaseOfContract"":""Closed"",""purposeOfFinancing"":""Other"",""relatedSubjectsList"":null,""roleOfClient"":""MainDebtor"",""startDate"":""2020-01-21T21:00:00Z"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC"",""subscriberType"":""Banks"",""totalAmount"":{""currency"":""TZS"",""localValue"":1000000.0,""value"":1000000.0},""typeOfContract"":""Installment"",""worstPastDueAmount"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0}}]}},""currentRelations"":{""contractRelationList"":null,""involvementList"":null,""relatedPartyList"":null},""dashboard"":{""cip"":{""grade"":""A3"",""score"":726},""ciq"":{""fraudAlerts"":0,""fraudAlertsThirdParty"":0},""collaterals"":{""highestCollateralValue"":{""currency"":""TZS"",""localValue"":0,""value"":0},""highestCollateralValueType"":""NotSpecified"",""numberOfCollaterals"":0,""totalCollateralValue"":{""currency"":""TZS"",""localValue"":0,""value"":0}},""disputes"":{""activeContractDisputes"":0,""activeSubjectDisputes"":0,""closedContractDisputes"":0,""closedSubjectDisputes"":0,""falseDisputes"":0},""inquiries"":{""inquiriesForLast12Months"":5},""paymentsProfile"":{""numberOfDifferentSubscribers"":1,""pastDueAmountSum"":{""currency"":""TZS"",""localValue"":0.0,""value"":0.0},""worstPastDueDaysCurrent"":0,""worstPastDueDaysForLast12Months"":0},""relations"":{""numberOfInvolvements"":0,""numberOfRelations"":0}},""disputes"":{""disputeList"":null,""summary"":{""numberOfActiveDisputesContracts"":0,""numberOfActiveDisputesInCourt"":0,""numberOfActiveDisputesSubjectData"":0,""numberOfClosedDisputesContracts"":0,""numberOfClosedDisputesSubjectData"":0,""numberOfFalseDisputes"":0}},""individual"":{""contact"":{""mobilePhone"":""+255754428793""},""general"":{""birthSurname"":""Kanjagaile"",""citizenship"":""NotSpecified"",""classificationOfIndividual"":""Individual"",""countryOfBirth"":""NotSpecified"",""dateOfBirth"":""1980-05-19T21:00:00Z"",""education"":""NotSpecified"",""employment"":""Banker"",""firstName"":""Elizabeth"",""fullName"":""Elizabeth JOHN Kanjagaile"",""gender"":""Female"",""maritalStatus"":""Single"",""nationality"":""TZ"",""negativeStatus"":""NotSpecified""},""identifications"":{""passportIssuerCountry"":""NotSpecified"",""wardID"":1713},""mainAddress"":{""country"":""TZ""},""secondaryAddress"":{""country"":""TZ""}},""inquiries"":{""inquiryList"":{""inquiry"":[{""dateOfInquiry"":""2023-03-10T07:08:31.503Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC (MOBILE MICROLENDING)""},{""dateOfInquiry"":""2023-03-10T04:35:39.02Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC (MOBILE MICROLENDING)""},{""dateOfInquiry"":""2023-03-09T15:51:57.393Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC (MOBILE MICROLENDING)""},{""dateOfInquiry"":""2023-03-09T14:48:41.71Z"",""product"":""WS Creditinfo Report Plus"",""reason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""sector"":""Banks"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC (MOBILE MICROLENDING)""},{""dateOfInquiry"":""2022-05-05T11:32:27.91Z"",""product"":""Quick Check"",""reason"":""NotSpecified"",""sector"":""Banks"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC""}]},""summary"":{""numberOfInquiriesLast12Months"":5,""numberOfInquiriesLast1Month"":4,""numberOfInquiriesLast24Months"":5,""numberOfInquiriesLast3Months"":4,""numberOfInquiriesLast6Months"":4}},""managers"":{""managerList"":null,""numberOfManagers"":0},""parameters"":{""consent"":true,""inquiryReason"":""ApplicationForCreditOrAmendmentOfCreditTerms"",""reportDate"":""2023-03-10T07:30:37.4793329Z"",""sections"":{""string"":""CreditinfoReportPlus""},""subjectType"":""Individual"",""idnumber"":676610,""idnumberType"":""CreditinfoId""},""paymentIncidentList"":{""paymentIncidentList"":null,""summary"":{""dueAmountSummary"":{""currency"":""TZS"",""localValue"":0,""value"":0},""outstandingAmountSummary"":{""currency"":""TZS"",""localValue"":0,""value"":0}}},""reportInfo"":{""created"":""2023-03-10T07:30:37.9486663Z"",""referenceNumber"":""28695908-676610"",""reportStatus"":""ReportGenerated"",""requestedBy"":""Emmanuel Robert"",""subscriber"":""NATIONAL MICROFINANCE BANK TANZANIA PLC (MOBILE MICROLENDING)"",""version"":573},""shareholders"":{""numberOfShareholders"":0,""shareholderList"":null},""subjectInfoHistory"":{""addressList"":""<AddressList xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""><Address><Item>MainAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2017-01-30T21:00:00Z</ValidFrom><ValidTo>2017-04-29T21:00:00Z</ValidTo><Value>Magu, CityTownVillage.Magu, Mwanza, Magu, TZ</Value></Address><Address><Item>MainAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2017-04-29T21:00:00Z</ValidFrom><ValidTo>2020-10-30T21:00:00Z</ValidTo><Value>BANKER STAFF, CityTownVillage.BOX 12 MAGU, Mwanza, Magu, TZ</Value></Address><Address><Item>SecondaryAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2017-01-30T21:00:00Z</ValidFrom><ValidTo>2017-04-29T21:00:00Z</ValidTo><Value>Magu, CityTownVillage.Magu, Mwanza, Magu, TZ</Value></Address><Address><Item>SecondaryAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2017-04-29T21:00:00Z</ValidFrom><ValidTo>2017-11-29T21:00:00Z</ValidTo><Value>BANKER STAFF, CityTownVillage.BOX 12 MAGU, Mwanza, Magu, TZ</Value></Address><Address><Item>SecondaryAddress</Item><Subscriber>B01</Subscriber><ValidFrom>2017-11-29T21:00:00Z</ValidFrom><ValidTo>2020-10-30T21:00:00Z</ValidTo><Value>BANKER STAFF, CityTownVillage.BOX 12 MAGU, Kilimanjaro, Magu, TZ</Value></Address></AddressList>"",""contactList"":""<ContactList xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""><Contact><Item>MobilePhone</Item><Subscriber>B01</Subscriber><ValidFrom>2017-01-30T21:00:00Z</ValidFrom><ValidTo>2017-11-29T21:00:00Z</ValidTo><Value>+255786987733</Value></Contact></ContactList>"",""generalList"":""<GeneralList xmlns=\""http://creditinfo.com/schemas/2012/09/MultiConnector/Connectors/INT/IdmStrategy/Response\""><General><Item>Names</Item><Subscriber>B01</Subscriber><ValidFrom>2017-01-30T21:00:00Z</ValidFrom><ValidTo>2017-11-29T21:00:00Z</ValidTo><Value>ELIZABETH JOHN KANJAGAILE</Value></General><General><Item>FullName</Item><Subscriber>B01</Subscriber><ValidFrom>2017-01-30T21:00:00Z</ValidFrom><ValidTo>2017-11-29T21:00:00Z</ValidTo><Value>ELIZABETH JOHN KANJAGAILE</Value></General><General><Item>BirthSurname</Item><Subscriber>B01</Subscriber><ValidFrom>2017-01-30T21:00:00Z</ValidFrom><ValidTo>2017-11-29T21:00:00Z</ValidTo><Value>KANJAGAILE</Value></General></GeneralList>"",""identificationsList"":null}},""extract"":{""nationalId"":0,""date"":""2023-03-10T07:30:38.1502187Z"",""decision"":""Approve"",""mobileScore"":625,""mobileGrade"":""C1"",""age"":42,""dateOfBirth"":""1980-05-19T21:00:00Z"",""mobileTotalBalance"":0,""apd1DPD"":0,""allContacts"":""255786987733255754428793"",""mobilePhone"":""+0786987733"",""scr2Result"":""NotEvaluated"",""scr2Parameter"":530,""scr2Value"":726,""scr4Result"":""NotEvaluated"",""scr4Parameter"":548,""inq2Result"":""NotEvaluated"",""inq2Parameter"":5,""inq2Value"":1,""rsk3Result"":""NotEvaluated"",""rsk3Parameter"":2,""rsk3Value"":0,""rsk4Result"":""NotEvaluated"",""rsk4Parameter"":1,""rsk4Value"":0,""rsk6Result"":""NotEvaluated"",""rsk6Parameter"":3,""rsk6Value"":1,""rsk7Result"":""NotEvaluated"",""rsk7Parameter"":1.1,""rsk10Result"":""NotEvaluated"",""cst1Result"":""NotEvaluated"",""cst1Parameter"":23,""cst1Value"":42,""cst2Result"":""NotEvaluated"",""cst2Parameter"":1000,""cst2Value"":0,""cst3Result"":""NotEvaluated"",""cst3Parameter"":700000,""cst3Value"":0,""cst12Result"":""NotEvaluated"",""cipscore"":726,""cipgrade"":""A3""},""strategy"":{""id"":""438bb263-fb48-470b-b0f1-7e4efa81220d"",""name"":""Mobile Loan Strategy - NMBPLC"",""beeStrategy"":""MobileLoanStrategy"",""templateName"":""TZA_IDM"",""preferEmbedded"":false,""returnOutputDataInSteps"":true}}}"

                    'Dim sr As StreamReader = File.OpenText("D:\response.txt")
                    'mstrReponseData = sr.ReadToEnd()
                    'sr.Close()
                    'Dim lstlist = jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract").Select(Function(x) x).DefaultIfEmpty.ToList()

                    'Dim i As Integer = 0
                    'For Each dr In lstlist
                    '    Dim dc = CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueAmount.value"), JToken), JValue).Value
                    '    i += 1
                    '    'GetKeys(CType(dr, JObject))
                    'Next

                    If mstrReponseData.Trim.Length > 0 Then

                        Dim jArray As JObject = JsonConvert.DeserializeObject(Of JObject)(mstrReponseData)

                        'If jArray IsNot Nothing AndAlso jArray.Count > 0 Then 

                        '    If CInt(CType(CType(jArray.SelectToken("statusCode"), JToken), JValue).Value) = 600 Then  'Success

                        '        If jArray.ContainsKey("body") Then

                        '            strBuilder.Append("<HTML>" & vbCrLf)
                        '            strBuilder.Append("<HEAD>" & vbCrLf)
                        '            strBuilder.Append("<STYLE TYPE='text/css'>" & vbCrLf)
                        '            strBuilder.Append("u.dotted" & vbCrLf)
                        '            strBuilder.Append("{" & vbCrLf)
                        '            strBuilder.Append("border-bottom: 1px dotted #000;" & vbCrLf)
                        '            strBuilder.Append("text-decoration: none;" & vbCrLf)
                        '            strBuilder.Append("}" & vbCrLf)
                        '            strBuilder.Append("</STYLE>" & vbCrLf)
                        '            strBuilder.Append("</HEAD>" & vbCrLf)
                        '            strBuilder.Append("<BODY style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("<TABLE width = '800px' align='center'>" & vbCrLf)
                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                        '            strBuilder.Append("            <tr style='text-align: Center; width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("                <td style='text-align: Center; width: 100%'>" & vbCrLf)
                        '            Dim objCompany As New clsCompany_Master
                        '            objCompany._Companyunkid = CInt(Session("CompanyUnkId"))
                        '            Dim strPath As String = String.Empty
                        '            Dim base64String As String = ""
                        '            If objCompany._Image IsNot Nothing Then
                        '                strPath = My.Computer.FileSystem.SpecialDirectories.Temp & "\clogo.png"
                        '                objCompany._Image.Save(strPath)
                        '                If strPath.Trim.Length > 0 Then
                        '                    Try
                        '                        base64String = Convert.ToBase64String(IO.File.ReadAllBytes(strPath))
                        '                    Catch ex1 As ArgumentException
                        '                        CommonCodes.LogErrorOnly(ex1)
                        '                    Catch ex2 As FormatException
                        '                        CommonCodes.LogErrorOnly(ex2)
                        '                    Catch ex As Exception
                        '                        CommonCodes.LogErrorOnly(ex)
                        '                    End Try
                        '                End If
                        '                strBuilder.Append(" <img src = ""data:image/png;base64, " & base64String & """ HEIGHT = '100px' WIDTH = '150px' />" & vbCrLf)
                        '            Else
                        '                strBuilder.Append(" <img src = '' HEIGHT = '100px' WIDTH = '150px' />" & vbCrLf)
                        '            End If
                        '            strBuilder.Append("                </td>" & vbCrLf)
                        '            strBuilder.Append("            </tr>" & vbCrLf)
                        '            strBuilder.Append("        </table>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)
                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td style='text-align: Center;'><B><FONT SIZE=3 FONT COLOR = 'Blue'>" & vbCrLf)
                        '            strBuilder.Append("                " & objCompany._Name & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)
                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                        '            strBuilder.Append("            <tr style='text-align: Center; width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("                <td style='text-align: Center;'><FONT SIZE=2>" & vbCrLf)
                        '            strBuilder.Append("                    <B>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 53, "CRB Status Report") & "</B></FONT>" & vbCrLf)
                        '            strBuilder.Append("                </td>" & vbCrLf)
                        '            strBuilder.Append("            </tr>" & vbCrLf)
                        '            strBuilder.Append("        </table>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)

                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                        '            strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("                <td><FONT SIZE=4><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 54, "Personal Information") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("            </tr>" & vbCrLf)
                        '            strBuilder.Append("        </table>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)
                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)

                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                        '            strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)

                        '            If jArray.SelectToken("body.personalInformation").Count > 0 Then

                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 55, "Full Name") & ": </b></td>" & vbCrLf)
                        '                If IsNothing(CType(CType(jArray.SelectToken("body.personalInformation.fullName"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.personalInformation.fullName"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.personalInformation.fullName"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If

                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 56, "Date of Birth") & ": </b></td>" & vbCrLf)
                        '                If IsNothing(CType(CType(jArray.SelectToken("body.personalInformation.dateOfBirth"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.personalInformation.dateOfBirth"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.personalInformation.dateOfBirth"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If
                        '                strBuilder.Append("            </tr>" & vbCrLf)

                        '                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 57, "Age") & ": </b></td>" & vbCrLf)
                        '                If IsNothing(CType(CType(jArray.SelectToken("body.personalInformation.age"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.personalInformation.age"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.personalInformation.age"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If
                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 58, "Gender") & ": </b></td>" & vbCrLf)
                        '                If IsNothing(CType(CType(jArray.SelectToken("body.personalInformation.gender"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.personalInformation.gender"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.personalInformation.gender"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If
                        '                strBuilder.Append("            </tr>" & vbCrLf)

                        '                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 59, "Marital Status") & ": </b></td>" & vbCrLf)
                        '                If IsNothing(CType(CType(jArray.SelectToken("body.personalInformation.maritalStatus"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.personalInformation.maritalStatus"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.personalInformation.maritalStatus"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If
                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 60, "Employment Status") & ": </b></td>" & vbCrLf)

                        '                If IsNothing(CType(CType(jArray.SelectToken("body.personalInformation.employmentStatus"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.personalInformation.employmentStatus"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.personalInformation.employmentStatus"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If

                        '            End If

                        '            strBuilder.Append("            </tr>" & vbCrLf)
                        '            strBuilder.Append("        </table>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)
                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)


                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                        '            strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("                <td><FONT SIZE=4><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 61, "Identification Used & Request Information") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("            </tr>" & vbCrLf)
                        '            strBuilder.Append("        </table>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)
                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)


                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                        '            strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 62, "Currency") & ": </b></td>" & vbCrLf)

                        '            If IsNothing(CType(CType(jArray.SelectToken("body.currency"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.currency"), JToken), JValue).Value) = False Then
                        '                strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.currency"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '            Else
                        '                strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                        '            End If

                        '            If jArray.SelectToken("body.generalInformation").Count > 0 Then

                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 63, "SubjectID Number") & ": </b></td>" & vbCrLf)

                        '                If IsNothing(CType(CType(jArray.SelectToken("body.generalInformation.subjectIDNumber"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.generalInformation.subjectIDNumber"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.generalInformation.subjectIDNumber"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If

                        '                strBuilder.Append("            </tr>" & vbCrLf)
                        '                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 64, "Request Date") & ": </b></td>" & vbCrLf)

                        '                If IsNothing(CType(CType(jArray.SelectToken("body.generalInformation.requestDate"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.generalInformation.requestDate"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.generalInformation.requestDate"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If

                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 65, "Reference Number") & ": </b></td>" & vbCrLf)

                        '                If IsNothing(CType(CType(jArray.SelectToken("body.generalInformation.referenceNumber"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.generalInformation.referenceNumber"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.generalInformation.referenceNumber"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If

                        '            End If

                        '            strBuilder.Append("            </tr>" & vbCrLf)
                        '            strBuilder.Append("        </table>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)
                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)


                        '            ' START OPEN COTRACTS
                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                        '            strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("                <td><FONT SIZE=4><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 66, "Open Contracts") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("            </tr>" & vbCrLf)
                        '            strBuilder.Append("        </table>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)
                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)


                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                        '            strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 67, "Contract Status") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 68, "Outstanding Amount") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 69, "Past Due Amount") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 70, "Past Due Days") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 71, "Phase Of Contract") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 72, "Role Of Client") & "</b></td>" & vbCrLf)


                        '            'Pinkal (10-Jul-2023) -- Start
                        '            'NMB Enhancement :(A1X-1106) NMB - CRB report changes to include additional parameters from the CRB API (due to API changes). 
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 100, "Subscriber Type") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 101, "Subscriber") & "</b></td>" & vbCrLf)
                        '            'Pinkal (10-Jul-2023) -- End

                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 73, "Sector") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 74, "Reporting Date") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 75, "Total Amount") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 76, "Type Of Contract") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("            </tr>" & vbCrLf)


                        '            If jArray.SelectToken("body.tzaCb5Data").Count > 0 AndAlso jArray.SelectToken("body.tzaCb5Data.contractOverview").Count > 0 AndAlso jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList").Count > 0 AndAlso jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract").Count > 0 Then


                        '                Dim lstlist = jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract").Select(Function(x) x).DefaultIfEmpty.ToList()
                        '                Dim i As Integer = 0

                        '                If lstlist IsNot Nothing AndAlso IsDBNull(lstlist) = False AndAlso lstlist.Count > 0 Then

                        '                    For Each dr In lstlist
                        '                        strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].contractStatus"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].contractStatus"), JToken), JValue).Value) = False Then
                        '                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].contractStatus"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                        Else
                        '                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                        End If

                        '                        'Pinkal (20-Feb-2023) -- Start
                        '                        'Found Error as They didn't give proper Response in CRB Response PDF.
                        '                        If CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].outstandingAmount"), JToken).Count > 0 Then
                        '                            If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].outstandingAmount.value"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].outstandingAmount.value"), JToken), JValue).Value) = False Then
                        '                                strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].outstandingAmount.value"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                            Else
                        '                                strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                            End If
                        '                        Else
                        '                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                        End If

                        '                        'Pinkal (20-Feb-2023) -- End

                        '                        'Pinkal (20-Feb-2023) -- Start
                        '                        'Found Error as They didn't give proper Response in CRB Response PDF.
                        '                        If CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueAmount"), JToken).Count > 0 Then
                        '                            If CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueAmount"), JToken), JObject).Count > 0 Then
                        '                                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueAmount.value"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueAmount.value"), JToken), JValue).Value) = False Then
                        '                                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueAmount.value"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                                Else
                        '                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                                End If
                        '                            Else
                        '                                strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                            End If
                        '                        Else
                        '                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                        End If
                        '                        'Pinkal (20-Feb-2023) -- End

                        '                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueDays"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueDays"), JToken), JValue).Value) = False Then
                        '                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueDays"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                        Else
                        '                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                        End If

                        '                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].phaseOfContract"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].phaseOfContract"), JToken), JValue).Value) = False Then
                        '                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].phaseOfContract"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                        Else
                        '                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                        End If


                        '                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].roleOfClient"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].roleOfClient"), JToken), JValue).Value) = False Then
                        '                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].roleOfClient"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                        Else
                        '                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                        End If


                        '                        'Pinkal (10-Jul-2023) -- Start
                        '                        'NMB Enhancement :(A1X-1106) NMB - CRB report changes to include additional parameters from the CRB API (due to API changes).
                        '                        If CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].SubscriberType"), JToken).Count > 0 Then
                        '                            If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].SubscriberType"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].SubscriberType"), JToken), JValue).Value) = False Then
                        '                                strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].SubscriberType"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                            Else
                        '                                strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                            End If
                        '                        Else
                        '                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                        End If

                        '                        If CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].Subscriber"), JToken).Count > 0 Then
                        '                            If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].Subscriber"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].Subscriber"), JToken), JValue).Value) = False Then
                        '                                strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].Subscriber"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                            Else
                        '                                strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                            End If
                        '                        Else
                        '                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                        End If
                        '                        'Pinkal (10-Jul-2023) -- End

                        '                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].sector"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].sector"), JToken), JValue).Value) = False Then
                        '                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].sector"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                        Else
                        '                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                        End If

                        '                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].startDate"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].startDate"), JToken), JValue).Value) = False Then
                        '                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].startDate"), JToken), JValue).ToString() & "</b></u></td>" & vbCrLf)
                        '                        Else
                        '                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                        End If

                        '                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].totalAmount.value"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].totalAmount.value"), JToken), JValue).Value) = False Then
                        '                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].totalAmount.value"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                        Else
                        '                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                        End If

                        '                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].typeOfContract"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].typeOfContract"), JToken), JValue).Value) = False Then
                        '                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList.contract[" + i.ToString() + "].typeOfContract"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                        Else
                        '                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                        End If

                        '                        strBuilder.Append("            </tr>" & vbCrLf)
                        '                        i += 1
                        '                    Next

                        '                End If

                        '            End If ' If jArray.SelectToken("body.tzaCb5Data").Contains("contractOverview") AndAlso jArray.SelectToken("body.tzaCb5Data.contractOverview").Contains("contractList") AndAlso jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList").Contains("contract") Then

                        '            strBuilder.Append("        </table>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)
                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)
                        '            ' END OPEN COTRACTS


                        '            ' START DISPUTES SUMMARY
                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                        '            strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("                <td><FONT SIZE=4><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 77, "Disputes Summary") & "</b></td>" & vbCrLf)
                        '            strBuilder.Append("            </tr>" & vbCrLf)
                        '            strBuilder.Append("        </table>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)
                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)

                        '            strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("    <td>" & vbCrLf)
                        '            strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                        '            strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '            strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 78, "Number Of Active Disputes Contracts") & "</b></td>" & vbCrLf)

                        '            If jArray.SelectToken("body.tzaCb5Data").Count > 0 AndAlso jArray.SelectToken("body.tzaCb5Data.disputes").Count > 0 AndAlso jArray.SelectToken("body.tzaCb5Data.disputes.summary").Count > 0 Then

                        '                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfActiveDisputesContracts"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfActiveDisputesContracts"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfActiveDisputesContracts"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If
                        '                strBuilder.Append("            </tr>" & vbCrLf)
                        '                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 79, "Number Of Active Disputes In Court") & "</b></td>" & vbCrLf)
                        '                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfActiveDisputesInCourt"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfActiveDisputesInCourt"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfActiveDisputesInCourt"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If
                        '                strBuilder.Append("            </tr>" & vbCrLf)
                        '                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 80, "Number Of Active Disputes Subject Data") & "</b></td>" & vbCrLf)
                        '                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfActiveDisputesSubjectData"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfActiveDisputesSubjectData"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfActiveDisputesSubjectData"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If
                        '                strBuilder.Append("            </tr>" & vbCrLf)
                        '                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 81, "Number Of Closed Disputes Contracts") & "</b></td>" & vbCrLf)
                        '                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfClosedDisputesContracts"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfClosedDisputesContracts"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfClosedDisputesContracts"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If
                        '                strBuilder.Append("            </tr>" & vbCrLf)
                        '                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 82, "Number Of Closed Disputes Subject Data") & "</b></td>" & vbCrLf)
                        '                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfClosedDisputesSubjectData"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfClosedDisputesSubjectData"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfClosedDisputesSubjectData"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If
                        '                strBuilder.Append("            </tr>" & vbCrLf)
                        '                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                        '                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 83, "Number Of False Disputes") & "</b></td>" & vbCrLf)
                        '                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfFalseDisputes"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfFalseDisputes"), JToken), JValue).Value) = False Then
                        '                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5Data.disputes.summary.numberOfFalseDisputes"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                        '                Else
                        '                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                        '                End If
                        '                strBuilder.Append("            </tr>" & vbCrLf)
                        '            End If
                        '            ' END DISPUTES SUMMARY

                        '            strBuilder.Append("    </td>" & vbCrLf)
                        '            strBuilder.Append("</tr>" & vbCrLf)
                        '            strBuilder.Append("</TABLE>" & vbCrLf)
                        '            strBuilder.Append("</BODY>" & vbCrLf)
                        '            strBuilder.Append("</HEAD>" & vbCrLf)
                        '            strBuilder.Append("</HTML>")

                        '            divhtml.InnerHtml = strBuilder.ToString
                        '            popupCRBReport.Show()


                        '            'Dim file As System.IO.FileInfo = New System.IO.FileInfo(IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & "CRB_Status_Report_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss") & ".html")
                        '            'Response.Clear()
                        '            'Response.Buffer = True
                        '            'Response.AddHeader("content-disposition", "attachment;filename=" & file.Name)
                        '            'Response.Charset = ""
                        '            'Response.ContentType = "text/HTML"

                        '            'Dim sw As New StringWriter(strBuilder)
                        '            'Dim hw As New HtmlTextWriter(sw)

                        '            'Response.Output.Write(sw.ToString())
                        '            'Response.Flush()
                        '            'Response.[End]()


                        '        End If   '   If jArray.ContainsKey("body") Then

                        '    Else
                        '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 84, "Report generation is failed.Reason : CRB Reponse is getting Error."), Me)
                        '        Exit Sub
                        '    End If 'If CInt(CType(CType(jArray.SelectToken("statusCode"), JToken), JValue).Value) = 600 Then

                        'End If  ' If jArray IsNot Nothing AndAlso jArray.Count > 0 Then

                        'Pinkal (15-Sep-2023) -- Start
                        '(A1X-1166) NMB - Disburse approved tranche amount on Flexcube via API.

                        If jArray IsNot Nothing AndAlso jArray.Count > 0 Then

                            If CInt(CType(CType(jArray.SelectToken("statusCode"), JToken), JValue).Value) = 600 Then  'Success

                                    strBuilder.Append("<HTML>" & vbCrLf)
                                    strBuilder.Append("<HEAD>" & vbCrLf)
                                    strBuilder.Append("<STYLE TYPE='text/css'>" & vbCrLf)
                                    strBuilder.Append("u.dotted" & vbCrLf)
                                    strBuilder.Append("{" & vbCrLf)
                                    strBuilder.Append("border-bottom: 1px dotted #000;" & vbCrLf)
                                    strBuilder.Append("text-decoration: none;" & vbCrLf)
                                    strBuilder.Append("}" & vbCrLf)
                                    strBuilder.Append("</STYLE>" & vbCrLf)
                                    strBuilder.Append("</HEAD>" & vbCrLf)
                                    strBuilder.Append("<BODY style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("<TABLE width = '800px' align='center'>" & vbCrLf)
                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                                    strBuilder.Append("            <tr style='text-align: Center; width: 100%'>" & vbCrLf)
                                    strBuilder.Append("                <td style='text-align: Center; width: 100%'>" & vbCrLf)
                                    Dim objCompany As New clsCompany_Master
                                    objCompany._Companyunkid = CInt(Session("CompanyUnkId"))
                                    Dim strPath As String = String.Empty
                                    Dim base64String As String = ""
                                    If objCompany._Image IsNot Nothing Then
                                        strPath = My.Computer.FileSystem.SpecialDirectories.Temp & "\clogo.png"
                                        objCompany._Image.Save(strPath)
                                        If strPath.Trim.Length > 0 Then
                                            Try
                                                base64String = Convert.ToBase64String(IO.File.ReadAllBytes(strPath))
                                            Catch ex1 As ArgumentException
                                                CommonCodes.LogErrorOnly(ex1)
                                            Catch ex2 As FormatException
                                                CommonCodes.LogErrorOnly(ex2)
                                            Catch ex As Exception
                                                CommonCodes.LogErrorOnly(ex)
                                            End Try
                                        End If
                                        strBuilder.Append(" <img src = ""data:image/png;base64, " & base64String & """ HEIGHT = '100px' WIDTH = '150px' />" & vbCrLf)
                                    Else
                                        strBuilder.Append(" <img src = '' HEIGHT = '100px' WIDTH = '150px' />" & vbCrLf)
                                    End If
                                    strBuilder.Append("                </td>" & vbCrLf)
                                    strBuilder.Append("            </tr>" & vbCrLf)
                                    strBuilder.Append("        </table>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)
                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td style='text-align: Center;'><B><FONT SIZE=3 FONT COLOR = 'Blue'>" & vbCrLf)
                                    strBuilder.Append("                " & objCompany._Name & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)
                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                                    strBuilder.Append("            <tr style='text-align: Center; width: 100%'>" & vbCrLf)
                                    strBuilder.Append("                <td style='text-align: Center;'><FONT SIZE=2>" & vbCrLf)
                                    strBuilder.Append("                    <B>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 53, "CRB Status Report") & "</B></FONT>" & vbCrLf)
                                    strBuilder.Append("                </td>" & vbCrLf)
                                    strBuilder.Append("            </tr>" & vbCrLf)
                                    strBuilder.Append("        </table>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)

                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("                <td><FONT SIZE=4><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 54, "Personal Information") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("            </tr>" & vbCrLf)
                                    strBuilder.Append("        </table>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)
                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)

                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)

                                If jArray.SelectToken("body.personalInformation").Count > 0 Then

                                        strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 55, "Full Name") & ": </b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.personalInformation.fullName") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.personalInformation.fullName"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.personalInformation.fullName"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.personalInformation.fullName"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If

                                        strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 56, "Date of Birth") & ": </b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.personalInformation.dateOfBirth") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.personalInformation.dateOfBirth"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.personalInformation.dateOfBirth"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.personalInformation.dateOfBirth"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                    End If

                                        strBuilder.Append("            </tr>" & vbCrLf)
                                        strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                        strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 57, "Age") & ": </b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.personalInformation.age") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.personalInformation.age"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.personalInformation.age"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.personalInformation.age"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If

                                        strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 58, "Gender") & ": </b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.personalInformation.gender") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.personalInformation.gender"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.personalInformation.gender"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.personalInformation.gender"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                    End If

                                        strBuilder.Append("            </tr>" & vbCrLf)

                                        strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                        strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 59, "Marital Status") & ": </b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.personalInformation.maritalStatus") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.personalInformation.maritalStatus"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.personalInformation.maritalStatus"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.personalInformation.maritalStatus"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If

                                        strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 60, "Employment Status") & ": </b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.personalInformation.employmentStatus") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.personalInformation.employmentStatus"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.personalInformation.employmentStatus"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.personalInformation.employmentStatus"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If

                                End If  '   If jArray.SelectToken("PersonalInformation").Count > 0 Then

                                    strBuilder.Append("            </tr>" & vbCrLf)
                                    strBuilder.Append("        </table>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)
                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)


                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("                <td><FONT SIZE=4><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 61, "Identification Used & Request Information") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("            </tr>" & vbCrLf)
                                    strBuilder.Append("        </table>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)
                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)


                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 62, "Currency") & ": </b></td>" & vbCrLf)

                                If jArray.SelectToken("body.currency") IsNot Nothing Then
                                    If IsNothing(CType(CType(jArray.SelectToken("body.currency"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.currency"), JToken), JValue).Value) = False Then
                                        strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.currency"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                    Else
                                        strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                    End If
                                    Else
                                        strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                    End If

                                If jArray.SelectToken("body.generalInformation").Count > 0 Then

                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 63, "SubjectID Number") & ": </b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.generalInformation.subjectIDNumber") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.generalInformation.subjectIDNumber"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.generalInformation.subjectIDNumber"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.generalInformation.subjectIDNumber"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                    End If

                                    strBuilder.Append("            </tr>" & vbCrLf)
                                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 64, "Request Date") & ": </b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.generalInformation.requestDate") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.generalInformation.requestDate"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.generalInformation.requestDate"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.generalInformation.requestDate"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                    End If

                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 65, "Reference Number") & ": </b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.generalInformation.referenceNumber") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.generalInformation.referenceNumber"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.generalInformation.referenceNumber"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><u class='dotted'><b>" & CType(CType(jArray.SelectToken("body.generalInformation.referenceNumber"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("                <td><u class='dotted'><b>&nbsp;</b></u></td>" & vbCrLf)
                                    End If

                                End If  '  If jArray.SelectToken("GeneralInformation").Count > 0 Then


                                    strBuilder.Append("            </tr>" & vbCrLf)
                                    strBuilder.Append("        </table>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)
                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)


                                    ' START OPEN COTRACTS
                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("                <td><FONT SIZE=4><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 66, "Open Contracts") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("            </tr>" & vbCrLf)
                                    strBuilder.Append("        </table>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)
                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)


                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 67, "Contract Status") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 68, "Outstanding Amount") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 69, "Past Due Amount") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 70, "Past Due Days") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 71, "Phase Of Contract") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 72, "Role Of Client") & "</b></td>" & vbCrLf)
                                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 100, "Subscriber Type") & "</b></td>" & vbCrLf)
                                strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 101, "Subscriber") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 73, "Sector") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 74, "Reporting Date") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 75, "Total Amount") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 76, "Type Of Contract") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("            </tr>" & vbCrLf)


                                If (jArray.SelectToken("body.tzaCb5_data") IsNot Nothing AndAlso jArray.SelectToken("body.tzaCb5_data").Count > 0) AndAlso _
                                   (jArray.SelectToken("body.tzaCb5_data.contractOverview") IsNot Nothing AndAlso jArray.SelectToken("body.tzaCb5_data.contractOverview").Count > 0) AndAlso _
                                   (jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList") IsNot Nothing AndAlso jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList").Count > 0) AndAlso _
                                   (jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract") IsNot Nothing AndAlso jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract").Count > 0) Then


                                    Dim lstlist = jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract").Select(Function(x) x).DefaultIfEmpty.ToList()
                                        Dim i As Integer = 0

                                        If lstlist IsNot Nothing AndAlso IsDBNull(lstlist) = False AndAlso lstlist.Count > 0 Then

                                            For Each dr In lstlist
                                                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)

                                            If jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].contractStatus") IsNot Nothing Then
                                                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].contractStatus"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].contractStatus"), JToken), JValue).Value) = False Then
                                                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].contractStatus"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If

                                            If jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].outstandingAmount") IsNot Nothing Then
                                                If CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].outstandingAmount"), JToken).Count > 0 Then
                                                    If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].outstandingAmount.value"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].outstandingAmount.value"), JToken), JValue).Value) = False Then
                                                        strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].outstandingAmount.value"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                                    Else
                                                        strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                    End If
                                                    Else
                                                        strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                    End If
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If


                                            If jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueAmount") IsNot Nothing AndAlso jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueAmount").Count > 0 Then
                                                If CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueAmount"), JToken), JObject).Count > 0 Then
                                                    If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueAmount.value"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.TzaCb5_data.ContractOverview.ContractList.Contract[" + i.ToString() + "].pastDueAmount.value"), JToken), JValue).Value) = False Then
                                                        strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueAmount.value"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If
                                                    Else
                                                        strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                    End If
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If

                                            If jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueDays") IsNot Nothing Then
                                                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueDays"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueDays"), JToken), JValue).Value) = False Then
                                                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].pastDueDays"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If

                                            If jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].phaseOfContract") IsNot Nothing Then
                                                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].phaseOfContract"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].phaseOfContract"), JToken), JValue).Value) = False Then
                                                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].phaseOfContract"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If

                                            If jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].roleOfClient") IsNot Nothing Then
                                                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].roleOfClient"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].roleOfClient"), JToken), JValue).Value) = False Then
                                                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].roleOfClient"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If
                                            Else
                                                strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                            End If

                                            If jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].subscriberType") IsNot Nothing Then
                                                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].subscriberType"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].subscriberType"), JToken), JValue).Value) = False Then
                                                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].subscriberType"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If

                                            If jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].subscriber") IsNot Nothing Then
                                                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].subscriber"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].subscriber"), JToken), JValue).Value) = False Then
                                                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].subscriber"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If
                                            Else
                                                strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                            End If

                                            If jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].sector") IsNot Nothing Then
                                                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].sector"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].sector"), JToken), JValue).Value) = False Then
                                                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].sector"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If

                                            If jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].startDate") IsNot Nothing Then
                                                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].startDate"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].startDate"), JToken), JValue).Value) = False Then
                                                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].startDate"), JToken), JValue).ToString() & "</b></u></td>" & vbCrLf)
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If

                                            If jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].totalAmount") IsNot Nothing AndAlso jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].totalAmount").Count > 0 Then
                                                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].totalAmount.value"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].totalAmount.value"), JToken), JValue).Value) = False Then
                                                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].totalAmount.value"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If

                                            If jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].typeOfContract") IsNot Nothing Then
                                                If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].typeOfContract"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].typeOfContract"), JToken), JValue).Value) = False Then
                                                    strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.contractOverview.contractList.contract[" + i.ToString() + "].typeOfContract"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If
                                                Else
                                                    strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                                End If

                                                strBuilder.Append("            </tr>" & vbCrLf)
                                                i += 1
                                            Next

                                        End If

                                    End If ' If jArray.SelectToken("body.tzaCb5Data").Contains("contractOverview") AndAlso jArray.SelectToken("body.tzaCb5Data.contractOverview").Contains("contractList") AndAlso jArray.SelectToken("body.tzaCb5Data.contractOverview.contractList").Contains("contract") Then

                                    strBuilder.Append("        </table>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)
                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)
                                    ' END OPEN COTRACTS


                                    ' START DISPUTES SUMMARY
                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("                <td><FONT SIZE=4><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 77, "Disputes Summary") & "</b></td>" & vbCrLf)
                                    strBuilder.Append("            </tr>" & vbCrLf)
                                    strBuilder.Append("        </table>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)
                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)

                                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                                    strBuilder.Append("    <td>" & vbCrLf)
                                    strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)

                                If (jArray.SelectToken("body.tzaCb5_data") IsNot Nothing AndAlso jArray.SelectToken("body.tzaCb5_data").Count > 0) AndAlso _
                                   (jArray.SelectToken("body.tzaCb5_data.disputes") IsNot Nothing AndAlso jArray.SelectToken("body.tzaCb5_data.disputes").Count > 0) AndAlso _
                                   (jArray.SelectToken("body.tzaCb5_data.disputes.summary") IsNot Nothing AndAlso jArray.SelectToken("body.tzaCb5_data.disputes.summary").Count > 0) Then

                                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 78, "Number Of Active Disputes Contracts") & "</b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfActiveDisputesContracts") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfActiveDisputesContracts"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfActiveDisputesContracts"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfActiveDisputesContracts"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        Else
                                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If

                                        strBuilder.Append("            </tr>" & vbCrLf)

                                        strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                        strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 79, "Number Of Active Disputes In Court") & "</b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfActiveDisputesInCourt") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfActiveDisputesInCourt"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfActiveDisputesInCourt"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfActiveDisputesInCourt"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        Else
                                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        strBuilder.Append("            </tr>" & vbCrLf)


                                        strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                        strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 80, "Number Of Active Disputes Subject Data") & "</b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfActiveDisputesSubjectData") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfActiveDisputesSubjectData"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfActiveDisputesSubjectData"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfActiveDisputesSubjectData"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        Else
                                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        strBuilder.Append("            </tr>" & vbCrLf)

                                        strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                        strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 81, "Number Of Closed Disputes Contracts") & "</b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfClosedDisputesContracts") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfClosedDisputesContracts"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfClosedDisputesContracts"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfClosedDisputesContracts"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        Else
                                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        strBuilder.Append("            </tr>" & vbCrLf)


                                        strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                        strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 82, "Number Of Closed Disputes Subject Data") & "</b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfClosedDisputesSubjectData") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfClosedDisputesSubjectData"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfClosedDisputesSubjectData"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfClosedDisputesSubjectData"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                    Else
                                        strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                    End If

                                        strBuilder.Append("            </tr>" & vbCrLf)
                                        strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                                        strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 83, "Number Of False Disputes") & "</b></td>" & vbCrLf)

                                    If jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfFalseDisputes") IsNot Nothing Then
                                        If IsNothing(CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfFalseDisputes"), JToken), JValue).Value) = False AndAlso IsDBNull(CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfFalseDisputes"), JToken), JValue).Value) = False Then
                                            strBuilder.Append("                <td><b>" & CType(CType(jArray.SelectToken("body.tzaCb5_data.disputes.summary.numberOfFalseDisputes"), JToken), JValue).Value.ToString() & "</b></u></td>" & vbCrLf)
                                        Else
                                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        Else
                                            strBuilder.Append("                <td><b>&nbsp;</b></u></td>" & vbCrLf)
                                        End If
                                        strBuilder.Append("            </tr>" & vbCrLf)
                                    End If
                                    ' END DISPUTES SUMMARY

                                'Pinkal (15-Sep-2023) -- End

                                    strBuilder.Append("    </td>" & vbCrLf)
                                    strBuilder.Append("</tr>" & vbCrLf)
                                    strBuilder.Append("</TABLE>" & vbCrLf)
                                    strBuilder.Append("</BODY>" & vbCrLf)
                                    strBuilder.Append("</HEAD>" & vbCrLf)
                                    strBuilder.Append("</HTML>")

                                    divhtml.InnerHtml = strBuilder.ToString
                                    popupCRBReport.Show()

                            Else
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 84, "Report generation is failed.Reason : CRB Reponse is getting Error."), Me)
                                Exit Sub
                            End If 'If CInt(CType(CType(jArray.SelectToken("statusCode"), JToken), JValue).Value) = 600 Then

                        End If  ' If jArray IsNot Nothing AndAlso jArray.Count > 0 Then

                    End If     'If mdtGetCRBData IsNot Nothing AndAlso mdtGetCRBData.Rows.Count > 0 Then

            End If   ' If mstrReponseData.Trim.Length > 0 Then

                End If '  If e.CommandName.ToUpper = "GETDATA" Then 

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            blnpopupCRBData = True
            popupCRBData.Show()
        End Try
    End Sub

    Protected Sub dgCRBDocument_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCRBDocument.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(getColumnId_Datagrid(dgCRBDocument, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtCRBDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(getColumnId_Datagrid(dgCRBDocument, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtCRBDocument.Select("GUID = '" & e.Item.Cells(getColumnId_Datagrid(dgCRBDocument, "objcolhGUID", False, True)).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 50, "Are you sure you want to delete this attachment?")
                        popup_YesNo.Show()
                        mintDeleteRowIndex = mdtCRBDocument.Rows.IndexOf(xrow(0))
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupCRBData.Show()
        End Try
    End Sub

    'Pinkal (23-Nov-2022) -- End

    'Pinkal (20-Feb-2023) -- Start
    'NMB - Loan Approval Screen Enhancements.
    Protected Sub dgViewExposures_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgViewExposures.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                If e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgcolhMaturityDate", False, True)).Text <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgcolhMaturityDate", False, True)).Text <> "&nbsp;" Then
                    e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgcolhMaturityDate", False, True)).Text = CDate(e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgcolhMaturityDate", False, True)).Text).ToShortDateString()
                End If

                If e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgColhInstallmentAmt", False, True)).Text <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgColhInstallmentAmt", False, True)).Text <> "&nbsp;" Then
                    e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgColhInstallmentAmt", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgColhInstallmentAmt", False, True)).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgColhOustandingInterest", False, True)).Text <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgColhOustandingInterest", False, True)).Text <> "&nbsp;" Then
                    e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgColhOustandingInterest", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgColhOustandingInterest", False, True)).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgcolhOutstandingPrincipal", False, True)).Text <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgcolhOutstandingPrincipal", False, True)).Text <> "&nbsp;" Then
                    e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgcolhOutstandingPrincipal", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgViewExposures, "dgcolhOutstandingPrincipal", False, True)).Text), Session("fmtCurrency").ToString())
                End If

                If CType(dgViewExposures.DataSource, DataTable) IsNot Nothing AndAlso e.Item.ItemIndex = CType(dgViewExposures.DataSource, DataTable).Rows.Count - 1 Then
                    e.Item.Font.Bold = True
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (20-Feb-2023) -- End

    'Pinkal (21-Mar-2024) -- Start
    'NMB - Mortgage UAT Enhancements.
    Protected Sub dgCreditCardExposures_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCreditCardExposures.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                If e.Item.Cells(getColumnId_Datagrid(dgCreditCardExposures, "dgcolhWalletCreditLimit", False, True)).Text <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgCreditCardExposures, "dgcolhWalletCreditLimit", False, True)).Text <> "&nbsp;" Then
                    e.Item.Cells(getColumnId_Datagrid(dgCreditCardExposures, "dgcolhWalletCreditLimit", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgCreditCardExposures, "dgcolhWalletCreditLimit", False, True)).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(getColumnId_Datagrid(dgCreditCardExposures, "dgcolhOutstandingBalWithLoan", False, True)).Text <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgCreditCardExposures, "dgcolhOutstandingBalWithLoan", False, True)).Text <> "&nbsp;" Then
                    e.Item.Cells(getColumnId_Datagrid(dgCreditCardExposures, "dgcolhOutstandingBalWithLoan", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgCreditCardExposures, "dgcolhOutstandingBalWithLoan", False, True)).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(getColumnId_Datagrid(dgCreditCardExposures, "dgcolhOutstandingBalWithoutLoan", False, True)).Text <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgCreditCardExposures, "dgcolhOutstandingBalWithoutLoan", False, True)).Text <> "&nbsp;" Then
                    e.Item.Cells(getColumnId_Datagrid(dgCreditCardExposures, "dgcolhOutstandingBalWithoutLoan", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgCreditCardExposures, "dgcolhOutstandingBalWithoutLoan", False, True)).Text), Session("fmtCurrency").ToString())
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (21-Mar-2024) -- End


#End Region

#Region "Checkbox Events"
    'Hemant (22 Nov 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
    Private Sub chkSalaryAdvanceLiquidation_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSalaryAdvanceLiquidation.CheckedChanged
        Try
            If chkSalaryAdvanceLiquidation.Checked = True Then
                mdecFinalSalaryAdvancePendingPrincipalAmt = mdecActualSalaryAdvancePendingPrincipalAmt
            Else
                mdecFinalSalaryAdvancePendingPrincipalAmt = 0
            End If

            txtTakeHome.Text = Format(CDec(txtLoanAmt.Text) - (CDec(TxtOutStandingPrinciple.Text) + CDec(TxtOutstandingInterest.Text) + CDec(txtInsuranceAmt.Text)) - mdecFinalSalaryAdvancePendingPrincipalAmt, GUI.fmtCurrency)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (22 Nov 2024) -- End

#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblApplicationDate.ID, Me.LblApplicationDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkMakeExceptionalApplication.ID, Me.chkMakeExceptionalApplication.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmpName.ID, Me.lblEmpName.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanSchemeCategory.ID, Me.lblLoanSchemeCategory.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblRepaymentDays.ID, Me.lblRepaymentDays.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblMinLoanAmount.ID, Me.lblMinLoanAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblMaxLoanAmount.ID, Me.lblMaxLoanAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblMaxInstallment.ID, Me.lblMaxInstallment.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblMaxInstallmentAmt.ID, Me.lblMaxInstallmentAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanInterest.ID, Me.lblLoanInterest.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblInsuranceRate.ID, Me.lblInsuranceRate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblDeductionPeriod.ID, Me.LblDeductionPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanCalcType.ID, Me.lblLoanCalcType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblInterestCalcType.ID, Me.lblInterestCalcType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblAmt.ID, Me.lblAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.elLoanAmountCalculation.ID, Me.elLoanAmountCalculation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEMIInstallments.ID, Me.lblEMIInstallments.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPrincipalAmt.ID, Me.lblPrincipalAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblIntAmt.ID, Me.lblIntAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEMIAmount.ID, Me.lblEMIAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblInsuranceAmt.ID, Me.lblInsuranceAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTakeHome.ID, Me.lblTakeHome.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPurposeOfCredit.ID, Me.lblPurposeOfCredit.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblApproverRemark.ID, Me.LblApproverRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblPlotNo.ID, Me.lblPlotNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblBlockNo.ID, Me.lblBlockNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStreet.ID, Me.lblStreet.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblTownCity.ID, Me.lblTownCity.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblMarketValue.ID, Me.lblMarketValue.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCTNo.ID, Me.lblCTNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLONo.ID, Me.lblLONo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblFSV.ID, Me.lblFSV.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblModel.ID, Me.lblModel.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblYOM.ID, Me.lblYOM.Text)
            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblOutStandingPrinciple.ID, Me.LblOutStandingPrinciple.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblOutstandingInterest.ID, Me.LblOutstandingInterest.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblPaidInstallment.ID, Me.LblPaidInstallment.Text)
            'Pinkal (12-Oct-2022) -- End
          
            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblDocumentType.ID, Me.lblDocumentType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblOtherDocumentType.ID, Me.lblOtherDocumentType.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblChassisNo.ID, Me.lblChassisNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblSupplier.ID, Me.lblSupplier.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblColour.ID, Me.lblColour.Text)
            'Pinkal (27-Oct-2022) -- End

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblBOQ.ID, Me.LblBOQ.Text)
            'Pinkal (17-May-2024) -- End

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnApprove.ID, Me.btnApprove.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReject.ID, Me.btnReject.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)


            'Pinkal (20-Feb-2023) -- Start
            'NMB - Loan Approval Screen Enhancements.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblCRBAttachmentInfo.ID, Me.lblCRBAttachmentInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblCRBDocument.ID, Me.LblCRBDocument.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblCRBData.ID, Me.LblCRBData.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSaveCRBIdentityData.ID, Me.btnSaveCRBIdentityData.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCloseCRBIdentityData.ID, Me.btnCloseCRBIdentityData.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgViewExposures.Columns(0).FooterText, Me.dgViewExposures.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgViewExposures.Columns(1).FooterText, Me.dgViewExposures.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgViewExposures.Columns(3).FooterText, Me.dgViewExposures.Columns(3).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCRBGenReport.ID, Me.btnCRBGenReport.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnCRBReportClose.ID, Me.btnCRBReportClose.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnGetCRBData.ID, Me.btnGetCRBData.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnViewExposure.ID, Me.btnViewExposure.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblViewExposures.ID, Me.LblViewExposures.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnppViewExposures.ID, Me.btnppViewExposures.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgViewExposures.Columns(0).FooterText, Me.dgViewExposures.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgViewExposures.Columns(1).FooterText, Me.dgViewExposures.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgViewExposures.Columns(2).FooterText, Me.dgViewExposures.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgViewExposures.Columns(3).FooterText, Me.dgViewExposures.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgViewExposures.Columns(4).FooterText, Me.dgViewExposures.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgViewExposures.Columns(5).FooterText, Me.dgViewExposures.Columns(5).HeaderText)
            'Pinkal (20-Feb-2023) --End

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblVerifyOTP.ID, Me.lblVerifyOTP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblOTP.ID, Me.LblOTP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSendCodeAgain.ID, Me.btnSendCodeAgain.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnVerify.ID, Me.btnVerify.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnTOTPClose.ID, Me.btnTOTPClose.Text)
            'Pinkal (02-Jun-2023) -- End


            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblCreditCardExposures.ID, Me.LblCreditCardExposures.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgCreditCardExposures.Columns(0).FooterText, Me.dgCreditCardExposures.Columns(0).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgCreditCardExposures.Columns(1).FooterText, Me.dgCreditCardExposures.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgCreditCardExposures.Columns(2).FooterText, Me.dgCreditCardExposures.Columns(2).HeaderText)
            'Pinkal (21-Mar-2024) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkSalaryAdvanceLiquidation.ID, Me.chkSalaryAdvanceLiquidation.Text)
            'Hemant (22 Nov 2024) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.LblLoanApplicationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)
            Me.LblApplicationDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblApplicationDate.ID, Me.LblApplicationDate.Text)
            Me.chkMakeExceptionalApplication.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkMakeExceptionalApplication.ID, Me.chkMakeExceptionalApplication.Text)
            Me.lblEmpName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.lblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblLoanSchemeCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanSchemeCategory.ID, Me.lblLoanSchemeCategory.Text)
            Me.lblRepaymentDays.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRepaymentDays.ID, Me.lblRepaymentDays.Text)
            Me.lblMinLoanAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMinLoanAmount.ID, Me.lblMinLoanAmount.Text)
            Me.lblMaxLoanAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMaxLoanAmount.ID, Me.lblMaxLoanAmount.Text)
            Me.lblMaxInstallment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMaxInstallment.ID, Me.lblMaxInstallment.Text)
            Me.lblMaxInstallmentAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMaxInstallmentAmt.ID, Me.lblMaxInstallmentAmt.Text)
            Me.lblLoanInterest.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanInterest.ID, Me.lblLoanInterest.Text)
            Me.lblInsuranceRate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInsuranceRate.ID, Me.lblInsuranceRate.Text)
            Me.LblDeductionPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblDeductionPeriod.ID, Me.LblDeductionPeriod.Text)
            Me.lblLoanCalcType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanCalcType.ID, Me.lblLoanCalcType.Text)
            Me.lblInterestCalcType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInterestCalcType.ID, Me.lblInterestCalcType.Text)
            Me.lblAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAmt.ID, Me.lblAmt.Text)
            Me.elLoanAmountCalculation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.elLoanAmountCalculation.ID, Me.elLoanAmountCalculation.Text)
            Me.lblEMIInstallments.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEMIInstallments.ID, Me.lblEMIInstallments.Text)
            Me.lblPrincipalAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPrincipalAmt.ID, Me.lblPrincipalAmt.Text)
            Me.lblIntAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIntAmt.ID, Me.lblIntAmt.Text)
            Me.lblEMIAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEMIAmount.ID, Me.lblEMIAmount.Text)
            Me.lblInsuranceAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInsuranceAmt.ID, Me.lblInsuranceAmt.Text)
            Me.lblTakeHome.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTakeHome.ID, Me.lblTakeHome.Text)
            Me.lblPurposeOfCredit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPurposeOfCredit.ID, Me.lblPurposeOfCredit.Text)
            Me.LblApproverRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblApproverRemark.ID, Me.LblApproverRemark.Text)
            Me.lblPlotNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPlotNo.ID, Me.lblPlotNo.Text)
            Me.lblBlockNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBlockNo.ID, Me.lblBlockNo.Text)
            Me.lblStreet.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStreet.ID, Me.lblStreet.Text)
            Me.lblTownCity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTownCity.ID, Me.lblTownCity.Text)
            Me.lblMarketValue.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMarketValue.ID, Me.lblMarketValue.Text)
            Me.lblCTNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCTNo.ID, Me.lblCTNo.Text)
            Me.lblLONo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLONo.ID, Me.lblLONo.Text)
            Me.lblFSV.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFSV.ID, Me.lblFSV.Text)
            Me.lblModel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblModel.ID, Me.lblModel.Text)
            Me.lblYOM.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblYOM.ID, Me.lblYOM.Text)

            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            Me.LblOutStandingPrinciple.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblOutStandingPrinciple.ID, Me.LblOutStandingPrinciple.Text)
            Me.LblOutstandingInterest.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblOutstandingInterest.ID, Me.LblOutstandingInterest.Text)
            Me.LblPaidInstallment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblPaidInstallment.ID, Me.LblPaidInstallment.Text)
            'Pinkal (12-Oct-2022) -- End


            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            Me.lblDocumentType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDocumentType.ID, Me.lblDocumentType.Text)
            Me.lblOtherDocumentType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblOtherDocumentType.ID, Me.lblOtherDocumentType.Text)
            Me.lblChassisNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblChassisNo.ID, Me.lblChassisNo.Text)
            Me.lblSupplier.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSupplier.ID, Me.lblSupplier.Text)
            Me.lblColour.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblColour.ID, Me.lblColour.Text)
            'Pinkal (27-Oct-2022) -- End

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            Me.LblBOQ.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblBOQ.ID, Me.LblBOQ.Text)
            'Pinkal (17-May-2024) -- End


            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnApprove.ID, Me.btnApprove.Text).Replace("&", "")
            Me.btnReject.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReject.ID, Me.btnReject.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            'Pinkal (20-Feb-2023) -- Start
            'NMB - Loan Approval Screen Enhancements.
            Me.lblCRBAttachmentInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCRBAttachmentInfo.ID, Me.lblCRBAttachmentInfo.Text)
            Me.LblCRBDocument.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblCRBDocument.ID, Me.LblCRBDocument.Text)
            Me.LblCRBData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblCRBData.ID, Me.LblCRBData.Text)
            Me.btnSaveCRBIdentityData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSaveCRBIdentityData.ID, Me.btnSaveCRBIdentityData.Text)
            Me.btnCloseCRBIdentityData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCloseCRBIdentityData.ID, Me.btnCloseCRBIdentityData.Text)
            Me.dgViewExposures.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgViewExposures.Columns(0).FooterText, Me.dgViewExposures.Columns(0).HeaderText)
            Me.dgViewExposures.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgViewExposures.Columns(1).FooterText, Me.dgViewExposures.Columns(1).HeaderText)
            Me.dgViewExposures.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgViewExposures.Columns(3).FooterText, Me.dgViewExposures.Columns(3).HeaderText)

            Me.btnCRBGenReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCRBGenReport.ID, Me.btnCRBGenReport.Text)

            Me.btnCRBReportClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnCRBReportClose.ID, Me.btnCRBReportClose.Text)
            Me.btnGetCRBData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnGetCRBData.ID, Me.btnGetCRBData.Text)

            Me.btnViewExposure.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnViewExposure.ID, Me.btnViewExposure.Text)
            Me.LblViewExposures.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblViewExposures.ID, Me.LblViewExposures.Text)
            Me.btnppViewExposures.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnppViewExposures.ID, Me.btnppViewExposures.Text)
            Me.dgViewExposures.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgViewExposures.Columns(0).FooterText, Me.dgViewExposures.Columns(0).HeaderText)
            Me.dgViewExposures.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgViewExposures.Columns(1).FooterText, Me.dgViewExposures.Columns(1).HeaderText)
            Me.dgViewExposures.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgViewExposures.Columns(2).FooterText, Me.dgViewExposures.Columns(2).HeaderText)
            Me.dgViewExposures.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgViewExposures.Columns(3).FooterText, Me.dgViewExposures.Columns(3).HeaderText)
            Me.dgViewExposures.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgViewExposures.Columns(4).FooterText, Me.dgViewExposures.Columns(4).HeaderText)
            Me.dgViewExposures.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgViewExposures.Columns(5).FooterText, Me.dgViewExposures.Columns(5).HeaderText)
            'Pinkal (20-Feb-2023) --End

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Me.lblVerifyOTP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVerifyOTP.ID, Me.lblVerifyOTP.Text)
            Me.LblOTP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblOTP.ID, Me.LblOTP.Text)
            Me.btnSendCodeAgain.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSendCodeAgain.ID, Me.btnSendCodeAgain.Text)
            Me.btnVerify.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnVerify.ID, Me.btnVerify.Text)
            Me.btnTOTPClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnTOTPClose.ID, Me.btnTOTPClose.Text)
            'Pinkal (02-Jun-2023) -- End


            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            Me.LblCreditCardExposures.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblCreditCardExposures.ID, Me.LblCreditCardExposures.Text)
            Me.dgCreditCardExposures.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgCreditCardExposures.Columns(0).FooterText, Me.dgCreditCardExposures.Columns(0).HeaderText)
            Me.dgCreditCardExposures.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgCreditCardExposures.Columns(1).FooterText, Me.dgCreditCardExposures.Columns(1).HeaderText)
            Me.dgCreditCardExposures.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgCreditCardExposures.Columns(2).FooterText, Me.dgCreditCardExposures.Columns(2).HeaderText)
            'Pinkal (21-Mar-2024) -- End


            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            Me.chkSalaryAdvanceLiquidation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkSalaryAdvanceLiquidation.ID, Me.chkSalaryAdvanceLiquidation.Text)
            'Hemant (22 Nov 2024) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Applied Amount cannot be blank. Applied Amount is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Currency is compulsory information.Please select currency.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Installment Amount cannot be 0.Please define installment amount greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Installment Amount cannot be greater than Loan Amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Principal Amount cannot be 0. Please enter Principal amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Plot No is compulsory information. Please enter Plot No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Block No is compulsory information. Please enter Block No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Street is compulsory information. Please enter Street to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Town/City is compulsory information. Please enter Town/City to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Market Value cannot be 0. Please enter Market Value.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "CT No is compulsory information. Please enter CT No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "LO No is compulsory information. Please enter LO No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "FSV cannot be 0.Please enter FSV.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Model is compulsory information. Please enter Model to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "YOM is compulsory information. Please enter YOM to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Installment months cannot be greater than")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "for")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, " Scheme.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "Sorry, you can not apply for this loan scheme: Reason, Principal amount  is exceeding the Max Principal Amt")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, " set for selected scheme.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 23, "Sorry, you can not apply for this loan scheme: Reason, Installment amount  is exceeding the Max Installment amount")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 24, "Sorry, you can not apply for this loan scheme: Reason, Principal amount  should be greater than the Minimum Principal amount")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 25, "Sorry, you are not confirmed yet. Please contact HR support.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 26, "Sorry, signature missing. Please update signature from your profile.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 27, "Sorry, your contract duration is less than 6 months. Please contact HR support.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 28, "Sorry, NIDA number is missing or in pending state.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 29, "Sorry, you have a pending disciplinary issue. Kindly contact HR ER.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 30, "Sorry, installments cannot go beyond the end of contract date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 31, "Sorry, no of installments selected cannot go beyond the retirement date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 32, "Remark cannot be blank.Remark is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 33, "File does not Exist...")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 34, "You can't Edit this Loan detail. Reason: This Loan is already rejected.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 35, "You can't Edit this Loan detail. Reason: This Loan is already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 36, "You can't Edit this Loan detail. Reason: This Loan is already approved/reject.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 37, "Sorry, you can not apply for Topup for this loan scheme: Reason, TopUp is not allowed")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 38, "set for selected scheme.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 39, "Sorry, you can not apply for Topup for this loan scheme: Reason, No of Installment Paid should be greater than minimum No of Installment Paid")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 40, "Deduction Period is compulsory information.Please Select Deduction Period to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 41, "Chassis No is compulsory information. Please enter Chassis No to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 42, "Supplier is compulsory information. Please enter Supplier to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 43, "Colour is compulsory information. Please enter Colour to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 44, "Loan Application")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 45, "successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 46, "Installment months cannot be less than")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 47, "Please get atleast one identity data from CRB system or attach CRB Report.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 48, "File does not exist on localpath")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 49, "Selected information is already present for particular employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 50, "Are you sure you want to delete this attachment?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 51, "You can not generate this report.Reason : This identity type got error status to get CRB response.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 52, "You can not generate this report.Reason : Before Generate Report you have to get CRB response for this identity type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 53, "CRB Status Report")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 54, "Personal Information")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 55, "Full Name")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 56, "Date of Birth")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 57, "Age")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 58, "Gender")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 59, "Marital Status")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 60, "Employment Status")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 61, "Identification Used & Request Information")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 62, "Currency")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 63, "SubjectID Number")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 64, "Request Date")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 65, "Reference Number")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 66, "Open Contracts")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 67, "Contract Status")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 68, "Outstanding Amount")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 69, "Past Due Amount")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 70, "Past Due Days")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 71, "Phase Of Contract")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 72, "Role Of Client")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 73, "Sector")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 74, "Reporting Date")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 75, "Total Amount")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 76, "Type Of Contract")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 77, "Disputes Summary")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 78, "Number Of Active Disputes Contracts")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 79, "Number Of Active Disputes In Court")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 80, "Number Of Active Disputes Subject Data")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 81, "Number Of Closed Disputes Contracts")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 82, "Number Of Closed Disputes Subject Data")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 83, "Number Of False Disputes")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 84, "Report generation is failed.Reason : CRB Reponse is getting Error.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 85, "Please enter any one tranche to complete the disbursement funds.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 86, "Sum of All Disbursement tranches should not be greater than principal amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 87, "and disbursed")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 88, "FSV cannot be less than the Market Value.Please enter proper FSV value.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 89, "Principal amount cannot be greather than the FSV Value.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 90, "Sorry, you do not qualify for a top-up at the moment. The take home amount should be greater than 0.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 91, "Sum of All Disbursement tranches must be equal to principal amount.")
            'Pinkal (15-Feb-2023) -- Start
            'When Employee Deleted the loan application and approver again access that link at that time we can give this message.
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 92, "Sorry, there is no pending loan application data.")
            'Pinkal (15-Feb-2023) -- End

            'Pinkal (20-Feb-2023) -- Start
            'NMB - Loan Approval Screen Enhancements.
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 93, "Total")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 94, "This employee doesn't have any bank account no.Please contact administrator.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 95, "This employee doesn't have any bank details.Please contact administrator.")
            'Pinkal (20-Feb-2023) -- End

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 96, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan approval.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 97, "Mobile No is compulsory information. Please set Approver's Mobile No to send TOTP.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 98, "You cannot save this loan application.Reason :  As You reached Maximum unsuccessful attempts.Please try after")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 99, "mintues")
            'Pinkal (02-Jun-2023) -- End
            'Hemant (07 Jul 2023) -- Start
            'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 100, "Title Issue Date is compulsory information. Please enter Title Issue Date to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 101, "Title Validity cannot be 0.Please enter Title Validity.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 102, "Title Expriry Date is compulsory information. Please enter Title Expriry Date to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 103, "Sorry, you can not apply for this Mortgage Loan: Reason, Title Expiry Date should not be less than Date :")
            'Hemant (07 July 2023) -- End            

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 104, "is compulsory information.Please set")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 105, "should be less than")
            'Pinkal (04-Aug-2023) -- End

            'Hemant (10 May 2024) -- Start
            'ENHANCEMENT(TADB): A1X - 2598 : Option to force privileged loan approvers to attach documents during the approval
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 106, "Approver Scan/Document(s) is mandatory information.Please attach Approver Scan/Document(s) in order to perform operation. ")
            'Hemant (10 May 2024) -- End

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 107, "Bill of Quantity(BOQ) cannot be blank.Please enter Bill of Quantity(BOQ).")
            'Pinkal (17-May-2024) -- End

            'Hemant (16 Aug 2024) -- Start
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 108, "FSV cannot be more than the Market Value.Please enter proper FSV value.")
            'Hemant (16 Aug 2024) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

   
   


End Class
