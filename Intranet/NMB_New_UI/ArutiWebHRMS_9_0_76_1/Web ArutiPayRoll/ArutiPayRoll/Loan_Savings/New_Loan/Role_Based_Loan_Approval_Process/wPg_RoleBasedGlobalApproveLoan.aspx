﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_RoleBasedGlobalApproveLoan.aspx.vb"
    Title="Role Based Global Approve Loan" Inherits="Loan_Savings_New_Loan_Role_Based_Loan_Approval_Process_wPg_RoleBasedGlobalApproveLoan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        $("body").on("click", "[id*=chkAllSelectApp]", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("[id*=chkSelectApp]").prop("checked", $(chkHeader).prop("checked"));

        });

        $("body").on("click", "[id*=chkSelectApp]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkAllSelectApp]", grid);

            if ($("[id*=chkSelectApp]", grid).length == $("[id*=chkSelectApp]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }

        });


        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }

        function countdown(minutes, seconds) {
            var element, endTime, mins, msLeft, time;

            function twoDigits(n) {
                return (n <= 9 ? "0" + n : n);
            }

            function updateTimer() {
                msLeft = endTime - (+new Date);

                if (msLeft < 1000) {
                    element.innerHTML = '00:00';
                    document.getElementById('<%=txtVerifyOTP.ClientID %>').readOnly = true;
                    document.getElementById('<%=btnVerify.ClientID %>').disabled = "disabled";
                    document.getElementById('<%=btnSendCodeAgain.ClientID %>').disabled = "";
                    document.getElementById('<%=btnTOTPClose.ClientID %>').disabled = "";
                } else {
                    document.getElementById('<%=btnSendCodeAgain.ClientID %>').disabled = "disabled";
                    document.getElementById('<%=btnTOTPClose.ClientID %>').disabled = "disabled";
                    time = new Date(msLeft);
                    mins = time.getUTCMinutes();
                    element.innerHTML = (twoDigits(mins)) + ':' + twoDigits(time.getUTCSeconds());
                    setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
                }
                if (msLeft <= 0) {
                    return;
                }
                document.getElementById('<%=hdf_TOTP.ClientID %>').value = twoDigits(time.getUTCSeconds());
            }
            element = document.getElementById('<%=LblSeconds.ClientID %>');
            endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
            updateTimer();
        }

        function VerifyOnClientClick() {
            var val = document.getElementById('<%=hdf_TOTP.ClientID %>').value;
            if (val !== undefined) {
                countdown(0, val, false);
            }
        }
    </script>

    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Role Based Global Approve Loan"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbFilter" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                                    <h2 />
                                                    <ul class="header-dropdown m-r--5">
                                                        <li class="dropdown">
                                                            <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" ToolTip="Allocations">
                                                            <i class="fas fa-sliders-h"></i>
                                                            </asp:LinkButton>
                                                        </li>
                                                    </ul>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboLoanScheme" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblDeductionPeriod" runat="server" Text="Deduction Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboDeductionPeriod" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLoanAmount" runat="server" Text="Loan Amount" CssClass="form-label"></asp:Label>
                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-l-0">
                                                            <div class="form-group m-t-0">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtLoanAmount" runat="server" Text="0" onKeypress="return onlyNumbers(this, event);"
                                                                        Style="text-align: right" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                                            <div class="form-group m-t-0">
                                                                <asp:DropDownList ID="cboAmountCondition" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbInfo" runat="server" Text="Information" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtApprover" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboStatus" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card inner-card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="gbPending" runat="server" Text="Pending Loan Application(s)." CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                DataKeyField="pendingloanaprovalunkid" ShowFooter="False" Width="125%" HeaderStyle-Font-Bold="false"
                                                CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="objdgcolhSelect">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAllSelectApp" runat="server" CssClass="filled-in" Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelectApp" runat="server" CssClass="filled-in" Text=" " />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--1--%>
                                                    <asp:BoundColumn DataField="ApplicationNo" HeaderText="Application No" FooterText="dgcolhApplicationNo" />
                                                    <%--2--%>
                                                    <asp:BoundColumn DataField="ApplicationDate" HeaderText="Application Date" FooterText="dgcolhApplicationDate" />
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="Employee" HeaderText="Employee" FooterText="dgcolhEName" />
                                                    <%--4--%>
                                                    <asp:BoundColumn DataField="Period" HeaderText="Deduction Period" FooterText="dgcolhPeriod" />
                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="sectiongrp" HeaderText="Section Group" FooterText="dgcolhSectiongrp">
                                                    </asp:BoundColumn>
                                                    <%--6--%>
                                                    <asp:BoundColumn DataField="classgrp" HeaderText="Class Group" FooterText="dgcolhClassgrp" />
                                                    <%--7--%>
                                                    <asp:BoundColumn DataField="classes" HeaderText="Class" FooterText="dgcolhClass" />
                                                    <%--8--%>
                                                    <asp:BoundColumn DataField="loan_amount" HeaderText="Applied Amount" ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhAppliedAmt"></asp:BoundColumn>
                                                    <%--9--%>
                                                    <asp:BoundColumn DataField="installmentamt" HeaderText="Installment Amount" ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhInstallmentAmt"></asp:BoundColumn>
                                                    <%--10--%>
                                                    <asp:BoundColumn DataField="deductionperiodunkid" HeaderText="objcolhDeductionPeriodId"
                                                        Visible="false" FooterText="objcolhDeductionPeriodId"></asp:BoundColumn>
                                                    <%--11--%>
                                                    <asp:BoundColumn DataField="processpendingloanunkid" HeaderText="objcolhPendingUnkid"
                                                        Visible="false" FooterText="objcolhPendingUnkid" />
                                                    <%--12--%>
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="objcolhEmployeeId" Visible="false"
                                                        FooterText="objcolhEmployeeId" />
                                                    <%--12--%>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popup_TOTP" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="hdf_TOTP" PopupControlID="pnl_TOTP" TargetControlID="hdf_TOTP">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_TOTP" runat="server" CssClass="card modal-dialog" Style="display: none;">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblVerifyOTP" runat="server" Text="Verify OTP"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="LblOTP" runat="server" Text="Enter OTP" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtVerifyOTP" runat="server" CssClass="form-control" MaxLength="6"
                                                onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                    <asp:Label ID="LblSeconds" runat="server" Text="00:00" CssClass="form-label" Font-Bold="true"
                                        ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnSendCodeAgain" runat="server" Text="Resend OTP" CssClass="btn btn-primary" />
                            <asp:Button ID="btnVerify" runat="server" Text="Verify" CssClass="btn btn-primary" />
                            <asp:Button ID="btnTOTPClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                            <asp:HiddenField ID="hdf_TOTP" runat="server" />
                        </div>
                    </asp:Panel>
                    <uc1:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
