﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Globalization
#End Region

Partial Class Loan_Savings_New_Loan_Role_Based_Loan_Approval_Process_wPg_RoleBasedLoanApprovalList
    Inherits Basepage

#Region "Private Variables"

    Private Shared ReadOnly mstrModuleName As String = "frmRoleBasedLoanApprovalList"
    Dim DisplayMessage As New CommonCodes
    Private objRoleApprovaltran As New clsroleloanapproval_process_Tran
    Private mstrAdvanceFilter As String = ""
    Private dtApprovalList As New DataTable
    Private mstrEmployeeIDs As String = ""

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            'Pinkal (20-Jan-2023) -- Start
            'Solved NMB Issue for Randamely Access of URL in Loan module.
            If Session("UserId") Is Nothing OrElse CInt(Session("UserId")) <= 0 Then
                DisplayMessage.DisplayMessage("you are not authorized to access this page.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Pinkal (20-Jan-2023) -- End

            If IsPostBack = False Then
                Call SetControlCaptions()
                'Pinkal (14-Dec-2022) -- Start
                'NMB Loan Module Enhancement.
                SetMessages()
                'Pinkal (14-Dec-2022) -- End

                Call SetLanguage()
                Call FillCombo()

                chkMyApprovals.Checked = True

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                chkMyApprovals.Visible = False
                'Pinkal (04-Aug-2023) -- End

                If dgLoanApproval.Items.Count <= 0 Then
                    'Pinkal (10-Jul-2023) -- Start
                    'NMB Enhancement :(A1X-1113) NMB - Staff Transfer Notifications to employee and approvers
                    'dgLoanApproval.DataSource = dtApprovalList
                    'dgLoanApproval.DataBind()
                    dgLoanApproval.DataSource = New List(Of String)
                    dgLoanApproval.DataBind()
                    'Pinkal (10-Jul-2023) -- End
                End If

                'Pinkal (20-Jan-2023) -- Start
                'Solved NMB Issue for Randamely Access of URL in Loan module.
                dgLoanApproval.Columns(0).Visible = CBool(Session("AllowToChangeLoanStatus"))
                'Pinkal (20-Jan-2023) -- End


                'Pinkal (15-May-2023) -- Start
                ' (A1X-850) Tujijenge - Dashboard pending actions to show list of records after redirecting to the action page.
                If Request.QueryString.Count > 0 AndAlso CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    If Request.QueryString.Keys(0).Contains("Id") Then
                        Dim ar As String = Server.UrlDecode(clsCrypto.Dicrypt(Request.QueryString("Id"))).ToString()
                        If ar.ToString().Length > 0 AndAlso ar.ToString().ToUpper() = "APPROVEDLOANFROMDASHBOARD" Then
                            btnSearch_Click(btnSearch, New EventArgs())
                        End If
                    End If
                End If
                'Pinkal (15-May-2023) -- End

            Else
                'Pinkal (10-Jul-2023) -- Start
                'NMB Enhancement :(A1X-1113) NMB - Staff Transfer Notifications to employee and approvers
                'dtApprovalList = CType(Me.ViewState("ApprovalList"), DataTable)
                'Pinkal (10-Jul-2023) -- End
                mstrEmployeeIDs = CStr(Me.ViewState("EmployeeIDs"))
                mstrAdvanceFilter = CStr(Me.ViewState("AdvanceFilter"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Pinkal (10-Jul-2023) -- Start
            'NMB Enhancement :(A1X-1113) NMB - Staff Transfer Notifications to employee and approvers
            'Me.ViewState("ApprovalList") = dtApprovalList
            'Pinkal (10-Jul-2023) -- End
            Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            Dim objEmployee As New clsEmployee_Master
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objLoanApplication As New clsProcess_pending_loan
            Dim objMasterData As New clsMasterData
            'Pinkal (20-Jan-2023) -- Start
            'Solved NMB Issue for Randamely Access of URL in Loan module.

            'Dim objLevel As New clslnapproverlevel_master
            'Dim objLoanApprover As New clsLoanApprover_master

            'dsList = objLoanApprover.GetEmployeeFromUser(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                          CStr(Session("UserAccessModeSetting")), _
            '                                          True, _
            '                                          CBool(Session("IsIncludeInactiveEmp")), _
            '                                          "List")

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                       CInt(Session("UserId")), _
                                       CInt(Session("Fin_year")), _
                                       CInt(Session("CompanyUnkId")), _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                       Session("UserAccessModeSetting").ToString(), True, _
                                       False, "List", True)

            'Pinkal (20-Jan-2023) -- End

            With cboEmployee
                .DataTextField = "EmpCodeName"
                .DataValueField = "employeeunkid"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim lstIDs As List(Of String) = (From p In dsList.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
            mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

            Dim mblnSchemeShowOnEss As Boolean = False
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then mblnSchemeShowOnEss = True
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", mblnSchemeShowOnEss)

            With cboLoanScheme
                .DataTextField = "name"
                .DataValueField = "loanschemeunkid"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objLoanApplication.GetLoan_Status("Status", True, False)
            With cboStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsList.Tables("Status")
                .DataBind()
                'Pinkal (10-Jul-2023) -- Start
                'NMB Enhancement :(A1X-1113) NMB - Staff Transfer Notifications to employee and approvers
                '.SelectedValue = "1"
                .SelectedValue = "0"
                'Pinkal (10-Jul-2023) -- End
            End With

            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            With cboApproverStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsList.Tables("Status").Copy()
                .DataBind()
                .SelectedValue = "1"
            End With
            'Pinkal (27-Oct-2022) -- End

            dsList = objMasterData.GetCondition(False, True, True, False, False)
            Dim dtCondition As DataTable = New DataView(dsList.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            With cboAmountCondition
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dtCondition
                .DataBind()
                .SelectedValue = "0"
            End With


            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(CInt(Session("UserId")))
                txtApprover.Text = objUser._Username.ToString()
                objUser = Nothing
            Else
                lblApprover.Visible = False
                txtApprover.Visible = False
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim strSearch As String = ""
            Dim dsList As DataSet = Nothing

            'Pinkal (20-Jan-2023) -- Start
            'Solved NMB Issue for Randamely Access of URL in Loan module.
            If CBool(Session("AllowToViewLoanApprovalList")) = False Then Exit Sub
            'Pinkal (20-Jan-2023) -- End

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND ln.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                strSearch &= "AND ln.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearch &= "AND ln.loan_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            'Pinkal (10-Jul-2023) -- Start
            'NMB Enhancement :(A1X-1113) NMB - Staff Transfer Notifications to employee and approvers
            If CInt(cboApproverStatus.SelectedValue) > 0 Then
                strSearch &= "AND lna.statusunkid = " & CInt(cboApproverStatus.SelectedValue) & " AND lna.visibleunkid = " & CInt(cboApproverStatus.SelectedValue) & " "
            Else
                strSearch &= "AND lna.visibleunkid <> -1 " & " "
            End If
            'Pinkal (10-Jul-2023) -- End

            If txtLoanAmount.Text.Trim <> "" AndAlso CDec(txtLoanAmount.Text) > 0 Then
                strSearch &= "AND lna.loan_amount " & cboAmountCondition.SelectedItem.Text & " " & CDec(txtLoanAmount.Text) & " "
            End If

            If dtpFromDate.IsNull = False Then
                strSearch &= "AND ln.application_date >= '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date) & "' "
            End If

            If dtpToDate.IsNull = False Then
                strSearch &= "AND ln.application_date >= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            End If


            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsList = objRoleApprovaltran.GetRoleBasedApprovalList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
                                             "List", strSearch, mstrAdvanceFilter, True, Nothing)

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            dtApprovalList = dsList.Tables(0).Copy()

            Dim dtTable As New DataTable
            If chkMyApprovals.Checked Then
                dtTable = New DataView(dtApprovalList, "mapuserunkid = " & CInt(Session("UserId")) & " OR mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
            Else
                If mstrEmployeeIDs.Trim.Length > 0 Then
                    dtTable = New DataView(dtApprovalList, "employeeunkid  in (" & mstrEmployeeIDs & ") OR mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
                End If
            End If
            'Pinkal (04-Aug-2023) -- End

            Dim mintProcesspendingloanunkid As Integer = 0
            Dim dList As DataTable = Nothing
            Dim mstrStaus As String = ""

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                For Each drRow As DataRow In dtTable.Rows

                    If CInt(drRow("pendingloanaprovalunkid")) <= 0 Then Continue For
                    mstrStaus = ""

                    If mintProcesspendingloanunkid <> CInt(drRow("processpendingloanunkid")) Then
                        dList = New DataView(dtTable, "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND processpendingloanunkid = " & CInt(drRow("processpendingloanunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                        mintProcesspendingloanunkid = CInt(drRow("processpendingloanunkid"))
                    End If

                    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                        Dim dr As DataRow() = dList.Select("priority >= " & CInt(drRow("priority")))

                        If dr.Length > 0 Then

                            For i As Integer = 0 To dr.Length - 1

                                If CInt(drRow("statusunkid")) = 2 Then
                                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Approved By :-") & " " & drRow("loginuser").ToString()
                                    Exit For

                                ElseIf CInt(drRow("statusunkid")) = 1 Then

                                    'Pinkal (04-Aug-2023) -- Start
                                    '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                                    'If CInt(dr(i)("statusunkid")) = 2 Then
                                    If CInt(dr(i)("visibleunkid")) = 2 Then
                                        'Pinkal (04-Aug-2023) -- End
                                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Approved By :-") & " " & dr(i)("loginuser").ToString()
                                        Exit For

                                    ElseIf CInt(dr(i)("statusunkid")) = 3 Then
                                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Rejected By :-") & " " & dr(i)("loginuser").ToString()
                                        Exit For

                                    End If

                                ElseIf CInt(drRow("statusunkid")) = 3 Then
                                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Rejected By :-") & " " & drRow("loginuser").ToString()
                                    Exit For

                                End If

                            Next

                        End If

                    End If

                    If mstrStaus <> "" Then
                        drRow("status") = mstrStaus.Trim
                    End If

                Next

            End If

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            'dtApprovalList = dsList.Tables(0).Copy()

            'Dim dtTable As New DataTable
            'If chkMyApprovals.Checked Then
            '    dtTable = New DataView(dtApprovalList, "mapuserunkid = " & CInt(Session("UserId")) & " OR mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    If mstrEmployeeIDs.Trim.Length > 0 Then
            '        dtTable = New DataView(dtApprovalList, "employeeunkid  in (" & mstrEmployeeIDs & ") OR mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
            '    End If
            'End If
            'Pinkal (04-Aug-2023) -- End

            Dim dtTemp As DataTable = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "processpendingloanunkid", "employeeunkid")

            Dim dRow1 = From drTable In dtTable Group Join drTemp In dtTemp On drTable.Field(Of Integer)("processpendingloanunkid") Equals drTemp.Field(Of Integer)("processpendingloanunkid") And _
                         drTable.Field(Of Integer)("employeeunkid") Equals drTemp.Field(Of Integer)("employeeunkid") Into Grp = Group Select drTable


            If dRow1.Count > 0 Then
                dRow1.ToList.ForEach(Function(x) DeleteRow(x, dtTable))
            End If

            'dgLoanApproval.Columns(0).Visible = CBool(Session("AllowToChangeLoanStatus"))
            dgLoanApproval.AutoGenerateColumns = False
            dgLoanApproval.DataSource = dtTable
            dgLoanApproval.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function DeleteRow(ByVal dr As DataRow, ByVal dtTable As DataTable) As Boolean
        Try
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drRow() As DataRow = dtTable.Select("processpendingloanunkid = " & CInt(dr("processpendingloanunkid").ToString()) & " AND employeeunkid = " & CInt(dr("employeeunkid")))
                If drRow.Length <= 1 Then
                    dtTable.Rows.Remove(drRow(0))
                    dtTable.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return True
    End Function

#End Region

#Region "Button's Events"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            dgLoanApproval.CurrentPageIndex = 0
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            dgLoanApproval.DataSource = New List(Of String)
            dgLoanApproval.DataBind()

            cboEmployee.SelectedValue = "0"
            cboLoanScheme.SelectedValue = "0"
            cboStatus.SelectedValue = "1"
            cboAmountCondition.SelectedIndex = 0

            txtLoanAmount.Text = "0"
            mstrAdvanceFilter = ""

            dtpFromDate.SetDate = Nothing
            dtpToDate.SetDate = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Link Button's Events"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DataGrid Events"

    Protected Sub dgLoanApproval_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgLoanApproval.ItemCommand
        Dim objRoleLoanApproval As New clsroleloanapproval_process_Tran
        Try
            If e.CommandName.ToUpper = "STATUS" Then

                If CInt(Session("UserId")) <> CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgLoanApproval, "objdgcolhUserID", False, True)).Text) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "You can't Edit this Loan detail. Reason: You are logged in into another user login."), Me)
                    Exit Sub
                End If


                objRoleLoanApproval._Pendingloanaprovalunkid = CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanApproval, "objdgcolhpendingloanaprovalunkid", False, True)).Text)

                Dim dsList As DataSet
                Dim dtList As DataTable

                Dim mstrSearch As String = "lna.processpendingloanunkid = " & CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanApproval, "objdgcolhprocesspendingloanunkid", False, True)).Text) & " AND lna.pendingloanaprovalunkid <> " & CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanApproval, "objdgcolhpendingloanaprovalunkid", False, True)).Text)

                dsList = objRoleLoanApproval.GetRoleBasedApprovalList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", mstrSearch, "", False, Nothing)

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                        If objRoleLoanApproval._Priority > CInt(dsList.Tables(0).Rows(i)("Priority")) Then
                            dtList = New DataView(dsList.Tables(0), "levelunkid = " & CInt(dsList.Tables(0).Rows(i)("levelunkid")) & " AND statusunkid = " & enLoanApplicationStatus.APPROVED, "", DataViewRowState.CurrentRows).ToTable
                            If dtList.Rows.Count > 0 Then Continue For

                            If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = enLoanApplicationStatus.APPROVED Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                                Exit Sub
                            ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = enLoanApplicationStatus.REJECTED Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                                Exit Sub
                            End If

                        ElseIf objRoleLoanApproval._Priority <= CInt(dsList.Tables(0).Rows(i)("Priority")) Then

                            If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = enLoanApplicationStatus.APPROVED Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                                Exit Sub
                            ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = enLoanApplicationStatus.REJECTED Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                                Exit Sub
                            End If
                        End If
                    Next
                End If

                If CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanApproval, "objdgcolhStatusunkid", False, True)).Text) = enLoanApplicationStatus.APPROVED Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                    Exit Sub
                End If

                If CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanApproval, "objdgcolhLoanStatusID", False, True)).Text) = enLoanApplicationStatus.APPROVED Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                        Exit Sub
                ElseIf CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanApproval, "objdgcolhLoanStatusID", False, True)).Text) = enLoanApplicationStatus.REJECTED Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                        Exit Sub
                    End If
          

                Session("pendingloanaprovalunkid") = CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanApproval, "objdgcolhpendingloanaprovalunkid", False, True)).Text)
                Session("ProcessPendingLoanunkid") = CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanApproval, "objdgcolhprocesspendingloanunkid", False, True)).Text)
                Session("Mapuserunkid") = CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanApproval, "objdgcolhUserID", False, True)).Text)

                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Role_Based_Loan_Approval_Process/wPg_RoleBasedLoanApproval.aspx", False)

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objRoleLoanApproval = Nothing
        End Try
    End Sub

    Protected Sub dgLoanApproval_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgLoanApproval.ItemDataBound
        Try
            'Pinkal (10-Jul-2023) -- Start
            'NMB Enhancement :(A1X-1113) NMB - Staff Transfer Notifications to employee and approvers
            If e.Item.ItemType = ListItemType.Header Then
            SetDateFormat()
                e.Item.Font.Bold = True
            End If
            'Pinkal (10-Jul-2023) -- End

            If e.Item.ItemIndex >= 0 Then

                If CBool(e.Item.Cells(11).Text) = True Then
                    CType(e.Item.Cells(0).FindControl("lnkChangeStatus"), LinkButton).Visible = False

                    e.Item.Cells(0).Visible = False
                    e.Item.Cells(2).ColumnSpan = e.Item.Cells.Count - 8
                    e.Item.Cells(2).Text = e.Item.Cells(1).Text
                    e.Item.Cells(2).Font.Bold = True

                    For i = 3 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next

                    e.Item.Cells(2).CssClass = "group-header"
                    e.Item.Cells(2).Style.Add("text-align", "left")

                Else
                    If e.Item.Cells(2).Text.Trim <> "" AndAlso e.Item.Cells(2).Text <> "&nbsp;" Then
                        e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).Date.ToShortDateString
                    End If

                    If e.Item.Cells(6).Text.Trim <> "" AndAlso e.Item.Cells(6).Text <> "&nbsp;" Then
                        e.Item.Cells(6).Text = Format(CDec(e.Item.Cells(6).Text), Session("fmtCurrency").ToString)
                    End If

                    If e.Item.Cells(7).Text.Trim <> "" AndAlso e.Item.Cells(7).Text <> "&nbsp;" Then
                        e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), Session("fmtCurrency").ToString)
                    End If

                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (10-Jul-2023) -- Start
    'NMB Enhancement :(A1X-1113) NMB - Staff Transfer Notifications to employee and approvers
    Protected Sub dgLoanApproval_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgLoanApproval.PageIndexChanged
        Try
            dgLoanApproval.CurrentPageIndex = e.NewPageIndex
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (10-Jul-2023) -- End

#End Region

#Region "CheckBox Events"

    Protected Sub chkMyApprovals_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMyApprovals.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, "gbFilterCriteria", Me.lblDetialHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblFromDate.ID, Me.lblFromDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblToDate.ID, Me.lblToDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblApprover.ID, Me.lblApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanAmount.ID, Me.lblLoanAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkMyApprovals.ID, Me.chkMyApprovals.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApproval.Columns(1).FooterText, Me.dgLoanApproval.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApproval.Columns(2).FooterText, Me.dgLoanApproval.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApproval.Columns(3).FooterText, Me.dgLoanApproval.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApproval.Columns(4).FooterText, Me.dgLoanApproval.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApproval.Columns(5).FooterText, Me.dgLoanApproval.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApproval.Columns(6).FooterText, Me.dgLoanApproval.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApproval.Columns(7).FooterText, Me.dgLoanApproval.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanApproval.Columns(8).FooterText, Me.dgLoanApproval.Columns(8).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lnkAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToDate.ID, Me.lblToDate.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblLoanAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanAmount.ID, Me.lblLoanAmount.Text)
            Me.chkMyApprovals.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkMyApprovals.ID, Me.chkMyApprovals.Text)
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgLoanApproval.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(1).FooterText, Me.dgLoanApproval.Columns(1).HeaderText)
            Me.dgLoanApproval.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(2).FooterText, Me.dgLoanApproval.Columns(2).HeaderText)
            Me.dgLoanApproval.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(3).FooterText, Me.dgLoanApproval.Columns(3).HeaderText)
            Me.dgLoanApproval.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(4).FooterText, Me.dgLoanApproval.Columns(4).HeaderText)
            Me.dgLoanApproval.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(5).FooterText, Me.dgLoanApproval.Columns(5).HeaderText)
            Me.dgLoanApproval.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(6).FooterText, Me.dgLoanApproval.Columns(6).HeaderText)
            Me.dgLoanApproval.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(7).FooterText, Me.dgLoanApproval.Columns(7).HeaderText)
            Me.dgLoanApproval.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanApproval.Columns(8).FooterText, Me.dgLoanApproval.Columns(8).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub


#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Approved By :-")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Rejected By :-")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "You can't Edit this Loan detail. Reason: You are logged in into another user login.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "You can't Edit this Loan detail. Reason: This Loan is already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "You can't Edit this Loan detail. Reason: This Loan is already rejected.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings

End Class
