﻿<%@ Page Language="VB" Title="Loan Approval" AutoEventWireup="false" MasterPageFile="~/Home1.master"
    CodeFile="wPg_RoleBasedLoanApproval.aspx.vb" Inherits="Loan_Savings_New_Loan_Role_Based_Loan_Approval_Process_wPg_RoleBasedLoanApproval" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc3" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

   <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, event) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, evemt) {
            $("#endreq").val("1");
        }

        function IsValidAttach() {
            debugger;
            var cbodoctype = $('#<%= cboCRBDocumentType.ClientID %>');

            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select CRB Document Type.');
                cbodoctype.focus();
                return false;
            }
            return true;
        }

        function IsValidApproverAttach() {
            debugger;
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');

            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            return true;
        }
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <script type="text/javascript">
        function pageLoad(sender, args) { $("select").searchable(); } </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }


        function countdown(minutes, seconds) {
            var element, endTime, mins, msLeft, time;

            function twoDigits(n) {
                return (n <= 9 ? "0" + n : n);
            }

            function updateTimer() {
                msLeft = endTime - (+new Date);

                if (msLeft < 1000) {
                    element.innerHTML = '00:00';
                    document.getElementById('<%=txtVerifyOTP.ClientID %>').readOnly = true;
                    document.getElementById('<%=btnVerify.ClientID %>').disabled = "disabled";
                    document.getElementById('<%=btnSendCodeAgain.ClientID %>').disabled = "";
                    document.getElementById('<%=btnTOTPClose.ClientID %>').disabled = "";
                } else {
                    document.getElementById('<%=btnSendCodeAgain.ClientID %>').disabled = "disabled";
                    document.getElementById('<%=btnTOTPClose.ClientID %>').disabled = "disabled";
                    time = new Date(msLeft);
                    mins = time.getUTCMinutes();
                    element.innerHTML = (twoDigits(mins)) + ':' + twoDigits(time.getUTCSeconds());
                    setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
                }
                if (msLeft <= 0) {
                    return;
                }
                document.getElementById('<%=hdf_TOTP.ClientID %>').value = twoDigits(time.getUTCSeconds());
            }
            element = document.getElementById('<%=LblSeconds.ClientID %>');
            endTime = (+new Date) + 1000 * (60 * minutes + seconds) + 500;
            updateTimer();
        }

        function VerifyOnClientClick() {
            var val = document.getElementById('<%=hdf_TOTP.ClientID %>').value;
            if (val !== undefined) {
                countdown(0, val, false);
            }
        }

        function stopCountDown() {
            clearTimeout(timerid);
        }
        
    </script>

      <script type="text/javascript">
          function Print() {
              var printWin = window.open('', '', 'left=0,top=0,width=1000,height=600,status=0');
              printWin.document.write(document.getElementById("<%=divhtml.ClientID %>").innerHTML);
              printWin.document.close();
              printWin.focus();
              printWin.print();
              printWin.close();
          }
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblPageHeader" runat="server" Text="Loan Approval" CssClass="form-label"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="card inner-card">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <asp:Label ID="LblLoanApplicationNo" runat="server" Text="Application No" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtLoanApplicationNo" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <asp:Label ID="LblApplicationDate" runat="server" Text="Application Date" CssClass="form-label"></asp:Label>
                                                    <uc1:DateCtrl ID="dtpApplicationDate" runat="server" AutoPostBack="false" Enabled="false" />
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 m-t-30">
                                                    <asp:CheckBox ID="chkMakeExceptionalApplication" runat="server" Text="Make Exceptional Application"
                                                        AutoPostBack="true" CssClass="filled-in" Enabled="false" />
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblEmpName" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboEmpName" runat="server" AutoPostBack="true" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboLoanScheme" runat="server" AutoPostBack="True" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblLoanSchemeCategory" runat="server" Text="Loan Category" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboLoanSchemeCategory" runat="server" data-live-search="true"
                                                            Enabled="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblRepaymentDays" runat="server" Text="Repayment Days" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtRepaymentDays" runat="server" ReadOnly="true" Style="text-align: right"
                                                                Width="99%" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <asp:Label ID="lblMinLoanAmount" runat="server" Text="Min Principal Amount" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtMinLoanAmount" AutoPostBack="true" Text="0.0" Style="text-align: right"
                                                                runat="server" onKeypress="return onlyNumbers(this, event);" ReadOnly="true"
                                                                CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <asp:Label ID="lblMaxLoanAmount" runat="server" Text="Max Principal Amount" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtMaxLoanAmount" AutoPostBack="true" Text="0.0" Style="text-align: right"
                                                                runat="server" onKeypress="return onlyNumbers(this, event);" ReadOnly="true"
                                                                CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <asp:Label ID="lblMaxInstallment" runat="server" Text="Max no of Installment" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtMaxInstallment" AutoPostBack="true" Text="1" Style="text-align: right"
                                                                runat="server" onKeypress="return onlyNumbers(this, event);" ReadOnly="true"
                                                                CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <asp:Label ID="lblMaxInstallmentAmt" runat="server" Text="Max Installment Amt" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtMaxInstallmentAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblLoanInterest" runat="server" Text="Loan Rate(%)" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtLoanRate" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblInsuranceRate" runat="server" Text="Insurance Rate(%)" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtInsuranceRate" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="LblDeductionPeriod" runat="server" Text="Month to start Deduction"
                                                        Style="width: 80%" CssClass="form-label"></asp:Label>
                                                    <asp:RequiredFieldValidator ID="requireFieldValidator" runat="server" ControlToValidate="cboDeductionPeriod"
                                                        InitialValue="0" ErrorMessage="* required" Display="Dynamic" ForeColor="Red"
                                                        Font-Bold="true" ValidationGroup="GrpApprovedonly"></asp:RequiredFieldValidator>
                                                     <div class="form-group">
                                                        <asp:DropDownList ID="cboDeductionPeriod" runat="server" AutoPostBack="true" data-live-search="true" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card inner-card">
                                                <div class="body">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblLoanCalcType" runat="server" Text="Loan Calc. Type" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboLoanCalcType" runat="server" Enabled="False" AutoPostBack="true"
                                                                    data-live-search="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblInterestCalcType" runat="server" Text="Int. Calc. Type" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboInterestCalcType" runat="server" AutoPostBack="true" Enabled="False"
                                                                    data-live-search="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <asp:Panel ID="Panel1" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPlotNo" runat="server" Text="Plot No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPlotNo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblBlockNo" runat="server" Text="Block No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtBlockNo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblStreet" runat="server" Text="Street" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtStreet" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTownCity" runat="server" Text="Town/City" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtTownCity" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblMarketValue" runat="server" Text="Market Value" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtMarketValue" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                    ReadOnly="true" Style="text-align: right" Text="0.0" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCTNo" runat="server" Text="CT No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtCTNo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLONo" runat="server" Text="LO No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtLONo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblFSV" runat="server" Text="FSV" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtFSV" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                    ReadOnly="true" Style="text-align: right" Text="0.0" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblBOQ" runat="server" Text="Bill Of Quantity (BOQ)" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtBOQ" runat="server" Text="" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblTitleIssueDate" runat="server" Text="Title Issue Date" CssClass="form-label"></asp:Label>
                                                                <uc1:DateCtrl ID="dtpTitleIssueDate" runat="server" Enabled="false" />
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblTitleValidity" runat="server" Text="Title Validity" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="nudTitleValidity" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                            Style="text-align: right" Text="0.0" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblTitleExpiryDate" runat="server" Text="Title Expiry Date" CssClass="form-label"></asp:Label>
                                                                <uc1:DateCtrl ID="dtpTitleExpiryDate" runat="server" Enabled="false" />
                                                            </div>
                                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="Panel2" runat="server">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblModel" runat="server" Text="Model" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtModel" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblYOM" runat="server" Text="YOM" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtYOM" runat="server" MaxLength="4" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblChassisNo" runat="server" Text="Chassis No" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtChassisNo" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblSupplier" runat="server" Text="Supplier" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtSupplier" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblColour" runat="server" Text="Colour" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtColour" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlScanAttachment" runat="server">
                                <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                        <asp:Label ID="lblDocumentType" runat="server" Text="Documents(.PDF and Images only)"
                                            CssClass="form-label"></asp:Label>
                                                        </h2>
                                    </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive" style="max-height: 350px;">
                                                        <asp:DataGrid ID="dgvAttachement" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                            Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                            HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                        <asp:BoundColumn HeaderText="Document Type" DataField="attachment_type" FooterText="colhDocType" />
                                                                <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                                <asp:TemplateColumn HeaderText="Download" FooterText="objcolhDownload" ItemStyle-HorizontalAlign="Center"
                                                                    ItemStyle-Font-Size="22px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkDownload" runat="server" CommandName="imgdownload" ToolTip="Download">
                                                                                                            <i class="fas fa-download"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--<asp:TemplateColumn HeaderText="Preview" FooterText="objcolhPreview" ItemStyle-HorizontalAlign="Center"
                                                                    ItemStyle-Font-Size="22px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkPreview" runat="server" CommandName="imgdownload" ToolTip="Preview">
                                                                                                            <i class="fas fa-download"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>--%>
                                                                <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                    Visible="false" />
                                                                <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                    Visible="false" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlOtherScanAttachment" runat="server">
                                <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                        <asp:Label ID="lblOtherDocumentType" runat="server" Text="Other Documents(.PDF and Images only)"
                                            CssClass="form-label"></asp:Label>
                                                        </h2>
                                    </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive" style="max-height: 350px;">
                                                        <asp:DataGrid ID="dgvOtherAttachement" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                            Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                            HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhotherName" />
                                                                        <asp:BoundColumn HeaderText="Document Type" DataField="attachment_type" FooterText="colhOtherDocType" />
                                                                <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhotherSize" />
                                                                <asp:TemplateColumn HeaderText="Download" FooterText="objcolhotherDownload" ItemStyle-HorizontalAlign="Center"
                                                                    ItemStyle-Font-Size="22px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkOtherDownload" runat="server" CommandName="imgotherdownload"
                                                                            ToolTip="Download">
                                                                               <i class="fas fa-download"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--<asp:TemplateColumn HeaderText="Preview" FooterText="objcolhPreview" ItemStyle-HorizontalAlign="Center"
                                                                    ItemStyle-Font-Size="22px">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkPreview" runat="server" CommandName="imgdownload" ToolTip="Preview">
                                                                                                            <i class="fas fa-download"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>--%>
                                                                <asp:BoundColumn HeaderText="objcolhotherGUID" DataField="GUID" FooterText="objcolhotherGUID"
                                                                    Visible="false" />
                                                                <asp:BoundColumn HeaderText="objcolhOtherScanUnkId" DataField="scanattachtranunkid"
                                                                    FooterText="objcolhOtherScanUnkId" Visible="false" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                                    <asp:Panel ID="pnlApproverScanAttachment" runat="server">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblApproverDocumentType" runat="server" Text="Approver Documents(.PDF and Images only)"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboDocumentType" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-20  ">
                                                                    <asp:Panel ID="pnl_ApproverImageAdd" runat="server">
                                                                        <div id="approverfileuploader">
                                                                            <input type="button" id="btnAddApproverFile" runat="server" onclick="return IsValidApproverAttach()"
                                                                                value="Browse" class="btn btn-primary" />
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 ">
                                                                    <asp:Button ID="btnAddApproverAttachment" runat="server" Style="display: none" Text="Browse" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive" style="max-height: 350px;">
                                                                <asp:DataGrid ID="dgvApproverAttachement" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                    Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                                    HeaderStyle-Font-Bold="false">
                                                                    <Columns>
                                                                        <asp:TemplateColumn FooterText="objcohDelete">
                                                                            <ItemTemplate>
                                                                                <span class="gridiconbc">
                                                                                    <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                                </span>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn HeaderText="Document Type" DataField="attachment_type" FooterText="colhDocType" />
                                                                        <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                        <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                                        <asp:TemplateColumn HeaderText="Download" FooterText="objcolhDownload" ItemStyle-HorizontalAlign="Center"
                                                                            ItemStyle-Font-Size="22px">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkDownload" runat="server" CommandName="imgdownload" ToolTip="Download">
                                                                                                            <i class="fas fa-download"></i>
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                            Visible="false" />
                                                                        <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                            Visible="false" />
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                        </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <div class="row clearfix" style="display: none;">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAmt" runat="server" Text="Applied Principal Amt" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtAppliedUptoPrincipalAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                        Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card inner-card">
                                        <div class="header">
                                            <h2>
                                                <asp:Label ID="elEmployeeDetails" runat="server" Text="Employee Details" CssClass="form-label"></asp:Label>
                                            </h2>
                                        </div>
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblPayPeriodsEOC" runat="server" Text="Pay Periods" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="nudPayPeriodEOC" AutoPostBack="true" Text="1" Style="text-align: right"
                                                                runat="server" onKeypress="return onlyNumbers(this, event);" CssClass="form-control"
                                                                Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblEmplDate" runat="server" Text="Til EOC Date" CssClass="form-label"></asp:Label>
                                                    <uc1:DateCtrl ID="dtpEndEmplDate" runat="server" AutoPostBack="false" Enabled="false" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card inner-card">
                                        <div class="header">
                                            <h2>
                                                <asp:Label ID="elLoanAmountCalculation" runat="server" Text="Loan Amount Calculation"
                                                    CssClass="form-label"></asp:Label>
                                            </h2>
                                        </div>
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblEMIInstallments" runat="server" Text="Loan Tenure (Months)" CssClass="form-label"
                                                        ReadOnly="True"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtEMIInstallments" AutoPostBack="true" Text="1" Style="text-align: right"
                                                                runat="server" onKeypress="return onlyNumbers(this, event);" CssClass="form-control"
                                                                ReadOnly="True"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblPrincipalAmt" runat="server" Text="Principal Amt." CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtLoanAmt" AutoPostBack="true" Text="0.0" Style="text-align: right"
                                                                runat="server" onKeypress="return onlyNumbers(this, event);" CssClass="form-control"
                                                                ReadOnly="True"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-25">
                                                    <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true" data-live-search="true"
                                                        Enabled="false">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="display: none;">
                                                    <asp:Label ID="lblIntAmt" runat="server" Text="Interest Amt." CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtIntAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblEMIAmount" runat="server" Text="Installment Amt" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtInstallmentAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                ReadOnly="true" Style="text-align: right" Text="0.0" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblInsuranceAmt" runat="server" Text="Insurance Amt." CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtInsuranceAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Panel ID="pnlTopupDetails" runat="server">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblOutStandingPrinciple" runat="server" Text="Outstanding Principle"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="TxtOutStandingPrinciple" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                            Style="text-align: right" Text="0.0" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <asp:Label ID="LblOutstandingInterest" runat="server" Text="Outstanding Interest"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="TxtOutstandingInterest" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                            Style="text-align: right" Text="0.0" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                                <asp:Label ID="LblPaidInstallment" runat="server" Text="No of Paid Installments"
                                                                    CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="TxtPaidInstallments" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                            Style="text-align: right" Text="0" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblTakeHome" runat="server" Text="Take Home" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtTakeHome" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30" style="text-align: right;">
                                                    <asp:CheckBox ID="chkSalaryAdvanceLiquidation" runat="server" Text="Salary Advance Liquidation"
                                                        AutoPostBack="true" CssClass="filled-in" Enabled="false" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Panel ID="pnlDisbursementfunds" runat="server">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="LblDisbursementfunds" runat="server" Text="Disbursement Of Funds"
                                                        CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="LblFirstTranche" runat="server" Text="First Tranche" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="TxtFirstTranche" runat="server" Text="0.0" onkeypress="return onlyNumbers(this, event);"
                                                                    Style="text-align: right" Width="99%" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                        <asp:Label ID="LblFirstTrancheDate" runat="server" Text="First Tranche Date" CssClass="form-label"></asp:Label>
                                                        <uc1:DateCtrl ID="dtpFirstTranche" runat="server" AutoPostBack="false" />
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                        <asp:Label ID="LblFirstTranche_Remark" runat="server" Text="First Tranche Remark"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtFirstTranche_Remark" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="LblSecondTranche" runat="server" Text="Second Tranche" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="TxtSecondTranche" runat="server" Text="0.0" onkeypress="return onlyNumbers(this, event);"
                                                                    Style="text-align: right" Width="99%" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                        <asp:Label ID="LblSecondTrancheDate" runat="server" Text="Second Tranche Date" CssClass="form-label"></asp:Label>
                                                        <uc1:DateCtrl ID="dtpSecondTranche" runat="server" AutoPostBack="false" />
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                        <asp:Label ID="LblSecondTranche_Remark" runat="server" Text="Second Tranche Remark"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtSecondTranche_Remark" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="LblThirdTranche" runat="server" Text="Third Tranche" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="TxtThirdTranche" runat="server" Text="0.0" onkeypress="return onlyNumbers(this, event);"
                                                                    Style="text-align: right" Width="99%" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                        <asp:Label ID="LblThirdTrancheDate" runat="server" Text="Third Tranche Date" CssClass="form-label"></asp:Label>
                                                        <uc1:DateCtrl ID="dtpThirdTranche" runat="server" AutoPostBack="false" />
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                        <asp:Label ID="LblThirdTranche_Remark" runat="server" Text="Third Tranche Remark"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtThirdTranche_Remark" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="LblFourthTranche" runat="server" Text="Fourth Tranche" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="TxtFourthTranche" runat="server" Text="0.0" onkeypress="return onlyNumbers(this, event);"
                                                                    Style="text-align: right" Width="99%" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                        <asp:Label ID="LblFourthTrancheDate" runat="server" Text="Fourth Tranche Date" CssClass="form-label"></asp:Label>
                                                        <uc1:DateCtrl ID="dtpFourthTranche" runat="server" AutoPostBack="false" />
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                        <asp:Label ID="LblFourthTranche_Remark" runat="server" Text="Fourth Tranche Remark"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtFourthTranche_Remark" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                  <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                        <asp:Label ID="LblFifthTranche" runat="server" Text="Fifth Tranche" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="TxtFifthTranche" runat="server" Text="0.0" onkeypress="return onlyNumbers(this, event);"
                                                                    Style="text-align: right" Width="99%" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                        <asp:Label ID="LblFifthTrancheDate" runat="server" Text="Fourth Tranche Date" CssClass="form-label"></asp:Label>
                                                        <uc1:DateCtrl ID="dtpFifthTranche" runat="server" AutoPostBack="false" />
                                                    </div>
                                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                        <asp:Label ID="LblFifthTranche_Remark" runat="server" Text="Fifth Tranche Remark"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtFifthTranche_Remark" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblPurposeOfCredit" runat="server" Text="Purpose of Credit" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtPurposeOfCredit" TextMode="MultiLine" Rows="4" runat="server"
                                                        ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="LblApproverRemark" runat="server" Text="Approver Remark" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="TxtApproverRemark" TextMode="MultiLine" Rows="3" runat="server"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Panel ID="pnlPrecedentSubsequentRemark" runat="server">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblPrecedentRemarks" runat="server" Text="Precedent Remark" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtPrecedentRemarks" TextMode="MultiLine" Rows="3" runat="server"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblSubsequentRemarks" runat="server" Text="Subsequent Remark" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtSubsequentRemarks" TextMode="MultiLine" Rows="3" runat="server"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <div class="btn-group m-l-0" style="float: left">
                                <asp:Label ID="objvalPeriodDuration" runat="server" Text="#periodDuration" Style="display: block;"
                                    Visible="false"></asp:Label>
                                <asp:Label ID="objlblExRate" runat="server" Style="float: left;" Text="" CssClass="form-label"></asp:Label>
                            </div>
                            <asp:Button ID="btnPreviousApproverComment" runat="server" Text="Previous Approver Comment"
                                CssClass="btn btn-default" />
                            <asp:Button ID="btnOfferLetter" runat="server" Text="Preview Offer Letter" CssClass="btn btn-primary" />
                            <asp:Button ID="btnViewExposure" runat="server" Text="View Exposure(s)" CssClass="btn btn-primary" />
                            <asp:Button ID="btnGetCRBData" runat="server" Text="Get CRB Data" CssClass="btn btn-primary" />
                            <asp:Button ID="btnReturnApplicant" runat="server" Text="Return To Applicant" CssClass="btn btn-primary" />
                            <asp:Button ID="btnReturnPreviousApprover" runat="server" Text="Return To Previous Approver"
                                CssClass="btn btn-primary" />
                            <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary"
                                ValidationGroup="GrpApprovedonly" />
                            <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupCRBData" BackgroundCssClass="modal-backdrop" TargetControlID="hdCRBData"
                    runat="server" PopupControlID="pnlCRBData" DropShadow="false" CancelControlID="LblCRBData">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlCRBData" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblCRBData" Text="Get CRB Data" runat="server" />
                        </h2>
                    </div>
                    <div class="body">
                        <asp:Panel ID="pnlEmpIdentity" runat="server">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 210px;">
                                        <asp:DataGrid ID="dgEmpIdentity" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                            CssClass="table table-hover table-bordered">
                                            <Columns>
                                                <asp:BoundColumn DataField="identities" HeaderText="Identity Type" FooterText="dgcolhIdentityType" />
                                                <asp:BoundColumn DataField="identity_no" HeaderText="Identity No" FooterText="dgcolhIdentityNo" />
                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" FooterText="btnGetData">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="lnkGetData" runat="server" ToolTip="Get Data" CommandName="GetData"
                                                                Font-Underline="false" Text="Get Data"></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="crb_status" HeaderText="Status" FooterText="dgcolhCRBStatus" />
                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" FooterText="btnGenerateReport">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="lnkGenerateReport" runat="server" ToolTip="Generate Report" CommandName="GetReport"
                                                                Font-Underline="false" Text="Generate Report"></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="idtypeunkid" HeaderText="Identity Type" FooterText="objdgcolhIdentityId"
                                                    Visible="false" />
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="row clearfix">
                            <asp:Panel ID="pnlCRBAttachment" runat="server">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="card inner-card">
                                        <div class="header">
                                            <h2>
                                                <asp:Label ID="lblCRBAttachmentInfo" runat="server" Text="Attach CRB Report Manually"
                                                    CssClass="form-label"></asp:Label>
                                            </h2>
                                        </div>
                                        <div class="body">
                                            <asp:Panel ID="pnl_CRBDoc" runat="server">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblCRBDocument" runat="server" Text="CRB Document Type(.PDF and Images only)"
                                                            CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboCRBDocumentType" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-t-30">
                                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-l-20  ">
                                                            <div id="fileuploader">
                                                                <input type="button" id="btnAddFile" runat="server" onclick="return IsValidAttach()"
                                                                    value="Browse" class="btn btn-primary" />
                                                            </div>
                                                        </div>
                                                        <asp:Button ID="btnAddCRBAttachment" runat="server" Style="display: none" Text="Browse"
                                                            CssClass="btn btn-primary" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive" style="max-height: 250px;">
                                                        <asp:DataGrid ID="dgCRBDocument" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                            Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false"
                                                            HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:TemplateColumn FooterText="objcohDelete">
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                    Visible="false" />
                                                                <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                    Visible="false" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSaveCRBIdentityData" runat="server" Text="Save CRB Data" CssClass="btn btn-primary" />
                        <asp:Button ID="btnCloseCRBIdentityData" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdCRBData" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupCRBReport" runat="server" CancelControlID="btnCRBReportClose"
                    PopupControlID="pnlCRBReport" TargetControlID="HiddenField2" PopupDragHandleControlID="pnlCRBReport">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlCRBReport" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="height: 600px" DefaultButton="btnCRBReportClose" ScrollBars="Auto">
                    <div class="header">
                        <h2>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div id="divhtml" runat="server" style="width: 89%; left: 76px; top: 13px">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnCRBGenReport" runat="server" Text="Print" CssClass="btn btn-primary"
                            OnClientClick="javascript:Print()" />
                        <asp:Button ID="btnCRBReportClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupViewExposures" BackgroundCssClass="modal-backdrop"
                    TargetControlID="hdViewExposures" runat="server" PopupControlID="pnlViewExposures"
                    DropShadow="false" CancelControlID="LblViewExposures">
                </cc1:ModalPopupExtender>
                 <asp:Panel ID="pnlViewExposures" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="height: 530px" DefaultButton="btnCRBReportClose" ScrollBars="Auto">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblViewExposures" Text="View Exposure(s)" runat="server" />
                        </h2>
                    </div>
                    <div class="body">
                       <asp:Panel ID="pnlLoanExposures" runat="server">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 210px;">
                                        <asp:DataGrid ID="dgViewExposures" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                            CssClass="table table-hover table-bordered">
                                            <Columns>
                                                <asp:BoundColumn DataField="PRODUCT_CODE" HeaderText="Product Code" FooterText="dgcolhProductCode" />
                                                <asp:BoundColumn DataField="MATURITY_DATE" HeaderText="Maturity Date" FooterText="dgcolhMaturityDate" />
                                                <asp:BoundColumn DataField="PAIDNOOFINSTALMENT" HeaderText="No of Paid Installment"
                                                    FooterText="dgColhNoPaidInstallment" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundColumn DataField="INSTALMENT_AMOUNT" HeaderText="Installment Amount" FooterText="dgColhInstallmentAmt"
                                                    HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundColumn DataField="CURRENT_OUTSTANDING_INTEREST" HeaderText="Outstanding Interest"
                                                    FooterText="dgColhOustandingInterest" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundColumn DataField="PRINCIPALOUTSTANDING" HeaderText="Outstanding Principal"
                                                    FooterText="dgcolhOutstandingPrincipal" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlCreditCardExposures" runat="server">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="LblCreditCardExposures" Text="Credit Card Exposure(s)" runat="server" />
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 110px;">
                                            <asp:DataGrid ID="dgCreditCardExposures" runat="server" AutoGenerateColumns="false"
                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:BoundColumn DataField="walletcreditlimit" HeaderText="Wallet Credit Limit" FooterText="dgcolhWalletCreditLimit"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="oustandingbalwithloan" HeaderText="Outstanding Balance with Loan"
                                                        FooterText="dgcolhOutstandingBalWithLoan" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="outstandingbalwithoutloan" HeaderText="Outstanding Balance without Loan"
                                                        FooterText="dgcolhOutstandingBalWithoutLoan" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnppViewExposures" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdViewExposures" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_TOTP" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="hdf_TOTP" PopupControlID="pnl_TOTP" TargetControlID="hdf_TOTP">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_TOTP" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblVerifyOTP" runat="server" Text="Verify OTP"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="LblOTP" runat="server" Text="Enter OTP" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtVerifyOTP" runat="server" CssClass="form-control" MaxLength="6"
                                            onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="LblSeconds" runat="server" Text="00:00" CssClass="form-label" Font-Bold="true"
                                    ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnSendCodeAgain" runat="server" Text="Resend OTP" CssClass="btn btn-primary" />
                        <asp:Button ID="btnVerify" runat="server" Text="Verify" CssClass="btn btn-primary" />
                        <asp:Button ID="btnTOTPClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdf_TOTP" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupPreviousApprover" BackgroundCssClass="modal-backdrop"
                    TargetControlID="lblPreviousApprovalInfo" runat="server" PopupControlID="pnlPreviousApprover"
                    DropShadow="false" CancelControlID="btnHiddenCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlPreviousApprover" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblPreviousApprovalInfo" Text="Previous Approval Info" runat="server" />
                        </h2>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body">
                                <%--   <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApproverName" runat="server" Text="Approver Name" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApproverName" runat="server" CssClass="form-control" ReadOnly="true" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRole" runat="server" Text="Role" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApproverRole" runat="server" CssClass="form-control" ReadOnly="true" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApproverLevel" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprRejectRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApprRejectRemark" runat="server" TextMode="MultiLine" Rows="3"
                                                    ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="table-responsive" style="height: 400px;">
                                    <asp:GridView ID="gvPreviousApproverRemarks" runat="server" AutoGenerateColumns="false"
                                        AllowPaging="false" CssClass="table table-hover table-bordered table-vertical-top">
                                        <Columns>
                                            <asp:BoundField DataField="UserName" HeaderText="Approver Name" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="150px" FooterText="dgcolhApproverName" />
                                            <asp:BoundField DataField="Role" HeaderText="Role" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="150px" FooterText="dgcolhRole" />
                                            <asp:BoundField DataField="Level" HeaderText="Level" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="150px" FooterText="dgcolhLevel" />
                                            <asp:BoundField DataField="Remark" HeaderText="Remark" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="300px" ItemStyle-Wrap="true" FooterText="dgcolhRemark" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnAClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                <asp:HiddenField ID="btnHiddenCancel" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <uc3:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
            </ContentTemplate>
             <Triggers>
                <asp:PostBackTrigger ControlID="dgvAttachement" />
                <asp:PostBackTrigger ControlID="dgvOtherAttachement" />
                <asp:PostBackTrigger ControlID="dgvApproverAttachement" />
                <asp:PostBackTrigger ControlID="dgEmpIdentity" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">

              $(document).ready(function() {
                  ImageLoad();
                  $(".ajax-upload-dragdrop").css("width", "auto");
              });

              function ImageLoad() {
                  debugger;
                  if ($(".ajax-upload-dragdrop").length <= 0) {
                      $("#fileuploader").uploadFile({
                    url: "wPg_RoleBasedLoanApproval.aspx?uploadimage=mSEfU19VPc4=",
                          method: "POST",
                          dragDropStr: "",
                          showStatusAfterSuccess: false,
                          showAbort: false,
                          showDone: false,
                          fileName: "myfile",
                          onSuccess: function(path, data, xhr) {
                        $("#<%= btnAddCRBAttachment.ClientID %>").click();
                          },
                          onError: function(files, status, errMsg) {
                              alert(errMsg);
                          }
                      });

                $("#approverfileuploader").uploadFile({
                    url: "wPg_RoleBasedLoanApproval.aspx?uploadapproverimage=mSEfU19VPc4=",
                    method: "POST",
                    dragDropStr: "",
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    showDone: false,
                    fileName: "myapproverfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnAddApproverAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
                  }
              }

        $("body").on("click", 'input[name*=myfile]', function() {
                  return IsValidAttach();
              });
    
        $("body").on("click", 'input[name*=myapproverfile]', function() {
            return IsValidApproverAttach();
        });
    
    </script>

</asp:Content>
