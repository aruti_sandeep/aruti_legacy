﻿Option Strict On
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data

#End Region


Partial Class Loan_Savings_New_Loan_Role_Based_Loan_Approval_Process_wPg_RoleBasedGlobalApproveLoan
    Inherits Basepage

#Region " Private Variables "
    Dim msg As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmRoleBasedGlobalApproveLoan"
    Private mstrAdvanceFilter As String = String.Empty

    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .
    Dim mstrSecretKey As String = ""
    Private blnShowTOTPPopup As Boolean = False
    Dim Counter As Integer = 0
    Dim mstrEmpMobileNo As String = ""
    Dim mstrEmployeeName As String = ""
    Dim mstrEmployeeEmail As String = ""
    Dim xStatusId As Integer = enLoanApplicationStatus.PENDING
    'Pinkal (02-Jun-2023) -- End
#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            'Pinkal (20-Jan-2023) -- Start
            'Solved NMB Issue for Randamely Access of URL in Loan module.
            If Session("UserId") Is Nothing OrElse CInt(Session("UserId")) <= 0 Then
                msg.DisplayMessage("you are not authorized to access this page.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Pinkal (20-Jan-2023) -- End


            If Not IsPostBack Then
                SetControlCaptions()
                'Pinkal (14-Dec-2022) -- Start
                'NMB Loan Module Enhancement.
                SetMessages()
                'Pinkal (14-Dec-2022) -- End
                SetLanguage()
                txtApprover.Text = Session("UserName").ToString()
                cboDeductionPeriod.Enabled = CBool(Session("AllowToChangeDeductionPeriodOnLoanApproval"))
                Call FillCombo()
                If dgvData.Items.Count <= 0 Then
                    dgvData.DataSource = New List(Of String)
                    dgvData.DataBind()
                End If

                'Pinkal (20-Jan-2023) -- Start
                'Solved NMB Issue for Randamely Access of URL in Loan module.
                btnSave.Enabled = CBool(Session("AllowToChangeLoanStatus"))
                'Pinkal (20-Jan-2023) -- End
            Else
                'Pinkal (02-Jun-2023) -- Start
                'NMB Enhancement : TOTP Related Changes .
                mstrSecretKey = CStr(Me.ViewState("mstrSecretKey"))
                Counter = CInt(Me.ViewState("Counter"))
                blnShowTOTPPopup = CBool(Me.ViewState("blnShowTOTPPopup"))
                mstrEmployeeName = CStr(Me.ViewState("EmployeeName"))
                mstrEmployeeEmail = CStr(Me.ViewState("EmployeeEmail"))
                mstrEmpMobileNo = CStr(Me.ViewState("EmployeeMobileNo"))
                xStatusId = CInt(Me.ViewState("xStatusId"))

                If blnShowTOTPPopup Then
                    popup_TOTP.Show()
                End If
                'Pinkal (02-Jun-2023) -- End
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Me.ViewState("Counter") = Counter
            Me.ViewState("mstrSecretKey") = mstrSecretKey
            Me.ViewState("blnShowTOTPPopup") = blnShowTOTPPopup
            Me.ViewState("EmployeeName") = mstrEmployeeName
            Me.ViewState("EmployeeEmail") = mstrEmployeeEmail
            Me.ViewState("EmployeeMobileNo") = mstrEmpMobileNo
            Me.ViewState("xStatusId") = xStatusId
            'Pinkal (02-Jun-2023) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsCombo As New DataSet
            Dim objAMaster As New clsLoanApprover_master
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objMasterData As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran

            dsCombo = objAMaster.GetEmployeeFromUser(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), "List")

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            objAMaster = Nothing


            dsCombo = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With
            objLoanScheme = Nothing

            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                CInt(Session("Fin_year")), Session("Database_Name").ToString, CDate(Session("fin_startdate")), _
                                                "Period", True, 1)
            With cboDeductionPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Period")
                .DataBind()
            End With
            objPeriod = Nothing

            dsCombo = objMasterData.GetCondition(False, True, True, False, False)
            Dim dtCondition As DataTable = New DataView(dsCombo.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            With cboAmountCondition
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dtCondition
                .DataBind()
            End With
            objMasterData = Nothing

            Dim objPendingLoan As New clsProcess_pending_loan
            dsCombo = objPendingLoan.GetLoan_Status("Status", True, False, False, CInt(Session("LoanIntegration")), CBool(Session("RoleBasedLoanApproval")))
            Dim xRow = From p In dsCombo.Tables("Status") Where CInt(p.Item("Id")) = enLoanApplicationStatus.APPROVED OrElse CInt(p.Item("Id")) = enLoanApplicationStatus.REJECTED OrElse CInt(p.Item("Id")) = enLoanApplicationStatus.RETURNTOAPPLICANT Select p
            Dim dtTable As DataTable = xRow.CopyToDataTable
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "2"
            End With
            objPendingLoan = Nothing

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillGrid()
        Dim objRoleBasedApproval As New clsroleloanapproval_process_Tran
        Try
            Dim mstrSearch As String = ""

            'Pinkal (20-Jan-2023) -- Start
            'Solved NMB Issue for Randamely Access of URL in Loan module.
            If CBool(Session("AllowToViewLoanApprovalList")) = False Then Exit Sub
            'Pinkal (20-Jan-2023) -- End

            If CInt(cboEmployee.SelectedValue) > 0 Then
                mstrSearch &= "AND lna.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                mstrSearch &= "AND ln.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If txtLoanAmount.Text.Trim <> "" AndAlso CDec(txtLoanAmount.Text) > 0 Then
                mstrSearch &= "AND ln.loan_amount " & cboAmountCondition.SelectedItem.Text & " " & CDec(txtLoanAmount.Text) & " "
            End If

            mstrSearch &= "AND ln.loan_statusunkid = " & enLoanApplicationStatus.PENDING & " AND lna.statusunkid = " & enLoanApplicationStatus.PENDING & " AND lna.visibleunkid <> -1 "

            If mstrAdvanceFilter.Trim.Length > 0 Then
                mstrSearch &= " AND " & mstrAdvanceFilter.Trim
            End If

            If mstrSearch.Trim.Length > 0 Then
                mstrSearch = mstrSearch.Trim.Substring(3)
            End If

            Dim dsList As DataSet = objRoleBasedApproval.GetRoleBasedApprovalList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
                                                "List", mstrSearch, mstrAdvanceFilter, False, Nothing)


            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "mapuserunkid = " & CInt(Session("UserId")), "", DataViewRowState.CurrentRows).ToTable

            dgvData.AutoGenerateColumns = False
            dgvData.DataSource = dtTable.Copy
            dgvData.DataBind()

            dsList.Clear()
            dsList = Nothing

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objRoleBasedApproval = Nothing
        End Try
    End Sub


    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .

    Private Sub Save(ByVal gRow As IEnumerable(Of DataGridItem))
        Dim objmailList As New List(Of clsEmailCollection)
        Try
            Dim mblnError As Boolean = False
            Dim mstrErrorMessage As String = ""
            Dim objRoleBasedApproval As New clsroleloanapproval_process_Tran
            Dim objProcesspendingloan As New clsProcess_pending_loan


            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            'If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.RETURNTOPREVIOUSAPPROVER AndAlso txtRemarks.Text.Trim.Length <= 0 Then
            '    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmRoleBasedLoanApproval", 32, "Remark cannot be blank.Remark is compulsory information."), Me)
            '    txtRemarks.Focus()
            '    Exit Sub
            'End If
            'Pinkal (21-Mar-2024) -- End

            'Hemant (25 Oct 2024) -- Start
            'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
            Dim intFinalApproverCategorizationTranunkid, intCentralizedApproverCategorizationTranunkid As Integer
            Dim intFinalApproverTransferunkid, intCentralizedApproverTransferunkid As Integer
            Dim intFinalApproverAtEmployeeunkid, intCentralizedApproverAtEmployeeunkid As Integer
            Dim objUser As New clsUserAddEdit
            objUser._Userunkid = CInt(Session("UserId"))
            GetEmployeeHistoryData(CInt(objUser._EmployeeUnkid), intFinalApproverCategorizationTranunkid, intFinalApproverTransferunkid, intFinalApproverAtEmployeeunkid)
            GetEmployeeHistoryData(CInt(Session("LoanCentralizedApproverId")), intCentralizedApproverCategorizationTranunkid, intCentralizedApproverTransferunkid, intCentralizedApproverAtEmployeeunkid)

            objUser = Nothing
            'Hemant (25 Oct 2024) -- End

            For i As Integer = 0 To gRow.Count - 1

                Dim mdtApplicationDate As Date = CDate(gRow(i).Cells(getColumnId_Datagrid(dgvData, "dgcolhApplicationDate", False, True)).Text)

                objRoleBasedApproval._Pendingloanaprovalunkid = CInt(dgvData.DataKeys(gRow(i).ItemIndex))
                objRoleBasedApproval._Processpendingloanunkid = CInt(gRow(i).Cells(getColumnId_Datagrid(dgvData, "objcolhPendingUnkid", False, True)).Text)
                objRoleBasedApproval._Employeeunkid = CInt(gRow(i).Cells(getColumnId_Datagrid(dgvData, "objcolhEmployeeId", False, True)).Text)
                objRoleBasedApproval._Approvaldate = ConfigParameter._Object._CurrentDateAndTime

                'Pinkal (02-Apr-2024) -- Start
                'PAYE Changes For Statutory Reports.
                Dim objLoanScheme As New clsLoan_Scheme
                objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                objRoleBasedApproval._LoanSchemeCode = objLoanScheme._Code.ToString()
                objRoleBasedApproval._LoanScheme = cboLoanScheme.SelectedItem.Text
                objLoanScheme = Nothing
                'Pinkal (02-Apr-2024) -- End


                If CBool(Session("AllowToChangeDeductionPeriodOnLoanApproval")) Then
                    If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                        objRoleBasedApproval._Deductionperiodunkid = CInt(cboDeductionPeriod.SelectedValue)
                    Else
                        objRoleBasedApproval._Deductionperiodunkid = CInt(gRow(i).Cells(getColumnId_Datagrid(dgvData, "objcolhDeductionPeriodId", False, True)).Text)
                    End If
                Else
                    objRoleBasedApproval._Deductionperiodunkid = CInt(gRow(i).Cells(getColumnId_Datagrid(dgvData, "objcolhDeductionPeriodId", False, True)).Text)
                End If

                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString()) = objRoleBasedApproval._Deductionperiodunkid
                Dim mdtPeriodStart As Date = objPeriod._Start_Date.Date
                Dim mdtPeriodEnd As Date = objPeriod._End_Date.Date
                objPeriod = Nothing

                If objRoleBasedApproval._Priority < 0 AndAlso objRoleBasedApproval._Roleunkid <= 0 AndAlso objRoleBasedApproval._Levelunkid <= 0 AndAlso objRoleBasedApproval._Mappingunkid <= 0 Then
                    objRoleBasedApproval._RequiredReportingToApproval = True
                Else
                    If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.RETURNTOAPPLICANT Then
                        Dim strSearch As String = "lna.mappingunkid <= 0 AND lna.roleunkid <=0 AND lna.levelunkid <= 0 AND lna.priority < 0 AND lna.processpendingloanunkid = " & objRoleBasedApproval._Processpendingloanunkid
                        Dim dsList As DataSet = objRoleBasedApproval.GetRoleBasedApprovalList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
                                                             "List", strSearch, "", False, Nothing)


                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                            objRoleBasedApproval._RequiredReportingToApproval = False
                        Else
                            objRoleBasedApproval._RequiredReportingToApproval = True
                        End If
                        dsList.Clear()
                        dsList = Nothing
                    Else
                        objRoleBasedApproval._RequiredReportingToApproval = False
                    End If  ' If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.RETURNTOAPPLICANT Then
                End If

                'Hemant (25 Oct 2024) -- Start
                'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
                objRoleBasedApproval._FinalApproverCategorizationTranunkid = intFinalApproverCategorizationTranunkid
                objRoleBasedApproval._FinalApproverTransferunkid = intFinalApproverTransferunkid
                objRoleBasedApproval._FinalApproverAtEmployeeunkid = intFinalApproverAtEmployeeunkid

                objRoleBasedApproval._CentralizedCategorizationTranunkid = intCentralizedApproverCategorizationTranunkid
                objRoleBasedApproval._CentralizedTransferunkid = intCentralizedApproverTransferunkid
                objRoleBasedApproval._CentralizedAtEmployeeunkid = intCentralizedApproverAtEmployeeunkid
                'Hemant (25 Oct 2024) -- End

                objRoleBasedApproval._Statusunkid = CInt(cboStatus.SelectedValue)
                objRoleBasedApproval._Visibleunkid = CInt(cboStatus.SelectedValue)
                objRoleBasedApproval._Remark = txtRemarks.Text.Trim
                objRoleBasedApproval._Userunkid = CInt(Session("UserId"))
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    objRoleBasedApproval._AuditUserId = CInt(Session("UserId"))
                End If
                objRoleBasedApproval._ClientIP = CStr(Session("IP_ADD"))
                objRoleBasedApproval._HostName = CStr(Session("HOST_NAME"))
                objRoleBasedApproval._FormName = mstrModuleName
                objRoleBasedApproval._IsFromWeb = True

                If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.RETURNTOAPPLICANT Then
                    objRoleBasedApproval._Voiduserunkid = CInt(Session("UserId"))
                    objRoleBasedApproval._Voidreason = txtRemarks.Text.Trim
                End If


                Dim blnFlag As Boolean = objRoleBasedApproval.Update(Session("Database_Name").ToString, CInt(Session("UserId")) _
                                                                        , CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                        , mdtPeriodStart, mdtPeriodEnd, CStr(Session("UserAccessModeSetting")) _
                                                                        , True, CBool(Session("IsIncludeInactiveEmp")), Nothing, mdtApplicationDate.Date, CInt(cboLoanScheme.SelectedValue))

                If blnFlag = False Then
                    mblnError = True
                    mstrErrorMessage = objRoleBasedApproval._Message
                    gRow(i).ForeColor = Drawing.Color.Red
                Else
                    objProcesspendingloan._Processpendingloanunkid = objRoleBasedApproval._Processpendingloanunkid

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        objProcesspendingloan._Userunkid = CInt(Session("UserId"))
                    End If
                    objProcesspendingloan._WebClientIP = CStr(Session("IP_ADD"))
                    objProcesspendingloan._WebHostName = CStr(Session("HOST_NAME"))
                    objProcesspendingloan._WebFormName = mstrModuleName

                    Dim objEmail As New List(Of clsEmailCollection)

                    'Pinkal (21-Mar-2024) -- Start
                    'NMB - Mortgage UAT Enhancements.
                    If objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.PENDING AndAlso CInt(cboStatus.SelectedValue) <> enLoanApplicationStatus.RETURNTOAPPLICANT Then

                        objProcesspendingloan.Send_NotificationFlexCube_Approver(Session("Database_Name").ToString, CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), objProcesspendingloan._Application_Date.Date, _
                                                                                             CInt(cboLoanScheme.SelectedValue), objRoleBasedApproval._Employeeunkid, objProcesspendingloan._Processpendingloanunkid.ToString(), _
                                                                                             Session("ArutiSelfServiceURL").ToString(), CInt(Session("CompanyUnkId")), CInt(cboStatus.SelectedValue), enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), False, objEmail, -999, "")

                        'If objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.PENDING AndAlso CInt(cboStatus.SelectedValue) <> enLoanApplicationStatus.RETURNTOAPPLICANT Then

                        '    objProcesspendingloan.Send_NotificationFlexCube_Approver(Session("Database_Name").ToString, CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), objProcesspendingloan._Application_Date.Date, _
                        '                                                                         CInt(cboLoanScheme.SelectedValue), objRoleBasedApproval._Employeeunkid, objProcesspendingloan._Processpendingloanunkid.ToString(), _
                        '                                                                         Session("ArutiSelfServiceURL").ToString(), CInt(Session("CompanyUnkId")), CInt(cboStatus.SelectedValue), _
                        '                                                                         enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), False, objEmail, CInt(IIf(CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.RETURNTOPREVIOUSAPPROVER, objRoleBasedApproval._Priority, -999)), _
                        '                                                                        IIf(CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.RETURNTOPREVIOUSAPPROVER, txtRemarks.Text, "").ToString())


                        'Pinkal (21-Mar-2024) -- End


                        objmailList.AddRange(objEmail)
                        objEmail.Clear()
                        objEmail = Nothing

                    End If


                    If objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED OrElse objProcesspendingloan._Loan_Statusunkid = enLoanApplicationStatus.REJECTED OrElse CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.RETURNTOAPPLICANT Then

                        objEmail = New List(Of clsEmailCollection)

                        objProcesspendingloan.Send_NotificationFlexCube_Employee(objRoleBasedApproval._Employeeunkid, objProcesspendingloan._Processpendingloanunkid, objProcesspendingloan._Application_No, _
                                                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("CompanyUnkId")), cboLoanScheme.SelectedItem.Text, _
                                                                                              CInt(IIf(CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.RETURNTOAPPLICANT, CInt(cboStatus.SelectedValue), objProcesspendingloan._Loan_Statusunkid)), txtRemarks.Text.Trim, _
                                                                                              enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), False, objEmail)

                        objmailList.AddRange(objEmail)
                        objEmail.Clear()
                        objEmail = Nothing

                    End If

                End If

            Next
            objProcesspendingloan = Nothing
            objRoleBasedApproval = Nothing


            If objmailList IsNot Nothing AndAlso objmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In objmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = CInt(Session("UserId"))
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    Try
                        objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                    Catch ex As Exception
                    End Try
                Next
                objmailList.Clear()
                objmailList = Nothing
            End If

            If mblnError Then
                msg.DisplayMessage(mstrErrorMessage, Me)
                mstrErrorMessage = ""
                mblnError = False
                Exit Sub
            Else
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "All Selected Loan Application(s)") & " " & _
                                             cboStatus.SelectedItem.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "successfully."), Me)
                FillGrid()
                txtRemarks.Text = ""
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SendOTP(ByVal mstrTOTPCode As String) As String
        Dim objLoan As New clsProcess_pending_loan
        Dim objSendMail As New clsSendMail
        Dim mstrError As String = ""
        Try
            objSendMail._ToEmail = Session("SMSGatewayEmail").ToString()
            'objSendMail._ToEmail = mstrEmployeeEmail
            objSendMail._Subject = mstrEmpMobileNo
            objSendMail._Form_Name = mstrModuleName
            objSendMail._LogEmployeeUnkid = -1
            objSendMail._UserUnkid = CInt(Session("UserId"))
            objSendMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
            objSendMail._SenderAddress = CStr(IIf(mstrEmployeeEmail.Trim.Length <= 0, mstrEmployeeName, mstrEmployeeEmail.Trim))
            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
            objSendMail._Message = objLoan.SetSMSForLoanApplication(mstrEmployeeName, mstrTOTPCode, Counter, True)
            mstrError = objSendMail.SendMail(CInt(Session("CompanyUnkId")))
        Catch ex As Exception
            mstrError = ex.Message.ToString()
            msg.DisplayError(ex, Me)
        Finally
            objSendMail = Nothing
            objLoan = Nothing
        End Try
        Return mstrError
    End Function

    Private Sub InsertAttempts(ByRef mblnprompt As Boolean)
        Dim objAttempts As New clsloantotpattempts_tran
        Dim objConfig As New clsConfigOptions
        Try
            Dim xAttempts As Integer = 0
            Dim xAttemptsunkid As Integer = 0
            Dim xOTPRetrivaltime As DateTime = Nothing
            Dim dsAttempts As DataSet = Nothing

            dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, CInt(Session("UserId")), 0)

            If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                xAttempts = CInt(dsAttempts.Tables(0).Rows(0)("attempts"))
                xAttemptsunkid = CInt(dsAttempts.Tables(0).Rows(0)("attemptsunkid"))

                If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                    xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))

                    If xOTPRetrivaltime > objConfig._CurrentDateAndTime Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "You cannot save this loan application.Reason :  As You reached Maximum unsuccessful attempts.Please try after") & " " & CInt(Session("TOTPRetryAfterMins")) & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "mintues") & ".", Me)
                        objAttempts = Nothing
                        mblnprompt = True
                        btnVerify.Enabled = False
                        LblSeconds.Text = "00:00"
                        LblSeconds.Visible = False
                        blnShowTOTPPopup = False
                        popup_TOTP.Hide()
                        Exit Sub
                    End If
                End If

                If xAttempts >= CInt(Session("MaxTOTPUnSuccessfulAttempts")) Then
                    xAttempts = 1
                    xAttemptsunkid = 0
                Else
                    xAttempts = xAttempts + 1
                End If
            Else
                xAttempts = 1
            End If

            objAttempts._Attemptsunkid = xAttemptsunkid
            objAttempts._Attempts = xAttempts
            objAttempts._Attemptdate = objConfig._CurrentDateAndTime
            objAttempts._Otpretrivaltime = Nothing
            objAttempts._loginemployeeunkid = -1
            objAttempts._Userunkid = CInt(Session("UserId"))
            objAttempts._Ip = Session("IP_ADD").ToString
            objAttempts._Machine_Name = Session("HOST_NAME").ToString
            objAttempts._FormName = mstrModuleName
            objAttempts._Isweb = True

            If xAttemptsunkid <= 0 Then
                If objAttempts.Insert() = False Then
                    msg.DisplayMessage(objAttempts._Message, Me)
                    Exit Sub
                End If
            Else
                If xAttempts >= CInt(Session("MaxTOTPUnSuccessfulAttempts")) Then
                    objAttempts._Otpretrivaltime = objConfig._CurrentDateAndTime.AddMinutes(CInt(Session("TOTPRetryAfterMins")))
                    blnShowTOTPPopup = False
                    popup_TOTP.Hide()
                End If
                If objAttempts.Update() = False Then
                    msg.DisplayMessage(objAttempts._Message, Me)
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objConfig = Nothing
            objAttempts = Nothing
        End Try
    End Sub

    'Pinkal (02-Jun-2023) -- End

    'Hemant (25 Oct 2024) -- Start
    'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
    Private Sub GetEmployeeHistoryData(ByVal intEmployeeId As Integer, ByRef intCategorizationTranunkid As Integer, ByRef intTransferunkid As Integer, ByRef intAtEmployeeunkid As Integer)
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim objETaransfer As New clsemployee_transfer_tran
        Dim objEmployee As New clsEmployee_Master
        Dim dsCategorize As New DataSet
        Dim dsTransfer As New DataSet
        Dim dsMovement As New DataSet
        Try

            dsCategorize = objCategorize.Get_Current_Job(ConfigParameter._Object._CurrentDateAndTime.Date, intEmployeeId)
            If dsCategorize.Tables(0).Rows.Count > 0 Then
                intCategorizationTranunkid = CInt(dsCategorize.Tables(0).Rows(0).Item("categorizationtranunkid"))
            Else
                intCategorizationTranunkid = -1
            End If
            dsTransfer = objETaransfer.Get_Current_Allocation(ConfigParameter._Object._CurrentDateAndTime.Date, intEmployeeId)
            If dsTransfer.Tables(0).Rows.Count > 0 Then
                intTransferunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("transferunkid"))
            Else
                intTransferunkid = -1
            End If

            Dim dtMovement As New DataTable
            dsMovement = objEmployee.GetMovement_Log(intEmployeeId, "Log")
            dtMovement = dsMovement.Tables(0).Copy
            dtMovement.DefaultView.Sort = "Date DESC, atemployeeunkid DESC"
            dtMovement = dtMovement.DefaultView.ToTable()
            If dtMovement.Rows.Count > 0 Then
                intAtEmployeeunkid = CInt(dtMovement.Rows(0).Item("atemployeeunkid"))
            Else
                intAtEmployeeunkid = -1
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objCategorize = Nothing
            objETaransfer = Nothing
            objEmployee = Nothing
        End Try
    End Sub
    'Hemant (25 Oct 2024) -- End

#End Region

#Region " Button's Events "

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try

            If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), Me)
                cboLoanScheme.Focus()
                Exit Sub

            ElseIf CInt(cboStatus.SelectedValue) <= 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Status is compulsory information.Please select status to continue."), Me)
                cboStatus.Focus()
                Exit Sub

            ElseIf (CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED OrElse CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.RETURNTOAPPLICANT) AndAlso txtRemarks.Text.Trim.Length <= 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Remark cannot be blank. Remark is compulsory information."), Me)
                txtRemarks.Focus()
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelectApp"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Please select atleast one requisition to do further operation on it."), Me)
                Exit Sub
            End If

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .

            If CBool(Session("ApplyTOTPForLoanApplicationApproval")) Then

                '/* START CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP
                Dim objConfig As New clsConfigOptions
                Dim objAttempts As New clsloantotpattempts_tran
                Dim dsAttempts As DataSet = Nothing

                dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, CInt(Session("UserId")), 0)

                Dim xOTPRetrivaltime As DateTime = Nothing
                If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                    If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                        xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))
                        If xOTPRetrivaltime > objConfig._CurrentDateAndTime Then
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "You cannot save this loan application.Reason : As You reached Maximum unsuccessful attempts.Please try after") & " " & CInt(Session("TOTPRetryAfterMins")) & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "minutes") & ".", Me)
                            objConfig = Nothing
                            objAttempts = Nothing
                            Exit Sub
                        End If
                    End If
                End If
                objConfig = Nothing
                objAttempts = Nothing
                '/* END CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP

                If Session("SMSGatewayEmail").ToString().Trim.Length <= 0 Then
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan approval."), Me)
                    Exit Sub
                End If

                Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(Session("UserId"))
                txtVerifyOTP.Text = ""
                txtVerifyOTP.ReadOnly = False
                btnVerify.Enabled = True
                btnTOTPClose.Enabled = False

                If objUser._TOTPSecurityKey.Trim.Length > 0 Then
                    mstrSecretKey = objUser._TOTPSecurityKey
                Else
                    mstrSecretKey = objTOTPHelper.GenerateSecretKey()
                    objUser._TOTPSecurityKey = mstrSecretKey
                    If objUser.Update(False) = False Then
                        msg.DisplayMessage(objUser._Message, Me)
                        Exit Sub
                    End If
                End If

                If objUser._EmployeeUnkid > 0 Then

                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objUser._EmployeeUnkid

                    mstrEmployeeName = objEmployee._Firstname + " " + objEmployee._Surname
                    mstrEmployeeEmail = objEmployee._Email

                    If CInt(Session("SMSGatewayEmailType")) = CInt(clsEmployee_Master.EmpColEnum.Col_Present_Mobile) Then
                        mstrEmpMobileNo = objEmployee._Present_Mobile.ToString()
                    ElseIf CInt(Session("SMSGatewayEmailType")) = CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Mobile) Then
                        mstrEmpMobileNo = objEmployee._Domicile_Mobile.ToString()
                    End If
                    objEmployee = Nothing
                Else
                    mstrEmployeeName = objUser._Firstname + " " + objUser._Lastname
                    mstrEmployeeEmail = objUser._Email
                    mstrEmpMobileNo = objUser._Phone
                End If
                objUser = Nothing

                If mstrEmpMobileNo.Trim.Length <= 0 Then
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Mobile No is compulsory information. Please set Approver's Mobile No to send TOTP."), Me)
                    Exit Sub
                End If

                Counter = objTOTPHelper._TimeStepSeconds

                Dim mstrTOTPCode As String = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
                Dim mstrError As String = SendOTP(mstrTOTPCode)
                If mstrError.Trim.Length > 0 Then
                    msg.DisplayMessage(mstrError, Me)
                    blnShowTOTPPopup = False
                    popup_TOTP.Hide()
                    Exit Sub
                End If

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:countdown(0," & Counter & "); ", True)
                blnShowTOTPPopup = True
                popup_TOTP.Show()

            Else
                Save(gRow)
            End If

            'Pinkal (02-Jun-2023) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), Me)
                cboLoanScheme.Focus()
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedValue = "0"
            cboLoanScheme.SelectedValue = "0"
            cboDeductionPeriod.SelectedValue = "0"
            txtLoanAmount.Text = "0"
            cboAmountCondition.SelectedIndex = 0
            dgvData.DataSource = New List(Of String)
            dgvData.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Application/wPg_LoanApplicationList.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .

    Protected Sub btnVerify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerify.Click
        Try
            If txtVerifyOTP.Text.Trim.Length <= 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmOTPValidator", 1, "OTP is compulsory information.Please Enter OTP."), Me)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                txtVerifyOTP.Focus()
                popup_TOTP.Show()
                Exit Sub
            End If
            Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))
            If objTOTPHelper.ValidateTotpCode(mstrSecretKey, txtVerifyOTP.Text.Trim) = False Then
                Dim mblnprompt As Boolean = False
                InsertAttempts(mblnprompt)
                If mblnprompt = False Then
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmOTPValidator", 2, "Invalid OTP Code. Please try again."), Me)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                    objTOTPHelper = Nothing
                    Exit Sub
                End If
            Else
                blnShowTOTPPopup = False
                popup_TOTP.Hide()

                Dim gRow As IEnumerable(Of DataGridItem) = Nothing
                gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkSelectApp"), CheckBox).Checked = True)

                If gRow Is Nothing OrElse gRow.Count <= 0 Then
                    msg.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please select atleast one Loan Application to do further operation on it."), Me)
                    Exit Sub
                End If
                Save(gRow)
                gRow = Nothing
            End If
            objTOTPHelper = Nothing
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnSendCodeAgain_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendCodeAgain.Click
        Try
            btnSendCodeAgain.Enabled = False
            btnTOTPClose.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True

            Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))
            Dim mstrTOTPCode As String = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
            Counter = objTOTPHelper._TimeStepSeconds
            Dim mstrError As String = SendOTP(mstrTOTPCode)
            If mstrError.Trim.Length > 0 Then
                msg.DisplayMessage(mstrError, Me)
                blnShowTOTPPopup = False
                popup_TOTP.Hide()
                Exit Sub
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:countdown(0," & Counter & "); ", True)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            popup_TOTP.Show()
        End Try
    End Sub

    Protected Sub btnTOTPClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTOTPClose.Click
        Try
            btnSendCodeAgain.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True
            blnShowTOTPPopup = False
            popup_TOTP.Hide()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (02-Jun-2023) -- End

#End Region

#Region " LinkButton Event"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DataGrid Event"

    Protected Sub dgvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvData.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhApplicationDate", False, True)).Text = CDate(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhApplicationDate", False, True)).Text).ToShortDateString()

                If e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmt", False, True)).Text <> "&nbsp;" AndAlso e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmt", False, True)).Text.Trim <> "" Then
                    e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmt", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhAppliedAmt", False, True)).Text), Session("fmtCurrency").ToString)
                End If

                If e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhInstallmentAmt", False, True)).Text <> "&nbsp;" AndAlso e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhInstallmentAmt", False, True)).Text.Trim <> "" Then
                    e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhInstallmentAmt", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgvData, "dgcolhInstallmentAmt", False, True)).Text), Session("fmtCurrency").ToString)
                End If

            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblDeductionPeriod.ID, Me.LblDeductionPeriod.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanAmount.ID, Me.lblLoanAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gbInfo.ID, Me.gbInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblApprover.ID, Me.lblApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblRemarks.ID, Me.lblRemarks.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.gbPending.ID, Me.gbPending.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvData.Columns(1).FooterText, dgvData.Columns(1).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvData.Columns(2).FooterText, dgvData.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvData.Columns(3).FooterText, dgvData.Columns(3).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvData.Columns(4).FooterText, dgvData.Columns(4).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvData.Columns(5).FooterText, dgvData.Columns(5).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvData.Columns(6).FooterText, dgvData.Columns(6).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvData.Columns(7).FooterText, dgvData.Columns(7).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvData.Columns(8).FooterText, dgvData.Columns(8).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgvData.Columns(9).FooterText, dgvData.Columns(9).HeaderText)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSave.ID, Me.btnSave.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblVerifyOTP.ID, Me.lblVerifyOTP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblOTP.ID, Me.LblOTP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSendCodeAgain.ID, Me.btnSendCodeAgain.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnVerify.ID, Me.btnVerify.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnTOTPClose.ID, Me.btnTOTPClose.Text)
            'Pinkal (02-Jun-2023) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lnkAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.LblDeductionPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblDeductionPeriod.ID, Me.LblDeductionPeriod.Text)
            Me.lblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblLoanAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanAmount.ID, Me.lblLoanAmount.Text)
            Me.gbInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbInfo.ID, Me.gbInfo.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblRemarks.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRemarks.ID, Me.lblRemarks.Text)
            Me.gbPending.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbPending.ID, Me.gbPending.Text)

            dgvData.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(1).FooterText, dgvData.Columns(1).HeaderText)
            dgvData.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(2).FooterText, dgvData.Columns(2).HeaderText)
            dgvData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(3).FooterText, dgvData.Columns(3).HeaderText)
            dgvData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(4).FooterText, dgvData.Columns(4).HeaderText)
            dgvData.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(5).FooterText, dgvData.Columns(5).HeaderText)
            dgvData.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(6).FooterText, dgvData.Columns(6).HeaderText)
            dgvData.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(7).FooterText, dgvData.Columns(7).HeaderText)
            dgvData.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(8).FooterText, dgvData.Columns(8).HeaderText)
            dgvData.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvData.Columns(9).FooterText, dgvData.Columns(9).HeaderText)

            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text)
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text)
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)


            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Me.lblVerifyOTP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVerifyOTP.ID, Me.lblVerifyOTP.Text)
            Me.LblOTP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblOTP.ID, Me.LblOTP.Text)
            Me.btnSendCodeAgain.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSendCodeAgain.ID, Me.btnSendCodeAgain.Text)
            Me.btnVerify.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnVerify.ID, Me.btnVerify.Text)
            Me.btnTOTPClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnTOTPClose.ID, Me.btnTOTPClose.Text)
            'Pinkal (02-Jun-2023) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Loan Scheme is compulsory information. Please select Loan Scheme to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Status is compulsory information.Please select status to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Remark cannot be blank. Remark is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Please select atleast one requisition to do further operation on it.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "All Selected Loan Application(s)")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "successfully.")

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan approval.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Mobile No is compulsory information. Please set Approver's Mobile No to send TOTP.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "You cannot save this loan application.Reason :  As You reached Maximum unsuccessful attempts.Please try after")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "mintues")
            'Pinkal (02-Jun-2023) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


End Class
