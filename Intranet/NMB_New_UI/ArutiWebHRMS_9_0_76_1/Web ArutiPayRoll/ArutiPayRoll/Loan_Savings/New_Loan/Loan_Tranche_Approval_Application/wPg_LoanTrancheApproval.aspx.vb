﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports System.Globalization
Imports System.IO
Imports System.Data.SqlClient

Partial Class Loan_Savings_New_Loan_Loan_Tranche_Approval_Application_wPg_LoanTrancheApproval
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Private objCONN As SqlConnection
    Private ReadOnly mstrModuleName As String = "frmLoanTrancheApproval"
    Private mintLoanTrancheApprovalId As Integer = 0
    Private mintLoanTrancheRequestId As Integer = 0
    Private mintMapUserId As Integer = 0
    Private mintDeleteRowIndex As Integer = -1
    Private mdtLoanTrancheApplicationDocument As DataTable = Nothing

    'Pinkal (17-May-2024) -- Start
    'NMB Enhancement For Mortgage Loan.
    'Private mblnValidTrancheDate As Boolean = False
    Private mblnValidTrancheDate As Boolean = True
    'Pinkal (17-May-2024) -- End

    Private mdecOriginalTrancheAmount As Decimal = 0
    Private mintDeductionPeriodUnkId As Integer = 0
    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Dim xStatusId As Integer = enLoanApplicationStatus.PENDING

    'Pinkal (16-Nov-2023) -- Start
    '(A1X-1489) NMB - Mortgage tranche disbursement API.
    Dim mstrSecretKey As String = ""
    Private blnShowTOTPPopup As Boolean = False
    Dim Counter As Integer = 0
    Dim mstrEmpMobileNo As String = ""
    Dim mstrEmployeeName As String = ""
    Dim mstrEmployeeEmail As String = ""
    'Pinkal (16-Nov-2023) -- End


#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If

                If Request.QueryString("uploadimage") Is Nothing Then

                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                    If arr.Length = 4 Then

                        mintLoanTrancheApprovalId = CInt(arr(0))
                        mintMapUserId = CInt(arr(1))
                        mintLoanTrancheRequestId = CInt(arr(2))
                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(3))
                        HttpContext.Current.Session("UserId") = mintMapUserId
                        HttpContext.Current.Session("loantrancheapprovalunkid") = mintLoanTrancheApprovalId
                        HttpContext.Current.Session("loantrancherequestunkid") = mintLoanTrancheRequestId
                        HttpContext.Current.Session("Mapuserunkid") = mintMapUserId

                        'Pinkal (23-Feb-2024) -- Start
                        '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                        Dim objConfig As New clsConfigOptions
                        Dim mblnATLoginRequiredToApproveApplications As Boolean = CBool(objConfig.GetKeyValue(CInt(Session("CompanyUnkId")), "LoginRequiredToApproveApplications", Nothing))
                        objConfig = Nothing

                        If mblnATLoginRequiredToApproveApplications = False Then
                            Dim objBasePage As New Basepage
                            objBasePage.GenerateAuthentication()
                            objBasePage = Nothing
                        End If


                        If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then

                            If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then
                                Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                                Session("ApproverUserId") = CInt(Session("UserId"))
                                DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                                Exit Sub
                            Else
                                If mblnATLoginRequiredToApproveApplications = False Then

                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        HttpContext.Current.Session("mdbname") = Session("Database_Name")

                        gobjConfigOptions = New clsConfigOptions
                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))

                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)


                        If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                            Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                        Else
                            Me.ViewState.Add("ArutiSelfServiceURL", ConfigParameter._Object._ArutiSelfServiceURL)
                        End If
                        Session("DateFormat") = ConfigParameter._Object._CompanyDateFormat
                        Session("DateSeparator") = ConfigParameter._Object._CompanyDateSeparator

                        Call SetDateFormat()

                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))
                        objUser = Nothing


                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName

                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                        strError = ""
                        If SetUserSessions(strError) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                                End If ' If mblnATLoginRequiredToApproveApplications = False Then

                            End If ' If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                        Else
                            Session("ApprovalLink") = Request.Url.AbsoluteUri.ToString()
                            Session("ApproverUserId") = CInt(Session("UserId"))
                            DisplayMessage.DisplayMessage("Sorry, Please Login to do futher operation on it.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                            Exit Sub

                        End If '  If Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing Then




                        Dim objLoanTrancheApproval As New clsloanTranche_approval_Tran

                        Dim dsList As DataSet = Nothing
                        Dim mstrSearch As String = "lta.loantrancherequestunkid = " & mintLoanTrancheRequestId & " AND lta.loantrancheapprovalunkid = " & mintLoanTrancheApprovalId


                        dsList = objLoanTrancheApproval.GetLoanTrancheApprovalList(Session("Database_Name").ToString, _
                                                                     CInt(Session("UserId")), _
                                                                     CInt(Session("Fin_year")), _
                                                                     CInt(Session("CompanyUnkId")), _
                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                     CStr(Session("UserAccessModeSetting")), True, _
                                                                     CBool(Session("IsIncludeInactiveEmp")), "List", mstrSearch, "", False, Nothing)

                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            If CInt(dsList.Tables(0).Rows(0)("visibleunkid")) <> enLoanApplicationStatus.PENDING Then
                                If CInt(dsList.Tables(0).Rows(0)("statusunkid")) = enLoanApplicationStatus.REJECTED Then
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "You can't Edit this Loan tranche detail. Reason: This Loan tranche is already rejected."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                                    Session.Remove("loantrancheapprovalunkid")
                                    Session.Remove("loantrancherequestunkid")
                                    Session.Remove("Mapuserunkid")

                                    'Pinkal (23-Feb-2024) -- Start
                                    '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                                    Session("ApprovalLink") = Nothing
                                    Session("ApproverUserId") = Nothing

                                    If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                        If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                            Session.Abandon()
                                            If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                                Response.Cookies("ASP.NET_SessionId").Value = ""
                                                Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                                Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                            End If

                                            If Request.Cookies("AuthToken") IsNot Nothing Then
                                                Response.Cookies("AuthToken").Value = ""
                                                Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                            End If

                                        End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                    Exit Sub

                                    'Pinkal (23-Feb-2024) -- End

                                Else
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "You can't Edit this Loan tranche detail. Reason: This Loan tranche is already approved."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                                    Session.Remove("loantrancheapprovalunkid")
                                    Session.Remove("loantrancherequestunkid")
                                    Session.Remove("Mapuserunkid")

                                    'Pinkal (23-Feb-2024) -- Start
                                    '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                                    Session("ApprovalLink") = Nothing
                                    Session("ApproverUserId") = Nothing

                                    If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                        If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                            Session.Abandon()
                                            If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                                Response.Cookies("ASP.NET_SessionId").Value = ""
                                                Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                                Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                            End If

                                            If Request.Cookies("AuthToken") IsNot Nothing Then
                                                Response.Cookies("AuthToken").Value = ""
                                                Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                            End If

                                        End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                    Exit Sub

                                    'Pinkal (23-Feb-2024) -- End

                                End If
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Sorry, there is no pending loan tranche application data."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Session.Remove("loantrancheapprovalunkid")
                            Session.Remove("loantrancherequestunkid")
                            Session.Remove("Mapuserunkid")

                            'Pinkal (23-Feb-2024) -- Start
                            '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                            Session("ApprovalLink") = Nothing
                            Session("ApproverUserId") = Nothing

                            If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    Session.Abandon()
                                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                        Response.Cookies("ASP.NET_SessionId").Value = ""
                                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                    End If

                                    If Request.Cookies("AuthToken") IsNot Nothing Then
                                        Response.Cookies("AuthToken").Value = ""
                                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                    End If

                                End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                            End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                            Exit Sub

                            'Pinkal (23-Feb-2024) -- End

                            Exit Sub
                        End If

                        Dim dtRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") >= CInt(dsList.Tables(0).Rows(0)("priority")) AndAlso x.Field(Of Integer)("statusunkid") <> enLoanApplicationStatus.PENDING)
                        If dtRow.Count > 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "You can't Edit this Loan tranche detail. Reason: This Loan tranche is already approved/reject."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Session.Remove("loantrancheapprovalunkid")
                            Session.Remove("loantrancherequestunkid")
                            Session.Remove("Mapuserunkid")

                            'Pinkal (23-Feb-2024) -- Start
                            '(A1X-2461) NMB : R&D - Force manual user login on approval links.

                            Session("ApprovalLink") = Nothing
                            Session("ApproverUserId") = Nothing

                            If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                                If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                                    Session.Abandon()
                                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                                        Response.Cookies("ASP.NET_SessionId").Value = ""
                                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                                    End If

                                    If Request.Cookies("AuthToken") IsNot Nothing Then
                                        Response.Cookies("AuthToken").Value = ""
                                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                                    End If

                                End If  '   If Session("AuthToken").ToString().Trim() <> Request.Cookies("AuthToken").Value.ToString().Trim() Then

                            End If  'If Session("clsuser") IsNot Nothing AndAlso (Session("AuthToken") IsNot Nothing AndAlso Request.Cookies("AuthToken") IsNot Nothing) Then

                            Exit Sub

                            'Pinkal (23-Feb-2024) -- End


                            Exit Sub
                        End If
                        GoTo Link

                    End If  'If arr.Length = 4 Then

                End If  'If Request.QueryString("uploadimage") Is Nothing Then

            End If  'If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

Link:

            If IsPostBack = False Then
                Call SetControlCaptions()
                SetMessages()
                Call SetLanguage()

                If Session("loantrancheapprovalunkid") IsNot Nothing Then
                    mintLoanTrancheApprovalId = CInt(Session("loantrancheapprovalunkid"))
                End If

                If Session("loantrancherequestunkid") IsNot Nothing Then
                    mintLoanTrancheRequestId = CInt(Session("loantrancherequestunkid"))
                End If

                If Session("Mapuserunkid") IsNot Nothing Then
                    mintMapUserId = CInt(Session("Mapuserunkid"))
                End If

                FillCombo()
                FillPeriod(False)
                SetVisibility()
                GetValue()
            Else
                mintLoanTrancheApprovalId = CInt(Me.ViewState("loantrancheapprovalunkid"))
                mintLoanTrancheRequestId = CInt(Me.ViewState("loantrancherequestunkid"))
                mintMapUserId = CInt(Me.ViewState("Mapuserunkid"))
                xStatusId = CInt(Me.ViewState("xStatusId"))
                mintDeleteRowIndex = CInt(ViewState("DeleteRowIndex"))
                mblnValidTrancheDate = CBool(ViewState("ValidTranchDate"))
                mdecOriginalTrancheAmount = CDec(ViewState("OriginalTrancheAmount"))
                mintDeductionPeriodUnkId = CInt(Me.ViewState("DeductionPeriodUnkId"))
                mdtPeriodStart = CDate(ViewState("mdtPeriodStart"))
                mdtPeriodEnd = CDate(ViewState("mdtPeriodEnd"))
                mstrBaseCurrSign = ViewState("mstrBaseCurrSign").ToString()
                mintBaseCurrId = CInt(ViewState("mintBaseCurrId"))
                mintPaidCurrId = CInt(ViewState("mintPaidCurrId"))
                mdecBaseExRate = CDec(ViewState("mdecBaseExRate"))
                mdecPaidExRate = CDec(ViewState("mdecPaidExRate"))

                mdtLoanTrancheApplicationDocument = CType(Session("LoanTrancheApplicationDocument"), DataTable)

                'Pinkal (16-Nov-2023) -- Start
                '(A1X-1489) NMB - Mortgage tranche disbursement API.
                mstrSecretKey = CStr(Me.ViewState("mstrSecretKey"))
                Counter = CInt(Me.ViewState("Counter"))
                blnShowTOTPPopup = CBool(Me.ViewState("blnShowTOTPPopup"))
                mstrEmployeeName = CStr(Me.ViewState("EmployeeName"))
                mstrEmployeeEmail = CStr(Me.ViewState("EmployeeEmail"))
                mstrEmpMobileNo = CStr(Me.ViewState("EmployeeMobileNo"))


                If blnShowTOTPPopup Then
                    popup_TOTP.Show()
                End If
                'Pinkal (16-Nov-2023) -- End

            End If

            If Request.QueryString("uploadimage") IsNot Nothing Then
                If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                    Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                    postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                    Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Request.QueryString.Count <= 0 Then
                Me.IsLoginRequired = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("loantrancheapprovalunkid") = mintLoanTrancheApprovalId
            Me.ViewState("loantrancherequestunkid") = mintLoanTrancheRequestId
            Me.ViewState("Mapuserunkid") = mintMapUserId
            Me.ViewState("OriginalTrancheAmount") = mdecOriginalTrancheAmount
            Me.ViewState("xStatusId") = xStatusId
            Me.ViewState("DeleteRowIndex") = mintDeleteRowIndex
            Me.ViewState("ValidTranchDate") = mblnValidTrancheDate
            Me.ViewState("DeductionPeriodUnkId") = mintDeductionPeriodUnkId
            Me.ViewState("mdtPeriodStart") = mdtPeriodStart
            Me.ViewState("mdtPeriodEnd") = mdtPeriodEnd
            Me.ViewState("mstrBaseCurrSign") = mstrBaseCurrSign
            Me.ViewState("mintBaseCurrId") = mintBaseCurrId
            Me.ViewState("mintPaidCurrId") = mintPaidCurrId
            Me.ViewState("mdecBaseExRate") = mdecBaseExRate
            Me.ViewState("mdecPaidExRate") = mdecPaidExRate

            Session("LoanTrancheApplicationDocument") = mdtLoanTrancheApplicationDocument


            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            Me.ViewState("Counter") = Counter
            Me.ViewState("mstrSecretKey") = mstrSecretKey
            Me.ViewState("blnShowTOTPPopup") = blnShowTOTPPopup
            Me.ViewState("EmployeeName") = mstrEmployeeName
            Me.ViewState("EmployeeEmail") = mstrEmployeeEmail
            Me.ViewState("EmployeeMobileNo") = mstrEmpMobileNo
            'Pinkal (16-Nov-2023) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetVisibility()
        Try
            LblDeductionPeriod.Visible = CBool(Session("AllowToChangeDeductionPeriodonLoanTrancheApproval"))
            cboDeductionPeriod.Visible = CBool(Session("AllowToChangeDeductionPeriodonLoanTrancheApproval"))

            lblDocumentType.Visible = CBool(Session("AllowToAttachDocumentOnLoanTrancheApproval"))
            cboDocumentType.Visible = CBool(Session("AllowToAttachDocumentOnLoanTrancheApproval"))
            btnAddFile.Visible = CBool(Session("AllowToAttachDocumentOnLoanTrancheApproval"))
            btnAddAttachment.Visible = CBool(Session("AllowToAttachDocumentOnLoanTrancheApproval"))
            dgAttachment.Columns(getColumnId_Datagrid(dgAttachment, "objcohDelete", False, True)).Visible = CBool(Session("AllowToAttachDocumentOnLoanTrancheApproval"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objLoanApplication As New clsloanTranche_request_Tran
        Try

            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If

            dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                        CInt(Session("UserId")), _
                                        CInt(Session("Fin_year")), _
                                        CInt(Session("CompanyUnkId")), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                        CStr(Session("UserAccessModeSetting")), True, _
                                        False, "Employee", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedIndex = 0
            End With


            Dim mblnSchemeShowOnEss As Boolean = True
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "loanschemecategory_id = " & enLoanSchemeCategories.MORTGAGE & " AND  ISNULL(ispostingtoflexcube,0) = 1 ", mblnSchemeShowOnEss)

            With cboLoanscheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim objCurrency As New clsExchangeRate
            dsList = objCurrency.getComboList("Currency", True)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsList.Tables("Currency")
                .DataBind()
                Dim drRow() As DataRow = dsList.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    Call cboCurrency_SelectedIndexChanged(cboCurrency, New System.EventArgs())
                End If
                drRow = Nothing
            End With

            FillDocumentTypeCombo()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanApplication = Nothing
            objLoanScheme = Nothing
        End Try
    End Sub

    Private Sub FillDocumentTypeCombo(Optional ByVal mstrDocumentIds As String = "")
        Dim objCommonMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Dim dt As New DataTable
        Dim strFilter As String = String.Empty
        Dim dtTable As DataTable = Nothing
        Try

            dsCombos = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")

            If mstrDocumentIds.Trim.Length > 0 Then
                dtTable = New DataView(dsCombos.Tables(0), "masterunkid in (0," & mstrDocumentIds & ")", "", DataViewRowState.CurrentRows).ToTable()
            Else
                dtTable = dsCombos.Tables(0)
            End If

            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dtTable.Copy
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dsCombos IsNot Nothing Then dsCombos.Clear()
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            dsCombos = Nothing
            objCommonMaster = Nothing
        End Try
    End Sub

    Private Sub FillPeriod(ByVal blnOnlyFirstPeriod As Boolean)
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombos As DataSet
        Try


            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                 CInt(Session("Fin_year")), _
                                                 Session("Database_Name").ToString, _
                                                 CDate(Session("fin_startdate")), _
                                                 "List", False, enStatusType.OPEN)

            With cboDeductionPeriod
                .DataTextField = "name"
                .DataValueField = "periodunkid"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With


            If dsCombos.Tables(0).Rows.Count > 0 Then
                mintDeductionPeriodUnkId = CInt(dsCombos.Tables(0).Rows(0).Item("periodunkid"))
            End If

            cboDeductionPeriod_SelectedIndexChanged()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub GetValue()
        Dim objLoanTrancheApproval As New clsloanTranche_approval_Tran
        Try
            If mintLoanTrancheApprovalId > 0 Then
                objLoanTrancheApproval._Loantrancheapprovalunkid = mintLoanTrancheApprovalId

                cboDeductionPeriod.SelectedValue = mintDeductionPeriodUnkId.ToString()

                Dim objLoanTranche As New clsloanTranche_request_Tran
                objLoanTranche._Loantrancherequestunkid = mintLoanTrancheRequestId
                txtTrancheLoanApplicationNo.Text = objLoanTranche._Applicationno

                If objLoanTranche._Tranchedate <> Nothing Then
                    dtpTrancheDate.SetDate = objLoanTranche._Tranchedate.Date
                Else
                    dtpTrancheDate.SetDate = Basepage.GetCurrentDateTime.Date
                End If
                dtpTrancheDate.Enabled = False

                cboEmployee.SelectedValue = CStr(objLoanTranche._Employeeunkid)
                cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())

                If objLoanTranche._Processpendingloanunkid > 0 Then
                    cboLoanApplicationNo.SelectedValue = objLoanTranche._Processpendingloanunkid.ToString()
                    cboLoanApplicationNo.Enabled = False
                    cboLoanApplicationNo_SelectedIndexChanged(cboLoanApplicationNo, New EventArgs())
                End If

                If objLoanTranche._Loanschemeunkid > 0 Then
                    cboLoanscheme.SelectedValue = objLoanTranche._Loanschemeunkid.ToString()
                    cboLoanscheme_SelectedIndexChanged(cboLoanscheme, New EventArgs())
                End If

                TxtTrancheAmount.Text = Format(objLoanTrancheApproval._Loantranche_Amount, Session("fmtcurrency").ToString())

                If objLoanTranche._Remark.Trim.Length > 0 Then
                    TxtRemark.Text = objLoanTranche._Remark.Trim()
                End If

                If Session("LoanTrancheApplicationDocument") Is Nothing Then
                    Dim objAttchement As New clsScan_Attach_Documents
                    'Call objAttchement.GetList(Session("Document_Path").ToString(), "List ", "", CInt(cboEmployee.SelectedValue), , , , "hrdocuments_tran.transactionunkid = " & mintLoanTrancheRequestId, CBool(IIf(mintLoanTrancheRequestId > 0, False, True)), , enScanAttactRefId.LOAN_TRANCHE, 0, True, "")
                    'mdtLoanTrancheApplicationDocument = objAttchement._Datatable.Copy()
                    mdtLoanTrancheApplicationDocument = objAttchement.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.LOAN_TRANCHE, mintLoanTrancheRequestId, Session("Document_Path").ToString())
                    objAttchement = Nothing
                    Session("LoanTrancheApplicationDocument") = mdtLoanTrancheApplicationDocument
                Else
                    mdtLoanTrancheApplicationDocument = CType(Session("LoanTrancheApplicationDocument"), DataTable)
                End If

                objLoanTranche = Nothing
            End If

            Call FillLoanTrancheRequestAttachment()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanTrancheApproval = Nothing
        End Try
    End Sub

    Private Sub FillLoanTrancheRequestAttachment()
        Dim dtView As DataView
        Try
            If mdtLoanTrancheApplicationDocument Is Nothing Then
                dgAttachment.DataSource = Nothing
                dgAttachment.DataBind()
                Exit Sub
            End If

            dtView = New DataView(mdtLoanTrancheApplicationDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgAttachment.AutoGenerateColumns = False
            dgAttachment.DataSource = dtView
            dgAttachment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim objDocument As New clsScan_Attach_Documents
            Dim dtRow() As DataRow = mdtLoanTrancheApplicationDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtLoanTrancheApplicationDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Loan_Balance
                dRow("scanattachrefid") = enScanAttactRefId.LOAN_TRANCHE
                dRow("transactionunkid") = mintLoanTrancheRequestId
                dRow("filepath") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = DateTime.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024

                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))
                dRow("Documentype") = GetMimeType(strfullpath)

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                dRow("attachment_type") = cboDocumentType.SelectedItem.Text
                'Pinkal (17-May-2024) -- End

                Dim bytes() As Byte
                bytes = Encoding.ASCII.GetBytes(strfullpath)
                Dim xDocumentData As Byte() = bytes
                Try
                    dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Catch ex As FormatException
                    CommonCodes.LogErrorOnly(ex)
                End Try

                Dim objFile As New FileInfo(strfullpath)
                dRow("fileextension") = objFile.Extension.ToString().Replace(".", "")
                objFile = Nothing
                dRow("file_data") = xDocumentData
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtLoanTrancheApplicationDocument.Rows.Add(dRow)

            Session("LoanTrancheApplicationDocument") = mdtLoanTrancheApplicationDocument

            Call FillLoanTrancheRequestAttachment()

            objDocument = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue(ByVal objLoanTranche As clsloanTranche_request_Tran)
        Try
            If mintLoanTrancheRequestId > 0 Then
                objLoanTranche._Loantrancherequestunkid = mintLoanTrancheRequestId
            End If

            objLoanTranche._Applicationno = txtTrancheLoanApplicationNo.Text.Trim
            objLoanTranche._Processpendingloanunkid = CInt(cboLoanApplicationNo.SelectedValue)
            objLoanTranche._Employeeunkid = CInt(Session("Employeeunkid"))
            objLoanTranche._Loanschemeunkid = CInt(cboLoanscheme.SelectedValue)
            objLoanTranche._Tranchedate = dtpApplicationDate.GetDate.Date
            objLoanTranche._Trancheamt = CDec(TxtTrancheAmount.Text)
            objLoanTranche._Statusunkid = enLoanApplicationStatus.PENDING
            objLoanTranche._Countryunkid = CInt(cboCurrency.SelectedValue)
            objLoanTranche._DeductionPeriodunkid = mintDeductionPeriodUnkId
            objLoanTranche._FinalDeductionPeriodunkid = 0
            objLoanTranche._Finalapproverunkid = 0
            objLoanTranche._Approved_Trancheamt = 0
            objLoanTranche._Remark = TxtRemark.Text.Trim
            objLoanTranche._Document = mdtLoanTrancheApplicationDocument.Copy()
            objLoanTranche._Isvoid = False
            objLoanTranche._Voiduserunkid = -1
            objLoanTranche._Voiddatetime = Nothing
            objLoanTranche._Voidreason = ""
            objLoanTranche._Userunkid = -1

            objLoanTranche._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            objLoanTranche._ClientIP = CStr(Session("IP_ADD"))
            objLoanTranche._HostName = CStr(Session("HOST_NAME"))
            objLoanTranche._FormName = mstrModuleName
            objLoanTranche._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation(ByVal xStatusId As Integer) As Boolean
        Dim mblnFlag As Boolean = True
        Try


            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Return False

            ElseIf CInt(cboLoanscheme.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), Me)
                cboLoanscheme.Focus()
                Return False

            ElseIf CInt(cboLoanApplicationNo.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Loan Application is compulsory information.Please Select Loan Application No."), Me)
                cboLoanApplicationNo.Focus()
                mblnFlag = False

            ElseIf TxtTrancheAmount.Text.Trim.Length <= 0 OrElse CDec(TxtTrancheAmount.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Loan Tranche Amount should be greater than 0.Please enter valid Loan Tranche Amount."), Me)
                TxtTrancheAmount.Focus()
                mblnFlag = False

            ElseIf mblnValidTrancheDate = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "You can not apply for this Loan Tranche application.Reason : You have to wait for the eligible date for the next tranche."), Me)
                mblnFlag = False

            ElseIf xStatusId = enLoanApplicationStatus.APPROVED AndAlso TxtTrancheAmount.Text.Trim.Length > 0 AndAlso mdecOriginalTrancheAmount < CDec(TxtTrancheAmount.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Loan Trache Amount should be less than or equal to") & " " & Format(mdecOriginalTrancheAmount, Session("fmtcurrency").ToString()) & ".", Me)
                TxtTrancheAmount.Focus()
                mblnFlag = False

            ElseIf xStatusId = enLoanApplicationStatus.REJECTED AndAlso TxtApprovalRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Remark cannot be blank.Remark is compulsory information."), Me)
                TxtApprovalRemark.Focus()
                mblnFlag = False

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            mblnFlag = False
        End Try
        Return mblnFlag
    End Function

    Private Sub SaveApproval(ByVal xStatusId As Integer)
        Dim objLoanTrancheApproval As New clsloanTranche_approval_Tran
        Dim mblnFinalApproval As Boolean = False
        Try
            Dim mdsDoc As DataSet
            Dim mstrFolderName As String = ""
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim strFileName As String = ""
            For Each dRowAttachment As DataRow In mdtLoanTrancheApplicationDocument.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRowAttachment("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault

                If dRowAttachment("AUD").ToString = "A" AndAlso dRowAttachment("localpath").ToString <> "" Then
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRowAttachment("localpath")))
                    If File.Exists(CStr(dRowAttachment("localpath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(dRowAttachment("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRowAttachment("fileuniquename") = strFileName
                        dRowAttachment("filepath") = strPath
                        dRowAttachment.AcceptChanges()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If
                ElseIf dRowAttachment("AUD").ToString = "D" AndAlso dRowAttachment("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRowAttachment("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "File does not exist on localpath"), Me)
                            Exit Sub
                        End If
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "File does not exist on localpath"), Me)
                        Exit Sub
                    End If
                End If
            Next

            objLoanTrancheApproval._Loantrancheapprovalunkid = mintLoanTrancheApprovalId
            objLoanTrancheApproval._Loantrancherequestunkid = mintLoanTrancheRequestId
            objLoanTrancheApproval._Processpendingloanunkid = CInt(cboLoanApplicationNo.SelectedValue)
            objLoanTrancheApproval._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objLoanTrancheApproval._Approvaldate = Basepage.GetCurrentDateTime()
            objLoanTrancheApproval._Loantranche_Amount = CDec(TxtTrancheAmount.Text)
            objLoanTrancheApproval._Deductionperiodunkid = CInt(cboDeductionPeriod.SelectedValue)
            objLoanTrancheApproval._Statusunkid = xStatusId
            objLoanTrancheApproval._Visibleunkid = xStatusId
            objLoanTrancheApproval._Remark = TxtApprovalRemark.Text.Trim
            objLoanTrancheApproval._Document = mdtLoanTrancheApplicationDocument
            objLoanTrancheApproval._Userunkid = CInt(Session("UserId"))
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objLoanTrancheApproval._AuditUserId = CInt(Session("UserId"))
            End If
            objLoanTrancheApproval._ClientIP = CStr(Session("IP_ADD"))
            objLoanTrancheApproval._HostName = CStr(Session("HOST_NAME"))
            objLoanTrancheApproval._FormName = mstrModuleName
            objLoanTrancheApproval._IsFromWeb = True

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            Dim objLoanScheme As New clsLoan_Scheme
            objLoanScheme._Loanschemeunkid = CInt(cboLoanscheme.SelectedValue)
            objLoanTrancheApproval._LoanSchemeCode = objLoanScheme._Code
            objLoanScheme = Nothing

            objLoanTrancheApproval._Currency = cboCurrency.SelectedItem.Text
            'Pinkal (16-Nov-2023) -- End


            Dim blnFlag As Boolean = False
            If mintLoanTrancheApprovalId > 0 Then
                blnFlag = objLoanTrancheApproval.Update(Session("Database_Name").ToString(), CInt(Session("UserId")) _
                                                                              , CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                              , dtpApplicationDate.GetDate.Date, dtpApplicationDate.GetDate.Date _
                                                                              , CStr(Session("UserAccessModeSetting")), True, False)

            End If

            If blnFlag = False And objLoanTrancheApproval._Message <> "" Then
                DisplayMessage.DisplayMessage(objLoanTrancheApproval._Message, Me)
                Exit Sub
            Else

                'Pinkal (16-Nov-2023) -- Start
                '(A1X-1489) NMB - Mortgage tranche disbursement API.
                SendNotifications(xStatusId, mblnFinalApproval)
                'Pinkal (16-Nov-2023) -- End

                Dim mstrStatus As String = ""
                If xStatusId = enLoanApplicationStatus.APPROVED Then
                    mstrStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsloanTranche_approval_Tran", 2, "Approved")
                    If mblnFinalApproval Then mstrStatus &= " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "and disbursed")
                ElseIf xStatusId = enLoanApplicationStatus.REJECTED Then
                    mstrStatus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsloanTranche_approval_Tran", 3, "Rejected")
                End If


                If Request.QueryString.Count > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Loan Tranche Application") & " " & mstrStatus & " " & _
                                                          Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "successfully") & ".", Me, Session("rootpath").ToString() & "Index.aspx")

                    'Pinkal (23-Feb-2024) -- Start
                    '(A1X-2461) NMB : R&D - Force manual user login on approval links..
                    Session.Clear()
                    Session.Abandon()
                    Session.RemoveAll()

                    If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                        Response.Cookies("ASP.NET_SessionId").Value = ""
                        Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                    End If

                    If Request.Cookies("AuthToken") IsNot Nothing Then
                        Response.Cookies("AuthToken").Value = ""
                        Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                    End If
                    'Pinkal (23-Feb-2024) -- End

                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Loan Tranche Application") & " " & mstrStatus & " " & _
                                                          Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "successfully") & ".", Me, Session("rootpath").ToString() & "Loan_Savings/New_Loan/Loan_Tranche_Approval_Application/wPg_LoanTrancheApprovalList.aspx")
                End If


                mintLoanTrancheApprovalId = 0
                mintLoanTrancheRequestId = 0
                mintMapUserId = 0

                Session("loantrancheapprovalunkid") = Nothing
                Session.Remove("loantrancheapprovalunkid")

                Session("loantrancherequestunkid") = Nothing
                Session.Remove("loantrancherequestunkid")

                Session("Mapuserunkid") = Nothing
                Session.Remove("Mapuserunkid")

                Session("LoanTrancheRequestId") = Nothing
                Session.Remove("LoanTrancheRequestId")

                If mdtLoanTrancheApplicationDocument IsNot Nothing Then mdtLoanTrancheApplicationDocument.Clear()
                mdtLoanTrancheApplicationDocument = Nothing
                Session("LoanTrancheApplicationDocument") = Nothing
                Session.Remove("LoanTrancheApplicationDocument")
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanTrancheApproval = Nothing
        End Try
    End Sub

    'Pinkal (16-Nov-2023) -- Start
    '(A1X-1489) NMB - Mortgage tranche disbursement API.

    Private Sub SendNotifications(ByVal xStatusId As Integer, ByRef mblnFinalApproval As Boolean)
        Try
            Dim objLoanRequest As New clsloanTranche_request_Tran
            objLoanRequest._Loantrancherequestunkid = mintLoanTrancheRequestId

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objLoanRequest._Userunkid = CInt(Session("UserId"))
            End If
            objLoanRequest._ClientIP = CStr(Session("IP_ADD"))
            objLoanRequest._HostName = CStr(Session("HOST_NAME"))
            objLoanRequest._FormName = mstrModuleName
            objLoanRequest._IsFromWeb = True

            If objLoanRequest._Statusunkid = enLoanApplicationStatus.PENDING AndAlso xStatusId <> enLoanApplicationStatus.REJECTED Then
                objLoanRequest.Send_NotificationLoanTranche_Approver(Session("Database_Name").ToString, CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, objLoanRequest._Tranchedate.Date, _
                                                                                      CInt(cboLoanscheme.SelectedValue), CInt(cboEmployee.SelectedValue), objLoanRequest._Loantrancherequestunkid.ToString(), _
                                                                                      Session("ArutiSelfServiceURL").ToString(), CInt(Session("CompanyUnkId")), enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))

            End If

            If objLoanRequest._Statusunkid = enLoanApplicationStatus.APPROVED OrElse objLoanRequest._Statusunkid = enLoanApplicationStatus.REJECTED Then

                If objLoanRequest._Statusunkid = enLoanApplicationStatus.APPROVED Then mblnFinalApproval = True

                objLoanRequest.Send_NotificationLoanTranche_Employee(CInt(cboEmployee.SelectedValue), objLoanRequest._Loantrancherequestunkid, objLoanRequest._Applicationno, _
                                                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("CompanyUnkId")), cboLoanscheme.SelectedItem.Text, _
                                                                                       xStatusId, TxtApprovalRemark.Text.Trim, _
                                                                                       enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))

            End If
            objLoanRequest = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SendOTP(ByVal mstrTOTPCode As String) As String
        Dim objLoanTranche As New clsloanTranche_request_Tran
        Dim objSendMail As New clsSendMail
        Dim mstrError As String = ""
        Try
            objSendMail._ToEmail = Session("SMSGatewayEmail").ToString()
            objSendMail._Subject = mstrEmpMobileNo
            objSendMail._Form_Name = mstrModuleName
            objSendMail._LogEmployeeUnkid = -1
            objSendMail._UserUnkid = CInt(Session("UserId"))
            objSendMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
            objSendMail._SenderAddress = CStr(IIf(mstrEmployeeEmail.Trim.Length <= 0, mstrEmployeeName, mstrEmployeeEmail.Trim))
            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
            objSendMail._Message = objLoanTranche.SetSMSForLoanTrancheApplication(mstrEmployeeName, mstrTOTPCode, Counter, True)
            mstrError = objSendMail.SendMail(CInt(Session("CompanyUnkId")))
        Catch ex As Exception
            mstrError = ex.Message.ToString()
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objSendMail = Nothing
            objLoanTranche = Nothing
        End Try
        Return mstrError
    End Function

    Private Sub InsertAttempts(ByRef mblnprompt As Boolean)
        Dim objAttempts As New clsloantotpattempts_tran
        Dim objConfig As New clsConfigOptions
        Try
            Dim xAttempts As Integer = 0
            Dim xAttemptsunkid As Integer = 0
            Dim xOTPRetrivaltime As DateTime = Nothing
            Dim dsAttempts As DataSet = Nothing

            dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, CInt(Session("UserId")), 0)

            If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                xAttempts = CInt(dsAttempts.Tables(0).Rows(0)("attempts"))
                xAttemptsunkid = CInt(dsAttempts.Tables(0).Rows(0)("attemptsunkid"))

                If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                    xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))

                    If xOTPRetrivaltime > objConfig._CurrentDateAndTime Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "You cannot approve this loan tranche application.Reason :  As You reached Maximum unsuccessful attempts.Please try after") & " " & CInt(Session("TOTPRetryAfterMins")) & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "mintues") & ".", Me)
                        objAttempts = Nothing
                        mblnprompt = True
                        btnVerify.Enabled = False
                        LblSeconds.Text = "00:00"
                        LblSeconds.Visible = False
                        blnShowTOTPPopup = False
                        popup_TOTP.Hide()
                        Exit Sub
                    End If
                End If

                If xAttempts >= CInt(Session("MaxTOTPUnSuccessfulAttempts")) Then
                    xAttempts = 1
                    xAttemptsunkid = 0
                Else
                    xAttempts = xAttempts + 1
                End If
            Else
                xAttempts = 1
            End If

            objAttempts._Attemptsunkid = xAttemptsunkid
            objAttempts._Attempts = xAttempts
            objAttempts._Attemptdate = objConfig._CurrentDateAndTime
            objAttempts._Otpretrivaltime = Nothing
            objAttempts._loginemployeeunkid = -1
            objAttempts._Userunkid = CInt(Session("UserId"))
            objAttempts._Ip = Session("IP_ADD").ToString
            objAttempts._Machine_Name = Session("HOST_NAME").ToString
            objAttempts._FormName = mstrModuleName
            objAttempts._Isweb = True

            If xAttemptsunkid <= 0 Then
                If objAttempts.Insert() = False Then
                    DisplayMessage.DisplayMessage(objAttempts._Message, Me)
                    Exit Sub
                End If
            Else
                If xAttempts >= CInt(Session("MaxTOTPUnSuccessfulAttempts")) Then
                    objAttempts._Otpretrivaltime = objConfig._CurrentDateAndTime.AddMinutes(CInt(Session("TOTPRetryAfterMins")))
                    blnShowTOTPPopup = False
                    popup_TOTP.Hide()
                End If
                If objAttempts.Update() = False Then
                    DisplayMessage.DisplayMessage(objAttempts._Message, Me)
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objConfig = Nothing
            objAttempts = Nothing
        End Try
    End Sub

    'Pinkal (16-Nov-2023) -- End

#End Region

#Region " Buttons Methods "

    Protected Sub btnAddAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAttachment.Click
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                Dim arrValidFileExtension() As String = {".PDF", ".JPG", ".JPEG", ".PNG", ".GIF", ".BMP", ".TIF"}
                If arrValidFileExtension.Contains(f.Extension.ToString.ToUpper()) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please select PDF or Image file."), Me)
                    Exit Sub
                End If
                AddDocumentAttachment(f, f.FullName)
                Call FillLoanTrancheRequestAttachment()
            End If
            Session.Remove("Imagepath")
            cboDocumentType.SelectedValue = CStr(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            mintDeleteRowIndex = -1
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If mintDeleteRowIndex >= 0 Then
                mdtLoanTrancheApplicationDocument.Rows(mintDeleteRowIndex)("AUD") = "D"
                mdtLoanTrancheApplicationDocument.AcceptChanges()
                Call FillLoanTrancheRequestAttachment()
                mintDeleteRowIndex = -1
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnReject.Click
        Dim blnFlag As Boolean = False
        Try
            If CType(sender, Button).ID.ToUpper() = btnApprove.ID.ToUpper() Then
                xStatusId = enLoanApplicationStatus.APPROVED
            ElseIf CType(sender, Button).ID.ToUpper() = btnReject.ID.ToUpper() Then
                xStatusId = enLoanApplicationStatus.REJECTED
            End If

            If Validation(xStatusId) = False Then Exit Sub

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.

            If CBool(Session("ApplyTOTPForLoanApplicationApproval")) Then

                '/* START CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP
                Dim objConfig As New clsConfigOptions
                Dim objAttempts As New clsloantotpattempts_tran
                Dim dsAttempts As DataSet = Nothing

                dsAttempts = objAttempts.GetList(objConfig._CurrentDateAndTime.Date, CInt(Session("UserId")), 0)

                Dim xOTPRetrivaltime As DateTime = Nothing
                If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                    If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                        xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))
                        If xOTPRetrivaltime > objConfig._CurrentDateAndTime Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "You cannot approve this loan tranche application.Reason : As You reached Maximum unsuccessful attempts.Please try after") & " " & CInt(Session("TOTPRetryAfterMins")) & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "minutes") & ".", Me)
                            objConfig = Nothing
                            objAttempts = Nothing
                            Exit Sub
                        End If
                    End If
                End If
                objConfig = Nothing
                objAttempts = Nothing
                '/* END CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP


                If Session("SMSGatewayEmail").ToString().Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan tranche approval."), Me)
                    Exit Sub
                End If

                Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(Session("UserId"))
                txtVerifyOTP.Text = ""
                txtVerifyOTP.ReadOnly = False
                btnVerify.Enabled = True
                btnSendCodeAgain.Enabled = False
                LblSeconds.Visible = True
                btnTOTPClose.Enabled = False

                If objUser._TOTPSecurityKey.Trim.Length > 0 Then
                    mstrSecretKey = objUser._TOTPSecurityKey
                Else
                    mstrSecretKey = objTOTPHelper.GenerateSecretKey()
                    objUser._TOTPSecurityKey = mstrSecretKey
                    If objUser.Update(False) = False Then
                        DisplayMessage.DisplayMessage(objUser._Message, Me)
                        Exit Sub
                    End If
                End If

                If objUser._EmployeeUnkid > 0 Then

                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objUser._EmployeeUnkid

                    mstrEmployeeName = objEmployee._Firstname + " " + objEmployee._Surname
                    mstrEmployeeEmail = objEmployee._Email

                    If CInt(Session("SMSGatewayEmailType")) = CInt(clsEmployee_Master.EmpColEnum.Col_Present_Mobile) Then
                        mstrEmpMobileNo = objEmployee._Present_Mobile.ToString()
                    ElseIf CInt(Session("SMSGatewayEmailType")) = CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Mobile) Then
                        mstrEmpMobileNo = objEmployee._Domicile_Mobile.ToString()
                    End If
                    objEmployee = Nothing

                Else
                    mstrEmployeeName = objUser._Firstname + " " + objUser._Lastname
                    mstrEmployeeEmail = objUser._Email
                    mstrEmpMobileNo = objUser._Phone

                End If

                objUser = Nothing

                If mstrEmpMobileNo.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Mobile No is compulsory information. Please set Approver's Mobile No to send TOTP."), Me)
                    Exit Sub
                End If

                Counter = objTOTPHelper._TimeStepSeconds

                Dim mstrTOTPCode As String = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
                Dim mstrError As String = SendOTP(mstrTOTPCode)
                If mstrError.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    blnShowTOTPPopup = False
                    popup_TOTP.Hide()
                    Exit Sub
                End If

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:countdown(0," & Counter & "); ", True)
                blnShowTOTPPopup = True
                popup_TOTP.Show()

            Else
                SaveApproval(xStatusId)
            End If

            'Pinkal (16-Nov-2023) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            mintLoanTrancheApprovalId = 0
            mintLoanTrancheRequestId = 0
            mintMapUserId = 0

            Session("loantrancheapprovalunkid") = Nothing
            Session.Remove("loantrancheapprovalunkid")

            Session("loantrancherequestunkid") = Nothing
            Session.Remove("loantrancherequestunkid")

            Session("Mapuserunkid") = Nothing
            Session.Remove("Mapuserunkid")

            If mdtLoanTrancheApplicationDocument IsNot Nothing Then mdtLoanTrancheApplicationDocument.Clear()
            mdtLoanTrancheApplicationDocument = Nothing

            Session("LoanTrancheApplicationDocument") = Nothing
            Session.Remove("LoanTrancheApplicationDocument")

            Session("LoanTrancheRequestId") = Nothing
            Session.Remove("LoanTrancheRequestId")

            If Me.Request.QueryString.Count > 0 Then

                'Pinkal (23-Feb-2024) -- Start
                '(A1X-2461) NMB : R&D - Force manual user login on approval links..

                Session.Clear()
                Session.Abandon()
                Session.RemoveAll()

                If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                    Response.Cookies("ASP.NET_SessionId").Value = ""
                    Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                    Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
                End If

                If Request.Cookies("AuthToken") IsNot Nothing Then
                    Response.Cookies("AuthToken").Value = ""
                    Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                End If

                'Response.Redirect(Session("rootpath").ToString() & "index.aspx", False)

                Response.Redirect("~/Index.aspx", False)

                'Pinkal (23-Feb-2024) -- End

            Else
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Tranche_Approval_Application/wPg_LoanTrancheApprovalList.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (16-Nov-2023) -- Start
    '(A1X-1489) NMB - Mortgage tranche disbursement API.

    Protected Sub btnVerify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerify.Click
        Try
            If txtVerifyOTP.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmOTPValidator", 1, "OTP is compulsory information.Please Enter OTP."), Me)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                txtVerifyOTP.Focus()
                popup_TOTP.Show()
                Exit Sub
            End If
            Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))
            If objTOTPHelper.ValidateTotpCode(mstrSecretKey, txtVerifyOTP.Text.Trim) = False Then
                Dim mblnprompt As Boolean = False
                InsertAttempts(mblnprompt)
                If mblnprompt = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmOTPValidator", 2, "Invalid OTP Code. Please try again."), Me)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                    objTOTPHelper = Nothing
                    Exit Sub
                End If
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:VerifyOnClientClick(); ", True)
                blnShowTOTPPopup = False
                popup_TOTP.Hide()
                SaveApproval(xStatusId)
            End If
            objTOTPHelper = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub btnSendCodeAgain_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendCodeAgain.Click
        Try
            btnSendCodeAgain.Enabled = False
            btnTOTPClose.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True

            Dim objTOTPHelper As New clsTOTPHelper(CInt(Session("CompanyUnkId")))
            Dim mstrTOTPCode As String = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
            Counter = objTOTPHelper._TimeStepSeconds
            Dim mstrError As String = SendOTP(mstrTOTPCode)
            If mstrError.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(mstrError, Me)
                blnShowTOTPPopup = False
                popup_TOTP.Hide()
                Exit Sub
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Javascript", "javascript:countdown(0," & Counter & "); ", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_TOTP.Show()
        End Try
    End Sub

    Protected Sub btnTOTPClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTOTPClose.Click
        Try
            btnSendCodeAgain.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True
            blnShowTOTPPopup = False
            popup_TOTP.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (16-Nov-2023) -- End


#End Region

#Region "Dropdown Events"

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim objLoanApplication As New clsloanTranche_request_Tran
        Try
            Dim dtTable As DataTable = objLoanApplication.GetMortgageLoanApplicationNo(CInt(cboEmployee.SelectedValue), True)
            With cboLoanApplicationNo
                .DataValueField = "processpendingloanunkid"
                .DataTextField = "application_no"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanApplication = Nothing
        End Try
    End Sub

    Protected Sub cboLoanApplicationNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanApplicationNo.SelectedIndexChanged
        Dim objLoanApplication As New clsloanTranche_request_Tran
        Dim mintFinalLoanApplicationApproveId As Integer = 0
        Try
            If CInt(cboLoanApplicationNo.SelectedValue) <= 0 Then
                cboLoanscheme.SelectedValue = "0"
                Exit Sub
            End If

            Dim dtTable As DataTable = objLoanApplication.GetMortgageLoanApplicationNo(CInt(cboEmployee.SelectedValue), False, "lnloan_process_pending_loan.processpendingloanunkid = " & CInt(cboLoanApplicationNo.SelectedValue))
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim xLoanSchemeId As Integer = dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("loanschemeunkid")).First()
                cboLoanscheme.SelectedValue = xLoanSchemeId.ToString()
                cboLoanscheme_SelectedIndexChanged(cboLoanscheme, New EventArgs())
            End If
            Dim objLoan As New clsProcess_pending_loan

            objLoan._Processpendingloanunkid = CInt(cboLoanApplicationNo.SelectedValue)
            mintFinalLoanApplicationApproveId = objLoan._Approverunkid

            TxtLoanApplicationNo.Text = objLoan._Application_No
            dtpApplicationDate.SetDate = objLoan._Application_Date.Date
            TxtLoanscheme.Text = cboLoanscheme.SelectedItem.Text
            TxtLoanEmployee.Text = cboEmployee.SelectedItem.Text
            TxtApprovedLoanAmt.Text = Format(objLoan._Approved_Amount, Session("fmtcurrency").ToString())

            Dim mstrFilter As String = "ln.processpendingloanunkid = " & objLoan._Processpendingloanunkid & " AND lna.mapuserunkid = " & objLoan._Approverunkid
            Dim objLoanApproval As New clsroleloanapproval_process_Tran
            Dim dsList As DataSet = objLoanApproval.GetRoleBasedApprovalList(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                             , dtpApplicationDate.GetDate.Date, dtpApplicationDate.GetDate.Date, Session("UserAccessModeSetting").ToString() _
                                                                                                             , True, False, "List", mstrFilter, "", False, Nothing)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                TxtFirstTranche.Text = Format(CDec(dsList.Tables(0).Rows(0)("first_tranche")), Session("fmtcurrency").ToString())
                TxtSecondTranche.Text = Format(CDec(dsList.Tables(0).Rows(0)("second_tranche")), Session("fmtcurrency").ToString())
                txtThirdTranche.Text = Format(CDec(dsList.Tables(0).Rows(0)("third_tranche")), Session("fmtcurrency").ToString())
                TxtFourthTranche.Text = Format(CDec(dsList.Tables(0).Rows(0)("fourth_tranche")), Session("fmtcurrency").ToString())
                TxtFifthTranche.Text = Format(CDec(dsList.Tables(0).Rows(0)("fifth_tranche")), Session("fmtcurrency").ToString())

                If IsDBNull(dsList.Tables(0).Rows(0)("first_tranchedate")) = False Then
                    dtpFirstTrancheDate.SetDate = CDate(dsList.Tables(0).Rows(0)("first_tranchedate")).Date
                End If
                If IsDBNull(dsList.Tables(0).Rows(0)("second_tranchedate")) = False Then
                    dtpSecondTrancheDate.SetDate = CDate(dsList.Tables(0).Rows(0)("second_tranchedate")).Date
                End If
                If IsDBNull(dsList.Tables(0).Rows(0)("third_tranchedate")) = False Then
                    dtpThirdTrancheDate.SetDate = CDate(dsList.Tables(0).Rows(0)("third_tranchedate")).Date
                End If
                If IsDBNull(dsList.Tables(0).Rows(0)("fourth_tranchedate")) = False Then
                    dtpFourthTranchDate.SetDate = CDate(dsList.Tables(0).Rows(0)("fourth_tranchedate")).Date
                End If
                If IsDBNull(dsList.Tables(0).Rows(0)("fifth_tranchedate")) = False Then
                    dtpFifthTrancheDate.SetDate = CDate(dsList.Tables(0).Rows(0)("fifth_tranchedate")).Date
                End If
            End If

            Dim mintTrancheNo As Integer = 1
            Dim dtTotalTranche As DataTable = objLoanApplication.GetLoanTracheNo(CInt(cboLoanApplicationNo.SelectedValue))

            If mintLoanTrancheRequestId > 0 Then
                mintTrancheNo = dtTotalTranche.Rows.Count
            Else
                mintTrancheNo = dtTotalTranche.Rows.Count + 1
            End If

            If (mintTrancheNo = 1 AndAlso dtpSecondTrancheDate.IsNull) OrElse (mintTrancheNo = 2 AndAlso dtpThirdTrancheDate.IsNull) _
                     OrElse (mintTrancheNo = 3 AndAlso dtpFourthTranchDate.IsNull) OrElse (mintTrancheNo = 4 AndAlso dtpFifthTrancheDate.IsNull) Then

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                'mblnValidTrancheDate = True
                'Pinkal (17-May-2024) -- End

            ElseIf mintTrancheNo = 1 AndAlso (dtpFirstTrancheDate.IsNull = False AndAlso dtpTrancheDate.GetDate.Date >= dtpFirstTrancheDate.GetDate.Date) Then
                'mintTrancheNo += 1

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                'mblnValidTrancheDate = True
                'Pinkal (17-May-2024) -- End

            ElseIf mintTrancheNo = 2 AndAlso (dtpSecondTrancheDate.IsNull = False AndAlso dtpTrancheDate.GetDate.Date >= dtpSecondTrancheDate.GetDate.Date) Then
                'mintTrancheNo += 1

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                'mblnValidTrancheDate = True
                'Pinkal (17-May-2024) -- End

            ElseIf mintTrancheNo = 3 AndAlso (dtpThirdTrancheDate.IsNull = False AndAlso dtpTrancheDate.GetDate.Date >= dtpThirdTrancheDate.GetDate.Date) Then
                'mintTrancheNo += 1
                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                'mblnValidTrancheDate = True
                'Pinkal (17-May-2024) -- End

            ElseIf mintTrancheNo = 4 AndAlso (dtpFourthTranchDate.IsNull = False AndAlso dtpTrancheDate.GetDate.Date >= dtpFourthTranchDate.GetDate.Date) Then
                'mintTrancheNo += 1
                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                'mblnValidTrancheDate = True
                'Pinkal (17-May-2024) -- End

            ElseIf mintTrancheNo = 5 AndAlso dtpFifthTrancheDate.IsNull = False AndAlso dtpTrancheDate.GetDate.Date >= dtpFifthTrancheDate.GetDate.Date Then
                mintTrancheNo += 1
                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                ' mblnValidTrancheDate = True
                'Pinkal (17-May-2024) -- End

            ElseIf mintTrancheNo > 5 Then
                mintTrancheNo += 1
                mblnValidTrancheDate = False
            End If

            Dim dtTrancheAmount As DataTable = objLoanApplication.GetLoanTracheAmount(CInt(cboLoanApplicationNo.SelectedValue), mintFinalLoanApplicationApproveId, mintTrancheNo)
            If dtTrancheAmount IsNot Nothing AndAlso dtTrancheAmount.Rows.Count > 0 Then

                If mintTrancheNo = 1 Then
                    mdecOriginalTrancheAmount = CDec(dtTrancheAmount.Rows(0)("first_tranche"))
                ElseIf mintTrancheNo = 2 Then
                    mdecOriginalTrancheAmount = CDec(dtTrancheAmount.Rows(0)("second_tranche"))
                ElseIf mintTrancheNo = 3 Then
                    mdecOriginalTrancheAmount = CDec(dtTrancheAmount.Rows(0)("third_tranche"))
                ElseIf mintTrancheNo = 4 Then
                    mdecOriginalTrancheAmount = CDec(dtTrancheAmount.Rows(0)("fourth_tranche"))
                ElseIf mintTrancheNo = 5 Then
                    mdecOriginalTrancheAmount = CDec(dtTrancheAmount.Rows(0)("fifth_tranche"))
                Else
                    mdecOriginalTrancheAmount = 0
                End If
                If mintLoanTrancheRequestId <= 0 Then
                    TxtTrancheAmount.Text = Format(mdecOriginalTrancheAmount, Session("fmtcurrency").ToString())
                End If
            End If
            dtTrancheAmount.Clear()
            dtTrancheAmount = Nothing

            objLoanApproval = Nothing
            objLoan = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanApplication = Nothing
        End Try
    End Sub

    Protected Sub cboLoanscheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanscheme.SelectedIndexChanged
        Dim mstrDocumentTypeIds As String = ""
        Try
            If CInt(cboLoanscheme.SelectedValue) > 0 Then
                Dim objLoanScheme As New clsLoan_Scheme
                objLoanScheme._Loanschemeunkid = CInt(cboLoanscheme.SelectedValue)
                mstrDocumentTypeIds = objLoanScheme._LoanTrancheDocumentTypeIDs
                objLoanScheme = Nothing
            End If
            FillDocumentTypeCombo(mstrDocumentTypeIds)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExRate.Text = ""
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, mdtPeriodEnd, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, Session("fmtCurrency").ToString()) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboDeductionPeriod_SelectedIndexChanged()
        Try
            Call SetDateFormat()

            Dim objPeriod As New clscommom_period_Tran
            objvalPeriodDuration.Visible = True
            If mintDeductionPeriodUnkId > 0 Then
                objPeriod._Periodunkid(Session("Database_Name").ToString) = mintDeductionPeriodUnkId
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
                objvalPeriodDuration.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Period Duration From :") & " " & objPeriod._Start_Date.Date & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "To") & " " & objPeriod._End_Date.Date
                objPeriod = Nothing
            Else
                objvalPeriodDuration.Text = ""
                objvalPeriodDuration.Visible = False
                mdtPeriodStart = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
                mdtPeriodEnd = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Gridview Events "

    Protected Sub dgAttachment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAttachment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then

                Dim xrow() As DataRow
                If CInt(e.Item.Cells(getColumnId_Datagrid(dgAttachment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtLoanTrancheApplicationDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(getColumnId_Datagrid(dgAttachment, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtLoanTrancheApplicationDocument.Select("GUID = '" & e.Item.Cells(getColumnId_Datagrid(dgAttachment, "objcolhGUID", False, True)).Text.Trim & "'")
                End If

                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Are you sure you want to delete this attachment?")
                        popup_YesNo.Show()
                        mintDeleteRowIndex = mdtLoanTrancheApplicationDocument.Rows.IndexOf(xrow(0))
                    End If

                ElseIf e.CommandName = "imgdownload" Then
                    Dim xPath As String = ""
                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("filepath").ToString
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If
                    If xPath.Trim <> "" Then
                        Dim fileInfo As New IO.FileInfo(xPath)
                        If fileInfo.Exists = False Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "File does not Exis."), Me)
                            Exit Sub
                        End If
                        fileInfo = Nothing
                        Dim strFile As String = xPath
                        Response.ContentType = "image/jpg/pdf"
                        Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(getColumnId_Datagrid(dgAttachment, "colhName", False, True)).Text & """")
                        Response.Clear()
                        Response.TransmitFile(strFile)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Linkbutton Events"

    Protected Sub lnkLoanDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLoanDetails.Click
        Try
            If CInt(cboLoanApplicationNo.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 1, "Loan Application is compulsory information.Please Select Loan Application."), Me)
                cboLoanApplicationNo.Focus()
                Exit Sub
            End If
            popupLoanDetails.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblTrancheApplicationNo.ID, Me.LblTrancheApplicationNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblTrancheDate.ID, Me.LblTrancheDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblEmployee.ID, Me.LblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanScheme.ID, Me.LblLoanScheme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblRemark.ID, Me.LblRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblApprovalRemark.ID, Me.LblApprovalRemark.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkLoanDetails.ID, Me.lnkLoanDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblTrancheAmount.ID, Me.LblTrancheAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnApprove.ID, Me.btnApprove.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReject.ID, Me.btnReject.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanDetails.ID, Me.LblLoanDetails.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblApplicationDate.ID, Me.LblApplicationDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanEmployee.ID, Me.LblLoanEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblLoanschme.ID, Me.LblLoanschme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblApprovedLoanAmt.ID, Me.LblApprovedLoanAmt.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFirstTranche.ID, Me.LblFirstTranche.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblSecodTranche.ID, Me.LblSecodTranche.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblThirdTranche.ID, Me.LblThirdTranche.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFourthTranche.ID, Me.LblFourthTranche.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFifthTranche.ID, Me.LblFifthTranche.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFirstTrancheDate.ID, Me.LblFirstTrancheDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblSecondTrancheDate.ID, Me.LblSecondTrancheDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblThirdTrancheDate.ID, Me.LblThirdTrancheDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFourthTrancheDate.ID, Me.LblFourthTrancheDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblFifthTrancheDate.ID, Me.LblFifthTrancheDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnLoanDetailClose.ID, Me.btnLoanDetailClose.Text)

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblVerifyOTP.ID, Me.lblVerifyOTP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.LblOTP.ID, Me.LblOTP.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSendCodeAgain.ID, Me.btnSendCodeAgain.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnVerify.ID, Me.btnVerify.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnTOTPClose.ID, Me.btnTOTPClose.Text)
            'Pinkal (16-Nov-2023) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.LblTrancheApplicationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblTrancheApplicationNo.ID, Me.LblTrancheApplicationNo.Text)
            Me.LblTrancheDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblTrancheDate.ID, Me.LblTrancheDate.Text)
            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.LblLoanApplicationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)
            Me.LblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanScheme.ID, Me.LblLoanScheme.Text)
            Me.LblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblRemark.ID, Me.LblRemark.Text)
            Me.LblApprovalRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblApprovalRemark.ID, Me.LblApprovalRemark.Text)
            Me.lnkLoanDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkLoanDetails.ID, Me.lnkLoanDetails.Text)
            Me.LblTrancheAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblTrancheAmount.ID, Me.LblTrancheAmount.Text)
            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnApprove.ID, Me.btnApprove.Text)
            Me.btnReject.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReject.ID, Me.btnReject.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text)

            Me.LblLoanDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanDetails.ID, Me.LblLoanDetails.Text)
            Me.LblLoanApplicationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanApplicationNo.ID, Me.LblLoanApplicationNo.Text)
            Me.LblApplicationDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblApplicationDate.ID, Me.LblApplicationDate.Text)
            Me.LblLoanEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanEmployee.ID, Me.LblLoanEmployee.Text)
            Me.LblLoanschme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLoanschme.ID, Me.LblLoanschme.Text)
            Me.LblApprovedLoanAmt.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblApprovedLoanAmt.ID, Me.LblApprovedLoanAmt.Text)
            Me.LblFirstTranche.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFirstTranche.ID, Me.LblFirstTranche.Text)
            Me.LblSecodTranche.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblSecodTranche.ID, Me.LblSecodTranche.Text)
            Me.LblThirdTranche.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblThirdTranche.ID, Me.LblThirdTranche.Text)
            Me.LblFourthTranche.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFourthTranche.ID, Me.LblFourthTranche.Text)
            Me.LblFifthTranche.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFifthTranche.ID, Me.LblFifthTranche.Text)
            Me.LblFirstTrancheDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFirstTrancheDate.ID, Me.LblFirstTrancheDate.Text)
            Me.LblSecondTrancheDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblSecondTrancheDate.ID, Me.LblSecondTrancheDate.Text)
            Me.LblThirdTrancheDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblThirdTrancheDate.ID, Me.LblThirdTrancheDate.Text)
            Me.LblFourthTrancheDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFourthTrancheDate.ID, Me.LblFourthTrancheDate.Text)
            Me.LblFifthTrancheDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblFifthTrancheDate.ID, Me.LblFifthTrancheDate.Text)
            Me.btnLoanDetailClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnLoanDetailClose.ID, Me.btnLoanDetailClose.Text)

            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            Me.lblVerifyOTP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVerifyOTP.ID, Me.lblVerifyOTP.Text)
            Me.LblOTP.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblOTP.ID, Me.LblOTP.Text)
            Me.btnSendCodeAgain.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSendCodeAgain.ID, Me.btnSendCodeAgain.Text)
            Me.btnVerify.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnVerify.ID, Me.btnVerify.Text)
            Me.btnTOTPClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnTOTPClose.ID, Me.btnTOTPClose.Text)
            'Pinkal (16-Nov-2023) -- End

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub


#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Loan Application is compulsory information.Please Select Loan Application No.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Loan Tranche Amount should be greater than 0.Please enter valid Loan Tranche Amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "You can not apply for this Loan Tranche application.Reason : You have to wait for the eligible date for the next tranche.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Selected information is already present for particular employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Loan Tranche application updated successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Loan Tranche application applied successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Loan Trache Amount should be less than or equal to")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "File does not exist on localpath.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Period Duration From :")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "To")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Are you sure you want to delete this attachment ? ")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "File does not exist.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Loan Tranche Application")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Remark cannot be blank.Remark is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "You can't Edit this Loan tranche detail. Reason: This Loan tranche is already rejected.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "You can't Edit this Loan tranche detail. Reason: This Loan tranche is already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Sorry, there is no pending loan tranche application data.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "You can't Edit this Loan tranche detail. Reason: This Loan tranche is already approved/reject.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "and disbursed")


            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "You cannot approve this loan tranche application.Reason :  As You reached Maximum unsuccessful attempts.Please try after")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, "mintues")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 23, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan tranche approval.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 24, "Mobile No is compulsory information. Please set Approver's Mobile No to send TOTP.")
            'Pinkal (16-Nov-2023) -- End

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings

End Class
