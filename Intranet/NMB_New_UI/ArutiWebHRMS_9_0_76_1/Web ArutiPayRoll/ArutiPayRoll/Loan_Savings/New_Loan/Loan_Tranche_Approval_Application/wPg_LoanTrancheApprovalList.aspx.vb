﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Globalization
#End Region

Partial Class Loan_Savings_New_Loan_Loan_Tranche_Approval_Application_wPg_LoanTrancheApprovalList
    Inherits Basepage

#Region "Private Variables"

    Private Shared ReadOnly mstrModuleName As String = "frmLoanTrancheApprovalList"
    Dim DisplayMessage As New CommonCodes
    Private objLoanTrancheApproval As New clsloanTranche_approval_Tran
    Private mstrAdvanceFilter As String = ""
    Private dtApprovalList As New DataTable
    Private mstrEmployeeIDs As String = ""

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If Session("UserId") Is Nothing OrElse CInt(Session("UserId")) <= 0 Then
                DisplayMessage.DisplayMessage("you are not authorized to access this page.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                SetMessages()
                Call SetLanguage()
                Call FillCombo()

                chkMyApprovals.Checked = True
                chkMyApprovals.Visible = False

                If dgLoanTrancheApproval.Items.Count <= 0 Then
                    dgLoanTrancheApproval.DataSource = New List(Of String)
                    dgLoanTrancheApproval.DataBind()
                End If

                dgLoanTrancheApproval.Columns(0).Visible = CBool(Session("AllowToApproveLoanTrancheApplication"))
            Else
                mstrEmployeeIDs = CStr(Me.ViewState("EmployeeIDs"))
                mstrAdvanceFilter = CStr(Me.ViewState("AdvanceFilter"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            Dim objEmployee As New clsEmployee_Master
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objLoanApplication As New clsProcess_pending_loan
            Dim objMasterData As New clsMasterData
         
            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                       CInt(Session("UserId")), _
                                       CInt(Session("Fin_year")), _
                                       CInt(Session("CompanyUnkId")), _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                       Session("UserAccessModeSetting").ToString(), True, _
                                       False, "List", True)

            With cboEmployee
                .DataTextField = "EmpCodeName"
                .DataValueField = "employeeunkid"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim lstIDs As List(Of String) = (From p In dsList.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
            mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))

            Dim mblnSchemeShowOnEss As Boolean = False
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then mblnSchemeShowOnEss = True
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "loanschemecategory_id = " & enLoanSchemeCategories.MORTGAGE & " AND  ISNULL(ispostingtoflexcube,0) = 1 ", mblnSchemeShowOnEss)

            With cboLoanScheme
                .DataTextField = "name"
                .DataValueField = "loanschemeunkid"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objLoanApplication.GetLoan_Status("Status", True, False)
            With cboStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsList.Tables("Status")
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboApproverStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsList.Tables("Status").Copy()
                .DataBind()
                .SelectedValue = "1"
            End With

            dsList = objMasterData.GetCondition(False, True, True, False, False)
            Dim dtCondition As DataTable = New DataView(dsList.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable
            With cboAmountCondition
                .DataTextField = "name"
                .DataValueField = "id"
                .DataSource = dtCondition
                .DataBind()
                .SelectedValue = "0"
            End With

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(CInt(Session("UserId")))
                txtApprover.Text = objUser._Username.ToString()
                objUser = Nothing
            Else
                lblApprover.Visible = False
                txtApprover.Visible = False
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim strSearch As String = ""
            Dim dsList As DataSet = Nothing

            If CBool(Session("AllowToViewLoanTrancheApprovalList")) = False Then Exit Sub

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND ltr.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                strSearch &= "AND ltr.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearch &= "AND ltr.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            If CInt(cboApproverStatus.SelectedValue) > 0 Then
                strSearch &= "AND lta.statusunkid = " & CInt(cboApproverStatus.SelectedValue) & " AND lta.visibleunkid = " & CInt(cboApproverStatus.SelectedValue) & " "
            Else
                strSearch &= "AND lta.visibleunkid <> -1 " & " "
            End If

            If txtLoanAmount.Text.Trim <> "" AndAlso CDec(txtLoanAmount.Text) > 0 Then
                strSearch &= "AND ltr.trancheamt " & cboAmountCondition.SelectedItem.Text & " " & CDec(txtLoanAmount.Text) & " "
            End If

            If dtpFromDate.IsNull = False Then
                strSearch &= "AND lnr.tranchedate >= '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date) & "' "
            End If

            If dtpToDate.IsNull = False Then
                strSearch &= "AND lnr.tranchedate >= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            End If


            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsList = objLoanTrancheApproval.GetLoanTrancheApprovalList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                                     CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), _
                                                                                                     "List", strSearch, mstrAdvanceFilter, True, Nothing)


            Dim dtTable As New DataTable
            If chkMyApprovals.Checked Then
                dtTable = New DataView(dsList.Tables(0), "mapuserunkid = " & CInt(Session("UserId")) & " OR mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
            Else
                If mstrEmployeeIDs.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables(0), "employeeunkid  in (" & mstrEmployeeIDs & ") OR mapuserunkid <= 0", "", DataViewRowState.CurrentRows).ToTable
                End If
            End If


            Dim mintLoanTrancheRequestId As Integer = 0
            Dim dList As DataTable = Nothing
            Dim mstrStaus As String = ""

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo

                For Each drRow As DataRow In dtTable.Rows

                    If CInt(drRow("loantrancheapprovalunkid")) <= 0 Then Continue For
                    mstrStaus = ""

                    If mintLoanTrancheRequestId <> CInt(drRow("loantrancherequestunkid")) Then
                        dList = New DataView(dtTable, "employeeunkid = " & CInt(drRow("employeeunkid")) & " AND loantrancherequestunkid = " & CInt(drRow("loantrancherequestunkid").ToString()), "", DataViewRowState.CurrentRows).ToTable
                        mintLoanTrancheRequestId = CInt(drRow("loantrancherequestunkid"))
                    End If

                    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                        Dim dr As DataRow() = dList.Select("priority >= " & CInt(drRow("priority")))

                        If dr.Length > 0 Then

                            For i As Integer = 0 To dr.Length - 1

                                If CInt(drRow("statusunkid")) = enLoanApplicationStatus.APPROVED Then
                                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Approved By :-") & " " & info1.ToTitleCase(drRow("loginuser").ToString())
                                    Exit For

                                ElseIf CInt(drRow("statusunkid")) = enLoanApplicationStatus.PENDING Then

                                    If CInt(dr(i)("statusunkid")) = enLoanApplicationStatus.APPROVED Then
                                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Approved By :-") & " " & info1.ToTitleCase(dr(i)("loginuser").ToString())
                                        Exit For

                                    ElseIf CInt(dr(i)("statusunkid")) = enLoanApplicationStatus.REJECTED Then
                                        mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Rejected By :-") & " " & info1.ToTitleCase(dr(i)("loginuser").ToString())
                                        Exit For

                                    End If

                                ElseIf CInt(drRow("statusunkid")) = enLoanApplicationStatus.REJECTED Then
                                    mstrStaus = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Rejected By :-") & " " & info1.ToTitleCase(drRow("loginuser").ToString())
                                    Exit For

                                End If

                            Next

                        End If

                    End If

                    If mstrStaus <> "" Then
                        drRow("status") = mstrStaus.Trim
                    End If

                Next

            End If

            'dtApprovalList = dtTable.Copy()

            Dim dtTemp As DataTable = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable(True, "loantrancherequestunkid", "employeeunkid")

            Dim dRow1 = From drTable In dtTable Group Join drTemp In dtTemp On drTable.Field(Of Integer)("loantrancherequestunkid") Equals drTemp.Field(Of Integer)("loantrancherequestunkid") And _
                         drTable.Field(Of Integer)("employeeunkid") Equals drTemp.Field(Of Integer)("employeeunkid") Into Grp = Group Select drTable


            If dRow1.Count > 0 Then
                dRow1.ToList.ForEach(Function(x) DeleteRow(x, dtTable))
            End If

            dgLoanTrancheApproval.AutoGenerateColumns = False
            dgLoanTrancheApproval.DataSource = dtTable
            dgLoanTrancheApproval.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function DeleteRow(ByVal dr As DataRow, ByVal dtTable As DataTable) As Boolean
        Try
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drRow() As DataRow = dtTable.Select("loantrancherequestunkid = " & CInt(dr("loantrancherequestunkid").ToString()) & " AND employeeunkid = " & CInt(dr("employeeunkid")))
                If drRow.Length <= 1 Then
                    dtTable.Rows.Remove(drRow(0))
                    dtTable.AcceptChanges()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return True
    End Function

#End Region

#Region "Button's Events"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            dgLoanTrancheApproval.CurrentPageIndex = 0
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            dgLoanTrancheApproval.DataSource = New List(Of String)
            dgLoanTrancheApproval.DataBind()

            cboEmployee.SelectedValue = "0"
            cboLoanScheme.SelectedValue = "0"
            cboStatus.SelectedValue = "1"
            cboAmountCondition.SelectedIndex = 0

            txtLoanAmount.Text = "0"
            mstrAdvanceFilter = ""

            dtpFromDate.SetDate = Nothing
            dtpToDate.SetDate = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Link Button's Events"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DataGrid Events"

    Protected Sub dgLoanTrancheApproval_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgLoanTrancheApproval.ItemCommand
        Dim objLoanTrancheApproval As New clsloanTranche_approval_Tran
        Try
            If e.CommandName.ToUpper = "STATUS" Then

                If CInt(Session("UserId")) <> CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhUserID", False, True)).Text) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "You can't Edit this Loan detail. Reason: You are logged in into another user login."), Me)
                    Exit Sub
                End If


                objLoanTrancheApproval._Loantrancheapprovalunkid = CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhloantrancheapprovalunkid", False, True)).Text)

                Dim dsList As DataSet
                Dim dtList As DataTable

                Dim mstrSearch As String = "lta.loantrancherequestunkid = " & CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhloantrancherequestunkid", False, True)).Text) & " AND lta.loantrancheapprovalunkid <> " & CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhloantrancheapprovalunkid", False, True)).Text)

                dsList = objLoanTrancheApproval.GetLoanTrancheApprovalList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", mstrSearch, "", False, Nothing)

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                        If objLoanTrancheApproval._Priority > CInt(dsList.Tables(0).Rows(i)("Priority")) Then
                            dtList = New DataView(dsList.Tables(0), "levelunkid = " & CInt(dsList.Tables(0).Rows(i)("levelunkid")) & " AND statusunkid = " & enLoanApplicationStatus.APPROVED, "", DataViewRowState.CurrentRows).ToTable
                            If dtList.Rows.Count > 0 Then Continue For

                            If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = enLoanApplicationStatus.APPROVED Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                                Exit Sub
                            ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = enLoanApplicationStatus.REJECTED Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                                Exit Sub
                            End If

                        ElseIf objLoanTrancheApproval._Priority <= CInt(dsList.Tables(0).Rows(i)("Priority")) Then

                            If CInt(dsList.Tables(0).Rows(i)("statusunkid")) = enLoanApplicationStatus.APPROVED Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                                Exit Sub
                            ElseIf CInt(dsList.Tables(0).Rows(i)("statusunkid")) = enLoanApplicationStatus.REJECTED Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                                Exit Sub
                            End If
                        End If
                    Next
                End If

                If CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhStatusunkid", False, True)).Text) = enLoanApplicationStatus.APPROVED Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                    Exit Sub
                End If

                If CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhLoanTrancheStatusID", False, True)).Text) = enLoanApplicationStatus.APPROVED Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me)
                    Exit Sub
                ElseIf CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhLoanTrancheStatusID", False, True)).Text) = enLoanApplicationStatus.REJECTED Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me)
                    Exit Sub
                End If


                Session("loantrancheapprovalunkid") = CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhloantrancheapprovalunkid", False, True)).Text)
                Session("loantrancherequestunkid") = CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhloantrancherequestunkid", False, True)).Text)
                Session("Mapuserunkid") = CInt(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhUserID", False, True)).Text)

                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Tranche_Approval_Application/wPg_LoanTrancheApproval.aspx", False)

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanTrancheApproval = Nothing
        End Try
    End Sub

    Protected Sub dgLoanTrancheApproval_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgLoanTrancheApproval.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then
                SetDateFormat()
                e.Item.Font.Bold = True
            End If

            If e.Item.ItemIndex >= 0 Then

                If CBool(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhIsGrp", False, True)).Text) = True Then
                    CType(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhChangeStatus", False, True)).FindControl("lnkChangeStatus"), LinkButton).Visible = False

                    e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "objdgcolhChangeStatus", False, True)).Visible = False
                    e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).ColumnSpan = e.Item.Cells.Count - 8
                    e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).Text = e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationNo", False, True)).Text
                    e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).Font.Bold = True

                    For i = 3 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next

                    e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).CssClass = "group-header"
                    e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).Style.Add("text-align", "left")

                Else
                    If e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).Text <> "&nbsp;" Then
                        e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).Text = CDate(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).Text).Date.ToShortDateString
                    End If

                    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                    If e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhUser", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhUser", False, True)).Text <> "&nbsp;" Then
                        e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhUser", False, True)).Text = info1.ToTitleCase(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhUser", False, True)).Text)
                    End If
                    info1 = Nothing

                    If e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLnAdvAmount", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLnAdvAmount", False, True)).Text <> "&nbsp;" Then
                        e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLnAdvAmount", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLnAdvAmount", False, True)).Text), Session("fmtCurrency").ToString)
                    End If

                    If e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApprovedAmount", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApprovedAmount", False, True)).Text <> "&nbsp;" Then
                        e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApprovedAmount", False, True)).Text = Format(CDec(e.Item.Cells(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApprovedAmount", False, True)).Text), Session("fmtCurrency").ToString)
                    End If

                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgLoanTrancheApproval_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgLoanTrancheApproval.PageIndexChanged
        Try
            dgLoanTrancheApproval.CurrentPageIndex = e.NewPageIndex
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "CheckBox Events"

    Protected Sub chkMyApprovals_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMyApprovals.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.Title)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, mstrModuleName, Me.lblPageHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, "gbFilterCriteria", Me.lblDetialHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblEmployee.ID, Me.lblEmployee.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblFromDate.ID, Me.lblFromDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblStatus.ID, Me.lblStatus.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblToDate.ID, Me.lblToDate.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblApprover.ID, Me.lblApprover.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.lblLoanTrancheAmount.ID, Me.lblLoanTrancheAmount.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.chkMyApprovals.ID, Me.chkMyApprovals.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnReset.ID, Me.btnReset.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.btnClose.ID, Me.btnClose.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationNo", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationNo", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhEmployee", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhEmployee", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhUser", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhUser", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLoanScheme", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLoanScheme", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLnAdvAmount", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLnAdvAmount", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApprovedAmount", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApprovedAmount", False, True)).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhStatus", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhStatus", False, True)).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lnkAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblFromDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblLoanScheme.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToDate.ID, Me.lblToDate.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblLoanTrancheAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLoanTrancheAmount.ID, Me.lblLoanTrancheAmount.Text)
            Me.chkMyApprovals.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkMyApprovals.ID, Me.chkMyApprovals.Text)
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationNo", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationNo", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationNo", False, True)).HeaderText)
            Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApplicationDate", False, True)).HeaderText)
            Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhEmployee", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhEmployee", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhEmployee", False, True)).HeaderText)
            Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhUser", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhUser", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhUser", False, True)).HeaderText)
            Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLoanScheme", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLoanScheme", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLoanScheme", False, True)).HeaderText)
            Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLnAdvAmount", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLnAdvAmount", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhLnAdvAmount", False, True)).HeaderText)
            Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApprovedAmount", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApprovedAmount", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhApprovedAmount", False, True)).HeaderText)
            Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhStatus", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhStatus", False, True)).FooterText, Me.dgLoanTrancheApproval.Columns(getColumnId_Datagrid(dgLoanTrancheApproval, "dgcolhStatus", False, True)).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub


#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Approved By :-")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Rejected By :-")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "You can't Edit this Loan detail. Reason: You are logged in into another user login.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "You can't Edit this Loan detail. Reason: This Loan is already approved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "You can't Edit this Loan detail. Reason: This Loan is already rejected.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings

End Class
