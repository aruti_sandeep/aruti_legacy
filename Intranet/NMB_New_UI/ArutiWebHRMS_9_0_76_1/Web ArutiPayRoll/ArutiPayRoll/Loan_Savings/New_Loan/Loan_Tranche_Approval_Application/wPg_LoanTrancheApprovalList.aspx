﻿<%@ Page Language="VB" Title="Loan Tranche Approval List" AutoEventWireup="false"
    MasterPageFile="~/Home1.master" CodeFile="wPg_LoanTrancheApprovalList.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Tranche_Approval_Application_wPg_LoanTrancheApprovalList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Loan Tranche Approval List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                    <ul class="header-dropdown m-r--5">
                                        <li class="dropdown">
                                            <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations">
                                                    <i class="fas fa-sliders-h"></i>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblFromDate" runat="server" Text="From Date" CssClass="form-label">
                                        </asp:Label>
                                        <uc1:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="LblApproverStatus" runat="server" Text="Approver Status" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboApproverStatus" runat="server" AutoPostBack="false" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApprover" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme" CssClass="form-label">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLoanScheme" runat="server" AutoPostBack="true" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="form-label">
                                        </asp:Label>
                                        <uc1:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblLoanTrancheAmount" runat="server" Text="Loan Tranche Amount" CssClass="form-label">
                                        </asp:Label>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                            <div class="form-group m-t-0">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtLoanAmount" runat="server" Style="text-align: right" onkeypress="return onlyNumbers(this, event);"
                                                        Text="0" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0 p-r-0">
                                            <div class="form-group m-t-0">
                                                <asp:DropDownList ID="cboAmountCondition" runat="server" AutoPostBack="false" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 m-t-30">
                                        <asp:CheckBox ID="chkMyApprovals" runat="server" AutoPostBack="true" Text="My Approvals"
                                            CssClass="filled-in" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblStatus" runat="server" Text="Application Status" CssClass="form-label"
                                            Visible="false">
                                        </asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="false" data-live-search="true"
                                                Visible="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:DataGrid ID="dgLoanTrancheApproval" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                Width="150%" AllowPaging="true" PageSize="500" PagerStyle-Position="Top" PagerStyle-Wrap="true"
                                                PagerStyle-Mode="NumericPages" PagerStyle-HorizontalAlign="Left">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="110px" Visible="true" ItemStyle-Width="110px"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText = "objdgcolhChangeStatus">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkChangeStatus" Font-Underline="false" CommandName="Status"
                                                                    Text="Change Status" ToolTip="Change Status" runat="server">
                                                                     <i class="fas fa-tasks"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="ApplicationNo" HeaderText="Application No" Visible="false"
                                                        FooterText="dgcolhApplicationNo" />
                                                    <asp:BoundColumn DataField="ApplicationDate" HeaderText="Application Date" FooterText="dgcolhApplicationDate" />
                                                    <asp:BoundColumn DataField="Employee" HeaderText="Employee" FooterText="dgcolhEmployee" />
                                                    <asp:BoundColumn DataField="CUser" HeaderText="Approver" FooterText="dgcolhUser" />
                                                    <asp:BoundColumn DataField="LoanScheme" HeaderText="Loan Scheme" FooterText="dgcolhLoanScheme" />
                                                    <asp:BoundColumn DataField="LoanTrancheAmt" HeaderText="Applied Amount" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" FooterText="dgcolhLnAdvAmount" />
                                                    <asp:BoundColumn DataField="ApprovedLoanTrancheamt" HeaderText="Approved Amount" FooterText="dgcolhApprovedAmount"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="status" HeaderText="Status" FooterText="dgcolhStatus" />
                                                    <asp:BoundColumn DataField="remark" HeaderText="Remarks" Visible="false" FooterText="objdgcolhStatus" />
                                                    <asp:BoundColumn DataField="priority" HeaderText="objdgcolhPriority" Visible="false"
                                                        FooterText="objdgcolhPriority" />
                                                    <asp:BoundColumn DataField="Isgrp" HeaderText="objdgcolhIsGrp" Visible="false" FooterText="objdgcolhIsGrp" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="objdgcolhEmployeeID" Visible="false"
                                                        FooterText="objdgcolhEmployeeID" />
                                                    <asp:BoundColumn DataField="mapuserunkid" HeaderText="objdgcolhUserID" Visible="false"
                                                        FooterText="objdgcolhUserID" />
                                                    <asp:BoundColumn DataField="statusunkid" HeaderText="objdgcolhStatusunkid" Visible="false"
                                                        FooterText="objdgcolhStatusunkid" />
                                                    <asp:BoundColumn DataField="Appstatusunkid" HeaderText="objdgcolhLoanTrancheStatusID"
                                                        Visible="false" FooterText="objdgcolhLoanTrancheStatusID" />
                                                    <asp:BoundColumn DataField="loantrancherequestunkid" HeaderText="objdgcolhloantrancherequestunkid"
                                                        Visible="false" FooterText="objdgcolhloantrancherequestunkid" />
                                                    <asp:BoundColumn DataField="loantrancheapprovalunkid" HeaderText="objdgcolhloantrancheapprovalunkid"
                                                        Visible="false" FooterText="objdgcolhloantrancheapprovalunkid" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
