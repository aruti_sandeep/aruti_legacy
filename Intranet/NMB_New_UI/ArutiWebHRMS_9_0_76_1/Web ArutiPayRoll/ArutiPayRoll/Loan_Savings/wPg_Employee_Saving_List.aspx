﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_Employee_Saving_List.aspx.vb"
    Inherits="Loan_Savings_wPg_Employee_Saving_List" Title="Employee Saving List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="Delete" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--  <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>

    <script type="text/javascript">

function closePopup() {
        $('#imgclose').click(function(evt) {
            $find('<%= popupChangeStatus.ClientID %>').hide();
        });
    }

    function onlyNumbers(txtBox, evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;
        //var cval = document.getElementById(txtBoxId).value;
        var cval = txtBox.value;

        if (cval.length > 0)
                if (charCode == 46)
            if (cval.indexOf(".") > -1)
                    return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

        return true;
    }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <cc1:ModalPopupExtender ID="popupChangeStatus" runat="server" TargetControlID="btnHidden"
                    PopupControlID="pnlChangeStatus" CancelControlID="btnClose" BackgroundCssClass="modal-backdrop">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlChangeStatus" runat="server" CssClass="card modal-dialog" Style="display: none">
                    <div class="block-header">
                        <h2>
                            <asp:Label ID="lblStatusform" runat="server" Text="Add/Edit Saving Status" CssClass="form-label"> </asp:Label>
                        </h2>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblTitle" runat="server" Text="Saving Status" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body" style="max-height: 525px;">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblSavingScheme" runat="server" Text="Saving Scheme" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtSavingScheme" runat="server" Text="" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmpName" runat="server" Text="Employee Name" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtEmployee" runat="server" Text="" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" Width="260px" runat="server" AutoPostBack="false"
                                                data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblSvStatus" runat="server" Text="Saving Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatusChange" Width="260px" runat="server" AutoPostBack="false"
                                                data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRemarks" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtStatusRemark" runat="server" Text="" TextMode="MultiLine" Rows="3"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
                                            <asp:Panel ID="pnl_dgvHistory" ScrollBars="Auto" Style="max-height: 300px" runat="server">
                                                <asp:DataGrid ID="dgvHistory" Style="margin: auto" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-hover table-bordered" AllowPaging="false" Width="100%">
                                                    <Columns>
                                                        <asp:BoundColumn HeaderText="User" ReadOnly="True" DataField="username" FooterText="dgcolhUser">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Transaction Date" ReadOnly="True" DataField="status_date"
                                                            FooterText="dgcolhTransactionDate"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Status" ReadOnly="True" DataField="status" FooterText="dgcolhStatus">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="IsGroup" ReadOnly="True" DataField="grp" Visible="false"
                                                            FooterText="objdgcolhIsGroup"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="objSavingstatustranunkid" ReadOnly="True" DataField="savingstatustranunkid"
                                                            Visible="false" FooterText="objSavingstatustranunkid"></asp:BoundColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you sure you want to Delete this Employee's Saving Scheme?"
                    ValidationGroup="SavingScheme" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Saving List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPayPeriod" runat="server" data-live-search="true" AutoPostBack="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblSvScheme" runat="server" Text="Saving Scheme" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboSavingsScheme" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblContribution" runat="server" Text="Contribution" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtContribution" runat="server" Style="text-align: right" Text="0"
                                                    onKeypress="return onlyNumbers(this);" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" CssClass="btn btn-primary" Text="New" />
                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-default" Text="Search" />
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:HiddenField ID="btnHidden" runat="Server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
                                            <asp:Panel ID="pnl_dgView" ScrollBars="Auto" runat="server" Style="max-height: 400px;">
                                                <asp:DataGrid ID="dgView" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                    AllowPaging="false" Width="150%">
                                                    <Columns>
                                                        <asp:TemplateColumn>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="EditImg" runat="server" CommandName="Change" ToolTip="Edit">
                                                                    <i class="fas fa-pencil-alt"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                                <%--<asp:ImageButton ID="" runat="server" CommandName="Change" ImageUrl="~/images/edit.png"
                                                                    ToolTip="Edit" />--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Remove" ToolTip="Delete">
                                                                    <i class="fas fa-trash text-danger"></i> 
                                                                    </asp:LinkButton>
                                                                </span>
                                                                <%-- <asp:ImageButton ID="DeleteImg" runat="server" CommandName="Remove" ImageUrl="~/images/remove.png"
                                                                    ToolTip="Delete" />--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="lnkChangeStatus" runat="server" CommandName="Status" Text="Change Status"
                                                                        ToolTip="Change Status">
                                                                     <i class="fas fa-tasks"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkPayment" runat="server" Text="Repayment/Withdrawal" Font-Underline="false"
                                                                    CommandName="Payment" ToolTip="Repayment/Withdrawal">
                                                                    <i class="far fa-money-bill-alt"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkDeposit" runat="server" Text="Deposit" Font-Underline="false"
                                                                    CommandName="Deposit" ToolTip="Deposit">
                                                                    <i class="fas fa-hand-holding-usd"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn HeaderText="Employee" ReadOnly="true" DataField="employeename" FooterText="colhEmpName">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Saving Scheme" ReadOnly="True" DataField="savingscheme"
                                                            FooterText="colhSavingScheme"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Pay Period" ReadOnly="true" DataField="period" FooterText="colhPayPeriod">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Balance Amount" ReadOnly="true" DataField="balance_amount"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" FooterText="colhBalAmount">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Interest Rate" ReadOnly="true" DataField="interest_rate"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" FooterText="colhInterestRate">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Interested Accrued" ReadOnly="true" DataField="interest_amount"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" FooterText="colhInterestAccrued">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Contribution" ReadOnly="True" DataField="contribution"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" FooterText="colhContribution">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Status" ReadOnly="True" DataField="statusname" FooterText="colhStatus">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Stop Date" ReadOnly="True" DataField="stopdate" FooterText="colhStopDate">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="StatusId" ReadOnly="True" DataField="savingstatus" Visible="false">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="EmployeeId" ReadOnly="True" DataField="employeeunkid"
                                                            Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="bf_balance" ReadOnly="True" DataField="bf_balance" Visible="false">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="broughtforward" ReadOnly="True" DataField="broughtforward"
                                                            Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="countryunkid" ReadOnly="True" DataField="countryunkid"
                                                            Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="effectivedate" Visible="false" />
                                                    </Columns>
                                                    <%--<PagerStyle Mode="NumericPages" />--%>
                                                </asp:DataGrid>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
