﻿Imports System
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Drawing.Text

Public Class CaptchaImage
    Private text As String
    Private width As Integer
    Private height As Integer
    Private familyName As String
    Private image As Bitmap
    Private random As Random = New Random()

    Public ReadOnly Property _Text As String
        Get
            Return text
        End Get
    End Property

    Public ReadOnly Property _Image As Bitmap
        Get
            Return image
        End Get
    End Property

    Public ReadOnly Property _Width As Integer
        Get
            Return width
        End Get
    End Property

    Public ReadOnly Property _Height As Integer
        Get
            Return height
        End Get
    End Property



    Public Sub New(ByVal s As String, ByVal width As Integer, ByVal height As Integer)
        Me.text = s
        Me.SetDimensions(width, height)
        Me.GenerateImage()
    End Sub

    Public Sub New(ByVal s As String, ByVal width As Integer, ByVal height As Integer, ByVal familyName As String)
        Me.text = s
        Me.SetDimensions(width, height)
        Me.SetFamilyName(familyName)
        Me.GenerateImage()
    End Sub

    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub

    Public Sub Dispose()
        GC.SuppressFinalize(Me)
        Me.Dispose(True)
    End Sub

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If disposing Then Me.image.Dispose()
    End Sub

    Private Sub SetDimensions(ByVal width As Integer, ByVal height As Integer)
        If width <= 0 Then Throw New ArgumentOutOfRangeException("width", width, "Argument out of range, must be greater than zero.")
        If height <= 0 Then Throw New ArgumentOutOfRangeException("height", height, "Argument out of range, must be greater than zero.")
        Me.width = width
        Me.height = height
    End Sub

    Private Sub SetFamilyName(ByVal familyName As String)
        Try
            Dim font As Font = New Font(Me.familyName, 12.0F)
            Me.familyName = familyName
            font.Dispose()
        Catch ex As Exception
            Me.familyName = System.Drawing.FontFamily.GenericSerif.Name
        End Try
    End Sub

    Private Sub GenerateImage()

        '*** Create a new 32-bit bitmap image.
        Dim bitmap As Bitmap = New Bitmap(Me.width, Me.height, PixelFormat.Format32bppArgb)

        '*** Create a graphics object for drawing.
        Dim g As Graphics = Graphics.FromImage(bitmap)
        g.SmoothingMode = SmoothingMode.AntiAlias
        Dim rect As Rectangle = New Rectangle(0, 0, Me.width, Me.height)

        '*** Fill in the background.
        Dim hatchBrush As HatchBrush = New HatchBrush(HatchStyle.SmallConfetti, Color.LightGray, Color.White)
        g.FillRectangle(hatchBrush, rect)

        '*** Set up the text font.
        Dim size As SizeF
        Dim fontSize As Single = rect.Height + 1
        Dim font As Font

        '*** Adjust the font size until the text fits within the image.
        Do
            fontSize -= 1
            font = New Font(Me.familyName, fontSize, FontStyle.Bold)
            size = g.MeasureString(Me.text, font)
        Loop While size.Width > rect.Width

        '*** Set up the text format.
        Dim format As StringFormat = New StringFormat()
        format.Alignment = StringAlignment.Center
        format.LineAlignment = StringAlignment.Center

        '*** Create a path using the text and warp it randomly.
        Dim path As GraphicsPath = New GraphicsPath()
        path.AddString(Me.text, font.FontFamily, CInt(font.Style), font.Size, rect, format)
        Dim v As Single = 4.0F
        Dim points As PointF() = {New PointF(Me.random.[Next](rect.Width) / v, Me.random.[Next](rect.Height) / v), New PointF(rect.Width - Me.random.[Next](rect.Width) / v, Me.random.[Next](rect.Height) / v), New PointF(Me.random.[Next](rect.Width) / v, rect.Height - Me.random.[Next](rect.Height) / v), New PointF(rect.Width - Me.random.[Next](rect.Width) / v, rect.Height - Me.random.[Next](rect.Height) / v)}
        Dim matrix As Matrix = New Matrix()
        matrix.Translate(0F, 0F)
        path.Warp(points, rect, matrix, WarpMode.Perspective, 0F)

        '*** Draw the text.
        'hatchBrush = New HatchBrush(HatchStyle.LargeConfetti, Color.LightGray, Color.DarkGray)
        hatchBrush = New HatchBrush(HatchStyle.LargeConfetti, Color.Black, Color.Black)
        g.FillPath(hatchBrush, path)

        '*** Add some random noise.
        Dim m As Integer = Math.Max(rect.Width, rect.Height)

        For i As Integer = 0 To CInt((rect.Width * rect.Height / 30.0F)) - 1
            Dim x As Integer = Me.random.[Next](rect.Width)
            Dim y As Integer = Me.random.[Next](rect.Height)
            Dim w As Integer = Me.random.[Next](m / 50)
            Dim h As Integer = Me.random.[Next](m / 50)
            g.FillEllipse(hatchBrush, x, y, w, h)
        Next

        '*** Clean up.
        font.Dispose()
        hatchBrush.Dispose()
        g.Dispose()

        '*** Set the image.
        Me.image = bitmap
    End Sub
End Class
