﻿Imports System.IO
Imports System.Web.Services

Public Class SApplicants
    Inherits Base_Page

    'Private mdtTable As DataTable
    'Private mdtCompany As DataTable

#Region " Method Functions "

    Private Sub FillCombo()
        Dim objCompany As New clsCompany
        Dim objApplicant As New clsApplicant
        Dim dsCombo As DataSet
        Try
            dsCombo = objCompany.GetCompanyForCombo("", 0, True)
            With cboCompany
                .DataValueField = "companyunkid"
                .DataTextField = "company_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            'dsCombo = objCompany.GetCompany("", 0, True)
            'mdtCompany = dsCombo.Tables(0)

            dsCombo = objApplicant.GetStatus()
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objCompany = Nothing
            objApplicant = Nothing
        End Try
    End Sub

    Private Sub FillVacancy()
        Dim objVacancy As New clsSearchJob
        Dim dsCombo As DataSet
        Try
            dsCombo = objVacancy.GetApplicantVacancy(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.VACANCY_MASTER, clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, CBool(IIf(Session("Vacancy") = "Int", False, True)), True, CInt(Session("timezone_minute_diff")), "")
            Dim dr As DataRow = dsCombo.Tables(0).NewRow
            dr.Item("vacancyid") = -2
            dr.Item("vacancy_title") = "All"
            dsCombo.Tables(0).Rows.InsertAt(dr, 0)

            dr = dsCombo.Tables(0).NewRow
            dr.Item("vacancyid") = -1
            dr.Item("vacancy_title") = "Open Vacancies"
            dsCombo.Tables(0).Rows.InsertAt(dr, 1)

            dr = dsCombo.Tables(0).NewRow
            dr.Item("vacancyid") = 0
            dr.Item("vacancy_title") = "Closed Vacancies"
            dsCombo.Tables(0).Rows.InsertAt(dr, 2)

            dsCombo.Tables(0).AcceptChanges()

            With cboVacancy
                .DataValueField = "vacancyid"
                .DataTextField = "vacancy_title"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "-2"
            End With

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objVacancy = Nothing
        End Try
    End Sub

    Private Sub FillList()
        ''Dim objApplicant As New clsApplicant
        'Dim dsList As DataSet
        'Try
        '    If CInt(Session("companyunkid")) <= 0 OrElse cboVacancy.Items.Count <= 0 Then
        '        Exit Try
        '    End If

        '    dsList = clsApplicant.GetApplicantList(strCompCode:=Session("CompCode").ToString _
        '                                           , intComUnkID:=CInt(Session("companyunkid")) _
        '                                           , intApplicantUnkId:=0 _
        '                                           , intVacancyUnkId:=CInt(cboVacancy.SelectedValue) _
        '                                           , strFirstName:=txtFirstName.Text _
        '                                           , strSurName:=txtSurname.Text _
        '                                           , strEmail:=txtEmail.Text _
        '                                           , strPresentMobileNo:=txtMobile.Text _
        '                                           , intStatusId:=CInt(cboStatus.SelectedValue) _
        '                                           , intTitleId:=clsCommon_Master.enCommonMaster.TITLE _
        '                                           , intVacancyMasterTypeId:=clsCommon_Master.enCommonMaster.VACANCY_MASTER _
        '                                           , intTimezoneMinuteDiff:=0 _
        '                                           , startRowIndex:=1 _
        '                                           , intPageSize:=10 _
        '                                           , strSortExpression:=""
        '                                           )
        '    'mdtTable = dsList.Tables(0)

        '    grdApplicants.Columns(0).Visible = True
        '    grdApplicants.Columns(1).Visible = True
        '    grdApplicants.Columns(2).Visible = True
        '    'grdApplicants.DataSource = mdtTable
        '    grdApplicants.DataSource = dsList.Tables(0)
        '    grdApplicants.DataBind()
        '    'grdApplicants.Columns(0).Visible = False
        '    'grdApplicants.Columns(1).Visible = False
        '    'grdApplicants.Columns(2).Visible = False

        '    'lblCount.Text = "(" & mdtTable.Rows.Count.ToString & ")"
        '    lblCount.Text = "(" & grdApplicants.Rows.Count & ")"
        'Catch ex As Exception
        '    Global_asax.CatchException(ex, Context)
        'Finally
        '    'objApplicant = Nothing
        'End Try
    End Sub
#End Region


#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()

                'Call FillVacancy()

                dsApplicants.SelectParameters.Item("intTitleId").DefaultValue = CInt(clsCommon_Master.enCommonMaster.TITLE)
                dsApplicants.SelectParameters.Item("intVacancyMasterTypeId").DefaultValue = CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER)
                'Page.ClientScript.RegisterStartupScript(Me.GetType, "Preview", "window.open('" & Request.Url.GetLeftPart(UriPartial.Authority) & "/" & Request.ApplicationPath & "/User/Preview.aspx','_blank');", True)

                'If Not Page.ClientScript.IsStartupScriptRegistered("Preview") Then
                '    Page.ClientScript.RegisterStartupScript(Me.GetType(), "Preview", "window.open('" & Request.Url.GetLeftPart(UriPartial.Authority) & "/" & Request.ApplicationPath & "/User/Preview.aspx','_blank');", True)
                'End If
            Else
                'mdtTable = CType(ViewState("mdtTable"), DataTable)
                'mdtCompany = CType(ViewState("mdtCompany"), DataTable)
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SApplicants_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
            'Me.ViewState.Add("mdtTable", mdtTable)
            'Me.ViewState.Add("mdtCompany", mdtCompany)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCompany.SelectedIndexChanged
        Dim objApplicant As New clsApplicant
        Dim objCompany As New clsCompany
        Try
            Dim strCompCode As String = ""
            Dim intCompanyunkid As Integer = CInt(cboCompany.SelectedValue)

            If intCompanyunkid > 0 Then
                'Dim dr() As DataRow = mdtCompany.Select("companyunkid = " & intCompanyunkid & " ")
                'If dr.Length > 0 Then

                '    If objApplicant.GetCompanyInfo(intCompanyunkid, dr(0).Item("company_code").ToString, True) = True Then
                '        Call FillVacancy()
                '    End If

                'End If
                Dim strCode As String = objCompany.GetCompanyCode(intComUnkID:=intCompanyunkid)
                dsApplicants.SelectParameters.Item("strCompCode").DefaultValue = strCode
                If objApplicant.GetCompanyInfo(intCompanyunkid, strCode, True) = True Then
                    dsApplicants.SelectParameters.Item("intTimezoneMinuteDiff").DefaultValue = Session("timezone_minute_diff").ToString
                    Call FillVacancy()
                End If
                'Hemant --(08 Sept 2018) --Start
                'Enhancement - Issue : To Stop Importing Deleted Applied Job Vancancy From HRMS Database Server to Local Database Server for TANAPA
                'Dim AllImportedRow As DataRow = dsCombo.Tables(0).NewRow()
                If (intCompanyunkid = 14 AndAlso strCode = "TNP") Then
                    'Hemant --(03 Oct 2018) --Start
                    Dim itemToRemove As ListItem = cboStatus.Items.FindByText("Imported")
                    If itemToRemove IsNot Nothing Then
                        cboStatus.Items.Remove(itemToRemove)
                    End If
                    'Hemant --(03 Oct 2018) --End
                    cboStatus.Items.Add(New ListItem("All Imported", "4"))
                Else
                    'Hemant --(03 Oct 2018) --Start
                    Dim itemToAdd As ListItem = cboStatus.Items.FindByText("Imported")

                    If itemToAdd Is Nothing Then
                        cboStatus.Items.Add(New ListItem("Imported", "2"))
                    End If
                    'Hemant --(03 Oct 2018) --End
                    Dim itemToRemove As ListItem = cboStatus.Items.FindByText("All Imported")

                    If itemToRemove IsNot Nothing Then
                        cboStatus.Items.Remove(itemToRemove)
                    End If
                End If
                cboStatus.SelectedValue = 0
                'Hemant --(08 Sept 2018) --End
            Else
                dsApplicants.SelectParameters.Item("strCompCode").DefaultValue = ""
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
            objCompany = Nothing
        End Try
    End Sub

#End Region

#Region " Datagridview Events "
    Private Sub grdApplicants_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdApplicants.RowCommand
        Try
            If e.CommandName = "Preview" Then
                Dim strCompCode As String = grdApplicants.Rows(CInt(e.CommandArgument)).Cells(1).Text
                Dim intCompanyunkid As Integer = CInt(grdApplicants.Rows(CInt(e.CommandArgument)).Cells(2).Text)
                Dim intApplicantunkid As Integer = CInt(grdApplicants.Rows(CInt(e.CommandArgument)).Cells(0).Text)

                Session("PreviewApplicantUnkid") = intApplicantunkid
                Session("PreviewCompCode") = strCompCode
                Session("Previewcompanyunkid") = intCompanyunkid

                'Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/User/Preview.aspx", False)
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "Preview", "openwin();", True)
                'Page.ClientScript.RegisterStartupScript(Me.GetType(), "openwin", "var a = document.createElement('a');  a.setAttribute('href', '../User/Preview.aspx');  a.setAttribute('target', '_blank');  a.setAttribute('id', 'openwin');  document.body.appendChild(a); a.click(); ", True)
                ' Page.ClientScript.RegisterStartupScript(Me.GetType(), "openwin", "openwin('" & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/User/Preview.aspx')", False)
                'Page.ClientScript.RegisterStartupScript(Me.GetType(), "openwin", "openwin('../User/Preview.aspx')", False)
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Preview", "window.open('" & Request.Url.GetLeftPart(UriPartial.Authority) & "/" & Request.ApplicationPath & "/User/Preview.aspx','_blank');", True)
                'Page.ClientScript.RegisterStartupScript(Me.GetType(), "OpenWindow", "window.open('../User/Preview.aspx','_newtab');", True)
                'Response.Write("<script> window.open( '" & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/User/Preview.aspx','_blank' ); </script>")
                'Page.ClientScript.RegisterStartupScript(Me.GetType, "Preview", "window.open('" & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/User/Preview.aspx','_blank');", True)
                'Page.ClientScript.RegisterStartupScript(Me.GetType, "Preview", "window.open('" & Request.Url.GetLeftPart(UriPartial.Authority) & "/" & Request.ApplicationPath & "/User/Preview.aspx','_blank');", True)
                'Dim strScript As String = "<script> "
                'strScript += "var newWindow = window.open('" & Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/User/Preview.aspx', '_blank','height=450, center:yes, width=600, status=no, resizable= yes, menubar=no, toolbar=no, location=yes, scrollbars=no, status=no');"
                'strScript += "</script>"
                'Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "strScript", strScript)

                'Response.End()
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
        End Try
    End Sub

    Private Sub grdApplicants_DataBound(sender As Object, e As EventArgs) Handles grdApplicants.DataBound
        Try
            'lblCount.Text = "(" & grdApplicants.Rows.Count & ")"
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub dsApplicants_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles dsApplicants.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is Integer Then
                Integer.TryParse(e.ReturnValue, intC)
            End If

            If TypeOf e.ReturnValue Is DataSet Then
                If CType(e.ReturnValue, DataSet).Tables(0).Rows.Count <= 0 Then grdApplicants.PageIndex = 0
            End If
            lblCount.Text = "(" & intC.ToString & ")"

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

#Region " Buttons Events "
    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboCompany.SelectedValue) <= 0 Then
                ShowMessage("Please select Company.")
                Exit Try
            End If
            'Call FillList()
            grdApplicants.DataBind()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Try
            txtFirstName.Text = ""
            txtSurname.Text = ""
            txtEmail.Text = ""
            txtMobile.Text = ""
            'Call FillList()
            grdApplicants.DataBind()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
        End Try
    End Sub

    Private Sub lnkExcel_Click(sender As Object, e As EventArgs) Handles lnkExcel.Click
        Try
            If CInt(cboCompany.SelectedValue) <= 0 Then
                ShowMessage("Please select company.")
                Exit Try
            End If

            Dim strSorting As String = " CASE WHEN rcapplicant_master.Syncdatetime IS NULL THEN 1  ELSE 2 END, rcapplicant_master.firstname + ISNULL(rcapplicant_master.othername, '''''''') + rcapplicant_master.surname "
            Select Case grdApplicants.SortExpression.ToString.ToUpper
                Case "CASE WHEN RCAPPLICANT_MASTER.TITLEUNKID > 0 THEN TITLE.NAME ELSE '' END"
                    strSorting = " CASE WHEN rcapplicant_master.titleunkid > 0 THEN title.name ELSE '''''''' END "

                Case "RCAPPLICANT_MASTER.FIRSTNAME + ISNULL(RCAPPLICANT_MASTER.OTHERNAME, '') + RCAPPLICANT_MASTER.SURNAME"
                    strSorting = " rcapplicant_master.firstname + ISNULL(rcapplicant_master.othername, '''''''') + rcapplicant_master.surname "

                Case "CASE RCAPPLICANT_MASTER.GENDER WHEN 1 THEN 1 WHEN 2 THEN 0 ELSE 0 END"
                    strSorting = " " & grdApplicants.SortExpression & " "

                Case "EMAIL"
                    strSorting = " " & grdApplicants.SortExpression & " "

                Case "PRESENT_MOBILENO"
                    strSorting = " " & grdApplicants.SortExpression & " "

                Case "ISNULL(VACANCY.NAME, '')"
                    strSorting = " ISNULL(Vacancy.name, '''''''') "

                Case "RCAPPLICANT_MASTER.TRANSACTIONDATE"
                    strSorting = " rcapplicant_master.transactiondate "

                Case "RCAPPLICANT_MASTER.SYNCDATETIME"
                    strSorting = " rcapplicant_master.Syncdatetime "

            End Select

            If grdApplicants.SortDirection = SortDirection.Descending Then
                strSorting += " DESC "
            End If

            Dim ds As DataSet = clsApplicant.GetApplicantList(strCompCode:=Session("CompCode").ToString, intComUnkID:=CInt(Session("companyunkid")), intApplicantUnkId:=0, intVacancyUnkId:=CInt(cboVacancy.SelectedValue), strFirstName:=txtFirstName.Text, strSurName:=txtSurname.Text, strEmail:=txtEmail.Text, strPresentMobileNo:=txtMobile.Text, intStatusId:=CInt(cboStatus.SelectedValue), intTitleId:=CInt(clsCommon_Master.enCommonMaster.TITLE), intVacancyMasterTypeId:=CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER), intTimezoneMinuteDiff:=CInt(Session("timezone_minute_diff")), startRowIndex:=0, intPageSize:=99999999, strSortExpression:=strSorting)
            If ds.Tables(0).Columns.Contains("referenceno") = True Then
                ds.Tables(0).Columns.Remove("referenceno")
            End If
            If ds.Tables(0).Columns.Contains("Syncdatetime") = True Then
                ds.Tables(0).Columns("Syncdatetime").ColumnName = "ImportedTime"
            End If
            If ds.Tables(0).Columns.Contains("Syncdatetime1") = True Then
                ds.Tables(0).Columns.Remove("Syncdatetime1")
            End If
            If ds.Tables(0).Columns.Contains("RowNumber") = True Then
                ds.Tables(0).Columns.Remove("RowNumber")
            End If


            'Create a dummy GridView
            Dim GridView1 As New GridView()

            GridView1.AllowPaging = False

            GridView1.DataSource = ds.Tables(0)

            GridView1.DataBind()



            Response.Clear()

            Response.Buffer = True

            'Response.ContentType = "application/x-msexcel"
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("content-disposition", "attachment;filename=Applicants.xls")

            Response.Charset = ""

            'Response.ContentType = "application/vnd.ms-excel"


            Dim sw As New StringWriter()

            Dim hw As New HtmlTextWriter(sw)



            For i As Integer = 0 To GridView1.Rows.Count - 1

                'Apply text style to each Row

                GridView1.Rows(i).Attributes.Add("class", "textmode")

            Next

            GridView1.RenderControl(hw)



            'style to format numbers to string

            Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"

            Response.Write(style)
            Response.Output.Write(sw.ToString)
            Response.Flush()
            'Response.End()

            ''Response.Flush()
            ''System.IO.File.Delete(sw.ToString())

            'Response.End()

            'For Each dsRow As DataRow In ds.Tables(0).Rows
            '    sw.WriteLine(String.Join(",", dsRow.ItemArray.Select(Function(r) r.ToString().Replace(",", ""))))
            'Next
            'Response.Write(sw.ToString())
            'sw.Flush()
            Response.SuppressContent = True
            HttpContext.Current.ApplicationInstance.CompleteRequest()
            'Response.End()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    <WebMethod()> Public Shared Sub PreviewApplicant(objApplicantId)
        Try
            HttpContext.Current.Session("PreviewCompCode") = HttpContext.Current.Session("CompCode")
            HttpContext.Current.Session("Previewcompanyunkid") = HttpContext.Current.Session("companyunkid")
            HttpContext.Current.Session("PreviewApplicantUnkid") = CInt(objApplicantId)
        Catch ex As Exception

        End Try
    End Sub
#End Region
End Class