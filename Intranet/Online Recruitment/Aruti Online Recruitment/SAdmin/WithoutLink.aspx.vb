﻿Option Strict On
Imports System.IO

Public Class WithoutLink
    Inherits Base_Page

#Region " Methods Functions "

    Private Sub FillCombo()
        Dim objCompany As New clsCompany
        Dim dsCombo As DataSet
        Try
            dsCombo = objCompany.GetCompanyForCombo("", 0, True)
            With cboCompany
                .DataValueField = "companyunkid"
                .DataTextField = "company_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally

        End Try
    End Sub

    Private Sub FillList()

        Try

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally

        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In grdLink.Rows
                cb = CType(grdLink.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                cb.Checked = blnCheckAll

            Next

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllEmployee(chkSelectAll.Checked)

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()

            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SendLink_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


#End Region

#Region " Datagridview Events "
    Private Sub grdLink_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdLink.RowCommand
        Try
            If e.CommandName = "DeleteApplicant" Then

                Dim strCompCode As String = grdLink.DataKeys(CInt(e.CommandArgument)).Item("Comp_Code").ToString
                Dim intCompanyunkid As Integer = 0
                Integer.TryParse(grdLink.DataKeys(CInt(e.CommandArgument)).Item("companyunkid").ToString, intCompanyunkid)
                Dim strEmail As String = grdLink.Rows(CInt(e.CommandArgument)).Cells(4).Text

                If Membership.GetUser(strEmail) IsNot Nothing Then
                    Membership.DeleteUser(strEmail)

                    grdLink.DataBind()
                    ShowMessage("Applicant deleted successfully!")
                End If


            End If


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            'FillList()
        End Try
    End Sub

    Private Sub grdLink_DataBound(sender As Object, e As EventArgs) Handles grdLink.DataBound
        Try

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub dsLink_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles dsLink.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is Integer Then
                Integer.TryParse(CType(e.ReturnValue, String), intC)
            End If

            If TypeOf e.ReturnValue Is DataSet Then
                If CType(e.ReturnValue, DataSet).Tables(0).Rows.Count <= 0 Then grdLink.PageIndex = 0
            End If

            lblCount.Text = "(" & intC.ToString & ")"

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    'Private Sub cboCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCompany.SelectedIndexChanged
    '    Dim objApplicant As New clsApplicant
    '    Dim objCompany As New clsCompany
    '    Try
    '        Dim strCompCode As String = ""
    '        Dim intCompanyunkid As Integer = CInt(cboCompany.SelectedValue)

    '        If intCompanyunkid > 0 Then

    '            Dim strCode As String = objCompany.GetCompanyCode(intComUnkID:=intCompanyunkid)
    '            If objApplicant.GetCompanyInfo(intCompanyunkid, strCode, True) = True Then

    '            End If
    '        End If
    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    Finally
    '        objApplicant = Nothing
    '    End Try
    'End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try


            Dim blnChecked As Boolean = False

            For Each dgRow As GridViewRow In grdLink.Rows

                Dim cb As CheckBox = DirectCast(dgRow.FindControl("chkSelect"), CheckBox)

                If cb IsNot Nothing AndAlso cb.Checked Then

                    Dim intUnkId As Integer = 0
                    Integer.TryParse(grdLink.DataKeys(dgRow.RowIndex).Item("activationlinkunkid").ToString, intUnkId)
                    Dim strCompCode As String = grdLink.DataKeys(dgRow.RowIndex).Item("Comp_Code").ToString
                    Dim intCompanyunkid As Integer = 0
                    Integer.TryParse(grdLink.DataKeys(dgRow.RowIndex).Item("companyunkid").ToString, intCompanyunkid)
                    Dim strEmail As String = grdLink.DataKeys(dgRow.RowIndex).Item("LoweredEmail").ToString

                    If Membership.GetUser(strEmail) IsNot Nothing Then
                        Membership.DeleteUser(strEmail)
                    End If

                    blnChecked = True
                End If

            Next

            If blnChecked = False Then
                ShowMessage("Please tick atleast one email to delete.")
            Else
                grdLink.DataBind()
                ShowMessage("Applicants deleted successfully!")
            End If


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
        End Try
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            'Call FillList()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally

        End Try
    End Sub

    Private Sub btnRegister_Click(sender As Object, e As EventArgs) Handles btnRegister.Click
        Dim objCompany As New clsCompany
        'Dim objApplicant As New clsApplicant
        Try
            If CInt(cboCompany.SelectedValue) <= 0 Then
                ShowMessage("Please select Company.")
                Exit Try
            End If

            Dim intCompanyunkid As Integer = CInt(cboCompany.SelectedValue)
            Dim strCompCode As String = objCompany.GetCompanyCode(intComUnkID:=intCompanyunkid)
            Dim mstrUrlArutiLink As String = Session("mstrUrlArutiLink").ToString
            Dim encrptlnk As String = ""

            Dim dsCompany As DataSet = objCompany.GetCompany(strCompCode, intCompanyunkid, True)

            If dsCompany.Tables(0).Rows.Count > 0 Then
                encrptlnk = "?ext=7&cc=" & dsCompany.Tables(0).Rows(0).Item("authentication_code").ToString
            End If

            Dim blnChecked As Boolean = False

            For Each dgRow As GridViewRow In grdLink.Rows

                Dim cb As CheckBox = DirectCast(dgRow.FindControl("chkSelect"), CheckBox)

                If cb IsNot Nothing AndAlso cb.Checked Then

                    Dim intUnkId As Integer = 0
                    Integer.TryParse(grdLink.DataKeys(dgRow.RowIndex).Item("activationlinkunkid").ToString, intUnkId)
                    'Dim strCompCode As String = grdLink.DataKeys(dgRow.RowIndex).Item("Comp_Code").ToString
                    'Dim intCompanyunkid As Integer = 0
                    'Integer.TryParse(grdLink.DataKeys(dgRow.RowIndex).Item("companyunkid").ToString, intCompanyunkid)
                    Dim strEmail As String = grdLink.DataKeys(dgRow.RowIndex).Item("LoweredEmail").ToString
                    Dim mstrCompName As String = cboCompany.SelectedItem.Text


                    Dim profile As UserProfile = UserProfile.GetUserProfile(strEmail)

                    Dim strBody As String = "<!DOCTYPE html>" &
                    "<html>" &
                        "<head>" &
                            "<title>#companyname# Email Confirmation!!!</title>" &
                            "<meta charset='utf-8'/>" &
                        "</head>" &
                        "<body style='font-size:9.0pt; font-family:'Verdana', 'Sans-Serif'; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" &
                            "<br/>" &
                        "#imgcmp#" &
                        "<p>Dear <b>#firstname# #lastname#</b>, </p>" &
                            "<p>Welcome To <b> #companyname# </b> Online Job Application System.</p>" &
                            "<p>We are glad To inform you that your Email <b> #username#</b> Is now registered With us.</p>" &
                            "<p>Please click On the below link Or copy And paste the link To address bar To activate your account.</p>" &
                            "<p> #activationlink# </p>" &
                            "<br/>" &
                            "<p><B>Thank you,</B></p>" &
                            "<p><B>#companyname# Support Team</B></p>" &
                            "<br/>" &
                            "<p><b>Online Job Application Powered by Aruti HR & Payroll Management Software</b></p>" &
                        "#imgaruti#" &
                    "</body>" &
                    "</html>"

                    Dim strSubject As String = ""
                    Dim reader As New StreamReader(Server.MapPath("~/EmailTemplate/RegisterSuccess.html"))
                    If reader IsNot Nothing Then
                        strBody = reader.ReadToEnd
                        reader.Close()
                    End If
                    strSubject = strBody.Substring(strBody.IndexOf("<title>") + 7, strBody.IndexOf("</title>") - strBody.IndexOf("<title>") - 7).Replace("#companyname#", mstrCompName)
                    If strSubject.Trim = "" Then
                        strSubject = mstrCompName & " Email confirmation!"
                    End If

                    Dim ActivationUrl As String = ""
                    ActivationUrl = Request.Form(hflocationorigin.UniqueID) & Replace(Request.ApplicationPath & "/ActivateUsers.aspx", "//", "/") & "?UserID=" + Server.UrlEncode(clsCrypto.Encrypt(Membership.GetUser(strEmail).ProviderUserKey.ToString)) + "&Email=" + Server.UrlEncode(clsCrypto.Encrypt(strEmail)) + "&lnk=" + Server.UrlEncode(clsCrypto.Encrypt(mstrUrlArutiLink.ToLower.Replace("slogin.aspx", "Login.aspx") & encrptlnk)) + "&c=" + Server.UrlEncode(clsCrypto.Encrypt(strCompCode)) + "&i=" + Server.UrlEncode(clsCrypto.Encrypt(intCompanyunkid.ToString))
                    ActivationUrl = ActivationUrl.Replace("&", "&amp;")

                    Dim intActUnkId As Integer = 0
                    Dim objApplicant As New clsApplicant()
                    If objApplicant.InsertActivationLink(strCompCode:=strCompCode _
                                                    , intCompUnkID:=CInt(intCompanyunkid) _
                                                    , strUserId:=Membership.GetUser(strEmail).ProviderUserKey.ToString _
                                                    , strEmail:=strEmail _
                                                    , strSubject:=strSubject _
                                                    , strMessage:=strBody.Replace("#username#", strEmail.Trim()).Replace("#activationlink#", ActivationUrl).Replace("#companyname#", mstrCompName).Replace("#firstname#", profile.FirstName).Replace("#lastname#", profile.LastName) _
                                                    , strLink:=ActivationUrl _
                                                    , intRet_UnkId:=intActUnkId
                                                    ) = True Then

                    End If

                    blnChecked = True
                End If

            Next

            If blnChecked = False Then
                ShowMessage("Please tick atleast one email to register.")
            Else
                grdLink.DataBind()
                ShowMessage("Applicants registered successfully!")
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally

        End Try


    End Sub

#End Region

End Class