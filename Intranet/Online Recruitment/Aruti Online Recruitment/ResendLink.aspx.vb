﻿Option Strict On

Imports System.Data.SqlClient
Imports System.IO
Imports System.Net.Mail
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.Owin

Public Class ResendLink
    Inherits Base_General

    Dim strEmail, ConString, ActivationUrl As String
    'Dim message As MailMessage
    'Dim smtp As SmtpClient

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                If Session("image") IsNot Nothing AndAlso IsDBNull(Session("image")) = False Then
                    Dim bytes As Byte() = DirectCast(Session("image"), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes)
                    img_shortcuticon.ImageUrl = Convert.ToString("data:image/jpg;base64,") & base64String
                Else
                    img_shortcuticon.ImageUrl = "~/Images/blank.png"
                End If

                '*** "Object reference not set to an instance of an object." error was coming if register.aspx is visited directly.
                If Session("mstrUrlArutiLink") Is Nothing Then
                    Response.Redirect("UnauthorizedAccess.aspx", False)
                    Exit Try
                End If

                If Session("mstrUrlArutiLink").ToString.ToLower.Contains("/alogin.aspx") = True Then

                End If

                If Request.UrlReferrer.ToString.ToLower.Contains("slogin.aspx") Then

                    If Session("sadminusername").ToString <> "" AndAlso Session("sadminpassword").ToString <> "" Then
                        Session("blnEmailSetupDone") = True
                    Else
                        Session("blnEmailSetupDone") = False
                        Response.Redirect("ucons.aspx", False)
                        Exit Sub
                    End If
                Else
                    If Session("username").ToString <> "" AndAlso Session("password").ToString <> "" Then
                        Session("blnEmailSetupDone") = True
                    Else
                        Session("blnEmailSetupDone") = False
                        Response.Redirect("ucons.as`px", False)
                        Exit Sub
                    End If
                End If

                If Request.Url.AbsoluteUri.ToString.ToLower.Contains("arutihr.com") = False Then
                    pnlPoweredBy.Visible = False
                Else
                    pnlPoweredBy.Visible = True
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim objApplicant As New clsApplicant
        Dim objMail As New clsMail
        Try
            If txtEmail.Text.Trim = "" Then
                ShowMessage("Please Enter Email.", MessageType.Info)
                txtEmail.Focus()
                Exit Try
            ElseIf clsApplicant.IsValidEmail(txtEmail.Text) = False Then
                ShowMessage("Please Enter Valid Email.", MessageType.Info)
                txtEmail.Focus()
                Exit Try
            End If

            strEmail = txtEmail.Text

            If Membership.GetUser(strEmail) IsNot Nothing Then

                'If Roles.IsUserInRole(strEmail, "superadmin") = True Then
                '    ShowMessage("Sorry, Please login through Super Admin Login.", MessageType.Info)
                '    e.Cancel = True
                '    Exit Try
                'End If

                If Membership.GetUser(strEmail).IsApproved = True Then
                    ShowMessage("Sorry, This Email is already activated.", MessageType.Info)
                    txtEmail.Focus()
                    Exit Try
                End If

                If Roles.IsUserInRole(strEmail, "superadmin") = False Then
                    Dim objSA As New clsSACommon

                    Dim ds1 As DataSet = objSA.GetCompanyCodeFromEmail(strEmail, Membership.GetUser(strEmail).ProviderUserKey.ToString)
                    If ds1.Tables(0).Rows.Count <= 0 Then
                        ShowMessage("Sorry, You are not registered.", MessageType.Info)
                        txtEmail.Focus()
                        Exit Try
                    Else
                        If ds1.Tables(0).Rows(0).Item("Comp_Code").ToString = Session("CompCode").ToString AndAlso CInt(ds1.Tables(0).Rows(0).Item("companyunkid")) = CInt(Session("companyunkid")) Then

                        Else
                            ShowMessage("Sorry, This company is restricted for your account.", MessageType.Info)
                            txtEmail.Focus()
                            Exit Try
                        End If
                    End If
                End If

                If Membership.GetUser(strEmail).IsLockedOut = True Then
                    Dim dtLastLockOut As Date = Membership.GetUser(strEmail).LastLockoutDate
                    Dim dtUnlockDate As Date = dtLastLockOut.AddMinutes(10)
                    'Dim dtUnlockDate As Date = dtLastLockOut.AddHours(3)

                    If DateTime.Now > dtUnlockDate Then
                        Membership.GetUser(strEmail).UnlockUser()
                    Else
                        If DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) <= 59 Then
                            ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) & " minutes.")
                        Else
                            ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Hour, DateTime.Now, dtUnlockDate) & " hour(s).")
                        End If
                        txtEmail.Focus()
                        Exit Try
                    End If
                End If

            Else
                ShowMessage("Sorry, This Email does not exist.")
                txtEmail.Focus()
                Exit Try
            End If


            Dim ds As DataSet = clsApplicant.GetActivationLink(strCompCode:=Session("CompCode").ToString, intComUnkID:=CInt(Session("companyunkid")), strEmail:=strEmail, intIsApproved:=0, startRowIndex:=0, intPageSize:=10, strSortExpression:="", isemailsent:=-1)

            lblResult.Text = "Activation Link has been sent to you on your email " & strEmail & ".<BR> Please check your email to activate your account. <BR>Please Activate your account before login. <BR><BR><a href=" & Session("mstrUrlArutiLink").ToString() & ">Click here</a> to Login after Activation. "

            pnlResend.Visible = False
            pnlResult.Visible = True

            If ds.Tables(0).Rows.Count > 0 Then

                If objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
                                                   , strCompCode:=Session("CompCode").ToString() _
                                                   , ByRefblnIsActive:=False
                                                   ) = False Then

                    Exit Try
                End If

                Dim objCompany1 As New clsCompany
                Session("e_emailsetupunkid") = 0
                objCompany1.GetEmailSetup(Session("CompCode").ToString, CInt(Session("companyunkid")))

                If objMail.SendEmail(strToEmail:=Server.HtmlDecode(ds.Tables(0).Rows(0).Item("LoweredEmail").ToString()) _
                                         , strSubject:=Server.HtmlDecode(ds.Tables(0).Rows(0).Item("subject").ToString()) _
                                         , strBody:=Server.HtmlDecode(ds.Tables(0).Rows(0).Item("message").ToString()).Replace("&", "&amp;") _
                                         , blnAddLogo:=True
                                         ) = False Then

                    objApplicant.UpdateActivationEmailSent(CInt(ds.Tables(0).Rows(0).Item("activationlinkunkid")), False, ds.Tables(0).Rows(0).Item("LoweredEmail").ToString(), DateAndTime.Now)
                    lblResult.ForeColor = Drawing.Color.Red
                    lblResult.Text = "Your attempt to Resend Activation Link was not successful. <BR>Please try again later."
                Else
                    objApplicant.UpdateActivationEmailSent(CInt(ds.Tables(0).Rows(0).Item("activationlinkunkid")), True, ds.Tables(0).Rows(0).Item("LoweredEmail").ToString(), DateAndTime.Now)
                    lblResult.ForeColor = Drawing.Color.Black
                    ShowMessage("Mail Sent successfully!")
                End If

            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
            objMail = Nothing
        End Try
    End Sub

    'Private Sub PasswordRecovery1_SendingMail(sender As Object, e As MailMessageEventArgs) Handles PasswordRecovery1.SendingMail
    '    Dim objApplicant As New clsApplicant
    '    Dim objMail As New clsMail
    '    Try
    '        e.Cancel = True

    '        Email = PasswordRecovery1.UserName

    '        Dim ds As DataSet = clsApplicant.GetActivationLink(strCompCode:=Session("CompCode").ToString, intComUnkID:=CInt(Session("companyunkid")), strEmail:=Email, intIsApproved:=0, startRowIndex:=0, intPageSize:=10, strSortExpression:="", isemailsent:=-1)

    '        PasswordRecovery1.SuccessText = "Activation Link has been sent to you on your email " & Email & ".<BR> Please check your email to activate your account. <BR>Please Activate your account before login. <BR><BR><a href=" & Session("mstrUrlArutiLink").ToString() & ">Click here</a> to Login after Activation. "

    '        If ds.Tables(0).Rows.Count > 0 Then

    '            If objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
    '                                               , strCompCode:=Session("CompCode").ToString() _
    '                                               , ByRefblnIsActive:=False
    '                                               ) = False Then

    '                Exit Try
    '            End If

    '            If objMail.SendEmail(strToEmail:=Server.HtmlDecode(ds.Tables(0).Rows(0).Item("LoweredEmail").ToString()) _
    '                                     , strSubject:=Server.HtmlDecode(ds.Tables(0).Rows(0).Item("subject").ToString()) _
    '                                     , strBody:=Server.HtmlDecode(ds.Tables(0).Rows(0).Item("message").ToString()) _
    '                                     , blnAddLogo:=True
    '                                     ) = False Then

    '                objApplicant.UpdateActivationEmailSent(CInt(ds.Tables(0).Rows(0).Item("activationlinkunkid")), False, ds.Tables(0).Rows(0).Item("LoweredEmail").ToString(), DateAndTime.Now)
    '                PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Red
    '                PasswordRecovery1.SuccessText = "Your attempt to Resend Activation Link was not successful. <BR>Please try again later."
    '            Else
    '                objApplicant.UpdateActivationEmailSent(CInt(ds.Tables(0).Rows(0).Item("activationlinkunkid")), True, ds.Tables(0).Rows(0).Item("LoweredEmail").ToString(), DateAndTime.Now)
    '                PasswordRecovery1.SuccessTextStyle.ForeColor = Drawing.Color.Black
    '                ShowMessage("Mail Sent successfully!")
    '            End If

    '        End If

    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    Finally
    '        objApplicant = Nothing
    '    End Try
    'End Sub

    'Private Sub PasswordRecovery1_VerifyingUser(sender As Object, e As LoginCancelEventArgs) Handles PasswordRecovery1.VerifyingUser
    '    Try
    '        If Membership.GetUser(PasswordRecovery1.UserName) IsNot Nothing Then

    '            'If Roles.IsUserInRole(PasswordRecovery1.UserName, "superadmin") = True Then
    '            '    ShowMessage("Sorry, Please login through Super Admin Login.", MessageType.Info)
    '            '    e.Cancel = True
    '            '    Exit Try
    '            'End If

    '            If Membership.GetUser(PasswordRecovery1.UserName).IsApproved = True Then
    '                ShowMessage("Sorry, This Email is already activated.", MessageType.Info)
    '                e.Cancel = True
    '                Exit Try
    '            End If

    '            If Roles.IsUserInRole(PasswordRecovery1.UserName, "superadmin") = False Then
    '                Dim objSA As New clsSACommon

    '                Dim ds As DataSet = objSA.GetCompanyCodeFromEmail(PasswordRecovery1.UserName, Membership.GetUser(PasswordRecovery1.UserName).ProviderUserKey.ToString)
    '                If ds.Tables(0).Rows.Count <= 0 Then
    '                    ShowMessage("Sorry, You are not registered.", MessageType.Info)
    '                    e.Cancel = True
    '                    Exit Try
    '                Else
    '                    If ds.Tables(0).Rows(0).Item("Comp_Code").ToString = Session("CompCode").ToString AndAlso CInt(ds.Tables(0).Rows(0).Item("companyunkid")) = CInt(Session("companyunkid")) Then

    '                    Else
    '                        ShowMessage("Sorry, This company is restricted for your account.", MessageType.Info)
    '                        e.Cancel = True
    '                        Exit Try
    '                    End If
    '                End If
    '            End If

    '            If Membership.GetUser(PasswordRecovery1.UserName).IsLockedOut = True Then
    '                Dim dtLastLockOut As Date = Membership.GetUser(PasswordRecovery1.UserName).LastLockoutDate
    '                Dim dtUnlockDate As Date = dtLastLockOut.AddMinutes(10)
    '                'Dim dtUnlockDate As Date = dtLastLockOut.AddHours(3)

    '                If DateTime.Now > dtUnlockDate Then
    '                    Membership.GetUser(PasswordRecovery1.UserName).UnlockUser()
    '                Else
    '                    If DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) <= 59 Then
    '                        ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) & " minutes.")
    '                    Else
    '                        ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Hour, DateTime.Now, dtUnlockDate) & " hour(s).")
    '                    End If
    '                    e.Cancel = True
    '                    Exit Try
    '                End If
    '            End If

    '        Else
    '            ShowMessage("Sorry, This Email does not exist.")
    '            Exit Try
    '        End If


    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

End Class