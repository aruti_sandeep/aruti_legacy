﻿Option Strict On

Imports System.Collections.ObjectModel
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Runtime.InteropServices
Imports System.Runtime.InteropServices.ComTypes
Imports System.Runtime.Remoting
Imports System.Security.Policy
Imports System.Windows.Documents
Imports System.Windows.Navigation
Imports iTextSharp.text.log
Imports Microsoft.AspNet.Identity
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports Org.BouncyCastle.Ocsp

Public Class Register
    Inherits Base_General

    'Dim Email, ConString, ActivationUrl As String
    'Dim message As MailMessage
    'Dim smtp As SmtpClient
    Dim objApplicant As New clsApplicant
    Dim mintCompanyunkid As Integer = 0
    Dim mstrCompCode As String = ""
    Dim mstrCompName As String = ""
    Dim mstrUrlArutiLink As String = ""
    Dim mstrContinueLink As String = ""
    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-893
    Dim mdicQuestions As New Dictionary(Of Integer, String)
    Dim mdicAnswers As New Dictionary(Of Integer, Boolean)
    Dim mblnIsNidaPopupShown As Boolean = False
    'Dim mJObject As New ansresponse
    Dim mblnIsNidaOtpPopupShown As Boolean = False

    Dim mstrNidaFName As String = ""
    Dim mstrNidaMName As String = ""
    Dim mstrNidaSName As String = ""
    Dim mstrNidaGender As String
    Dim mstrNidaDoB As String = ""
    Dim mstrNidaMobile As String = ""
    Dim mstrNidaTin As String = ""
    Dim mstrNidaVillage As String = ""
    Dim mstrNidaPhoneNum1 As String = ""
    Dim mstrNidaApplPhoto As String = ""
    Dim mstrNidaApplSign As String = ""
    'S.SANDEEP |04-MAY-2023| -- END

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                'Sohail (27 Oct 2020) -- Start
                'NMB Enhancement # : Allow to access login page with simple URL and without encrypted text in URL.
                If Session("companyunkid") Is Nothing OrElse Session("CompCode") Is Nothing OrElse Session("CompCode").ToString = "" OrElse Session("Vacancy") Is Nothing OrElse Session("Vacancy").ToString = "" Then
                    Dim strVD As String = Request.ApplicationPath.ToLower.Replace("/", "")
                    Dim blnIsArutiHRM As Boolean = True
                    If strVD <> "arutihrm" Then
                        Dim objCompany As New clsCompany
                        Dim ds As DataSet = objCompany.GetCompanyByVirtualDirectory(strVD)
                        If ds.Tables(0).Rows.Count > 0 Then
                            Session("companyunkid") = CInt(ds.Tables(0).Rows(0).Item("companyunkid"))
                            Session("CompCode") = ds.Tables(0).Rows(0).Item("company_code").ToString
                            Session("Vacancy") = "Ext"

                            Session("mstrUrlArutiLink") = Request.Url.ToString.ToLower.Replace("register.aspx", "Login.aspx")

                            Dim blnActive As Boolean = False
                            If objApplicant.GetCompanyInfo(CInt(Session("companyunkid")), Session("CompCode").ToString, blnActive) = False Then
                                'Response.Redirect("UnauthorizedAccess.aspx", False)
                                Exit Sub
                            Else
                                If blnActive = False Then
                                    ShowMessage("Sorry, This company is not active.", MessageType.Info)
                                    Exit Try
                                End If
                            End If

                            Dim objCompany1 As New clsCompany
                            Session("e_emailsetupunkid") = 0
                            objCompany1.GetEmailSetup(Session("CompCode").ToString, CInt(Session("companyunkid")))

                            blnIsArutiHRM = False
                        End If
                        objCompany = Nothing
                    End If
                End If
                'Sohail (27 Oct 2020) -- End

                Call FillCombo()

                If Session("image") IsNot Nothing AndAlso IsDBNull(Session("image")) = False Then
                    Dim bytes As Byte() = DirectCast(Session("image"), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes)
                    'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("img_shortcuticon"), Image).ImageUrl = Convert.ToString("data:image/jpg;base64,") & base64String
                    'CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("img_shortcuticon"), Image).ImageUrl = Convert.ToString("data:image/jpg;base64,") & base64String
                    img_shortcuticon.ImageUrl = Convert.ToString("data:image/jpg;base64,") & base64String

                Else
                    'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("img_shortcuticon"), Image).ImageUrl = "~/Images/blank.png"
                    img_shortcuticon.ImageUrl = "~/Images/blank.png"
                End If

                '*** "Object reference not set to an instance of an object." error was coming if register.aspx is visited directly.
                If Session("mstrUrlArutiLink") Is Nothing Then
                    Response.Redirect("UnauthorizedAccess.aspx", False)
                    Exit Try
                End If

                If Session("mstrUrlArutiLink").ToString.ToLower.Contains("/alogin.aspx") = True Then
                    'CreateUserWizard1.UserName = Session("admin_email").ToString'Sohail (20 Dec 2017) - Commented as we are allowing multiple admin emails now
                    'CreateUserWizard1.Email = Session("admin_email").ToString
                    'DirectCast(CreateUserWizardStep1.ContentTemplateContainer.FindControl("UserName"), TextBox).Enabled = False
                    'DirectCast(CreateUserWizardStep1.ContentTemplateContainer.FindControl("UserName"), TextBox).ReadOnly = True

                End If

                If (ViewState("mstrUrlArutiLink") Is Nothing OrElse ViewState("mstrUrlArutiLink").ToString = "") AndAlso Session("mstrUrlArutiLink") IsNot Nothing Then
                    ViewState("mstrUrlArutiLink") = Session("mstrUrlArutiLink").ToString
                    '**CreateUserWizard1.ContinueDestinationPageUrl = ViewState("mstrUrlArutiLink").ToString
                    mstrContinueLink = ViewState("mstrUrlArutiLink").ToString
                End If

                If Session("mstrUrlArutiLink") IsNot Nothing Then
                    'If Request.Cookies("LogOutPageURL") IsNot Nothing Then
                    '    Request.Cookies.Remove("LogOutPageURL")
                    'End If
                    Dim ck As New HttpCookie("LogOutPageURL") With {
                        .HttpOnly = True,
                        .Value = clsSecurity.Encrypt(Convert.ToString(Session("mstrUrlArutiLink")), "ezee"),
                        .Path = ";SameSite=Strict"
                    }

                    If Request.IsSecureConnection Then
                        ck.Secure = True
                    End If
                    Response.Cookies.Add(ck)

                    mintCompanyunkid = CInt(Session("companyunkid"))
                    mstrCompCode = Session("CompCode").ToString
                    mstrCompName = Session("CompName").ToString
                    mstrUrlArutiLink = Session("mstrUrlArutiLink").ToString
                End If

                If Request.Url.AbsoluteUri.ToString.ToLower.Contains("arutihr.com") = False Then
                    pnlPoweredBy.Visible = False
                Else
                    pnlPoweredBy.Visible = True
                End If
            Else
                mintCompanyunkid = CInt(ViewState("mintCompanyunkid"))
                mstrCompCode = ViewState("mstrCompCode").ToString
                mstrCompName = ViewState("mstrCompName").ToString
                mstrUrlArutiLink = ViewState("mstrUrlArutiLink").ToString
                mstrContinueLink = ViewState("mstrContinueLink").ToString

            End If

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-893
            mdicQuestions = CType(ViewState("mdicQuestions"), Dictionary(Of Integer, String))
            mdicAnswers = CType(ViewState("mdicAnswers"), Dictionary(Of Integer, Boolean))
            mblnIsNidaPopupShown = CBool(ViewState("mblnIsNidaPopupShown"))
            mblnIsNidaOtpPopupShown = CBool(ViewState("mblnIsNidaOtpPopupShown"))
            'mJObject = CType(ViewState("mJObject"), ansresponse)

            If ViewState("mstrNidaFName") IsNot Nothing Then mstrNidaFName = ViewState("mstrNidaFName").ToString()
            If ViewState("mstrNidaMName") IsNot Nothing Then mstrNidaMName = ViewState("mstrNidaMName").ToString()
            If ViewState("mstrNidaSName") IsNot Nothing Then mstrNidaSName = ViewState("mstrNidaSName").ToString()
            If ViewState("mstrNidaGender") IsNot Nothing Then mstrNidaGender = ViewState("mstrNidaGender").ToString()
            If ViewState("mstrNidaDoB") IsNot Nothing Then mstrNidaDoB = ViewState("mstrNidaDoB").ToString()
            If ViewState("mstrNidaMobile") IsNot Nothing Then mstrNidaMobile = ViewState("mstrNidaMobile").ToString()
            If ViewState("mstrNidaTin") IsNot Nothing Then mstrNidaTin = ViewState("mstrNidaTin").ToString()
            If ViewState("mstrNidaVillage") IsNot Nothing Then mstrNidaVillage = ViewState("mstrNidaVillage").ToString()
            If ViewState("mstrNidaPhoneNum1") IsNot Nothing Then mstrNidaPhoneNum1 = ViewState("mstrNidaPhoneNum1").ToString()

            If ViewState("mstrNidaApplPhoto") IsNot Nothing Then mstrNidaApplPhoto = ViewState("mstrNidaApplPhoto").ToString()
            If ViewState("mstrNidaApplSign") IsNot Nothing Then mstrNidaApplSign = ViewState("mstrNidaApplSign").ToString()

            If ViewState("hfPassword") IsNot Nothing Then hfPassword.Value = ViewState("hfPassword").ToString()

            If mblnIsNidaPopupShown Then
                popupNidaQuestions.Show()
            Else
                popupNidaQuestions.Hide()
            End If

            If mblnIsNidaOtpPopupShown Then
                popupNidaOtp.Show()
            Else
                popupNidaOtp.Hide()
            End If
            'S.SANDEEP |04-MAY-2023| -- END           

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    'Private Sub CreateUserWizard1_CreatedUser(sender As Object, e As EventArgs) Handles CreateUserWizard1.CreatedUser
    '    'Dim Email, ActivationUrl As String
    '    Try
    '        'Dim profile As UserProfile = UserProfile.GetUserProfile(CreateUserWizard1.UserName)
    '        ''profile.FirstName = CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtFirstName"), TextBox).Text.Trim
    '        ''profile.MiddleName = CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtMiddleName"), TextBox).Text.Trim
    '        ''profile.LastName = CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtLastName"), TextBox).Text.Trim
    '        ''profile.Gender = CInt(CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("drpGender"), DropDownList).SelectedValue)
    '        ''profile.BirthDate = CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).GetDate
    '        ''profile.MobileNo = CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtMobileNo"), TextBox).Text.Trim
    '        'profile.FirstName = CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtFirstName"), TextBox).Text.Trim
    '        'profile.MiddleName = CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtMiddleName"), TextBox).Text.Trim
    '        'profile.LastName = CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtLastName"), TextBox).Text.Trim
    '        'profile.Gender = CInt(CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("drpGender"), DropDownList).SelectedValue)
    '        'profile.BirthDate = CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).GetDate
    '        'profile.MobileNo = CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtMobileNo"), TextBox).Text.Trim
    '        'profile.Save()

    '        'Email = CreateUserWizard1.UserName
    '        ''Email = CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("UserName"), TextBox).Text
    '        ''message = New MailMessage(Session("username").ToString, Email.Trim())

    '        'Dim strBody As String = "<!DOCTYPE html>" &
    '        '"<html>" &
    '        '    "<head>" &
    '        '        "<title>#companyname# Email Confirmation!!!</title>" &
    '        '        "<meta charset='utf-8'/>" &
    '        '    "</head>" &
    '        '    "<body style='font-size:9.0pt; font-family:'Verdana', 'Sans-Serif'; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" &
    '        '        "<br/>" &
    '        '    "#imgcmp#" &
    '        '    "<p>Dear <b>#firstname# #lastname#</b>, </p>" &
    '        '        "<p>Welcome To <b> #companyname# </b> Online Job Application System.</p>" &
    '        '        "<p>We are glad To inform you that your Email <b> #username#</b> Is now registered With us.</p>" &
    '        '        "<p>Please click On the below link Or copy And paste the link To address bar To activate your account.</p>" &
    '        '        "<p> #activationlink# </p>" &
    '        '        "<br/>" &
    '        '        "<p><B>Thank you,</B></p>" &
    '        '        "<p><B>#companyname# Support Team</B></p>" &
    '        '        "<br/>" &
    '        '        "<p><b>Online Job Application Powered by Aruti HR & Payroll Management Software</b></p>" &
    '        '    "#imgaruti#" &
    '        '"</body>" &
    '        '"</html>"

    '        'Dim strSubject As String = ""
    '        'Dim reader As New StreamReader(Server.MapPath("/ArutiHRM/EmailTemplate/RegisterSuccess.html"))
    '        'If reader IsNot Nothing Then
    '        '    strBody = reader.ReadToEnd
    '        '    reader.Close()
    '        'End If
    '        'strSubject = strBody.Substring(strBody.IndexOf("<title>") + 7, strBody.IndexOf("</title>") - strBody.IndexOf("<title>") - 7).Replace("#companyname#", Session("CompName").ToString)
    '        'If strSubject.Trim = "" Then
    '        '    strSubject = Session("CompName").ToString & " Email confirmation!"
    '        'End If
    '        'CreateUserWizard1.CompleteSuccessText = "Your account has been successfully created.<BR>Activation link has been sent to you on your Email " & Email & ". <BR>Please Activate your account before login."
    '        ''CreateUserWizard1.CompleteSuccessText = "Your account has been successfully created. <BR> You can now login to this account and apply."
    '        ''message.To.Add(Email.Trim())
    '        ''message.Subject = strSubject
    '        ''ActivationUrl = Request.Url.GetLeftPart(UriPartial.Authority) & "/ActivateUsers.aspx?UserID=" + Server.UrlEncode(Membership.GetUser(CreateUserWizard1.Email).ProviderUserKey.ToString) + "&Email=" + Server.UrlEncode(CreateUserWizard1.Email) + "&lnk=" + Server.UrlEncode(Session("mstrUrlArutiLink").ToString)
    '        'ActivationUrl = Request.Url.GetLeftPart(UriPartial.Authority) & ResolveUrl("ActivateUsers.aspx") & "?UserID=" + Server.UrlEncode(clsCrypto.Encrypt(Membership.GetUser(Email).ProviderUserKey.ToString)) + "&Email=" + Server.UrlEncode(clsCrypto.Encrypt(Email)) + "&lnk=" + Server.UrlEncode(clsCrypto.Encrypt(Session("mstrUrlArutiLink").ToString)) + "&c=" + Server.UrlEncode(clsCrypto.Encrypt(Session("CompCode").ToString)) + "&i=" + Server.UrlEncode(clsCrypto.Encrypt(Session("companyunkid").ToString))
    '        ''message.Body = "Hi <B>" + CreateUserWizard1.UserName + "</B>!<BR>" &
    '        ''       "Welcome to Aruti!<BR>" &
    '        ''       "Please click <a href='" + ActivationUrl + "'>here</a> to activate your account. <BR>Thanks!"
    '        ''message.Body = strBody.Replace("#username#", Email.Trim()).Replace("#activationlink#", ActivationUrl)
    '        ''Message.IsBodyHtml = True
    '        ''smtp = New SmtpClient
    '        ''smtp.Host = Session("mailserverip").ToString
    '        ''smtp.Port = CInt(Session("mailserverport"))
    '        ''smtp.Credentials = New System.Net.NetworkCredential(Session("username").ToString, Session("password").ToString)
    '        ''smtp.EnableSsl = CBool(Session("isloginssl"))
    '        ''smtp.Send(message)

    '        ''Dim profile As UserProfile = UserProfile.GetUserProfile(Email)
    '        'Dim intUnkId As Integer = 0
    '        'Dim objApplicant As New clsApplicant()
    '        'If objApplicant.InsertActivationLink(strCompCode:=Session("CompCode").ToString _
    '        '                                    , intCompUnkID:=CInt(Session("companyunkid")) _
    '        '                                    , strUserId:=Membership.GetUser(Email).ProviderUserKey.ToString _
    '        '                                    , strEmail:=Email _
    '        '                                    , strSubject:=strSubject _
    '        '                                    , strMessage:=strBody.Replace("#username#", Email.Trim()).Replace("#activationlink#", ActivationUrl).Replace("#companyname#", Session("CompName").ToString).Replace("#firstname#", profile.FirstName).Replace("#lastname#", profile.LastName) _
    '        '                                    , strLink:=ActivationUrl _
    '        '                                    , intRet_UnkId:=intUnkId
    '        '                                    ) = True Then



    '        '    If objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
    '        '                                       , strCompCode:=Session("CompCode").ToString() _
    '        '                                       , ByRefblnIsActive:=False
    '        '                                       ) = False Then

    '        '        Exit Try
    '        '    End If

    '        '    Dim objMail As New clsMail
    '        '    If objMail.SendEmail(strToEmail:=Email _
    '        '                         , strSubject:=strSubject _
    '        '                         , strBody:=strBody.Replace("#username#", Email.Trim()).Replace("#activationlink#", ActivationUrl).Replace("#companyname#", Session("CompName").ToString).Replace("#firstname#", profile.FirstName).Replace("#lastname#", profile.LastName) _
    '        '                         , blnAddLogo:=True
    '        '                         ) = False Then

    '        '        objApplicant.UpdateActivationEmailSent(intUnkId, False, Email, DateAndTime.Now)
    '        '        CreateUserWizard1.ContinueDestinationPageUrl = "~/ResendLink.aspx"
    '        '        CreateUserWizard1.CompleteSuccessText = "Your account has been successfully created.<BR><BR>To Activate your account, Please click on Resend Activation Link option on Login screen to get activation link. <BR><BR>Please Activate your account before login."
    '        '        'e.Cancel = True
    '        '    Else
    '        '        objApplicant.UpdateActivationEmailSent(intUnkId, True, Email, DateAndTime.Now)
    '        '        'CreateUserWizard1.ContinueDestinationPageUrl = "~/Login.aspx"
    '        '    End If
    '        '    'If objApplicant.ActivateUser(strCompCode:=Session("CompCode").ToString, intComUnkID:=CInt(Session("companyunkid")), strEmail:=Email, strUserID:=Membership.GetUser(CreateUserWizard1.Email).ProviderUserKey.ToString) <= 0 Then

    '        '    'End If
    '        'End If
    '        Dim cuw As CreateUserWizard = CType(sender, CreateUserWizard)
    '        Dim strEmail As String = cuw.Email
    '        Dim strPwd As String = cuw.Password
    '        Dim strSQ As String = cuw.Question
    '        Dim strSP As String = cuw.Answer
    '        Dim ActivationUrl As String = ""
    '        'Dim createStatus As MembershipCreateStatus

    '        If Membership.GetUser(strEmail) IsNot Nothing Then
    '            'Membership.CreateUser(strEmail, strPwd, strEmail, strSQ, strSP, False, createStatus)

    '            'If createStatus = MembershipCreateStatus.Success Then

    '            Dim profile As UserProfile = UserProfile.GetUserProfile(CreateUserWizard1.UserName)
    '                profile.FirstName = CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtFirstName"), TextBox).Text.Trim
    '                profile.MiddleName = CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtMiddleName"), TextBox).Text.Trim
    '                profile.LastName = CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtLastName"), TextBox).Text.Trim
    '                profile.Gender = CInt(CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("drpGender"), DropDownList).SelectedValue)
    '                profile.BirthDate = CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).GetDate
    '                profile.MobileNo = CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtMobileNo"), TextBox).Text.Trim
    '                profile.Save()


    '                Dim strBody As String = "<!DOCTYPE html>" &
    '                "<html>" &
    '                    "<head>" &
    '                        "<title>#companyname# Email Confirmation!!!</title>" &
    '                        "<meta charset='utf-8'/>" &
    '                    "</head>" &
    '                    "<body style='font-size:9.0pt; font-family:'Verdana', 'Sans-Serif'; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" &
    '                        "<br/>" &
    '                    "#imgcmp#" &
    '                    "<p>Dear <b>#firstname# #lastname#</b>, </p>" &
    '                        "<p>Welcome To <b> #companyname# </b> Online Job Application System.</p>" &
    '                        "<p>We are glad To inform you that your Email <b> #username#</b> Is now registered With us.</p>" &
    '                        "<p>Please click On the below link Or copy And paste the link To address bar To activate your account.</p>" &
    '                        "<p> #activationlink# </p>" &
    '                        "<br/>" &
    '                        "<p><B>Thank you,</B></p>" &
    '                        "<p><B>#companyname# Support Team</B></p>" &
    '                        "<br/>" &
    '                        "<p><b>Online Job Application Powered by Aruti HR & Payroll Management Software</b></p>" &
    '                    "#imgaruti#" &
    '                "</body>" &
    '                "</html>"

    '                Dim strSubject As String = ""
    '            Dim reader As New StreamReader(Server.MapPath("~/EmailTemplate/RegisterSuccess.html"))
    '            If reader IsNot Nothing Then
    '                    strBody = reader.ReadToEnd
    '                    reader.Close()
    '                End If
    '                strSubject = strBody.Substring(strBody.IndexOf("<title>") + 7, strBody.IndexOf("</title>") - strBody.IndexOf("<title>") - 7).Replace("#companyname#", mstrCompName)
    '                If strSubject.Trim = "" Then
    '                    strSubject = mstrCompName & " Email confirmation!"
    '                End If
    '                CreateUserWizard1.CompleteSuccessText = "Your account has been successfully created.<BR>Activation link has been sent to you on your Email " & strEmail & ". <BR>Please Activate your account before login."

    '            ActivationUrl = Request.Url.GetLeftPart(UriPartial.Authority) & ResolveUrl("ActivateUsers.aspx") & "?UserID=" + Server.UrlEncode(clsCrypto.Encrypt(Membership.GetUser(strEmail).ProviderUserKey.ToString)) + "&Email=" + Server.UrlEncode(clsCrypto.Encrypt(strEmail)) + "&lnk=" + Server.UrlEncode(clsCrypto.Encrypt(mstrUrlArutiLink)) + "&c=" + Server.UrlEncode(clsCrypto.Encrypt(mstrCompCode)) + "&i=" + Server.UrlEncode(clsCrypto.Encrypt(mintCompanyunkid.ToString))
    '            ActivationUrl = ActivationUrl.Replace("&", "&amp;")

    '            Dim intUnkId As Integer = 0
    '                Dim objApplicant As New clsApplicant()
    '                If objApplicant.InsertActivationLink(strCompCode:=mstrCompCode _
    '                                                    , intCompUnkID:=CInt(mintCompanyunkid) _
    '                                                    , strUserId:=Membership.GetUser(strEmail).ProviderUserKey.ToString _
    '                                                    , strEmail:=strEmail _
    '                                                    , strSubject:=strSubject _
    '                                                    , strMessage:=strBody.Replace("#username#", strEmail.Trim()).Replace("#activationlink#", ActivationUrl).Replace("#companyname#", mstrCompName).Replace("#firstname#", profile.FirstName).Replace("#lastname#", profile.LastName) _
    '                                                    , strLink:=ActivationUrl _
    '                                                    , intRet_UnkId:=intUnkId
    '                                                    ) = True Then



    '                    If objApplicant.GetCompanyInfo(intComUnkID:=CInt(mintCompanyunkid) _
    '                                                       , strCompCode:=mstrCompCode _
    '                                                       , ByRefblnIsActive:=False
    '                                                       ) = False Then

    '                        Exit Try
    '                    End If

    '                    Dim objMail As New clsMail
    '                If objMail.SendEmail(strToEmail:=strEmail _
    '                                         , strSubject:=strSubject _
    '                                         , strBody:=strBody.Replace("#username#", strEmail.Trim()).Replace("#activationlink#", ActivationUrl).Replace("#companyname#", mstrCompName).Replace("#firstname#", profile.FirstName).Replace("#lastname#", profile.LastName) _
    '                                         , blnAddLogo:=True
    '                                         ) = False Then

    '                    objApplicant.UpdateActivationEmailSent(intUnkId, False, strEmail, DateAndTime.Now)
    '                    CreateUserWizard1.ContinueDestinationPageUrl = "~/ResendLink.aspx"
    '                    CreateUserWizard1.CompleteSuccessText = "Your account has been successfully created.<BR><BR>To Activate your account, Please click on Resend Activation Link option on Login screen to get activation link. <BR><BR>Please Activate your account before login."
    '                Else
    '                    objApplicant.UpdateActivationEmailSent(intUnkId, True, strEmail, DateAndTime.Now)

    '                    End If

    '                End If

    '                'End If
    '            End If

    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    'Private Sub CreateUserWizard1_CreatingUser(sender As Object, e As LoginCancelEventArgs) Handles CreateUserWizard1.CreatingUser
    '    Try

    '        Dim cuw As CreateUserWizard = CType(sender, CreateUserWizard)
    '        cuw.Email = cuw.UserName

    '        'Threading.Thread.Sleep(5000)

    '        If CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtFirstName"), TextBox).Text.Trim = "" Then
    '            'If CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtFirstName"), TextBox).Text.Trim = "" Then
    '            ShowMessage("Please Enter First Name", MessageType.Info)
    '            'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtFirstName"), TextBox).Focus()
    '            CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtFirstName"), TextBox).Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtLastName"), TextBox).Text.Trim = "" Then
    '            'ElseIf CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtLastName"), TextBox).Text.Trim = "" Then
    '            ShowMessage("Please Enter Surname", MessageType.Info)
    '            'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtLastName"), TextBox).Focus()
    '            CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtLastName"), TextBox).Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf CInt(CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("drpGender"), DropDownList).SelectedValue) <= 0 Then
    '            'ElseIf CInt(CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("drpGender"), DropDownList).SelectedValue) <= 0 Then
    '            ShowMessage("Please Select Gender.", MessageType.Info)
    '            'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("drpGender"), DropDownList).Focus()
    '            CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("drpGender"), DropDownList).Focus()
    '            e.Cancel = True
    '            Exit Try
    '            'ElseIf CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).GetDate = CDate("01/Jan/1900") Then
    '        ElseIf CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).GetDate = CDate("01/Jan/1900") Then
    '            ShowMessage("Please Enter Valid Birth Date.", MessageType.Info)
    '            'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).Focus()
    '            CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).Focus()
    '            e.Cancel = True
    '            Exit Try
    '            'ElseIf CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).GetDate >= System.DateTime.Today.Date Then
    '        ElseIf CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).GetDate >= System.DateTime.Today.Date Then
    '            ShowMessage("Birth Date should be less than current Date.", MessageType.Info)
    '            'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).Focus()
    '            CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtMobileNo"), TextBox).Text.Trim = "" Then
    '            'ElseIf CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtMobileNo"), TextBox).Text.Trim = "" Then
    '            ShowMessage("Please Enter Mobile No.", MessageType.Info)
    '            'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtMobileNo"), TextBox).Focus()
    '            CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("txtMobileNo"), TextBox).Focus()
    '            e.Cancel = True
    '            Exit Try
    '        End If

    '        If Session("mstrUrlArutiLink") Is Nothing Then
    '            'If Request.Cookies("LogOutPageURL") IsNot Nothing Then
    '            '    Session("mstrUrlArutiLink") = clsSecurity.Decrypt(Request.Cookies("LogOutPageURL").Value, "ezee")
    '            'End If
    '            Session("mstrUrlArutiLink") = mstrUrlArutiLink
    '        End If

    '        If Session("mstrUrlArutiLink").ToString.ToLower.Contains("/alogin.aspx") = True Then
    '            'Sohail (20 Dec 2017) -- Start
    '            'Enhancement - Multilple admin email registration.
    '            'If cuw.UserName.ToLower.Trim <> Session("admin_email").ToString.Trim Then
    '            Dim arrAdmin() As String = Session("admin_email").ToString.Split(CChar(";"))
    '            Dim arrAdmin1() As String = Session("admin_email").ToString.Split(CChar(","))
    '            If arrAdmin.Contains(cuw.UserName.ToLower.Trim) = False AndAlso arrAdmin1.Contains(cuw.UserName.ToLower.Trim) = False Then
    '                'Sohail (20 Dec 2017) -- End
    '                ShowMessage("Sorry, You cannot register with different email.")
    '                'Sohail (20 Dec 2017) -- Start
    '                'Enhancement - Multilple admin email registration.
    '                e.Cancel = True
    '                Exit Try
    '                'Sohail (20 Dec 2017) -- End
    '            End If
    '        End If


    '    Catch ex As Exception
    '        e.Cancel = True
    '        'S.SANDEEP [03-NOV-2016] -- START
    '        Global_asax.CatchException(ex, Context)
    '        'S.SANDEEP [03-NOV-2016] -- END
    '    End Try
    'End Sub

    'Private Sub CreateUserWizard1_SendingMail(sender As Object, e As MailMessageEventArgs) Handles CreateUserWizard1.SendingMail
    '    'Dim Email, ActivationUrl As String
    '    'Dim objApplicant As New clsApplicant
    '    Try
    '        ''e.Cancel = True

    '        ''Email = CreateUserWizard1.Email
    '        ''Email = CreateUserWizard1.UserName
    '        'Email = CType(CreateUserWizardStep1.ContentTemplateContainer.FindControl("UserName"), TextBox).Text
    '        ''message = New MailMessage(Session("username").ToString, Email.Trim())

    '        'Dim strBody As String = "<!DOCTYPE html>" &
    '        '"<html>" &
    '        '    "<head>" &
    '        '        "<title>#companyname# Email Confirmation!!!</title>" &
    '        '        "<meta charset='utf-8'/>" &
    '        '    "</head>" &
    '        '    "<body style='font-size:9.0pt; font-family:'Verdana', 'Sans-Serif'; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" &
    '        '        "<br/>" &
    '        '    "#imgcmp#" &
    '        '    "<p>Dear <b>#firstname# #lastname#</b>, </p>" &
    '        '        "<p>Welcome To <b> #companyname# </b> Online Job Application System.</p>" &
    '        '        "<p>We are glad To inform you that your Email <b> #username#</b> Is now registered With us.</p>" &
    '        '        "<p>Please click On the below link Or copy And paste the link To address bar To activate your account.</p>" &
    '        '        "<p> #activationlink# </p>" &
    '        '        "<br/>" &
    '        '        "<p><B>Thank you,</B></p>" &
    '        '        "<p><B>#companyname# Support Team</B></p>" &
    '        '        "<br/>" &
    '        '        "<p><b>Online Job Application Powered by Aruti HR & Payroll Management Software</b></p>" &
    '        '    "#imgaruti#" &
    '        '"</body>" &
    '        '"</html>"

    '        'Dim strSubject As String = ""
    '        'Dim reader As New StreamReader(Server.MapPath("/ArutiHRM/EmailTemplate/RegisterSuccess.html"))
    '        'If reader IsNot Nothing Then
    '        '    strBody = reader.ReadToEnd
    '        '    reader.Close()
    '        'End If
    '        'strSubject = strBody.Substring(strBody.IndexOf("<title>") + 7, strBody.IndexOf("</title>") - strBody.IndexOf("<title>") - 7).Replace("#companyname#", Session("CompName").ToString)
    '        'If strSubject.Trim = "" Then
    '        '    strSubject = Session("CompName").ToString & " Email confirmation!"
    '        'End If
    '        'CreateUserWizard1.CompleteSuccessText = "Your account has been successfully created.<BR>Activation link has been sent to you on your Email " & Email & ". <BR>Please Activate your account before login."
    '        ''CreateUserWizard1.CompleteSuccessText = "Your account has been successfully created. <BR> You can now login to this account and apply."
    '        ''message.To.Add(Email.Trim())
    '        ''message.Subject = strSubject
    '        ''ActivationUrl = Request.Url.GetLeftPart(UriPartial.Authority) & "/ActivateUsers.aspx?UserID=" + Server.UrlEncode(Membership.GetUser(CreateUserWizard1.Email).ProviderUserKey.ToString) + "&Email=" + Server.UrlEncode(CreateUserWizard1.Email) + "&lnk=" + Server.UrlEncode(Session("mstrUrlArutiLink").ToString)
    '        'ActivationUrl = Request.Url.GetLeftPart(UriPartial.Authority) & ResolveUrl("ActivateUsers.aspx") & "?UserID=" + Server.UrlEncode(clsCrypto.Encrypt(Membership.GetUser(Email).ProviderUserKey.ToString)) + "&Email=" + Server.UrlEncode(clsCrypto.Encrypt(Email)) + "&lnk=" + Server.UrlEncode(clsCrypto.Encrypt(Session("mstrUrlArutiLink").ToString)) + "&c=" + Server.UrlEncode(clsCrypto.Encrypt(Session("CompCode").ToString)) + "&i=" + Server.UrlEncode(clsCrypto.Encrypt(Session("companyunkid").ToString))
    '        ''message.Body = "Hi <B>" + CreateUserWizard1.UserName + "</B>!<BR>" &
    '        ''       "Welcome to Aruti!<BR>" &
    '        ''       "Please click <a href='" + ActivationUrl + "'>here</a> to activate your account. <BR>Thanks!"
    '        ''message.Body = strBody.Replace("#username#", Email.Trim()).Replace("#activationlink#", ActivationUrl)
    '        ''Message.IsBodyHtml = True
    '        ''smtp = New SmtpClient
    '        ''smtp.Host = Session("mailserverip").ToString
    '        ''smtp.Port = CInt(Session("mailserverport"))
    '        ''smtp.Credentials = New System.Net.NetworkCredential(Session("username").ToString, Session("password").ToString)
    '        ''smtp.EnableSsl = CBool(Session("isloginssl"))
    '        ''smtp.Send(message)

    '        'Dim profile As UserProfile = UserProfile.GetUserProfile(Email)
    '        'Dim intUnkId As Integer = 0
    '        'Dim objApplicant As New clsApplicant()
    '        'If objApplicant.InsertActivationLink(strCompCode:=Session("CompCode").ToString _
    '        '                                    , intCompUnkID:=CInt(Session("companyunkid")) _
    '        '                                    , strUserId:=Membership.GetUser(Email).ProviderUserKey.ToString _
    '        '                                    , strEmail:=Email _
    '        '                                    , strSubject:=strSubject _
    '        '                                    , strMessage:=strBody.Replace("#username#", Email.Trim()).Replace("#activationlink#", ActivationUrl).Replace("#companyname#", Session("CompName").ToString).Replace("#firstname#", profile.FirstName).Replace("#lastname#", profile.LastName) _
    '        '                                    , strLink:=ActivationUrl _
    '        '                                    , intRet_UnkId:=intUnkId
    '        '                                    ) = True Then



    '        '    If objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
    '        '                                       , strCompCode:=Session("CompCode").ToString() _
    '        '                                       , ByRefblnIsActive:=False
    '        '                                       ) = False Then

    '        '        Exit Try
    '        '    End If

    '        '    Dim objMail As New clsMail
    '        '    If objMail.SendEmail(strToEmail:=Email _
    '        '                         , strSubject:=strSubject _
    '        '                         , strBody:=strBody.Replace("#username#", Email.Trim()).Replace("#activationlink#", ActivationUrl).Replace("#companyname#", Session("CompName").ToString).Replace("#firstname#", profile.FirstName).Replace("#lastname#", profile.LastName) _
    '        '                         , blnAddLogo:=True
    '        '                         ) = False Then

    '        '        objApplicant.UpdateActivationEmailSent(intUnkId, False, Email, DateAndTime.Now)
    '        '        CreateUserWizard1.ContinueDestinationPageUrl = "~/ResendLink.aspx"
    '        '        CreateUserWizard1.CompleteSuccessText = "Your account has been successfully created.<BR><BR>To Activate your account, Please click on Resend Activation Link option on Login screen to get activation link. <BR><BR>Please Activate your account before login."
    '        '        e.Cancel = True
    '        '    Else
    '        '        objApplicant.UpdateActivationEmailSent(intUnkId, True, Email, DateAndTime.Now)
    '        '        'CreateUserWizard1.ContinueDestinationPageUrl = "~/Login.aspx"
    '        '    End If
    '        '    'If objApplicant.ActivateUser(strCompCode:=Session("CompCode").ToString, intComUnkID:=CInt(Session("companyunkid")), strEmail:=Email, strUserID:=Membership.GetUser(CreateUserWizard1.Email).ProviderUserKey.ToString) <= 0 Then

    '        '    'End If
    '        'End If

    '        e.Cancel = True

    '    Catch ex As Exception
    '        e.Cancel = True
    '        Global_asax.CatchException(ex, Context)
    '    Finally
    '        'objApplicant = Nothing
    '    End Try
    'End Sub

    Private Sub Register_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
            ViewState("mintCompanyunkid") = mintCompanyunkid
            ViewState("mstrCompCode") = mstrCompCode
            ViewState("mstrCompName") = mstrCompName
            ViewState("mstrUrlArutiLink") = mstrUrlArutiLink
            ViewState("mstrContinueLink") = mstrContinueLink
            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-893
            ViewState("mdicQuestions") = mdicQuestions
            ViewState("mdicAnswers") = mdicAnswers
            ViewState("mblnIsNidaPopupShown") = mblnIsNidaPopupShown
            'ViewState("mJObject") = mJObject
            ViewState("mblnIsNidaOtpPopupShown") = mblnIsNidaOtpPopupShown
            ViewState("hfPassword") = hfPassword.Value

            ViewState("mstrNidaFName") = mstrNidaFName
            ViewState("mstrNidaMName") = mstrNidaMName
            ViewState("mstrNidaSName") = mstrNidaSName
            ViewState("mstrNidaGender") = mstrNidaGender
            ViewState("mstrNidaDoB") = mstrNidaDoB
            ViewState("mstrNidaMobile") = mstrNidaMobile
            ViewState("mstrNidaTin") = mstrNidaTin
            ViewState("mstrNidaVillage") = mstrNidaVillage
            ViewState("mstrNidaPhoneNum1") = mstrNidaPhoneNum1

            ViewState("mstrNidaApplPhoto") = mstrNidaApplPhoto
            ViewState("mstrNidaApplSign") = mstrNidaApplSign
            'S.SANDEEP |04-MAY-2023| -- END

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objPersonalInfo As New clsPersonalInfo
        Dim dsCombo As DataSet
        Try
            dsCombo = objPersonalInfo.GetGender()
            'With CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("drpGender"), DropDownList)
            With drpGender
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objPersonalInfo = Nothing
        End Try
    End Sub

    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-884
    Private Function IsValidate() As Boolean
        Try
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() = "TRA" Then
                If txtNinNumber.Text.Trim = "" Then
                    ShowMessage("Please Enter National ID Number.", MessageType.Info)
                    Return False
                End If
                If txtNINMobile.Text.Trim = "" Then
                    ShowMessage("Please Enter Mobile Registered With Nida.", MessageType.Info)
                    Return False
                End If
                If objApplicant.IsNidaNumberExists(_nin:=txtNinNumber.Text.Trim(), _companycode:=Session("CompCode").ToString(), _companyunkid:=CInt(Session("companyunkid"))) Then
                    ShowMessage("Sorry, this Nida Number is already registerd with our system. Please login with the email address you have provided at the time of registration.", MessageType.Info)
                    Return False
                End If
            Else
            If txtFirstName.Text.Trim = "" Then
                ShowMessage("Please Enter First Name", MessageType.Info)
                txtFirstName.Focus()
                    Return False
            ElseIf txtLastName.Text.Trim = "" Then
                ShowMessage("Please Enter Surname", MessageType.Info)
                txtLastName.Focus()
                    Return False
            ElseIf CInt(drpGender.SelectedValue) <= 0 Then
                ShowMessage("Please Select Gender.", MessageType.Info)
                drpGender.Focus()
                    Return False
            ElseIf dtBirthdate.GetDate = CDate("01/Jan/1900") Then
                ShowMessage("Please Enter Valid Birth Date.", MessageType.Info)
                dtBirthdate.Focus()
                    Return False
            ElseIf dtBirthdate.GetDate >= System.DateTime.Today.Date Then
                ShowMessage("Birth Date should be less than current Date.", MessageType.Info)
                dtBirthdate.Focus()
                    Return False
            ElseIf txtMobileNo.Text.Trim = "" Then
                ShowMessage("Please Enter Mobile No.", MessageType.Info)
                txtMobileNo.Focus()
                    Return False
                ElseIf Question.Text.Trim = "" Then
                    ShowMessage("Security question Is required.", MessageType.Info)
                    Question.Focus()
                    Return False
                ElseIf Answer.Text.Trim = "" Then
                    ShowMessage("Security answer Is required.", MessageType.Info)
                    Answer.Focus()
                    Return False
                End If
            End If

            If UserName.Text.Trim = "" Then
                ShowMessage("Please Enter Email.", MessageType.Info)
                UserName.Focus()
                Return False
            ElseIf clsApplicant.IsValidEmail(UserName.Text) = False Then
                ShowMessage("Please Enter Valid Email.", MessageType.Info)
                UserName.Focus()
                Return False
            ElseIf Password.Text.Trim = "" Then
                ShowMessage("Please Enter Password.", MessageType.Info)
                Password.Focus()
                Return False
            ElseIf ConfirmPassword.Text.Trim = "" Then
                ShowMessage("Please Enter Confirm Password.", MessageType.Info)
                ConfirmPassword.Focus()
                Return False
            ElseIf String.Compare(Password.Text, ConfirmPassword.Text, False) > 0 Then
                ShowMessage("The Password and Confirmation Password must match.", MessageType.Info)
                ConfirmPassword.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
        End Try
    End Function

    Private Function Verify_Nida_Number(_ApiUrl As String, _nidanumber As String, _mobile As String, ByRef _exceptionMsg As String) As String
        Dim _finaloutput As String = ""
        Dim strUrl = ""
        Dim objNidaLog As New clsApplicant
        Dim _output As Object = Nothing
        Try
            strUrl = _ApiUrl + "?nin=" + _nidanumber.Trim() + "&mobile=" + _mobile.Trim()
            System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) True

            Dim httpWebRequest As HttpWebRequest = CType(WebRequest.Create(strUrl), HttpWebRequest)
            httpWebRequest.ContentType = "application/json"
            httpWebRequest.Method = "GET"


            Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
            Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                _output = streamReader.ReadToEnd()
            End Using


            'If _output IsNot Nothing Then

            '    Dim parsejson As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(_output.ToString())
            '    _finaloutput = parsejson.Item("nextStep").ToString()
            '    If parsejson.Item("questions") IsNot Nothing Then
            '        If TypeOf parsejson.Item("questions") Is JArray Then
            '            'Dim list = JsonConvert.DeserializeObject(Of IEnumerable(Of KeyValuePair(Of Integer, String)))(parsejson.Item("questions").ToString())
            '            Dim list = parsejson.Item("questions").Cast(Of JObject).Cast(Of JObject)().[Select](Function(o) New KeyValuePair(Of Integer, String)(CInt(o.PropertyValues().ElementAtOrDefault(0)), CStr(o.PropertyValues().ElementAtOrDefault(1)))).ToList()
            '            mdicQuestions = New Dictionary(Of Integer, String)
            '            For Each _item In list
            '                If mdicQuestions.ContainsKey(_item.Key) = False Then
            '                    mdicQuestions.Add(_item.Key, _item.Value)
            '                End If
            '            Next

            '            lblNidaQuestion.Text = "" : txtNidaQueryAnswer.Text = ""
            '            If mdicQuestions.Keys.Count() > 0 Then
            '                For Each _key As Integer In mdicQuestions.Keys
            '                    lblNidaQuestion.Text = mdicQuestions.Values(mdicQuestions.Keys(_key))
            '                    hdfNidaQestionTag.Value = _key.ToString
            '                    Exit For
            '                Next
            '            End If
            '        End If
            '    End If
            'End If

            If _output IsNot Nothing Then

                Dim jsonSettings = New JsonSerializerSettings With {
                         .NullValueHandling = NullValueHandling.Ignore,
                         .MissingMemberHandling = MissingMemberHandling.Ignore
                        }

                Dim json_data = JsonConvert.DeserializeObject(Of nidanum)(CStr(_output), jsonSettings)

                'Dim _txt As New TextBox
                '_txt.Text = "{" & vbCrLf & vbTab & """nin"": ""19860406141210000526""," & vbCrLf & vbTab & """mobile"": ""0688941461""," & vbCrLf & vbTab & """nextStep"": ""AnswerQuestions""," & vbCrLf & vbTab & """questions"": [" & vbCrLf & vbTab & vbTab & "{" & vbCrLf & vbTab & vbTab & vbTab & """qnId"": 4," & vbCrLf & vbTab & vbTab & vbTab & """qnDesc"": ""Name the primary school you graduated from / Taja jina la shule ya msingi ulipohitimu darasa la saba""" & vbCrLf & vbTab & vbTab & "}," & vbCrLf & vbTab & vbTab & "{" & vbCrLf & vbTab & vbTab & vbTab & """qnId"": 12," & vbCrLf & vbTab & vbTab & vbTab & """qnDesc"": ""Mention your mother's First name / Taja jina la kwanza la mama""" & vbCrLf & vbTab & vbTab & "}," & vbCrLf & vbTab & vbTab & "{" & vbCrLf & vbTab & vbTab & vbTab & """qnId"": 3," & vbCrLf & vbTab & vbTab & vbTab & """qnDesc"": ""Mention your District of residence / Taja Wilaya unaoishi""" & vbCrLf & vbTab & vbTab & "}" & vbCrLf & vbTab & "]" & vbCrLf & "}"
                'Dim json_data = JsonConvert.DeserializeObject(Of nidanum)(CStr(_txt.Text), jsonSettings)

                _finaloutput = json_data.nextStep.ToString()

                lblNidaQuestion.Text = "" : txtNidaQueryAnswer.Text = ""
                If json_data.questions.Count > 0 Then
                    Dim rnd As New Random
                    Dim _idx As Integer = rnd.Next(0, json_data.questions.Count)
                    lblNidaQuestion.Text = json_data.questions(_idx).qnDesc.ToString()
                    hdfNidaQestionTag.Value = json_data.questions(_idx).qnId.ToString()
                End If


                'Dim parsejson As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(_output.ToString())
                '_finaloutput = parsejson.Item("nextStep").ToString()
                'If parsejson.Item("questions") IsNot Nothing Then
                '    If TypeOf parsejson.Item("questions") Is JArray Then
                '        'Dim list = JsonConvert.DeserializeObject(Of IEnumerable(Of KeyValuePair(Of Integer, String)))(parsejson.Item("questions").ToString())
                '        Dim list = parsejson.Item("questions").Cast(Of JObject).Cast(Of JObject)().[Select](Function(o) New KeyValuePair(Of Integer, String)(CInt(o.PropertyValues().ElementAtOrDefault(0)), CStr(o.PropertyValues().ElementAtOrDefault(1)))).ToList()
                '        mdicQuestions = New Dictionary(Of Integer, String)
                '        For Each _item In list
                '            If mdicQuestions.ContainsKey(_item.Key) = False Then
                '                mdicQuestions.Add(_item.Key, _item.Value)
                '            End If
                '        Next

                '        lblNidaQuestion.Text = "" : txtNidaQueryAnswer.Text = ""
                '        If mdicQuestions.Keys.Count() > 0 Then
                '            For Each _key As Integer In mdicQuestions.Keys
                '                lblNidaQuestion.Text = mdicQuestions.Values(mdicQuestions.Keys(_key))
                '                hdfNidaQestionTag.Value = _key.ToString
                '                Exit For
                '            Next
                '        End If
                '    End If
                'End If

            End If

        Catch wx As WebException
            Dim err As String = ""
            If wx.InnerException IsNot Nothing Then err = wx.InnerException.Message
            err &= ", " & wx.Message
            Using response As WebResponse = wx.Response
                Dim httpResponse As HttpWebResponse = CType(response, HttpWebResponse)
                Using data As Stream = response.GetResponseStream()
                    Using reader = New StreamReader(data)
                        Dim text As String = reader.ReadToEnd()
                        err &= ", " & text
                        Dim parsejson As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(text.ToString())
                        If parsejson.Item("title") IsNot Nothing Then
                            _exceptionMsg = parsejson.Item("title").ToString()
                        End If
                    End Using
                End Using
            End Using
            objNidaLog.AddNidaRequestLogs(_ApiUrl, strUrl, err, txtNinNumber.Text, txtNINMobile.Text, UserName.Text)
            Global_asax.CatchException(wx, Context)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            If _output IsNot Nothing Then
                objNidaLog.AddNidaRequestLogs(_ApiUrl, strUrl, CStr(_output), txtNinNumber.Text, txtNINMobile.Text, UserName.Text)
            End If
        End Try
        Return _finaloutput
    End Function

    Private Sub SetNidaParameters(_object As ansresponse)
        Try
            If _object.firstName IsNot Nothing Then mstrNidaFName = _object.firstName.ToString()
            If _object.middleName IsNot Nothing Then mstrNidaMName = _object.middleName.ToString()
            If _object.surname IsNot Nothing Then mstrNidaSName = _object.surname.ToString()
            If _object.gender IsNot Nothing Then mstrNidaGender = _object.gender.ToString()
            If _object.dateOfBirth IsNot Nothing Then mstrNidaDoB = _object.dateOfBirth.ToString()
            If _object.mobileNumber IsNot Nothing Then mstrNidaMobile = _object.mobileNumber.ToString()
            If _object.tin IsNot Nothing Then mstrNidaTin = _object.tin.ToString()
            If _object.village IsNot Nothing Then mstrNidaVillage = _object.village.ToString()
            If _object.phoneNumber1 IsNot Nothing Then mstrNidaPhoneNum1 = _object.phoneNumber1.ToString()
            If _object.applicantPhoto IsNot Nothing Then mstrNidaApplPhoto = _object.applicantPhoto.ToString()
            If _object.applicantSignature IsNot Nothing Then mstrNidaApplSign = _object.applicantSignature.ToString()

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Function Verify_Nida_SecurityQuestion(_ApiUrl As String, _Payload As String, ByRef _exceptionMsg As String, ByRef _counter As Integer) As String
        Dim _finaloutput As String = ""
        Dim json As String = ""
        Dim objNidaLog As New clsApplicant
        Dim _output As Object = Nothing
        Dim iLine As String = ""
        Dim blnExceptionOccured As Boolean = False
        Try
            'json = _Payload
            'System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) True
            'Dim httpWebRequest = CType(WebRequest.Create(_ApiUrl), HttpWebRequest)
            'httpWebRequest.ContentType = "application/json"
            'httpWebRequest.Method = "POST"

            'Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
            '    streamWriter.Write(json)
            '    streamWriter.Flush()
            '    streamWriter.Close()
            'End Using

            'Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)

            'Using streamReader = New StreamReader(httpResponse.GetResponseStream())
            '    _output = streamReader.ReadToEnd()
            'End Using

            'If _output IsNot Nothing Then
            '    Dim parsejson As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(_output.ToString())

            '    ShowMessage("parsejson : " & parsejson.ToString, MessageType.Info)
            '    _finaloutput = parsejson.Item("nextStep").ToString()
            '    If mdicAnswers.ContainsKey(CInt(hdfNidaQestionTag.Value)) = False Then
            '        mdicAnswers.Add(CInt(hdfNidaQestionTag.Value), CBool(parsejson.Item("isCorrect")))
            '    End If
            '    If parsejson.Item("questions") IsNot Nothing Then
            '        ShowMessage("questions : " & parsejson.Item("questions").ToString, MessageType.Info)
            '        If TypeOf parsejson.Item("questions") Is JArray Then
            '            Dim list = parsejson.Item("questions").Cast(Of JObject).Cast(Of JObject)().[Select](Function(o) New KeyValuePair(Of Integer, String)(CInt(o.PropertyValues().ElementAtOrDefault(0)), CStr(o.PropertyValues().ElementAtOrDefault(1)))).ToList()
            '            ShowMessage("list : " & list.Count().ToString, MessageType.Info)
            '            'mdicQuestions = New Dictionary(Of Integer, String)
            '            ShowMessage("mdicQuestions : " & mdicQuestions.Keys.Count.ToString, MessageType.Info)

            '            'For Each _item In list
            '            '    If mdicQuestions.ContainsKey(_item.Key) = False Then
            '            '        mdicQuestions.Add(_item.Key, _item.Value)
            '            '    End If
            '            'Next

            '            lblNidaQuestion.Text = "" : txtNidaQueryAnswer.Text = ""
            '            lblNidaQuestion.Text = list.Item(0).Value
            '            hdfNidaQestionTag.Value = list.Item(0).Value

            '            'If mdicQuestions.Keys.Count() > 0 Then
            '            '    For Each _key As Integer In mdicQuestions.Keys
            '            '        lblNidaQuestion.Text = mdicQuestions.Values(mdicQuestions.Keys(_key))
            '            '        hdfNidaQestionTag.Value = _key.ToString
            '            '        Exit For
            '            '    Next
            '            'End If

            '        End If
            '    End If

            '    Dim iCounter As Integer = 1
            '    Dim strStyle = ""
            '    For Each iKey As Integer In mdicAnswers.Keys
            '        If iCounter > 5 Then Exit For
            '        Select Case iCounter
            '            Case 1
            '                If mdicAnswers(iKey) Then strStyle &= "<i class='fa fa-check' style='font-size:24px; color:green'></i> &nbsp;" Else strStyle &= "<i style='font-size:24px;color:red' class='fa'>&#xf00d;</i> &nbsp;"
            '            Case 2
            '                If mdicAnswers(iKey) Then strStyle &= "<i class='fa fa-check' style='font-size:24px; color:green'></i> &nbsp;" Else strStyle &= "<i style='font-size:24px;color:red' class='fa'>&#xf00d;</i> &nbsp;"
            '            Case 3
            '                If mdicAnswers(iKey) Then strStyle &= "<i class='fa fa-check' style='font-size:24px; color:green'></i> &nbsp;" Else strStyle &= "<i style='font-size:24px;color:red' class='fa'>&#xf00d;</i> &nbsp;"
            '            Case 4
            '                If mdicAnswers(iKey) Then strStyle &= "<i class='fa fa-check' style='font-size:24px; color:green'></i> &nbsp;" Else strStyle &= "<i style='font-size:24px;color:red' class='fa'>&#xf00d;</i> &nbsp;"
            '            Case 5
            '                If mdicAnswers(iKey) Then strStyle &= "<i class='fa fa-check' style='font-size:24px; color:green'></i> &nbsp;" Else strStyle &= "<i style='font-size:24px;color:red' class='fa'>&#xf00d;</i> &nbsp;"
            '        End Select

            '        iCounter += 1
            '    Next
            '    objlblResult.InnerHtml = strStyle
            'End If

            json = _Payload

            System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) True
            Dim httpWebRequest = CType(WebRequest.Create(_ApiUrl), HttpWebRequest)
            httpWebRequest.ContentType = "application/json"
            httpWebRequest.Method = "POST"
            httpWebRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)"


            Dim postBytes() As Byte = Encoding.UTF8.GetBytes(json)
            httpWebRequest.ContentLength = postBytes.Length
            Dim requestStream As Stream = httpWebRequest.GetRequestStream()

            requestStream.Write(postBytes, 0, postBytes.Length)
            requestStream.Close()

            Dim response As HttpWebResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)

            Using rdr As New StreamReader(response.GetResponseStream())
                _output = rdr.ReadToEnd()
            End Using

            'Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
            '    streamWriter.Write(json)
            '    'streamWriter.Flush()
            '    'streamWriter.Close()
            'End Using

            'Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)

            'Using streamReader = New StreamReader(httpResponse.GetResponseStream())
            '    _output = streamReader.ReadToEnd()
            'End Using

            If _output IsNot Nothing Then

                Dim jsonSettings = New JsonSerializerSettings With {
                         .NullValueHandling = NullValueHandling.Ignore,
                         .MissingMemberHandling = MissingMemberHandling.Ignore
                        }

                Dim json_data = JsonConvert.DeserializeObject(Of ansresponse)(CStr(_output), jsonSettings)

                'Dim _txt As New TextBox
                '_txt.Text = "{" & vbCrLf & "    ""applicantPhoto"": ""data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDACodICUgGiolIiUvLSoyP2lEPzo6P4FcYUxpmYagnpaGk5GovfLNqLPltZGT0v/V5fr/////o8v///////L/////2wBDAS0vLz83P3xERHz/rpOu////////////////////////////////////////////////////////////////////wAARCAKAAeADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDXPWkpT1pKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAU9aSlPWkoAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigBT1pKU9aSgAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAFPWkpT1pKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAU9aSg9aKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAD1ooPWigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAFPWkoPWigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAA9aKD1ooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAPWig9aKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKSgBaKQsB1OKia5iX+IUATUVSe/UH5RmgagmOQaALtFUxfoT0NPF5Ee9AFmimLIjDIYU+gAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAD1ooPWigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKazBRknAqjPenJEfT1oAtyzLEOTzVSS+OPlFUXkZjkmmEk0ASyXDv8AeY1HuJptJzQAuTRupucUbqAHZo3U36UtAEiyFehIqzDeuhwxyKpZoBoA2Y72Nzg8VZBBGQa58NzVmK6kj4zxQBsUVXgukkHJwanoAWiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAD1ooPWigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiikoAWoZ5hEue9JcTiJT61lyytIck0AOluHkPJ/CoCaVevvSbTmgYlIcVLhR2phUZNIBvWkp3TpSEd80ANoxTsUUAAFIRTqQ8UANJpfpR1oApgH1pwPajPFIKAJFYqcg1ftbvPyufxrNxTlPoaBG8CCMilrNtLna21zxWiCCMigBaKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAA9aKD1ooAKKKKACiikoAWikpaACikpaACikooAWikooAKKKKACiiigAqG4nEannmpJHCISTWPLIXckmgBJJCxJJzTFG402pYlNAxcdhQympNtKFpDsQ4yMmkxnqKmKUm2lcdiErTSpqzto2ZouFivtwKTGas+XR5VArFYrTTVox880wRDNMLFfBo6VZKU0x0XCxCOaXoKk8sijac9KAsR/WlAp+PalA70CGZxV+yuB9xqoHINCttPFMRvUtVbS48xArH5hVmgApaSigBaKSigBaKSigBaKSloAKKSloAKKKKACiiigAooooAKKKKAA9aKD1ooAKKKSgBaSiigAooooAKKKKACiiigAooooAKKSigAoopsjBELHtQBRvZ8t5Y6DrVHr0p8jbmLetNHBoGOjXmrSxgVDCMnNWaQxMCkpTTaQwNJQTRmkMWikFLQAtAoopgB5pCBS0lAhMUmKcaSkMTFGBS0UAIUBFIY8U8U4UxMqOuOtR1edAwqrIm0mmSLC5RwRWwjBlBHesIda07GTcm09qYi5RSUtABRRRQAUUUUAFFFFABRRRQAtFJRQAtFJRQAtFJS0AFFFFAAetFB60lABRRRQAUUUUAFFFFABRRRQAUUUUAFJRRQAUUUUAFVL99sYUd6tVn6g2ZAvoKAKZpy+9IvXNPA5pDJYR3qXNMj6U40igNJQaSgYYoAozS0gFxRikFOpgJRS0UAJRS0hoAQ0lLmk4oAQ0ZpaSkAoNOFR08UxMfUMwzzUtMf7tMRUI9KsWTlZgPWo2A7ikiIWQH0NMRtUUinKg0tAhaKSloAKKKKACiiigAooooAKKKKACiiigAooooAWikpaAA9aSlPWigBKKKKACiiigAooooAKKKKACiikoAKKKKACiiigArJum3zvWqxwCaxn5dj6mgAXpT1FNWnpnNIolQcUtA4oNIYhpKKKBhS5pBS0gFFGaKKYCiikzS5oAKKO1JQAhpMUtJQAhpKWikAlOFNpwpgOoYZFAoPSgkrkDvUfRuamIqJsUxM14TmJSPSpKgtD/AKOtTUxC0UUUALRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABS0lLQAHrSUp60lABRRRQAUUUUAFFFFABRRRQAlFFFABRRRQAlFFFADZTiNj7VkE1qznELn2rJoAevJqdRgVCn3qsCkUFNNONNpDEoxTgKXbSGMpadijFACZpaXFLimA2ilpKAFpKWkNIBCKTFOooAYRSGpMZpCKBjKBTsUmKBCilPSgUtMkhPeoj1qZuGqJ+tMTNGzOYBU9VbEYiP1q1TEFLSUtABS0lLQAUUUUAFFFFABRRRQAUUUUAFFFFABS0lLQAHrSUp60lABRRRQAUUUUAFFFFABSUUUAFFFFABRRRQAUlLSUAMnGYXHtWT6VsOMqR7VkHhiPegCSMZNTVHEOalpFAaaeKceKibmkMXzBSiQVGFo2UhknmCgSLUJBFJ1oAtB1PelyKqAnpUik4oAnpKZu5p2aAHUmKKaWxQMdSA81Gz8cVE0jCgRazSZHrVIyMeM0u4460WC5b3CkLCqys3c1JmgLkw5pajVqlFNCZFKMc1Ax5q1KMpVU85FMlmjZf6gfWrFQWgxbrmp6YgpaSigBaKKKAFooooAKKKKACiiigAooooAKKKKAClpKWgAPWkpT1pKACiiigAooooAKSlpKACiiigAooooAKKKKACkopHYIuTQAtZc67bhh71c+1YPI4qC82vtlQ57GgY2DnNSHrTIeEz60/NIYjdKZinHmkbCikNAOKUYqLf+H1ppnUHBf8qQ7krLUZXFNMqno9ALHoc0wuGcdacrVGSe4ozSGTg04VCpNTL0oAcTxUTGpMVE/FADCRTWOaaTmgc9KBChc1IseetMCn1pwIB5fFAEnl0Mopu4dpAaQue4yPagBelTxnK1EmGHFSJxTQmK/wB01UPX61bPQ1XhUtOq+9Mk04hiNR7U6m70BxuFOoELRRRTAWiiigBaKKKACiiigAooooAKKKKACiiigApaSloAD1pKU9aSgAooooAKKKKAEooooAKKKKACiiigAoopKACqt7JsUVaqjqHakxrcrxuW4ah1I6Hg9RTo1708jtSLaHjhAKBSn0pKBICQKryseTjJ6CpWGTSlMrQMjS2Ozc5ycVVVQrHcufarqOycHkVDMiscrwaaZLRCQjcBMH61aeDaileDUcESo25jk1YZ2boMfWhgkVSSOGGKQg9e1TsGIOcGoSGzg0ihydKsJ0qFBxU6jikMWoJasGonXNAFYrmk56AU5gQeKkjZsfdFMQRwl1yxwPSoFSPcd+7r2q4jlfvDj2qvcRAnch5PamhMgkRP4N341MYXWMMpz7UQwZILtgCrDyhRhOaGxJEMT9GHXoRVio0iwM9zTxnNIY7pVZi6SkJxnvVmmOPnzTEM3iNck1dtpN6dazpBlqt6fwCKSY2tC7RRRVEC0UUUALRRRQAUUUUAFFFFABRRRQAUUUUAFLSUtAAetJSnrSUAFFFFABRRRQAlFLSUAFFFFABRRRQAUlFFABVLUBwKu1BdLuj+lJjW5Uj+4DRvHmBcUqjCAVGD+/FSaFjvRijvQaZKDFIaWlxSKI8Ubc1JilAoAjCCl20+mk4piGtgCotuTmnkEnJpRSuAIuKeKQUopDHYpjCn5pppgQOuaE44NSGmmgQ7YDTTGacpxT85FMCDZSgYqUjNJtpDBTTsUgGKdQISmv2NOpkp4H1piI3HOamsKib7hqxZLgfhSQ3sW6KSiqMxaWkopgLS0lFAC0UUUAFFFFABRRRQAUUUUAFFFFACnrSUp60lABRRRQAUUUUAFJS0lABRRRQAUUUUAFJS0lABTJRmNh7U+kIyCKAKB6VCP9ZUzDkg1AyMrZ7VJr0LVFIOQKKCRc0oNNzRmkUPozSZpKAEJoxS0tADSOKbmlc9hQFoAUUuKAKcBSATFBFOpDTAYRTDwalxTGXNACAUvTpQp7GloAA1LSUUALRmkzR2oQmLUU5+UfWpBUNweBTF1HDlKt2oxHVNPu81dt/9UKEEiWiiimQLRRRTAWiiloAKKKKACiiigAooooAKKKKACiiigBT1pKU9aSgAooooAKKKKACkpaSgAooooAKKKKACkoooAKKKKAKs6Ykz2NRPgirsi7kIrPbOalloehytKKbF0px4NACZ5pM80HrQKBjhS02nCkMWkJ4opCKAEUZbJp+RUZO01BKXP3TigC3uFODCqCmVevzCp0lyOeKALORTS1R76Y0mBSAlLijcKpO8jH5BgUqCQdWpgWm6gindqhU54qUUAFBoNITQAUZoFIaYhwqNgDJz2p69KiZvmoJH7fSr0Y2oBVSAbnAq7TQMKKKKCRaWkopgLS0lFAC0UUUAFFFFABRRRQAUUUUAFFFFACnrSUp60lABRRRQAUUUUAFJS0lABRRRQAUlLRQAlFFFABRRRQAVWmgJJZfyqzRQBnqCpOQRTmq3KN0ZqoaRVxtFFIaRQuaUGm0ZpDJKKaDS0AI4yKhMZ7GpzTDQA1eKUgEUmMmigBNhx1oC+tSYpGGKAGH0poVj3p/GaUUACDFPBpBQaAHGmGjNJmgBwopKWmSxR0qHgtVhE3nb609LNFOdxPtTFcLVOrVZpAABgdKWglhRRRQAtFFLTAKKKWgAooooAKKKKACiiigAooooAKKKKAFPWkpT1pKACiiigAooooAKSlpKACiiigAooooASiiigAooooAKKKKAEPIxVNxgkVdqvcLg59aBor0lKaaakoKWkFLQMUUZpM00nmkA4nAozkUg5peBQAmKNppd2aTOKdguKSfSg5NG84pu4miwXDaaACDR0oDCgBQ1LmmmmA9qQElJQDxQaYC04U2nCgRPbr1NWKjhGEqSmQFFFFABS0lLTAKWkpaACiiigAooooAWiiigAooooAKKKKACiiigBT1pKU9aSgAooooAKKKKAEooooAKKKKACiiigApKKKACiiigAoopKAFpki7kIp9FAFBqZUsgwxqKkUhaQ0tIaQxpNMLYNONNxmgA88Cm+bk07y19KcqgdqAGh6NxPQVLgU9QPSmOxB82OlJ8w7VZ4owKAsVdzelNLH0NWzj0phx6UgsQCb1oD5pzID2pmAOBQK5IrcU/qtQjrUw6UAKKcvWm05aYi5GMIKfTV+6KWgkWiiimAUtJS0AFLSUtABRRRQAUtFFABRRRQAUUUUAFFFFABRRRQAHrRQetFACA5paKKACiikoAKKKKACiiigAooooASiiigAooooAKKKKAEooooAqz8SGoTxU9z96oOopFISg0UhPFIY00CkzRmgB1FJmloGG7FODj1qM000AT+YPWjePWqpFAouBYLj1pm6o6WgB+aTGaKM0CEwM1KvSowakXhaAF704daQcCgHmmIvL90UtIn3BS0Ei0UUUwClpKWgApaSloAKKKKACiiigBaKKKACiiigAooooAKKKKAA9aKD1ooAKKKKACkoooAKKKKACiiigBKKKKACiiigAooooAKKKKACiiigCtddqq5wauXP3RVM0mUhevNIaQHBpc8UgGEc0UpNMDc0DHUUuM0YoGIaaQalC0u2gCDFGKmxSFRQBFilp+2kIoENopcUhoAAOam7Co0HenM2BTEKxoTrUYOakWgC/H9wU6mRH92KfQSFLSUtMApaSloAKWkpaACiiigAooooAWikooAWiiigAooooAKKKKAA9aSlPWkoAKKKKACiig0AFJS0UAJRmiigAooooAKKKKACiiigAooooAKKSigCK5H7v8apGr04zEaomkykIaaeKdSUiiJ2GMd6i3nPFPmHeoO9MgtxnjmnBxmqquQKUPilYq5dDClLVTEhFO82gLlgsKN1VfMpRJQFywTSE1AZeaGkz0oC48uBTS+ahLUA07EtlkNgYpvLHmmKCalUYpDQqjFSLTKetAy7B/q6kqK3+5UtMgKWkpaYBS0lLQAUtJS0AFFFFABRRRQAUUUUAFLSUUALRRRQAUUUUAB60lKetJQAUUUUAFFFFABRRRQAUlFFABRRRQAUUUUAFFFFABSUUUAFFFV7mcR8UAJNOpyi8nvVdqreYGuN2e9WDSZSEpO9LSUihrrkVVZCDzVymOmaExNFSkqV48VHjmqJEzijdQRSYoELmgGkxS0AGaM0YpQKAEHNSKuaFWpFXFK40hyjFPBpBSgUihad0pBSigYSzPCqshxzVm2uDKuWxVK65i/Go7aQrxVIhm0KWoLeTeMHrU9AgpaSigBaKKKAFooooAKKKKACiiigAooooAKKKKAFopKKAFPWkpT1pKACiiigBKKKKAFpKKKACiiigAooooAKKKKACkoooAKKKKACs24DOWIGa0JD8pqt5fmd8KKaAyhwR9auBs1Fcx+UxxyDSRtxUspE9JSBqWkUFIaWkIpANPNRlBmpKKYiBkpuw1YK000XCxDso2VLtzTglFwsQ7KeFFP20AYouFhAKeBQBS4pDADHSloFLQAUZpKazcUAMnbK4ptnE0khx0FMfLHC8k1et90EYBTr1NWiGPjBicZ6VdFQMBIuRT4WymD1FNkktFJS0hi0UUUAFLSUUALRRRQAUUUUAFFFFABRRRQAUUUUAB6mig9TRQAUlFFABRRRQAUUUUAFFFFABRRRQAUUUlABRRRQAUUUmRQAyTpgVFM3lpipAwd8DtVe75P0qkJlSZjICDUCEqcGp2GaDbuV6Y9zRJAmIpp4NQcq21uCKkU1maIlBopoNOpDENJinUUANxRinUUANxS4pcUUDGmkxSmkoEOFLSCloGFJmgmmsaBCk1E7UM1NVSzZxmmlcTZJGCmeMMe9SpKy+49DURBHX9aUVqkZNl2JhkFfunt6VIDsb2NVrcHaT2zViUZxSGTgg9KWqaS7ODSG+2sQy0rAXaWoo5ldQemfWpKQxaKKKAFopKKAFooooAKKKKACiiigAooooAD1NJSnqaSgAooooAKKKKACiiigAooooAKKKSgAoopjyBR6n2oAczBRkmoTcDPC8UxlMmGYnr0AqVYgByKqwitPcN0XIFNWU7dueasOqMMZGaohWWdlPrxTQMu2/ALGo5fmJNPU4gIHaolO5aAGxqFy5GcdPrT1haT5nY07YTtX8TUkjbB6UMexTuoMAMOcd6rK1WZJSxOBx71WI5qWgTJVNPFQKakU1maElFIDS0DClpBS0AFJRSE0ABpBRRmgBaCeKTNNLUCFJqNjQTSxx7zls7aaQXGqpkPsKu20SqMsRn0pgdEOAvHpSqEc/ISD6GtErGbdyd4gR0z7GqskWzkdP5VYjl2/K/X1pbggRlqdxNEcZxEwHXrVhDuj3GqNsxMmT0PBqwj/OyjoKQEMhwxqJo9xznrUzRkyDHfrUhhwSAfzFUIYMkgKDheBVpHKgZzTEKRDBPNSF42Qc1JWth4lU96eDVfMWMbhSGPjMbkfjSsItUVTE8kRxIMirMcqyDKn8KGhj6KKKQBS0lFAC0UlLQAUUUUAB6mkpT1NJQAUUUUAFFFFABRRRQAUlFNZwvU0AOpCwFQSuxX5WUfjVUtOpzyf1p2AuPlujEU0R9yxNVxcv6fpSnzpB049+KYWLQkQDqOtDuWBC8D1qukYRf3jDr0FLK+UIHAosGg0eXu5YkiomYbiytupE6MfaockHIqrCuXJDsiAqNG+YehqOWbegpiPg0gNRQOWqnO5ZsVYR8xfrVKQ7cmhAyORwg96ZBJulAJxmoZGLNzSDilcC7cwGMBgPxFRq1XIi7QLvHUY5qpLEYnODke3apaLTHq1OzUSmnA1BRJxRSCloGJSY96WigBMUcCjGO9HHegBCfamMadjJwKQAAnOOKaVyW7DVQkgnpU7SkDBOFHYU1ZN5G1cDpUMh2zH6VaViG7jZLks+QMVLG+4ZHWqZp8TMp4ppiNJWEow3D9jTJCSgRjwDzUSnOCKSdyzEUwuHmcgLwBUwbbPk9DVUVOJFMah+oNAItrJH5mMjNSM64PIqkPJB3B2PtTlJagNB8kkat9wk+5o81fKzsFQzdRSg/6OfrQFx4eJ+CCp9c0bnhYHOVNValilAGx+Vp2BMtF0nXaeD2qLy5Y24Bz6igRgkNE+fY1ZRpORz1pDsugJOUXEpGfQVMkiuODVeRMglsA0RHBwNp9waTQFuiohKOhpRMhOM4+tTYCSigHPSigApaSigBT1NJSnqaSgAooooAKKQsB1NNLjHWgB1RvMi98mopJ1PG7FQkxnq5P0FNICYys5wCFFQTybPlBye5qT5QAVzVO4P7w1Qh0jfu196jWR16MafJwqj2qOmImFzJ7flTXmdurGmdqQUCJ4vu/jU0n3DUMPQfWrRiyhz+QoYyrHyHx6UiwsRliF+tXIosZwAv86cEC89T6mlcDMlhaP5s5FMFakhQghyKpCFVkJJ+Qc0ATxtt2Ke61Dcrhc5wafBmSXd2FPuFyrUDZlZ55p4Td90GlC/Nwv51YSNmHLYHtSAs2Mokj8t/vCkuf3ZORkEUkcSocr19afKvmgA0WAoj2qRTmneTJEdwG4UMo+8vHtUuJSkFGaQUuKgsXNGaTFGKAAmkUFjgU4KGU7TkjtSxl0OSnWqURXB5PKXaiHf3NVSHJ5BzV3zwTgLz61GbhScFeKsizYkUZC+9ILcrkuclqd9pw3yrkUhuVY8jFArMi8nyz0DCgRqOTkY7VajAbnNQv+9nwD8opiCFNzd/WmTD5t3r1+tWY2XcRmkePOSeh/SgZU7UnU1IYnJIHOKdHbsW+b5RTEJFGzngVeSDA5P6U6JokXCkcVOHUjrSuOxTmgzj5sfWoZFMcG0+taLYPoahlT5cAZHcUXAzKBUzwHkpyPTuKj8tv7ppiBCdw+tWlY5bk9ahSF8jirSQNk5NAEbHINRRnDirjQYU9fyqssLCQEcigYyRyJDg0glf+9n60kit5h4PWk2n0NAiZJnU5B/CrkVyr8Hg1nhWH8Jp2D3BoauFzUBB6UtUY5GTgfrU6T/3hUWHcsN1NJTHlAcqBk5pO2XP4UrDB5lQepqD7QzHjgU5zGTgITQEjAyQR9apWDUrtMxP3qY0jdzVryUPTFNNsD2p3RJTJzT4kLuAKnFqO+fzqRQIwRGuT607gkSiMKuKqTxBn5H4ipXkdDzmlilVznIzUjsV5IAWA3HgdhQLUf7R/CrckiKeWFR/aU96dwsQNaj/AGhSC19z+VT/AGlPel+0J70XDlY6GAKBxj61K7Ki88VGJSxwo7U2VfkO9gtIdhyzqWwMmq0lwxJA4+lOj8kP94k4phuEQny4x9TTsF0hFhdhliEHvTXiXosoPtUckryHLGmUWFzMvqFjQEdutQXEpAyDyf5VGJsJ8x6dB61LZR+bIztzj1o2ArAcbv1qVZVA5zUl4oVsLxkc4qslyU4KgikmBKbr+6v50zz3ZvvY+gpwuk/55CnpcozgeUKYXJUcleWNQGRlkOBnn0q1HKpGdoqKW52sQEFIaZE5bumD6ikB9aluLh9qkYGRVYyu3DHik4jUiXOKYzE/KKQNSoM89ycClFDkx8afkKdJMycA5p+Aq/Sqy/vJeaogneZBGA68nrioikO3cGP0qOY5kPtUZpiLRS3cfK2DULW56h1I+tREUhzQO5YVeeHz9Kk8uSABivB6mmxERPH6EZNaMm1oTnpipbsBnMCkm9funmrSSq6eh9KqxShRscZQ0uxAcmUY7AUx3Lf7oDsTUTNEx+aQj2FVnYdF6UyqJL6JEVbbKanCcDDg1nx/6p6tDoKVh3YT70KkdPUVIsqscE4IqEyFZVU8qeoprRKzEpIPoaB3vuWyit1H403Yo6/zqv5cg6Ov50hUfxyj8KBWRY3xoeop6zKWIwapb4FPQsanSRC5/djpRYNCz5ikdaQBSc1GdjKcErUUaMGGHBH1pD0J2jGe9Gwe9QOj7zhx+dN8uX+8P++qAsizsX/Jo2rVXypO7L+dIQq/ek/KgLIugJkcilMasKrI0Z28tUwB6owNAaCuQJGCDLZpvy9XfJ9qgmmPnso4G4imIeSPSnYVyW4lwuE4FRF2EQIJpWG5SKhU5jIPagB4m9R+IqQS56SY+tVc0Zp2C5bMwA5YsfQVEZWZW5wPQVFnihTw30osK5P5vGG5UiiKD5wVdSKrucr+FLb/AH/wpDTsWHEQYl5Mn0WmeZAOiE/U1XJ+Y0U7Bdk/mQd0I/GlEkI6IT9TVanJ94UWFcvCU8hQFHtUc3+rPNKnf60k3+rNAEUP3z9Kib7xqSD75+lRN940AxKKKACTgDNADimYie+adbXHkggjOanjhYxgNgc96imtyj5HIPpSeoCBjPNk96rMPnbHrVyQrbxYHMjD8qpigBKVSVYEUpGaTpQBYjc4+tJLk80kRyhHpzU+N65wKBkRO+Aeq1AamZTEc/wmoWHPHSgRJFG7qWAzU6LhsY+6KnhgCxr2IGcinhDt+Zc57igZWmOE+tRQ8Fm9BViaNWIAbB9DTDE8cB4zn0oDoVDyc0nenYOcYo8qQ/wmmIZQq7jipRbyH+GpordlBJxnoOaQET/MAQOgxSb324LHHpVlkMO0sOO9QzoFAZTlTQBEBkH2pBUsUbMnpnuaf5C95P0pgQUVObc/wMGphhkH8BoEOj/1L/UVaHSoI4pPKI2nrVgqwHIoGV25uPoKrk8mp15uDUDfeNCAXNJmkooAUdatx/fP0FVB1q3H98/SgB7fdNVoz861aPQ1UXhx9aBjpuJDUeT61JP9+oqBDsn1pM0lFMCxF/B+NT96gjP+rqakBXuP+PiT/eNPU/OD/eFE0LmeQnAG49TTkjG1cuODQA6qx+WQ+lXhGP735CmmH58qnPq1IaIUgHcEn0p/2cf3B+dThdoznJ7mmtOi8Z/Ki4WIWtxjlD+BqEwgbtmfoatieM/xY+tKQHHH50XFYz3+4adbfeY+1TTQ5HHBz0pkEMi7sr2oGVz1NFSCB+rYUe5qRYF7kt9BTEV6fH98VZEA7Rn8TUscOGHyqP1ouBHECV4GeadJE5Q8Y+tWUTjlj+FK0aleRmlcCjDDtc5cdO1N8hMn75+gq+iqDwAKQkA9aLjZTEC9onP1NSLE2OAqD261NuX+8KQyxjqwouII41X3PvUxUMOQKq/aV3YUZp6ysV5OPpSKsyCW1IlJ6gjvUAt1U4ZufQCrU5fAK7jSRO/O5D+VMViEQpnhHapBCSMCJF+vNTCRSOuPrS7l9RQIrG1ZW3DHuBTo0IGGIFStNGvVhTRNG2cZz9KB2YPEjLgtn6VAEVOfLOPUmrQmXGRUbtHJ8rNjPSgB6TBlwRgnirA6cVUWEq3ynIUUrOUBPIpDsmTOoY8gGopIxkKhIxTIp2OSwBApUuEYkk4J9aAaaE8oscPj6jrTxAP7zfnTDcqCQATSG79F/OncXKyYQp3GfqakESYA2iqf2lyeCB+FTpKxc8k49qB8pM0YIwOnoah+zLnnp6VJ5jH/APVVRpJC38XWgEi7sVQOKQgVVuJCpUBmHFRrcOP4s/WkHKXDEjdRTfJH95vzqEXTd1B+lO+1f7H60XDlZOsS7epPPrUhTjg1XW4JTO0dcdan8wdxigVmVfKHnZ+6arvbPuOMHn1rRXBPrTGhQscqKdxMofZpO+B+NPS2JPJz7CrYijH8IoaWNOM/lRcLDFg29Ao/WphHz0U1D9qXspqVZgWIxRcfKxxhHoPzqIW6g/dFTeb7VB9p5wVPWlcaiwltwzfd/I1H9lHox/Gp5bhVIyDTPtSeh/KncXKxBb4HCr+PNMeH1jH1WpRcRnvj608Mp6EUXCxCkK/Jw3HtUwiHo1P3KMcin719RRcCJ0XzGOOc0qgbSABVSeaTznCk8MRwKSKSQtg7uaQ7GgOlMkdVGSwqGMSMvIP40SQsyEEgUBoI06srKnJqBIty7mcKKfHGiOC8gPsKhuGHmYXoKEDdth5gU/clBPoaWEvE+1+lVCcdKsW7+YNrn6GnYVy8wVlGcEUiRKM4z+dV5TJGARn8KdDcMVbOOKQ7EoiQfwjPvTuB7VSa4c9D+QpNsr9mP1oCxcMiDqwpFnTdwc1VFvJ32rT0iRWG6Xn0FAaFoTZHA/OmyTMFOMU1DCBwCee9K8kYQ/JQF0QxzOz4JJ47VGUlZjhGPPepI7kb8IgFQyXEhc/Nj6U0gchfIm/uAfjThby9yoqAzSH+M0m9j1Y0WFzMsiBVOXl/KrCvEhIUZNZueatR/e+oosFyeadghKgcVBFcyNJgmnOMoRVaM4lWgB8k7ByrKCKZujb1U0lwP3pqKmK5OIS33XQ1LHAUYM0gFU6TJ9aVh8zNJUiTgvkVVOGnOPuimq2UB7jiljGQT68UCLETMq5B680stwvCuuadHGX6cAd6ZcWb8sp3e1K4wZYfK4O0tUZt+PlkUio5+No9BURJAphcsBbdfvMWPtTvNgXpHn61VFFOwrlxLlN4AjAqdJ8lvlHWs+H/AFgq1COCfekwLBnIB4FVlu3LKNop8hwjfSqkX+tWgZamugHwUBFM86BvvR4qCc5lNR0xFs/ZT60mbYf3qrUUBcvIbcoMAjmpigI+RvwqlH9xP96p2YqCRSHcbEzpNg5FD3LhyMiiC5JfDAGmyXIDnbGufWiw2wzNL7D8qPJRfvyj8KgeZ36saZmnYXMWv9HB5ZjU6PFv4U5xWdVtP9Z/wGgVy1uiI6EVX8uEniQ9acehqmvMg+tIpFyeKNmGZcVH5Ef/AD2qG5/1lRUCuy0bdv4HDU3ypR/B+VQBmHQkU8XEmPvGiwczLcccmEyAMHnNT+W3qtU45XYJlj1qbJz1osO7Iri5ZZnCgDDGohdyZHIptx/x8Sf7xqKmSX1lcuQWPPIpSSepqvG3CN+BqY0gKb5Vznsakm/hb1FNnH7z60p+a3HqKBkbU6HtTW6U+HtTEWGmdEHOfrToZ0ZWJjGagm/1Y+tJb/dekMebrH3I1FMa4kb+LH0qKimIcXY9SadFzIKjqSH/AFgoAsRfc/Glk/1bfSki+5+NOk/1bfSkBWh/1gpj/fb60+H/AFgpsn+sb600DGUUUUCCrUfVfpVWrUfVPpQMlxwapjhx9augE9qr+RJvzjAz3pDQy5/1n4VDVy4gywJdRxUXkL/z1WmIgpDVjyF/56fpSGBf+eg/KgCBOpqxEPlX60sEBViVKvx0qcRjcuVK8fUUgLFt/qql71ULtBypDA9qQ3buAFjIz3qGmUVbn/XGoG6VNPnzTxURqyQFFFLTAfB/rBVqH/Viq0AO8n2q1CD5a8dqGA2Y4jNQQ8zCrE6nyjgGoYFIm5B6UhkUv+tb60ynyA+Y3HekEbHop/KmISipBBIf4aDBL/dNAiSLon1NSSH5G+lNjQrsBBHBpZf9W1IZDb/638Kjk/1h+tS2/wDrfwqJ/wDWN9aaBjaKWjBJ4FACCraffX3WoFhkPRTVpIn+TjtigB1VBxKPrV/y29KqSxOJCQp60hkdx/rKiqe4U5DYPIqCmIKKKKALEP3V+tWarQ/dT61YFAFS5/4+JP8AeNR1ZlhLTyMx2ruPNPSAY+VM+7UAVouQy/jVlTlQalWJgf4PyqRIyBg7fypAUbhcgGmwAkMuDV942KnG38qakbA8vx7CgZGsT+WPlTHoRTDDjBA2n0q0WRflLAfU0uA2KLiM6b7mPei3+49W54gw+YfiKjihUK2GOPpQPoU6KtLAvZXb68CplhI6Ki/rTEUVRmPCmpooJN4O3FWhEe7n8OKckShu5+ppXAjjhcLyO/rSvGxQ8dqnWNQOBSmNcHigChFAwcEso/GnGFS54Zz7cCrSRoGGFFK2AeuKLjZWEPpEg+ppfJP9xKlMsY6sKTzo/wC8KLisRiE/3UqZY2BH3Rx6U3zo/wC8KlEqZ69qLjsLtPdvyqIwruycnn1qXzFqBrgA4Ck80hpMkeNMj5RQFA6AVFPcMpGAKgN0/wDeA/Ci4crLmB6U1gMdBVP7S/8AfFIbp/Y0XDlZbQKpPQE1KPv/AIVQy8oDbOPap0YhzyRgUwsTyxLIuGFMSFVIAJ496DIQp5FRx3BLAECkHKxzRZlPzGk8rp835ike4CyHKmk+1LnlSKdxcrH+UexX8qPKb1X8qT7TH6mj7SnvQFmPSNxn5h09KlVW2jLfpUSXCHPX8qlWVSBSCzEkRiv3sfhTY43D8vn8Kc8qBeTSJMhY4NA7MjMb7j84H/AaXyyersaRriMHqT+FMN0OymncVmSeSnv+dHkr2LD8ah+1f7P604XSnqpFK4+Vk4Rh0OeO9RyxAoQVI+lTI6seDTz92gRQhhRZM7j09KGhTecI7c1dUfNTXIBOSBTuDKwiPaNF+vNPETd2A+gpxljHVxTGuUHTJouFh/kr3LH8aesSYXjp71XN0OyVItwSqnb39aVx8rLGxfSo3iXdnn86XzT6CopZyrDgUAose0eUGG/OoHgz1VT9ODUi3GYixWkSdHOM4PvRcTTKpgT+8y/UUeSn98/lV+jA9qdxFeKNAq4Vjg1aCnsAtA6U4so6mgZ//9k=""," & vbCrLf & "    ""applicantSignature"": ""data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAcFBQYFBAcGBQYIBwcIChELCgkJChUPEAwRGBUaGRgVGBcbHichGx0lHRcYIi4iJSgpKywrGiAvMy8qMicqKyr/2wBDAQcICAoJChQLCxQqHBgcKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKir/wAARCABMAUADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD6RooooAKKKKACiiigAooooAKKKKACiiigAooooA4DSfGmquvji1ubDzbzw5PK0Cf8942DSxD/AL52itT4f+Ml8beBLTxHJbC0W4MuYy2QgSQrnP8AwHNZ9pcx/wDC5Nf0Y7FW90W2uGx95mDyxk/livPoLy48I/svz6fYJIL6e9udKtl/iDPdSJ+e3NAFn4LWBn8bal4te7E48SxXkyDGNqxXYQfmCK6X45eK9a8MeEbMeGAft95dbQwGSEjjaRuPotaPhXw/F4W1vQdEgAIsvD8kbEd382LcfxOa6rUtDs9VvbKe9QSGzMhjU9PnQof0JoAZ4a1uDxL4X07WbXHl3tuswA/hJHI/A5Fa1ebfA8paeDdR0OLzMaLrF1ZDzOu0PuX/AMdYV6TQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHnurWsFl8e/D+pyL5bX+lXNkJP+ejqyyKv/fPmGvO9E1O68XQeFLCaQJ5Xje6knOP9aYd1wP54rvvilNcab4i8C61Dgx2uteRKPaaMxn9N1cF8KILc+P9HNqTLBNDe6mVP/LJpo7cg/kSPzoA9jl/5KZa/wDYHn/9HRV0Vc7L/wAlMtf+wPP/AOjoq6KgDzXwPIdO+MXj3RpJ9yzva6jDHjH30IkP6IK9KriBpTwfHcaoqKsV14caHI6l47lSc/hItdvQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHmH7QEE//AAqmfU7Obyp9JvIL2M+pDbP/AGp+lc58GdFn0r4l6zAf3kVnoenQNLj+Iwxtj8v5V2Pxv2H4R6mkhX95PaKAf4v9Ki4/LNL4NVY/i18QYo1CrG+nAAdh9loA3pf+SmWv/YHn/wDR0VdFXlWi+PYtW/aG1PQXXZFY2Ulratj/AFkgMbyj8MV6rQBz9z/yUfTuf+YTd/8Ao63roOtec6Zc3uq/tAa+Q4Nlo+jQWgRT0kmfzc/X5CPwFTfBm/1C7+HqWuszedf6beXFjO+ckmOQjmgD0CiiigAorIsfEdhfalrFlHJsk0iVIrln4ALRhxz9DVmz1jTr+QxWV9b3EgGSsUoY4/CgC9RRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHiv7SD3k3h3R7Kzm8pGmuLyT3NvA0oH/jpqhD8StP8ABHiTxvq2rust9cQaTNFbBsNOTaoGx9N2a2vi5Pp0OvGbXpgunQeHryMrn5vNneOFSo9cMfwrwfT7RNf8K634m1vzH1azitdIsbcj/XNJCYQT7hRu/CgCb/hKdfsPFGleL7jSpI/+Jlda15mMedbymFWH0AGP+BV7Bd/tIadZeGzeXGlvHqIvPINiW+YJ5YcSfTnFO+I/h/TtNtfh/b61IbaxEcmi3e3hVWa3xk/R0U/hXiehfCfxJ8QGvdV0IefYJdtBFPcNgyKuMH3+UigD6K+BukXQ8K3PivVrsXWpeJpFuppB2RRhF/D5vzrP+B93LH4h8e6ZPCyFNbe9jc/xpK7gY/79H86m+H2pD4ft4n8J6zcqun+G447mxllYBpoJRJIceuCpFYXg3V7vTNb8I+LNSCWum+JLGWxuSTgC4M000ZPtjgH3oA94orzn4hfEO50dtF0zwb9l1PWNWuzFFGJA6hFXLE4+q/rV+9+IVtpHwotfFmpFPNnsY5RDGc7pmAGwfRjigDz3x/dt/b3j7QtBuNt5q0WlwFF6m4mk8th/35ArtfC3wn07wd4xttY0aeVYl097aeKSQt5jlkIf/wAdNebW8P2Twf4d8f6vHKt9rXi2C+1ElceREpnUD2UDFdl8Ufi6NCVdE8IR/wBp61cbP9UNyRo+4ZyO+QPzoA9boriPhfFqeneH7/Q9cujdXWk6g9uJnOWkRlSVSf8Av7j8K0PFnj3Q/B2n/atVulY+csIhhIaQuc4G3r2oA6eis/RdWg13RLTVLRXWC7iEqCQYYA+orQoAKKKKACiiigAooooAKKKKACiiigAooooAKKSigBaKSigBaKSigBaKSigBaKSigBaKSigBaKSigDmfFPgDQfF9/ZXeuW7TSWf+rAfAIyDgjvyBXg/grwfrWofH6+0vVI86Xomoi9mwvylkU/Z/zBB/Ovp6ue063jh8f67JGuHnsrKSQ+rZnXP5Kv5UAT+KPCuleMdHOma5AZrfeJAFbBDDoQataJolj4d0a20vS4RDa2y7Y0Hp71oUUAeY/Fv4SD4hm0urC7+w6hFiGSU5xJD8x2nHua6bW/h9oPiDwzp+g6jbs1lpxQ26xttKFUKA/kTXUUUAcR4d+EvhbwxrkOr6bbzG7gDCJpZd2zIwcfhXEf8ACkdYuPEWlxX+tJN4a0nUHuLSwOciNpfM2H9BXt1FAGdq2iWGt6JcaTqECtZ3EZjeMDGAfT0NYfhX4aeGvB9zPc6TaEzzqFd528w4ByMZ6V1tFAHlmu+BPGw8cazrXhPxHHY22qtC0kEi5wY4hH/So/CvwWtoL9tZ8aXLavrRv/tqy7v3YPBA2n0IP516vRQA2ONYowkahVAwAowBT6SigBaKSigBaKSigBaKSigBaKSigBaKSigBaKSigD//2Q==""," & vbCrLf & "    ""applicationId"": null," & vbCrLf & "    ""createdDate"": null," & vbCrLf & "    ""dateOfBirth"": ""1986-04-06T00:00:00""," & vbCrLf & "    ""email"": null," & vbCrLf & "    ""existingTIN"": null," & vbCrLf & "    ""fingerImage1"": null," & vbCrLf & "    ""fingerImage2"": null," & vbCrLf & "    ""fingerLabel1"": null," & vbCrLf & "    ""fingerLabel2"": null," & vbCrLf & "    ""firstName"": ""LOUIS""," & vbCrLf & "    ""gender"": ""MALE""," & vbCrLf & "    ""inv_result"": null," & vbCrLf & "    ""isCorrect"": 0," & vbCrLf & "    ""matchStatus"": null," & vbCrLf & "    ""middleName"": ""WILLIAM""," & vbCrLf & "    ""mobileNumber"": ""210000526""," & vbCrLf & "    ""nationality"": ""TANZANIAN""," & vbCrLf & "    ""nextStep"": null," & vbCrLf & "    ""nin"": ""19860406141210000526""," & vbCrLf & "    ""phoneNumber1"": null," & vbCrLf & "    ""phoneNumber2"": null," & vbCrLf & "    ""picture"": null," & vbCrLf & "    ""questions"": null," & vbCrLf & "    ""score"": null," & vbCrLf & "    ""signature"": null," & vbCrLf & "    ""surname"": ""RIWA""," & vbCrLf & "    ""tin"": null," & vbCrLf & "    ""tinApplicationDate"": null," & vbCrLf & "    ""tinGenerated"": 0," & vbCrLf & "    ""village"": null," & vbCrLf & "    ""wardPostCode"": null" & vbCrLf & "}"
                'Dim json_data = JsonConvert.DeserializeObject(Of ansresponse)(CStr(_txt.Text), jsonSettings)


                If json_data.nextStep IsNot Nothing Then
                    _finaloutput = json_data.nextStep.ToString()
                ElseIf json_data.nextStep Is Nothing AndAlso json_data.firstName IsNot Nothing Then
                    _finaloutput = "found"
                End If

                If mdicAnswers Is Nothing Then mdicAnswers = New Dictionary(Of Integer, Boolean)

                If mdicAnswers.ContainsKey(CInt(hdfNidaQestionTag.Value)) = False Then
                    mdicAnswers.Add(CInt(hdfNidaQestionTag.Value), CBool(json_data.isCorrect))

                    If CBool(json_data.isCorrect) Then
                        SetNidaParameters(json_data)

                    ElseIf CBool(json_data.isCorrect) = False AndAlso json_data.firstName IsNot Nothing Then
                        mdicAnswers(CInt(hdfNidaQestionTag.Value)) = True
                        SetNidaParameters(json_data)

                    End If
                End If
                If json_data.questions IsNot Nothing Then
                    If json_data.questions.Count > 0 Then
                        lblNidaQuestion.Text = "" : txtNidaQueryAnswer.Text = ""
                        Dim rnd As New Random
                        Dim _idx As Integer = rnd.Next(0, json_data.questions.Count)
                        lblNidaQuestion.Text = json_data.questions.Item(_idx).qnDesc
                        hdfNidaQestionTag.Value = json_data.questions.Item(_idx).qnId.ToString()
                        txtNidaQueryAnswer.Focus()
                    End If
                End If

                Dim iCounter As Integer = 1

                Dim strStyle = ""
                For Each iKey As Integer In mdicAnswers.Keys
                    If iCounter > 5 Then Exit For
                    Select Case iCounter
                        Case 1
                            objlblSt1.Text = iCounter.ToString() & ". "
                            If mdicAnswers(iKey) Then objlblSt1.CssClass = "tick-mark" Else objlblSt1.CssClass = "cross-mark"
                        Case 2
                            objlblSt2.Text = iCounter.ToString() & ". "
                            If mdicAnswers(iKey) Then objlblSt2.CssClass = "tick-mark" Else objlblSt2.CssClass = "cross-mark"
                        Case 3
                            objlblSt3.Text = iCounter.ToString() & ". "
                            If mdicAnswers(iKey) Then objlblSt3.CssClass = "tick-mark" Else objlblSt3.CssClass = "cross-mark"
                        Case 4
                            objlblSt4.Text = iCounter.ToString() & ". "
                            If mdicAnswers(iKey) Then objlblSt4.CssClass = "tick-mark" Else objlblSt4.CssClass = "cross-mark"
                        Case 5
                            objlblSt5.Text = iCounter.ToString() & ". "
                            If mdicAnswers(iKey) Then objlblSt5.CssClass = "tick-mark" Else objlblSt5.CssClass = "cross-mark"
                    End Select

                    iCounter += 1
                Next

                'Dim strStyle = ""
                'For Each iKey As Integer In mdicAnswers.Keys
                '    If iCounter > 5 Then Exit For
                '    Select Case iCounter
                '        Case 1
                '            If mdicAnswers(iKey) Then strStyle &= "<i class='fa fa-check' style='font-size:24px; color:green'></i> &nbsp;" Else strStyle &= "<i style='font-size:24px;color:red' class='fa'>&#xf00d;</i> &nbsp;"
                '        Case 2
                '            If mdicAnswers(iKey) Then strStyle &= "<i class='fa fa-check' style='font-size:24px; color:green'></i> &nbsp;" Else strStyle &= "<i style='font-size:24px;color:red' class='fa'>&#xf00d;</i> &nbsp;"
                '        Case 3
                '            If mdicAnswers(iKey) Then strStyle &= "<i class='fa fa-check' style='font-size:24px; color:green'></i> &nbsp;" Else strStyle &= "<i style='font-size:24px;color:red' class='fa'>&#xf00d;</i> &nbsp;"
                '        Case 4
                '            If mdicAnswers(iKey) Then strStyle &= "<i class='fa fa-check' style='font-size:24px; color:green'></i> &nbsp;" Else strStyle &= "<i style='font-size:24px;color:red' class='fa'>&#xf00d;</i> &nbsp;"
                '        Case 5
                '            If mdicAnswers(iKey) Then strStyle &= "<i class='fa fa-check' style='font-size:24px; color:green'></i> &nbsp;" Else strStyle &= "<i style='font-size:24px;color:red' class='fa'>&#xf00d;</i> &nbsp;"
                '    End Select

                '    iCounter += 1
                'Next
                'objlblResult.InnerHtml = strStyle '-- DIV
            End If
            _counter = mdicAnswers.Keys.Count



            If (mdicAnswers.AsEnumerable().Where(Function(x) CBool(x.Value) = True).Count()) >= 3 Then
                mblnIsNidaPopupShown = False
                popupNidaQuestions.Hide()
                _finaloutput = ""
                Call SetInfoFromNida(_firstname:=mstrNidaFName, _middleName:=mstrNidaMName, _surname:=mstrNidaSName, _gender:=mstrNidaGender, _dateOfBirth:=mstrNidaDoB, _mobileNumber:=mstrNidaMobile)
                Call RegisterAccountAndActivationLink(_tin:=mstrNidaTin, _village:=mstrNidaVillage, _phoneNumber1:=mstrNidaPhoneNum1, _applPhoto:=mstrNidaApplPhoto, _applSign:=mstrNidaApplSign)
                Exit Try
            End If

            If (mdicAnswers.Keys.Count = 5) Then
                mblnIsNidaPopupShown = False
                popupNidaQuestions.Hide()
                _finaloutput = ""
                ShowMessage("Sorry, we cannot authenticate your information from the provided information and answers. Please provide correct information in order to register. ", MessageType.Info)
                Exit Try
            End If

        Catch wx As WebException
            Dim err As String = ""
            If wx.InnerException IsNot Nothing Then err = wx.InnerException.Message
            err &= ", " & wx.Message

            mdicAnswers = New Dictionary(Of Integer, Boolean)

            Using response As WebResponse = wx.Response
                Dim httpResponse As HttpWebResponse = CType(response, HttpWebResponse)
                Using data As Stream = response.GetResponseStream()
                    Using reader = New StreamReader(data)
                        Dim text As String = reader.ReadToEnd()
                        err &= ", " & text
                        Dim parsejson As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(text.ToString())
                        If parsejson.Item("title") IsNot Nothing Then
                            _exceptionMsg = parsejson.Item("title").ToString()
                        End If
                    End Using
                End Using
            End Using

            objNidaLog.AddNidaRequestLogs(_ApiUrl, json, err, txtNinNumber.Text, txtNINMobile.Text, UserName.Text)
            Global_asax.CatchException(wx, Context)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            If _output IsNot Nothing Then
                objNidaLog.AddNidaRequestLogs(_ApiUrl, json, CStr(_output), txtNinNumber.Text, txtNINMobile.Text, UserName.Text)
            End If
        End Try
        Return _finaloutput
    End Function

    Private Function Verify_Nida_Otp(_ApiUrl As String, _nidanumber As String, _opt As Integer, ByRef objresp As _response, ByRef _exceptionMsg As String) As String
        Dim _finaloutput As String = ""
        Dim json As String = ""
        Dim objNidaLog As New clsApplicant
        Dim _output As Object = Nothing
        Try
            Dim ninotp As New nidaotp With {
                .nin = _nidanumber,
                .otp = _opt
            }

            json = JsonConvert.SerializeObject(ninotp, Formatting.Indented)

            System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) True

            Dim httpWebRequest = CType(WebRequest.Create(_ApiUrl), HttpWebRequest)
            httpWebRequest.ContentType = "application/json"
            httpWebRequest.Method = "POST"

            Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
                streamWriter.Write(json)
                streamWriter.Flush()
                streamWriter.Close()
            End Using

            Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
            Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                _output = streamReader.ReadToEnd()
            End Using

            If _output IsNot Nothing Then

                Dim jsonSettings = New JsonSerializerSettings With {
                         .NullValueHandling = NullValueHandling.Ignore,
                         .MissingMemberHandling = MissingMemberHandling.Ignore
                        }

                Dim json_data = JsonConvert.DeserializeObject(Of _response)(CStr(_output), jsonSettings)

                'Dim _txt As New TextBox
                '_txt.Text = "{" & vbCrLf & "    ""applicantPhoto"": """"," & vbCrLf & "    ""applicantSignature"": """"," & vbCrLf & "    ""applicationId"": null," & vbCrLf & "    ""createdDate"": null," & vbCrLf & "    ""dateOfBirth"": ""1986-04-06T00:00:00""," & vbCrLf & "    ""email"": null," & vbCrLf & "    ""existingTIN"": null," & vbCrLf & "    ""fingerImage1"": null," & vbCrLf & "    ""fingerImage2"": null," & vbCrLf & "    ""fingerLabel1"": null," & vbCrLf & "    ""fingerLabel2"": null," & vbCrLf & "    ""firstName"": ""LOUIS""," & vbCrLf & "    ""gender"": ""MALE""," & vbCrLf & "    ""inv_result"": null," & vbCrLf & "    ""isCorrect"": 0," & vbCrLf & "    ""matchStatus"": null," & vbCrLf & "    ""middleName"": ""WILLIAM""," & vbCrLf & "    ""mobileNumber"": ""0688941466""," & vbCrLf & "    ""nationality"": ""TANZANIAN""," & vbCrLf & "    ""nextStep"": null," & vbCrLf & "    ""nin"": ""19860406141210000526""," & vbCrLf & "    ""phoneNumber1"": null," & vbCrLf & "    ""phoneNumber2"": null," & vbCrLf & "    ""picture"": null," & vbCrLf & "    ""questions"": null," & vbCrLf & "    ""score"": null," & vbCrLf & "    ""signature"": null," & vbCrLf & "    ""surname"": ""RIWA""," & vbCrLf & "    ""tin"": null," & vbCrLf & "    ""tinApplicationDate"": null," & vbCrLf & "    ""tinGenerated"": 0," & vbCrLf & "    ""village"": null," & vbCrLf & "    ""wardPostCode"": null" & vbCrLf & "}"
                'Dim json_data = JsonConvert.DeserializeObject(Of _response)(CStr(_txt.Text), jsonSettings)

                If json_data.nextStep IsNot Nothing Then
                    _finaloutput = json_data.nextStep.ToString()
                End If

                objresp = json_data
            End If

        Catch wx As WebException
            Dim err As String = ""
            If wx.InnerException IsNot Nothing Then err = wx.InnerException.Message
            err &= ", " & wx.Message

            Using response As WebResponse = wx.Response
                Dim httpResponse As HttpWebResponse = CType(response, HttpWebResponse)
                Using data As Stream = response.GetResponseStream()
                    Using reader = New StreamReader(data)
                        Dim text As String = reader.ReadToEnd()
                        err &= ", " & text
                        Dim parsejson As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(text.ToString())
                        If parsejson.Item("title") IsNot Nothing Then
                            _exceptionMsg = parsejson.Item("title").ToString()
                        End If
                    End Using
                End Using
            End Using

            objNidaLog.AddNidaRequestLogs(_ApiUrl, json, err, txtNinNumber.Text, txtNINMobile.Text, UserName.Text)
            Global_asax.CatchException(wx, Context)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            If _output IsNot Nothing Then
                objNidaLog.AddNidaRequestLogs(_ApiUrl, json, CStr(_output), txtNinNumber.Text, txtNINMobile.Text, UserName.Text)
            End If
        End Try
        Return _finaloutput
    End Function

    Private Sub RegisterAccountAndActivationLink(Optional ByVal _tin As Object = Nothing,
                                                 Optional ByVal _village As Object = Nothing,
                                                 Optional ByVal _phoneNumber1 As Object = "",
                                                 Optional ByVal _applPhoto As Object = Nothing,
                                                 Optional ByVal _applSign As Object = Nothing)
        Try
            If Session("mstrUrlArutiLink") Is Nothing Then
                'If Request.Cookies("LogOutPageURL") IsNot Nothing Then
                '    Session("mstrUrlArutiLink") = clsSecurity.Decrypt(Request.Cookies("LogOutPageURL").Value, "ezee")
                'End If
                Session("mstrUrlArutiLink") = mstrUrlArutiLink
            End If

            If Session("mstrUrlArutiLink").ToString.ToLower.Contains("/alogin.aspx") = True Then
                'Sohail (20 Dec 2017) -- Start
                'Enhancement - Multilple admin email registration.
                'If cuw.UserName.ToLower.Trim <> Session("admin_email").ToString.Trim Then
                Dim arrAdmin() As String = Session("admin_email").ToString.Split(CChar(";"))
                Dim arrAdmin1() As String = Session("admin_email").ToString.Split(CChar(","))
                If arrAdmin.Contains(UserName.Text.ToLower.Trim) = False AndAlso arrAdmin1.Contains(UserName.Text.ToLower.Trim) = False Then
                    'Sohail (20 Dec 2017) -- End
                    ShowMessage("Sorry, You cannot register with different email.")
                    'Sohail (20 Dec 2017) -- Start
                    'Enhancement - Multilple admin email registration.
                    Exit Try
                    'Sohail (20 Dec 2017) -- End
                End If
            End If

            mstrContinueLink = mstrUrlArutiLink

            If Session("CompCode").ToString().ToLower() = "tra" Then
                If Password.Text.Trim() = "" Then Password.Text = hfPassword.Value
                If ConfirmPassword.Text.Trim() = "" Then ConfirmPassword.Text = hfPassword.Value

                If Question.Text.Trim() = "" Then Question.Text = "What is your nida number?"
                If Answer.Text.Trim() = "" Then Answer.Text = txtNinNumber.Text
            End If

            Dim createStatus As MembershipCreateStatus
            Dim user As MembershipUser = Membership.CreateUser(UserName.Text, Password.Text, UserName.Text, Question.Text, Answer.Text, False, createStatus)

            Select Case createStatus

                Case MembershipCreateStatus.Success

                    ShowMessage("Your account is successfully created!")
                    Exit Select

                Case MembershipCreateStatus.DuplicateUserName

                    ShowMessage("The user with the same UserName already exists!")
                    Exit Try

                Case MembershipCreateStatus.DuplicateEmail

                    ShowMessage("The user with the same email address already exists!")
                    Exit Try

                Case MembershipCreateStatus.InvalidEmail

                    ShowMessage("The email address you provided is invalid.")
                    Exit Try

                Case MembershipCreateStatus.InvalidAnswer

                    ShowMessage("The security answer was invalid.")
                    Exit Try

                Case MembershipCreateStatus.InvalidPassword

                    ShowMessage("The password you provided is invalid. It must be 7 characters long and have at least 1 special character.")
                    Exit Try
                Case Else

                    ShowMessage("There was an unknown error; the user account was NOT created.")
                    Exit Try
            End Select

            '------------------------------------------------------------------------
            'Dim cuw As CreateUserWizard = CType(sender, CreateUserWizard)
            Dim strEmail As String = UserName.Text
            Dim strPwd As String = Password.Text
            Dim strSQ As String = Question.Text
            Dim strSP As String = Answer.Text
            Dim ActivationUrl As String = ""
            'Dim createStatus As MembershipCreateStatus

            If Membership.GetUser(strEmail) IsNot Nothing Then
                'Membership.CreateUser(strEmail, strPwd, strEmail, strSQ, strSP, False, createStatus)

                'If createStatus = MembershipCreateStatus.Success Then

                Dim profile As UserProfile = UserProfile.GetUserProfile(UserName.Text)
                profile.FirstName = txtFirstName.Text.Trim
                profile.MiddleName = txtMiddleName.Text.Trim
                profile.LastName = txtLastName.Text.Trim
                profile.Gender = CInt(drpGender.SelectedValue)
                profile.BirthDate = dtBirthdate.GetDate
                profile.MobileNo = txtMobileNo.Text.Trim
                'S.SANDEEP |04-MAY-2023| -- START
                'ISSUE/ENHANCEMENT : A1X-833
                If Session("CompCode").ToString().ToLower() = "tra" Then
                    profile.NIN = txtNinNumber.Text.Trim
                    profile.NinMobile = txtNINMobile.Text.Trim
                    If _tin IsNot Nothing Then profile.TIN = _tin.ToString()
                    If _village IsNot Nothing Then profile.Village = _village.ToString()
                    If _phoneNumber1 IsNot Nothing Then profile.Phonenumber = _phoneNumber1.ToString()
                    profile.Nationality = 208

                    Dim _base64String As String = ""
                    _base64String = clsApplicant.GetOnlyBase64String(_applPhoto.ToString().Trim())
                    If _applPhoto IsNot Nothing Then
                        profile.ApplicantPhoto = _base64String
                    End If
                    _base64String = ""
                    _base64String = clsApplicant.GetOnlyBase64String(_applSign.ToString().Trim())
                    If _applSign IsNot Nothing Then
                        profile.ApplicantSignature = _base64String
                    End If
                Else
                    profile.NIN = ""
                    profile.NinMobile = ""
                    profile.TIN = ""
                    profile.Village = ""
                    profile.Phonenumber = ""
                    profile.Nationality = 0
                    profile.ApplicantPhoto = ""
                    profile.ApplicantSignature = ""
                End If
                'S.SANDEEP |04-MAY-2023| -- END

                profile.Save()


                Dim strBody As String = "<!DOCTYPE html>" &
                "<html>" &
                    "<head>" &
                        "<title>#companyname# Email Confirmation!!!</title>" &
                        "<meta charset='utf-8'/>" &
                    "</head>" &
                    "<body style='font-size:9.0pt; font-family:'Verdana', 'Sans-Serif'; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" &
                        "<br/>" &
                    "#imgcmp#" &
                    "<p>Dear <b>#firstname# #lastname#</b>, </p>" &
                        "<p>Welcome To <b> #companyname# </b> Online Job Application System.</p>" &
                        "<p>We are glad To inform you that your Email <b> #username#</b> Is now registered With us.</p>" &
                        "<p>Please click On the below link Or copy And paste the link To address bar To activate your account.</p>" &
                        "<p> #activationlink# </p>" &
                        "<br/>" &
                        "<p><B>Thank you,</B></p>" &
                        "<p><B>#companyname# Support Team</B></p>" &
                        "<br/>" &
                        "<p><b>Online Job Application Powered by Aruti HR & Payroll Management Software</b></p>" &
                    "#imgaruti#" &
                "</body>" &
                "</html>"

                Dim strSubject As String = ""
                Dim reader As New StreamReader(Server.MapPath("~/EmailTemplate/RegisterSuccess.html"))
                If reader IsNot Nothing Then
                    strBody = reader.ReadToEnd
                    reader.Close()
                End If
                strSubject = strBody.Substring(strBody.IndexOf("<title>") + 7, strBody.IndexOf("</title>") - strBody.IndexOf("<title>") - 7).Replace("#companyname#", mstrCompName)
                If strSubject.Trim = "" Then
                    strSubject = mstrCompName & " Email confirmation!"
                End If
                'CreateUserWizard1.CompleteSuccessText = "Your account has been successfully created.<BR>Activation link has been sent to you on your Email " & strEmail & ". <BR>Please Activate your account before login."

                'ActivationUrl = Request.Url.GetLeftPart(UriPartial.Authority) & ResolveUrl("ActivateUsers.aspx") & "?UserID=" + Server.UrlEncode(clsCrypto.Encrypt(Membership.GetUser(strEmail).ProviderUserKey.ToString)) + "&Email=" + Server.UrlEncode(clsCrypto.Encrypt(strEmail)) + "&lnk=" + Server.UrlEncode(clsCrypto.Encrypt(mstrUrlArutiLink)) + "&c=" + Server.UrlEncode(clsCrypto.Encrypt(mstrCompCode)) + "&i=" + Server.UrlEncode(clsCrypto.Encrypt(mintCompanyunkid.ToString))
                ActivationUrl = Request.Form(hflocationorigin.UniqueID) & Replace(Request.ApplicationPath & "/ActivateUsers.aspx", "//", "/") & "?UserID=" + Server.UrlEncode(clsCrypto.Encrypt(Membership.GetUser(strEmail).ProviderUserKey.ToString)) + "&Email=" + Server.UrlEncode(clsCrypto.Encrypt(strEmail)) + "&lnk=" + Server.UrlEncode(clsCrypto.Encrypt(mstrUrlArutiLink)) + "&c=" + Server.UrlEncode(clsCrypto.Encrypt(mstrCompCode)) + "&i=" + Server.UrlEncode(clsCrypto.Encrypt(mintCompanyunkid.ToString))
                ActivationUrl = ActivationUrl.Replace("&", "&amp;")

                Dim intUnkId As Integer = 0
                Dim objApplicant As New clsApplicant()
                If objApplicant.InsertActivationLink(strCompCode:=mstrCompCode _
                                                    , intCompUnkID:=CInt(mintCompanyunkid) _
                                                    , strUserId:=Membership.GetUser(strEmail).ProviderUserKey.ToString _
                                                    , strEmail:=strEmail _
                                                    , strSubject:=strSubject _
                                                    , strMessage:=strBody.Replace("#username#", strEmail.Trim()).Replace("#activationlink#", ActivationUrl).Replace("#companyname#", mstrCompName).Replace("#firstname#", profile.FirstName).Replace("#lastname#", profile.LastName) _
                                                    , strLink:=ActivationUrl _
                                                    , intRet_UnkId:=intUnkId
                                                    ) = True Then



                    If objApplicant.GetCompanyInfo(intComUnkID:=CInt(mintCompanyunkid) _
                                                       , strCompCode:=mstrCompCode _
                                                       , ByRefblnIsActive:=False
                                                       ) = False Then

                        Exit Try
                    End If

                    Dim objCompany1 As New clsCompany
                    Session("e_emailsetupunkid") = 0
                    objCompany1.GetEmailSetup(mstrCompCode, mintCompanyunkid)

                    Dim objMail As New clsMail
                    If objMail.SendEmail(strToEmail:=strEmail _
                                             , strSubject:=strSubject _
                                             , strBody:=strBody.Replace("#username#", strEmail.Trim()).Replace("#activationlink#", ActivationUrl).Replace("#companyname#", mstrCompName).Replace("#firstname#", profile.FirstName).Replace("#lastname#", profile.LastName) _
                                             , blnAddLogo:=True
                                             ) = False Then

                        objApplicant.UpdateActivationEmailSent(intUnkId, False, strEmail, DateAndTime.Now)
                        'CreateUserWizard1.ContinueDestinationPageUrl = "~/ResendLink.aspx"
                        'CreateUserWizard1.CompleteSuccessText = "Your account has been successfully created.<BR><BR>To Activate your account, Please click on Resend Activation Link option on Login screen to get activation link. <BR><BR>Please Activate your account before login."
                        mstrContinueLink = "~/ResendLink.aspx"
                        'lblMsg2.Text = "Activation link has been sent to you on your Email" & " " & UserName.Text

                    Else
                        objApplicant.UpdateActivationEmailSent(intUnkId, True, strEmail, DateAndTime.Now)
                        lblMsg2.Text = "Activation link has been sent to you on your Email" & " " & UserName.Text
                    End If

                End If

                'End If

                pnlMain.Visible = False
                pnlComplete.Visible = True
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SetInfoFromNida(ByVal _firstname As String,
                                _middleName As String,
                                _surname As String,
                                _gender As String,
                                _dateOfBirth As String,
                                _mobileNumber As String)
        Try
            'txtFirstName.Text = _jObject.Item("firstName").ToString()
            'txtMiddleName.Text = _jObject.Item("middleName").ToString()
            'txtLastName.Text = _jObject.Item("surname").ToString()
            'Select Case _jObject.Item("gender").ToString().ToUpper()
            '    Case "MALE"
            '        drpGender.SelectedValue = "1"
            '    Case "FEMALE"
            '        drpGender.SelectedValue = "2"
            'End Select
            'Dim bdt = DateTimeOffset.Parse(_jObject.Item("dateOfBirth").ToString()).Date
            'dtBirthdate.SetDate = bdt
            'txtMobileNo.Text = _jObject.Item("mobileNumber").ToString()

            txtFirstName.Text = _firstname '_jObject.firstName.ToString()
            txtMiddleName.Text = _middleName '_jObject.middleName.ToString()
            txtLastName.Text = _surname '_jObject.surname.ToString()
            Select Case _gender.ToUpper()'_jObject.gender.ToString().ToUpper()
                Case "MALE"
                    drpGender.SelectedValue = "1"
                Case "FEMALE"
                    drpGender.SelectedValue = "2"
            End Select
            'Dim bdt = DateTimeOffset.Parse(_jObject.dateOfBirth.ToString()).Date
            Dim bdt = DateTimeOffset.Parse(_dateOfBirth.ToString()).Date
            dtBirthdate.SetDate = bdt
            txtMobileNo.Text = _mobileNumber '_jObject.mobileNumber.ToString()            
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    'S.SANDEEP |04-MAY-2023| -- END

    Private Sub btnRegister_Click(sender As Object, e As EventArgs) Handles btnRegister.Click
        Try

            'Dim cuw As CreateUserWizard = CType(sender, CreateUserWizard)
            'cuw.Email = cuw.UserName

            'Threading.Thread.Sleep(5000)

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-884            
            'If txtFirstName.Text.Trim = "" Then
            '    ShowMessage("Please Enter First Name", MessageType.Info)
            '    'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtFirstName"), TextBox).Focus()
            '    txtFirstName.Focus()
            '    Exit Try
            'ElseIf txtLastName.Text.Trim = "" Then
            '    'ElseIf CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtLastName"), TextBox).Text.Trim = "" Then
            '    ShowMessage("Please Enter Surname", MessageType.Info)
            '    'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtLastName"), TextBox).Focus()
            '    txtLastName.Focus()
            '    Exit Try
            'ElseIf UserName.Text.Trim = "" Then
            '    ShowMessage("Please Enter Email.", MessageType.Info)
            '    UserName.Focus()
            '    Exit Try
            'ElseIf clsApplicant.IsValidEmail(UserName.Text) = False Then
            '    ShowMessage("Please Enter Valid Email.", MessageType.Info)
            '    UserName.Focus()
            '    Exit Try
            'ElseIf CInt(drpGender.SelectedValue) <= 0 Then
            '    'ElseIf CInt(CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("drpGender"), DropDownList).SelectedValue) <= 0 Then
            '    ShowMessage("Please Select Gender.", MessageType.Info)
            '    'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("drpGender"), DropDownList).Focus()
            '    drpGender.Focus()
            '    Exit Try
            '    'ElseIf CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).GetDate = CDate("01/Jan/1900") Then
            'ElseIf dtBirthdate.GetDate = CDate("01/Jan/1900") Then
            '    ShowMessage("Please Enter Valid Birth Date.", MessageType.Info)
            '    'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).Focus()
            '    dtBirthdate.Focus()
            '    Exit Try
            '    'ElseIf CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).GetDate >= System.DateTime.Today.Date Then
            'ElseIf dtBirthdate.GetDate >= System.DateTime.Today.Date Then
            '    ShowMessage("Birth Date should be less than current Date.", MessageType.Info)
            '    'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("dtBirthdate"), DateCtrl).Focus()
            '    dtBirthdate.Focus()
            '    Exit Try
            'ElseIf txtMobileNo.Text.Trim = "" Then
            '    'ElseIf CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtMobileNo"), TextBox).Text.Trim = "" Then
            '    ShowMessage("Please Enter Mobile No.", MessageType.Info)
            '    'CType(CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("txtMobileNo"), TextBox).Focus()
            '    txtMobileNo.Focus()
            '    Exit Try
            'ElseIf Password.Text.Trim = "" Then
            '    ShowMessage("Please Enter Password.", MessageType.Info)
            '    Password.Focus()
            '    Exit Try
            'ElseIf ConfirmPassword.Text.Trim = "" Then
            '    ShowMessage("Please Enter Confirm Password.", MessageType.Info)
            '    ConfirmPassword.Focus()
            '    Exit Try
            'ElseIf String.Compare(Password.Text, ConfirmPassword.Text, False) > 0 Then
            '    ShowMessage("The Password And Confirmation Password must match.", MessageType.Info)
            '    ConfirmPassword.Focus()
            '    Exit Try
            'ElseIf Question.Text.Trim = "" Then
            '    ShowMessage("Security question Is required.", MessageType.Info)
            '    Question.Focus()
            '    Exit Try
            'ElseIf Answer.Text.Trim = "" Then
            '    ShowMessage("Security answer Is required.", MessageType.Info)
            '    Answer.Focus()
            '    Exit Try
            'End If
            If IsValidate() = False Then
                Exit Try
            End If
            If Password.Text.Trim.Length > 0 Then hfPassword.Value = Password.Text
            'S.SANDEEP |04-MAY-2023| -- END


            Dim r As New Text.RegularExpressions.Regex("(?=.{7,})(?=(.*\d){1,})(?=(.*\W){1,})")

            If r.IsMatch(Password.Text) = False Then
                'If Text.RegularExpressions.Regex.IsMatch(Password.Text, "@\'(?:.{7,})(?=(.*\d){1,})(?=(.*\W){1,})") = False Then
                ShowMessage("Password should have a minimum length of 7 characters. It should also include at least 1 special character such as ! # $ % ( ) * + , - . / \ : ; = ? @ [ ] ^ _ ` | ~ .")
                Exit Try
            End If

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-893
            If Session("CompCode").ToString().ToLower() = "tra" Then

                mdicAnswers = New Dictionary(Of Integer, Boolean)
                ViewState("mdicAnswers") = mdicAnswers

                objlblSt1.Text = "" : objlblSt2.Text = ""
                objlblSt3.Text = "" : objlblSt4.Text = "" : objlblSt5.Text = ""

                objlblSt1.CssClass = "" : objlblSt2.CssClass = ""
                objlblSt3.CssClass = "" : objlblSt4.CssClass = "" : objlblSt5.CssClass = ""

                If CBool(Session("IsNidaWRIntegrated")) Then
                    Dim strMSg = ""
                    Dim _exMsg As String = ""
                    strMSg = Verify_Nida_Number(Session("NIDA_VERIFY_ID").ToString(), txtNinNumber.Text.Trim, txtNINMobile.Text.Trim, _exMsg)
                    If _exMsg IsNot Nothing AndAlso _exMsg.Trim.Length > 0 Then
                        ShowMessage(_exMsg, MessageType.Errorr)
                        Exit Sub
                    End If
                    If strMSg.ToLower() = "answerquestions" Then
                        mblnIsNidaPopupShown = True
                        'lblNidaQuestion.Text = "" : txtNidaQueryAnswer.Text = ""
                        'If mdicQuestions.Keys.Count() > 0 Then
                        '    For Each _key As Integer In mdicQuestions.Keys
                        '        lblNidaQuestion.Text = mdicQuestions.Values(mdicQuestions.Keys(_key))
                        '        hdfNidaQestionTag.Value = _key.ToString
                        '        Exit For
                        '    Next
                        'End If
                        popupNidaQuestions.Show()
                        Exit Try
                    ElseIf strMSg.ToLower() = "enterotp" Then
                        mblnIsNidaOtpPopupShown = True
                        popupNidaOtp.Show()
                        Exit Try
                    Else
                        ShowMessage("Sorry, We are not able to connect to NIDA to fetch your information. Please try after some time.", MessageType.Info)
                        Exit Try
                    End If
                End If
            Else
                RegisterAccountAndActivationLink()
            End If
            'S.SANDEEP |04-MAY-2023| -- END

        Catch ex As Exception
            'e.Cancel = True
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        Try
            Response.Redirect(mstrContinueLink, False)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-880
    Private Sub btnVerify_Click(sender As Object, e As EventArgs) Handles btnVerify.Click
        Try
            mblnIsNidaPopupShown = False
            popupNidaQuestions.Hide()

            If txtNidaQueryAnswer.Text.Trim() = "" Then
                popupNidaQuestions.Hide()
                ShowMessage("Please Enter Answer to continue.", MessageType.Info)
                popupNidaQuestions.Show()
                Exit Try
            End If

            Dim objans As New An With {
                .qnID = CInt(hdfNidaQestionTag.Value),
                .answer = txtNidaQueryAnswer.Text.Trim()
            }
            Dim _ans As New List(Of An) From {
                objans
            }
            Dim clsNidaAns As New Nidans With {
                .nin = txtNinNumber.Text.Trim(),
                .mobile = txtNINMobile.Text.Trim(),
                .ans = _ans
            }

            Dim payload As String = JsonConvert.SerializeObject(clsNidaAns)
            Dim _exMsg As String = ""
            Dim _qCnt As Integer = -1
            Dim strMsg As String = Verify_Nida_SecurityQuestion(Session("NIDA_SECURITY_QA").ToString(), payload, _exMsg, _qCnt)
            If _exMsg IsNot Nothing AndAlso _exMsg.Trim.Length > 0 Then
                ShowMessage(_exMsg, MessageType.Errorr)
                mblnIsNidaPopupShown = False
                popupNidaQuestions.Hide()
                Exit Sub
            End If


            If strMsg.ToLower() = "answerquestions" AndAlso _qCnt < 5 Then
                popupNidaQuestions.Show()
            Else
                mblnIsNidaPopupShown = False
                popupNidaQuestions.Hide()
                Exit Sub
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub btnValidateOtp_Click(sender As Object, e As EventArgs) Handles btnValidateOtp.Click
        Try
            If txtNidaOtpNumber.Text.Trim() = "" Then
                'popupNidaOtp.Hide()
                ShowMessage("Please Enter One time passcode to continue.", MessageType.Info)
                Exit Try
            End If
            Dim objResp As New _response
            Dim _exMsg As String = ""
            Dim strMsg As String = Verify_Nida_Otp(Session("NIDA_OTP_VERIFY").ToString(), txtNinNumber.Text.Trim, CInt(txtNidaOtpNumber.Text.Trim), objResp, _exMsg)

            If _exMsg IsNot Nothing AndAlso _exMsg.Trim.Length > 0 Then
                mblnIsNidaOtpPopupShown = False
                popupNidaOtp.Hide()
                ShowMessage(_exMsg, MessageType.Errorr)
                Exit Sub
            End If
            If strMsg.ToLower() = "" Then
                mblnIsNidaOtpPopupShown = False
                popupNidaOtp.Hide()
                Call SetInfoFromNida(_firstname:=objResp.firstName, _middleName:=objResp.middleName, _surname:=objResp.surname, _gender:=objResp.gender, _dateOfBirth:=objResp.dateOfBirth, _mobileNumber:=objResp.mobileNumber)
                Call RegisterAccountAndActivationLink(_tin:=objResp.tin, _village:=objResp.village, _phoneNumber1:=objResp.phoneNumber1, _applPhoto:=objResp.applicantPhoto, _applSign:=objResp.applicantSignature)
            Else
                mblnIsNidaOtpPopupShown = False
                popupNidaOtp.Hide()
                ShowMessage("Sorry, We are not able to connect to NIDA to fetch your information. Please try after some time.", MessageType.Info)
                Exit Try
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    'S.SANDEEP |04-MAY-2023| -- END

End Class