﻿Public Enum enImg_Email_RefId
    Employee_Module = 1
    Applicant_Module = 2
    Leave_Module = 3
    Payroll_Module = 4
    Dependants_Beneficiaries = 5
    Medical_Module = 6
    Discipline_Module = 7
    Appraisal_Module = 8
    Training_Module = 9
    Scan_Attached_Doc = 10
    Qualification = 11
    Claim_Request = 12
    TimeAndAttendance = 13
    PerformanceAssessment_Employee = 14
    PerformanceAssessment_Assessor = 15
    PerformanceAssessment_Reviewer = 16
    Leave_Planner_Module = 17  'THIS IS ONLY USED IN REFLEX
    Applicant_Job_Vacancy = 22
End Enum

Public Enum enScanAttactRefId
    IDENTITYS = 1
    MEMBERSHIPS = 2
    QUALIFICATIONS = 3
    JOBHISTORYS = 4
    LEAVEFORMS = 5
    DISCIPLINES = 6
    CURRICULAM_VITAE = 7
    OTHERS = 8
    TRAININGS = 9
    ASSET_DECLARATION = 10
    DEPENDANTS = 11
    CLAIM_REQUEST = 12
    ASSESSMENT = 13
    GRIEVANCES = 14
    TRAINING_REQUISITION = 15
    PERSONAL_ADDRESS_TYPE = 16
    DOMICILE_ADDRESS_TYPE = 17
    RECRUITMENT_ADDRESS_TYPE = 18
    EMERGENCY_CONTACT1 = 19
    EMERGENCY_CONTACT2 = 20
    EMERGENCY_CONTACT3 = 21
    EMPLOYEE_BIRTHINFO = 22
    EMPLYOEE_OTHERLDETAILS = 23
    COVER_LETTER = 24

    'Pinkal (30-Sep-2023) -- Start
    '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
    STAFF_REQUISITION = 25
    CLAIM_RETIREMENT = 26
    TRAINING_NEED_FORM = 27
    PDP = 28
    Talent = 29
    Succession = 30
    LOAN_ADVANCE = 31
    GROUP_TRAINING_REQUEST = 32
    LOAN_APPROVAL = 33
    TRANSFERS = 34
    REHIRE = 35
    SALARY = 36
    PROMOTION = 37
    RECATEGORIZE = 38
    STAFF_TRANSFER_REQUEST = 39
    APPLICANT_BIRTHINFO = 40
    'Pinkal (30-Sep-2023) -- End

End Enum

'Sohail (18 Feb 2020) -- Start
'NMB Enhancement # : On the references page on recruitment portal, provide all those fields on configuration where they can be made mandatory or optional.
Public Enum enMandatorySettingRecruitment
    MiddleName = 1
    Gender = 2
    BirthDate = 3
    Nationality = 4

    Address1 = 5
    Address2 = 6
    Country = 7
    State = 8
    City = 9
    Region = 10
    PostCode = 11

    Language1 = 12
    MaritalStatus = 13
    MotherTongue = 14
    CurrentSalary = 15
    ExpectedSalary = 16
    ExpectedBenefits = 17
    NoticePeriod = 18

    QualificationStartDate = 19
    QualificationAwardDate = 20
    HighestQualification = 21

    Employer = 22
    CompanyName = 23
    PositionHeld = 24

    ReferencePosition = 25
    ReferenceType = 26
    ReferenceEmail = 27
    ReferenceAddress = 28
    ReferenceCountry = 29
    ReferenceState = 30
    ReferenceCity = 31
    ReferenceGeneder = 32
    ReferenceMobile = 33
    ReferenceTelNo = 34

    CurriculumVitae = 35
    CoverLetter = 36

    EarliestPossibleStartDate = 37
    VacancyFoundOutFrom = 38
    ApplicantComments = 39

    QualificationResultCode = 40
    QualificationGPA = 41

    'Hemant (30 Mar 2022) -- Start            
    'ISSUE/ENHANCEMENT(ZRA) : Recruitement Changes
    OtherInfoAchievement = 42
    JournalsResearchPapers = 43
    CertificateNo = 44
    JobAchievement = 45
    AttachmentQualification = 46
    AttachmentTypeImage = 47
    AttachmentTypeDocument = 48
    ContactPlotNo = 49
    ContactEstate = 50
    ContactStreet = 51
    CantactFax = 52
    'Hemant (30 Mar 2022) -- End

    'Pinkal (30-Sep-2023) -- Start
    '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
    BirthCertificate = 53
    'Pinkal (30-Sep-2023) -- End

End Enum
'Sohail (18 Feb 2020) -- End

Public Class clsCommon_Master
    Public Enum enCommonMaster
        ADVERTISE_CATEGORY = 1
        ALLERGIES = 2
        ASSET_CONDITION = 3
        BENEFIT_GROUP = 4
        BLOOD_GROUP = 5
        COMPLEXION = 6
        DISABILITIES = 7
        EMPLOYEMENT_TYPE = 8
        ETHNICITY = 9
        EYES_COLOR = 10
        HAIR_COLOR = 11
        IDENTITY_TYPES = 12
        INTERVIEW_TYPE = 13
        LANGUAGES = 14
        MARRIED_STATUS = 15
        MEMBERSHIP_CATEGORY = 16
        PAY_TYPE = 17
        QUALIFICATION_COURSE_GROUP = 18
        RELATIONS = 19
        RELIGION = 20
        RESULT_GROUP = 21
        SHIFT_TYPE = 22
        SKILL_CATEGORY = 23
        TITLE = 24
        TRAINING_RESOURCES = 25
        ASSET_STATUS = 26
        ASSETS = 27
        LETTER_TYPE = 28
        SALINC_REASON = 29
        TRAINING_COURSEMASTER = 30
        STRATEGIC_GOAL = 31
        SOURCES_FUNDINGS = 32
        ATTACHMENT_TYPES = 33
        VACANCY_MASTER = 34
        'Sohail (27 Sep 2019) -- Start
        'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
        VACANCY_SOURCE = 71
        'Sohail (27 Sep 2019) -- End
    End Enum
End Class

Public Enum enYesno
    None = 0
    Yes = 1
    No = 2
End Enum
