﻿Imports System.Data
Imports System.Data.SqlClient
Imports Aruti_Online_Recruitment

'<Serializable>
Public Class clsWebLanguage

    Public Sub New()
    End Sub

    Public Property _Comp_Code As String
    Public Property _companyunkid As String
    Public Property _formname As String
    Public Property _controlunqid As String
    Public Property _language As String
    Public Property _language1 As String
    Public Property _language2 As String
    Public Property _languageunkid As Integer
    Public Property _currentlang As String
    Public Property _parent As String

    Public Function GetAllCaptions(intCompanyUnkId As Integer, strCompCode As String, ByVal strModuleName As String, ByVal intLanguageID As Integer, Optional strControlName As String = "") As List(Of clsWebLanguage)
        Dim list As List(Of clsWebLanguage) = Nothing
        list = GetAllLanguage(intCompanyUnkId:=intCompanyUnkId, strCompCode:=strCompCode, strFormName:=strModuleName, intLanguageID:=intLanguageID, strControlName:=strControlName)
        Return list
    End Function

    'Public Function GetAllCaptions(ByVal strModuleName As String) As List(Of clsWebLanguage)
    '    Dim list As List(Of clsWebLanguage) = Nothing
    '    list = GetAllLanguage(strModuleName, -1, "")
    '    Return list
    'End Function

    'Public Function GetAllCaptionsFromUID(ByVal strModuleName As String, ByVal intLanguageID As Integer, ByVal uID As String) As List(Of clsWebLanguage)
    '    Dim list As List(Of clsWebLanguage) = Nothing
    '    list = GetAllLanguage(strModuleName, -1, uID)
    '    Return list
    'End Function

    Private Function GetAllLanguage(intCompanyUnkId As Integer, strCompCode As String, ByVal strFormName As String, ByVal intLanguageID As Integer, ByVal strControlName As String) As List(Of clsWebLanguage)
        Dim list As List(Of clsWebLanguage) = Nothing
        Dim StrQ As String = ""

        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            StrQ = "procGetLanguage"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(StrQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intCompanyUnkId
                        cmd.Parameters.Add(New SqlParameter("@comp_code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@LangId", SqlDbType.Int)).Value = intLanguageID
                        If strControlName <> "" Then
                            cmd.Parameters.Add(New SqlParameter("@controlname", SqlDbType.NVarChar)).Value = strControlName
                        Else
                            cmd.Parameters.Add(New SqlParameter("@controlname", SqlDbType.NVarChar)).Value = ""
                        End If
                        cmd.Parameters.Add(New SqlParameter("@formname", SqlDbType.NVarChar)).Value = strFormName

                        Using dataReader As SqlDataReader = cmd.ExecuteReader()

                            If dataReader.HasRows Then
                                list = New List(Of clsWebLanguage)()

                                While dataReader.Read()
                                    list.Add(New clsWebLanguage() With {
                                        ._companyunkid = CInt(dataReader("companyunkid")),
                                        ._Comp_Code = dataReader("Comp_Code").ToString(),
                                        ._languageunkid = CInt(dataReader("langunkid")),
                                        ._formname = dataReader("formname").ToString(),
                                        ._language = dataReader("language").ToString(),
                                        ._language1 = dataReader("language1").ToString(),
                                        ._language2 = dataReader("language2").ToString(),
                                        ._controlunqid = dataReader("controlname").ToString(),
                                        ._currentlang = If(intLanguageID >= 0, dataReader("lang").ToString(), dataReader("language").ToString()),
                                        ._parent = dataReader("parent").ToString()
                                    })
                                End While
                            End If
                        End Using

                    End Using
                End Using

            End Using


        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try

        Return list
    End Function

    Public Function GetBlankStructure() As DataTable
        Dim dt As DataTable = Nothing
        Dim strQ As String = ""
        Dim exception As Exception = Nothing
        Dim ds As New DataSet
        Try
            dt = New DataTable("List")

            dt.Columns.Add("Comp_Code", System.Type.GetType("System.String")).DefaultValue = ""
            dt.Columns.Add("companyunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dt.Columns.Add("langunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            dt.Columns.Add("formname", System.Type.GetType("System.String")).DefaultValue = ""
            dt.Columns.Add("controlname", System.Type.GetType("System.String")).DefaultValue = ""
            dt.Columns.Add("language", System.Type.GetType("System.String")).DefaultValue = ""
            dt.Columns.Add("language1", System.Type.GetType("System.String")).DefaultValue = ""
            dt.Columns.Add("language2", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try

        Return dt
    End Function

    Public Function InsertUpdateDelete(strCompCode As String, intCompanyUnkId As Integer) As Boolean
        Dim flag As Boolean = False
        Dim strQ As String = ""

        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            strQ = "procInsertUpdateLanguage"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intCompanyUnkId
                        cmd.Parameters.Add(New SqlParameter("@comp_code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@formname", SqlDbType.NVarChar)).Value = _formname
                        cmd.Parameters.Add(New SqlParameter("@controlname", SqlDbType.NVarChar)).Value = _controlunqid
                        cmd.Parameters.Add(New SqlParameter("@language", SqlDbType.NVarChar)).Value = _language
                        cmd.Parameters.Add(New SqlParameter("@language1", SqlDbType.NVarChar)).Value = _language1
                        cmd.Parameters.Add(New SqlParameter("@language2", SqlDbType.NVarChar)).Value = _language2

                        cmd.ExecuteNonQuery()

                    End Using
                End Using
            End Using

            flag = True

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try

        Return flag
    End Function
End Class
