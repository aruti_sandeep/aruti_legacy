﻿Public Class basepage
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If CType(Session("sessionexpired"), Boolean) = True Then
                Response.Write("<script>alert('Session Expired');</script>")
                Session("sessionexpired") = Nothing
            End If
            Session("mainfullurl") = Request.Url.OriginalString
            Session("issitelocal") = InStr(Session("mainfullurl"), "localhost") > 0
        Catch ex As Exception
            Throw New Exception("Basepage-->Page Load event !!!" & ex.Message.ToString)
        Finally
        End Try
    End Sub

    Protected Overloads Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        Try
            MyBase.OnPreInit(e)
            If Session("cur_theme") Is Nothing Then
                Session.Add("cur_theme", System.Configuration.ConfigurationManager.AppSettings("DefaultTheme"))
                Page.Theme = DirectCast(Session("cur_theme"), String)
            Else
                Page.Theme = DirectCast(Session("cur_theme"), String)
            End If
            If Request.ServerVariables("http_user_agent").IndexOf("Safari", System.StringComparison.CurrentCultureIgnoreCase) <> -1 Then
                Page.ClientTarget = "uplevel"
            End If
        Catch ex As Exception
            Throw New Exception("Basepage->OnPreInit event!!!" & ex.Message.ToString)
        Finally
        End Try


    End Sub
    Protected Overloads Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Try
            If Me.IsLoginRequired Then
                If Session("Login") = Nothing Then
                    Me.Response.Redirect("~/index.aspx")
                Else
                    If CType(Session("Login"), Boolean) = False Then
                        Me.Response.Redirect("~/index.aspx")
                    End If
                End If
            End If

            MyBase.OnLoad(e)
        Catch ex As Exception
            Throw New Exception("Basepage->OnLoad event!!!" & ex.Message.ToString)
        Finally
        End Try


    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Try
            If Not System.Web.HttpContext.Current Is Nothing Then

            End If
        Catch ex As Exception
            Throw New Exception("Basepage->Page Error Event!!!" & ex.Message.ToString)
        Finally
        End Try

    End Sub



    Private pIsLoginRequired As Boolean
    Public Property IsLoginRequired() As Boolean
        Get
            Return pIsLoginRequired
        End Get
        Set(ByVal value As Boolean)
            pIsLoginRequired = value
        End Set
    End Property
End Class
