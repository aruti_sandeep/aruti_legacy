﻿Imports System.Runtime.CompilerServices

Public Module clsControlExtensions
    <Extension()>
    Function GetAllControlsOfType(Of T As Control)(ByVal parent As Control) As IEnumerable(Of T)
        Dim result = New List(Of T)()

        For Each control As Control In parent.Controls

            If TypeOf control Is T Then
                result.Add(CType(control, T))
            End If

            If control.HasControls() Then
                result.AddRange(control.GetAllControlsOfType(Of T)())
            End If
        Next

        Return result
    End Function
End Module
