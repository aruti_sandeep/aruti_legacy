﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Error.aspx.vb" Inherits="Aruti_Online_Recruitment._Error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Error</title>

     <link rel="shortcut icon" href="Images/logo_32.ico" type="image/x-icon" />
    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/bootstrap-dialog.css" />
    <link rel="stylesheet" href="Content/style.css?version=1" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <p></p>
            <p class="text-danger h6" >Something Bad happened, Please contact Aruti Support Team!!!!</p> <%--style="color: red; font-size: large;"--%>
    </div>
    </form>
</body>
</html>
