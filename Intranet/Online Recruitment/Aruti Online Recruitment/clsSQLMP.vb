﻿Public Class clsSQLMP
    Inherits SqlMembershipProvider

    Public Overrides Sub Initialize(ByVal name As String, ByVal config As NameValueCollection)
        config("connectionString") = HttpContext.Current.Session("ConnectionString").ToString()
        MyBase.Initialize(name, config)
    End Sub

End Class

Public Class clsSQLPP
    Inherits SqlProfileProvider

    Public Overrides Sub Initialize(ByVal name As String, ByVal config As NameValueCollection)
        config("connectionString") = HttpContext.Current.Session("ConnectionString").ToString()
        MyBase.Initialize(name, config)
    End Sub

End Class

Public Class clsSQLRP
    Inherits SqlRoleProvider

    Public Overrides Sub Initialize(ByVal name As String, ByVal config As NameValueCollection)
        config("connectionStringName") = "myConnectionString"
        'config("connectionString") = HttpContext.Current.Session("ConnectionString").ToString()
        MyBase.Initialize(name, config)
    End Sub

    Private Function myConnectionString() As String
        Return HttpContext.Current.Session("ConnectionString").ToString()
    End Function


    Public Overrides Property ApplicationName As String
        Get
            Return "Recruitment"
        End Get

        Set(ByVal value As String)

        End Set
    End Property
End Class