﻿$(document).ready(function () {

    $$('btnSubmit').on('click', function () {
        return IsValidate();
    });


});

var prm = Sys.WebForms.PageRequestManager.getInstance();
if (prm != null) {
    prm.add_endRequest(function (sender, e) {

        $$('btnSubmit').on('click', function () {
            return IsValidate();
        });


    });
};

function IsValidate() {
    if ($$('txtEmail').val().trim() == '') {
        ShowMessage('Please Enter Email.', 'MessageType.Info');
        $$('txtEmail').focus();
        return false;
    }
    else if (IsValidEmail($$('txtEmail').val()) == false) {
        ShowMessage('Please enter Valid Email.', 'MessageType.Info');
        $$('txtEmail').focus();
        return false;
    }

    return true;
}

function IsValidEmail(strEmail) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(strEmail)) {
        return false;
    } else {
        return true;
    }
}