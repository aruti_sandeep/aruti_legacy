﻿<%@ Page Title="Applicants" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/Site3.Master" CodeBehind="Applicants.aspx.vb" Inherits="Aruti_Online_Recruitment.Applicants" %>

<%@ Register Src="../Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">

        function pageLoad(sender, args) {

            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

        function openwin() {
            //var a = document.createElement("a");
            //a.setAttribute("href", url);
            //a.setAttribute("target", "_blank");
            //a.setAttribute("id", "openwin");
            //document.body.appendChild(a);
            //a.click();
            var popUp = window.open('../User/Preview.aspx', '_blank');
            if (popUp == null || typeof (popUp) == 'undefined') {
                alert('Please disable your pop-up blocker and click the "Preview" link again.');
            }
            else {
                popUp.focus();
            }
        }

        function prev(lnk) {
            var row = lnk.parentNode.parentNode;

            PageMethods.PreviewApplicant(row.cells[0].innerHTML);

            setTimeout(function () {
                //
            }, 5000);
            window.open('../User/Preview.aspx', '_blank');
        }
    </script>



    <div class="card">

        <div class="card-header">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h4>Applicants List
                    <asp:Label ID="lblCount" runat="server" Text=""></asp:Label></h4>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="grdApplicants" EventName="DataBound" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="card-body">
            <asp:Panel ID="pnlCompany" runat="server" Visible="false">
                <div class="form-group row">

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="col-md-4">
                                <asp:Label ID="lblCompany" runat="server" Text="Company:"></asp:Label>
                                <asp:DropDownList ID="cboCompany" runat="server" CssClass="selectpicker form-control" AutoPostBack="true" />
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="chkOnlyActive" EventName="CheckedChanged" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <div class="col-md-4">
                        <asp:CheckBox ID="chkOnlyActive" runat="server" Text="Only Active" Checked="true" AutoPostBack="true" />
                    </div>

                    <div class="col-md-4">
                    </div>
                </div>
            </asp:Panel>

            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <asp:Label ID="lblFirstName" runat="server" Text="First Name"></asp:Label>
                            <asp:TextBox ID="txtFirstName" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblSurname" runat="server" Text="Surname"></asp:Label>
                            <asp:TextBox ID="txtSurname" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                            <asp:TextBox ID="txtEmail" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            <asp:Label ID="lblMobile" runat="server" Text="Mobile"></asp:Label>
                            <asp:TextBox ID="txtMobile" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-md-4">

                            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="lblVacancy" runat="server" Text="Vacancy"></asp:Label>
                                    <asp:DropDownList ID="cboVacancy" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="cboCompany" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                            <asp:DropDownList ID="cboStatus" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                        </div>
                    </div>

                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="card-footer">
            <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary width100" Text="Reset" ValidationGroup="Search" />  <%--Style="width: 100%;"--%>
                        </div>

                        <div class="col-md-4">
                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary width100" Text="Search" ValidationGroup="Search" />  <%--Style="width: 100%;"--%>
                        </div>

                        <div class="col-md-4">
                            <asp:LinkButton ID="lnkExcel" runat="server" Text="Export to Excel..."></asp:LinkButton>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <%--<asp:Panel ID="pnl_gvError" runat="server" CssClass="table-responsive" Style="min-height: 300px;">
            <asp:GridView ID="gvError" runat="server" AutoGenerateColumns="False" CssClass="table" ShowHeaderWhenEmpty="true"
                        AllowPaging="false" DataKeyNames="ErrorId">
                <Columns>
                    <asp:BoundField DataField="User" HeaderText="User" />
                    <asp:BoundField DataField="Application" HeaderText="Application" Visible="false" />
                    <asp:BoundField DataField="Host" HeaderText="Host" />
                    <asp:BoundField DataField="Type" HeaderText="Type" />
                    <asp:BoundField DataField="Source" HeaderText="Source" />
                    <asp:BoundField DataField="Message" HeaderText="Message" />
                    
                    <asp:BoundField DataField="StatusCode" HeaderText="Status Code" />
                    <asp:BoundField DataField="TimeUtc" HeaderText="Time Utc" />
                    <asp:BoundField DataField="Sequence" HeaderText="Sequence" Visible="false" />
                    <asp:TemplateField HeaderText="Details">
                        <ItemTemplate>
                            <asp:TextBox ID="objtxtAllXML" runat="server" CssClass="form-control" ReadOnly="true" TextMode="MultiLine" ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>--%>

                <asp:Panel ID="pnl_Applicants" runat="server" CssClass="table-responsive minheight300" >  <%--Style="min-height: 300px;"--%>
                    <asp:GridView ID="grdApplicants" runat="server" AutoGenerateColumns="False" CssClass="table" ShowHeaderWhenEmpty="True"
                        AllowPaging="True" AllowSorting="true" DataSourceID="dsApplicants" PageSize="15" DataKeyNames="applicantunkid, companyunkid, Comp_Code">

                        <Columns>
                            <asp:BoundField DataField="applicantunkid" HeaderText="ID" NullDisplayText=" " ReadOnly="True"></asp:BoundField>
                            <%--<asp:BoundField DataField="Comp_Code" HeaderText="Comp. Code" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="companyunkid" HeaderText="Web Client ID" Visible="false"></asp:BoundField>--%>
                            <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="CASE WHEN rcapplicant_master.titleunkid > 0 THEN title.name ELSE '' END" HeaderStyle-CssClass="text-white" ></asp:BoundField> <%-- HeaderStyle-ForeColor="White"--%>
                            <asp:BoundField DataField="ApplicantName" HeaderText="Applicant Name" SortExpression="rcapplicant_master.firstname + ISNULL(rcapplicant_master.othername, '') + rcapplicant_master.surname" HeaderStyle-CssClass="text-white"  ></asp:BoundField>  <%--HeaderStyle-ForeColor="White"--%>
                            <asp:BoundField DataField="GenderName" HeaderText="Gender" SortExpression="CASE rcapplicant_master.gender WHEN 1 THEN 1 WHEN 2 THEN 0 ELSE 0 END" HeaderStyle-CssClass="text-white"></asp:BoundField>  <%--HeaderStyle-ForeColor="White"--%>
                            <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" HeaderStyle-CssClass="text-white"></asp:BoundField> <%-- HeaderStyle-ForeColor="White"--%>
                            <asp:BoundField DataField="present_mobileno" HeaderText="Mobile No." SortExpression="present_mobileno" HeaderStyle-CssClass="text-white"></asp:BoundField>  <%--HeaderStyle-ForeColor="White"--%>
                            <asp:BoundField DataField="VacancyName" HeaderText="Applied For" SortExpression="ISNULL(Vacancy.name, '')" HeaderStyle-CssClass="text-white"></asp:BoundField>  <%--HeaderStyle-ForeColor="White"--%>
                            <asp:BoundField DataField="ProfileLastUpdated" HeaderText="Profile Last Updated" SortExpression="rcapplicant_master.transactiondate" HeaderStyle-CssClass="text-white" ></asp:BoundField> <%-- HeaderStyle-ForeColor="White"--%>
                            <asp:BoundField DataField="Syncdatetime" HeaderText="Imported Time" SortExpression="rcapplicant_master.Syncdatetime" HeaderStyle-CssClass="text-white"></asp:BoundField>  <%--HeaderStyle-ForeColor="White"--%>
                            <asp:TemplateField HeaderText="Preview CV" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="objlnkPreview" Text="Preview" runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Preview" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Preview CV" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Visible="false">
                                <ItemTemplate>
                                    <a onclick="return prev(this);" href="javascript:void(0);">Preview</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>



                    <asp:ObjectDataSource ID="dsApplicants" runat="server" SelectMethod="GetApplicantList" TypeName="Aruti_Online_Recruitment.clsApplicant" EnablePaging="true"
                        MaximumRowsParameterName="intPageSize" StartRowIndexParameterName="startRowIndex" SelectCountMethod="GetApplicantCount" SortParameterName="strSortExpression">
                        <SelectParameters>
                            <asp:SessionParameter Name="strCompCode" SessionField="CompCode" Type="String" />
                            <asp:SessionParameter Name="intComUnkID" SessionField="Companyunkid" Type="Int32" />
                            <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                            <asp:ControlParameter ControlID="cboVacancy" DefaultValue="0" Name="intVacancyUnkId" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="txtFirstName" DefaultValue="" Name="strFirstName" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txtSurname" DefaultValue="" Name="strSurName" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txtEmail" DefaultValue="" Name="strEmail" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txtMobile" DefaultValue="" Name="strPresentMobileNo" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="cboStatus" DefaultValue="0" Name="intStatusId" PropertyName="SelectedValue" Type="Int32" />
                            <asp:Parameter Name="intTitleId" Type="Int32" />
                            <asp:Parameter Name="intVacancyMasterTypeId" Type="Int32" />
                            <%--<asp:Parameter Name="intTimezoneMinuteDiff" Type="Int32" />--%>
                            <asp:SessionParameter Name="intTimezoneMinuteDiff" SessionField="timezone_minute_diff" Type="Int32" />
                            <asp:Parameter Name="startRowIndex" Type="Int32" />
                            <asp:Parameter Name="intPageSize" Type="Int32" />
                            <asp:Parameter Name="strSortExpression" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>



                    <%--<asp:ObjectDataSource ID="dsApplicants" runat="server" SelectMethod="GetApplicantList" SelectCountMethod="GetApplicantCount" EnablePaging="True" MaximumRowsParameterName="intPageSize" 
                                         SortParameterName="strSortExpression" TypeName="Aruti_Online_Recruitment.clsApplicant" StartRowIndexParameterName="intStartRowIndex" >
                        <SelectParameters>
                            <asp:SessionParameter Name="strCompCode" SessionField="CompCode" Type="String" />
                            <asp:SessionParameter Name="intComUnkID" SessionField="Companyunkid" Type="Int32" />
                            <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                            <asp:ControlParameter ControlID="cboVacancy" DefaultValue="0" Name="intVacancyUnkId" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="txtFirstName" DefaultValue="" Name="strFirstName" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txtSurname" DefaultValue="" Name="strSurName" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txtEmail" DefaultValue="" Name="strEmail" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="txtMobile" DefaultValue="" Name="strPresentMobileNo" PropertyName="Text" Type="String" />
                            <asp:ControlParameter ControlID="cboStatus" DefaultValue="0" Name="intStatusId" PropertyName="SelectedValue" Type="Int32" />
                            <asp:Parameter Name="intTitleId" Type="Int32"  />
                            <asp:Parameter Name="intVacancyMasterTypeId" Type="Int32" />
                            <asp:Parameter Name="intTimezoneMinuteDiff" Type="Int32" />
                            <asp:Parameter Name="intStartRowIndex" Type="Int32" />
                            <asp:Parameter Name="intPageSize" Type="Int32" />
                            <asp:Parameter Name="strSortExpression" Type="String"/>
                        </SelectParameters>
                    </asp:ObjectDataSource>--%>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                <asp:PostBackTrigger ControlID="lnkExcel" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

</asp:Content>
