﻿<%@ Page Title="Home : Aruti" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/Site3.Master" CodeBehind="UserHome.aspx.vb" Inherits="Aruti_Online_Recruitment.UserHome2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" href="UserHome.css" />

    <div class="card">

        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <div class="card-body p-md-2">
                    <asp:Timer ID="Timer1" runat="server" Interval="30000"></asp:Timer>
                    <asp:DataList ID="dlDashboard" runat="server" DataKeyField="Id"
                        DataSourceID="odsDashboard" CssClass="w-100" RepeatDirection="Horizontal" RepeatColumns="3">
                        <ItemTemplate>

                            <div class="card rounded m-md-2 my-2">
                                <div class="card-body text-center p-0">
                                    <%--<asp:Panel ID="pnlInfoBox" runat="server" CssClass="panel-heading"><%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("Caption")) %></asp:Panel>--%>
                                    <asp:Panel ID="pnlInfoBox" runat="server" CssClass="panel-heading"><%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Caption"), True) %></asp:Panel>
                                </div>
                                <div class="card-footer text-center">
                                    <h2>
                                        <%--<asp:Label ID="lblCount" runat="server"><%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("Cnt")) %></asp:Label></h1>--%>
                                        <asp:Label ID="lblCount" runat="server"><%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Cnt"), True) %></asp:Label></h2>
                                </div>
                            </div>

                        </ItemTemplate>
                    </asp:DataList>


                    <asp:ObjectDataSource ID="odsDashboard" runat="server" SelectMethod="GetDashboard" TypeName="Aruti_Online_Recruitment.clsSACommon" EnablePaging="false">
                        <SelectParameters>
                            <asp:Parameter Name="strComp_Code" Type="String" DefaultValue="" />
                            <asp:Parameter Name="intCompanyunkid" Type="Int32" DefaultValue="0" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <p class="font-italic">
                        Refresh counts automatically after 30 seconds
                    <asp:LinkButton ID="lnkStart" runat="server" Text="<i aria-hidden='true' class='fa fa-play'></i>"></asp:LinkButton><asp:LinkButton ID="lnkStop" runat="server" Visible="false" Text="<i aria-hidden='true' class='fa fa-pause'></i>"></asp:LinkButton>
                    </p>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                <asp:AsyncPostBackTrigger ControlID="lnkStart" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="lnkStop" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
