﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ForgotPassword.aspx.vb" Inherits="Aruti_Online_Recruitment.ForgotPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Forgot Password</title>

    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/bootstrap-dialog.css" />
    <link rel="shortcut icon" href="Images/logo_32.ico" type="image/x-icon" />
    <link rel="stylesheet" href="Content/style.css?version=1" />
    <link rel="stylesheet" href="customtheme.css" />

    <script type="text/javascript" src="scripts/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap-dialog.js"></script>

   
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>

        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="400" EnablePageMethods="true" ScriptMode="Release">
        </asp:ScriptManager>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1" DynamicLayout="false">
            <ProgressTemplate>
                <div class="UpdateProgressBG">
                    <div class="center">
                        <img alt="progress" src="images/waiting.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <asp:UpdatePanel ID="UpdatePanel" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <%--<asp:PasswordRecovery ID="PasswordRecovery1" runat="server" Style="margin-left: auto; margin-right: auto;" SuccessText="Your password has been successfully reset. Password Restore Link and Verification Code has been sent to you on your email. Please check your email to reset your password. This Password Restore Link is valid for 24 hours." TextLayout="TextOnTop" UserNameInstructionText="Enter your email to receive your password." UserNameLabelText="Email:">
                            <SubmitButtonStyle CssClass="btn btn-primary" Width="100%" />
                            <MailDefinition From="suhail4ezee@gmail.com">
                            </MailDefinition>
                            <TextBoxStyle CssClass="form-control" />
                        </asp:PasswordRecovery>--%>
                <div class="card MaxWidth1422">
                    <div class="card-header">
                        <div class="form-group row">
                            <div class="col-md-12 text-center" >
                                <div class="col-md-12">
                                    <asp:Image ID="img_shortcuticon" CssClass="img-rounded img_shortcuticon" runat="server" />
                                </div>
                            </div>
                        </div>

                        <h5 class="text-center">Recruitment Portal</h5>
                    </div>
                    <asp:Panel ID="pnlForgot" runat="server">
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                                <asp:Label ID="lblForgotPassword" runat="server">Forgot Your Password?</asp:Label>
                                <br />
                                <asp:Label ID="lblEnterEmail" runat="server">Enter your email to receive your password.</asp:Label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="txtUserName">Email:</asp:Label>
                                <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" TextMode="Email"></asp:TextBox>

                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>

                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <asp:Button ID="SubmitButton" runat="server" Text="Submit" CssClass="btn btn-primary width100" />
                            </div>
                        </div>
                    </div>
                        </asp:Panel>

                    <asp:Panel ID="pnlSuccess" runat="server" Visible="false">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="lblSuccess" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </asp:Panel>

                    <asp:Panel ID="pnlPoweredBy" runat="server">
                        <div class="card-footer">
                            <div class="form-group row">
                                <div class="col-md-12 text-center mt-3" >
                                    <asp:Label ID="lblPoweredBy" runat="server" Text="Powered By" CssClass="PoweredBy" ></asp:Label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 text-center" >
                                    <img src="Images/logo_aruti.png" class="img-rounded" alt="Aruti" width="100" height="60" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hflocationorigin" runat="server" Value="" />
    </form>
    <script type="text/javascript" src="forgotpassword.js"></script>
    <script type="text/javascript" src="showmessage.js"></script>
    <script type="text/javascript" src="Browser.js"></script>
</body>
</html>
