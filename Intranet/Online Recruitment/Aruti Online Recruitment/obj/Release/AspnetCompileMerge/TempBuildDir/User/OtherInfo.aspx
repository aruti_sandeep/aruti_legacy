﻿<%@ Page Title="Other Information" Language="vb" AutoEventWireup="false" MasterPageFile="~/User/Site4.Master" CodeBehind="OtherInfo.aspx.vb" Inherits="Aruti_Online_Recruitment.OtherInfo" %>

<%@ Register Src="../Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <script type="text/javascript">

        $(document).ready(function () {

            //var $drplanguage1 = $$("drplanguage1");
            //var $drplanguage2 = $$("drplanguage2");
            //var $drplanguage3 = $$("drplanguage3");
            //var $drplanguage4 = $$("drplanguage4");


            //$drplanguage1.on("change", function () {
            //    IsValidLanguage(1, $(this).val());
            //});

            //$drplanguage2.on("change", function () {
            //    IsValidLanguage(2, $(this).val());
            //});

            //$drplanguage3.on("change", function () {
            //    IsValidLanguage(3, $(this).val());
            //});

            //$drplanguage4.on("change", function () {
            //    IsValidLanguage(4, $(this).val());
            //});

            checkBoxCSS();
            setEvent();
        });

        function pageLoad(sender, args) {

            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });

            
                       

        }

        function IsValidLanguage(lngidx, value) {
            var lng1 = $$("drplanguage1")[0];
            var lng2 = $$("drplanguage2")[0];
            var lng3 = $$("drplanguage3")[0];
            var lng4 = $$("drplanguage4")[0];

                if (lngidx == 1 && value > 0) {
                    if (lng1.value == lng2.value || lng1.value == lng3.value || lng1.value == lng4.value) {
                        ShowMessage($$('lblLang1Msg2').text(), 'MessageType.Errorr');
                        lng1.value = 0;
                        $(lng1).selectpicker('refresh');
                        return false;
                    }
                }
                else if (lngidx == 2 && value > 0) {
                    if (lng2.value == lng1.value || lng2.value == lng3.value || lng2.value == lng4.value) {
                        ShowMessage($$('lblLang2Msg2').text(), 'MessageType.Errorr');
                        lng2.value = 0;
                        $(lng2).selectpicker('refresh');
                        return false;
                    }
                }
                else if (lngidx == 3 && value > 0) {
                    if (lng3.value == lng1.value || lng3.value == lng2.value || lng3.value == lng4.value) {
                        ShowMessage($$('lblLang3Msg2').text(), 'MessageType.Errorr');
                        lng3.value = 0;
                        $(lng3).selectpicker('refresh');
                        return false;
                    }
                }
                else if (lngidx == 4 && value > 0) {
                    if (lng4.value == lng1.value || lng4.value == lng2.value || lng4.value == lng3.value) {
                        ShowMessage($$('lblLang4Msg2').text(), 'MessageType.Errorr');
                        lng4.value = 0;
                        $(lng4).selectpicker('refresh');
                        return false;
                    }
            }

                return true;
        }

        function checkBoxCSS() {
            $(".switch input").bootstrapSwitch();
            $('.switch input').bootstrapSwitch('onText', 'Yes');
            $('.switch input').bootstrapSwitch('offText', 'No');
            $('.switch input').bootstrapSwitch('labelWidth', 'auto');
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {

                    //var $drplanguage1 = $$("drplanguage1");
                    //var $drplanguage2 = $$("drplanguage2");
                    //var $drplanguage3 = $$("drplanguage3");
                    //var $drplanguage4 = $$("drplanguage4");


                    //$drplanguage1.on("change", function () {
                    //    IsValidLanguage(1, $(this).val());
                    //});

                    //$drplanguage2.on("change", function () {
                    //    IsValidLanguage(2, $(this).val());
                    //});

                    //$drplanguage3.on("change", function () {
                    //    IsValidLanguage(3, $(this).val());
                    //});

                    //$drplanguage4.on("change", function () {
                    //    IsValidLanguage(4, $(this).val());
                    //});

                    $$('btnSaveOtherDetails').on('click', function () {
                        return IsValidate();
                    });
                    checkBoxCSS();
                    setEvent();
                }
            });
        };


        $(document).ready(function () {
            $$('btnSaveOtherDetails').on('click', function () {
                return IsValidate();
            });
        });

        function IsValidate() {
            if (parseInt($$('drplanguage1').val()) <= 0 && '<%= Session("Language1Mandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblLang1Msg').text(), 'MessageType.Errorr');
                $$('drplanguage1').focus();
                return false;
            }

            else if ($$('txtMotherTongue').val().trim() == '' && '<%= Session("MotherTongueMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblMotherTongueMsg').text(), 'MessageType.Errorr');
                $$('txtMotherTongue').focus();
                return false;
            }
            else if (parseInt($$('drpMaritalstatus').val()) <= 0 && '<%= Session("MaritalStatusMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblMaritalStatusMsg').text(), 'MessageType.Errorr');
                $$('drpMaritalstatus').focus();
                return false;
            }

            else if ($$('chkImpaired').prop("checked") == true && $$('txtImpairment').val().trim() == '') {
                ShowMessage($$('lblImpairedMsg').text(), 'MessageType.Errorr');
                $$('txtImpairment').focus();
                return false;
            }

            else if ($$('txtCurrent_salary').val().trim() == '' && '<%= Session("CurrentSalaryMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblCurrent_salaryMsg').text(), 'MessageType.Errorr');
                $$('txtCurrent_salary').focus();
                return false;
            }

            else if ($$('txtExpected_salary').length > 0 && $$('txtExpected_salary').val().trim() == '' && '<%= Session("ExpectedSalaryMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblExpected_salaryMsg').text(), 'MessageType.Errorr');
                $$('txtExpected_salary').focus();
                return false;
            }

            else if ($$('txtExpected_benefits').val().trim() == '' && '<%= Session("ExpectedBenefitsMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblExpected_benefitsMsg').text(), 'MessageType.Errorr');
                $$('txtExpected_benefits').focus();
                return false;
            }

            else if ($$('txtNotice_period_days').val().trim() == '' && '<%= Session("NoticePeriodMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblNotice_period_daysMsg').text(), 'MessageType.Errorr');
                $$('txtNotice_period_days').focus();
                return false;
            }


            else if (parseInt($$('txtCurrent_salary').val()) <= 0 && '<%= Session("CurrentSalaryMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblCurrent_salaryMsg').text(), 'MessageType.Errorr');
                $$('txtCurrent_salary').focus();
                return false;
            }

            else if (parseInt($$('txtExpected_salary').val()) <= 0 && '<%= Session("ExpectedSalaryMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblExpected_salaryMsg').text(), 'MessageType.Errorr');
                $$('txtExpected_salary').focus();
                return false;
            }

            else if (parseInt($$('txtNotice_period_days').val()) <= 0 && '<%= Session("NoticePeriodMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblNotice_period_daysMsg').text(), 'MessageType.Errorr');
                $$('txtNotice_period_days').focus();
                return false;
            }

            else if ($$('txtAchievements').length > 0 && $$('txtAchievements').val().trim() == '' && '<%= Session("OtherInfoAchievementMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblAchievementsMsg').text(), 'MessageType.Errorr');
                $$('txtAchievements').focus();
                return false;
            }

            else if ($$('txtJournalsResearchPapers').length > 0 && $$('txtJournalsResearchPapers').val().trim() == '' && '<%= Session("JournalsResearchPapersMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblJournalsResearchPapersMsg').text(), 'MessageType.Errorr');
                $$('txtJournalsResearchPapers').focus();
                return false;
            }

            return true;
        }

        function setEvent() {

            $("[id*=drplanguage1]").on('change', function () {
                if (IsValidLanguage(1, $(this).val()) == true)
                    __doPostBack($(this).get(0).id, 0)
            });

            $("[id*=drplanguage2]").on('change', function () {
                if (IsValidLanguage(2, $(this).val()) == true)
                    __doPostBack($(this).get(0).id, 0);
            });

            $("[id*=drplanguage3]").on('change', function () {
                if (IsValidLanguage(3, $(this).val()) == true)
                    __doPostBack($(this).get(0).id, 0);
            });

            $("[id*=drplanguage4]").on('change', function () {
                if (IsValidLanguage(4, $(this).val()) == true)
                    __doPostBack($(this).get(0).id, 0);
            });

            $$("chkImpaired").on('click', function () {
                __doPostBack($(this).get(0).id, 0);
            });
        }

    </script>

    <div class="card">

        <asp:Panel ID="pnlForm" runat="server">
            <div class="card-header">
                <%--<div style="float: right">
                    
                </div>--%>


                <h4>
                    <asp:Label ID="lblOtherInformation" runat="server">Other Information</asp:Label>
                    <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" />
                </h4>
            </div>

            <div class="card-body">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <h5>
                            <asp:Label ID="lblSubHeader1" runat="server">Languages</asp:Label></h5>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <asp:Label ID="lblLanguagesKnown" runat="server" Text="Languages Known:" Visible="false"></asp:Label>
                                <asp:Label ID="lblLang1" runat="server" Text="Language1:" CssClass="required"></asp:Label>
                                <asp:DropDownList ID="drplanguage1" runat="server" Enabled="true" CssClass="selectpicker form-control" AutoPostBack="false" />
                                <asp:Label ID="lblLang1Msg" runat="server" Text="Please Select Language 1." CssClass="d-none"></asp:Label>
                                <asp:Label ID="lblLang1Msg2" runat="server" Text="Language1 cannot be same as other languages. Please select other language." CssClass="d-none"></asp:Label>
                               <%-- <asp:RequiredFieldValidator ID="rfvLang1" runat="server" Display="None" ControlToValidate="drplanguage1" InitialValue="0"
                                    ErrorMessage="Please Select Language 1" ValidationGroup="OtherInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                            </div>

                            <div class="col-md-4">
                                <asp:Label ID="lbllang2" runat="server" Text="Language2:"></asp:Label>
                                <asp:DropDownList ID="drplanguage2" runat="server" Enabled="true" CssClass="selectpicker form-control" AutoPostBack="false" />
                                <asp:Label ID="lblLang2Msg2" runat="server" Text="Language2 cannot be same as other languages. Please select other language." CssClass="d-none"></asp:Label>
                            </div>

                            <div class="col-md-4">
                                <asp:Label ID="lbllng3" runat="server" Text="Language3:"></asp:Label>
                                <asp:DropDownList ID="drplanguage3" runat="server" Enabled="true" CssClass="selectpicker form-control" AutoPostBack="false" />
                                <asp:Label ID="lblLang3Msg2" runat="server" Text="Language3 cannot be same as other languages. Please select other language." CssClass="d-none"></asp:Label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                <asp:Label ID="lbllng4" runat="server" Text="Language4:"></asp:Label>
                                <asp:DropDownList ID="drplanguage4" runat="server" Enabled="true" CssClass="selectpicker form-control" AutoPostBack="false" />
                                <asp:Label ID="lblLang4Msg2" runat="server" Text="Language4 cannot be same as other languages. Please select other language." CssClass="d-none"></asp:Label>
                            </div>

                            <div class="col-md-4">
                                <%-- <asp:Label ID="lblNationality" runat="server" Text="Nationality:"></asp:Label>
                <asp:DropDownList ID="drpNationality" runat="server" CssClass="selectpicker form-control" />
                <asp:RequiredFieldValidator ID="rfvNationality" runat="server" Display="None" ControlToValidate="drpNationality" InitialValue="0"
                    ErrorMessage="Please Select Nationality" ValidationGroup="OtherInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                <asp:Panel ID="pnlMotherTongue" runat="server">
                                    <asp:Label ID="lblMotherTongue" runat="server" Text="Mother Tongue:" CssClass="required"></asp:Label>
                                    <asp:TextBox ID="txtMotherTongue" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:Label ID="lblMotherTongueMsg" runat="server" Text="Please enter Mother Tongue." CssClass="d-none"></asp:Label>
                                   <%-- <asp:RequiredFieldValidator ID="rfvMotherTongue" runat="server" Display="None"
                                        ControlToValidate="txtMotherTongue" ErrorMessage="Please enter Mother Tongue."
                                        ValidationGroup="OtherInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                </asp:Panel>
                            </div>

                            <div class="col-md-4">
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="drplanguage1" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="drplanguage2" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="drplanguage3" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="drplanguage4" EventName="SelectedIndexChanged" />
                        <%--<asp:PostBackTrigger ControlID="drplanguage1"  />
                <asp:PostBackTrigger ControlID="drplanguage2"  />
                <asp:PostBackTrigger ControlID="drplanguage3"  />
                <asp:PostBackTrigger ControlID="drplanguage4"  />--%>
                    </Triggers>
                </asp:UpdatePanel>

                <h5>
                    <asp:Label ID="lblSubHeader2" runat="server">Other Details</asp:Label></h5>

                <div class="form-group row">
                    <asp:Panel ID="pnlMaritalStatus" runat="server" CssClass="row col-md-8 m-0 p-0">

                        <div class="col-md-6">
                            <asp:Label ID="lblMaritalStatus" runat="server" Text="Marital Status:" CssClass="required"></asp:Label>
                            <asp:DropDownList ID="drpMaritalstatus" runat="server" CssClass="selectpicker form-control" />
                            <asp:Label ID="lblMaritalStatusMsg" runat="server" Text="Please Select Marital Status." CssClass="d-none"></asp:Label>
                            <%--<asp:RequiredFieldValidator ID="rfvMaritalStatus" runat="server" Display="None"
                                ControlToValidate="drpMaritalstatus" ErrorMessage="Please Select Marital Status." InitialValue="0"
                                ValidationGroup="OtherInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                        </div>

                        <div class="col-md-6 col-sm-12 col-md-offset-0">
                            <asp:Label ID="lblMarriedDate" runat="server" Text="Married Date:"></asp:Label>
                            <uc2:DateCtrl ID="dtMarrieddate" runat="server" AutoPostBack="false" />
                            <asp:Label ID="lblMarriedDateMsg" runat="server" Text="Married Date should be less than current Date." CssClass="d-none"></asp:Label>
                            <asp:Label ID="lblMarriedDateMsg2" runat="server" Text="Married Date should be greater than Birth Date." CssClass="d-none"></asp:Label>
                        </div>
                    </asp:Panel>

                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" class="row col-md-4 m-0 p-0">
                        <ContentTemplate>
                            <div class="col-md-12">
                                <asp:Label ID="lblImpaired" runat="server" Text="Impaired:" Visible="false"></asp:Label>
                                <asp:CheckBox ID="chkImpaired" runat="server" Text="Impaired/Disabled (Please specify if Yes)"  AutoPostBack="false" />
                                <asp:TextBox ID="txtImpairment" runat="server" Rows="1" TextMode="MultiLine" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                <asp:Label ID="lblImpairedMsg" runat="server" Text="Please specify Impairment detail." CssClass="d-none"></asp:Label>
                                <%--<asp:RequiredFieldValidator ID="rfvImpairment" runat="server" Display="None"
                                    ControlToValidate="txtImpairment" ErrorMessage="Please specify Impairment detail."
                                    ValidationGroup="OtherInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                            </div>
                        </ContentTemplate>

                    </asp:UpdatePanel>


                </div>

                <div class="form-group row">

                    <div class="col-md-4 p-0">
                        <div class="row">
                            <div class="col-md-7">
                                <asp:Label ID="lblCurrent_salary" runat="server" Text="Current Gross Salary :" CssClass="required"></asp:Label>
                            </div>
                            <div class="col-md-5">
                                <asp:Label ID="lblCurrent_salary_Currency" runat="server" Text="Currency :" CssClass="text-left"></asp:Label>
                            </div>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtCurrent_salary" runat="server" ValidationGroup="OtherInfo" CssClass="form-control text-right" ></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtCurrent_salary" FilterMode="ValidChars" FilterType="Numbers, Custom" ValidChars=".,"></cc1:FilteredTextBoxExtender>
                                <asp:Label ID="lblCurrent_salaryMsg" runat="server" Text="Please enter Current Salary." CssClass="d-none"></asp:Label>
                               <%-- <asp:RangeValidator ID="NumericValidator" runat="server" ControlToValidate="txtCurrent_salary"
                                    CssClass="ErrorControl" ErrorMessage="Current Salary is Invalid!" Display="None"
                                    ForeColor="White" MaximumValue="9999999999" MinimumValue="0" Type="Currency" ValidationGroup="OtherInfo"
                                    SetFocusOnError="True">
                                </asp:RangeValidator>--%>

                              <%--  <asp:RequiredFieldValidator ID="rfvCurrent_salary" runat="server" Display="None"
                                    ControlToValidate="txtCurrent_salary" ErrorMessage="Please enter Current Salary."
                                    ValidationGroup="OtherInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                            </div>

                            <div class="col-md-5">

                                <asp:DropDownList ID="ddlCurrentCurrencyCountry" runat="server" CssClass="selectpicker form-control" />
                            </div>

                        </div>

                    </div>

                    <div class="col-md-4 p-0">
                        <div class="row">
                            <div class="col-md-7">
                                <asp:Label ID="lblExpected_salary" runat="server" Text="Expected Gross Salary :" CssClass="required"></asp:Label>
                            </div>
                            <div class="col-md-5">
                                <asp:Label ID="lblExpected_salary_Currency" runat="server" Text="Currency :"></asp:Label>
                            </div>
                            <div class="col-md-7">
                                <asp:TextBox ID="txtExpected_salary" runat="server" ValidationGroup="OtherInfo" CssClass="form-control text-right"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtExpected_salary" FilterMode="ValidChars" FilterType="Numbers, Custom" ValidChars=".,"></cc1:FilteredTextBoxExtender>
                                 <asp:Label ID="lblExpected_salaryMsg" runat="server" Text="Please enter Expected Salary." CssClass="d-none"></asp:Label>
                               <%-- <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtExpected_salary"
                                    CssClass="ErrorControl" ErrorMessage="Expected Salary should be greater than Zero" Display="None"
                                    ForeColor="White" MaximumValue="9999999999" MinimumValue="0" Type="Currency" ValidationGroup="OtherInfo"
                                    SetFocusOnError="True">
                                </asp:RangeValidator>--%>
                               <%-- <asp:RequiredFieldValidator ID="rfvExpected_salary" runat="server" Display="None"
                                    ControlToValidate="txtCurrent_salary" ErrorMessage="Please enter Expected Salary."
                                    ValidationGroup="OtherInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                            </div>

                            <div class="col-md-5">

                                <asp:DropDownList ID="ddlExpectCurrencyCountry" runat="server" CssClass="selectpicker form-control" />
                            </div>

                        </div>

                    </div>

                    <div class="col-md-4 ">
                        <asp:Label ID="lblExpected_benefits" runat="server" Text="Expected Benefits:" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtExpected_benefits" runat="server" Rows="1" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                        <asp:Label ID="lblExpected_benefitsMsg" runat="server" Text="Please enter Expected Benefits." CssClass="d-none"></asp:Label>
                        <%--<asp:RequiredFieldValidator ID="rfvExpected_benefits" runat="server" Display="None"
                            ControlToValidate="txtExpected_benefits" ErrorMessage="Please enter Expected Benefits."
                            ValidationGroup="OtherInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                    </div>

                </div>

                <div class="form-group row">

                    <div class="col-md-4">
                        <asp:Label ID="lblWilling_to_relocate" runat="server" Text="Willing to Relocate" CssClass="font-weight-bold"></asp:Label>
                        <%--<asp:DropDownList ID="ddlWilling_to_relocate" runat="server" CssClass="selectpicker form-control" />--%>
                        <asp:CheckBox ID="chkWilling_to_relocate" runat="server" CssClass="switch float-right"  AutoPostBack="false" />
                    </div>

                    <div class="col-md-4">
                        <asp:Label ID="lblWilling_to_travel" runat="server" Text="Willing to Travel" CssClass="font-weight-bold"></asp:Label>
                        <%--<asp:DropDownList ID="ddlWilling_to_travel" runat="server" CssClass="selectpicker form-control" />--%>
                        <asp:CheckBox ID="chkWilling_to_travel" runat="server" CssClass="switch float-right"  AutoPostBack="false" />
                    </div>

                    <div class="col-md-4">
                        <asp:Label ID="lblNotice_period_days" runat="server" Text="Notice Period (in Days):" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtNotice_period_days" runat="server" ValidationGroup="OtherInfo" CssClass="form-control"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtNotice_period_days" FilterMode="ValidChars" FilterType="Numbers"></cc1:FilteredTextBoxExtender>
                        <asp:Label ID="lblNotice_period_daysMsg" runat="server" Text="Please enter Notice Period (in Days)." CssClass="d-none"></asp:Label>
                       <%-- <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtNotice_period_days"
                            CssClass="ErrorControl" ErrorMessage="Please enter Notice Period (in Days)." Display="None"
                            ForeColor="White" MaximumValue="999" MinimumValue="0" Type="Integer" ValidationGroup="OtherInfo"
                            SetFocusOnError="True">
                        </asp:RangeValidator>--%>
                        <%--<asp:RequiredFieldValidator ID="rfvNotice_period_days" runat="server" Display="None"
                            ControlToValidate="txtNotice_period_days" ErrorMessage="Please enter Notice Period (in Days)."
                            ValidationGroup="OtherInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                    </div>

                </div>

                <%--<div class="form-group row">

            <div class="col-md-4">
                <asp:Label ID="lblEarliest_possible_startdate" runat="server" Text="Earliest Possible Start Date:"></asp:Label>
                <uc2:DateCtrl ID="dtpEarliest_possible_startdate" runat="server" AutoPostBack="false" />
                <asp:RequiredFieldValidator ID="rfvEarliest_possible_startdate" runat="server" Display="None"
                            ControlToValidate="dtpEarliest_possible_startdate" ErrorMessage="Please enter Earliest Possible Start Date."
                            ValidationGroup="OtherInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </div>

            <div class="col-md-4">
                <asp:Label ID="lblComments" runat="server" Text="Comments:"></asp:Label>
                <asp:TextBox ID="txtComments" runat="server" Rows="1" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvComments" runat="server" Display="None"
                            ControlToValidate="txtComments" ErrorMessage="Please enter Comments."
                            ValidationGroup="OtherInfo" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </div>

            <div class="col-md-4">
            </div>

        </div>--%>

                <div class="form-group row">
                    <div class="col-md-12 mb-1">
                        <asp:Label ID="lblMembership" runat="server" Text="Membership:" Visible="false"></asp:Label>
                        <asp:Label ID="lblMemberships" runat="server" Text="Professional Memberships (Include Membership No.)"></asp:Label>
                        <asp:TextBox ID="txtMemberships" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                    </div>

                    <asp:Panel ID="pnlAchievement" runat="server" class="col-md-12 mb-1">
                        <asp:Label ID="lblAchievement" runat="server" Text="Achievements:" Visible="false"></asp:Label>
                        <asp:Label ID="lblAchievements" runat="server" Text="Professional Awards / Achievements"></asp:Label>
                        <asp:TextBox ID="txtAchievements" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                        <asp:Label ID="lblAchievementsMsg" runat="server" Text="Please enter Professional Awards / Achievements." CssClass="d-none"></asp:Label>
                    </asp:Panel>

                    <asp:Panel ID="pnlJournalsResearchPaper" runat="server" class="col-md-12 mb-1">
                        <asp:Label ID="lblJournalsResearchPaper" runat="server" Text="Links / Journals / Research Papers:" Visible="false"></asp:Label>
                        <asp:Label ID="lblJournalsResearchPapers" runat="server" Text="Journals/Research Papers/ Newspaper Articles (Add link or title)"></asp:Label>
                        <asp:TextBox ID="txtJournalsResearchPapers" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                        <asp:Label ID="lblJournalsResearchPapersMsg" runat="server" Text="Please enter Links / Journals / Research Papers." CssClass="d-none"></asp:Label>
                    </asp:Panel>

                </div>

                <%--<div class="form-group row">
                    
                </div>

                <div class="form-group row">
                    
                </div>--%>

               <%-- <div class="form-group row">
                    <div class="col-md-12" style="text-align: center;">
                        <asp:ValidationSummary ID="vs"
                            HeaderText="You must enter a value in the following fields:"
                            DisplayMode="BulletList"
                            EnableClientScript="true"
                            runat="server" ValidationGroup="OtherInfo" Style="color: red" />
                    </div>
                </div>--%>

                <%-- <div class="form-group row">
            <div class="col-md-12" style="text-align: center;">
                <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="Something Bad happened, Please contact Administrator!!!!" Visible="false" />
            </div>
        </div>--%>
            </div>

            <div class="card-footer">
                <div class="form-group row d-flex justify-content-center">
                    <%--<div class="col-md-4">
                    </div>--%>

                    <div class="col-md-4">
                        <asp:Button ID="btnSaveOtherDetails" runat="server" CssClass="btn btn-primary w-100"  Text="Update"
                            ValidationGroup="OtherInfo" />
                        <asp:Label ID="lblSaveOtherDetailsMsg" runat="server" Text="Language and Other Information Saved Successfully! \n\n Click Ok to continue with other section(s)." CssClass="d-none"></asp:Label>
                    </div>

                    <%--<div class="col-md-4">
                    </div>--%>
                </div>
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hfCancelLang" runat="server" />
        <cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsApplicant" _ModuleName="OtherInfo" _TargetControl="pnlForm" CancelControlName="hfCancelLang" />
    </div>

    <script type="text/javascript">
        $('#<%= txtCurrent_salary.ClientID %>').blur(function () {
            $('#<%= txtCurrent_salary.ClientID %>').val(addCommas($('#<%= txtCurrent_salary.ClientID %>').val()))
        });
        $('#<%= txtExpected_salary.ClientID %>').blur(function () {
            $('#<%= txtExpected_salary.ClientID %>').val(addCommas($('#<%= txtExpected_salary.ClientID %>').val()))
        });

        function addCommas(nStr) {
            if (nStr.toString().indexOf('.') < 0)
                nStr = nStr.toString().concat('.00');


            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }
    </script>

</asp:Content>
