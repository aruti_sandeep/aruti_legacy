﻿<%@ Page Title="Attachment" Language="vb" AutoEventWireup="false" MasterPageFile="~/User/Site4.Master" CodeBehind="MiscAttachment.aspx.vb" Inherits="Aruti_Online_Recruitment.MiscAttachment" %>

<%@ Register Src="../Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">

        $(document).ready(function () {
            var fupld = document.getElementById('<%= flUpload.ClientID %>' + '_image_file');
            if (fupld != null)
                fileUpLoadChange(fupld);

            //$(".switch input").bootstrapSwitch();
            //$('.switch input').bootstrapSwitch('onText', 'Yes');
            //$('.switch input').bootstrapSwitch('offText', 'No'); 


            $("[id*=flUpload]").on('click', function () {
                return IsValidAttach();
            });

            $("[id*=btnCertidelete]").on('click', function () {
                if (!ShowConfirm($$('lblConfirmMsg').text(), this))
                    return false;
                else
                    return true;
            });

        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                $("[id*=flUpload]").on('click', function () {
                    return IsValidAttach();
                });

                $("[id*=btnCertidelete]").on('click', function () {
                    if (!ShowConfirm($$('lblConfirmMsg').text(), this))
                        return false;
                    else
                        return true;
                });

            });
        };

        function pageLoad(sender, args) {
            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }


        function IsValidAttach() {

            var doctype = document.getElementById('<%= ddlDocType.ClientID %>');

            //if (parseInt(doctype.value) <= 0) {
            //    //alert('Please Select Document Type.');
            //    BootstrapDialog.show({
            //        type: BootstrapDialog.TYPE_INFO,
            //        closable: true, // <-- Default value is false
            //        draggable: true, // <-- Default value is false
            //        title: 'Aruti',
            //        message: 'Please Select Document Type.',
            //        buttons: [{
            //            label: 'Ok',
            //            cssClass: 'btn-primary',
            //            action: function (dialogItself) {
            //                dialogItself.close();
            //            }
            //        }]
            //    });

            //    doctype.focus();
            //    return false;
            //}


            var attach = document.getElementById('<%= ddlDocTypeQuali.ClientID %>');

            <%--var cntQuali = $('#<%= grdQualification.ClientID %> tr').filter(function () {
                return $(this).css('display') !== 'none';
            }).length;

            if (cntQuali <= 1) {
                //alert('Please add atleast one Qualification from Qualification Tab.');
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_INFO,
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: 'Aruti',
                    message: 'Please add atleast one Qualification.',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function (dialogItself) {
                            dialogItself.close();
                        }
                    }]
                });
                return false;
            }--%>

            //if (parseInt(attach.value) <= 0) {
            //    //alert('Please Select Document Type.');
            //    BootstrapDialog.show({
            //        type: BootstrapDialog.TYPE_INFO,
            //        closable: true, // <-- Default value is false
            //        draggable: true, // <-- Default value is false
            //        title: 'Aruti',
            //        message: 'Please Select Attachment Type.',
            //        buttons: [{
            //            label: 'Ok',
            //            cssClass: 'btn-primary',
            //            action: function (dialogItself) {
            //                dialogItself.close();
            //            }
            //        }]
            //    });

            //    attach.focus();
            //    return false;
            //}

            if ($$('ddlDocType').val() <= 0) {
                ShowMessage($$('lblDocTypeMsg').text(), 'MessageType.Errorr');
                $$('ddlDocType').focus();
                return false;
            }
            else if ($$('ddlDocTypeQuali').val() <= 0) {
                ShowMessage($$('lblDocTypeQualiMsg').text(), 'MessageType.Errorr');
                $$('ddlDocTypeQuali').focus();
                return false;
            }
            else if ($('select[id*=ddlDocType]').val() != 3 && $('table[id*=gvQualiCerti] tr > .attrefid input').filter(function () { return $(this).val() == $('select[id*=ddlDocType]').val() }).length > 0) {
                //Not qualification attachment then
                ShowMessage($$('lblDocTypeExistMsg').text(), 'MessageType.Errorr');
                $$('ddlDocType').focus();
                return false;
            }


        }

    </script>

    <div class="card">


        <asp:Panel ID="pnlForm" runat="server" Visible="false">
            <div class="card-header">
                <%--<div style="float: right">
                    
                </div>--%>

                <h4>
                    <asp:Label ID="lblHeader" runat="server">Attachments</asp:Label>
                    <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" />
                </h4>

                <%--<h4>Misc. Attachments</h4>--%>
            </div>

            <div class="card-body">
                <div class="form-group row">
                    <div class="col-md-4">
                        <asp:Label ID="lblDocType" runat="server" Text="Document Type" CssClass="required"></asp:Label>
                        <asp:DropDownList ID="ddlDocType" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>
                        <asp:Label ID="lblDocTypeMsg" runat="server" CssClass="d-none" Text="Please select Document Type."></asp:Label>
                        <asp:Label ID="lblDocTypeExistMsg" runat="server" CssClass="d-none" Text="Sorry, This Document Type attachment is already exist."></asp:Label>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None" ControlToValidate="ddlDocType" ErrorMessage="Please select Document Type."
                            InitialValue="0" ValidationGroup="Attach" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="col-md-8">
                        <asp:Label ID="objlblRequired" runat="server" Text="" CssClass="text-danger"></asp:Label>
                        <%--Style="color: red;"--%>
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-4">
                        <asp:Label ID="lblDocTypeQuali" runat="server" Text="Attachment Type" CssClass="required"></asp:Label>
                        <asp:DropDownList ID="ddlDocTypeQuali" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>
                        <asp:Label ID="lblDocTypeQualiMsg" runat="server" CssClass="d-none" Text="Please select Attachment Type."></asp:Label>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="None" ControlToValidate="ddlDocTypeQuali" ErrorMessage="Please select Attachment Type."
                            InitialValue="0" ValidationGroup="Attach" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                    </div>

                    <div class="col-md-4">
                        <asp:Label ID="lblUpload" runat="server" Text="Select Certificate (Image / PDF) (All file size should not exceed more than 3 MB.)" CssClass="d-block">   <%--Style="display: block;"--%>
                        </asp:Label>
                    </div>

                    <div class="col-md-4">
                        <%--<uc9:FileUpload ID="flUpload" runat="server" MaxSizeKB="1048576" OnClick="return IsValidAttach();" />--%>
						<uc9:FileUpload ID="flUpload" runat="server" MaxSizeKB="5242880" /> <%-- OnClick="return IsValidAttach();"--%>
                        <asp:Label ID="objlblTotSize" runat="server" Text=""></asp:Label>
                        <asp:Label ID="lblConfirmMsg" runat="server" CssClass="d-none" Text="Are you sure you want to permanently delete this attachment?"></asp:Label>
                        <asp:Label ID="lblAttachExistMsg" runat="server" CssClass="d-none" Text="Sorry, Selected file with same name is already added to the list."></asp:Label>
                        <asp:Label ID="lblAttachImageMsg" runat="server" CssClass="d-none" Text="Please select proper Image file or PDF file."></asp:Label>
                        <asp:Label ID="lblAttachSizeMsg" runat="server" CssClass="d-none" Text="Sorry,You cannot upload file greater than #maxsize# MB."></asp:Label>
                            <asp:Label ID="lblAttachDeleteMsg" runat="server" CssClass="d-none" Text="Attachment Deleted Successfully! \n\n Click Ok to add another Attachment or to continue with other section(s)."></asp:Label>
                            <asp:Label ID="lblAttachSaveMsg" runat="server" CssClass="d-none" Text="Attachment Saved Successfully! \n\n Click Ok to add another Attachment or to continue with other section(s)."></asp:Label>
                        <asp:Label ID="lblQualiAttachMsg" runat="server" CssClass="d-none" Text="Qualification Attachments."></asp:Label>
                        <asp:Label ID="lblCVAttachMsg" runat="server" CssClass="d-none" Text="Curriculum Vitae Attachment."></asp:Label>
                        <asp:Label ID="lblCLAttachMsg" runat="server" CssClass="d-none" Text="Cover Letter Attachments."></asp:Label>
                    </div>

                </div>
            </div>

            <asp:UpdatePanel ID="updgvQualiCerti" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnl_gvQualiCerti" runat="server" CssClass="table-responsive overflow-auto">

                        <asp:GridView ID="gvQualiCerti" runat="server" AutoGenerateColumns="False" CssClass="table" ShowHeaderWhenEmpty="true"
                            AllowPaging="false" DataKeyNames="attachfiletranunkid, filepath, attachrefid">
                            <Columns>

                                <asp:TemplateField HeaderText="Document Type" FooterText="colhDocType">
                                    <ItemTemplate>
                                        <asp:Label ID="objlblDocType" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("attachrefname"), True)  %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="File Name" FooterText="colhFileName">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="objlblFilename" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("filename"))  %>'></asp:Label>--%>
                                        <asp:Label ID="objlblFilename" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("filename"), True)  %>'></asp:Label>
                                    </ItemTemplate>
                                    <%-- <EditItemTemplate>
                                    <asp:TextBox ID="objtxtFilename" runat="server" CssClass="form-control"></asp:TextBox>
                                </EditItemTemplate>--%>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="File Path" Visible="false" FooterText="objcolhFilePath">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="objlblFilepath" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("filepath"))  %>'></asp:Label>--%>
                                        <asp:Label ID="objlblFilepath" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("filepath"), True)  %>'></asp:Label>
                                    </ItemTemplate>
                                    <%--<EditItemTemplate>
                                    <asp:TextBox ID="objtxtFilepath" runat="server" CssClass="form-control"></asp:TextBox>
                                </EditItemTemplate>--%>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="File Size" FooterText="colhFileSize">
                                    <HeaderStyle CssClass="text-right" />
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%--<asp:Label ID="objlblFilesize" runat="server" Text='<%# If(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("file_size")) = 0, "Less than 1024 KB", Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("file_size")) & " KB")  %>'></asp:Label>--%>
                                        <asp:Label ID="objlblFilesize" runat="server" Text='<%# If(AntiXss.AntiXssEncoder.HtmlEncode(Eval("file_size"), True) = 0, "Less than 1024 KB", AntiXss.AntiXssEncoder.HtmlEncode(Eval("file_size"), True) & " KB")  %>'></asp:Label>
                                    </ItemTemplate>
                                    <%--<EditItemTemplate>
                                    <asp:TextBox ID="objtxtFilesize" runat="server" CssClass="form-control"></asp:TextBox>
                                </EditItemTemplate>--%>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Download" FooterText="colhDownload" HeaderStyle-CssClass="width90px" ItemStyle-CssClass="width90px"><%--HeaderStyle-Width="90px" ItemStyle-Width="90px"--%>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDownload" runat="server" CommandName="Download" CommandArgument="<%# Container.DataItemIndex %>" CausesValidation="false">Download</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Delete" FooterText="colhDelete">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
										<asp:LinkButton ID="btnCertidelete" runat="server" CommandName="Remove" CommandArgument="<%# Container.DataItemIndex %>" Text="<i aria-hidden='true' class='fa fa-trash'></i>" ></asp:LinkButton>  <%--OnClientClick='if(!ShowConfirm("Are you sure you want to permanently delete this attachment?", this)) return false; else return true;'--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ShowHeader="false" HeaderStyle-CssClass="d-none" ItemStyle-CssClass="d-none attrefid">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hfAttachrefid" runat="server" Value='<%# Eval("attachrefid") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                    </asp:Panel>
                    &#160;<asp:Button ID="objbtnRemoveImage" runat="server" Text="Remove Image" Visible="false" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="gvQualiCerti" />
                    <asp:PostBackTrigger ControlID="flUpload" />
                    <%--<asp:AsyncPostBackTrigger ControlID="gvQualiCerti" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="gvQualiCerti" EventName="DataBound" />--%>
                </Triggers>
            </asp:UpdatePanel>
            <%--<div id="divAttachQuali" class="panel-default">
            <div id="Div19" class="panel-heading-default panel-height">
                <div style="float: left;">
                    <asp:Label ID="lblAttachQuali" runat="server" Text="Qualification Certificate Attachments"></asp:Label>
                </div>
            </div>
            <div id="Div20" class="panel-body-default">
                <asp:UpdatePanel ID="UPUpload" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="responsive">
                            <div class="newrow">
                                <div class="lbl">
                                    <asp:Label ID="lblDocTypeQuali" runat="server" Text="Document Type"></asp:Label>
                                </div>
                                <div class="ctrl">
                                    <uc1:dropdownlist id="ddlDocTypeQuali" runat="server" autopostback="false"></uc1:dropdownlist>
                                </div>
                                <div class="col-1-2-3-4">
                                    <div class="divlblUpload" style="float: left;">
                                        <asp:Label ID="lblUpload" runat="server" Text="Select Certificate (Image / PDF)"
                                            Style="display: block;"></asp:Label>
                                        <asp:Label ID="lblUpload2" runat="server" Text="(Max. size 1 MB, Max. attachments 3.)"></asp:Label>
                                    </div>
                                    <div class="divflUpload" style="float: left;">
                                        <uc9:fileupload id="flUpload" runat="server" maxsizekb="1048576" onclick="return IsValidAttach();" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
            </div>
        </div>--%>
        </asp:Panel>

    </div>
    <asp:HiddenField ID="hfCancelLang" runat="server" />
    <cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsApplicant" _ModuleName="MiscAttachment" _TargetControl="pnlForm" CancelControlName="hfCancelLang" />
</asp:Content>
