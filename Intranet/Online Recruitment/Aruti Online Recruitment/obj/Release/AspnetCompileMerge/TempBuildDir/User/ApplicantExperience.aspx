﻿<%@ Page Title="Job Experience" Language="vb" AutoEventWireup="false" MasterPageFile="~/User/Site4.Master" CodeBehind="ApplicantExperience.aspx.vb" Inherits="Aruti_Online_Recruitment.ApplicantExperience" %>

<%@ Register Src="../Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>
<%@ Register Src="../Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

	<script type="text/javascript">

        //'S.SANDEEP |04-MAY-2023| -- START
        //'ISSUE/ENHANCEMENT : A1X-833        
        function pageLoad(sender, args) {
            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }
        function IsValidAttach(objFile) {
            if ($$('ddlDocType').val() <= 0) {
                ShowMessage($$('lblDocTypeMsg').text(), 'MessageType.Errorr');
                $$('ddlDocType').focus();
                return false;
            }
            else if ($$('ddlAttachType').val() <= 0) {
                ShowMessage($$('lblAttachTypeMsg').text(), 'MessageType.Errorr');
                $$('ddlAttachType').focus();
                return false;
            }
            return true;
        }
        function setEvent() {
            $$("chkYesNo").on('click', function () {
                __doPostBack($(this).get(0).id, 0);
            });

            var fupld = document.getElementById('<%= flUpload.ClientID %>' + '_image_file');
            if (fupld != null)
                fileUpLoadChange(fupld);

            $("[id*=flUpload]").on('click', function () {
                return IsValidAttach();
            });

            $('.close').on('click', function () {
                $(this).parents('[id*=pnlFile]').find('span[id*=lblFileName]').text('');
                $(this).parents('[id*=pnlFile]').find('input[id*=hfFileName]').val('');
                $(this).parents('[id*=pnlFile]').hide();
            });
        }
        //'S.SANDEEP |04-MAY-2023| -- END
		var prm = Sys.WebForms.PageRequestManager.getInstance();
		if (prm != null) {
			prm.add_endRequest(function (sender, e) {
				$$('btnAddJoB').on('click', function () {
					return IsValidate();
				});

				$("[id*=btndelete]").on('click', function () {
                    if (!ShowConfirm($$('lblgrdJobRowDeletingMsg2').text(), this)) return false; else return true;
				});

                setEvent();
			});
		};

		$(document).ready(function () {
			$$('btnAddJoB').on('click', function () {
				return IsValidate();
			});


			$("[id*=btndelete]").on('click', function () {
                if (!ShowConfirm($$('lblgrdJobRowDeletingMsg2').text(), this)) return false; else return true;
			});

            setEvent();
		});

	function IsValidate() {
			if ($$('txtEmployer').val().trim() == '' && '<%= Session("EmployerMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblEmployerMsg').text(), 'MessageType.Errorr');
				$$('txtEmployer').focus();
				return false;
			}
			else if ($$('txtCompName').val().trim() == '' && '<%= Session("CompanyNameJobHistoryMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblCompNamMsg').text(), 'MessageType.Errorr');
				$$('txtCompName').focus();
				return false;
			}
			else if ($$('txtCompName').val().trim() == '' && $$('txtEmployer').val().trim() == '') {
                ShowMessage($$('lblCompNamMsg').text(), 'MessageType.Errorr');
				$$('txtCompName').focus();
				return false;
			}
			else if ($$('txtDesignation').val().trim() == '' && '<%= Session("PositionHeldMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblDesignationMsg').text(), 'MessageType.Errorr');
				$$('txtDesignation').focus();
				return false;
			}
            else if ($$('txtAchievement').length > 0 && $$('txtAchievement').val().trim() == '' && '<%= Session("JobAchievementMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblAchievementMsg').text(), 'MessageType.Errorr');
                $$('txtAchievement').focus();
                return false;
            }

			return true;
		}

</script>


    <div class="card">

        <asp:Panel ID="pnlForm" runat="server">
            <div class="card-header">
                <%--<div style="float: right">
                   
                </div>--%>

                <h4>
                    <asp:Label ID="lblHeader" runat="server">Job Experience Details</asp:Label>
                     <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" />
                </h4>
                <asp:Label ID="lblWorkExperience" runat="server" Visible="false">Work Experience</asp:Label>
            </div>

            <div class="card-body">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <asp:Label ID="lblEmployer" runat="server" Text="Employer:"></asp:Label>
                            <asp:TextBox ID="txtEmployer" runat="server" MaxLength="250" CssClass="form-control"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator ID="rfvEmployer" runat="server"  Display="None"
                                ControlToValidate="txtEmployer" ErrorMessage="Employer Name can not be Blank. "
                                ValidationGroup="JobHistory" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                            <asp:Label ID="lblEmployerMsg" runat="server" Text="Employer Name is compulsory information, it can not be blank. Please give Employer Name to continue." CssClass="d-none"></asp:Label>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblCompNam" runat="server" Text="Company Name:" CssClass="required"></asp:Label>
                            <asp:TextBox ID="txtCompName" runat="server" MaxLength="250" CssClass="form-control"></asp:TextBox>
                          <%--  <asp:RequiredFieldValidator ID="rfvCompanyName" runat="server"  Display="None"
                                ControlToValidate="txtCompName" ErrorMessage="Company Name can not be Blank. "
                                ValidationGroup="JobHistory" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                              <asp:Label ID="lblCompNamMsg" runat="server" Text="Company Name is compulsory information, it can not be blank. Please give Company Name to continue." CssClass="d-none"></asp:Label>
                        </div>

                        <div class="col-md-4">
                            <asp:Label ID="lblOfficePhone" runat="server" Text="Office Phone:"></asp:Label>
                            <asp:TextBox ID="txtOfficePhone" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            <asp:Label ID="lblDesignation" runat="server" Text="Position Held:"></asp:Label>
                            <asp:TextBox ID="txtDesignation" runat="server" MaxLength="250" CssClass="form-control"></asp:TextBox>
                           <%-- <asp:RequiredFieldValidator ID="rfvPositionHeld" runat="server" CssClass="d-none"
                                ControlToValidate="txtDesignation" ErrorMessage="Position held can not be Blank. "
                                ValidationGroup="JobHistory" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                              <asp:Label ID="lblDesignationMsg" runat="server" Text="Position held is compulsory information, it can not be blank. Please give Position held to continue." CssClass="d-none"></asp:Label>
                        </div>

                        <div class="col-md-4 col-sm-12 col-md-offset-0">
                            <asp:Label ID="lblDuration" runat="server" Text="Duration:" Visible="false"></asp:Label>
                            <asp:Label ID="lbljoinDate" runat="server" Text="Join Date:" CssClass="required"></asp:Label>
                            <uc2:DateCtrl ID="dtjoindate" runat="server" AutoPostBack="false" />
                             <asp:Label ID="lblDurationMsg1" runat="server" Text="Joining Date is mandatory information. Please Enter Valid Joining Date." CssClass="d-none"></asp:Label>
                             <asp:Label ID="lblDurationMsg2" runat="server" Text="Joining Date should be less than current Date." CssClass="d-none"></asp:Label>
                        </div>

                        <div class="col-md-4 col-sm-12 col-md-offset-0">
                            <asp:Label ID="lblTermDate" runat="server" Text="Termination Date:" CssClass="required"></asp:Label>
                            <uc2:DateCtrl ID="dtTerminationDate" runat="server" AutoPostBack="false" />
                             <asp:Label ID="lblTermDateMsg1" runat="server" Text="Termination Date should be less than current Date." CssClass="d-none"></asp:Label>
                             <asp:Label ID="lblTermDateMsg2" runat="server" Text="Termination should  be greater than Join Date" CssClass="d-none"></asp:Label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                            <asp:Label ID="lblRes" runat="server" Text="Duties (Summary):"></asp:Label>
                            <asp:TextBox ID="txtResponsebilty" runat="server" MaxLength="500" TextMode="MultiLine" CssClass="form-control"> </asp:TextBox>
                        </div>

                            <asp:Panel ID="pnlAchievement" runat="server" class="col-md-4 mb-1">
                            <asp:Label ID="lblAchievement" runat="server" Text="Achievement:"></asp:Label>
                            <asp:TextBox ID="txtAchievement" runat="server" MaxLength="50" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                <asp:Label ID="lblAchievementMsg" runat="server" Text="Achievement is compulsory information, it can not be blank. Please give Achievement to continue." CssClass="d-none"></asp:Label>
                            </asp:Panel>

                        <div class="col-md-4">
                            <asp:Label ID="lblLeavingReson" runat="server" Text="Leaving Reason:"></asp:Label>
                            <asp:TextBox ID="txtLeavingreson" runat="server" MaxLength="50" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4">
                        </div>

                        <div class="col-md-8">
                            <%--<asp:Label ID="lblJobTerminationMsg" Style="" runat="server" ForeColor="Red" Text="* For current job please give today's date in termination date."></asp:Label>--%>
                                <asp:Label ID="lblJobTerminationMsg" runat="server" CssClass="text-danger" Text="* For current job please leave termination date blank."></asp:Label>
                                <%--ForeColor="Red" --%>
                        </div>

                    </div>
                        <%--'S.SANDEEP |04-MAY-2023| -- START--%>
                        <%--'ISSUE/ENHANCEMENT : A1X-833--%>
                        <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() = "TRA" Then %>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <asp:CheckBox ID="chkYesNo" runat="server" AutoPostBack="false" Text="Government Agency?" Font-Bold="true" />
                            </div>

                        </div>
                        <asp:Panel ID="pnlGovtInfo" runat="server" Enabled="false">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Panel ID="pnlAttachMsg" runat="server">
                                        <asp:Label ID="lblUpload" runat="server" Font-Bold="true" Text="Select file (Image / PDF) (All file size should not exceed more than 5 MB.)" CssClass="d-block">                                                    
                                                        </asp:Label>
                                    </asp:Panel>
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Panel ID="pnlCV" runat="server" CssClass="form-group row attachdoc">
                                        <div class="col-md-3">
                                            <asp:Label ID="lblDocTypeCV" runat="server" Text="Document Type" CssClass="required"></asp:Label>
                                            <asp:DropDownList ID="ddlDocType" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ID="lblAttachTypeCV" runat="server" Text="Attachment Type" CssClass="required"></asp:Label>
                                            <asp:DropDownList ID="ddlAttachType" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-2 mt-3">
                                            <asp:Panel ID="pnluploadbutton" runat="server" Enabled="false">
                                                <uc9:fileupload id="flUpload" runat="server" maxsizekb="5242880" />
                                            </asp:Panel>
                                        </div>
                                        <div class="col-md-4 mt-2">
                                            <asp:Panel ID="pnlFile" runat="server" CssClass="card rounded" Visible="false">
                                                <div class="row no-gutters m-0">
                                                    <div class="col-md-3 text-center pt-3 fileext">
                                                        <asp:Label ID="lblFileExt" runat="server" Text="PDF"></asp:Label>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="card-body py-1 pr-2">
                                                            <button type="button" class="close pr-1" aria-label="Close"><span aria-hidden="true">&times</span></button>
                                                            <h6 class="card-title m-0">
                                                                <asp:Label ID="lblFileName" runat="server" Text=""></asp:Label></h6>
                                                            <p class="card-text">
                                                                <small class="text-muted">
                                                                    <asp:Label ID="lblDocSize" runat="server" Text="0.0 MB"></asp:Label></small>
                                                            </p>
                                                            <asp:HiddenField ID="hfDocSize" runat="server" Value="" />
                                                            <asp:HiddenField ID="hfFileName" runat="server" Value="" />
                                                            <asp:HiddenField ID="hftranid" runat="server" Value="0" />
                                                            <asp:HiddenField ID="hfOldFileName" runat="server" Value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </asp:Panel>

                        <%End If %>
                        <%--'S.SANDEEP |04-MAY-2023| -- END--%>
                    <div class="form-group row">
                            <div class="col-md-12 text-center">
                                <%-- style="text-align: center;"--%>
                          <%--  <asp:ValidationSummary ID="vs"
                                HeaderText="You must enter a value in the following fields:"
                                DisplayMode="BulletList"
                                EnableClientScript="true"
                                runat="server" ValidationGroup="JobHistory" CssClass="text-danger"  />--%>   <%--Style="color: red"--%>
                        </div>
                    </div>

                    <%--<div class="form-group row">
                    <div class="col-md-12" style="text-align: center;">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="Something Bad happened, Please contact Administrator!!!!" Visible="false" />
                    </div>
                </div>--%>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnAddJoB" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            </div>

            <div class="card-footer">
            <div class="form-group row d-flex justify-content-center">
                <%--<div class="col-md-4">
                </div>--%>

                <div class="col-md-4 ">
                    <asp:Button ID="btnAddJoB" runat="server" CssClass="btn btn-primary w-50"  Text="Add Job History"
                        ValidationGroup="JobHistory" />
                    <asp:Label ID="lblbtnAddJoBMsg1" runat="server" Text="Sorry, Selected Experience is already added to the list." CssClass="d-none"></asp:Label>
                    <asp:Label ID="lblbtnAddJoBMsg2" runat="server" Text="Job Experience Saved Successfully! \n\n Click Ok to add another Job Experience or to continue with other section(s)." CssClass="d-none"></asp:Label>
                </div>

                <%--<div class="col-md-4">
                    <%--<asp:Button ID="btnjobEdit" runat="server" CssClass="btn btn-primary" Style="width: 100%" Enabled="False"
                    Text="Update Job History" ValidationGroup="JobHistory" OnClientClick="return IsValidJobHistory();" />--%>
                <%--</div>--%>
            </div>
            </div>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnl_grdJob" runat="server" CssClass="table-responsive overflow-auto min mh-100">
                        <%-- Style="min-height: 300px;"--%>
                        <asp:ListView ID="lvJob" runat="server" ItemPlaceholderID="JobItemsPlaceHolder" DataSourceID="objodsJob" DataKeyNames="jobhistorytranunkid">
                            <LayoutTemplate>
                                <div class="form-group row">

                                    <div class="col-md-12 text-center font-weight-bold">
                                        <asp:Label ID="lblJobHistoryList" runat="server" Text="Job History list"></asp:Label>
                                        <hr class="mt-0" />
                                    </div>
                                </div>
                                <asp:PlaceHolder ID="JobItemsPlaceHolder" runat="server"></asp:PlaceHolder>
                            </LayoutTemplate>
                                    <ItemTemplate>
                                <div class="form-group row">
                                    <div class="col-md-10 mb-1">
                                        <div class="form-group row mb-0">
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvItemEmployer" runat="server" Text="Employer:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvEmployer" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Employer"), True)  %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lblItemCompany" runat="server" Text="Company:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvCompany" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("company"), True)  %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvItemPhone" runat="server" Text="Phone:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvPhone" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Phone"), True)  %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lblItemDesignation" runat="server" Text="Position Held:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvDesignation" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Designation"), True)  %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvItemJoinDate" runat="server" Text="Joining Date:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvJoinDate" runat="server" Text='<%# CDate(AntiXss.AntiXssEncoder.HtmlEncode(Eval("JoinDate"), True)).ToShortDateString %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvItemterminationdate" runat="server" Text="Terminat. Date:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvterminationdate" runat="server" Text='<%# IIf(CDate(AntiXss.AntiXssEncoder.HtmlEncode(Eval("terminationdate"), True)).Date <> CDate(AntiXss.AntiXssEncoder.HtmlEncode("01/Jan/1900", True)).Date, CDate(AntiXss.AntiXssEncoder.HtmlEncode(Eval("terminationdate"), True)).ToShortDateString, AntiXss.AntiXssEncoder.HtmlEncode("Present", True)) %>'></asp:Label>
                                            </div>

                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lbllvItemResponsibility" runat="server" Text="Duties:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlblResponsibility" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Responsibility"), True)  %>'></asp:Label>
                                            </div>
                                            <asp:Panel ID="pnllvEditItemAchievement" runat="server" class="col-md-2 px-2">
                                                <asp:Label ID="ItemAchievement" runat="server" Text="Achievement:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlblAchievement" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("achievements"), True)  %>'></asp:Label>
                                            </asp:Panel>

                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvItemleavingReason" runat="server" Text="Leaving Reason:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvleavingReason" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("leavingReason"), True)  %>'></asp:Label>
                                            </div>
                                            <%--'S.SANDEEP |04-MAY-2023| -- START--%>
                                            <%--'ISSUE/ENHANCEMENT : A1X-833--%>
                                            <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() = "TRA" Then %>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvItemIsgovJob" runat="server" Text="Government Agency:" CssClass="font-weight-bold"></asp:Label>
                                                <br />
                                                <asp:Label ID="objlbllvItemIsgovJob" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("govtagency"), True)  %>'></asp:Label>
                                                <asp:HiddenField ID="objlblIsGovtJob" runat="server" Value='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("isgovjob"), True)  %>' />
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvItemAttachedFile" runat="server" Text="File Attached:" CssClass="font-weight-bold"></asp:Label>
                                                <br />
                                                <asp:Label ID="objlbllvItemAttachedFile" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("attachedfile"), True)  %>'></asp:Label>
                                            </div>
                                            <% End If %>
                                            <%--'S.SANDEEP |04-MAY-2023| -- END--%>
                                        </div>
                                    </div>
                                    <div class="col-md-1 mb-1">
                                        <asp:LinkButton ID="objlvbtnedit" runat="server" CommandName="Edit" Text="<i aria-hidden='true' class='fa fa-edit'></i>"></asp:LinkButton>
                                    </div>
                                    <div class="col-md-1 mb-1">
                                        <asp:LinkButton ID="objbtndelete" runat="server" CommandName="Delete" Text="<i aria-hidden='true' class='fa fa-trash'></i>"></asp:LinkButton>
                                    </div>
                                </div>
                                <hr class="mt-0" />
                                    </ItemTemplate>

                                    <EditItemTemplate>
                                <div class="form-group row">
                                    <div class="col-md-10 mb-1">
                                        <div class="form-group row mb-0">
                                            <div class="col-md-4 px-2">

                                                <asp:Label ID="lbllvEditItemEmployer" runat="server" Text="Employer:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:TextBox ID="objtxtlvEmployer" runat="server" CssClass="form-control" MaxLength="250" Text='<%# Eval("Employer")  %>'></asp:TextBox>

                                            </div>
                                            <div class="col-md-4 px-2">

                                                <asp:Label ID="lblEditItemCompany" runat="server" Text="Company:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:TextBox ID="objtxtlvCompany" runat="server" CssClass="form-control" MaxLength="250" Text='<%# Eval("company")  %>'></asp:TextBox>


                                            </div>
                                            <div class="col-md-4 px-2">

                                                <asp:Label ID="lblEditItemPhone" runat="server" Text="Phone:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:TextBox ID="objtxtlvPhone" runat="server" CssClass="form-control" MaxLength="50" Text='<%# Eval("Phone")  %>'></asp:TextBox>




                                            </div>
                                            <div class="col-md-4 px-2">

                                                <asp:Label ID="lbllvEditItemDesignation" runat="server" Text="Position Held:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:TextBox ID="objtxtlvDesignation" runat="server" CssClass="form-control" MaxLength="250" Text='<%# Eval("Designation")  %>'></asp:TextBox>

                                            </div>
                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lbllvEditItemJoinDate" runat="server" Text="Joining Date:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <uc2:DateCtrl ID="objdtlvJoinDate" runat="server" AutoPostBack="false" SetDate='<%# CDate(AntiXss.AntiXssEncoder.HtmlEncode(Eval("JoinDate"), True)).ToShortDateString %>' />

                                            </div>
                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lbllvEditItemterminationdate" runat="server" Text="Terminat. Date:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <uc2:DateCtrl ID="objdtlvterminationdate" runat="server" AutoPostBack="false" SetDate='<%# IIf(CDate(AntiXss.AntiXssEncoder.HtmlEncode(Eval("terminationdate"), True)).Date <> CDate(AntiXss.AntiXssEncoder.HtmlEncode("01/Jan/1900", True)).Date, CDate(AntiXss.AntiXssEncoder.HtmlEncode(Eval("terminationdate"), True)).ToShortDateString, Nothing) %>' />
                                            </div>
                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lbllvEditItemResponsibility" runat="server" Text="Duties:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:TextBox ID="objtxtlvResponsibility" runat="server" CssClass="form-control" TextMode="MultiLine" Text='<%# Eval("Responsibility")  %>'></asp:TextBox>
                                            </div>
                                            <asp:Panel ID="pnllvEditItemAchievement" runat="server" class="col-md-4 px-2 mb-1">
                                                <asp:Label ID="lbllvEditItemAchievement" runat="server" Text="Achievement:" CssClass="font-weight-bold"></asp:Label><br />
                                        <br />
                                                <asp:TextBox ID="objtxtlvAchievement" runat="server" CssClass="form-control" MaxLength="50" TextMode="MultiLine" Text='<%# Eval("achievements")  %>'></asp:TextBox>
                                            </asp:Panel>
                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lblEditItemleavingReason" runat="server" Text="Leaving Reason:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:TextBox ID="objtxtlvleavingReason" runat="server" CssClass="form-control" MaxLength="50" TextMode="MultiLine" Text='<%# Eval("leavingReason")  %>'></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1 mb-1">
                                        <asp:LinkButton ID="btnlvupdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                    </div>
                                    <div class="col-md-1 mb-1">
                                        <asp:LinkButton ID="btnlvcancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                    </div>
                                </div>
                                    </EditItemTemplate>
                        </asp:ListView>
                        <asp:ObjectDataSource ID="objodsJob" runat="server" SelectMethod="GetAppExperience" UpdateMethod="EditAppExperience" TypeName="Aruti_Online_Recruitment.clsApplicantExperience" DeleteMethod="DeleteApplicantExperience" EnablePaging="false" EnableCaching="True" CacheDuration="120" >
                            <SelectParameters>
                                <asp:Parameter Name="strCompCode" Type="String" DefaultValue="" />
                                <asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="dtCurrentDate" Type="DateTime" DefaultValue="" />
                            </SelectParameters>

                            <UpdateParameters>
                                <asp:Parameter Name="objAppJobHistoryUpdate" Type="Object" />
                            </UpdateParameters>

                            <DeleteParameters>
                                <asp:Parameter Name="strCompCode" Type="String" DefaultValue="" />
                                <asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intJobhistorytranunkid" Type="Int32" DefaultValue="0" />
                            </DeleteParameters>

                        </asp:ObjectDataSource>

                        <asp:Label ID="lblgrdJobRowUpdatingMsg1" runat="server" Text="Sorry, Selected Job Experience is already added to the list." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblgrdJobRowUpdatingMsg2" runat="server" Text="Job Experience Updated Successfully! \n\n Click Ok to add another Job Experience or to continue with other section(s)." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblgrdJobRowDeletingMsg1" runat="server" Text="Job Experience Deleted Successfully! \n\n Click Ok to add another Job Experience or to continue with other section(s)." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblgrdJobRowDeletingMsg2" runat="server" Text="Are you sure you want to Delete this Job History?" CssClass="d-none"></asp:Label>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnAddJoB" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="lvJob" EventName="ItemUpdating" />
                </Triggers>
            </asp:UpdatePanel>

        </asp:Panel>
    </div>
    <asp:Label ID="lblDocTypeMsg" runat="server" CssClass="d-none" Text="Please select Document Type."></asp:Label>
    <asp:Label ID="lblAttachTypeMsg" runat="server" CssClass="d-none" Text="Please select Attachment Type."></asp:Label>
    <asp:Label ID="lblAttachImageMsg" runat="server" CssClass="d-none" Text="Please select proper Image file or PDF file."></asp:Label>
    <asp:Label ID="lblAttachSizeMsg" runat="server" CssClass="d-none" Text="Sorry,You cannot upload file greater than #maxsize# MB."></asp:Label>
    <asp:Label ID="lblAttachMsg" runat="server" CssClass="d-none" Text="Sorry, Document attachment is mandatory."></asp:Label>
    <asp:Label ID="lblUpdateMsg" runat="server" CssClass="d-none" Text="Changes are updated successfully."></asp:Label>
    <asp:HiddenField ID="hfCancelLang" runat="server" />
    <cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsApplicantExperience" _ModuleName="ApplicantExperience" _TargetControl="pnlForm" CancelControlName="hfCancelLang" />
</asp:Content>
