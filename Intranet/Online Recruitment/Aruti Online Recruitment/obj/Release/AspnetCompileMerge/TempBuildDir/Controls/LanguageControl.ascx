﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="LanguageControl.ascx.vb" Inherits="Aruti_Online_Recruitment.LanguageControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<script>
    function pageLoad(sender, args) {
        if ($('select').length > 0) {

            $('select').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

        //if ($('input[type="checkbox"]').length > 0) {
        //        $('input').addClass("filled-in");
        //    }

        //    if ($('input[type="radio"]').length > 0) {
        //        $('input').addClass("with-gap");
        //    } 

        
	}

    $(document).ready(function () {
        setEvent_<%=uniqueKey %>();
    });

	var prm = Sys.WebForms.PageRequestManager.getInstance();
	if (prm != null) {
		prm.add_endRequest(function (sender, e) {
            setEvent_<%=uniqueKey %>();
		});


	};

    function setEvent_<%=uniqueKey %>() {
        $("[id*=objlnkLanguage]").on('click', function () {
			__doPostBack($(this).get(0).id, 0);
        });

        //$("[id*=lnklanguageclose]").on('click', function () {
        //    popupblur();
        //    $("[id*=popuplanguage]").modal("hide");
        //    __doPostBack($(this).get(0).id, 0);
        //});
        var lngc = '<%= lnklanguageclose.ClientID  %>';
        $$('lnklanguageclose').on('click', function () {
            popupblur();
            //$("[id*=popuplanguage]").modal("hide");
            __doPostBack($(this).get(0).id, 0);
        });


		$("[id*=objlblLanguage2]").on('change', function () {
			__doPostBack($(this).get(0).id, 0)
		});

		$("[id*=objlblLanguage3]").on('change', function () {
			__doPostBack($(this).get(0).id, 0)
		});

		$("[id*=objlbloMessage2]").on('change', function () {
			__doPostBack($(this).get(0).id, 0)
		});

		$("[id*=objlbloMessage3]").on('change', function () {
			__doPostBack($(this).get(0).id, 0)
		});

		$("[id*=lblfMessage2]").on('change', function () {
			__doPostBack($(this).get(0).id, 0)
		});

		$("[id*=lblfMessage3]").on('change', function () {
			__doPostBack($(this).get(0).id, 0)
		});

        //$("[id*=lnklanguageclose]").on('click', function () {
        //    //$("[id*=popuplanguage]").modal("hide");
        //});
	};

    function popupblur() {
        $(document).ready(function () {
            if ($(".newpopup:visible").length > 0) {
                $("header").addClass('blur');
                $("footer").addClass('blur');
                $(".p-2").addClass('blur');
            }
            else {
                $("header").removeClass('blur');
                $("footer").removeClass('blur');
                $(".p-2").removeClass('blur');
            }
        });
    }

</script>

<asp:LinkButton ID="objlnkLanguage" runat="server" > <%--OnClick="objlnkLanguage_Click"--%>
<%--    <i class="fa fa-language"></i>--%>
</asp:LinkButton>

<ajaxToolkit:ModalPopupExtender ID="popuplanguage" runat="server" CancelControlID="lnklanguageclose"
    PopupControlID="pnllanguage" TargetControlID="hflanguageNew" BackgroundCssClass="ModalPopupBG">
</ajaxToolkit:ModalPopupExtender>

<asp:Panel ID="pnllanguage" runat="server" CssClass="modal1" Style="display:none;" TabIndex="-1" role="dialog">  <%--displayNone--%>
	<%--Style="display: none;"--%>
	<div class="modal-dialog modal-lg width80vw">
		<%--style="width: 80vw;"--%>
        <div class="modal-content">
            <div id="abc" class="modal-header ">
                <h6>
                    <asp:HiddenField ID="hflanguageNew" runat="server" />
                    <asp:HiddenField ID="hflanguageClose" runat="server" />
                    <asp:HiddenField ID="hflanguageClose2" runat="server" />
                    <asp:Label ID="objlblPageHeader" runat="server" Text="Language / Messages / Other"></asp:Label>
                </h6>
            </div>

            <div class="modal-body">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item">
                        <a data-toggle="tab" class="nav-link active" href="#tbLanguage">Language</a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#tbMessage">Message</a>
                    </li>

                    <li role="presentation" class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#tbOther">Other</a>
                    </li>
                </ul>
				<div class="tab-content p-b-0 max-height50vh overflow-auto">
					<%-- style="max-height: 50vh; overflow: auto;"--%>
                    <div role="tabpanel" id="tbLanguage" class="tab-pane active">
						<div class="col-md-12 col-sm-12 mb-md-2 no-padding table-responsive mb-1">
							<%--style="margin-bottom: 2px;"--%>
							<asp:GridView ID="objgrd_control" runat="server" CssClass="table table-bordered w-100" AutoGenerateColumns="false"  > <%-- Width="100%"--%>
                                <Columns>
                                    <asp:TemplateField HeaderText="Default English">
                                        <ItemTemplate>
                                            <asp:TextBox ID="objlblLanguage1" runat="server" CssClass="form-control" Text='<%#Eval("language") %>' Wrap="true" Rows="1" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                            <asp:HiddenField ID="hfctrid" runat="server" Value='<%#Eval("controlname") %>' />
                                            <asp:HiddenField ID="hflangid" runat="server" Value='<%#Eval("langunkid") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Custom1">
                                        <ItemTemplate>
											<asp:TextBox ID="objlblLanguage2" runat="server" CssClass="form-control" Text='<%#Eval("language1") %>' Wrap="true" Rows="1" TextMode="MultiLine" OnTextChanged="lblLanguage2_TextChanged" ></asp:TextBox>  <%-- AutoPostBack="true"--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Custom2">
                                        <ItemTemplate>
											<asp:TextBox ID="objlblLanguage3" runat="server" CssClass="form-control" Text='<%#Eval("language2") %>' Wrap="true" Rows="1" TextMode="MultiLine" OnTextChanged="lblLanguage3_TextChanged"></asp:TextBox> <%-- AutoPostBack="true"--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div role="tabpanel" id="tbMessage" class="tab-pane">
						<div class="col-md-12 col-sm-12 mb-md-2 no-padding mb-1">
							<%--style="margin-bottom: 2px;"--%>
							<asp:GridView ID="objgrd_fMessages" runat="server" AutoGenerateColumns="false" HeaderStyle-CssClass="w-100">
								<%-- HeaderStyle-Width="30%" Width="100%"--%>
                                <Columns>
                                    <asp:TemplateField HeaderText="DefaultEnglish">
                                        <ItemTemplate>
                                            <asp:TextBox ID="lblfMessage1" runat="server" Text='<%#Eval("_message") %>' ReadOnly="true" Wrap="true" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                            <asp:HiddenField ID="hffmodulename" runat="server" Value='<%#Eval("_module_name") %>' />
                                            <asp:HiddenField ID="hffmsgid" runat="server" Value='<%#Eval("_messageunkid") %>' />
                                            <asp:HiddenField ID="hffmsgcode" runat="server" Value='<%#Eval("_messagecode") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Custom 1">
                                        <ItemTemplate>
											<asp:TextBox ID="lblfMessage2" runat="server" Text='<%#Eval("_message1") %>' Wrap="true" TextMode="MultiLine" Rows="2" OnTextChanged="lblMessage_TextChanged"></asp:TextBox>  <%-- AutoPostBack="true"--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Custom 2">
                                        <ItemTemplate>
											<asp:TextBox ID="lblfMessage3" runat="server" Text='<%#Eval("_message2") %>' Wrap="true" TextMode="MultiLine" Rows="2" OnTextChanged="lblMessage_TextChanged"></asp:TextBox>  <%-- AutoPostBack="true"--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <div role="tabpanel" id="tbOther" class="tab-pane">
						<div class="col-md-12 col-sm-12 mb-md-2 no-padding mb-1">
							<%--style="margin-bottom: 2px;"--%>
							<asp:GridView ID="objgrd_oMessages" runat="server" AutoGenerateColumns="false" HeaderStyle-CssClass="w-100">
								<%--HeaderStyle-Width="30%" Width="100%"--%>
                                <Columns>

                                    <asp:TemplateField HeaderText="DefaultEnglish">
                                        <ItemTemplate>
                                            <asp:TextBox ID="objlbloMessage1" runat="server" Text='<%#Eval("_message") %>' ReadOnly="true" TextMode="MultiLine"></asp:TextBox>
                                            <asp:HiddenField ID="hfomodulename" runat="server" Value='<%#Eval("_module_name") %>' />
                                            <asp:HiddenField ID="hfomsgid" runat="server" Value='<%#Eval("_messageunkid") %>' />
                                            <asp:HiddenField ID="hfomsgcode" runat="server" Value='<%#Eval("_messagecode") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Custom 1">
                                        <ItemTemplate>
											<asp:TextBox ID="objlbloMessage2" runat="server" Text='<%#Eval("_message1") %>' onkeydown="txtOnKeyPress(this);" TextMode="MultiLine"></asp:TextBox>  <%--OnTextChanged="lblMessage_TextChanged"  AutoPostBack="true"--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Custom 2">
                                        <ItemTemplate>
											<asp:TextBox ID="objlbloMessage3" runat="server" Text='<%#Eval("_message2") %>' TextMode="MultiLine" ></asp:TextBox> <%--OnTextChanged="lblMessage_TextChanged"   AutoPostBack="true"--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>

			<div class="modal-footer text-right">
				<%--style="text-align: right;"--%>
				<asp:LinkButton ID="lnklanguageclose" runat="server" CssClass="btn btn-primary border-btn"  ValidationGroup="languageclose">Close</asp:LinkButton> <%--OnClick="lnklanguageclose_Click"--%>
				<%--OnClientClick="popupblur" --%>
            </div>
        </div>
    </div>
</asp:Panel>
