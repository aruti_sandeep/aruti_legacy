﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SLogin.aspx.vb" Inherits="Aruti_Online_Recruitment.SLogin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Login</title>


    <%--<link rel="stylesheet" id="lnkBootstrap" runat="server" href='<%= Page.ResolveUrl("~/Content/bootstrap.css")  %>' />
    <link rel="stylesheet" id="lnkBootstrapDialog" runat="server" href='<%= Page.ResolveUrl("~/Content/bootstrap-dialog.css")  %>' />--%>
    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/bootstrap-dialog.css" />
    <link rel="shortcut icon" href="Images/logo_32.ico" type="image/x-icon" />
    <link rel="stylesheet" href="Content/fontawesome-all.css" />
    <link rel="stylesheet" href="Content/style.css?version=1" />
    <link rel="stylesheet" href="customtheme.css" />

    <script type="text/javascript" src="scripts/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap-dialog.js"></script>    

    <%--<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

    <script type="text/javascript">
       // $(document).ready(function () {

           // if ($("[id*=pnlLogin]").length > 0) {

        var onloadCallback = function () {
            grecaptcha.render('dvCaptcha', {
                'sitekey': '<%=ReCaptcha_Key %>',
                'callback': function (response) {
                    $.ajax({
                        type: "POST",
                        url: "SLogin.aspx/VerifyCaptcha",
                        data: "{response: '" + response + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (r) {
                            var captchaResponse = jQuery.parseJSON(r.d);
                            if (captchaResponse.success) {
                                $("[id*=txtCaptcha]").val(captchaResponse.success);
                                $("[id*=rfvCaptcha]").hide();
                            } else {
                                $("[id*=txtCaptcha]").val("");
                                $("[id*=rfvCaptcha]").show();
                                var error = captchaResponse["error-codes"][0];
                                $("[id*=rfvCaptcha]").html("RECaptcha error. " + error);
                            }
                        }
                    });
                }
            });
        };
       // }
      //  });
    </script>--%>
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>

        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="400" EnablePageMethods="true" ScriptMode="Release">
        </asp:ScriptManager>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1" DynamicLayout="false">
            <ProgressTemplate>
                <div class="UpdateProgressBG">
                    <div class="center">
                        <img alt="progress" src="images/waiting.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div class="card MaxWidth1422">
                    <asp:Panel ID="pnlLogin" runat="server" CssClass="width100">
                        <%-- <asp:Login ID="Login1" runat="server" PasswordRecoveryText="Forgot Password?" PasswordRecoveryUrl="~/ForgotPassword.aspx" TextLayout="TextOnTop" UserNameLabelText="Email:" Style="width: 100%;" DisplayRememberMe="False" DestinationPageUrl="~/SAdmin/UserHome.aspx">

                            <LayoutTemplate>--%>

                        <div class="card-header">
                            <div class="form-group row">
                                <div class="col-md-12 text-center">
                                    <img src="Images/logo_aruti.png" class="img-rounded img_shortcuticon" alt="Aruti" />
                                </div>
                            </div>

                            <h4 class="text-center">Sign in</h4>
                        </div>

                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Email:</asp:Label>
                                    <asp:TextBox ID="UserName" runat="server" CssClass="form-control" TextMode="Email"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="Email is required." ToolTip="Email is required." ValidationGroup="Login1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="EmailValidator" runat="server" ControlToValidate="UserName"
                                                ErrorMessage="Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" ValidationGroup="Login1" Display="None"></asp:RegularExpressionValidator>--%>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                    <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                    <%-- <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="Login1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                                </div>
                            </div>

                            <asp:Panel ID="pnlCaptcha" runat="server" Visible="true">
                                <%--<div class="form-group row">
                                    <div class="col-md-12">
                                        <asp:Label ID="SecurityLabel" runat="server" AssociatedControlID="Password">Security Check:</asp:Label>

                                        <div id="dvCaptcha">
                                        </div>
                                        <asp:TextBox ID="txtCaptcha" runat="server" CssClass="d-none" />--%>
                                <%--<asp:RequiredFieldValidator ID="rfvCaptcha" ErrorMessage="Captcha validation is required." ControlToValidate="txtCaptcha" ValidationGroup="Login1"
                                                    runat="server" ForeColor="Red" Display="None" />--%>
                                <%-- </div>
                                </div>--%>
                                <div class="form-group row">
                                    <asp:Label ID="lblVerificationCode" runat="server" CssClass="pl-2" AssociatedControlID="Password">Verification Code:</asp:Label>
                                    <div class="col-md-12">
                                        <img id="mycaptcha" src="Captcha.aspx">&nbsp;&nbsp;
                                        <i class="fa fa-sync f-s-20 cursor-pointer btnrefresh" title="Refresh captcha"></i>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <asp:Label ID="lblEnterVerificationCode" runat="server" AssociatedControlID="Password">Enter Verifaction Code:</asp:Label>
                                        <asp:TextBox runat="server" ID="txtVerificationCode" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </asp:Panel>


                            <div class="form-group row">
                                <div class="col-md-12 text-center">
                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                    <%-- <asp:ValidationSummary
                                        HeaderText="You must enter a value in the following fields:"
                                        DisplayMode="BulletList"
                                        EnableClientScript="true"
                                                runat="server" ValidationGroup="Login1" Style="color: red" />--%>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Button ID="LoginButton" runat="server" Text="Log In" CssClass="btn btn-primary width100" />
                                </div>
                            </div>                            

                            <div class="form-group row mt-2">
                                <div class="col-md-12">
                                    <%--<asp:HyperLink ID="CreateUserLink" runat="server" NavigateUrl="~/Register.aspx" Visible="false">Not Registered? Click here to Sign up</asp:HyperLink>--%>
                                    <asp:HyperLink ID="CreateUserLink" runat="server" NavigateUrl="~/Register.aspx" CssClass="btn btn-primary width100" ToolTip="Not Registered? Click here to Sign up">Sign up</asp:HyperLink>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:HyperLink ID="PasswordRecoveryLink" runat="server" NavigateUrl="~/ForgotPassword.aspx">Forgot your password? Click here</asp:HyperLink>
                                </div>
                            </div>
                        </div>

                        <%-- </LayoutTemplate>
                        </asp:Login>--%>
                    </asp:Panel>

                    <asp:Panel ID="pnlSetup" runat="server" Visible="false" CssClass="width100">

                        <div class="card-header">
                            <h4>Setup Database</h4>
                        </div>

                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblServer" runat="server" AssociatedControlID="txtServerName">Server Name:</asp:Label>
                                    <asp:TextBox ID="txtServerName" runat="server" CssClass="form-control" Text="(Local)"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtServerName" ErrorMessage="Server Name is required." ToolTip="Server Name is required." ValidationGroup="Test" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                                </div>
                            </div>

                            <%--<div class="form-group row">
                    <div class="col-md-12">
                        <asp:Label ID="lblInstance" runat="server">Instance:</asp:Label>
                        <asp:Label ID="txtInstance" runat="server" CssClass="form-control">APayroll</asp:Label>
                    </div>
                </div>   --%>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblUserId" runat="server" AssociatedControlID="txtUserId">User ID:</asp:Label>
                                    <asp:TextBox ID="txtUserId" runat="server" CssClass="form-control"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="UserIdRequired" runat="server" ControlToValidate="txtUserId" ErrorMessage="User ID is required." ToolTip="User ID is required." ValidationGroup="Test" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Label ID="lblPassword" runat="server" AssociatedControlID="txtPassword">Password:</asp:Label>
                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="PwdRequired" runat="server" ControlToValidate="txtPassword" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="Test" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12 text-center">
                                    <asp:Literal ID="FailureTextTest" runat="server" EnableViewState="False"></asp:Literal>
                                    <%-- <asp:ValidationSummary
                                HeaderText="You must enter a value in the following fields:"
                                DisplayMode="BulletList"
                                EnableClientScript="true"
                                        runat="server" ValidationGroup="Test" Style="color: red" />--%>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <asp:Button ID="btnTest" runat="server" Text="Generate Database" CssClass="btn btn-primary width100 " />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hflocationhref" runat="server" Value="" />
    </form>
    <script type="text/javascript" src="slogin.js"></script>
    <script type="text/javascript" src="showmessage.js"></script>
    <script type="text/javascript" src="Browser.js"></script>
</body>
</html>
