﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Test.aspx.vb" Inherits="Aruti_Online_Recruitment.Test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>

        <div>
            <asp:Label ID="lblText" runat="server" Text=""  ></asp:Label>
               <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Test" ValidationGroup="Test" Style="width: 150px;" />
        </div>
    </form>
</body>
</html>
