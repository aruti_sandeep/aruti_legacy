﻿<%@ Page Title="Copy Details" Language="vb" AutoEventWireup="false" MasterPageFile="~/SAdmin/Site2.Master" CodeBehind="Details.aspx.vb" Inherits="Aruti_Online_Recruitment.Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script src="../scripts/ZeroClipboard.min.js"></script>

    <script>
        $(function () {
            var clip = new ZeroClipboard(document.getElementById("copy-button"), {
                moviePath: "../scripts/ZeroClipboard.swf"
            });

            clip.on('load', function (client) {
                //alert("movie is loaded" + document.getElementById('<%= txtDetailCopy.ClientID %>'));
            });

            clip.on('dataRequested', function (client, args) {

                clip.setText(document.getElementById('<%= txtDetailCopy.ClientID %>').value);
                // Don't make this mistake, alert seems to prevent it from working
                //             alert("Clipboard should be set from click on " + this.id);
            });

            clip.on('complete', function (client, args) {
                //this.style.display = 'none'; // "this" is the element that was clicked
                //alert("Copied text to clipboard: " + args.text);
                alert("Copied text to clipboard: ");
            });
        });
    </script>

    <div class="card">

        <div class="card-header">
            <h4>Company Details</h4>
        </div>

        <div class="card-body">
            <div class="form-group row">
                <div class="col-md-12">
                    <asp:TextBox ID="txtDetailCopy" runat="server" Rows="20" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="card-footer">
            <div class="form-group row">
                <div class="col-md-4">
                    <button id="copy-button" title="Click to copy me." formnovalidate="formnovalidate" class="btn btn-primary">Copy to Clipboard</button>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
