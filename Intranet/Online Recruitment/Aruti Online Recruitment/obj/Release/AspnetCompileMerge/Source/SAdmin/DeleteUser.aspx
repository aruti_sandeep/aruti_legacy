﻿<%@ Page Title="Delete User" Language="vb" AutoEventWireup="false" MasterPageFile="~/SAdmin/Site2.Master" CodeBehind="DeleteUser.aspx.vb" Inherits="Aruti_Online_Recruitment.DeleteUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">

        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("[id$=_" + id + "]", context);
            return el;
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {

                $$('btnSearch').on('click', function () {
                    return IsValidate();
                });

                $("[id*=objlnkDelete]").on('click', function () {
                    if (!ShowConfirm("Are you sure you want to Delete this User?", this))
                        return false;
                    else
                        return true;
                });

            });
        };

        $(document).ready(function () {

            $$('btnSearch').on('click', function () {
                return IsValidate();
            });

            $("[id*=objlnkDelete]").on('click', function () {
                if (!ShowConfirm("Are you sure you want to Delete this User?", this))
                    return false;
                else
                    return true;
            });

        });

        function IsValidate() {
            if ($$('txtEmail').val().trim() == '') {
                ShowMessage('Email is compulsory information, it can not be blank. Please enter Email to continue.', 'MessageType.Errorr');
                $$('txtEmail').focus();
                return false;
            }
            else if (IsValidEmail($$('txtEmail').val()) == false) {
                ShowMessage('Sorry, Email is not valid.Please enter valid email.', 'MessageType.Errorr')
                $$('txtEmail').focus();
                return false;
            } 
        }

        function IsValidEmail(strEmail) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(strEmail)) {
                return false;
            } else {
                return true;
            }
        }
        </script>
    <div class="card">

        <div class="card-header">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <h4>Delete User
                    <asp:Label ID="lblCount" runat="server" Text=""></asp:Label>
                </h4>
            </ContentTemplate>
            <Triggers>
                <%--<asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />--%>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="grdLink" EventName="DataBound" />
            </Triggers>
        </asp:UpdatePanel>
        </div>

        <div class="card-body">
        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="form-group row">
                    <div class="col-md-4">
                         <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                        <asp:TextBox ID="txtEmail" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email is required." ToolTip="Email is required." ValidationGroup="SearchUser" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="EmailValidator" runat="server" ControlToValidate="txtEmail"
                                            ErrorMessage="Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="SearchUser" Display="None" SetFocusOnError="True"></asp:RegularExpressionValidator>--%>
                    </div>

                    <div class="col-md-4">
                       <%--<asp:ValidationSummary
                                            HeaderText="You must enter a value in the following fields:"
                                            DisplayMode="BulletList"
                                            EnableClientScript="true"
                                            runat="server" ValidationGroup="SearchUser" />--%>
                    </div>

                    <div class="col-md-4">
                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary w-100"  Text="Search" ValidationGroup="SearchUser" /> <%--Style="width: 100%;"--%>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
            </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnl_link" runat="server" CssClass="table-responsive minheight300" > <%--Style="min-height: 300px;"--%>
                    <asp:GridView ID="grdLink" runat="server" AutoGenerateColumns="False" CssClass="table" ShowHeaderWhenEmpty="true"
                        AllowPaging="true" DataSourceID="dsLink" PageSize="15" DataKeyNames="activationlinkunkid, Comp_Code, companyunkid, LoweredEmail, subject, message">

                        <Columns>
                            <asp:BoundField DataField="Comp_Code" HeaderText="Comp. Code" />
                            <asp:BoundField DataField="companyunkid" HeaderText="Client ID" />
                            <asp:BoundField DataField="subject" HeaderText="subject" Visible="false" />
                            <asp:BoundField DataField="message" HeaderText="message" Visible="false" />
                            <asp:BoundField DataField="LoweredEmail" HeaderText="Email" />
                            <asp:BoundField DataField="LastLoginDate" HeaderText="Last Login Date" />
                            <asp:TemplateField HeaderText="Is Approved" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%--<asp:CheckBox ID="objchkIsApproved" runat="server" Text="" Checked='<%# CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("IsApproved"))) %>' Enabled="false" />--%>
                                    <asp:CheckBox ID="objchkIsApproved" runat="server" Text="" Checked='<%# CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("IsApproved"), True)) %>' Enabled="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="objlnkDelete" Text="Delete" runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="DeleteApplicant" />
                                </ItemTemplate>
                            </asp:TemplateField>
                           <%-- <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                         />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                        </Columns>
                    </asp:GridView>

                    <asp:ObjectDataSource ID="dsLink" runat="server" SelectMethod="GetActivationLink" TypeName="Aruti_Online_Recruitment.clsApplicant" EnablePaging="true"
                        MaximumRowsParameterName="intPageSize" StartRowIndexParameterName="startRowIndex" SelectCountMethod="GetActivationLinkTotal">
                        <SelectParameters>
                            <%--<asp:SessionParameter Name="strCompCode" SessionField="CompCode" Type="String" />
                            <asp:SessionParameter Name="intComUnkID" SessionField="Companyunkid" Type="Int32" />--%>
                            <asp:Parameter Name="strCompCode" Type="String" DefaultValue="" />
                            <asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />
                            <asp:ControlParameter ControlID="txtEmail" DefaultValue="xyz" Name="strEmail" PropertyName="Text" Type="String" />
                            <asp:Parameter Name="intIsApproved" Type="Int32" DefaultValue="-1" />
                            <asp:Parameter Name="startRowIndex" Type="Int32" />
                            <asp:Parameter Name="intPageSize" Type="Int32" />
                            <asp:Parameter Name="strSortExpression" Type="String" />
                            <asp:Parameter Name="isemailsent" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <%--<asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />--%>
                <%--<asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />--%>
            </Triggers>
        </asp:UpdatePanel>
               
        <%--<asp:UpdatePanel ID="upnlButton" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="form-group row">
                    <div class="col-md-4">
                    </div>

                    <div class="col-md-4">
                        <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-primary" Style="width: 100%" Text="Delete Applicants" 
                            ValidationGroup="DeleteApplicants" />
                    </div>

                    <div class="col-md-4">
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>--%>

    </div>
</asp:Content>
