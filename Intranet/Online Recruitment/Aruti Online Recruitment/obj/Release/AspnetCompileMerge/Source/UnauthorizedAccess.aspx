﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UnauthorizedAccess.aspx.vb" Inherits="Aruti_Online_Recruitment.UnauthorizedAccess" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Unauthorized access</title>
    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/bootstrap-dialog.css" />
    <link rel="stylesheet" href="Content/style.css?version=1" />
</head>
<body>
    <form id="form1" runat="server">
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>
    <div>
            <p></p>
            <p>
                <span class="h3 text-danger">Unauthorized Access</span>
            </p>
 <p>
 You have attempted to access a page that you are not authorized to view.
 </p>
 <p>
 If you have any questions, please contact the site administrator.
 </p>
    </div>
    </form>
</body>
</html>
