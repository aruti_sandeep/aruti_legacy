﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ResetPassword.aspx.vb" Inherits="Aruti_Online_Recruitment.ResetPassword" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Reset Password</title>

    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/bootstrap-dialog.css" />
    <script type="text/javascript" src="scripts/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/bootstrap-dialog.js"></script>
    <link rel="shortcut icon" href="Images/logo_32.ico" type="image/x-icon" />
    <link rel="stylesheet" href="Content/style.css" />
    <link rel="stylesheet" href="customtheme.css" />

</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>

        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="400" EnablePageMethods="true" ScriptMode="Release">
        </asp:ScriptManager>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="1" DynamicLayout="false">
            <ProgressTemplate>
                <div class="UpdateProgressBG">
                    <div class="center">
                        <img alt="progress" src="images/waiting.gif" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <div class="card MaxWidth1422">

            <div class="card-header">
                <h4>Reset Password</h4>
            </div>

            <asp:Panel ID="pnlReset" runat="server">
                <div class="card-body">
                    <div class="form-group row">

                        <div class="col-md-12">
                            <asp:Label ID="lblOTP" runat="server" Text="Verification Code"></asp:Label>
                            <asp:TextBox ID="txtOTP" runat="server" CssClass="form-control" ValidationGroup="Reset"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtOTP" ValidationGroup="Reset" SetFocusOnError="true" ErrorMessage="Please enter a Verification Code sent to your email." Display="None"></asp:RequiredFieldValidator>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="lblNewPwd" runat="server" Text="New Password"></asp:Label>
                            <asp:TextBox ID="txtNewPwd" runat="server" TextMode="Password" CssClass="form-control" ValidationGroup="Reset"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNewPwd" ValidationGroup="Reset" SetFocusOnError="true" ErrorMessage="Please enter New Password" Display="None"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Your password must be 7 characters long, and contain at least one number and one special character." ForeColor="Red" ValidationExpression="^(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{7,}$" ControlToValidate="txtNewPwd" Display="None" ValidationGroup="Reset">
                            </asp:RegularExpressionValidator>--%>
                            <%--<cc1:PasswordStrength ID="PasswordStrength1" runat="server" TargetControlID="txtNewPwd" MinimumNumericCharacters="1" MinimumSymbolCharacters="1" PreferredPasswordLength="10" StrengthIndicatorType="Text" TextStrengthDescriptions="Very Poor;Weak;Average;Good;Excellent" DisplayPosition="BelowLeft"></cc1:PasswordStrength>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="lblConfirmPwd" runat="server" Text="Confirm Password"></asp:Label>
                            <asp:TextBox ID="txtConfirmPwd" runat="server" TextMode="Password" CssClass="form-control" ValidationGroup="Reset"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtConfirmPwd" ValidationGroup="Reset" SetFocusOnError="true" ErrorMessage="Please Retype Password" Display="None"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtConfirmPwd" ControlToCompare="txtNewPwd" SetFocusOnError="True" ValidationGroup="Reset" Display="None" ErrorMessage="Confirm Password must match with new password!"></asp:CompareValidator>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="lblMsg" runat="server" Text="" CssClass="text-danger" ></asp:Label> <%--ForeColor="Red"--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12 text-center" >
                            <%--<asp:ValidationSummary
                                HeaderText="You must enter a value in the following fields:"
                                DisplayMode="BulletList"
                                EnableClientScript="true"
                                runat="server" ValidationGroup="Reset" Style="color: red" />--%>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Button ID="btnChangePwd" runat="server" Text="Reset Password" CssClass="btn btn-primary w-100" ValidationGroup="Reset"  />
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlSuccess" runat="server" Visible="false">
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="lblSuccess" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </asp:Panel>

        </div>
    </form>

     <script type="text/javascript" src="resetpassword.js"></script>
    <script type="text/javascript" src="showmessage.js"></script>
</body>
</html>
