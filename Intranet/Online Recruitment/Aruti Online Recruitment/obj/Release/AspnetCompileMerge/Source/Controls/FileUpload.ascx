﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FileUpload.ascx.vb" Inherits="Aruti_Online_Recruitment.FileUpload" %>

<script type="text/javascript">

    function fileUpLoadChange(imgFile) {
        //    $('#image_file').change(function() {
        //        var filename = $('#image_file').val();
        //        $('#select_file').html(filename.replace(/^.*[\\\/]/, ''));
        //     });

        //var imgFile = '#<%= image_file.ClientID %>';
        var imgFile = $(imgFile);
        //if ($.browser.msie) {
        //    //alert('This is a Microsoft Internet Explorer ' + $.browser.version.substr(0, 1));
        //    $(imgFile).css({ 'width': '102px' });
        //    $(imgFile).css({ 'right': '60px' });
        //}
        //else if ($.browser.mozilla) {
        //    //alert('This is a Mozilla Firefox ' + $.browser.version.substr(0, 1));    
        //    $(imgFile).css({ 'width': '102px' });
        //    $(imgFile).css({ 'right': '50px' });
        //    $(imgFile).css({ 'font-size': '14px' });
        //}
        //else if ($.browser.webkit || $.browser.safari) {
        //    //alert('This is a Webkit Engine Browser (Apple Safari or Google Chrome) with version ' + $.browser.version.substr(0, 1));
        //    $(imgFile).css({ 'width': '102px' });
        //    $(imgFile).css({ 'right': '52px' });
        //}
        //else if ($.browser.opera) {
        //    //alert('This is an Opera ' + $.browser.version.substr(0, 1));
        //    $(imgFile).css({ 'width': 'auto' });
        //    $(imgFile).css({ 'right': '0px' });
        //}

        //if ($(imgFile).is(':disabled') == true) {
        //    $('#ctl0_config_btnUpload').css({ 'color': '#A6A6A6' });
        //    $('#ctl0_config_btnUpload').css({ 'background-image': '#FFFFFF' });
        //}
        //else {
        //    $('#ctl0_config_btnUpload').css({ 'color': '#222' });
        //    $('#ctl0_config_btnUpload').css({ 'background-image': '#BEB7B9' });
        //}
        $(imgFile).on('change', function () {
            var blnInvalidFile = false;
            //var ext = this.value.match(/\.(.+)$/)[1];
            var ext = this.value.split('.').pop();
            var msgInvalid = 'Sorry, Allowable file type(s) are PDF';
            var blnImage = <%= AllowImageFile.ToString.ToLower %>;
            var blnDoc = <%= AllowDocumentFile.ToString.ToLower %>;                       

            if (blnImage == true) {
                msgInvalid = msgInvalid + ', Image';
            }
            if (blnDoc == true) {
                msgInvalid = msgInvalid + ', Document';
            }
            msgInvalid = msgInvalid + ' Only.';

            switch (ext.toLowerCase()) {
                case 'jpg':
                case 'jpeg':
                case 'bmp':
                case 'png':
                case 'gif':
                    if (blnImage == true) {                        
                        break;
                    }
                    else {
                        this.value = '';
                        blnInvalidFile = true;           
                        break;
                    }
                case 'doc':
                case 'docx':
                    if (blnDoc == true) {
                        msgInvalid = msgInvalid + ', Document';
                        break;
                    }
                    else {
                        this.value = '';
                        blnInvalidFile = true;
                        break;
                    }
                case 'pdf':
                    break;                
                default:
                    //alert('Please select proper Image file or PDF file.');
                    //BootstrapDialog.show({
                    //    type: BootstrapDialog.TYPE_INFO,
                    //    closable: true, // <-- Default value is false
                    //    draggable: true, // <-- Default value is false
                    //    title: 'Aruti',
                    //    message: 'Please select proper Image file, Document file or PDF file.',
                    //    buttons: [{
                    //        label: 'Ok',
                    //        cssClass: 'btn-primary',
                    //        action: function (dialogItself) {
                    //            dialogItself.close();
                    //        }
                    //    }]
                    //});

                    this.value = '';
                    blnInvalidFile = true;
            }           

            if (blnInvalidFile == true) {

                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_INFO,
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: 'Aruti',
                    message: msgInvalid,
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function (dialogItself) {
                            dialogItself.close();
                        }
                    }]
                });

            }
                        
            if (window.FileReader && window.File && window.FileList && window.Blob && blnInvalidFile==false) {
                var maxsize = '<%= RemainingSizeKB %>';
                //if (maxsize > 0 && (this.files[0].size / 1024 / 1024) > maxsize) {
                if (maxsize > 0 && (this.files[0].size) > maxsize) {
                    //alert('Sorry,You cannot upload file greater than ' + maxsize + ' MB.');
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_INFO,
                        closable: true, // <-- Default value is false
                        draggable: true, // <-- Default value is false
                        title: 'Aruti',
                        message: 'Sorry,You cannot upload file greater than ' + (maxsize / 1024 / 1024).toFixed(2) + ' MB.',
                        buttons: [{
                            label: 'Ok',
                            cssClass: 'btn-primary',
                            action: function (dialogItself) {
                                dialogItself.close();
                            }
                        }]
                    });

                    this.value = '';
                    blnInvalidFile = true;
                }
            }
            //var filename = $(imgFile).val();
            var filename = '';
            if (blnInvalidFile == false) {
                filename = $(imgFile).val();
            }
            //$('#select_file').html(filename.replace(/^.*[\\\/]/, ''));

            if (filename != '') {
                $('#' + $(imgFile).parents('div[id*=fileUpload]:first').find("input[id*='btnUpload']")[0].id).click();
                //$('#<%= btnUpload.ClientID %>').click();
        }

        });
}

</script>

<div id="fileUpload" class="width150px">

    <%--<style>
        
    </style>--%>
    <%--<input type="file" id="image_file" runat="server" style="display: none;" />--%>
    <asp:UpdatePanel ID="UPUpload1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <%-- <div style="position:relative;">
            <asp:FileUpload id="image_file" runat="server" accept="image/*" CssClass="flupload" />            
            <a id="ctl0_config_btnUpload" class="btn" onclick="return false" href="javascript:;//ctl0_config_btnUpload">Choose File</a>                        
            </div>--%>
            <label class="myLabel">
                <%--<input type="file" required />--%>
                <asp:FileUpload ID="image_file" runat="server" accept="MIME_type" CssClass="flupload" />
                <span class="form-control btn btn-primary">Choose file</span>
            </label>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="float-left d-none" />
</div>
