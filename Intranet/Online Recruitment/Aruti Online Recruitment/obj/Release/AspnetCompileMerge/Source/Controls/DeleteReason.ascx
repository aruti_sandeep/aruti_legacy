﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DeleteReason.ascx.vb" Inherits="Aruti_Online_Recruitment.DeleteReason" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalPopupBG2"
    CancelControlID="btnDelReasonNo" PopupControlID="Panel1" TargetControlID="hdnfieldDelReason">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="newpopup" Style="display: none; width: 450px;
    z-index: 100002!important;" DefaultButton="btnDelReasonYes">
    <div class="panel-primary" style="margin-bottom: 0px;">
        <div class="panel-heading">
            <asp:Label ID="lblpopupHeader" runat="server" Text="Delete Reason"></asp:Label>
        </div>
        <div class="panel-body">
            <div id="FilterCriteria" class="panel-default">
                <div id="FilterCriteriaBody" class="panel-body-default">
                    <table style="width: 100%">
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" Width="97%" Height="50px"
                                    Style="margin-bottom: 5px;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                                    ControlToValidate="txtReason" ErrorMessage="Delete Reason can not be blank. "
                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" SetFocusOnError="True"
                                    ValidationGroup="Reason"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="PNReqE" TargetControlID="RequiredFieldValidator1"
                                    Width="300px" />
                            </td>
                        </tr>
                    </table>
                    <div class="btn-default">
                        <asp:Button ID="btnDelReasonYes" runat="server" Text="Yes" Width="70px" Style="margin-right: 10px"
                            CssClass="btnDefault" ValidationGroup="Reason" />
                        <asp:Button ID="btnDelReasonNo" runat="server" Text="No" Width="70px" Style="margin-right: 10px"
                            CssClass="btnDefault" />
                        <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>