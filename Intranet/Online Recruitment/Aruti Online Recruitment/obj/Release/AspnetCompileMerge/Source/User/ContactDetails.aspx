﻿<%@ Page Title="Contact Details" Language="vb" AutoEventWireup="false" MasterPageFile="~/User/Site4.Master" EnableEventValidation="false" CodeBehind="ContactDetails.aspx.vb" Inherits="Aruti_Online_Recruitment.ContactDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <script type="text/javascript">

        function pageLoad(sender, args) {

            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {

                $$('btnSave').on('click', function () {
                    return IsValidate();
                });

            });

            prm.add_endRequest(function (sender, e) {

                $$('ddlCountry').on('change', function () {
                    __doPostBack('<%= ddlCountry.UniqueID %>', 'onChanged');
                });

            });

            prm.add_endRequest(function (sender, e) {

                $$('ddlState').on('change', function () {
                    __doPostBack('<%= ddlState.UniqueID %>', 'onChanged');
                });

            });

            prm.add_endRequest(function (sender, e) {

                $$('ddlPostTown').on('change', function () {
                    __doPostBack('<%= ddlPostTown.UniqueID %>', 'onChanged');
                });

            });

            prm.add_endRequest(function (sender, e) {

                $$('ddlPrmCountry').on('change', function () {
                    __doPostBack('<%= ddlPrmCountry.UniqueID %>', 'onChanged');
                });

            });

            prm.add_endRequest(function (sender, e) {

                $$('ddlPrmState').on('change', function () {
                    __doPostBack('<%= ddlPrmState.UniqueID %>', 'onChanged');
                });

            });

            prm.add_endRequest(function (sender, e) {

                $$('ddlPrmPostTown').on('change', function () {
                    __doPostBack('<%= ddlPrmPostTown.UniqueID %>', 'onChanged');
                });

            });

            

        };   

        $(document).ready(function () {

            $$('btnSave').on('click', function () {
                return IsValidate();
            });

            $$('ddlCountry').on('change', function () {
                __doPostBack('<%= ddlCountry.UniqueID %>', 'onChanged');
            });

            $$('ddlState').on('change', function () {
                __doPostBack('<%= ddlState.UniqueID %>', 'onChanged');
            });

            $$('ddlPostTown').on('change', function () {
                __doPostBack('<%= ddlPostTown.UniqueID %>', 'onChanged');
            });

            $$('ddlPrmCountry').on('change', function () {
                __doPostBack('<%= ddlPrmCountry.UniqueID %>', 'onChanged');
            });

            $$('ddlPrmState').on('change', function () {
                __doPostBack('<%= ddlPrmState.UniqueID %>', 'onChanged');
             });

            $$('ddlPrmPostTown').on('change', function () {
                 __doPostBack('<%= ddlPrmPostTown.UniqueID %>', 'onChanged');
            });

        });

        function IsValidate() {
            if ($$('txtpresentAdd').length > 0 &&  $$('txtpresentAdd').val().trim() == '' && '<%= Session("Address1Mandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblPresentAdd1Msg').text(), 'MessageType.Errorr');
                $$('txtpresentAdd').focus();
                return false;
              }
            else if ($$('txtpresentadd2').length > 0 && $$('txtpresentadd2').val().trim() == '' && '<%= Session("Address2Mandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblPresentAdd2Msg').text(), 'MessageType.Errorr');
                $$('txtpresentadd2').focus();
                return false;
            }
            else if ($$('txtPlotno').length > 0 && $$('txtPlotno').val().trim() == '' && '<%= Session("ContactPlotNoMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblPlotnoMsg').text(), 'MessageType.Errorr');
                $$('txtPlotno').focus();
                return false;
            }
            else if ($$('txtEstate').length > 0 && $$('txtEstate').val().trim() == '' && '<%= Session("ContactEstateMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblEstateMsg').text(), 'MessageType.Errorr');
                $$('txtEstate').focus();
                return false;
            }
            else if ($$('txtroad').length > 0 && $$('txtEstate').val().trim() == '' && '<%= Session("ContactStreetMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblRoadMsg').text(), 'MessageType.Errorr');
                $$('txtroad').focus();
                return false;
            }
            else if ($$('txtProvince').length > 0 && $$('txtProvince').val().trim() == '' && '<%= Session("RegionMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblPresentRegionMsg').text(), 'MessageType.Errorr');
                $$('txtProvince').focus();
                  return false;
            }
            else if ($$('ddlCountry').length > 0 && $$('ddlCountry').val() <= 0 && '<%= Session("CountryMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblPresentCountryMsg').text(), 'MessageType.Errorr');
                $$('ddlCountry').focus();
                return false;
            }
            else if ($$('ddlState').length > 0 && $$('ddlState').val() <= 0 && '<%= Session("StateMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblPresentStateMsg').text(), 'MessageType.Errorr');
                $$('ddlState').focus();
                return false;
            }

            else if ($$('ddlPostTown').length > 0 && $$('ddlPostTown').val() <= 0 && '<%= Session("CityMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblPresentCityMsg').text(), 'MessageType.Errorr');
                $$('ddlPostTown').focus();
                return false;
            }
            else if ($$('ddlPostcode').length > 0 && $$('ddlPostcode').val() <= 0 && '<%= Session("PostCodeMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblPresentPostcodeMsg').text(), 'MessageType.Errorr');
                $$('ddlRefCountry').focus();
                return false;
            }
            else if ($$('txtFax').length > 0 && $$('txtFax').val().trim() == '' && '<%= Session("ContactFaxMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblFaxMsg').text(), 'MessageType.Errorr');
                $$('txtFax').focus();
                return false;
            }
            
          }
    </script>


    <div class="card">
        <asp:Panel ID="pnlForm" runat="server">
            <div class="card-header">
                <%--<div style="float: right">
                    
                </div>--%>

                <h4>
                    <asp:Label ID="lblHeader" runat="server">Contact Details</asp:Label>
                    <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" />
                </h4>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-md-12 text-center" >
                        <h5>
                            <asp:Label ID="lblCurrentAddress" runat="server">Current Address</asp:Label></h5>
                    </div>
                </div>

                <div class="form-group row">
                    <asp:Panel ID="pnlAdd1" runat="server" class="col-md-4 mb-1">
                        <asp:Label ID="lblPresentAdd" runat="server" Text="Address1:" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtpresentAdd" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtpresentAdd" FilterType="Custom" FilterMode="InvalidChars" InvalidChars="[^--]"></cc1:FilteredTextBoxExtender>--%>
                        <%--<asp:RequiredFieldValidator ID="rfvPresentAdd1" runat="server" Display="None"
                            ControlToValidate="txtpresentAdd" ErrorMessage="Current Address 1 can not be blank "
                            ValidationGroup="ContactDetail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>

                    </asp:Panel>

                    <asp:Panel ID="pnlAdd2" runat="server" CssClass="col-md-4 mb-1">
                        <asp:Label ID="lblPresentAdd1" runat="server" Text="Address2:" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtpresentadd2" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtpresentadd2" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                       <%-- <asp:RequiredFieldValidator ID="rfvPresentAdd2" runat="server" Display="None"
                            ControlToValidate="txtpresentadd2" ErrorMessage="Current Address 2 can not be blank "
                            ValidationGroup="ContactDetail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                    </asp:Panel>

                    <asp:Panel ID="pnlPlotno" runat="server" class="col-md-4 mb-1">
                        <asp:Label ID="lblPlotno" runat="server" Text="Plot No:"></asp:Label>
                        <asp:TextBox ID="txtPlotno" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <asp:Label ID="lblPlotnoMsg" runat="server" Text="Please Enter Plot No." CssClass="d-none"></asp:Label>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtPlotno" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                    </asp:Panel>


                    <asp:Panel ID="pnlEstate" runat="server" class="col-md-4 mb-1">
                        <asp:Label ID="lblEstate" runat="server" Text="Estate:"></asp:Label>
                        <asp:TextBox ID="txtEstate" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <asp:Label ID="lblEstateMsg" runat="server" Text="Please Enter Estate" CssClass="d-none"></asp:Label>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtEstate" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                    </asp:Panel>

                    <asp:Panel ID="pnlStreet" runat="server" class="col-md-4 mb-1">
                        <asp:Label ID="lblRoad" runat="server" Text="Street:"></asp:Label>
                        <asp:TextBox ID="txtroad" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <asp:Label ID="lblRoadMsg" runat="server" Text="Please Enter Street" CssClass="d-none"></asp:Label>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtroad" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                    </asp:Panel>

                    <asp:Panel ID="pnlProvince" runat="server" class="col-md-4 mb-1">
                        <asp:Label ID="lblProvince" runat="server" Text="Region/Area:" CssClass="required"></asp:Label>
                        <asp:TextBox ID="txtProvince" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtRegion" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                        <%--<asp:RequiredFieldValidator ID="rfvPresentRegion" runat="server" Display="None"
                            ControlToValidate="txtProvince" ErrorMessage="Current Region can not be blank "
                            ValidationGroup="ContactDetail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                    </asp:Panel>


                    <div class="col-md-4 mb-1">
                        <asp:Label ID="lblCountry" runat="server" Text="Country:" CssClass="required"></asp:Label>
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                        <%--<asp:RequiredFieldValidator ID="rfvCountry" runat="server" Display="None" ControlToValidate="ddlCountry" InitialValue="0"
                            ErrorMessage="Please Select Current Country" ValidationGroup="ContactDetail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                        <%--<cc1:CascadingDropDown ID="cddCountry" runat="server" TargetControlID="ddlCountry"
                    Category="country" PromptText="-- Select Country --" LoadingText="[Loading Country...]"
                    ServiceMethod="GetCountry" BehaviorID="cddCountry" />--%>
                    </div>

                    <div class="col-md-4 mb-1">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="lblState" runat="server" Text="State/Province:" CssClass="required"></asp:Label>
                                <asp:DropDownList ID="ddlState" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                               <%-- <asp:RequiredFieldValidator ID="rfvState" runat="server" Display="None" ControlToValidate="ddlState" InitialValue="0"
                                    ErrorMessage="Please Select Current State" ValidationGroup="ContactDetail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlCountry" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="lnkCopyAdd" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <%--<cc1:CascadingDropDown ID="cddState" runat="server" TargetControlID="ddlState" Category="state"
                    ParentControlID="ddlCountry" PromptText="-- Select State/Province --" LoadingText="[Loading State/Province ...]"
                    ServiceMethod="GetState" BehaviorID="cddState" />--%>
                    </div>

                    <asp:Panel ID="pnlPostTown" runat="server" class="col-md-4 mb-1">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="lblPostTown" runat="server" Text="Post Town/Dist. :" CssClass="required"></asp:Label>
                                <asp:DropDownList ID="ddlPostTown" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                                <%--<asp:RequiredFieldValidator ID="rfvCity" runat="server" Display="None" ControlToValidate="ddlPostTown" InitialValue="0"
                                    ErrorMessage="Please Select Current Town/City" ValidationGroup="ContactDetail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                <%--<cc1:CascadingDropDown ID="cddPostTown" runat="server" TargetControlID="ddlPostTown"
                    Category="city" ParentControlID="ddlState" PromptText="-- Select Post Town/District --"
                    LoadingText="[Loading Post Town/District ...]" ServiceMethod="GetCity" />--%>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlState" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="lnkCopyAdd" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </asp:Panel>


                    <asp:Panel ID="pnlPostCode" runat="server" class="col-md-4 mb-1">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="lblPostCode" runat="server" Text="Post Code:" CssClass="required"></asp:Label>
                                <asp:DropDownList ID="ddlPostcode" runat="server" CssClass="selectpicker form-control" />
                                <%--<asp:RequiredFieldValidator ID="rfvPostCode" runat="server" Display="None" ControlToValidate="ddlPostcode" InitialValue="0"
                                    ErrorMessage="Please Select Current Post Code" ValidationGroup="ContactDetail" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                <%-- <cc1:CascadingDropDown ID="cddPostcode" runat="server" TargetControlID="ddlPostcode"
                    Category="zipcode" ParentControlID="ddlPostTown" PromptText="-- Select Post Code --"
                    LoadingText="[Loading Post Code ...]" ServiceMethod="GetZipCode" />--%>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlPostTown" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="lnkCopyAdd" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </asp:Panel>

                    <div class="col-md-4 mb-1">
                        <asp:Label ID="lblTelno" runat="server" Text="Tel. No:"></asp:Label>
                        <asp:TextBox ID="txttelno" runat="server" MaxLength="30" CssClass="form-control"></asp:TextBox>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txttelno" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                    </div>

                    <asp:Panel ID="pnlFax" runat="server" class="col-md-4 mb-1">
                        <asp:Label ID="lblFax" runat="server" Text="Fax:"></asp:Label>
                        <asp:TextBox ID="txtFax" runat="server" MaxLength="30" CssClass="form-control"></asp:TextBox>
                        <asp:Label ID="lblFaxMsg" runat="server" Text="Please Enter Fax." CssClass="d-none"></asp:Label>
                        <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtFax" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                    </asp:Panel>




                </div>

                <%--<div class="form-group row">
                    
                </div>

                <div class="form-group row">
                    
                </div>

                <div class="form-group row">
                    
                </div>--%>

                <asp:Panel ID="pnlPermAddress" runat="server" >

                <div class="form-group row">
                    <div class="col-12">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12 text-center" >
                        <h5>
                            <asp:Label ID="lblPermanentAddress" runat="server">Permanent Address</asp:Label></h5>
                    </div>
                </div>
                

                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="form-group row">
                            <%--<div class="col-md-4">
                    </div>--%>
                            <div class="col-md-12 text-center" >
                                <asp:LinkButton ID="lnkCopyAdd" runat="server" ValidationGroup="ContactDetail">Copy Above Address</asp:LinkButton>
                            </div>
                            <%-- <div class="col-md-4">
                    </div>--%>
                        </div>


                        <div class="form-group row">
                            <asp:Panel ID="pnlPermAdd1" runat="server" class="col-md-4 mb-1">
                                <asp:Label ID="lblPrmAdd" runat="server" Text="Address1:"></asp:Label>
                                <asp:TextBox ID="txtprmAdd" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtprmAdd" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                            </asp:Panel>

                            <asp:Panel ID="pnlPermAdd2" runat="server" class="col-md-4 mb-1">
                                <asp:Label ID="lblPrmAdd2" runat="server" Text="Address2:"></asp:Label>
                                <asp:TextBox ID="txtprmAdd2" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtprmAdd2" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                            </asp:Panel>

                            <asp:Panel ID="pnlPermPlotno" runat="server" class="col-md-4 mb-1">
                                <asp:Label ID="lblPrmPlotno" runat="server" Text="Plot No:"></asp:Label>
                                <asp:TextBox ID="txtprmPlotno" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtprmPlotno" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                            </asp:Panel>


                            <asp:Panel ID="pnlPermEstate" runat="server" class="col-md-4 mb-1">
                                <asp:Label ID="lblPrmEstate" runat="server" Text="Estate:"></asp:Label>
                                <asp:TextBox ID="txtPrmEstate" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtPrmEstate" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                            </asp:Panel>

                            <asp:Panel ID="pnlPermStreet" runat="server" class="col-md-4 mb-1">
                                <asp:Label ID="lblPrmRoad" runat="server" Text="Street:"></asp:Label>
                                <asp:TextBox ID="txtPrmRoad" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtPrmRoad" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                            </asp:Panel>

                            <asp:Panel ID="pnlPermProvince" runat="server" class="col-md-4 mb-1">
                                <asp:Label ID="lblPrmProvince" runat="server" Text="Region/Area:"></asp:Label>
                                <asp:TextBox ID="txtPrmProvince" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="txtPrmregion" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                            </asp:Panel>


                            <div class="col-md-4 mb-1">
                                <asp:Label ID="lblPrmCountry" runat="server" Text="Country:"></asp:Label>
                                <asp:DropDownList ID="ddlPrmCountry" runat="server" CssClass="selectpicker form-control"  />
                                <%-- <cc1:CascadingDropDown ID="cddPrmCountry" runat="server" TargetControlID="ddlPrmCountry"
                    Category="country" PromptText="-- Select Country --" LoadingText="[Loading Country...]"
                    ServiceMethod="GetCountry" />--%>
                            </div>

                            <div class="col-md-4 mb-1">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Label ID="lblPrmstate" runat="server" Text="State/Province:"></asp:Label>
                                        <asp:DropDownList ID="ddlPrmState" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                                        <%--<cc1:CascadingDropDown ID="cddPrmState" runat="server" TargetControlID="ddlPrmState"
                    Category="state" ParentControlID="ddlPrmCountry" PromptText="-- Select State/Province --"
                    LoadingText="[Loading State/Province ...]" ServiceMethod="GetState" />--%>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlPrmCountry" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="lnkCopyAdd" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>

                            <asp:Panel ID="pnlPermPostTown" runat="server" class="col-md-4 mb-1">
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Label ID="lblPrmposttown" runat="server" Text="Post Town/Dist. :"></asp:Label>
                                        <asp:DropDownList ID="ddlPrmPostTown" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                                        <%--<cc1:CascadingDropDown ID="cddPrmPostTown" runat="server" TargetControlID="ddlPrmPostTown"
                    Category="city" ParentControlID="ddlPrmState" PromptText="-- Select Post Town/District --"
                    LoadingText="[Loading Post Town/District ...]" ServiceMethod="GetCity" />--%>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlPrmState" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="lnkCopyAdd" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>

                            <asp:Panel ID="pnlPermPostCode" runat="server" class="col-md-4 mb-1">
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Label ID="lblPrmPostCode" runat="server" Text="Post Code:"></asp:Label>
                                        <asp:DropDownList ID="ddlPrmPostcode" runat="server" CssClass="selectpicker form-control" />
                                        <%--<cc1:CascadingDropDown ID="cddPrmPostCode" runat="server" TargetControlID="ddlPrmPostCode"
                    Category="zipcode" ParentControlID="ddlPrmPostTown" PromptText="-- Select Post Code --"
                    LoadingText="[Loading Post Code ...]" ServiceMethod="GetZipCode" />--%>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlPrmPostTown" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="lnkCopyAdd" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </asp:Panel>

                            <div class="col-md-4 mb-1">
                                <asp:Label ID="lblPrmTelNo" runat="server" Text="Tel. No:"></asp:Label>
                                <asp:TextBox ID="txtPrmTelNo" runat="server" MaxLength="30" CssClass="form-control"></asp:TextBox>
                                <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="txtPrmTelNo" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                            </div>

                            <asp:Panel ID="pnlPermFax" runat="server" class="col-md-4 mb-1">
                                <asp:Label ID="lblPrmfax" runat="server" Text="Fax:"></asp:Label>
                                <asp:TextBox ID="txtPrmfax" runat="server" MaxLength="30" CssClass="form-control"></asp:TextBox>
                                <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="txtPrmfax" FilterType="LowercaseLetters, UppercaseLetters, Numbers, Custom" ValidChars=" , &, :, /"></cc1:FilteredTextBoxExtender>--%>
                            </asp:Panel>

                        </div>

                        <%--<div class="form-group row">
                            
                        </div>

                        <div class="form-group row">
                            
                        </div>

                        <div class="form-group row">
                            
                        </div>--%>
                    </ContentTemplate>
                    <Triggers>
                        <%--<asp:AsyncPostBackTrigger ControlID="lnkCopyAdd" EventName="Click" />--%>
                    </Triggers>
                </asp:UpdatePanel>

                </asp:Panel>

                <div class="form-group row">
                    <div class="col-md-12 text-center" >
                       <%-- <asp:ValidationSummary ID="vs"
                            HeaderText="You must enter a value in the following fields:"
                            DisplayMode="BulletList"
                            EnableClientScript="true"
                            runat="server" ValidationGroup="ContactDetail" Style="color: red" />--%>                        
                        <asp:Label ID="lblPresentAdd1Msg" runat="server" Text="Please Enter Current Address 1." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblPresentAdd2Msg" runat="server" Text="Please Enter Current Address 2." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblPresentRegionMsg" runat="server" Text="Please Enter Current Region." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblPresentCountryMsg" runat="server" Text="Please Select Current Country." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblPresentStateMsg" runat="server" Text="Please Select Current State." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblPresentCityMsg" runat="server" Text="Please Select Current Post Town/City." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblPresentPostcodeMsg" runat="server" Text="Please Select Current Post Code." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblSaveMsg" runat="server" Text="Contact Details Saved Successfully! \n\n Click Ok to continue with other section(s)." CssClass="d-none"></asp:Label>
                    </div>
                </div>

                <%-- <div class="form-group row">
            <div class="col-md-12" style="text-align: center;">
                <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="Something Bad happened, Please contact Administrator!!!!" Visible="false" />
            </div>
        </div>--%>
            </div>

            <div class="card-footer">
                <div class="form-group row d-flex justify-content-center">
                   <%-- <div class="col-md-4">
                    </div>--%>
                    <div class="col-md-4">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary w-100" ValidationGroup="ContactDetail"  />
                    </div>
                    <%--<div class="col-md-4">
                    </div>--%>
                </div>
            </div>
        </asp:Panel>
        <asp:HiddenField ID="hfCancelLang" runat="server" />
        <cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsApplicant" _ModuleName="ContactDetails" _TargetControl="pnlForm" CancelControlName="hfCancelLang" />
    </div>


</asp:Content>
