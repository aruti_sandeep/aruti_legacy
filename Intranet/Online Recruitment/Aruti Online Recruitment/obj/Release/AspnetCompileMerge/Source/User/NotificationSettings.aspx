﻿<%@ Page Language="vb" Title="Notification Settings" AutoEventWireup="false" MasterPageFile="~/User/Site4.Master" CodeBehind="NotificationSettings.aspx.vb" Inherits="Aruti_Online_Recruitment.NotificationSettings" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">

        function checkBoxCSS() {

            $(".switch input").bootstrapSwitch();
            $('.switch input').bootstrapSwitch('onText', 'Yes');
            $('.switch input').bootstrapSwitch('offText', 'No');
            $('.switch input').bootstrapSwitch('labelWidth', 'auto');
            $('.yesno').on('switchChange.bootstrapSwitch', function (event, state) {
                javascript: __doPostBack('.yesno', 'chkNotification_CheckedChanged');
            });
        }

        $(document).ready(function () {
            checkBoxCSS();

            $("[id*=lnkUp]").on('click', function () {
                __doPostBack($(this).get(0).id, 0);
            });

            $("[id*=lnkdown]").on('click', function () {
                __doPostBack($(this).get(0).id, 0);
            });
        });

        function integersOnly(obj) {
            obj.value = obj.value.replace(/[^0-9]/g, '');
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    checkBoxCSS();

                    $("[id*=lnkUp]").on('click', function () {
                        __doPostBack($(this).get(0).id, 0);
                    });

                    $("[id*=lnkdown]").on('click', function () {
                        __doPostBack($(this).get(0).id, 0);
                    });
                }
            });
        };

    </script>

    <div class="card">
        <div class="card-header">
            <h4>Notification Settings
                    <asp:Label ID="LblHeader" runat="server" Text=""></asp:Label>
            </h4>
        </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="card-body">
                    <asp:DataList ID="dlVacanciesList" runat="server" CssClass ="width100"  DataKeyField="Appnotificationpriorityunkid" DataSourceID="odsVacancyNotification"> <%-- Width="100%"--%>
                        <HeaderTemplate>
                            <div class="form-group row">
                                <div class="col-4">
                                    <asp:Label ID="LblVacancy" runat="server" Text="Vacancy" CssClass="font-weight-bold" ></asp:Label>  <%--Font-Bold="true"--%>
                                </div>
                                <div class="col-4">
                                    <asp:Label ID="LblSendNotification" runat="server" Text="Send Notification" CssClass="font-weight-bold"  ></asp:Label>  <%--Font-Bold="true"--%>
                                </div>
                                <div class="col-4">
                                    <%--<asp:Label ID="LblPriority" runat="server" Text="Priority" Font-Bold="true"></asp:Label>--%>
                                </div>
                            </div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="form-group row">
                                <div class="col-4">
                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("Vacancytitle")) %>--%>
                                    <%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Vacancytitle"), True) %>
                                </div>
                                <div class="col-4">
                                    <asp:CheckBox ID="chkNotification" runat="server" Checked='<%# Convert.ToBoolean(Eval("Isactive"))%>' CssClass="switch yesno" AutoPostBack="true" OnCheckedChanged="chkNotification_CheckedChanged" />
                                    <asp:HiddenField ID="hdnfieldVacancytitleId" runat="server" Value='<%# Convert.ToInt32(Eval("Vacancytitleunkid"))%>' />
                                    <asp:HiddenField ID="hdnfieldPriority" runat="server" Value='<%# Convert.ToInt32(Eval("priority"))%>' />
                                </div>
                                <div class="col-4">
                                    <%--<asp:TextBox ID="txtPriority" runat="server" TextMode="Number" Text ='<%# Convert.ToInt32(Eval("priority"))%>'  Style="text-align: center" Width="70px" CssClass="form-control" onkeyup="integersOnly(this)"></asp:TextBox>--%>
                                    <asp:LinkButton ID="lnkUp" runat="server" CssClass="updown" OnClick="lnkupdown_Click"><i class="fa fa-arrow-up h5"></i></asp:LinkButton>   <%--style="font-size:large"--%>
                                    <asp:LinkButton ID="lnkdown" runat="server" CssClass="updown" OnClick="lnkupdown_Click"><i class="fa fa-arrow-down h5" ></i></asp:LinkButton>  <%--style="font-size:large"--%>
                                </div>

                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:ObjectDataSource ID="odsVacancyNotification" runat="server" SelectMethod="GetApplicantNotificationSettings" TypeName="Aruti_Online_Recruitment.clsNotificationSettings" EnablePaging="false">
            <SelectParameters>
                <asp:Parameter Name="xCompanyID" Type="Int32" DefaultValue="0" />
                <asp:Parameter Name="xCompanyCode" Type="String" DefaultValue="" />
                <asp:Parameter Name="xApplicantID" Type="Int32" DefaultValue="0" />
                <asp:Parameter Name="intMasterType" Type="Int32" DefaultValue="0" />
            </SelectParameters>
        </asp:ObjectDataSource>

        <asp:Label ID="lblMaximumMsg" runat="server" Text="Sorry, you can set notification for maximum #numof# vacancies only." CssClass="d-none"></asp:Label>
    </div>
</asp:Content>
