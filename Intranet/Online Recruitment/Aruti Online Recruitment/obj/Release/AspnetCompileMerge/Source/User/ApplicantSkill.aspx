﻿<%@ Page Title="Skills" Language="vb" AutoEventWireup="false" MasterPageFile="~/User/Site4.Master" CodeBehind="ApplicantSkill.aspx.vb" Inherits="Aruti_Online_Recruitment.ApplicantSkill" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <script type="text/javascript">

        $(document).ready(function () {

            checkBoxCSS();

            $("[id*=ddlskillCate]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            $("[id*=objddlSkillCat]").on('change', function () {
                __doPostBack($(this).get(0).id, 0)
            });

            $("[id*=btndelete]").on('click', function () {
                if (!ShowConfirm($$('lblgrdviewSkillRowDeletingMsg2').text(), this))
                    return false;
                else
                    return true;
            });
        });

        function pageLoad(sender, args) {

            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });

        }

        function checkBoxCSS() {
            $(".switch input").bootstrapSwitch();
            $('.switch input').bootstrapSwitch('onText', 'Yes');
            $('.switch input').bootstrapSwitch('offText', 'No');
            //$('.switch input').bootstrapSwitch('labelText', 'Other Skill Not In List');
            $('.switch input').bootstrapSwitch('labelWidth', 'auto');

            $('#<%= chkOtherSkill.ClientID %>').on('switchChange.bootstrapSwitch', function (event, state) {
                javascript: __doPostBack('<%= chkOtherSkill.UniqueID %>', 'chkOtherSkill_CheckedChanged');
            });

             $('#<%= ddlskillCate.ClientID %>').on('switchChange.bootstrapSwitch', function (event, state) {
                javascript: __doPostBack('<%= chkOtherSkill.UniqueID %>', 'chkOtherSkill_CheckedChanged');
            });
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    checkBoxCSS();

                    $("[id*=ddlskillCate]").on('change', function () {
                        __doPostBack($(this).get(0).id, 0)
                    });

                    $("[id*=objddlSkillCat]").on('change', function () {
                        __doPostBack($(this).get(0).id, 0)
                    });

                    $$('btnskillAdd').on('click', function () {
                        return IsValidate();
                    });

                    $("[id*=btndelete]").on('click', function () {
                        if (!ShowConfirm($$('lblgrdviewSkillRowDeletingMsg2').text(), this))
                            return false;
                        else
                            return true;
                    });
                }
            });
        };

        $(document).ready(function () {
            $$('btnskillAdd').on('click', function () {
                return IsValidate();
            });
        });

        function IsValidate() {

            if ($$('chkOtherSkill')[0].checked) {

                if ($$('txtOtherSkillCategory').val().trim() == '') {
                    ShowMessage($$('lblOtherSkillCategoryMsg').text(), 'MessageType.Errorr');
                    $$('txtOtherSkillCategory').focus();
                    return false;
                }

                else if ($$('txtOtherSkill').val().trim() == '') {
                    ShowMessage($$('lblOtherSkillMsg').text(), 'MessageType.Errorr');
                    $$('txtOtherSkill').focus();
                    return false;
                }
            }
            else {
                if (parseInt($$('ddlskillCate').val()) <= 0) {
                    ShowMessage($$('lblCategoryMsg').text(), 'MessageType.Errorr');
                    $$('ddlskillCate').focus();
                    return false;
                }

                else if (parseInt($$('ddlskill').val()) <= 0) {
                    ShowMessage($$('lblSkillMsg').text(), 'MessageType.Errorr');
                    $$('ddlskill').focus();
                    return false;
                }
            }
            if (parseInt($$('cboSkillExpertise').val()) <= 0) {
                ShowMessage($$('lblSkillExpertiseMsg').text(), 'MessageType.Errorr');                
                $$('cboSkillExpertise').focus();
                return false;
            }
            //if ($$('txtRemarks').val().trim() == '') {
            //    ShowMessage('Please Enter Remark', 'MessageType.Errorr');
            //    $$('txtRemarks').focus();
            //    return false;
            //}
            

            //else if (parseInt($$('cboSkillExpertise').val()) <= 0) {
            //    ShowMessage('Please Enter Skill Expertise', 'MessageType.Errorr');
            //    $$('cboSkillExpertise').focus();
            //    return false;
            //}

            //else if (parseInt($$('cboSkillExpertise').val()) <= 0) {
            //    ShowMessage('Please Enter Skill Expertise', 'MessageType.Errorr');
            //    $$('cboSkillExpertise').focus();
            //    return false;
            //}



            return true;
        }

    </script>

    <div class="card">

        <asp:Panel ID="pnlForm" runat="server">
            <div class="card-header">
                <%--<div style="float: right">
                    
                </div>--%>


                <h4>
                    <asp:Label ID="lblHeader" runat="server">Skills</asp:Label>
                    <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" />
                </h4>
                <asp:Label ID="lblKeySkill" runat="server" Visible="false">Key Skills</asp:Label>
            </div>

            <div class="card-body">
                <div class="form-group row">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional" class="row col-md-8 m-0 p-0">
                        <ContentTemplate>
                            <asp:Panel ID="pnlSkill" runat="server" CssClass="row col-12 m-0 p-0">
                                <div class="col-md-6">
                                    <asp:Label ID="lblCategory" runat="server" Text="Category:" CssClass="required"></asp:Label>
                                    <asp:DropDownList ID="ddlskillCate" runat="server" CssClass="selectpicker form-control" AutoPostBack="false"  />
                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="None"
                                        ControlToValidate="ddlskillCate" ErrorMessage="Please select Skill Category." InitialValue="0"
                                        ValidationGroup="Skill" SetFocusOnError="True">
                                    </asp:RequiredFieldValidator>--%>
                                    <%--<cc1:CascadingDropDown ID="cddskillCate" runat="server" TargetControlID="ddlskillCate"
                        Category="skillcat" PromptText="-- Select Skill Category --" LoadingText="[Loading Skill Category...]"
                        ServiceMethod="GetSkillCategory" />--%>
                                    <asp:Label ID="lblCategoryMsg" runat="server" Text="Skill Category is compulsory information, it can not be blank. Please select Skill Category to continue." CssClass="d-none"></asp:Label>
                                </div>

                                <div class="col-md-6">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Label ID="lblSkill" runat="server" Text="Skill:" CssClass="required"></asp:Label>
                                            <asp:DropDownList ID="ddlskill" runat="server" CssClass="selectpicker form-control" ValidationGroup="Skill" />
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="None"
                                                ControlToValidate="ddlskill" ErrorMessage="Please select Skill." InitialValue="0"
                                                ValidationGroup="Skill" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>--%>
                                            <%--<cc1:CascadingDropDown ID="cddskill" runat="server" TargetControlID="ddlskill" ParentControlID="ddlskillCate"
                        Category="skill" PromptText="-- Select Skill --" LoadingText="[Loading Skill...]"
                        ServiceMethod="GetSkill" />--%>
                                            <asp:Label ID="lblSkillMsg" runat="server" Text="Skill is compulsory information, it can not be blank. Please select Skill to continue." CssClass="d-none"></asp:Label>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlskillCate" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="pnlOtherSkill" runat="server" CssClass="row col-12 m-0 p-0">
                                <div class="col-md-6">
                                    <asp:Label ID="lblOtherSkillCategory" runat="server" Text="Other Category:" CssClass="required"></asp:Label>
                                    <asp:TextBox ID="txtOtherSkillCategory" runat="server" CssClass="form-control"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" Display="None"
                                        ControlToValidate="txtOtherSkillCategory" ErrorMessage="Skill Category can not be Blank. "
                                        ValidationGroup="Skill"
                                        SetFocusOnError="True">
                                    </asp:RequiredFieldValidator>--%>
                                    <asp:Label ID="lblOtherSkillCategoryMsg" runat="server" Text="Skill Category is compulsory information, it can not be blank. Please enter Skill Category to continue." CssClass="d-none"></asp:Label>

                                </div>

                                <div class="col-md-6">
                                    <asp:Label ID="lblOtherSkill" runat="server" Text="Other Skills:" CssClass="required"></asp:Label>
                                    <asp:TextBox ID="txtOtherSkill" runat="server" CssClass="form-control"></asp:TextBox>
                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" Display="None"
                                        ControlToValidate="txtOtherSkill" ErrorMessage="Skill can not be Blank. "
                                        ValidationGroup="Skill" SetFocusOnError="True">
                                    </asp:RequiredFieldValidator>--%>
                                    <asp:Label ID="lblOtherSkillMsg" runat="server" Text="Skill is compulsory information, it can not be blank. Please enter Skill to continue." CssClass="d-none"></asp:Label>
                                </div>
                            </asp:Panel>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="chkOtherSkill" EventName="CheckedChanged" />
                            <%--<asp:AsyncPostBackTrigger ControlID="btnskillAdd" EventName="Click" />--%>
                        </Triggers>
                    </asp:UpdatePanel>

                    <div class="col-md-4">
                        <asp:Label ID="lblSkillExpertise" runat="server" Text="Skill Expertise:"></asp:Label>
                        <asp:DropDownList ID="cboSkillExpertise" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" ValidationGroup="Skill" />
                        <%-- <asp:RequiredFieldValidator ID="rfvSkillExpertise" runat="server" Display="None"
                            ControlToValidate="cboSkillExpertise" ErrorMessage="Please select Skill Expertise." InitialValue="0"
                            ValidationGroup="Skill" SetFocusOnError="True">
                        </asp:RequiredFieldValidator>--%>
                        <asp:Label ID="lblSkillExpertiseMsg" runat="server" Text="Skill Expertise is compulsory information, it can not be blank. Please select Skill Expertise to continue." CssClass="d-none"></asp:Label>
                    </div>
                </div>

                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="form-group row">

                            <div class="col-md-12">
                                <asp:Label ID="lblRemarkSkill" runat="server" Text="Remarks:"></asp:Label>
                                <asp:TextBox ID="txtRemarks" runat="server" MaxLength="255" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                            </div>

                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="chkOtherSkill" EventName="CheckedChanged" />
                        <%--<asp:AsyncPostBackTrigger ControlID="btnskillAdd" EventName="Click" />--%>
                    </Triggers>
                </asp:UpdatePanel>


                <%-- <div class="form-group row">
                    <div class="col-md-12" style="text-align: center;">
                        <asp:ValidationSummary ID="vs"
                            HeaderText="You must enter a value in the following fields:"
                            DisplayMode="BulletList"
                            EnableClientScript="true"
                            runat="server" ValidationGroup="Skill" Style="color: red" />
                    </div>
                </div>--%>

                <%--<div class="form-group row">
            <div class="col-md-12" style="text-align: center;">
                <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="Something Bad happened, Please contact Administrator!!!!" Visible="false" />
            </div>
        </div>--%>
            </div>

            <div class="card-footer">
                <div class="form-group row">
                    <div class="col-md-4">
                        <asp:Label ID="lblOtherSkillNotInList" runat="server" Text="Other Skill Not In List" CssClass="font-weight-bold"></asp:Label>
                        <asp:CheckBox ID="chkOtherSkill" runat="server" CssClass="switch float-right" AutoPostBack="false" />
                    </div>

                    <div class="col-md-4">
                        <asp:Button ID="btnskillAdd" runat="server" CssClass="btn btn-primary w-100" Text="Add Skill" />
                        <asp:Label ID="lblbtnskillAddMsg1" runat="server" Text="Sorry, Selected Skill is already added to the list." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblbtnskillAddMsg2" runat="server" Text="Skill Saved Successfully! \n\n Click Ok to add another Skill or to continue with other section(s)." CssClass="d-none"></asp:Label>
                    </div>

                    <%--<div class="col-md-4">
                        <%--<asp:Button ID="btnskillEdit" runat="server" CssClass="btn btn-primary" Style="width: 100%;" Enabled="False"
                    Text="Update Skill" ValidationGroup="Skill" />--%>
                    <%--</div>--%>
                </div>
            </div>

            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnl_grdviewSkill" runat="server" CssClass="table-responsive minheight300" >
                        <asp:GridView ID="grdviewSkill" runat="server" AutoGenerateColumns="False" CssClass="table"
                            AllowPaging="false" ShowHeaderWhenEmpty="True" DataKeyNames="skilltranunkid">
                            <Columns>
                                <asp:TemplateField HeaderText="Skill Category" FooterText="colhSkillCategory">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="objlblSkillCat" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("SkillCategory")) %>'></asp:Label>--%>
                                        <asp:Label ID="objlblSkillCat" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(If(CInt(Eval("skillcategoryunkid")) > 0, Eval("SkillCategory"), Eval("other_skillcategory")), True) %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="objddlSkillCat" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" OnSelectedIndexChanged="objddlSkillCat_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:TextBox ID="objtxtSkillCat" runat="server" Text='<%# Eval("other_skillcategory") %>' TextMode="MultiLine" CssClass="form-control" Visible="false"></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Skill" FooterText="colhSkill">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="objlblSkill" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("SKILL")) %>'></asp:Label>--%>
                                        <asp:Label ID="objlblSkill" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(If(CInt(Eval("skillunkid")) > 0, Eval("SKILL"), Eval("other_skill")), True) %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="objddlSkill" runat="server" CssClass="selectpicker form-control"></asp:DropDownList>
                                        <asp:TextBox ID="objtxtSkill" runat="server" Text='<%# Eval("other_skill") %>' TextMode="MultiLine" CssClass="form-control" Visible="false"></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Expertise" FooterText="colhExpertise">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="objlblSkillExpertise" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("SkillExpertise")) %>'></asp:Label>--%>
                                        <asp:Label ID="objlblSkillExpertise" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("SkillExpertise"), True) %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="objcboSkillExpertise" runat="server" CssClass="selectpicker form-control"></asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Remark" FooterText="colhRemark">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="objlblRemark" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("Remark")) %>'></asp:Label>--%>
                                        <asp:Label ID="objlblRemark" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Remark"), True) %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="objtxtRemark" runat="server" Text='<%# Eval("Remark") %>' TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Edit" FooterText="colhEdit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="objbtnedit" runat="server" CommandName="Edit" Text="<i aria-hidden='true' class='fa fa-edit'></i>"></asp:LinkButton>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="btnupdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                        <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                    </EditItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Delete" FooterText="colhDelete">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="objbtndelete" runat="server" CommandName="Delete" Text="<i aria-hidden='true' class='fa fa-trash'></i>" ></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lblgrdviewSkillRowUpdatingMsg1" runat="server" Text="Sorry, Selected Skill is already added to the list." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblgrdviewSkillRowUpdatingMsg2" runat="server" Text="Skill Updated Successfully! \n\n Click Ok to add another Skill or to continue with other section(s)." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblgrdviewSkillRowDeletingMsg" runat="server" Text="Skill Deleted Successfully! \n\n Click Ok to add another Skill or to continue with other section(s)." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblgrdviewSkillRowDeletingMsg2" runat="server" Text="Are you sure you want to Delete this Skill?" CssClass="d-none"></asp:Label>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnskillAdd" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="chkOtherSkill" EventName="CheckedChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:HiddenField ID="hfCancelLang" runat="server" />
        <cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsApplicantSkill" _ModuleName="ApplicantSkill" _TargetControl="pnlForm" CancelControlName="hfCancelLang" />
    </div>
</asp:Content>
