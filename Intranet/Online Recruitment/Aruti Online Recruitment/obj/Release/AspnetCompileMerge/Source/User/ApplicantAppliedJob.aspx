﻿<%@ Page Title="Job Applied" Language="vb" AutoEventWireup="false" MasterPageFile="~/User/Site4.Master" CodeBehind="ApplicantAppliedJob.aspx.vb" Inherits="Aruti_Online_Recruitment.ApplicantAppliedJob" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>
<%@ Register Src="../Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<%--'S.SANDEEP |01-JUN-2023| -- START {A1X-960 - [uc9]} -- END--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <script type="text/javascript">
        function pageLoad(sender, args) {
            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                setevents();

            });

        };

        $(document).ready(function () {
            setevents();
        });

        function setevents() {
            $("[id*=btnApply]").on('click', function () {
                if (!ShowConfirm("Are you sure you want to Delete Applied job?", this))
                    return false;
                else
                    return true;
            });

            //'S.SANDEEP |01-JUN-2023| -- START
            //'ISSUE/ENHANCEMENT : A1X-960
            $("[id*=_image_file]").each(function () {
                fileUpLoadChange('#' + this.id)
            });

            $("[id*=_image_file]").each(function () {
                $(this).on('click', function () {
                    return IsValidAttach('#' + this.id);
                });
            });


            //$('.close').on('click', function () {
            //    $(this).parents('[id*=pnlFile]').find('span[id*=lblFileName]').text('');
            //    $(this).parents('[id*=pnlFile]').find('input[id*=hfFileName]').val('');
            //    $(this).parents('[id*=pnlFile]').hide();
            //});
            //'S.SANDEEP |01-JUN-2023| -- END
        }

        //'S.SANDEEP |01-JUN-2023| -- START
        //'ISSUE/ENHANCEMENT : A1X-960
        function IsValidAttach(objFile) {
            if ($(objFile).parents('.attachdoc:first').find("select[id*='ddlDocType']").val() <= 0) {
                ShowMessage($$('lblDocTypeMsg').text(), 'MessageType.Errorr');
                $(objFile).parents('.attachdoc:first').find("select[id*='ddlDocType']").focus();
                return false;
            }
            else if ($(objFile).parents('.attachdoc:first').find("select[id*='ddlAttachType']").val() <= 0) {
                ShowMessage($$('lblAttachTypeMsg').text(), 'MessageType.Errorr');
                $(objFile).parents('.attachdoc:first').find("select[id*='ddlAttachType']").focus();
                return false;
        }
            return true;
        }
        //'S.SANDEEP |01-JUN-2023| -- END

        //'Pinkal (16-Oct-2023) -- Start
        //'(A1X-1425) TRA - Provide applicant feedback on the online recruitment portal.
         function openwin() {
            var popUp = window.open('../User/ApplicantFeedback.aspx', '_blank');
            if (popUp == null || typeof (popUp) == 'undefined') {
                alert('Please disable your pop-up blocker and click the "Show Feedback" button again.');
            }
            else {
                popUp.focus();
            }
        }
     /*   'Pinkal (16-Oct-2023) -- End*/

    </script>



    <div class="card">
        <asp:Panel ID="pnlForm" runat="server">
             <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
            <div class="card" >
                <div class="card-header">
                <%--<div style="float: right">
                    
                </div>--%>
                            <h4>
                                <asp:Label ID="lblAppliedHeader" runat="server">Applied Job List</asp:Label>
                    <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" />
                </h4>
                </div>

                <div class="card-body pb-0">
                    <asp:DataList ID="dlVaanciesList" runat="server" DataKeyField="appvacancytranunkid" DataSourceID="odsVacancy" CssClass="width100 vacancy">
                        <ItemTemplate>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="card mb-3 panel-default">
                                        <div class="card-header">
                                            <div class="form-group row mb-0">
                                                <div class="col-md-12">
                                                    <h4 class="">
                                                        <div class="d-none">
                                                            <div class="far fa-hand-point-right"></div>
                                                        </div>
                                                        <h4>
                                                                    <%--   'Pinkal (16-Oct-2023) -- Start
                                                                    '(A1X-1425) TRA - Provide applicant feedback on the online recruitment portal.--%>

                                                                    <div class="form-group row">
                                                                        <div class="col-md-10">
                                                            <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("vacancytitle")) %>--%>
                                                            <asp:Label ID="objlblVacancyTitle" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("vacancytitle"), True) %>'></asp:Label>
                                                            <%--  'Pinkal (01-Jan-2018) -- Start
                                                      'Enhancement - Job seekers to get alerts if they already signed up account on the recruitment site.--%>
                                                            <%--<asp:HiddenField ID ="hdnFieldVacancyTitleId" runat="server" Value ='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("VacancyTitleUnkid")) %>' />--%>
                                                            <asp:HiddenField ID="hdnFieldVacancyTitleId" runat="server" Value='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("VacancyTitleUnkid"), True) %>' />
                                                            <%--Pinkal (01-Jan-2018) -- Start--%>
                                                                        </div>

                                                                        <div class="col-md-2 text-right">
                                                                            <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() = "TRA" Then %>
                                                                            <asp:LinkButton ID="btnFeedback" runat="server" CommandName="Feedback" CssClass="btn btn-primary sthf text-decoration-none" Text="">
                                                                                <div id="div2" runat="server" class="fa fa-check mr-1" visible="false"></div>
                                                                                    <asp:Label ID="LblFeedback" runat="server" Text="Show Feedback"></asp:Label>
                                                                                    <asp:HiddenField ID="hdnFieldVacancyId" runat="server" Value='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("vacancyid"), True) %>' />
                                                                                </a>
                                                                            </asp:LinkButton>
                                                                           <% End If %>
                                                                        </div>

                                                                    </div>
                                                                    <%-- 'Pinkal (16-Oct-2023) -- End--%>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 d-none">
                                                    <div class="row d-none">
                                                        <div class="col-md-1"></div>
                                                        <div class="col-md-11">
                                                            <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Session("CompName").ToString) %>--%>
                                                            <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(Session("CompName").ToString, True) %>--%>
                                                            <asp:Label ID="objlblCompName" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Session("CompName").ToString, True) %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                
                                        <div class="card-body">
                                            <div class="form-group row" id="divJobLocation" runat="server">
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblJobLocation" runat="server" Text="Job Location :" CssClass="font-weight-bold"></asp:Label>                                                                    
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="objlblJobLocation" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" id="divRemark" runat="server">
                                                <%-- style="padding-left: 15px"--%>
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblJobDiscription" runat="server" Text="Job Description :" CssClass="font-weight-bold"></asp:Label>
                                                </div>
                                                <div class="col-md-10">
                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("remark")) %>--%>
                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("remark"), True) %>--%>
                                                    <asp:Label ID="objlblJobDiscription" runat="server" Text='<%# Eval("remark").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" id="divResponsblity" runat="server">
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblResponsblity" runat="server" Text="Responsiblity: " CssClass="font-weight-bold"></asp:Label>
                                                </div>
                                                <div class="col-md-10">
                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("duties")) %>--%>
                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("duties"), True) %>--%>
                                                    <asp:Label ID="objlblResponsblity" runat="server" Text='<%#  Eval("duties").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") %> '></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" id="divSkill" runat="server">
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblSkill" runat="server" Text="Skill Required :" CssClass="font-weight-bold"></asp:Label>
                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("skill")) %>--%>
                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("skill"), True) %>--%>
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="objlblSkill" runat="server" Text='<%# "• " & Eval("skill").ToString.Replace(",", "<BR> • ") %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" id="divQualification" runat="server">
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblQualification" runat="server" Text="Qualification Required: " CssClass="font-weight-bold"></asp:Label>
                                                </div>
                                                <div class="col-md-10">
                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("Qualification")) %>--%>
                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Qualification"), True) %>--%>
                                                    <asp:Label ID="objlblQualification" runat="server" Text='<%# Eval("Qualification") %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" id="divExp" runat="server">
                                                <%-- style="padding-left: 15px; padding-top: 15px"--%>
                                                <div class="col-md-2" >
                                                    <asp:Label ID="lblExp" runat="server" Text="Experience :" CssClass="font-weight-bold"></asp:Label>
                                                    <%--<%# IIf(Eval("experience") <> 0, Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Format(CDec(Eval("experience")) / 12, "###0.0#")) + " Year(s)", Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment("Fresher Can Apply")) %>--%>
                                                    <%--<%# IIf(Eval("experience") <> 0, AntiXss.AntiXssEncoder.HtmlEncode(Format(CDec(Eval("experience")) / 12, "###0.0#"), True) + " Year(s)", AntiXss.AntiXssEncoder.HtmlEncode("Fresher Can Apply", True)) %>--%>                                            
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="objlblExp" runat="server" Text='<%# IIf(Eval("experience") <> 0, AntiXss.AntiXssEncoder.HtmlEncode(Format(CDec(Eval("experience")) / 12, "###0.0#"), True) + " Year(s)", AntiXss.AntiXssEncoder.HtmlEncode("Fresher Can Apply", True)) %>'></asp:Label>
                                                    <%--<br />--%>
                                                    <asp:Label ID="objlblExpCmt" runat="server" Text='<%# Eval("experience_comment")%>'></asp:Label>                                        
                                                </div>
                                                <div class="col-md-6 d-none" id="divNoPosition" runat="server">
                                                    <asp:Label ID="lblNoPosition" runat="server" Text="No. of Position :" CssClass="font-weight-bold"></asp:Label>
                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("noposition")) %>--%>
                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("noposition"), True) %>--%>
                                                    <asp:Label ID="objlblNoPosition" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("noposition"), True) %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" id="divLang" runat="server">
                                                <%--style="padding-left: 15px"--%>
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblLang" runat="server" Text="Preferred Language Skill :" CssClass="font-weight-bold"></asp:Label>                                            
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="objlblLang" runat="server" Text='<%# Eval("Lang") %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" id="divScale" runat="server">
                                                <%--style="padding-left: 15px"--%>
                                                <div class="col-md-12">
                                                    <asp:Label ID="lblScale" runat="server" Text="Scale :" CssClass="font-weight-bold"></asp:Label>
                                                    <asp:Label ID="objlblScale" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <%-- style="padding-left: 15px"--%>
                                                <div class="col-md-6" id="divOpenningDate" runat="server">
                                                    <asp:Label ID="lblOpeningDate" runat="server" Text="Job Opening date :" CssClass="font-weight-bold"></asp:Label>
                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(CDate(Eval("openingdate")).ToShortDateString()) %>--%>
                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(CDate(Eval("openingdate")).ToShortDateString(), True) %>--%>
                                                    <asp:Label ID="objlblOpeningDate" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(CDate(Eval("openingdate")).ToShortDateString(), True) %>'></asp:Label>
                                                </div>
                                                <div class="col-md-6" id="divClosuingDate" runat="server">
                                                    <asp:Label ID="lblClosingDate" runat="server" Text="Job closing date :" CssClass="font-weight-bold"></asp:Label>
                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(CDate(Eval("closingdate")).ToShortDateString()) %>--%>
                                                    <%--<%# AntiXss.AntiXssEncoder.HtmlEncode(CDate(Eval("closingdate")).ToShortDateString(), True) %>--%>
                                                    <asp:Label ID="objlblClosingDate" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(CDate(Eval("closingdate")).ToShortDateString(), True) %>'></asp:Label>
                                                </div>
                                            </div>

                                                    <%--'S.SANDEEP |01-JUN-2023| -- START--%>
                                                    <%--'ISSUE/ENHANCEMENT : A1X-960--%>
                                                    <asp:Panel ID="pnlAttachMsg" runat="server">
                                                        <asp:Label ID="lblUpload" runat="server" Font-Bold="true" Text="Select file (Image / PDF) (All file size should not exceed more than 5 MB.)" CssClass="d-block">                                                    
                                                        </asp:Label>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlCV" runat="server" CssClass="form-group row attachdoc">
                                                        <div class="col-md-3">
                                                            <asp:Label ID="lblDocTypeCV" runat="server" Text="Document Type" CssClass="required"></asp:Label>
                                                            <asp:DropDownList ID="ddlDocTypeCV" runat="server" AutoPostBack="true" CssClass="selectpicker form-control" OnSelectedIndexChanged="ddlDocTypeCV_OnSelectedIndexChanged"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <asp:Label ID="lblAttachTypeCV" runat="server" Text="Attachment Type" CssClass="required"></asp:Label>
                                                            <asp:DropDownList ID="ddlAttachTypeCV" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>
                                                            <asp:Label ID="lblAttachTypeCVMsg" runat="server" CssClass="d-none" Text="Please select Attachment Type."></asp:Label>
                                                        </div>
                                                        <div class="col-md-2 mt-3">
                                                            <uc9:FileUpload ID="flUploadCV" runat="server" MaxSizeKB="5242880" OnbtnUpload_Click="flUpload_btnUpload_Click" />
                                                        </div>
                                                        <div class="col-md-4 mt-2">
                                                            <asp:Panel ID="pnlFileCV" runat="server" CssClass="card rounded">
                                                                <div class="row no-gutters m-0">
                                                                    <div class="col-md-3 text-center pt-3 fileext">
                                                                        <asp:Label ID="lblFileExtCV" runat="server" Text="PDF"></asp:Label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        <div class="card-body py-1 pr-2">

                                                                            <asp:LinkButton ID="btncvDocDel"
                                                                                runat="server"
                                                                                OnClientClick='if(!ShowConfirm("Are you sure you want to Delete attached document for this applied job?", this)) return false; else return true;'
                                                                                CommandName="DelDocCV" CssClass="sthf text-decoration-none" Text="">
                                                                                 <button type="button" class="close pr-1" aria-label="Close"><span aria-hidden="true">&times</span></button>
                                                                            </asp:LinkButton>


                                                                            <h6 class="card-title m-0">
                                                                                <asp:Label ID="lblFileNameCV" runat="server" Text=""></asp:Label></h6>
                                                                            <p class="card-text">
                                                                                <small class="text-muted">
                                                                                    <asp:Label ID="lblDocSizeCV" runat="server" Text="0.0 MB"></asp:Label></small>
                                                                            </p>
                                                                            <asp:HiddenField ID="hfDocSizeCV" runat="server" Value="" />
                                                                            <asp:HiddenField ID="hfFileNameCV" runat="server" Value="" />
                                                                            <asp:HiddenField ID="hftranidCV" runat="server" Value="0" />
                                                                            <asp:HiddenField ID="hfOldFileNameCV" runat="server" Value="" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlCoverLetter" runat="server" CssClass="form-group row attachdoc">
                                                        <div class="col-md-3">
                                                            <asp:Label ID="lblDocTypeCoverLetter" runat="server" Text="Document Type" CssClass="required"></asp:Label>
                                                            <asp:DropDownList ID="ddlDocTypeCoverLetter" runat="server" AutoPostBack="true" CssClass="selectpicker form-control" OnSelectedIndexChanged="ddlDocTypeCoverLetter_OnSelectedIndexChanged"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <asp:Label ID="lblAttachTypeCoverLetter" runat="server" Text="Attachment Type" CssClass="required"></asp:Label>
                                                            <asp:DropDownList ID="ddlAttachTypeCoverLetter" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-md-2 mt-3">
                                                            <uc9:FileUpload ID="flUploadCoverLetter" runat="server" MaxSizeKB="5242880" OnbtnUpload_Click="flUpload_btnUpload_Click" />
                                                        </div>
                                                        <div class="col-md-4 mt-2">
                                                            <asp:Panel ID="pnlFileCoverLetter" runat="server" CssClass="card rounded">
                                                                <div class="row no-gutters m-0">
                                                                    <div class="col-md-3 text-center pt-3 fileext">
                                                                        <asp:Label ID="lblFileExtCoverLetter" runat="server" Text="PDF"></asp:Label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        <div class="card-body py-1 pr-2">
                                                                            <asp:LinkButton ID="btnclDocDel"
                                                                                runat="server"
                                                                                OnClientClick='if(!ShowConfirm("Are you sure you want to Delete attached document for this applied job?", this)) return false; else return true;'
                                                                                CommandName="DelDocCovLettr" CssClass="sthf text-decoration-none" Text="">
                                                                                 <button type="button" class="close pr-1" aria-label="Close"><span aria-hidden="true">&times</span></button>
                                                                            </asp:LinkButton>

                                                                            <h6 class="card-title m-0">
                                                                                <asp:Label ID="lblFileNameCoverLetter" runat="server" Text=""></asp:Label></h6>
                                                                            <p class="card-text">
                                                                                <small class="text-muted">
                                                                                    <asp:Label ID="lblDocSizeCoverLetter" runat="server" Text="0.0 MB"></asp:Label></small>
                                                                            </p>
                                                                            <asp:HiddenField ID="hfDocSizeCoverLetter" runat="server" Value="" />
                                                                            <asp:HiddenField ID="hfFileNameCoverLetter" runat="server" Value="" />
                                                                            <asp:HiddenField ID="hftranidCoverLetter" runat="server" Value="0" />
                                                                            <asp:HiddenField ID="hfOldFileNameCoverLetter" runat="server" Value="" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </asp:Panel>
                                                    <% If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString() <> "TRA" Then %>
                                                    <asp:Panel ID="pnlQuali" runat="server" CssClass="form-group row attachdoc">
                                                        <div class="col-md-3">
                                                            <asp:Label ID="lblDocTypeQuali" runat="server" Text="Document Type" CssClass="required"></asp:Label>
                                                            <asp:DropDownList ID="ddlDocTypeQuali" runat="server" AutoPostBack="true" CssClass="selectpicker form-control" OnSelectedIndexChanged="ddlDocTypeQuali_OnSelectedIndexChanged"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <asp:Label ID="lblAttachTypeQuali" runat="server" Text="Attachment Type" CssClass="required"></asp:Label>
                                                            <asp:DropDownList ID="ddlAttachTypeQuali" runat="server" AutoPostBack="false" CssClass="selectpicker form-control"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-md-2 mt-3">
                                                            <uc9:FileUpload ID="flUploadQuali" runat="server" MaxSizeKB="5242880" OnbtnUpload_Click="flUpload_btnUpload_Click" />
                                                        </div>
                                                        <div class="col-md-4 mt-2">
                                                            <asp:Panel ID="pnlFileQuali" runat="server" CssClass="card rounded">
                                                                <div class="row no-gutters m-0">
                                                                    <div class="col-md-3 text-center pt-3 fileext">
                                                                        <asp:Label ID="lblFileExtQuali" runat="server" Text="PDF"></asp:Label>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        <div class="card-body py-1 pr-2">
                                                                            <asp:LinkButton ID="btnQualiDocDel"
                                                                                runat="server"
                                                                                OnClientClick='if(!ShowConfirm("Are you sure you want to Delete attached document for this applied job?", this)) return false; else return true;'
                                                                                CommandName="DelDocQuali" CssClass="sthf text-decoration-none" Text="">
                                                                                <button type="button" class="close pr-1" aria-label="Close"><span aria-hidden="true">&times</span></button>
                                                                            </asp:LinkButton>

                                                                            <h6 class="card-title m-0">
                                                                                <asp:Label ID="lblFileNameQuali" runat="server" Text=""></asp:Label></h6>
                                                                            <p class="card-text">
                                                                                <small class="text-muted">
                                                                                    <asp:Label ID="lblDocSizeQuali" runat="server" Text="0.0 MB"></asp:Label></small>
                                                                            </p>
                                                                            <asp:HiddenField ID="hfDocSizeQuali" runat="server" Value="" />
                                                                            <asp:HiddenField ID="hfFileNameQuali" runat="server" Value="" />
                                                                            <asp:HiddenField ID="hftranidQuali" runat="server" Value="0" />
                                                                            <asp:HiddenField ID="hfOldFileNameQuali" runat="server" Value="" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </asp:Panel>
                                                    <% End If%>
                                                    <%--'S.SANDEEP |01-JUN-2023| -- END--%>
                                        </div>

                                                <%--<div class="card-footer  bg-light d-none">--%>
                                                <div class="card-footer  bg-light">
                                                    <%--'S.SANDEEP |01-JUN-2023| -- START {d-none} END--%>
                                            <div class="form-group row mb-1">
                                                <div class="col-md-12 text-right">
                                                    <div>
                                                        <%-- Hemant (08 Sept 2018) -- Start
                                                            Enhancement - Issue : To Stop Importing Deleted Applied Job Vancancy From HRMS Database Server to Local Database Server  --%>

                                                        <%-- <asp:LinkButton ID="btnApply" runat="server" OnClientClick='if(!ShowConfirm("Are you sure you want to Delete Applied job?", this)) return false; else return true;' CommandName="Delete" Font-Underline="false" CssClass="btn btn-primary" Text="">
                                                        <div id="divApplied" runat="server" class="glyphicon glyphicon-trash" style="margin-right: 5px"></div>
                                                        <asp:Label ID="lblApplyText" runat="server" Text="Delete"></asp:Label>
                                                    </asp:LinkButton>--%>
                                                        <asp:LinkButton ID="btnApply" runat="server" Visible="False" CommandName="Delete" Font-Underline="false" CssClass="btn btn-primary" Text="">
                                                            <div id="divApplied" runat="server" class="fa fa-trash mr-1"></div>
                                                            <asp:Label ID="lblApplyText" runat="server" Text="Delete" Visible="False"></asp:Label>
                                                        </asp:LinkButton>
                                                        <%-- Hemant (08 Sept 2018) --End --%>

                                                                <%--'S.SANDEEP |01-JUN-2023| -- START--%>
                                                                <%--'ISSUE/ENHANCEMENT : A1X-960--%>
                                                                <%--<asp:LinkButton ID="" runat="server" Font-Underline="false" CssClass="btn btn-primary" Text="">
                                                                    <asp:Label ID="" runat="server" Text="Are you sure you want to update the changes done?" Visible="False"></asp:Label>
                                                                </asp:LinkButton>--%>
                                                                <asp:LinkButton ID="btnUpdate" runat="server" CommandName="Update" CssClass="btn btn-primary sthf text-decoration-none" Text="">
                                                                    <div id="div1" runat="server" class="fa fa-check mr-1" visible="false"></div>
                                                                    <asp:Label ID="lblUpdateText" runat="server" Text="Update Changes"></asp:Label>
                                                                </asp:LinkButton>
                                                                <%--'S.SANDEEP |01-JUN-2023| -- END--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </div>
           
                    <asp:ObjectDataSource ID="odsVacancy" runat="server" SelectMethod="GetApplicantJobApplied" TypeName="Aruti_Online_Recruitment.clsApplicantApplyJob" EnablePaging="false">

                        <SelectParameters>
                            <asp:Parameter Name="strCompCode" Type="String" DefaultValue="" />
                            <asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />
                            <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>

            <asp:Label ID="lblPositionMsg" runat="server" Text="Position(s)" CssClass="d-none"></asp:Label>
            <%--'S.SANDEEP |01-JUN-2023| -- START--%>
            <%--'ISSUE/ENHANCEMENT : A1X-960--%>
            <asp:Label ID="lblDocTypeMsg" runat="server" CssClass="d-none" Text="Please select Document Type."></asp:Label>
            <asp:Label ID="lblAttachTypeMsg" runat="server" CssClass="d-none" Text="Please select Attachment Type."></asp:Label>
            <asp:Label ID="lblAttachImageMsg" runat="server" CssClass="d-none" Text="Please select proper Image file or PDF file."></asp:Label>
            <asp:Label ID="lblAttachSizeMsg" runat="server" CssClass="d-none" Text="Sorry,You cannot upload file greater than #maxsize# MB."></asp:Label>
            <asp:Label ID="lblAttachMsg" runat="server" CssClass="d-none" Text="Sorry, #attachmenttype# attachment is mandatory."></asp:Label>
            <asp:Label ID="lblUpdateMsg" runat="server" CssClass="d-none" Text="Changes are updated successfully."></asp:Label>
            <%--'S.SANDEEP |01-JUN-2023| -- END--%>
        </asp:Panel>
    </div>
    <asp:HiddenField ID="hfCancelLang" runat="server" />
    <cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsSearchJob" _ModuleName="SearchJob" _TargetControl="pnlForm" CancelControlName="hfCancelLang" />

</asp:Content>
