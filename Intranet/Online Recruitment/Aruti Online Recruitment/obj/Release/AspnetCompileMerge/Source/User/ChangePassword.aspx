﻿<%@ Page Title="Change Password" Language="vb" AutoEventWireup="false" CodeBehind="ChangePassword.aspx.vb" Inherits="Aruti_Online_Recruitment.ChangePassword" MasterPageFile="~/User/Site4.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="card  mx-auto MaxWidth1422">
        <%--style="max-width: 422px; margin-left: auto; margin-right: auto;"--%>

			<script type="text/javascript">

					
				$(document).ready(function () {
					$$('ChangePasswordPushButton').on('click', function () {
						return IsValidate();
					});
				});

				var prm = Sys.WebForms.PageRequestManager.getInstance();
				if (prm != null) {

					$$('ChangePasswordPushButton').on('click', function () {
						return IsValidate();
					});
				}

				function IsValidate() {

					var strEmailValidExpression = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/g
					var strEmail = $('#UserName').val();

					if ($$('UserName').val().trim() == '') {
                    ShowMessage($$('lblFirstNameMsg').text(), 'MessageType.Errorr');
						$$('UserName').focus();
						return false;
					}
					else if ($$('CurrentPassword').val().trim() == '') {
                    ShowMessage($$('lblCurrentPasswordMsg').text(), 'MessageType.Errorr');
						$$('CurrentPassword').focus();
						return false;
					}
					else if ($$('NewPassword').val().trim() == '') {
                    ShowMessage($$('lblNewPasswordMsg').text(), 'MessageType.Errorr');
						$$('NewPassword').focus();
						return false;
					}
					else if ($$('ConfirmNewPassword').val().trim() == '') {
                    ShowMessage($$('lblConfirmNewPasswordMsg').text(), 'MessageType.Errorr');
						$$('ConfirmNewPassword').focus();
						return false;
					}
					else if ($$('NewPassword').val().trim() != $$('ConfirmNewPassword').val().trim()) {
                    ShowMessage($$('lblConfirmNewPasswordMsg2').text(), 'MessageType.Errorr');
						$$('NewPassword').focus();
						return false;
					}
					else if (!strEmailValidExpression.test(strEmail)) {
                    ShowMessage($$('lblEmailValidMsg').text(), 'MessageType.Errorr');
						return false;
					}

				}

		    </script>


        <div class="card-header">
            <h4>Change Password</h4>
        </div>

        <asp:ChangePassword ID="ChangePassword1" runat="server" PasswordLabelText="Old Password:" SuccessPageUrl="~/Login.aspx" DisplayUserName="True" CssClass="w-100" CancelDestinationPageUrl="~/User/UserHome.aspx">
            <%-- Style="width: 100%;"--%>
            <ChangePasswordTemplate>
                <div class="card-body">
                    <%--<table cellpadding="1" cellspacing="0" style="border-collapse: collapse;">
                        <tr>
                            <td>
                                <table cellpadding="0">
                                    <tr>
                                        <td align="center" colspan="2">Change Your Password</td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Old Password:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">New Password:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword" ErrorMessage="New Password is required." ToolTip="New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword" ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry." ValidationGroup="ChangePassword1"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2" style="color: Red;">
                                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword" Text="Change Password" ValidationGroup="ChangePassword1" />
                                        </td>
                                        <td>
                                            <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>--%>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                            <asp:TextBox ID="UserName" runat="server" CssClass="form-control" ClientIDMode="Static" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="Email is required." ToolTip="Email is required." ValidationGroup="ChangePassword1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                          <%--  <asp:RegularExpressionValidator ID="EmailValidator" runat="server" ControlToValidate="UserName"
                                ErrorMessage="Email address is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" SetFocusOnError="True" ValidationGroup="ChangePassword1" Display="None"></asp:RegularExpressionValidator>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Old Password:</asp:Label>
                            <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="ChangePassword1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">New Password:</asp:Label>
                            <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword" ErrorMessage="New Password is required." ToolTip="New Password is required." ValidationGroup="ChangePassword1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label>
                            <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword" ErrorMessage="Confirm New Password is required." ToolTip="Confirm New Password is required." ValidationGroup="ChangePassword1" Display="None" SetFocusOnError="True">*</asp:RequiredFieldValidator>--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <%--<asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" Display="None" ErrorMessage="The Confirm New Password must match the New Password entry." ValidationGroup="ChangePassword1"></asp:CompareValidator>--%>
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12" style="text-align: center;">
                            <asp:Literal ID="Literal1" runat="server" EnableViewState="False"></asp:Literal>
                           <%-- <asp:ValidationSummary
                                HeaderText="You must enter a value in the following fields:"
                                DisplayMode="BulletList"
                                EnableClientScript="true"
                                runat="server" ValidationGroup="ChangePassword1" Style="color: red" />--%>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword" Text="Change Password" ValidationGroup="ChangePassword1" CssClass="btn btn-primary width100" />
                            <%--Style="width: 100%;"--%>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" CssClass="btn btn-primary width100" />
                            <%--Style="width: 100%;"--%>
                        </div>
                    </div>
                </div>

            </ChangePasswordTemplate>
        </asp:ChangePassword>
        <asp:Label ID="lblUserNameMsg" runat="server" Text="Please Enter Email." CssClass="d-none"></asp:Label>
        <asp:Label ID="lblUserNameMsg2" runat="server" Text="Sorry, You cannot change password of different email." CssClass="d-none"></asp:Label>
        <asp:Label ID="lblEmailValidMsg" runat="server" Text="Sorry, Email address is not valid." CssClass="d-none"></asp:Label>
        <asp:Label ID="lblCurrentPasswordMsg" runat="server" Text="Please Enter Password." CssClass="d-none"></asp:Label>
        <asp:Label ID="lblNewPasswordMsg" runat="server" Text="Please Enter New Password." CssClass="d-none"></asp:Label>
        <asp:Label ID="lblConfirmNewPasswordMsg" runat="server" Text="Please Enter Confirm Password." CssClass="d-none"></asp:Label>
        <asp:Label ID="lblConfirmNewPasswordMsg2" runat="server" Text="The Confirm New Password must match the New Password entry" CssClass="d-none"></asp:Label>
    </div>
</asp:Content>

