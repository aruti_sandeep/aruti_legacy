﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ActivateUsers.aspx.vb" Inherits="Aruti_Online_Recruitment.ActivateUsers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Activate User</title>
    <link rel="stylesheet" href="Content/bootstrap.css" />
    <link rel="stylesheet" href="Content/bootstrap-dialog.css" />
    <link rel="stylesheet" href="Content/style.css?version=1" />
</head>
<body>
    <form id="form1" runat="server">
        <%= System.Web.Helpers.AntiForgery.GetHtml()  %>

        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnableScriptGlobalization="True">
        </asp:ScriptManager>

        <div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Label ID="lblMsg" runat="server" CssClass="text-dark" ></asp:Label> <%--ForeColor="Black"--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
