﻿function pageLoad(sender, args) {

    $('.selectpicker').selectpicker({
        liveSearch: true,
        maxOptions: 1
    });
}

$(document).ready(function () {

    $$('btnRegister').on('click', function () {
        //S.SANDEEP |04-MAY-2023| -- START
        /*return IsValidate();*/
        return IsValidate($$('hdfCompCode').val().trim().toLowerCase());
        //S.SANDEEP |04-MAY-2023| -- END
    });

    //S.SANDEEP |04-MAY-2023| -- START
    $$('txtNidaOtpNumber').on('keyup', function () {
        if ($$('txtNidaOtpNumber').val().trim().length > 0) {
            $$('btnValidateOtp').prop("disabled", false);
        }
        else {
            $$('btnValidateOtp').prop("disabled", true);
        }
    });

    $$('txtNidaQueryAnswer').on('keyup', function () {
        if ($$('txtNidaQueryAnswer').val().trim().length > 0) {
            $$('btnVerify').prop("disabled", false);
        }
        else {
            $$('btnVerify').prop("disabled", true);
        }
    });
    //S.SANDEEP |04-MAY-2023| -- END

});

var prm = Sys.WebForms.PageRequestManager.getInstance();
if (prm != null) {
    prm.add_endRequest(function (sender, e) {

        $$('btnRegister').on('click', function () {
            //S.SANDEEP |04-MAY-2023| -- START
            /*return IsValidate();*/
            return IsValidate($$('hdfCompCode').val().trim().toLowerCase());
            //S.SANDEEP |04-MAY-2023| -- END
        });
        
        //S.SANDEEP |04-MAY-2023| -- START
        $$('txtNidaOtpNumber').on('keyup', function () {
            if ($$('txtNidaOtpNumber').val().trim().length > 0) {
                $$('btnValidateOtp').prop("disabled", false);
            }
            else {
                $$('btnValidateOtp').prop("disabled", true);
            }
        });

        $$('txtNidaQueryAnswer').on('keyup', function () {
            if ($$('txtNidaQueryAnswer').val().trim().length > 0) {
                $$('btnVerify').prop("disabled", false);
            }
            else {
                $$('btnVerify').prop("disabled", true);
            }
        });
         //S.SANDEEP |04-MAY-2023| -- END
    });
};

//S.SANDEEP |04-MAY-2023| -- START
//ISSUE/ENHANCEMENT : A1X-884
/*
function IsValidate() {
        
    if ($$('txtFirstName').val().trim() == '') {
        ShowMessage('Please Enter First Name.', 'MessageType.Info');
        $$('txtFirstName').focus();
        return false;
    }
    else if ($$('txtLastName').val().trim() == '') {
        ShowMessage('Please Enter Surname.', 'MessageType.Info');
        $$('txtLastName').focus();
        return false;
    }
    else if ($$('UserName').val().trim() == '') {
        ShowMessage('Please Enter Email.', 'MessageType.Info');
        $$('UserName').focus();
        return false;
    }
    else if (IsValidEmail($$('UserName').val()) == false) {
        ShowMessage('Please enter Valid Email.', 'MessageType.Info');
        $$('UserName').focus();
        return false;
    }
    else if ($$('drpGender').val() <= 0) {
        ShowMessage('Please Select Gender.', 'MessageType.Info');
        $$('drpGender').focus();
        return false;
    }
    //else if (isValidDate($("[id*=dtBirthdate]").val()) == false) {
    //    ShowMessage('Please enter Valid Birth Date.', 'MessageType.Info');
    //    $("[id*=dtBirthdate]").focus();
    //    return false;
    //}
    //else if ($("[id*=dtBirthdate]").val() >= new Date().format('dd-MMM-yyyy') ) {
    //    ShowMessage('Birth Date should be less than current Date.', 'MessageType.Info');
    //    $("[id*=dtBirthdate]").focus();
    //    return false;
    //}
    else if ($$('txtMobileNo').val().trim() == '') {
        ShowMessage('Please Enter Mobile No..', 'MessageType.Info');
        $$('txtMobileNo').focus();
        return false;
    }
    else if ($$('Password').val().trim() == '') {
        ShowMessage('Please enter Password.', 'MessageType.Info');
        $$('Password').focus();
        return false;
    }
    else if ($$('ConfirmPassword').val() == '') {
        ShowMessage('Please enter Confirm Password.', 'MessageType.Info');
        $$('ConfirmPassword').focus();
        return false;
    }
    else if ($$('Password').val() != $$('ConfirmPassword').val()) {
        ShowMessage('The Password and Confirmation Password must match.', 'MessageType.Info');
        $$('ConfirmPassword').focus();
        return false;
    }
    else if ($$('Question').val().trim() == '') {
        ShowMessage('Security question is required.', 'MessageType.Info');
        $$('Question').focus();
        return false;
    }
    else if ($$('Answer').val().trim() == '') {
        ShowMessage('Security answer is required.', 'MessageType.Info');
        $$('Answer').focus();
        return false;
    }

    return true;
}
*/
function IsValidate(comp_code) {

    if (comp_code === 'tra') {
        if ($$('txtNinNumber').val().trim() == '') {
            ShowMessage('Please Enter National ID Number.', 'MessageType.Info');
            $$('txtNinNumber').focus();
            return false;
        }
        else if ($$('txtNINMobile').val().trim() == '') {
            ShowMessage('Please Enter Mobile Registered With Nida.', 'MessageType.Info');
            $$('txtNINMobile').focus();
            return false;
        }
    }
    else {
        if ($$('txtFirstName').val().trim() == '') {
            ShowMessage('Please Enter First Name.', 'MessageType.Info');
            $$('txtFirstName').focus();
            return false;
        }
        else if ($$('txtLastName').val().trim() == '') {
            ShowMessage('Please Enter Surname.', 'MessageType.Info');
            $$('txtLastName').focus();
            return false;
        }
        else if ($$('drpGender').val() <= 0) {
            ShowMessage('Please Select Gender.', 'MessageType.Info');
            $$('drpGender').focus();
            return false;
        }
        else if ($$('txtMobileNo').val().trim() == '') {
            ShowMessage('Please Enter Mobile No..', 'MessageType.Info');
            $$('txtMobileNo').focus();
            return false;
        }
        else if ($$('Question').val().trim() == '') {
            ShowMessage('Security question is required.', 'MessageType.Info');
            $$('Question').focus();
            return false;
        }
        else if ($$('Answer').val().trim() == '') {
            ShowMessage('Security answer is required.', 'MessageType.Info');
            $$('Answer').focus();
            return false;
        }
    }

    if ($$('UserName').val().trim() == '') {
        ShowMessage('Please Enter Email.', 'MessageType.Info');
        $$('UserName').focus();
        return false;
    }
    else if (IsValidEmail($$('UserName').val()) == false) {
        ShowMessage('Please enter Valid Email.', 'MessageType.Info');
        $$('UserName').focus();
        return false;
    }

    else if ($$('Password').val().trim() == '') {
        ShowMessage('Please enter Password.', 'MessageType.Info');
        $$('Password').focus();
        return false;
    }
    else if ($$('ConfirmPassword').val() == '') {
        ShowMessage('Please enter Confirm Password.', 'MessageType.Info');
        $$('ConfirmPassword').focus();
        return false;
    }
    else if ($$('Password').val() != $$('ConfirmPassword').val()) {
        ShowMessage('The Password and Confirmation Password must match.', 'MessageType.Info');
        $$('ConfirmPassword').focus();
        return false;
    }

    return true;
}

//S.SANDEEP |04-MAY-2023| -- END

function IsValidEmail(strEmail) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(strEmail)) {
        return false;
    } else {
        return true;
    }
}

function isValidDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return false;

    var rxDatePattern = /^(\d{1,2})(\/|-)(\w{1,3})(\/|-)(\d{4})$/; 
    var dtArray = currVal.match(rxDatePattern); // is format OK?

    if (dtArray == null)
        return false;

    var months = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
    dtDay= dtArray[1];
    dtMonth = dtArray[3];
    dtMonth = months.indexOf(dtMonth.toLowerCase()) + 1;
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}

