﻿<%@ Page Title="Reset Password Link" Language="vb" AutoEventWireup="false" MasterPageFile="~/Admin/Site3.Master" CodeBehind="ASendResetPwd.aspx.vb" Inherits="Aruti_Online_Recruitment.ASendResetPwd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" href="ASendResetPwd.css" />

    <script type="text/javascript">

        function pageLoad(sender, args) {

            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

       
    </script>


    <div class="card">

        <div class="card-header">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <h4>Send Reset Password Link
                    <asp:Label ID="lblCount" runat="server" Text=""></asp:Label>
                    </h4>
                </ContentTemplate>
                <Triggers>
                    <%--<asp:AsyncPostBackTrigger ControlID="btnSendLink" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />--%>
                    <asp:AsyncPostBackTrigger ControlID="grdLink" EventName="DataBound" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <div class="card-body">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="form-group row d-flex align-items-end">

                        <div class="col-md-4">
                            <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                            <asp:TextBox ID="txtEmail" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div class="col-md-4">
                        </div>

                        <div class="col-md-4">
                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary btn-block"  Text="Search" ValidationGroup="Search" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnl_link" runat="server" CssClass="table-responsive overflow-auto minheight300">
                    <asp:GridView ID="grdLink" runat="server" AutoGenerateColumns="False" CssClass="table" ShowHeaderWhenEmpty="true"
                        AllowPaging="true" PageSize="15" DataSourceID="dsLink" DataKeyNames="resetunkid, Comp_Code, companyunkid, LoweredEmail, subject, message, UserId">

                        <Columns>
                            <asp:BoundField DataField="Comp_Code" HeaderText="Code" />
                            <asp:BoundField DataField="companyunkid" HeaderText="ID" />
                            <%--<asp:BoundField DataField="subject" HeaderText="subject" />
                            <asp:BoundField DataField="message" HeaderText="message" />--%>
                            <asp:BoundField DataField="LoweredEmail" HeaderText="Email" />
                            <asp:BoundField DataField="CreateDate" HeaderText="Create Date" />
                            <asp:BoundField DataField="LastLoginDate" HeaderText="Last Login Date" />
                            <asp:BoundField DataField="resetexpiredate" HeaderText="Link Expiry Date" />
                            <asp:TemplateField HeaderText="Is Approved" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%--<asp:CheckBox ID="objchkIsApproved" runat="server" Text="" Checked='<%# CBool(Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("IsApproved"))) %>' Enabled="false" />--%>
                                    <asp:CheckBox ID="objchkIsApproved" runat="server" Text="" Checked='<%# CBool(AntiXss.AntiXssEncoder.HtmlEncode(Eval("IsApproved"), True)) %>' Enabled="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Send Link">
                                <ItemTemplate>
                                    <asp:LinkButton ID="objlnkSend" Text="Send" runat="server" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Send" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <asp:ObjectDataSource ID="dsLink" runat="server" SelectMethod="GetResetPasswordLink" TypeName="Aruti_Online_Recruitment.clsApplicant" EnablePaging="true"
                        MaximumRowsParameterName="intPageSize" StartRowIndexParameterName="startRowIndex" SelectCountMethod="GetResetPasswordLinkTotal">
                        <SelectParameters>
                            <asp:SessionParameter Name="strCompCode" SessionField="CompCode" Type="String" />
                            <asp:SessionParameter Name="intComUnkID" SessionField="Companyunkid" Type="Int32" />
                            <asp:ControlParameter ControlID="txtEmail" DefaultValue="" Name="strEmail" PropertyName="Text" Type="String" />
                            <asp:Parameter Name="startRowIndex" Type="Int32" />
                            <asp:Parameter Name="intPageSize" Type="Int32" />
                            <asp:Parameter Name="strSortExpression" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
