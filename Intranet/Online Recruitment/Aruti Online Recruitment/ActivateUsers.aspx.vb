﻿Option Strict On

Imports System.Data.SqlClient

Public Class ActivateUsers
    Inherits Base_General

    Dim objApplicant As New clsApplicant

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End
            Dim strEmail As String
            Dim strUserID As String
            Dim lnk As String
            Dim ccode As String
            Dim cid As Integer = 0
            Dim i As Integer = 0

            Dim strQ As String = "procActivateUser"

            If (Request.QueryString("UserID") IsNot Nothing) AndAlso (Request.QueryString("Email") IsNot Nothing) AndAlso Request.QueryString("lnk") IsNot Nothing AndAlso Request.QueryString("c") IsNot Nothing AndAlso Request.QueryString("i") IsNot Nothing Then
                strUserID = clsCrypto.Dicrypt(Request.QueryString("UserID"))
                strEmail = Server.UrlDecode(clsCrypto.Dicrypt(Request.QueryString("Email")))
                lnk = clsCrypto.Dicrypt(Request.QueryString("lnk"))
                ccode = Server.UrlDecode(clsCrypto.Dicrypt(Request.QueryString("c")))
                cid = CInt(Server.UrlDecode(clsCrypto.Dicrypt(Request.QueryString("i"))))

                If Membership.GetUser(strEmail).IsApproved = True Then
                    lblMsg.Text = "Sorry, This Email is already activated!"
                    ShowMessage("Sorry, This Email is already activated!")
                    Exit Try
                End If

                'Sohail (20 Dec 2017) -- Start
                'Enhancement - Multilple admin email registration.
                'If Session("admin_email") Is Nothing OrElse strEmail.Trim <> Session("admin_email").ToString.Trim Then
                'Sohail (16 Aug 2019) -- Start
                'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID. (Giving language change option in online recruitment)
                'If Session("admin_email") Is Nothing OrElse (Session("admin_email").ToString.Split(CChar(";")).Contains(strEmail.Trim) = False AndAlso Session("admin_email").ToString.Split(CChar(",")).Contains(strEmail.Trim) = False) Then
                If 1 = 1 Then
                    'Sohail (16 Aug 2019) -- End
                    'Sohail (20 Dec 2017) -- End


                    'Using con As New SqlConnection(strConn)
                    '    con.Open()

                    '    Using cmd As New SqlCommand(strQ, con)
                    '        cmd.CommandTimeout = 0

                    '        Dim profile As UserProfile = UserProfile.GetUserProfile(strEmail)

                    '        cmd.CommandType = CommandType.StoredProcedure
                    '        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = CInt(cid)
                    '        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = ccode
                    '        cmd.Parameters.Add(New SqlParameter("@userid", SqlDbType.UniqueIdentifier)).Value = New Guid(strUserID)
                    '        cmd.Parameters.Add(New SqlParameter("@firstname", SqlDbType.NVarChar)).Value = profile.FirstName
                    '        cmd.Parameters.Add(New SqlParameter("@othername", SqlDbType.NVarChar)).Value = profile.MiddleName
                    '        cmd.Parameters.Add(New SqlParameter("@surname", SqlDbType.NVarChar)).Value = profile.LastName
                    '        cmd.Parameters.Add(New SqlParameter("@Email", SqlDbType.NVarChar)).Value = strEmail
                    '        cmd.Parameters.Add(New SqlParameter("@gender", SqlDbType.Int)).Value = profile.Gender
                    '        cmd.Parameters.Add(New SqlParameter("@birth_date", SqlDbType.DateTime)).Value = profile.BirthDate
                    '        cmd.Parameters.Add(New SqlParameter("@present_mobileno", SqlDbType.NVarChar)).Value = profile.MobileNo
                    '        cmd.Parameters.Add(New SqlParameter("@ret_applicantunkid", SqlDbType.Int)).Value = 0
                    '        cmd.Parameters("@ret_applicantunkid").Direction = ParameterDirection.Output

                    '        Dim intApplicantID As Integer = 0
                    '        intApplicantID = cmd.ExecuteNonQuery

                    '        If CInt(cmd.Parameters("@ret_applicantunkid").Value) > 0 Then
                    '            'Session("applicantunkid") = CInt(cmd.Parameters("@ret_applicantunkid").Value)

                    '            'Response.Write("Thank you for activation. You can login now!")
                    '            'Response.Write("<BR>Click <a href='" & lnk & "'>here</a> to Login.")
                    '            lblMsg.Text = "Thank you for activation. You can login now!" & vbCrLf & "Click <a href='" & lnk & "'>here</a> to Login."
                    '        End If

                    '    End Using
                    'End Using
                    If objApplicant.ActivateUser(strCompCode:=ccode, intComUnkID:=CInt(cid), strEmail:=strEmail, strUserID:=strUserID) > 0 Then
                        lblMsg.Text = "Thank you for activation. You can login now!" & vbCrLf & "Click <a href='" & lnk & "'>here</a> to Login."
                    End If
                Else 'Admin Email Activation

                    Dim usr As MembershipUser = Membership.GetUser(strEmail)
                    usr.IsApproved = True
                    Membership.UpdateUser(usr)

                    'Response.Write("Thank you for activation. You can now login!")
                    'Response.Write("<BR> Click <a href='" & lnk & "'>here</a> to login.")
                    lblMsg.Text = "Thank you for activation. You can now login!" & vbCrLf & "Click <a href='" & lnk & "'>here</a> to login."
                End If

            End If

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

#End Region


End Class