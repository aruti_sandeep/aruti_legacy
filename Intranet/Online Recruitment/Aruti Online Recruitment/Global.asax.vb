﻿'Imports eZeeCommonLib

Public Class Global_asax
    Inherits HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup

        'Schedule task for the first time
        ScheduleTask()
    End Sub

    Shared Sub ScheduleTask()
        ' Cache will expire after one hour
        ' You can change this time interval according
        ' to your requriements
        ' SetTimer function will be called when cache expire
        HttpRuntime.Cache.Add("ScheduledTask", "1", Nothing, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(1), CacheItemPriority.NotRemovable, New CacheItemRemovedCallback(AddressOf SetTimer))
    End Sub

    ' This function si called when cache is expired
    Shared Sub SetTimer(ByVal key As String, ByVal value As Object, ByVal reason As CacheItemRemovedReason)
        ' Call the task function
        DoTask()
        ' Schedule new execution time
        ScheduleTask()
    End Sub

    Shared Sub DoTask()
        Try
            ' Task code which is executed periodically
            ' In this example, code will be executed every hour
            clsSACommon.DeleteExpiredSessions()
        Catch ex As Exception

        End Try
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    'Sohail (20 Nov 2019) -- Start
    'NMB Enhancement : Security issues in Self Service in 75.1
    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        Response.AddHeader("X-Frame-Options", "DENY")
    End Sub
    'Sohail (20 Nov 2019) -- End

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is 

        'Sohail (06 Nov 2019) -- Start
        'NMB Recruitment UAT # 20: Email sent back to employee after application of a vacancy should read as below. And should not have Powered by Aruti Signature.
        If Request.IsSecureConnection = True Then
            'Response.Cookies("ASP.NET_SessionId").Secure = True
        End If
        'Sohail (06 Nov 2019) -- End

        'Sohail (30 Nov 2017) -- Start
        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
        'Session("ConnectionString") = ConfigurationManager.ConnectionStrings("paydb").ToString().Replace("****", clsSecurity.Decrypt("Qw0hgX7HJ0cge3U8zJbx44dZa+ZiNCzTLLNaxe5cIlv5vcoS7F/wiymPNWvVT+YK", "ezee"))
        Session("ConnectionString") = ConfigurationManager.ConnectionStrings("paydb").ToString()
        Session("CommandTimeOut") = 180
        'Sohail (30 Nov 2017) -- End

        'Session("companyunkid") = -1
        Session("CompCode") = ""
        Session("CompName") = ""
        Session("Vacancy") = "" 'Int=Internal / Ext=External
        Session("Candidate") = "" 'Reg=Registered / UnReg=UnRegistered
        Session("IsJobEdit") = False
        Session("IsNewApplicant") = False 'Sohail (02 May 2012)

        '** For editing application
        Session("applicantunkid") = 0
        Session("VacancyUnkId") = 0
        Session("VacancyUnkIdList") = ""
        Session("firstname") = ""
        Session("surname") = ""
        Session("othername") = ""
        Session("email") = ""
        Session("titleunkid") = 0
        Session("gender") = 0
        Session("present_address1") = ""
        Session("present_address2") = ""
        Session("present_countryunkid") = 0
        Session("present_stateunkid") = 0
        Session("present_province") = ""
        Session("present_post_townunkid") = 0
        Session("present_zipcode") = 0
        Session("present_road") = ""
        Session("present_estate") = ""
        Session("present_plotno") = ""
        Session("present_mobileno") = ""
        Session("present_alternateno") = ""
        Session("present_tel_no") = ""
        Session("present_fax") = ""
        Session("perm_address1") = ""
        Session("perm_address2") = ""
        Session("perm_countryunkid") = 0
        Session("perm_stateunkid") = 0
        Session("perm_province") = ""
        Session("perm_post_townunkid") = 0
        Session("perm_zipcode") = 0
        Session("perm_road") = ""
        Session("perm_estate") = ""
        Session("perm_plotno") = ""
        Session("perm_mobileno") = ""
        Session("perm_alternateno") = ""
        Session("perm_tel_no") = ""
        Session("perm_fax") = ""
        Session("birth_date") = ""
        Session("marital_statusunkid") = 0
        Session("anniversary_date") = ""
        Session("language1unkid") = 0
        Session("language2unkid") = 0
        Session("language3unkid") = 0
        Session("language4unkid") = 0
        Session("nationality") = 0
        Session("other_skill") = ""
        Session("other_qualification") = ""
        'Sohail (30 May 2012) -- Start
        'TRA - ENHANCEMENT
        Session("memberships") = ""
        Session("achievements") = ""
        Session("journalsresearchpapers") = ""
        'Sohail (30 May 2012) -- End

        Session("employeecode") = ""
        Session("referenceno") = ""
        Session("applicant_declaration") = ""
        '********

        '**** Email Setting ******
        Session("websiteurlinternal") = ""
        Session("sendername") = ""
        Session("senderaddress") = ""
        Session("reference") = ""
        Session("mailserverip") = ""
        Session("mailserverport") = 0
        Session("username") = ""
        Session("password") = ""
        'Session("password") = "fGDGc7zwpqWQwumWnr/qgg=="
        Session("isloginssl") = False
        Session("mail_body") = ""

        'Nilay (01-July-2015) -- Start
        HttpContext.Current.Session("divAddresses") = True
        HttpContext.Current.Session("divLangSkills") = True
        HttpContext.Current.Session("divQualiDetails") = True
        HttpContext.Current.Session("divJobHistory") = True
        HttpContext.Current.Session("divReferences") = True
        HttpContext.Current.Session("divOtherDetails") = True
        'Nilay (01-July-2015) -- End

    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub

    Sub Session_OnStart(ByVal sender As Object, ByVal e As EventArgs)
        'Dim tracker As SessionTracker = New SessionTracker()
        'Session("Tracker") = tracker

        'Dim dt As Date = DateAndTime.Now.Date

        'Dim strMsg As String = vbCrLf &
        '                       "----------   START : " & DateAndTime.Now.ToString & "  " & vbTab & "    [SessionID : " & HttpContext.Current.Session.SessionID & "] -------------" &
        '                       vbCrLf &
        '                       "UserHostAddress : " + tracker.SessionUserHostAddress &
        '                       vbCrLf &
        '                       "UserAgent : " + tracker.SessionUserAgent &
        '                       vbCrLf &
        '                       "Browser : " + tracker.Browser.Browser + ", Version : " + tracker.Browser.Version & ", Beta : " & tracker.Browser.Beta.ToString & ", OS : " & tracker.Browser.Platform &
        '                       vbCrLf &
        '                       "URL : " + tracker.SessionURL &
        '                       vbCrLf &
        '                       "URL Referer : " + tracker.SessionReferrer &
        '                       vbCrLf &
        '                       "Visits : " + tracker.VisitCount.ToString &
        '                       vbCrLf &
        '                       "Orig. URL : " + tracker.OriginalURL &
        '                       vbCrLf &
        '                       "Orig. URL Referer : " + tracker.OriginalReferrer &
        '                       vbCrLf


    End Sub
    'Sohail (06 Nov 2019) -- Start
    'NMB Recruitment UAT # 20: Email sent back to employee after application of a vacancy should read as below. And should not have Powered by Aruti Signature.
    Protected Sub Application_AcquireRequestState(sender As Object, e As EventArgs)
        Dim _sessionIPAddress As String = String.Empty

        'If HttpContext.Current.Session IsNot Nothing Then
        '    Dim _encryptedString As String = Convert.ToString(Session("encryptedSession"))
        '    Dim _encodeASBytes As Byte() = System.Convert.FromBase64String(_encryptedString)
        '    Dim _decryptedString As String = System.Text.ASCIIEncoding.ASCII.GetString(_encodeASBytes)

        '    Dim _separator() As Char = New Char() {"^"}

        '    If _decryptedString <> String.Empty AndAlso _decryptedString <> "" AndAlso _decryptedString <> Nothing Then
        '        Dim _splitStrings() As String = _decryptedString.Split(_separator)

        '        If _splitStrings.Count > 0 Then

        '            If _splitStrings(2).Count > 0 Then
        '                Dim _userBrowserInfo() As String = _splitStrings(2).Split("~")
        '                If _userBrowserInfo.Count > 0 Then
        '                    _sessionIPAddress = _userBrowserInfo(1)
        '                End If
        '            End If
        '        End If

        '    End If

        '    Dim _currentUserIPAddress As String = ""
        '    If String.IsNullOrEmpty(Request.ServerVariables("HTTP_X_FORWARDED_FOR")) = True Then
        '        _currentUserIPAddress = Request.ServerVariables("REMOTE_ADDR")
        '    Else
        '        _currentUserIPAddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR").Split(",".ToCharArray, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault()
        '    End If

        '    Dim result As System.Net.IPAddress = System.Net.IPAddress.None
        '    If System.Net.IPAddress.TryParse(_currentUserIPAddress, result) = False Then
        '        result = System.Net.IPAddress.None
        '    End If

        '    If _sessionIPAddress <> "" AndAlso _sessionIPAddress <> String.Empty Then

        '        If _sessionIPAddress <> _currentUserIPAddress Then
        '            Session.RemoveAll()
        '            Session.Clear()
        '            Session.Abandon()
        '            Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddSeconds(-30)
        '            Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
        '            'Response.Redirect(Session("mstrUrlArutiLink").ToString)
        '            Dim strUrlArutiLink As String = clsSecurity.Decrypt(Request.Cookies("LogOutPageURL").Value, "ezee")
        '            Response.Redirect(strUrlArutiLink)
        '        Else
        '            'valid user
        '        End If
        '    End If
        'End If
    End Sub
    'Sohail (06 Nov 2019) -- End
    'S.SANDEEP [03-NOV-2016] -- START
    Public Shared Sub CatchException(ByVal exc As Exception, ByVal context As HttpContext, Optional MethodName As String = "", Optional ModuleName As String = "", Optional blnRedirectErrorPage As Boolean = True)
        Try
            'Elmah.ErrorSignal.FromContext(context).Raise(exc)
            'If exc.Message.Contains("Invalid viewstate") = False Then
            Elmah.ErrorSignal.FromContext(context).Raise(exc)
            'End If
            If exc.Message.ToLower.Contains("mail") = True Then

            ElseIf HttpContext.Current.Session Is Nothing Then

            ElseIf HttpContext.Current.Session IsNot Nothing AndAlso HttpContext.Current.Session("email") IsNot Nothing AndAlso HttpContext.Current.Session("email").ToString <> "" Then
                'If blnRedirectErrorPage = True Then HttpContext.Current.Response.Redirect("~/Error.aspx", False) 'TO DO UNCOMMENT
                HttpContext.Current.Session("CustomError") = exc.Message.ToString & "<BR>" & exc.StackTrace.ToString 'TO DO COMMENT
                If blnRedirectErrorPage = True Then HttpContext.Current.Response.Redirect("~/AError.aspx", False) 'TO DO COMMENT
            Else
                'HttpContext.Current.Response.Redirect("~/AError.aspx", False)
                If exc.Message.StartsWith("Login failed for user ") = True Then
                    HttpContext.Current.Session("CustomError") = "Login Failed for User *****."
                    'If blnRedirectErrorPage = True Then HttpContext.Current.Response.Redirect("~/AError.aspx", False)
                    'Exit Try
                ElseIf exc.Message.StartsWith("Cannot open database " & Chr(34) & "arutihrms" & Chr(34) & "") = True Then
                    HttpContext.Current.Session("CustomError") = "Database ***** does not exist."
                    'If blnRedirectErrorPage = True Then HttpContext.Current.Response.Redirect("~/AError.aspx", False)
                    'Exit Try
                Else
                    HttpContext.Current.Session("CustomError") = AntiXss.AntiXssEncoder.HtmlEncode(exc.Message.ToString, True)
                    'If blnRedirectErrorPage = True Then HttpContext.Current.Response.Redirect("~/Error.aspx", False)
                    'Exit Try
                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

    End Sub
    'S.SANDEEP [03-NOV-2016] -- END

End Class