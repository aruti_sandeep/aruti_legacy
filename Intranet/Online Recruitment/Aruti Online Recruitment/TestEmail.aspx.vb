﻿Option Strict On
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Web.Services
Imports Microsoft.Exchange.WebServices.Data
Imports Newtonsoft.Json.Linq

Public Class TestEmail
    Inherits Base_General

    Protected Shared ReCaptcha_Key As String = ConfigurationManager.AppSettings.Item("recaptchaPublicKey").ToString
    Protected Shared ReCaptcha_Secret As String = ConfigurationManager.AppSettings.Item("recaptchaPrivateKey").ToString

#Region " Private Function "

    Private Function IsValidated() As Boolean
        Try

            If txtSenderName.Text.Trim = "" Then
                ShowMessage("Please Enter Sender Name", MessageType.Errorr)
                txtSenderName.Focus()
                Return False
            ElseIf txtSenderAddress.Text.Trim = "" Then
                ShowMessage("Please Enter Sender Email", MessageType.Errorr)
                txtSenderAddress.Focus()
                Return False
            ElseIf txtPassword.Text.Trim = "" Then
                ShowMessage("Please Enter Password", MessageType.Errorr)
                txtPassword.Focus()
                Return False
            ElseIf radSMTP.Checked = True AndAlso txtMailServer.Text.Trim = "" Then
                ShowMessage("Please Enter SMTP Mail Server.", MessageType.Errorr)
                txtMailServer.Focus()
                Return False
            ElseIf radSMTP.Checked = True AndAlso txtMailServerPort.Text.Trim = "" Then
                ShowMessage("Please Enter Mail Server Port.", MessageType.Errorr)
                txtMailServerPort.Focus()
                Return False
            ElseIf radEWS.Checked = True AndAlso txtEWS_URL.Text.Trim = "" Then
                ShowMessage("Please Enter EWS URL.", MessageType.Errorr)
                txtEWS_URL.Focus()
                Return False
            ElseIf radEWS.Checked = True AndAlso txtEWS_Domain.Text.Trim = "" Then
                ShowMessage("Please Enter EWS Domain.", MessageType.Errorr)
                txtEWS_Domain.Focus()
                Return False
            End If

            'If clsApplicant.IsValidEmail(txtSenderAddress.Text) = False Then
            If radSMTP.Checked = True AndAlso clsApplicant.IsValidEmail(txtSenderAddress.Text) = False Then
                ShowMessage("Sender Email address is not valid", MessageType.Errorr)
                txtSenderAddress.Focus()
                Return False
            ElseIf clsApplicant.IsValidEmail(txtSenderName.Text) = False Then
                ShowMessage("Receiver Email address is not valid", MessageType.Errorr)
                txtSenderName.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
            Return False
        End Try
    End Function

    Private Sub EmailType()
        Try
            If radSMTP.Checked = True Then
                mvEmail_Type.ActiveViewIndex = 0
                'lblSenderName.Text = "Sender Name"
                lblSenderAddress.Text = "Sender Email"
                'EmailValidator.ControlToValidate = "txtSenderAddress"
                'txtSenderAddress.ValidationGroup = "TestEmail"
                'EmailValidator.ValidationGroup = "TestEmail"

                txtMailServer.ValidationGroup = "TestEmail"
                txtMailServerPort.ValidationGroup = "TestEmail"

                txtEWS_URL.ValidationGroup = "nothing"
                txtEWS_Domain.ValidationGroup = "nothing"
            ElseIf radEWS.Checked = True Then
                mvEmail_Type.ActiveViewIndex = 1
                'lblSenderName.Text = "User Name"
                lblSenderAddress.Text = "User Name"
                'EmailValidator.ControlToValidate = ""
                'txtSenderAddress.ValidationGroup = "nothing"
                'EmailValidator.ValidationGroup = "nothing"

                txtMailServer.ValidationGroup = "nothing"
                txtMailServerPort.ValidationGroup = "nothing"

                txtEWS_URL.ValidationGroup = "TestEmail"
                txtEWS_Domain.ValidationGroup = "TestEmail"
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    <WebMethod()>
    Public Shared Function VerifyCaptcha(response As String) As String
        Try
            System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
        Catch ex As Exception

        End Try
        Dim url As String = "https://www.google.com/recaptcha/api/siteverify?secret=" & ReCaptcha_Secret & "&response=" & response
        'Return (New WebClient()).DownloadString(url)
        Dim result As Boolean = False
        Dim captchaResponse = HttpContext.Current.Request.Form("g-recaptcha-response")
        Dim request = CType(WebRequest.Create(url), HttpWebRequest)

        Using rs As WebResponse = request.GetResponse
            Using stream As StreamReader = New StreamReader(rs.GetResponseStream())
                Dim jResponse As JObject = JObject.Parse(stream.ReadToEnd())
                Dim isSuccess = jResponse.Value(Of Boolean)("success")
                result = If((isSuccess), True, False)
            End Using
        End Using
        Return result.ToString.ToLower
    End Function

#End Region

#Region " Page Events "
    Private Sub TestEmail_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

#Region " Button Events "

    Private Sub btnTestEmail_Click(sender As Object, e As EventArgs) Handles btnTestEmail.Click
        Dim message As MailMessage
        Dim smtp As SmtpClient
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidated() = False Then Exit Try

            'If Recaptcha1.VerifyIfSolvedAsync IsNot Nothing Then
            '    'Response.Redirect("Welcome.aspx")
            'Else
            '    ShowMessage("Incorrect CAPTCHA response.", MessageType.Errorr)

            '    Exit Try
            'End If

            Dim intPort As Integer
            Integer.TryParse(txtMailServerPort.Text, intPort)

            Dim strUserName As String = txtSenderAddress.Text.Trim
            Dim strReceiverName As String = txtSenderName.Text.Trim

            If radSMTP.Checked = True Then '0 = SMTP, 1 = EWS

                Dim strSenderName As String = txtReference.Text
                If strSenderName.Trim.Length <= 0 Then
                    strSenderName = strUserName
                End If
                'message = New MailMessage(strUserName, strUserName)
                'message = New MailMessage(strUserName, strReceiverName)
                message = New MailMessage(New MailAddress(strUserName, strSenderName), New MailAddress(strReceiverName))

                'message.To.Add(strUserName)
                message.To.Add(strReceiverName)
                message.Subject = "Test Email"
                message.IsBodyHtml = True

                message.Body = "<P>Hi,</P><P>This is testing email from Aruti Online Recruitment. Please do not reply.</P>"

                smtp = New SmtpClient
                smtp.Host = txtMailServer.Text.Trim
                smtp.Port = intPort
                smtp.Credentials = New System.Net.NetworkCredential(strUserName, txtPassword.Text.Trim)
                smtp.EnableSsl = chkIsSSL.Checked
                smtp.Send(message)

            ElseIf radEWS.Checked = True Then '0 = SMTP, 1 = EWS

                'Creating the ExchangeService Object 
                Dim objService As New ExchangeService()

                'Seting Credentials to be used
                'objService.Credentials = New WebCredentials(txtSenderName.Text, txtPassword.Text.Trim, txtEWS_Domain.Text)
                objService.Credentials = New WebCredentials(strUserName, txtPassword.Text.Trim, txtEWS_Domain.Text)

                'Setting the EWS Uri
                objService.Url = New Uri(txtEWS_URL.Text)

                'Creating an Email Service and passing in the service
                Dim objMessage As New EmailMessage(objService)

                objMessage.Subject = "Test Email"
                'objMessage.ToRecipients.Add(strUserName)
                objMessage.ToRecipients.Add(strReceiverName)

                objMessage.Body = "<P>Hi,</P><P>This is testing email from Aruti Online Recruitment. Please do not reply.</P>"

                'objMessage.Send()
                objMessage.SendAndSaveCopy()

            End If


            ShowMessage("Email sent successfully!", "TestEmail.aspx")

            'Response.Redirect("TestEmail.aspx", False)
        Catch ex As Exception
            'ShowMessage(ex.Message.Replace("'", "`") & "\n" & ex.InnerException.Message.Replace("'", "`") & "\n" & ex.InnerException.InnerException.Message.Replace("'", "`"), MessageType.Errorr)
            Dim strMsg As String = ex.Message.Replace("'", "`")
            If ex.InnerException IsNot Nothing Then
                strMsg &= "\n" & ex.InnerException.Message.Replace("'", "`")

                If ex.InnerException.InnerException IsNot Nothing Then
                    strMsg &= "\n" & ex.InnerException.InnerException.Message.Replace("'", "`")
                End If
            End If
            ShowMessage(strMsg, MessageType.Errorr)
        End Try
    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If txtVerificationCode.Text.Trim = "" Then
                ShowMessage("Please Enter Verification Code.", MessageType.Errorr)
                txtVerificationCode.Focus()
                Exit Try
            ElseIf txtVerificationCode.Text <> Session("CaptchaImageText").ToString() Then
                ShowMessage("Please enter valid Verification Code.", MessageType.Errorr)
                txtVerificationCode.Focus()
                Exit Try
            End If

            pnlCaptcha.Visible = False
            pnlEmail.Visible = True
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

#Region " Radio Buttons Events "

    Private Sub radSMTP_CheckedChanged(sender As Object, e As EventArgs) Handles radSMTP.CheckedChanged
        Try
            Call EmailType()

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub radEWS_CheckedChanged(sender As Object, e As EventArgs) Handles radEWS.CheckedChanged
        Try
            Call EmailType()

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

End Class