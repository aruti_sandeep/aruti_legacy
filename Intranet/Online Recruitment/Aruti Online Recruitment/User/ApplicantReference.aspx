﻿<%@ Page Title="Reference" Language="vb" AutoEventWireup="false" MasterPageFile="~/User/Site4.Master" CodeBehind="ApplicantReference.aspx.vb" Inherits="Aruti_Online_Recruitment.ApplicantReference" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/LanguageOpner.ascx" TagPrefix="cnfpopup" TagName="LanguageOpner" %>
<%@ Register Src="~/Controls/LanguageControl.ascx" TagPrefix="cnfpopup" TagName="LanguageControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="ApplicantReference.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <script type="text/javascript">

        function pageLoad(sender, args) {

            $('.selectpicker').selectpicker({
                liveSearch: true,
                maxOptions: 1
            });
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                setEvent();
            });
        }

        $(document).ready(function () {
            setEvent();
        });

        $(document).ready(function () {
           
        });

        function IsValidate() {
            if ($$('txtRefName').val().trim() == '') {
                ShowMessage($$('lblRefNameMsg').text(), 'MessageType.Errorr');
                $$('txtRefName').focus();
                return false;
            }
            else if ($$('txtRefPosition').val().trim() == '' && '<%= Session("ReferencePositionMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblRefPositionMsg').text(), 'MessageType.Errorr');
                $$('txtRefPosition').focus();
                return false;
            }
            else if (parseInt($$('drpReferenceType').val()) <= 0 && '<%= Session("ReferenceTypeMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblReferenceTypeMsg').text(), 'MessageType.Errorr');
                $$('drpReferenceType').focus();
                return false;
            }
            else if ($$('txtRefEmail').val().trim() == '' && '<%= Session("ReferenceEmailMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblRefEmailMsg').text(), 'MessageType.Errorr');
                $$('txtRefEmail').focus();
                return false;
            }
            else if (emailTest($$('txtRefEmail').val())) {
                ShowMessage($$('lblRefEmailMsg2').text(), 'MessageType.Errorr');
                $$('txtRefEmail').focus();
                return false;
            }
            else if ($$('txtRefAddress').val().trim() == '' && '<%= Session("ReferenceAddressMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblRefAddressMsg').text(), 'MessageType.Errorr');
                $$('txtRefAddress').focus();
                return false;
            }
            else if (parseInt($$('ddlRefCountry').val()) <= 0 && '<%= Session("ReferenceCountryMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblRefCountryMsg').text(), 'MessageType.Errorr');
                $$('ddlRefCountry').focus();
                return false;
            }
            else if (parseInt($$('ddlRefState').val()) <= 0 && '<%= Session("ReferenceStateMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblRefStateMsg').text(), 'MessageType.Errorr');
                $$('ddlRefState').focus();
                return false;
            }
            else if ($$('ddlRefTown').length > 0 && parseInt($$('ddlRefTown').val()) <= 0 && '<%= Session("ReferenceCityMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblRefTownMsg').text(), 'MessageType.Errorr');
                $$('ddlRefTown').focus();
                return false;
            }
            else if (parseInt($$('drpRefGender').val()) <= 0 && '<%= Session("ReferenceGenederMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblRefGenderMsg').text(), 'MessageType.Errorr');
                $$('drpRefGender').focus();
                return false;
            }
            else if ($$('txtRefMobile').val().trim() == '' && '<%= Session("ReferenceMobileMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblRefMobileMsg').text(), 'MessageType.Errorr');
                $$('txtRefMobile').focus();
                return false;
            }
            else if ($$('txtRefTelNo').val().trim() == '' && '<%= Session("ReferenceTelNoMandatory") %>'.toLowerCase() == 'true') {
                ShowMessage($$('lblRefTelNoMsg').text(), 'MessageType.Errorr');
                $$('txtRefTelNo').focus();
                return false;
            }

            //else if (parseInt($$('txtCurrent_salary').val()) <= 0) {
            //    ShowMessage('Please enter Current Salary.', 'MessageType.Errorr');
            //    $$('txtCurrent_salary').focus();
            //    return false;
            //}

            return true;
        }

        function emailTest(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return !regex.test(email);
        }

        function setEvent() {

            $("[id*=ddlRefCountry]").on('change', function () {
                __doPostBack($(this).get(0).id, 0);
            });

            $("[id*=ddlRefState]").on('change', function () {
                __doPostBack($(this).get(0).id, 0);
            });

            $("[id*=objddlCountry]").on('change', function () {
                __doPostBack($(this).get(0).id, 0);
            });
            $("[id*=objddlState]").on('change', function () {
                __doPostBack($(this).get(0).id, 0);
            });

            $("[id*=objddllvCountry]").on('change', function () {
                __doPostBack($(this).get(0).id, 0);
            });
            $("[id*=objddllvState]").on('change', function () {
                __doPostBack($(this).get(0).id, 0);
            });

            $$('btnAddReference').on('click', function () {
                return IsValidate();
            });

            $("[id*=btndelete]").on('click', function () {
                if (!ShowConfirm($$('lblgrdReferenceRowDeletingMsg2').text(), this))
                    return false;
                else
                    return true;
            });
        }

        
    </script>

    <div class="card">
        <asp:Panel ID="pnlForm" runat="server">
            <div class="card-header">
                <%--<div style="float: right">
                    
                </div>--%>

                <h4>
                    <asp:Label ID="lblHeader" runat="server">Reference Details</asp:Label>
                    <cnfpopup:LanguageOpner runat="server" ID="LanguageOpner" OnButtonClick="LanguageOpner_ButtonClick" />
                </h4>
                <asp:Label ID="lblReference" runat="server" Visible="false">Reference</asp:Label>
            </div>

            <div class="card-body">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="form-group row">
                            <div class="col-md-4">
                                <asp:Label ID="lblRefName" runat="server" Text="Name:" CssClass="required"></asp:Label>
                                <asp:TextBox ID="txtRefName" runat="server" MaxLength="200" CssClass="form-control" ValidationGroup="Reference" />
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                                    ControlToValidate="txtRefName" ErrorMessage="Name can not be Blank. " CssClass="ErrorControl"
                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Reference" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                                 <asp:Label ID="lblRefNameMsg" runat="server" Text="Reference Name is compulsory information, it can not be blank. Please enter Reference Name to continue." CssClass="d-none"></asp:Label>
                            </div>

                            <div class="col-md-4">
                                <asp:Label ID="lblRefPosition" runat="server" Text="Position:"></asp:Label>
                                <asp:TextBox ID="txtRefPosition" runat="server" CssClass="form-control" MaxLength="250" />
                                <%-- <asp:RequiredFieldValidator ID="rfvRefPosition" runat="server" Display="Dynamic"
                                    ControlToValidate="txtRefPosition" ErrorMessage="Position can not be Blank. " CssClass="ErrorControl"
                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Reference" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                                  <asp:Label ID="lblRefPositionMsg" runat="server" Text="Position is compulsory information, it can not be blank. Please enter Position to continue." CssClass="d-none"></asp:Label>
                            </div>

                            <div class="col-md-4">
                                <asp:Label ID="lblReferenceType" runat="server" Text="Ref. Type:"></asp:Label>
                                <asp:DropDownList ID="drpReferenceType" runat="server" CssClass="selectpicker form-control" />
                                <%--<asp:RequiredFieldValidator ID="rfvReferenceType" runat="server" Display="Dynamic"
                                    ControlToValidate="drpReferenceType" ErrorMessage="Please select Ref. Type. " CssClass="ErrorControl" InitialValue="0"
                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Reference" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                                  <asp:Label ID="lblReferenceTypeMsg" runat="server" Text="Reference type is compulsory information. Please select Reference type to continue." CssClass="d-none"></asp:Label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4">
                                <asp:Label ID="lblRefCountry" runat="server" Text="Country:"></asp:Label>
                                <asp:DropDownList ID="ddlRefCountry" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                                <%--<asp:RequiredFieldValidator ID="rfvRefCountry" runat="server" Display="Dynamic"
                                    ControlToValidate="ddlRefCountry" ErrorMessage="Please select Country. " CssClass="ErrorControl" InitialValue="0"
                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Reference" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                                <%-- <cc1:cascadingdropdown id="cddRefCountry" runat="server" targetcontrolid="ddlRefCountry"
                    category="country" prompttext="-- Select Country --" loadingtext="[Loading Country...]"
                    servicemethod="GetCountry" />--%>
                                  <asp:Label ID="lblRefCountryMsg" runat="server" Text="Country is compulsory information. Please select Country to continue." CssClass="d-none"></asp:Label>
                            </div>

                            <div class="col-md-4">
                                <asp:Label ID="lblRefState" runat="server" Text="State/Province:"></asp:Label>
                                <asp:DropDownList ID="ddlRefState" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" />
                                <%--<asp:RequiredFieldValidator ID="rfvRefState" runat="server" Display="Dynamic"
                                    ControlToValidate="ddlRefState" ErrorMessage="Please select State/Province. " CssClass="ErrorControl" InitialValue="0"
                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Reference" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                                <%-- <cc1:cascadingdropdown id="cddRefState" runat="server" targetcontrolid="ddlRefState"
                    category="state" parentcontrolid="ddlRefCountry" prompttext="-- Select State/Province --"
                    loadingtext="[Loading State/Province ...]" servicemethod="GetState" />--%>
                                  <asp:Label ID="lblRefStateMsg" runat="server" Text="State is compulsory information. Please select State to continue." CssClass="d-none"></asp:Label>
                            </div>

                            <asp:Panel ID="pnlRefTown" runat="server" class="col-md-4 mb-1">
                                <asp:Label ID="lblRefTown" runat="server" Text="Town/District:"></asp:Label>
                                <asp:DropDownList ID="ddlRefTown" runat="server" CssClass="selectpicker form-control" />
                                <%--<asp:RequiredFieldValidator ID="rfvRefTown" runat="server" Display="Dynamic"
                                    ControlToValidate="ddlRefTown" ErrorMessage="Please select Town/District. " CssClass="ErrorControl" InitialValue="0"
                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Reference" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                                <%--  <cc1:cascadingdropdown id="cddRefTown" runat="server" targetcontrolid="ddlRefTown"
                    category="city" parentcontrolid="ddlRefState" prompttext="-- Select Post Town/District --"
                    loadingtext="[Loading Post Town/District ...]" servicemethod="GetCity" />--%>
                                  <asp:Label ID="lblRefTownMsg" runat="server" Text="City is compulsory information. Please select City to continue." CssClass="d-none"></asp:Label>
                            </asp:Panel>



                            <div class="col-md-4 mb-1">
                                <asp:Label ID="lblRefGender" runat="server" Text="Gender:"></asp:Label>
                                <asp:DropDownList ID="drpRefGender" runat="server" CssClass="selectpicker form-control" />
                                <%-- <asp:RequiredFieldValidator ID="rfvRefGender" runat="server" Display="Dynamic"
                                    ControlToValidate="drpRefGender" ErrorMessage="Please select Gender. " CssClass="ErrorControl" InitialValue="0"
                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Reference" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                                  <asp:Label ID="lblRefGenderMsg" runat="server" Text="Gender is compulsory information, it can not be blank. Please select Gender to continue." CssClass="d-none"></asp:Label>
                            </div>

                            <div class="col-md-4 mb-1">
                                <asp:Label ID="lblRefMobile" runat="server" Text="Mobile:"></asp:Label>
                                <asp:TextBox ID="txtRefMobile" runat="server" MaxLength="30" CssClass="form-control" />
                                <%-- <asp:RequiredFieldValidator ID="rfvRefMobile" runat="server" Display="Dynamic"
                                    ControlToValidate="txtRefMobile" ErrorMessage="Mobile can not be Blank. " CssClass="ErrorControl"
                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Reference" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                                  <asp:Label ID="lblRefMobileMsg" runat="server" Text="Mobile is compulsory information, it can not be blank. Please enter Mobile to continue." CssClass="d-none"></asp:Label>
                            </div>

                            <div class="col-md-4 mb-1">
                                <asp:Label ID="lblRefTelNo" runat="server" Text="Tel. No.:"></asp:Label>
                                <asp:TextBox ID="txtRefTelNo" runat="server" MaxLength="30" CssClass="form-control" />
                                <%--<asp:RequiredFieldValidator ID="rfvRefTelNo" runat="server" Display="Dynamic"
                                    ControlToValidate="txtRefTelNo" ErrorMessage="Tel. No. can not be Blank. " CssClass="ErrorControl"
                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Reference" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                                  <asp:Label ID="lblRefTelNoMsg" runat="server" Text="Tel. No. is compulsory information, it can not be blank. Please enter Tel. No. to continue." CssClass="d-none"></asp:Label>
                            </div>



                            <div class="col-md-4 mb-1">
                                <asp:Label ID="lblRefAddress" runat="server" Text="Address:"></asp:Label>
                                <asp:TextBox ID="txtRefAddress" runat="server" TextMode="MultiLine" MaxLength="250" CssClass="form-control" />
                                <%-- <asp:RequiredFieldValidator ID="rfvRefAddress" runat="server" Display="Dynamic"
                                    ControlToValidate="txtRefAddress" ErrorMessage="Address can not be Blank. " CssClass="ErrorControl"
                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Reference" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                                  <asp:Label ID="lblRefAddressMsg" runat="server" Text="Address is compulsory information, it can not be blank. Please enter Address to continue." CssClass="d-none"></asp:Label>
                            </div>

                            <div class="col-md-4 mb-1">
                                <asp:Label ID="lblRefEmail" runat="server" Text="Email:"></asp:Label>
                                <asp:TextBox ID="txtRefEmail" runat="server" MaxLength="250" ValidationGroup="Reference" CssClass="form-control" />
                                <%--<asp:RequiredFieldValidator ID="rfvRefEmail" runat="server" Display="Dynamic"
                                    ControlToValidate="txtRefEmail" ErrorMessage="Email can not be Blank. " CssClass="ErrorControl"
                                    ForeColor="White" Style="z-index: 1000" ValidationGroup="Reference" SetFocusOnError="True"> </asp:RequiredFieldValidator>--%>
                                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                                    ControlToValidate="txtRefEmail" ErrorMessage="Enter Valid Email ID" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    CssClass="ErrorControl" ForeColor="White" ValidationGroup="Reference" SetFocusOnError="True"> </asp:RegularExpressionValidator>--%>
                                  <asp:Label ID="lblRefEmailMsg" runat="server" Text="Email is compulsory information, it can not be blank. Please enter Email to continue." CssClass="d-none"></asp:Label>
                                  <asp:Label ID="lblRefEmailMsg2" runat="server" Text="Sorry, Email is not valid. Please enter valid email." CssClass="d-none"></asp:Label>
                            </div>

                            <div class="col-md-4 mb-1">
                            </div>
                        </div>
                        <%--<div class="form-group row">
                            <div class="col-md-12" style="text-align: center;">
                                <asp:ValidationSummary ID="vs"
                                    HeaderText="You must enter a value in the following fields:"
                                    DisplayMode="BulletList"
                                    EnableClientScript="true"
                                    runat="server" ValidationGroup="Reference" Style="color: red" />
                            </div>
                        </div>--%>
                        <%-- <div class="form-group row">
                    <div class="col-md-12" style="text-align: center;">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="Something Bad happened, Please contact Administrator!!!!" Visible="false" />
                    </div>
                </div>--%>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddReference" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

            <div class="card-footer">
                <div class="form-group row d-flex justify-content-center">
                    <%--<div class="col-md-4">
            </div>--%>

                    <div class="col-md-4">
                        <asp:Button ID="btnAddReference" runat="server" CssClass="btn btn-primary w-100" Text="Add Reference"
                            ValidationGroup="Reference" />
                        <asp:Label ID="lblbtnAddReferenceMsg1" runat="server" Text="Sorry, Selected Reference is already added to the list." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblbtnAddReferenceMsg2" runat="server" Text="Reference Saved Successfully! \n\n Click Ok to add another Reference or to continue with other section(s)." CssClass="d-none"></asp:Label>
                    </div>

                    <%--<div class="col-md-4">
                <%--<asp:Button ID="btnEditReference" runat="server" CssClass="btn btn-primary" Style="width: 100%;" Text="Update Reference"
                    ValidationGroup="Reference" Enabled="false" Visible="false" />--%>
                    <%--</div>--%>
                </div>
            </div>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <asp:Panel ID="pnl_grdReference" runat="server" CssClass="table-responsive overflow-auto m-h-300">
                        <asp:Label ID="colhRefrenceName" runat="server" Text="Name" Visible="false"></asp:Label>
                        <asp:Label ID="colhContactDetail" runat="server" Text="Contact Details" Visible="false"></asp:Label>
                        <asp:ListView ID="lvReference" runat="server" ItemPlaceholderID="ReferenceItemsPlaceHolder" DataSourceID="objodsReference" DataKeyNames="referencetranunkid">
                            <LayoutTemplate>
                                <div class="form-group row">

                                    <div class="col-md-12 text-center font-weight-bold">
                                        <asp:Label ID="lblReferenceList" runat="server" Text="Reference list"></asp:Label>
                                        <hr class="mt-0" />
                                    </div>
                                </div>
                                <asp:PlaceHolder ID="ReferenceItemsPlaceHolder" runat="server"></asp:PlaceHolder>
                            </LayoutTemplate>
                                    <ItemTemplate>
                                <div class="form-group row">
                                    <div class="col-md-10 mb-1">
                                        <div class="form-group row mb-0">
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvItemName" runat="server" Text="Name:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvName" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("name"), True)  %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lbllvItemPosition" runat="server" Text="Position:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvPosition" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("position"), True)  %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lblItemRefType" runat="server" Text="Ref. Type:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvRefType" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Relatoin"), True) %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lblItemCountry" runat="server" Text="Country:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvCountry" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Country_Name"), True)  %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lblItemState" runat="server" Text="State/Province:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvState" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("State_Name"), True)  %>'></asp:Label>
                                            </div>
                                            <asp:Panel ID="pnllvItemCity" runat="server" class="col-md-2 px-2">
                                                <asp:Label ID="lblItemCity" runat="server" Text="Town/District:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvCity" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("City_name"), True)  %>'></asp:Label>
                                            </asp:Panel>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lblItemGender" runat="server" Text="Gender:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvGender" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Gender_name"), True)  %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lblItemMobile" runat="server" Text="Mobile:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvMobile" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("mobile_no"), True)  %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lblItemTelNo" runat="server" Text="Tel. No.:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvTelNo" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("telephone_no"), True)  %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lblItemEmail" runat="server" Text="Email:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvEmail" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("email"), True)  %>'></asp:Label>
                                            </div>
                                            <div class="col-md-2 px-2">
                                                <asp:Label ID="lblItemAddress" runat="server" Text="Address:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:Label ID="objlbllvAddress" runat="server" Text='<%# AntiXss.AntiXssEncoder.HtmlEncode(Eval("Address"), True)  %>'></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1 mb-1">
                                        <asp:LinkButton ID="objlvbtnedit" runat="server" CommandName="Edit" Text="<i aria-hidden='true' class='fa fa-edit'></i>"></asp:LinkButton>
                                    </div>
                                    <div class="col-md-1 mb-1">
                                        <asp:LinkButton ID="objbtndelete" runat="server" CommandName="Delete" Text="<i aria-hidden='true' class='fa fa-trash'></i>"></asp:LinkButton>
                                    </div>
                                </div>
                                <hr class="mt-0" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                <div class="form-group row">
                                    <div class="col-md-10 mb-1">
                                        <div class="form-group row mb-0">
                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lblItemName" runat="server" Text="Name:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:TextBox ID="objtxtlvName" runat="server" CssClass="form-control" TextMode="MultiLine" Text='<%# Eval("name")  %>'></asp:TextBox>
                                            </div>
                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lblItemPosition" runat="server" Text="Position:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:TextBox ID="objtxtlvPosition" runat="server" CssClass="form-control" TextMode="MultiLine" Text='<%# Eval("position")  %>'></asp:TextBox>
                                            </div>
                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lblItemRefType" runat="server" Text="Ref. Type:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:DropDownList ID="objddllvRefType" runat="server" CssClass="selectpicker form-control" />
                                            </div>
                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lblItemCountry" runat="server" Text="Country:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:DropDownList ID="objddllvCountry" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" OnSelectedIndexChanged="objddllvCountry_SelectedIndexChanged" />
                                            </div>
                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lblItemState" runat="server" Text="State/Province:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:DropDownList ID="objddllvState" runat="server" CssClass="selectpicker form-control" AutoPostBack="false" OnSelectedIndexChanged="objddllvState_SelectedIndexChanged" />
                                            </div>
                                            <asp:Panel ID="pnllvItemCity" runat="server" class="col-md-4 px-2 mb-1">
                                                <asp:Label ID="lblItemCity" runat="server" Text="Town/District:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:DropDownList ID="objddllvCity" runat="server" CssClass="selectpicker form-control" />
                                            </asp:Panel>
                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lblItemGender" runat="server" Text="Gender:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:DropDownList ID="objddllvGender" runat="server" CssClass="selectpicker form-control" />
                                            </div>
                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lblItemMobile" runat="server" Text="Mobile:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:TextBox ID="objtxtlvMobile" runat="server" CssClass="form-control" TextMode="MultiLine" Text='<%# Eval("mobile_no")  %>'></asp:TextBox>
                                            </div>
                                            <div class="col-md-4 px-2">
                                        <asp:Label ID="lblItemTelNo" runat="server" Text="Tel. No.:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:TextBox ID="objtxtlvTelNo" runat="server" CssClass="form-control" TextMode="MultiLine" Text='<%# Eval("telephone_no")  %>'></asp:TextBox>
                                            </div>
                                            <div class="col-md-4 px-2">
                                        <asp:Label ID="lblItemEmail" runat="server" Text="Email:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:TextBox ID="objtxtlvEmail" runat="server" CssClass="form-control" TextMode="MultiLine" Text='<%# Eval("email")  %>'></asp:TextBox>
                                            </div>
                                            <div class="col-md-4 px-2">
                                                <asp:Label ID="lblItemAddress" runat="server" Text="Address:" CssClass="font-weight-bold"></asp:Label>
                                        <br />
                                                <asp:TextBox ID="objtxtlvAddress" runat="server" Rows="5" TextMode="MultiLine" CssClass="form-control" Text='<%# Eval("Address")  %>'></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1 mb-1">
                                        <asp:LinkButton ID="btnlvupdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                    </div>
                                    <div class="col-md-1 mb-1">
                                        <asp:LinkButton ID="btnlvcancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                    </div>
                                </div>
                                    </EditItemTemplate>
                        </asp:ListView>
                        <asp:ObjectDataSource ID="objodsReference" runat="server" SelectMethod="GetAppReference" UpdateMethod="EditAppReference" TypeName="Aruti_Online_Recruitment.clsApplicantReference" DeleteMethod="DeleteApplicantReference" EnablePaging="false" EnableCaching="True" CacheDuration="120" >
                            <SelectParameters>
                                <asp:Parameter Name="strCompCode" Type="String" DefaultValue="" />
                                <asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intRelation_id" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="dtCurrentDate" Type="DateTime" DefaultValue="" />
                            </SelectParameters>

                            <UpdateParameters>
                                <asp:Parameter Name="objAppReferenceUpdate" Type="Object" />
                            </UpdateParameters>

                            <DeleteParameters>
                                <asp:Parameter Name="strCompCode" Type="String" DefaultValue="" />
                                <asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                                <asp:Parameter Name="intReferenceTranUnkId" Type="Int32" DefaultValue="0" />
                            </DeleteParameters>

                        </asp:ObjectDataSource>

                        <asp:Label ID="lblgrdReferenceRowUpdatingMsg1" runat="server" Text="Sorry, Selected Reference is already added to the list." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblgrdReferenceRowUpdatingMsg2" runat="server" Text="Reference Updated Successfully! \n\n Click Ok to add another Reference or to continue with other section(s)." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblgrdReferenceRowDeletingMsg1" runat="server" Text="Reference Deleted Successfully! \n\n Click Ok to add another Reference or to continue with other section(s)." CssClass="d-none"></asp:Label>
                        <asp:Label ID="lblgrdReferenceRowDeletingMsg2" runat="server" Text="Are you sure you want to Delete this Reference?" CssClass="d-none"></asp:Label>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnAddReference" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>

        </asp:Panel>
    </div>
    <asp:HiddenField ID="hfCancelLang" runat="server" />
    <cnfpopup:LanguageControl runat="server" ID="LanguageControl" _csvclassname="clsApplicantReference" _ModuleName="ApplicantReference" _TargetControl="pnlForm" CancelControlName="hfCancelLang" />
</asp:Content>
