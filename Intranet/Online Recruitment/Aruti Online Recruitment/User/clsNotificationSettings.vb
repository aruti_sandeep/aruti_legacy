﻿Option Strict On
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient

Public Class clsNotificationSettings

#Region " Public Properties "
    Private _encommn As clsCommon_Master.enCommonMaster
    Public Property enMastertype() As Integer
        Get
            Return CType(_encommn, Integer)
        End Get
        Set(ByVal value As Integer)
            _encommn = CType(value, clsCommon_Master.enCommonMaster)
        End Set
    End Property
#End Region

#Region "General Methods"

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Function GetApplicantNotificationSettings(xCompanyID As Integer, xCompanyCode As String, xApplicantID As Integer, ByVal intMasterType As Integer) As DataSet
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            Dim strQ As String = "procGetApplicantNotficationSettings "

            If xCompanyCode Is Nothing Then xCompanyCode = ""

            Using con As New SqlConnection(strConn)
                con.Open()
                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = xCompanyID
                        cmd.Parameters.Add(New SqlParameter("@compcode", SqlDbType.NVarChar)).Value = xCompanyCode
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = xApplicantID
                        cmd.Parameters.Add(New SqlParameter("@VacanyMasterType", SqlDbType.Int)).Value = intMasterType
                        da.SelectCommand = cmd
                        da.Fill(ds, "NotificationSettings")
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    Public Function InsertUpdate_ApplicantNotificationSettings(xCompanyID As Integer, xCompanyCode As String, xApplicantID As Integer, ByVal xVacancytitleId As Integer, xPriority As Integer, xIsActive As Boolean) As Boolean
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            Dim strQ As String = "procInsertUpdateApplicantNotficationSettings"
            Using con As New SqlConnection(strConn)
                con.Open()
                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = xCompanyID
                        cmd.Parameters.Add(New SqlParameter("@compcode", SqlDbType.NVarChar)).Value = xCompanyCode
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = xApplicantID
                        cmd.Parameters.Add(New SqlParameter("@vacancytitleId", SqlDbType.Int)).Value = xVacancytitleId
                        cmd.Parameters.Add(New SqlParameter("@priority", SqlDbType.Int)).Value = xPriority
                        cmd.Parameters.Add(New SqlParameter("@isactive", SqlDbType.Bit)).Value = xIsActive
                        cmd.ExecuteNonQuery()
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        Finally
        End Try
        Return True
    End Function

    Public Function GetApplicantNotificationStatus() As DataSet
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            Dim strQ As String = "procGetApplicantNotificationStatus"
            Using con As New SqlConnection(strConn)
                con.Open()
                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)
                        cmd.CommandType = CommandType.StoredProcedure
                        da.SelectCommand = cmd
                        da.Fill(ds, "GetApplicantNotificationStatus")
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetApplicantsNotificationList(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , intApplicantUnkId As Integer _
                                     , intVacancyUnkId As Integer _
                                     , strFirstName As String _
                                     , strSurName As String _
                                     , strEmail As String _
                                     , strPresentMobileNo As String _
                                     , intStatusId As Integer _
                                     , intTitleId As Integer _
                                     , intVacancyMasterTypeId As Integer _
                                     , startRowIndex As Integer _
                                     , intPageSize As Integer _
                                     , strSortExpression As String _
									 , intMaxalert As Integer _
									 , isAdmin As Boolean
                                     ) As DataSet
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            Dim strQ As String = "procGetApplicantsNotification"
            startRowIndex = Convert.ToInt32(startRowIndex / intPageSize) + 1

            If strCompCode Is Nothing Then strCompCode = ""
            If strFirstName Is Nothing Then strFirstName = ""
            If strSurName Is Nothing Then strSurName = ""
            If strEmail Is Nothing Then strEmail = ""
            If strPresentMobileNo Is Nothing Then strPresentMobileNo = ""
            If strSortExpression Is Nothing Then strSortExpression = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)
                        cmd.CommandTimeout = CInt(HttpContext.Current.Session("CommandTimeOut"))

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkId
                        cmd.Parameters.Add(New SqlParameter("@vacancyunkid", SqlDbType.Int)).Value = intVacancyUnkId
                        cmd.Parameters.Add(New SqlParameter("@firstname", SqlDbType.NVarChar)).Value = strFirstName.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@surname", SqlDbType.NVarChar)).Value = strSurName.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@present_mobileno", SqlDbType.NVarChar)).Value = strPresentMobileNo.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@status_id", SqlDbType.Int)).Value = intStatusId
                        cmd.Parameters.Add(New SqlParameter("@title_id", SqlDbType.Int)).Value = intTitleId
                        cmd.Parameters.Add(New SqlParameter("@vacancymaster_id", SqlDbType.Int)).Value = intVacancyMasterTypeId
                        cmd.Parameters.Add(New SqlParameter("@PageSize", SqlDbType.Int)).Value = intPageSize
                        cmd.Parameters.Add(New SqlParameter("@CurrentPage", SqlDbType.Int)).Value = startRowIndex
                        cmd.Parameters.Add(New SqlParameter("@SortExpression", SqlDbType.NVarChar)).Value = strSortExpression.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@maxalert", SqlDbType.Int)).Value = intMaxalert
						cmd.Parameters.Add(New SqlParameter("@isAdmin", SqlDbType.Bit)).Value = isAdmin

                        da.SelectCommand = cmd
                        da.Fill(ds, "GetApplicantsNotificationList")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, False)>
    Public Shared Function GetApplicantNotificationCount(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , intApplicantUnkId As Integer _
                                     , intVacancyUnkId As Integer _
                                     , strFirstName As String _
                                     , strSurName As String _
                                     , strEmail As String _
                                     , strPresentMobileNo As String _
                                     , intStatusId As Integer _
                                     , intTitleId As Integer _
                                     , intVacancyMasterTypeId As Integer _
                                     , startRowIndex As Integer _
                                     , intPageSize As Integer _
                                     , strSortExpression As String _
									 , intMaxalert As Integer _
									 , isAdmin As Boolean
                                     ) As Integer
        Dim ds As New DataSet
        Dim intCount As Integer = 0
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procGetApplicantNotificationTotal"

            If strCompCode Is Nothing Then strCompCode = ""
            If strFirstName Is Nothing Then strFirstName = ""
            If strSurName Is Nothing Then strSurName = ""
            If strEmail Is Nothing Then strEmail = ""
            If strPresentMobileNo Is Nothing Then strPresentMobileNo = ""
            If strSortExpression Is Nothing Then strSortExpression = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)
                        cmd.CommandTimeout = CInt(HttpContext.Current.Session("CommandTimeOut"))

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkId
                        cmd.Parameters.Add(New SqlParameter("@vacancyunkid", SqlDbType.Int)).Value = intVacancyUnkId
                        cmd.Parameters.Add(New SqlParameter("@firstname", SqlDbType.NVarChar)).Value = strFirstName.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@surname", SqlDbType.NVarChar)).Value = strSurName.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@present_mobileno", SqlDbType.NVarChar)).Value = strPresentMobileNo.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@status_id", SqlDbType.Int)).Value = intStatusId
                        cmd.Parameters.Add(New SqlParameter("@title_id", SqlDbType.Int)).Value = intTitleId
                        cmd.Parameters.Add(New SqlParameter("@vacancymaster_id", SqlDbType.Int)).Value = intVacancyMasterTypeId
						cmd.Parameters.Add(New SqlParameter("@maxalert", SqlDbType.Int)).Value = intMaxalert
						cmd.Parameters.Add(New SqlParameter("@isAdmin", SqlDbType.Bit)).Value = isAdmin

                        Dim intCnt As Object = cmd.ExecuteScalar()

						intCount = DirectCast(IIf(intCnt Is Nothing, 0, intCnt), Integer)
                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return intCount
    End Function

    Public Function InsertUpdateApplicantVacancyNotfications(xCompanyId As Integer, mstrCompanyCode As String, xApplicantID As Integer _
                                                                      , xVacancyID As Integer, xNotificationpriorityID As Integer, mstrLink As String _
                                                                      , mstrSubject As String, mstrMessage As String, mblnIsEmailSent As Boolean) As DataSet
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            Dim strQ As String = "procInsertUpdateApplicantVacancyNotfications"
            Using con As New SqlConnection(strConn)
                con.Open()
                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = xCompanyId
                        cmd.Parameters.Add(New SqlParameter("@compcode", SqlDbType.NVarChar)).Value = mstrCompanyCode
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = xApplicantID
                        cmd.Parameters.Add(New SqlParameter("@vacancyunkid", SqlDbType.Int)).Value = xVacancyID
                        cmd.Parameters.Add(New SqlParameter("@appnotificationpriorityunkid", SqlDbType.Int)).Value = xNotificationpriorityID
                        cmd.Parameters.Add(New SqlParameter("@link", SqlDbType.NVarChar)).Value = mstrLink
                        cmd.Parameters.Add(New SqlParameter("@subject", SqlDbType.NVarChar)).Value = mstrSubject
                        cmd.Parameters.Add(New SqlParameter("@message", SqlDbType.NVarChar)).Value = mstrMessage
                        cmd.Parameters.Add(New SqlParameter("@isemailsent", SqlDbType.Bit)).Value = mblnIsEmailSent
                        cmd.ExecuteNonQuery()
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    Public Function GetApplicantForJobAlerts(xCompanyId As Integer, mstrCompanyCode As String, xMaxVacancyAlert As Integer _
                                                                     , xApplicantID As Integer, xVacancyID As Integer, mblnIsEmailSent As Boolean) As DataSet
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            Dim strQ As String = "procGetApplicantForJobAlerts"
            Using con As New SqlConnection(strConn)
                con.Open()
                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = xCompanyId
                        cmd.Parameters.Add(New SqlParameter("@compcode", SqlDbType.NVarChar)).Value = mstrCompanyCode
                        cmd.Parameters.Add(New SqlParameter("@maxalert", SqlDbType.Int)).Value = xMaxVacancyAlert
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = xApplicantID
                        cmd.Parameters.Add(New SqlParameter("@vacancyunkid", SqlDbType.Int)).Value = xVacancyID
                        cmd.Parameters.Add(New SqlParameter("@isemailsent", SqlDbType.Bit)).Value = mblnIsEmailSent
                        da.SelectCommand = cmd
                        da.Fill(ds, "JobAlerts")
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function


#End Region

End Class
