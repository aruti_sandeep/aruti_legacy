﻿Imports System.Globalization
Imports System.Web.Services

Public Class Base_Page
    Inherits System.Web.UI.Page

    Private Const AntiXsrfTokenKey As String = "__AntiXsrfToken"
    Private Const AntiXsrfUserNameKey As String = "__AntiXsrfUserName"
    Private _antiXsrfTokenValue As String = ""

#Region " Public Enum "
    Public Enum MessageType
        Success
        Errorr
        Info
        Warning
    End Enum
#End Region

    Protected Overloads Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        Try
            MyBase.OnPreInit(e)


            'If HttpContext.Current.User IsNot Nothing AndAlso User.Identity.IsAuthenticated Then
            '    ViewStateUserKey = Session.SessionID
            'End If

            'If Session("email") Is Nothing OrElse Session("email").ToString.Trim = "" Then
            '    Response.Redirect(ViewState("mstrUrlArutiLink"), False)
            'End If
            Response.AppendHeader("X-XSS-Protection", "1; mode=block")

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception("Basepage->OnPreInit event!!!" & ex.Message.ToString)
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        Finally
        End Try


    End Sub

    Protected Overloads Overrides Sub OnInit(ByVal e As System.EventArgs)
        Try
            If Not Request.IsSecureConnection Then
                Response.Redirect(Request.Url.AbsoluteUri.ToLower().Replace("http://", "https://"), False)
                Exit Sub
            End If

            Dim ASpNetIDSessionIDCookie = Request.Cookies("ASP.NET_SessionId")
            'If ASpNetIDSessionIDCookie IsNot Nothing AndAlso Request.IsSecureConnection Then
            '    ASpNetIDSessionIDCookie.HttpOnly = True
            '    ASpNetIDSessionIDCookie.Secure = True
            '    ASpNetIDSessionIDCookie.Path = "/;sameSite=Strict"
            '    Request.Cookies.Set(ASpNetIDSessionIDCookie)
            'End If

            Dim arr() As String = (From p As String In HttpContext.Current.Request.Cookies Select (p.ToString)).ToArray
            For Each s As String In arr
                Dim Cookie = Request.Cookies(s)
                Cookie.HttpOnly = True
                Cookie.Secure = True
                Cookie.Path = "/;sameSite=Strict"
                Response.Cookies.Set(Cookie)
            Next

            ''First, check for the existence of the Anti-XSS cookie
            Dim requestCookie = Request.Cookies(AntiXsrfTokenKey)
            Dim requestCookieGuidValue As Guid

            'If the CSRF cookie is found, parse the token from the cookie.
            'Then, set the global page variable and view state user
            'key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
            'method.
            If requestCookie IsNot Nothing AndAlso Guid.TryParse(requestCookie.Value, requestCookieGuidValue) Then
                'Set the global token variable so the cookie value can be
                'validated against the value in the view state form field in
                'the Page.PreLoad method.
                _antiXsrfTokenValue = requestCookie.Value

                'Set the view state user key, which will be validated by the
                'framework during each request
                Page.ViewStateUserKey = _antiXsrfTokenValue
            Else
                'If the CSRF cookie is not found, then this is a new session.
                'Generate a new Anti-XSRF token
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N")

                'Set the view state user key, which will be validated by the
                'framework during each request
                Page.ViewStateUserKey = _antiXsrfTokenValue

                'Create the non-persistent CSRF cookie
                'Set the HttpOnly property to prevent the cookie from
                'being accessed by client side script

                'Add the Anti-XSRF token to the cookie value
                Dim responseCookie = New HttpCookie(AntiXsrfTokenKey)
                responseCookie.HttpOnly = True
                responseCookie.Value = _antiXsrfTokenValue
                responseCookie.Path = "/;sameSite=Strict"

                'If we are using SSL, the cookie should be set to secure to
                'prevent it from being sent over HTTP connections
                If FormsAuthentication.RequireSSL AndAlso Request.IsSecureConnection Then
                    responseCookie.Secure = True
                    responseCookie.HttpOnly = True
                End If

                'Add the CSRF cookie to the response
                'Response.Cookies.[Set](responseCookie)
                Response.Cookies.Set(responseCookie)

                Dim responseAntiForgeryCookie = New HttpCookie(System.Web.Helpers.AntiForgeryConfig.CookieName.ToString())
                Dim newToken As String = ""
                Dim formToken As String = ""

                Web.Helpers.AntiForgery.GetTokens("", newToken, formToken)

                responseAntiForgeryCookie.Value = newToken
                responseAntiForgeryCookie.HttpOnly = True
                responseAntiForgeryCookie.Path = "/;sameSite=Strict"

                If FormsAuthentication.RequireSSL AndAlso Request.IsSecureConnection Then
                    responseAntiForgeryCookie.Secure = True
                    responseAntiForgeryCookie.HttpOnly = True
                End If
                Response.Cookies.Set(responseAntiForgeryCookie)

            End If


            If Session.IsNewSession Then
                ' Force session to be created;
                ' otherwise the session ID changes on every request.
                Session("ForceSession") = DateTime.Now
            End If
            ' 'Sign' the viewstate with the current session.
            'ViewStateUserKey = Session.SessionID
            If Page.EnableViewState Then
                ' Make sure ViewState wasn't passed on the querystring.
                ' This helps prevent one-click attacks.
                If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                    Throw New Exception("Viewstate existed, but not on the form.")
                End If
            End If
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.Cache.AppendCacheExtension("no-store, must-revalidate")
            Response.Cache.SetNoStore()
            Response.AppendHeader("Pragma", "no-cache")
            Response.AppendHeader("Expires", "0")

            If (ViewState("mstrUrlArutiLink") Is Nothing OrElse ViewState("mstrUrlArutiLink") = "") AndAlso Session("mstrUrlArutiLink") IsNot Nothing Then
                ViewState("mstrUrlArutiLink") = Session("mstrUrlArutiLink")
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception("Basepage->OnInit event!!!" & ex.Message.ToString)
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Protected Overloads Overrides Sub OnPreLoad(ByVal e As System.EventArgs)
        Try
            'During the initial page load, add the Anti-XSRF token and user
            'name to the ViewState
            If Not IsPostBack Then
                'Set Anti-XSRF token
                ViewState(AntiXsrfTokenKey) = ViewStateUserKey

                'If a user name is assigned, set the user name
                ViewState(AntiXsrfUserNameKey) = If(Context.User.Identity.Name, [String].Empty)

            Else
                'During all subsequent post backs to the page, the token value from
                'the cookie should be validated against the token in the view state
                'form field. Additionally user name should be compared to the
                'authenticated users name

                'Validate the Anti-XSRF token
                If (ViewState(AntiXsrfTokenKey).ToString <> _antiXsrfTokenValue OrElse ViewState(AntiXsrfUserNameKey).ToString <> (Context.User.Identity.Name)) Then
                    Throw New InvalidOperationException("Validation of Anti-XSRF token failed.")
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    Protected Overloads Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Try
            If Session("email") Is Nothing OrElse Session("email") = "" Then
                'If Session.IsNewSession = True OrElse Session("email") Is Nothing OrElse Session("email") = "" Then
                If Request.Cookies("LogOutPageURL") IsNot Nothing Then
                    Dim strUrlArutiLink As String = clsSecurity.Decrypt(Request.Cookies("LogOutPageURL").Value, "ezee")
                    ShowMessage("Sorry, Session is expired!", strUrlArutiLink, MessageType.Info)
                    Exit Try
                End If
            End If
            'If ViewStateUserKey = "" Then
            '    Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) & "/Login.aspx", True)
            'End If

            ''During the initial page load, add the Anti-XSRF token and user
            ''name to the ViewState
            'If Not IsPostBack Then
            '    'Set Anti-XSRF token
            '    ViewState(AntiXsrfTokenKey) = ViewStateUserKey

            '    'If a user name is assigned, set the user name
            '    ViewState(AntiXsrfUserNameKey) = If(Context.User.Identity.Name, [String].Empty)

            'Else
            '    'During all subsequent post backs to the page, the token value from
            '    'the cookie should be validated against the token in the view state
            '    'form field. Additionally user name should be compared to the
            '    'authenticated users name

            '    'Validate the Anti-XSRF token
            '    If (ViewState(AntiXsrfTokenKey).ToString <> _antiXsrfTokenValue OrElse ViewState(AntiXsrfUserNameKey).ToString <> (Context.User.Identity.Name)) Then
            '        Throw New InvalidOperationException("Validation of Anti-XSRF token failed.")
            '    End If
            'End If

            Call SetDateFormat()
            If IsPostBack = True Then
                Web.Helpers.AntiForgery.Validate()
            End If

            MyBase.OnLoad(e)
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception("Basepage->OnLoad event!!!" & ex.Message.ToString)
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
            Response.Redirect(Request.Url.AbsolutePath, True)
        Finally
        End Try


    End Sub

    Public Sub SetDateFormat()
        Try
            Dim NewCulture As CultureInfo = CType(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
            Dim strDateFormat = "dd-MMM-yyyy"
            Dim strDateSeparator = "-"
            If Session("DateFormat") IsNot Nothing AndAlso Session("DateFormat").ToString.Trim <> "" Then
                strDateFormat = Session("DateFormat").ToString
            End If
            If Session("DateSeparator") IsNot Nothing AndAlso Session("DateSeparator").ToString.Trim <> "" Then
                strDateSeparator = Session("DateSeparator").ToString
            End If

            NewCulture.DateTimeFormat.ShortDatePattern = strDateFormat
            NewCulture.DateTimeFormat.DateSeparator = strDateSeparator
            System.Threading.Thread.CurrentThread.CurrentCulture = NewCulture

        Catch ex As Exception
            Throw New Exception("SetDateFormat:- " & ex.Message)
        End Try
    End Sub

    Public Sub ShowMessage(strMessage As String, Optional type As MessageType = MessageType.Info)
        Try
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" & Message & "', '" & type & "');", True)
            If strMessage.Trim = "" Then
                strMessage = "Transaction Saved Successfully!"
            End If
            Dim strTitle As String = "Aruti"
            Dim strType As String = "BootstrapDialog.TYPE_PRIMARY"
            Dim strClass As String = ""
            Select Case type
                Case MessageType.Errorr
                    strType = "BootstrapDialog.TYPE_DANGER"
                    strClass = "fa fa-exclamation-circle"
                Case MessageType.Success
                    'strType = "Success!"
                    strClass = "fa fa-info-circle"
                Case MessageType.Warning
                    strType = "BootstrapDialog.TYPE_WARNING"
                    strClass = "fa fa-exclamation-triangle"
                Case Else
                    strType = "BootstrapDialog.TYPE_INFO"
                    strClass = "fa fa-info-circle"
            End Select
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "$('#msgicon').removeClass();$('#msgicon').addClass('" & strClass & "');$('.modal-body #msgbody').text('" & strType & "' + ' ' + '" & strMessage & "');$('#myModal').modal();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(),
               "BootstrapDialog.show({
                    type: " & strType & ",
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: '" & strTitle & "',
                    message: '" & strMessage & "',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function(dialogItself){
                        dialogItself.close();
                        }
                    }]
                });", True)
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Public Sub ShowMessage(strMessage As String, strRedirectLink As String, Optional type As MessageType = MessageType.Info)
        Try
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" & Message & "', '" & type & "');", True)
            If strMessage.Trim = "" Then
                strMessage = "Transaction Saved Successfully!"
            End If
            Dim strTitle As String = "Aruti"
            Dim strType As String = "BootstrapDialog.TYPE_PRIMARY"
            Dim strClass As String = ""
            Select Case type
                Case MessageType.Errorr
                    strType = "BootstrapDialog.TYPE_DANGER"
                    strClass = "fa fa-exclamation-circle"
                Case MessageType.Success
                    'strType = "Success!"
                    strClass = "fa fa-info-circle"
                Case MessageType.Warning
                    strType = "BootstrapDialog.TYPE_WARNING"
                    strClass = "fa fa-exclamation-triangle"
                Case Else
                    strType = "BootstrapDialog.TYPE_INFO"
                    strClass = "fa fa-info-circle"
            End Select
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "$('#msgicon').removeClass();$('#msgicon').addClass('" & strClass & "');$('.modal-body #msgbody').text('" & strType & "' + ' ' + '" & strMessage & "');$('#myModal').modal();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(),
               "BootstrapDialog.show({
                    type: " & strType & ",
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: '" & strTitle & "',
                    message: '" & strMessage & "',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function(){
                        window.location.href='" & strRedirectLink & "';
                        return false;
                        }
                    }]
                });", True)
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Public Sub ShowConfirm(strMessage As String, Optional type As MessageType = MessageType.Info)
        Try
            If strMessage.Trim = "" Then
                strMessage = "Are you sure you want to Delete?"
            End If
            Dim strTitle As String = "Aruti"
            Dim strType As String = "BootstrapDialog.TYPE_PRIMARY"
            Dim strClass As String = ""
            Select Case type
                Case MessageType.Errorr
                    strType = "BootstrapDialog.TYPE_DANGER"
                    strClass = "fa fa-exclamation-circle"
                Case MessageType.Success
                    'strType = "Success!"
                    strClass = "fa fa-info-circle"
                Case MessageType.Warning
                    strType = "BootstrapDialog.TYPE_WARNING"
                    strClass = "fa fa-exclamation-triangle"
                Case Else
                    strType = "BootstrapDialog.TYPE_INFO"
                    strClass = "fa fa-info-circle"
            End Select
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "$('#msgicon').removeClass();$('#msgicon').addClass('" & strClass & "');$('.modal-body #msgbody').text('" & strType & "' + ' ' + '" & strMessage & "');$('#myModal').modal();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(),
               "BootstrapDialog.confirm({
                    title: '" & strTitle & "',
                    message: '" & strMessage & "',
                    type: " & strType & ", // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                    closable: false, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    btnCancelLabel: 'No', // <-- Default value is 'Cancel',
                    btnOKLabel: 'Yes', // <-- Default value is 'OK',
                    btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                    callback: function(result) {
                        // result will be true if button was click, while it will be false if users close the dialog directly.
                        return result;
                        //if(result) {
                        //    alert('Yup.');
                        //}else {
                        //    alert('Nope.');
                        //}
                    }
                });", True)
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    <WebMethod>
    Public Shared Function SwitchToAdmin(strEmail As String) As String
        Try
            If Roles.IsUserInRole(strEmail, "superadmin") = True Then
                Return ""
                Exit Function
            End If
            If Roles.IsUserInRole(strEmail, "admin") = False Then
                Return "Sorry, You are not authorized to view Admin panel."
                Exit Function
            End If

            Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
            If arrAdmin.Contains(strEmail.Trim) = False Then

                arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                If arrAdmin.Contains(strEmail.Trim) = False Then
                    Return "Sorry, You are not authorized to view Admin panel."
                    Exit Function
                End If
            End If

            HttpContext.Current.Session("email") = strEmail
            HttpContext.Current.Session("CanViewErrorLog") = False
            HttpContext.Current.Session("CanTruncateErrorLog") = False
            HttpContext.Current.Session("CanShrinkDataBase") = False
            HttpContext.Current.Session("CanRunFullScript") = False
            HttpContext.Current.Session("CanRunAlterScript") = False

            Dim objSAdmin As New clsSACommon
            Dim ds As DataSet = objSAdmin.GetCompanyCodeFromEmail(strEmail, Membership.GetUser(strEmail).ProviderUserKey.ToString)

            If ds.Tables(0).Rows.Count > 0 Then
                HttpContext.Current.Session("CanViewErrorLog") = CBool(ds.Tables(0).Rows(0).Item("CanViewErrorLog"))
                HttpContext.Current.Session("CanTruncateErrorLog") = CBool(ds.Tables(0).Rows(0).Item("CanTruncateErrorLog"))
                HttpContext.Current.Session("CanShrinkDataBase") = CBool(ds.Tables(0).Rows(0).Item("CanShrinkDataBase"))
                HttpContext.Current.Session("CanRunFullScript") = CBool(ds.Tables(0).Rows(0).Item("CanRunFullScript"))
                HttpContext.Current.Session("CanRunAlterScript") = CBool(ds.Tables(0).Rows(0).Item("CanRunAlterScript"))
            End If

            Return ""

        Catch ex As Exception
            'Global_asax.CatchException(ex, Context)
            Return ex.Message.ToString
        End Try
    End Function

    <WebMethod>
    Public Shared Function SwitchToApplicant(strEmail As String) As String
        Dim objApplicant As New clsApplicant
        Try
            Dim strUserId As String = Membership.GetUser(strEmail).ProviderUserKey.ToString
            If Roles.IsUserInRole(HttpContext.Current.Session("email").ToString, "superadmin") = True Then
                If HttpContext.Current.Session("CompCode") Is Nothing OrElse HttpContext.Current.Session("CompCode").ToString.Trim = "" Then
                    Return "Sorry, Please select company from Home page."
                    Exit Function
                Else
                    HttpContext.Current.Session("applicantunkid") = -999
                    Return ""
                    Exit Function
                End If
            End If

            HttpContext.Current.Session("applicantunkid") = objApplicant.GetApplicantUnkid(HttpContext.Current.Session("CompCode").ToString, CInt(HttpContext.Current.Session("companyunkid")), strUserId, strEmail)

            If CInt(HttpContext.Current.Session("applicantunkid")) <= 0 Then
                HttpContext.Current.Session("applicantunkid") = objApplicant.AssignNewCompany(HttpContext.Current.Session("CompCode").ToString, CInt(HttpContext.Current.Session("companyunkid")), strEmail)
                If CInt(HttpContext.Current.Session("applicantunkid")) <= 0 Then
                    Return "Sorry, You are not authorized to view Applicant panel."
                    Exit Function
                End If
            End If

            HttpContext.Current.Session("email") = strEmail
            HttpContext.Current.Session("PreviewApplicantUnkid") = HttpContext.Current.Session("applicantunkid")
            HttpContext.Current.Session("PreviewCompCode") = HttpContext.Current.Session("CompCode")
            HttpContext.Current.Session("Previewcompanyunkid") = HttpContext.Current.Session("companyunkid")

            Return ""

        Catch ex As Exception
            'Global_asax.CatchException(ex, Context)
            Return ex.Message.ToString
        End Try
    End Function
    'Sohail (16 Aug 2019) -- End

    <WebMethod>
    Public Shared Function DeleteExpiredSession() As String
        Try

            clsSACommon.DeleteExpiredSessions()

            Return ""

        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function

    Public Shared Function RemoveRTFFormatting(ByVal rtfContent As String) As String
        rtfContent = rtfContent.Trim()
        Dim rtfRegEx As Regex = New Regex("({\\)(.+?)(})|(\\)(.+?)(\b)", RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Singleline Or RegexOptions.ExplicitCapture Or RegexOptions.IgnorePatternWhitespace Or RegexOptions.Compiled)
        Dim output As String = rtfRegEx.Replace(rtfContent, String.Empty)
        output = Regex.Replace(output, "\}", String.Empty)
        'Return output.Remove(output.Length - 1)
        Return output.Trim
    End Function
End Class
