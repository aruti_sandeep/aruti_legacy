﻿Option Strict On
Imports System.Runtime.Caching

Public Class ApplicantReference
    Inherits Base_Page

#Region "Provate Variable"
    Private objReference As New clsApplicantReference
#End Region

#Region " Method Functions "

    Private Sub FillCombo()
        Dim dsCombo As DataSet
        Try
            dsCombo = (New clsContactDetails).GetCountry()
            With ddlRefCountry
                .DataValueField = "countryunkid"
                .DataTextField = "Country_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                Call ddlRefCountry_SelectedIndexChanged(ddlRefCountry, New System.EventArgs)
            End With

            dsCombo = (New clsPersonalInfo).GetGender()
            With drpRefGender
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = (New clsApplicant).GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.RELATIONS)
            With drpReferenceType
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally

        End Try
    End Sub

    Private Sub GetValue()
        Try
            Call FillApplicantReference()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub FillApplicantReference(Optional blnRefreshCache As Boolean = False)
        'Dim dsList As DataSet
        Try
            'If blnRefreshCache = True Then
            '    Dim strCacheKey As String = "AppRef_" & CStr(Session("CompCode")) & "_" & CInt(Session("companyunkid")).ToString & "_" & CInt(Session("applicantunkid")).ToString & "_" & CInt(clsCommon_Master.enCommonMaster.RELATIONS).ToString
            '    Dim cache As ObjectCache = MemoryCache.Default
            '    If cache(strCacheKey) IsNot Nothing Then
            '        cache.Remove(strCacheKey)
            '    End If
            'End If
            'dsList = clsApplicantReference.GetApplicantReference(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.RELATIONS)
            'grdReference.DataSource = dsList.Tables(0)
            'grdReference.DataBind()

            objodsReference.SelectParameters.Item("strCompCode").DefaultValue = Session("CompCode").ToString
            objodsReference.SelectParameters.Item("intComUnkID").DefaultValue = CInt(Session("companyunkid")).ToString
            objodsReference.SelectParameters.Item("intApplicantUnkId").DefaultValue = CInt(Session("applicantunkid")).ToString
            objodsReference.SelectParameters.Item("intRelation_id").DefaultValue = CStr(clsCommon_Master.enCommonMaster.RELATIONS)
            If Session("ReferenceAddDate") Is Nothing OrElse blnRefreshCache = True Then
                Session("ReferenceAddDate") = DateAndTime.Now.ToString
            End If
            objodsReference.SelectParameters.Item("dtCurrentDate").DefaultValue = CStr(Session("ReferenceAddDate"))

            lvReference.DataBind()

            Dim s4 As Site4 = CType(Me.Master, Site4)
            s4.MarkStarToMenuName(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Function IsValidated() As Boolean
        'lblError.Visible = fa 
        Try
            If txtRefName.Text.Trim = "" Then
                ShowMessage(lblRefNameMsg.Text, MessageType.Info)
                txtRefName.Focus()
                Return False
            ElseIf txtRefPosition.Text.Trim = "" AndAlso CBool(Session("ReferencePositionMandatory")) = True Then
                ShowMessage(lblRefPositionMsg.Text, MessageType.Info)
                txtRefPosition.Focus()
                Return False
            ElseIf CInt(drpReferenceType.SelectedValue) <= 0 AndAlso CBool(Session("ReferenceTypeMandatory")) = True Then
                ShowMessage(lblReferenceTypeMsg.Text, MessageType.Info)
                drpReferenceType.Focus()
                Return False
            ElseIf txtRefEmail.Text.Trim = "" AndAlso CBool(Session("ReferenceEmailMandatory")) = True Then
                ShowMessage(lblRefEmailMsg.Text, MessageType.Info)
                txtRefEmail.Focus()
                Return False
            ElseIf txtRefEmail.Text.Trim <> "" AndAlso clsApplicant.IsValidEmail(txtRefEmail.Text) = False Then
                ShowMessage(lblRefEmailMsg2.Text, MessageType.Info)
                txtRefEmail.Focus()
                Return False
            ElseIf txtRefAddress.Text.Trim = "" AndAlso CBool(Session("ReferenceAddressMandatory")) = True Then
                ShowMessage(lblRefAddressMsg.Text, MessageType.Info)
                txtRefAddress.Focus()
                Return False
            ElseIf CInt(ddlRefCountry.SelectedValue) <= 0 AndAlso CBool(Session("ReferenceCountryMandatory")) = True Then
                ShowMessage(lblRefCountryMsg.Text, MessageType.Info)
                ddlRefCountry.Focus()
                Return False
            ElseIf CInt(ddlRefState.SelectedValue) <= 0 AndAlso CBool(Session("ReferenceStateMandatory")) = True Then
                ShowMessage(lblRefStateMsg.Text, MessageType.Info)
                ddlRefState.Focus()
                Return False
            ElseIf CInt(ddlRefTown.SelectedValue) <= 0 AndAlso CBool(Session("ReferenceCityMandatory")) = True Then
                ShowMessage(lblRefTownMsg.Text, MessageType.Info)
                ddlRefTown.Focus()
                Return False
            ElseIf CInt(drpRefGender.SelectedValue) <= 0 AndAlso CBool(Session("ReferenceGenederMandatory")) = True Then
                ShowMessage(lblRefGenderMsg.Text, MessageType.Info)
                drpRefGender.Focus()
                Return False
            ElseIf txtRefMobile.Text.Trim = "" AndAlso CBool(Session("ReferenceMobileMandatory")) = True Then
                ShowMessage(lblRefMobileMsg.Text, MessageType.Info)
                txtRefMobile.Focus()
                Return False
            ElseIf txtRefTelNo.Text.Trim = "" AndAlso CBool(Session("ReferenceTelNoMandatory")) = True Then
                ShowMessage(lblRefTelNoMsg.Text, MessageType.Info)
                txtRefTelNo.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
            Return False
        End Try
    End Function

    Private Sub Reset_ReferenceHistory()
        Try
            txtRefName.Text = ""
            txtRefAddress.Text = ""
            txtRefEmail.Text = ""
            txtRefMobile.Text = ""
            txtRefPosition.Text = ""
            txtRefTelNo.Text = ""
            ddlRefCountry.SelectedValue = "0"
            ddlRefState.SelectedValue = "0"
            ddlRefTown.SelectedValue = "0"
            drpRefGender.SelectedValue = "0"
            drpReferenceType.SelectedValue = "0"
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Form Event(S) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()
                Call GetValue()

                If CBool(Session("ReferencePositionMandatory")) = True Then
                    'rfvRefPosition.ValidationGroup = "Reference"
                    If CInt(HttpContext.Current.Session("RecruitMandatoryReferences")) > 0 Then
                        lblRefPosition.CssClass = "required"
                    Else
                        lblRefPosition.CssClass = ""
                    End If
                Else
                    'rfvRefPosition.ValidationGroup = "Nothing"
                    lblRefPosition.CssClass = ""
                End If
                If CBool(Session("ReferenceTypeMandatory")) = True Then
                    'rfvReferenceType.ValidationGroup = "Reference"
                    If CInt(HttpContext.Current.Session("RecruitMandatoryReferences")) > 0 Then
                        lblReferenceType.CssClass = "required"
                    Else
                        lblReferenceType.CssClass = ""
                    End If
                Else
                    'rfvReferenceType.ValidationGroup = "Nothing"
                    lblReferenceType.CssClass = ""
                End If
                If CBool(Session("ReferenceEmailMandatory")) = True Then
                    'rfvRefEmail.ValidationGroup = "Reference"
                    If CInt(HttpContext.Current.Session("RecruitMandatoryReferences")) > 0 Then
                        lblRefEmail.CssClass = "required"
                    Else
                        lblRefEmail.CssClass = ""
                    End If
                Else
                    'rfvRefEmail.ValidationGroup = "Nothing"
                    lblRefEmail.CssClass = ""
                End If
                If CBool(Session("ReferenceAddressMandatory")) = True Then
                    'rfvRefAddress.ValidationGroup = "Reference"
                    If CInt(HttpContext.Current.Session("RecruitMandatoryReferences")) > 0 Then
                        lblRefAddress.CssClass = "required"
                    Else
                        lblRefAddress.CssClass = ""
                    End If
                Else
                    'rfvRefAddress.ValidationGroup = "Nothing"
                    lblRefAddress.CssClass = ""
                End If
                If CBool(Session("ReferenceCountryMandatory")) = True Then
                    'rfvRefCountry.ValidationGroup = "Reference"
                    If CInt(HttpContext.Current.Session("RecruitMandatoryReferences")) > 0 Then
                        lblRefCountry.CssClass = "required"
                    Else
                        lblRefCountry.CssClass = ""
                    End If
                Else
                    'rfvRefCountry.ValidationGroup = "Nothing"
                    lblRefCountry.CssClass = ""
                End If
                If CBool(Session("ReferenceStateMandatory")) = True Then
                    'rfvRefState.ValidationGroup = "Reference"
                    If CInt(HttpContext.Current.Session("RecruitMandatoryReferences")) > 0 Then
                        lblRefState.CssClass = "required"
                    Else
                        lblRefState.CssClass = ""
                    End If
                Else
                    'rfvRefState.ValidationGroup = "Nothing"
                    lblRefState.CssClass = ""
                End If
                If CBool(Session("ReferenceCityMandatory")) = True Then
                    'rfvRefTown.ValidationGroup = "Reference"
                    If CInt(HttpContext.Current.Session("RecruitMandatoryReferences")) > 0 Then
                        lblRefTown.CssClass = "required"
                    Else
                        lblRefTown.CssClass = ""
                        'pnlRefTown.Visible = False
                    End If
                Else
                    'rfvRefTown.ValidationGroup = "Nothing"
                    lblRefTown.CssClass = ""
                    pnlRefTown.Visible = False
                End If
                If CBool(Session("ReferenceGenederMandatory")) = True Then
                    'rfvRefGender.ValidationGroup = "Reference"
                    If CInt(HttpContext.Current.Session("RecruitMandatoryReferences")) > 0 Then
                        lblRefGender.CssClass = "required"
                    Else
                        lblRefGender.CssClass = ""
                    End If
                Else
                    'rfvRefGender.ValidationGroup = "Nothing"
                    lblRefGender.CssClass = ""
                End If
                If CBool(Session("ReferenceMobileMandatory")) = True Then
                    'rfvRefMobile.ValidationGroup = "Reference"
                    If CInt(HttpContext.Current.Session("RecruitMandatoryReferences")) > 0 Then
                        lblRefMobile.CssClass = "required"
                    Else
                        lblRefMobile.CssClass = ""
                    End If
                Else
                    'rfvRefMobile.ValidationGroup = "Nothing"
                    lblRefMobile.CssClass = ""
                End If
                If CBool(Session("ReferenceTelNoMandatory")) = True Then
                    'rfvRefTelNo.ValidationGroup = "Reference"
                    If CInt(HttpContext.Current.Session("RecruitMandatoryReferences")) > 0 Then
                        lblRefTelNo.CssClass = "required"
                    Else
                        lblRefTelNo.CssClass = ""
                    End If
                Else
                    'rfvRefTelNo.ValidationGroup = "Nothing"
                    lblRefTelNo.CssClass = ""
                End If
            End If

            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            'LanguageOpner.Visible = False
            'LanguageControl.Visible = False 'Sohail (05 May 2021)
            Dim lo As Boolean = False
            Dim lc As Boolean = False
            If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner.Visible = True
                        lo = True
                        lc = True
                    End If
                Else
                    arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner.Visible = True
                            lo = True
                            lc = True
                        End If
                    End If
                End If
            ElseIf Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                    'LanguageOpner.Visible = True
                    lo = True
                    lc = True
                End If
                btnAddReference.Enabled = False
            End If
            If lo = False Then LanguageOpner.Visible = False
            If lc = False Then LanguageControl.Visible = False
            'Sohail (16 Aug 2019) -- End
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub ApplicantReference_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

#Region " Button Event(S) "

    Private Sub btnAddReference_Click(sender As Object, e As EventArgs) Handles btnAddReference.Click
        'lblError.Visible = False
        Try

            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidated() = False Then Exit Try

            If objReference.IsExistApplicantReference(strCompCode:=Session("CompCode").ToString,
                                                      intComUnkID:=CInt(Session("companyunkid")),
                                                      intApplicantUnkid:=CInt(Session("applicantunkid")),
                                                      intReferenceTranUnkId:=-1,
                                                      strName:=txtRefName.Text
                                                    ) = True Then

                ShowMessage(lblbtnAddReferenceMsg1.Text, MessageType.Info)
                Call FillApplicantReference()
                txtRefName.Focus()
                Exit Try
            End If


            If objReference.AddApplicantReference(strCompCode:=Session("CompCode").ToString,
                                                  intComUnkID:=CInt(Session("companyunkid")),
                                                  intApplicantUnkid:=CInt(Session("applicantunkid")),
                                                  strRefName:=txtRefName.Text.Trim,
                                                  strAddress:=txtRefAddress.Text.Trim,
                                                  intCountryUnkId:=CInt(ddlRefCountry.SelectedValue),
                                                  intStateUnkId:=CInt(ddlRefState.SelectedValue),
                                                  intCityUnkid:=CInt(ddlRefTown.SelectedValue),
                                                  strEmail:=txtRefEmail.Text.Trim,
                                                  strGeneder:=drpRefGender.SelectedValue,
                                                  strPosition:=txtRefPosition.Text.Trim,
                                                  strTelNo:=txtRefTelNo.Text.Trim,
                                                  strMobile:=txtRefMobile.Text.Trim,
                                                  intRelationUnkId:=CInt(drpReferenceType.SelectedValue),
                                                  dtCreated_date:=Now) = True Then
                ShowMessage(lblbtnAddReferenceMsg2.Text, MessageType.Info)
            End If

            Call FillApplicantReference(True)
            Call Reset_ReferenceHistory()

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            'LanguageControl.Visible = True
            LanguageControl.show()
        End If
    End Sub
    'Sohail (16 Aug 2019) -- End
#End Region

#Region " Control(S) Event(S) "

    Private Sub ddlRefCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRefCountry.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            dsCombo = (New clsContactDetails).GetState(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(ddlRefCountry.SelectedValue))
            With ddlRefState
                .DataValueField = "stateunkid"
                .DataTextField = "state_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                Call ddlRefState_SelectedIndexChanged(ddlRefState, New System.EventArgs)
            End With
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub ddlRefState_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRefState.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            dsCombo = (New clsContactDetails).GetCity(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(ddlRefState.SelectedValue))
            With ddlRefTown
                .DataValueField = "cityunkid"
                .DataTextField = "city_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Protected Sub objddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    Dim dsCombo As DataSet
    '    Try
    '        dsCombo = (New clsContactDetails).GetState(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(DirectCast(sender, DropDownList).SelectedValue))
    '        Dim dp As DropDownList = DirectCast(grdReference.Rows(grdReference.EditIndex).FindControl("objddlState"), DropDownList)
    '        With dp
    '            .DataValueField = "stateunkid"
    '            .DataTextField = "state_name"
    '            .DataSource = dsCombo.Tables(0)
    '            .DataBind()
    '            .SelectedValue = "0"
    '            Call objddlState_SelectedIndexChanged(dp, New System.EventArgs)
    '        End With
    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    'Protected Sub objddlState_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    Dim dsCombo As DataSet
    '    Try
    '        dsCombo = (New clsContactDetails).GetCity(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(DirectCast(sender, DropDownList).SelectedValue))
    '        Dim dp As DropDownList = DirectCast(grdReference.Rows(grdReference.EditIndex).FindControl("objddlCity"), DropDownList)
    '        With dp
    '            .DataValueField = "cityunkid"
    '            .DataTextField = "city_name"
    '            .DataSource = dsCombo.Tables(0)
    '            .DataBind()
    '            .SelectedValue = "0"
    '        End With
    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    Protected Sub objddllvCountry_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim dsCombo As DataSet
        Try
            dsCombo = (New clsContactDetails).GetState(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(DirectCast(sender, DropDownList).SelectedValue))
            Dim dp As DropDownList = DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objddllvState"), DropDownList)
            With dp
                .DataValueField = "stateunkid"
                .DataTextField = "state_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                Call objddllvState_SelectedIndexChanged(dp, New System.EventArgs)
            End With
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Protected Sub objddllvState_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim dsCombo As DataSet
        Try
            dsCombo = (New clsContactDetails).GetCity(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(DirectCast(sender, DropDownList).SelectedValue))
            Dim dp As DropDownList = DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objddllvCity"), DropDownList)
            With dp
                .DataValueField = "cityunkid"
                .DataTextField = "city_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Datagridview Events "

    'Private Sub grdReference_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdReference.RowDataBound
    '    Dim dsCombo As DataSet
    '    Try
    '        Dim drv As DataRowView = CType(e.Row.DataItem, DataRowView)
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            If e.Row.RowState = DataControlRowState.Edit OrElse e.Row.RowState = 5 Then '5 = Alternate Or Edit
    '                DirectCast(e.Row.FindControl("objddlCountry"), DropDownList).Visible = True

    '                Dim dpCountry As DropDownList = DirectCast(e.Row.FindControl("objddlCountry"), DropDownList)
    '                dpCountry.Visible = True
    '                dsCombo = (New clsContactDetails).GetCountry()
    '                With dpCountry
    '                    .DataValueField = "countryunkid"
    '                    .DataTextField = "Country_name"
    '                    .DataSource = dsCombo.Tables(0)
    '                    .DataBind()
    '                    .SelectedValue = drv.Item("countryid").ToString
    '                End With

    '                Dim dpState As DropDownList = DirectCast(e.Row.FindControl("objddlState"), DropDownList)
    '                dpState.Visible = True
    '                dsCombo = (New clsContactDetails).GetState(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(dpCountry.SelectedValue))
    '                With dpState
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "state_name"
    '                    .DataSource = dsCombo.Tables(0)
    '                    .DataBind()
    '                    .SelectedValue = drv.Item("stateid").ToString
    '                End With

    '                Dim dpCity As DropDownList = DirectCast(e.Row.FindControl("objddlCity"), DropDownList)
    '                dpCity.Visible = True
    '                dsCombo = (New clsContactDetails).GetCity(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(dpState.SelectedValue))
    '                With dpCity
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "city_name"
    '                    .DataSource = dsCombo.Tables(0)
    '                    .DataBind()
    '                    .SelectedValue = drv.Item("cityid").ToString
    '                End With

    '                Dim dpGender As DropDownList = DirectCast(e.Row.FindControl("objddlGender"), DropDownList)
    '                dpGender.Visible = True
    '                dsCombo = (New clsPersonalInfo).GetGender()
    '                With dpGender
    '                    .DataValueField = "Id"
    '                    .DataTextField = "Name"
    '                    .DataSource = dsCombo.Tables(0)
    '                    .DataBind()
    '                    .SelectedValue = drv.Item("gender").ToString
    '                End With

    '                Dim dpRefType As DropDownList = DirectCast(e.Row.FindControl("objddlRefType"), DropDownList)
    '                dpRefType.Visible = True
    '                dsCombo = (New clsApplicant).GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.RELATIONS)
    '                With dpRefType
    '                    .DataValueField = "masterunkid"
    '                    .DataTextField = "Name"
    '                    .DataSource = dsCombo.Tables(0)
    '                    .DataBind()
    '                    .SelectedValue = drv.Item("relationunkid").ToString
    '                End With
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Dim a As String = ex.Message
    '    End Try
    'End Sub

    'Private Sub grdReference_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles grdReference.RowEditing
    '    Try
    '        grdReference.EditIndex = e.NewEditIndex
    '        Call FillApplicantReference()
    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    'Private Sub grdReference_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles grdReference.RowCancelingEdit
    '    Try
    '        grdReference.EditIndex = -1
    '        Call FillApplicantReference()
    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    'Private Sub grdReference_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles grdReference.RowUpdating
    '    Try
    '        '** To Prevent Buttun Click when Page is Refreshed by User. **
    '        If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
    '        Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

    '        Dim intCountry, intState, intCity, intGender, intRefType As Integer
    '        intCountry = 0 : intState = 0 : intCity = 0 : intGender = 0 : intRefType = 0

    '        Integer.TryParse(DirectCast(grdReference.Rows(e.RowIndex).FindControl("objddlRefType"), DropDownList).SelectedValue, intRefType)
    '        Integer.TryParse(DirectCast(grdReference.Rows(e.RowIndex).FindControl("objddlCountry"), DropDownList).SelectedValue, intCountry)
    '        Integer.TryParse(DirectCast(grdReference.Rows(e.RowIndex).FindControl("objddlState"), DropDownList).SelectedValue, intState)
    '        Integer.TryParse(DirectCast(grdReference.Rows(e.RowIndex).FindControl("objddlCity"), DropDownList).SelectedValue, intCity)
    '        Integer.TryParse(DirectCast(grdReference.Rows(e.RowIndex).FindControl("objddlGender"), DropDownList).SelectedValue, intGender)

    '        Dim strname, strPosition, strEmail, strAddress, strTelno, strMobile As String
    '        strname = "" : strPosition = "" : strEmail = "" : strAddress = "" : strTelno = "" : strMobile = ""

    '        strname = DirectCast(grdReference.Rows(e.RowIndex).FindControl("objtxtName"), TextBox).Text
    '        strPosition = DirectCast(grdReference.Rows(e.RowIndex).FindControl("objtxtPosition"), TextBox).Text
    '        strEmail = DirectCast(grdReference.Rows(e.RowIndex).FindControl("objtxtEmail"), TextBox).Text
    '        strAddress = DirectCast(grdReference.Rows(e.RowIndex).FindControl("objtxtAddress"), TextBox).Text
    '        strTelno = DirectCast(grdReference.Rows(e.RowIndex).FindControl("objtxtTelNo"), TextBox).Text
    '        strMobile = DirectCast(grdReference.Rows(e.RowIndex).FindControl("objtxtMobile"), TextBox).Text

    '        If strname.Trim = "" Then
    '            ShowMessage(lblRefNameMsg.Text, MessageType.Info)
    '            grdReference.Rows(e.RowIndex).FindControl("objtxtName").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf strPosition.Trim = "" AndAlso CBool(Session("ReferencePositionMandatory")) = True Then
    '            ShowMessage(lblRefPositionMsg.Text, MessageType.Info)
    '            grdReference.Rows(e.RowIndex).FindControl("objtxtPosition").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf intRefType <= 0 AndAlso CBool(Session("ReferenceTypeMandatory")) = True Then
    '            ShowMessage(lblReferenceTypeMsg.Text, MessageType.Info)
    '            grdReference.Rows(e.RowIndex).FindControl("objddlRefType").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf strEmail.Trim = "" AndAlso CBool(Session("ReferenceEmailMandatory")) = True Then
    '            ShowMessage(lblRefEmailMsg.Text, MessageType.Info)
    '            grdReference.Rows(e.RowIndex).FindControl("objtxtEmail").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf strEmail.Trim <> "" AndAlso clsApplicant.IsValidEmail(strEmail) = False Then
    '            ShowMessage(lblRefEmailMsg2.Text, MessageType.Info)
    '            grdReference.Rows(e.RowIndex).FindControl("objtxtEmail").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf strAddress.Trim = "" AndAlso CBool(Session("ReferenceAddressMandatory")) = True Then
    '            ShowMessage(lblRefAddressMsg.Text, MessageType.Info)
    '            grdReference.Rows(e.RowIndex).FindControl("objtxtAddress").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf intCountry <= 0 AndAlso CBool(Session("ReferenceCountryMandatory")) = True Then
    '            ShowMessage(lblRefCountryMsg.Text, MessageType.Info)
    '            grdReference.Rows(e.RowIndex).FindControl("objddlCountry").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf intState <= 0 AndAlso CBool(Session("ReferenceStateMandatory")) = True Then
    '            ShowMessage(lblRefStateMsg.Text, MessageType.Info)
    '            grdReference.Rows(e.RowIndex).FindControl("objddlState").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf intCity <= 0 AndAlso CBool(Session("ReferenceCityMandatory")) = True Then
    '            ShowMessage(lblRefTownMsg.Text, MessageType.Info)
    '            grdReference.Rows(e.RowIndex).FindControl("objddlCity").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf intGender <= 0 AndAlso CBool(Session("ReferenceGenederMandatory")) = True Then
    '            ShowMessage(lblRefGenderMsg.Text, MessageType.Info)
    '            grdReference.Rows(e.RowIndex).FindControl("objddlGender").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf strMobile.Trim = "" AndAlso CBool(Session("ReferenceMobileMandatory")) = True Then
    '            ShowMessage(lblRefMobileMsg.Text, MessageType.Info)
    '            grdReference.Rows(e.RowIndex).FindControl("objtxtMobile").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        ElseIf strTelno.Trim = "" AndAlso CBool(Session("ReferenceTelNoMandatory")) = True Then
    '            ShowMessage(lblRefTelNoMsg.Text, MessageType.Info)
    '            grdReference.Rows(e.RowIndex).FindControl("objtxtTelNo").Focus()
    '            e.Cancel = True
    '            Exit Try
    '        End If


    '        If objReference.IsExistApplicantReference(strCompCode:=Session("CompCode").ToString,
    '                                                  intComUnkID:=CInt(Session("companyunkid")),
    '                                                  intApplicantUnkid:=CInt(Session("applicantunkid")),
    '                                                  intReferenceTranUnkId:=CInt(e.Keys("referencetranunkid")),
    '                                                  strName:=strname) = True Then

    '            ShowMessage(lblgrdReferenceRowUpdatingMsg1.Text, MessageType.Info)
    '            Exit Try
    '        End If

    '        If objReference.EditApplicantReference(strCompCode:=Session("CompCode").ToString,
    '                                               intComUnkID:=CInt(Session("companyunkid")),
    '                                               intApplicantUnkid:=CInt(Session("applicantunkid")),
    '                                               intReferenceTranUnkId:=CInt(e.Keys("referencetranunkid")),
    '                                               strRefName:=strname,
    '                                               strAddress:=strAddress,
    '                                               intCountryUnkId:=intCountry,
    '                                               intStateUnkId:=intState,
    '                                               intCityUnkid:=intCity,
    '                                               strEmail:=strEmail,
    '                                               intGeneder:=intGender,
    '                                               strPosition:=strPosition,
    '                                               strTelNo:=strTelno,
    '                                               strMobile:=strMobile,
    '                                               intRelationUnkId:=intRefType
    '                                               ) = True Then

    '            ShowMessage(lblgrdReferenceRowUpdatingMsg2.Text, MessageType.Info)
    '            grdReference.EditIndex = -1
    '            Call FillApplicantReference()
    '        End If

    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    'Private Sub grdReference_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grdReference.RowDeleting
    '    Try

    '        '** To Prevent Buttun Click when Page is Refreshed by User. **
    '        If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
    '        Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

    '        If objReference.DeleteApplicantReference(strCompCode:=Session("CompCode").ToString,
    '                                                 intComUnkID:=CInt(Session("companyunkid")),
    '                                                 intApplicantUnkid:=CInt(Session("applicantunkid")),
    '                                                 intReferenceTranUnkId:=CInt(e.Keys("referencetranunkid"))
    '                                                 ) = True Then

    '            ShowMessage(lblgrdReferenceRowDeletingMsg1.Text, MessageType.Info)

    '            Call FillApplicantReference()
    '        End If
    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    Private Sub lvReference_ItemDataBound(sender As Object, e As ListViewItemEventArgs) Handles lvReference.ItemDataBound
        Dim dsCombo As DataSet
        Try
            If e.Item.ItemType = ListViewItemType.DataItem AndAlso lvReference.EditIndex = -1 Then
                Dim pnllvItemCity As Panel = DirectCast(e.Item.FindControl("pnllvItemCity"), Panel)

                If CBool(Session("ReferenceCityMandatory")) = False Then
                    pnllvItemCity.Visible = False
                End If

            End If
            If e.Item.ItemType = ListViewItemType.DataItem AndAlso lvReference.EditIndex >= 0 AndAlso e.Item.DataItemIndex = lvReference.EditIndex Then
                Dim pnllvItemCity As Panel = DirectCast(e.Item.FindControl("pnllvItemCity"), Panel)

                If CBool(Session("ReferenceCityMandatory")) = False Then
                    pnllvItemCity.Visible = False
                End If

                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)

                Dim dpCountry As DropDownList = DirectCast(e.Item.FindControl("objddllvCountry"), DropDownList)
                dpCountry.Visible = True
                dsCombo = (New clsContactDetails).GetCountry()
                With dpCountry
                    .DataValueField = "countryunkid"
                    .DataTextField = "Country_name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                    .SelectedValue = drv.Item("countryid").ToString
                End With

                Dim dpState As DropDownList = DirectCast(e.Item.FindControl("objddllvState"), DropDownList)
                dpState.Visible = True
                dsCombo = (New clsContactDetails).GetState(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(dpCountry.SelectedValue))
                With dpState
                    .DataValueField = "stateunkid"
                    .DataTextField = "state_name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                    .SelectedValue = drv.Item("stateid").ToString
                End With

                Dim dpCity As DropDownList = DirectCast(e.Item.FindControl("objddllvCity"), DropDownList)
                dpCity.Visible = True
                dsCombo = (New clsContactDetails).GetCity(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(dpState.SelectedValue))
                With dpCity
                    .DataValueField = "cityunkid"
                    .DataTextField = "city_name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                    .SelectedValue = drv.Item("cityid").ToString
                End With

                Dim dpGender As DropDownList = DirectCast(e.Item.FindControl("objddllvGender"), DropDownList)
                dpGender.Visible = True
                dsCombo = (New clsPersonalInfo).GetGender()
                With dpGender
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                    .SelectedValue = drv.Item("gender").ToString
                End With

                Dim dpRefType As DropDownList = DirectCast(e.Item.FindControl("objddllvRefType"), DropDownList)
                dpRefType.Visible = True
                dsCombo = (New clsApplicant).GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.RELATIONS)
                With dpRefType
                    .DataValueField = "masterunkid"
                    .DataTextField = "Name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                    .SelectedValue = drv.Item("relationunkid").ToString
                End With
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lvReference_ItemEditing(sender As Object, e As ListViewEditEventArgs) Handles lvReference.ItemEditing
        Try
            lvReference.EditIndex = e.NewEditIndex
            lvReference.DataBind()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lvReference_ItemUpdating(sender As Object, e As ListViewUpdateEventArgs) Handles lvReference.ItemUpdating
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            Dim intCountry, intState, intCity, intGender, intRefType As Integer
            intCountry = 0 : intState = 0 : intCity = 0 : intGender = 0 : intRefType = 0

            Integer.TryParse(DirectCast(lvReference.Items(e.ItemIndex).FindControl("objddllvRefType"), DropDownList).SelectedValue, intRefType)
            Integer.TryParse(DirectCast(lvReference.Items(e.ItemIndex).FindControl("objddllvCountry"), DropDownList).SelectedValue, intCountry)
            Integer.TryParse(DirectCast(lvReference.Items(e.ItemIndex).FindControl("objddllvState"), DropDownList).SelectedValue, intState)
            Integer.TryParse(DirectCast(lvReference.Items(e.ItemIndex).FindControl("objddllvCity"), DropDownList).SelectedValue, intCity)
            Integer.TryParse(DirectCast(lvReference.Items(e.ItemIndex).FindControl("objddllvGender"), DropDownList).SelectedValue, intGender)

            Dim strname, strPosition, strEmail, strAddress, strTelno, strMobile As String
            strname = "" : strPosition = "" : strEmail = "" : strAddress = "" : strTelno = "" : strMobile = ""

            strname = DirectCast(lvReference.Items(e.ItemIndex).FindControl("objtxtlvName"), TextBox).Text
            strPosition = DirectCast(lvReference.Items(e.ItemIndex).FindControl("objtxtlvPosition"), TextBox).Text
            strEmail = DirectCast(lvReference.Items(e.ItemIndex).FindControl("objtxtlvEmail"), TextBox).Text
            strAddress = DirectCast(lvReference.Items(e.ItemIndex).FindControl("objtxtlvAddress"), TextBox).Text
            strTelno = DirectCast(lvReference.Items(e.ItemIndex).FindControl("objtxtlvTelNo"), TextBox).Text
            strMobile = DirectCast(lvReference.Items(e.ItemIndex).FindControl("objtxtlvMobile"), TextBox).Text

            If strname.Trim = "" Then
                ShowMessage(lblRefNameMsg.Text, MessageType.Info)
                lvReference.Items(e.ItemIndex).FindControl("objtxtlvName").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strPosition.Trim = "" AndAlso CBool(Session("ReferencePositionMandatory")) = True Then
                ShowMessage(lblRefPositionMsg.Text, MessageType.Info)
                lvReference.Items(e.ItemIndex).FindControl("objtxtlvPosition").Focus()
                e.Cancel = True
                Exit Try
            ElseIf intRefType <= 0 AndAlso CBool(Session("ReferenceTypeMandatory")) = True Then
                ShowMessage(lblReferenceTypeMsg.Text, MessageType.Info)
                lvReference.Items(e.ItemIndex).FindControl("objddllvRefType").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strEmail.Trim = "" AndAlso CBool(Session("ReferenceEmailMandatory")) = True Then
                ShowMessage(lblRefEmailMsg.Text, MessageType.Info)
                lvReference.Items(e.ItemIndex).FindControl("objtxtlvEmail").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strEmail.Trim <> "" AndAlso clsApplicant.IsValidEmail(strEmail) = False Then
                ShowMessage(lblRefEmailMsg2.Text, MessageType.Info)
                lvReference.Items(e.ItemIndex).FindControl("objtxtlvEmail").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strAddress.Trim = "" AndAlso CBool(Session("ReferenceAddressMandatory")) = True Then
                ShowMessage(lblRefAddressMsg.Text, MessageType.Info)
                lvReference.Items(e.ItemIndex).FindControl("objtxtlvAddress").Focus()
                e.Cancel = True
                Exit Try
            ElseIf intCountry <= 0 AndAlso CBool(Session("ReferenceCountryMandatory")) = True Then
                ShowMessage(lblRefCountryMsg.Text, MessageType.Info)
                lvReference.Items(e.ItemIndex).FindControl("objddllvCountry").Focus()
                e.Cancel = True
                Exit Try
            ElseIf intState <= 0 AndAlso CBool(Session("ReferenceStateMandatory")) = True Then
                ShowMessage(lblRefStateMsg.Text, MessageType.Info)
                lvReference.Items(e.ItemIndex).FindControl("objddllvState").Focus()
                e.Cancel = True
                Exit Try
            ElseIf intCity <= 0 AndAlso CBool(Session("ReferenceCityMandatory")) = True Then
                ShowMessage(lblRefTownMsg.Text, MessageType.Info)
                lvReference.Items(e.ItemIndex).FindControl("objddllvCity").Focus()
                e.Cancel = True
                Exit Try
            ElseIf intGender <= 0 AndAlso CBool(Session("ReferenceGenederMandatory")) = True Then
                ShowMessage(lblRefGenderMsg.Text, MessageType.Info)
                lvReference.Items(e.ItemIndex).FindControl("objddllvGender").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strMobile.Trim = "" AndAlso CBool(Session("ReferenceMobileMandatory")) = True Then
                ShowMessage(lblRefMobileMsg.Text, MessageType.Info)
                lvReference.Items(e.ItemIndex).FindControl("objtxtlvMobile").Focus()
                e.Cancel = True
                Exit Try
            ElseIf strTelno.Trim = "" AndAlso CBool(Session("ReferenceTelNoMandatory")) = True Then
                ShowMessage(lblRefTelNoMsg.Text, MessageType.Info)
                lvReference.Items(e.ItemIndex).FindControl("objtxtlvTelNo").Focus()
                e.Cancel = True
                Exit Try
            End If


            If objReference.IsExistApplicantReference(strCompCode:=Session("CompCode").ToString,
                                                      intComUnkID:=CInt(Session("companyunkid")),
                                                      intApplicantUnkid:=CInt(Session("applicantunkid")),
                                                      intReferenceTranUnkId:=CInt(e.Keys("referencetranunkid")),
                                                      strName:=strname) = True Then

                ShowMessage(lblgrdReferenceRowUpdatingMsg1.Text, MessageType.Info)
                Exit Try
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub objodsReference_Updating(sender As Object, e As ObjectDataSourceMethodEventArgs) Handles objodsReference.Updating
        Try
            Dim intCountry, intState, intCity, intGender, intRefType As Integer
            intCountry = 0 : intState = 0 : intCity = 0 : intGender = 0 : intRefType = 0

            Integer.TryParse(DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objddllvRefType"), DropDownList).SelectedValue, intRefType)
            Integer.TryParse(DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objddllvCountry"), DropDownList).SelectedValue, intCountry)
            Integer.TryParse(DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objddllvState"), DropDownList).SelectedValue, intState)
            Integer.TryParse(DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objddllvCity"), DropDownList).SelectedValue, intCity)
            Integer.TryParse(DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objddllvGender"), DropDownList).SelectedValue, intGender)

            Dim strname, strPosition, strEmail, strAddress, strTelno, strMobile As String
            strname = "" : strPosition = "" : strEmail = "" : strAddress = "" : strTelno = "" : strMobile = ""

            strname = DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objtxtlvName"), TextBox).Text
            strPosition = DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objtxtlvPosition"), TextBox).Text
            strEmail = DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objtxtlvEmail"), TextBox).Text
            strAddress = DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objtxtlvAddress"), TextBox).Text
            strTelno = DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objtxtlvTelNo"), TextBox).Text
            strMobile = DirectCast(lvReference.Items(lvReference.EditIndex).FindControl("objtxtlvMobile"), TextBox).Text

            Dim objAppReferenceUpdate As New dtoAppReferenceUpdate
            With objAppReferenceUpdate
                .CompCode = Session("CompCode").ToString
                .ComUnkID = CInt(Session("companyunkid"))
                .ApplicantUnkid = CInt(Session("applicantunkid"))
                .ReferenceTranUnkId = CInt(lvReference.DataKeys(lvReference.EditItem.DataItemIndex).Item("referencetranunkid"))
                .RefName = strname
                .Address = strAddress
                .CountryUnkId = intCountry
                .StateUnkId = intState
                .CityUnkid = intCity
                .Email = strEmail
                .Geneder = intGender
                .Position = strPosition
                .TelNo = strTelno
                .Mobile = strMobile
                .RelationUnkId = intRefType
            End With
            e.InputParameters("objAppReferenceUpdate") = objAppReferenceUpdate


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub objodsReference_Updated(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles objodsReference.Updated
        Try
            Call FillApplicantReference(True)
            ShowMessage(lblgrdReferenceRowUpdatingMsg2.Text, MessageType.Info)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lvReference_ItemDeleting(sender As Object, e As ListViewDeleteEventArgs) Handles lvReference.ItemDeleting
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            objodsReference.DeleteParameters.Item("strCompCode").DefaultValue = Session("CompCode").ToString
            objodsReference.DeleteParameters.Item("intComUnkID").DefaultValue = CInt(Session("companyunkid")).ToString
            objodsReference.DeleteParameters.Item("intApplicantUnkid").DefaultValue = CInt(Session("applicantunkid")).ToString
            objodsReference.DeleteParameters.Item("intReferenceTranUnkId").DefaultValue = CInt(e.Keys("referencetranunkid")).ToString

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub objodsReference_Deleted(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles objodsReference.Deleted
        Try
            Call FillApplicantReference(True)
            ShowMessage(lblgrdReferenceRowDeletingMsg1.Text, MessageType.Info)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub lvReference_ItemCanceling(sender As Object, e As ListViewCancelEventArgs) Handles lvReference.ItemCanceling
        Try
            lvReference.EditIndex = -1
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


#End Region

End Class