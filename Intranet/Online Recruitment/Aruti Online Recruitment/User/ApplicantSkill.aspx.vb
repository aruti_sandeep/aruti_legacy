﻿Option Strict On

Imports System.Web.Services

Public Class ApplicantSkill
    Inherits Base_Page

    Private objAppSkill As New clsApplicantSkill

#Region " Method Functions "

    Private Sub FillCombo()
        Dim objApplicant As New clsApplicant
        Dim objSkill As New clsApplicantSkill
        Dim dsCombo As DataSet
        Try
            dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.SKILL_CATEGORY)
            With ddlskillCate
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
                Call ddlskillCate_SelectedIndexChanged(ddlskillCate, New System.EventArgs)
            End With

            dsCombo = objSkill.GetSkillExpertise(Session("CompCode").ToString, CInt(Session("companyunkid")))
            With cboSkillExpertise
                .DataValueField = "skillexpertiseunkid"
                .DataTextField = "expertise_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        Finally
            objApplicant = Nothing
        End Try
    End Sub

    Private Sub FillApplicantSkills(Optional blnRefreshCache As Boolean = False)
        Dim dsList As DataSet
        Try
            dsList = objAppSkill.GetApplicantSkills(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.SKILL_CATEGORY, CInt(Session("applicantunkid")), blnRefreshCache)

            grdviewSkill.DataSource = dsList.Tables(0)
            grdviewSkill.DataBind()
            Dim s4 As Site4 = CType(Me.Master, Site4)
            s4.MarkStarToMenuName(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Function IsValidated() As Boolean
        Try
            If chkOtherSkill.Checked = False AndAlso CInt(ddlskillCate.SelectedItem.Value) <= 0 Then
                ShowMessage(lblCategoryMsg.Text, MessageType.Info)
                ddlskillCate.Focus()
                Return False
            ElseIf chkOtherSkill.Checked = False AndAlso CInt(ddlskill.SelectedItem.Value) <= 0 Then
                ShowMessage(lblSkillMsg.Text, MessageType.Info)
                ddlskill.Focus()
                Return False
            ElseIf chkOtherSkill.Checked = True AndAlso txtOtherSkillCategory.Text.Trim = "" Then
                ShowMessage(lblOtherSkillCategoryMsg.Text, MessageType.Info)
                txtOtherSkillCategory.Focus()
                Return False
            ElseIf chkOtherSkill.Checked = True AndAlso txtOtherSkill.Text.Trim = "" Then
                ShowMessage(lblOtherSkillMsg.Text, MessageType.Info)
                txtOtherSkill.Focus()
                Return False
            ElseIf cboSkillExpertise.Items.Count > 1 AndAlso CInt(cboSkillExpertise.SelectedItem.Value) <= 0 Then
                ShowMessage(lblSkillExpertiseMsg.Text, MessageType.Info)
                cboSkillExpertise.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try
    End Function

    Private Sub GetValue()
        Try
            Call FillApplicantSkills()

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub Reset_Skill()
        Try
            ddlskillCate.SelectedValue = "0"
            ddlskill.SelectedValue = "0"
            cboSkillExpertise.SelectedValue = "0"
            txtRemarks.Text = ""

            txtOtherSkillCategory.Text = ""
            txtOtherSkill.Text = ""
            chkOtherSkill.Enabled = True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub
#End Region

#Region " Page Events "
    Private Sub ApplicantSkill_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()

                If cboSkillExpertise.Items.Count > 1 Then
                    'cboSkillExpertise.ValidationGroup = "Skill"
                    'rfvSkillExpertise.ValidationGroup = "Skill"
                    If CInt(HttpContext.Current.Session("RecruitMandatorySkills")) > 0 Then
                        lblSkillExpertise.CssClass = "required"
                    Else
                        lblSkillExpertise.CssClass = ""
                    End If
                Else
                    'cboSkillExpertise.ValidationGroup = "Nothing"
                    'rfvSkillExpertise.ValidationGroup = "Nothing"
                    lblSkillExpertise.CssClass = ""
                End If

                If CInt(HttpContext.Current.Session("RecruitMandatorySkills")) > 0 Then
                    lblCategory.CssClass = "required"
                    lblSkill.CssClass = "required"
                Else
                    lblCategory.CssClass = ""
                    lblSkill.CssClass = ""
                End If

                Call chkOtherSkill_CheckedChanged(chkOtherSkill, New System.EventArgs)
                Call GetValue()
            End If

            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            'LanguageOpner.Visible = False
            'LanguageControl.Visible = False 'Sohail (05 May 2021)
            Dim lo As Boolean = False
            Dim lc As Boolean = False
            If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner.Visible = True
                        lo = True
                        lc = True
                    End If
                Else
                    arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner.Visible = True
                            lo = True
                            lc = True
                        End If
                    End If
                End If
            ElseIf Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                    'LanguageOpner.Visible = True
                    lo = True
                    lc = True
                End If
                btnskillAdd.Enabled = False
            End If
            If lo = False Then LanguageOpner.Visible = False
            If lc = False Then LanguageControl.Visible = False
            'Sohail (16 Aug 2019) -- End

            'Me.LogOutPageURL = mstrUrlArutiLink

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

#End Region

#Region " Combobox Events "
    Private Sub ddlskillCate_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlskillCate.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            dsCombo = objAppSkill.GetSkills(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(ddlskillCate.SelectedValue))
            With ddlskill
                .DataValueField = "Skillunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Protected Sub objddlSkillCat_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim dsCombo As DataSet
        Try
            dsCombo = objAppSkill.GetSkills(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(DirectCast(sender, DropDownList).SelectedValue))
            Dim dp As DropDownList = DirectCast(grdviewSkill.Rows(grdviewSkill.EditIndex).FindControl("objddlSkill"), DropDownList)
            With dp
                .DataValueField = "Skillunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

#End Region

#Region " Datagridview Events "

    Private Sub grdviewSkill_RowEditing(sender As Object, e As GridViewEditEventArgs) Handles grdviewSkill.RowEditing
        Try
            grdviewSkill.EditIndex = e.NewEditIndex
            Call FillApplicantSkills()
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub grdviewSkill_RowCancelingEdit(sender As Object, e As GridViewCancelEditEventArgs) Handles grdviewSkill.RowCancelingEdit
        Try
            grdviewSkill.EditIndex = -1
            Call FillApplicantSkills()
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub grdviewSkill_RowUpdating(sender As Object, e As GridViewUpdateEventArgs) Handles grdviewSkill.RowUpdating
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            Dim intSkillCat As Integer = 0
            Dim intSkill As Integer = 0
            Dim intSkillExpertise As Integer = 0
            Integer.TryParse(DirectCast(grdviewSkill.Rows(e.RowIndex).FindControl("objddlSkillCat"), DropDownList).SelectedValue, intSkillCat)
            Integer.TryParse(DirectCast(grdviewSkill.Rows(e.RowIndex).FindControl("objddlSkill"), DropDownList).SelectedValue, intSkill)
            Integer.TryParse(DirectCast(grdviewSkill.Rows(e.RowIndex).FindControl("objcboSkillExpertise"), DropDownList).SelectedValue, intSkillExpertise)

            Dim txtRemark As String = DirectCast(grdviewSkill.Rows(e.RowIndex).FindControl("objtxtRemark"), TextBox).Text
            Dim txtSkillCat As String = DirectCast(grdviewSkill.Rows(e.RowIndex).FindControl("objtxtSkillCat"), TextBox).Text
            Dim txtSkill As String = DirectCast(grdviewSkill.Rows(e.RowIndex).FindControl("objtxtSkill"), TextBox).Text


            If grdviewSkill.Rows(e.RowIndex).FindControl("objddlSkillCat").Visible = True AndAlso grdviewSkill.Rows(e.RowIndex).FindControl("objddlSkill").Visible = True Then
                If intSkillCat <= 0 Then
                    ShowMessage(lblCategoryMsg.Text, MessageType.Info)
                    grdviewSkill.Rows(e.RowIndex).FindControl("objddlSkillCat").Focus()
                    e.Cancel = True
                    Exit Try
                ElseIf intSkill <= 0 Then
                    ShowMessage(lblSkillMsg.Text, MessageType.Info)
                    grdviewSkill.Rows(e.RowIndex).FindControl("objddlSkill").Focus()
                    e.Cancel = True
                    Exit Try
                End If
            Else
                If txtSkillCat.Trim = "" Then
                    ShowMessage(lblCategoryMsg.Text, MessageType.Info)
                    grdviewSkill.Rows(e.RowIndex).FindControl("objtxtSkillCat").Focus()
                    e.Cancel = True
                    Exit Try
                ElseIf txtSkill.Trim = "" Then
                    ShowMessage(lblSkillMsg.Text, MessageType.Info)
                    grdviewSkill.Rows(e.RowIndex).FindControl("objtxtSkill")
                    e.Cancel = True
                    Exit Try
                End If
            End If

            If cboSkillExpertise.Items.Count > 1 AndAlso intSkillExpertise <= 0 Then
                ShowMessage(lblSkillExpertiseMsg.Text, MessageType.Info)
                grdviewSkill.Rows(e.RowIndex).FindControl("objcboSkillExpertise").Focus()
                e.Cancel = True
                Exit Try
            End If

            If objAppSkill.IsExistApplicantSkill(strCompCode:=Session("CompCode").ToString _
                                                 , intComUnkID:=CInt(Session("companyunkid")) _
                                                 , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                 , intSkillTranUnkid:=CInt(e.Keys("skilltranunkid")) _
                                                 , intSkillCategoryUnkid:=intSkillCat _
                                                 , intSkillUnkid:=intSkill _
                                                 , strOtherSkillCategory:=txtSkillCat.Trim _
                                                 , strOtherSkill:=txtSkill.Trim
                                                 ) = True Then

                ShowMessage(lblgrdviewSkillRowUpdatingMsg1.Text, MessageType.Info)
                Exit Try
            End If

            If objAppSkill.EditApplicantSkill(strCompCode:=Session("CompCode").ToString _
                                             , intComUnkID:=CInt(Session("companyunkid")) _
                                             , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                             , intSkillTranUnkid:=CInt(e.Keys("skilltranunkid")) _
                                             , intSkillCategoryUnkid:=intSkillCat _
                                             , intSkillUnkid:=intSkill _
                                             , strRemark:=txtRemark.Trim _
                                             , strOtherSkillCategory:=txtSkillCat.Trim _
                                             , strOtherSkill:=txtSkill.Trim _
                                             , intSkillExpertiseUnkid:=intSkillExpertise
                                             ) = True Then

                ShowMessage(lblgrdviewSkillRowUpdatingMsg2.Text, MessageType.Info)

                grdviewSkill.EditIndex = -1
                Call FillApplicantSkills(True)

            End If

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub grdviewSkill_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdviewSkill.RowDataBound
        Dim objApplicant As New clsApplicant
        Dim dsCombo As DataSet
        Try
            Dim drv As DataRowView = CType(e.Row.DataItem, DataRowView)

            If e.Row.RowType = DataControlRowType.DataRow Then

                If e.Row.RowState = DataControlRowState.Edit OrElse e.Row.RowState = 5 Then '5 = Alternate Or Edit

                    If CInt(drv.Item("skillcategoryunkid")) <= 0 AndAlso CInt(drv.Item("skillunkid")) <= 0 Then
                        DirectCast(e.Row.FindControl("objtxtSkillCat"), TextBox).Visible = True
                        DirectCast(e.Row.FindControl("objddlSkillCat"), DropDownList).Visible = False

                        DirectCast(e.Row.FindControl("objtxtSkill"), TextBox).Visible = True
                        DirectCast(e.Row.FindControl("objddlSkill"), DropDownList).Visible = False

                    Else
                        DirectCast(e.Row.FindControl("objtxtSkillCat"), TextBox).Visible = False
                        DirectCast(e.Row.FindControl("objddlSkillCat"), DropDownList).Visible = True

                        DirectCast(e.Row.FindControl("objtxtSkill"), TextBox).Visible = False
                        DirectCast(e.Row.FindControl("objddlSkill"), DropDownList).Visible = True

                        Dim dp As DropDownList = DirectCast(e.Row.FindControl("objddlSkillCat"), DropDownList)
                        dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.SKILL_CATEGORY)
                        With dp
                            .DataValueField = "masterunkid"
                            .DataTextField = "Name"
                            .DataSource = dsCombo.Tables(0)
                            .DataBind()
                            .SelectedValue = drv.Item("skillcategoryunkid").ToString
                        End With
                        Dim drp As DropDownList = DirectCast(e.Row.FindControl("objddlSkill"), DropDownList)
                        dsCombo = objAppSkill.GetSkills(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(dp.SelectedValue))
                        With drp
                            .DataValueField = "Skillunkid"
                            .DataTextField = "Name"
                            .DataSource = dsCombo.Tables(0)
                            .DataBind()
                        End With
                        DirectCast(e.Row.FindControl("objddlSkill"), DropDownList).SelectedValue = drv.Item("skillunkid").ToString
                    End If

                    Dim cbo As DropDownList = DirectCast(e.Row.FindControl("objcboSkillExpertise"), DropDownList)
                    dsCombo = objAppSkill.GetSkillExpertise(Session("CompCode").ToString, CInt(Session("companyunkid")))
                    With cbo
                        .DataValueField = "skillexpertiseunkid"
                        .DataTextField = "expertise_name"
                        .DataSource = dsCombo.Tables(0)
                        .DataBind()
                    End With
                    DirectCast(e.Row.FindControl("objcboSkillExpertise"), DropDownList).SelectedValue = drv.Item("skillexpertiseunkid").ToString

                ElseIf e.Row.RowState = DataControlRowState.Normal OrElse e.Row.RowState = DataControlRowState.Alternate Then
                    ''Dim colSkillCategory As IEnumerable(Of DataControlField) = (From c In grdviewSkill.Columns.Cast(Of DataControlField) Where (c.HeaderText = "Skill Category") Select (c))
                    ''Dim colSkill As IEnumerable(Of DataControlField) = (From c In grdviewSkill.Columns.Cast(Of DataControlField) Where (c.HeaderText = "Skill") Select (c))
                    'Dim colSkillCategory As IEnumerable(Of DataControlField) = (From c In grdviewSkill.Columns.Cast(Of DataControlField) Where (c.FooterText = "colhSkillCategory") Select (c))
                    'Dim colSkill As IEnumerable(Of DataControlField) = (From c In grdviewSkill.Columns.Cast(Of DataControlField) Where (c.FooterText = "colhSkill") Select (c))


                    'If CInt(drv.Item("skillcategoryunkid")) <= 0 AndAlso CInt(drv.Item("skillunkid")) <= 0 Then
                    '    e.Row.Cells(grdviewSkill.Columns.IndexOf(colSkillCategory(0))).Text = drv.Item("other_skillcategory").ToString
                    '    e.Row.Cells(grdviewSkill.Columns.IndexOf(colSkill(0))).Text = drv.Item("other_skill").ToString
                    'End If
                End If
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub grdviewSkill_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grdviewSkill.RowDeleting
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If objAppSkill.DeleteApplicantSkill(strCompCode:=Session("CompCode").ToString _
                                             , intComUnkID:=CInt(Session("companyunkid")) _
                                             , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                             , intSkillTranUnkid:=CInt(e.Keys("skilltranunkid"))
                                             ) = True Then

                ShowMessage(lblgrdviewSkillRowDeletingMsg.Text, MessageType.Info)

                Call FillApplicantSkills(True)
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

#End Region

#Region " Checkbox Events "
    Private Sub chkOtherSkill_CheckedChanged(sender As Object, e As EventArgs) Handles chkOtherSkill.CheckedChanged
        Try
            If chkOtherSkill.Checked = True Then
                pnlSkill.Visible = False
                pnlOtherSkill.Visible = True
                ddlskillCate.SelectedValue = "0"
                Call ddlskillCate_SelectedIndexChanged(ddlskillCate, New System.EventArgs)
                ddlskill.SelectedValue = "0"
            Else
                pnlSkill.Visible = True
                pnlOtherSkill.Visible = False
                txtOtherSkillCategory.Text = ""
                txtOtherSkill.Text = ""
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub
#End Region

#Region " Button Events "

    Private Sub btnskillAdd_Click(sender As Object, e As EventArgs) Handles btnskillAdd.Click
        'lblError.Visible = False
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidated() = False Then Exit Try

            If objAppSkill.IsExistApplicantSkill(strCompCode:=Session("CompCode").ToString _
                                                 , intComUnkID:=CInt(Session("companyunkid")) _
                                                 , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                                 , intSkillTranUnkid:=-1 _
                                                 , intSkillCategoryUnkid:=CInt(ddlskillCate.SelectedValue) _
                                                 , intSkillUnkid:=CInt(ddlskill.SelectedValue) _
                                                 , strOtherSkillCategory:=txtOtherSkillCategory.Text.Trim _
                                                 , strOtherSkill:=txtOtherSkill.Text.Trim
                                                 ) = True Then

                ShowMessage(lblbtnskillAddMsg1.Text, MessageType.Info)
                Call FillApplicantSkills()
                ddlskill.Focus()
                Exit Try
            End If


            If objAppSkill.AddApplicantSkill(strCompCode:=Session("CompCode").ToString _
                                             , intComUnkID:=CInt(Session("companyunkid")) _
                                             , intApplicantUnkid:=CInt(Session("applicantunkid")) _
                                             , intSkillCategoryUnkid:=CInt(ddlskillCate.SelectedValue) _
                                             , intSkillUnkid:=CInt(ddlskill.SelectedValue) _
                                             , strRemark:=txtRemarks.Text.Trim _
                                             , strOtherSkillCategory:=txtOtherSkillCategory.Text.Trim _
                                             , strOtherSkill:=txtOtherSkill.Text.Trim _
                                             , intSkillExpertiseUnkid:=CInt(cboSkillExpertise.SelectedValue) _
                                             , dtCreated_date:=Now
                                             ) = True Then

                ShowMessage(lblbtnskillAddMsg2.Text, MessageType.Info)
            End If

            Call FillApplicantSkills(True)
            Call Reset_Skill()
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            'LanguageControl.Visible = True
            LanguageControl.show()
        End If
    End Sub
    'Sohail (16 Aug 2019) -- End

#End Region

End Class