﻿Option Strict On

'Imports Microsoft.Security.Application
Imports System.Web.Security.AntiXss
Imports System.Configuration
Imports System.Web.Configuration

Public Class Site4
    Inherits System.Web.UI.MasterPage

    'Sohail (21 Nov 2017) -- Start
    'Enhancement - 70.1 - OWASP changes.
    Private Const AntiXsrfTokenKey As String = "__AntiXsrfToken"
    Private Const AntiXsrfUserNameKey As String = "__AntiXsrfUserName"
    Private _antiXsrfTokenValue As String
    'Sohail (21 Nov 2017) -- End
    'Private mstrUrlArutiLink As String = ""

#Region " Public Enum "
    Public Enum MessageType
        Success
        Errorr
        Info
        Warning
    End Enum
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If Session.IsNewSession = True OrElse Session("companyunkid") Is Nothing Then
            If Session("companyunkid") Is Nothing Then
                pnlSwitchToAdmin.Visible = False
                'Dim strUserId As String = Membership.GetUser().ProviderUserKey.ToString
                'Dim strAuthCode As String = (New clsApplicant).GetCompanyAuthenticationCode(strUserId)
                'If strAuthCode.Trim <> "" Then
                '    ShowMessage("Sorry, Session is expired!", Request.Url.GetLeftPart(UriPartial.Authority) & "/" & Request.ApplicationPath & "/Login.aspx?ext=7&cc=" & strAuthCode, MessageType.Info)
                'Else
                '    ShowMessage("Sorry, Session is expired!", Request.Url.GetLeftPart(UriPartial.Authority) & "/" & Request.ApplicationPath & "/Login.aspx", MessageType.Info)
                'End If
                'ShowMessage("Sorry, Session is expired!", mstrUrlArutiLink, MessageType.Info)
                Exit Try
            End If

            If IsPostBack = False Then

                'lblCompanyName.Text = Sanitizer.GetSafeHtmlFragment(Session("CompName").ToString)
                'lblUserName.Text = Sanitizer.GetSafeHtmlFragment(Session("email").ToString)
                lblCompanyName.Text = AntiXss.AntiXssEncoder.HtmlEncode((Session("CompName").ToString), True)
                objlblUserName.Text = AntiXssEncoder.HtmlEncode((Session("email").ToString), True)
                lnkMisc.Visible = False

                If Session("image") IsNot Nothing AndAlso IsDBNull(Session("image")) = False Then
                    Dim bytes As Byte() = DirectCast(Session("image"), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes)
                    img_shortcuticon.ImageUrl = Convert.ToString("data:image/jpg;base64,") & base64String
                Else
                    img_shortcuticon.ImageUrl = "~/Images/blank.png"
                End If

                'If Session("companyunkid") <= 0 OrElse Session("CompCode") = "" Then
                '    Response.Redirect("~/UnauthorizedAccessaspx", False)
                '    Exit Sub
                'End If

                'Sohail (16 Aug 2019) -- Start
                'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
                pnlSwitchToAdmin.Visible = False
                If Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                    Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        pnlSwitchToAdmin.Visible = True
                    Else
                        arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                        If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                            pnlSwitchToAdmin.Visible = True
                        End If
                    End If
                    objlblAdmin.Text = "Admin"
                ElseIf Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                    pnlSwitchToAdmin.Visible = True
                    objlblAdmin.Text = "Super Admin"
                End If

                'Dim config As Configuration = WebConfigurationManager.OpenWebConfiguration("~/Web.Config")
                'Dim section As SessionStateSection = DirectCast(config.GetSection("system.web/sessionState"), SessionStateSection)
                'Session("timeout") = CInt(section.Timeout.TotalMinutes) * 1000 * 60
                'Sohail (16 Aug 2019) -- End

                Call MarkStarToMenuName(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))

                'Sohail (16 Aug 2019) -- Start
                'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
                'LanguageOpner1.Visible = False
                'LanguageControl2.Visible = False
                Dim lo As Boolean = False
                Dim lc As Boolean = False
                If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                    Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner1.Visible = True
                            'LanguageControl2.Visible = True 'Sohail (05 May 2021)
                            lo = True
                            lc = True
                        End If
                    Else
                        arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                        If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                                'LanguageOpner1.Visible = True
                                'LanguageControl2.Visible = True 'Sohail (05 May 2021)
                                lo = True
                                lc = True
                            End If
                        End If
                    End If
                End If
                If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner1.Visible = True
                        'LanguageControl2.Visible = True 'Sohail (05 May 2021)
                        lo = True
                        lc = True
                    End If
                End If
                If lo = False Then LanguageOpner1.Visible = False
                If lc = False Then LanguageControl2.Visible = False
                'Sohail (16 Aug 2019) -- End
            End If

            If (ViewState("mstrUrlArutiLink") Is Nothing OrElse ViewState("mstrUrlArutiLink").ToString = "") AndAlso Session("mstrUrlArutiLink") IsNot Nothing Then
                ViewState("mstrUrlArutiLink") = Session("mstrUrlArutiLink").ToString
                LoginStatus1.LogoutPageUrl = ViewState("mstrUrlArutiLink").ToString
            End If

            If Session("email") Is Nothing OrElse Session("email").ToString.Trim = "" Then
                Response.Redirect(ViewState("mstrUrlArutiLink").ToString, False)
                Exit Sub
            End If

            ''Sohail (16 Aug 2019) -- Start
            ''NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            'LanguageOpner1.Visible = False
            'LanguageControl2.Visible = False
            'If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
            '    Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
            '    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
            '        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            '            LanguageOpner1.Visible = True
            '            'LanguageControl2.Visible = True 'Sohail (05 May 2021)
            '        End If
            '    Else
            '        arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
            '        If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
            '            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            '                LanguageOpner1.Visible = True
            '                'LanguageControl2.Visible = True 'Sohail (05 May 2021)
            '            End If
            '        End If
            '    End If
            'End If
            'If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
            '    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            '        LanguageOpner1.Visible = True
            '        'LanguageControl2.Visible = True 'Sohail (05 May 2021)
            '    End If
            'End If
            ''Sohail (16 Aug 2019) -- End
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub Site1_Init(sender As Object, e As EventArgs) Handles Me.Init
        Try
            'Sohail (21 Nov 2017) -- Start
            'Enhancement - 70.1 - OWASP changes.                
            'The code below helps to protect against XSRF attacks
            Dim requestCookie = Request.Cookies(AntiXsrfTokenKey)
            Dim requestCookieGuidValue As Guid
            If requestCookie IsNot Nothing AndAlso Guid.TryParse(requestCookie.Value, requestCookieGuidValue) Then

                'Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value
                Page.ViewStateUserKey = _antiXsrfTokenValue

            Else

                'Generate a New Anti-XSRF token And save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N")
                Page.ViewStateUserKey = _antiXsrfTokenValue

                Dim responseCookie = New HttpCookie(AntiXsrfTokenKey) With {
                    .HttpOnly = True,
                    .Value = _antiXsrfTokenValue,
                    .Path = "/;sameSite=Strict"
                    }

                If FormsAuthentication.RequireSSL AndAlso Request.IsSecureConnection Then

                    responseCookie.Secure = True
                End If
                Response.Cookies.Set(responseCookie)
            End If

            Dim AspxAuthCookie = Request.Cookies(".ASPXAUTH")
            If AspxAuthCookie IsNot Nothing AndAlso Request.IsSecureConnection Then
                AspxAuthCookie.HttpOnly = True
                AspxAuthCookie.Secure = True
                AspxAuthCookie.Path = "/;sameSite=Strict"
                Response.Cookies.Set(AspxAuthCookie)
            End If

            AddHandler Page.PreLoad, AddressOf Site1_PreLoad
            'Sohail (21 Nov 2017) -- End

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Sohail (21 Nov 2017) -- Start
    'Enhancement - 70.1 - OWASP changes.  
    Private Sub Site1_PreLoad(sender As Object, e As EventArgs)
        Try
            If Not IsPostBack Then

                'Set Anti-XSRF token
                ViewState(AntiXsrfTokenKey) = Page.ViewStateUserKey
                ViewState(AntiXsrfUserNameKey) = If(Context.User.Identity.Name, [String].Empty)

            Else

                'Validate the Anti-XSRF token
                If ViewState(AntiXsrfTokenKey).ToString() <> _antiXsrfTokenValue Then
                    Throw New InvalidOperationException("Validation of Anti-XSRF token failed.")
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    'Sohail (21 Nov 2017) -- End

    Public Sub ShowMessage(strMessage As String, Optional type As MessageType = MessageType.Info)
        Try
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" & Message & "', '" & type & "');", True)
            If strMessage.Trim = "" Then
                strMessage = "Transaction Saved Successfully!"
            End If
            Dim strTitle As String = "Aruti"
            Dim strType As String = "BootstrapDialog.TYPE_PRIMARY"
            Dim strClass As String = ""
            Select Case type
                Case MessageType.Errorr
                    strType = "BootstrapDialog.TYPE_DANGER"
                    strClass = "fa fa-exclamation-circle"
                Case MessageType.Success
                    'strType = "Success!"
                    strClass = "fa fa-info-circle"
                Case MessageType.Warning
                    strType = "BootstrapDialog.TYPE_WARNING"
                    strClass = "fa fa-exclamation-triangle"
                Case Else
                    strType = "BootstrapDialog.TYPE_INFO"
                    strClass = "fa fa-info-circle"
            End Select
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "$('#msgicon').removeClass();$('#msgicon').addClass('" & strClass & "');$('.modal-body #msgbody').text('" & strType & "' + ' ' + '" & strMessage & "');$('#myModal').modal();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(),
               "BootstrapDialog.show({
                    type: " & strType & ",
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: '" & strTitle & "',
                    message: '" & strMessage & "',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function(dialogItself){
                        dialogItself.close();
                        }
                    }]
                });", True)
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Public Sub ShowMessage(strMessage As String, strRedirectLink As String, Optional type As MessageType = MessageType.Info)
        Try
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" & Message & "', '" & type & "');", True)
            If strMessage.Trim = "" Then
                strMessage = "Transaction Saved Successfully!"
            End If
            Dim strTitle As String = "Aruti"
            Dim strType As String = "BootstrapDialog.TYPE_PRIMARY"
            Dim strClass As String = ""
            Select Case type
                Case MessageType.Errorr
                    strType = "BootstrapDialog.TYPE_DANGER"
                    strClass = "fa fa-exclamation-circle"
                Case MessageType.Success
                    'strType = "Success!"
                    strClass = "fa fa-info-circle"
                Case MessageType.Warning
                    strType = "BootstrapDialog.TYPE_WARNING"
                    strClass = "fa fa-exclamation-triangle"
                Case Else
                    strType = "BootstrapDialog.TYPE_INFO"
                    strClass = "fa fa-info-circle"
            End Select
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "$('#msgicon').removeClass();$('#msgicon').addClass('" & strClass & "');$('.modal-body #msgbody').text('" & strType & "' + ' ' + '" & strMessage & "');$('#myModal').modal();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(),
               "BootstrapDialog.show({
                    type: " & strType & ",
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: '" & strTitle & "',
                    message: '" & strMessage & "',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function(){
                        window.location.href='" & strRedirectLink & "';
                        return false;
                        }
                    }]
                });", True)
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub LoginStatus1_LoggingOut(sender As Object, e As LoginCancelEventArgs) Handles LoginStatus1.LoggingOut
        FormsAuthentication.SignOut()
    End Sub

    Private Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        Try
            FormsAuthentication.SignOut()
            Session.Abandon()

            If ViewState("mstrUrlArutiLink") IsNot Nothing Then
                Response.Redirect(ViewState("mstrUrlArutiLink").ToString, False)
            Else
                Response.Redirect(Request.Form(hflocationorigin.UniqueID) & Replace(Request.ApplicationPath & "/Login.aspx", "//", "/"), False)
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try

    End Sub

    Public Sub MarkStarToMenuName(strCompCode As String _
                                           , intCompanyunkid As Integer _
                                           , intApplicantunkid As Integer)

        Dim dsList As DataSet

        hrefPersonalInfo.Attributes.Add("class", "nav-link")
        dsList = (New clsPersonalInfo).GetPersonalInfo(strCompCode, intCompanyunkid, intApplicantunkid)
        If dsList.Tables(0).Rows.Count > 0 Then
            If dsList.Tables(0).Rows(0).Item("othername").ToString.Length <= 0 AndAlso CBool(HttpContext.Current.Session("MiddleNameMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefPersonalInfo.Attributes.Add("class", "requiredmenu")
                hrefPersonalInfo.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
            End If
            If CInt(dsList.Tables(0).Rows(0).Item("gender")) <= 0 AndAlso CBool(HttpContext.Current.Session("GenderMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefPersonalInfo.Attributes.Add("class", "requiredmenu")
                hrefPersonalInfo.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
            End If
            If ((IsDBNull(dsList.Tables(0).Rows(0).Item("birth_date")) OrElse CDate(dsList.Tables(0).Rows(0).Item("birth_date")) = CDate("01/01/1900")) AndAlso CBool(HttpContext.Current.Session("BirthDateMandatory")) = True) Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefPersonalInfo.Attributes.Add("class", "requiredmenu")
                hrefPersonalInfo.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
            End If
            If CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 AndAlso CBool(HttpContext.Current.Session("NationalityMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefPersonalInfo.Attributes.Add("class", "requiredmenu")
                hrefPersonalInfo.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
            End If
            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-833
            If HttpContext.Current.Session("CompCode").ToString().ToLower() = "tra" Then
                If CInt(dsList.Tables(0).Rows(0).Item("residency")) <= 0 Then
                    hrefPersonalInfo.Attributes.Add("class", "nav-link required")
                End If

                If CStr(dsList.Tables(0).Rows(0).Item("indexno")).Trim.Length <= 0 Then
                    hrefPersonalInfo.Attributes.Add("class", "nav-link required")
                End If
            End If
            'S.SANDEEP |04-MAY-2023| -- END

            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : On job master,  Need a tab to define the language(s) attached to the job. Those languages will be displayed on job report.
            If Session("Vacancy").ToString = "Int" AndAlso dsList.Tables(0).Rows(0).Item("employeecode").ToString.Length <= 0 Then
                hrefPersonalInfo.Attributes.Add("class", "nav-link required")
            End If
            'Sohail (18 Feb 2020) -- End
        End If

        hrefContactDetails.Attributes.Add("class", "nav-link")
        dsList = (New clsContactDetails).GetContactDetail(strCompCode, intCompanyunkid, intApplicantunkid)
        If dsList.Tables(0).Rows.Count > 0 Then
            If dsList.Tables(0).Rows(0).Item("present_address1").ToString.Length <= 0 AndAlso CBool(HttpContext.Current.Session("Address1Mandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefContactDetails.Attributes.Add("class", "requiredmenu")
                hrefContactDetails.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
                '    Exit Sub
                'Else
                '    hrefContactDetails.Attributes.Add("class", "")
            End If
            If dsList.Tables(0).Rows(0).Item("present_address2").ToString.Length <= 0 AndAlso CBool(HttpContext.Current.Session("Address2Mandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefContactDetails.Attributes.Add("class", "requiredmenu")
                hrefContactDetails.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
                '    Exit Sub
                'Else
                '    hrefContactDetails.Attributes.Add("class", "")
            End If
            If dsList.Tables(0).Rows(0).Item("present_province").ToString.Length <= 0 AndAlso CBool(HttpContext.Current.Session("RegionMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefContactDetails.Attributes.Add("class", "requiredmenu")
                hrefContactDetails.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
                '    Exit Sub
                'Else
                '    hrefContactDetails.Attributes.Add("class", "")
            End If
            If CInt(dsList.Tables(0).Rows(0).Item("present_countryunkid")) <= 0 AndAlso CBool(HttpContext.Current.Session("CountryMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefContactDetails.Attributes.Add("class", "requiredmenu")
                hrefContactDetails.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
                '    Exit Sub
                'Else
                '    hrefContactDetails.Attributes.Add("class", "")
            End If

            If CInt(dsList.Tables(0).Rows(0).Item("present_stateunkid")) <= 0 AndAlso CBool(HttpContext.Current.Session("StateMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefContactDetails.Attributes.Add("class", "requiredmenu")
                hrefContactDetails.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
                '    Exit Sub
                'Else
                '    hrefContactDetails.Attributes.Add("class", "")
            End If
            If CInt(dsList.Tables(0).Rows(0).Item("present_post_townunkid")) <= 0 AndAlso CBool(HttpContext.Current.Session("CityMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefContactDetails.Attributes.Add("class", "requiredmenu")
                hrefContactDetails.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
                '    Exit Sub
                'Else
                '    hrefContactDetails.Attributes.Add("class", "")
            End If

            If CInt(dsList.Tables(0).Rows(0).Item("present_zipcode")) <= 0 AndAlso CBool(HttpContext.Current.Session("PostCodeMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefContactDetails.Attributes.Add("class", "requiredmenu")
                hrefContactDetails.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
                '    Exit Sub
                'Else
                '    hrefContactDetails.Attributes.Add("class", "")
            End If

        End If

        hrefOtherInfo.Attributes.Add("class", "nav-link")
        dsList = (New clsOtherInfo).GetOtherInfo(strCompCode, intCompanyunkid, intApplicantunkid, clsCommon_Master.enCommonMaster.LANGUAGES, clsCommon_Master.enCommonMaster.MARRIED_STATUS)
        If dsList.Tables(0).Rows.Count > 0 Then
            If CInt(dsList.Tables(0).Rows(0).Item("language1unkid")) <= 0 AndAlso CBool(HttpContext.Current.Session("Language1Mandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefOtherInfo.Attributes.Add("class", "requiredmenu")
                hrefOtherInfo.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
            End If
            If CInt(dsList.Tables(0).Rows(0).Item("marital_statusunkid")) <= 0 AndAlso CBool(HttpContext.Current.Session("MaritalStatusMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefOtherInfo.Attributes.Add("class", "requiredmenu")
                hrefOtherInfo.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
            End If
            If dsList.Tables(0).Rows(0).Item("mother_tongue").ToString.Trim = "" AndAlso CBool(HttpContext.Current.Session("MotherTongueMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefOtherInfo.Attributes.Add("class", "requiredmenu")
                hrefOtherInfo.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
            End If
            If CDec(dsList.Tables(0).Rows(0).Item("current_salary")) <= 0 AndAlso CBool(HttpContext.Current.Session("CurrentSalaryMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefOtherInfo.Attributes.Add("class", "requiredmenu")
                hrefOtherInfo.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
            End If
            'Sohail (10 Oct 2018) -- Start
            'If CDec(dsList.Tables(0).Rows(0).Item("expected_salary")) <= 0 AndAlso CBool(HttpContext.Current.Session("CurrentSalaryMandatory")) = True Then
            If CDec(dsList.Tables(0).Rows(0).Item("expected_salary")) <= 0 AndAlso CBool(HttpContext.Current.Session("ExpectedSalaryMandatory")) = True Then
                'Sohail (10 Oct 2018) -- End
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefOtherInfo.Attributes.Add("class", "requiredmenu")
                hrefOtherInfo.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
            End If
            If dsList.Tables(0).Rows(0).Item("expected_benefits").ToString.Trim = "" AndAlso CBool(HttpContext.Current.Session("ExpectedBenefitsMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefOtherInfo.Attributes.Add("class", "requiredmenu")
                hrefOtherInfo.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
            End If
            If CInt(dsList.Tables(0).Rows(0).Item("notice_period_days")) <= 0 AndAlso CBool(HttpContext.Current.Session("NoticePeriodMandatory")) = True Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefOtherInfo.Attributes.Add("class", "requiredmenu")
                hrefOtherInfo.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
            End If
        End If

        hrefApplicantSkill.Attributes.Add("class", "nav-link")
        dsList = (New clsApplicantSkill).GetApplicantSkills(strCompCode, intCompanyunkid, clsCommon_Master.enCommonMaster.SKILL_CATEGORY, intApplicantunkid)
        'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("OneSkillMandatory")) = True Then
        If dsList.Tables(0).Rows.Count < CInt(HttpContext.Current.Session("RecruitMandatorySkills")) Then
            'Hemant (05 july 2018) -- -- Start
            'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
            ' hrefApplicantSkill.Attributes.Add("class", "requiredmenu")
            hrefApplicantSkill.Attributes.Add("class", "nav-link required")
            'Hemant (05 july 2018) -- -- End
        End If

        hrefApplicantQualification.Attributes.Add("class", "nav-link")
        dsList = (New clsApplicantQualification).GetApplicantQualifications(strCompCode, intCompanyunkid, intApplicantunkid, clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, CInt(HttpContext.Current.Session("appqualisortbyid")))
        If dsList.Tables(0).Rows.Count > 0 Then
            If CBool(HttpContext.Current.Session("HighestQualificationMandatory")) = True AndAlso dsList.Tables(0).Select("ishighestqualification =  1 ").Length <= 0 Then
                'Hemant (05 july 2018) -- -- Start
                'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
                ' hrefApplicantQualification.Attributes.Add("class", "requiredmenu")
                hrefApplicantQualification.Attributes.Add("class", "nav-link required")
                'Hemant (05 july 2018) -- -- End
            End If
        Else
            'If CBool(HttpContext.Current.Session("OneQualificationMandatory")) = True Then
            '    'Hemant (05 july 2018) -- -- Start
            '    'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
            '    ' hrefApplicantQualification.Attributes.Add("class", "requiredmenu")
            '    hrefApplicantQualification.Attributes.Add("class", "required")
            '    'Hemant (05 july 2018) -- -- End
            'End If

        End If
        If dsList.Tables(0).Rows.Count < CInt(HttpContext.Current.Session("RecruitMandatoryQualifications")) Then
            hrefApplicantQualification.Attributes.Add("class", "nav-link required")
        End If

        'Sohail (11 Nov 2021) -- Start
        'NMB Enhancement: Attachments for applicants should be restricted to only CV and Cover letter on portal. Applicants should not see other document types.
        'dsList = (New clsApplicantQualification).GetApplicantAttachments(strCompCode, intCompanyunkid, intApplicantunkid, CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.QUALIFICATIONS))
        'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("isQualiCertAttachMandatory")) = True Then
        '    'Hemant (05 july 2018) -- -- Start
        '    'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
        '    ' hrefApplicantQualification.Attributes.Add("class", "requiredmenu")
        '    hrefApplicantQualification.Attributes.Add("class", "nav-link required")
        '    'Hemant (05 july 2018) -- -- End
        'End If
        'Sohail (11 Nov 2021) -- End

        Dim blnAttachReq As Boolean = False
        hrefMiscAttachment.Attributes.Add("class", "nav-link")
        'If blnAttachReq = False Then
        '    dsList = (New clsApplicantQualification).GetApplicantAttachments(strCompCode, intCompanyunkid, intApplicantunkid, CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.CURRICULAM_VITAE))
        '    If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("OneCurriculamVitaeMandatory")) = True Then
        '        'hrefMiscAttachment.Attributes.Add("class", "nav-link required")
        '        blnAttachReq = True
        '    End If
        'End If

        'If blnAttachReq = False Then
        '    dsList = (New clsApplicantQualification).GetApplicantAttachments(strCompCode, intCompanyunkid, intApplicantunkid, CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.COVER_LETTER))
        '    If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("OneCoverLetterMandatory")) = True Then
        '        'hrefMiscAttachment.Attributes.Add("class", "nav-link required")
        '        blnAttachReq = True
        '    End If
        'End If

        'If blnAttachReq = False Then
        '    dsList = (New clsApplicantQualification).GetApplicantAttachments(strCompCode, intCompanyunkid, intApplicantunkid, CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.QUALIFICATIONS))
        '    If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("AttachmentQualificationMandatory")) = True Then
        '        'hrefMiscAttachment.Attributes.Add("class", "nav-link required")
        '        blnAttachReq = True
        '    End If
        'End If

        If blnAttachReq = True Then
            hrefMiscAttachment.Attributes.Add("class", "nav-link required")
        End If

        hrefApplicantExperience.Attributes.Add("class", "nav-link")
        dsList = (New clsApplicantExperience).GetApplicantExperience(strCompCode, intCompanyunkid, intApplicantunkid)
        'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("OneJobExperienceMandatory")) = True Then
        If dsList.Tables(0).Rows.Count < CInt(HttpContext.Current.Session("RecruitMandatoryJobExperiences")) Then
            'Hemant (05 july 2018) -- -- Start
            'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
            ' hrefApplicantExperience.Attributes.Add("class", "requiredmenu")
            hrefApplicantExperience.Attributes.Add("class", "nav-link required")
            'Hemant (05 july 2018) -- -- End
        End If

        hrefApplicantReference.Attributes.Add("class", "nav-link")
        dsList = (New clsApplicantReference).GetApplicantReference(strCompCode, intCompanyunkid, intApplicantunkid, clsCommon_Master.enCommonMaster.RELATIONS)
        'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("OneReferenceMandatory")) = True Then
        If dsList.Tables(0).Rows.Count < CInt(HttpContext.Current.Session("RecruitMandatoryReferences")) Then
            'Hemant (05 july 2018) -- -- Start
            'Enhancement – RefNo: 264 - Summary of Duties - Under Experiences, lets allow only a summary on Duties. We can have max number of characters (150) etc to avoid applicants dumping pages of detail. This will also assist in having a readable CV (we will add caption Duties (Summary)).
            ' hrefApplicantReference.Attributes.Add("class", "requiredmenu")
            hrefApplicantReference.Attributes.Add("class", "nav-link required")
            'Hemant (05 july 2018) -- -- End
        End If

        If CBool(Session("HideRecruitementSkillScreen")) = True Then
            lnkSkill.Visible = False
        End If

        If CBool(Session("HideRecruitementOtherInfoScreen")) = True Then
            lnkOtherInfo.Visible = False
        End If
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            'LanguageControl2.Visible = True
            LanguageControl2.show()
        End If
    End Sub
    'Sohail (16 Aug 2019) -- End

End Class