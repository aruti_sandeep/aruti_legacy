﻿Option Strict On
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Runtime.Caching

Public Class clsApplicantQualification

    Dim cache As ObjectCache = MemoryCache.Default

#Region " Method Functions "

    Public Function GetQualifications(ByVal strCompCode As String _
                                        , intComUnkID As Integer _
                                        , intQualificationGroupUnkid As Integer
                                        ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetQualifications"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@qualificationgroupunkid", SqlDbType.Int)).Value = intQualificationGroupUnkid

                        da.SelectCommand = cmd
                        da.Fill(ds, "QualificationGroup")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function GetResultCodes(ByVal strCompCode As String _
                                    , intComUnkID As Integer _
                                    , intQualificationUnkid As Integer
                                    ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetResults"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@qualificationunkid", SqlDbType.Int)).Value = intQualificationUnkid

                        da.SelectCommand = cmd
                        da.Fill(ds, "Result")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function GetInstitute(ByVal strCompCode As String _
                                    , intComUnkID As Integer
                                    ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetInstitute"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode

                        da.SelectCommand = cmd
                        da.Fill(ds, "Institute")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function GetApplicantQualifications(ByVal strCompCode As String _
                                               , intComUnkID As Integer _
                                               , intApplicantUnkid As Integer _
                                               , intQualificationGroup_ID As Integer _
                                               , intSortOrder As Integer _
                                               , Optional blnRefreshCache As Boolean = False
                                               ) As DataSet
        Dim ds As New DataSet
        Try
            Dim strCacheKey As String = "AppQuali_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_" & intQualificationGroup_ID.ToString & "_" & intSortOrder.ToString
            Dim dsCache As DataSet = TryCast(Cache(strCacheKey), DataSet)
            If dsCache Is Nothing OrElse blnRefreshCache = True Then

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
                Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
                'Sohail (30 Nov 2017) -- End

                Dim strQ As String = "procGetApplicantQualifications"

                Using con As New SqlConnection(strConn)

                    con.Open()

                    Using da As New SqlDataAdapter()
                        Using cmd As New SqlCommand(strQ, con)

                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                            cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                            cmd.Parameters.Add(New SqlParameter("@QualificationGrp_id", SqlDbType.Int)).Value = intQualificationGroup_ID
                            cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                            cmd.Parameters.Add(New SqlParameter("@appqualisortbyid", SqlDbType.Int)).Value = intSortOrder

                            da.SelectCommand = cmd
                            da.Fill(ds, "ApplicantQualifications")

                            'Select Case intSortOrder
                            '    Case 0 'By Completion Date
                            '        ds.Tables(0).DefaultView.Sort = "enddate DESC"

                            '    Case 1 'By Highest followed by Completion Date
                            '        ds.Tables(0).DefaultView.Sort = "ishighestqualification DESC, enddate DESC"

                            'End Select

                        End Using
                    End Using
                End Using

                Cache.Set(strCacheKey, ds, Now.AddMinutes(2))
            Else
                ds = dsCache
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetAppQualifications(ByVal strCompCode As String _
                                               , intComUnkID As Integer _
                                               , intApplicantUnkid As Integer _
                                               , intQualificationGroup_ID As Integer _
                                               , intSortOrder As Integer _
                                               , Optional dtCurrentDate As Date = Nothing
                                               ) As DataSet
        Dim ds As New DataSet
        Dim cache As ObjectCache = MemoryCache.Default
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetApplicantQualifications"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@QualificationGrp_id", SqlDbType.Int)).Value = intQualificationGroup_ID
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                        cmd.Parameters.Add(New SqlParameter("@appqualisortbyid", SqlDbType.Int)).Value = intSortOrder

                        da.SelectCommand = cmd
                        da.Fill(ds, "ApplicantQualifications")

                        'Select Case intSortOrder
                        '    Case 0 'By Completion Date
                        '        ds.Tables(0).DefaultView.Sort = "enddate DESC"

                        '    Case 1 'By Highest followed by Completion Date
                        '        ds.Tables(0).DefaultView.Sort = "ishighestqualification DESC, enddate DESC"

                        'End Select

                    End Using
                End Using
            End Using

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function AddApplicantQualification(ByVal strCompCode As String _
                                              , intComUnkID As Integer _
                                              , intApplicantUnkid As Integer _
                                              , intQualificationGroupUnkid As Integer _
                                              , intQualificationUnkid As Integer _
                                              , dtTransaction_Date As Date _
                                              , strReference_no As String _
                                              , dtAward_start_Date As Date _
                                              , dtAward_end_Date As Date _
                                              , intInstituteunkid As Integer _
                                              , intResultunkid As Integer _
                                              , decGPACode As Decimal _
                                              , strCertificateno As String _
                                              , strOther_Qualificationgrp As String _
                                              , strOther_Qualification As String _
                                              , strOther_Institute As String _
                                              , strOther_Resultcode As String _
                                              , strRemark As String _
                                              , blnHighestQualification As Boolean _
                                              , dtCreated_date As Date _
                                              , objAddAppAttach As List(Of dtoAddAppAttach)
                                              ) As Boolean

        'Pinkal (30-Sep-2023) --' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.[ objAddAppAttach As List(Of dtoAddAppAttach)]

        Dim ds As New DataSet
        Dim cache As ObjectCache = MemoryCache.Default

        'Pinkal (30-Sep-2023) -- Start
        '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
        Dim trn As SqlTransaction = Nothing
        Dim intApplicantQualificationTranunkid = 0
        'Pinkal (30-Sep-2023) -- End

        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End


            'Pinkal (30-Sep-2023) -- Start
            ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
            Dim strQ As String = "procAddApplicantQualification"
            Dim strQ1 As String = "procAddApplicantAttachment"
            'Pinkal (30-Sep-2023) -- End


            Using con As New SqlConnection(strConn)

                con.Open()

                'Pinkal (30-Sep-2023) -- Start
                '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
                trn = con.BeginTransaction
                'Pinkal (30-Sep-2023) -- End


                Using cmd As New SqlCommand(strQ, con)

                    'Pinkal (30-Sep-2023) -- Start
                    '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
                    cmd.Transaction = trn
                    'Pinkal (30-Sep-2023) -- End

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@qualificationgroupunkid", SqlDbType.Int)).Value = intQualificationGroupUnkid
                    cmd.Parameters.Add(New SqlParameter("@qualificationunkid", SqlDbType.Int)).Value = intQualificationUnkid
                    cmd.Parameters.Add(New SqlParameter("@transaction_date", SqlDbType.Date)).Value = dtTransaction_Date
                    cmd.Parameters.Add(New SqlParameter("@reference_no", SqlDbType.NVarChar)).Value = strReference_no.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@award_start_date", SqlDbType.Date)).Value = dtAward_start_Date
                    cmd.Parameters.Add(New SqlParameter("@award_end_date", SqlDbType.Date)).Value = dtAward_end_Date
                    cmd.Parameters.Add(New SqlParameter("@instituteunkid", SqlDbType.Int)).Value = intInstituteunkid
                    cmd.Parameters.Add(New SqlParameter("@resultunkid", SqlDbType.Int)).Value = intResultunkid
                    cmd.Parameters.Add(New SqlParameter("@gpacode", SqlDbType.Decimal)).Value = decGPACode
                    cmd.Parameters.Add(New SqlParameter("@certificateno", SqlDbType.NVarChar)).Value = strCertificateno.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_qualificationgrp", SqlDbType.NVarChar)).Value = strOther_Qualificationgrp.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_qualification", SqlDbType.NVarChar)).Value = strOther_Qualification.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_institute", SqlDbType.NVarChar)).Value = strOther_Institute.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_resultcode", SqlDbType.NVarChar)).Value = strOther_Resultcode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@remark", SqlDbType.NVarChar)).Value = strRemark.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@ishighestqualification", SqlDbType.Bit)).Value = blnHighestQualification
                    cmd.Parameters.Add(New SqlParameter("@created_date", SqlDbType.DateTime)).Value = dtCreated_date

                    'Pinkal (30-Sep-2023) -- Start
                    ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
                    ' cmd.ExecuteNonQuery())
                    intApplicantQualificationTranunkid = CInt(cmd.ExecuteScalar())
                    'Pinkal (30-Sep-2023) -- End


                End Using



                'Pinkal (30-Sep-2023) -- Start
                ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
                If objAddAppAttach IsNot Nothing AndAlso objAddAppAttach.Count > 0 Then

                    For Each dto As dtoAddAppAttach In objAddAppAttach

                        Using cmd As New SqlCommand(strQ1, con)

                            With cmd
                                .Parameters.Clear()
                                .Transaction = trn
                                .CommandType = CommandType.StoredProcedure
                                .Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = dto.ComUnkID
                                .Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = dto.CompCode
                                .Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = dto.ApplicantUnkid
                                .Parameters.Add(New SqlParameter("@documentunkid", SqlDbType.Int)).Value = dto.DocumentUnkid
                                .Parameters.Add(New SqlParameter("@modulerefid", SqlDbType.Int)).Value = dto.ModulerefId
                                .Parameters.Add(New SqlParameter("@attachrefid", SqlDbType.Int)).Value = dto.AttachrefId
                                .Parameters.Add(New SqlParameter("@filepath", SqlDbType.NVarChar, 2000)).Value = dto.Filepath.Trim.Replace("'", "''")
                                .Parameters.Add(New SqlParameter("@filename", SqlDbType.NVarChar)).Value = dto.Filename.Trim.Replace("'", "''")
                                .Parameters.Add(New SqlParameter("@fileuniquename", SqlDbType.NVarChar)).Value = dto.Fileuniquename.Trim.Replace("'", "''")
                                .Parameters.Add(New SqlParameter("@attached_date", SqlDbType.Date)).Value = dto.Attached_Date
                                .Parameters.Add(New SqlParameter("@file_size", SqlDbType.Int)).Value = dto.File_size
                                .Parameters.Add(New SqlParameter("@appvacancytranunkid", SqlDbType.Int)).Value = 0
                                .Parameters.Add(New SqlParameter("@jobhistorytranunkid", SqlDbType.Int)).Value = 0
                                .Parameters.Add(New SqlParameter("@applicantqualiftranunkid", SqlDbType.Int)).Value = intApplicantQualificationTranunkid

                                .ExecuteNonQuery()

                            End With

                End Using

                    Next

                End If  ' If objAddAppAttach IsNot Nothing AndAlso objAddAppAttach.Count > 0 Then

                trn.Commit()

                'Pinkal (30-Sep-2023) -- End

            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppQuali_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

    <DataObjectMethod(DataObjectMethodType.Update, True)>
    Public Shared Function EditApplicantQualification(ByVal strCompCode As String _
                                              , intComUnkID As Integer _
                                              , intApplicantUnkid As Integer _
                                              , intQualificationTranUnkid As Integer _
                                              , intQualificationGroupUnkid As Integer _
                                              , intQualificationUnkid As Integer _
                                              , dtTransaction_Date As Date _
                                              , strReference_no As String _
                                              , dtAward_start_Date As Date _
                                              , dtAward_end_Date As Date _
                                              , intInstituteunkid As Integer _
                                              , intResultunkid As Integer _
                                              , decGPACode As Decimal _
                                              , strCertificateno As String _
                                              , strOther_Qualificationgrp As String _
                                              , strOther_Qualification As String _
                                              , strOther_Institute As String _
                                              , strOther_Resultcode As String _
                                              , strRemark As String _
                                              , blnHighestQualification As Boolean _
                                              , qualificationtranunkid As Integer
                                              ) As Boolean
        Dim ds As New DataSet
        Dim cache As ObjectCache = MemoryCache.Default
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            If strReference_no Is Nothing Then strReference_no = ""
            If strCertificateno Is Nothing Then strCertificateno = ""
            If strOther_Qualificationgrp Is Nothing Then strOther_Qualificationgrp = ""
            If strOther_Qualification Is Nothing Then strOther_Qualification = ""
            If strOther_Institute Is Nothing Then strOther_Institute = ""
            If strOther_Resultcode Is Nothing Then strOther_Resultcode = ""
            If strRemark Is Nothing Then strRemark = ""

            Dim strQ As String = "procEditApplicantQualification"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@qualificationtranunkid", SqlDbType.Int)).Value = intQualificationTranUnkid
                    cmd.Parameters.Add(New SqlParameter("@qualificationgroupunkid", SqlDbType.Int)).Value = intQualificationGroupUnkid
                    cmd.Parameters.Add(New SqlParameter("@qualificationunkid", SqlDbType.Int)).Value = intQualificationUnkid
                    cmd.Parameters.Add(New SqlParameter("@transaction_date", SqlDbType.Date)).Value = dtTransaction_Date
                    cmd.Parameters.Add(New SqlParameter("@reference_no", SqlDbType.NVarChar)).Value = strReference_no.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@award_start_date", SqlDbType.Date)).Value = dtAward_start_Date
                    cmd.Parameters.Add(New SqlParameter("@award_end_date", SqlDbType.Date)).Value = dtAward_end_Date
                    cmd.Parameters.Add(New SqlParameter("@instituteunkid", SqlDbType.Int)).Value = intInstituteunkid
                    cmd.Parameters.Add(New SqlParameter("@resultunkid", SqlDbType.Int)).Value = intResultunkid
                    cmd.Parameters.Add(New SqlParameter("@gpacode", SqlDbType.Decimal)).Value = decGPACode
                    cmd.Parameters.Add(New SqlParameter("@certificateno", SqlDbType.NVarChar)).Value = strCertificateno.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_qualificationgrp", SqlDbType.NVarChar)).Value = strOther_Qualificationgrp.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_qualification", SqlDbType.NVarChar)).Value = strOther_Qualification.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_institute", SqlDbType.NVarChar)).Value = strOther_Institute.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_resultcode", SqlDbType.NVarChar)).Value = strOther_Resultcode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@remark", SqlDbType.NVarChar)).Value = strRemark.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@ishighestqualification", SqlDbType.Bit)).Value = blnHighestQualification

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppQuali_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

    <DataObjectMethod(DataObjectMethodType.Update, True)>
    Public Shared Function EditAppQualification(objAppQualiUpdate As dtoAppQualiUpdate _
                                              , qualificationtranunkid As Integer
                                              ) As Boolean
        Dim ds As New DataSet
        Dim cache As ObjectCache = MemoryCache.Default
        Try

            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            'If strReference_no Is Nothing Then strReference_no = ""
            'If strCertificateno Is Nothing Then strCertificateno = ""
            'If strOther_Qualificationgrp Is Nothing Then strOther_Qualificationgrp = ""
            'If strOther_Qualification Is Nothing Then strOther_Qualification = ""
            'If strOther_Institute Is Nothing Then strOther_Institute = ""
            'If strOther_Resultcode Is Nothing Then strOther_Resultcode = ""
            'If strRemark Is Nothing Then strRemark = ""

            Dim strQ As String = "procEditApplicantQualification"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = objAppQualiUpdate.ComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = objAppQualiUpdate.CompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = objAppQualiUpdate.ApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@qualificationtranunkid", SqlDbType.Int)).Value = objAppQualiUpdate.QualificationTranUnkid
                    cmd.Parameters.Add(New SqlParameter("@qualificationgroupunkid", SqlDbType.Int)).Value = objAppQualiUpdate.QualificationGroupUnkid
                    cmd.Parameters.Add(New SqlParameter("@qualificationunkid", SqlDbType.Int)).Value = objAppQualiUpdate.QualificationUnkid
                    cmd.Parameters.Add(New SqlParameter("@transaction_date", SqlDbType.Date)).Value = objAppQualiUpdate.Transaction_Date
                    cmd.Parameters.Add(New SqlParameter("@reference_no", SqlDbType.NVarChar)).Value = objAppQualiUpdate.Reference_no.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@award_start_date", SqlDbType.Date)).Value = objAppQualiUpdate.Award_start_Date
                    cmd.Parameters.Add(New SqlParameter("@award_end_date", SqlDbType.Date)).Value = objAppQualiUpdate.Award_end_Date
                    cmd.Parameters.Add(New SqlParameter("@instituteunkid", SqlDbType.Int)).Value = objAppQualiUpdate.Instituteunkid
                    cmd.Parameters.Add(New SqlParameter("@resultunkid", SqlDbType.Int)).Value = objAppQualiUpdate.Resultunkid
                    cmd.Parameters.Add(New SqlParameter("@gpacode", SqlDbType.Decimal)).Value = objAppQualiUpdate.GPACode
                    cmd.Parameters.Add(New SqlParameter("@certificateno", SqlDbType.NVarChar)).Value = objAppQualiUpdate.Certificateno.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_qualificationgrp", SqlDbType.NVarChar)).Value = objAppQualiUpdate.Other_Qualificationgrp.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_qualification", SqlDbType.NVarChar)).Value = objAppQualiUpdate.Other_Qualification.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_institute", SqlDbType.NVarChar)).Value = objAppQualiUpdate.Other_Institute.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_resultcode", SqlDbType.NVarChar)).Value = objAppQualiUpdate.Other_Resultcode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@remark", SqlDbType.NVarChar)).Value = objAppQualiUpdate.Remark.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@ishighestqualification", SqlDbType.Bit)).Value = objAppQualiUpdate.HighestQualification

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppQuali_" & objAppQualiUpdate.CompCode & "_" & objAppQualiUpdate.ComUnkID.ToString & "_" & objAppQualiUpdate.ApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try

    End Function

    <DataObjectMethod(DataObjectMethodType.Delete, True)>
    Public Shared Function DeleteApplicantQualification(ByVal strCompCode As String _
                                                , intComUnkID As Integer _
                                                , intApplicantUnkid As Integer _
                                                , intQualificationTranUnkid As Integer _
                                                , qualificationtranunkid As Integer
                                                ) As Boolean
        Dim ds As New DataSet
        Dim cache As ObjectCache = MemoryCache.Default
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procDeleteApplicantQualification"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@qualificationtranunkid", SqlDbType.Int)).Value = intQualificationTranUnkid

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppQuali_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

    Public Function IsExistApplicantQualification(ByVal strCompCode As String _
                                                  , intComUnkID As Integer _
                                                  , intApplicantUnkid As Integer _
                                                  , intQualificationTranUnkid As Integer _
                                                  , intQualificationGroupUnkid As Integer _
                                                  , intQualificationUnkid As Integer _
                                                  , strOther_Qualificationgrp As String _
                                                  , strOther_Qualification As String
                                                  ) As Boolean
        Dim blnIsExist As Boolean = False
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procIsExistApplicantQualification"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@qualificationtranunkid", SqlDbType.Int)).Value = intQualificationTranUnkid
                    cmd.Parameters.Add(New SqlParameter("@qualificationgroupunkid", SqlDbType.Int)).Value = intQualificationGroupUnkid
                    cmd.Parameters.Add(New SqlParameter("@qualificationunkid", SqlDbType.Int)).Value = intQualificationUnkid
                    cmd.Parameters.Add(New SqlParameter("@other_qualificationgrp", SqlDbType.NVarChar)).Value = strOther_Qualificationgrp.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@other_qualification", SqlDbType.NVarChar)).Value = strOther_Qualification.Trim.Replace("'", "''")

                    cmd.Parameters.Add(New SqlParameter("@result", SqlDbType.Bit)).Value = False
                    cmd.Parameters("@result").Direction = ParameterDirection.Output

                    cmd.ExecuteNonQuery()

                    blnIsExist = CBool(cmd.Parameters("@result").Value)

                End Using
            End Using

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return blnIsExist
    End Function

    Public Function GetApplicantAttachments(ByVal strCompCode As String _
                                               , intComUnkID As Integer _
                                               , intApplicantUnkid As Integer _
                                               , intModulerefId As Integer _
                                               , intAttachrefId As Integer _
                                               , Optional blnRefreshCache As Boolean = False _
                                               , Optional intAppvacancytranunkid As Integer = -1
                                               ) As DataSet
        Dim ds As New DataSet
        Try
            Dim strCacheKey As String = "AppAttach_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_" & intModulerefId.ToString & "_" & intAttachrefId.ToString
            Dim dsCache As DataSet = TryCast(cache(strCacheKey), DataSet)
            If dsCache Is Nothing OrElse blnRefreshCache = True Then

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
                Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
                'Sohail (30 Nov 2017) -- End

                Dim strQ As String = "procGetApplicantAttachments"

                Using con As New SqlConnection(strConn)

                    con.Open()

                    Using da As New SqlDataAdapter()
                        Using cmd As New SqlCommand(strQ, con)

                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                            cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                            cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                            cmd.Parameters.Add(New SqlParameter("@modulerefid", SqlDbType.Int)).Value = intModulerefId
                            cmd.Parameters.Add(New SqlParameter("@attachrefid", SqlDbType.Int)).Value = intAttachrefId
                            cmd.Parameters.Add(New SqlParameter("@appvacancytranunkid", SqlDbType.Int)).Value = intAppvacancytranunkid

                            da.SelectCommand = cmd
                            da.Fill(ds, "ApplicantAttachments")


                        End Using
                    End Using
                End Using

                cache.Set(strCacheKey, ds, Now.AddMinutes(2))
            Else
                ds = dsCache
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function AddApplicantAttachment(ByVal strCompCode As String _
                                              , intComUnkID As Integer _
                                              , intApplicantUnkid As Integer _
                                              , intDocumentUnkid As Integer _
                                              , intModulerefId As Integer _
                                              , intAttachrefId As Integer _
                                              , strFilepath As String _
                                              , strFilename As String _
                                              , strFileuniquename As String _
                                              , dtAttached_Date As Date _
                                              , intFile_size As Integer
                                              ) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procAddApplicantAttachment"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@documentunkid", SqlDbType.Int)).Value = intDocumentUnkid
                    cmd.Parameters.Add(New SqlParameter("@modulerefid", SqlDbType.Int)).Value = intModulerefId
                    cmd.Parameters.Add(New SqlParameter("@attachrefid", SqlDbType.Int)).Value = intAttachrefId
                    cmd.Parameters.Add(New SqlParameter("@filepath", SqlDbType.NVarChar, 2000)).Value = strFilepath.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@filename", SqlDbType.NVarChar)).Value = strFilename.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@fileuniquename", SqlDbType.NVarChar)).Value = strFileuniquename.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@attached_date", SqlDbType.Date)).Value = dtAttached_Date
                    cmd.Parameters.Add(New SqlParameter("@file_size", SqlDbType.Int)).Value = intFile_size
                    'S.SANDEEP |21-JUL-2023| -- START
                    'ISSUE/ENHANCEMENT : NOT ADDED THIS
                    cmd.Parameters.Add(New SqlParameter("@jobhistorytranunkid", SqlDbType.Int)).Value = 0
                    'S.SANDEEP |21-JUL-2023| -- END

                    'Pinkal (30-Sep-2023) -- Start
                    ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
                    cmd.Parameters.Add(New SqlParameter("@applicantqualiftranunkid", SqlDbType.Int)).Value = 0
                    'Pinkal (30-Sep-2023) -- End

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppAttach_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

    Public Function EditApplicantAttachment(ByVal strCompCode As String _
                                              , intComUnkID As Integer _
                                              , intApplicantUnkid As Integer _
                                              , intAttachfiletranUnkid As Integer _
                                              , intDocumentUnkid As Integer _
                                              , intModulerefId As Integer _
                                              , intAttachrefId As Integer _
                                              , strFilepath As String _
                                              , strFilename As String _
                                              , strFileuniquename As String _
                                              , dtAttached_Date As Date _
                                              , intFile_size As Integer
                                              ) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procEditApplicantAttachment"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@attachfiletranunkid", SqlDbType.Int)).Value = intAttachfiletranUnkid
                    cmd.Parameters.Add(New SqlParameter("@documentunkid", SqlDbType.Int)).Value = intDocumentUnkid
                    cmd.Parameters.Add(New SqlParameter("@modulerefid", SqlDbType.Int)).Value = intModulerefId
                    cmd.Parameters.Add(New SqlParameter("@attachrefid", SqlDbType.Int)).Value = intAttachrefId
                    cmd.Parameters.Add(New SqlParameter("@filepath", SqlDbType.NVarChar, 2000)).Value = strFilepath.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@filename", SqlDbType.NVarChar)).Value = strFilename.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@fileuniquename", SqlDbType.NVarChar)).Value = strFileuniquename.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@attached_date", SqlDbType.Date)).Value = dtAttached_Date
                    cmd.Parameters.Add(New SqlParameter("@file_size", SqlDbType.Int)).Value = intFile_size

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppAttach_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

    Public Function DeleteApplicantAttachment(ByVal strCompCode As String _
                                                , intComUnkID As Integer _
                                                , intApplicantUnkid As Integer _
                                                , intAttachfiletranUnkid As Integer
                                                ) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procDeleteApplicantAttachment"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@attachfiletranunkid", SqlDbType.Int)).Value = intAttachfiletranUnkid

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppAttach_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

    Public Function IsExistApplicantAttachment(ByVal strCompCode As String _
                                                  , intComUnkID As Integer _
                                                  , intApplicantUnkid As Integer _
                                                  , intAttachfiletranUnkid As Integer _
                                                  , intModuleRefId As Integer _
                                                  , intAttachRefId As Integer _
                                                  , strFilename As String
                                                  ) As Boolean
        Dim blnIsExist As Boolean = False
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procIsExistApplicantAttachment"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@attachfiletranunkid", SqlDbType.Int)).Value = intAttachfiletranUnkid
                    cmd.Parameters.Add(New SqlParameter("@modulerefid", SqlDbType.Int)).Value = intModuleRefId
                    cmd.Parameters.Add(New SqlParameter("@attachrefid", SqlDbType.Int)).Value = intAttachRefId
                    cmd.Parameters.Add(New SqlParameter("@filename", SqlDbType.NVarChar)).Value = strFilename.Trim.Replace("'", "''")

                    cmd.Parameters.Add(New SqlParameter("@result", SqlDbType.Bit)).Value = False
                    cmd.Parameters("@result").Direction = ParameterDirection.Output

                    cmd.ExecuteNonQuery()

                    blnIsExist = CBool(cmd.Parameters("@result").Value)

                End Using
            End Using

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return blnIsExist
    End Function

#End Region

End Class
