﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ApplicantQualification

    '''<summary>
    '''pnlForm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlForm As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHeader As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LanguageOpner control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LanguageOpner As Global.Aruti_Online_Recruitment.LanguageOpner

    '''<summary>
    '''lblQualification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblQualification As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSubHeader1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSubHeader1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''UpdatePanel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lblqalifiawagp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblqalifiawagp As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkqualificationgroup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkqualificationgroup As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''pnlQualficationGrp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlQualficationGrp As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlAwardgp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAwardgp As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblAwardgpMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAwardgpMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlOtherQualficationGrp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlOtherQualficationGrp As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtOtherQualiGroup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOtherQualiGroup As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnHideOtherQualiGrp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHideOtherQualiGrp As Global.System.Web.UI.HtmlControls.HtmlButton

    '''<summary>
    '''lblOtherQualiGroupMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOtherQualiGroupMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblQuliAwrd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblQuliAwrd As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkqualificationAward control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkqualificationAward As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''pnlQualficationAward control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlQualficationAward As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlAward control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAward As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblAwardMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAwardMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlOtherQualficationAward control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlOtherQualficationAward As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtOtherQualification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOtherQualification As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnHideOtherQuali control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHideOtherQuali As Global.System.Web.UI.HtmlControls.HtmlButton

    '''<summary>
    '''lblOtherQualificationMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOtherQualificationMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblResult control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblResult As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkResultCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkResultCode As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''pnlResultCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlResultCode As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ddlResultCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlResultCode As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblResultCodeMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblResultCodeMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlOtherResultCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlOtherResultCode As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtOtherResultCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOtherResultCode As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnHideOtherResultCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHideOtherResultCode As Global.System.Web.UI.HtmlControls.HtmlButton

    '''<summary>
    '''lblOtherResultCodeMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOtherResultCodeMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''UpdatePanel3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel3 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lblInstitute control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInstitute As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkInstitute control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkInstitute As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''pnlInstitute control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlInstitute As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''drpInstitute control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents drpInstitute As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''pnlOtherInstitute control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlOtherInstitute As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtOtherInstitution control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOtherInstitution As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnHideOtherInstitute control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHideOtherInstitute As Global.System.Web.UI.HtmlControls.HtmlButton

    '''<summary>
    '''lblAwardDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAwardDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''dtAwardDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtAwardDate As Global.Aruti_Online_Recruitment.DateCtrl

    '''<summary>
    '''lblAwardDateMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAwardDateMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDateto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDateto As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''dtdateto control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtdateto As Global.Aruti_Online_Recruitment.DateCtrl

    '''<summary>
    '''lblDatetoMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDatetoMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDatetoMsg2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDatetoMsg2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlGPA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlGPA As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblGPA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblGPA As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtGPAcode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtGPAcode As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''FilteredTextBoxExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FilteredTextBoxExtender1 As Global.AjaxControlToolkit.FilteredTextBoxExtender

    '''<summary>
    '''lblGPAMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblGPAMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblGPAMsg2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblGPAMsg2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlCertNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlCertNo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblCertiNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCertiNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtCertiNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCertiNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblCertNoMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCertNoMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlRemark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlRemark As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblRemark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRemark As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtQualiremark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtQualiremark As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''pnlGPANotApplicable control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlGPANotApplicable As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''dtDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtDate As Global.Aruti_Online_Recruitment.DateCtrl

    '''<summary>
    '''lblGPNotApplicable control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblGPNotApplicable As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkNaGPA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkNaGPA As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''lblRefNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRefNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtRefno control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRefno As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''pnlHighestQuali control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlHighestQuali As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lvlHigh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lvlHigh As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''chkHighestqualification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkHighestqualification As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''pnlQualificationAttachmentMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlQualificationAttachmentMsg As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblQualificationAttachmentUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblQualificationAttachmentUpload As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlQualificationAttachment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlQualificationAttachment As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''LblQualificationAttachType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblQualificationAttachType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlQualificationAttachType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlQualificationAttachType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''LblQualificationAttachTypeMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblQualificationAttachTypeMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''flQualificationAttachment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents flQualificationAttachment As Global.Aruti_Online_Recruitment.FileUpload

    '''<summary>
    '''lblAttachMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAttachMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlFileQualificationAttachment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlFileQualificationAttachment As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblFileExtQualificationAttachment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFileExtQualificationAttachment As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFileNameQualificationAttachment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFileNameQualificationAttachment As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDocSizeQualificationAttachment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDocSizeQualificationAttachment As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''hfDocSizeQualificationAttachment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfDocSizeQualificationAttachment As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfFileNameQualificationAttachment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfFileNameQualificationAttachment As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''btnAddQuali control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddQuali As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblExistMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblExistMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSaveMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSaveMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblUpdateMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUpdateMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDeleteMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDeleteMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDeleteConfMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDeleteConfMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOtherNotInListmsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOtherNotInListmsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''pnl_grdQualification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnl_grdQualification As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''colhQualification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents colhQualification As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''colhInstitute control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents colhInstitute As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''colhResultcode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents colhResultcode As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''colhDuration control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents colhDuration As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''colhGpacode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents colhGpacode As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lvQualification control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lvQualification As Global.System.Web.UI.WebControls.ListView

    '''<summary>
    '''objodsQuali control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents objodsQuali As Global.System.Web.UI.WebControls.ObjectDataSource

    '''<summary>
    '''pnlAttachQuali control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAttachQuali As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblSubHeader2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSubHeader2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDocTypeQuali control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDocTypeQuali As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlDocTypeQuali control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDocTypeQuali As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblDocTypeQualiMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDocTypeQualiMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUpload As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''flUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents flUpload As Global.Aruti_Online_Recruitment.FileUpload

    '''<summary>
    '''objlblTotSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents objlblTotSize As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAttachExistMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAttachExistMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAttachImageMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAttachImageMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAttachSizeMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAttachSizeMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAttachDeleteMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAttachDeleteMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAttachSaveMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAttachSaveMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAttachDeleteConfMsg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAttachDeleteConfMsg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''updgvQualiCerti control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updgvQualiCerti As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''pnl_gvQualiCerti control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnl_gvQualiCerti As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''gvQualiCerti control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvQualiCerti As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''btnRemoveImage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRemoveImage As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hfCancelLang control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfCancelLang As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''LanguageControl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LanguageControl As Global.Aruti_Online_Recruitment.LanguageControl
End Class
