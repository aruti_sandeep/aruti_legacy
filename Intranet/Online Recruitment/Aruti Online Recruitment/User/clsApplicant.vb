﻿Option Strict On
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
'Imports eZeeCommonLib
Imports System.Web
Imports System.Web.Http
Imports System.Runtime.Caching
Imports Newtonsoft.Json
Imports System.Drawing

Public Class clsApplicant

    Dim cache As ObjectCache = MemoryCache.Default

#Region " Public Properties "
    Private _encommn As clsCommon_Master.enCommonMaster
    Public Property enMastertype() As Integer
        Get
            Return CType(_encommn, Integer)
        End Get
        Set(ByVal value As Integer)
            _encommn = CType(value, clsCommon_Master.enCommonMaster)
        End Set
    End Property
#End Region


    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-880
    'NOTE : PLEASE SYNC THIS ENUM WITH ARUTI MOD GLOBAL ENUM
    Public Enum enNIDA_WEB_Service
        NIDA_VERIFY_ID = 1
        NIDA_SECURITY_QA = 2
        NIDA_OTP_VERIFY = 3
    End Enum
    'S.SANDEEP |04-MAY-2023| -- END

#Region " General Methods "

    Public Function GetCompanyInfo(ByVal intComUnkID As Integer, ByVal strCompCode As String, ByRef ByRefblnIsActive As Boolean) As Boolean
        Dim ds As New DataSet
        Try
            ByRefblnIsActive = False

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetCompanyInfo"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@company_code", SqlDbType.NVarChar)).Value = strCompCode
                        da.SelectCommand = cmd
                        da.Fill(ds, "CompanyInfo")

                    End Using
                End Using
            End Using



            For Each dsRow As DataRow In ds.Tables("CompanyInfo").Rows

                ByRefblnIsActive = CBool(dsRow.Item("isactive"))

                HttpContext.Current.Session("companyunkid") = dsRow.Item("companyunkid")
                HttpContext.Current.Session("CompCode") = dsRow.Item("company_code")
                HttpContext.Current.Session("CompName") = dsRow.Item("company_name")
                HttpContext.Current.Session("companyemail") = dsRow.Item("email")
                HttpContext.Current.Session("Database_Name") = dsRow.Item("database_name")
                HttpContext.Current.Session("company_date_added") = dsRow.Item("company_date_added")
                HttpContext.Current.Session("company_last_updated") = dsRow.Item("company_last_updated")
                HttpContext.Current.Session("authentication_code") = dsRow.Item("authentication_code")
                HttpContext.Current.Session("websiteurl") = dsRow.Item("websiteurl")
                HttpContext.Current.Session("websiteurlinternal") = dsRow.Item("websiteurlinternal")
                HttpContext.Current.Session("sendername") = dsRow.Item("sendername")
                HttpContext.Current.Session("senderaddress") = dsRow.Item("senderaddress")
                HttpContext.Current.Session("reference") = dsRow.Item("reference")
                HttpContext.Current.Session("mailserverip") = dsRow.Item("mailserverip")
                HttpContext.Current.Session("mailserverport") = dsRow.Item("mailserverport")
                HttpContext.Current.Session("username") = dsRow.Item("username").ToString
                HttpContext.Current.Session("originuserpassword") = dsRow.Item("password").ToString
                If dsRow.Item("password").ToString.Trim = "" Then
                    HttpContext.Current.Session("password") = dsRow.Item("password").ToString
                Else
                    If CBool(dsRow.Item("isemailpwd_encrypted")) = True Then
                        HttpContext.Current.Session("password") = clsSecurity.Decrypt(dsRow.Item("password").ToString, "ezee")
                    Else
                        HttpContext.Current.Session("password") = dsRow.Item("password").ToString
                    End If
                End If
                HttpContext.Current.Session("isloginssl") = dsRow.Item("isloginssl")
                HttpContext.Current.Session("mail_body") = dsRow.Item("mail_body")
                HttpContext.Current.Session("applicant_declaration") = dsRow.Item("applicant_declaration").ToString.Replace(vbCrLf, " ").Replace(vbLf, " ").Replace("'", "''")
                HttpContext.Current.Session("timezone_minute_diff") = dsRow.Item("timezone_minute_diff")
                HttpContext.Current.Session("image") = dsRow.Item("image")
                HttpContext.Current.Session("isQualiCertAttachMandatory") = dsRow.Item("isQualiCertAttachMandatory")
                If IsDBNull(dsRow.Item("admin_email")) = True Then
                    HttpContext.Current.Session("admin_email") = ""
                Else
                    HttpContext.Current.Session("admin_email") = dsRow.Item("admin_email").ToString
                End If
                If IsDBNull(dsRow.Item("Date_Format")) = True Then
                    HttpContext.Current.Session("DateFormat") = "dd-MMM-yyyy"
                Else
                    HttpContext.Current.Session("DateFormat") = dsRow.Item("Date_Format").ToString
                End If
                If IsDBNull(dsRow.Item("DateSeparator")) = True Then
                    HttpContext.Current.Session("DateSeparator") = "-"
                Else
                    HttpContext.Current.Session("DateSeparator") = dsRow.Item("DateSeparator").ToString
                End If

                'Sohail (18 Feb 2020) -- Start
                'NMB Enhancement # : On the references page on recruitment portal, provide all those fields on configuration where they can be made mandatory or optional.
                Dim strValue As String = clsConfiguration.GetConfigurationValue(strCompCode:=dsRow.Item("company_code").ToString _
                                                                  , intCompanyUnkID:=CInt(dsRow.Item("companyunkid")) _
                                                                  , strKeyName:="RECRUITMANDATORYFIELDSIDS"
                                                                  )
                Dim arrSettings() As String = strValue.Split(CChar(","))
                'Sohail (18 Feb 2020) -- End

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("MiddleNameMandatory")) = True Then
                        HttpContext.Current.Session("MiddleNameMandatory") = False
                    Else
                        HttpContext.Current.Session("MiddleNameMandatory") = CBool(dsRow.Item("MiddleNameMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.MiddleName).ToString) = True Then
                        HttpContext.Current.Session("MiddleNameMandatory") = True
                    Else
                        HttpContext.Current.Session("MiddleNameMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("GenderMandatory")) = True Then
                        HttpContext.Current.Session("GenderMandatory") = True
                    Else
                        HttpContext.Current.Session("GenderMandatory") = CBool(dsRow.Item("GenderMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.Gender).ToString) = True Then
                        HttpContext.Current.Session("GenderMandatory") = True
                    Else
                        HttpContext.Current.Session("GenderMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("BirthDateMandatory")) = True Then
                        HttpContext.Current.Session("BirthDateMandatory") = True
                    Else
                        HttpContext.Current.Session("BirthDateMandatory") = CBool(dsRow.Item("BirthDateMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.BirthDate).ToString) = True Then
                        HttpContext.Current.Session("BirthDateMandatory") = True
                    Else
                        HttpContext.Current.Session("BirthDateMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("Address1Mandatory")) = True Then
                        HttpContext.Current.Session("Address1Mandatory") = True
                    Else
                        HttpContext.Current.Session("Address1Mandatory") = CBool(dsRow.Item("Address1Mandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.Address1).ToString) = True Then
                        HttpContext.Current.Session("Address1Mandatory") = True
                    Else
                        HttpContext.Current.Session("Address1Mandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("Address2Mandatory")) = True Then
                        HttpContext.Current.Session("Address2Mandatory") = False
                    Else
                        HttpContext.Current.Session("Address2Mandatory") = CBool(dsRow.Item("Address2Mandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.Address2).ToString) = True Then
                        HttpContext.Current.Session("Address2Mandatory") = True
                    Else
                        HttpContext.Current.Session("Address2Mandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("CountryMandatory")) = True Then
                        HttpContext.Current.Session("CountryMandatory") = False
                    Else
                        HttpContext.Current.Session("CountryMandatory") = CBool(dsRow.Item("CountryMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.Country).ToString) = True Then
                        HttpContext.Current.Session("CountryMandatory") = True
                    Else
                        HttpContext.Current.Session("CountryMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("StateMandatory")) = True Then
                        HttpContext.Current.Session("StateMandatory") = False
                    Else
                        HttpContext.Current.Session("StateMandatory") = CBool(dsRow.Item("StateMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.State).ToString) = True Then
                        HttpContext.Current.Session("StateMandatory") = True
                    Else
                        HttpContext.Current.Session("StateMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("CityMandatory")) = True Then
                        HttpContext.Current.Session("CityMandatory") = False
                    Else
                        HttpContext.Current.Session("CityMandatory") = CBool(dsRow.Item("CityMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.City).ToString) = True Then
                        HttpContext.Current.Session("CityMandatory") = True
                    Else
                        HttpContext.Current.Session("CityMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("PostCodeMandatory")) = True Then
                        HttpContext.Current.Session("PostCodeMandatory") = False
                    Else
                        HttpContext.Current.Session("PostCodeMandatory") = CBool(dsRow.Item("PostCodeMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.PostCode).ToString) = True Then
                        HttpContext.Current.Session("PostCodeMandatory") = True
                    Else
                        HttpContext.Current.Session("PostCodeMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("MaritalStatusMandatory")) = True Then
                        HttpContext.Current.Session("MaritalStatusMandatory") = True
                    Else
                        HttpContext.Current.Session("MaritalStatusMandatory") = CBool(dsRow.Item("MaritalStatusMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.MaritalStatus).ToString) = True Then
                        HttpContext.Current.Session("MaritalStatusMandatory") = True
                    Else
                        HttpContext.Current.Session("MaritalStatusMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("Language1Mandatory")) = True Then
                        HttpContext.Current.Session("Language1Mandatory") = False
                    Else
                        HttpContext.Current.Session("Language1Mandatory") = CBool(dsRow.Item("Language1Mandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.Language1).ToString) = True Then
                        HttpContext.Current.Session("Language1Mandatory") = True
                    Else
                        HttpContext.Current.Session("Language1Mandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("NationalityMandatory")) = True Then
                        HttpContext.Current.Session("NationalityMandatory") = True
                    Else
                        HttpContext.Current.Session("NationalityMandatory") = CBool(dsRow.Item("NationalityMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.Nationality).ToString) = True Then
                        HttpContext.Current.Session("NationalityMandatory") = True
                    Else
                        HttpContext.Current.Session("NationalityMandatory") = False
                    End If
                End If

                Dim strSkillValue As String = clsConfiguration.GetConfigurationValue(strCompCode:=dsRow.Item("company_code").ToString _
                                                                  , intCompanyUnkID:=CInt(dsRow.Item("companyunkid")) _
                                                                  , strKeyName:="RECRUITMANDATORYSKILLS"
                                                                  )
                If strSkillValue = "" Then
                    If IsDBNull(dsRow.Item("OneSkillMandatory")) = True Then
                        'HttpContext.Current.Session("OneSkillMandatory") = False
                        HttpContext.Current.Session("RecruitMandatorySkills") = 0
                    Else
                        'HttpContext.Current.Session("OneSkillMandatory") = CBool(dsRow.Item("OneSkillMandatory"))
                        HttpContext.Current.Session("RecruitMandatorySkills") = CInt(dsRow.Item("OneSkillMandatory"))
                    End If
                Else
                    'HttpContext.Current.Session("OneSkillMandatory") = CBool(strSkillValue)
                    HttpContext.Current.Session("RecruitMandatorySkills") = CInt(strSkillValue)
                End If

                Dim strQualiValue As String = clsConfiguration.GetConfigurationValue(strCompCode:=dsRow.Item("company_code").ToString _
                                                                  , intCompanyUnkID:=CInt(dsRow.Item("companyunkid")) _
                                                                  , strKeyName:="RECRUITMANDATORYQUALIFICATIONS"
                                                                  )
                If strQualiValue = "" Then
                    If IsDBNull(dsRow.Item("OneQualificationMandatory")) = True Then
                        'HttpContext.Current.Session("OneQualificationMandatory") = True
                        HttpContext.Current.Session("RecruitMandatoryQualifications") = 1
                    Else
                        'HttpContext.Current.Session("OneQualificationMandatory") = CBool(dsRow.Item("OneQualificationMandatory"))
                        HttpContext.Current.Session("RecruitMandatoryQualifications") = CInt(dsRow.Item("OneQualificationMandatory"))
                    End If
                Else
                    'HttpContext.Current.Session("OneQualificationMandatory") = CBool(strQualiValue)
                    HttpContext.Current.Session("RecruitMandatoryQualifications") = CInt(strQualiValue)
                End If

                Dim strExpValue As String = clsConfiguration.GetConfigurationValue(strCompCode:=dsRow.Item("company_code").ToString _
                                                                  , intCompanyUnkID:=CInt(dsRow.Item("companyunkid")) _
                                                                  , strKeyName:="RECRUITMANDATORYJOBEXPERIENCES"
                                                                  )
                If strExpValue = "" Then
                    If IsDBNull(dsRow.Item("OneJobExperienceMandatory")) = True Then
                        'HttpContext.Current.Session("OneJobExperienceMandatory") = False
                        HttpContext.Current.Session("RecruitMandatoryJobExperiences") = 0
                    Else
                        'HttpContext.Current.Session("OneJobExperienceMandatory") = CBool(dsRow.Item("OneJobExperienceMandatory"))
                        HttpContext.Current.Session("RecruitMandatoryJobExperiences") = CInt(dsRow.Item("OneJobExperienceMandatory"))
                    End If
                Else
                    'HttpContext.Current.Session("OneJobExperienceMandatory") = CBool(strExpValue)
                    HttpContext.Current.Session("RecruitMandatoryJobExperiences") = CInt(strExpValue)
                End If

                Dim strRefValue As String = clsConfiguration.GetConfigurationValue(strCompCode:=dsRow.Item("company_code").ToString _
                                                                  , intCompanyUnkID:=CInt(dsRow.Item("companyunkid")) _
                                                                  , strKeyName:="RECRUITMANDATORYREFERENCES"
                                                                  )
                If strRefValue = "" Then
                    If IsDBNull(dsRow.Item("OneReferenceMandatory")) = True Then
                        'HttpContext.Current.Session("OneReferenceMandatory") = False
                        HttpContext.Current.Session("RecruitMandatoryReferences") = 0
                    Else
                        'HttpContext.Current.Session("OneReferenceMandatory") = CBool(dsRow.Item("OneReferenceMandatory"))
                        HttpContext.Current.Session("RecruitMandatoryReferences") = CInt(dsRow.Item("OneReferenceMandatory"))
                    End If
                Else
                    'HttpContext.Current.Session("OneReferenceMandatory") = CBool(strRefValue)
                    HttpContext.Current.Session("RecruitMandatoryReferences") = CInt(strRefValue)
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("RegionMandatory")) = True Then
                        HttpContext.Current.Session("RegionMandatory") = False
                    Else
                        HttpContext.Current.Session("RegionMandatory") = CBool(dsRow.Item("RegionMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.Region).ToString) = True Then
                        HttpContext.Current.Session("RegionMandatory") = True
                    Else
                        HttpContext.Current.Session("RegionMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("HighestQualificationMandatory")) = True Then
                        HttpContext.Current.Session("HighestQualificationMandatory") = False
                    Else
                        HttpContext.Current.Session("HighestQualificationMandatory") = CBool(dsRow.Item("HighestQualificationMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.HighestQualification).ToString) = True Then
                        HttpContext.Current.Session("HighestQualificationMandatory") = True
                    Else
                        HttpContext.Current.Session("HighestQualificationMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("EmployerMandatory")) = True Then
                        HttpContext.Current.Session("EmployerMandatory") = False
                    Else
                        HttpContext.Current.Session("EmployerMandatory") = CBool(dsRow.Item("EmployerMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.Employer).ToString) = True Then
                        HttpContext.Current.Session("EmployerMandatory") = True
                    Else
                        HttpContext.Current.Session("EmployerMandatory") = False
                    End If
                End If

                If IsDBNull(dsRow.Item("appqualisortbyid")) = True Then
                    HttpContext.Current.Session("appqualisortbyid") = 0
                Else
                    HttpContext.Current.Session("appqualisortbyid") = CInt(dsRow.Item("appqualisortbyid"))
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("MotherTongueMandatory")) = True Then
                        HttpContext.Current.Session("MotherTongueMandatory") = False
                    Else
                        HttpContext.Current.Session("MotherTongueMandatory") = CBool(dsRow.Item("MotherTongueMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.MotherTongue).ToString) = True Then
                        HttpContext.Current.Session("MotherTongueMandatory") = True
                    Else
                        HttpContext.Current.Session("MotherTongueMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("CurrentSalaryMandatory")) = True Then
                        HttpContext.Current.Session("CurrentSalaryMandatory") = False
                    Else
                        HttpContext.Current.Session("CurrentSalaryMandatory") = CBool(dsRow.Item("CurrentSalaryMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.CurrentSalary).ToString) = True Then
                        HttpContext.Current.Session("CurrentSalaryMandatory") = True
                    Else
                        HttpContext.Current.Session("CurrentSalaryMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("ExpectedSalaryMandatory")) = True Then
                        HttpContext.Current.Session("ExpectedSalaryMandatory") = False
                    Else
                        HttpContext.Current.Session("ExpectedSalaryMandatory") = CBool(dsRow.Item("ExpectedSalaryMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ExpectedSalary).ToString) = True Then
                        HttpContext.Current.Session("ExpectedSalaryMandatory") = True
                    Else
                        HttpContext.Current.Session("ExpectedSalaryMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("ExpectedBenefitsMandatory")) = True Then
                        HttpContext.Current.Session("ExpectedBenefitsMandatory") = False
                    Else
                        HttpContext.Current.Session("ExpectedBenefitsMandatory") = CBool(dsRow.Item("ExpectedBenefitsMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ExpectedBenefits).ToString) = True Then
                        HttpContext.Current.Session("ExpectedBenefitsMandatory") = True
                    Else
                        HttpContext.Current.Session("ExpectedBenefitsMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("NoticePeriodMandatory")) = True Then
                        HttpContext.Current.Session("NoticePeriodMandatory") = False
                    Else
                        HttpContext.Current.Session("NoticePeriodMandatory") = CBool(dsRow.Item("NoticePeriodMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.NoticePeriod).ToString) = True Then
                        HttpContext.Current.Session("NoticePeriodMandatory") = True
                    Else
                        HttpContext.Current.Session("NoticePeriodMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("EarliestPossibleStartDateMandatory")) = True Then
                        HttpContext.Current.Session("EarliestPossibleStartDateMandatory") = False
                    Else
                        HttpContext.Current.Session("EarliestPossibleStartDateMandatory") = CBool(dsRow.Item("EarliestPossibleStartDateMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.EarliestPossibleStartDate).ToString) = True Then
                        HttpContext.Current.Session("EarliestPossibleStartDateMandatory") = True
                    Else
                        HttpContext.Current.Session("EarliestPossibleStartDateMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("CommentsMandatory")) = True Then
                        HttpContext.Current.Session("CommentsMandatory") = False
                    Else
                        HttpContext.Current.Session("CommentsMandatory") = CBool(dsRow.Item("CommentsMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ApplicantComments).ToString) = True Then
                        HttpContext.Current.Session("CommentsMandatory") = True
                    Else
                        HttpContext.Current.Session("CommentsMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("VacancyFoundOutFromMandatory")) = True Then
                        HttpContext.Current.Session("VacancyFoundOutFromMandatory") = False
                    Else
                        HttpContext.Current.Session("VacancyFoundOutFromMandatory") = CBool(dsRow.Item("VacancyFoundOutFromMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.VacancyFoundOutFrom).ToString) = True Then
                        HttpContext.Current.Session("VacancyFoundOutFromMandatory") = True
                    Else
                        HttpContext.Current.Session("VacancyFoundOutFromMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("CompanyNameJobHistoryMandatory")) = True Then
                        HttpContext.Current.Session("CompanyNameJobHistoryMandatory") = True
                    Else
                        HttpContext.Current.Session("CompanyNameJobHistoryMandatory") = CBool(dsRow.Item("CompanyNameJobHistoryMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.CompanyName).ToString) = True Then
                        HttpContext.Current.Session("CompanyNameJobHistoryMandatory") = True
                    Else
                        HttpContext.Current.Session("CompanyNameJobHistoryMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    If IsDBNull(dsRow.Item("PositionHeldMandatory")) = True Then
                        HttpContext.Current.Session("PositionHeldMandatory") = False
                    Else
                        HttpContext.Current.Session("PositionHeldMandatory") = CBool(dsRow.Item("PositionHeldMandatory"))
                    End If
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.PositionHeld).ToString) = True Then
                        HttpContext.Current.Session("PositionHeldMandatory") = True
                    Else
                        HttpContext.Current.Session("PositionHeldMandatory") = False
                    End If
                End If

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                HttpContext.Current.Session("email_type") = CInt(dsRow.Item("email_type"))
                HttpContext.Current.Session("ews_url") = (dsRow.Item("ews_url")).ToString()
                HttpContext.Current.Session("ews_domain") = (dsRow.Item("ews_domain")).ToString()
                'Sohail (30 Nov 2017) -- End

                'Pinkal (01-Jan-2018) -- Start
                'Enhancement - Job seekers to get alerts if they already signed up account on the recruitment site.
                HttpContext.Current.Session("isvacancyalert") = Convert.ToBoolean(dsRow.Item("isvacancyalert"))
                HttpContext.Current.Session("maxvacancyalert") = Convert.ToInt32(dsRow.Item("maxvacancyalert"))
                HttpContext.Current.Session("issendjobconfirmationemail") = Convert.ToBoolean(dsRow.Item("issendjobconfirmationemail"))
                'Pinkal (01-Jan-2018) -- End

                'Pinkal (18-May-2018) -- Start
                'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.
                HttpContext.Current.Session("isattachapplicantcv") = Convert.ToBoolean(dsRow.Item("isattachapplicantcv"))
                'Pinkal (18-May-2018) -- End
                'Sohail (04 Jul 2019) -- Start
                'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (Setting on aruti configuration to set Curriculam Vitae and Cover Letter attachment mandatory).
                If strValue = "" Then
                    HttpContext.Current.Session("OneCurriculamVitaeMandatory") = Convert.ToBoolean(dsRow.Item("OneCurriculamVitaeMandatory"))
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.CurriculumVitae).ToString) = True Then
                        HttpContext.Current.Session("OneCurriculamVitaeMandatory") = True
                    Else
                        HttpContext.Current.Session("OneCurriculamVitaeMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    HttpContext.Current.Session("OneCoverLetterMandatory") = Convert.ToBoolean(dsRow.Item("OneCoverLetterMandatory"))
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.CoverLetter).ToString) = True Then
                        HttpContext.Current.Session("OneCoverLetterMandatory") = True
                    Else
                        HttpContext.Current.Session("OneCoverLetterMandatory") = False
                    End If
                End If
                'Sohail (04 Jul 2019) -- End]

                If strValue = "" Then
                    HttpContext.Current.Session("QualificationStartDateMandatory") = Convert.ToBoolean(dsRow.Item("QualificationStartDateMandatory"))
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.QualificationStartDate).ToString) = True Then
                        HttpContext.Current.Session("QualificationStartDateMandatory") = True
                    Else
                        HttpContext.Current.Session("QualificationStartDateMandatory") = False
                    End If
                End If

                If strValue = "" Then
                    HttpContext.Current.Session("QualificationAwardDateMandatory") = Convert.ToBoolean(dsRow.Item("QualificationAwardDateMandatory"))
                Else
                    If arrSettings.Contains(CInt(enMandatorySettingRecruitment.QualificationAwardDate).ToString) = True Then
                        HttpContext.Current.Session("QualificationAwardDateMandatory") = True
                    Else
                        HttpContext.Current.Session("QualificationAwardDateMandatory") = False
                    End If
                End If

                'Sohail (18 Feb 2020) -- Start
                'NMB Enhancement # : On the references page on recruitment portal, provide all those fields on configuration where they can be made mandatory or optional.                
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ReferencePosition).ToString) = True Then
                    HttpContext.Current.Session("ReferencePositionMandatory") = True
                Else
                    HttpContext.Current.Session("ReferencePositionMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ReferenceType).ToString) = True Then
                    HttpContext.Current.Session("ReferenceTypeMandatory") = True
                Else
                    HttpContext.Current.Session("ReferenceTypeMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ReferenceEmail).ToString) = True Then
                    HttpContext.Current.Session("ReferenceEmailMandatory") = True
                Else
                    HttpContext.Current.Session("ReferenceEmailMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ReferenceAddress).ToString) = True Then
                    HttpContext.Current.Session("ReferenceAddressMandatory") = True
                Else
                    HttpContext.Current.Session("ReferenceAddressMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ReferenceCountry).ToString) = True Then
                    HttpContext.Current.Session("ReferenceCountryMandatory") = True
                Else
                    HttpContext.Current.Session("ReferenceCountryMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ReferenceState).ToString) = True Then
                    HttpContext.Current.Session("ReferenceStateMandatory") = True
                Else
                    HttpContext.Current.Session("ReferenceStateMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ReferenceCity).ToString) = True Then
                    HttpContext.Current.Session("ReferenceCityMandatory") = True
                Else
                    HttpContext.Current.Session("ReferenceCityMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ReferenceGeneder).ToString) = True Then
                    HttpContext.Current.Session("ReferenceGenederMandatory") = True
                Else
                    HttpContext.Current.Session("ReferenceGenederMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ReferenceMobile).ToString) = True Then
                    HttpContext.Current.Session("ReferenceMobileMandatory") = True
                Else
                    HttpContext.Current.Session("ReferenceMobileMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ReferenceTelNo).ToString) = True Then
                    HttpContext.Current.Session("ReferenceTelNoMandatory") = True
                Else
                    HttpContext.Current.Session("ReferenceTelNoMandatory") = False
                End If
                'Sohail (18 Feb 2020) -- End

                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.QualificationResultCode).ToString) = True Then
                    HttpContext.Current.Session("QualificationResultCodeMandatory") = True
                Else
                    HttpContext.Current.Session("QualificationResultCodeMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.QualificationGPA).ToString) = True Then
                    HttpContext.Current.Session("QualificationGPAMandatory") = True
                Else
                    HttpContext.Current.Session("QualificationGPAMandatory") = False
                End If

                'Sohail (31 Mar 2022) - Start
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.OtherInfoAchievement).ToString) = True Then
                    HttpContext.Current.Session("OtherInfoAchievementMandatory") = True
                Else
                    HttpContext.Current.Session("OtherInfoAchievementMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.JournalsResearchPapers).ToString) = True Then
                    HttpContext.Current.Session("JournalsResearchPapersMandatory") = True
                Else
                    HttpContext.Current.Session("JournalsResearchPapersMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.CertificateNo).ToString) = True Then
                    HttpContext.Current.Session("CertificateNoMandatory") = True
                Else
                    HttpContext.Current.Session("CertificateNoMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.JobAchievement).ToString) = True Then
                    HttpContext.Current.Session("JobAchievementMandatory") = True
                Else
                    HttpContext.Current.Session("JobAchievementMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.AttachmentQualification).ToString) = True Then
                    HttpContext.Current.Session("AttachmentQualificationMandatory") = True
                Else
                    HttpContext.Current.Session("AttachmentQualificationMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.AttachmentTypeImage).ToString) = True Then
                    HttpContext.Current.Session("AllowableAttachmentTypeImage") = True
                Else
                    HttpContext.Current.Session("AllowableAttachmentTypeImage") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.AttachmentTypeDocument).ToString) = True Then
                    HttpContext.Current.Session("AllowableAttachmentTypeDocument") = True
                Else
                    HttpContext.Current.Session("AllowableAttachmentTypeDocument") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ContactPlotNo).ToString) = True Then
                    HttpContext.Current.Session("ContactPlotNoMandatory") = True
                Else
                    HttpContext.Current.Session("ContactPlotNoMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ContactEstate).ToString) = True Then
                    HttpContext.Current.Session("ContactEstateMandatory") = True
                Else
                    HttpContext.Current.Session("ContactEstateMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.ContactStreet).ToString) = True Then
                    HttpContext.Current.Session("ContactStreetMandatory") = True
                Else
                    HttpContext.Current.Session("ContactStreetMandatory") = False
                End If
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.CantactFax).ToString) = True Then
                    HttpContext.Current.Session("ContactFaxMandatory") = True
                Else
                    HttpContext.Current.Session("ContactFaxMandatory") = False
                End If


                'Pinkal (30-Sep-2023) -- Start
                '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
                If arrSettings.Contains(CInt(enMandatorySettingRecruitment.BirthCertificate).ToString) = True Then
                    HttpContext.Current.Session("BirthCertificateAttachment") = True
                Else
                    HttpContext.Current.Session("BirthCertificateAttachment") = False
                End If
                'Pinkal (30-Sep-2023) -- End


                Dim strPermAddress As String = clsConfiguration.GetConfigurationValue(strCompCode:=dsRow.Item("company_code").ToString _
                                                                  , intCompanyUnkID:=CInt(dsRow.Item("companyunkid")) _
                                                                  , strKeyName:="HIDERECRUITEMENTPERMANENTADDRESS"
                                                                  )
                If strPermAddress = "" Then
                    HttpContext.Current.Session("HideRecruitementPermanentAddress") = False
                Else
                    HttpContext.Current.Session("HideRecruitementPermanentAddress") = CBool(strPermAddress)
                End If

                Dim strHideSkill As String = clsConfiguration.GetConfigurationValue(strCompCode:=dsRow.Item("company_code").ToString _
                                                                  , intCompanyUnkID:=CInt(dsRow.Item("companyunkid")) _
                                                                  , strKeyName:="HIDERECRUITEMENTSKILLSCREEN"
                                                                  )
                If strHideSkill = "" Then
                    HttpContext.Current.Session("HideRecruitementSkillScreen") = False
                Else
                    HttpContext.Current.Session("HideRecruitementSkillScreen") = CBool(strHideSkill)
                End If

                Dim strHideQualiRemark As String = clsConfiguration.GetConfigurationValue(strCompCode:=dsRow.Item("company_code").ToString _
                                                                  , intCompanyUnkID:=CInt(dsRow.Item("companyunkid")) _
                                                                  , strKeyName:="HIDERECRUITEMENTQUALIFICATIONREMARK"
                                                                  )
                If strHideQualiRemark = "" Then
                    HttpContext.Current.Session("HideRecruitementQualificationRemark") = False
                Else
                    HttpContext.Current.Session("HideRecruitementQualificationRemark") = CBool(strHideQualiRemark)
                End If

                Dim strHideOtherInfo As String = clsConfiguration.GetConfigurationValue(strCompCode:=dsRow.Item("company_code").ToString _
                                                                  , intCompanyUnkID:=CInt(dsRow.Item("companyunkid")) _
                                                                  , strKeyName:="HIDERECRUITEMENTOTHERINFOSCREEN"
                                                                  )
                If strHideOtherInfo = "" Then
                    HttpContext.Current.Session("HideRecruitementOtherInfoScreen") = False
                Else
                    HttpContext.Current.Session("HideRecruitementOtherInfoScreen") = CBool(strHideOtherInfo)
                End If
                'Sohail (31 Mar 2022) - End


                'S.SANDEEP |04-MAY-2023| -- START
                'ISSUE/ENHANCEMENT : A1X-884                
                Dim IsNidaWRIntegrated As String = clsConfiguration.GetConfigurationValue(strCompCode:=dsRow.Item("company_code").ToString _
                                                                    , intCompanyUnkID:=CInt(dsRow.Item("companyunkid")) _
                                                                    , strKeyName:="ISNIDAWRINTEGRATED"
                                                                    )
                If IsNidaWRIntegrated = "" Then
                    HttpContext.Current.Session("IsNidaWRIntegrated") = False
                Else
                    HttpContext.Current.Session("IsNidaWRIntegrated") = CBool(IsNidaWRIntegrated)
                End If

                For Each tstEnum As enNIDA_WEB_Service In System.Enum.GetValues(GetType(enNIDA_WEB_Service))
                    Dim SrvUrl As String = clsConfiguration.GetConfigurationValue(strCompCode:=dsRow.Item("company_code").ToString _
                                                                    , intCompanyUnkID:=CInt(dsRow.Item("companyunkid")) _
                                                                    , strKeyName:="_NidaSrv_" & CInt(tstEnum).ToString() & ""
                                                                    )
                    Select Case CInt(tstEnum)
                        Case enNIDA_WEB_Service.NIDA_VERIFY_ID
                            If SrvUrl = "" Then
                                HttpContext.Current.Session("NIDA_VERIFY_ID") = ""
                            Else
                                HttpContext.Current.Session("NIDA_VERIFY_ID") = SrvUrl
                            End If
                        Case enNIDA_WEB_Service.NIDA_SECURITY_QA
                            If SrvUrl = "" Then
                                HttpContext.Current.Session("NIDA_SECURITY_QA") = ""
                            Else
                                HttpContext.Current.Session("NIDA_SECURITY_QA") = SrvUrl
                            End If
                        Case enNIDA_WEB_Service.NIDA_OTP_VERIFY
                            If SrvUrl = "" Then
                                HttpContext.Current.Session("NIDA_OTP_VERIFY") = ""
                            Else
                                HttpContext.Current.Session("NIDA_OTP_VERIFY") = SrvUrl
                            End If
                    End Select
                Next
                'S.SANDEEP |04-MAY-2023| -- END

            Next

            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START            
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Function

    Public Shared Function ClearSessionForSAdmin() As Boolean
        Try

            HttpContext.Current.Session("companyunkid") = 0
            HttpContext.Current.Session("CompCode") = ""
            HttpContext.Current.Session("CompName") = ""
            HttpContext.Current.Session("companyemail") = ""
            HttpContext.Current.Session("Database_Name") = ""
            HttpContext.Current.Session("company_date_added") = ""
            HttpContext.Current.Session("company_last_updated") = ""
            HttpContext.Current.Session("authentication_code") = ""
            HttpContext.Current.Session("websiteurl") = ""
            HttpContext.Current.Session("websiteurlinternal") = ""
            HttpContext.Current.Session("sendername") = ""
            HttpContext.Current.Session("senderaddress") = ""
            HttpContext.Current.Session("reference") = ""
            HttpContext.Current.Session("mailserverip") = ""
            HttpContext.Current.Session("mailserverport") = 0
            HttpContext.Current.Session("username") = ""
            HttpContext.Current.Session("password") = ""
            HttpContext.Current.Session("isloginssl") = Nothing
            HttpContext.Current.Session("mail_body") = ""
            HttpContext.Current.Session("applicant_declaration") = ""
            HttpContext.Current.Session("timezone_minute_diff") = Nothing
            HttpContext.Current.Session("image") = Nothing
            HttpContext.Current.Session("isQualiCertAttachMandatory") = Nothing
            HttpContext.Current.Session("admin_email") = ""

            Return True

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function

    'Public Function GetUserID(strEmail As String) As String
    '    Dim UserID As String = ""
    '    Try
    '        Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
    '        Using con As New SqlConnection(strConn)
    '            con.Open()

    '            Using cmd As New SqlCommand("SELECT UserId FROM aspnet_Membership WHERE Email = @Email", con)

    '                cmd.CommandType = CommandType.Text
    '                cmd.Parameters.AddWithValue("@Email", strEmail)

    '                UserID = cmd.ExecuteScalar().ToString()
    '            End Using
    '        End Using

    '    Catch ex As Exception
    '        'S.SANDEEP [03-NOV-2016] -- START
    '        'Throw New Exception(ex.Message)
    '        Global_asax.CatchException(ex, System.Web.HttpContext.Current)
    '        'S.SANDEEP [03-NOV-2016] -- END
    '    End Try
    '    Return UserID
    'End Function

    Public Function GetApplicantUnkid(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , strUserID As String _
                                     , strEmail As String
                                     ) As Integer
        Dim intApplicantId As Integer = 0
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetApplicantUnkid"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@UserId", SqlDbType.UniqueIdentifier)).Value = New Guid(strUserID)
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail

                        intApplicantId = CInt(cmd.ExecuteScalar())

                    End Using
                End Using
            End Using
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START            
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return intApplicantId
    End Function

    Public Function AssignNewCompany(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , strEmail As String
                                     ) As Integer
        Dim intApplicantId As Integer = 0
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procAssignNewCompany"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail

                        intApplicantId = CInt(cmd.ExecuteScalar())

                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return intApplicantId
    End Function

    Public Function GetCommonMaster(ByVal strCompCode As String, intComUnkID As Integer, ByVal enmastertype As clsCommon_Master.enCommonMaster) As DataSet
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetCommonMaster"
            Dim intMasterType As Integer = CInt(enmastertype)

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@mastertype", SqlDbType.Int)).Value = intMasterType

                        If enmastertype = clsCommon_Master.enCommonMaster.RELATIONS Then
                            cmd.Parameters.Add(New SqlParameter("@isrefreeallowed", True))
                        End If
                        If enmastertype = clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES Then
                            cmd.Parameters.Add(New SqlParameter("@issyncwithrecruitment", True))
                        End If
                        If enmastertype = clsCommon_Master.enCommonMaster.VACANCY_SOURCE Then
                            cmd.Parameters.Add(New SqlParameter("@issyncwithrecruitment", True))
                        End If


                        da.SelectCommand = cmd
                        da.Fill(ds, "CommonMaster")

                    End Using
                End Using
            End Using

            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Columns.Count <= 0 Then
                ds.Tables(0).Columns.Add("masterunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                ds.Tables(0).Columns.Add("Name", System.Type.GetType("System.String")).DefaultValue = ""
            End If

            Dim dr As DataRow = ds.Tables(0).NewRow
            dr.Item("masterunkid") = "0"
            dr.Item("Name") = "Select"

            ds.Tables(0).Rows.InsertAt(dr, 0)

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    Public Function GenerateOTP(intNoOfChars As Integer) As String
        Dim strOTP As String = ""
        Try
            If intNoOfChars <= 0 Then Exit Try

            Dim charArr As Char() = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray()
            Dim strRandom As String = String.Empty
            Dim objran As New Random()
            Dim noofcharacters As Integer = intNoOfChars
            For i As Integer = 0 To noofcharacters - 1

                'It will not allow Repetation of Characters
                Dim pos As Integer = objran.[Next](1, charArr.Length)
                If Not strRandom.Contains(charArr.GetValue(pos).ToString()) Then
                    strRandom &= charArr.GetValue(pos).ToString
                Else
                    i -= 1
                End If
            Next

            strOTP = strRandom

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START            
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return strOTP
    End Function

    Public Function InsertResetPassword(strCompCode As String _
                                        , intCompUnkID As Integer _
                                        , strEmail As String _
                                        , strCode As String _
                                        , strOTP As String _
                                        , strSubject As String _
                                        , strMessage As String _
                                        , strLink As String
                                        ) As Boolean
        'Dim UserID As String = ""
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procInsertResetPassword"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intCompUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail
                    cmd.Parameters.Add(New SqlParameter("@code", SqlDbType.NVarChar)).Value = strCode
                    cmd.Parameters.Add(New SqlParameter("@otp", SqlDbType.NVarChar)).Value = strOTP
                    cmd.Parameters.Add(New SqlParameter("@subject", SqlDbType.NVarChar)).Value = strSubject
                    cmd.Parameters.Add(New SqlParameter("@message", SqlDbType.NVarChar, 5000)).Value = strMessage
                    cmd.Parameters.Add(New SqlParameter("@link", SqlDbType.NVarChar, 1000)).Value = strLink

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Return True

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START            
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END   

            Return False
        End Try
    End Function

    Public Function IsExistResetPassword(strCompCode As String _
                                        , intCompUnkID As Integer _
                                        , strEmail As String _
                                        , strCode As String _
                                        , strOTP As String _
                                        , ByRef strRefResult As String
                                        ) As Boolean
        Dim blnResult As Boolean = False
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procIsExistResetPassword"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intCompUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail
                    cmd.Parameters.Add(New SqlParameter("@code", SqlDbType.NVarChar)).Value = strCode
                    cmd.Parameters.Add(New SqlParameter("@otp", SqlDbType.NVarChar)).Value = strOTP.Replace("'", "''")

                    cmd.Parameters.Add(New SqlParameter("@result", SqlDbType.NVarChar, 510)).Value = ""
                    cmd.Parameters("@result").Direction = ParameterDirection.Output

                    cmd.ExecuteNonQuery()

                    strRefResult = cmd.Parameters("@result").Value.ToString

                    If strRefResult = "1" Then
                        blnResult = True
                    End If

                End Using
            End Using

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START            
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try
        Return blnResult
    End Function

    Public Function ResetPasswordActivated(strCompCode As String _
                                            , intCompUnkID As Integer _
                                            , strEmail As String _
                                            , strCode As String _
                                            , strOTP As String
                                            ) As Boolean
        Dim UserID As String = ""
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procResetPasswordActivated"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intCompUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail
                    cmd.Parameters.Add(New SqlParameter("@code", SqlDbType.NVarChar)).Value = strCode
                    cmd.Parameters.Add(New SqlParameter("@otp", SqlDbType.NVarChar)).Value = strOTP

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Return True

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try
    End Function

    Public Function InsertActivationLink(ByVal strCompCode As String _
                                        , intCompUnkID As Integer _
                                        , strUserId As String _
                                        , strEmail As String _
                                        , strSubject As String _
                                        , strMessage As String _
                                        , strLink As String _
                                        , ByRef intRet_UnkId As Integer
                                        ) As Boolean
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End
            intRet_UnkId = 0

            Dim strQ As String = "procInsertActivationLink"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intCompUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@UserId", SqlDbType.NVarChar)).Value = strUserId
                    cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail
                    cmd.Parameters.Add(New SqlParameter("@subject", SqlDbType.NVarChar)).Value = strSubject
                    cmd.Parameters.Add(New SqlParameter("@message", SqlDbType.NVarChar, 5000)).Value = strMessage
                    cmd.Parameters.Add(New SqlParameter("@link", SqlDbType.NVarChar, 1000)).Value = strLink

                    'Sohail (20 Dec 2017) -- Start
                    'Enhancement - Multilple admin email registration.
                    'cmd.ExecuteNonQuery()
                    cmd.Parameters.Add(New SqlParameter("@ret_unkid", SqlDbType.Int)).Value = 0
                    cmd.Parameters("@ret_unkid").Direction = ParameterDirection.Output

                    cmd.ExecuteNonQuery()

                    intRet_UnkId = CInt(cmd.Parameters("@ret_unkid").Value)
                    'Sohail (20 Dec 2017) -- End


                End Using
            End Using

            Return True

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function

    Public Function ActivateUser(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , strEmail As String _
                                     , strUserID As String _
                                     , Optional blnSkipActivation As Boolean = False
                                     ) As Integer
        'Sohail (16 Aug 2019) -- Start
        'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
        'Optional blnSkipActivation As Boolean = False
        'Sohail (16 Aug 2019) -- End
        Dim intApplicantID As Integer = 0
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procActivateUser"

            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            'If HttpContext.Current.Session("admin_email") Is Nothing OrElse (HttpContext.Current.Session("admin_email").ToString.Split(CChar(";")).Contains(strEmail.Trim) = False AndAlso HttpContext.Current.Session("admin_email").ToString.Split(CChar(",")).Contains(strEmail.Trim) = False) Then
            If 1 = 1 Then
                'Sohail (16 Aug 2019) -- End
                'Sohail (20 Dec 2017) -- End


                Using con As New SqlConnection(strConn)
                    con.Open()

                    Using cmd As New SqlCommand(strQ, con)
                        cmd.CommandTimeout = 0

                        Dim profile As UserProfile = UserProfile.GetUserProfile(strEmail)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = CInt(intComUnkID)
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@userid", SqlDbType.UniqueIdentifier)).Value = New Guid(strUserID)
                        cmd.Parameters.Add(New SqlParameter("@firstname", SqlDbType.NVarChar)).Value = profile.FirstName
                        cmd.Parameters.Add(New SqlParameter("@othername", SqlDbType.NVarChar)).Value = profile.MiddleName
                        cmd.Parameters.Add(New SqlParameter("@surname", SqlDbType.NVarChar)).Value = profile.LastName
                        cmd.Parameters.Add(New SqlParameter("@Email", SqlDbType.NVarChar)).Value = strEmail
                        cmd.Parameters.Add(New SqlParameter("@gender", SqlDbType.Int)).Value = profile.Gender
                        cmd.Parameters.Add(New SqlParameter("@birth_date", SqlDbType.DateTime)).Value = profile.BirthDate
                        cmd.Parameters.Add(New SqlParameter("@present_mobileno", SqlDbType.NVarChar)).Value = profile.MobileNo
                        cmd.Parameters.Add(New SqlParameter("@ret_applicantunkid", SqlDbType.Int)).Value = 0
                        'Sohail (16 Aug 2019) -- Start
                        'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID. (Allow admin to access user panel)
                        cmd.Parameters.Add(New SqlParameter("@skip_activation", SqlDbType.Bit)).Value = blnSkipActivation
                        'Sohail (16 Aug 2019) -- End

                        'S.SANDEEP |04-MAY-2023| -- START
                        'ISSUE/ENHANCEMENT : A1X-833                        
                        cmd.Parameters.Add(New SqlParameter("@nin", SqlDbType.NVarChar)).Value = profile.NIN
                        cmd.Parameters.Add(New SqlParameter("@tin", SqlDbType.NVarChar)).Value = profile.TIN
                        cmd.Parameters.Add(New SqlParameter("@ninmobile", SqlDbType.NVarChar)).Value = profile.NinMobile
                        cmd.Parameters.Add(New SqlParameter("@village", SqlDbType.NVarChar)).Value = profile.Village
                        cmd.Parameters.Add(New SqlParameter("@phonenum", SqlDbType.NVarChar)).Value = profile.Phonenumber
                        cmd.Parameters.Add(New SqlParameter("@nationality", SqlDbType.Int)).Value = profile.Nationality

                        Dim _base64String As String = ""
                        _base64String = clsApplicant.GetOnlyBase64String(profile.ApplicantPhoto.Trim)
                        If _base64String.Trim.Length > 0 Then
                            cmd.Parameters.Add(New SqlParameter("@applicant_photo", SqlDbType.Binary)).Value = DirectCast(Convert.FromBase64String(_base64String), Byte())
                        Else
                            cmd.Parameters.Add(New SqlParameter("@applicant_photo", SqlDbType.Binary)).Value = DBNull.Value
                        End If
                        _base64String = ""
                        _base64String = clsApplicant.GetOnlyBase64String(profile.ApplicantSignature.Trim)
                        If _base64String.Trim.Length > 0 Then
                            cmd.Parameters.Add(New SqlParameter("@applicant_signature", SqlDbType.Binary)).Value = DirectCast(Convert.FromBase64String(_base64String), Byte())
                        Else
                            cmd.Parameters.Add(New SqlParameter("@applicant_signature", SqlDbType.Binary)).Value = DBNull.Value
                        End If
                        'S.SANDEEP |04-MAY-2023| -- END

                        cmd.Parameters("@ret_applicantunkid").Direction = ParameterDirection.Output


                        cmd.ExecuteNonQuery()

                        If CInt(cmd.Parameters("@ret_applicantunkid").Value) > 0 Then
                            intApplicantID = CInt(cmd.Parameters("@ret_applicantunkid").Value)
                        Else

                        End If

                    End Using
                End Using
            Else 'Admin Email Activation

                Dim usr As MembershipUser = Membership.GetUser(strEmail)
                usr.IsApproved = True
                Membership.UpdateUser(usr)

            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return intApplicantID
    End Function


    Public Function UpdateActivationEmailSent(intActivationLinkUnkId As Integer _
                                              , blnIsEmailSent As Boolean _
                                              , strAuditEmail As String _
                                              , dtAuditDateTime As Date
                                        ) As Boolean
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procUpdateActivationEMailSend"

            If strAuditEmail Is Nothing Then strAuditEmail = ""

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@activationlinkunkid", SqlDbType.Int)).Value = intActivationLinkUnkId
                    cmd.Parameters.Add(New SqlParameter("@isemailsent", SqlDbType.Bit)).Value = blnIsEmailSent
                    cmd.Parameters.Add(New SqlParameter("@auditemail", SqlDbType.NVarChar)).Value = strAuditEmail.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@auditdatetime", SqlDbType.Date)).Value = dtAuditDateTime

                    cmd.ExecuteNonQuery()
                End Using
            End Using

            Return True

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function

    Public Function GetFailedPasswordAttemptCount(strUserID As String
                                     ) As Integer
        Dim intCount As Integer = 0
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetFailedPasswordAttemptCount"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@UserId", SqlDbType.UniqueIdentifier)).Value = New Guid(strUserID)

                        intCount = CInt(cmd.ExecuteScalar())

                    End Using
                End Using
            End Using
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START            
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return intCount
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetActivationLink(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , strEmail As String _
                                     , intIsApproved As Integer _
                                     , startRowIndex As Integer _
                                     , intPageSize As Integer _
                                     , strSortExpression As String _
                                     , isemailsent As Integer
                                     ) As DataSet
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetActivationLink"

            startRowIndex = Convert.ToInt32(startRowIndex / intPageSize) + 1

            If strCompCode Is Nothing Then strCompCode = ""
            If strEmail Is Nothing Then strEmail = ""
            If strSortExpression Is Nothing Then strSortExpression = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@isapproved", SqlDbType.Int)).Value = intIsApproved
                        cmd.Parameters.Add(New SqlParameter("@CurrentPage", SqlDbType.Int)).Value = startRowIndex
                        cmd.Parameters.Add(New SqlParameter("@PageSize", SqlDbType.Int)).Value = intPageSize
                        cmd.Parameters.Add(New SqlParameter("@SortExpression", SqlDbType.NVarChar)).Value = strSortExpression.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@isemailsent", SqlDbType.Int)).Value = isemailsent

                        da.SelectCommand = cmd
                        da.Fill(ds, "ActivationLink")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetActivationLinkTotal(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , strEmail As String _
                                     , intIsApproved As Integer _
                                     , startRowIndex As Integer _
                                     , intPageSize As Integer _
                                     , strSortExpression As String _
                                     , isemailsent As Integer
                                     ) As Integer
        Dim ds As New DataSet
        Dim intCount As Integer = 0
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetActivationLinkTotal"

            If strCompCode Is Nothing Then strCompCode = ""
            If strEmail Is Nothing Then strEmail = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@isapproved", SqlDbType.Int)).Value = intIsApproved
                        cmd.Parameters.Add(New SqlParameter("@isemailsent", SqlDbType.Int)).Value = isemailsent

                        Dim intCnt As Object = cmd.ExecuteScalar()

                        intCount = DirectCast(intCnt, Integer)

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return intCount
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetWithoutActivationLink(ByVal strCompCode As String _
                                , intComUnkID As Integer _
                                , strEmail As String _
                                , startRowIndex As Integer _
                                , intPageSize As Integer _
                                , strSortExpression As String
                                ) As DataSet
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetEmailWithoutLink"

            startRowIndex = Convert.ToInt32(startRowIndex / intPageSize) + 1

            If strCompCode Is Nothing Then strCompCode = ""
            If strEmail Is Nothing Then strEmail = ""
            If strSortExpression Is Nothing Then strSortExpression = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@CurrentPage", SqlDbType.Int)).Value = startRowIndex
                        cmd.Parameters.Add(New SqlParameter("@PageSize", SqlDbType.Int)).Value = intPageSize
                        cmd.Parameters.Add(New SqlParameter("@SortExpression", SqlDbType.NVarChar)).Value = strSortExpression.Trim.Replace("'", "''")

                        da.SelectCommand = cmd
                        da.Fill(ds, "ActivationLink")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetWithoutActivationLinkTotal(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , strEmail As String _
                                     , startRowIndex As Integer _
                                     , intPageSize As Integer _
                                     , strSortExpression As String
                                     ) As Integer

        Dim ds As New DataSet
        Dim intCount As Integer = 0
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetEmailWithoutLinkTotal"

            If strCompCode Is Nothing Then strCompCode = ""
            If strEmail Is Nothing Then strEmail = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")

                        Dim intCnt As Object = cmd.ExecuteScalar()

                        intCount = DirectCast(intCnt, Integer)

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return intCount
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetResetPasswordLink(ByVal strCompCode As String _
                                            , intComUnkID As Integer _
                                            , strEmail As String _
                                            , startRowIndex As Integer _
                                            , intPageSize As Integer _
                                            , strSortExpression As String
                                            ) As DataSet
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetResetUserPasswordLink"

            startRowIndex = Convert.ToInt32(startRowIndex / intPageSize) + 1

            If strCompCode Is Nothing Then strCompCode = ""
            If strEmail Is Nothing Then strEmail = ""
            If strSortExpression Is Nothing Then strSortExpression = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)
                        cmd.CommandTimeout = CInt(HttpContext.Current.Session("CommandTimeOut"))

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@CurrentPage", SqlDbType.Int)).Value = startRowIndex
                        cmd.Parameters.Add(New SqlParameter("@PageSize", SqlDbType.Int)).Value = intPageSize
                        cmd.Parameters.Add(New SqlParameter("@SortExpression", SqlDbType.NVarChar)).Value = strSortExpression.Trim.Replace("'", "''")

                        da.SelectCommand = cmd
                        da.Fill(ds, "ResetPasswordLink")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetResetPasswordLinkTotal(ByVal strCompCode As String _
                                            , intComUnkID As Integer _
                                            , strEmail As String _
                                            , startRowIndex As Integer _
                                            , intPageSize As Integer _
                                            , strSortExpression As String
                                            ) As Integer
        Dim ds As New DataSet
        Dim intCount As Integer = 0
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetResetUserPasswordLinkTotal"

            If strCompCode Is Nothing Then strCompCode = ""
            If strEmail Is Nothing Then strEmail = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)
                        cmd.CommandTimeout = CInt(HttpContext.Current.Session("CommandTimeOut"))

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")

                        Dim intCnt As Object = cmd.ExecuteScalar()

                        intCount = DirectCast(intCnt, Integer)

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return intCount
    End Function

    Public Function GetCompanyAuthenticationCode(ByVal strUserId As String
                                                ) As String
        Dim ds As New DataSet
        Dim strResult As String = ""
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetAuthenticationCode"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@UserId", SqlDbType.NVarChar, 510)).Value = strUserId

                        cmd.Parameters.Add(New SqlParameter("@result", SqlDbType.NVarChar, 510)).Value = ""
                        cmd.Parameters("@result").Direction = ParameterDirection.Output

                        cmd.ExecuteNonQuery()

                        strResult = cmd.Parameters("@result").Value.ToString

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return strResult
    End Function

    Public Function IsValidForApplyVacancy(strCompCode As String _
                                           , intCompanyunkid As Integer _
                                           , intApplicantunkid As Integer _
                                           , blnAddLinks As Boolean _
                                           , ByRef strMessage As String
                                           ) As Boolean

        Dim dsList As DataSet
        Dim blnValidate As Boolean = True
        Dim strMsg As String = ""
        Dim i As Integer = 0
        strMessage = ""
        Try

            dsList = (New clsPersonalInfo).GetPersonalInfo(strCompCode, intCompanyunkid, intApplicantunkid)
            If dsList.Tables(0).Rows.Count > 0 Then
                If dsList.Tables(0).Rows(0).Item("firstname").ToString.Length <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>First Name</B> <br/>"
                    blnValidate = False
                End If
                If dsList.Tables(0).Rows(0).Item("othername").ToString.Length <= 0 AndAlso CBool(HttpContext.Current.Session("MiddleNameMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Other Name</B> <br/>"
                    blnValidate = False
                ElseIf dsList.Tables(0).Rows(0).Item("othername").ToString.Length <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Other Name [Optional] <br/>"
                End If
                If dsList.Tables(0).Rows(0).Item("surname").ToString.Length <= 0 Then
                    i = i + 1
                    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". SurName <br/>"
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>SurName</B> <br/>"
                    blnValidate = False
                End If
                'Sohail (18 Feb 2020) -- Start
                'NMB Enhancement # : On job master,  Need a tab to define the language(s) attached to the job. Those languages will be displayed on job report.
                If HttpContext.Current.Session("Vacancy").ToString = "Int" AndAlso dsList.Tables(0).Rows(0).Item("employeecode").ToString.Length <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Employee Code</B> <br/>"
                    blnValidate = False
                End If
                'Sohail (18 Feb 2020) -- End
                If CInt(dsList.Tables(0).Rows(0).Item("gender")) <= 0 AndAlso CBool(HttpContext.Current.Session("GenderMandatory")) = True Then
                    i = i + 1
                    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Gender <br/>"
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Gender <br/>"
                    blnValidate = False
                ElseIf CInt(dsList.Tables(0).Rows(0).Item("gender")) <= 0 Then
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Gender [Optional] <br/>"
                End If
                If ((IsDBNull(dsList.Tables(0).Rows(0).Item("birth_date")) OrElse CDate(dsList.Tables(0).Rows(0).Item("birth_date")) = CDate("01/01/1900")) AndAlso CBool(HttpContext.Current.Session("BirthDateMandatory")) = True) Then
                    i = i + 1
                    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Birth Date <br/>"
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Birth Date</B> <br/>"
                    blnValidate = False
                ElseIf IsDBNull(dsList.Tables(0).Rows(0).Item("birth_date")) OrElse CDate(dsList.Tables(0).Rows(0).Item("birth_date")) = CDate("01/01/1900") Then
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Birth Date [Optional] <br/>"
                End If
                If dsList.Tables(0).Rows(0).Item("present_mobileno").ToString.Length <= 0 Then
                    i = i + 1
                    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Mobile No. <br/>"
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Mobile No.</B> <br/>"
                    blnValidate = False
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 AndAlso CBool(HttpContext.Current.Session("NationalityMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Nationality.</B> <br/>"
                    blnValidate = False
                ElseIf CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. [Optional] <br/>"
                End If
                'S.SANDEEP |04-MAY-2023| -- START
                'ISSUE/ENHANCEMENT : A1X-833
                If HttpContext.Current.Session("CompCode").ToString().ToLower() = "tra" Then
                    If CInt(dsList.Tables(0).Rows(0).Item("residency")) <= 0 Then
                        i = i + 1
                        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Residency Type.</B> <br/>"
                        blnValidate = False
                    End If

                    If CStr(dsList.Tables(0).Rows(0).Item("indexno")).Trim.Length <= 0 Then
                        i = i + 1
                        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Index No.</B> <br/>"
                        blnValidate = False
                    End If
                End If
                'S.SANDEEP |04-MAY-2023| -- END
            Else
                'strMsg &= "&nbsp;&nbsp;&nbsp;1. First Name <br/>"
                'strMsg &= "&nbsp;&nbsp;&nbsp;2. SurName <br/>"
                'strMsg &= "&nbsp;&nbsp;&nbsp;3. Gender <br/>"
                'strMsg &= "&nbsp;&nbsp;&nbsp;4. Birth Date <br/>"
                'strMsg &= "&nbsp;&nbsp;&nbsp;5. Mobile Number <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>First Name</B> <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>SurName</B> <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Gender</B> <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Birth Date</B> <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Mobile Number</B> <br/>"
                i = 5
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. <br/>"
                i = i + 1
                blnValidate = False
            End If
            If strMsg.Trim <> "" Then
                If blnAddLinks = True Then
                    strMessage &= "<h5>My Profile > <a href='PersonalInfo.aspx'>Personal Info</a> > </h5>" & strMsg
                Else
                    strMessage &= "<h5>My Profile > Personal Info > </h5>" & strMsg
                End If

            End If

            strMsg = ""
            dsList = (New clsContactDetails).GetContactDetail(strCompCode, intCompanyunkid, intApplicantunkid)
            If dsList.Tables(0).Rows.Count > 0 Then
                If dsList.Tables(0).Rows(0).Item("present_address1").ToString.Length <= 0 AndAlso CBool(HttpContext.Current.Session("Address1Mandatory")) = True Then
                    i = i + 1
                    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Address 1. <br/>"
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Address 1.</B> <br/>"
                    blnValidate = False
                ElseIf dsList.Tables(0).Rows(0).Item("present_address1").ToString.Length <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Address 1. [Optional] <br/>"
                End If
                If dsList.Tables(0).Rows(0).Item("present_address2").ToString.Length <= 0 AndAlso CBool(HttpContext.Current.Session("Address2Mandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Address 2.</B> <br/>"
                    blnValidate = False
                ElseIf dsList.Tables(0).Rows(0).Item("present_address2").ToString.Length <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Address 2. [Optional] <br/>"
                End If
                If dsList.Tables(0).Rows(0).Item("present_province").ToString.Length <= 0 AndAlso CBool(HttpContext.Current.Session("RegionMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Region.</B> <br/>"
                    blnValidate = False
                ElseIf dsList.Tables(0).Rows(0).Item("present_province").ToString.Length <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Region. [Optional] <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("present_countryunkid")) <= 0 AndAlso CBool(HttpContext.Current.Session("CountryMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Current Country.</B> <br/>"
                    blnValidate = False
                ElseIf CInt(dsList.Tables(0).Rows(0).Item("present_countryunkid")) <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Country. [Optional] <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("present_stateunkid")) <= 0 AndAlso CBool(HttpContext.Current.Session("StateMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Current State.</B> <br/>"
                    blnValidate = False
                ElseIf CInt(dsList.Tables(0).Rows(0).Item("present_stateunkid")) <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current State. [Optional] <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("present_post_townunkid")) <= 0 AndAlso CBool(HttpContext.Current.Session("CityMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Current Post Town/City.</B> <br/>"
                    blnValidate = False
                ElseIf CInt(dsList.Tables(0).Rows(0).Item("present_post_townunkid")) <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Post Town/City. [Optional] <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("present_zipcode")) <= 0 AndAlso CBool(HttpContext.Current.Session("PostCodeMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Current Post Code.</B> <br/>"
                    blnValidate = False
                ElseIf CInt(dsList.Tables(0).Rows(0).Item("present_zipcode")) <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Post Code. [Optional] <br/>"
                End If
            Else
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Address 1. <br/>"
            End If
            If strMsg.Trim <> "" Then
                If blnAddLinks = True Then
                    strMessage &= "<h5>My Profile > <a href='ContactDetails.aspx'>Contact Details</a> > </h5>" & strMsg
                Else
                    strMessage &= "<h5>My Profile > Contact Details > </h5>" & strMsg
                End If
            End If

            strMsg = ""
            dsList = (New clsOtherInfo).GetOtherInfo(strCompCode, intCompanyunkid, intApplicantunkid, clsCommon_Master.enCommonMaster.LANGUAGES, clsCommon_Master.enCommonMaster.MARRIED_STATUS)
            If dsList.Tables(0).Rows.Count > 0 Then
                If CInt(dsList.Tables(0).Rows(0).Item("language1unkid")) <= 0 AndAlso CBool(HttpContext.Current.Session("Language1Mandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Language Known.</B> <br/>"
                    blnValidate = False
                ElseIf CInt(dsList.Tables(0).Rows(0).Item("language1unkid")) <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Language Known [Optional]. <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("marital_statusunkid")) <= 0 AndAlso CBool(HttpContext.Current.Session("MaritalStatusMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Marital Status.</B> <br/>"
                    blnValidate = False
                ElseIf CInt(dsList.Tables(0).Rows(0).Item("marital_statusunkid")) <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Marital Status. [Optional] <br/>"
                End If
                'If CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 AndAlso CBool(Session("NationalityMandatory")) = True Then
                '    i = i + 1
                '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. <br/>"
                'ElseIf CInt(dsList.Tables(0).Rows(0).Item("nationality")) <= 0 Then
                '    i = i + 1
                '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. [Optional] <br/>"
                'End If
                If dsList.Tables(0).Rows(0).Item("mother_tongue").ToString.Trim = "" AndAlso CBool(HttpContext.Current.Session("MotherTongueMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Mother Tongue.</B> <br/>"
                    blnValidate = False
                ElseIf dsList.Tables(0).Rows(0).Item("mother_tongue").ToString.Trim = "" Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Mother Tongue. [Optional] <br/>"
                End If
                If CDec(dsList.Tables(0).Rows(0).Item("current_salary")) <= 0 AndAlso CBool(HttpContext.Current.Session("CurrentSalaryMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Current Salary.</B> <br/>"
                    blnValidate = False
                ElseIf CInt(dsList.Tables(0).Rows(0).Item("current_salary")) <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Current Salary. [Optional] <br/>"
                End If
                'Sohail (10 Oct 2018) -- Start
                'If CDec(dsList.Tables(0).Rows(0).Item("expected_salary")) <= 0 AndAlso CBool(HttpContext.Current.Session("CurrentSalaryMandatory")) = True Then
                If CDec(dsList.Tables(0).Rows(0).Item("expected_salary")) <= 0 AndAlso CBool(HttpContext.Current.Session("ExpectedSalaryMandatory")) = True Then
                        'Sohail (10 Oct 2018) -- End
                        i = i + 1
                        strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Expected Salary.</B> <br/>"
                        blnValidate = False
                    ElseIf CInt(dsList.Tables(0).Rows(0).Item("expected_salary")) <= 0 Then
                        i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Expected Salary. [Optional] <br/>"
                End If
                If dsList.Tables(0).Rows(0).Item("expected_benefits").ToString.Trim = "" AndAlso CBool(HttpContext.Current.Session("ExpectedBenefitsMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Expected Benefits.</B> <br/>"
                    blnValidate = False
                ElseIf dsList.Tables(0).Rows(0).Item("expected_benefits").ToString.Trim = "" Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Expected Benefits. [Optional] <br/>"
                End If
                If CInt(dsList.Tables(0).Rows(0).Item("notice_period_days")) <= 0 AndAlso CBool(HttpContext.Current.Session("NoticePeriodMandatory")) = True Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Notice Period.</B> <br/>"
                    blnValidate = False
                ElseIf CInt(dsList.Tables(0).Rows(0).Item("notice_period_days")) <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Notice Period. [Optional] <br/>"
                End If
                'If CDate(dsList.Tables(0).Rows(0).Item("earliest_possible_startdate")) = CDate("01/Jan/1900") AndAlso CBool(HttpContext.Current.Session("EarliestPossibleStartDateMandatory")) = True Then
                '    i = i + 1
                '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Earliest Possible Start Date. <br/>"
                '    blnValidate = False
                'ElseIf CDate(dsList.Tables(0).Rows(0).Item("earliest_possible_startdate")) = CDate("01/Jan/1900") Then
                '    i = i + 1
                '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Earliest Possible Start Date. [Optional] <br/>"
                'End If
                'If dsList.Tables(0).Rows(0).Item("comments").ToString.Trim = "" AndAlso CBool(HttpContext.Current.Session("CommentsMandatory")) = True Then
                '    i = i + 1
                '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Comments. <br/>"
                '    blnValidate = False
                'ElseIf dsList.Tables(0).Rows(0).Item("comments").ToString.Trim = "" Then
                '    i = i + 1
                '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Comments. [Optional] <br/>"
                'End If
            Else
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Language Known [Optional]. <br/>"
                i = i + 1
                'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Nationality. <br/>"
                'strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Nationality. <br/>"
                'i = i + 1
                'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Marital Status. <br/>"
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Marital Status.<B> <br/>"
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Mother Tongue.</B> <br/>"
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Current Salary.</B> <br/>"
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Expected Salary.</B> <br/>"
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Expected Benefits.</B> <br/>"
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Notice Period.</B> <br/>"
                'i = i + 1
                'strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Earliest Possible Start Date. <br/>"
                'i = i + 1
                'strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Comments. <br/>"
                blnValidate = False
            End If
            If strMsg.Trim <> "" Then
                If blnAddLinks = True Then
                    strMessage &= "<h5>My Profile > <a href='OtherInfo.aspx'>Language and Other Information</a> > </h5>" & strMsg
                Else
                    strMessage &= "<h5>My Profile > Language and Other Information > </h5>" & strMsg
                End If
            End If


            strMsg = ""
            dsList = (New clsApplicantSkill).GetApplicantSkills(strCompCode, intCompanyunkid, clsCommon_Master.enCommonMaster.SKILL_CATEGORY, intApplicantunkid)
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("OneSkillMandatory")) = True Then
            If dsList.Tables(0).Rows.Count < CInt(HttpContext.Current.Session("RecruitMandatorySkills")) Then
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Skills. (" & HttpContext.Current.Session("RecruitMandatorySkills").ToString & ")</B> <br/>"
                blnValidate = False
            ElseIf dsList.Tables(0).Rows.Count <= 0 Then
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Skills. [Optional] <br/>"
            End If
            If strMsg.Trim <> "" Then
                If blnAddLinks = True Then
                    strMessage &= "<h5><a href='ApplicantSkill.aspx'>Skills</a> > </h5>" & strMsg
                Else
                    strMessage &= "<h5> Skills > </h5>" & strMsg
                End If
            End If


            strMsg = ""
            dsList = (New clsApplicantQualification).GetApplicantQualifications(strCompCode, intCompanyunkid, intApplicantunkid, clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, CInt(HttpContext.Current.Session("appqualisortbyid")))
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneQualificationMandatory")) = True Then
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Qualifications. <br/>"
            'ElseIf dsList.Tables(0).Rows.Count <= 0 Then
            '    i = i + 1
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Atleast one Qualification. <br/>"
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Qualifications. [Optional] <br/>"
            'End If
            If dsList.Tables(0).Rows.Count > 0 Then
                If CBool(HttpContext.Current.Session("HighestQualificationMandatory")) = True AndAlso dsList.Tables(0).Select("ishighestqualification =  1 ").Length <= 0 Then
                    i = i + 1
                    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Highest Qualification.</B> <br/>"
                    blnValidate = False
                End If
            Else
                'If CBool(HttpContext.Current.Session("OneQualificationMandatory")) = True Then
                '    i = i + 1
                '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Qualifications.</B> <br/>"
                '    blnValidate = False
                'Else
                '    i = i + 1
                '    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Atleast one Qualification. <br/>"
                '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Qualifications. [Optional] <br/>"
                'End If
            End If
            If dsList.Tables(0).Rows.Count < CInt(HttpContext.Current.Session("RecruitMandatoryQualifications")) Then
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Qualifications. (" & HttpContext.Current.Session("RecruitMandatoryQualifications").ToString & ")</B> <br/>"
                blnValidate = False
            ElseIf dsList.Tables(0).Rows.Count <= 0 Then
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Qualifications. [Optional] <br/>"
            End If

            'Sohail (11 Nov 2021) -- Start
            'NMB Enhancement: Attachments for applicants should be restricted to only CV and Cover letter on portal. Applicants should not see other document types.
            'dsList = (New clsApplicantQualification).GetApplicantAttachments(strCompCode, intCompanyunkid, intApplicantunkid, CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.QUALIFICATIONS))
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("isQualiCertAttachMandatory")) = True Then
            '    i = i + 1
            '    'strMsg &= "&nbsp;&nbsp;&nbsp;" & i.ToString & ". Atleast one Qualification Attchment. <br/>"
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Qualification Attachments.</B> <br/>"
            '    blnValidate = False
            'End If
            'Sohail (11 Nov 2021) -- End
            If strMsg.Trim <> "" Then
                If blnAddLinks = True Then
                    strMessage &= "<h5><a href='ApplicantQualification.aspx'>Qualifications</a> > </h5>" & strMsg
                Else
                    strMessage &= "<h5>Qualifications > </h5>" & strMsg
                End If
            End If


            'strMsg = ""
            'dsList = (New clsApplicantQualification).GetApplicantAttachments(strCompCode, intCompanyunkid, intApplicantunkid, CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.CURRICULAM_VITAE))
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("OneCurriculamVitaeMandatory")) = True Then
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Curriculum Vitae Attachment.</B> <br/>"
            '    blnValidate = False
            'End If
            'If strMsg.Trim <> "" Then
            '    If blnAddLinks = True Then
            '        strMessage &= "<h5><a href='MiscAttachment.aspx'>Attachment</a> > </h5>" & strMsg
            '    Else
            '        strMessage &= "<h5>Attachments > </h5>" & strMsg
            '    End If
            'End If

            'strMsg = ""
            'dsList = (New clsApplicantQualification).GetApplicantAttachments(strCompCode, intCompanyunkid, intApplicantunkid, CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.COVER_LETTER))
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("OneCoverLetterMandatory")) = True Then
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Cover Letter Attachment.</B> <br/>"
            '    blnValidate = False
            'End If
            'If strMsg.Trim <> "" Then
            '    If blnAddLinks = True Then
            '        strMessage &= "<h5><a href='MiscAttachment.aspx'>Attachment</a> > </h5>" & strMsg
            '    Else
            '        strMessage &= "<h5>Other Attachments > </h5>" & strMsg
            '    End If
            'End If

            'strMsg = ""
            'dsList = (New clsApplicantQualification).GetApplicantAttachments(strCompCode, intCompanyunkid, intApplicantunkid, CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.QUALIFICATIONS))
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("AttachmentQualificationMandatory")) = True Then
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Qualification Attachment.</B> <br/>"
            '    blnValidate = False
            'End If
            'If strMsg.Trim <> "" Then
            '    If blnAddLinks = True Then
            '        strMessage &= "<h5><a href='MiscAttachment.aspx'>Attachment</a> > </h5>" & strMsg
            '    Else
            '        strMessage &= "<h5>Other Attachments > </h5>" & strMsg
            '    End If
            'End If

            'Sohail (13 May 2022) -- Start
            'Enhancement : AC2-324 : ZRA - As an applicant with zero experience, I should be able to apply for jobs that require zero experience regardless of experience years set in config.
            'strMsg = ""
            'dsList = (New clsApplicantExperience).GetApplicantExperience(strCompCode, intCompanyunkid, intApplicantunkid)
            ''If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("OneJobExperienceMandatory")) = True Then
            'If dsList.Tables(0).Rows.Count < CInt(HttpContext.Current.Session("RecruitMandatoryJobExperiences")) Then
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>Experiences. (" & HttpContext.Current.Session("RecruitMandatoryJobExperiences").ToString & ")</B> <br/>"
            '    blnValidate = False
            'ElseIf dsList.Tables(0).Rows.Count <= 0 Then
            '    i = i + 1
            '    strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; Experiences. [Optional] <br/>"
            'End If
            'If strMsg.Trim <> "" Then
            '    If blnAddLinks = True Then
            '        strMessage &= "<h5><a href='ApplicantExperience.aspx'>Experiences</a> > </h5>" & strMsg
            '    Else
            '        strMessage &= "<h5> Experiences > </h5>" & strMsg
            '    End If
            'End If
            'Sohail (13 May 2022) -- End

            strMsg = ""
            dsList = (New clsApplicantReference).GetApplicantReference(strCompCode, intCompanyunkid, intApplicantunkid, clsCommon_Master.enCommonMaster.RELATIONS)
            'If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(HttpContext.Current.Session("OneReferenceMandatory")) = True Then
            If dsList.Tables(0).Rows.Count < CInt(HttpContext.Current.Session("RecruitMandatoryReferences")) Then
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; <B>References. (" & HttpContext.Current.Session("RecruitMandatoryReferences").ToString & ")</B> <br/>"
                blnValidate = False
            ElseIf dsList.Tables(0).Rows.Count <= 0 Then
                i = i + 1
                strMsg &= "&nbsp;&nbsp;&nbsp;&#9679; References. [Optional] <br/>"
            End If
            If strMsg.Trim <> "" Then
                If blnAddLinks = True Then
                    strMessage &= "<h5><a href='ApplicantReference.aspx'>References</a> > </h5>" & strMsg
                Else
                    strMessage &= "<h5> References > </h5>" & strMsg
                End If
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return blnValidate
    End Function

    Public Shared Function IsValidEmail(email As String) As Boolean
        Dim r = New Regex("^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$")

        Return Not String.IsNullOrEmpty(email) AndAlso r.IsMatch(email)
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetApplicantList(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , intApplicantUnkId As Integer _
                                     , intVacancyUnkId As Integer _
                                     , strFirstName As String _
                                     , strSurName As String _
                                     , strEmail As String _
                                     , strPresentMobileNo As String _
                                     , intStatusId As Integer _
                                     , intTitleId As Integer _
                                     , intVacancyMasterTypeId As Integer _
                                     , intTimezoneMinuteDiff As Integer _
                                     , startRowIndex As Integer _
                                     , intPageSize As Integer _
                                     , strSortExpression As String
                                     ) As DataSet
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetApplicants"

            startRowIndex = Convert.ToInt32(startRowIndex / intPageSize) + 1

            If strCompCode Is Nothing Then strCompCode = ""
            If strFirstName Is Nothing Then strFirstName = ""
            If strSurName Is Nothing Then strSurName = ""
            If strEmail Is Nothing Then strEmail = ""
            If strPresentMobileNo Is Nothing Then strPresentMobileNo = ""
            If strSortExpression Is Nothing Then strSortExpression = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()

                    'For performance issue on view applicants screen
                    Using c As New SqlCommand("SET ARITHABORT ON", con)
                        c.ExecuteNonQuery()
                    End Using

                    Using cmd As New SqlCommand(strQ, con)
                        cmd.CommandTimeout = CInt(HttpContext.Current.Session("CommandTimeOut"))

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkId
                        cmd.Parameters.Add(New SqlParameter("@vacancyunkid", SqlDbType.Int)).Value = intVacancyUnkId
                        cmd.Parameters.Add(New SqlParameter("@firstname", SqlDbType.NVarChar)).Value = strFirstName.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@surname", SqlDbType.NVarChar)).Value = strSurName.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@present_mobileno", SqlDbType.NVarChar)).Value = strPresentMobileNo.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@status_id", SqlDbType.Int)).Value = intStatusId
                        cmd.Parameters.Add(New SqlParameter("@title_id", SqlDbType.Int)).Value = intTitleId
                        cmd.Parameters.Add(New SqlParameter("@vacancymaster_id", SqlDbType.Int)).Value = intVacancyMasterTypeId
                        cmd.Parameters.Add(New SqlParameter("@timezone_minute_diff", SqlDbType.Int)).Value = intTimezoneMinuteDiff
                        cmd.Parameters.Add(New SqlParameter("@CurrentPage", SqlDbType.Int)).Value = startRowIndex
                        cmd.Parameters.Add(New SqlParameter("@PageSize", SqlDbType.Int)).Value = intPageSize
                        cmd.Parameters.Add(New SqlParameter("@SortExpression", SqlDbType.NVarChar)).Value = strSortExpression.Trim.Replace("'", "''")

                        da.SelectCommand = cmd
                        da.Fill(ds, "ApplicantList")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, False)>
    Public Shared Function GetApplicantCount(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , intApplicantUnkId As Integer _
                                     , intVacancyUnkId As Integer _
                                     , strFirstName As String _
                                     , strSurName As String _
                                     , strEmail As String _
                                     , strPresentMobileNo As String _
                                     , intStatusId As Integer _
                                     , intTitleId As Integer _
                                     , intVacancyMasterTypeId As Integer _
                                     , intTimezoneMinuteDiff As Integer _
                                     , startRowIndex As Integer _
                                     , intPageSize As Integer _
                                     , strSortExpression As String
                                     ) As Integer
        Dim ds As New DataSet
        Dim intCount As Integer = 0
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetApplicantTotal"

            If strCompCode Is Nothing Then strCompCode = ""
            If strFirstName Is Nothing Then strFirstName = ""
            If strSurName Is Nothing Then strSurName = ""
            If strEmail Is Nothing Then strEmail = ""
            If strPresentMobileNo Is Nothing Then strPresentMobileNo = ""
            If strSortExpression Is Nothing Then strSortExpression = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()

                    'For performance issue on view applicants screen
                    Using c As New SqlCommand("SET ARITHABORT ON", con)
                        c.CommandType = CommandType.Text
                        c.ExecuteNonQuery()
                    End Using

                    Using cmd As New SqlCommand(strQ, con)
                        cmd.CommandTimeout = CInt(HttpContext.Current.Session("CommandTimeOut"))

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkId
                        cmd.Parameters.Add(New SqlParameter("@vacancyunkid", SqlDbType.Int)).Value = intVacancyUnkId
                        cmd.Parameters.Add(New SqlParameter("@firstname", SqlDbType.NVarChar)).Value = strFirstName.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@surname", SqlDbType.NVarChar)).Value = strSurName.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@present_mobileno", SqlDbType.NVarChar)).Value = strPresentMobileNo.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@status_id", SqlDbType.Int)).Value = intStatusId
                        cmd.Parameters.Add(New SqlParameter("@title_id", SqlDbType.Int)).Value = intTitleId
                        cmd.Parameters.Add(New SqlParameter("@vacancymaster_id", SqlDbType.Int)).Value = intVacancyMasterTypeId
                        cmd.Parameters.Add(New SqlParameter("@timezone_minute_diff", SqlDbType.Int)).Value = intTimezoneMinuteDiff

                        Dim intCnt As Object = cmd.ExecuteScalar()

                        intCount = DirectCast(intCnt, Integer)
                        'Dim dr As SqlDataReader = cmd.ExecuteReader()
                        'If dr.HasRows = True Then
                        '    While dr.Read
                        '        intCount = dr.GetInt32(0)
                        '    End While
                        'End If
                        'dr.Close()
                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return intCount
    End Function

    Public Function GetStatus() As DataSet
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetStatus"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        da.SelectCommand = cmd
                        da.Fill(ds, "GetStatus")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    Public Function GetApprovedPending() As DataSet
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetApprovedPending"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        da.SelectCommand = cmd
                        da.Fill(ds, "GetApproved")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    Public Function GetPendingSent() As DataSet
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procGetPendingSent"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        da.SelectCommand = cmd
                        da.Fill(ds, "GetPendingSent")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    Public Function GetPageSize() As DataSet
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procGetPageSize"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        da.SelectCommand = cmd
                        da.Fill(ds, "GetPageSize")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    Public Function ActivateApplicant(intCompanyId As Integer _
                                      , strCompanyCode As String _
                                      , strEmail As String _
                                      , strUserID As String
                                      ) As Boolean
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procActivateUser"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandTimeout = 0

                    Dim profile As UserProfile = UserProfile.GetUserProfile(strEmail)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = CInt(intCompanyId)
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompanyCode
                    cmd.Parameters.Add(New SqlParameter("@userid", SqlDbType.UniqueIdentifier)).Value = New Guid(strUserID)
                    cmd.Parameters.Add(New SqlParameter("@firstname", SqlDbType.NVarChar)).Value = profile.FirstName
                    cmd.Parameters.Add(New SqlParameter("@othername", SqlDbType.NVarChar)).Value = profile.MiddleName
                    cmd.Parameters.Add(New SqlParameter("@surname", SqlDbType.NVarChar)).Value = profile.LastName
                    cmd.Parameters.Add(New SqlParameter("@Email", SqlDbType.NVarChar)).Value = strEmail
                    cmd.Parameters.Add(New SqlParameter("@gender", SqlDbType.Int)).Value = profile.Gender
                    cmd.Parameters.Add(New SqlParameter("@birth_date", SqlDbType.DateTime)).Value = profile.BirthDate
                    cmd.Parameters.Add(New SqlParameter("@present_mobileno", SqlDbType.NVarChar)).Value = profile.MobileNo
                    cmd.Parameters.Add(New SqlParameter("@ret_applicantunkid", SqlDbType.Int)).Value = 0
                    cmd.Parameters("@ret_applicantunkid").Direction = ParameterDirection.Output

                    Dim intApplicantID As Integer = 0
                    intApplicantID = cmd.ExecuteNonQuery

                    If CInt(cmd.Parameters("@ret_applicantunkid").Value) > 0 Then
                        'Session("applicantunkid") = CInt(cmd.Parameters("@ret_applicantunkid").Value)

                        'lblMsg.Text = "Thank you for activation. You can login now!" & vbCrLf & "Click <a href='" & lnk & "'>here</a> to Login."
                    Else
                        Return False
                    End If


                End Using
            End Using

            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function

    Public Function GetDocType() As DataSet
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procGetDocType"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        da.SelectCommand = cmd
                        da.Fill(ds, "GetDocType")

                    End Using
                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally
        End Try
        Return ds
    End Function

    Public Function GetYesNo() As DataSet
        Dim ds As New DataSet
        Try

            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()


            Dim strQ As String = "procGetYesNo"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        da.SelectCommand = cmd
                        da.Fill(ds, "YesNo")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return ds
    End Function

    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-893
    Public Function AddNidaRequestLogs(ByVal strApi_url As String,
                                             strPayload As String,
                                             strResponse As String,
                                             strNin As String,
                                             strMobile As String,
                                             strEmail As String
                                        ) As Boolean
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procAddNidaLog"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@api_url", SqlDbType.NVarChar)).Value = strApi_url
                        cmd.Parameters.Add(New SqlParameter("@payload", SqlDbType.NVarChar)).Value = strPayload
                        cmd.Parameters.Add(New SqlParameter("@response", SqlDbType.NVarChar)).Value = strResponse
                        cmd.Parameters.Add(New SqlParameter("@nin", SqlDbType.NVarChar)).Value = strNin
                        cmd.Parameters.Add(New SqlParameter("@mobile", SqlDbType.NVarChar)).Value = strMobile
                        cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail

                        cmd.ExecuteNonQuery()

                    End Using
                End Using
            End Using
            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function

    Public Shared Function GetOnlyBase64String(ByVal _input As String) As String
        Dim _output As String = ""
        Try
            Dim pattern = "^data:image\/[a-z]+;base64,"
            Dim regex As Regex = New Regex(pattern)
            _output = regex.Replace(_input, "", 1)
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return _output
    End Function

    Public Function IsNidaNumberExists(ByVal _nin As String, _companycode As String, _companyunkid As Integer) As Boolean
        Dim blnIsExist As Boolean = False
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procIsExistApplicantNidaNum"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = _companyunkid
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = _companycode
                    cmd.Parameters.Add(New SqlParameter("@nin", SqlDbType.NVarChar)).Value = _nin.Trim()

                    cmd.Parameters.Add(New SqlParameter("@result", SqlDbType.Bit)).Value = False
                    cmd.Parameters("@result").Direction = ParameterDirection.Output

                    cmd.ExecuteNonQuery()

                    blnIsExist = CBool(cmd.Parameters("@result").Value)

                End Using
            End Using

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return blnIsExist
    End Function
    'S.SANDEEP |04-MAY-2023| -- END

#End Region

End Class

#Region " Personal Info "

Public Class clsPersonalInfo

    Dim cache As ObjectCache = MemoryCache.Default

    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-833
    Public Function GetResidenceType() As DataSet
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procGetResidenceType"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        da.SelectCommand = cmd
                        da.Fill(ds, "ResidenceType")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return ds
    End Function
    'S.SANDEEP |04-MAY-2023| -- END

    Public Function GetGender() As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetGender"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        da.SelectCommand = cmd
                        da.Fill(ds, "Gender")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END        
        End Try
        Return ds
    End Function

    Public Function GetPersonalInfo(ByVal strCompCode As String _
                                    , intComUnkID As Integer _
                                    , intApplicantUnkid As Integer _
                                    , Optional blnRefreshCache As Boolean = False
                                    ) As DataSet
        Dim ds As New DataSet
        Try
            Dim strCacheKey As String = "AppPersonal_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString
            Dim dsCache As DataSet = TryCast(Cache(strCacheKey), DataSet)
            If dsCache Is Nothing OrElse blnRefreshCache = True Then

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
                Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
                'Sohail (30 Nov 2017) -- End

                Dim strQ As String = "procGetPersonalInfo"

                Using con As New SqlConnection(strConn)

                    con.Open()

                    Using da As New SqlDataAdapter()
                        Using cmd As New SqlCommand(strQ, con)

                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                            cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                            cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid

                            da.SelectCommand = cmd
                            da.Fill(ds, "PersonalInfo")

                        End Using
                    End Using
                End Using

                cache.Set(strCacheKey, ds, Now.AddMinutes(2))
            Else
                ds = dsCache
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function SavePersonalInfo(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , intApplicantUnkid As Integer _
                                     , strApplicantCode As String _
                                     , intTitleUnkid As Integer _
                                     , strFirstName As String _
                                     , strSurname As String _
                                     , strOtherName As String _
                                     , intGenderUnkid As Integer _
                                     , strEmail As String _
                                     , strEmployeecode As String _
                                     , dtBirthDate As Date _
                                     , strPresentMobileNo As String _
                                     , strPresent_AlternetNo As String _
                                     , strPerm_AlternetNo As String _
                                     , intNationality As Integer _
                                     , strNin As String _
                                     , strTin As String _
                                     , strNinMobile As String _
                                     , strVillage As String _
                                     , strPhoneNum1 As String _
                                     , intResidency As Integer _
                                     , strIndexNo As String _
                                     , objAddAppAttach As List(Of dtoAddAppAttach)
                                     ) As Boolean

        'Pinkal (30-Sep-2023) --'(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".[ objAddAppAttach As List(Of dtoAddAppAttach)]

        'S.SANDEEP |04-MAY-2023|  -- START 'ISSUE/ENHANCEMENT : A1X-833 {txtNin,txtTin,txtNidaMobile,txtvillage,txtphoneNumber} -- END
        Dim ds As New DataSet

        'Pinkal (30-Sep-2023) -- Start
        '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
        Dim trn As SqlTransaction = Nothing
        'Pinkal (30-Sep-2023) -- End

        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
            Dim strQ As String = "procSavePersonalInfo"
            Dim strQ1 As String = "procAddApplicantAttachment"
            'Pinkal (30-Sep-2023) -- End


            Using con As New SqlConnection(strConn)

                con.Open()

                'Pinkal (30-Sep-2023) -- Start
                '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
                trn = con.BeginTransaction
                'Pinkal (30-Sep-2023) -- End

                Using cmd As New SqlCommand(strQ, con)

                    'Pinkal (30-Sep-2023) -- Start
                    '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
                    cmd.Transaction = trn
                    'Pinkal (30-Sep-2023) -- End

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@applicant_code", SqlDbType.NVarChar)).Value = strApplicantCode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@titleunkid", SqlDbType.Int)).Value = intTitleUnkid
                    cmd.Parameters.Add(New SqlParameter("@firstname", SqlDbType.NVarChar)).Value = strFirstName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@surname", SqlDbType.NVarChar)).Value = strSurname.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@othername", SqlDbType.NVarChar)).Value = strOtherName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@gender", SqlDbType.Int)).Value = intGenderUnkid
                    cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail
                    cmd.Parameters.Add(New SqlParameter("@employeecode", SqlDbType.NVarChar)).Value = strEmployeecode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@birth_date", SqlDbType.DateTime)).Value = dtBirthDate
                    cmd.Parameters.Add(New SqlParameter("@present_mobileno", SqlDbType.NVarChar)).Value = strPresentMobileNo.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@present_alternateno", SqlDbType.NVarChar)).Value = strPresent_AlternetNo.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@perm_alternateno", SqlDbType.NVarChar)).Value = strPerm_AlternetNo.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@nationality", SqlDbType.Int)).Value = intNationality
                    'S.SANDEEP |04-MAY-2023| -- START
                    'ISSUE/ENHANCEMENT : A1X-833
                    cmd.Parameters.Add(New SqlParameter("@nin", SqlDbType.NVarChar)).Value = strNin.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@tin", SqlDbType.NVarChar)).Value = strTin.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@ninMobile", SqlDbType.NVarChar)).Value = strNinMobile.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@village", SqlDbType.NVarChar)).Value = strVillage.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@phonenum", SqlDbType.NVarChar)).Value = strPhoneNum1.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@residency", SqlDbType.Int)).Value = intResidency
                    cmd.Parameters.Add(New SqlParameter("@indexno", SqlDbType.NVarChar)).Value = strIndexNo.Trim.Replace("'", "''")
                    'S.SANDEEP |04-MAY-2023| -- END
                    cmd.ExecuteNonQuery()

                End Using


                'Pinkal (30-Sep-2023) -- Start
                '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
                If objAddAppAttach IsNot Nothing AndAlso objAddAppAttach.Count > 0 Then

                    For Each dto As dtoAddAppAttach In objAddAppAttach

                        Using cmd As New SqlCommand(strQ1, con)

                            With cmd
                                .Parameters.Clear()
                                .Transaction = trn
                                .CommandType = CommandType.StoredProcedure
                                .Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = dto.ComUnkID
                                .Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = dto.CompCode
                                .Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = dto.ApplicantUnkid
                                .Parameters.Add(New SqlParameter("@documentunkid", SqlDbType.Int)).Value = dto.DocumentUnkid
                                .Parameters.Add(New SqlParameter("@modulerefid", SqlDbType.Int)).Value = dto.ModulerefId
                                .Parameters.Add(New SqlParameter("@attachrefid", SqlDbType.Int)).Value = dto.AttachrefId
                                .Parameters.Add(New SqlParameter("@filepath", SqlDbType.NVarChar, 2000)).Value = dto.Filepath.Trim.Replace("'", "''")
                                .Parameters.Add(New SqlParameter("@filename", SqlDbType.NVarChar)).Value = dto.Filename.Trim.Replace("'", "''")
                                .Parameters.Add(New SqlParameter("@fileuniquename", SqlDbType.NVarChar)).Value = dto.Fileuniquename.Trim.Replace("'", "''")
                                .Parameters.Add(New SqlParameter("@attached_date", SqlDbType.Date)).Value = dto.Attached_Date
                                .Parameters.Add(New SqlParameter("@file_size", SqlDbType.Int)).Value = dto.File_size
                                .Parameters.Add(New SqlParameter("@appvacancytranunkid", SqlDbType.Int)).Value = 0
                                .Parameters.Add(New SqlParameter("@jobhistorytranunkid", SqlDbType.Int)).Value = 0
                                'Pinkal (30-Sep-2023) -- Start
                                ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
                                .Parameters.Add(New SqlParameter("@applicantqualiftranunkid", SqlDbType.Int)).Value = 0
                                'Pinkal (30-Sep-2023) -- End

                                .ExecuteNonQuery()

                            End With


                        End Using

                    Next

                End If   ' If objAddAppAttach IsNot Nothing AndAlso objAddAppAttach.Count > 0 Then

                trn.Commit()

                'Pinkal (30-Sep-2023) -- End

            End Using

            Return True
        Catch ex As Exception
            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
            trn.Rollback()
            'Pinkal (30-Sep-2023) -- End
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function
End Class

#End Region

#Region " Contact Details "
Public Class clsContactDetails

    Dim cache As ObjectCache = MemoryCache.Default

    Public Function GetCountry() As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetCountry"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure

                        da.SelectCommand = cmd
                        da.Fill(ds, "Country")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function GetState(ByVal strCompCode As String _
                                    , intComUnkID As Integer _
                                    , intCountryUnkid As Integer
                                    ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetState"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@countryunkid", SqlDbType.Int)).Value = intCountryUnkid

                        da.SelectCommand = cmd
                        da.Fill(ds, "State")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function GetCity(ByVal strCompCode As String _
                                    , intComUnkID As Integer _
                                    , intStateUnkid As Integer
                                    ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetCity"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@stateunkid", SqlDbType.Int)).Value = intStateUnkid

                        da.SelectCommand = cmd
                        da.Fill(ds, "City")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function GetZipCode(ByVal strCompCode As String _
                                    , intComUnkID As Integer _
                                    , intCityUnkid As Integer
                                    ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetZipCode"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@Cityunkid", SqlDbType.Int)).Value = intCityUnkid

                        da.SelectCommand = cmd
                        da.Fill(ds, "ZipCode")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function GetContactDetail(ByVal strCompCode As String _
                                    , intComUnkID As Integer _
                                    , intApplicantUnkid As Integer _
                                    , Optional blnRefreshCache As Boolean = False
                                    ) As DataSet
        Dim ds As New DataSet
        Try
            Dim strCacheKey As String = "AppContact_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString
            Dim dsCache As DataSet = TryCast(cache(strCacheKey), DataSet)
            If dsCache Is Nothing OrElse blnRefreshCache = True Then

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
                Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
                'Sohail (30 Nov 2017) -- End

                Dim strQ As String = "procGetContactDetail"

                Using con As New SqlConnection(strConn)

                    con.Open()

                    Using da As New SqlDataAdapter()
                        Using cmd As New SqlCommand(strQ, con)

                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                            cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                            cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid

                            da.SelectCommand = cmd
                            da.Fill(ds, "ContactDetail")

                        End Using
                    End Using
                End Using

                cache.Set(strCacheKey, ds, Now.AddMinutes(2))
            Else
                ds = dsCache
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function SaveContactDetail(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , intApplicantUnkid As Integer _
                                     , strPresent_Address1 As String _
                                     , strPresent_Address2 As String _
                                     , strPresent_PlotNo As String _
                                     , strPresent_Estate As String _
                                     , strPresent_Road As String _
                                     , strPresent_Province As String _
                                     , intPresent_Country As Integer _
                                     , intPresent_State As Integer _
                                     , intPresent_PostTown As Integer _
                                     , intPresent_Zipcode As Integer _
                                     , strPresent_Tel_No As String _
                                     , strPresent_Fax As String _
                                     , strPerm_Address1 As String _
                                     , strPerm_Address2 As String _
                                     , strPerm_PlotNo As String _
                                     , strPerm_Estate As String _
                                     , strPerm_Road As String _
                                     , strPerm_Province As String _
                                     , intPerm_Country As Integer _
                                     , intPerm_State As Integer _
                                     , intPerm_PostTown As Integer _
                                     , intPerm_Zipcode As Integer _
                                     , strPerm_Tel_No As String _
                                     , strPerm_Fax As String
                                     ) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procSaveContactDetail"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@present_address1", SqlDbType.NVarChar)).Value = strPresent_Address1.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@present_address2", SqlDbType.NVarChar)).Value = strPresent_Address2.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@present_plotno", SqlDbType.NVarChar)).Value = strPresent_PlotNo.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@present_estate", SqlDbType.NVarChar)).Value = strPresent_Estate.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@present_road", SqlDbType.NVarChar)).Value = strPresent_Road.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@present_province", SqlDbType.NVarChar)).Value = strPresent_Province.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@present_countryunkid", SqlDbType.Int)).Value = intPresent_Country
                    cmd.Parameters.Add(New SqlParameter("@present_stateunkid", SqlDbType.Int)).Value = intPresent_State
                    cmd.Parameters.Add(New SqlParameter("@present_post_townunkid", SqlDbType.Int)).Value = intPresent_PostTown
                    cmd.Parameters.Add(New SqlParameter("@present_zipcode", SqlDbType.Int)).Value = intPresent_Zipcode
                    cmd.Parameters.Add(New SqlParameter("@present_tel_no", SqlDbType.NVarChar)).Value = strPresent_Tel_No.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@present_fax", SqlDbType.NVarChar)).Value = strPresent_Fax.Trim.Replace("'", "''")

                    cmd.Parameters.Add(New SqlParameter("@perm_address1", SqlDbType.NVarChar)).Value = strPerm_Address1.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@perm_address2", SqlDbType.NVarChar)).Value = strPerm_Address2.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@perm_plotno", SqlDbType.NVarChar)).Value = strPerm_PlotNo
                    cmd.Parameters.Add(New SqlParameter("@perm_estate", SqlDbType.NVarChar)).Value = strPerm_Estate
                    cmd.Parameters.Add(New SqlParameter("@perm_road", SqlDbType.NVarChar)).Value = strPerm_Road
                    cmd.Parameters.Add(New SqlParameter("@perm_province", SqlDbType.NVarChar)).Value = strPerm_Province
                    cmd.Parameters.Add(New SqlParameter("@perm_countryunkid", SqlDbType.Int)).Value = intPerm_Country
                    cmd.Parameters.Add(New SqlParameter("@perm_stateunkid", SqlDbType.Int)).Value = intPerm_State
                    cmd.Parameters.Add(New SqlParameter("@perm_post_townunkid", SqlDbType.Int)).Value = intPerm_PostTown
                    cmd.Parameters.Add(New SqlParameter("@perm_zipcode", SqlDbType.Int)).Value = intPerm_Zipcode
                    cmd.Parameters.Add(New SqlParameter("@perm_tel_no", SqlDbType.NVarChar)).Value = strPerm_Tel_No
                    cmd.Parameters.Add(New SqlParameter("@perm_fax", SqlDbType.NVarChar)).Value = strPerm_Fax


                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

End Class
#End Region

#Region " Other Info "
Public Class clsOtherInfo

    Dim cache As ObjectCache = MemoryCache.Default

    Public Function GetOtherInfo(ByVal strCompCode As String _
                                    , intComUnkID As Integer _
                                    , intApplicantUnkid As Integer _
                                    , intLanguage_id As Integer _
                                    , intMaritalStatus_id As Integer _
                                    , Optional blnRefreshCache As Boolean = False
                                    ) As DataSet
        Dim ds As New DataSet
        Try
            Dim strCacheKey As String = "AppOtherInfo_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_" & intLanguage_id.ToString & "_" & intMaritalStatus_id.ToString
            Dim dsCache As DataSet = TryCast(Cache(strCacheKey), DataSet)
            If dsCache Is Nothing OrElse blnRefreshCache = True Then

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
                Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetOtherInfo"

                Using con As New SqlConnection(strConn)

                    con.Open()

                    Using da As New SqlDataAdapter()
                        Using cmd As New SqlCommand(strQ, con)

                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                            cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                            cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                            cmd.Parameters.Add(New SqlParameter("@language_id", SqlDbType.Int)).Value = intLanguage_id
                            cmd.Parameters.Add(New SqlParameter("@maritalstatus_id", SqlDbType.Int)).Value = intMaritalStatus_id

                            da.SelectCommand = cmd
                            da.Fill(ds, "OtherInfo")

                        End Using
                    End Using
                End Using

                Cache.Set(strCacheKey, ds, Now.AddMinutes(2))
            Else
                ds = dsCache
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function SaveOtherInfo(ByVal strCompCode As String _
                                     , intComUnkID As Integer _
                                     , intApplicantUnkid As Integer _
                                     , intLanguage1unkid As Integer _
                                     , intLanguage2unkid As Integer _
                                     , intLanguage3unkid As Integer _
                                     , intLanguage4unkid As Integer _
                                     , intMaritalStatusUnkid As Integer _
                                     , dtAnniversaryDate As Date _
                                     , strMemberships As String _
                                     , strAchievements As String _
                                     , strJournalsresearchpapers As String _
                                     , strMother_tongue As String _
                                     , blnIsimpaired As Boolean _
                                     , strImpairment As String _
                                     , decCurrent_Salary As Decimal _
                                     , decExpected_Salary As Decimal _
                                     , strExpected_benefits As String _
                                     , blnWilling_to_relocate As Boolean _
                                     , blnWilling_to_travel As Boolean _
                                     , intNotice_period_days As Integer _
                                     , intCurrent_salary_currency_id As Integer _
                                     , intExpected_salary_currency_id As Integer
                                     ) As Boolean
        'Hemant (05 july 2018) -- [intCurrent_salary_currency_id, intExpected_salary_currency_id  ]

        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procSaveOtherInfo"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@language1unkid", SqlDbType.Int)).Value = intLanguage1unkid
                    cmd.Parameters.Add(New SqlParameter("@language2unkid", SqlDbType.Int)).Value = intLanguage2unkid
                    cmd.Parameters.Add(New SqlParameter("@language3unkid", SqlDbType.Int)).Value = intLanguage3unkid
                    cmd.Parameters.Add(New SqlParameter("@language4unkid", SqlDbType.Int)).Value = intLanguage4unkid
                    'cmd.Parameters.Add(New SqlParameter("@nationality", SqlDbType.Int)).Value = intNationality
                    cmd.Parameters.Add(New SqlParameter("@mother_tongue", SqlDbType.NVarChar)).Value = strMother_tongue.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@isimpaired", SqlDbType.Bit)).Value = blnIsimpaired
                    cmd.Parameters.Add(New SqlParameter("@impairment", SqlDbType.NVarChar)).Value = strImpairment.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@current_salary", SqlDbType.Decimal)).Value = decCurrent_Salary
                    cmd.Parameters.Add(New SqlParameter("@expected_salary", SqlDbType.Decimal)).Value = decExpected_Salary
                    cmd.Parameters.Add(New SqlParameter("@expected_benefits", SqlDbType.NVarChar)).Value = strExpected_benefits.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@willing_to_relocate", SqlDbType.Bit)).Value = blnWilling_to_relocate
                    cmd.Parameters.Add(New SqlParameter("@willing_to_travel", SqlDbType.Bit)).Value = blnWilling_to_travel
                    'cmd.Parameters.Add(New SqlParameter("@willing_to_relocate", SqlDbType.SmallInt)).Value = blnWilling_to_relocate
                    'cmd.Parameters.Add(New SqlParameter("@willing_to_travel", SqlDbType.SmallInt)).Value = blnWilling_to_travel
                    cmd.Parameters.Add(New SqlParameter("@notice_period_days", SqlDbType.Int)).Value = intNotice_period_days
                    cmd.Parameters.Add(New SqlParameter("@marital_statusunkid", SqlDbType.Int)).Value = intMaritalStatusUnkid
                    cmd.Parameters.Add(New SqlParameter("@anniversary_date", SqlDbType.DateTime)).Value = dtAnniversaryDate
                    cmd.Parameters.Add(New SqlParameter("@memberships", SqlDbType.NVarChar)).Value = strMemberships.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@achievements", SqlDbType.NVarChar)).Value = strAchievements.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@journalsresearchpapers", SqlDbType.NVarChar)).Value = strJournalsresearchpapers.Trim.Replace("'", "''")
                    'Hemant (05 july 2018) -- -- Start
                    'Enhancement – RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)
                    cmd.Parameters.Add(New SqlParameter("@current_salary_currency_id", SqlDbType.Int)).Value = intCurrent_salary_currency_id
                    cmd.Parameters.Add(New SqlParameter("@expected_salary_currency_id", SqlDbType.Int)).Value = intExpected_salary_currency_id
                    'Hemant (05 july 2018) -- -- End
                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function


End Class
#End Region

#Region " Nida Classes "

'Public Class clsNidaAns
'    Private _nin As String = ""
'    Public Property nin() As String
'        Get
'            Return _nin
'        End Get
'        Set(ByVal value As String)
'            _nin = value
'        End Set
'    End Property

'    Private _mobile As String = ""
'    Public Property mobile() As String
'        Get
'            Return _mobile
'        End Get
'        Set(ByVal value As String)
'            _mobile = value
'        End Set
'    End Property

'    Private _ans As List(Of ans)
'    Public Property ans As List(Of ans)
'        Get
'            Return _ans
'        End Get
'        Set(ByVal value As List(Of ans))
'            _ans = value
'        End Set
'    End Property
'End Class

'Public Class ans
'    Private _qnID As Integer = 0
'    Public Property qnID() As Integer
'        Get
'            Return _qnID
'        End Get
'        Set(ByVal value As Integer)
'            _qnID = value
'        End Set
'    End Property

'    Private _answer As String = ""
'    Public Property answer() As String
'        Get
'            Return _answer
'        End Get
'        Set(ByVal value As String)
'            _answer = value
'        End Set
'    End Property

'End Class

Public Class An
    <JsonProperty("qnID", NullValueHandling:=NullValueHandling.Ignore)>
    Public qnID As Integer

    <JsonProperty("answer", NullValueHandling:=NullValueHandling.Ignore)>
    Public answer As String
End Class

Public Class Nidans
    <JsonProperty("nin", NullValueHandling:=NullValueHandling.Ignore)>
    Public nin As String

    <JsonProperty("mobile", NullValueHandling:=NullValueHandling.Ignore)>
    Public mobile As String

    <JsonProperty("ans", NullValueHandling:=NullValueHandling.Ignore)>
    Public ans As List(Of An)
End Class

Public Class nidaotp
    Private _nin As String = ""
    Private _otp As Integer = 0
    Public Property nin() As String
        Get
            Return _nin
        End Get
        Set(ByVal value As String)
            _nin = value
        End Set
    End Property

    Public Property otp() As Integer
        Get
            Return _otp
        End Get
        Set(ByVal value As Integer)
            _otp = value
        End Set
    End Property
End Class

Public Class questions
    <JsonProperty("qnId", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property qnId() As Integer

    <JsonProperty("qnDesc", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property qnDesc() As String
End Class

Public Class nidanum
    <JsonProperty("nin", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property nin() As String

    <JsonProperty("mobile", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property mobile() As String

    <JsonProperty("nextStep", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property nextStep() As String

    <JsonProperty("questions", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property questions() As List(Of questions)
End Class

Public Class ansresponse
    <JsonProperty("nin", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property nin() As String

    <JsonProperty("existingTIN", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property existingTIN() As Object

    <JsonProperty("tin", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property tin() As Object

    <JsonProperty("firstName", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property firstName() As Object

    <JsonProperty("middleName", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property middleName() As Object

    <JsonProperty("surname", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property surname() As Object

    <JsonProperty("email", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property email() As Object

    <JsonProperty("mobileNumber", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property mobileNumber() As String

    <JsonProperty("createdDate", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property createdDate() As Object

    <JsonProperty("matchStatus", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property matchStatus() As Object

    <JsonProperty("inv_result", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property inv_result() As Object

    <JsonProperty("tinGenerated", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property tinGenerated() As Integer

    <JsonProperty("applicationId", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property applicationId() As Object

    <JsonProperty("score", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property score() As Object

    <JsonProperty("phoneNumber1", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property phoneNumber1() As Object

    <JsonProperty("phoneNumber2", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property phoneNumber2() As Object

    <JsonProperty("dateOfBirth", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property dateOfBirth() As Object

    <JsonProperty("tinApplicationDate", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property tinApplicationDate() As Object

    <JsonProperty("village", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property village() As Object

    <JsonProperty("gender", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property gender() As Object

    <JsonProperty("nationality", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property nationality() As Object

    <JsonProperty("signature", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property signature() As Object

    <JsonProperty("picture", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property picture() As Object

    <JsonProperty("applicantPhoto", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property applicantPhoto() As Object

    <JsonProperty("applicantSignature", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property applicantSignature() As Object

    <JsonProperty("fingerImage1", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property fingerImage1() As Object

    <JsonProperty("fingerLabel1", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property fingerLabel1() As Object

    <JsonProperty("fingerImage2", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property fingerImage2() As Object

    <JsonProperty("fingerLabel2", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property fingerLabel2() As Object

    <JsonProperty("wardPostCode", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property wardPostCode() As Object

    <JsonProperty("nextStep", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property nextStep() As String

    <JsonProperty("isCorrect", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property isCorrect() As Integer

    <JsonProperty("questions", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property questions() As List(Of questions)
End Class

Public Class _response
    <JsonProperty("nin", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property nin() As String

    <JsonProperty("existingTIN", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property existingTIN() As Object

    <JsonProperty("tin", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property tin() As Object

    <JsonProperty("firstName", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property firstName() As String

    <JsonProperty("middleName", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property middleName() As String

    <JsonProperty("surname", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property surname() As String

    <JsonProperty("email", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property email() As Object

    <JsonProperty("mobileNumber", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property mobileNumber() As String

    <JsonProperty("createdDate", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property createdDate() As Object

    <JsonProperty("matchStatus", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property matchStatus() As Object

    <JsonProperty("inv_result", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property inv_result() As Object

    <JsonProperty("tinGenerated", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property tinGenerated() As Integer?

    <JsonProperty("applicationId", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property applicationId() As Object

    <JsonProperty("score", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property score() As Object

    <JsonProperty("phoneNumber1", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property phoneNumber1() As Object

    <JsonProperty("phoneNumber2", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property phoneNumber2() As Object

    <JsonProperty("dateOfBirth", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property dateOfBirth() As String

    <JsonProperty("tinApplicationDate", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property tinApplicationDate() As Object

    <JsonProperty("village", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property village() As Object

    <JsonProperty("gender", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property gender() As String

    <JsonProperty("nationality", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property nationality() As String

    <JsonProperty("signature", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property signature() As Object

    <JsonProperty("picture", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property picture() As Object

    <JsonProperty("applicantPhoto", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property applicantPhoto() As String

    <JsonProperty("applicantSignature", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property applicantSignature() As String

    <JsonProperty("fingerImage1", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property fingerImage1() As Object

    <JsonProperty("fingerLabel1", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property fingerLabel1() As Object

    <JsonProperty("fingerImage2", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property fingerImage2() As Object

    <JsonProperty("fingerLabel2", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property fingerLabel2() As Object

    <JsonProperty("wardPostCode", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property wardPostCode() As Object

    <JsonProperty("nextStep", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property nextStep() As Object

    <JsonProperty("isCorrect", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property isCorrect() As Integer?

    <JsonProperty("questions", NullValueHandling:=NullValueHandling.Ignore)>
    Public Property questions() As Object
End Class

#End Region