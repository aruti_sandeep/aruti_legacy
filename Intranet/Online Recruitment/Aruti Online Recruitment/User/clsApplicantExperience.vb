﻿Option Strict On
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Runtime.Caching

Public Class clsApplicantExperience

    Dim cache As ObjectCache = MemoryCache.Default

#Region " Method Functions "

    Public Function GetApplicantExperience(ByVal strCompCode As String _
                                               , intComUnkID As Integer _
                                               , intApplicantUnkid As Integer _
                                               , Optional blnRefreshCache As Boolean = False
                                               ) As DataSet
        Dim ds As New DataSet
        Try
            Dim strCacheKey As String = "AppExp_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_"
            Dim dsCache As DataSet = TryCast(cache(strCacheKey), DataSet)
            If dsCache Is Nothing OrElse blnRefreshCache = True Then

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
                Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
                'Sohail (30 Nov 2017) -- End

                Dim strQ As String = "procGetApplicantJobHistory"

                Using con As New SqlConnection(strConn)

                    con.Open()

                    Using da As New SqlDataAdapter()
                        Using cmd As New SqlCommand(strQ, con)

                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                            cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                            cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid

                            da.SelectCommand = cmd
                            da.Fill(ds, "ApplicantJobHistory")


                        End Using
                    End Using
                End Using

                cache.Set(strCacheKey, ds, Now.AddMinutes(2))
            Else
                ds = dsCache
            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetAppExperience(ByVal strCompCode As String _
                                               , intComUnkID As Integer _
                                               , intApplicantUnkid As Integer _
                                               , Optional dtCurrentDate As Date = Nothing
                                               ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetApplicantJobHistory"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid

                        da.SelectCommand = cmd
                        da.Fill(ds, "ApplicantJobHistory")


                    End Using
                End Using
            End Using
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return ds
    End Function

    Public Function AddApplicantExperience(ByVal strCompCode As String _
                                            , intComUnkID As Integer _
                                            , intApplicantUnkid As Integer _
                                            , strEmployerName As String _
                                            , strCompanyName As String _
                                            , strDesignation As String _
                                            , strResponsibility As String _
                                            , dtJoiningdate As Date _
                                            , dtTerminationdate As Date _
                                            , strOfficephone As String _
                                            , strLeavingreason As String _
                                            , strAchievements As String _
                                            , dtCreated_date As Date _
                                            , bnlIsGovJob As Boolean
                                            ) As Boolean 'S.SANDEEP |04-MAY-2023| -- START {A1X-833 , bnlIsGovJob} -- END
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procAddApplicantJobHistory"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@employername", SqlDbType.NVarChar)).Value = strEmployerName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@companyname", SqlDbType.NVarChar)).Value = strCompanyName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@designation", SqlDbType.NVarChar)).Value = strDesignation.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@responsibility", SqlDbType.NVarChar)).Value = strResponsibility.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@joiningdate", SqlDbType.Date)).Value = dtJoiningdate
                    cmd.Parameters.Add(New SqlParameter("@terminationdate", SqlDbType.Date)).Value = dtTerminationdate
                    cmd.Parameters.Add(New SqlParameter("@officephone", SqlDbType.NVarChar)).Value = strOfficephone.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@leavingreason", SqlDbType.NVarChar)).Value = strLeavingreason.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@achievements", SqlDbType.NVarChar)).Value = strAchievements.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@created_date", SqlDbType.DateTime)).Value = dtCreated_date
                    'S.SANDEEP |04-MAY-2023| -- START
                    'ISSUE/ENHANCEMENT : A1X-833
                    cmd.Parameters.Add(New SqlParameter("@isgovjob", SqlDbType.Bit)).Value = bnlIsGovJob
                    'S.SANDEEP |04-MAY-2023| -- END

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppExp_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-833
    Public Function AddApplicantExperience_Attachment(ByVal strCompCode As String _
                                                        , intComUnkID As Integer _
                                                        , intApplicantUnkid As Integer _
                                                        , strEmployerName As String _
                                                        , strCompanyName As String _
                                                        , strDesignation As String _
                                                        , strResponsibility As String _
                                                        , dtJoiningdate As Date _
                                                        , dtTerminationdate As Date _
                                                        , strOfficephone As String _
                                                        , strLeavingreason As String _
                                                        , strAchievements As String _
                                                        , dtCreated_date As Date _
                                                        , objAddAppAttach As List(Of dtoAddAppAttach) _
                                                        , bnlIsGovJob As Boolean
                                                        ) As Boolean
        Dim ds As New DataSet
        Dim trn As SqlTransaction = Nothing
        Dim intRet_UnkID = 0
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ1 As String = "procAddApplicantJobHistory"
            Dim strQ2 As String = "procAddApplicantAttachment"

            Using con As New SqlConnection(strConn)

                con.Open()

                trn = con.BeginTransaction

                Using cmd As New SqlCommand(strQ1, con)

                    With cmd
                        .Transaction = trn
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        .Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        .Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                        .Parameters.Add(New SqlParameter("@employername", SqlDbType.NVarChar)).Value = strEmployerName.Trim.Replace("'", "''")
                        .Parameters.Add(New SqlParameter("@companyname", SqlDbType.NVarChar)).Value = strCompanyName.Trim.Replace("'", "''")
                        .Parameters.Add(New SqlParameter("@designation", SqlDbType.NVarChar)).Value = strDesignation.Trim.Replace("'", "''")
                        .Parameters.Add(New SqlParameter("@responsibility", SqlDbType.NVarChar)).Value = strResponsibility.Trim.Replace("'", "''")
                        .Parameters.Add(New SqlParameter("@joiningdate", SqlDbType.Date)).Value = dtJoiningdate
                        .Parameters.Add(New SqlParameter("@terminationdate", SqlDbType.Date)).Value = dtTerminationdate
                        .Parameters.Add(New SqlParameter("@officephone", SqlDbType.NVarChar)).Value = strOfficephone.Trim.Replace("'", "''")
                        .Parameters.Add(New SqlParameter("@leavingreason", SqlDbType.NVarChar)).Value = strLeavingreason.Trim.Replace("'", "''")
                        .Parameters.Add(New SqlParameter("@achievements", SqlDbType.NVarChar)).Value = strAchievements.Trim.Replace("'", "''")
                        .Parameters.Add(New SqlParameter("@created_date", SqlDbType.DateTime)).Value = dtCreated_date
                        'S.SANDEEP |04-MAY-2023| -- START
                        'ISSUE/ENHANCEMENT : A1X-833
                        .Parameters.Add(New SqlParameter("@isgovjob", SqlDbType.Bit)).Value = bnlIsGovJob
                        'S.SANDEEP |04-MAY-2023| -- END

                        intRet_UnkID = CInt(.ExecuteScalar())

                    End With

                End Using

                If objAddAppAttach.Count > 0 Then

                    For Each dto As dtoAddAppAttach In objAddAppAttach

                        Using cmd As New SqlCommand(strQ2, con)

                            With cmd
                                .Parameters.Clear()
                                .Transaction = trn
                                .CommandType = CommandType.StoredProcedure
                                .Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = dto.ComUnkID
                                .Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = dto.CompCode
                                .Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = dto.ApplicantUnkid
                                .Parameters.Add(New SqlParameter("@documentunkid", SqlDbType.Int)).Value = dto.DocumentUnkid
                                .Parameters.Add(New SqlParameter("@modulerefid", SqlDbType.Int)).Value = dto.ModulerefId
                                .Parameters.Add(New SqlParameter("@attachrefid", SqlDbType.Int)).Value = intRet_UnkID
                                .Parameters.Add(New SqlParameter("@filepath", SqlDbType.NVarChar, 2000)).Value = dto.Filepath.Trim.Replace("'", "''")
                                .Parameters.Add(New SqlParameter("@filename", SqlDbType.NVarChar)).Value = dto.Filename.Trim.Replace("'", "''")
                                .Parameters.Add(New SqlParameter("@fileuniquename", SqlDbType.NVarChar)).Value = dto.Fileuniquename.Trim.Replace("'", "''")
                                .Parameters.Add(New SqlParameter("@attached_date", SqlDbType.Date)).Value = dto.Attached_Date
                                .Parameters.Add(New SqlParameter("@file_size", SqlDbType.Int)).Value = dto.File_size
                                .Parameters.Add(New SqlParameter("@appvacancytranunkid", SqlDbType.Int)).Value = 0
                                .Parameters.Add(New SqlParameter("@jobhistorytranunkid", SqlDbType.Int)).Value = intRet_UnkID
                                'Pinkal (30-Sep-2023) -- Start
                                ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.
                                .Parameters.Add(New SqlParameter("@applicantqualiftranunkid", SqlDbType.Int)).Value = 0
                                'Pinkal (30-Sep-2023) -- End
                                .ExecuteNonQuery()

                            End With

                        End Using

                    Next

                End If

                trn.Commit()

            End Using

            Return True
        Catch ex As Exception
            trn.Rollback()
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function
    'S.SANDEEP |04-MAY-2023| -- END

    <DataObjectMethod(DataObjectMethodType.Update, True)>
    Public Shared Function EditApplicantExperience(ByVal strCompCode As String _
                                            , intComUnkID As Integer _
                                            , intApplicantUnkid As Integer _
                                            , intJobhistorytranunkid As Integer _
                                            , strEmployerName As String _
                                            , strCompanyName As String _
                                            , strDesignation As String _
                                            , strResponsibility As String _
                                            , dtJoiningdate As Date _
                                            , dtTerminationdate As Date _
                                            , strOfficephone As String _
                                            , strLeavingreason As String _
                                            , strAchievements As String _
                                            , jobhistorytranunkid As Integer
                                            ) As Boolean
        Dim ds As New DataSet
        Dim cache As ObjectCache = MemoryCache.Default
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procEditApplicantJobHistory"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@jobhistorytranunkid", SqlDbType.Int)).Value = intJobhistorytranunkid
                    cmd.Parameters.Add(New SqlParameter("@employername", SqlDbType.NVarChar)).Value = strEmployerName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@companyname", SqlDbType.NVarChar)).Value = strCompanyName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@designation", SqlDbType.NVarChar)).Value = strDesignation.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@responsibility", SqlDbType.NVarChar)).Value = strResponsibility.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@joiningdate", SqlDbType.Date)).Value = dtJoiningdate
                    cmd.Parameters.Add(New SqlParameter("@terminationdate", SqlDbType.Date)).Value = dtTerminationdate
                    cmd.Parameters.Add(New SqlParameter("@officephone", SqlDbType.NVarChar)).Value = strOfficephone.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@leavingreason", SqlDbType.NVarChar)).Value = strLeavingreason.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@achievements", SqlDbType.NVarChar)).Value = strAchievements.Trim.Replace("'", "''")

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppExp_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

    <DataObjectMethod(DataObjectMethodType.Update, True)>
    Public Shared Function EditAppExperience(objAppJobHistoryUpdate As dtoAppJobHistoryUpdate _
                                                    , jobhistorytranunkid As Integer
                                                    ) As Boolean
        Dim ds As New DataSet
        Dim cache As ObjectCache = MemoryCache.Default
        Try

            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procEditApplicantJobHistory"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = objAppJobHistoryUpdate.ComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = objAppJobHistoryUpdate.CompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = objAppJobHistoryUpdate.ApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@jobhistorytranunkid", SqlDbType.Int)).Value = objAppJobHistoryUpdate.Jobhistorytranunkid
                    cmd.Parameters.Add(New SqlParameter("@employername", SqlDbType.NVarChar)).Value = objAppJobHistoryUpdate.EmployerName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@companyname", SqlDbType.NVarChar)).Value = objAppJobHistoryUpdate.CompanyName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@designation", SqlDbType.NVarChar)).Value = objAppJobHistoryUpdate.Designation.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@responsibility", SqlDbType.NVarChar)).Value = objAppJobHistoryUpdate.Responsibility.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@joiningdate", SqlDbType.Date)).Value = objAppJobHistoryUpdate.Joiningdate
                    cmd.Parameters.Add(New SqlParameter("@terminationdate", SqlDbType.Date)).Value = objAppJobHistoryUpdate.Terminationdate
                    cmd.Parameters.Add(New SqlParameter("@officephone", SqlDbType.NVarChar)).Value = objAppJobHistoryUpdate.Officephone.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@leavingreason", SqlDbType.NVarChar)).Value = objAppJobHistoryUpdate.Leavingreason.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@achievements", SqlDbType.NVarChar)).Value = objAppJobHistoryUpdate.Achievements.Trim.Replace("'", "''")

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppExp_" & objAppJobHistoryUpdate.CompCode & "_" & objAppJobHistoryUpdate.ComUnkID.ToString & "_" & objAppJobHistoryUpdate.ApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try

    End Function

    <DataObjectMethod(DataObjectMethodType.Delete, True)>
    Public Shared Function DeleteApplicantExperience(ByVal strCompCode As String _
                                                , intComUnkID As Integer _
                                                , intApplicantUnkid As Integer _
                                                , intJobhistorytranunkid As Integer _
                                                , jobhistorytranunkid As Integer
                                                ) As Boolean
        Dim ds As New DataSet
        Dim cache As ObjectCache = MemoryCache.Default
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procDeleteApplicantJobHistory"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@jobhistorytranunkid", SqlDbType.Int)).Value = intJobhistorytranunkid

                    cmd.ExecuteNonQuery()

                End Using
            End Using

            Dim aa = (From p In cache.AsEnumerable() Where (p.Key.StartsWith("AppExp_" & strCompCode & "_" & intComUnkID.ToString & "_" & intApplicantUnkid.ToString & "_") = True) Select (p)).ToList
            For Each a In aa
                If cache(a.Key) IsNot Nothing Then
                    cache.Remove(a.Key)
                End If
            Next

            Return True
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try

    End Function

    Public Function IsExistApplicantExperience(ByVal strCompCode As String _
                                                , intComUnkID As Integer _
                                                , intApplicantUnkid As Integer _
                                                , intJobhistorytranunkid As Integer _
                                                , strCompanyName As String _
                                                , dtJoiningdate As Date _
                                                , dtTerminationdate As Date
                                                  ) As Boolean
        Dim blnIsExist As Boolean = False
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procIsExistApplicantJobHistory"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = intApplicantUnkid
                    cmd.Parameters.Add(New SqlParameter("@jobhistorytranunkid", SqlDbType.Int)).Value = intJobhistorytranunkid
                    cmd.Parameters.Add(New SqlParameter("@companyname", SqlDbType.NVarChar)).Value = strCompanyName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@joiningdate", SqlDbType.Date)).Value = dtJoiningdate
                    cmd.Parameters.Add(New SqlParameter("@terminationdate", SqlDbType.Date)).Value = dtTerminationdate

                    cmd.Parameters.Add(New SqlParameter("@result", SqlDbType.Bit)).Value = False
                    cmd.Parameters("@result").Direction = ParameterDirection.Output

                    cmd.ExecuteNonQuery()

                    blnIsExist = CBool(cmd.Parameters("@result").Value)

                End Using
            End Using

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            'Throw New Exception(ex.Message)
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
        Return blnIsExist
    End Function
#End Region

End Class
