﻿Option Strict On
Imports System.Runtime.Caching

Public Class PersonalInfo
    Inherits Base_Page

    Private objApplicant As New clsApplicant
    Private objPersonalInfo As New clsPersonalInfo

    'Pinkal (30-Sep-2023) -- Start
    '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
    Private mblnFileUploaded As Boolean = False
    'Pinkal (30-Sep-2023) -- End


#Region " Private Function "

    Private Sub FillCombo()
        Dim objContactDetails As New clsContactDetails
        Dim dsCombo As DataSet
        Try
            dsCombo = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.TITLE)
            With DrpTitle
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objPersonalInfo.GetGender()
            With drpGender
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objContactDetails.GetCountry()
            With drpNationality
                .DataValueField = "countryunkid"
                .DataTextField = "Country_name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-833
            dsCombo = objPersonalInfo.GetResidenceType()
            With drpResidency
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            'S.SANDEEP |04-MAY-2023| -- END

            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
            pnlBirthCertificate.Visible = CBool(Session("BirthCertificateAttachment"))
            If pnlBirthCertificate.Visible Then
                Dim dtTable As DataTable
                Dim dsAttachType As DataSet
                dsAttachType = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
                dsCombo = objApplicant.GetDocType()

                dtTable = New DataView(dsCombo.Tables(0), "ID IN (0, " & CInt(enScanAttactRefId.APPLICANT_BIRTHINFO).ToString & ") ", "", DataViewRowState.CurrentRows).ToTable

                With ddlBirthCertificateDocType
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dtTable.Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                With ddlBirthCertificateAttachType
                    .DataValueField = "masterunkid"
                    .DataTextField = "Name"
                    .DataSource = dsAttachType.Tables(0).Copy
                    .DataBind()
                    .SelectedValue = "0"
                End With

                flBirthCertificate.AllowImageFile = CBool(Session("AllowableAttachmentTypeImage"))
                flBirthCertificate.AllowDocumentFile = CBool(Session("AllowableAttachmentTypeDocument"))
                pnlFileBirthCertificate.Visible = False

            End If
            'Pinkal (30-Sep-2023) -- End


        Catch ex As Exception
            Call Global_asax.CatchException(ex, Context)
        Finally
            objContactDetails = Nothing
        End Try
    End Sub

    Private Function IsValidated() As Boolean
        Try

            If txtFirstName.Text.Trim = "" Then
                ShowMessage(lblFirstNameMsg.Text, MessageType.Errorr)
                txtFirstName.Focus()
                Return False
            ElseIf txtSurName.Text.Trim = "" Then
                ShowMessage(lblSurNameMsg.Text, MessageType.Errorr)
                txtSurName.Focus()
                Return False
            ElseIf txtOtherName.Text.Trim = "" AndAlso CBool(Session("MiddleNameMandatory")) = True Then
                ShowMessage(lblOthernameMsg.Text, MessageType.Errorr)
                txtSurName.Focus()
                Return False
            ElseIf CInt(drpGender.SelectedValue) <= 0 AndAlso CBool(Session("GenderMandatory")) = True Then
                ShowMessage(lblGenderMsg.Text, MessageType.Errorr)
                drpGender.Focus()
                Return False
            ElseIf dtBirthdate.GetDate = CDate("01/Jan/1900") AndAlso CBool(Session("BirthDateMandatory")) Then
                ShowMessage(lblBirthDateMsg.Text, MessageType.Errorr)
                dtBirthdate.Focus()
                Return False
            ElseIf dtBirthdate.GetDate >= System.DateTime.Today.Date Then
                ShowMessage(lblBirthDateMsg2.Text, MessageType.Errorr)
                dtBirthdate.Focus()
                Return False
            ElseIf txtMobileNo.Text.Trim = "" Then
                ShowMessage(lblMobileNoMsg.Text, MessageType.Errorr)
                txtMobileNo.Focus()
                Return False
            ElseIf CInt(drpNationality.SelectedValue) <= 0 AndAlso CBool(Session("NationalityMandatory")) = True Then
                ShowMessage(lblNationalityMsg.Text, MessageType.Errorr)
                drpNationality.Focus()
                Return False
            ElseIf txtEmpCode.Visible = True AndAlso txtEmpCode.Text.Trim = "" Then
                ShowMessage(lblEmpCodeMsg.Text, MessageType.Errorr)
                txtEmpCode.Focus()
                Return False
                'S.SANDEEP |04-MAY-2023| -- START
                'ISSUE/ENHANCEMENT : A1X-833
            ElseIf CInt(drpResidency.SelectedValue) <= 0 AndAlso Session("CompCode").ToString().ToLower() = "tra" Then
                ShowMessage(lblResidencyMsg.Text, MessageType.Errorr)
                drpResidency.Focus()
                Return False
            ElseIf txtIndexNo.Text.Trim = "" AndAlso Session("CompCode").ToString().ToLower() = "tra" Then
                ShowMessage(lblIndexNoMsg.Text, MessageType.Errorr)
                txtIndexNo.Focus()
                Return False
                'S.SANDEEP |04-MAY-2023| -- END
            End If


            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then
                If CBool(Session("BirthCertificateAttachment")) Then
                    If CInt(ddlBirthCertificateDocType.SelectedValue) <= 0 Then
                        ShowMessage(lblDocTypeMsg.Text, MessageType.Info)
                        ddlBirthCertificateDocType.Focus()
                        Return False
                    ElseIf CInt(ddlBirthCertificateAttachType.SelectedValue) <= 0 Then
                        ShowMessage(lblAttachTypeMsg.Text, MessageType.Info)
                        ddlBirthCertificateAttachType.Focus()
                        Return False
                    End If
                    If lblFileNameBirthCertificate.Text.Trim = "" Then
                        ShowMessage(lblAttachMsg.Text.Replace("#attachmenttype#", "Birth Certificate"), MessageType.Errorr)
                        Return False
                    End If
                End If
            End If
            'Pinkal (30-Sep-2023) -- End 

            Return True

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
            Return False
        End Try
    End Function

    Private Sub GetValue()
        Dim dsList As DataSet
        Try
            dsList = objPersonalInfo.GetPersonalInfo(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))

            If dsList.Tables(0).Rows.Count > 0 Then
                DrpTitle.SelectedValue = dsList.Tables(0).Rows(0).Item("titleunkid").ToString
                txtFirstName.Text = dsList.Tables(0).Rows(0).Item("firstname").ToString
                txtSurName.Text = dsList.Tables(0).Rows(0).Item("surname").ToString
                txtOtherName.Text = dsList.Tables(0).Rows(0).Item("othername").ToString
                drpGender.SelectedValue = dsList.Tables(0).Rows(0).Item("gender").ToString
                txtEmpCode.Text = dsList.Tables(0).Rows(0).Item("employeecode").ToString
                If IsDBNull(dsList.Tables(0).Rows(0).Item("birth_date")) = False Then
                    dtBirthdate.SetDate = CDate(dsList.Tables(0).Rows(0).Item("birth_date").ToString)
                End If

                txtMobileNo.Text = dsList.Tables(0).Rows(0).Item("present_mobileno").ToString
                txtAlterNativeno.Text = dsList.Tables(0).Rows(0).Item("present_alternateno").ToString
                drpNationality.SelectedValue = dsList.Tables(0).Rows(0).Item("nationality").ToString

                'Sohail (31 May 2018) -- Start
                'TANAPA Enhancement - Ref #  : Show Online Recruitment Internal Vacancy link in ESS in 72.1.
                If Session("Employeecode").ToString.Trim.Length > 0 Then
                    txtEmpCode.Text = Session("Employeecode").ToString
                End If
                'Sohail (31 May 2018) -- End

                'S.SANDEEP |04-MAY-2023| -- START
                'ISSUE/ENHANCEMENT : A1X-833
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then
                    drpResidency.SelectedValue = dsList.Tables(0).Rows(0).Item("residency").ToString()
                    txtIndexNo.Text = dsList.Tables(0).Rows(0).Item("indexno").ToString()
                    txtNin.Text = dsList.Tables(0).Rows(0).Item("nin").ToString()
                    txtTin.Text = dsList.Tables(0).Rows(0).Item("tin").ToString()
                    txtNidaMobile.Text = dsList.Tables(0).Rows(0).Item("ninmobile").ToString()
                    txtvillage.Text = dsList.Tables(0).Rows(0).Item("village").ToString()
                    txtphoneNumber.Text = dsList.Tables(0).Rows(0).Item("phonenum").ToString()
                    If IsDBNull(dsList.Tables(0).Rows(0).Item("applicant_photo")) = False Then
                        imgApplPhoto.ImageUrl = Convert.ToString("data:image/jpg;base64,") & Convert.ToBase64String(CType(dsList.Tables(0).Rows(0).Item("applicant_photo"), Byte()))
                    Else
                        imgApplPhoto.Visible = False
                    End If

                    'Pinkal (30-Sep-2023) -- Start
                    '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".

                    If CBool(Session("BirthCertificateAttachment")) Then

                        Dim ds As DataSet = (New clsApplicantQualification).GetApplicantAttachments(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(enImg_Email_RefId.Applicant_Module), CInt(enScanAttactRefId.APPLICANT_BIRTHINFO), True, 0)

                        If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                            lblFileNameBirthCertificate.Text = ds.Tables(0).Rows(0)("filename").ToString()
                            lblFileExtBirthCertificate.Text = ds.Tables(0).Rows(0)("filename").ToString().Split(CChar("."))(1).ToUpper()
                            lblDocSizeBirthCertificate.Text = Format((CDec(ds.Tables(0).Rows(0)("file_size")) / 1024), "0.00") & " MB."
                            hfDocSizeBirthCertificate.Value = CDec(ds.Tables(0).Rows(0)("file_size")).ToString()
                            hfFileNameBirthCertificate.Value = lblFileNameBirthCertificate.Text
                            pnlFileBirthCertificate.Visible = True
                            hfBirthCertificateAttachId.Value = ds.Tables(0).Rows(0)("attachfiletranunkid").ToString()
                        Else
                            pnlFileBirthCertificate.Visible = False
                            hfBirthCertificateAttachId.Value = ""
                        End If  'If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then

                    End If  '  If CBool(Session("BirthCertificateAttachment")) Then

                    If hfBirthCertificateAttachId.Value Is Nothing OrElse hfBirthCertificateAttachId.Value = "0" Then hfBirthCertificateAttachId.Value = ""

                    'Pinkal (30-Sep-2023) -- End

                End If

                'S.SANDEEP |04-MAY-2023| -- END

            End If
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub


#End Region

#Region " Page Events "
    Private Sub PersonalInfo_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")

            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
            Me.ViewState("IsFileUpload") = mblnFileUploaded
            'Pinkal (30-Sep-2023) -- End

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Call FillCombo()

                If Session("Vacancy").ToString = "Int" Then
                    lblEmpCode.Visible = True
                    txtEmpCode.Visible = True
                    'ReqValidEmpCode.ValidationGroup = "PersonalInfo"
                Else
                    lblEmpCode.Visible = False
                    txtEmpCode.Visible = False
                    'ReqValidEmpCode.ValidationGroup = ""
                End If

                If CBool(Session("MiddleNameMandatory")) = True Then
                    'rfvOtherName.ValidationGroup = btnSave.ValidationGroup
                    'lblOthername.Text = lblOthername.Text & "*"
                    lblOthername.CssClass = "required"
                Else
                    'rfvOtherName.ValidationGroup = ""
                    lblOthername.CssClass = ""
                End If

                If CBool(Session("GenderMandatory")) = True Then
                    'rfvGender.ValidationGroup = btnSave.ValidationGroup
                    'lblGender.Text = lblGender.Text & "*"
                    lblGender.CssClass = "required"
                Else
                    'rfvGender.ValidationGroup = ""
                    lblGender.CssClass = ""
                End If

                If CBool(Session("BirthDateMandatory")) = True Then
                    'lblBirthDate.Text = lblBirthDate.Text & "*"
                    lblBirthDate.CssClass = "required"
                Else
                    lblBirthDate.CssClass = ""
                End If

                If CBool(Session("NationalityMandatory")) = True Then
                    'rfvNationality.ValidationGroup = btnSave.ValidationGroup
                    'lblNationality.Text = lblNationality.Text & "*"
                    lblNationality.CssClass = "required"
                Else
                    'rfvNationality.ValidationGroup = ""
                    lblNationality.CssClass = ""
                End If

                Call GetValue()

                'Pinkal (30-Sep-2023) -- Start
                '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
            Else

                If (hfFileNameBirthCertificate.Value Is Nothing OrElse hfFileNameBirthCertificate.Value.Length <= 0) Then
                    lblFileNameBirthCertificate.Text = ""
                    lblFileExtBirthCertificate.Text = ""
                    lblDocSizeBirthCertificate.Text = ""
                    hfDocSizeBirthCertificate.Value = ""
                    hfFileNameBirthCertificate.Value = ""
                    pnlFileBirthCertificate.Visible = False
                    Session(flBirthCertificate.ClientID) = Nothing
                End If

                mblnFileUploaded = CBool(Me.ViewState("IsFileUpload"))
                'Pinkal (30-Sep-2023) -- End
            End If

            'Sohail (16 Aug 2019) -- Start
            'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
            'LanguageOpner.Visible = False
            'LanguageControl.Visible = False 'Sohail (05 May 2021)
            Dim lo As Boolean = False
            Dim lc As Boolean = False
            If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
                Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
                If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                        'LanguageOpner.Visible = True
                        lo = True
                        lc = True
                    End If
                Else
                    arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
                        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                            'LanguageOpner.Visible = True
                            lo = True
                            lc = True
                        End If
                    End If
                End If
            ElseIf Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
                If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
                    'LanguageOpner.Visible = True
                    lo = True
                    lc = True
                End If
                btnSave.Enabled = False
            End If
            If lo = False Then LanguageOpner.Visible = False
            If lc = False Then LanguageControl.Visible = False
            'Sohail (16 Aug 2019) -- End
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub
#End Region

#Region " Button Events "

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        'lblError.Visible = False
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidated() = False Then Exit Try

            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".

            Dim dtoAttach As dtoAddAppAttach = Nothing
            Dim lstAttach As List(Of dtoAddAppAttach) = Nothing

            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then

                If CBool(Session("BirthCertificateAttachment")) Then

                    '* START IF ANY APPLICANT ATTACH BIRTH CERTIFICATE AGAIN THEN PREVIOUS ONE WILL BE DELETED AND NEW WILL INSERT

                    If mblnFileUploaded Then

                        If hfBirthCertificateAttachId.Value IsNot Nothing AndAlso hfBirthCertificateAttachId.Value <> "" AndAlso CInt(hfBirthCertificateAttachId.Value) > 0 Then
                            Dim objSearch As New clsSearchJob
                            If objSearch.DeleteApplicantAttachment(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), CInt(hfBirthCertificateAttachId.Value)) = False Then
                                Exit Sub
                            End If
                            objSearch = Nothing
                            hfBirthCertificateAttachId.Value = ""
                            hfBirthCertificateAttachId.Value = Nothing
                        End If
                        '* END  IF ANY APPLICANT ATTACH BIRTH CERTIFICATE AGAIN THEN PREVIOUS ONE WILL BE DELETED AND NEW WILL INSERT

                        Dim strFolder As String = System.Configuration.ConfigurationManager.AppSettings("ApplicantBirthCertificate")
                        Dim strUnkImgName As String = Session("CompCode").ToString & "_" & Guid.NewGuid.ToString & "." & lblFileNameBirthCertificate.Text.Substring(lblFileNameBirthCertificate.Text.LastIndexOf(".") + 1)
                        Dim decSize As Decimal = 0
                        Decimal.TryParse(hfDocSizeBirthCertificate.Value, decSize)

                        dtoAttach = New dtoAddAppAttach
                        lstAttach = New List(Of dtoAddAppAttach)

                        With dtoAttach
                            .CompCode = Session("CompCode").ToString
                            .ComUnkID = CInt(Session("companyunkid"))
                            .ApplicantUnkid = CInt(Session("applicantunkid"))
                            .DocumentUnkid = CInt(ddlBirthCertificateAttachType.SelectedValue)
                            .ModulerefId = CInt(enImg_Email_RefId.Applicant_Module)
                            .AttachrefId = CInt(enScanAttactRefId.APPLICANT_BIRTHINFO)
                            .FolderName = strFolder
                            .Filepath = Server.MapPath("~") & "\" & strFolder & "\" & strUnkImgName
                            .Filename = lblFileNameBirthCertificate.Text
                            .Fileuniquename = strUnkImgName
                            .File_size = CInt(decSize / 1024)
                            .Attached_Date = Now
                            .flUpload = CType(Session(flBirthCertificate.ClientID), HttpPostedFile)
                        End With

                        lstAttach.Add(dtoAttach)

                        'Pinkal (30-Sep-2023) -- Start
                        '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
                        Dim hpf As HttpPostedFile
                        If System.IO.Directory.Exists(Server.MapPath("~") & "\" & dtoAttach.FolderName) = False Then
                            System.IO.Directory.CreateDirectory(Server.MapPath("~") & "\" & dtoAttach.FolderName)
                        End If
                        hpf = dtoAttach.flUpload
                        hpf.SaveAs(Server.MapPath("~") & "\" & dtoAttach.FolderName & "\" & dtoAttach.Fileuniquename)
                        'Pinkal (30-Sep-2023) -- End

                    End If  '  If CBool(Session("BirthCertificateAttachment")) Then

                End If   '  If CBool(Session("BirthCertificateAttachment")) Then

                mblnFileUploaded = False

            End If  'If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then

            If objPersonalInfo.SavePersonalInfo(Session("CompCode").ToString _
                                             , CInt(Session("companyunkid")) _
                                             , CInt(Session("applicantunkid")) _
                                             , "", CInt(DrpTitle.SelectedValue) _
                                             , txtFirstName.Text _
                                             , txtSurName.Text _
                                             , txtOtherName.Text _
                                             , CInt(drpGender.SelectedValue) _
                                             , Session("email").ToString _
                                             , txtEmpCode.Text _
                                             , dtBirthdate.GetDate() _
                                             , txtMobileNo.Text _
                                             , txtAlterNativeno.Text _
                                             , txtAlterNativeno.Text _
                                             , CInt(drpNationality.SelectedValue) _
                                             , txtNin.Text _
                                             , txtTin.Text _
                                             , txtNidaMobile.Text _
                                             , txtvillage.Text _
                                             , txtphoneNumber.Text _
                                             , CInt(drpResidency.SelectedValue) _
                             , txtIndexNo.Text _
                             , lstAttach
                                             ) = True Then

                'Pinkal (30-Sep-2023) -- (A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".[ , lstAttach]
                'S.SANDEEP |04-MAY-2023|  -- START 'ISSUE/ENHANCEMENT : A1X-833 {txtNin,txtTin,txtNidaMobile,txtvillage,txtphoneNumber} -- END
                ShowMessage(lblSaveMsg.Text, MessageType.Info)

                Dim strCacheKey As String = "AppPersonal_" & CStr(Session("CompCode")) & "_" & CInt(Session("companyunkid")).ToString & "_" & CInt(Session("applicantunkid")).ToString
                Dim cache As ObjectCache = MemoryCache.Default
                If cache(strCacheKey) IsNot Nothing Then
                    cache.Remove(strCacheKey)
                End If

                Dim s4 As Site4 = CType(Me.Master, Site4)
                s4.MarkStarToMenuName(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))


            End If  'If objPersonalInfo.SavePersonalInfo



            'Pinkal (30-Sep-2023) -- End

        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    'Sohail (16 Aug 2019) -- Start
    'NMB Recruitment UAT # TC006 - 76.1 - On internal recruitment portal, rename Emp. Code to Staff ID.
    Protected Sub LanguageOpner_ButtonClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            'LanguageControl.Visible = True
            LanguageControl.show()
        End If
    End Sub
    'Sohail (16 Aug 2019) -- End

    'Pinkal (30-Sep-2023) -- Start
    '(A1X-1355) TRA - Provide a separate attachment category option called "Birth Certificate".
    Protected Sub flUpload_btnUpload_Click(sender As Object, e As EventArgs)
        Try
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())


            If flBirthCertificate.HasFile = False Then Exit Try

            If CInt(ddlBirthCertificateDocType.SelectedValue) <= 0 Then
                ShowMessage(LblBirthCertificateDocType.Text, MessageType.Info)
                ddlBirthCertificateDocType.Focus()
                Exit Sub
            ElseIf CInt(ddlBirthCertificateAttachType.SelectedValue) <= 0 Then
                ShowMessage(LblBirthCertificateAttachType.Text, MessageType.Info)
                ddlBirthCertificateAttachType.Focus()
                Exit Sub
            End If

            Dim blnInvalidFile As Boolean = False
            Select Case flBirthCertificate.FileName.Substring(flBirthCertificate.FileName.LastIndexOf(".") + 1).ToUpper
                Case "JPG", "JPEG", "BMP", "GIF", "PNG"
                    If flBirthCertificate.AllowImageFile = False Then
                        blnInvalidFile = True
                    End If
                Case "DOC", "DOCX"
                    If flBirthCertificate.AllowDocumentFile = False Then
                        blnInvalidFile = True
                    End If
                Case "PDF"

                Case Else
                    blnInvalidFile = True
            End Select

            If blnInvalidFile = True Then
                ShowMessage(lblAttachImageMsg.Text, MessageType.Info)
                Exit Try
            End If

            Dim mintByes As Integer = CInt(flBirthCertificate.RemainingSizeKB) '614400 = 600 KB, 819200 = 800 KB, 1048576 = 1 MB, 6291456 = 6 MB
            If flBirthCertificate.FileBytes.LongLength > mintByes Then   ' 6 MB = 6291456 Bytes
                ShowMessage(lblAttachSizeMsg.Text.Replace("#maxsize#", Format(((mintByes / 1024) / 1024), "0.00")), MessageType.Info)
                Exit Try
            End If

            lblFileNameBirthCertificate.Text = flBirthCertificate.PostedFile.FileName
            lblFileExtBirthCertificate.Text = flBirthCertificate.FileName.Substring(flBirthCertificate.FileName.LastIndexOf(".") + 1).ToUpper
            lblDocSizeBirthCertificate.Text = Format(((flBirthCertificate.FileBytes.LongLength / 1024) / 1024), "0.00") & " MB."
            hfDocSizeBirthCertificate.Value = flBirthCertificate.FileBytes.LongLength.ToString()
            hfFileNameBirthCertificate.Value = lblFileNameBirthCertificate.Text
            pnlFileBirthCertificate.Visible = True
            Session(flBirthCertificate.ClientID) = flBirthCertificate.PostedFile

            mblnFileUploaded = True

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Pinkal (30-Sep-2023) -- End

#End Region

#Region " Combobox Events "

    'Hemant (17 Oct 2023) -- Start
    'ISSUE/ENHANCEMENT(TRA) : Every qualification category should be bound with specific document type in attachment.
    Private Sub ddlBirthCertificateDocType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBirthCertificateDocType.SelectedIndexChanged
        Try
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() = "TRA" Then
                If CInt(ddlBirthCertificateDocType.SelectedValue) > 0 Then
                    Dim dsAttachmentType As DataSet = objApplicant.GetCommonMaster(Session("CompCode").ToString, CInt(Session("companyunkid")), clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES)
                    Dim drAttachment() As DataRow = dsAttachmentType.Tables(0).Select("name = '" & ddlBirthCertificateDocType.SelectedItem.Text & "' ")
                    If drAttachment.Length > 0 Then
                        ddlBirthCertificateAttachType.SelectedValue = CStr(drAttachment(0).Item("masterunkid"))
                        ddlBirthCertificateAttachType.Enabled = False
                    Else
                        ddlBirthCertificateAttachType.Enabled = True
                    End If

                Else
                    ddlBirthCertificateAttachType.SelectedIndex = 0
                    ddlBirthCertificateAttachType.Enabled = True
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    'Hemant (17 Oct 2023) -- End

#End Region

End Class