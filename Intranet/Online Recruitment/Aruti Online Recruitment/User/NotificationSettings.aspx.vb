﻿Public Class NotificationSettings
    Inherits Base_Page

#Region "Private Variables"
    Private objAppNotificationSettings As New clsNotificationSettings
    Private mintTotalVacancy As Integer = 0
#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
                FillApplicantNotificationSettings()
                If Session("maxvacancyalert") IsNot Nothing AndAlso CInt(Session("maxvacancyalert")) > 0 Then
                    LblHeader.Text = "[" & Session("maxvacancyalert").ToString() & " Maximum Vacancies]"
                End If
            Else
                mintTotalVacancy = Me.ViewState("TotalVacanyCount")
            End If
            If CBool(Session("isvacancyalert")) = False Then
                Response.Redirect("~/User/UserHome.aspx", False)
                Exit Sub
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub NotificationSettings_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
            ViewState("TotalVacanyCount") = mintTotalVacancy
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region " Method Functions "
    Private Sub FillApplicantNotificationSettings()
        Try
            odsVacancyNotification.SelectParameters.Item("xCompanyID").DefaultValue = CInt(Session("companyunkid"))
            odsVacancyNotification.SelectParameters.Item("xCompanyCode").DefaultValue = Session("CompCode").ToString()
            odsVacancyNotification.SelectParameters.Item("xApplicantID").DefaultValue = CInt(Session("applicantunkid"))
            odsVacancyNotification.SelectParameters.Item("intMasterType").DefaultValue = CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER)
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region "CheckBox Events"
    Protected Sub chkNotification_CheckedChanged(sender As Object, e As EventArgs)
        Try
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Dim chk As CheckBox = CType(sender, CheckBox)
            Dim mintPriority As Integer = 0

            If chk.Checked Then
                Dim dsList As DataSet = objAppNotificationSettings.GetApplicantNotificationSettings(CInt(Session("companyunkid")), Session("CompCode").ToString(), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.VACANCY_MASTER)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim drRow() As DataRow = dsList.Tables(0).Select("isactive = 1")
                    If drRow.Length > 0 AndAlso (CInt(Session("maxvacancyalert")) < drRow.Length + 1) Then
                        chk.Checked = False
                        ShowMessage(lblMaximumMsg.Text.Replace("#numof#", Session("maxvacancyalert").ToString()), MessageType.Info)
                        Exit Sub
                    Else
                        If drRow.Length > 0 Then  'IF DATAROW HAD ROWS OTHERWISE INCREASE PRIORITY TO 1.
                            Dim dtPriority As DataTable = drRow.CopyToDataTable()
                            mintPriority = dtPriority.Compute("Max(priority)", "isactive = 1")
                        End If
                        mintPriority += 1
                    End If
                End If
            Else
                mintPriority = 0
            End If

            Dim dR As DataListItem = CType(chk.NamingContainer, DataListItem)
            Dim xVacancyID As Integer = CInt(CType(dlVacanciesList.Items(dR.ItemIndex).FindControl("hdnfieldVacancytitleId"), HiddenField).Value)
            objAppNotificationSettings.InsertUpdate_ApplicantNotificationSettings(CInt(Session("companyunkid")), Session("CompCode").ToString(), CInt(Session("applicantunkid")), xVacancyID, mintPriority, chk.Checked)

            dlVacanciesList.DataBind()

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub dlVacanciesList_ItemDataBound(sender As Object, e As DataListItemEventArgs) Handles dlVacanciesList.ItemDataBound
        Try
            If e.Item.ItemIndex < 0 Then Exit Sub
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                Dim lnkup As LinkButton = CType(e.Item.FindControl("lnkUp"), LinkButton)
                If e.Item.ItemIndex <= 0 Then
                    lnkup.Enabled = False
                End If

                Dim lnkdown As LinkButton = CType(e.Item.FindControl("lnkDown"), LinkButton)
                If e.Item.ItemIndex = mintTotalVacancy Then
                    lnkdown.Enabled = False
                End If

                Dim chk As CheckBox = CType(e.Item.FindControl("chkNotification"), CheckBox)

                lnkdown.Visible = chk.Checked
                lnkup.Visible = chk.Checked

            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region "LinkButton Events"

    Protected Sub lnkupdown_Click(sender As Object, e As EventArgs)
        Try
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub

            Dim lnk As LinkButton = CType(sender, LinkButton)
            Dim item As DataListItem = CType(lnk.NamingContainer, DataListItem)

            Dim xCurrentVacancyID As Integer = CInt(CType(dlVacanciesList.Items(item.ItemIndex).FindControl("hdnfieldVacancytitleId"), HiddenField).Value)
            Dim mblnCurrentIsActive As Boolean = CBool(CType(dlVacanciesList.Items(item.ItemIndex).FindControl("chkNotification"), CheckBox).Checked)
            Dim xCurrentPriority As Integer = CInt(CType(dlVacanciesList.Items(item.ItemIndex).FindControl("hdnfieldPriority"), HiddenField).Value)

            If lnk.ID = "lnkUp" Then

                Dim xPreviousVacancyID As Integer = CInt(CType(dlVacanciesList.Items(item.ItemIndex - 1).FindControl("hdnfieldVacancytitleId"), HiddenField).Value)
                Dim mblnPreviousIsActive As Boolean = CBool(CType(dlVacanciesList.Items(item.ItemIndex - 1).FindControl("chkNotification"), CheckBox).Checked)
                Dim xPreviousPriority As Integer = CInt(CType(dlVacanciesList.Items(item.ItemIndex - 1).FindControl("hdnfieldPriority"), HiddenField).Value)

                objAppNotificationSettings.InsertUpdate_ApplicantNotificationSettings(CInt(Session("companyunkid")), Session("CompCode").ToString(), CInt(Session("applicantunkid")), xCurrentVacancyID, xPreviousPriority, mblnCurrentIsActive)
                objAppNotificationSettings.InsertUpdate_ApplicantNotificationSettings(CInt(Session("companyunkid")), Session("CompCode").ToString(), CInt(Session("applicantunkid")), xPreviousVacancyID, xCurrentPriority, mblnPreviousIsActive)

            ElseIf lnk.ID = "lnkdown" Then

                Dim xNextVacancyID As Integer = CInt(CType(dlVacanciesList.Items(item.ItemIndex + 1).FindControl("hdnfieldVacancytitleId"), HiddenField).Value)
                Dim mblnNextIsActive As Boolean = CBool(CType(dlVacanciesList.Items(item.ItemIndex + 1).FindControl("chkNotification"), CheckBox).Checked)
                Dim xNextPriority As Integer = CInt(CType(dlVacanciesList.Items(item.ItemIndex + 1).FindControl("hdnfieldPriority"), HiddenField).Value)

                objAppNotificationSettings.InsertUpdate_ApplicantNotificationSettings(CInt(Session("companyunkid")), Session("CompCode").ToString(), CInt(Session("applicantunkid")), xCurrentVacancyID, xNextPriority, mblnCurrentIsActive)
                objAppNotificationSettings.InsertUpdate_ApplicantNotificationSettings(CInt(Session("companyunkid")), Session("CompCode").ToString(), CInt(Session("applicantunkid")), xNextVacancyID, xCurrentPriority, mblnNextIsActive)

            End If

            dlVacanciesList.DataBind()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region "Object Datasource Events"
    Private Sub odsVacancyNotification_Selected(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles odsVacancyNotification.Selected
        Try
            'Dim dtTable As DataTable = CType(e.ReturnValue, DataSet).Tables(0)
            'Dim drRow() As DataRow = dtTable.Select("isactive = 1")
            'mintTotalVacancy = drRow.Length - 1
            Dim intC As Integer = 0
            If TypeOf e.ReturnValue Is DataSet Then
                Dim dtTable As DataTable = CType(e.ReturnValue, DataSet).Tables(0)
                Dim drRow() As DataRow = dtTable.Select("isactive = 1")
                intC = drRow.Length - 1
            End If
            mintTotalVacancy = intC
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

End Class
