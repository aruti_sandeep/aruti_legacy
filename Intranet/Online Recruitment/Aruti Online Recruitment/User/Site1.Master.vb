﻿Option Strict On

'Imports Microsoft.Security.Application

Public Class Site1
    Inherits System.Web.UI.MasterPage

    'Sohail (21 Nov 2017) -- Start
    'Enhancement - 70.1 - OWASP changes.
    Private Const AntiXsrfTokenKey As String = "__AntiXsrfToken"
    Private Const AntiXsrfUserNameKey As String = "__AntiXsrfUserName"
    Private _antiXsrfTokenValue As String
    'Sohail (21 Nov 2017) -- End

#Region " Public Enum "
    Public Enum MessageType
        Success
        Errorr
        Info
        Warning
    End Enum
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session.IsNewSession = True OrElse Session("companyunkid") Is Nothing Then
                Dim strUserId As String = Membership.GetUser().ProviderUserKey.ToString
                Dim strAuthCode As String = (New clsApplicant).GetCompanyAuthenticationCode(strUserId)
                If strAuthCode.Trim <> "" Then
                    ShowMessage("Sorry, Session is expired!", Request.Url.GetLeftPart(UriPartial.Authority) & "/" & Request.ApplicationPath & "/Login.aspx?ext=7&cc=" & strAuthCode, MessageType.Info)
                Else
                    ShowMessage("Sorry, Session is expired!", Request.Url.GetLeftPart(UriPartial.Authority) & "/" & Request.ApplicationPath & "/Login.aspx", MessageType.Info)
                End If
                Exit Try
            End If

            If IsPostBack = False Then
                'lblCompanyName.Text = Sanitizer.GetSafeHtmlFragment(Session("CompName").ToString)
                'lblUserName.Text = Sanitizer.GetSafeHtmlFragment(Session("email").ToString)
                lblCompanyName.Text = AntiXss.AntiXssEncoder.HtmlEncode(Session("CompName").ToString, True)
                lblUserName.Text = AntiXss.AntiXssEncoder.HtmlEncode(Session("email").ToString, True)

                If Session("image") IsNot Nothing AndAlso IsDBNull(Session("image")) = False Then
                    Dim bytes As Byte() = DirectCast(Session("image"), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes)
                    img_shortcuticon.ImageUrl = Convert.ToString("data:image/jpg;base64,") & base64String
                Else
                    img_shortcuticon.ImageUrl = "~/Images/blank.png"
                End If

                'If Session("companyunkid") <= 0 OrElse Session("CompCode") = "" Then
                '    Response.Redirect("~/UnauthorizedAccessaspx", False)
                '    Exit Sub
                'End If

            End If

            If (ViewState("mstrUrlArutiLink") Is Nothing OrElse ViewState("mstrUrlArutiLink").ToString = "") AndAlso Session("mstrUrlArutiLink") IsNot Nothing Then
                ViewState("mstrUrlArutiLink") = Session("mstrUrlArutiLink").ToString
                LoginStatus1.LogoutPageUrl = ViewState("mstrUrlArutiLink").ToString
            End If

            If Session("email") Is Nothing OrElse Session("email").ToString.Trim = "" Then
                Response.Redirect(ViewState("mstrUrlArutiLink").ToString, False)
                Exit Sub
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub Site1_Init(sender As Object, e As EventArgs) Handles Me.Init
        Try
            'Sohail (21 Nov 2017) -- Start
            'Enhancement - 70.1 - OWASP changes.                
            'The code below helps to protect against XSRF attacks
            Dim requestCookie = Request.Cookies(AntiXsrfTokenKey)
            Dim requestCookieGuidValue As Guid
            If requestCookie IsNot Nothing AndAlso Guid.TryParse(requestCookie.Value, requestCookieGuidValue) Then

                'Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value
                Page.ViewStateUserKey = _antiXsrfTokenValue

            Else

                'Generate a New Anti-XSRF token And save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N")
                Page.ViewStateUserKey = _antiXsrfTokenValue

                Dim responseCookie = New HttpCookie(AntiXsrfTokenKey) With {
                    .HttpOnly = True,
                    .Value = _antiXsrfTokenValue,
                    .Path = "/;sameSite=Strict"
                    }

                If FormsAuthentication.RequireSSL AndAlso Request.IsSecureConnection Then

                    responseCookie.Secure = True
                End If
                Response.Cookies.Set(responseCookie)
            End If

            Dim AspxAuthCookie = Request.Cookies(".ASPXAUTH")
            If AspxAuthCookie IsNot Nothing AndAlso Request.IsSecureConnection Then
                AspxAuthCookie.HttpOnly = True
                AspxAuthCookie.Secure = True
                AspxAuthCookie.Path = "/;sameSite=Strict"
                Response.Cookies.Set(AspxAuthCookie)
            End If

            AddHandler Page.PreLoad, AddressOf Site1_PreLoad
            'Sohail (21 Nov 2017) -- End

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Sohail (21 Nov 2017) -- Start
    'Enhancement - 70.1 - OWASP changes.  
    Private Sub Site1_PreLoad(sender As Object, e As EventArgs)
        Try
            If Not IsPostBack Then

                'Set Anti-XSRF token
                ViewState(AntiXsrfTokenKey) = Page.ViewStateUserKey
                ViewState(AntiXsrfUserNameKey) = If(Context.User.Identity.Name, [String].Empty)

            Else

                'Validate the Anti-XSRF token
                If ViewState(AntiXsrfTokenKey).ToString() <> _antiXsrfTokenValue Then
                    Throw New InvalidOperationException("Validation of Anti-XSRF token failed.")
                End If
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
    'Sohail (21 Nov 2017) -- End

    Public Sub ShowMessage(strMessage As String, Optional type As MessageType = MessageType.Info)
        Try
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" & Message & "', '" & type & "');", True)
            If strMessage.Trim = "" Then
                strMessage = "Transaction Saved Successfully!"
            End If
            Dim strTitle As String = "Aruti"
            Dim strType As String = "BootstrapDialog.TYPE_PRIMARY"
            Dim strClass As String = ""
            Select Case type
                Case MessageType.Errorr
                    strType = "BootstrapDialog.TYPE_DANGER"
                    strClass = "glyphicon glyphicon-exclamation-sign"
                Case MessageType.Success
                    'strType = "Success!"
                    strClass = "glyphicon glyphicon-info-sign"
                Case MessageType.Warning
                    strType = "BootstrapDialog.TYPE_WARNING"
                    strClass = "glyphicon glyphicon-warning-sign"
                Case Else
                    strType = "BootstrapDialog.TYPE_INFO"
                    strClass = "glyphicon glyphicon-info-sign"
            End Select
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "$('#msgicon').removeClass();$('#msgicon').addClass('" & strClass & "');$('.modal-body #msgbody').text('" & strType & "' + ' ' + '" & strMessage & "');$('#myModal').modal();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(),
               "BootstrapDialog.show({
                    type: " & strType & ",
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: '" & strTitle & "',
                    message: '" & strMessage & "',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function(dialogItself){
                        dialogItself.close();
                        }
                    }]
                });", True)
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Public Sub ShowMessage(strMessage As String, strRedirectLink As String, Optional type As MessageType = MessageType.Info)
        Try
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" & Message & "', '" & type & "');", True)
            If strMessage.Trim = "" Then
                strMessage = "Transaction Saved Successfully!"
            End If
            Dim strTitle As String = "Aruti"
            Dim strType As String = "BootstrapDialog.TYPE_PRIMARY"
            Dim strClass As String = ""
            Select Case type
                Case MessageType.Errorr
                    strType = "BootstrapDialog.TYPE_DANGER"
                    strClass = "glyphicon glyphicon-exclamation-sign"
                Case MessageType.Success
                    'strType = "Success!"
                    strClass = "glyphicon glyphicon-info-sign"
                Case MessageType.Warning
                    strType = "BootstrapDialog.TYPE_WARNING"
                    strClass = "glyphicon glyphicon-warning-sign"
                Case Else
                    strType = "BootstrapDialog.TYPE_INFO"
                    strClass = "glyphicon glyphicon-info-sign"
            End Select
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(), "$('#msgicon').removeClass();$('#msgicon').addClass('" & strClass & "');$('.modal-body #msgbody').text('" & strType & "' + ' ' + '" & strMessage & "');$('#myModal').modal();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), System.Guid.NewGuid().ToString(),
               "BootstrapDialog.show({
                    type: " & strType & ",
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    title: '" & strTitle & "',
                    message: '" & strMessage & "',
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        action: function(){
                        window.location.href='" & strRedirectLink & "';
                        return false;
                        }
                    }]
                });", True)
        Catch ex As Exception
            'S.SANDEEP [03-NOV-2016] -- START
            Global_asax.CatchException(ex, Context)
            'S.SANDEEP [03-NOV-2016] -- END
        End Try
    End Sub

    Private Sub LoginStatus1_LoggingOut(sender As Object, e As LoginCancelEventArgs) Handles LoginStatus1.LoggingOut
        FormsAuthentication.SignOut()

    End Sub
End Class