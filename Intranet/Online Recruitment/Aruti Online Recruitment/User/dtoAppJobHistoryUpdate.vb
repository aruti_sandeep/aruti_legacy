﻿Imports System.ComponentModel.DataAnnotations
Public Class dtoAppJobHistoryUpdate

    <Required()>
    Public Property CompCode As String
    <Required()>
    Public Property ComUnkID As Integer
    <Required()>
    Public Property ApplicantUnkid As Integer
    <Required()>
    Public Property Jobhistorytranunkid As Integer
    <Required()>
    Public Property EmployerName As String
    <Required()>
    Public Property CompanyName As String
    <Required()>
    Public Property Designation As String
    <Required()>
    Public Property Responsibility As String
    <Required()>
    Public Property Joiningdate As Date
    <Required()>
    Public Property Terminationdate As Date
    <Required()>
    Public Property Officephone As String
    <Required()>
    Public Property Leavingreason As String
    <Required()>
    Public Property Achievements As String

End Class
