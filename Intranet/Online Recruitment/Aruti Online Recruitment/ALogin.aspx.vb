﻿Option Strict On
Imports System.Net
Imports System.Web.Services

Public Class ALogin
    Inherits Base_General

    Private objApplicant As New clsApplicant
    Protected Shared ReCaptcha_Key As String = ConfigurationManager.AppSettings.Item("recaptchaPublicKey").ToString
    Protected Shared ReCaptcha_Secret As String = ConfigurationManager.AppSettings.Item("recaptchaPrivateKey").ToString
#Region " Private Function "
    <WebMethod()>
    Public Shared Function VerifyCaptcha(response As String) As String
        Try
            System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
        Catch ex As Exception

        End Try
        Dim url As String = "https://www.google.com/recaptcha/api/siteverify?secret=" & ReCaptcha_Secret & "&response=" & response
        Return (New WebClient()).DownloadString(url)
    End Function
#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If IsPostBack = False Then
            'Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Dim comp As String

            If IsPostBack = False OrElse Session("companyunkid") Is Nothing OrElse Session("CompCode") Is Nothing OrElse Session("CompCode").ToString = "" OrElse Session("Vacancy") Is Nothing OrElse Session("Vacancy").ToString = "" Then

                Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
                Dim comp As String

                If Session.IsNewSession Then
                    ' Force session to be created;
                    ' otherwise the session ID changes on every request.
                    Session("ForceSession") = DateTime.Now
                End If

                'Sohail (27 Oct 2020) -- Start
                'NMB Enhancement # : Allow to access login page with simple URL and without encrypted text in URL.
                Dim strVD As String = Request.ApplicationPath.ToLower.Replace("/", "")
                Dim blnIsArutiHRM As Boolean = True
                If strVD <> "arutihrm" Then
                    Dim objCompany As New clsCompany
                    Dim ds As DataSet = objCompany.GetCompanyByVirtualDirectory(strVD)
                    If ds.Tables(0).Rows.Count > 0 Then
                        Session("companyunkid") = CInt(ds.Tables(0).Rows(0).Item("companyunkid"))
                        Session("CompCode") = ds.Tables(0).Rows(0).Item("company_code").ToString
                        Session("Vacancy") = "Ext"
                        blnIsArutiHRM = False
                    End If
                    objCompany = Nothing
                End If
                If blnIsArutiHRM = True Then
                    'Sohail (27 Oct 2020) -- End
                    If Request.QueryString("cc") Is Nothing OrElse Request.QueryString("cc") = "" OrElse Request.QueryString("ext") Is Nothing OrElse Request.QueryString("ext") = "" Then
                        Response.Redirect("UnauthorizedAccess.aspx", False)
                        Exit Try
                    End If

                    comp = clsCrypto.Dicrypt(Request.QueryString("cc").ToString)
                    Dim a As String() = Split(comp, "?")
                    Dim intCompID As Integer = 0
                    If a.Length <> 2 Then
                        Response.Redirect("UnauthorizedAccess.aspx", False)
                        Exit Sub
                    Else
                        intCompID = CInt(a(0))
                        comp = a(1)
                    End If
                    Session("companyunkid") = intCompID
                    Session("CompCode") = comp

                    Dim ExtInt As String = Request.QueryString("ext").ToString
                    If ExtInt = "7" Then
                        Session("Vacancy") = "Ext" 'Int=Internal / Ext=External
                    ElseIf ExtInt = "9" Then
                        'Sohail (27 Oct 2020) -- Start
                        'NMB Enhancement # : Allow to access login page with simple URL and without encrypted text in URL. (only external vacancies will be allowed from recruitment portal)
                        'Session("Vacancy") = "Int" 'Int=Internal / Ext=External
                        Session("Vacancy") = "Ext" 'Int=Internal / Ext=External
                        'Sohail (27 Oct 2020) -- End 
                        'ElseIf ExtInt = "8" Then
                        '    Response.Redirect("ViewData.aspx", False)
                        '    Exit Sub
                    End If

                    If Session("Vacancy").ToString = "" Then
                        Response.Redirect("UnauthorizedAccess.aspx", False)
                        Exit Sub
                    End If

                End If 'Sohail (27 Oct 2020)

                Dim objSA As New clsSACommon
                If objSA.RunAlterScript(strConn) = False Then

                End If

                Dim blnActive As Boolean = False
                If objApplicant.GetCompanyInfo(CInt(Session("companyunkid")), Session("CompCode").ToString, blnActive) = False Then
                    'Response.Redirect("UnauthorizedAccess.aspx", False)
                    Exit Sub
                Else
                    If blnActive = False Then
                        ShowMessage("Sorry, This company is not active.", MessageType.Info)
                        Exit Try
                    End If
                End If

                If Session("image") IsNot Nothing AndAlso IsDBNull(Session("image")) = False Then
                    Dim bytes As Byte() = DirectCast(Session("image"), Byte())
                    Dim base64String As String = Convert.ToBase64String(bytes)
                    img_shortcuticon.ImageUrl = Convert.ToString("data:image/jpg;base64,") & base64String
                Else
                    img_shortcuticon.ImageUrl = "~/Images/blank.png"
                End If

                If Session("senderaddress").ToString <> "" AndAlso Session("password").ToString <> "" Then
                    Session("blnEmailSetupDone") = True
                Else
                    Session("blnEmailSetupDone") = False
                    Response.Redirect("ucons.aspx", False)
                    Exit Sub
                End If

                clsSACommon.DeleteExpiredSessions()

                'Session("mstrUrlArutiLink") = Request.Url.ToString

                If Roles.RoleExists("admin") = False Then
                    Response.Redirect("UnauthorizedAccess.aspx", False)
                    Exit Sub
                End If

                Dim objCompany1 As New clsCompany
                Session("e_emailsetupunkid") = 0
                objCompany1.GetEmailSetup(Session("CompCode").ToString, CInt(Session("companyunkid")))

                'Temp
                'Response.Redirect("https://arutihr.com.162-222-225-71.plesk-web1.webhostbox.net/arutihrm/ALogin.aspx?ext=" & ExtInt & "&cc=" & Server.UrlEncode(Request.QueryString("cc").ToString), False)
                'Exit Sub
                'Temp

                'Sohail (27 Oct 2020) -- Start
                'NMB Enhancement # : Allow to access login page with simple URL and without encrypted text in URL.
            Else
                If img_shortcuticon.ImageUrl = "" Then
                    If Session("image") IsNot Nothing AndAlso IsDBNull(Session("image")) = False Then
                        Dim bytes As Byte() = DirectCast(Session("image"), Byte())
                        Dim base64String As String = Convert.ToBase64String(bytes)
                        img_shortcuticon.ImageUrl = Convert.ToString("data:image/jpg;base64,") & base64String
                    Else
                        img_shortcuticon.ImageUrl = "~/Images/blank.png"
                    End If
                End If
                'Sohail (27 Oct 2020) -- End
            End If

            If Request.Url.AbsoluteUri.ToString.ToLower.Contains("arutihr.com") = False Then
                pnlPoweredBy.Visible = False
            Else
                pnlPoweredBy.Visible = True
            End If
            'End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Private Sub Login1_LoggingIn(sender As Object, e As LoginCancelEventArgs) Handles Login1.LoggingIn
    '    Try
    '        If Membership.GetUser(Login1.UserName) IsNot Nothing Then
    '            Login1.FailureText = "Your login attempt was not successful. Please try again."

    '            If Roles.IsUserInRole(Login1.UserName, "superadmin") = True Then
    '                ShowMessage("Sorry, Please login through Super Admin Login.", MessageType.Info)
    '                e.Cancel = True
    '                Exit Try
    '            End If

    '            Dim arrAdmin() As String = Session("admin_email").ToString.Split(CChar(";"))

    '            'If Login1.UserName.Trim = Session("admin_email").ToString.Trim Then
    '            If arrAdmin.Contains(Login1.UserName.Trim) = True Then
    '                If Roles.IsUserInRole(Login1.UserName, "admin") = False Then
    '                    Roles.AddUserToRole(Login1.UserName, "admin")
    '                End If
    '            Else
    '                arrAdmin = Session("admin_email").ToString.Split(CChar(","))
    '                If arrAdmin.Contains(Login1.UserName.Trim) = True Then
    '                    If Roles.IsUserInRole(Login1.UserName, "admin") = False Then
    '                        Roles.AddUserToRole(Login1.UserName, "admin")
    '                    End If
    '                Else
    '                    ShowMessage("Sorry, You are not registered as an Admin.", MessageType.Info)
    '                    e.Cancel = True
    '                    Exit Try
    '                End If
    '            End If

    '            If Roles.IsUserInRole(Login1.UserName, "admin") = False Then
    '                ShowMessage("Sorry, Please only Admin user can Login.", MessageType.Info)
    '                e.Cancel = True
    '                Exit Try
    '            End If

    '            If Membership.GetUser(Login1.UserName).IsLockedOut = True Then
    '                Dim dtLastLockOut As Date = Membership.GetUser(Login1.UserName).LastLockoutDate
    '                Dim dtUnlockDate As Date = dtLastLockOut.AddMinutes(10)
    '                'Dim dtUnlockDate As Date = dtLastLockOut.AddHours(3)

    '                If DateTime.Now > dtUnlockDate Then
    '                    Membership.GetUser(Login1.UserName).UnlockUser()
    '                Else
    '                    If DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) <= 59 Then
    '                        ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) & " minutes.")
    '                    Else
    '                        ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Hour, DateTime.Now, dtUnlockDate) & " hour(s).")
    '                    End If
    '                    e.Cancel = True
    '                    Exit Try
    '                End If
    '            End If

    '            If Membership.GetUser(Login1.UserName).IsApproved = True Then
    '                Session("email") = Login1.UserName
    '                Session("CanViewErrorLog") = False
    '                Session("CanTruncateErrorLog") = False
    '                Session("CanShrinkDataBase") = False
    '                Session("CanRunFullScript") = False
    '                Session("CanRunAlterScript") = False

    '                Dim objSAdmin As New clsSACommon
    '                Dim ds As DataSet = objSAdmin.GetCompanyCodeFromEmail(Login1.UserName, Membership.GetUser(Login1.UserName).ProviderUserKey.ToString)

    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    Session("CanViewErrorLog") = CBool(ds.Tables(0).Rows(0).Item("CanViewErrorLog"))
    '                    Session("CanTruncateErrorLog") = CBool(ds.Tables(0).Rows(0).Item("CanTruncateErrorLog"))
    '                    Session("CanShrinkDataBase") = CBool(ds.Tables(0).Rows(0).Item("CanShrinkDataBase"))
    '                    Session("CanRunFullScript") = CBool(ds.Tables(0).Rows(0).Item("CanRunFullScript"))
    '                    Session("CanRunAlterScript") = CBool(ds.Tables(0).Rows(0).Item("CanRunAlterScript"))
    '                End If

    '                'Sohail (20 Dec 2017) -- Start
    '                'Enhancement - Multilple admin email registration.
    '            Else
    '                Login1.FailureText = "Sorry, This email is not activated. Please activate your email from your Mailbox."
    '                ShowMessage("Sorry, This email is not activated. Please activate your email from your mailbox.\n\n Please click on Resend Actication Link to get Activation link again in your Mailbox.", MessageType.Info)
    '                e.Cancel = True
    '                Exit Try
    '                'Sohail (20 Dec 2017) -- End
    '            End If

    '            'Sohail (20 Dec 2017) -- Start
    '            'Enhancement - Multilple admin email registration.
    '            If Membership.ValidateUser(Login1.UserName, Login1.Password) = False Then
    '                Dim objApplicant As New clsApplicant
    '                Dim intCount As Integer = objApplicant.GetFailedPasswordAttemptCount(Membership.GetUser(Login1.UserName).ProviderUserKey.ToString)

    '                Login1.FailureText = "Sorry, Password does not match."
    '                ShowMessage("Sorry, Password does not match. You have " & (Membership.MaxInvalidPasswordAttempts - intCount).ToString() & " attempts left.", MessageType.Info)
    '                e.Cancel = True
    '                Exit Try
    '            End If
    '            'Sohail (20 Dec 2017) -- End

    '            'If Request.Cookies("LogOutPageURL") IsNot Nothing Then
    '            '    Request.Cookies.Remove("LogOutPageURL")
    '            'End If
    '            Dim ck As New HttpCookie("LogOutPageURL") With {
    '                .HttpOnly = True,
    '                .Value = clsSecurity.Encrypt(Convert.ToString(Session("mstrUrlArutiLink")), "ezee"),
    '                .Path = ";SameSite=Strict"
    '            }

    '            If Request.IsSecureConnection Then
    '                ck.Secure = True
    '            End If
    '            Response.Cookies.Set(ck)

    '        Else
    '            Login1.FailureText = "Sorry, This Email does not exist!"
    '            'e.Cancel = True
    '            Exit Try
    '        End If

    '        HttpContext.Current.Session("LangId") = 1

    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    'Private Sub Login1_LoggedIn(sender As Object, e As EventArgs) Handles Login1.LoggedIn
    '    Try
    '        If objApplicant.GetCompanyInfo(CInt(Session("companyunkid")), Session("CompCode").ToString, False) = False Then
    '            Exit Sub
    '        End If
    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    Private Sub LoginButton_Click(sender As Object, e As EventArgs) Handles LoginButton.Click
        Try
            If UserName.Text.Trim = "" Then
                ShowMessage("Please enter Email.", MessageType.Errorr)
                UserName.Focus()
                Exit Try
            ElseIf clsApplicant.IsValidEmail(UserName.Text) = False Then
                ShowMessage("Please enter Valid Email.", MessageType.Errorr)
                UserName.Focus()
                Exit Try
            ElseIf Password.Text.Trim = "" Then
                ShowMessage("Please enter Password.", MessageType.Errorr)
                Password.Focus()
                Exit Try
                'ElseIf txtCaptcha.Text.Trim = "" Then
                '    ShowMessage("Please enter valid captcha.", MessageType.Errorr)
                '    txtCaptcha.Focus()
                '    Exit Try
            ElseIf txtVerificationCode.Text.Trim = "" Then
                ShowMessage("Please Enter Verification Code.", MessageType.Errorr)
                txtVerificationCode.Focus()
                Exit Try
            ElseIf txtVerificationCode.Text <> Session("CaptchaImageText").ToString() Then
                ShowMessage("Please enter valid Verification Code.", MessageType.Errorr)
                txtVerificationCode.Focus()
                Exit Try
            End If
            If Membership.GetUser(UserName.Text) IsNot Nothing Then
                FailureText.Text = "Your login attempt was not successful. Please try again."

                If Roles.IsUserInRole(UserName.Text, "superadmin") = True Then
                    ShowMessage("Sorry, Please login through Super Admin Login.", MessageType.Info)
                    Exit Try
                End If

                Dim arrAdmin() As String = Session("admin_email").ToString.Split(CChar(";"))

                'If Login1.UserName.Trim = Session("admin_email").ToString.Trim Then
                If arrAdmin.Contains(UserName.Text.Trim) = True Then
                    If Roles.IsUserInRole(UserName.Text, "admin") = False Then
                        Roles.AddUserToRole(UserName.Text, "admin")
                    End If
                Else
                    arrAdmin = Session("admin_email").ToString.Split(CChar(","))
                    If arrAdmin.Contains(UserName.Text.Trim) = True Then
                        If Roles.IsUserInRole(UserName.Text, "admin") = False Then
                            Roles.AddUserToRole(UserName.Text, "admin")
                        End If
                    Else
                        ShowMessage("Sorry, You are not registered as an Admin.", MessageType.Info)
                        Exit Try
                    End If
                End If

                If Roles.IsUserInRole(UserName.Text, "admin") = False Then
                    ShowMessage("Sorry, Please only Admin user can Login.", MessageType.Info)
                    Exit Try
                End If

                If Membership.GetUser(UserName.Text).IsLockedOut = True Then
                    Dim dtLastLockOut As Date = Membership.GetUser(UserName.Text).LastLockoutDate
                    Dim dtUnlockDate As Date = dtLastLockOut.AddMinutes(10)
                    'Dim dtUnlockDate As Date = dtLastLockOut.AddHours(3)

                    If DateTime.Now > dtUnlockDate Then
                        Membership.GetUser(UserName.Text).UnlockUser()
                    Else
                        If DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) <= 59 Then
                            ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) & " minutes.")
                        Else
                            ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Hour, DateTime.Now, dtUnlockDate) & " hour(s).")
                        End If
                        'e.Cancel = True
                        Exit Try
                    End If
                End If

                If Membership.GetUser(UserName.Text).IsApproved = True Then
                    Session("email") = UserName.Text
                    Session("CanViewErrorLog") = False
                    Session("CanTruncateErrorLog") = False
                    Session("CanShrinkDataBase") = False
                    Session("CanRunFullScript") = False
                    Session("CanRunAlterScript") = False

                    Dim objSAdmin As New clsSACommon
                    Dim ds As DataSet = objSAdmin.GetCompanyCodeFromEmail(UserName.Text, Membership.GetUser(UserName.Text).ProviderUserKey.ToString)

                    If ds.Tables(0).Rows.Count > 0 Then
                        Session("CanViewErrorLog") = CBool(ds.Tables(0).Rows(0).Item("CanViewErrorLog"))
                        Session("CanTruncateErrorLog") = CBool(ds.Tables(0).Rows(0).Item("CanTruncateErrorLog"))
                        Session("CanShrinkDataBase") = CBool(ds.Tables(0).Rows(0).Item("CanShrinkDataBase"))
                        Session("CanRunFullScript") = CBool(ds.Tables(0).Rows(0).Item("CanRunFullScript"))
                        Session("CanRunAlterScript") = CBool(ds.Tables(0).Rows(0).Item("CanRunAlterScript"))
                    End If

                    'Sohail (20 Dec 2017) -- Start
                    'Enhancement - Multilple admin email registration.
                Else
                    FailureText.Text = "Sorry, This email is not activated. Please activate your email from your Mailbox."
                    ShowMessage("Sorry, This email is not activated. Please activate your email from your mailbox.\n\n Please click on Resend Actication Link to get Activation link again in your Mailbox.", MessageType.Info)
                    'e.Cancel = True
                    Exit Try
                    'Sohail (20 Dec 2017) -- End
                End If

                'Sohail (20 Dec 2017) -- Start
                'Enhancement - Multilple admin email registration.
                If Membership.ValidateUser(UserName.Text, Password.Text) = False Then
                    Dim objApplicant As New clsApplicant
                    Dim intCount As Integer = objApplicant.GetFailedPasswordAttemptCount(Membership.GetUser(UserName.Text).ProviderUserKey.ToString)

                    FailureText.Text = "Sorry, Password does not match."
                    ShowMessage("Sorry, Password does not match. You have " & (Membership.MaxInvalidPasswordAttempts - intCount).ToString() & " attempts left.", MessageType.Info)
                    'e.Cancel = True
                    Exit Try
                End If
                'Sohail (20 Dec 2017) -- End

                'If Session("mstrUrlArutiLink") Is Nothing Then
                'Session("mstrUrlArutiLink") = Request.Url.ToString()
                Session("mstrUrlArutiLink") = Request.Form(hflocationhref.UniqueID)
                'End If

                'If Request.Cookies("LogOutPageURL") IsNot Nothing Then
                '    Request.Cookies.Remove("LogOutPageURL")
                'End If
                Dim ck As New HttpCookie("LogOutPageURL") With {
                    .HttpOnly = True,
                    .Value = clsSecurity.Encrypt(Convert.ToString(Session("mstrUrlArutiLink")), "ezee"),
                    .Path = ";SameSite=Strict"
                }

                If Request.IsSecureConnection Then
                    ck.Secure = True
                End If
                Response.Cookies.Set(ck)

                If objApplicant.GetCompanyInfo(CInt(Session("companyunkid")), Session("CompCode").ToString, False) = False Then
                    Exit Sub
                End If

                Dim objCompany1 As New clsCompany
                Session("e_emailsetupunkid") = 0
                objCompany1.GetEmailSetup(Session("CompCode").ToString, CInt(Session("companyunkid")))

                FormsAuthentication.RedirectFromLoginPage(UserName.Text, False)
                Response.Redirect("Admin/UserHome.aspx", False)

            Else
                FailureText.Text = "Sorry, This Email does not exist!"
                'e.Cancel = True
                Exit Try
            End If

            HttpContext.Current.Session("LangId") = 1
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

End Class