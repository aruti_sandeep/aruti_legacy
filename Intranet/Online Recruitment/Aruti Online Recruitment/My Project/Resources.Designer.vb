﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.Microsoft.VisualBasic.HideModuleNameAttribute()>  _
    Friend Module Resources
        
        Private resourceMan As Global.System.Resources.ResourceManager
        
        Private resourceCulture As Global.System.Globalization.CultureInfo
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("Aruti_Online_Recruitment.Resources", GetType(Resources).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to /****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
        '''IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = &apos;cfcompany_info&apos; AND COLUMNS.COLUMN_NAME = &apos;admin_email&apos;)
        '''BEGIN
        '''	ALTER TABLE cfcompany_info ADD admin_email NVARCHAR(510) NULL
        '''END
        '''GO
        '''
        '''/****** SP: 64.1    Script Date: 08/11/2016 05:12 PM ******/
        '''IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMNS.TABLE_NAME = &apos;cfcompany_info&apos; AND COLUMNS.COLUMN_NAME = &apos;Date_Format&apos;)
        '''BEGIN
        '''	ALTER TABLE cfco [rest of string was truncated]&quot;;.
        '''</summary>
        Friend ReadOnly Property alter_script() As String
            Get
                Return ResourceManager.GetString("alter_script", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot; ?&gt;
        '''&lt;root&gt;
        '''  &lt;Data&gt;
        '''    &lt;Id&gt;1&lt;/Id&gt;
        '''    &lt;Version&gt;9.0.64.1&lt;/Version&gt;
        '''    &lt;CheckType&gt;90641&lt;/CheckType&gt;
        '''    &lt;Script&gt;
        '''      IF NOT EXISTS(SELECT 1 FROM sys.databases WHERE databases.name = &apos;arutihrms&apos;)
        '''      BEGIN
        '''        CREATE DATABASE arutihrms
        '''      END
        '''    &lt;/Script&gt;
        '''  &lt;/Data&gt;  
        '''&lt;/root&gt;.
        '''</summary>
        Friend ReadOnly Property CreateDatabase() As String
            Get
                Return ResourceManager.GetString("CreateDatabase", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to /****** Object:  DatabaseRole [aspnet_Membership_BasicAccess]    Script Date: 08/11/2016 03:19 PM ******/
        '''IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N&apos;aspnet_Membership_BasicAccess&apos; AND type = &apos;R&apos;)
        '''CREATE ROLE [aspnet_Membership_BasicAccess]
        '''GO
        '''/****** Object:  DatabaseRole [aspnet_Membership_FullAccess]    Script Date: 08/11/2016 03:19 PM ******/
        '''IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N&apos;aspnet_Membership_FullAccess&apos; AND type = &apos;R&apos;)
        '''CREATE ROLE [aspnet [rest of string was truncated]&quot;;.
        '''</summary>
        Friend ReadOnly Property full_script() As String
            Get
                Return ResourceManager.GetString("full_script", resourceCulture)
            End Get
        End Property
    End Module
End Namespace
