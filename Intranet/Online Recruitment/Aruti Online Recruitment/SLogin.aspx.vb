﻿Option Strict On
Imports System.Net
Imports System.Web.Services
Imports System.Data
Imports System.Data.SqlClient
'Imports eZeeCommonLib

Public Class SLogin
    Inherits Base_General
    Protected Shared ReCaptcha_Key As String = ConfigurationManager.AppSettings.Item("recaptchaPublicKey").ToString
    Protected Shared ReCaptcha_Secret As String = ConfigurationManager.AppSettings.Item("recaptchaPrivateKey").ToString

#Region " Private Function "
    Private Function IsConnected() As Boolean
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Using con As New SqlConnection(strConn)

                con.Open()

            End Using

            Return True
        Catch exc As Exception
            If exc.Message.StartsWith("Login failed for user ") = True Then
                ShowMessage("Login Failed for User *****.", MessageType.Info)

            ElseIf exc.Message.StartsWith("Cannot open database " & Chr(34) & "arutihrms" & Chr(34) & "") = True Then
                ShowMessage("Database ***** does not exist.", MessageType.Info)

            ElseIf exc.Message.StartsWith("A network-related or instance-specific error occurred") = True Then
                ShowMessage("SQL Server or Instance not found.", MessageType.Info)

            Else
                Response.Redirect("~/AError.aspx", False)
                HttpContext.Current.Session("CustomError") = AntiXss.AntiXssEncoder.HtmlEncode(exc.Message.ToString, True)
                Return False
            End If
            Return False
        End Try
    End Function

    Private Function IsValidatedTestConnection() As Boolean
        Try
            If txtServerName.Text.Trim.Length <= 0 Then
                ShowMessage("Please enterServer Name.", MessageType.Info)
                txtServerName.Focus()
                Return False
            ElseIf txtUserId.Text.Trim.Length <= 0 Then
                ShowMessage("Please enter User ID.", MessageType.Info)
                txtUserId.Focus()
                Return False
            ElseIf txtPassword.Text.Trim.Length <= 0 Then
                ShowMessage("Please enter Password.", MessageType.Info)
                txtPassword.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
            Return False
        End Try
    End Function

    <WebMethod()>
    Public Shared Function VerifyCaptcha(response As String) As String
        Try
            System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
        Catch ex As Exception

        End Try
        Dim url As String = "https://www.google.com/recaptcha/api/siteverify?secret=" & ReCaptcha_Secret & "&response=" & response
        Return (New WebClient()).DownloadString(url)
    End Function
#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                clsApplicant.ClearSessionForSAdmin()

                pnlLogin.Visible = True
                pnlSetup.Visible = False

                If IsConnected() = False Then
                    pnlLogin.Visible = False
                    pnlSetup.Visible = True

                    Exit Try
                End If

                'Session("mstrUrlArutiLink") = Request.Url.ToString

                Dim objSA As New clsSACommon

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
                Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
                'Sohail (30 Nov 2017) -- End

                'Sohail (30 Oct 2018) -- Start
                'Medium Trust Level issue - unable to create database in 75.1.
                Using con As New SqlConnection(strConn)

                    con.Open()

                    Using cmd1 As New SqlCommand("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'rcapplicant_master' ", con)

                        cmd1.CommandType = CommandType.Text

                        Dim objDatabaseName = cmd1.ExecuteScalar()

                        If objDatabaseName Is Nothing Then

                            Call objSA.RunFullScript(strConn)

                            Call objSA.RunAlterScript(strConn)

                            Dim strUser As String = "aruti_sa"

                            'Dim StrQ As String
                            'StrQ = "IF NOT EXISTS(SELECT name FROM sys.syslogins WHERE name = '" & strUser & "') " &
                            '   "BEGIN " &
                            '   "    USE [arutihrms] " &
                            '   "    CREATE LOGIN [" & strUser & "] WITH PASSWORD=N'" & clsSecurity.Decrypt("Qw0hgX7HJ0cge3U8zJbx44dZa+ZiNCzTLLNaxe5cIlv5vcoS7F/wiymPNWvVT+YK", "ezee") & "', DEFAULT_DATABASE=[arutihrms], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF " &
                            '   "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'bulkadmin' " &
                            '   "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'dbcreator' " &
                            '   "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'diskadmin' " &
                            '   "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'processadmin' " &
                            '   "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'securityadmin' " &
                            '   "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'serveradmin' " &
                            '   "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'setupadmin' " &
                            '   "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'sysadmin' " &
                            '   "END "


                            'Using cmd As New SqlCommand(StrQ, con)

                            '    cmd.CommandType = CommandType.Text
                            '    cmd.ExecuteNonQuery()

                            'End Using


                            Call objSA.CreateRoles()

                            Call objSA.CreateSAdminUser()

                        End If

                    End Using
                End Using
                'Sohail (30 Oct 2018) -- End

                If objSA.RunAlterScript(strConn) = False Then

                End If

                If objSA.CreateRoles() = False Then

                End If

                If objSA.CreateSAdminUser() = False Then

                End If

                If objSA.GetSuperAdminEmailSettings() = False Then

                End If

                If objSA.EncryptMailPassword() = False Then

                End If

            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SLogin_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    'Private Sub Login1_LoggingIn(sender As Object, e As LoginCancelEventArgs) Handles Login1.LoggingIn
    '    Try
    '        If Membership.GetUser(Login1.UserName) IsNot Nothing Then
    '            Login1.FailureText = "Your login attempt was not successful. Please try again."

    '            If Roles.IsUserInRole(Login1.UserName, "superadmin") = False Then
    '                ShowMessage("Sorry, Login is alllowed for Admin only.", MessageType.Info)
    '                e.Cancel = True
    '                Exit Try
    '            End If

    '            If Membership.GetUser(Login1.UserName).IsLockedOut = True Then
    '                Dim dtLastLockOut As Date = Membership.GetUser(Login1.UserName).LastLockoutDate
    '                Dim dtUnlockDate As Date = dtLastLockOut.AddMinutes(10)
    '                'Dim dtUnlockDate As Date = dtLastLockOut.AddHours(3)

    '                If DateTime.Now > dtUnlockDate Then
    '                    Membership.GetUser(Login1.UserName).UnlockUser()
    '                Else
    '                    If DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) <= 59 Then
    '                        ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) & " minutes.")
    '                    Else
    '                        ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Hour, DateTime.Now, dtUnlockDate) & " hour(s).")
    '                    End If
    '                    e.Cancel = True
    '                    Exit Try
    '                End If
    '            End If

    '            If Membership.GetUser(Login1.UserName).IsApproved = True Then
    '                Session("email") = Login1.UserName

    '                'Sohail (20 Dec 2017) -- Start
    '                'Enhancement - Multilple admin email registration.
    '            Else
    '                Login1.FailureText = "Sorry, This email is not activated. Please contact aruti support team."
    '                ShowMessage("Sorry, This email is not activated. Please contact aruti support team.", MessageType.Info)
    '                e.Cancel = True
    '                Exit Try
    '                'Sohail (20 Dec 2017) -- End
    '            End If

    '            'Sohail (20 Dec 2017) -- Start
    '            'Enhancement - Multilple admin email registration.
    '            If Membership.ValidateUser(Login1.UserName, Login1.Password) = False Then
    '                Dim objApplicant As New clsApplicant
    '                Dim intCount As Integer = objApplicant.GetFailedPasswordAttemptCount(Membership.GetUser(Login1.UserName).ProviderUserKey.ToString)

    '                Login1.FailureText = "Sorry, Password does not match."
    '                ShowMessage("Sorry, Password does not match. You have " & (Membership.MaxInvalidPasswordAttempts - intCount).ToString() & " attempts left.", MessageType.Info)
    '                e.Cancel = True
    '                Exit Try
    '            End If
    '            'Sohail (20 Dec 2017) -- End

    '            clsSACommon.DeleteExpiredSessions()

    '            'If Request.Cookies("LogOutPageURL") IsNot Nothing Then
    '            '    Request.Cookies.Remove("LogOutPageURL")
    '            'End If
    '            Dim ck As New HttpCookie("LogOutPageURL") With {
    '                .HttpOnly = True,
    '                .Value = clsSecurity.Encrypt(Convert.ToString(Session("mstrUrlArutiLink")), "ezee"),
    '                .Path = ";SameSite=Strict"
    '            }

    '            If Request.IsSecureConnection Then
    '                ck.Secure = True
    '            End If
    '            Response.Cookies.Set(ck)

    '        Else
    '            Login1.FailureText = "Sorry, This Email does not exist!"
    '            'e.Cancel = True
    '            Exit Try
    '        End If

    '        HttpContext.Current.Session("LangId") = 1

    '    Catch ex As Exception
    '        Global_asax.CatchException(ex, Context)
    '    End Try
    'End Sub

    Private Sub LoginButton_Click(sender As Object, e As EventArgs) Handles LoginButton.Click
        Try
            If UserName.Text.Trim = "" Then
                ShowMessage("Please enter Email.", MessageType.Errorr)
                UserName.Focus()
                Exit Try
            ElseIf clsApplicant.IsValidEmail(UserName.Text) = False Then
                ShowMessage("Please enter Valid Email.", MessageType.Errorr)
                UserName.Focus()
                Exit Try
            ElseIf Password.Text.Trim = "" Then
                ShowMessage("Please enter Password.", MessageType.Errorr)
                Password.Focus()
                Exit Try
                'ElseIf txtCaptcha.Text.Trim = "" Then
                '    ShowMessage("Please enter valid captcha.", MessageType.Errorr)
                '    txtCaptcha.Focus()
                '    Exit Try
            ElseIf txtVerificationCode.Text.Trim = "" Then
                ShowMessage("Please Enter Verification Code.", MessageType.Errorr)
                txtVerificationCode.Focus()
                Exit Try
            ElseIf txtVerificationCode.Text <> Session("CaptchaImageText").ToString() Then
                ShowMessage("Please enter valid Verification Code.", MessageType.Errorr)
                txtVerificationCode.Focus()
                Exit Try
            End If
            If Membership.GetUser(UserName.Text) IsNot Nothing Then
                'FailureText.Text = "Your login attempt was not successful. Please try again."

                If Roles.IsUserInRole(UserName.Text, "superadmin") = False Then
                    ShowMessage("Sorry, Login is alllowed for Admin only.", MessageType.Info)
                    'e.Cancel = True
                    Exit Try
                End If

                If Membership.GetUser(UserName.Text).IsLockedOut = True Then
                    Dim dtLastLockOut As Date = Membership.GetUser(UserName.Text).LastLockoutDate
                    Dim dtUnlockDate As Date = dtLastLockOut.AddMinutes(10)
                    'Dim dtUnlockDate As Date = dtLastLockOut.AddHours(3)

                    If DateTime.Now > dtUnlockDate Then
                        Membership.GetUser(UserName.Text).UnlockUser()
                    Else
                        If DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) <= 59 Then
                            ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) & " minutes.")
                        Else
                            ShowMessage("Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Hour, DateTime.Now, dtUnlockDate) & " hour(s).")
                        End If
                        'e.Cancel = True
                        Exit Try
                    End If
                End If

                If Membership.GetUser(UserName.Text).IsApproved = True Then
                    Session("email") = UserName.Text

                    'Sohail (20 Dec 2017) -- Start
                    'Enhancement - Multilple admin email registration.
                Else
                    FailureText.Text = "Sorry, This email is not activated. Please contact aruti support team."
                    ShowMessage("Sorry, This email is not activated. Please contact aruti support team.", MessageType.Info)
                    'e.Cancel = True
                    Exit Try
                    'Sohail (20 Dec 2017) -- End
                End If

                'Sohail (20 Dec 2017) -- Start
                'Enhancement - Multilple admin email registration.
                If Membership.ValidateUser(UserName.Text, Password.Text) = False Then
                    Dim objApplicant As New clsApplicant
                    Dim intCount As Integer = objApplicant.GetFailedPasswordAttemptCount(Membership.GetUser(UserName.Text).ProviderUserKey.ToString)

                    FailureText.Text = "Sorry, Password does not match."
                    ShowMessage("Sorry, Password does not match. You have " & (Membership.MaxInvalidPasswordAttempts - intCount).ToString() & " attempts left.", MessageType.Info)
                    'e.Cancel = True
                    Exit Try
                End If
                'Sohail (20 Dec 2017) -- End

                clsSACommon.DeleteExpiredSessions()

                'If Session("mstrUrlArutiLink") Is Nothing Then
                'Session("mstrUrlArutiLink") = Request.Url.ToString()
                Session("mstrUrlArutiLink") = Request.Form(hflocationhref.UniqueID)
                'End If

                'If Request.Cookies("LogOutPageURL") IsNot Nothing Then
                '    Request.Cookies.Remove("LogOutPageURL")
                'End If
                Dim ck As New HttpCookie("LogOutPageURL") With {
                    .HttpOnly = True,
                    .Value = clsSecurity.Encrypt(Convert.ToString(Session("mstrUrlArutiLink")), "ezee"),
                    .Path = ";SameSite=Strict"
                }

                If Request.IsSecureConnection Then
                    ck.Secure = True
                End If
                Response.Cookies.Set(ck)

                FormsAuthentication.RedirectFromLoginPage(UserName.Text, False)
                Response.Redirect("SAdmin/UserHome.aspx", False)

            Else
                FailureText.Text = "Sorry, This Email does not exist!"
                'e.Cancel = True
                Exit Try
            End If

            HttpContext.Current.Session("LangId") = 1
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

#Region " Buttons Events "
    Private Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click
        Dim objSA As New clsSACommon
        Dim dsList As New DataSet
        Dim dtDatabaseScript As DataTable
        Dim strXML As String = String.Empty
        Dim StrQ As String = String.Empty
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            If IsValidatedTestConnection() = False Then Exit Try

            Dim strConn As String = String.Format("Data Source={0};user id={1};Initial Catalog=master;Password={2};", txtServerName.Text.Trim, txtUserId.Text.Trim, txtPassword.Text)

            Using con As New SqlConnection(strConn)

                con.Open()

                strXML = My.Resources.CreateDatabase

                dsList.ReadXml(New IO.StringReader(strXML))
                dtDatabaseScript = dsList.Tables(0)

                For Each dtRow As DataRow In dtDatabaseScript.Rows
                    StrQ = CStr(dtRow.Item("Script"))

                    Using cmd As New SqlCommand(StrQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.ExecuteNonQuery()

                    End Using
                Next

                Call objSA.RunFullScript(strConn)

                Call objSA.RunAlterScript(strConn)

                Dim strUser As String = "aruti_sa"

                StrQ = "IF NOT EXISTS(SELECT name FROM sys.syslogins WHERE name = '" & strUser & "') " &
                       "BEGIN " &
                       "    USE [master] " &
                       "    CREATE LOGIN [" & strUser & "] WITH PASSWORD=N'" & clsSecurity.Decrypt("Qw0hgX7HJ0cge3U8zJbx44dZa+ZiNCzTLLNaxe5cIlv5vcoS7F/wiymPNWvVT+YK", "ezee") & "', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'bulkadmin' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'dbcreator' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'diskadmin' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'processadmin' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'securityadmin' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'serveradmin' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'setupadmin' " &
                       "    EXEC master..sp_addsrvrolemember @loginame = N'" & strUser & "', @rolename = N'sysadmin' " &
                       "END "


                Using cmd As New SqlCommand(StrQ, con)

                    cmd.CommandType = CommandType.Text
                    cmd.ExecuteNonQuery()

                End Using


                Call objSA.CreateRoles()

                Call objSA.CreateSAdminUser()


            End Using



            pnlLogin.Visible = True
            pnlSetup.Visible = False

            ShowMessage("Database Generated successfully.", MessageType.Info)

        Catch exc As Exception
            If exc.Message.StartsWith("Login failed for user ") = True Then
                ShowMessage("Login Failed for User *****.", MessageType.Info)

            ElseIf exc.Message.StartsWith("Cannot open database " & Chr(34) & "arutihrmss" & Chr(34) & "") = True Then
                ShowMessage("Database ***** does not exist.", MessageType.Info)

            ElseIf exc.Message.StartsWith("A network-related or instance-specific error occurred") = True Then
                ShowMessage("SQL Server or Instance not found.", MessageType.Info)

            Else
                Response.Redirect("~/AError.aspx", False)
                HttpContext.Current.Session("CustomError") = AntiXss.AntiXssEncoder.HtmlEncode(exc.Message.ToString, True)
                Exit Sub
            End If
        End Try
    End Sub


#End Region
End Class