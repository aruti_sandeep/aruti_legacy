﻿Imports System.Reflection

Public Class LanguageControl
    Inherits System.Web.UI.UserControl

    Protected uniqueKey As String

#Region " Properties "
    Public Property _ModuleName As String

    Public Property _webPage As Page
        Get
            Return Me.Page
        End Get
        Set(ByVal value As Page)
            Me.Page = value
        End Set
    End Property

    Public Property _csvclassname As String
    Public Property _TargetControl As String
    Public Property _TargetControl2 As String
    Private blnShowpopup As Boolean

    Public WriteOnly Property IsFireButtonNoClick() As Boolean
        Set(ByVal value As Boolean)
            If value Then
                popuplanguage.CancelControlID = hflanguageClose2.ID
            End If
        End Set
    End Property

    Private sCancelControlID As String = "lnklanguageclose"

    Public Property CancelControlName() As String
        Get
            Return sCancelControlID
        End Get
        Set(ByVal value As String)
            sCancelControlID = value
            popuplanguage.CancelControlID = sCancelControlID
        End Set
    End Property

#End Region

#Region " Private Variables "
    Private AllFormsControl As List(Of Control) = New List(Of Control)()
    Private AllFormsParentIds As Dictionary(Of String, String) = New Dictionary(Of String, String)()
    Private wLang As New clsWebLanguage
    Private wMsgs As clsWebMessages = New clsWebMessages()
    Private flag As Boolean = True
    Private ctrName As String = ""
    Private dtlink As DataTable = Nothing
    Private list As List(Of clsWebLanguage) = Nothing
#End Region

#Region " Form's Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        popuplanguage.CancelControlID = sCancelControlID
        Me.uniqueKey = "" 'Guid.NewGuid.ToString("N")
        If Not IsPostBack Then
            'list = wLang.GetAllCaptions(_ModuleName, Session("mdbname").ToString())
            list = wLang.GetAllCaptions(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strModuleName:=_ModuleName, intLanguageID:=0)
            SetControlLanguage()
            SetFormMessages()
            SetClassMessages()
        Else
            blnShowpopup = Convert.ToBoolean(Me.ViewState("blnShowpopup"))
        End If

        'If ViewState("blnShowpopup") IsNot Nothing Then
        '    blnShowpopup = Convert.ToBoolean(Me.ViewState("blnShowpopup"))
        'End If

        'If blnShowpopup Then
        '    popuplanguage.Show()
        'End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ViewState("blnShowpopup") = blnShowpopup
    End Sub
#End Region

#Region " Private Methods "
    Private Sub SetFormMessages()
        Try
            Dim fMsgs As List(Of clsWebMessages) = Nothing
            'fMsgs = wMsgs.GetAllMessages(_ModuleName, Session("mdbname").ToString())
            fMsgs = wMsgs.GetAllMessages(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strCsvModuleNames:="", strModuleName:=_ModuleName, intLanguageID:=0, strMsgCode:="")

            If fMsgs IsNot Nothing Then
                objgrd_fMessages.DataSource = fMsgs
                objgrd_fMessages.DataBind()
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub SetClassMessages()
        Try
            Dim oMsgs As List(Of clsWebMessages) = Nothing
            'oMsgs = wMsgs.GetAllMessages(_csvclassname, Session("mdbname").ToString())
            oMsgs = wMsgs.GetAllMessages(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strCsvModuleNames:=_csvclassname, strModuleName:="", intLanguageID:=0, strMsgCode:="")

            If oMsgs IsNot Nothing Then
                objgrd_oMessages.DataSource = oMsgs
                objgrd_oMessages.DataBind()
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Public Sub SetControlLanguage()
        Dim list As List(Of clsWebLanguage) = Nothing
        'list = wLang.GetAllCaptions(_ModuleName, Session("mdbname").ToString())
        'dtlink = wLang.GetBlankStructure(Session("mdbname").ToString())
        dtlink = wLang.GetBlankStructure()
        list = wLang.GetAllCaptions(intCompanyUnkId:=CInt(Session("companyunkid")), strCompCode:=Session("CompCode").ToString, strModuleName:=_ModuleName, intLanguageID:=0)
        Dim dco As DataColumn = Nothing
        dco = New DataColumn()
        dco.DataType = GetType(String)
        dco.ColumnName = "Id"
        dco.DefaultValue = ""
        dtlink.Columns.Add(dco)
        dco = New DataColumn()
        dco.DataType = GetType(String)
        dco.ColumnName = "Type"
        dco.DefaultValue = ""
        dtlink.Columns.Add(dco)
        dco = New DataColumn()
        dco.DataType = GetType(Boolean)
        dco.ColumnName = "istext"
        dco.DefaultValue = False
        dtlink.Columns.Add(dco)
        Dim ctl As UpdatePanel = _webPage.GetAllControlsOfType(Of UpdatePanel)().FirstOrDefault()
        InitContolList(ctl)

        If list IsNot Nothing Then
            Dim langid As Integer = 0
            Dim itext As String = ""

            Try
                langid = Convert.ToInt32(HttpContext.Current.Session("LangId"))
            Catch
                langid = 0
            End Try

            For Each l As clsWebLanguage In list
                Dim ctrls As List(Of Control) = AllFormsControl.FindAll(Function(x) x.ID = l._controlunqid).ToList

                If ctrls Is Nothing OrElse ctrls.Count <= 0 Then

                    Dim types As IEnumerator = AllFormsControl.AsEnumerable().OfType(Of DataControlFieldHeaderCell)().Where(Function(c) (CType(c, System.Web.UI.WebControls.DataControlFieldCell)).ContainingField.FooterText = l._controlunqid).GetEnumerator

                    If types.MoveNext() Then
                        ctrls.Add(TryCast(types.Current, Control))
                    Else
                        Continue For
                    End If

                End If

                For Each ctrl As Control In ctrls


                    If ctrl Is Nothing Then
                        Dim type As IEnumerator = AllFormsControl.AsEnumerable().OfType(Of DataControlFieldHeaderCell)().Where(Function(c) (CType(c, System.Web.UI.WebControls.DataControlFieldCell)).ContainingField.FooterText = l._controlunqid).GetEnumerator()

                        If type.MoveNext() Then
                            ctrl = TryCast(type.Current, Control)
                        End If
                    End If

                    If ctrl Is Nothing Then
                        Continue For
                    End If

                    AllFormsParentIds = New Dictionary(Of String, String)()
                    flag = True
                    InitContolParentList(ctrl.UniqueID)
                    Dim Parent As String = ""

                    If AllFormsParentIds IsNot Nothing Then
                        Parent = AllFormsParentIds(ctrl.UniqueID)
                    End If

                    If ctrl IsNot Nothing Then
                        Dim enumerator As IEnumerator = AllFormsControl.AsEnumerable().Where(Function(x) x.UniqueID = l._controlunqid OrElse (If(x.ID IsNot Nothing, x.ID = ctrl.ID, x.UniqueID = ctrl.UniqueID))).GetEnumerator()

                        If True Then

                            While enumerator.MoveNext()

                                If enumerator.Current IsNot Nothing Then
                                    Dim CurrentParent As String = ""
                                    AllFormsParentIds = New Dictionary(Of String, String)()
                                    flag = True
                                    InitContolParentList((CType(enumerator.Current, System.Web.UI.Control)).UniqueID)

                                    If AllFormsParentIds IsNot Nothing AndAlso AllFormsParentIds.ContainsKey((CType(enumerator.Current, Control)).UniqueID) = True Then
                                        CurrentParent = AllFormsParentIds((CType(enumerator.Current, Control)).UniqueID)
                                    End If

                                    If Parent = CurrentParent Then

                                        If ctrl.[GetType]().Name.ToString().ToUpper() = "RADIOBUTTONLIST" OrElse ctrl.[GetType]().Name.ToString().ToUpper() = "DROPDOWNLIST" OrElse ctrl.[GetType]().Name.ToString().ToUpper() = "TEXTBOX" OrElse ctrl.[GetType]().Name.ToString().ToUpper() = "LITERALCONTROL" OrElse ctrl.[GetType]().Name.ToString().ToUpper() = "UPDATEPANEL" OrElse ctrl.[GetType]().Name.ToString().ToUpper() = "CONTROLS_LANGUAGEMESSAGECONTROL_ASCX" OrElse ctrl.[GetType]().Name.ToString().ToUpper() = "CALENDAREXTENDER" OrElse ctrl.[GetType]().Name.ToString().ToUpper() = "CONTROLS_DATECTRL_ASCX" Then
                                            Continue While
                                        End If

                                        Dim row As DataRow = dtlink.NewRow()
                                        row("langunkid") = l._languageunkid
                                        row("formname") = _ModuleName
                                        row("controlname") = l._controlunqid
                                        'row("language") = l._language
                                        'row("language1") = l._language1
                                        'row("language2") = l._language2
                                        row("language") = AntiXss.AntiXssEncoder.HtmlEncode(l._language, True)
                                        row("language1") = AntiXss.AntiXssEncoder.HtmlEncode(l._language1, True)
                                        row("language2") = AntiXss.AntiXssEncoder.HtmlEncode(l._language2, True)
                                        row("Id") = ctrl.ID
                                        row("Type") = ctrl.[GetType]().Name
                                        row("istext") = True
                                        row("comp_code") = l._Comp_Code
                                        row("companyunkid") = l._companyunkid
                                        If dtlink.Select("controlname = '" & l._controlunqid & "' ").Length <= 0 Then
                                            dtlink.Rows.Add(row)
                                        End If
                                        Dim properties As PropertyInfo() = ctrl.[GetType]().GetProperties()

                                        For Each pi As PropertyInfo In properties
                                            Dim intLastIdx As Integer = ctrl.UniqueID.Length - 1
                                            If ctrl.UniqueID.Contains("$") = True Then intLastIdx = ctrl.UniqueID.ToString().LastIndexOf("$"c)
                                            'Dim st1 As String = ctrl.UniqueID.Substring(0, ctrl.UniqueID.ToString().LastIndexOf("$"c))
                                            Dim st1 As String = ctrl.UniqueID.Substring(0, intLastIdx)

                                            If pi.Name = "Text" OrElse pi.Name = "Title" OrElse pi.Name = "HeaderText" OrElse pi.Name = "ErrorMessage" Then
                                                itext = ""

                                                If pi.GetValue(enumerator.Current).ToString().ToUpper().StartsWith("OBJ") <> True Then

                                                    If ctrl.UniqueID = l._controlunqid OrElse ctrl.ID = l._controlunqid OrElse (If(ctrl.[GetType]().Name.ToString().ToUpper() = "DATACONTROLFIELDHEADERCELL", (CType(ctrl, DataControlFieldCell)).ContainingField.FooterText = l._controlunqid, False)) Then
                                                        Dim wlg As clsWebLanguage = list.Find(Function(x) x._controlunqid = (CType(enumerator.Current, Control)).ID OrElse x._controlunqid = (CType(enumerator.Current, Control)).UniqueID OrElse (If((CType(enumerator.Current, Control)).[GetType]().Name.ToString().ToUpper() = "DATACONTROLFIELDHEADERCELL", x._controlunqid = (CType((CType(enumerator.Current, Control)), DataControlFieldCell)).ContainingField.FooterText, False)))

                                                        If wlg IsNot Nothing Then

                                                            If wlg._language = l._language Then

                                                                Select Case langid
                                                                    Case 1
                                                                        'itext = l._language1.ToString().Replace("&", "")
                                                                        itext = AntiXss.AntiXssEncoder.HtmlEncode(l._language1.ToString().Replace("&", ""), True)
                                                                    Case 2
                                                                        'itext = l._language2.ToString().Replace("&", "")
                                                                        itext = AntiXss.AntiXssEncoder.HtmlEncode(l._language2.ToString().Replace("&", ""), True)
                                                                    Case Else
                                                                        'itext = l._language.ToString().Replace("&", "")
                                                                        itext = AntiXss.AntiXssEncoder.HtmlEncode(l._language.ToString().Replace("&", ""), True)
                                                                End Select
                                                            End If
                                                        End If
                                                    ElseIf st1.ToString() = l._parent Then
                                                        Dim ls As clsWebLanguage = list.Find(Function(x) x._formname = _ModuleName AndAlso x._language = pi.GetValue(enumerator.Current).ToString() AndAlso x._parent = st1)

                                                        If ls IsNot Nothing Then
                                                            wLang._formname = _ModuleName
                                                            wLang._controlunqid = l._controlunqid
                                                            wLang._language = ls._language
                                                            wLang._language1 = ls._language1
                                                            wLang._language2 = ls._language2
                                                            wLang._languageunkid = l._languageunkid
                                                            'wLang.InsertUpdateDelete(Session("mdbname").ToString())
                                                            wLang.InsertUpdateDelete(strCompCode:=Session("CompCode").ToString, intCompanyUnkId:=CInt(Session("companyunkid")))

                                                            Select Case langid
                                                                Case 1
                                                                    itext = AntiXss.AntiXssEncoder.HtmlEncode(l._language1, True)
                                                                Case 2
                                                                    itext = AntiXss.AntiXssEncoder.HtmlEncode(l._language2, True)
                                                                Case Else
                                                                    itext = AntiXss.AntiXssEncoder.HtmlEncode(l._language, True)
                                                            End Select
                                                        End If
                                                    End If

                                                    If ctrl.[GetType]().Name.ToString().ToUpper() = "DATACONTROLFIELDHEADERCELL" Then
                                                        CType((CType(enumerator.Current, Control)), DataControlFieldCell).ContainingField.HeaderText = itext
                                                        pi.SetValue(enumerator.Current, itext)
                                                    Else
                                                        pi.SetValue(enumerator.Current, itext)
                                                    End If
                                                End If

                                                Exit For
                                            End If
                                        Next
                                    End If
                                End If
                            End While
                        End If
                    End If

                Next
            Next
        End If

        Dim j As Integer = 0

        For Each ctr As Control In AllFormsControl
            j += 1
            Dim objctrid As Object = Nothing

            If ctr.ID IsNot Nothing Then
                objctrid = ctr.ID
            ElseIf ctr.UniqueID IsNot Nothing Then
                objctrid = ctr.UniqueID
            End If

            If objctrid IsNot Nothing Then

                If ctr.[GetType]().Name.ToString().ToUpper() = "RADIOBUTTONLIST" OrElse ctr.[GetType]().Name.ToString().ToUpper() = "DROPDOWNLIST" OrElse ctr.[GetType]().Name.ToString().ToUpper() = "TEXTBOX" OrElse ctr.[GetType]().Name.ToString().ToUpper() = "LITERALCONTROL" OrElse ctr.[GetType]().Name.ToString().ToUpper() = "UPDATEPANEL" OrElse ctr.[GetType]().Name.ToString().ToUpper() = "CONTROLS_LANGUAGEMESSAGECONTROL_ASCX" OrElse ctr.[GetType]().Name.ToString().ToUpper() = "CALENDAREXTENDER" OrElse ctr.[GetType]().Name.ToString().ToUpper() = "CONTROLS_DATECTRL_ASCX" Then 'OrElse ctr.[GetType]().Name.ToUpper() = "DATACONTROLFIELDCELL"
                    Continue For
                End If

                If objctrid.ToString().ToUpper() = "SIDEBARBUTTON" OrElse objctrid.ToString().ToUpper().StartsWith("OBJ") = True Then
                    Continue For
                End If

                If ctr.[GetType]().Name.ToString().ToUpper() = "DATACONTROLFIELDHEADERCELL" Then
                    objctrid = (CType(ctr, DataControlFieldCell)).ContainingField.FooterText
                End If

                If ctr.[GetType]().Name.ToString().ToUpper() = "DATACONTROLFIELDCELL" Then
                    Continue For
                End If

                Dim row As DataRow = dtlink.NewRow()
                row("langunkid") = -1
                row("formname") = _ModuleName
                row("controlname") = objctrid
                row("Id") = objctrid
                row("Type") = ctr.[GetType]().Name
                row("comp_code") = Session("CompCode").ToString
                row("companyunkid") = CInt(Session("companyunkid"))
                Try
                    Dim properties As PropertyInfo() = ctr.[GetType]().GetProperties()

                    For Each pi As PropertyInfo In properties

                        If pi.Name = "Text" OrElse pi.Name = "Title" OrElse pi.Name = "HeaderText" OrElse pi.Name = "ErrorMessage" Then

                            If pi.GetValue(ctr).ToString().ToUpper().StartsWith("OBJ") <> True Then
                                row("language") = pi.GetValue(ctr)
                                row("language1") = pi.GetValue(ctr)
                                row("language2") = pi.GetValue(ctr)
                            End If

                            row("istext") = True
                            Exit For
                        End If
                    Next

                Catch
                End Try

                Dim drtmp As DataRow() = Nothing
                drtmp = dtlink.[Select]("Id = '" & objctrid & "'")

                If drtmp.Length <= 0 Then
                    drtmp = dtlink.[Select]("controlname = '" & ctr.UniqueID & "'")
                End If

                If drtmp.Length <= 0 Then
                    drtmp = dtlink.[Select]("controlname = '" & objctrid & "'")
                End If

                If drtmp.Length <= 0 Then
                    dtlink.Rows.Add(row)
                End If
            End If
        Next

        objgrd_control.DataSource = Nothing
        objgrd_control.DataBind()
        Dim dataView As DataView = dtlink.DefaultView
        dataView.RowFilter = "istext = 'true' AND language <> '' AND language <> '&nbsp;' AND Type <> 'LiteralControl'"
        dataView.Sort = "langunkid, language"
        dtlink = dataView.ToTable()
        objgrd_control.DataSource = dtlink
        objgrd_control.DataBind()
    End Sub

    Private Controlflag As Boolean = False
    Private Controlflag2 As Boolean = False

    Public Sub InitContolList(ByVal nControl As Control)
        If nControl.Controls.Count > 0 Then

            For Each item As Control In nControl.Controls

                If Controlflag AndAlso Controlflag2 Then
                    Exit For
                ElseIf Controlflag = False OrElse Controlflag2 = False Then
                    InitContolList(item)

                    If _TargetControl2 = Nothing OrElse _TargetControl2.Trim = "" Then
                        Controlflag2 = True
                    End If

                    If item.ID = _TargetControl Then
                        Controlflag = True
                        getdatafromControl(item)
                    End If
                    If item.ID = _TargetControl2 AndAlso Controlflag2 = False Then
                        Controlflag2 = True
                        getdatafromControl(item)
                    End If
                End If
            Next
        End If
    End Sub

    Public Sub getdatafromControl(ByVal control As Control)
        If control.Controls.Count > 0 Then

            For Each item As Control In control.Controls

                'If item.[GetType]().Name.ToString().ToUpper() = "TABLECELL" OrElse item.[GetType]().Name.ToString().ToUpper() = "DATACONTROLFIELDCELL" OrElse item.[GetType]().Name.ToString().ToUpper() = "LITERALCONTROL" OrElse item.[GetType]().Name.ToString().ToUpper() = "RADIOBUTTONLIST" OrElse item.[GetType]().Name.ToString().ToUpper() = "DROPDOWNLIST" OrElse item.[GetType]().Name.ToString().ToUpper() = "TEXTBOX" OrElse item.[GetType]().Name.ToString().ToUpper() = "LITERALITEM" OrElse item.[GetType]().Name.ToString().ToUpper() = "UPDATEPANEL" OrElse item.[GetType]().Name.ToString().ToUpper() = "ITEMS_LANGUAGEMESSAGEITEM_ASCX" OrElse item.[GetType]().Name.ToString().ToUpper() = "CALENDAREXTENDER" OrElse item.[GetType]().Name.ToString().ToUpper() = "ITEMS_DATECTRL_ASCX" Then
                'If item.[GetType]().Name.ToString().ToUpper() = "TABLECELL" OrElse item.[GetType]().Name.ToString().ToUpper() = "DATACONTROLFIELDCELL" OrElse item.[GetType]().Name.ToString().ToUpper() = "LITERALCONTROL" OrElse item.[GetType]().Name.ToString().ToUpper() = "RADIOBUTTONLIST" OrElse item.[GetType]().Name.ToString().ToUpper() = "DROPDOWNLIST" OrElse item.[GetType]().Name.ToString().ToUpper() = "TEXTBOX" OrElse item.[GetType]().Name.ToString().ToUpper() = "LITERALITEM" OrElse item.[GetType]().Name.ToString().ToUpper() = "ITEMS_LANGUAGEMESSAGEITEM_ASCX" OrElse item.[GetType]().Name.ToString().ToUpper() = "CALENDAREXTENDER" OrElse item.[GetType]().Name.ToString().ToUpper() = "ITEMS_DATECTRL_ASCX" Then
                If item.[GetType]().Name.ToString().ToUpper() = "TABLECELL" OrElse item.[GetType]().Name.ToString().ToUpper() = "LITERALCONTROL" OrElse item.[GetType]().Name.ToString().ToUpper() = "RADIOBUTTONLIST" OrElse item.[GetType]().Name.ToString().ToUpper() = "DROPDOWNLIST" OrElse item.[GetType]().Name.ToString().ToUpper() = "TEXTBOX" OrElse item.[GetType]().Name.ToString().ToUpper() = "LITERALITEM" OrElse item.[GetType]().Name.ToString().ToUpper() = "ITEMS_LANGUAGEMESSAGEITEM_ASCX" OrElse item.[GetType]().Name.ToString().ToUpper() = "CALENDAREXTENDER" OrElse item.[GetType]().Name.ToString().ToUpper() = "ITEMS_DATECTRL_ASCX" Then
                    Continue For
                Else
                    getdatafromControl(item)
                    Dim properties As PropertyInfo() = item.[GetType]().GetProperties()

                    For Each pi As PropertyInfo In properties

                        If pi.Name = "Text" OrElse pi.Name = "Title" OrElse pi.Name = "HeaderText" OrElse pi.Name = "ErrorMessage" Then

                            If Not String.IsNullOrEmpty(pi.GetValue(item).ToString()) Then
                                AllFormsControl.Add(item)
                            End If
                        End If
                    Next
                End If
            Next
        End If
    End Sub

    Public Sub InitContolList(ByVal nControl As ContentPlaceHolder)
        If nControl.Controls.Count > 0 Then

            For Each item As Control In nControl.Controls
                InitContolList(item)
                AllFormsControl.Add(item)
            Next
        End If
    End Sub

    Public Sub InitContolParentList(ByVal nControl As String)
        Dim ct As Control = _webPage.FindControl(nControl)
        Dim i As Integer = 1

        If flag = True Then
            ctrName = ct.UniqueID
            flag = False
            i = 0
        End If

        If ct IsNot Nothing Then

            If AllFormsParentIds.ContainsKey(ctrName) = True Then

                If ct.Parent IsNot Nothing Then
                    AllFormsParentIds(ctrName) += "," & ct.Parent.UniqueID
                End If
            Else

                If i = 0 Then
                Else
                    AllFormsParentIds.Add(ctrName, ct.Parent.UniqueID)
                End If
            End If

            If ct.Parent IsNot Nothing Then
                InitContolParentList(ct.Parent.UniqueID.ToString())
            End If
        End If
    End Sub

    Private Sub ChangeMessages(ByVal sender As Object)
        Try
            Dim txt As TextBox = TryCast(sender, TextBox)
            Dim row As GridViewRow = TryCast(txt.NamingContainer, GridViewRow)
            Dim grid As GridView = TryCast(row.NamingContainer, GridView)

            If grid.ID.ToString().ToUpper() = objgrd_fMessages.ID.ToUpper().ToString() Then
                wMsgs._message = (CType(row.FindControl("lblfMessage1"), TextBox)).Text
                wMsgs._message1 = (CType(row.FindControl("lblfMessage2"), TextBox)).Text
                wMsgs._message2 = (CType(row.FindControl("lblfMessage3"), TextBox)).Text
                wMsgs._messagecode = (CType(row.FindControl("hffmsgcode"), HiddenField)).Value.ToString()
                wMsgs._messageunkid = Convert.ToInt32((CType(row.FindControl("hffmsgid"), HiddenField)).Value)
                wMsgs._module_name = (CType(row.FindControl("hffmodulename"), HiddenField)).Value.ToString()
            ElseIf grid.ID.ToString().ToUpper() = objgrd_oMessages.ID.ToUpper().ToString() Then
                wMsgs._message = (CType(row.FindControl("lbloMessage1"), TextBox)).Text
                wMsgs._message1 = (CType(row.FindControl("lbloMessage2"), TextBox)).Text
                wMsgs._message2 = (CType(row.FindControl("lbloMessage3"), TextBox)).Text
                wMsgs._messagecode = (CType(row.FindControl("hfomsgcode"), HiddenField)).Value.ToString()
                wMsgs._messageunkid = Convert.ToInt32((CType(row.FindControl("hfomsgid"), HiddenField)).Value)
                wMsgs._module_name = (CType(row.FindControl("hfomodulename"), HiddenField)).Value.ToString()
            End If

            'wMsgs.InsertUpdateDelete(Session("mdbname").ToString())
            wMsgs.InsertUpdateDelete(strCompCode:=Session("CompCode").ToString, intCompanyUnkId:=CInt(Session("companyunkid")))

            If grid.ID.ToString().ToUpper() = objgrd_fMessages.ID.ToUpper().ToString() Then
                SetFormMessages()
            ElseIf grid.ID.ToString().ToUpper() = objgrd_oMessages.ID.ToUpper().ToString() Then
                SetClassMessages()
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
        End Try
    End Sub

    Private Sub ChangeLanguage(ByVal sender As Object)
        Try
            Dim txt As TextBox = TryCast(sender, TextBox)
            Dim row As GridViewRow = TryCast(txt.NamingContainer, GridViewRow)

            If txt.Text <> "" Then
                Dim hfctrid As HiddenField = CType(row.FindControl("hfctrid"), HiddenField)
                Dim hflangid As HiddenField = CType(row.FindControl("hflangid"), HiddenField)
                Dim lblLanguage1 As TextBox = CType(row.FindControl("objlblLanguage1"), TextBox)
                Dim lblLanguage2 As TextBox = CType(row.FindControl("objlblLanguage2"), TextBox)
                Dim lblLanguage3 As TextBox = CType(row.FindControl("objlblLanguage3"), TextBox)
                wLang._formname = _ModuleName
                wLang._controlunqid = hfctrid.Value
                wLang._language = lblLanguage1.Text
                wLang._language1 = lblLanguage2.Text
                wLang._language2 = lblLanguage3.Text
                Dim intLangId As Integer = 0
                Integer.TryParse(hflangid.Value, intLangId)
                'wLang._languageunkid = CInt(hflangid.Value)
                wLang._languageunkid = intLangId
                'wLang.InsertUpdateDelete(Session("mdbname").ToString())
                wLang.InsertUpdateDelete(strCompCode:=Session("CompCode").ToString, intCompanyUnkId:=CInt(Session("companyunkid")))
            End If

            SetControlLanguage()
            popuplanguage.Show()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
        End Try
    End Sub

    Public Sub show()
        Try
            popuplanguage.Show()
            blnShowpopup = True
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region

#Region " TextChange Events "
    Protected Sub lblLanguage2_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        ChangeLanguage(sender)
    End Sub

    Protected Sub lblLanguage3_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        ChangeLanguage(sender)
    End Sub

    Protected Sub lblMessage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        ChangeMessages(sender)
    End Sub

#End Region

#Region " Button Events "
    Protected Sub lnklanguageclose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnklanguageclose.Click
        blnShowpopup = False
        popuplanguage.Hide()
        'pnllanguage.Visible = False
    End Sub

    Protected Sub objlnkLanguage_Click(ByVal sender As Object, ByVal e As EventArgs) Handles objlnkLanguage.Click
        Try
            show()
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub
#End Region
End Class