﻿Public Class DeleteReason
    Inherits System.Web.UI.UserControl

    Public Event buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event buttonDelReasonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    'S.SANDEEP [29 JAN 2015] -- START
    Private sCancelControlID As String = "btnDelReasonNo"
    Public Property CancelControlName() As String
        Get
            Return sCancelControlID
        End Get
        Set(ByVal value As String)
            sCancelControlID = value
            ModalPopupExtender1.CancelControlID = sCancelControlID
        End Set
    End Property
    'S.SANDEEP [29 JAN 2015] -- END

    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            lblTitle.Text = value
        End Set
    End Property

    Public Property Reason() As String
        Get
            Return txtReason.Text
        End Get
        Set(ByVal value As String)
            txtReason.Text = value
        End Set
    End Property

    Public Property ErrorMessage() As String
        Get
            Return RequiredFieldValidator1.ErrorMessage
        End Get
        Set(ByVal value As String)
            RequiredFieldValidator1.ErrorMessage = value
        End Set
    End Property

    Public Property ErrorControlWidth() As Unit
        Get
            Return PNReqE.Width
        End Get
        Set(ByVal value As Unit)
            PNReqE.Width = value
        End Set
    End Property

    Public Property ValidationGroup() As String
        Get
            Return txtReason.ValidationGroup
        End Get
        Set(ByVal value As String)
            txtReason.ValidationGroup = value
            RequiredFieldValidator1.ValidationGroup = value
            btnDelReasonYes.ValidationGroup = value
        End Set
    End Property

    Protected Sub btnDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelReasonYes.Click
        RaiseEvent buttonDelReasonYes_Click(sender, e)
    End Sub

    Protected Sub btnDelReasonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelReasonNo.Click
        RaiseEvent buttonDelReasonNo_Click(sender, e)
    End Sub

    Public Sub Show()
        ModalPopupExtender1.Show()
    End Sub

    Public Sub Hide()
        ModalPopupExtender1.Hide()
    End Sub

    'S.SANDEEP [29 JAN 2015] -- START
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ModalPopupExtender1.CancelControlID = sCancelControlID
    End Sub
    'S.SANDEEP [29 JAN 2015] -- END

End Class