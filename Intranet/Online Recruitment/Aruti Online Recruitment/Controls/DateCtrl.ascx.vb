﻿Imports System.Globalization

Public Class DateCtrl
    Inherits System.Web.UI.UserControl

    Public Event objbtnDateReminder_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    Public Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    'Private pMaxDate As Date
    'Public Property MaxDate() As Date
    '    Get
    '        Return pMaxDate
    '    End Get
    '    Set(ByVal value As Date)
    '        pMaxDate = value
    '    End Set
    'End Property

    'Private pMinDate As Date
    'Public Property MinDate() As Date
    '    Get
    '        Return pMinDate
    '    End Get
    '    Set(ByVal value As Date)
    '        pMinDate = value
    '    End Set
    'End Property

    'Public WriteOnly Property SetDate() As String
    '    Set(ByVal value As String)
    '        TxtDate.Text = IIf(IsDate(value), value, CDate("01/01/1900"))
    '    End Set
    'End Property

    Public WriteOnly Property SetDate() As Date
        Set(ByVal value As Date)

            'If (value = Nothing) Then
            '    TxtDate.Text = ""
            'Else
            '    TxtDate.Text = IIf(IsDate(value), value, CDate("01/01/1900"))
            'End If

            'Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))
            'System.Threading.Thread.CurrentThread.CurrentUICulture = culture
            'System.Threading.Thread.CurrentThread.CurrentCulture = culture
            'calendarButtonExtender.Format = culture.DateTimeFormat.ShortDatePattern.ToString

            Dim NewCulture As CultureInfo = CType(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
            If Session("DateFormat") IsNot Nothing Then
                NewCulture.DateTimeFormat.ShortDatePattern = Session("DateFormat")
                NewCulture.DateTimeFormat.DateSeparator = Session("DateSeparator")
                System.Threading.Thread.CurrentThread.CurrentCulture = NewCulture
                calendarButtonExtender.Format = Session("DateFormat")
            End If

            If (value = Nothing) Then
                TxtDate.Text = ""
            Else
                TxtDate.Text = IIf(IsDate(value), value.ToString(calendarButtonExtender.Format), CDate("01/01/1900"))
            End If
        End Set
    End Property

    Public ReadOnly Property GetDate() As Date
        Get
            Try
                'Return IIf(IsDate(TxtDate.Text), TxtDate.Text, CDate("01/01/1900"))
                'Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))
                'System.Threading.Thread.CurrentThread.CurrentUICulture = culture
                'System.Threading.Thread.CurrentThread.CurrentCulture = culture
                Dim NewCulture As CultureInfo = CType(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
                If Session("DateFormat") Is Nothing Then Session("DateFormat") = "dd-MMM-yyyy" 'Sohail (04 Oct 2019)
                If Session("DateSeparator") Is Nothing Then Session("DateSeparator") = "-" 'Sohail (04 Oct 2019)
                NewCulture.DateTimeFormat.ShortDatePattern = Session("DateFormat")
                NewCulture.DateTimeFormat.DateSeparator = Session("DateSeparator")
                System.Threading.Thread.CurrentThread.CurrentCulture = NewCulture
                calendarButtonExtender.Format = Session("DateFormat")

                'If TxtDate.Text.Trim.Length > 0 Then
                '    If IsNull() = False Then
                '        Return DateTime.ParseExact(TxtDate.Text, calendarButtonExtender.Format, CultureInfo.InvariantCulture)
                '    Else
                '        Return DateTime.ParseExact("01/01/1900", calendarButtonExtender.Format, CultureInfo.InvariantCulture)
                '    End If
                'Else
                '    Return DateTime.ParseExact("01/01/1900", calendarButtonExtender.Format, CultureInfo.InvariantCulture)
                'End If
                If TxtDate.Text.Trim.Length > 0 Then
                    If IsNull() = False Then
                        Return DateTime.ParseExact(TxtDate.Text, calendarButtonExtender.Format, CultureInfo.InvariantCulture)
                    Else
                        Return DateTime.ParseExact(CDate("01/01/1900"), calendarButtonExtender.Format, CultureInfo.InvariantCulture)
                    End If
                Else
                    Return DateTime.ParseExact(CDate("01/01/1900"), calendarButtonExtender.Format, CultureInfo.InvariantCulture)
                End If
            Catch ex As Exception
                Global_asax.CatchException(ex, Context)
                'Return DateTime.ParseExact("01/01/1900", calendarButtonExtender.Format, CultureInfo.InvariantCulture)
                Return DateTime.ParseExact(CDate("01/01/1900"), calendarButtonExtender.Format, CultureInfo.InvariantCulture) 'Sohail (04 Oct 2019)
            End Try
        End Get
    End Property

    Public ReadOnly Property IsNull() As Boolean
        Get
            Try
                'Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))
                'System.Threading.Thread.CurrentThread.CurrentUICulture = culture
                'System.Threading.Thread.CurrentThread.CurrentCulture = culture

                Dim dtDate As Date = DateTime.ParseExact(TxtDate.Text, calendarButtonExtender.Format, CultureInfo.InvariantCulture)
                If IsDate(dtDate) = False Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                'Global_asax.CatchException(ex, Context)
                'Return False
                TxtDate.Text = ""
                Return True
            End Try
        End Get
    End Property

    Public Property Enabled() As Boolean
        Get
            Return TxtDate.Enabled
        End Get
        Set(ByVal value As Boolean)
            TxtDate.Enabled = value
            Image1.Enabled = value
        End Set
    End Property

    Public Property ReadonlyDate() As Boolean
        Get
            Return TxtDate.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            TxtDate.ReadOnly = value
            Image1.Enabled = Not value
        End Set
    End Property

    Public Property Width() As Integer
        Get
            Return TxtDate.Width.Value
        End Get
        Set(ByVal value As Integer)
            TxtDate.Width = value
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return TxtDate.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            TxtDate.AutoPostBack = value
        End Set
    End Property


    Public WriteOnly Property ReminderVisible() As Boolean
        Set(ByVal value As Boolean)
            objbtnDateReminder.Visible = value
        End Set
    End Property

    Protected Sub TxtDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtDate.TextChanged
        RaiseEvent TextChanged(sender, e)
    End Sub


    Public Sub BindServerDateTime()
        Try
            Dim msql As String
            Dim ds As DataSet = Nothing
            msql = "select Getdate() as ServerDt "
            'ds = clsdataopr.WExecQuery(msql, "in")
            TxtDate.Text = ds.Tables(0).Rows(0)("ServerDt").ToString
            Me.SysDatetime = ds.Tables(0).Rows(0)("ServerDt").ToString
        Catch ex As Exception
            Throw New Exception("DateCtrl-->BindServerDateTime Method!!!" & ex.Message.ToString)
        Finally
        End Try
    End Sub
    Private _pSysdate As Date
    Public Property SysDatetime() As Date
        Get
            Return _pSysdate
        End Get
        Set(ByVal value As Date)
            _pSysdate = value
        End Set
    End Property

    'Public Property ChkVisible() As Boolean
    '    Get
    '        Return ChkInclude.Visible
    '    End Get
    '    Set(ByVal value As Boolean)
    '        ChkInclude.Visible = value
    '    End Set
    'End Property

    'Public ReadOnly Property ChkInclude_Checked() As Boolean
    '    Get
    '        If (ChkInclude.Checked) Then
    '            Return True
    '        ElseIf (ChkInclude.Checked = False) Then
    '            Return False
    '        End If
    '    End Get
    'End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Sohail (05 Jul 2011) -- Start
            'calendarButtonExtender.Format = CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern.ToString
            'calendarButtonExtender.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToString
            'Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))
            'System.Threading.Thread.CurrentThread.CurrentUICulture = culture
            'System.Threading.Thread.CurrentThread.CurrentCulture = culture
            'calendarButtonExtender.Format = culture.DateTimeFormat.ShortDatePattern.ToString
            'Sohail (05 Jul 2011) -- End
            calendarButtonExtender.Format = Session("DateFormat")
        End If

    End Sub


    Protected Sub objbtnDteReminder_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles objbtnDateReminder.Click
        RaiseEvent objbtnDateReminder_Click(sender, e)
    End Sub

End Class