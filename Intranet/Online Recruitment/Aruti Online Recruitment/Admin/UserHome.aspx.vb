﻿Public Class UserHome2
    Inherits Base_Page

#Region " Method Functions "

    Private Sub FillCombo()

        Try


        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        Finally

        End Try
    End Sub

    Private Sub FillList()

        Try

        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
    End Sub

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                'Call FillCombo()
                If Request.Browser.IsMobileDevice = True Then
                    dlDashboard.RepeatColumns = 1
                Else
                    dlDashboard.RepeatColumns = 3
                End If
                odsDashboard.SelectParameters("strComp_Code").DefaultValue = Session("CompCode").ToString
                odsDashboard.SelectParameters("intCompanyunkid").DefaultValue = Session("companyunkid").ToString
                'Call FillList()
                Timer1.Enabled = False
            End If
        Catch ex As Exception
            'Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
    End Sub

#End Region

#Region " Combobox Events "


#End Region

#Region " DataList Events "

    Private Sub dlDashboard_ItemDataBound(sender As Object, e As DataListItemEventArgs) Handles dlDashboard.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
                If CInt(drv.Item("Id")) <> 5 Then
                    'CType(e.Item.FindControl("lblCount"), Label).ForeColor = Drawing.Color.Black
                    CType(e.Item.FindControl("lblCount"), Label).CssClass = "text-black"
                Else
                    'CType(e.Item.FindControl("pnlInfoBox"), Panel).Style.Add("background-color", "red")
                    'CType(e.Item.FindControl("lblCount"), Label).ForeColor = Drawing.Color.Red
                    CType(e.Item.FindControl("pnlInfoBox"), Panel).CssClass = "bg-red"
                    CType(e.Item.FindControl("lblCount"), Label).CssClass = "text-red"
                End If
            End If
        Catch ex As Exception
            'Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
    End Sub

#End Region

#Region " Timer Events "

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Try
            dlDashboard.DataBind()
        Catch ex As Exception
            'Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
    End Sub

    Private Sub lnkStart_Click(sender As Object, e As EventArgs) Handles lnkStart.Click
        Try
            Timer1.Enabled = True
            lnkStart.Visible = False
            lnkStop.Visible = True
        Catch ex As Exception

        End Try
    End Sub

    Private Sub lnkStop_Click(sender As Object, e As EventArgs) Handles lnkStop.Click
        Try
            Timer1.Enabled = False
            lnkStop.Visible = False
            lnkStart.Visible = True
        Catch ex As Exception

        End Try
    End Sub

#End Region

End Class