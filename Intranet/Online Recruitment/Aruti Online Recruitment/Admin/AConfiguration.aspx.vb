﻿Public Class AConfiguration
    Inherits Base_Page


#Region "Variable Declarations"

    Private objCompanySettings As New clsCompany

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
                GetValue()
            End If
            If CBool(Session("isvacancyalert")) = False OrElse 1 = 1 Then
                Response.Redirect("~/Admin/UserHome.aspx", False)
                Exit Sub
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub ApplicantQualification_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub GetValue()
        Dim dsList As DataSet = Nothing
        Try
            dsList = objCompanySettings.GetCompany(strCompCode:=Session("CompCode").ToString, intComUnkID:=CInt(Session("companyunkid")), blnOnlyActive:=True)

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                chkVacancyalert.Checked = Convert.ToBoolean(dsList.Tables(0).Rows(0)("isvacancyAlert"))
                txtMaxAlert.Text = Convert.ToInt32(dsList.Tables(0).Rows(0)("maxvacancyalert")).ToString()
            End If
            chkVacancyalert_CheckedChanged(New Object(), New EventArgs())
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            dsList.Clear()
            dsList = Nothing
        End Try
    End Sub


#End Region

#Region " Checkbox Events "

    Private Sub chkVacancyalert_CheckedChanged(sender As Object, e As EventArgs) Handles chkVacancyalert.CheckedChanged
        Try
            If chkVacancyalert.Checked = True Then
                txtMaxAlert.Attributes.Add("max", "10")
                txtMaxAlert.Attributes.Add("min", "1")
                If (txtMaxAlert.Text.Length <= 0 OrElse Convert.ToInt32(txtMaxAlert.Text) <= 0) Then txtMaxAlert.Text = "1"
            Else
                txtMaxAlert.Attributes.Add("max", "0")
                txtMaxAlert.Attributes.Add("min", "0")
                txtMaxAlert.Text = "0"
            End If
            txtMaxAlert.Enabled = chkVacancyalert.Checked
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region "Textbox Event"

    Private Sub txtMaxAlert_TextChanged(sender As Object, e As EventArgs) Handles txtMaxAlert.TextChanged
        Try
            If txtMaxAlert.Enabled AndAlso txtMaxAlert.Text.Trim = "" Then
                txtMaxAlert.Text = "1"
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

#Region "Button Events"

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            '** To Prevent Buttun Click when Page is Refreshed by User. **
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            Dim mintVacaryAlert As Integer = 0

            If objCompanySettings.EditCompanySettings(strCompCode:=Session("CompCode").ToString _
                             , intComUnkID:=CInt(Session("companyunkid")) _
                             , mblnVacancyAlert:=chkVacancyalert.Checked _
                             , xMaxVacancyAlert:=IIf(txtMaxAlert.Text.Length <= 0, "0", Convert.ToInt32(txtMaxAlert.Text))) = True Then

                ShowMessage("Company Settings Saved Successfully.", MessageType.Info)
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

#End Region

End Class