﻿Option Strict On

Public Class ASendResetPwd
    Inherits Base_Page

#Region " Methods Functions "

    Private Sub FillList()
        'Dim objApplicant As New clsApplicant
        'Dim dsList As DataSet
        Try
            'dsList = objApplicant.GetResetPasswordLink(strCompCode:=Session("CompCode").ToString _
            '                                            , intComUnkID:=CInt(Session("companyunkid"))
            '                                            )

            ''grdLink.Columns(0).Visible = True
            ''grdLink.Columns(1).Visible = True
            'grdLink.Columns(2).Visible = True
            'grdLink.Columns(3).Visible = True
            'grdLink.DataSource = dsList.Tables(0)
            'grdLink.DataBind()
            ''grdLink.Columns(0).Visible = False
            ''grdLink.Columns(1).Visible = False
            'grdLink.Columns(2).Visible = False
            'grdLink.Columns(3).Visible = False

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            'objApplicant = Nothing
        End Try
    End Sub

#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                'Call FillList()
            End If

        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub

    Private Sub ASendResetPwd_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        End Try
    End Sub


#End Region

#Region " Datagridview Events "
    Private Sub grdLink_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdLink.RowCommand
        Dim objApplicant As New clsApplicant
        Dim objMail As New clsMail
        Try
            If e.CommandName = "Send" Then

                'Dim strCompCode As String = grdLink.Rows(CInt(e.CommandArgument)).Cells(0).Text
                'Dim intCompanyunkid As Integer = CInt(grdLink.Rows(CInt(e.CommandArgument)).Cells(1).Text)
                Dim strCompCode As String = grdLink.DataKeys(CInt(e.CommandArgument)).Item("Comp_Code").ToString
                Dim intCompanyunkid As Integer = CInt(grdLink.DataKeys(CInt(e.CommandArgument)).Item("companyunkid").ToString)

                If intCompanyunkid > 0 Then

                    'If objApplicant.GetCompanyInfo(intComUnkID:=intCompanyunkid _
                    '                           , strCompCode:=strCompCode _
                    '                           , ByRefblnIsActive:=False
                    '                           ) Then

                    If objMail.SendEmail(strToEmail:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("LoweredEmail").ToString()) _
                                         , strSubject:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("subject").ToString()) _
                                         , strBody:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("message").ToString()).Replace("&", "&amp;")
                                         ) Then


                        ShowMessage("Mail Sent successfully!")
                    Else
                        ShowMessage("Mail sending failed!", MessageType.Errorr)
                        Exit Try
                    End If

                    'Else

                    'End If

                Else
                    Dim objSA As New clsSACommon
                    If objSA.GetSuperAdminEmailSettings() Then

                        If objMail.SendEmail(strToEmail:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("LoweredEmail").ToString()) _
                                         , strSubject:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("subject").ToString()) _
                                         , strBody:=Server.HtmlDecode(grdLink.DataKeys(CInt(e.CommandArgument)).Item("message").ToString()) _
                                         , blnAddLogo:=False _
                                         , blnSuperAdmin:=True
                                         ) Then


                            ShowMessage("Mail Sent successfully!")
                        Else
                            ShowMessage("Mail sending failed!", MessageType.Errorr)
                            Exit Try
                        End If

                    Else

                    End If

                End If


            End If


        Catch ex As Exception
            Global_asax.CatchException(ex, Context)
        Finally
            objApplicant = Nothing
            FillList()
        End Try
    End Sub
#End Region

End Class