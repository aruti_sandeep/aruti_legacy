﻿Option Strict On
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class clsCompany

#Region " General Methods "

    Public Function GetCompanyForCombo(ByVal strCompCode As String _
                                      , intComUnkID As Integer _
                                      , blnOnlyActive As Boolean
                                      ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetCompanyForCombo"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@isactive", SqlDbType.Bit)).Value = blnOnlyActive

                        da.SelectCommand = cmd
                        da.Fill(ds, "Company")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return ds
    End Function

    Public Function GetCompany(ByVal strCompCode As String _
                              , intComUnkID As Integer _
                              , blnOnlyActive As Boolean
                              ) As DataSet
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetCompany"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@onlyactive", SqlDbType.Bit)).Value = blnOnlyActive

                        da.SelectCommand = cmd
                        da.Fill(ds, "Company")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return ds
    End Function

    <DataObjectMethod(DataObjectMethodType.Select, True)>
    Public Shared Function GetCompanies(ByVal strCompCode As String _
                                  , intComUnkID As Integer _
                                  , blnOnlyActive As Boolean
                                  ) As DataSet
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetCompany"

            If strCompCode Is Nothing Then strCompCode = ""

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode
                        cmd.Parameters.Add(New SqlParameter("@onlyactive", SqlDbType.Bit)).Value = blnOnlyActive

                        da.SelectCommand = cmd
                        da.Fill(ds, "Company")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return ds
    End Function

    Public Function GetCompanyCode(ByVal intComUnkID As Integer
                                      ) As String
        Dim ds As New DataSet
        Dim strCode As String = ""
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procGetCompanyCode"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    Dim rslt As Object = cmd.ExecuteScalar()

                    If rslt IsNot Nothing Then
                        strCode = rslt.ToString
                    End If

                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return strCode
    End Function

    'Sohail (27 Oct 2020) -- Start
    'NMB Enhancement # : Allow to access login page with simple URL and without encrypted text in URL.
    Public Function GetCompanyByVirtualDirectory(ByVal strVirtualDirectory As String
                                                ) As DataSet
        Dim ds As New DataSet
        Try

            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procGetCompanyByVirtualDirectory"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@virtual_directory", SqlDbType.NVarChar)).Value = strVirtualDirectory

                        da.SelectCommand = cmd
                        da.Fill(ds, "Company")

                    End Using
                End Using
            End Using
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
        End Try
        Return ds
    End Function
    'Sohail (27 Oct 2020) -- End

    Public Function IsExistCompanyInfo(ByVal strCompCode As String _
                                               , intComUnkID As Integer
                                                 ) As Boolean
        Dim blnIsExist As Boolean = False
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procIsExistCompanyInfo"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@company_code", SqlDbType.NVarChar)).Value = strCompCode

                    cmd.Parameters.Add(New SqlParameter("@result", SqlDbType.Bit)).Value = False
                    cmd.Parameters("@result").Direction = ParameterDirection.Output

                    cmd.ExecuteNonQuery()

                    blnIsExist = CBool(cmd.Parameters("@result").Value)

                End Using
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return blnIsExist
    End Function

    Public Function AddCompanyInfo(ByVal strCompCode As String,
                                          strCompanyName As String,
                                          strEmail As String,
                                          strDBName As String,
                                          intUserUnkId As Integer,
                                          dtCompanyAdded As Date,
                                          dtCompanyupdated As Date,
                                          strAuthenticationCode As String,
                                          strWesiteurlExternal As String,
                                          strWesiteurlInternal As String,
                                          blnIsactive As Boolean,
                                          strVirtualDirectory As String
                                           ) As Boolean
        'Sohail (27 Oct 2020) - [strVirtualDirectory]
        Dim ds As New DataSet
        Try
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procAddCompanyInfo"

            Using con As New SqlConnection(strConn)
                con.Open()

                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@company_code", SqlDbType.NVarChar)).Value = strCompCode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@company_name", SqlDbType.NVarChar)).Value = strCompanyName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@database_name", SqlDbType.NVarChar)).Value = strDBName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@userunkid", SqlDbType.Int)).Value = intUserUnkId
                    cmd.Parameters.Add(New SqlParameter("@company_date_added", SqlDbType.Date)).Value = dtCompanyAdded
                    cmd.Parameters.Add(New SqlParameter("@company_last_updated", SqlDbType.Date)).Value = dtCompanyupdated
                    cmd.Parameters.Add(New SqlParameter("@authentication_code", SqlDbType.NVarChar)).Value = strAuthenticationCode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@websiteurl", SqlDbType.NVarChar)).Value = strWesiteurlExternal.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@websiteurlinternal", SqlDbType.NVarChar)).Value = strWesiteurlInternal.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@isactive", SqlDbType.Bit)).Value = blnIsactive
                    cmd.Parameters.Add(New SqlParameter("@virtual_directory", SqlDbType.NVarChar)).Value = strVirtualDirectory.Trim.Replace("'", "''") 'Sohail (27 Oct 2020)

                    Dim intUnkId As Integer = Convert.ToInt32(cmd.ExecuteScalar)

                    If intUnkId > 0 Then

                        strQ = "procUpdateAuthCode"

                        cmd.CommandText = strQ
                        cmd.CommandType = CommandType.StoredProcedure

                        cmd.Parameters.Clear()
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intUnkId
                        cmd.Parameters.Add(New SqlParameter("@company_code", SqlDbType.NVarChar)).Value = strCompCode.Trim.Replace("'", "''")
                        Dim strAuthCode As String = HttpUtility.UrlEncode(clsCrypto.Encrypt(intUnkId.ToString & "?" & strCompCode.Trim))
                        cmd.Parameters.Add(New SqlParameter("@authentication_code", SqlDbType.NVarChar)).Value = strAuthCode.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@websiteurl", SqlDbType.NVarChar)).Value = strWesiteurlExternal.Trim.Replace("'", "''") & "&cc=" & strAuthCode
                        cmd.Parameters.Add(New SqlParameter("@websiteurlinternal", SqlDbType.NVarChar)).Value = strWesiteurlInternal.Trim.Replace("'", "''") & "&cc=" & strAuthCode

                        cmd.ExecuteNonQuery()
                    End If

                End Using
            End Using

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    Public Function EditCompanyInfo(ByVal strCompCode As String,
                                          intComUnkID As Integer,
                                          strCompanyName As String,
                                          strEmail As String,
                                          strDBName As String,
                                          intUserUnkId As Integer,
                                          dtCompanyAdded As Date,
                                          dtCompanyupdated As Date,
                                          strAuthenticationCode As String,
                                          strWesiteurlExternal As String,
                                          strWesiteurlInternal As String,
                                          blnIsactive As Boolean,
                                          strVirtualDirectory As String
                                           ) As Boolean
        'Sohail (27 Oct 2020) - [strVirtualDirectory]
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procEditCompanyInfo"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@company_code", SqlDbType.NVarChar)).Value = strCompCode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@company_name", SqlDbType.NVarChar)).Value = strCompanyName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@email", SqlDbType.NVarChar)).Value = strEmail.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@database_name", SqlDbType.NVarChar)).Value = strDBName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@userunkid", SqlDbType.Int)).Value = intUserUnkId
                    cmd.Parameters.Add(New SqlParameter("@company_date_added", SqlDbType.Date)).Value = dtCompanyAdded
                    cmd.Parameters.Add(New SqlParameter("@company_last_updated", SqlDbType.Date)).Value = dtCompanyupdated
                    cmd.Parameters.Add(New SqlParameter("@authentication_code", SqlDbType.NVarChar)).Value = strAuthenticationCode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@websiteurl", SqlDbType.NVarChar)).Value = strWesiteurlExternal.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@websiteurlinternal", SqlDbType.NVarChar)).Value = strWesiteurlInternal.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@isactive", SqlDbType.Bit)).Value = blnIsactive
                    cmd.Parameters.Add(New SqlParameter("@virtual_directory", SqlDbType.NVarChar)).Value = strVirtualDirectory.Trim.Replace("'", "''") 'Sohail (27 Oct 2020)

                    cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    Public Function DeleteCompanyInfo(ByVal strCompCode As String,
                                             intComUnkID As Integer
                                            ) As Boolean
        Dim ds As New DataSet
        Try

            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            'Sohail (30 Nov 2017) -- End

            Dim strQ As String = "procDeleteCompanyInfo"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@company_code", SqlDbType.NVarChar)).Value = strCompCode
                    cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

    'Pinkal (01-Jan-2018) -- Start
    'Enhancement - Job seekers to get alerts if they already signed up account on the recruitment site.
    Public Function EditCompanySettings(ByVal strCompCode As String, intComUnkID As Integer _
                                                                 , mblnVacancyAlert As Boolean, xMaxVacancyAlert As Integer) As Boolean
        Dim ds As New DataSet
        Try
            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            Dim strQ As String = "procEditCompanySettings"
            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@company_code", SqlDbType.NVarChar)).Value = strCompCode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@isvacancyAlert", SqlDbType.Bit)).Value = mblnVacancyAlert
                    cmd.Parameters.Add(New SqlParameter("@maxvacancyalert", SqlDbType.Int)).Value = xMaxVacancyAlert
                    cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try
    End Function

    'Pinkal (28-Dec-2017) -- End

    'Sohail (02 Nov 2018) -- Start
    'Update Email Settings.
    Public Function EditCompanyEmailSettings(ByVal strCompCode As String,
                                             intComUnkID As Integer,
                                             strUserName As String,
                                             strPassword As String,
                                             strMailServerIP As String,
                                             intMailServerPort As Integer,
                                             blnIsLoginSSL As Boolean,
                                             intEmailType As Integer,
                                             strEWS_URL As String,
                                             strEWS_Domain As String,
                                             strSenderName As String
                                             ) As Boolean
        Dim ds As New DataSet
        Try

            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procEditEmailSettings"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@company_code", SqlDbType.NVarChar)).Value = strCompCode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@username", SqlDbType.NVarChar)).Value = strUserName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@password", SqlDbType.NVarChar)).Value = clsSecurity.Encrypt(strPassword.Trim.Replace("'", "''"), "ezee")
                    cmd.Parameters.Add(New SqlParameter("@mailserverip", SqlDbType.NVarChar)).Value = strMailServerIP.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@sendername", SqlDbType.NVarChar)).Value = strSenderName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@mailserverport", SqlDbType.Int)).Value = intMailServerPort
                    cmd.Parameters.Add(New SqlParameter("@isloginssl", SqlDbType.Bit)).Value = blnIsLoginSSL
                    cmd.Parameters.Add(New SqlParameter("@email_type", SqlDbType.Int)).Value = intEmailType
                    cmd.Parameters.Add(New SqlParameter("@ews_url", SqlDbType.NVarChar)).Value = strEWS_URL.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@ews_domain", SqlDbType.NVarChar)).Value = strEWS_Domain.Trim.Replace("'", "''")


                    cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function
    'Sohail (02 Nov 2018) -- End

    Public Function GetEmailSetup(ByVal strCompCode As String,
                                  intComUnkID As Integer
                                  ) As Boolean
        Dim ds As New DataSet
        Try

            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procGetEmailSetup"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = strCompCode.Trim.Replace("'", "''")
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                        da.SelectCommand = cmd
                        da.Fill(ds, "EmailSetup")

                    End Using
                End Using
            End Using



            For Each dsRow As DataRow In ds.Tables(0).Rows

                HttpContext.Current.Session("e_emailsetupunkid") = dsRow.Item("emailsetupunkid")
                HttpContext.Current.Session("e_sendername") = dsRow.Item("sendername")
                HttpContext.Current.Session("e_senderaddress") = dsRow.Item("senderaddress")
                HttpContext.Current.Session("e_reference") = dsRow.Item("reference")
                HttpContext.Current.Session("e_mailserverip") = dsRow.Item("mailserverip")
                HttpContext.Current.Session("e_mailserverport") = dsRow.Item("mailserverport")
                HttpContext.Current.Session("e_username") = dsRow.Item("username").ToString
                HttpContext.Current.Session("e_originuserpassword") = dsRow.Item("password").ToString
                If dsRow.Item("password").ToString.Trim = "" Then
                    HttpContext.Current.Session("e_password") = dsRow.Item("password").ToString
                Else
                    HttpContext.Current.Session("e_password") = clsSecurity.Decrypt(dsRow.Item("password").ToString, "ezee")
                End If
                HttpContext.Current.Session("e_isloginssl") = dsRow.Item("isloginssl")
                HttpContext.Current.Session("e_mail_body") = dsRow.Item("mail_body")
                HttpContext.Current.Session("e_email_type") = dsRow.Item("email_type")
                HttpContext.Current.Session("e_ews_url") = dsRow.Item("ews_url")
                HttpContext.Current.Session("e_ews_domain") = dsRow.Item("ews_domain")

            Next

            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Global_asax.CatchException(ex, System.Web.HttpContext.Current)
            Return False
        End Try
    End Function

    Public Function EditEmailSeup(ByVal strCompCode As String,
                                 intComUnkID As Integer,
                                 intEmailsetupunkid As Integer,
                                 strUserName As String,
                                 strPassword As String,
                                 strMailServerIP As String,
                                 intMailServerPort As Integer,
                                 blnIsLoginSSL As Boolean,
                                 intEmailType As Integer,
                                 strEWS_URL As String,
                                 strEWS_Domain As String,
                                 strSenderName As String
                                 ) As Boolean
        Dim ds As New DataSet
        Try

            Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()

            Dim strQ As String = "procEditEmailSetup"

            Using con As New SqlConnection(strConn)
                con.Open()
                Using cmd As New SqlCommand(strQ, con)
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(New SqlParameter("@comp_code", SqlDbType.NVarChar)).Value = strCompCode.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = intComUnkID
                    cmd.Parameters.Add(New SqlParameter("@emailsetupunkid", SqlDbType.Int)).Value = intEmailsetupunkid
                    cmd.Parameters.Add(New SqlParameter("@username", SqlDbType.NVarChar)).Value = strUserName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@password", SqlDbType.NVarChar)).Value = clsSecurity.Encrypt(strPassword.Trim.Replace("'", "''"), "ezee")
                    cmd.Parameters.Add(New SqlParameter("@mailserverip", SqlDbType.NVarChar)).Value = strMailServerIP.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@sendername", SqlDbType.NVarChar)).Value = strSenderName.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@mailserverport", SqlDbType.Int)).Value = intMailServerPort
                    cmd.Parameters.Add(New SqlParameter("@isloginssl", SqlDbType.Bit)).Value = blnIsLoginSSL
                    cmd.Parameters.Add(New SqlParameter("@email_type", SqlDbType.Int)).Value = intEmailType
                    cmd.Parameters.Add(New SqlParameter("@ews_url", SqlDbType.NVarChar)).Value = strEWS_URL.Trim.Replace("'", "''")
                    cmd.Parameters.Add(New SqlParameter("@ews_domain", SqlDbType.NVarChar)).Value = strEWS_Domain.Trim.Replace("'", "''")


                    cmd.ExecuteNonQuery()
                End Using
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return False
        End Try

    End Function

#End Region

End Class
