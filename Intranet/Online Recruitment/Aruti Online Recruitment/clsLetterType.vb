﻿Option Strict On
Imports System.ComponentModel
Imports System.Data.SqlClient
Public Class clsLetterType

    Public Property _lettertypeunkid As Integer = 0
    Public Property _alias As String = ""
    Public Property _lettermasterunkid As Integer = 0
    Public Property _lettername As String = ""
    Public Property _lettername1 As String = ""
    Public Property _lettername2 As String = ""
    Public Property _lettercontent As String = ""
    Public Property _fieldtypeid As Integer = 0
    Public Property _isactive As Boolean = False
    Public Property _companyunkid As Integer = 0
    Public Property _comp_code As String = ""

    Public Function GetList(intCompanyId As Integer _
                            , strCompanyCode As String _
                            , Optional intUnkId As Integer = 0 _
                            , Optional intLetterMasterUnkId As Integer = 0
                            ) As List(Of clsLetterType)

        Dim ds As New DataSet
        Dim objList As List(Of clsLetterType) = Nothing
        Try

            'Dim strConn As String = HttpContext.Current.Session("ConnectionString").ToString()
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            Dim strQ As String = "procGetLetterTypeList"

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = CInt(intCompanyId)
                        cmd.Parameters.Add(New SqlParameter("@comp_code", SqlDbType.NVarChar)).Value = strCompanyCode
                        cmd.Parameters.Add(New SqlParameter("@lettertypeunkid", SqlDbType.Int)).Value = CInt(intUnkId)
                        cmd.Parameters.Add(New SqlParameter("@lettermasterunkid", SqlDbType.Int)).Value = CInt(intLetterMasterUnkId)

                        da.SelectCommand = cmd
                        da.Fill(ds, "LetterType")

                    End Using
                End Using
            End Using

            If ds.Tables(0).Rows.Count > 0 Then

                objList = (From p In ds.Tables(0) Select New clsLetterType With
                                                      {._lettertypeunkid = CInt(p.Item("lettertypeunkid")) _
                                                      , ._alias = p.Item("alias").ToString _
                                                      , ._lettermasterunkid = CInt(p.Item("lettermasterunkid")) _
                                                      , ._lettername = p.Item("lettername").ToString _
                                                      , ._lettername1 = p.Item("lettername1").ToString _
                                                      , ._lettername2 = p.Item("lettername2").ToString _
                                                      , ._lettercontent = p.Item("lettercontent").ToString _
                                                      , ._fieldtypeid = CInt(p.Item("fieldtypeid")) _
                                                      , ._isactive = CBool(p.Item("isactive")) _
                                                      , ._companyunkid = CInt(p.Item("companyunkid")) _
                                                      , ._comp_code = p.Item("Comp_Code").ToString
                                                      }).ToList

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return objList
    End Function
End Class
