﻿function pageLoad(sender, args) {
    //Don't remove pageLoad function for ReCaptcha to reload after post back.

}

function Ticked(id, i) {
    alert(id);
    //PageMethods.SMTPEWS_Ticked
}

function $$(id, context) {
    var el = $("#" + id, context);
    if (el.length < 1)
        el = $("[id$=_" + id + "]", context);
    return el;
}

var prm = Sys.WebForms.PageRequestManager.getInstance();
if (prm != null) {
    prm.add_endRequest(function (sender, e) {
        $$('LoginButton').on('click', function () {
            return IsValidate();
        });
      
        $('.btnrefresh').on('click', function () {
            $$('mycaptcha').attr('src', 'Captcha.aspx?v' + '' + $(Math.random())[0] + '');
        });
      
        //if ($$('pnlLogin').length > 0) {

            //grecaptcha.render('dvCaptcha', {
            //    'sitekey': '6LdGxiEUAAAAAIKv-RMBSB54MeT52NfD0je6ix8E',
            //    'callback': function (response) {
            //        $.ajax({
            //            type: "POST",
            //            url: "SLogin.aspx/VerifyCaptcha",
            //            data: "{response: '" + response + "'}",
            //            contentType: "application/json; charset=utf-8",
            //            dataType: "json",
            //            success: function (r) {
            //                var captchaResponse = jQuery.parseJSON(r.d);
            //                if (captchaResponse.success) {
            //                    $("[id*=txtCaptcha]").val(captchaResponse.success);
            //                    $("[id*=rfvCaptcha]").hide();
            //                } else {
            //                    $("[id*=txtCaptcha]").val("");
            //                    $("[id*=rfvCaptcha]").show();
            //                    var error = captchaResponse["error-codes"][0];
            //                    $("[id*=rfvCaptcha]").html("RECaptcha error. " + error);
            //                }
            //            }
            //        });
            //    }
            //});
            //$$('txtCaptcha').val('');
        //}
    });

    prm.add_endRequest(function (sender, e) {
        $$('btnTest').on('click', function () {
            return IsValidateTestConnection();
        });


    });
};

$(document).ready(function () {

    $$('LoginButton').on('click', function () {
        return IsValidate();
    });

    $('.btnrefresh').on('click', function () {
        $$('mycaptcha').attr('src', 'Captcha.aspx?v' + '' + $(Math.random())[0] + '');
    });

    $$('btnTest').on('click', function () {
        return IsValidateTestConnection();
    });

    PageMethods.SetURLLink(window.location.href);
});

function IsValidate() {
    if ($$('UserName').val().trim() == '') {
        ShowMessage('Please Enter Email.', 'MessageType.Errorr');
        $$('UserName').focus();
        return false;
    }
    else if (IsValidEmail($$('UserName').val()) == false) {
        ShowMessage('Please enter Valid Email.', 'MessageType.Errorr');
        $$('UserName').focus();
        return false;
    }
    else if ($$('Password').val().trim() == '') {
        ShowMessage('Please Enter Password.', 'MessageType.Errorr');
        $$('Password').focus();
        return false;
    }
    //else if ($$('txtCaptcha').val().trim() == '') {
    //    ShowMessage('Please Enter valid captcha.', 'MessageType.Errorr');
    //    $$('txtCaptcha').focus();
    //    return false;
    //}

    else if ($$('txtVerificationCode').val().trim() == '') {
        ShowMessage('Please Enter Verification Code.', 'MessageType.Errorr');
        $$('txtVerificationCode').focus();
        return false;
    }

    //else if ($$('txtVerificationCode').val() != '<%= Session("CaptchaImageText") %>') {
    //    alert('<%= Session("CaptchaImageText") %>')
    //    ShowMessage('Please enter valid Verification Code.', 'MessageType.Errorr');
    //    $$('txtVerificationCode').focus();
    //    return false;
    //}

    return true;
}

function IsValidEmail(strEmail) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(strEmail)) {
        return false;
    } else {
        return true;
    }
}

function IsValidateTestConnection() {
    if ($$('txtServerName').val().trim() == '') {
        ShowMessage('Please enter Server Name.', 'MessageType.Errorr');
        $$('txtServerName').focus();
        return false;
    }

    else if ($$('txtUserId').val().trim() == '') {
        ShowMessage('Please enter User ID.', 'MessageType.Errorr');
        $$('txtUserId').focus();
        return false;
    }

    else if ($$('txtPassword').val().trim() == '') {
        ShowMessage('Please enter Password.', 'MessageType.Errorr');
        $$('txtPassword').focus();
        return false;
    }
}