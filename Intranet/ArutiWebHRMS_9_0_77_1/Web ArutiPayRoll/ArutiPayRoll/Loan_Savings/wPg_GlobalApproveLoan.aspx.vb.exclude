﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data

#End Region


Partial Class Loan_Savings_wPg_GlobalApproveLoan
    Inherits Basepage

#Region " Private Variables "

    Private objPendingLoan As clsProcess_pending_loan
    Dim msg As New CommonCodes

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmGlobalApproveLoan"
    'Anjan [04 June 2014] -- End

#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End


            objPendingLoan = New clsProcess_pending_loan
            If Not IsPostBack Then
                Call FillCombo()
                radAll.Checked = True
                radAll_CheckedChanged(New Object(), New EventArgs())
            End If

        Catch ex As Exception
            msg.DisplayError("Page_Load1 Event : " & ex.Message, Me)
        Finally

        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objDMaster As New clsDepartment
        Dim objBMaster As New clsStation
        Dim objSMaster As New clsSections
        Dim objJMaster As New clsJobs
        Dim objAMaster As New clsLoan_Approver
        Try
            dsCombo = objDMaster.getComboList("List", True)
            With cboDepartment
                .DataValueField = "departmentunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objBMaster.getComboList("List", True)
            With cboBranch
                .DataValueField = "stationunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objSMaster.getComboList("List", True)
            With cboSection
                .DataValueField = "sectionunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objJMaster.getComboList("List", True)
            With cboJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objAMaster.GetList("List", True, True, True, Session("UserId"), Session("AccessLevelFilterString"))
            With cboApprover
                .DataValueField = "approverunkid"
                .DataTextField = "approvername"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objPendingLoan.GetLoan_Status("List", True)
            Dim dtTemp() As DataRow = dsCombo.Tables("List").Select("Id NOT IN(2)")
            For i As Integer = 0 To dtTemp.Length - 1
                dsCombo.Tables("List").Rows.Remove(dtTemp(i))
            Next
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = 2
            End With

        Catch ex As Exception
            msg.DisplayError("FillCombo :- " & ex.Message, Me)
        Finally
            dsCombo.Dispose() : objAMaster = Nothing : objBMaster = Nothing : objDMaster = Nothing _
            : objJMaster = Nothing : objSMaster = Nothing
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dtTab As DataTable = Nothing
        Try
            Dim intMode As Integer

            If radAdvance.Checked Then
                intMode = 0
            ElseIf radLoan.Checked Then
                intMode = 1
            ElseIf radAll.Checked Then
                intMode = -1
            End If

            dtTab = objPendingLoan.GetDataList(1, _
                                               CInt(cboDepartment.SelectedValue), _
                                               CInt(cboBranch.SelectedValue), _
                                               CInt(cboSection.SelectedValue), _
                                               CInt(cboJob.SelectedValue), _
                                               intMode)


            If dtTab.Columns.Contains("ExpCol") = False Then
                Dim dc As New DataColumn
                dc.DataType = Type.GetType("System.String")
                dc.ColumnName = "ExpCol"
                dc.DefaultValue = ""
                dtTab.Columns.Add(dc)

                Dim drRow() As DataRow = dtTab.Select("IsGrp = 1")
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        drRow(i)("ExpCol") = "+"
                    Next
                    dtTab.AcceptChanges()
                End If

            End If


            dgvData.AutoGenerateColumns = False

        Catch ex As Exception
            msg.DisplayError("FillGrid :- " & ex.Message, Me)
        Finally
            If dtTab.Rows.Count <= 0 Then
                Dim drRow As DataRow = dtTab.NewRow
                drRow("AppNo") = "None"
                drRow("Amount") = 0
                dtTab.Rows.Add(drRow)
            End If
            Me.ViewState.Add("PendingLoan", dtTab)
            dgvData.DataSource = dtTab
            dgvData.DataBind()

        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CInt(cboApprover.SelectedValue) <= 0 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                msg.DisplayError(Language.getMessage(mstrModuleName, 2, "Approver is compulsory information. Please select Approver to continue."), Me)
                'Anjan [04 June 2014] -- End

                cboApprover.Focus()
                Exit Sub
            End If

            Dim dtTemp() As DataRow = Nothing

            Dim dtTab As DataTable = CType(Me.ViewState("PendingLoan"), DataTable)
            dtTemp = dtTab.Select("IsChecked = 1 And IsGrp = false")
            If dtTemp.Length <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                msg.DisplayError(Language.getMessage(mstrModuleName, 2, "Please Select atleast one information to Approve."), Me)
                'Anjan [04 June 2014] -- End
                Exit Sub
            End If

            Blank_ModuleName()
            objPendingLoan._WebFormName = "frmGlobalApproveLoan"
            StrModuleName2 = "mnuLoan_Advance_Savings"
            objPendingLoan._WebClientIP = Session("IP_ADD")
            objPendingLoan._WebHostName = Session("HOST_NAME")
            objPendingLoan._Userunkid = Session("UserId")

            For i As Integer = 0 To dtTemp.Length - 1
                objPendingLoan._Processpendingloanunkid = CInt(dtTemp(i).Item("PId"))
                objPendingLoan._Approverunkid = CInt(cboApprover.SelectedValue)
                objPendingLoan._Loan_Statusunkid = CInt(cboStatus.SelectedValue)
                objPendingLoan._Approved_Amount = CDec(dtTemp(i).Item("AppAmount"))
                objPendingLoan.Update()
            Next

            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError("btnSave_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("wPg_LA_PendingList.aspx")
        Catch ex As Exception
            msg.DisplayError("btnClose_Click :- " & ex.Message, Me)
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("wPg_LA_PendingList.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region " Radio Button Event"

    Protected Sub radAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radAll.CheckedChanged, radLoan.CheckedChanged, radAdvance.CheckedChanged
        Try
            FillGrid()
        Catch ex As Exception
            msg.DisplayError("radAll_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " CheckBox Event"

    Protected Sub chkSelect_CheckChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try
            Dim dtTab As DataTable = CType(Me.ViewState("PendingLoan"), DataTable)
            Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)

            If gvRow.Cells.Count > 0 Then
                dtTab.Rows(gvRow.RowIndex).Item("IsChecked") = cb.Checked
                If dtTab.Rows(gvRow.RowIndex).Item("IsGrp") Then
                    Dim drRow() As DataRow = dtTab.Select("GrpID = " & CInt(dtTab.Rows(gvRow.RowIndex).Item("GrpID")) & " AND IsGrp = 0")

                    If drRow.Length > 0 Then
                        For i As Integer = 1 To drRow.Length
                            dtTab.Rows(gvRow.RowIndex + i).Item("IsChecked") = cb.Checked
                            Dim gRow As GridViewRow = dgvData.Rows(gvRow.RowIndex + i)
                            CType(gRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
                            CType(gRow.FindControl("lnkExpand"), LinkButton).Text = ""
                        Next
                    End If

                Else
                    Dim dTotalChildRow() As DataRow = dtTab.Select("GrpID = '" & CInt(dtTab.Rows(gvRow.RowIndex).Item("GrpID")) & "' AND IsGrp =  0")
                    Dim dCheckedchildRow() As DataRow = dtTab.Select("GrpID = '" & CInt(dtTab.Rows(gvRow.RowIndex).Item("GrpID")) & "' AND IsChecked =  1 AND IsGrp =  0")
                    
                    Dim mintRowIndex As Integer = -1
                    Dim drParentRow() As DataRow = dtTab.Select("GrpID = '" & CInt(dtTab.Rows(gvRow.RowIndex).Item("GrpID")) & "' AND IsGrp =  1")
                    mintRowIndex = dtTab.Rows.IndexOf(drParentRow(0))
                    Dim gParentRow As GridViewRow = dgvData.Rows(mintRowIndex)
                    If dTotalChildRow.Length = dCheckedchildRow.Length Then
                        CType(gParentRow.FindControl("chkSelect"), CheckBox).Checked = True
                    Else
                        CType(gParentRow.FindControl("chkSelect"), CheckBox).Checked = False
                    End If


                    ' START TO TEXT BLANK OF LINK BUTTON FOR CHILD ROW
                    If dTotalChildRow.Length > 0 Then
                        For i As Integer = 1 To dTotalChildRow.Length
                            mintRowIndex = dtTab.Rows.IndexOf(dTotalChildRow(i - 1))
                            Dim gRow As GridViewRow = dgvData.Rows(mintRowIndex)
                            CType(gRow.FindControl("lnkExpand"), LinkButton).Text = ""
                        Next
                    End If
                    ' END TO TEXT BLANK OF LINK BUTTON FOR CHILD ROW

                End If
            End If
            Me.ViewState("PendingLoan") = dtTab
        Catch ex As Exception
            msg.DisplayError("chkSelect_CheckChanged :- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " LinkButton Event"

    Protected Sub lnkExpand_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim lnkBtn As LinkButton = CType(sender, LinkButton)
            Dim dtTab As DataTable = CType(Me.ViewState("PendingLoan"), DataTable)
            Dim gvRow As GridViewRow = CType(lnkBtn.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                If dtTab.Rows(gvRow.RowIndex).Item("IsGrp") Then
                    Dim drRow() As DataRow = dtTab.Select("GrpID = " & CInt(dtTab.Rows(gvRow.RowIndex).Item("GrpID")) & " AND IsGrp = 0")

                    If drRow.Length > 0 Then

                        If lnkBtn.Text = "+" Then
                            For i As Integer = 1 To drRow.Length
                                Dim gRow As GridViewRow = dgvData.Rows(gvRow.RowIndex + i)
                                gRow.Visible = True
                                gRow.Cells(0).Text = ""
                            Next
                            lnkBtn.Text = "-"
                        Else
                            For i As Integer = 1 To drRow.Length
                                Dim gRow As GridViewRow = dgvData.Rows(gvRow.RowIndex + i)
                                gRow.Visible = False
                            Next
                            lnkBtn.Text = "+"
                        End If
                    End If
                End If

                dtTab.Rows(gvRow.RowIndex)("ExpCol") = lnkBtn.Text
                Me.ViewState("PendingLoan") = dtTab
            End If
        Catch ex As Exception
            msg.DisplayError("lnkExpand_Click :- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "GridView Event"

    Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
        Try
            Dim dtTab As DataTable = Me.ViewState("PendingLoan")

            If e.Row.RowIndex >= 0 Then

                If e.Row.Cells(5).Text = "None" AndAlso e.Row.Cells(6).Text = "0" Then
                    e.Row.Visible = False

                Else
                    If CBool(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then
                        For i = 4 To dgvData.Columns.Count - 1
                            e.Row.Cells(i).Visible = False
                        Next
                        e.Row.Cells(2).Visible = False
                        e.Row.Cells(3).ColumnSpan = dgvData.Columns.Count - 1
                        e.Row.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Row.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Row.Cells(3).CssClass = "GroupHeaderStyleBorderRight"

                        If (DataBinder.Eval(e.Row.DataItem, "ExpCol")) = "+" Then
                            Me.ViewState.Add("IsExpand", False)
                        ElseIf (DataBinder.Eval(e.Row.DataItem, "ExpCol")) = "-" Then
                            Me.ViewState.Add("IsExpand", True)
                        End If

                    Else

                        If Me.ViewState("IsExpand") = True Then
                            e.Row.Visible = True
                        ElseIf Me.ViewState("IsExpand") = False Then
                            e.Row.Visible = False
                        End If


                        'SHANI [01 FEB 2015]-START
                        'Enhancement - REDESIGN SELF SERVICE.
                        'e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#ddd'")
                        'e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=''")
                        'SHANI [01 FEB 2015]--END
                    End If
                End If

            If e.Row.Cells(6).Text.Trim <> "&nbsp;" AndAlso e.Row.Cells(6).Text.Trim <> "" Then
                e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtcurrency"))
            End If
            If e.Row.Cells(7).Text.Trim <> "&nbsp;" AndAlso e.Row.Cells(7).Text.Trim <> "" Then
                e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtcurrency"))
                e.Row.Cells(7).ForeColor = Drawing.Color.Blue
                e.Row.Cells(7).Font.Bold = True
            End If

            If e.Row.Cells(7).Controls.Count > 0 Then
                CType(e.Row.Cells(7).Controls(0), TextBox).CssClass = "RightTextAlign"
                CType(e.Row.Cells(7).Controls(0), TextBox).Text = CDec(CType(e.Row.Cells(7).Controls(0), TextBox).Text).ToString(Session("fmtcurrency"))
                CType(e.Row.Cells(7).Controls(0), TextBox).Focus()
            End If

            End If
        Catch ex As Exception
            msg.DisplayError("dgvData_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub dgvData_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgvData.RowEditing
        Try
            dgvData.DataSource = Me.ViewState("PendingLoan")
            dgvData.EditIndex = e.NewEditIndex
            dgvData.DataBind()

            dgvData.Columns(1).Visible = False

        Catch ex As Exception
            msg.DisplayError("dgvData_RowEditing :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub dgvData_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles dgvData.RowUpdating
        Try

            Dim dtTab As DataTable = Me.ViewState("PendingLoan")

            For i = 0 To dgvData.Columns.Count - 1

                If TypeOf (dgvData.Columns(i)) Is CommandField Or TypeOf (dgvData.Columns(i)) Is TemplateField Then Continue For

                If DirectCast(DirectCast(dgvData.Columns(i), System.Web.UI.WebControls.DataControlField), System.Web.UI.WebControls.BoundField).ReadOnly = False Then

                    Dim cell As DataControlFieldCell = dgvData.Rows(e.RowIndex).Cells(i)
                    cell.HorizontalAlign = HorizontalAlign.Right
                    dgvData.Columns(i).ExtractValuesFromCell(e.NewValues, cell, DataControlRowState.Edit, True)


                    For Each key As String In e.NewValues.Keys
                        Dim str As String = e.NewValues(key)

                        If dtTab.Columns(key).DataType Is System.Type.GetType("System.String") Then
                            Dim decTemp As Decimal = 0
                            If Decimal.TryParse(str, decTemp) = True Then

                                If CDec(dtTab.Rows(e.RowIndex)("Amount")) < decTemp Then
                                    msg.DisplayError("Approved Amount cannot be greater than amount.Please enter proper approved amount.", Me)
                                    e.Cancel = True
                                    Exit Sub
                                End If

                                dtTab.Rows(e.RowIndex).Item(key) = decTemp.ToString(Session("fmtcurrency"))
                            Else
                                msg.DisplayError("Please enter proper approved amount.", Me)
                                cell.Focus()
                                e.Cancel = True
                                Exit Sub
                            End If

                        Else
                            dtTab.Rows(e.RowIndex).Item(key) = str
                        End If

                    Next
                End If
            Next

            If e.NewValues.Count > 0 Then
                dgvData.DataSource = dtTab
                dgvData.EditIndex = -1
                dgvData.DataBind()
                dgvData.Columns(1).Visible = True
            End If

        Catch ex As Exception
            msg.DisplayError("dgvData_RowUpdating :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub dgvData_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles dgvData.RowCancelingEdit
        Try
            dgvData.DataSource = Me.ViewState("PendingLoan")
            dgvData.EditIndex = -1
            dgvData.DataBind()
            dgvData.Columns(1).Visible = True
        Catch ex As Exception
            msg.DisplayError("dgvData_RowCancelingEdit :- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "ImageButton Events"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.

    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError("Closebotton_CloseButton_click : -  " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region



    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try

            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.gbPending.Text = Language._Object.getCaption(Me.gbPending.ID, Me.gbPending.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.ID, Me.lblDepartment.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.ID, Me.lblSection.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.ID, Me.lblJob.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.ID, Me.lblBranch.Text)
            Me.radAdvance.Text = Language._Object.getCaption(Me.radAdvance.ID, Me.radAdvance.Text)
            Me.radLoan.Text = Language._Object.getCaption(Me.radLoan.ID, Me.radLoan.Text)
            Me.radAll.Text = Language._Object.getCaption(Me.radAll.ID, Me.radAll.Text)
            Me.gbFilter.Text = Language._Object.getCaption(Me.gbFilter.ID, Me.gbFilter.Text)
            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.ID, Me.gbInfo.Text)

            Me.dgvData.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(3).FooterText, Me.dgvData.Columns(3).HeaderText)
            Me.dgvData.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(4).FooterText, Me.dgvData.Columns(4).HeaderText)
            Me.dgvData.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(5).FooterText, Me.dgvData.Columns(5).HeaderText)
            Me.dgvData.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(6).FooterText, Me.dgvData.Columns(6).HeaderText)
            Me.dgvData.Columns(7).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(7).FooterText, Me.dgvData.Columns(7).HeaderText)
            

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

End Class
