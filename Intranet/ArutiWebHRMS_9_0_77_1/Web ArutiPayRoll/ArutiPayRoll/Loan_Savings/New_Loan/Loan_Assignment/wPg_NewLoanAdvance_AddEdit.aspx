﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_NewLoanAdvance_AddEdit.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Assignment_wPg_NewLoanAdvance_AddEdit" Title="Add/Edit Loan And Advance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <%--<script type="text/javascript">
        function pageLoad(sender, args) {
            alert('hi');
        }
         
    </script>--%>
    <%-- <script language="javascript" type="text/javascript">
        $(window).load(function() 
        {   alert("load");
//            var numud = $find('#<%=NumericUpDownExtender1.ClientID %>');
//            numud.add_currentChanged(onChanged);
        });  
//        function onChanged(sender, e)
//         {       
//            __doPostBack('<%# txtDuration.ClientID %>', 'true');
//         } 
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, event) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, evemt) {
            $('.panel-36').slideToggle(10);
            $("#endreq").val("1");
            //SetDesign()
        }
        function ShowDiv(result) {
            var strarry = result.split(",");
            for (i = 0; i < strarry.length; i++) {
                $("#" + strarry[i]).children(".panel-36").slideToggle(10);
                $("#" + strarry[i]).children(".toggler-36").toggleClass("active");
                $("#" + strarry[i]).children(".toggler-36").children('div').children('i').toggleClass("fa-chevron-circle-down");
            }
        }
    </script>

    <%--<script type="text/javascript">
    function SetDesign(){
        $("#<%= txtDuration.ClientID %>").next().next().css("top","0px");
        $("#<%= txtDuration.ClientID %>").next().css("top","-12px");
    }
    </script>--%>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            var charCode;

            if (window.event)
                charCode = window.event.keyCode;       // IE
            else
                charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <%--<script>
    $("#<%=txtDuration.ClientID %>_bUp").live("click",function(){
        CountInstallment();
    });
    function CountInstallment()
    {
    alert('hiii'
        $("#<%=txtDuration.ClientID %>").click()
    }
</script>--%>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Loan And Advance"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Loan/Advance General Information"></asp:Label>
                                    </div>
                                    <div style="float: right">
                                        <asp:Panel ID="fpnlType" runat="server">
                                            <asp:RadioButton ID="radLoan" runat="server" Checked="true" GroupName="LoanAdvance"
                                                Text="Loan" Style="margin-right: 20px" />
                                            <asp:RadioButton ID="radAdvance" runat="server" GroupName="LoanAdvance" Text="Advance" />
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 50%; border-right: 1px solid #DDD; margin-right: 10px" valign="top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblVoucherNo" runat="server" Text="Voucher No."></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <asp:TextBox ID="txtVoucherNo" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%" align="right">
                                                            <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%" align="right">
                                                            <uc1:DateCtrl ID="dtpDate" runat="server" AutoPostBack="false" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="3">
                                                            <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblEmpName" runat="server" Text="Employee"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="3">
                                                            <asp:DropDownList ID="cboEmpName" runat="server" Enabled="false" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblApprovedBy" runat="server" Text="Approved By"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="3">
                                                            <asp:TextBox ID="txtApprovedBy" runat="server" ReadOnly="true">
                                                            </asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="3">
                                                            <asp:DropDownList ID="cboLoanScheme" runat="server" Enabled="false" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblDeductionPeriod" runat="server" Text="Deduction Period"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="3">
                                                            <asp:DropDownList ID="cboDeductionPeriod" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblloanamt" runat="server" Text="Loan Amount"></asp:Label>
                                                            <asp:Label ID="lblAdvanceAmt" runat="server" Text="Advance Amount" Visible="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%; padding: 0px" colspan="3">
                                                            <table style="width: 100%; padding: 0px">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 33%; padding-left: 0px">
                                                                        <asp:TextBox ID="txtLoanAmt" Style="text-align: right" ReadOnly="true" Text="0" onKeypress="return onlyNumbers(this, event);"
                                                                            runat="server"></asp:TextBox>
                                                                        <asp:TextBox ID="txtAdvanceAmt" Style="text-align: right" Text="0" runat="server"
                                                                            ReadOnly="true" onKeypress="return onlyNumbers(this, event);" Visible="false"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 15%; padding-right: 0px">
                                                                        <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 52%">
                                                                        <asp:Label ID="objlblExRate" Style="margin-left: 5px" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%; font-weight: bold">
                                                            <asp:Label ID="lblLoanCalcType" runat="server" Text="Loan Calc. Type"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="3">
                                                            <asp:DropDownList ID="cboLoanCalcType" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%; font-weight: bold">
                                                            <asp:Label ID="lblInterestCalcType" runat="server" Text="Interest Calc. Type"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="3">
                                                            <asp:DropDownList ID="cboInterestCalcType" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%; font-weight: bold">
                                                            <asp:Label ID="lblMappedHead" runat="server" Text="Mapped Head"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="3">
                                                            <asp:DropDownList ID="cboMappedHead" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 50%" valign="top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="2">
                                                            <asp:Panel ID="pnlProjectedAmt" runat="server">
                                                                <table style="width: 100%">
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 100%" colspan="4">
                                                                            <h4 style="font-weight: bold; text-align: left; margin-bottom: 5px">
                                                                                <asp:Label ID="lnProjectedAmount" runat="server" Text="Projected Amount"></asp:Label></h4>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 30%">
                                                                            <asp:Label ID="lblInterestAmt" runat="server" Text="Interest Ammount" Style="margin-left: 15px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 70%" colspan="3">
                                                                            <asp:TextBox ID="txtInterestAmt" runat="server" Text="0" ReadOnly="true" onKeypress="return onlyNumbers(this, event);"
                                                                                Style="text-align: right"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 30%">
                                                                            <asp:Label ID="lblNetAmount" runat="server" Text="Net Ammount" Style="margin-left: 15px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 70%" colspan="3">
                                                                            <asp:TextBox ID="txtNetAmount" runat="server" Text="0" ReadOnly="true" onKeypress="return onlyNumbers(this, event);"
                                                                                Style="text-align: right"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="2">
                                                            <asp:Panel ID="pnlInformation" runat="server" Style="margin-top: 5px">
                                                                <table style="width: 100%">
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 100%; padding-bottom: 5px" colspan="4">
                                                                            <h4 style="font-weight: bold; text-align: left">
                                                                                <asp:Label ID="elProjectedINSTLAmt" runat="server" Text="Projected Installment Amount (First Installment)"></asp:Label></h4>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 30%">
                                                                            <asp:Label ID="lblLoanInterest" runat="server" Text="Rate (%)" Style="margin-left: 15px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 70%; padding-left: 0px; padding-right: 0px" colspan="3">
                                                                            <table style="width: 100%">
                                                                                <tr style="width: 100%">
                                                                                    <td style="width: 20%; padding-left: 0px">
                                                                                        <asp:TextBox ID="txtLoanRate" AutoPostBack="true" runat="server" Style="text-align: right"
                                                                                            Text="0" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 59%; text-align: right">
                                                                                        <asp:Label ID="lblDuration" runat="server" Style="margin-right: 15px" Text="Installment (In Months)"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 21%">
                                                                                        <asp:TextBox ID="txtDuration" AutoPostBack="true" Text="1" Style="text-align: right"
                                                                                            onclick="txtDuration_TextChanged" runat="server" onKeypress="return onlyNumbers(this, event);"></asp:TextBox><%--<cc1:NumericUpDownExtender ID="NumericUpDownExtender1" BehaviorID="numupdown" runat="server"
                                                                            TargetControlID="txtDuration" Width="100" Minimum="0" Maximum="1000">
                                                                        </cc1:NumericUpDownExtender>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 30%">
                                                                            <asp:Label ID="lblPrincipalAmt" runat="server" Text="Principal Amt." Style="margin-left: 15px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 30%;">
                                                                            <asp:TextBox ID="txtPrincipalAmt" AutoPostBack="true" runat="server" Style="text-align: right"
                                                                                ReadOnly="true" Text="0" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                        </td>
                                                                        <td style="width: 25%; text-align: right">
                                                                            <asp:Label ID="lblIntAmt" runat="server" Style="margin-right: 15px" Text="Interest Amt."></asp:Label>
                                                                        </td>
                                                                        <td style="width: 15%">
                                                                            <asp:TextBox ID="txtIntAmt" AutoPostBack="true" Text="0" Style="text-align: right"
                                                                                ReadOnly="true" runat="server" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 30%">
                                                                            <asp:Label ID="lblEMIAmount" runat="server" Text="Installment Amt." Style="margin-left: 15px"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 70%; font-weight: bold" colspan="3">
                                                                            <asp:TextBox ID="txtInstallmentAmt" runat="server" AutoPostBack="true" ReadOnly="true"
                                                                                Text="0" onKeypress="return onlyNumbers(this, event);" Style="text-align: right"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%" valign="top">
                                                            <asp:Label ID="lblPurpose" runat="server" Text="Loan Purpose" Style="margin-left: 20px"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <asp:TextBox ID="txtPurpose" runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="2">
                                                <div style="border-bottom: 1px solid #DDD; margin-top: 10px; margin-bottom: 10px">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="pnl_LoanInterestInfo" runat="server" Visible="true">
                                        <div id="divLoanInterestInfo" class="node">
                                            <div class="toggler-36">
                                                <div style="float: left; font-size: 20px; padding-right: 5px;">
                                                    <i class="fa fa-chevron-circle-right"></i>
                                                </div>
                                                <asp:Label ID="lblLoanInterestInfo" runat="server" Text="Loan Interest Information"></asp:Label></div>
                                            <div class="panel-36">
                                                <div class="panel-body">
                                                    <div id="Div1" class="panel-default">
                                                        <div id="Div3" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:Panel ID="pnlGridInterestHistory" ScrollBars="Auto" Style="max-height: 300px"
                                                                            runat="server">
                                                                            <asp:DataGrid ID="dgvInterestHistory" runat="server" AutoGenerateColumns="false"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                                <Columns>
                                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnAdd">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="imgAdd" runat="server" ToolTip="Add" CommandName="AddInterest">
                                                                                            <div style="font-size: 20px; color: Green;">
                                                                                                <i class="fa fa-plus"></i>
                                                                                            </div>
                                                                                            </asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="brnEdit">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="EditInterest"
                                                                                                    CssClass="gridedit"></asp:LinkButton></span></ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                                                    CommandName="DeleteInterest" CssClass="griddelete"></asp:LinkButton></span></ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="dperiod" HeaderText="Period" ReadOnly="true" FooterText="dgcolhInterestPeriod">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="ddate" HeaderText="Effective Date" ReadOnly="true" FooterText="dgcolhInterestEffectiveDate">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="interest_rate" HeaderText="Interest(%)" ReadOnly="true"
                                                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterText="dgcolhInterestRate">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="lninteresttranunkid" HeaderText="" ReadOnly="true" Visible="false"
                                                                                        FooterText="objdgcolhlninteresttranunkid"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="pstatusid" HeaderText="" ReadOnly="true" Visible="false"
                                                                                        FooterText="objdgcolhIPStatusId"></asp:BoundColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnl_LoanEMIInfo" runat="server" Visible="true">
                                        <div id="divLoanEMIInfo" class="node">
                                            <div class="toggler-36">
                                                <div style="float: left; font-size: 20px; padding-right: 5px;">
                                                    <i class="fa fa-chevron-circle-right"></i>
                                                </div>
                                                <asp:Label ID="lblLoanEMIInfo" runat="server" Text="Loan EMI Information"></asp:Label></div>
                                            <div class="panel-36">
                                                <div class="panel-body">
                                                    <div id="Div4" class="panel-default">
                                                        <div id="Div5" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:Panel ID="pnlGriddgvEMIHistory" ScrollBars="Auto" Style="max-height: 300px"
                                                                            runat="server">
                                                                            <asp:DataGrid ID="dgvEMIHistory" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                HeaderStyle-Font-Bold="false" Width="99%">
                                                                                <Columns>
                                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnAdd">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="imgAdd" runat="server" ToolTip="Add" CommandName="AddEMI">
                                                                                                <div style="font-size: 20px; color: Green;"><i class="fa fa-plus"></i></div>
                                                                                            </asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="brnEdit">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="EditEMI"
                                                                                                    CssClass="gridedit"></asp:LinkButton></span></ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                                                    CommandName="DeleteEMI" CssClass="griddelete"></asp:LinkButton></span></ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="dperiod" HeaderText="Period" ReadOnly="true" FooterText="dgcolhEMIPeriod">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="ddate" HeaderText="Effective Date" ReadOnly="true" FooterText="dgcolhEMIeffectivedate">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="loan_duration" HeaderText="Loan Duration" ReadOnly="true"
                                                                                        Visible="false" FooterText="dgcolhloan_duration" HeaderStyle-HorizontalAlign="Right"
                                                                                        ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="emi_amount" HeaderText="Installment Amt." ReadOnly="true"
                                                                                        FooterText="dgcolhInstallmentAmt" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="emi_tenure" HeaderText="No. of Installments" ReadOnly="true"
                                                                                        FooterText="dgcolhEMINoofInstallment" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="lnemitranunkid" HeaderText="" ReadOnly="true" Visible="false"
                                                                                        FooterText="objdgcolhlnemitranunkid"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="pstatusid" HeaderText="" ReadOnly="true" Visible="false"
                                                                                        FooterText="dgcolhEPstatusid"></asp:BoundColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnl_LoanTopupInfo" runat="server" Visible="true">
                                        <div id="divLoanTopupInfo" class="node">
                                            <div class="toggler-36">
                                                <div style="float: left; font-size: 20px; padding-right: 5px;">
                                                    <i class="fa fa-chevron-circle-right"></i>
                                                </div>
                                                <asp:Label ID="lblLoanTopupInfo" runat="server" Text="Loan Topup Information"></asp:Label></div>
                                            <div class="panel-36">
                                                <div class="panel-body">
                                                    <div id="Div6" class="panel-default">
                                                        <div id="Div7" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:Panel ID="pnlGriddgvTopupHistory" ScrollBars="Auto" Style="max-height: 300px"
                                                                            runat="server">
                                                                            <asp:DataGrid ID="dgvTopupHistory" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                HeaderStyle-Font-Bold="false" Width="99%">
                                                                                <Columns>
                                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnAdd">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="imgAdd" runat="server" ToolTip="Add" CommandName="AddEMI">
                                                                                                <div style="font-size: 20px; color: Green;"><i class="fa fa-plus"></i></div>
                                                                                            </asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="brnEdit">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="EditTopup"
                                                                                                    CssClass="gridedit"></asp:LinkButton></span></ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                                                    CommandName="DeleteTopup" CssClass="griddelete"></asp:LinkButton></span></ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="dperiod" HeaderText="Period" ReadOnly="true" FooterText="dgcolhTopupPeriod">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="ddate" HeaderText="Effective Date" ReadOnly="true" FooterText="dgColhTopupEffectiveDate">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="topup_amount" HeaderText="Topup Amount" ReadOnly="true"
                                                                                        FooterText="dgcolhTopupAmount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="interest_amount" HeaderText="Topup Interest" ReadOnly="true"
                                                                                        Visible="false" FooterText="dgcolhTopupInterest"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="lntopuptranunkid" HeaderText="" ReadOnly="true" Visible="false"
                                                                                        FooterText="objdgcolhlntopuptranunkid"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="pstatusid" HeaderText="" ReadOnly="true" Visible="false"
                                                                                        FooterText="objdgcolhTPstatusid"></asp:BoundColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <%--<asp:panel id="pnl_loanadvancehistory" runat="server" visible="false">
                                        <div id="divloanadvancehistory" class="node">
                                            <div class="toggler-36">
                                                <div style="float: left; font-size: 20px; padding-right: 5px;">
                                                    <i class="fa fa-chevron-circle-right"></i>
                                                </div>
                                                <asp:label id="lblloanadvancehistory" runat="server" text="Loan/Advance History"></asp:label>
                                            </div>
                                            <div class="panel-36">
                                                <div class="panel-body">
                                                    <div id="div8" class="panel-default">
                                                        <div id="div9" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:panel id="pnlgriddgvhistory" scrollbars="auto" style="max-height: 300px" runat="server">
                                                                            <asp:datagrid id="dgvhistory" runat="server" autogeneratecolumns="false" cssclass="gridview"
                                                                                headerstyle-cssclass="griviewheader" itemstyle-cssclass="griviewitem" allowpaging="false"
                                                                                headerstyle-font-bold="false" width="99%">
                                                                                <columns>
                                                                                    <asp:boundcolumn datafield="lnscheme" headertext="loan scheme" readonly="true" footertext="dgcolhloanscheme">
                                                                                    </asp:boundcolumn>
                                                                                    <asp:boundcolumn datafield="lnvocno" headertext="voc #" readonly="true" footertext="dgcolhvoucherno">
                                                                                    </asp:boundcolumn>
                                                                                    <asp:boundcolumn datafield="lnvdate" headertext="date" readonly="true" footertext="dgcolhdate">
                                                                                    </asp:boundcolumn>
                                                                                    <asp:boundcolumn datafield="lnamount" headertext="amount" readonly="true" footertext="dgcolhamount"
                                                                                        headerstyle-horizontalalign="right" itemstyle-horizontalalign="right"></asp:boundcolumn>
                                                                                    <asp:boundcolumn datafield="lnnetamount" headertext="net amount" readonly="true"
                                                                                        footertext="dgcolhnetamount" headerstyle-horizontalalign="right" itemstyle-horizontalalign="right">
                                                                                    </asp:boundcolumn>
                                                                                    <asp:boundcolumn datafield="lnpaid" headertext="repayment" readonly="true" footertext="dgcolhrepayment"
                                                                                        headerstyle-horizontalalign="right" itemstyle-horizontalalign="right"></asp:boundcolumn>
                                                                                    <asp:boundcolumn datafield="lnbalance" headertext="balance amount" readonly="true"
                                                                                        footertext="dgcolhbalanceamt" headerstyle-horizontalalign="right" itemstyle-horizontalalign="right">
                                                                                    </asp:boundcolumn>
                                                                                    <asp:boundcolumn datafield="lnstatus" headertext="status" readonly="true" footertext="dgcolhstatus">
                                                                                    </asp:boundcolumn>
                                                                                </columns>
                                                                            </asp:datagrid>
                                                                        </asp:panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:panel>--%>
                                    <div class="btn-default" id="btnfixedbottom">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                        <%--<asp:Button ID="btnPopup" runat="server" OnClientClick="return showpopup();" Text="Show Popup"
                                            CssClass="btndefault" />--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc2:DeleteReason ID="popup_DeleteReason" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <script>
        $(document).ready(function() {
            $('.panel-36').slideToggle(1200);
        });

        $('.toggler-36').live("click", function() {
            $(this).next('.panel-36').slideToggle(500);
            $(this).toggleClass("active");
            $(this).children('div').children('i').toggleClass("fa-chevron-circle-down");
            if ($(this).next('.panel-36').css("height") == "1px") {
                PageMethods.DivIdArray($(this).next('.panel-36').parent().attr("id"), true);
            } else {
                PageMethods.DivIdArray($(this).next('.panel-36').parent().attr("id"), false);
            }
        }); 
    </script>

</asp:Content>
