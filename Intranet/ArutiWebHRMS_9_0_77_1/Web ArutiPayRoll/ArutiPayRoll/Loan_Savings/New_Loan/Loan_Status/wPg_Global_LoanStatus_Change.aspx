﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_Global_LoanStatus_Change.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Status_wPg_Global_LoanStatus_Change" Title="Global Approve Loan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Global Approve Loan"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="panel-body">
                                <div id="Div7" class="panel-default">
                                    <div id="Div9" class="panel-body-default">
                                        <table style="width: 100%; margin-top: -10px; margin-bottom: -10px">
                                            <tr style="width: 100%">
                                                <td style="width: 60%" valign="top">
                                                    <div id="FilterCriteria" class="panel-default" style="margin-left: -2px; margin-right: -2px">
                                                        <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="gbFilter" runat="server" Text="Filter Criteria"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="FilterCriteriaBody" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblLoanAdvance" runat="server" Text="Loan/Advance"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <asp:DropDownList ID="cboLoanAdvance" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblPayPeriod" runat="server" Style="margin-left: 10px" Text="Assigned Period"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <asp:DropDownList ID="cboPayPeriod" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <asp:DropDownList ID="cboLoanScheme" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblFrmDate" runat="server" Style="margin-left: 10px" Text="Date From"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <uc1:DateControl ID="dtpDateFrom" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblStatus" runat="server" Text="Staus"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblToDate" runat="server" Style="margin-left: 10px" Text="Date To"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <uc1:DateControl ID="dtpToDate" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 40%" valign="top">
                                                    <div id="Div1" class="panel-default" style="margin-right: -2px">
                                                        <div id="Div2" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="gbOperation" runat="server" Text="Operation"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div3" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 30%">
                                                                        <asp:Label ID="lblPeriod" runat="server" Text="Status Period"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 70%">
                                                                        <asp:DropDownList ID="cboStatusPeriod" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 30%">
                                                                        <asp:Label ID="lblOperation" runat="server" Text="Change Status"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 70%">
                                                                        <asp:DropDownList ID="cboOperation" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 30%">
                                                                        <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 70%">
                                                                        <asp:TextBox ID="txtRemarks" runat="server" Enabled="false">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Div4" class="panel-default">
                                <div id="Div6" class="panel-body-default" style="position: relative">
                                    <div id="scrollable-container" onscroll="$(scroll1.Y).val(this.scrollTop);" style="max-height: 400px;
                                        overflow: auto; margin-bottom: 5px">
                                        <asp:DataGrid ID="dgvGridData" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                            ShowFooter="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            ItemStyle-CssClass-CssClass="griviewitem" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" 
                                                            oncheckedchanged="chkSelectAll_CheckedChanged" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" 
                                                            oncheckedchanged="chkSelect_CheckedChanged" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="VocNo" HeaderText="Voc #" FooterText="dgcolhVocNo"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="empcode" HeaderText="Code" FooterText="dgcolhEmpCode">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="EmpName" HeaderText="Employee" FooterText="dgcolhEmployee">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="ddate" HeaderText="Date" FooterText="dgcolhDate">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="LoanScheme" HeaderText="Loan Scheme" FooterText="dgcolhLoanScheme">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Loan_Advance" HeaderText="Loan/Advance" FooterText="dgcolhLoan_Advance">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="PeriodName" HeaderText="Period" FooterText="dgcolhPeriod">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="isloan" HeaderText="objdgcolhIsLoan" Visible="false"
                                                    FooterText="objdgcolhIsLoan"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="employeeunkid" HeaderText="objdgcolhemployeeunkid" Visible="false"
                                                    FooterText="objdgcolhemployeeunkid"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="loanadvancetranunkid" HeaderText="objdgcolhLoanAdvId"
                                                    Visible="false" FooterText="objdgcolhLoanAdvId"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="isbrought_forward" HeaderText="objdgisbrought_forward"
                                                    Visible="false" FooterText="objdgisbrought_forward"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="effective_date" HeaderText="objdgcolhEffDate" Visible="false" FooterText="objdgcolhEffDate">
                                                </asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                        </asp:DataGrid>
                                    </div>
                                    
                                    <div class="btn-default" id="btnfixedbottom">
                                        <div style="float:left">
                                            <asp:Panel ID="objPnlData" runat="server">
                                            <table style="width:100%">
                                                <tr style="width:100%">
                                                    <td style="width:5%">
                                                        <asp:Panel ID="objPnlMsg" runat="server" Width="30px" Height="15" style="background-color:Maroon"></asp:Panel>
                                                    </td>
                                                    <td style="width:95%"> 
                                                        <asp:Label ID="objlblCaption" runat="server" style="margin-left:5px" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        </div>
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
