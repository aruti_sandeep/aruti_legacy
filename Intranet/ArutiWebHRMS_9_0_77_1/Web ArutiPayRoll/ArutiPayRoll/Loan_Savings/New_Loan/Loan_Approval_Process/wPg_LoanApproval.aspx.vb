﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Net.Dns
#End Region

Partial Class Loan_Savings_New_Loan_Loan_Approval_Process_wPg_LoanApproval
    Inherits Basepage

#Region "Private Variables"

    Private Shared ReadOnly mstrModuleName As String = "frmLoanApproval"
    Dim DisplayMessage As New CommonCodes
    Private objLoanApproval As New clsloanapproval_process_Tran
    Private mintProcesspendingloanunkid As Integer = -1
    Private mintApproverID As Integer = -1
    Private mintPendingloantranunkid As Integer = -1
    Private mintEmployeeunkid As Integer = -1
    Private mintPriority As Integer = -1
    Private mintApproverEmpunkid As Integer = -1
    Private mintCountryunkid As Integer = -1
    Private mdecInstallmentAmt As Decimal

    'Shani (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification 
    Private objCONN As SqlConnection
    'Shani (21-Jul-2016) -- End

    'Nilay (10-Dec-2016) -- Start
    'Issue #26: Setting to be included on configuration for Loan flow Approval process
    Private mblnIsSendEmailNotification As Boolean = False
    'Nilay (10-Dec-2016) -- End


#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                'Sohail (02 Apr 2019) -- Start
                'NMB Issue - 74.1 - Error "On Load Event !! Bad Data" on clicking any page with link after session get expired.
                If Request.QueryString.Count <= 0 Then Exit Sub
                'Sohail (02 Apr 2019) -- End
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                mintPendingloantranunkid = CInt(arr(0))
                mintApproverID = CInt(arr(1))
                mintProcesspendingloanunkid = CInt(arr(2))
                If arr.Length = 5 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try
                    'Blank_ModuleName()
                    'clsCommonATLog._WebFormName = "frmPerformanceEvaluation"
                    'StrModuleName2 = "mnuAssessment"
                    'StrModuleName3 = "mnuPerformaceEvaluation"
                    'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString
                    'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString
                    Me.ViewState.Add("IsDirect", True)

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(3))
                    HttpContext.Current.Session("UserId") = CInt(arr(4))

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ''ArtLic._Object = New ArutiLic(False)
                    ''If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                    ''    Dim objGroupMaster As New clsGroup_Master
                    ''    objGroupMaster._Groupunkid = 1
                    ''    ArtLic._Object.HotelName = objGroupMaster._Groupname
                    ''End If

                    ''If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                    ''    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                    ''    Exit Sub
                    ''End If

                    ''If ConfigParameter._Object._IsArutiDemo Then
                    ''    If ConfigParameter._Object._IsExpire Then
                    ''        DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                    ''        Exit Try
                    ''    Else
                    ''        If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                    ''            DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                    ''            Exit Try
                    ''        End If
                    ''    End If
                    ''End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If
                    Session("LoanApprover_ForLoanScheme") = clsConfig._IsLoanApprover_ForLoanScheme

                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    Session("DateFormat") = clsConfig._CompanyDateFormat
                    Session("DateSeparator") = clsConfig._CompanyDateSeparator
                    Call SetDateFormat()
                    'Pinkal (16-Apr-2016) -- End

                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))

                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If
                    'Dim objUserPrivilege As New clsUserPrivilege
                    'objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                    'Session("AllowtoAddReviewerEvaluation") = objUserPrivilege._AllowtoAddReviewerEvaluation
                    Dim dsList As DataSet = Nothing
                    dsList = objLoanApproval.GetApprovalTranList(Session("Database_Name").ToString, _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 CStr(Session("UserAccessModeSetting")), True, _
                                                                 CBool(Session("IsIncludeInactiveEmp")), "List", _
                                                                 0, CInt(arr(2)))
                    'employeeunkid CInt
                    'ProcessPendingunkid
                    Dim dRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("pendingloantranunkid") = CInt(arr(0)))

                    If CInt(dRow(0).Item("statusunkid")) <> 1 Then
                        If CInt(dRow(0).Item("statusunkid")) = 3 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        Else
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "You can't Edit this Loan detail. Reason: This Loan is already approved."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If
                    End If

                    Dim dtRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") >= CInt(dRow(0).Item("priority")) AndAlso x.Field(Of Integer)("statusunkid") <> 1)
                    If dtRow.Count > 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "You can't Edit this Loan detail. Reason: This Loan is already approved/reject or assign"), Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If
                    GoTo Link
                End If

            End If
            'Shani (21-Jul-2016) -- End

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
Link:
            If IsPostBack = False Then

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromDesktopMSS"))
                ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromESS"))
                End If
                'Nilay (10-Dec-2016) -- End

                Call SetLanguage()
                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'If Request.QueryString.Count > 0 Then
                '    Dim array() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                '    If array.Length = 3 Then
                '        mintPendingloantranunkid = CInt(array(0))
                '        mintApproverID = CInt(array(1))
                '        mintProcesspendingloanunkid = CInt(array(2))
                '    End If
                'End If
                If Session("PendingLoanTranunkid") IsNot Nothing Then
                    mintPendingloantranunkid = CInt(Session("PendingLoanTranunkid"))
                    Session("PendingLoanTranunkid") = Nothing
                End If
                If Session("Approverunkid") IsNot Nothing Then
                    mintApproverID = CInt(Session("Approverunkid"))
                    Session("Approverunkid") = Nothing
                    End If
                If Session("ProcessPendingLoanunkid") IsNot Nothing Then
                    mintProcesspendingloanunkid = CInt(Session("ProcessPendingLoanunkid"))
                    Session("ProcessPendingLoanunkid") = Nothing
                End If
                'Nilay (06-Aug-2016) -- END

                Call FillCombo()
                Call GetValue()

            Else
                mintPendingloantranunkid = CInt(Me.ViewState("Pendingloantranunkid"))
                mintApproverID = CInt(Me.ViewState("ApproverID"))
                mintProcesspendingloanunkid = CInt(Me.ViewState("Processpendingloanunkid"))
                mintEmployeeunkid = CInt(Me.ViewState("Employeeunkid"))
                mintPriority = CInt(Me.ViewState("Priority"))
                mintApproverEmpunkid = CInt(Me.ViewState("ApproverEmpunkid"))
                mintCountryunkid = CInt(Me.ViewState("Countryunkid"))
                mdecInstallmentAmt = CDec(Me.ViewState("InstallmentAmt"))
                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                mblnIsSendEmailNotification = CBool(Me.ViewState("SendEmailNotification"))
                'Nilay (10-Dec-2016) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load1:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("Pendingloantranunkid") = mintPendingloantranunkid
            Me.ViewState("ApproverID") = mintApproverID
            Me.ViewState("Processpendingloanunkid") = mintProcesspendingloanunkid
            Me.ViewState("Employeeunkid") = mintEmployeeunkid
            Me.ViewState("Priority") = mintPriority
            Me.ViewState("ApproverEmpunkid") = mintApproverEmpunkid
            Me.ViewState("Countryunkid") = mintCountryunkid
            Me.ViewState("InstallmentAmt") = mdecInstallmentAmt
            'Nilay (10-Dec-2016) -- Start
            'Issue #26: Setting to be included on configuration for Loan flow Approval process
            Me.ViewState("SendEmailNotification") = mblnIsSendEmailNotification
            'Nilay (10-Dec-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
        Me.IsLoginRequired = True
        End If
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            Dim objScheme As New clsLoan_Scheme
            Dim objPeriod As New clscommom_period_Tran
            Dim objLoan As New clsProcess_pending_loan

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objScheme.getComboList(True, "Scheme")
            dsList = objScheme.getComboList(True, "Scheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .DataTextField = "name"
                .DataValueField = "loanschemeunkid"
                .DataSource = dsList.Tables("Scheme")
                .DataBind()
            End With
            objScheme = Nothing
            dsList = Nothing

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                               CInt(Session("Fin_year")), _
                                               CStr(Session("Database_Name")), _
                                               CDate(Session("fin_startdate")), _
                                               "Period", True, 1)

            With cboDeductionPeriod
                .DataTextField = "name"
                .DataValueField = "periodunkid"
                .DataSource = dsList.Tables("Period")
                .DataBind()
            End With
            objPeriod = Nothing
            dsList = Nothing

            dsList = objLoan.GetLoan_Status("Status", True, False)
            'Nilay (25-Jul-2016) -- Start
            Dim dtView As DataView = New DataView(dsList.Tables("Status"), "Id <> 1", "", DataViewRowState.CurrentRows)
            'Nilay (25-Jul-2016) -- End
            With cboStatus
                .DataTextField = "NAME"
                .DataValueField = "Id"
                .DataSource = dtView 'Nilay (25-Jul-2016)
                .SelectedValue = "0" 'Nilay (25-Jul-2016)
                .DataBind()
            End With
            objLoan = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("" & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetValue()
        Try
            Dim objLoanapplication As New clsProcess_pending_loan
            objLoanapplication._Processpendingloanunkid = mintProcesspendingloanunkid

            txtApplicationNo.Text = objLoanapplication._Application_No
            If Not (objLoanapplication._Application_Date = Nothing) Then
                dtpApplicationDate.SetDate = objLoanapplication._Application_Date.Date
            End If

            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objLoanapplication._Employeeunkid
            mintEmployeeunkid = objLoanapplication._Employeeunkid
            txtEmployee.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = -1

            Dim objLoanApprover As New clsLoanApprover_master
            Dim objLoanLevel As New clslnapproverlevel_master
            objLoanApprover._lnApproverunkid = mintApproverID
            objLoanLevel._lnLevelunkid = objLoanApprover._lnLevelunkid
            mintPriority = objLoanLevel._Priority
            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objLoanApprover._ApproverEmpunkid
            'mintApproverEmpunkid = objLoanApprover._ApproverEmpunkid
            'txtApprover.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " - " & objLoanLevel._lnLevelname
            If objLoanApprover._IsExternalApprover = True Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = objLoanApprover._ApproverEmpunkid
                Dim strAppName As String = objUser._Username & " - " & objLoanLevel._lnLevelname
                If strAppName.Trim.Length > 0 Then
                    txtApprover.Text = strAppName
                Else
                    txtApprover.Text = objUser._Username
                End If
                mintApproverEmpunkid = objUser._Userunkid
                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                If objUser._Userunkid <= 0 Then
                    txtApprover.Text = ""
                End If
                'Nilay (06-Aug-2016) -- END
                objUser = Nothing
            Else
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objLoanApprover._ApproverEmpunkid
            txtApprover.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " - " & objLoanLevel._lnLevelname
                mintApproverEmpunkid = objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) <= 0 Then
                    txtApprover.Text = ""
                End If
                'Nilay (06-Aug-2016) -- END
            End If
            'Nilay (21-Jul-2016) -- End

            objLoanLevel = Nothing
            objLoanApprover = Nothing
            objEmployee = Nothing

            radLoan.Checked = objLoanapplication._Isloan
            radAdvance.Checked = Not objLoanapplication._Isloan
            'Nilay (27-Dec-2016) -- Start
            Call radLoan_CheckedChanged(radLoan, New EventArgs())
            'Nilay (27-Dec-2016) -- End
            txtExternalEntity.Text = objLoanapplication._External_Entity_Name
            cboLoanScheme.SelectedValue = CStr(objLoanapplication._Loanschemeunkid)

            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran
            Dim dsScale As DataSet

            Dim intFirstOpenPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1, False, True)

            objPeriod._Periodunkid(Session("Database_Name").ToString) = intFirstOpenPeriodId

            dsScale = objMaster.Get_Current_Scale("Scale", objLoanapplication._Employeeunkid, objPeriod._End_Date.Date)
            If dsScale IsNot Nothing AndAlso dsScale.Tables("Scale").Rows.Count > 0 Then
                txtBasicSal.Text = Format(CDec(dsScale.Tables("Scale").Rows(0)("newscale")), Session("fmtCurrency").ToString)
            End If
            'Nilay (21-Oct-2015) -- End

            'Sohail (15 May 2018) -- Start
            'CCK Enhancement - Ref # 179 : On loan approval screen on MSS, not all approvers should see the basic salary of the staff. in 72.1.
            lblBasicSal.Visible = CBool(Session("ViewScale"))
            txtBasicSal.Visible = CBool(Session("ViewScale"))
            'Sohail (15 May 2018) -- End

            If mintPendingloantranunkid > 0 Then
                objLoanApproval._Pendingloantranunkid = mintPendingloantranunkid
                txtLoanAmt.Text = Format(CDec(objLoanApproval._Loan_Amount), Session("fmtCurrency").ToString)
                txtAppliedAmnt.Text = Format(CDec(objLoanapplication._Loan_Amount), Session("fmtCurrency").ToString)

                Dim objCurrency As New clsExchangeRate
                Dim dsList As DataSet = objCurrency.GetList("List", True, False, 0, objLoanApproval._Countryunkid)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    txtCurrency.Text = dsList.Tables(0).Rows(0)("currency_sign").ToString()
                    mintCountryunkid = CInt(dsList.Tables(0).Rows(0)("countryunkid").ToString())

                End If
                dsList = Nothing
                objCurrency = Nothing

                'txtDurationInMths.Text = objLoanApproval._Duration
                cboDeductionPeriod.SelectedValue = CStr(objLoanApproval._Deductionperiodunkid)
                txtInstallmentAmt.Text = Format(CDec(objLoanApproval._Installmentamt), Session("fmtCurrency").ToString)
                'mdecInstallmentAmt = Format(CDec(objLoanApproval._Installmentamt), Session("fmtCurrency"))
                mdecInstallmentAmt = CDec(objLoanApproval._Installmentamt)
                txtEMIInstallments.Text = objLoanApproval._Noofinstallment.ToString()
                'Nilay (25-Jul-2016) -- Start
                'cboStatus.SelectedValue = CStr(objLoanApproval._Statusunkid)
                'Nilay (25-Jul-2016) -- End

            ElseIf mintProcesspendingloanunkid > 0 Then

                txtLoanAmt.Text = Format(CDec(objLoanapplication._Loan_Amount), Session("fmtCurrency").ToString)
                Dim objCurrency As New clsExchangeRate
                Dim dsList As DataSet = objCurrency.GetList("List", True, False, 0, objLoanapplication._Countryunkid)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    txtCurrency.Text = dsList.Tables(0).Rows(0)("currency_sign").ToString()
                    mintCountryunkid = CInt(dsList.Tables(0).Rows(0)("countryunkid").ToString())
                End If
                dsList = Nothing
                objCurrency = Nothing

                'txtDurationInMths.Text = objLoanapplication._DurationInMonths
                cboDeductionPeriod.SelectedValue = CStr(objLoanapplication._DeductionPeriodunkid)
                txtInstallmentAmt.Text = Format(CDec(objLoanapplication._InstallmentAmount), Session("fmtCurrency").ToString)
                'mdecInstallmentAmt = Format(CDec(objLoanapplication._InstallmentAmount), Session("fmtCurrency"))
                mdecInstallmentAmt = CDec(objLoanapplication._InstallmentAmount)
                txtEMIInstallments.Text = objLoanapplication._NoOfInstallment.ToString()
                'Nilay (25-Jul-2016) -- Start
                'cboStatus.SelectedValue = CStr(objLoanapplication._Loan_Statusunkid)
                'Nilay (25-Jul-2016) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetValue()
        Try
            If mintPendingloantranunkid > 0 Then
                objLoanApproval._Pendingloantranunkid = mintPendingloantranunkid
            End If
            objLoanApproval._Processpendingloanunkid = mintProcesspendingloanunkid
            objLoanApproval._Employeeunkid = mintEmployeeunkid
            objLoanApproval._Approvertranunkid = mintApproverID
            objLoanApproval._Approverempunkid = mintApproverEmpunkid
            objLoanApproval._Priority = mintPriority
            objLoanApproval._Approvaldate = DateAndTime.Now.Date
            objLoanApproval._Deductionperiodunkid = CInt(cboDeductionPeriod.SelectedValue)
            objLoanApproval._Loan_Amount = CDec(txtLoanAmt.Text)
            objLoanApproval._Countryunkid = mintCountryunkid
            'objLoanApproval._Duration = CInt(txtDurationInMths.Text)
            objLoanApproval._Installmentamt = mdecInstallmentAmt
            objLoanApproval._Noofinstallment = CInt(txtEMIInstallments.Text)
            objLoanApproval._Statusunkid = CInt(cboStatus.SelectedValue)
            objLoanApproval._Remark = txtRemark.Text
            objLoanApproval._Userunkid = CInt(Session("UserId"))
            objLoanApproval._Isvoid = False
            'Nilay (05-May-2016) -- Start
            'Blank_ModuleName()
            'StrModuleName2 = "mnuLoan_Advance_Savings"
            'objLoanApproval._WebClientIP = Session("IP_ADD").ToString
            'objLoanApproval._WebFormName = "frmLoanApproval"
            'objLoanApproval._WebHostName = Session("HOST_NAME").ToString
            'Nilay (05-May-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function IsValidation() As Boolean
        Try
            If txtEmployee.Text.Trim.Length <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), Me)
                txtEmployee.Focus()
                Return False
            End If
            If radLoan.Checked = True Then
                If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), Me)
                    cboLoanScheme.Focus()
                    Return False
                End If
            End If
            If CDec(txtLoanAmt.Text) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Loan/Advance Amount cannot be blank. Loan/Advance Amount is compulsory information."), Me)
                txtLoanAmt.Focus()
                Return False
            End If
            If txtCurrency.Text.Trim.Length <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Currency is compulsory information.Please select currency."), Me)
                txtCurrency.Focus()
                Return False
            End If

            If CInt(cboDeductionPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Deduction Period is compulsory information.Please select deduction period."), Me)
                cboDeductionPeriod.Focus()
                Return False
            End If

            If radLoan.Checked = True Then
                'If txtDurationInMths.Text.Trim.Length <= 0 Then
                '    Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Duration in Months cannot be 0.Please define duration in months greater than 0."), Me)
                '    txtDurationInMths.Focus()
                '    Return False
                'End If

                If CDec(txtInstallmentAmt.Text) <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Installment Amount cannot be 0.Please define installment amount greater than 0."), Me)
                    txtInstallmentAmt.Focus()
                    Return False
                End If
                If CInt(txtEMIInstallments.Text) <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "No of Installment cannot be 0.Please define No of Installment greater than 0."), Me)
                    txtEMIInstallments.Focus()
                    Return False
                End If
                If CDec(txtInstallmentAmt.Text) > CDec(txtLoanAmt.Text) Then
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    Language.setLanguage(mstrModuleName)
                    'Varsha (25 Nov 2017) -- End
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Installment Amount cannot be greater than Loan Amount."), Me)
                    txtInstallmentAmt.Focus()
                    Return False
                End If
                'If Format(CDec(txtLoanAmt.Text), Session("fmtCurrency").ToString) <> Format((CDec(txtEMIInstallments.Text) * mdecInstallmentAmt), Session("fmtCurrency").ToString) Then
                '    Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "You are changing loan amount.We recommanded you that you have to change No of installments or Installment amount."), Me)
                '    txtEMIInstallments.Focus()
                '    Return False
                'End If
            End If

            'Nilay (25-Jul-2016) -- Start
            If CInt(cboStatus.SelectedValue) = 0 Then
                'Varsha (25 Nov 2017) -- Start
                'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                Language.setLanguage(mstrModuleName)
                'Varsha (25 Nov 2017) -- End
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select atleast one Status from the list to perform further operation."), Me)
                cboStatus.Focus()
                Return False
            End If
            'Nilay (25-Jul-2016) -- End

            If CInt(cboStatus.SelectedValue) = 3 AndAlso txtRemark.Text.Trim.Length <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Remark cannot be blank.Remark is compulsory information."), Me)
                txtRemark.Focus()
                Return False
            End If

            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            Dim objLoanScheme As New clsLoan_Scheme
            objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(txtEMIInstallments.Text) > objLoanScheme._MaxNoOfInstallment Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Language.getMessage(mstrModuleName, 16, " for ") & objLoanScheme._Name & Language.getMessage(mstrModuleName, 17, " Scheme."), Me)
                txtEMIInstallments.Focus()
                Return False
            End If
            'Varsha (25 Nov 2017) -- End

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidation:- " & ex.Message, Me)
            DisplayMessage.DisplayError("IsValidation:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

#End Region

#Region "Butto's Events"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim blnFlag As Boolean = False

            If IsValidation() = False Then Exit Sub

            Call SetValue()
            With objLoanApproval
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
            If mintPendingloantranunkid > 0 Then
                'Pinkal (20-Sep-2022) -- Start
                'NMB Loan Module Enhancement.
                'blnFlag = objLoanApproval.Update(CBool(Session("LoanApprover_ForLoanScheme")), CInt(cboLoanScheme.SelectedValue))
                blnFlag = objLoanApproval.Update(Session("Database_Name").ToString, CInt(Session("UserId")) _
                                                                          , CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                          , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CStr(Session("UserAccessModeSetting")) _
                                                                          , True, CBool(Session("IsIncludeInactiveEmp")), CBool(Session("LoanApprover_ForLoanScheme")), CInt(cboLoanScheme.SelectedValue), Nothing)
                'Pinkal (20-Sep-2022)-- End
            Else
                blnFlag = objLoanApproval.Insert(CBool(Session("LoanApprover_ForLoanScheme")), CInt(cboLoanScheme.SelectedValue))
            End If

            'mintPendingloantranunkid = objLoanApproval._Pendingloantranunkid

            If blnFlag = False And objLoanApproval._Message <> "" Then
                DisplayMessage.DisplayMessage(objLoanApproval._Message, Me)
            Else
                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                Dim objProcessLoan As New clsProcess_pending_loan
                If mblnIsSendEmailNotification = True Then
                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                'Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approval_Process/wPg_LoanApprovalList.aspx", False)
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'If CInt(cboStatus.SelectedValue) = 2 Then 'Approve
                If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.APPROVED Then
                    'Nilay (20-Sept-2016) -- End
                    If objLoanApproval.IsPendingLoanApplication(mintProcesspendingloanunkid, True) = False Then
                        If radLoan.Checked Then
                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                '                                          CInt(cboLoanScheme.SelectedValue), _
                                '                                          mintEmployeeunkid, _
                                '                                          mintPriority, _
                                '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                '                                          False, mintProcesspendingloanunkid.ToString, _
                                '                                          Session("ArutiSelfServiceURL").ToString, True, enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                            objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                                                      CInt(cboLoanScheme.SelectedValue), _
                                                                      mintEmployeeunkid, _
                                                                      mintPriority, _
                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                      False, mintProcesspendingloanunkid.ToString, _
                                                                          Session("ArutiSelfServiceURL").ToString, CInt(Session("CompanyUnkId")), True, enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                                'Sohail (30 Nov 2017) -- End
                        Else
                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                '                                          CInt(cboLoanScheme.SelectedValue), _
                                '                                          mintEmployeeunkid, _
                                '                                          mintPriority, _
                                '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                '                                          False, mintProcesspendingloanunkid.ToString, _
                                '                                          Session("ArutiSelfServiceURL").ToString, True, enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                            objProcessLoan.Send_Notification_Approver(CBool(Session("LoanApprover_ForLoanScheme")), _
                                                                      CInt(cboLoanScheme.SelectedValue), _
                                                                      mintEmployeeunkid, _
                                                                      mintPriority, _
                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                      False, mintProcesspendingloanunkid.ToString, _
                                                                          Session("ArutiSelfServiceURL").ToString, CInt(Session("CompanyUnkId")), True, enLogin_Mode.DESKTOP, 0, CInt(Session("UserId")))
                                'Sohail (30 Nov 2017) -- End
                        End If
                        
                    Else 'Assign

                            'Nilay (08-Dec-2016) -- Start
                            'Issue #7: Same user is the approver and posting loan, loan should by pass Approval process
                            'If radLoan.Checked Then
                            'Nilay (08-Dec-2016) -- End


                            'Nilay (23-Aug-2016) -- Start
                            'Enhancement - Create New Loan Notification 
                            'objProcessLoan.Send_Notification_Assign(mintEmployeeunkid, _
                            '                                       CInt(Session("Fin_year")), _
                            '                                       Session("ArutiSelfServiceURL").ToString, _
                            '                                       mintPendingloantranunkid, _
                            '                                       Session("Database_Name").ToString, _
                            '                                       CInt(Session("CompanyUnkId")), _
                            '                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                            '                                       CStr(Session("UserAccessModeSetting")), _
                            '                                       CBool(Session("IsIncludeInactiveEmp")), _
                            '                                       enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                            objProcessLoan.Send_Notification_Assign(mintEmployeeunkid, _
                                                                    CInt(Session("Fin_year")), _
                                                                    Session("ArutiSelfServiceURL").ToString, _
                                                                   mintProcesspendingloanunkid, _
                                                                    Session("Database_Name").ToString, _
                                                                    CInt(Session("CompanyUnkId")), _
                                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                    CStr(Session("UserAccessModeSetting")), _
                                                                    CBool(Session("IsIncludeInactiveEmp")), _
                                                                   False, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), , _
                                                                   Session("Notify_LoanAdvance_Users").ToString)
                            'Hemant (30 Aug 2019) -- [Session("Notify_LoanAdvance_Users").ToString)]	
                            'Nilay (23-Aug-2016) -- End

                            'Nilay (08-Dec-2016) -- Start
                            'Issue #7: Same user is the approver and posting loan, loan should by pass Approval process
                            If radLoan.Checked Then
                                'Nilay (08-Dec-2016) -- End

                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objProcessLoan.Send_Notification_Employee(mintEmployeeunkid, _
                                '                                      mintProcesspendingloanunkid, _
                                '                                      clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                '                                      clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, , _
                                '                                      enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                            objProcessLoan.Send_Notification_Employee(mintEmployeeunkid, _
                                                                      mintProcesspendingloanunkid, _
                                                                      clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(Session("CompanyUnkId")), , _
                                                                      enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                'Sohail (30 Nov 2017) -- End
                        Else
                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objProcessLoan.Send_Notification_Employee(mintEmployeeunkid, _
                                '                                          mintProcesspendingloanunkid, _
                                '                                          clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, , _
                                '                                          enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                            objProcessLoan.Send_Notification_Employee(mintEmployeeunkid, _
                                                                      mintProcesspendingloanunkid, _
                                                                      clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(Session("CompanyUnkId")), , _
                                                                      enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                'Sohail (30 Nov 2017) -- End
                        End If
                    End If
                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'ElseIf CInt(cboStatus.SelectedValue) = 3 Then ' Reject
                ElseIf CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED Then
                    'Nilay (20-Sept-2016) -- End
                    If radLoan.Checked Then
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objProcessLoan.Send_Notification_Employee(mintEmployeeunkid, _
                            '                                          mintProcesspendingloanunkid, _
                            '                                          clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                            '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                            '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                            '                                          txtRemark.Text, _
                            '                                          enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                        objProcessLoan.Send_Notification_Employee(mintEmployeeunkid, _
                                                                  mintProcesspendingloanunkid, _
                                                                  clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                                                  clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(Session("CompanyUnkId")), _
                                                                  txtRemark.Text, _
                                                                  enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                            'Sohail (30 Nov 2017) -- End
                    Else
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objProcessLoan.Send_Notification_Employee(mintEmployeeunkid, _
                            '                                          mintProcesspendingloanunkid, _
                            '                                          clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                            '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                            '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                            '                                          txtRemark.Text, _
                            '                                          enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                        objProcessLoan.Send_Notification_Employee(mintEmployeeunkid, _
                                                                  mintProcesspendingloanunkid, _
                                                                  clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                                                  clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CInt(Session("CompanyUnkId")), _
                                                                  txtRemark.Text, _
                                                                  enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                            'Sohail (30 Nov 2017) -- End
                    End If
                    
                End If
                If Request.QueryString.Count > 0 Then
                    Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
                Else
                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approval_Process/wPg_LoanApprovalList.aspx", False)
            End If
                'Shani (21-Jul-2016) -- End
                End If
                'Nilay (10-Dec-2016) -- End

                'Hemant (18 Aug 2023) -- Start
                'ENHANCEMENT(ZSSF): A1X-1194 - Real time posting of approved loans and leave expenses to Navision
                If CInt(Session("EFTIntegration")) = enEFTIntegration.DYNAMICS_NAVISION Then
                    objProcessLoan._Processpendingloanunkid = mintProcesspendingloanunkid
                    If objProcessLoan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED Then
                        Dim objPeriod As New clscommom_period_Tran

                        objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboDeductionPeriod.SelectedValue)
                        objProcessLoan.SendToDynamicNavision(mintProcesspendingloanunkid, CInt(Session("UserId")), objPeriod._Start_Date, objPeriod._End_Date, _
                                                             CStr(Session("SQLDataSource")), _
                                                             CStr(Session("SQLDatabaseName")), _
                                                             CStr(Session("SQLDatabaseOwnerName")), _
                                                             CStr(Session("SQLUserName")), _
                                                             CStr(Session("SQLUserPassword")), _
                                                             CStr(Session("FinancialYear_Name")))

                        objPeriod = Nothing
                    End If

                End If
                'Hemant (18 Aug 2023) -- End

                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                If Request.QueryString.Count > 0 Then
                    Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
                Else
                    Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approval_Process/wPg_LoanApprovalList.aspx", False)
                End If
                'Sohail (29 Apr 2019) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSave_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            'Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approval_Process/wPg_LoanApprovalList.aspx", False)
            If Request.QueryString.Count > 0 Then
                Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
            Else
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approval_Process/wPg_LoanApprovalList.aspx", False)
            End If
            'Shani (21-Jul-2016) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ComboBox Events"

    Protected Sub cboDeductionPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDeductionPeriod.SelectedIndexChanged
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboDeductionPeriod.SelectedValue)
                Language.setLanguage(mstrModuleName)
                objvalPeriodDuration.Text = Language.getMessage(mstrModuleName, 11, "Period Duration From :") & " " & objPeriod._Start_Date.Date & " " & Language.getMessage(mstrModuleName, 12, "To") & " " & objPeriod._End_Date.Date
                objPeriod = Nothing
            Else
                objvalPeriodDuration.Text = ""
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboDeductionPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboDeductionPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "TextBox Events"

    Protected Sub txtLoanAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLoanAmt.TextChanged
        Try
            'S.SANDEEP [09-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#204|#ARUTI-103}
            'If CDec(txtLoanAmt.Text) > 0 AndAlso CDec(txtInstallmentAmt.Text) > 0 Then
            '    txtEMIInstallments.Text = CInt(Math.Ceiling(CDec(txtLoanAmt.Text) / CDec(txtInstallmentAmt.Text))).ToString
            'End If

            If CDec(txtLoanAmt.Text.Trim.Length) > 0 AndAlso CInt(txtEMIInstallments.Text) > 0 Then
                txtInstallmentAmt.Text = CInt(Math.Ceiling(CDec(txtLoanAmt.Text) / CDec(txtEMIInstallments.Text))).ToString()
            End If
            txtInstallmentAmt.Text = CDec(txtInstallmentAmt.Text).ToString("############0.00#")
            'S.SANDEEP [09-May-2018] -- END
            txtLoanAmt.Text = CDec(txtLoanAmt.Text).ToString("############0.00#")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtLoanAmt_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("txtLoanAmt_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtInstallmentAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            If CDec(txtLoanAmt.Text) > 0 AndAlso CDec(txtInstallmentAmt.Text) > 0 Then
                txtEMIInstallments.Text = CDec(CDec(txtLoanAmt.Text) / CDec(txtInstallmentAmt.Text)).ToString
            Else
                txtEMIInstallments.Text = "0"
            End If
            mdecInstallmentAmt = CDec(txtInstallmentAmt.Text)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtInstallmentAmt_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("txtInstallmentAmt_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtEMIInstallments_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEMIInstallments.TextChanged
        Try
            If CDec(txtLoanAmt.Text) > 0 AndAlso CDec(txtEMIInstallments.Text) > 0 Then
                mdecInstallmentAmt = CDec(CDec(txtLoanAmt.Text) / CDec(txtEMIInstallments.Text))
                txtInstallmentAmt.Text = Format(CDec(txtLoanAmt.Text) / CDec(txtEMIInstallments.Text), Session("fmtCurrency").ToString)
            Else
                mdecInstallmentAmt = CDec(txtLoanAmt.Text)
                txtInstallmentAmt.Text = Format(CDec(txtLoanAmt.Text), Session("fmtCurrency").ToString)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtEMIInstallments_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("txtEMIInstallments_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region
    
    'Nilay (27-Dec-2016) -- Start
#Region " Radio Button's Events "
    Protected Sub radLoan_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radLoan.CheckedChanged, radAdvance.CheckedChanged
        Try
            If radLoan.Checked Then
                cboLoanScheme.Enabled = True
                txtInstallmentAmt.Enabled = True
                txtEMIInstallments.Enabled = True
            Else
                txtInstallmentAmt.Enabled = False
                txtEMIInstallments.Enabled = False
                cboLoanScheme.Enabled = False
                cboLoanScheme.SelectedValue = "0"
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("radLoan_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("radLoan_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region
    'Nilay (27-Dec-2016) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    
    Private Sub SetLanguage()
        Try
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, lblPageHeader.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbProcessPendingLoanInfo", Me.lblDetialHeader.Text)
            Me.lblDuration.Text = Language._Object.getCaption(Me.lblDuration.ID, Me.lblDuration.Text)
            Me.lblDeductionPeriod.Text = Language._Object.getCaption(Me.lblDeductionPeriod.ID, Me.lblDeductionPeriod.Text)
            Me.lblEMIInstallments.Text = Language._Object.getCaption(Me.lblEMIInstallments.ID, Me.lblEMIInstallments.Text)
            Me.lblEMIAmount.Text = Language._Object.getCaption(Me.lblEMIAmount.ID, Me.lblEMIAmount.Text)
            Me.lblApproverRemarks.Text = Language._Object.getCaption(Me.lblApproverRemarks.ID, Me.lblApproverRemarks.Text)
            Me.lblApplicationDate.Text = Language._Object.getCaption(Me.lblApplicationDate.ID, Me.lblApplicationDate.Text)
            Me.lblApplicationNo.Text = Language._Object.getCaption(Me.lblApplicationNo.ID, Me.lblApplicationNo.Text)
            Me.lblAmt.Text = Language._Object.getCaption(Me.lblAmt.ID, Me.lblAmt.Text)
            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.radAdvance.Text = Language._Object.getCaption(Me.radAdvance.ID, Me.radAdvance.Text)
            Me.radLoan.Text = Language._Object.getCaption(Me.radLoan.ID, Me.radLoan.Text)
            Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.ID, Me.lblLoanAdvance.Text)
            Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblExternalEntity.Text = Language._Object.getCaption(Me.lblExternalEntity.ID, Me.lblExternalEntity.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.ID, Me.lblApprover.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError("SetLanguage:- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region 'Language & UI Settings
    '</Language>

End Class
