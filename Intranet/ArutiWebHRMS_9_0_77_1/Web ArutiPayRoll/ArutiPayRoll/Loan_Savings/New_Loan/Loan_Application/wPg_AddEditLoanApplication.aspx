﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_AddEditLoanApplication.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Application_wPg_AddEditLoanApplication"
    Title="Add/Edit Loan Application" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 70%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Loan Application"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Loan Application Info"></asp:Label>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 12.5%"><asp:Label ID="lblApplicationNo" runat="server" Text="Application No."></asp:Label></td>
                                        <td style="width: 12.5%"><asp:TextBox ID="txtApplicationNo" runat="server" Enabled="false"></asp:TextBox></td>
                                        <td colspan="2"><asp:Label ID="lblApplicationDate" runat="server" Text="Date" style="padding-right:31px"></asp:Label><uc1:DateCtrl ID="dtpApplicationDate" runat="server" AutoPostBack="false" /></td>
                                        <%--<td style="width: 12.5%"></td>--%>
                                        <td style="width: 12.5%"><asp:Label ID="lblLoanCalcType" runat="server" Text="Loan Calc. Type"></asp:Label></td>
                                        <td colspan="3"><asp:DropDownList ID="cboLoanCalcType" runat="server" 
                                                Enabled="False" AutoPostBack="true"></asp:DropDownList></td>                                        
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 12.5%"><asp:Label ID="lblEmpName" runat="server" Text="Employee"></asp:Label></td>
                                        <td colspan="3"><asp:DropDownList ID="cboEmpName" runat="server"></asp:DropDownList></td>                                        
                                        <td style="width: 12.5%"><asp:Label ID="lblInterestCalcType" runat="server" Text="Int. Calc. Type"></asp:Label></td>
                                        <td colspan="3"><asp:DropDownList ID="cboInterestCalcType" runat="server" AutoPostBack="true"
                                                Enabled="False"></asp:DropDownList></td> 
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 12.5%"><asp:Label ID="lblLoanAdvance" runat="server" Text="Select"></asp:Label></td>
                                        <td style="width: 12.5%"><asp:RadioButton ID="radLoan" runat="server" Text="Loan" GroupName="LoanAdvance" AutoPostBack="true" Checked="true" /></td>
                                        <td style="width: 12.5%"><asp:RadioButton ID="radAdvance" runat="server" Text="Advance" GroupName="LoanAdvance" AutoPostBack="true" /></td>
                                        <td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"><asp:Label ID="lblMaxInstallmentAmt" runat="server" Text="Max INSTL Amt."></asp:Label></td></td>
                                        <td colspan="3"><asp:TextBox ID="txtMaxInstallmentAmt" runat="server" onkeypress="return onlyNumbers(this, event);" Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly="true"></asp:TextBox></td>                                        
                                     </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"></td>
                                        <td colspan="4"><asp:Label ID="lnProjectedAmount" runat="server" Text ="Projected Amount" Font-Bold="true"></asp:Label></td> 
                                    </tr>
                                    <tr style="width: 100%">
                                    <td style="width: 12.5%"><asp:Label ID="lblDeductionPeriod" runat="server" Text="Deduction Period"></asp:Label></td>
                                        <td colspan="3"><asp:DropDownList ID="cboDeductionPeriod" runat="server" AutoPostBack="true"></asp:DropDownList></td>                                                                                
                                        <td style="width: 12.5%"><asp:Label ID="lblInterestAmt" runat="server" Text="Interest Amount"></asp:Label></td>
                                        <td colspan="3"><asp:TextBox ID="txtInterestAmt" runat="server" ReadOnly="true" style="text-align:right" Width="99%"></asp:TextBox></td>                                        
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 12.5%"><asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme"></asp:Label></td>
                                        <td colspan="3"><asp:DropDownList ID="cboLoanScheme" runat="server" AutoPostBack="True"></asp:DropDownList></td>                                        
                                        <td style="width: 12.5%"><asp:Label ID="lblNetAmount" runat="server" Text="Net Amount"></asp:Label></td>
                                        <td colspan="3"><asp:TextBox ID="txtNetAmount" runat="server" ReadOnly="true" style="text-align:right" Width="99%"></asp:TextBox></td>
                                    </tr>
                                   
                                    <tr style="width: 100%">
                                        <td style="width: 12.5%"><asp:Label ID="lblAmt" runat="server" Text="Amount"></asp:Label></td>
                                        <td style="width: 12.5%"><asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                                        <td colspan="2"><asp:TextBox ID="txtLoanAmt" runat="server" onkeypress="return onlyNumbers(this, event);" Style="text-align: right" Text="0.0" AutoPostBack="true"></asp:TextBox></td>
                                        <td colspan="4"><asp:Label ID="elProjectedINSTLAmt" runat="server" Text ="Projected Installment Amount (First Installment)" Font-Bold="true"></asp:Label></td> 
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 12.5%"><asp:Label ID="lblLoanAccNumber" runat="server" Text="Loan Account Number."></asp:Label></td>
                                        <td colspan="3"><asp:TextBox ID="txtLoanAccNumber" runat="server" ></asp:TextBox></td>
                                        <td style="width: 12.5%"><asp:Label ID="lblLoanInterest" runat="server" Text="Rate (%)"></asp:Label></td>
                                        <td style="width: 12.5%"><asp:TextBox ID="txtLoanRate" runat="server" onkeypress="return onlyNumbers(this, event);" Style="text-align: right" Text="0.0" AutoPostBack="true"></asp:TextBox></td>
                                        <td style="width: 12.5%"><asp:Label ID="lblEMIInstallments" runat="server" Text="INSTL(In Months)"></asp:Label></td>
                                        <td style="width: 12.5%"><asp:TextBox ID="txtEMIInstallments" AutoPostBack="true" Text="1" Style="text-align: right" runat="server" onKeypress="return onlyNumbers(this, event);"></asp:TextBox></td>
                                     </tr>
                                     <tr style="width: 100%">
                                        <td style="width: 12.5%"><asp:Label ID="lblEmpRemark" runat="server" Text="Employee Remark"></asp:Label></td>
                                        <td colspan="3" rowspan="3" valign="top"><asp:TextBox ID="txtEmployeeRemark" TextMode="MultiLine" Rows="6" runat="server"></asp:TextBox></td>                                        
                                        <%--<td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"></td>--%>
                                        <td style="width: 12.5%"><asp:Label ID="lblPrincipalAmt" runat="server" Text="Principal Amt."></asp:Label></td>
                                        <td style="width: 12.5%"><asp:TextBox ID="txtPrincipalAmt" runat="server" onkeypress="return onlyNumbers(this, event);" Style="text-align: right" Text="0.0" AutoPostBack="true" ReadOnly=true></asp:TextBox></td>
                                        <td style="width: 12.5%"><asp:Label ID="lblIntAmt" runat="server" Text="Interest Amt."></asp:Label></td>
                                        <td style="width: 12.5%"><asp:TextBox ID="txtIntAmt" runat="server" 
                                                onkeypress="return onlyNumbers(this, event);" Style="text-align: right" 
                                                Text="0.0" AutoPostBack="true" ReadOnly="True"></asp:TextBox></td>
                                     </tr>
                                     
                                     <tr style="width: 100%">
                                        <td style="width: 12.5%"></td>
                                        <%--<td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"></td>--%>
                                        <td style="width: 12.5%"><asp:Label ID="lblEMIAmount" runat="server" Text="INSTL Amt."></asp:Label></td></td>
                                        <td colspan="3"><asp:TextBox ID="txtInstallmentAmt" runat="server" onkeypress="return onlyNumbers(this, event);" Style="text-align: right" Text="0.0" AutoPostBack="true"></asp:TextBox></td>                                        
                                     </tr>
                                     <tr style="width: 100%">
                                        <td style="width: 12.5%"><asp:Label ID="lblExternalEntity" runat="server" Text="Is External Entity" Visible="false"></asp:Label></td>
                                        <td style="width: 12.5%"><asp:CheckBox ID="chkExternalEntity" runat="server" Checked="True" AutoPostBack="true" Visible="false" /></td>
                                        <td style="width: 12.5%"><asp:TextBox ID="txtExternalEntity" runat="server" Visible="False"></asp:TextBox></td>
                                        <td style="width: 12.5%"><asp:Label ID="lblDuration" runat="server" Text="Duration In Months" Visible="false"></asp:Label></td>
                                        <td style="width: 12.5%"><asp:TextBox ID="txtDurationInMths" CssClass="numberonly" Visible="false" runat="server"
                                                                onkeypress="return onlyNumbers(this, event);" Style="text-align: right; background-color: White"
                                                                Text="0"></asp:TextBox></td>
                                        <td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"></td>
                                     </tr>
                                     <tr style="width: 100%">
                                        <td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"><asp:Label ID="lblEMIAmount1" runat="server" Text="Installment Amt." Visible="false"></asp:Label></td>
                                        <td style="width: 12.5%"><asp:TextBox ID="txtInstallmentAmt1" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                    AutoPostBack="true" Style="text-align: right" Text="0.0" Visible="false"></asp:TextBox></td>
                                        <td style="width: 12.5%"><asp:Label ID="lblEMIInstallments1" runat="server" Text="No. of Installments" Visible="false"></asp:Label></td>
                                        <td style="width: 12.5%"><asp:TextBox ID="txtEMIInstallments1" runat="server" CssClass="numberonly" onkeypress="return onlyNumbers(this, event);"
                                                                AutoPostBack="true" Style="text-align: right; background-color: White" Text="0" Visible="false"></asp:TextBox></td>
                                        <td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"></td>
                                        <td style="width: 12.5%"></td>
                                     </tr>
                                </table>
                                <div class="btn-default">
                                        <div id="caption" runat="server" style="float:left"><asp:Label ID="objvalPeriodDuration" runat="server" Text="#periodDuration" style="display:block;" Visible="false"></asp:Label><asp:Label ID="objlblExRate" runat="server" style="float:left;" Text=""></asp:Label></div><asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                            </div>
                        </div>
                        <uc2:Cnf_YesNo ID="cnftopup" runat="server" Title="Aruti" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <%--<asp:Panel ID="MainPan" runat="server" Style="width: 70%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Loan Application"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Loan Application Info"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblApplicationNo" runat="server" Text="Application No."></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox ID="txtApplicationNo" runat="server" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td style="width: 12%">
                                            </td>
                                            <td style="width: 53%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%">
                                                        </td>
                                                        <td style="width: 40%">
                                                            <asp:Label ID="lblApplicationDate" runat="server" Text="Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc1:DateCtrl ID="dtpApplicationDate" runat="server" Width="90" AutoPostBack="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmpName" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboEmpName" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblDeductionPeriod" runat="server" Text="Deduction Period"></asp:Label>
                                            </td>
                                            <td colspan="3" style="width: 80%">
                                                <asp:DropDownList ID="cboDeductionPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblLoanAdvance" runat="server" Text="Select"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <table style="width: 30%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 50%" align="left">
                                                            <asp:RadioButton ID="radLoan" runat="server" Text="Loan" GroupName="LoanAdvance"
                                                                AutoPostBack="true" Checked="true" />
                                                        </td>
                                                        <td style="width: 50%" align="left">
                                                            <asp:RadioButton ID="radAdvance" runat="server" Text="Advance" GroupName="LoanAdvance"
                                                                AutoPostBack="true" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblExternalEntity" runat="server" Text="Is External Entity" Visible="false"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 5%">
                                                            <asp:CheckBox ID="chkExternalEntity" runat="server" Checked="True" AutoPostBack="true"
                                                                Visible="false" />
                                                        </td>
                                                        <td style="width: 95%">
                                                            <asp:TextBox ID="txtExternalEntity" runat="server" Visible="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboLoanScheme" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblAmt" runat="server" Text="Amount"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox ID="txtLoanAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right" Text="0.0" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:DropDownList ID="cboCurrency" runat="server" >
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 53%" align="right">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%">
                                                        </td>
                                                        <td style="width: 50%">
                                                            <asp:Label ID="lblDuration" runat="server" Text="Duration In Months" Visible="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%">
                                                            <asp:TextBox ID="txtDurationInMths" CssClass="numberonly" Visible="false" runat="server"
                                                                onkeypress="return onlyNumbers(this, event);" Style="text-align: right; background-color: White"
                                                                Text="0"></asp:TextBox>
                                                        </td>
                                                                                                             
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                &nbsp;</td>
                                            <td style="width: 80%" colspan="3">
                                                &nbsp;</td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:Label ID="objvalPeriodDuration" runat="server" Text="#periodDuration" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEMIAmount" runat="server" Text="Installment Amt."></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox ID="txtInstallmentAmt" runat="server" onkeypress="return onlyNumbers(this, event);"
                                                    AutoPostBack="true" Style="text-align: right" Text="0.0"></asp:TextBox>
                                            </td>
                                            <td style="width: 12%">
                                            </td>
                                            <td style="width: 53%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%">
                                                        </td>
                                                        <td style="width: 40%">
                                                            <asp:Label ID="lblEMIInstallments" runat="server" Text="No. of Installments"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <asp:TextBox ID="txtEMIInstallments" runat="server" CssClass="numberonly" onkeypress="return onlyNumbers(this, event);"
                                                                AutoPostBack="true" Style="text-align: right; background-color: White" Text="0"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%" valign="top">
                                                <asp:Label ID="lblEmpRemark" runat="server" Text="Employee Remark"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:TextBox ID="txtEmployeeRemark" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <uc2:Cnf_YesNo ID="cnftopup" runat="server" Title="Aruti" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>--%>
    </center>
</asp:Content>
<%--<td style="width: 5%" align="right">
<cc1:NumericUpDownExtender ID="nudDurationInMths" TargetControlID="txtDurationInMths"
runat="server" Width="85" Minimum="0" Maximum="85">
</cc1:NumericUpDownExtender>
</td>--%>