﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_Medicalclaim.aspx.vb"
    Inherits="Medical_wPg_Medicalclaim" MasterPageFile="~/home.master" Title="Medical Claim" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <%--    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
    }
    </script>--%>

    <script type="text/javascript">

    function onlyNumbers(txtBox, e) {
        //        var e = event || evt; // for trans-browser compatibility
        //        var charCode = e.which || e.keyCode;
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
                if (charCode == 46)
            if (cval.indexOf(".") > -1)
                    return false;

        if (charCode == 13)
            return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

        return true;
    }
  
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Medical Claim"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblInstutution" runat="server" Text="Service Provider" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="drpProvider" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Pay Period" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="drpPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblSickSheetNo" runat="server" Text="Sick Sheet No" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtSickSheetNo" runat="server" AutoPostBack="true" />
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblDependants" runat="server" Text="Dependants" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="drpdepedants" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 32%" colspan="2">
                                                <asp:CheckBox ID="chkEmpexempted" runat="server" Text="Exempt From Deduction" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblClaimdate" runat="server" Text="Claim Date" />
                                            </td>
                                            <td style="width: 22%">
                                                <uc2:DateCtrl ID="dtClaimdate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblClaimno" runat="server" Text="Claim No" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtClaimno" runat="server" />
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblClaimAmount" runat="server" Text="Claim Amount" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtclaimAmount" AutoPostBack="true" runat="server" Style="text-align: right;"
                                                    Text="0" onKeypress="return onlyNumbers(this, event);" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="LblInvoiceDate" runat="server" Text="Invoice Date" />
                                            </td>
                                            <td style="width: 22%">
                                                <uc2:DateCtrl ID="dtpInvoiceDate" runat="server" AutoPostBack="true" />
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblRemarks" runat="server" Text="Remarks" />
                                            </td>
                                            <td style="width: 55%" rowspan="2" colspan="3">
                                                <asp:TextBox ID="txtRemark"  runat="server" TextMode="MultiLine" Rows="3" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblInvoiceno" runat="server" Text="Invoice No" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtInvoiceNo" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblTotInvoiceAmt" runat="server" Text="Total Invoice Amount" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtTotInvoiceAmt" AutoPostBack="true" runat="server" Style="text-align: right;"
                                                    Text="0" onKeypress="return onlyNumbers(this, event);" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnImportData" runat="server" CssClass="btndefault" Text="Import" />
                                        <asp:Button ID="btnAdd" runat="server" CssClass="btndefault" Text="Add" />
                                        <asp:Button ID="btnEdit" runat="server" CssClass="btndefault" Text="Edit" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="btndefault" Text="Delete" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div4" class="panel-default">
                                <div id="Div5" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="3">
                                                <asp:Panel ID="pnl_GvMedicalClaim" ScrollBars="Auto" Height="200px" runat="server">
                                                    <asp:GridView ID="GvMedicalClaim" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" SelectImageUrl="~/images/edit.png"
                                                                ShowSelectButton="True" />
                                                            <asp:BoundField DataField="employeename" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee" />
                                                            <asp:BoundField DataField="dependentsname" HeaderText="Depedant" ReadOnly="True"
                                                                FooterText="colhDependants" />
                                                            <asp:BoundField DataField="sicksheetno" HeaderText="Sick Sheet No" ReadOnly="True"
                                                                FooterText="colhSickSheetNo" />
                                                            <asp:BoundField DataField="claimdate" HeaderText="Claim Date" ReadOnly="True" FooterText="colhClaimDate" />
                                                            <asp:BoundField DataField="claimno" HeaderText="Claim No" ReadOnly="True" FooterText="colhClaimNo" />
                                                            <asp:BoundField DataField="amount" HeaderText="Claim Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                                                                ReadOnly="True" FooterText="colhAmount" />
                                                            <asp:BoundField DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                            <asp:BoundField DataField="isempexempted" HeaderText="Isempexempted" ReadOnly="True"
                                                                Visible="false" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" align="right">
                                                <div align="right">
                                                    <asp:Label ID="lblTotAmt" runat="server" Style="margin-right: 5px" Text="Total Amount"></asp:Label>
                                                    <asp:TextBox ID="txtTotalAmount" runat="server" Width="20%" ReadOnly="true" Style="text-align: right;"
                                                        Text="0" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btndefault" Text="Save" />
                                        <asp:Button ID="btnFinalSave" runat="server" CssClass="btndefault" Text="Final Save" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="modalBackground"
                        TargetControlID="lblImportEmployeeCode" runat="server" PopupControlID="pnlpopup"
                        DropShadow="true" CancelControlID="btnImportCancel" />
                    <asp:Panel ID="pnlpopup" runat="server" CssClass="newpopup" Style="display: none;
                        width: 500px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="LblText" runat="server" Text="To Import Medical Claim :"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div6" class="panel-default">
                                    <div id="Div7" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="Label2" runat="server" Text="To Import Medical Claim :"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div8" class="panel-body-default">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            <asp:Label ID="LblFile" runat="server" Text="Choose File " />
                                                        </td>
                                                        <td style="width: 55%">
                                                            <asp:FileUpload ID="FlUpload" runat="server" />
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Button ID="BtnGet" Text="Ok" CssClass="btndefault" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="BtnGet" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <table width="100%">
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:Label ID="lblImportEmployeeCode" runat="server" Text="Employee Code " />
                                                </td>
                                                <td style="width: 70%">
                                                    <asp:DropDownList ID="drpImportEmployeeCode" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:Label ID="lblImportEmployee" runat="server" Text="Employee " />
                                                </td>
                                                <td style="width: 70%">
                                                    <asp:DropDownList ID="drpImportEmployee" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:Label ID="lblImportDepedant" runat="server" Text="Depedant " />
                                                </td>
                                                <td style="width: 70%">
                                                    <asp:DropDownList ID="drpImportDependant" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:Label ID="lblImportSicksheet" runat="server" Text="Sick Sheet " />
                                                </td>
                                                <td style="width: 70%">
                                                    <asp:DropDownList ID="drpImportSickSheet" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:Label ID="lblImportClaimNo" runat="server" Text="Claim No " />
                                                </td>
                                                <td style="width: 70%">
                                                    <asp:DropDownList ID="drpImportClaimNo" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:Label ID="lblImportClaimDate" runat="server" Text="Claim Date " />
                                                </td>
                                                <td style="width: 70%">
                                                    <asp:DropDownList ID="drpImportClaimDate" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:Label ID="lblImportClaimAmount" runat="server" Text="Claim Amount " />
                                                </td>
                                                <td style="width: 70%">
                                                    <asp:DropDownList ID="drpImportClaimAmount" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:Label ID="LblEmpExempted" runat="server" Text="Exempt From Deduction" />
                                                </td>
                                                <td style="width: 70%">
                                                    <asp:DropDownList ID="drpImportExemptFromDeduction" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnImportOk" Text="Import" CssClass="btndefault" runat="server" />
                                            <asp:Button ID="btnImportCancel" Text="Cancel" CssClass="btndefault" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
