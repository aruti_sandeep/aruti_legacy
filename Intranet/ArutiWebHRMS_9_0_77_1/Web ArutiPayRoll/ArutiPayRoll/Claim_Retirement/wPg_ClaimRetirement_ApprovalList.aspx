<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_ClaimRetirement_ApprovalList.aspx.vb"
    Inherits="Claim_Retirement_wPg_ClaimRetirement_ApprovalList" Title="Claim Retirement Approval List" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="ComboList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/status.ascx" TagName="Status" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Claim Retirement Approval List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="float: right">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" CssClass="lnkhover"
                                            Visible="false"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkReset" runat="server" Text="Reset" CssClass="lnkhover" Visible="false"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 9%">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 28%">
                                            <asp:DropDownList ID="cboEmployee" runat="server" />
                                        </div>
                                        <div class="ib" style="width: 9%">
                                            <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 15%">
                                            <uc2:DateCtrl ID="dtpFDate" AutoPostBack="false" runat="server" />
                                        </div>
                                        <div class="ib" style="width: 9%">
                                            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 22%">
                                            <asp:DropDownList ID="cboStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 9%">
                                            <asp:Label ID="lblRetirementNo" runat="server" Text="Retirement No"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 28%">
                                            <asp:TextBox ID="txtRetirementNo" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="ib" style="width: 9%">
                                            <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 15%">
                                            <uc2:DateCtrl ID="dtpTDate" AutoPostBack="false" runat="server" />
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 9%">
                                            <asp:Label ID="lblRetirementApprover" runat="server" Text="Approver"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 28%">
                                            <asp:TextBox ID="txtRetirementApprover" runat="server" Enabled="false"></asp:TextBox>
                                        </div>
                                        <div class="ib" style="width: 50%">
                                            <asp:CheckBox ID="ChkMyApprovals" runat="server" Text="My Approvals" Checked="true" />
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnSearch" CssClass="btndefault" runat="server" Text="Search" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                        <asp:Panel ID="pnl_lvClaimRetirementList" ScrollBars="Auto" Height="300px" runat="server">
                                            <asp:DataGrid ID="lvClaimRetirementList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"  DataKeyField = "claimretirementunkid"
                                                AllowPaging="true" HeaderStyle-Font-Bold="false" Width="99.5%" PageSize = "500" PagerStyle-HorizontalAlign="Left" 
                                                PagerStyle-Position="Top" PagerStyle-Wrap="true" PagerStyle-Mode="NumericPages" >
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="70px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Left" HeaderText = "Approval" FooterText="btnChangeStatus">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc" style="padding:0 25px; display:block">
                                                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Approval" ToolTip ="Change Approval Status"
                                                                    CssClass="lnkhover">
                                                                      <img src="../images/change_status.png" height="20px" width="20px" alt="Change Approval Status" />
                                                                 </asp:LinkButton>
                                                            </span>  
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="claimretirementno" HeaderText="Retirement No" ReadOnly="true"
                                                        FooterText="dgcolhretirementno" />
                                                    <asp:BoundColumn DataField="claimrequestno" HeaderText="Claim No" ReadOnly="true"
                                                        FooterText="dgcolhclaimno" />
                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Emp. Code" ReadOnly="true"
                                                        FooterText="dgcolhEmployeecode" />
                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="true" FooterText="dgcolhemployee" />
                                                    <asp:BoundColumn DataField="approvername" HeaderText="Approver" ReadOnly="true" FooterText="dgcolhRetirementApprover" />
                                                    <asp:BoundColumn DataField="approvaldate" HeaderText="Approval Date" ReadOnly="true" FooterText="dgcolhapprovaldate" />
                                                    <asp:BoundColumn DataField="amount" HeaderText="Imprest Amount" ReadOnly="true" FooterText="dgcolhamount" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                    <%--'Pinkal (07-Nov-2019) -- Start
                                                    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.--%>
                                                    <asp:BoundColumn DataField="balance" HeaderText="Balance" ReadOnly="true" FooterText="dgcolhbalance" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                    <%--'Pinkal (07-Nov-2019) -- End--%>
                                                    
                                                    <asp:BoundColumn DataField="AppprovalStatus" HeaderText="Status" ReadOnly="true" FooterText="dgcolhStatus" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeunkId" ReadOnly="true" Visible="false"  FooterText = "objdgcolhemployeeunkid" />
                                                    <asp:BoundColumn DataField="approverunkid" HeaderText="approverunkid" ReadOnly="true" Visible="false" FooterText = "objdgcolhapproverunkid" />
                                                    <asp:BoundColumn DataField="approveremployeeunkid" HeaderText="approverEmpID" ReadOnly="true"  Visible="false" FooterText = "objdgcolhapproveremployeeunkid" />
                                                    <asp:BoundColumn DataField="statusunkid" HeaderText="Statusunkid" ReadOnly="true" Visible="false" FooterText = "objdgcolhstatusunkid" />
                                                    <asp:BoundColumn DataField="iscancel" HeaderText="IsCancel" ReadOnly="true" Visible="false" FooterText = "objdgcolhIsCancel" />
                                                    <asp:BoundColumn DataField="mapuserunkid" HeaderText="Map UserId" ReadOnly="true" visible="false" FooterText = "objdgcolhmapuserunkid" />
                                                    <asp:BoundColumn DataField="crpriority" HeaderText="Priority" ReadOnly="true" Visible="false" FooterText = "objdgcolhcrpriority" />
                                                    <asp:BoundColumn DataField="crmasterunkid" HeaderText="crmasterunkid" ReadOnly="true" Visible="false" FooterText = "objdgcolhcrmasterunkid" />
                                                    <asp:BoundColumn DataField="crlevelname" ReadOnly="true" Visible="false" FooterText = "objdgcolhcrlevelname"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="tdate" HeaderText="tdate" Visible="false" FooterText ="objcolhTranscationDate" />
                                                    <asp:BoundColumn DataField="isexternalapprover" Visible="false" FooterText= "objdgcolhisexternalapprover" />
                                                    <asp:BoundColumn DataField="IsGrp" Visible="false" FooterText = "objdgcolhIsGrp" />
                                                    <asp:BoundColumn DataField="claimretirementunkid" HeaderText="claimretirementunkid" ReadOnly="true" FooterText = "objdgcolhclaimretirementunkid" Visible="false" />
                                                    
                                                    <%--'Pinkal (07-Nov-2019) -- Start
                                                    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.--%>
                                                    <asp:BoundColumn DataField="claim_amount" HeaderText="Claim Amount" ReadOnly="true" FooterText="objdgcolhclaimamount" Visible ="false" />
                                                    <%--'Pinkal (07-Nov-2019) -- End--%>
                                                    
                                                      <%--'Pinkal (28-Oct-2021)-- Start
                                                      'Problem in Assigning Leave Accrue Issue.--%>
                                                    <asp:BoundColumn DataField="crlevelunkid" HeaderText="LevelId" ReadOnly="true" Visible="false" FooterText = "objdgcolhcrlevelunkid" />
                                                    <%--'Pinkal (28-Oct-2021)-- End--%>
                                                </Columns>
                                            </asp:DataGrid>
                                        </asp:Panel>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc5:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
