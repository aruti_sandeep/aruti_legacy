﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Rpt_AssessmentDoneReport
    Inherits Basepage

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentDoneReport"
    Private objAssessmetDone As clsAssessmentDoneReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim dsCombos As New DataSet
        Try
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "List", True)
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "employeename"
                    .DataSource = dsCombos.Tables("List")
                    .DataBind()
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee.Copy
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                End With
            End If
            

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo : " & ex.Message, Me)
        Finally
            ObjEmp = Nothing : ObjPeriod = Nothing : dsCombos = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            chkAssessorAssessment.Checked = False
            chkReviewerAssessment.Checked = False
            chkSelfAssessment.Checked = False
            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = String.Empty
            mdtStartDate = Nothing
            mdtEndDate = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError("ResetValue : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAssessmetDone.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Period is mandatory information. Please select period to continue"), Me)
                cboPeriod.Focus()
                Return False
            End If

            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                objAssessmetDone._ApplyUserAccessFilter = False
            End If
            objAssessmetDone._EmployeeId = CInt(cboEmployee.SelectedValue)
            objAssessmetDone._EmplyeeName = cboEmployee.SelectedItem.Text
            objAssessmetDone._IncludeAssessorAssessment = chkAssessorAssessment.Checked
            objAssessmetDone._IncludeReviewerAssessment = chkReviewerAssessment.Checked
            objAssessmetDone._IncludeSelfAssessment = chkSelfAssessment.Checked
            objAssessmetDone._ReviewerAssessmentCaption = chkReviewerAssessment.Text
            objAssessmetDone._SelfAssessmentCaption = chkSelfAssessment.Text
            objAssessmetDone._AssessorAssessmentCaption = chkAssessorAssessment.Text
            objAssessmetDone._PeriodId = CInt(cboPeriod.SelectedValue)
            objAssessmetDone._PeriodName = cboPeriod.SelectedItem.Text
            objAssessmetDone._ViewByIds = mstrStringIds
            objAssessmetDone._ViewIndex = mintViewIdx
            objAssessmetDone._ViewByName = mstrStringName
            objAssessmetDone._Analysis_Fields = mstrAnalysis_Fields
            objAssessmetDone._Analysis_Join = mstrAnalysis_Join
            objAssessmetDone._Analysis_OrderBy = mstrAnalysis_OrderBy
            objAssessmetDone._Report_GroupName = mstrReport_GroupName
            objAssessmetDone._Advance_Filter = mstrAdvanceFilter

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("SetFilter : " & ex.Message, Me)
        End Try
    End Function

#End Region

#Region " Forms "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objAssessmetDone = New clsAssessmentDoneReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                Call FillCombo()
                Call ResetValue()
            Else
                mstrStringIds = Me.ViewState("mstrStringIds")
                mstrStringName = Me.ViewState("mstrStringName")
                mintViewIdx = Me.ViewState("mintViewIdx")
                mstrAnalysis_Fields = Me.ViewState("mstrAnalysis_Fields")
                mstrAnalysis_Join = Me.ViewState("mstrAnalysis_Join")
                mstrAnalysis_OrderBy = Me.ViewState("mstrAnalysis_OrderBy")
                mstrReport_GroupName = Me.ViewState("mstrReport_GroupName")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender : " & ex.Message, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                If chkConsiderEmployeeTermination.Checked = True Then
                    Dim objPrd As New clscommom_period_Tran
                    objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    mdtStartDate = objPrd._Start_Date
                    mdtEndDate = objPrd._End_Date
                    objPrd = Nothing
                Else
                    mdtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                    mdtEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                End If
            Else
                mdtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                mdtEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("cboPeriod_SelectedIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click : " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReset_Click : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()

            If CInt(cboPeriod.SelectedValue) > 0 Then
                If chkConsiderEmployeeTermination.Checked = True Then
                    Dim objPrd As New clscommom_period_Tran
                    objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    mdtStartDate = objPrd._Start_Date
                    mdtEndDate = objPrd._End_Date
                    objPrd = Nothing
                Else
                    mdtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                    mdtEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                End If
            Else
                mdtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                mdtEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            End If

            objAssessmetDone._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objAssessmetDone._UserUnkId = CInt(Session("UserId"))
            objAssessmetDone.generateReportNew(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                mdtStartDate, _
                                                mdtEndDate, _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CStr(Session("ExportReportPath")), _
                                                CBool(Session("OpenAfterExport")), 0, enPrintAction.None, _
                                                enExportAction.None, _
                                                CInt(Session("Base_CurrencyId")))
            Session("objRpt") = objAssessmetDone._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnReport_Click : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            Dim objPeriod As New clscommom_period_Tran
            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkAnalysisBy_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError("popupAnalysisBy_buttonApply_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError("popupAnalysisBy_buttonClose_Click : " & ex.Message, Me)
        End Try
    End Sub

#End Region

End Class
