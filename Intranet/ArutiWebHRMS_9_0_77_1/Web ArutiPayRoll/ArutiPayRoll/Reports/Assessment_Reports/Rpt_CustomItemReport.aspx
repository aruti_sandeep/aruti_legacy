﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_CustomItemReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_CustomItemReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

<center>
    <asp:Panel ID="Panel1" runat="server" Style="width: 40%;">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
           <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Custom Item Value Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <%--'S.SANDEEP |09-FEB-2019| -- START--%>
                                        <%--'ISSUE/ENHANCEMENT : REPORT NOT COMING {CUSTOM ITEM REPORT}--%>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblReportType" runat="server" Text="Report Type"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="cboReportType" runat="server" Width="99%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <%--'S.SANDEEP |09-FEB-2019| -- END--%>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>                                            
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblItemGroup" runat="server" Text="Custom Header"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="cboCustomHeader" runat="server" Width="99%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%">
                                                <asp:Panel ID="Panel2" runat="server" Width="100%" Height="180px" ScrollBars="Auto">
                                                    <asp:DataGrid ID="lvCustomItem" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:TemplateColumn ItemStyle-Width="25">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkHeder1_CheckedChanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkbox1_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="Name" HeaderText="" />
                                                            <asp:BoundColumn DataField="Id" HeaderText="Id" Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <%--'S.SANDEEP |09-FEB-2019| -- START--%>
                                        <%--'ISSUE/ENHANCEMENT : REPORT NOT COMING {CUSTOM ITEM REPORT}--%>
                                        <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <%--'S.SANDEEP |09-FEB-2019| -- END--%>
                                        <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</center>
</asp:Content>
