﻿<%@ Page Title="Asset Declaration Status Report" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_EmpAssetDeclarationStatusT2_Report.aspx.vb" Inherits="Reports_Rpt_EmpAssetDeclarationStatusT2_Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 60%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Declaration Status Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right; display:none;">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" CssClass="lnkhover"
                                            Style="color: Blue; vertical-align: top"></asp:LinkButton>
                                    </div>
                                    <div style="text-align: right">
                                                <asp:LinkButton ID="lnkSetAnalysis" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                            </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblFinalcialYear" runat="server" Text="Financial Year"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboFinalcialYear" runat="server" AutoPostBack="True" Width="150px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblDate" runat="server" Text="As On Date"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <uc2:DateCtrl ID="dtpDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                       <tr style="width: 100%;">
                                            <td style="width: 20%">
                                                
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnAdvanceFilter" runat="server" CssClass="btndefault" Text="Advance Filter"  Visible= "false" />
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnExport" runat="server" CssClass="btndefault" Text="Export" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                            <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                            <uc7:AnalysisBy ID="popupAnalysisBy" runat="server" />
                            <uc9:Export runat="server" ID="Export" />
                        </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
