﻿Option Strict On 'Shani(11 Feb 2016)
#Region " Imports "
Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports
#End Region

Partial Class Reports_Rpt_EmployeeLeaveAbsenceSummary
    Inherits Basepage

#Region "Private Variables"
    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmEmployeeLeaveAbsenceSummaryReport"

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objEmpLeaveAbsence As New clsEmployeeLeaveAbsenceSummary
    'Pinkal (11-Sep-2020) -- End

#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End

            Call SetLanguage()

            If IsPostBack = False Then
                Call FillCombo()
                Call ResetValue()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load :- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"
    Public Sub FillCombo()
        Try
            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            'Pinkal (06-Dec-2019) -- Start
            'Enhancement SPORT PESA -  They needs to allow short leave to appear on screen even when "Show on ESS" is not selected.
            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            '    cboLeaveType.DataSource = (New clsleavetype_master).getListForCombo("LeaveType", True, , Session("Database_Name").ToString, "", True).Tables(0)
            'Else
            '    cboLeaveType.DataSource = (New clsleavetype_master).getListForCombo("LeaveType", True, , Session("Database_Name").ToString, "", False).Tables(0)
            'End If
                cboLeaveType.DataSource = (New clsleavetype_master).getListForCombo("LeaveType", True, , Session("Database_Name").ToString, "", False).Tables(0)
            'Pinkal (06-Dec-2019) -- End

            'Pinkal (25-May-2019) -- End

            cboLeaveType.DataValueField = "leavetypeunkid"
            cboLeaveType.DataTextField = "name"
            cboLeaveType.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo" & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboLeaveType.SelectedValue = "0"
            dtStartDateFrom.SetDate = DateTime.Now
            dtStartDateTo.SetDate = DateTime.Now

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objEmpLeaveAbsence.setDefaultOrderBy(0)
            'Pinkal (11-Sep-2020) -- End

            chkInactiveemp.Checked = False
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValues" & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValues" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByRef objEmpLeaveAbsence As clsEmployeeLeaveAbsenceSummary) As Boolean
        'Pinkal (11-Sep-2020) -- End
        Try

            'Pinkal (31-Jul-2020) -- Start
            'Optimization  - Working on Approve/Reject Update Progress in Assessment.	
            If dtStartDateFrom.GetDate = Nothing OrElse dtStartDateTo.GetDate = Nothing Then
                DisplayMessage.DisplayMessage("Please Select Proper date.", Me)
                Return False
            End If
            'Pinkal (31-Jul-2020) -- End


            objEmpLeaveAbsence.SetDefaultValue()
            objEmpLeaveAbsence._LeaveTypeId = CInt(cboLeaveType.SelectedValue)
            objEmpLeaveAbsence._LeaveTypeName = cboLeaveType.SelectedItem.Text
            objEmpLeaveAbsence._StartDateFrom = dtStartDateFrom.GetDate
            objEmpLeaveAbsence._StartDateTo = dtStartDateTo.GetDate
            objEmpLeaveAbsence._IncludeInactiveEmp = chkInactiveemp.Checked
            objEmpLeaveAbsence._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objEmpLeaveAbsence._UserUnkId = CInt(Session("UserId"))
            objEmpLeaveAbsence._UserAccessFilter = Session("AccessLevelFilterString").ToString()
            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter" & ex.Message, Me)
            DisplayMessage.DisplayError("SetFilter" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try

    End Function
#End Region

#Region "Button Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnClose_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objEmpLeaveAbsence As New clsEmployeeLeaveAbsenceSummary(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (11-Sep-2020) -- End
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If SetFilter() = False Then Exit Sub
            If SetFilter(objEmpLeaveAbsence) = False Then Exit Sub
            'Pinkal (11-Sep-2020) -- End




            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            objEmpLeaveAbsence.generateReportNew(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                 Session("ExportReportPath").ToString(), _
                                                 CBool(Session("OpenAfterExport")), _
                                                 0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))
            Session("objRpt") = objEmpLeaveAbsence._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            If Session("objRpt") IsNot Nothing Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(11-Feb-2016) -- End


            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objEmpLeaveAbsence = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Control Event"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.

    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("Closebotton1_CloseButton_click :- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End


#End Region

    Public Sub SetLanguage()
        Language.setLanguage(mstrModuleName)


        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        'Me.Title = Language._Object.getCaption(mstrModuleName, objEmpLeaveAbsence._ReportName)
        'Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, objEmpLeaveAbsence._ReportName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Pinkal (11-Sep-2020) -- End


        Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.ID, Me.lblLeaveType.Text)
        Me.lblStartDateFrom.Text = Language._Object.getCaption(Me.lblStartDateFrom.ID, Me.lblStartDateFrom.Text)
        Me.lblStartDateTo.Text = Language._Object.getCaption(Me.lblStartDateTo.ID, Me.lblStartDateTo.Text)
        Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)

        Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
        Me.btnReport.Text = Language._Object.getCaption(Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

    End Sub

End Class
