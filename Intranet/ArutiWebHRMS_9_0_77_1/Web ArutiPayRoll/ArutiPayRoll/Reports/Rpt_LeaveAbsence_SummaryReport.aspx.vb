﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region


Partial Class Reports_Rpt_LeaveAbsence_SummaryReport
    Inherits Basepage


#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Dim objLvAbsenceSummary As clsLeaveAbsence_Summary_Report
    'Pinkal (11-Sep-2020) -- End

    Private mstrModuleName As String = "frmLeaveAbsence_Summary_Report"
    Private mstrViewByIds As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysisFields As String = String.Empty
    Private mstrAnalysisJoin As String = String.Empty
    Private mstrAnalysisOrderBy As String = String.Empty
    Private mstrAnalysisOrderByGName As String = String.Empty
    Private mstrReportGroupName As String = String.Empty
    Private mdtPeriodStartDate As DateTime = Nothing
    Private mdtPeriodEndDate As DateTime = Nothing
    Private mintPeriodDays As Integer = 0

#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try

            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet



            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString(), CDate(Session("fin_startdate")).Date, "List", True, 0, False)

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objPeriod = Nothing

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = -1
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                 blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmployee = Nothing


            Dim objStatus As New clsMasterData
            dsList = objStatus.getLeaveStatusList("Status", Session("ApplicableLeaveStatus").ToString(), True, False)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "statusunkid <> 6", "", DataViewRowState.CurrentRows).ToTable()
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "name"
                .DataSource = dtTable
                .DataBind()
            End With
            objStatus = Nothing

            Dim objLeaveType As New clsleavetype_master
            dsList = objLeaveType.getListForCombo("Leave", False, 1, Session("Database_Name").ToString, "", False)
            With chkLeaveType
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objLeaveType = Nothing

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboPeriod.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            cboStatus.SelectedIndex = 0
            chkSelectAll.Checked = True
            chkSelectAll_CheckedChanged(chkSelectAll, New EventArgs())
            cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysisFields = ""
            mstrAnalysisJoin = ""
            mstrAnalysisOrderBy = ""
            mstrAnalysisOrderByGName = ""
            mstrReportGroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByRef objLvAbsenceSummary As clsLeaveAbsence_Summary_Report) As Boolean
        'Pinkal (11-Sep-2020) -- End
        Try
            objLvAbsenceSummary.SetDefaultValue()

            objLvAbsenceSummary._PeriodId = CInt(cboPeriod.SelectedValue)
            objLvAbsenceSummary._Period = cboPeriod.SelectedItem.Text
            objLvAbsenceSummary._FromDate = mdtPeriodStartDate.Date
            objLvAbsenceSummary._ToDate = mdtPeriodEndDate.Date
            objLvAbsenceSummary._PeriodDays = mintPeriodDays
            objLvAbsenceSummary._EmployeeID = CInt(cboEmployee.SelectedValue)
            objLvAbsenceSummary._EmployeeName = cboEmployee.SelectedItem.Text
            objLvAbsenceSummary._StatusId = CInt(cboStatus.SelectedValue)
            objLvAbsenceSummary._StatusName = cboStatus.SelectedItem.Text

            Dim mstrLeaveTypeName As String = ""
            objLvAbsenceSummary._LeaveIds = GetLeaveType(mstrLeaveTypeName)
            objLvAbsenceSummary._LeaveName = mstrLeaveTypeName


            objLvAbsenceSummary._ViewByIds = mstrViewByIds
            objLvAbsenceSummary._ViewIndex = mintViewIndex
            objLvAbsenceSummary._ViewByName = mstrViewByName
            objLvAbsenceSummary._Analysis_Fields = mstrAnalysisFields
            objLvAbsenceSummary._Analysis_Join = mstrAnalysisJoin
            objLvAbsenceSummary._Analysis_OrderBy = mstrAnalysisOrderBy
            objLvAbsenceSummary._Analysis_OrderBy_GName = mstrAnalysisOrderByGName
            objLvAbsenceSummary._Report_GroupName = mstrReportGroupName

            objLvAbsenceSummary._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objLvAbsenceSummary._OpenAfterExport = False
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objLvAbsenceSummary._CompanyUnkId = CInt(Session("CompanyUnkId"))

            objLvAbsenceSummary._UserUnkId = CInt(Session("UserId"))
            objLvAbsenceSummary._UserAccessFilter = CStr(Session("AccessLevelFilterString"))

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Function

    Private Sub SetLeaveType(ByVal blnchecked As Boolean)
        Try
            If chkLeaveType.Items.Count > 0 Then
                For i As Integer = 0 To chkLeaveType.Items.Count - 1
                    chkLeaveType.Items(i).Selected = blnchecked
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Function GetLeaveType(ByRef mstrLeaveTypeName As String) As String
        Dim mstrIDs As String = ""
        Try
            If chkLeaveType.Items.Count > 0 Then
                For i As Integer = 0 To chkLeaveType.Items.Count - 1
                    If chkLeaveType.Items(i).Selected = True Then
                        mstrIDs &= chkLeaveType.Items(i).Value & ","
                        mstrLeaveTypeName &= chkLeaveType.Items(i).Text & ","
                    End If
                Next
            End If
            If mstrIDs.Trim.Length > 0 Then
                mstrIDs = mstrIDs.Substring(0, mstrIDs.Trim.Length - 1)
                mstrLeaveTypeName = mstrLeaveTypeName.Substring(0, mstrLeaveTypeName.Trim.Length - 1)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        Return mstrIDs
    End Function


#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objLvAbsenceSummary = New clsLeaveAbsence_Summary_Report
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End


            SetLanguage()
            If Not IsPostBack Then
                Call FillCombo()
                ResetValue()
            Else
                mdtPeriodStartDate = CDate(Me.ViewState("PeriodStartDate"))
                mdtPeriodEndDate = CDate(Me.ViewState("PeriodEndDate"))
                mintPeriodDays = CInt(Me.ViewState("PeriodDays"))
                mstrViewByIds = Me.ViewState("mstrViewByIds").ToString()
                mstrViewByName = Me.ViewState("mstrViewByName").ToString()
                mintViewIndex = CInt(Me.ViewState("mintViewIndex"))
                mstrAnalysisFields = Me.ViewState("mstrAnalysisFields").ToString()
                mstrAnalysisJoin = Me.ViewState("mstrAnalysisJoin").ToString()
                mstrAnalysisOrderBy = Me.ViewState("mstrAnalysisOrderBy").ToString()
                mstrReportGroupName = Me.ViewState("mstrReportGroupName").ToString()
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                mstrAnalysisOrderByGName = Me.ViewState("mstrAnalysisOrderByGName").ToString()
                'Pinkal (11-Sep-2020) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("PeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("PeriodEndDate") = mdtPeriodEndDate
            Me.ViewState("PeriodDays") = mintPeriodDays
            Me.ViewState("mstrViewByIds") = mstrViewByIds
            Me.ViewState("mstrViewByName") = mstrViewByName
            Me.ViewState("mintViewIndex") = mintViewIndex
            Me.ViewState("mstrAnalysisFields") = mstrAnalysisFields
            Me.ViewState("mstrAnalysisJoin") = mstrAnalysisJoin
            Me.ViewState("mstrAnalysisOrderBy") = mstrAnalysisOrderBy
            Me.ViewState("mstrReportGroupName") = mstrReportGroupName

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Me.ViewState("mstrAnalysisOrderByGName") = mstrAnalysisOrderByGName
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLvAbsenceSummary As New clsLeaveAbsence_Summary_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (11-Sep-2020) -- End

        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            ElseIf chkLeaveType.SelectedItem Is Nothing Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please Select atleast one Leave Type."), Me)
                Exit Sub
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If SetFilter() = False Then Exit Sub
            If SetFilter(objLvAbsenceSummary) = False Then Exit Sub
            'Pinkal (11-Sep-2020) -- End


            SetDateFormat()
            objLvAbsenceSummary.Generate_DetailReport(Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  Session("EmployeeAsOnDate").ToString, _
                                                  Session("UserAccessModeSetting").ToString, True)

            If objLvAbsenceSummary._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objLvAbsenceSummary._FileNameAfterExported
                Export.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLvAbsenceSummary = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            SetLeaveType(chkSelectAll.Checked)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrViewByIds = popupAnalysisBy._ReportBy_Ids
            mstrViewByName = popupAnalysisBy._ReportBy_Name
            mintViewIndex = popupAnalysisBy._ViewIndex
            mstrAnalysisFields = popupAnalysisBy._Analysis_Fields
            mstrAnalysisJoin = popupAnalysisBy._Analysis_Join
            mstrAnalysisOrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReportGroupName = popupAnalysisBy._Report_GroupName

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            mstrAnalysisOrderByGName = popupAnalysisBy._Analysis_OrderBy_GName
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString()) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStartDate = objPeriod._Start_Date.Date
                mdtPeriodEndDate = objPeriod._End_Date.Date
                mintPeriodDays = objPeriod._Constant_Days
                objPeriod = Nothing
            Else
                mdtPeriodStartDate = ConfigParameter._Object._CurrentDateAndTime.Date
                mdtPeriodEndDate = ConfigParameter._Object._CurrentDateAndTime.Date
                mintPeriodDays = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.Title = Language._Object.getCaption(mstrModuleName, objLvAbsenceSummary._ReportName)
            'Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, objLvAbsenceSummary._ReportName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Pinkal (11-Sep-2020) -- End


            Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.ID, Me.LblPeriod.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.ID, Me.lblLeaveType.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.ID, Me.chkSelectAll.Text)

            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    
End Class
