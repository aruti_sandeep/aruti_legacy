﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_Employee_Listing.aspx.vb"
    Inherits="Reports_Rpt_Employee_Listing" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 73%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Listing Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <%--'Hemant (28 Apr 2020) -- Start--%>
                                    <%--'Enhancement - Analysis By on Employee Listing Report--%>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                    <%--'Hemant (28 Apr 2020) -- End--%>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative;">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="drpemployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblEmployeementType" runat="server" Text="Employement Type"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="drpEmployeementType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <%--'S.SANDEEP |15-JUL-2019| -- START--%>
                                        <%--'ISSUE/ENHANCEMENT : {ARUTI-757|Ref#0003859}--%>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 70%">
                                                <asp:CheckBox ID="chkGeneratePeriod" runat="server" Text="Generate Based on Period"
                                                    AutoPostBack="true" OnCheckedChanged="chkGeneratePeriod_CheckedChanged" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <%--'S.SANDEEP |15-JUL-2019| -- END--%>
                                        <%--'S.SANDEEP |06-Jan-2023| -- START--%>
                                        <%--'ISSUE/ENHANCEMENT : A1X-554--%>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 70%">
                                                            <asp:Label ID="gbEmpDisplayName" runat="server" Text="Employee Name Display Setting"
                                                                Font-Bold="true"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%" align="right">
                                                            <asp:LinkButton ID="lblSaveSelection" runat="server" Text="Save Setting" ToolTip="Save Setting"
                                                                Font-Bold="true">                                                        
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 70%">
                                                <asp:RadioButton AutoPostBack="true" runat="server" Text="Show Firstname, Surname & Othername  Separately"
                                                    ID="radShowAllSeparately" GroupName="NS" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 70%">
                                                <asp:RadioButton AutoPostBack="true" runat="server" Text="Show in desired format set, Please use #FName#, #OName#, #SName#"
                                                    ID="radShowInCombination" GroupName="NS" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 70%">
                                                <asp:TextBox ID="txtNameSetting" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 70%">
                                                <asp:Label ID="lblExample" runat="server" Text="Sample Format : #FName#-#SName# Put '-' in between"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 70%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <%--'S.SANDEEP |06-Jan-2023| -- END--%>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblBirthdateFrom" runat="server" Text="Birthdate From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpBirthdate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblBirthdateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpBirthdateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblAnniversaryFrom" runat="server" Text="Anniversary Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpAnniversaryDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblAnniversaryDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpAnniversaryDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblFirstAppointDateFrom" runat="server" Text="First Appointment Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpFAppointDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblFirstAppointDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpFAppointDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblAppointDateFrom" runat="server" Text="Appointment Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpAppointDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblAppointDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpAppointDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblConfirmationDateFrom" runat="server" Text="Confirmation Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpConfirmationDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblConfirmationDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpConfirmationDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblProbationStartDateFrom" runat="server" Text="Probation Start Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpProbationStartDateFrom" runat="server" AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblProbationStartDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpProbationStartDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblProbationEndDateFrom" runat="server" Text="Probation End Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpProbationEndDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblProbationEndDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpProbationEndDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblSuspendedStartDateFrom" runat="server" Text="Suspended Start Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpSuspendedStartDateFrom" runat="server" AutoPostBack="false">
                                                            </uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblSuspendedStartDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpSuspendedStartDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblSuspendedEndDateFrom" runat="server" Text="Suspended End Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpSuspendedEndDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblSuspendedEndDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpSuspendedEndDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblReinstatementDateFrom" runat="server" Text="Reinstatement Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpReinstatementFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblReinstatementDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpReinstatementTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblEOCDateFrom" runat="server" Text="End Of Contract Date From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpEOCDateFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblEOCDateTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpEOCDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="Label1" runat="server"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee"
                                                    Checked="false" CssClass="chkbx" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="Label2" runat="server"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:CheckBox ID="chkShowEmpScale" runat="server" Text="Show Employee Scale" Checked="false"
                                                    CssClass="chkbx" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 70%">
                                                            <asp:Label ID="gbColumns" runat="server" Text="Add/Remove Field(s)"
                                                                Font-Bold="true"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%" align="right">
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                
                                            </td>
                                            <td style="width: 70%">
                                                 <input type="text" id="txtSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                maxlength="50" class="form-control" onkeyup="FromSearching('txtSearch', '<%= dgvchkEmpfields.ClientID %>');" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                            </td>
                                            <td style="width: 70%">
                                                <div class="table-responsive" style="max-height: 170px; overflow: auto;">
                                                    <asp:GridView ID="dgvchkEmpfields" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        AllowPaging="false" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                        RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false" RowStyle-Wrap="false"
                                                        DataKeyNames="Id">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-CssClass="align-center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkSelectAll" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'chkSelect');" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" Text=" " CssClass="chk-sm" Checked='<%# Eval("IsChecked")%>' />
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="id" HeaderText="Id" ReadOnly="true" Visible="false"></asp:BoundField>
                                                            <asp:BoundField DataField="name" HeaderText="Field" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                FooterText="cohField">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <HeaderStyle Font-Bold="False" />
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnExport" runat="server" Text="Export" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--'Hemant (28 Apr 2020) -- Start--%>
                        <%--'Enhancement - Analysis By on Employee Listing Report--%>
                        <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                        <%--'Hemant (28 Apr 2020) -- End--%>
                    </div>
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
     <script type="text/javascript">

        function CheckAll(chkbox, chkgvSelect) {
            var grid = $(chkbox).closest(".table-responsive");
            $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));
        }

        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };
        
        function FromSearching(txtID, gridClientID) {
            if (gridClientID.toString().includes('%') == true)
                gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();

            if ($('#' + txtID).val().length > 0) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
               
                  $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();

            }
            else if ($('#' + txtID).val().length == 0) {
                resetFromSearchValue(txtID, gridClientID,);
            }
            if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue(txtID, gridClientID);
            }
        }

        function resetFromSearchValue(txtID, gridClientID) {


            $('#' + txtID).val('');
              $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
              $('.norecords').remove();            
            $('#' + txtID).focus();
        }
       
    </script>
</asp:Content>
