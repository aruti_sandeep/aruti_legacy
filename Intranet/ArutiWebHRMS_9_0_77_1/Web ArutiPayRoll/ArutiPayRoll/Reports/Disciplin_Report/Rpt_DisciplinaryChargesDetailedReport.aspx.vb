﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region
Partial Class Reports_Disciplin_Report_Rpt_DisciplinaryChargesDetailedReport
    Inherits Basepage

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplinaryChargesDetailedReport"
    Private objDisciplinaryChargesDetailReport As clsDisciplinaryChargesDetailReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private DisplayMessage As New CommonCodes
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objCategory As New clsCommon_Master
        Dim dsList As New DataSet
        Try
            dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "List", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            dsList = objCategory.getComboList(clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY, True, "list")
            With cboOffenceCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
            Call cboOffenceCategory_SelectedIndexChanged(New Object, New EventArgs())

            dsList = objCategory.getComboList(clsCommon_Master.enCommonMaster.DISC_REPONSE_TYPE, True, "list")
            With cboResponsetype
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            dsList.Dispose()
            objEmployee = Nothing
            objCategory = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboDisciplineType.SelectedValue = 0
            cboOffenceCategory.SelectedValue = 0
            dtpFromDate.SetDate = Nothing
            dtpToDate.SetDate = Nothing
            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = String.Empty
            dtpResponseFromDate.SetDate = Nothing
            dtpResponseToDate.SetDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objDisciplinaryChargesDetailReport.SetDefaultValue()

            objDisciplinaryChargesDetailReport._EmployeeId = cboEmployee.SelectedValue
            objDisciplinaryChargesDetailReport._EmployeeName = cboEmployee.SelectedItem.Text

            If dtpFromDate.IsNull = False AndAlso dtpToDate.IsNull = False Then
                objDisciplinaryChargesDetailReport._ChargeDateFrom = dtpFromDate.GetDate.Date
                objDisciplinaryChargesDetailReport._ChargeDateTo = dtpToDate.GetDate.Date
            End If

            If dtpResponseFromDate.IsNull = False AndAlso dtpResponseToDate.IsNull = False Then
                objDisciplinaryChargesDetailReport._ResponseDateFrom = dtpResponseFromDate.GetDate.Date
                objDisciplinaryChargesDetailReport._ResponseDateTo = dtpResponseToDate.GetDate.Date
            End If
            objDisciplinaryChargesDetailReport._OffenceCategory = cboOffenceCategory.SelectedItem.Text
            objDisciplinaryChargesDetailReport._OffenceCategoryId = CInt(cboOffenceCategory.SelectedValue)
            objDisciplinaryChargesDetailReport._OffenceDescrId = CInt(cboDisciplineType.SelectedValue)
            objDisciplinaryChargesDetailReport._OffenceDescription = cboDisciplineType.SelectedItem.Text

            objDisciplinaryChargesDetailReport._Advance_Filter = mstrAdvanceFilter
            objDisciplinaryChargesDetailReport._ViewByIds = mstrStringIds
            objDisciplinaryChargesDetailReport._ViewIndex = mintViewIdx
            objDisciplinaryChargesDetailReport._ViewByName = mstrStringName
            objDisciplinaryChargesDetailReport._Analysis_Fields = mstrAnalysis_Fields
            objDisciplinaryChargesDetailReport._Analysis_Join = mstrAnalysis_Join
            objDisciplinaryChargesDetailReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objDisciplinaryChargesDetailReport._Report_GroupName = mstrReport_GroupName
            objDisciplinaryChargesDetailReport._Advance_Filter = mstrAdvanceFilter
            objDisciplinaryChargesDetailReport._IncludeInactiveEmployee = chkIncludeInactiveEmployee.Checked
            objDisciplinaryChargesDetailReport._ShowAllocationBasedOnChargeDate = chkDisplayAllocationBasedOnChargeDate.Checked
            objDisciplinaryChargesDetailReport._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objDisciplinaryChargesDetailReport._OpenAfterExport = False
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Function

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = objDisciplinaryChargesDetailReport._ReportName

            Me.lblPageHeader.Text = objDisciplinaryChargesDetailReport._ReportName
            Me.lblDateFrom.Text = Language._Object.getCaption(Me.lblDateFrom.ID, Me.lblDateFrom.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.ID, Me.lblTo.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblOffenceCategory.Text = Language._Object.getCaption(Me.lblOffenceCategory.ID, Me.lblOffenceCategory.Text)
            Me.lblDisciplineType.Text = Language._Object.getCaption(Me.lblDisciplineType.ID, Me.lblDisciplineType.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Forms "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objDisciplinaryChargesDetailReport = New clsDisciplinaryChargesDetailReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            SetLanguage()
            If Not IsPostBack Then
                Call FillCombo()
                Call ResetValue()
            Else
                mstrStringIds = Me.ViewState("mstrStringIds")
                mstrStringName = Me.ViewState("mstrStringName")
                mintViewIdx = Me.ViewState("mintViewIdx")
                mstrAnalysis_Fields = Me.ViewState("mstrAnalysis_Fields")
                mstrAnalysis_Join = Me.ViewState("mstrAnalysis_Join")
                mstrAnalysis_OrderBy = Me.ViewState("mstrAnalysis_OrderBy")
                mstrReport_GroupName = Me.ViewState("mstrReport_GroupName")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()
            objDisciplinaryChargesDetailReport._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objDisciplinaryChargesDetailReport._UserUnkId = CInt(Session("UserId"))

            objDisciplinaryChargesDetailReport._OpenAfterExport = False
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objDisciplinaryChargesDetailReport._ExportReportPath = strFilePath

            objDisciplinaryChargesDetailReport.generateReportNew(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CStr(Session("ExportReportPath")), _
                                                CBool(Session("OpenAfterExport")), 0, enPrintAction.None, _
                                                enExportAction.None, _
                                                CInt(Session("Base_CurrencyId")))

            If objDisciplinaryChargesDetailReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objDisciplinaryChargesDetailReport._FileNameAfterExported
                Export.Show()
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboOffenceCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOffenceCategory.SelectedIndexChanged
        Dim objOffence As New clsDisciplineType
        Dim dsList As New DataSet
        Try
            dsList = objOffence.getComboList("List", True, CInt(cboOffenceCategory.SelectedValue))
            With cboDisciplineType
                .DataValueField = "disciplinetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objOffence = Nothing : dsList.Dispose()
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy._Hr_EmployeeTable_Alias = "EM"
            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub



    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region

End Class
