﻿<%@ Page Title="Employee Project Code Time Sheet Report" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Budget_Employee_TimeSheet.aspx.vb" Inherits="Reports_Budget_Employee_TimeSheet_Rpt_Budget_Employee_TimeSheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 40%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Project Code Time Sheet Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboPeriod" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboEmployee" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%"></td>
                                            <td style="width: 75%">
                                                <asp:CheckBox ID="chkShowReportInHtml" runat="server" Text="Show Report In HTML" />
                                            </td>
                                        </tr>
                                            <%--  'Pinkal (28-Jul-2018) -- Start
                                                   'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]--%>
                                          <tr style="width: 100%">
                                            <td style="width: 25%"></td>
                                            <td style="width: 75%">
                                                <asp:CheckBox ID="chkShowSubmissionRemark" runat="server" Text="Show Submission Remark" />
                                            </td>
                                        </tr>
                                        
                                         <tr style="width: 100%">
                                            <td style="width: 25%"></td>
                                            <td style="width: 75%;text-align: right; font-weight: bold">
                                                <asp:LinkButton ID="btnSaveSettings" Text="Save Settings" runat="server" CssClass="lnkhover"></asp:LinkButton>
                                            </td>
                                        </tr>
                                         <%--  'Pinkal (28-Jul-2018) -- End --%>
                                    </table>
                                    <div class="btn-default"><asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnExport" runat="server" CssClass="btndefault" Text="Export" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
