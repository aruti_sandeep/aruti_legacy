﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_CRSummaryReport.aspx.vb"
    Inherits="Reports_Rpt_CRSummaryReport" Title="Claim Request Summary Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 60%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Claim Request Summary Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkSetAnalysis" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative;">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblPostedPeriodFrom" runat="server" Text="Posted Period From"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <asp:DropDownList ID="cboFromPeriod" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="LblTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <asp:DropDownList ID="cboToPeriod" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblTranFromDate" runat="server" Text="From Date"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpTranFromDate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                        <td style="width: 15%; text-align: left">
                                                            <asp:Label ID="lblTranToDate" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpTranToDate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblExpenseCategory" runat="server" Text="Exp. Category"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboExpenseCategory" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblExpense" runat="server" Text="Expense"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboExpense" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblUOM" runat="server" Text="UOM"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboUOM" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        
                                         <%--Pinkal (30-Mar-2021)-- Start
                                         NMB Enhancement  -  Working on Employee Recategorization history Report.--%>
                                         <tr style="width: 100%">
                                                <td style="width: 30%">
                                                       <asp:Label ID =  "LblCurrency" runat="server" Text = "Currency"></asp:Label>
                                                </td>
                                                <td style="width: 70%">
                                                       <asp:DropDownList ID = "cboCurrency" runat="server"></asp:DropDownList>     
                                                </td>
                                          </tr>
                                          <%--'Pinkal (30-Mar-2021) -- End--%>
                                        
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 70%">
                                                <asp:CheckBox ID="chkShowClaimStatus" runat="server" Text="Show Claim Form Status"
                                                    Checked="true" />
                                            </td>
                                        </tr>
                                        
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
