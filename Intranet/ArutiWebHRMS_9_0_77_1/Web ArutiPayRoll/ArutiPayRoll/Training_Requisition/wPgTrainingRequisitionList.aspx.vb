﻿#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.Globalization
Imports System.IO

#End Region

Partial Class Training_Requisition_wPgTrainingRequisitionList
    Inherits Basepage

#Region " Private Variable "

    Private DisplayMessage As New CommonCodes
    Private objRequisitionMaster As New clstraining_requisition_master
    Private objRequisitionTran As New clstraining_requisition_tran
    Private objRequisitionApproval As New clstraining_requisition_approval_master
    Private objRequisitionApprovalTran As New clstraining_requisition_approval_tran
    Private ReadOnly mstrModuleName As String = "frmTrainingRequisitionList"
    Private mintRequisitionMasterId As Integer = 0
    Private mstrRequisitionMasterguid As String = ""
    Private mdtRequisitionTran As DataTable
    Private mintCRMasterunkid As Integer = 0
    Private mintLinkedMasterId As Integer = 0
    Private mintEmployeeUnkid As Integer = 0

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objMst As New clsCommon_Master
        Dim dsCombo As New DataSet
        Try
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            Dim blnApplyFilter As Boolean = True

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                blnSelect = True
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If
            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                             Session("UserAccessModeSetting").ToString(), True, _
                                             False, "List", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = intEmpId
            End With

            dsCombo = objRequisitionApproval.getStatusComboList("List", True)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objMst.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            With cboCourseType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objMst.getComboList(clsCommon_Master.enCommonMaster.TRAINING_MODE, True, "List")
            With cboTrainingMode
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo : " & ex.Message, Me)
        Finally
            objEmp = Nothing
            objMst = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                cboEmployee.SelectedValue = 0
            End If
            cboCourseType.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboTrainingMode.SelectedValue = 0
            dtpFromDate.SetDate = Nothing
            dtpToDate.SetDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("ResetValue : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub FillGrid(ByVal isblank As Boolean)
        Dim strApprFilter As String = String.Empty
        Dim strMainFilter As String = String.Empty
        Dim dsList As New DataSet
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                strApprFilter += "AND TRAM.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
                strMainFilter += "AND TRM.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If CInt(IIf(cboCourseType.SelectedValue = "", 0, cboCourseType.SelectedValue)) > 0 Then
                strApprFilter += "AND TRAM.coursemasterunkid = '" & CInt(cboCourseType.SelectedValue) & "' "
                strMainFilter += "AND TRM.coursemasterunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If CInt(IIf(cboStatus.SelectedValue = "", 0, cboStatus.SelectedValue)) > 0 Then
                strApprFilter += "AND TRAM.iStatusId = '" & CInt(cboStatus.SelectedValue) & "' "
                strMainFilter += "AND TRM.iStatusId = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If CInt(IIf(cboTrainingMode.SelectedValue = "", 0, cboTrainingMode.SelectedValue)) > 0 Then
                strApprFilter += "AND TRAM.trainingmodeunkid = '" & CInt(cboTrainingMode.SelectedValue) & "' "
                strMainFilter += "AND TRM.trainingmodeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If dtpFromDate.IsNull = False AndAlso dtpToDate.IsNull = False Then
                strApprFilter += "AND ISNULL(CONVERT(NVARCHAR(8),TRAM.training_startdate,112),'') >= '" & eZeeDate.convertDate(dtpFromDate.GetDate()) & "' AND ISNULL(CONVERT(NVARCHAR(8),TRAM.training_enddate,112),'') <= '" & eZeeDate.convertDate(dtpToDate.GetDate()) & "' "
                strMainFilter += "AND ISNULL(CONVERT(NVARCHAR(8),TRM.training_startdate,112),'') >= '" & eZeeDate.convertDate(dtpFromDate.GetDate()) & "' AND ISNULL(CONVERT(NVARCHAR(8),TRM.training_enddate,112),'') <= '" & eZeeDate.convertDate(dtpToDate.GetDate()) & "' "
            End If

            If isblank Then
                strApprFilter = "AND 1 = 2 "
                strMainFilter = "AND 1 = 2 "
            End If

            If strApprFilter.Trim.Length > 0 Then strApprFilter = strApprFilter.Substring(3)
            If strMainFilter.Trim.Length > 0 Then strMainFilter = strMainFilter.Substring(3)

            dsList = objRequisitionMaster.GetList("List", strMainFilter, Nothing, isblank)

            Dim columnNames As String() = dsList.Tables(0).Columns.Cast(Of DataColumn).Select(Function(x) x.ColumnName).ToArray()
            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList = objRequisitionMaster.GetList("List", "1=2", Nothing, isblank)
            End If
            If isblank = False Then
                Dim dsData As New DataSet
                dsData = objRequisitionApproval.GetList("List", strApprFilter, Nothing, isblank)
                If dsData.Tables(0).Rows.Count > 0 Then
                    Dim dt As DataTable = New DataView(dsData.Tables(0), "rno = 1", "", DataViewRowState.CurrentRows).ToTable(True, columnNames)
                    dsList.Tables(0).Merge(dt, True)
                End If
            End If
            gvRequisitionList.DataSource = dsList
            gvRequisitionList.DataBind()
            If isblank = True Then
                gvRequisitionList.Rows(0).Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("FillGrid : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub ClearForm()
        Try
            Me.ViewState.Remove("mintRequisitionMasterId")
            Me.ViewState.Remove("mstrRequisitionMasterguid")
            Me.ViewState.Remove("mdtRequisitionTran")
            Me.ViewState.Remove("mintCRMasterunkid")
            Me.ViewState.Remove("mintLinkedMasterId")
            Me.ViewState.Remove("mintEmployeeUnkid")
            mintRequisitionMasterId = 0
            mstrRequisitionMasterguid = ""
            mintCRMasterunkid = 0
            mintLinkedMasterId = 0
            mintEmployeeUnkid = 0
            mdtRequisitionTran = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("ClearForm : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Form Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If (Page.IsPostBack = False) Then
                FillCombo()
                ResetValue()
                FillGrid(True)
            Else
                mintRequisitionMasterId = ViewState("mintRequisitionMasterId")
                mstrRequisitionMasterguid = ViewState("mstrRequisitionMasterguid")
                mdtRequisitionTran = ViewState("mdtRequisitionTran")
                mintCRMasterunkid = ViewState("mintCRMasterunkid")
                mintLinkedMasterId = ViewState("mintLinkedMasterId")
                mintEmployeeUnkid = ViewState("mintEmployeeUnkid")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintRequisitionMasterId") = mintRequisitionMasterId
            ViewState("mstrRequisitionMasterguid") = mstrRequisitionMasterguid
            ViewState("mdtRequisitionTran") = mdtRequisitionTran
            ViewState("mintCRMasterunkid") = mintCRMasterunkid
            ViewState("mintLinkedMasterId") = mintLinkedMasterId
            ViewState("mintEmployeeUnkid") = mintEmployeeUnkid
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnListReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnListReset_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnListSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListSearch.Click
        Try
            Call FillGrid(False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnListSearch_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnListNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListNew.Click
        Try
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage("Sorry, Employee is mandatory information. Please select employee to continue.", Me)
            End If
            Dim strMsg As String = String.Empty
            strMsg = objRequisitionApproval.IsApproverPresent(Session("Database_Name").ToString(), Session("UserAccessModeSetting").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), 1224, CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)))
            If strMsg.Trim.Length <= 0 Then
                Session("mblnIsAddMode") = True
                Response.Redirect("~\Training_Requisition\wPgApplyTrainingRequisition.aspx")
            Else
                DisplayMessage.DisplayMessage(strMsg, Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnListNew_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Cnf_Delete_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cnf_Delete.buttonDelReasonYes_Click
        Try
            If Cnf_Delete.Reason.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage("Delete reason is mandatory information. Please provide reason to continue.", Me)
                Cnf_Delete.Show()
                Exit Sub
            End If

            objRequisitionApproval._Isvoid = True
            objRequisitionApproval._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objRequisitionApproval._Voidreason = Cnf_Delete.Reason
            objRequisitionApproval._Voiduserunkid = CInt(Session("UserId"))
            objRequisitionApproval._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
            objRequisitionApproval._Audituserunkid = CInt(Session("UserId"))
            objRequisitionApproval._Loginemoployeeunkid = CInt(cboEmployee.SelectedValue)
            objRequisitionApproval._Form_Name = mstrModuleName
            objRequisitionApproval._Hostname = CStr(Session("HOST_NAME"))
            objRequisitionApproval._Ip = CStr(Session("IP_ADD"))

            objRequisitionApproval._ObjClaimRequestMaster._Crmasterunkid = mintCRMasterunkid
            objRequisitionApproval._ObjClaimRequestMaster._Isvoid = True
            objRequisitionApproval._ObjClaimRequestMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objRequisitionApproval._ObjClaimRequestMaster._Voidreason = Cnf_Delete.Reason
            objRequisitionApproval._ObjClaimRequestMaster._Voiduserunkid = CInt(Session("UserId"))
            objRequisitionApproval._ObjClaimRequestMaster._VoidLoginEmployeeID = CInt(cboEmployee.SelectedValue)
            'objRequisitionApproval._ObjClaimRequestMaster._FormName = mstrModuleName
            'objRequisitionApproval._ObjClaimRequestMaster._HostName = CStr(Session("HOST_NAME"))
            'objRequisitionApproval._ObjClaimRequestMaster._ClientIP = CStr(Session("IP_ADD"))
            With objRequisitionApproval._ObjClaimRequestMaster
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            Dim objDocument As New clsScan_Attach_Documents
            Dim mdtTrainingAttachment As DataTable = objDocument.GetQulificationAttachment(mintEmployeeUnkid, enScanAttactRefId.TRAINING_REQUISITION, mintLinkedMasterId, CStr(Session("Document_Path")))
            Dim mdtFullClaimAttachment As DataTable = objDocument.GetQulificationAttachment(mintEmployeeUnkid, enScanAttactRefId.TRAINING_REQUISITION, mintCRMasterunkid, CStr(Session("Document_Path")))
            objDocument = Nothing

            If objRequisitionApproval.Delete(mstrRequisitionMasterguid, CInt(Session("UserId")), Session("Database_Name").ToString(), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString(), Session("UserAccessModeSetting"), True, mdtTrainingAttachment, mdtFullClaimAttachment, mintCRMasterunkid, True) = False Then
                DisplayMessage.DisplayMessage(objRequisitionApproval._Message, Me)
                Exit Sub
            Else
                ClearForm()
                FillGrid(False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Cnf_Delete_buttonDelReasonYes_Click : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            If CInt(gvRequisitionList.DataKeys(row.RowIndex)("iStatusId")) <> clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval Then
                DisplayMessage.DisplayMessage("Sorry, you cannot open this transaction in edit mode. Reason your approver has already approved and it's escalted to next level approver", Me)
                Exit Sub
            End If
            Session("mblnIsAddMode") = False
            Session("mintLinkedMasterId") = gvRequisitionList.DataKeys(row.RowIndex)("linkedmasterid")
            Session("mintClaimRequestMasterId") = gvRequisitionList.DataKeys(row.RowIndex)("crmasterunkid")
            Session("RequisitionMasterguid") = Convert.ToString(lnk.CommandArgument.ToString())
            Response.Redirect("~\Training_Requisition\wPgApplyTrainingRequisition.aspx")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkedit_Click : " & ex.Message, Me)
            DisplayMessage.DisplayError("lnkedit_Click : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            If CInt(gvRequisitionList.DataKeys(row.RowIndex)("iStatusId")) <> clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval Then
                DisplayMessage.DisplayMessage("Sorry, you cannot delete this transaction. Reason your approver has already approved and it's escalted to next level approver", Me)
                Exit Sub
            End If

            mstrRequisitionMasterguid = Convert.ToString(lnk.CommandArgument.ToString()).Trim
            mintCRMasterunkid = CInt(gvRequisitionList.DataKeys(row.RowIndex)("crmasterunkid"))
            mintEmployeeUnkid = CInt(gvRequisitionList.DataKeys(row.RowIndex)("employeeunkid"))
            mintLinkedMasterId = CInt(gvRequisitionList.DataKeys(row.RowIndex)("linkedmasterid"))

            If Convert.ToString(lnk.CommandArgument.ToString()).Trim.Length > 0 Then
                Cnf_Delete.Reason = ""
                Cnf_Delete.Show()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkdelete_Click : " & ex.Message, Me)
            DisplayMessage.DisplayError("lnkdelete_Click : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkpreview_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkpreview_Click : " & ex.Message, Me)
            DisplayMessage.DisplayError("lnkpreview_Click : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Grid Event(s) "

    Protected Sub gvRequisitionList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRequisitionList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRequisitionList, "colhStartDate", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRequisitionList, "colhStartDate", False, True)).Text = eZeeDate.convertDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRequisitionList, "colhStartDate", False, True)).Text).ToShortDateString()
                End If
                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRequisitionList, "colhEndDate", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRequisitionList, "colhEndDate", False, True)).Text = eZeeDate.convertDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRequisitionList, "colhEndDate", False, True)).Text).ToShortDateString()
                End If
                Dim lnkselect As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                Dim lnkDelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                If CInt(gvRequisitionList.DataKeys(e.Row.RowIndex)("requisitionmasterunkid")) > 0 Then
                    lnkselect.Visible = False
                    lnkDelete.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvRequisitionList_RowDataBound : " & ex.Message, Me)
        End Try
    End Sub

#End Region

End Class
