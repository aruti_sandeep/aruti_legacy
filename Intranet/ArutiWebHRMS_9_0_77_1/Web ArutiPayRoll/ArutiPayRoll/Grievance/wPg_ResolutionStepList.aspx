﻿<%@ Page Title="Resolution Step List" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_ResolutionStepList.aspx.vb" Inherits="Grievance_wPg_ResolutionStepList" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>


    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Resolution Step"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 12%">
                                            <asp:Label ID="lblGrievanceDateFrom" runat="server" Text="Grievnce Date From"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <uc2:DateCtrl ID="dtGrievanceDateFrom" runat="server" AutoPostBack="false" />
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblGrievanceDateTo" runat="server" Text="To"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <uc2:DateCtrl ID="dtGrievanceDateTo" runat="server" AutoPostBack="false" />
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblGrievanceType" runat="server" Text="Grievance Type"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboGrievanceType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 12%">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboEmpName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblRefNo" runat="server" Text="Ref No."></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:TextBox ID="TxtRefNo" runat="server">
                                            </asp:TextBox>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblResponseType" runat="server" Text="ResponseType"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboRepsonseType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Height="250px">
                                                <asp:GridView ID="dgResolutionStepList" runat="server" AutoGenerateColumns="false"
                                                    AllowPaging="false" Width="120%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                    RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false" DataKeyNames="resolutionsteptranunkid,responsetypeunkid,issubmitted,mapuserunkid">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                            HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" ToolTip="Edit"
                                                                    CommandName="Change" CommandArgument="<%# Container.DataItemIndex %>" OnClick="lnkEdit_Click"></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                            HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                                    CommandName="Remove" CommandArgument="<%# Container.DataItemIndex %>" OnClick="lnkdelete_Click"></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                            <asp:LinkButton ID="lnkPreview" runat="server" CommandName="Preview" ToolTip="View Detail"
                                                                Font-Underline="false" CommandArgument="<%# Container.DataItemIndex %>" OnClick="lnkPreview_Click"> 
                                                                    <i class="fa fa-eye"></i>
                                                                    </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="grievancetype" HeaderText="Grievance Type" ReadOnly="True"
                                                            FooterText="colhGrievanceType" />
                                                    <asp:BoundField DataField="RaisedByEmp" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee"
                                                        Visible="false" />
                                                    <asp:BoundField DataField="grievancerefno" HeaderText="Ref No" ReadOnly="True" FooterText="colhRefNo"
                                                        Visible="false" />
                                                        <asp:BoundField DataField="grievancedate" HeaderText="Meeting Date" ReadOnly="True"
                                                            FooterText="colhMeetingDate" />
                                                        <asp:BoundField DataField="responseremark" HeaderText="Resolution Remark" ReadOnly="True"
                                                            FooterText="colhResolutionRemark" />
                                                        <asp:BoundField DataField="responsetype" HeaderText="Resolution Type" ReadOnly="True"
                                                            FooterText="colhResolutiontype" />
                                                        <asp:BoundField DataField="members" HeaderText="Committee Member" ReadOnly="True"
                                                            FooterText="colhmembers" ItemStyle-Width="20%"/>
                                                              <asp:BoundField DataField="approvername" HeaderText="Approver Name" ReadOnly="True"
                                                            FooterText="colhapprover" />
                                                    <asp:BoundField DataField="againstEmployee" HeaderText="Against Employee" ReadOnly="True"
                                                        FooterText="colhAgainstEmployee" />
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc10:DeleteReason ID="popupDelete" runat="server"  />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
