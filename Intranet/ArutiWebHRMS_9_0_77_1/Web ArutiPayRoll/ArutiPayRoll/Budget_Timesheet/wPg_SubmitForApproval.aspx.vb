﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports Microsoft.VisualBasic
#End Region
Partial Class Budget_Timesheet_wPg_SubmitForApproval
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmSubmitForApproval"
    Private DisplayMessage As New CommonCodes
    Private objBudgetTimesheet As clsBudgetEmp_timesheet

    Private mdicETS As Dictionary(Of String, Integer)
    Private mintPeriodID As Integer = -1
    'Nilay (01 Apr 2017) -- Start
    'Private mblnIsFromPendingSubmitForApproval As Boolean = False
    'Nilay (01 Apr 2017) -- End
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mdtEmpTimesheet As DataTable = Nothing
    Private mintConstantDays As Integer = 0
    Private mintLoginTypeID As Integer = -1
    Private mintLoginEmployeeID As Integer = -1
    Private mintUserID As Integer = -1

    'Nilay (01 Apr 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Private menOperation As clsBudgetEmp_timesheet.enBudgetTimesheetStatus
    'Nilay (01 Apr 2017) -- End

#End Region

#Region " Page's Events "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            objBudgetTimesheet = New clsBudgetEmp_timesheet

            mdicETS = dgvEmpTimesheetList.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvEmpTimesheetList.Columns.IndexOf(x))

            If IsPostBack = False Then

                If Session("PeriodID") IsNot Nothing Then
                    mintPeriodID = CInt(Session("PeriodID"))

                    'Pinkal (13-Apr-2017) -- Start
                    'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                    'Session("PeriodID") = Nothing
                    'Pinkal (13-Apr-2017) -- End
                End If
                'Nilay (01 Apr 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                'If Session("IsFromPendingSubmitForApproval") IsNot Nothing Then
                '    mblnIsFromPendingSubmitForApproval = CBool(Session("IsFromPendingSubmitForApproval"))
                '    Session("IsFromPendingSubmitForApproval") = Nothing
                'End If
                If Session("OperationID") IsNot Nothing Then
                    menOperation = CType(Session("OperationID"), clsBudgetEmp_timesheet.enBudgetTimesheetStatus)

                    'Pinkal (13-Apr-2017) -- Start
                    'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                    'Session("OperationID") = Nothing
                    'Pinkal (13-Apr-2017) -- End

                End If
                'Nilay (01 Apr 2017) -- End

                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    mintLoginTypeID = enLogin_Mode.MGR_SELF_SERVICE
                    mintLoginEmployeeID = 0
                    mintUserID = CInt(Session("UserId"))
                    'Pinkal (03-May-2017) -- Start
                    'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
                    SetVisibility()
                    'Pinkal (03-May-2017) -- End
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    mintLoginTypeID = enLogin_Mode.EMP_SELF_SERVICE
                    mintLoginEmployeeID = CInt(Session("Employeeunkid"))
                    mintUserID = 0
                End If

                'Nilay (01 Apr 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                'If mblnIsFromPendingSubmitForApproval = True Then
                '    btnSubmitForApproval.Visible = True
                '    lblStatus.Visible = False
                '    cboStatus.Visible = False
                '    dgvEmpTimesheetList.Columns(mdicETS("objdgcolhSelect")).Visible = True
                'Else
                '    btnSubmitForApproval.Visible = False
                '    lblStatus.Visible = True
                '    cboStatus.Visible = True
                '    dgvEmpTimesheetList.Columns(mdicETS("objdgcolhSelect")).Visible = False
                'End If
                'Nilay (01 Apr 2017) -- End

                Call FillCombo()

            Else
                mintPeriodID = CInt(Me.ViewState("PeriodID"))
                'Nilay (01 Apr 2017) -- Start
                'mblnIsFromPendingSubmitForApproval = CBool(Me.ViewState("IsFromPendingSubmitForApproval"))
                'Nilay (01 Apr 2017) -- End
                mdtStartDate = CDate(Me.ViewState("StartDate")).Date
                mdtEndDate = CDate(Me.ViewState("EndDate")).Date
                mdtEmpTimesheet = CType(Me.ViewState("EmpTimesheetTable"), DataTable)
                mintConstantDays = CInt(Me.ViewState("ConstantDays"))
                mintLoginTypeID = CInt(Me.ViewState("LoginTypeID"))
                mintLoginEmployeeID = CInt(Me.ViewState("LoginEmployeeID"))
                mintUserID = CInt(Me.ViewState("UserID"))
                'Nilay (01 Apr 2017) -- Start
                menOperation = CType(Me.ViewState("OperationID"), clsBudgetEmp_timesheet.enBudgetTimesheetStatus)
                'Nilay (01 Apr 2017) -- End

            End If


            'Pinkal (13-Apr-2017) -- Start
            'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
            Call SetControlVisibility()
            'Pinkal (13-Apr-2017) -- End


            'Pinkal (31-Oct-2017) -- Start
            'Enhancement - Solve Budget Timesheet Issue As Per Suzan's Comment.
            Call SetLanguage()
            'Pinkal (31-Oct-2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load1:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("PeriodID") = mintPeriodID
            'Nilay (01 Apr 2017) -- Start
            'Me.ViewState("IsFromPendingSubmitForApproval") = mblnIsFromPendingSubmitForApproval
            'Nilay (01 Apr 2017) -- End
            Me.ViewState("StartDate") = mdtStartDate
            Me.ViewState("EndDate") = mdtEndDate
            Me.ViewState("EmpTimesheetTable") = mdtEmpTimesheet
            Me.ViewState("ConstantDays") = mintConstantDays
            Me.ViewState("LoginTypeID") = mintLoginTypeID
            Me.ViewState("LoginEmployeeID") = mintLoginEmployeeID
            Me.ViewState("UserID") = mintUserID
            'Nilay (01 Apr 2017) -- Start
            Me.ViewState("OperationID") = menOperation
            'Nilay (01 Apr 2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
                                               CDate(Session("fin_startdate")).Date, "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = mintPeriodID.ToString
            End With
            objPeriod = Nothing
            dsList = Nothing
            Call cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())

            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'If mblnIsFromPendingSubmitForApproval = False Then
            '    Dim dtTable As DataTable = objMaster.getLeaveStatusList("Status", False, False).Tables(0).Select("statusunkid NOT IN (4,6)").CopyToDataTable  'statusunkid = 4 = Rescheduled
            '    With cboStatus
            '        .DataValueField = "statusunkid"
            '        .DataTextField = "name"
            '        .DataSource = dtTable
            '        .DataBind()
            '        .SelectedValue = "0"
            '    End With
            'End If
            Select Case menOperation
                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED


                    'Pinkal (03-Jan-2020) -- Start
                    'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
                    'Dim dtTable As DataTable = objMaster.getLeaveStatusList("Status", False, False).Tables(0).Select("statusunkid IN (0,1,2)").CopyToDataTable  'statusunkid = 4 = Rescheduled
                    Dim dtTable As DataTable = objMaster.getLeaveStatusList("Status", "", False, False).Tables(0).Select("statusunkid IN (0,1,2)").CopyToDataTable  'statusunkid = 4 = Rescheduled
                    'Pinkal (03-Jan-2020) -- End


                    With cboStatus
                        .DataValueField = "statusunkid"
                        .DataTextField = "name"
                        .DataSource = dtTable
                        .DataBind()
                        .SelectedValue = "0"
                    End With
            End Select
            'Nilay (01 Apr 2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Try

            'Pinkal (03-May-2017) -- Start
            'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
            If menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED Then
                If CBool(Session("AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval")) = False Then Exit Sub
            ElseIf menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL Then
                If CBool(Session("AllowToViewPendingEmpBudgetTimesheetSubmitForApproval")) = False Then Exit Sub
            End If
            End If
            'Pinkal (03-May-2017) -- End



            Dim dsList As DataSet = Nothing
            Dim strSearch As String = ""

            If dtpDate.GetDate <> Nothing Then
                strSearch &= "AND ltbemployee_timesheet.activitydate='" & eZeeDate.convertDate(dtpDate.GetDate) & "' "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND ltbemployee_timesheet.employeeunkid=" & CInt(cboEmployee.SelectedValue) & " "
            End If

            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'If mblnIsFromPendingSubmitForApproval = False Then
            '    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=1 "
            '    If CInt(cboStatus.SelectedValue) > 0 Then
            '        strSearch &= "AND ltbemployee_timesheet.statusunkid=" & CInt(cboStatus.SelectedValue) & " "
            '    End If
            'Else
            '    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=0 "
            'End If
            Select Case menOperation
                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL, clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED
                    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=0 AND ltbemployee_timesheet.statusunkid=2 "

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED
                    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=1 "
                    If CInt(cboStatus.SelectedValue) > 0 Then
                        strSearch &= "AND ltbemployee_timesheet.statusunkid=" & CInt(cboStatus.SelectedValue) & " "
                    End If

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED
                    strSearch &= "AND ltbemployee_timesheet.issubmit_approval=1 AND ltbemployee_timesheet.statusunkid=1 "

            End Select
            'Nilay (01 Apr 2017) -- End

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

            'dsList = objBudgetTimesheet.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), _
            '                                    CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                    mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), _
            '                                    True, CBool(Session("IsIncludeInactiveEmp")), , True, True, _
            '                                    CInt(cboPeriod.SelectedValue), , strSearch, , True)

            'Pinkal (28-Mar-2019) -- Start
            'Enhancement - Working on Budget Timesheet enhancement for Pact.
            Dim mblnApplyAccessFilter As Boolean = True
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                mblnApplyAccessFilter = False
            End If

            'dsList = objBudgetTimesheet.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), _
            '                                    CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                    mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), _
            '                                 True, CBool(Session("IsIncludeInactiveEmp")), CBool(Session("AllowOverTimeToEmpTimesheet")), , True, True, _
            '                                    CInt(cboPeriod.SelectedValue), , strSearch, , True)

            dsList = objBudgetTimesheet.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), _
                                             True, CBool(Session("IsIncludeInactiveEmp")), CBool(Session("AllowOverTimeToEmpTimesheet")), , True, True, _
                                             CInt(cboPeriod.SelectedValue), , strSearch, mblnApplyAccessFilter, True)

            'Pinkal (28-Mar-2019) -- End


            'Pinkal (28-Mar-2018) -- End


            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            If CBool(Session("AllowOverTimeToEmpTimesheet")) Then
                Dim objEmpHoliday As New clsemployee_holiday

                'Pinkal (28-Mar-2019) -- Start
                'Enhancement - Working on Budget Timesheet enhancement for Pact.


                'Pinkal (28-Mar-2019) -- Start
                'Enhancement - Working on Budget Timesheet enhancement for Pact.

                'Dim dsHolidayList As DataSet = objEmpHoliday.GetList("Holiday", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                '                                                                                  , mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True _
                '                                                                                  , CBool(Session("IsIncludeInactiveEmp")), True, -1, Nothing, _
                '                                                                                  , " AND  CONVERT(char(8),lvholiday_master.holidaydate,112) >= '" & eZeeDate.convertDate(mdtStartDate) & "'  AND CONVERT(char(8),lvholiday_master.holidaydate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "'")

                Dim dsHolidayList As DataSet = objEmpHoliday.GetList("Holiday", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                  , mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True _
                                                                                              , CBool(Session("IsIncludeInactiveEmp")), mblnApplyAccessFilter, -1, Nothing, _
                                                                                                  , " AND  CONVERT(char(8),lvholiday_master.holidaydate,112) >= '" & eZeeDate.convertDate(mdtStartDate) & "'  AND CONVERT(char(8),lvholiday_master.holidaydate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "'")

                'Pinkal (28-Mar-2019) -- End

                objEmpHoliday = Nothing

                For Each drRow As DataRow In dsHolidayList.Tables(0).Rows
                    Dim dRow() As DataRow = dsList.Tables(0).Select("ADate = '" & drRow("holidaydate").ToString() & "' AND employeeunkid = " & CInt(drRow("employeeunkid")))
                    If dRow.Length > 0 Then
                        For i As Integer = 0 To dRow.Length - 1
                            dRow(i)("isholiday") = True
                            dRow(i).AcceptChanges()
                        Next
                    End If
                Next
                dsList.AcceptChanges()
            End If
            'Pinkal (13-Aug-2018) -- End



            dgvEmpTimesheetList.AutoGenerateColumns = False

            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'If mblnIsFromPendingSubmitForApproval = False AndAlso CInt(cboStatus.SelectedValue) > 0 Then
            '    dgvEmpTimesheetList.Columns(mdicETS("dgcolhApprovedActHrs")).Visible = True
            'End If
            If menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED AndAlso CInt(cboStatus.SelectedValue) > 0 Then
                dgvEmpTimesheetList.Columns(mdicETS("dgcolhApprovedActHrs")).Visible = True

            ElseIf menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED Then
                dgvEmpTimesheetList.Columns(mdicETS("dgcolhApprovedActHrs")).Visible = True
            End If
            'Nilay (01 Apr 2017) -- End

            mdtEmpTimesheet = dsList.Tables("List")
            dgvEmpTimesheetList.DataSource = mdtEmpTimesheet

            Dim strColumnArray() As String = {"dgcolhDate"}
            Dim dCol = dgvEmpTimesheetList.Columns.Cast(Of DataGridColumn).Where(Function(x) strColumnArray.Contains(x.FooterText))
            If dCol.Count > 0 Then
                For Each dc As DataGridColumn In dCol
                    CType(dgvEmpTimesheetList.Columns(dgvEmpTimesheetList.Columns.IndexOf(dc)), BoundColumn).DataFormatString = "{0:" & Session("DateFormat").ToString & "}"
                Next
            End If

            dgvEmpTimesheetList.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillList:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01 Apr 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Private Sub SetControlVisibility()
        Try
            Select Case menOperation
                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL
                    lblPageHeader.Text = Language.getMessage(mstrModuleName, 6, "Pending Submit For Approval")
                    btnSubmitForApproval.Visible = True
                    btnCancel.Visible = False
                    btnDelete.Visible = False
                    lblStatus.Visible = False
                    cboStatus.Visible = False
                    dgvEmpTimesheetList.Columns(mdicETS("objdgcolhSelect")).Visible = True
                    dgvEmpTimesheetList.Columns(mdicETS("dgcolhStatus")).Visible = False


                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    lblStatus.Visible = False
                    cboStatus.Visible = False
                    LblSubmissionRemark.Visible = True
                    txtSubmissionRemark.Visible = True
                    'Pinkal (28-Jul-2018) -- End


                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED
                    lblPageHeader.Text = Language.getMessage(mstrModuleName, 7, "Completed Submit For Approval")
                    btnSubmitForApproval.Visible = False
                    btnCancel.Visible = False
                    btnDelete.Visible = False
                    lblStatus.Visible = True
                    cboStatus.Visible = True
                    dgvEmpTimesheetList.Columns(mdicETS("objdgcolhSelect")).Visible = False
                    dgvEmpTimesheetList.Columns(mdicETS("dgcolhStatus")).Visible = True

                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    LblSubmissionRemark.Visible = False
                    txtSubmissionRemark.Visible = False
                    'Pinkal (28-Jul-2018) -- End

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED
                    lblPageHeader.Text = Language.getMessage(mstrModuleName, 8, "Global Cancel Timesheet")
                    btnCancel.Visible = True
                    btnSubmitForApproval.Visible = False
                    btnDelete.Visible = False
                    lblStatus.Visible = False
                    cboStatus.Visible = False

                    dgvEmpTimesheetList.Columns(mdicETS("objdgcolhSelect")).Visible = True
                    dgvEmpTimesheetList.Columns(mdicETS("dgcolhStatus")).Visible = False

                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    LblSubmissionRemark.Visible = False
                    txtSubmissionRemark.Visible = False
                    'Pinkal (28-Jul-2018) -- End


                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED
                    lblPageHeader.Text = Language.getMessage(mstrModuleName, 9, "Global Delete Timesheet")
                    btnDelete.Visible = True
                    btnSubmitForApproval.Visible = False
                    btnCancel.Visible = False
                    dgvEmpTimesheetList.Columns(mdicETS("objdgcolhSelect")).Visible = True
                    dgvEmpTimesheetList.Columns(mdicETS("dgcolhStatus")).Visible = False
                    lblStatus.Visible = False
                    cboStatus.Visible = False

                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    LblSubmissionRemark.Visible = False
                    txtSubmissionRemark.Visible = False
                    'Pinkal (28-Jul-2018) -- End

            End Select

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetControlVisibility:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetControlVisibility:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01 Apr 2017) -- End

    'Pinkal (03-May-2017) -- Start
    'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
    Private Sub SetVisibility()
        Try
            btnSubmitForApproval.Enabled = CBool(Session("AllowToSubmitForApprovalForPendingEmpBudgetTimesheet"))
            btnDelete.Enabled = CBool(Session("AllowToDeleteEmployeeBudgetTimesheet"))
            btnCancel.Enabled = CBool(Session("AllowToCancelEmployeeBudgetTimesheet"))

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            If menOperation <> clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL Then
                dgvEmpTimesheetList.Columns(mdicETS("dgcolhSubmissionRemark")).Visible = True
            Else
                dgvEmpTimesheetList.Columns(mdicETS("dgcolhSubmissionRemark")).Visible = False
            End If
            'Pinkal (28-Jul-2018) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetVisibility:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetVisibility:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Pinkal (03-May-2017) -- End


#End Region

#Region " ComboBox's Events "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

                'Pinkal (05-Jun-2017) -- Start
                'Enhancement - Implemented TnA Period for PSI Malawi AS Per Suzan's Request .

                'mdtStartDate = CDate(objPeriod._Start_Date).Date
                'mdtEndDate = CDate(objPeriod._End_Date).Date
                'mintConstantDays = CInt(objPeriod._Constant_Days)
                mdtStartDate = CDate(objPeriod._TnA_StartDate).Date
                mdtEndDate = CDate(objPeriod._TnA_EndDate).Date
                mintConstantDays = Convert.ToInt32(DateDiff(DateInterval.Day, CDate(objPeriod._TnA_StartDate).Date, CDate(CDate(objPeriod._TnA_EndDate).Date)) + 1)
                'Pinkal (05-Jun-2017) -- End



                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    Dim objEmployee As New clsEmployee_Master

                    'Pinkal (12-Oct-2017) -- Start
                    'Enhancement - Working on CCK Leave Planner Changes & Budget Timesheet changes.

                    'Dim dsList As DataSet = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                    '                                                    CInt(Session("CompanyUnkId")), mdtEndDate, mdtEndDate, _
                    '                                                    CStr(Session("UserAccessModeSetting")), True, _
                    '                                                    CBool(Session("IsIncludeInactiveEmp")), "List", True)

                    Dim dsList As DataSet = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                     CInt(Session("CompanyUnkId")), mdtStartDate, mdtEndDate, _
                                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                                        CBool(Session("IsIncludeInactiveEmp")), "List", True)

                    'Pinkal (12-Oct-2017) -- End

                    With cboEmployee
                        .DataValueField = "employeeunkid"

                        'Pinkal (31-Oct-2017) -- Start
                        'Enhancement - Solve Budget Timesheet Issue As Per Suzan's Comment.
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Pinkal (31-Oct-2017) -- End

                        .DataSource = dsList.Tables("List")
                        .DataBind()
                        .SelectedValue = "0"
                    End With
                    objEmployee = Nothing
                    dsList = Nothing
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    Dim objglobalassess = New GlobalAccess
                    objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                    cboEmployee.DataSource = objglobalassess.ListOfEmployee
                    cboEmployee.DataTextField = "loginname"
                    cboEmployee.DataValueField = "employeeunkid"
                    cboEmployee.DataBind()
                End If

            Else
                dtpDate.SetDate = Nothing
                mdtStartDate = Nothing : mdtEndDate = Nothing
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError("GetProjectedTotalHRs :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory information. Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            Call FillList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSearch_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboPeriod.SelectedValue = "0"
            dtpDate.SetDate = Nothing
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then cboEmployee.SelectedValue = "0"
            cboStatus.SelectedValue = "0"

            dgvEmpTimesheetList.DataSource = New List(Of String)
            dgvEmpTimesheetList.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSubmitForApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitForApproval.Click
        Try
            Dim blnFlag As Boolean = False
            objBudgetTimesheet = New clsBudgetEmp_timesheet


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtEmpTimesheet Is Nothing Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
                Exit Sub
            End If
            'Pinkal (05-Sep-2020) -- End


            Dim dtEmpTimeSheet As DataTable = New DataView(mdtEmpTimesheet, "IsChecked=1 AND IsGrp=0", "", DataViewRowState.CurrentRows).ToTable 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]

            'Nilay (21 Mar 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'If mdtEmpTimesheet.Rows.Count <= 0 Then
            If dtEmpTimeSheet.Rows.Count <= 0 Then
                'Nilay (21 Mar 2017) -- End
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
                Exit Sub
            End If

            Dim dR As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0 AND issubmit_approval=1") 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
            If dR.Length > 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Some of the checked employee activity(s) is already sent for approval."), Me)
                Exit Sub
            End If

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

            If CBool(Session("RemarkMandatoryForTimesheetSubmission")) AndAlso txtSubmissionRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Submission Remark is compulsory information.Please enter submission remark."), Me)
                txtSubmissionRemark.Focus()
                Exit Sub
            End If

            dR = Nothing
            dR = dtEmpTimeSheet.Select("IsChecked=1 AND IsGrp=0 AND issubmit_approval = 0")
            If dR.Length > 0 Then
                For i As Integer = 0 To dR.Length - 1
                    dR(i)("submission_remark") = txtSubmissionRemark.Text.Trim()
                Next
                dtEmpTimeSheet.AcceptChanges()
            End If
            'Pinkal (28-Jul-2018) -- End



            objBudgetTimesheet._dtEmployeeActivity = dtEmpTimeSheet
            objBudgetTimesheet._Periodunkid = CInt(cboPeriod.SelectedValue)


            'Pinkal (13-Apr-2017) -- Start
            'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objBudgetTimesheet._Userunkid = CInt(Session("UserId"))
                objBudgetTimesheet._LoginEmployeeunkid = 0
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            objBudgetTimesheet._Userunkid = CInt(Session("UserId"))
                objBudgetTimesheet._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
            End If

            'objBudgetTimesheet._WebFormName = mstrModuleName
            'objBudgetTimesheet._WebClientIP = CStr(Session("IP_ADD"))
            'objBudgetTimesheet._WebHostName = CStr(Session("HOST_NAME"))

            'Pinkal (13-Apr-2017) -- End



            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            objBudgetTimesheet._IsSubmitForApproval = True
            'Nilay (01 Apr 2017) -- End

            If CBool(Session("NotAllowIncompleteTimesheet")) = True Then


                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                'blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtEmpTimesheet, mintConstantDays, mdtStartDate, mdtEndDate, _
                '                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()))
                blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtEmpTimesheet, mintConstantDays, mdtStartDate, mdtEndDate, _
                                                                                                           clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL, _
                                                                                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                                                           CBool(Session("AllowOverTimeToEmpTimesheet")))

                'Pinkal (28-Jul-2018) -- End

                If objBudgetTimesheet._Message <> "" Then
                    DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                    Exit Sub
                Else
                    If blnFlag = False Then
                        'Pinkal (31-Oct-2017) -- Start
                        'Enhancement - Solve Budget Timesheet Issue As Per Suzan's Comment.
                        'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot perform Submit For Approval. Reason : On Configuration --> Options --> Payroll Settings --> 'Don't allow incomplete timesheet to Submit For Apprval/Approve/Reject/Cancel' is alraeady set."), Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot perform Submit For Approval. Reason : your timesheet is incomplete, please complete it and submit again."), Me)
                        'Pinkal (31-Oct-2017) -- End
                        Exit Sub
                    End If
                End If
            End If

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            'If objBudgetTimesheet.Update(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), _
            '                            CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                            CBool(Session("IsIncludeInactiveEmp")), True) = False Then

With objBudgetTimesheet
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            If objBudgetTimesheet.Update(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), _
                                         CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                       CBool(Session("IsIncludeInactiveEmp")), True, CBool(Session("AllowOverTimeToEmpTimesheet"))) = False Then
                'Pinkal (13-Aug-2018) -- End


                DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                Exit Sub
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Employee Timesheet activity(s) submitted for approval successfully."), Me)

                Dim strEmployeeIDs As String = String.Join(",", dtEmpTimeSheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)

                'Nilay (21 Mar 2017) -- Start
                'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                'objBudgetTimesheet.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                '                                              CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                              CBool(Session("IsIncludeInactiveEmp")), strEmployeeIDs, "", CInt(cboPeriod.SelectedValue), _
                '                                              -1, mintLoginTypeID, mintLoginEmployeeID, mintUserID, CStr(Session("ArutiSelfServiceURL")))
                Dim dtSelectedData As DataTable = New DataView(mdtEmpTimesheet, "IsChecked=1", "", DataViewRowState.CurrentRows).ToTable

                objBudgetTimesheet.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                                                              CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                              CBool(Session("IsIncludeInactiveEmp")), strEmployeeIDs, CInt(cboPeriod.SelectedValue), _
                                                              -1, dtSelectedData, mintLoginTypeID, mintLoginEmployeeID, mintUserID, _
                                                              CStr(Session("ArutiSelfServiceURL")), False)
                'Nilay (21 Mar 2017) -- End


                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                txtSubmissionRemark.Text = ""
                'Pinkal (28-Jul-2018) -- End

            End If

            Call FillList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSubmitForApproval_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSubmitForApproval_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Budget_Timesheet/wPg_EmployeeTimeSheet.aspx", False)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01 Apr 2017) -- Start
    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If dgvEmpTimesheetList.Items.Count <= 0 Then Exit Sub

            Dim dRow As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0")
            If dRow.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
                Exit Sub
            End If

            Language.setLanguage(mstrModuleName)
            popupConfirmYesNo.Title = Language.getMessage(mstrModuleName, 8, "Global Cancel Timesheet")
            popupConfirmYesNo.Message = Language.getMessage(mstrModuleName, 12, "Are you sure you want to cancel this employee timesheet?")
            popupConfirmYesNo.Show()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnCancel_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnCancel_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If dgvEmpTimesheetList.Items.Count <= 0 Then Exit Sub

            Dim dRow As DataRow() = mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0")
            If dRow.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please tick atleast one record for further process."), Me)
                Exit Sub
            End If

            Language.setLanguage(mstrModuleName)
            popupConfirmYesNo.Title = Language.getMessage(mstrModuleName, 9, "Global Delete Timesheet")
            popupConfirmYesNo.Message = Language.getMessage(mstrModuleName, 10, "Are you sure you want to delete this employee timesheet?")
            popupConfirmYesNo.Show()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnDelete_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnDelete_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupConfirmYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmYesNo.buttonYes_Click
        Try
            Select Case menOperation
                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED
                    Language.setLanguage(mstrModuleName)
                    popupDeleteReason.Title = Language.getMessage(mstrModuleName, 21, "Cancel Reason")
                    popupDeleteReason.Reason = ""
                    popupDeleteReason.Show()
                   
                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED
                    Language.setLanguage(mstrModuleName)
                    popupDeleteReason.Title = "Delete Reason"
                    popupDeleteReason.Reason = ""
                    popupDeleteReason.Show()

            End Select
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupConfirmYesNo_buttonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupConfirmYesNo_buttonYes_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteReason.buttonDelReasonYes_Click
        Try
            objBudgetTimesheet = New clsBudgetEmp_timesheet

            Call SetDateFormat()

            Select Case menOperation

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED

                    Dim blnFlag As Boolean
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)

                    If CBool(Session("NotAllowIncompleteTimesheet")) = True Then

                        'Pinkal (28-Jul-2018) -- Start
                        'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

                        'blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtEmpTimesheet, CInt(objPeriod._Constant_Days), _
                        '                                                          objPeriod._Start_Date.Date, objPeriod._End_Date.Date, _
                        '                                                          clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()))



                        blnFlag = objBudgetTimesheet.IsTimesheetCompleteForPeriod(mdtEmpTimesheet, CInt(objPeriod._Constant_Days), _
                                                                                                                   objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, _
                                                                                                                   clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED, _
                                                                                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                                                                   CBool(Session("AllowOverTimeToEmpTimesheet")))

                        'Pinkal (28-Jul-2018) -- End
                        

                        If objBudgetTimesheet._Message <> "" Then
                            DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                            Exit Sub
                        Else
                            If blnFlag = False Then
                                'Pinkal (31-Oct-2017) -- Start
                                'Enhancement - Solve Budget Timesheet Issue As Per Suzan's Comment.
                                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot perform Cancel operation. Reason : On Configuration --> Options --> Payroll Settings --> 'Don't allow incomplete timesheet to Submit For Apprval/Approve/Reject/Cancel' is alraeady set"), Me)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, you cannot perform Cancel operation. Reason : your timesheet is incomplete, please complete it."), Me)
                                'Pinkal (31-Oct-2017) -- End
                                Exit Sub
                            End If
                        End If
                    End If

                    Dim strEmployeeIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)


                    'Pinkal (13-Oct-2017) -- Start
                    'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                    'Dim strEmpTimesheetIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False) _
                    '                                               .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)
                    Dim strEmpTimesheetIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False AndAlso x.Field(Of Boolean)("ischecked") = True) _
                                                                   .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)
                    'Pinkal (13-Oct-2017) -- End

                    

                    objBudgetTimesheet._Userunkid = CInt(Session("UserId"))


                    'Pinkal (13-Apr-2017) -- Start
                    'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                    'objBudgetTimesheet._WebFormName = mstrModuleName
                    'objBudgetTimesheet._WebClientIP = CStr(Session("IP_ADD"))
                    'objBudgetTimesheet._WebHostName = CStr(Session("HOST_NAME"))
                    'Pinkal (13-Apr-2017) -- End

                    With objBudgetTimesheet
                        ._FormName = mstrModuleName
                        If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                            ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                        Else
                            ._AuditUserId = Convert.ToInt32(Session("UserId"))
                        End If
                        ._ClientIP = Session("IP_ADD").ToString()
                        ._HostName = Session("HOST_NAME").ToString()
                        ._FromWeb = True
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                    End With

                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    'If objBudgetTimesheet.CancelTimesheet(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), _
                    '                                      CInt(Session("CompanyUnkId")), CBool(Session("IsIncludeInactiveEmp")), _
                    '                                      CStr(Session("UserAccessModeSetting")), popupDeleteReason.Reason, strEmpTimesheetIDs, Nothing) = False Then
                    If objBudgetTimesheet.CancelTimesheet(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("UserId")), _
                                                          CInt(Session("CompanyUnkId")), CBool(Session("IsIncludeInactiveEmp")), _
                                                      CStr(Session("UserAccessModeSetting")), popupDeleteReason.Reason, strEmpTimesheetIDs, CBool(Session("AllowOverTimeToEmpTimesheet")), Nothing) = False Then
                        'Pinkal (28-Jul-2018) -- End

                        DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                        Exit Sub
                    Else
                        objBudgetTimesheet.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                      CBool(Session("IsIncludeInactiveEmp")), strEmployeeIDs, strEmpTimesheetIDs, _
                                                                      CInt(cboPeriod.SelectedValue), clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED, _
                                                                      mintLoginTypeID, mintLoginEmployeeID, mintUserID, CStr(Session("ArutiSelfServiceURL")), _
                                                                      popupDeleteReason.Reason, , , False)
                    End If

                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Timesheet Cancelled Successfully."), Me)

                    Call FillList()

                Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED

                    For Each row As DataRow In mdtEmpTimesheet.Select("IsChecked=1 AND IsGrp=0")

                        objBudgetTimesheet._Voiduserunkid = CInt(Session("UserId"))
                        objBudgetTimesheet._LoginEmployeeunkid = -1
                        objBudgetTimesheet._Voidreason = popupDeleteReason.Reason

                        'Pinkal (13-Apr-2017) -- Start
                        'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                        'objBudgetTimesheet._WebFormName = mstrModuleName
                        'objBudgetTimesheet._WebClientIP = CStr(Session("IP_ADD"))
                        'objBudgetTimesheet._WebHostName = CStr(Session("HOST_NAME"))
                        'Pinkal (13-Apr-2017) -- End

                        With objBudgetTimesheet
                            ._FormName = mstrModuleName
                            If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                                ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                            Else
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                            End If
                            ._ClientIP = Session("IP_ADD").ToString()
                            ._HostName = Session("HOST_NAME").ToString()
                            ._FromWeb = True
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                        End With


                        If objBudgetTimesheet.Delete(CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                     CInt(row.Item("emptimesheetunkid"))) = False Then
                            DisplayMessage.DisplayMessage(objBudgetTimesheet._Message, Me)
                            Exit Sub
                        End If
                    Next

                    Dim strEmployeeIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)
                    Dim strEmpTimesheetIDs As String = String.Join(",", mdtEmpTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False) _
                                                                   .Select(Function(x) x.Field(Of Integer)("emptimesheetunkid").ToString).Distinct().ToArray)

                    objBudgetTimesheet.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                  CBool(Session("IsIncludeInactiveEmp")), strEmployeeIDs, strEmpTimesheetIDs, _
                                                                  CInt(cboPeriod.SelectedValue), clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED, _
                                                                  mintLoginTypeID, mintLoginEmployeeID, mintUserID, CStr(Session("ArutiSelfServiceURL")), _
                                                                  popupDeleteReason.Reason, , , False)

                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Timesheet deleted Successfully."), Me)

                    Call FillList()

            End Select

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupDeleteReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupDeleteReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01 Apr 2017) -- End

#End Region

#Region " DataGrid's Events "

    Protected Sub dgvEmpTimesheetList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvEmpTimesheetList.ItemDataBound
        Try
            Call SetDateFormat()


            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            dgvEmpTimesheetList.Columns(mdicETS("dgcolhActivity")).Visible = CBool(Session("ShowBgTimesheetActivity"))
            dgvEmpTimesheetList.Columns(mdicETS("dgcolhProject")).Visible = CBool(Session("ShowBgTimesheetActivityProject"))
            'Pinkal (28-Jul-2018) -- End

            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)

                Dim intVisible = dgvEmpTimesheetList.Columns.Cast(Of DataGridColumn).Where(Function(x) x.Visible = True).Count

                If e.Item.ItemIndex >= 0 Then

                    If CBool(dr("IsGrp")) = True Then 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]

                        e.Item.Cells(mdicETS("dgcolhEmployee")).ColumnSpan = intVisible

                        For i = mdicETS("dgcolhEmployee") + 1 To e.Item.Cells.Count - 1
                            e.Item.Cells(i).Visible = False
                        Next

                        e.Item.Cells(mdicETS("dgcolhEmployee")).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(mdicETS("dgcolhEmployee")).Style.Add("text-align", "left")
                      
                        Select Case menOperation
                            Case clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL, clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED, _
                                 clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED

                                CType(e.Item.Cells(mdicETS("objdgcolhSelect")).FindControl("chkSelect"), CheckBox).Visible = False
                                e.Item.Cells(mdicETS("objdgcolhSelect")).CssClass = "GroupHeaderStyleBorderLeft"
                            Case Else
                                e.Item.Cells(mdicETS("dgcolhEmployee")).Style.Add("padding-left", "10px")
                        End Select

                    Else
                        'Pinkal (13-Aug-2018) -- Start
                        'Enhancement - Changes For PACT [Ref #249,252]
                        If CBool(dr("isholiday")) Then
                            For i As Integer = 0 To e.Item.Cells.Count - 1
                                e.Item.Cells(i).Style.Add("color", "red")
                            Next
                        End If
                        'Pinkal (13-Aug-2018) -- End

                    End If

                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvEmpTimesheetList_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgvEmpTimesheetList_ItemDataBound:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkSelectAll As CheckBox = CType(sender, CheckBox)


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtEmpTimesheet Is Nothing Then Exit Sub
            'Pinkal (05-Sep-2020) -- End
            If mdtEmpTimesheet.Rows.Count <= 0 Then Exit Sub



            For Each item As DataGridItem In dgvEmpTimesheetList.Items
                CType(item.Cells(mdicETS("objdgcolhSelect")).FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
                Dim dRow As DataRow() = mdtEmpTimesheet.Select("emptimesheetunkid=" & CInt(item.Cells(mdicETS("objdgcolhEmpTimesheetID")).Text))
                If dRow.Length > 0 Then
                    For Each dR In dRow
                        dR.Item("IsChecked") = chkSelectAll.Checked
                    Next
                End If
            Next
            mdtEmpTimesheet.AcceptChanges()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If dgvEmpTimesheetList.Items.Count <= 0 Then Exit Sub

            Dim chkSelect As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)

            Dim dsRow As DataRow() = mdtEmpTimesheet.Select("emptimesheetunkid=" & CInt(item.Cells(mdicETS("objdgcolhEmpTimesheetID")).Text))
            If dsRow.Length > 0 Then
                dsRow(0).Item("IsChecked") = chkSelect.Checked
            End If
            mdtEmpTimesheet.AcceptChanges()

            Dim drRow As DataRow() = mdtEmpTimesheet.Select("IsGrp=0 AND employeeunkid=" & CInt(item.Cells(mdicETS("objdgcolhEmployeeID")).Text)) 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
            Dim dRow As DataRow() = mdtEmpTimesheet.Select("IsGrp=0 AND IsChecked=0 AND employeeunkid=" & CInt(item.Cells(mdicETS("objdgcolhEmployeeID")).Text)) 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
            Dim gRow As DataRow() = mdtEmpTimesheet.Select("IsGrp=1 AND employeeunkid=" & CInt(item.Cells(mdicETS("objdgcolhEmployeeID")).Text)) 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]

            If gRow.Length <= 0 Then Exit Sub

            If drRow.Length = dRow.Length Then
                gRow(0).Item("IsChecked") = False
            Else
                gRow(0).Item("IsChecked") = True
            End If
            gRow(0).AcceptChanges()

            Dim xCount As DataRow() = mdtEmpTimesheet.Select("IsChecked=True")
            If xCount.Length = dgvEmpTimesheetList.Items.Count Then
                CType(dgvEmpTimesheetList.Controls(mdicETS("objdgcolhSelect")).Controls(mdicETS("objdgcolhSelect")).FindControl("chkSelectAll"), CheckBox).Checked = True
            Else
                CType(dgvEmpTimesheetList.Controls(mdicETS("objdgcolhSelect")).Controls(mdicETS("objdgcolhSelect")).FindControl("chkSelectAll"), CheckBox).Checked = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelect_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkSelect_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            'Pinkal (31-Oct-2017) -- Start
            'Enhancement - Solve Budget Timesheet Issue As Per Suzan's Comment.
            If menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL Then
                Me.Title = Language._Object.getCaption("mnuViewPendingSubmitApproval", Me.lblPageHeader.Text)
                Me.lblPageHeader.Text = Language._Object.getCaption("mnuViewPendingSubmitApproval", Me.lblPageHeader.Text)

            ElseIf menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.VIEW_COMPLETED Then
                Me.Title = Language._Object.getCaption("mnuViewCompletedSubmitApproval", Me.lblPageHeader.Text)
                Me.lblPageHeader.Text = Language._Object.getCaption("mnuViewCompletedSubmitApproval", Me.lblPageHeader.Text)

            ElseIf menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.CANCELLED Then
                Me.Title = Language._Object.getCaption("mnuViewCompletedSubmitApproval", Me.lblPageHeader.Text)
                Me.lblPageHeader.Text = Language._Object.getCaption("mnuViewCompletedSubmitApproval", Me.lblPageHeader.Text)

            ElseIf menOperation = clsBudgetEmp_timesheet.enBudgetTimesheetStatus.DELETED Then
                Me.Title = Language._Object.getCaption("mnuGlobalDeleteTimesheet", Me.lblPageHeader.Text)
                Me.lblPageHeader.Text = Language._Object.getCaption("mnuGlobalDeleteTimesheet", Me.lblPageHeader.Text)
            End If

            'Pinkal (31-Oct-2017) -- End


            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)

            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)

            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnSubmitForApproval.Text = Language._Object.getCaption(Me.btnSubmitForApproval.ID, Me.btnSubmitForApproval.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.ID, Me.btnCancel.Text).Replace("&", "")
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.ID, Me.btnDelete.Text).Replace("&", "")




            Me.dgvEmpTimesheetList.Columns(2).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(2).FooterText, dgvEmpTimesheetList.Columns(2).HeaderText)
            Me.dgvEmpTimesheetList.Columns(3).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(3).FooterText, dgvEmpTimesheetList.Columns(3).HeaderText)
            Me.dgvEmpTimesheetList.Columns(4).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(4).FooterText, dgvEmpTimesheetList.Columns(4).HeaderText)
            Me.dgvEmpTimesheetList.Columns(5).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(5).FooterText, dgvEmpTimesheetList.Columns(5).HeaderText)
            Me.dgvEmpTimesheetList.Columns(6).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(6).FooterText, dgvEmpTimesheetList.Columns(6).HeaderText)
            Me.dgvEmpTimesheetList.Columns(7).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(7).FooterText, dgvEmpTimesheetList.Columns(7).HeaderText)
            Me.dgvEmpTimesheetList.Columns(8).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(8).FooterText, dgvEmpTimesheetList.Columns(8).HeaderText)
            Me.dgvEmpTimesheetList.Columns(9).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(9).FooterText, dgvEmpTimesheetList.Columns(9).HeaderText)
            Me.dgvEmpTimesheetList.Columns(10).HeaderText = Language._Object.getCaption(dgvEmpTimesheetList.Columns(10).FooterText, dgvEmpTimesheetList.Columns(10).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError("SetLanguage:- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

End Class
