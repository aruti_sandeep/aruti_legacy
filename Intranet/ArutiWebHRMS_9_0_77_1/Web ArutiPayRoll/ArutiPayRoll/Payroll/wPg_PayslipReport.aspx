﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_PayslipReport.aspx.vb"
    Inherits="Payroll_wPg_PayslipReport" Title="Salary Slip" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <center>
        <asp:Panel ID="pnlESS" runat="server" Style="width: 40%">
            <asp:UpdatePanel ID="uppnl_mianESS" runat="server">
                <ContentTemplate>
            <div class="panel-primary">
                <div class="panel-heading">
                    <asp:Label ID="lblPageHeader" runat="server" Text="Salary Slip"></asp:Label>
                </div>
                <div class="panel-body">
                    <div id="FilterCriteria" class="panel-default">
                        <div id="FilterCriteriaTitle" class="panel-heading-default">
                            <div style="float: left;">
                                <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                            </div>
                        </div>
                        <div id="FilterCriteriaBody" class="panel-body-default">
                            <table style="width: 100%">
                                <tr style="width: 100%">
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <td style="width: 30%">
                                                <asp:Label ID="lblFinancialYear" runat="server" Text="Financial Year"></asp:Label>
                                            </td>
                                            <td style="width: 70%" align="right">
                                                <asp:DropDownList ID="drpFinancialYear" runat="server" Width="95%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnPreview" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </tr>
                                <tr style="width: 100%">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <td style="width: 30%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label></label>
                                            </td>
                                            <td style="width: 70%" align="right">
                                                <asp:DropDownList ID="drpPeriod" runat="server" Width="95%">
                                                </asp:DropDownList>
                                            </td>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="drpFinancialYear" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="btnPreview" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </tr>
                            </table>
                            <div class="btn-default">
                                <asp:Button ID="btnPreview" runat="server" Text="Preview" CssClass="btnDefault" ValidationGroup="Payslip" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" ValidationGroup="Payslip"
                                    CausesValidation="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Panel ID="pnlMSS" runat="server" Style="width: 60%">
            <asp:UpdatePanel ID="uppnl_mianMSS" runat="server">
                <ContentTemplate>
            <div class="panel-primary">
                <div class="panel-heading">
                    <asp:Label ID="Label1" runat="server" Text="Salary Slip"></asp:Label>
                </div>
                <div class="panel-body">
                    <div id="Div2" class="panel-default">
                        <div id="Div3" class="panel-heading-default">
                            <div style="float: left;">
                                <asp:Label ID="Label2" runat="server" Text="Filter Criteria"></asp:Label>
                            </div>
                                    <div style="text-align: right;">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" CssClass="lnkhover"
                                            Style="color: Blue; vertical-align: top"></asp:LinkButton>
                                    </div>
                        </div>
                        <div id="Div4" class="panel-body-default" style="position:relative">
                            <table style="width: 100%">
                                <tr style="width: 100%">
                                            <%--<asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>--%>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblMembership" runat="server" Text="Membership"></asp:Label></label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboMembership" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                                <%--</ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnPreview" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>--%>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboLeaveType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                                <%--</ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnPreview" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                                        </Triggers>
                                            </asp:UpdatePanel>--%>
                                </tr>
                                <tr style="width: 100%">
                                           <%-- <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>--%>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblCurrency" runat="server" Text="Currency"></asp:Label>
                                            </td>
                                            <td style="width: 35%" >
                                                <asp:DropDownList ID="cboCurrency" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                                    <td colspan="2" style="width: 50%">
                                                        <asp:CheckBox ID="chkShowEachPayslipOnNewPage" runat="server" Text="Show Each Payslip On New Page"
                                                            Checked="true" AutoPostBack="true" />
                                                    </td>
                                               <%-- </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnPreview" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                                        </Triggers>
                                            </asp:UpdatePanel>--%>
                                </tr>
                                <tr style="width: 100%">
                                            <td colspan="2" style="width: 50%">
                                        <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                    </td>
                                            <td colspan="2" style="width: 50%">
                                        <asp:CheckBox ID="chkShowTwoPayslipPerPage" runat="server" Text="Show Two Payslip Per Page"
                                             AutoPostBack="true" />
                                    </td>
                                </tr>
                                <tr style="width: 100%">
                                            <td colspan="4" style="width: 100%">
                                        <asp:CheckBox ID="chkShowBankAccountNo" runat="server" Text="Show Bank Account No."
                                             AutoPostBack="false" />
                                    </td>
                                    <tr style="width: 100%">
                                            <td colspan="4" style="width: 100%">
                                        <asp:CheckBox ID="chkShowLoanReceived" runat="server" Text="Show Loan Received" Checked="true" Visible="false"
                                             AutoPostBack="false" />
                                    </td>
                                </tr>
                            </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                    </div>
                            <table style="width: 100%; margin-top:10px">
                                <tr style="width: 100%">
                                            <td style="width: 48%">
                                                <%--<asp:TextBox ID="txtSearchEmployee" runat="server" AutoPostBack="true"></asp:TextBox>--%>
                                                <input type="text" id="txtSearchEmployee" name="txtSearchEmployee"
                                                           placeholder="Search Employee" maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchEmployee', '<%= dgvEmployee.ClientID %>', 'chkEmpOnlyTicked', 'chkSelect');" />
                                            </td>
                                            <td style="width: 48%">
                                                <asp:CheckBox ID="chkEmpOnlyTicked" runat="server" Text="Only Ticked Items" ToolTip="Show Only Ticked Items"
                                                    CssClass="chk-sm" AutoPostBack="false" onclick="FromSearching('txtSearchEmployee', '<% dgvEmployee.ClientID %>', 'chkEmpOnlyTicked', 'chkSelect');" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 48%; vertical-align: top;">
                                                <%--<asp:Panel ID="pnl_gvEmployee" runat="server" ScrollBars="Auto" Height="421px">--%>
                                                <div id="scrollable-container" style="width: 100%; height: 315px; overflow: auto;
                                                    vertical-align: top" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                    <asp:GridView ID="dgvEmployee" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                    AllowPaging="False" Width="99%" CssClass="gridview" HeaderStyle-Font-Bold="false"
                                                        HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" DataKeyNames="employeeunkid">                                                    
                                                <Columns>
                                                        <asp:TemplateField HeaderStyle-CssClass="headerstyle" ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="false" onclick="CheckAll(this, 'chkSelect');"
                                                                    Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="false" 
                                                                    Text=" " />
                                                        </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                            Visible="false"></asp:BoundField>
                                                        <asp:BoundField DataField="employeename" HeaderText="Employee" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                FooterText="lblEmployees">
                                                        </asp:BoundField>
                                                    </Columns>                                                    
                                                </asp:GridView>
                                                </div>
                                                <%--</asp:Panel>--%>
                                        <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
                                    </td>
                                            <td style="width: 48%; vertical-align: top;">
                                                <%--<asp:Panel ID="pnl_gvPeriod" runat="server" ScrollBars="Auto">--%>
                                                <div id="scrollable-container1" style="width: 100%; overflow: auto; vertical-align: top"
                                                    onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                    <asp:GridView ID="gvPeriod" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        AllowPaging="False" Width="99%" CssClass="gridview" HeaderStyle-Font-Bold="false"
                                                        HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" DataKeyNames="periodunkid, start_date, end_date">
                                                <Columns>
                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAllPeriod" runat="server" AutoPostBack="false" onclick="CheckAll(this, 'chkSelectPeriod');" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelectPeriod" runat="server" AutoPostBack="false"   />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="periodunkid" HeaderText="periodunkid" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundField DataField="period_name" HeaderText="Pay Period" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                        FooterText="lblPeriods" />
                                                    <asp:BoundField DataField="start_date" HeaderText="start_date" ReadOnly="true" Visible="false"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="end_date" HeaderText="end_date" ReadOnly="true" Visible="false"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                </Columns>
                                            </asp:GridView>
                                                </div>
                                                <%--</asp:Panel>--%>
                                        <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
                                    </td>
                                </tr>
                            </table>
                            <div id="btnfixedbottom" class="btn-default">
                                <asp:Button ID="btnMSSReport" runat="server" CssClass="btndefault" Text="Report"
                                    ValidationGroup="PayslipMSS" />
                                <asp:Button ID="btnMSSClose" runat="server" Text="Close" CssClass="btndefault" ValidationGroup="PayslipMSS"
                                    CausesValidation="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    
    <script type="text/javascript">
    var scroll = {
        Y: '#<%= hfScrollPosition.ClientID %>'
    };
    var scroll1 = {
        Y: '#<%= hfScrollPosition1.ClientID %>'
    };
    function pageLoad(sender, args) {
        $("select").searchable();

    }

    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    function EndRequestHandler(sender, args) {
        if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll1.Y).val());
    }
}

function CheckAll(chkbox, chkgvSelect) {
    var grid = $(chkbox).closest(".gridview");
    //$("[id*=" + chkgvSelect + "]", grid).prop("checked", $(chkbox).prop("checked"));
    $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));
}

function FromSearching(txtID, gridClientID, chkID, chkGvID) {
    if (gridClientID.toString().includes('%') == true)
        gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();

    if ($('#' + txtID).val().length > 0) {
        $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
        $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
        //$('#' + gridClientID + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
        if (chkID != null && $$(chkID).is(':checked') == true) {
            $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\') ').parent().find("[id*='" + chkGvID + "']:checked").closest('tr').show();
        }
        else {
            $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();
        }

    }
    else if ($('#' + txtID).val().length == 0) {
        resetFromSearchValue(txtID, gridClientID, chkID, chkGvID);
    }
    if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
        $('.norecords').remove();
    }

    if (event.keyCode == 27) {
        resetFromSearchValue(txtID, gridClientID, chkID);
    }
}

function resetFromSearchValue(txtID, gridClientID, chkID, chkGvID) {

    $('#' + txtID).val('');
    //$('#' + gridClientID + ' tr').show();
    //$('.norecords').remove();
    if (chkID != null && $$(chkID).is(':checked') == true) {
        $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
        $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
        $('#' + $$(gridClientID).get(0).id + ' tr').find("[id*='" + chkGvID + "']:checked").closest('tr').show();
    }
    else {
        $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
        $('.norecords').remove();
    }
    $('#' + txtID).focus();
}
    
    </script>
</asp:Content>
