﻿'Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.Data
Imports System.Security.Cryptography
Imports Newtonsoft.Json

#End Region


Partial Class Payroll_Rpt_Bank_Payment_List
    Inherits Basepage

#Region " Private Variable "

    Dim DisplayMessage As New CommonCodes
    Private objBankPaymentList As clsBankPaymentList
    Private objBank As clspayrollgroup_master
   
    Private mintFirstOpenPeriod As Integer = 0

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmBankPaymentList"
    'Anjan [04 June 2014 ] -- End

    'Hemant (27 June 2019) -- Start
    'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
    Private Shared ReadOnly mstrModuleName1 As String = "frmEFTCustomColumnsExport"
    'Hemant (27 June 2019) -- End
    'Hemant (26 May 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
    Private Shared ReadOnly mstrModuleName3 As String = "frmPaymentBatchPosting"
    Private Shared ReadOnly mstrModuleName4 As String = "frmPaymentBatchPostingApproval"
    'Hemant (26 May 2023) -- End

    'Sohail (12 Feb 2015) -- Start
    'Enhancement - EFT CBA with New CSV Format.
    Private mstrOrderByDisplay As String = ""
    Private mstrOrderByQuery As String = ""
    'Sohail (12 Feb 2015) -- End

    'SHANI [13 Mar 2015]-START
    'Enhancement - New EFT Report Custom CSV Report.
    Private objUserDefRMode As New clsUserDef_ReportMode
    Private mintModeId As Integer = -1
    Private mstrEFTCustomColumnsIds As String = String.Empty
    Private mblnShowColumnHeader As Boolean = False
    Private marrEFTCustomColumnIds As New ArrayList
    Private dtEFTTable As New DataTable
    'SHANI [13 Mar 2015]--END 
    'Sohail (13 Apr 2016) -- Start
    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
    Private mintMembershipUnkId As Integer = 0
    'Sohail (13 Apr 2016) -- End
    'Hemant (27 June 2019) -- Start
    'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
    Private mblnSaveAsTXT As Boolean = False
    Private mblnTabDelimiter As Boolean = False
    'Hemant (27 June 2019) -- End
    'Hemant (02 Jul 2020) -- Start
    'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
    Private mstrDateFormat As String = String.Empty
    'Hemant (02 Jul 2020) -- End
    'Hemant (26 May 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
    Private mdsGenericCSVPostWeb As DataSet
    Private mintEFTMembershipUnkId As Integer
    Private mblnpopupReconciliation As Boolean = False
    Private mblnpopupBatchApproval As Boolean = False
    Private mdtReconciliationData As New DataTable
    Private mstrVoucherNo As String
    Private mblnIsFromGrid As Boolean
    'Hemant (26 May 2023) -- End
    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    Private mdtStatus As New DataTable
    Private mstrOldBatchName As String
    'Hemant (21 Jul 2023) -- End
#End Region

    'Sohail (01 Apr 2014) -- Start
    'ENHANCEMENT - 
#Region " Private Enum "
    Private Enum enHeadTypeId
        Company_Bank = 1
        Company_Branch = 2
        Company_Account_No = 3
        Report_Mode = 4
        Employee_Bank = 5
        Employee_Branch = 6
        Country = 7
        Paid_Currency = 8
        Show_Signatory_1 = 9
        Show_Signatory_2 = 10
        Show_Signatory_3 = 11
        Show_Defined_Signatory_1 = 12
        Show_Defined_Signatory_2 = 13
        Show_Defined_Signatory_3 = 14
        Show_Bank_Branch_Group = 15
        Show_Employee_Sign = 16
        Include_Inactive_Employee = 17
        Show_Period = 18
        Show_Sort_Code = 19
        Show_Separate_FName_Surname = 20
        Show_Bank_Code = 21
        Show_Branch_Code = 22
        Show_Account_Type = 23
        Show_employee_code = 24
        'Sohail (28 Feb 2017) -- Start
        'Enhancement - Add New Bank Payment List Integration for TAMA
        Letterhead = 26
        Address_to_Employee_Bank = 27
        'Sohail (28 Feb 2017) -- End

        'Sohail (12 Feb 2018) -- Start
        'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
        Present_Days = 28
        Basic_Salary = 29
        Social_Security = 30
        'Sohail (12 Feb 2018) -- End
        'Sohail (16 Apr 2018) -- Start
        'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
        ExtraIncome = 31
        AbsentDays = 32
        IdentityType = 33
        PaymentType = 34
        'Sohail (16 Apr 2018) -- End
        'Sohail (19 Apr 2018) -- Start
        'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
        CustomCurrencyFormat = 35
        'Sohail (19 Apr 2018) -- End
        'Hemant (19 June 2019) -- Start
        'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
        Show_Selected_Bank_Info = 36
        'Hemant (19 June 2019) -- End
        'Hemant 20 Jul 2019) -- Start
        'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
        Show_Company_Group_Info = 37
        'Hemant 20 Jul 2019) -- End
        'Hemant (16 Jul 2020) -- Start
        'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
        Show_Company_Logo = 38
        Save_As_TXT = 39
        'Hemant (16 Jul 2020) -- End        
        'Sohail (08 Jul 2021) -- Start
        'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
        Membership = 40
        'Sohail (08 Jul 2021) -- End
    End Enum
#End Region
    'Sohail (01 Apr 2014) -- End

    'Sohail (13 Apr 2016) -- Start
    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
#Region " Private Enum "
    Private Enum enReportMode
        CUSTOM_COLUMNS = 0
        SHOW_REPORT_HEADER = 1
        MEMBERSHIP = 2
        'Hemant (27 June 2019) -- Start
        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
        SAVE_AS_TXT = 3
        TAB_DELIMITER = 4
        'Hemant (27 June 2019) -- End
        'Hemant (02 Jul 2020) -- Start
        'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
        DATE_FORMAT = 5
        'Hemant (02 Jul 2020) -- End
    End Enum
#End Region
    'Sohail (13 Apr 2016) -- End

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Payroll Summary Report for KBC.
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (21 Jan 2016) -- End

            objBankPaymentList = New clsBankPaymentList(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            SetLanguage()
            'Anjan [04 June 2014 ] -- End

            Dim mintCurrPeriodID As Integer = 0
            If Not IsPostBack Then
                FillCombo()
                ResetValue()
                'Sohail (12 Feb 2015) -- Start
                'Enhancement - EFT CBA with New CSV Format.
            Else
                mstrOrderByDisplay = Me.ViewState("mstrOrderByDisplay").ToString
                mstrOrderByQuery = Me.ViewState("mstrOrderByQuery").ToString
                'Sohail (12 Feb 2015) -- End
                mintModeId = CInt(Me.ViewState("mintModeId"))
                mstrEFTCustomColumnsIds = Me.ViewState("mstrEFTCustomColumnsIds").ToString
                mblnShowColumnHeader = CBool(Me.ViewState("mblnShowColumnHeader"))
                marrEFTCustomColumnIds = CType(Me.ViewState("marrEFTCustomColumnIds"), ArrayList)
                dtEFTTable = CType(Me.ViewState("dtEFTTable"), DataTable)
                mintMembershipUnkId = CInt(Me.ViewState("mintMembershipUnkId"))   'Sohail (13 Apr 2016)
                'Hemant (27 June 2019) -- Start
                'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                mblnSaveAsTXT = CBool(Me.ViewState("mblnSaveAsTXT"))
                mblnTabDelimiter = CBool(Me.ViewState("mblnTabDelimiter"))
                'Hemant (27 June 2019) -- End
                'Hemant (02 Jul 2020) -- Start
                'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                mstrDateFormat = CStr(Me.ViewState("mstrDateFormat"))
                'Hemant (02 Jul 2020) -- End
                'Hemant (26 May 2023) -- Start
                'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
                mdsGenericCSVPostWeb = CType(Me.ViewState("mdsGenericCSVPostWeb"), DataSet)
                mdtReconciliationData = CType(Me.ViewState("mdtReconciliationData"), DataTable)
                mblnpopupReconciliation = CBool(Me.ViewState("mblnpopupReconciliation"))
                mblnpopupBatchApproval = CBool(Me.ViewState("mblnpopupBatchApproval"))
                mstrVoucherNo = CStr(Me.ViewState("mstrVoucherNo"))
                mblnIsFromGrid = CBool(Me.ViewState("mblnIsFromGrid"))
                If mblnpopupReconciliation Then
                    popupReconciliation.Show()
                Else
                    popupReconciliation.Hide()
                End If
                If mblnpopupBatchApproval Then
                    popupBatchApproval.Show()
                Else
                    popupBatchApproval.Hide()
                End If
                mdtStatus = CType(Me.ViewState("mdtStatus"), DataTable)
                mstrOldBatchName = Me.ViewState("mstrOldBatchName")
                'Hemant (26 May 2023) -- End
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Sohail (12 Feb 2015) -- Start
    'Enhancement - EFT CBA with New CSV Format.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrOrderByDisplay") = mstrOrderByDisplay
            Me.ViewState("mstrOrderByQuery") = mstrOrderByQuery
            Me.ViewState("mintModeId") = mintModeId
            Me.ViewState("mstrEFTCustomColumnsIds") = mstrEFTCustomColumnsIds
            Me.ViewState("mblnShowColumnHeader") = mblnShowColumnHeader
            Me.ViewState("marrEFTCustomColumnIds") = marrEFTCustomColumnIds
            Me.ViewState("dtEFTTable") = dtEFTTable
            Me.ViewState("mintMembershipUnkId") = mintMembershipUnkId 'Sohail (13 Apr 2016)
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            Me.ViewState("mblnSaveAsTXT") = mblnSaveAsTXT
            Me.ViewState("mblnTabDelimiter") = mblnTabDelimiter
            'Hemant (27 June 2019) -- End
            'Hemant (02 Jul 2020) -- Start
            'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
            Me.ViewState("mstrDateFormat") = mstrDateFormat
            'Hemant (02 Jul 2020) -- End
            'Hemant (26 May 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
            Me.ViewState("mdsGenericCSVPostWeb") = mdsGenericCSVPostWeb
            Me.ViewState("mdtReconciliationData") = mdtReconciliationData
            Me.ViewState("mblnpopupReconciliation") = mblnpopupReconciliation
            Me.ViewState("mblnpopupBatchApproval") = mblnpopupBatchApproval
            Me.ViewState("mstrVoucherNo") = mstrVoucherNo
            Me.ViewState("mblnIsFromGrid") = mblnIsFromGrid
            Me.ViewState("mdtStatus") = mdtStatus
            Me.ViewState("mstrOldBatchName") = mstrOldBatchName
            'Hemant (26 May 2023) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (12 Feb 2015) -- End

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objExRate As New clsExchangeRate
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim objCountry As clsMasterData
        Dim objEmp As clsEmployee_Master
        Dim objPeriod As clscommom_period_Tran
        Dim objCurrentPeriodId As clsMasterData
        Dim itm As ListItem
        Dim objMembership As New clsmembership_master 'Sohail (13 Apr 2016)
        Dim objMaster As New clsMasterData 'Nilay (10-Nov-2016)
        'Sohail (12 Feb 2018) -- Start
        'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
        Dim objTransactionHead As New clsTransactionHead
        'Sohail (12 Feb 2018) -- End
        Try
            objEmp = New clsEmployee_Master
            objBank = New clspayrollgroup_master
            objCountry = New clsMasterData
            objPeriod = New clscommom_period_Tran
            objCurrentPeriodId = New clsMasterData

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
            '                                CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                Session("UserAccessModeSetting").ToString, True, _
            '                                CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            Session("UserAccessModeSetting").ToString, True, _
                                            True, "Emp", True)
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            With cboEmployeeName
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = "0"
            End With


            dsList = objBank.getListForCombo(enPayrollGroupType.Bank, "List", True)
            With cboBankName
                .DataValueField = "groupmasterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            cboBankName_SelectedIndexChanged(New Object(), New EventArgs())


            dsList = objCountry.getCountryList("List", True)
            With cboCountry
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

       
            mintFirstOpenPeriod = objCountry.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "List", True, , , , Session("database_name").ToString)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString, CDate(Session("fin_startdate")), "List", True)
            'Shani(20-Nov-2015) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = mintFirstOpenPeriod.ToString
                Me.ViewState.Add("FirstOpenPeriod", mintFirstOpenPeriod)
                Call cboPeriod_SelectedIndexChanged(cboPeriod, New System.EventArgs) 'Sohail (07 Dec 2013)
            End With

            dsList = objExRate.getComboList("Currency", True)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_name"
                .DataSource = dsList.Tables("Currency")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            dsList = objMaster.GetCondition(False, False, True, True, False)
            With cboCondition
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedIndex = 0
            End With
            'Nilay (10-Nov-2016) -- End

            cboReportType.Items.Clear()
            Language.setLanguage(mstrModuleName)
            itm = New ListItem(Language.getMessage(mstrModuleName, 2, "Bank Payment List"), CInt(enBankPaymentReport.Bank_payment_List).ToString)
            cboReportType.Items.Add(itm)

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            'Anjan [04 June 2014 ] -- End
            itm = New ListItem(Language.getMessage(mstrModuleName, 3, "Electronic Funds Transfer"), CInt(enBankPaymentReport.Electronic_Fund_Transfer).ToString)
            cboReportType.Items.Add(itm)

            If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_StandardBank Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                itm = New ListItem(Language.getMessage(mstrModuleName, 10, "EFT Standard Bank"), CInt(enBankPaymentReport.EFT_Standard_Bank).ToString)
                cboReportType.Items.Add(itm)
            End If

            If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_StandardCharteredBank Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                itm = New ListItem(Language.getMessage(mstrModuleName, 12, "EFT Standard Chartered Bank"), CInt(enBankPaymentReport.EFT_StandardCharteredBank).ToString)
                cboReportType.Items.Add(itm)
            End If

            If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_SFIBank Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                itm = New ListItem(Language.getMessage(mstrModuleName, 13, "EFT SFI Bank"), CInt(enBankPaymentReport.EFT_SFIBANK).ToString)
                cboReportType.Items.Add(itm)
            End If

            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            itm = New ListItem("Bank Payment Voucher Report", CInt(enBankPaymentReport.Bank_Payment_Voucher).ToString)
            cboReportType.Items.Add(itm)
            'Pinkal (12-Jun-2014) -- End

            'Sohail (25 Mar 2015) -- Start
            'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
            itm = New ListItem(Language.getMessage(mstrModuleName, 21, "EFT Advance Salary CSV"), CInt(enBankPaymentReport.EFT_ADVANCE_SALARY_CSV).ToString)
            cboReportType.Items.Add(itm)

            itm = New ListItem(Language.getMessage(mstrModuleName, 22, "EFT Advance Salary XSV"), CInt(enBankPaymentReport.EFT_ADVANCE_SALARY_XLS).ToString)
            cboReportType.Items.Add(itm)
            'Sohail (25 Mar 2015) -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            If CInt(Session("Compcountryid")) = 162 Then 'OMAN Salary Format WPS
                itm = New ListItem(Language.getMessage(mstrModuleName, 27, "Salary Format WPS"), CInt(enBankPaymentReport.Salary_Format_WPS).ToString())
                cboReportType.Items.Add(itm)
            End If
            'Sohail (12 Feb 2018) -- End

            cboReportType.SelectedIndex = 0
            With cboReportMode
                .Items.Clear()
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                .Items.Add(Language.getMessage(mstrModuleName, 8, "DRAFT"))
                .Items.Add(Language.getMessage(mstrModuleName, 9, "AUTHORIZED"))
                .SelectedIndex = 0
            End With

            cboReportType_SelectedIndexChanged(New Object(), New EventArgs())

            dsList = objCompanyBank.GetComboList(CInt(Session("CompanyUnkId")), 1, "Bank")
            With cboCompanyBankName
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Bank")
                .DataBind()
                .SelectedValue = "0"
            End With

            cboCompanyBankName_SelectedIndexChanged(New Object(), New EventArgs())


            'Sohail (13 Apr 2016) -- Start
            'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
            dsList = objMembership.getListForCombo("Membership", True)
            With cboMembership
                .DataValueField = "membershipunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Membership")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Sohail (13 Apr 2016) -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            dsList = objTransactionHead.getComboList(Session("Database_Name").ToString, "List", True, CInt(enTranHeadType.Informational))
            With cboPresentDays
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            'dsList = objTransactionHead.getComboList(Session("Database_Name").ToString, "List", True, CInt(enTranHeadType.EarningForEmployees))
            dsList = objTransactionHead.getComboList(Session("Database_Name").ToString, "List", True)
            'Sohail (16 Apr 2018) -- End
            With cboBasicSalary
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            With cboExtraIncome
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboAbsentDays
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
                .SelectedValue = "0"
            End With
            'Sohail (16 Apr 2018) -- End

            dsList = objTransactionHead.getComboList(Session("Database_Name").ToString, "List", True, , , , , , "trnheadtype_id IN (" & CInt(enTranHeadType.DeductionForEmployee) & ", " & CInt(enTranHeadType.EmployeesStatutoryDeductions) & ")")
            With cboSocialSecurity
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Sohail (12 Feb 2018) -- End

            'Sohail (08 Jul 2021) -- Start
            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
            dsList = objMembership.getListForCombo("Membership", True)
            With cboMembershipRepo
                .DataValueField = "membershipunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Membership")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Sohail (08 Jul 2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo :-" & ex.Message, Me)
        Finally

            dsList.Dispose()
            dsList = Nothing
            objEmp = Nothing
            objBank = Nothing
            objCountry = Nothing
            objPeriod = Nothing
            objCurrentPeriodId = Nothing
            objExRate = Nothing 
            objMembership = Nothing 'Sohail (13 Apr 2016)
            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            objTransactionHead = Nothing
            'Sohail (12 Feb 2018) -- End
        End Try

    End Sub

    Private Sub ResetValue()

        Try
            cboBankName.SelectedValue = "0"
            cboBranchName.SelectedValue = "0"
            cboCountry.SelectedValue = "0"
            cboEmployeeName.SelectedValue = "0"
            cboPeriod.SelectedValue = mintFirstOpenPeriod.ToString
            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            'txtEmpCode.Text = ""
            cboChequeNo.SelectedIndex = 0
            'Sohail (07 Dec 2013) -- End
            txtAmount.Text = "0"
            txtAmountTo.Text = "0"

            chkEmployeeSign.Checked = False
            chkSignatory1.Checked = False
            chkSignatory2.Checked = False
            chkSignatory3.Checked = False
            chkDefinedSignatory.Checked = False
            'Sohail (24 Dec 2013) -- Start
            'Enhancement - Oman
            If chkShowPayrollPeriod.Enabled = True Then
                chkShowPayrollPeriod.Checked = True
            Else
                chkShowPayrollPeriod.Checked = False
            End If
            If chkShowSortCode.Enabled = True Then
                chkShowSortCode.Checked = True
            Else
                chkShowSortCode.Checked = False
            End If
            'Sohail (24 Dec 2013) -- End
            chkShowFNameSeparately.Checked = False
            objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            cboCurrency.SelectedValue = "0"
            chkShowGroupByBankBranch.Checked = False
            cboReportMode.SelectedIndex = 0
            cboCompanyBankName.SelectedValue = "0"
            cboCompanyBranchName.SelectedValue = "0"
            cboCompanyAccountNo.SelectedValue = "0"


            'Anjan [03 Feb 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            chkShowBankCode.Checked = True
            chkShowBranchCode.Checked = True
            chkDefinedSignatory2.Checked = False
            chkDefinedSignatory3.Checked = False
            'Anjan [03 Feb 2014 ] -- End

            txtCutOffAmount.Text = "" 'Sohail (15 Mar 2014)

            'Sohail (01 Apr 2014) -- Start
            'ENHANCEMENT - 
            chkShowAccountType.Checked = False
            chkShowReportHeader.Checked = True
            chkShowEmployeeCode.Checked = True
            'Sohail (01 Apr 2014) -- End
            chkShowSelectedBankInfo.Checked = False 'Hemant (19 June 2019)
            chkShowCompanyGrpInfo.Checked = False 'Hemant (20 Jul 2019)

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            cboCondition.SelectedIndex = 0
            'Nilay (10-Nov-2016) -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            cboPresentDays.SelectedValue = "0"
            cboBasicSalary.SelectedValue = "0"
            cboSocialSecurity.SelectedValue = "0"
            'Sohail (12 Feb 2018) -- End
            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            cboExtraIncome.SelectedValue = "0"
            cboAbsentDays.SelectedValue = "0"
            txtIdentityType.Text = ""
            txtPaymentType.Text = ""
            'Sohail (16 Apr 2018) -- End
            'Sohail (19 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            txtCustomCurrFormat.Text = ""
            'Sohail (19 Apr 2018) -- End
            'Hemant (16 Jul 2020) -- Start
            'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
            chkSaveAsTXT_WPS.Checked = False
            'Hemant (16 Jul 2020) -- End
            cboMembershipRepo.SelectedValue = "0" 'Sohail (08 Jul 2021)"

            Call GetValue() 'Sohail (01 Apr 2014)

        Catch ex As Exception
            DisplayMessage.DisplayError("ResetValue :- " & ex.Message, Me)
        End Try

    End Sub

    Private Function SetFilter() As Boolean
        Try

            If CInt(cboReportType.SelectedIndex) > 0 AndAlso CInt(cboCurrency.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 4, "Currency is mandatory information. Please select Currency."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Currency is mandatory information. Please select Currency."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Return False

            ElseIf CInt(cboCompanyBankName.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 5, "Please select Company Bank."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please select Company Bank."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCompanyBankName.Focus()
                Return False

            ElseIf CInt(cboCompanyBranchName.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 6, "Please select Company Bank Branch."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Please select Company Bank Branch."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCompanyBranchName.Focus()
                Return False

            ElseIf CInt(cboCompanyAccountNo.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 7, "Please select Company Bank Account."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please select Company Bank Account."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCompanyAccountNo.Focus()
                Return False

                Return False

            End If

            objBankPaymentList.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objBankPaymentList._PeriodId = CInt(cboPeriod.SelectedValue).ToString
            End If

            If CInt(cboBankName.SelectedValue) > 0 Then
                objBankPaymentList._BankId = CInt(cboBankName.SelectedValue)
                objBankPaymentList._BankName = cboBankName.SelectedItem.Text
            End If

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            If CInt(cboCondition.SelectedValue) > 0 Then
                objBankPaymentList._ConditionId = CInt(cboCondition.SelectedValue)
                objBankPaymentList._ConditionText = cboCondition.SelectedItem.Text
            End If
            'Nilay (10-Nov-2016) -- End

            If CInt(cboBranchName.SelectedValue) > 0 Then
                objBankPaymentList._BranchId = CInt(cboBranchName.SelectedValue)
                objBankPaymentList._BankBranchName = cboBranchName.SelectedItem.Text
            End If

            If CInt(cboCountry.SelectedValue) > 0 Then
                objBankPaymentList._CountryId = CInt(cboCountry.SelectedValue)
                objBankPaymentList._CountryName = cboCountry.SelectedItem.Text
            End If

            If CInt(cboEmployeeName.SelectedValue) > 0 Then
                objBankPaymentList._EmployeeUnkId = CInt(cboEmployeeName.SelectedValue)
                objBankPaymentList._EmpName = cboEmployeeName.SelectedItem.Text
            End If

            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            'If Trim(txtEmpCode.Text) <> "" Then
            '    objBankPaymentList._EmpCode = txtEmpCode.Text
            'End If
            If cboChequeNo.SelectedIndex > 0 Then
                objBankPaymentList._ChequeNo = cboChequeNo.Text
            Else
                objBankPaymentList._ChequeNo = ""
            End If
            'Sohail (07 Dec 2013) -- End

            If Trim(txtAmount.Text) <> "" And Trim(txtAmountTo.Text) <> "" Then
                objBankPaymentList._Amount = CDec(txtAmount.Text)
                objBankPaymentList._AmountTo = CDec(txtAmountTo.Text)
            End If

            If chkEmployeeSign.Checked = True Then
                objBankPaymentList._IsEmployeeSign = True
            End If
            If chkSignatory1.Checked = True Then
                objBankPaymentList._IsSignatory1 = True
            End If
            If chkSignatory2.Checked = True Then
                objBankPaymentList._IsSignatory2 = True
            End If
            If chkSignatory3.Checked = True Then
                objBankPaymentList._IsSignatory3 = True
            End If

            'Anjan [ 22 Nov 2013 ] -- Start
            'ENHANCEMENT : Requested by Rutta
            If chkDefinedSignatory.Checked = True Then
                objBankPaymentList._IsDefinedSignatory = True
            End If
            'Anjan [22 Nov 2013 ] -- End

            'Sohail (24 Dec 2013) -- Start
            'Enhancement - Oman
            objBankPaymentList._ShowPayrollPeriod = chkShowPayrollPeriod.Checked
            objBankPaymentList._ShowSortCode = chkShowSortCode.Checked
            'Sohail (24 Dec 2013) -- End
            objBankPaymentList._ShowFNameSurNameInSeparateColumn = chkShowFNameSeparately.Checked
            objBankPaymentList._CurrencyId = CInt(cboCurrency.SelectedValue)
            objBankPaymentList._CurrencyName = cboCurrency.SelectedItem.Text
            objBankPaymentList._ShowGroupByBankBranch = chkShowGroupByBankBranch.Checked
            objBankPaymentList._PeriodName = cboPeriod.SelectedItem.Text

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            objBankPaymentList._PeriodCode = objPeriod._Period_Code 'Sohail (24 Dec 2014) 
            objBankPaymentList._PeriodStartDate = objPeriod._Start_Date
            objBankPaymentList._PeriodEndDate = objPeriod._End_Date
            'Sohail (24 Mar 2018) -- Start
            'Voltamp Enhancement - 70.2 - Changes in WPS report as per discussion with Prabhakar. [Salary month should be character for example Jan to be displayed as 01, Feb as 02, two digit number]
            objBankPaymentList._PeriodCustomCode = objPeriod._Sunjv_PeriodCode
            'Sohail (24 Mar 2018) -- End
            objPeriod = Nothing

            objBankPaymentList._ReportTypeId = CInt(CType(cboReportType.SelectedItem, ListItem).Value)
            objBankPaymentList._ReportTypeName = cboReportType.SelectedItem.Text
            objBankPaymentList._ReportModeId = CInt(cboReportMode.SelectedIndex)
            objBankPaymentList._ReportModeName = cboReportMode.SelectedItem.Text

            objBankPaymentList._CompanyBranchId = CInt(cboCompanyBranchName.SelectedValue)
            objBankPaymentList._CompanyBankBranchName = cboCompanyBranchName.SelectedItem.Text
            objBankPaymentList._CompanyBankAccountNo = cboCompanyAccountNo.SelectedItem.Text

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            objBankPaymentList._PresentDaysId = CInt(cboPresentDays.SelectedValue)
            objBankPaymentList._PresentDaysName = cboPresentDays.SelectedItem.Text

            objBankPaymentList._BasicSalaryId = CInt(cboBasicSalary.SelectedValue)
            objBankPaymentList._BasicSalaryName = cboBasicSalary.SelectedItem.Text

            objBankPaymentList._SocialSecurityId = CInt(cboSocialSecurity.SelectedValue)
            objBankPaymentList._SocialSecurityName = cboSocialSecurity.SelectedItem.Text
            'Sohail (12 Feb 2018) -- End

            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            objBankPaymentList._ExtraIncomeId = CInt(cboExtraIncome.SelectedValue)
            objBankPaymentList._ExtraIncomeName = cboExtraIncome.SelectedItem.Text

            objBankPaymentList._AbsentDaysId = CInt(cboAbsentDays.SelectedValue)
            objBankPaymentList._AbsentDaysName = cboAbsentDays.SelectedItem.Text

            objBankPaymentList._IdentityType = txtIdentityType.Text.Trim
            objBankPaymentList._PaymentType = txtPaymentType.Text.Trim
            'Sohail (16 Apr 2018) -- End
            'Sohail (19 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            objBankPaymentList._CustomCurrencyFormat = txtCustomCurrFormat.Text.Trim
            'Sohail (19 Apr 2018) -- End
            'Hemant (16 Jul 2020) -- Start
            'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
            objBankPaymentList._SaveAsTXT = chkSaveAsTXT_WPS.Checked
            'Hemant (16 Jul 2020) -- End

            objBankPaymentList._UserUnkId = CInt(Session("UserId"))
            objBankPaymentList._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objBankPaymentList._UserAccessFilter = Session("AccessLevelFilterString").ToString
            'objBankPaymentList._CompanyBankGroupId = Session("CompanyBankGroupId")
            'objBankPaymentList._CompanyBranchId = Session("CompanyBankBranchId")
            GUI.fmtCurrency = Session("fmtCurrency").ToString


            'Anjan [03 Feb 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            
            If chkDefinedSignatory2.Checked = True Then
                objBankPaymentList._IsDefinedSignatory2 = True
            End If
            If chkDefinedSignatory3.Checked = True Then
                objBankPaymentList._IsDefinedSignatory3 = True
            End If
            If chkShowBankCode.Checked = True Then
                objBankPaymentList._ShowBankCode = True
            End If
            If chkShowBranchCode.Checked = True Then
                objBankPaymentList._ShowBranchCode = True
            End If
            'Anjan [03 Feb 2014 ] -- End

            'Sohail (15 Mar 2014) -- Start
            'Enhancement - Grouping on cut off amount.
            Dim decAmt As Decimal
            Decimal.TryParse(txtCutOffAmount.Text, decAmt)
            objBankPaymentList._Cut_Off_Amount = decAmt
            'Sohail (15 Mar 2014) -- End

            'Sohail (01 Apr 2014) -- Start
            'ENHANCEMENT - 
            objBankPaymentList._ShowReportHeader = chkShowReportHeader.Checked
            objBankPaymentList._ShowAccountType = chkShowAccountType.Checked
            objBankPaymentList._ShowEmployeeCode = chkShowEmployeeCode.Checked
            'Sohail (01 Apr 2014) -- End

            'Sohail (12 Feb 2015) -- Start
            'Enhancement - EFT CBA with New CSV Format.
            objBankPaymentList.OrderByDisplay = mstrOrderByDisplay
            objBankPaymentList.OrderByQuery = mstrOrderByQuery
            'Sohail (12 Feb 2015) -- End

            'Sohail (01 Jun 2016) -- Start
            'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.
            objBankPaymentList._Posting_Date = dtpPostingDate.GetDate
            'Sohail (01 Jun 2016) -- End

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            objBankPaymentList._EFTCitiDirectCountryCode = Session("EFTCitiDirectCountryCode").ToString
            objBankPaymentList._EFTCitiDirectSkipPriorityFlag = CBool(Session("EFTCitiDirectSkipPriorityFlag"))
            objBankPaymentList._EFTCitiDirectChargesIndicator = Session("EFTCitiDirectChargesIndicator").ToString
			objBankPaymentList._EFTCitiDirectAddPaymentDetail = CBool(Session("EFTCitiDirectAddPaymentDetail"))
            'Sohail (09 Jan 2016) -- End

            'Sohail (28 Feb 2017) -- Start
            'Enhancement - Add New Bank Payment List Integration for TAMA
            objBankPaymentList._RoundOff_Type = CDbl(Session("RoundOff_Type"))
            objBankPaymentList._IsLetterhead = chkLetterhead.Checked
            objBankPaymentList._IsAddress_To_Emp_Bank = chkAddresstoEmployeeBank.Checked
            'Sohail (28 Feb 2017) -- End

            'Hemant (25 Jul 2019) -- Start
            'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Note the commas in the Bank branch address, they should be in all fields of the branch address or all be removed
            objBankPaymentList._CompanyBankGroupId = CInt(cboCompanyBankName.SelectedValue)
            'Hemant (25 Jul 2019) -- End
            'Sohail (04 Nov 2019) -- Start
            'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
            Dim objBankGroup As New clspayrollgroup_master
            objBankGroup._Groupmasterunkid = CInt(cboCompanyBankName.SelectedValue)
            objBankPaymentList._CompanyBankGroupCode = objBankGroup._Groupcode
            'Sohail (04 Nov 2019) -- End
            'Hemant (19 June 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            objBankPaymentList._ShowSelectedBankInfo = chkShowSelectedBankInfo.Checked
            'Hemant (19 June 2019) -- End
            'Hemant 20 Jul 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            objBankPaymentList._ShowCompanyGroupInfo = chkShowCompanyGrpInfo.Checked
            'Hemant 20 Jul 2019) -- End

            'Sohail (08 Jul 2021) -- Start
            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
            objBankPaymentList._MembershipUnkId = CInt(cboMembershipRepo.SelectedValue)
            objBankPaymentList._MembershipName = cboMembershipRepo.SelectedItem.Text
            'Sohail (08 Jul 2021) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("SetFilter :- " & ex.Message, Me)
        End Try

    End Function


    'Sohail (01 Apr 2014) -- Start
    'ENHANCEMENT - 
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            'SHANI (13 Mar 2015) -- Start
            'Enhancement - New EFT Report Custom CSV Report.
            'dsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList)
            dsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList, 0)
            'SHANI (13 Mar 2015) -- End

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Company_Bank
                            cboCompanyBankName.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString
                            Call cboCompanyBankName_SelectedIndexChanged(cboCompanyBankName, New EventArgs)

                        Case enHeadTypeId.Company_Branch
                            cboCompanyBranchName.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Company_Account_No
                            cboCompanyAccountNo.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Report_Mode
                            cboReportMode.SelectedIndex = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Employee_Bank
                            cboBankName.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString
                            Call cboBankName_SelectedIndexChanged(cboBankName, New EventArgs)

                        Case enHeadTypeId.Employee_Branch
                            cboBranchName.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Country
                            cboCountry.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Paid_Currency
                            cboCurrency.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Show_Signatory_1
                            chkSignatory1.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Signatory_2
                            chkSignatory2.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Signatory_3
                            chkSignatory3.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Defined_Signatory_1
                            chkDefinedSignatory.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Defined_Signatory_2
                            chkDefinedSignatory2.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Defined_Signatory_3
                            chkDefinedSignatory3.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Bank_Branch_Group
                            chkShowGroupByBankBranch.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Employee_Sign
                            chkEmployeeSign.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Include_Inactive_Employee
                            'chkIncludeInactiveEmp.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Period
                            chkShowPayrollPeriod.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Sort_Code
                            chkShowSortCode.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Separate_FName_Surname
                            chkShowFNameSeparately.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Bank_Code
                            chkShowBankCode.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Branch_Code
                            chkShowBranchCode.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Account_Type
                            chkShowAccountType.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_employee_code
                            chkShowEmployeeCode.Checked = CBool(dsRow.Item("transactionheadid"))

                            'Sohail (28 Feb 2017) -- Start
                            'Enhancement - Add New Bank Payment List Integration for TAMA
                        Case enHeadTypeId.Letterhead
                            chkLetterhead.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Address_to_Employee_Bank
                            chkAddresstoEmployeeBank.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Sohail (28 Feb 2017) -- End

                            'Sohail (12 Feb 2018) -- Start
                            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                        Case enHeadTypeId.Present_Days
                            cboPresentDays.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Basic_Salary
                            cboBasicSalary.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Social_Security
                            cboSocialSecurity.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString
                            'Sohail (12 Feb 2018) -- End

                            'Sohail (16 Apr 2018) -- Start
                            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
                        Case CInt(enHeadTypeId.ExtraIncome)
                            cboExtraIncome.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case CInt(enHeadTypeId.AbsentDays)
                            cboAbsentDays.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case CInt(enHeadTypeId.IdentityType)
                            txtIdentityType.Text = dsRow.Item("transactionheadid").ToString

                        Case CInt(enHeadTypeId.PaymentType)
                            txtPaymentType.Text = dsRow.Item("transactionheadid").ToString
                            'Sohail (16 Apr 2018) -- End

                            'Sohail (19 Apr 2018) -- Start
                            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
                        Case CInt(enHeadTypeId.CustomCurrencyFormat)
                            txtCustomCurrFormat.Text = dsRow.Item("transactionheadid").ToString
                            'Sohail (19 Apr 2018) -- End

                            'Hemant (16 Jul 2020) -- Start
                            'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
                        Case CInt(enHeadTypeId.Save_As_TXT)
                            chkSaveAsTXT_WPS.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Hemant (16 Jul 2020) -- End

                            'Hemant (19 June 2019) -- Start
                            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                        Case enHeadTypeId.Show_Selected_Bank_Info
                            chkShowSelectedBankInfo.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Hemant (19 June 2019) -- End

                            'Hemant (20 Jul 2019) -- Start
                            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                        Case enHeadTypeId.Show_Company_Group_Info
                            chkShowCompanyGrpInfo.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Hemant (20 Jul 2019) -- End

                            'Sohail (08 Jul 2021) -- Start
                            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
                        Case CInt(enHeadTypeId.Membership)
                            cboMembershipRepo.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString
                            'Sohail (08 Jul 2021) -- End

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("GetValue :- " & ex.Message, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Sohail (01 Apr 2014) -- End

    
    'SHANI [13 Mar 2015]-START
    'Enhancement - New EFT Report Custom CSV Report.
    Private Sub GetEFTValue()
        Dim dsList As DataSet
        Try
            'Sohail (25 Mar 2015) -- Start
            'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
            'dsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList, mintModeId)
            dsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList, mintModeId, CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            'Sohail (25 Mar 2015) -- End
            If dsList.Tables("List").Rows.Count > 0 Then
                'Hemant (27 June 2019) -- Start
                'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                'For i = 0 To 2
                For i As Integer = 0 To GetEnumHighestValue(Of enReportMode)()
                    'Hemant (27 June 2019) -- End
                    If i = enReportMode.CUSTOM_COLUMNS Then 'Column order
                        marrEFTCustomColumnIds.AddRange(dsList.Tables("List").Rows(i).Item("transactionheadid").ToString.Split(CChar(",")))

                    ElseIf i = enReportMode.SHOW_REPORT_HEADER Then 'Show Column Header
                        chkShowColumnHeader.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))

                        'Sohail (13 Apr 2016) -- Start
                        'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                    ElseIf i = enReportMode.MEMBERSHIP Then
                        cboMembership.SelectedValue = CInt(dsList.Tables("List").Rows(i).Item("transactionheadid")).ToString

                        If CInt(cboMembership.SelectedValue) > 0 Then
                            cboMembership.Enabled = True
                        Else
                            cboMembership.SelectedValue = "0"
                            cboMembership.Enabled = False
                        End If
                        'Sohail (13 Apr 2016) -- End
                        'Hemant (27 June 2019) -- Start
                        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                    ElseIf i = enReportMode.SAVE_AS_TXT Then 'SAVE AS TXT
                        If dsList.Tables("List").Rows.Count > 3 Then
                            chkSaveAsTXT.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        End If


                    ElseIf i = enReportMode.TAB_DELIMITER Then 'TAB DELIMITER
                        If dsList.Tables("List").Rows.Count > 4 Then
                            chkTABDelimiter.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        End If
                        'Hemant (27 June 2019) -- End

                        'Hemant (02 Jul 2020) -- Start
                        'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                    ElseIf i = enReportMode.DATE_FORMAT Then 'DATE FORMAT
                        If dsList.Tables("List").Rows.Count > 5 Then
                            txtDateFormat.Text = CStr(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        End If
                        'Hemant (02 Jul 2020) -- End

                    End If
                Next

            End If
        Catch ex As Exception
            popup_EFTCustom.Show()
            'Throw New Exception(ex.Message & "; Procedure : GetValue ; Module Name : " & mstrModuleName)
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub SetEFTValue()
        Try
            objUserDefRMode._Reportmodeid = mintModeId
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportunkid = enArutiReport.BankPaymentList
        Catch ex As Exception
            popup_EFTCustom.Show()
            'Throw New Exception(ex.Message & "; Procedure : SetValue ; Module Name : " & mstrModuleName)
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub FillEFTList()
        Dim objMaster As New clsMasterData
        Dim dsTicked As DataSet = Nothing
        Dim dsUnticked As DataSet = Nothing
        Dim lvItem As ListViewItem
        Try
            Dim xColCheck As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
            Dim xColName As New DataColumn("ID", System.Type.GetType("System.Int64"))
            Dim xColID As New DataColumn("Name", System.Type.GetType("System.String"))

            dtEFTTable.Columns.Add(xColCheck)
            dtEFTTable.Columns.Add(xColID)
            dtEFTTable.Columns.Add(xColName)

            Dim dicTicked As Dictionary(Of Integer, DataRow) = Nothing
            If marrEFTCustomColumnIds.Count > 0 Then
                dsTicked = objMaster.GetComboListForEFTCustomColumns("List", False, "ID IN (" & String.Join(",", CType(marrEFTCustomColumnIds.ToArray(Type.GetType("System.String")), String())) & ") ")
                dicTicked = (From p In dsTicked.Tables(0).AsEnumerable Select New With {.ID = CInt(p.Item("Id")), .DATAROW = p}).ToDictionary(Function(x) CInt(x.ID), Function(y) y.DATAROW)
                dsUnticked = objMaster.GetComboListForEFTCustomColumns("List", False, "ID NOT IN (" & String.Join(",", CType(marrEFTCustomColumnIds.ToArray(Type.GetType("System.String")), String())) & ") ")
            Else
                dsUnticked = objMaster.GetComboListForEFTCustomColumns("List", False)
            End If

            If dsTicked IsNot Nothing Then

                Dim dtRow As DataRow = Nothing
                For Each id As Integer In CType(marrEFTCustomColumnIds.Clone, ArrayList)
                    dtRow = dicTicked.Item(id)
                    Dim xRow As DataRow = dtEFTTable.NewRow
                    xRow("IsChecked") = True
                    xRow("ID") = dtRow.Item("Id")
                    xRow("Name") = dtRow.Item("NAME")
                    dtEFTTable.Rows.Add(xRow)
                Next

            End If

            If dsUnticked IsNot Nothing Then
                For Each dtRow As DataRow In dsUnticked.Tables(0).Rows
                    Dim xRow As DataRow = dtEFTTable.NewRow
                    xRow("IsChecked") = False
                    xRow("ID") = dtRow.Item("Id")
                    xRow("Name") = dtRow.Item("NAME")
                    dtEFTTable.Rows.Add(xRow)
                    lvItem = Nothing
                Next
            End If
            With lvEFTCustomColumns
                .DataSource = dtEFTTable
                .DataBind()
            End With

        Catch ex As Exception
            popup_EFTCustom.Show()
            'Throw New Exception(ex.Message & "; Procedure : FillList ; Module Name : " & mstrModuleName)
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            lvItem = Nothing
        End Try
    End Sub

    Private Function IsEFTValidate() As Boolean
        Try
            Select Case CInt(mintModeId)
                Case enEFT_Export_Mode.CSV, enEFT_Export_Mode.XLS
                    Dim xRow() As DataRow = dtEFTTable.Select("IsChecked=True")

                    If xRow.Length <= 0 Then
                        'Hemant (27 June 2019) -- Start
                        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                        'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please Select atleast one column to export EFT report."), Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Please Select atleast one column to export EFT report."), Me)
                        'Hemant (27 June 2019) -- End
                        lvEFTCustomColumns.Focus()
                        popup_EFTCustom.Show()
                        Return False
                        'Sohail (13 Apr 2016) -- Start
                        'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                    ElseIf CBool(dtEFTTable.Select("Id = " & enEFT_EFT_Custom_Columns.EMP_MEMBERSHIP_NO & " ")(0).Item("isChecked")) = True AndAlso CInt(cboMembership.SelectedValue) <= 0 Then
                        'Hemant (27 June 2019) -- Start
                        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                        'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Please Select Membership."), Me) 
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Please Select Membership."), Me)
                        'Hemant (27 June 2019) -- End
                        cboMembership.Focus()
                        popup_EFTCustom.Show()
                        Return False
                        'Sohail (13 Apr 2016) -- End
                        'Hemant (02 Jul 2020) -- Start
                        'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                    ElseIf CBool(dtEFTTable.Select("Id = " & enEFT_EFT_Custom_Columns.PAYMENT_DATE & " ")(0).Item("isChecked")) = True AndAlso txtDateFormat.Text = "" Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 4, "Please Enter Date Format."), Me)
                        cboMembership.Focus()
                        popup_EFTCustom.Show()
                        Return False
                        'Hemant (02 Jul 2020) -- End
                    End If
            End Select

            mstrEFTCustomColumnsIds = String.Join(",", (From p In dtEFTTable Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("ID").ToString)).ToArray)
            mblnShowColumnHeader = chkShowColumnHeader.Checked
            'Sohail (13 Apr 2016) -- Start
            'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
            mintMembershipUnkId = CInt(cboMembership.SelectedValue)
            'Sohail (13 Apr 2016) -- End
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            mblnSaveAsTXT = chkSaveAsTXT.Checked
            mblnTabDelimiter = chkTABDelimiter.Checked
            'Hemant (27 June 2019) -- End
            'Hemant (02 Jul 2020) -- Start
            'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
            mstrDateFormat = txtDateFormat.Text
            'Hemant (02 Jul 2020) -- End
            Call SetEFTValue()
            Return True
        Catch ex As Exception
            popup_EFTCustom.Show()
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "IsEFTValidate", mstrModuleName)
            DisplayMessage.DisplayError("Procedure : IsEFTValidate : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Sub Export_CSV_XLS()
        Try
            If SetFilter() = False Then Exit Sub
            If IsEFTValidate() = False Then Exit Sub
            GUI.fmtCurrency = Session("fmtCurrency").ToString

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Sohail (09 Jan 2016) -- End

            If mintModeId = enEFT_Export_Mode.CSV Then
                objBankPaymentList._OpenAfterExport = False
                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                'Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\EFT_CUSTOM_CSV.csv"
                Dim mstrPath As String = ""
                Dim strDilimiter As String = "" 'Hemant (27 June 2019) 
                'If objBankPaymentList.EFT_Custom_Columns_CSV_Export_GenerateReport(mstrPath, mstrEFTCustomColumnsIds, mblnShowColumnHeader) = True Then
                '    Session("ExFileName") = mstrPath
                '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                'End If
                Select Case CInt(CType(cboReportType.SelectedItem, ListItem).Value)
                    Case enBankPaymentReport.Electronic_Fund_Transfer
                        'Hemant (27 June 2019) -- Start
                        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                        If chkSaveAsTXT.Checked = True Then
                            mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\EFT_CUSTOM_TXT.txt"
                        Else
                            'Hemant (27 June 2019) -- End
                            mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\EFT_CUSTOM_CSV.csv"
                        End If

                        'Hemant (27 June 2019) -- Start
                        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                        If chkTABDelimiter.Checked = True Then
                            strDilimiter = vbTab
                        Else
                            strDilimiter = ","
                        End If
                        'Hemant (27 June 2019) -- End

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objBankPaymentList.EFT_Custom_Columns_CSV_Export_GenerateReport(mstrPath, mstrEFTCustomColumnsIds, mblnShowColumnHeader) = True Then
                        'Sohail (13 Apr 2016) -- Start
                        'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                        'If objBankPaymentList.EFT_Custom_Columns_CSV_Export_GenerateReport(Session("Database_Name").ToString, _
                        '                                                                   CInt(Session("UserId")), _
                        '                                                                   CInt(Session("Fin_year")), _
                        '                                                                   CInt(Session("CompanyUnkId")), _
                        '                                                                   objPeriod._Start_Date, _
                        '                                                                   objPeriod._End_Date, _
                        '                                                                   Session("UserAccessModeSetting").ToString, True, _
                        '                                                                    CBool(Session("IsIncludeInactiveEmp")), True, _
                        '                                                                   mstrPath, _
                        '                                                                   mstrEFTCustomColumnsIds, _
                        '                                                                   mblnShowColumnHeader, _
                        '                                                                   Session("fmtCurrency").ToString) = True Then
                        If objBankPaymentList.EFT_Custom_Columns_CSV_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                                           CInt(Session("UserId")), _
                                                                                           CInt(Session("Fin_year")), _
                                                                                           CInt(Session("CompanyUnkId")), _
                                                                                           objPeriod._Start_Date, _
                                                                                           objPeriod._End_Date, _
                                                                                           Session("UserAccessModeSetting").ToString, True, _
                                                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                                           mstrPath, _
                                                                                           mstrEFTCustomColumnsIds, _
                                                                                           mblnShowColumnHeader, _
                                                                                           Session("fmtCurrency").ToString, mintMembershipUnkId, _
                                                                                           strDilimiter, _
                                                                                           mstrDateFormat) = True Then
                            'Hemant (02 Jul 2020) -- [mstrDateFormat]
                            'Hemant (27 June 2019) -- [strDilimiter]
                            'Sohail (13 Apr 2016) -- End
                            'Shani(20-Nov-2015) -- End

                            Session("ExFileName") = mstrPath
                            'Gajanan (3 Jan 2019) -- Start
                            'Enhancement :Add Report Export Control 
                            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                            Export.Show()
                            'Gajanan (3 Jan 2019) -- End

                        End If

                    Case enBankPaymentReport.EFT_ADVANCE_SALARY_CSV
                        mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\ADVANCE_SALARY_CUSTOM_CSV.csv"


                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objBankPaymentList.EFT_Advance_Salary_CSV_Export_GenerateReport(mstrPath, mstrEFTCustomColumnsIds, mblnShowColumnHeader) = True Then
                        If objBankPaymentList.EFT_Advance_Salary_CSV_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                                           CInt(Session("UserId")), _
                                                                                           CInt(Session("Fin_year")), _
                                                                                           CInt(Session("CompanyUnkId")), _
                                                                                           objPeriod._Start_Date, _
                                                                                           objPeriod._End_Date, _
                                                                                           Session("UserAccessModeSetting").ToString, True, _
                                                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                                           mstrPath, mstrEFTCustomColumnsIds, _
                                                                                           mblnShowColumnHeader, Session("fmtCurrency").ToString) = True Then
                            'Shani(20-Nov-2015) -- End

                            Session("ExFileName") = mstrPath
                            'Gajanan (3 Jan 2019) -- Start
                            'Enhancement :Add Report Export Control 
                            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                            Export.Show()
                            'Gajanan (3 Jan 2019) -- End

                        End If

                End Select
                'Sohail (25 Mar 2015) -- End
            ElseIf mintModeId = enEFT_Export_Mode.XLS Then
                objBankPaymentList._OpenAfterExport = False
                Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                'If objBankPaymentList.EFT_Custom_Columns_XLS_Export_GenerateReport(mstrPath, mstrEFTCustomColumnsIds, mblnShowColumnHeader) = True Then
                '    If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                '        Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                '        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                '    End If
                'End If
                Select Case CInt(CType(cboReportType.SelectedItem, ListItem).Value)
                    Case enBankPaymentReport.Electronic_Fund_Transfer

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objBankPaymentList.EFT_Custom_Columns_XLS_Export_GenerateReport(mstrPath, mstrEFTCustomColumnsIds, mblnShowColumnHeader) = True Then
                        'Sohail (13 Apr 2016) -- Start
                        'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                        'If objBankPaymentList.EFT_Custom_Columns_XLS_Export_GenerateReport(Session("Database_Name").ToString, _
                        '                                                                   CInt(Session("UserId")), _
                        '                                                                   CInt(Session("Fin_year")), _
                        '                                                                   CInt(Session("CompanyUnkId")), _
                        '                                                                   objPeriod._Start_Date, _
                        '                                                                   objPeriod._End_Date, _
                        '                                                                   Session("UserAccessModeSetting").ToString, True, _
                        '                                                                   CBool(Session("IsIncludeInactiveEmp")), True, _
                        '                                                                   mstrPath, mstrEFTCustomColumnsIds, _
                        '                                                                   mblnShowColumnHeader, _
                        '                                                                   CBool(Session("OpenAfterExport")), _
                        '                                                                   Session("fmtCurrency").ToString) = True Then
                        If objBankPaymentList.EFT_Custom_Columns_XLS_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                                           CInt(Session("UserId")), _
                                                                                           CInt(Session("Fin_year")), _
                                                                                           CInt(Session("CompanyUnkId")), _
                                                                                           objPeriod._Start_Date, _
                                                                                           objPeriod._End_Date, _
                                                                                           Session("UserAccessModeSetting").ToString, True, _
                                                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                                           mstrPath, mstrEFTCustomColumnsIds, _
                                                                                           mblnShowColumnHeader, _
                                                                                           CBool(Session("OpenAfterExport")), _
                                                                                           Session("fmtCurrency").ToString, mintMembershipUnkId, _
                                                                                           mstrDateFormat) = True Then
                            'Hemant (02 Jul 2020) -- [mstrDateFormat]
                            'Sohail (13 Apr 2016) -- End
                            'Shani(20-Nov-2015) -- End

                            If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                                Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                                'Gajanan (3 Jan 2019) -- Start
                                'Enhancement :Add Report Export Control 
                                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                                Export.Show()
                                'Gajanan (3 Jan 2019) -- End

                            End If
                        End If

                    Case enBankPaymentReport.EFT_ADVANCE_SALARY_XLS

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objBankPaymentList.EFT_Advance_Salary_XLS_Export_GenerateReport(mstrPath, mstrEFTCustomColumnsIds, mblnShowColumnHeader) = True Then
                        If objBankPaymentList.EFT_Advance_Salary_XLS_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                                           CInt(Session("UserId")), _
                                                                                           CInt(Session("Fin_year")), _
                                                                                           CInt(Session("CompanyUnkId")), _
                                                                                           objPeriod._Start_Date, _
                                                                                           objPeriod._End_Date, _
                                                                                           Session("UserAccessModeSetting").ToString, True, _
                                                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                                           mstrPath, mstrEFTCustomColumnsIds, _
                                                                                           mblnShowColumnHeader, _
                                                                                           CBool(Session("OpenAfterExport")), _
                                                                                           Session("fmtCurrency").ToString) = True Then
                            'Shani(20-Nov-2015) -- End

                            If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                                Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                                'Gajanan (3 Jan 2019) -- Start
                                'Enhancement :Add Report Export Control 
                                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                                Export.Show()
                                'Gajanan (3 Jan 2019) -- End

                            End If
                        End If

                End Select
                'Sohail (25 Mar 2015) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Export_CSV_EXLS :" & ex.Message, Me)
        End Try
    End Sub
    'SHANI [13 Mar 2015]--END 

    'Hemant (26 May 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
    Private Function GenarateJSONGenericEFT(ByVal strBatchId As String _
                                       , ByVal intBatchCount As Integer _
                                       , ByVal strOrganizationCode As String _
                                       , ByVal strbatchTimestamp As String _
                                       , ByVal drBatchData() As DataRow _
                                       , ByVal strDebitAccountNumber As String _
                                       , ByVal strDebitBankName As String _
                                       , ByVal strDebitBankCode As String _
                                       , ByVal strDebitBankBranchSortCode As String _
                                       , ByVal strDebitCurrency As String _
                                       , ByVal strbatchSignature As String _
                                       , ByVal strEFTCustomCSVColumnsIds As String _
                                       , ByVal strFmtCurrency As String _
                                       , ByVal intPeriodId As Integer _
                                       , Optional ByVal strDateFormat As String = "ddMMyyyy" _
                                       ) As String
        'Hemant (07 Jul 2023) -- Start
        'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
        Dim objCompany As New clsCompany_Master
        'Hemant (07 July 2023) -- End
        Dim strJSON As String = String.Empty

        Try

            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            Dim strPeriodName As String = String.Empty
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = intPeriodId
            strPeriodName = objPeriod._Period_Name
            Dim dtPeriodStartDate As Date = objPeriod._Start_Date
            objPeriod = Nothing
            Dim intProcessCount As Integer = strBatchId.ToString.Split("-1").Length
            Dim strProcessCode As String
            If intProcessCount.ToString.Length < 2 Then
                strProcessCode = "0" & intProcessCount.ToString
            Else
                strProcessCode = intProcessCount.ToString
            End If
            'Hemant (21 Jul 2023) -- End
            'Hemant (07 Jul 2023) -- Start
            'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
            objCompany._Companyunkid = CInt(Session("CompanyUnkId"))
            'Dim decTotalAmount As Decimal = (From p In drBatchData.CopyToDataTable Select (CDec(p.Item("Amount")))).Sum()
            Dim decTotalAmount As Decimal = 0
            'Hemant (07 July 2023) -- End
            Dim arrList As New ArrayList(strEFTCustomCSVColumnsIds.Split(CChar(",")))

            strJSON = "{"
            strJSON &= """batchId"":""" & strBatchId & """"
            strJSON &= ",""batchCount"":""" & intBatchCount & """"
            strJSON &= ",""organizationCode"":""" & strOrganizationCode & """"
            strJSON &= ",""batchTimestamp"":""" & strbatchTimestamp & """"
            'Hemant (07 Jul 2023) -- Start
            'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
            'strJSON &= ",""batchAmount"":""" & Format(decTotalAmount, "#########################0.#0") & """"
            strJSON &= ",""batchNarration"":""" & strPeriodName & " " & Language.getMessage(mstrModuleName, 59, "Salary payment") & """"
            'Hemant (07 July 2023) -- End
            Dim ds As DataSet = (New clsMasterData).GetComboListForEFTCustomColumns("List", False)
            Dim dic As New Dictionary(Of Integer, String)
            If ds.Tables(0).Rows.Count > 0 Then
                dic = (From p In ds.Tables(0).AsEnumerable() Select New With {Key .id = CInt(p.Item("id")), .Name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(y) y.Name)
            End If

            strJSON &= ",""batchData"": ["
            Dim intCount As Integer = 1
            Dim strAllEmployeesJSON As String = String.Empty
            For Each drEmployeeBatch In drBatchData
                Dim stCurrentEmployeeJSON As String = String.Empty
                Dim stCurrentEmployeeData As String = String.Empty
                stCurrentEmployeeJSON &= "{"
                For Each id As String In arrList
                    Select Case CInt(id)
                        Case enEFT_EFT_Custom_Columns.COMP_BANK_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_BGCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_NAME
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_BankName").ToString & """"

                        Case enEFT_EFT_Custom_Columns.COMP_BRANCH_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_BRCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.COMP_BRANCH_NAME
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_BranchName").ToString & """"

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_SORT_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_SortCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_SWIFT_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_SwiftCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_ACCOUNT_NO
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_Account").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_BGCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_NAME
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_BankName").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BRANCH_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_BRCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BRANCH_NAME
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_BranchName").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_SORT_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_SortCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_SWIFT_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_SwiftCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_ACCOUNT_NO
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_AccountNo").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMPLOYEE_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EmpCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMPLOYEE_NAME
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EmpName").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_AMOUNT
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & Format(drEmployeeBatch.Item("Amount"), strFmtCurrency).Replace(",", "") & """"

                        Case enEFT_EFT_Custom_Columns.DESCRIPTION
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & strPeriodName & " " & Language.getMessage(mstrModuleName4, 2, "Salary") & """"

                        Case enEFT_EFT_Custom_Columns.EMP_MEMBERSHIP_NO
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("Membershipno").ToString & """"

                        Case enEFT_EFT_Custom_Columns.PAYMENT_DATE
                            Dim dtDate As DateTime
                            If DateTime.TryParseExact(drEmployeeBatch.Item("paymentdate").ToString, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, dtDate) Then
                                stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & dtDate.ToString(strDateFormat) & """"
                            End If

                        Case enEFT_EFT_Custom_Columns.PAYMENT_CURRENCY
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("currency_sign").ToString & """"

                    End Select
                Next
                If stCurrentEmployeeData.Trim.Length > 0 Then
                    stCurrentEmployeeData = stCurrentEmployeeData.Substring(1)
                End If

                Dim strCount As String = String.Empty
                If intCount.ToString.Trim.Length = 1 Then
                    strCount = "00000" & intCount.ToString
                ElseIf intCount.ToString.Trim.Length = 2 Then
                    strCount = "0000" & intCount.ToString
                ElseIf intCount.ToString.Trim.Length = 3 Then
                    strCount = "000" & intCount.ToString
                ElseIf intCount.ToString.Trim.Length = 4 Then
                    strCount = "00" & intCount.ToString
                ElseIf intCount.ToString.Trim.Length = 5 Then
                    strCount = "0" & intCount.ToString
                Else
                    strCount = intCount.ToString
                End If

                'Hemant (07 Jul 2023) -- Start
                'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
                'Hemant (21 Jul 2023) -- Start
                'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
                If drEmployeeBatch.Item("EMP_SwiftCode").ToString.Trim.ToUpper.Contains("NLCBTZ") = True Then
                    stCurrentEmployeeData &= ",""transactionType"":""INTERNAL"""
                Else
                    'Hemant (21 Jul 2023) -- End
                    stCurrentEmployeeData &= ",""transactionType"":""EFT"""
                End If 'Hemant (21 Jul 2023)
                stCurrentEmployeeData &= ",""chargeType"":""O"""
                stCurrentEmployeeData &= ",""postalAddress"":""" & objCompany._Address1 & """"
                stCurrentEmployeeData &= ",""physicalAddress"":""" & objCompany._Address2 & """"
                stCurrentEmployeeData &= ",""clientReference"":""" & dtPeriodStartDate.ToString("yyMM") & drEmployeeBatch.Item("voucherno").ToString & strProcessCode & drEmployeeBatch.Item("EmpCode").ToString & strCount.ToString & """"
                stCurrentEmployeeData &= ",""email"":""" & objCompany._Email & """"
                stCurrentEmployeeData &= ",""phoneNumber"":""" & objCompany._Phone1 & """"
                'Hemant (07 July 2023) -- End

                stCurrentEmployeeJSON &= stCurrentEmployeeData

                stCurrentEmployeeJSON &= "}"

                If strAllEmployeesJSON.Trim.Length > 0 Then
                    strAllEmployeesJSON = "," & stCurrentEmployeeJSON
                Else
                    strAllEmployeesJSON = stCurrentEmployeeJSON
                End If

                strJSON &= strAllEmployeesJSON

                'Hemant (21 Jul 2023) -- Start
                'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
                drEmployeeBatch.Item("clientreference") = dtPeriodStartDate.ToString("yyMM") & drEmployeeBatch.Item("voucherno").ToString & strProcessCode & drEmployeeBatch.Item("EmpCode").ToString & strCount.ToString
                drEmployeeBatch.Item("status") = ""
                'Hemant (21 Jul 2023) -- End

                'Hemant (07 Jul 2023) -- Start
                'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
                intCount = intCount + 1
                'Hemant (07 July 2023) -- End
                decTotalAmount = decTotalAmount + Format(drEmployeeBatch.Item("Amount"), strFmtCurrency)
            Next
            strJSON &= "]"
            strJSON &= ",""companyBankDetails"" : {"
            strJSON &= """debitAccount"":""" & strDebitAccountNumber & """"
            strJSON &= ",""bankName"":""" & strDebitBankName & """"
            strJSON &= ",""bankCode"":""" & strDebitBankCode & """"
            strJSON &= ",""bankBranchSortCode"":""" & strDebitBankBranchSortCode & """"
            strJSON &= ",""debitCurrency"":""" & strDebitCurrency & """"
            strJSON &= ",""debitAccountName"":""TRA Salary Account"""
            strJSON &= "}"
            strJSON &= ",""batchAmount"":""" & Format(decTotalAmount, "#########################0.#0") & """"
            strJSON &= ",""batchSignature"":""" & strbatchSignature & """"
            strJSON &= "}"

            Return strJSON
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (07 Jul 2023) -- Start
            'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
        Finally
            objCompany = Nothing
            'Hemant (07 July 2023) -- End
        End Try
    End Function

    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    'Private Function GenarateJSONReconciliation(ByVal drRow As DataRow, _
    '                                                ByVal intPaymentBatchPostingId As Integer _
    '                                                ) As String
    '    Dim strJSON As String = String.Empty
    '    Try
    '        strJSON = "{"
    '        If intPaymentBatchPostingId < 0 Then
    '            strJSON &= """batchId"":""" & drRow.Item("voucherno") & """"
    '            strJSON &= ",""employeeCode"":""" & drRow.Item("empcode") & """"
    '            strJSON &= ",""accountNumber"":""" & drRow.Item("emp_accountno") & """"
    '        Else
    '            strJSON &= """batchId"":""" & drRow.Item("batchname") & """"
    '            strJSON &= ",""employeeCode"":""" & drRow.Item("employeecode") & """"
    '            strJSON &= ",""accountNumber"":""" & drRow.Item("accountno") & """"
    '        End If
    '        strJSON &= ",""timestamp"":""" & CDate(ConfigParameter._Object._CurrentDateAndTime).ToString("yyyy-MM-dd hh:mm:ss") & """"
    '        strJSON &= "}"
    '        Return strJSON
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GenarateJSONReconciliation", mstrModuleName)
    '    End Try
    'End Function
    Private Function GenarateJSONReconciliation(ByVal strBatchId As String _
                                            , ByVal intBatchCount As Integer _
                                            , ByVal strOrganizationCode As String _
                                            , ByVal strbatchTimestamp As String _
                                            , ByVal strDebitAccountNumber As String _
                                            , ByVal strDebitCurrency As String _
                                            , ByVal decTotalAmount As Decimal _
                                            ) As String
        Dim strJSON As String = String.Empty
        Try
            strJSON = "{"
            strJSON &= """batchId"":""" & strBatchId & """"
            strJSON &= ",""organizationCodeName"":""" & strOrganizationCode & """"
            strJSON &= ",""batchTimestamp"":""" & strbatchTimestamp & """"
            strJSON &= ",""debitAccount"":""" & strDebitAccountNumber & """"
            strJSON &= ",""debitCurrency"":""" & strDebitCurrency & """"
            strJSON &= ",""debitAccountName"":""TRA Salary Account"""
            strJSON &= ",""totalAmount"":""" & Format(decTotalAmount, "#########################0.#0") & """"
            strJSON &= ",""batchCount"":""" & intBatchCount & """"
            strJSON &= ",""accountingType"":""SDMC"""
            strJSON &= ",""batchNarration"":""TRA batch"""
            strJSON &= "}"
            Return strJSON
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function
    'Hemant (21 Jul 2023) -- End

    Public Function SignData(ByVal StringToSign As String) As String
        Try

            Dim rsa As RSACryptoServiceProvider = New RSACryptoServiceProvider()
            rsa.FromXmlString("<RSAKeyValue><Modulus>7W+lAXsQXX4bIITjBLeGGzKIaOIWKLyMItGNYs3ozTvemyBigsTRKJToBhwsx5p+fDsHOGmFPyL1z8P2k3lPUM6Yg28qKl1h9WxOz1W8t4/WMBTCEu9h04ldspJSgMOynovjeaerj7bpge/ffpS8/LRA0j0ZvRaMs7DkfvzCsGby7aaeXm3uglvGhA2p0Gl/TNxfFNo+9eWnjN3Dg/7qXNwYjlYbFVMA/SZ5xJDY+F5YzjVDpKq6XVpB+9H5iD5lv04VLveWk+EkJct8oq+WmgNEqZzIWzmadgv++B0VtxiPnkpzeqRmWoZAtAT1IeevyX0y/KKXKKJYam7CFwETCQ==</Modulus><Exponent>AQAB</Exponent><P>+ZpHDGtbuc03Nl5Dop96bi8M6VMiR4n7wye97cTn76tQ6iNbioUnlmmdUXllognllUmk6h+sD92wuQbMpbkgh5WZmodcrY/N2acafKBrrAS5JwgN2WioxoHyIqJa9Z9B7GfxTLNTXfoz7CEoxDUJD1QHsoWFDjDUqzyMFxdZ7e0=</P><Q>84WJ27U7gNLTpFFTLwMKidHuroWx7PkyWVwyx2qxki1mmGYHf+13jlSCdT7Jz7d8h3XAykrDsHElTus6jRV2PSFfs4g4u1rsrqmgHYa2jrAGvX8U9LfnjoYs9v6alu74Z90HCLTBdJ5Hea8yumEMMiKkDxp/prpiItBplMRANg0=</Q><DP>TLJ1ZoGOu/ctIg2xJsVub3ERvJiJDgZ+UCdkGy3IP0MbJ/cZZ+Umlvd5GdH9wt7bpxXsEO0OiAmNBi3qsHnEXyU+/9bcSZDIpjrMzsLUkxUYd7/n0YhxZB4F81KENLltHmGKKhFoapY5YjOGPVQ2pnkhrF+O1R94Ge4O9gF85rk=</DP><DQ>me8Y1LQ8F9OtCxqJPZdrivEUMme6r/RaGliIlLvh4WgniUA9j2U5hNPw31JAWbg/1JTfuEAIcTkkfz18doBRjJTTHPaH/g6cvE/nMaLdNVcZ+6EgSw0RJ2uzcrJAYBZRGb6C2sL/4srGnancpCoCfpKdKBr1BByfOiiKBQsFF+U=</DQ><InverseQ>ywk/7sgD8R/8QWqnUF2fmXLgEUnSWh2Y6bKFbiDffnH1+AZdp3x8s4tocyu+RXFUt+IXHWKP5sxTNyjYSJ0YGxGeUSwjhn87t0G6L2/KGOe6ihtfXEOgvlngVbUVTCtdJYrXD50+/pvBxOoWB9Yxsve1NYJfhr5V3YLZJg97jL4=</InverseQ><D>aV6Of6W5kYQRTdErXkCDxzYZy1HqO5HRLvKIKDzw/4N+OqGYlif6GmRaw7tlM/+f+knH3oUVmPtO0zFIEBJZ3KaSkGGY+MwQWPYD04ddBKlUiGnt5rFNXK8tYb4F1xcCAdJa1PZP8Ktf3UYyjN49MHhd++8ZqQyEzInIHYLWc6mhdcNN6QuLMPhwcA19yoAMeuP6Bu03XcmObhulrFaEl4AVA4VK8KmZKG80rl3bjE9L59a83T6nEiIkD4xpvKJjmGrnI0pSsSptJZUkrWgCTDAmTaaMIdRU93+WF6N3/ycAd5c1uU72Z7qhgT8HWO2+AArqoAuBB3jeIrVF5H69wQ==</D></RSAKeyValue>")
            Dim originalString As String = StringToSign
            Dim signedData As Byte() = rsa.SignData(System.Text.Encoding.UTF8.GetBytes(originalString), CryptoConfig.MapNameToOID("SHA256"))
            Return Convert.ToBase64String(signedData)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub FillReconciliationCombo()
        Dim objCountry As New clsMasterData
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Dim objPeriod As New clscommom_period_Tran
        Dim objBank As New clspayrollgroup_master
        Dim dsCombo As New DataSet
        Try
            Dim intFirstOpenPeriod As Integer = objCountry.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString, CDate(Session("fin_startdate")), "List", True)

            With cboRPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = intFirstOpenPeriod.ToString
                Call cboRPeriod_SelectedIndexChanged(cboRPeriod, New System.EventArgs)
            End With


            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            'dsCombo = objPaymentBatchPostingMaster.getStatusComboList("List", True)
            'With cboRStatus
            '    .DataValueField = "Id"
            '    .DataTextField = "Name"
            '    .DataSource = dsCombo.Tables(0)
            '    .DataBind()
            '    .SelectedValue = "-1"
            '    .SelectedIndex = 0
            'End With
            FillStatusCombo(mdtStatus)
            'Hemant (21 Jul 2023) -- End

            

            dsCombo = objBank.getListForCombo(enPayrollGroupType.Bank, "Banks", True)
            With cboBankGroup
                .DataValueField = "groupmasterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Banks")
                .DataBind()
                .SelectedValue = 0
                Call cboBankGroup_SelectedIndexChanged(cboBankGroup, New System.EventArgs)
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCountry = Nothing
            objPeriod = Nothing
            objPaymentBatchPostingMaster = Nothing
            objBank = Nothing
        End Try
    End Sub

    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    Private Sub FillStatusCombo(ByVal dtTable As DataTable)
        Try
            If Not dtTable.Columns.Contains("status") Then
                Dim dCol As DataColumn

                dCol = New DataColumn("status")
                dCol.Caption = "status"
                dCol.DataType = System.Type.GetType("System.String")
                dtTable.Columns.Add(dCol)
            End If

            With cboRStatus
                .Items.Clear()
                .Items.Add("Select")
                For Each dr As DataRow In dtTable.Rows
                    .Items.Add(dr.Item("status").ToString)
                Next
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
    'Hemant (21 Jul 2023) -- End

    Private Sub FillBatchApprovalCombo()
        Dim objCountry As New clsMasterData
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Dim objPeriod As New clscommom_period_Tran
        Dim objBank As New clspayrollgroup_master
        Dim dsCombo As New DataSet
        Try
            Dim intFirstOpenPeriod As Integer = objCountry.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString, CDate(Session("fin_startdate")), "List", True)

            With cboAPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = intFirstOpenPeriod.ToString
            End With


            dsCombo = objPaymentBatchPostingMaster.getApprovalStatusComboList("List", True)
            With cboAStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedIndex = 0
            End With
            Call cboAPeriod_SelectedIndexChanged(cboAStatus, New System.EventArgs)

            dsCombo = objBank.getListForCombo(enPayrollGroupType.Bank, "Banks", True)
            With cboBankGroup
                .DataValueField = "groupmasterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Banks")
                .DataBind()
                .SelectedValue = 0
                Call cboBankGroup_SelectedIndexChanged(cboBankGroup, New System.EventArgs)
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCountry = Nothing
            objPeriod = Nothing
            objPaymentBatchPostingMaster = Nothing
            objBank = Nothing
        End Try
    End Sub

    Private Sub FillReconciliationList()
        Dim objPaymentBatchReconciliationTran As New clspaymentbatchreconciliation_tran
        Dim drRow() As DataRow
        Dim strFilter As String = String.Empty
        Try
            If CInt(cboRBatch.SelectedValue) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName3, 15, "Please select Batch."), Me)
                cboRBatch.Focus()
                Exit Sub
            End If
            Dim dtTable As DataTable

            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            'If CInt(cboRStatus.SelectedValue) <= 0 Then
            '    dtTable = mdtReconciliationData.Copy()
            'Else
            '    drRow = mdtReconciliationData.Select("status = '" & cboRStatus.SelectedItem.Text & "'")
            '    If drRow.Length > 0 Then
            '        dtTable = drRow.CopyToDataTable()

            '    End If
            'End If
            strFilter = "1 = 1 "
            If CInt(cboRPeriod.SelectedValue) > 0 Then
                strFilter &= "AND periodunkID = " & CInt(cboRPeriod.SelectedValue) & " "
            End If

            'Hemant (04 Aug 2023) -- Start
            'If CInt(cboRBatch.SelectedValue) <> 0 Then
            '    strFilter &= "AND batchname = '" & CStr(cboRBatch.SelectedItem.Text) & "' "
            'End If
            'Hemant (04 Aug 2023) -- End

            If CInt(cboRStatus.SelectedIndex) > 0 Then
                strFilter &= "AND status = '" & CStr(cboRStatus.SelectedItem.Text) & "' "
            End If
            'drRow = mdtReconciliationData.Select(strFilter)
            'If drRow.Length > 0 Then
            '    dtTable = drRow.CopyToDataTable()
            'End If
            'Hemant (21 Jul 2023) -- End
            Dim dsList As DataSet = objPaymentBatchReconciliationTran.GetList("List", IIf(CInt(cboRBatch.SelectedValue) <> 0, cboRBatch.SelectedItem.Text, ""))
            If dsList.Tables(0).Columns.Contains("ischeck") = False Then
                Dim dCol As New DataColumn
                With dCol
                    .ColumnName = "ischeck"
                    .DataType = GetType(System.Boolean)
                    .DefaultValue = False
                End With
                dsList.Tables(0).Columns.Add(dCol)
            End If

            mdtStatus = dsList.Tables(0).DefaultView.ToTable(True, "status")
            If mstrOldBatchName <> cboRBatch.SelectedItem.Text Then
                FillStatusCombo(mdtStatus)
            End If
            drRow = dsList.Tables(0).Select(strFilter)
            If drRow.Length > 0 Then
                dtTable = drRow.CopyToDataTable()
            End If
            Call SetGridDataSource(dtTable)
            mstrOldBatchName = cboRBatch.SelectedItem.Text

            If cboRStatus.SelectedItem.Text.ToUpper = "FAILED" Then
                btnSubmitForApproval.Visible = True
            Else
                btnSubmitForApproval.Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Aug 2023) -- Start
        Finally
            objPaymentBatchReconciliationTran = Nothing
            'Hemant (04 Aug 2023) -- End
        End Try
    End Sub

    Private Sub FillBatchApprovalList()
        Dim objPaymentBatchPostingTran As New clspaymentbatchposting_tran
        Dim dsList As DataSet
        'Dim drRow() As DataRow
        Try

            'If CInt(cboStatus.SelectedValue) <= 0 Then
            '    dtTable = mdsBatchData.Tables(0)
            'Else
            '    drRow = mdsBatchData.Tables(0).Select("status = '" & cboStatus.Text & "'")
            '    If drRow.Length > 0 Then
            '        dtTable = drRow.CopyToDataTable()

            '    End If
            'End If

            'Call SetGridDataSource(dtTable)
            dsList = objPaymentBatchPostingTran.GetList("List", CInt(cboABatch.SelectedValue))

            dgvBatchApprovalList.DataSource = dsList.Tables(0)
            dgvBatchApprovalList.DataBind()
            If CInt(cboAStatus.SelectedValue) = clspaymentbatchposting_master.enApprovalStatus.SubmitForApproval Then
                btnApproveReject.Visible = CBool(Session("AllowToApproveRejectPaymentBatchPosting"))
            Else
                btnApproveReject.Visible = False
            End If

            If CInt(cboAStatus.SelectedValue) = clspaymentbatchposting_master.enApprovalStatus.Approved Then
                btnSendRequest.Enabled = True
            Else
                btnSendRequest.Enabled = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            objPaymentBatchPostingTran = Nothing
        End Try
    End Sub

    Private Sub SetGridDataSource(ByVal dtTable As DataTable)
        Try
            'dgvDataList.AutoGenerateColumns = False
            'objdgcolhSelect.DataPropertyName = "IsCheck"
            'dgcolhBatchId.DataPropertyName = "batchId"
            'dgcolhEmployeeCode.DataPropertyName = "employeeCode"
            'dgcolhAccountNo.DataPropertyName = "accountnumber"


            'Dim objBranch As New clsbankbranch_master
            'Dim dsBranch As DataSet = objBranch.getListForCombo("Branch", True)
            'With dgcolhBranch
            '    .ValueMember = "branchunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsBranch.Tables("Branch")
            '    .DataPropertyName = "branchunkid"
            '    .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            'End With
            'objdgcolhBranchUnkid.DataPropertyName = "branchunkid"
            'objBranch = Nothing

            dgvDataList.DataSource = dtTable
            dgvDataList.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValueReconciliation(ByVal objPaymentBatchPostingMaster As clspaymentbatchposting_master)
        Try
            If mblnIsFromGrid = True Then
                objPaymentBatchPostingMaster._BatchName = mstrVoucherNo & "-1"
            Else
                objPaymentBatchPostingMaster._BatchName = cboRBatch.SelectedItem.Text & "-1"
            End If
            objPaymentBatchPostingMaster._Periodunkid = CInt(cboRPeriod.SelectedValue)
            objPaymentBatchPostingMaster._VoucherNo = mstrVoucherNo
            objPaymentBatchPostingMaster._Statusunkid = clspaymentbatchposting_master.enApprovalStatus.SubmitForApproval
            objPaymentBatchPostingMaster._Userunkid = Session("UserId")
            objPaymentBatchPostingMaster._Refno = objPaymentBatchPostingMaster.getNextRefNo(mstrVoucherNo).ToString
            objPaymentBatchPostingMaster._Isvoid = False
            objPaymentBatchPostingMaster._Voiduserunkid = -1
            objPaymentBatchPostingMaster._Voiddatetime = Nothing
            objPaymentBatchPostingMaster._Voidreason = ""
            objPaymentBatchPostingMaster._ClientIP = CStr(Session("IP_ADD"))
            objPaymentBatchPostingMaster._HostName = CStr(Session("HOST_NAME"))
            objPaymentBatchPostingMaster._FormName = mstrModuleName
            objPaymentBatchPostingMaster._AuditUserId = Session("UserId")
            objPaymentBatchPostingMaster._Isweb = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (26 May 2023) -- End
    Private Sub ReconciliationProcess()
        Dim objPaymentBatchReconciliationTran As New clspaymentbatchreconciliation_tran
        Dim strError As String = ""
        Dim strPostedData As String = ""
        Try
            Dim dsRenciliationData As DataSet = objPaymentBatchReconciliationTran.GetList("List", cboRBatch.SelectedItem.Text)

            Dim decTotalBatchAmount As Decimal = (From p In dsRenciliationData.Tables(0) Select (CDec(p.Item("Amount")))).Sum()
            Dim strJSON As String = GenarateJSONReconciliation(cboRBatch.SelectedItem.Text, dsRenciliationData.Tables(0).Rows.Count, Session("CompanyCode").ToString(), CDate(dsRenciliationData.Tables(0).Rows(0).Item("batchposted_date")).ToString("yyyyMMdd"), cboCompanyAccountNo.SelectedItem.Text, cboCurrency.SelectedItem.Text, decTotalBatchAmount)
            Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(Session("EFTRequestClientID") & ":" & Session("EFTRequestPassword"))
            Dim strBase64Credential As String = Convert.ToBase64String(byt)

            Dim strMessage = objBankPaymentList.GetSetPaymentAPIRequest(Session("EFTReconciliationURL").ToString(), strBase64Credential, strJSON, strError, strPostedData, True, SignData(strJSON))

            If strMessage IsNot Nothing Then
                eZeeMsgBox.Show(" Request Response : " & strMessage("statusCode").ToString() & " - " & strMessage("message").ToString(), enMsgBoxStyle.Information)
            End If

            'Dim strMessage = "{""statusCode"":4000,""message"":""Success"",""payload"":{""batchId"":""PV13496"",""accountNumber"":""0321101809097"",""amount"":""306500.00"",""status"":""FAILED"",""reference"":null,""timestamp"":""2023-06-06 10:21:46""}}"
            'Dim strMessage = "{""statusCode"":4000,""message"":""Success"",""payload"":[{""clientReference"":""2106611011000584000001"",""status"":""failed""},{""clientReference"":""210661103000000000001"",""status"":""success""},{""clientReference"":""2106611011000025000003"",""status"":""failed""},{""clientReference"":""2106611021000584000002"",""status"":""success""},{""clientReference"":""210661102000000000003"",""status"":""failed""}]}"
            'Dim strMessage = "{""statusCode"":4000,""message"":""Success"",""payload"":[{""clientReference"": ""2106618011000584000001"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""P"",""bankRef"": null,""paymentStatusDesc"":""Pending"",""resv1"":null,""resv2"":null}, " & _
            '                                                                          "{""clientReference"": ""2106618011000009000002"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""S"",""bankRef"": null,""paymentStatusDesc"":""Success"",""resv1"":null,""resv2"":null}, " & _
            '                                                                          "{""clientReference"": ""2106618011000025000003"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""U"",""bankRef"": null,""paymentStatusDesc"":""Unknown"",""resv1"":null,""resv2"":null}," & _
            '                                                                          "{""clientReference"": ""2106618011000584000004"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""S"",""bankRef"": null,""paymentStatusDesc"":""Submitted"",""resv1"":null,""resv2"":null}, " & _
            '                                                                           "{""clientReference"": ""210661801000000000005"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""F"",""bankRef"": null,""paymentStatusDesc"":""Failed"",""resv1"":null,""resv2"":null}]}"
            'Dim ResultJSON = JsonConvert.DeserializeObject(strMessage)
            'Dim objObject As Object = ResultJSON
            'Dim strPayload = objObject("payload").ToString
            Dim strPayload = strMessage("payload").ToString
            Dim PayloadJSON = JsonConvert.DeserializeObject(strPayload.Trim)
            Dim dtResponse As DataTable = JsonConvert.DeserializeObject(Of DataTable)(PayloadJSON.ToString())

            Dim intCount As Integer = 0
            Dim intTotalCount As Integer = dsRenciliationData.Tables(0).Rows.Count
            For Each drEmployeeData As DataRow In dsRenciliationData.Tables(0).Rows
                Dim drResponse() As DataRow = dtResponse.Select("clientReference = '" & drEmployeeData.Item("clientReference") & "'")
                If drResponse.Length > 0 Then
                    drEmployeeData.Item("status") = drResponse(0).Item("PaymentStatusDesc").ToString
                    drEmployeeData.Item("reconciliation_date") = CDate(ConfigParameter._Object._CurrentDateAndTime)
                    dsRenciliationData.Tables(0).AcceptChanges()
                End If
                intCount = intCount + 1
            Next

            objPaymentBatchReconciliationTran._TranDataTable = dsRenciliationData.Tables(0)
            If objPaymentBatchReconciliationTran.UpdateAll() = False And objPaymentBatchReconciliationTran._Message <> "" Then
                DisplayMessage.DisplayMessage(objPaymentBatchReconciliationTran._Message, Me)
            End If

            btnReconciliation.Enabled = False
            Call FillReconciliationList()
        Catch ex As Exception

            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPaymentBatchReconciliationTran = Nothing

        End Try
    End Sub
#End Region

#Region "Combobox Event"

    Protected Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            chkEmployeeSign.Checked = False
            chkShowGroupByBankBranch.Checked = False
            chkSignatory1.Checked = False
            chkSignatory2.Checked = False
            chkSignatory3.Checked = False
            cboCountry.SelectedIndex = 0
            'cboCurrency.SelectedIndex = 0 'Sohail (01 Apr 2014)
            cboReportMode.Enabled = True
            chkShowFNameSeparately.Checked = False
            'Sohail (24 Dec 2013) -- Start
            'Enhancement - Oman
            chkShowPayrollPeriod.Checked = True
            chkShowSortCode.Checked = True
            'Sohail (24 Dec 2013) -- End
            cboCountry.Enabled = False
            cboCurrency.Enabled = True
            chkEmployeeSign.Enabled = False
            chkShowGroupByBankBranch.Enabled = False
            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            'Anjan [04 June 2014 ] -- End
            chkShowGroupByBankBranch.Text = Language.getMessage(mstrModuleName, 15, "Show Group By Bank / Branch")
            chkSignatory1.Enabled = False
            chkSignatory2.Enabled = False
            chkSignatory3.Enabled = False
            'Anjan [03 Feb 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            chkDefinedSignatory.Enabled = False
            chkDefinedSignatory2.Enabled = False
            chkDefinedSignatory3.Enabled = False

            chkShowBankCode.Checked = False
            chkShowBranchCode.Checked = False


            chkShowBankCode.Enabled = False
            chkShowBranchCode.Enabled = False

            'Sohail (01 Apr 2014) -- Start
            'ENHANCEMENT - 
            chkShowReportHeader.Visible = False
            chkShowReportHeader.Checked = False
            chkShowAccountType.Visible = False
            chkShowAccountType.Checked = False
            'Sohail (01 Apr 2014) -- End
            'Hemant (19 June 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            chkShowSelectedBankInfo.Visible = False
            'Hemant (19 June 2019) -- End
            'Hemant 20 Jul 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            chkShowCompanyGrpInfo.Visible = False
            'Hemant 20 Jul 2019) -- End

            'Sohail (18 Nov 2016) -- Start
            lblPostingDate.Enabled = False
            dtpPostingDate.Enabled = False
            'Sohail (18 Nov 2016) -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            pnlOtherSetting.Visible = False
            'Sohail (12 Feb 2018) -- End
            'Hemant (16 Jul 2020) -- Start
            'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
            chkSaveAsTXT_WPS.Visible = False
            'Hemant (16 Jul 2020) -- End

            'Sohail (08 Jul 2021) -- Start
            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
            lblMembershipRepo.Visible = False
            cboMembershipRepo.Visible = False
            cboMembershipRepo.SelectedValue = "0"
            'Sohail (08 Jul 2021) -- End

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            If CInt(cboReportType.SelectedIndex) >= 0 Then
                cboCondition.SelectedIndex = 0
            End If
            'Nilay (10-Nov-2016) -- End

            Select Case CInt(CType(cboReportType.SelectedItem, ListItem).Value)

                Case enBankPaymentReport.Bank_payment_List
                    cboCountry.Enabled = True
                    chkEmployeeSign.Enabled = True
                    chkShowGroupByBankBranch.Enabled = True
                    chkSignatory1.Enabled = True
                    chkSignatory2.Enabled = True
                    chkSignatory3.Enabled = True
                    'Anjan [03 Feb 2014] -- Start
                    'ENHANCEMENT : Requested by Rutta
                    chkDefinedSignatory.Enabled = True
                    chkDefinedSignatory2.Enabled = True
                    chkDefinedSignatory3.Enabled = True

                    chkShowBankCode.Checked = True
                    chkShowBranchCode.Checked = True


                    chkShowBankCode.Enabled = True
                    chkShowBranchCode.Enabled = True
                    'Anjan [03 Feb 2014 ] -- End

                    'Sohail (01 Apr 2014) -- Start
                    'ENHANCEMENT - 
                    chkShowReportHeader.Visible = True
                    chkShowReportHeader.Checked = True
                    chkShowAccountType.Visible = True
                    chkShowAccountType.Checked = False
                    chkShowSortCode.Enabled = True
                    'Sohail (01 Apr 2014) -- End

                Case enBankPaymentReport.Electronic_Fund_Transfer
                    chkShowGroupByBankBranch.Enabled = True
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    Language.setLanguage(mstrModuleName)
                    'Anjan [04 June 2014 ] -- End
                    chkShowGroupByBankBranch.Text = Language.getMessage(mstrModuleName, 16, "Show Group By Bank")
                    'Sohail (01 Apr 2014) -- Start
                    'ENHANCEMENT - 
                    chkShowReportHeader.Visible = True
                    chkShowReportHeader.Checked = True
                    'Sohail (01 Apr 2014) -- End
                    'Hemant (19 June 2019) -- Start
                    'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                    chkShowSelectedBankInfo.Visible = True
                    'Hemant (19 June 2019) -- End
                    'Hemant 20 Jul 2019) -- Start
                    'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                    chkShowCompanyGrpInfo.Visible = True
                    'Hemant 20 Jul 2019) -- End
                    'Sohail (12 Feb 2018) -- Start
                    'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                Case enBankPaymentReport.Salary_Format_WPS
                    pnlOtherSetting.Visible = True
                    'Hemant (16 Jul 2020) -- Start
                    'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
                    chkSaveAsTXT_WPS.Visible = True
                    'Hemant (16 Jul 2020) -- End
                    'Sohail (12 Feb 2018) -- End

            End Select

            If CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.Electronic_Fund_Transfer Then

                chkShowFNameSeparately.Enabled = True
                'Sohail (24 Dec 2013) -- Start
                'Enhancement - Oman
                chkShowPayrollPeriod.Enabled = True
                chkShowSortCode.Enabled = True
                'Sohail (24 Dec 2013) -- End

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                chkShowEmployeeCode.Visible = True
                'Pinkal (12-Jun-2014) -- End

                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_CityDirect Then
                    lnkEFTCityBankExport.Visible = True
                Else
                    lnkEFTCityBankExport.Visible = False
                End If

                'Sohail (08 Dec 2014) -- Start
                'Enhancement - New Mobile Money EFT integration with MPESA.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_CBA Then
                    'lnkEFT_CBA_Export.Location = lnkEFTCityBankExport.Location
                    'lnkEFT_CBA_Export.Size = lnkEFTCityBankExport.Size
                    lnkEFT_CBA_Export.Visible = True
                Else
                    lnkEFT_CBA_Export.Visible = False
                End If

                'Sohail (24 Dec 2014) -- Start
                'AMI Enhancement - New EFT Report EFT EXIM.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_EXIM Then
                    'lnkEFT_EXIM_Export.Location = lnkEFTCityBankExport.Location
                    'lnkEFT_EXIM_Export.Size = lnkEFTCityBankExport.Size
                    lnkEFT_EXIM_Export.Visible = True
                Else
                    lnkEFT_EXIM_Export.Visible = False
                End If
                'Sohail (24 Dec 2014) -- End

                If CInt(Session("MobileMoneyEFTIntegration")) = enMobileMoneyEFTIntegration.MPESA Then
                    lnkMobileMoneyEFTMPesaExport.Visible = True
                Else
                    lnkMobileMoneyEFTMPesaExport.Visible = False
                End If
                'Sohail (08 Dec 2014) -- End

                'SHANI (13 Mar 2015) -- Start
                'Enhancement - New EFT Report Custom CSV Report.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_CUSTOM_CSV Then
                    lnkEFT_Custom_CSV_Export.Visible = True
                Else
                    lnkEFT_Custom_CSV_Export.Visible = False
                End If

                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_CUSTOM_XLS Then
                    lnkEFT_Custom_XLS_Export.Visible = True
                Else
                    lnkEFT_Custom_XLS_Export.Visible = False
                End If
                'SHANI (13 Mar 2015) -- End

                'Sohail (01 Jun 2016) -- Start
                'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_FLEX_CUBE_RETAIL Then
                    lnkEFT_FlexCubeRetailGEFU_Export.Visible = True
                    'Nilay (10-Nov-2016) -- Start
                    'Enhancement : New EFT report : EFT ECO bank for OFFGRID
                    'lblPostingDate.Visible = True
                    'dtpPostingDate.Visible = True
                    lblPostingDate.Enabled = True
                    dtpPostingDate.Enabled = True
                    'Nilay (10-Nov-2016) -- End
                Else
                    lnkEFT_FlexCubeRetailGEFU_Export.Visible = False
                    'Nilay (10-Nov-2016) -- Start
                    'Enhancement : New EFT report : EFT ECO bank for OFFGRID
                    'lblPostingDate.Visible = False
                    'dtpPostingDate.Visible = False
                    'Sohail (18 Nov 2016) -- Start
                    'lblPostingDate.Enabled = False
                    'dtpPostingDate.Enabled = False
                    'Sohail (18 Nov 2016) -- End
                    'Nilay (10-Nov-2016) -- End
                End If
                'Sohail (01 Jun 2016) -- End

                'Nilay (10-Nov-2016) -- Start
                'Enhancement : New EFT report : EFT ECO bank for OFFGRID
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_ECO_BANK Then
                    lnkEFT_ECO_Bank.Visible = True
                    lblPostingDate.Enabled = True
                    dtpPostingDate.Enabled = True
                Else
                    lnkEFT_ECO_Bank.Visible = False
                    'Sohail (18 Nov 2016) -- Start
                    'lblPostingDate.Enabled = False
                    'dtpPostingDate.Enabled = False
                    'Sohail (18 Nov 2016) -- End
                End If
                'Nilay (10-Nov-2016) -- End

                'Sohail (28 Feb 2017) -- Start
                'Enhancement - Add New Bank Payment List Integration for TAMA
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_BANK_PAYMENT_LETTER Then
                    lnkBankPaymentLetter.Visible = True
                    chkLetterhead.Visible = True
                    chkAddresstoEmployeeBank.Visible = True
                Else
                    lnkBankPaymentLetter.Visible = False
                    chkLetterhead.Visible = False
                    chkAddresstoEmployeeBank.Visible = False
                End If
                'Sohail (28 Feb 2017) -- End


                'Varsha (20 Sep 2017) -- Start
                'Paytech Enhancement - 69.1 - EFT file for barclays bank should be in csv format. And contain the codes representing a specific item. A sample EFT file was shared with Rutta.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_BARCLAYS_BANK Then
                    lnkEFTBarclaysBankExport.Visible = True
                    lblPostingDate.Enabled = True
                    dtpPostingDate.Enabled = True
                Else
                    lnkEFTBarclaysBankExport.Visible = False
                End If
                'Varsha (20 Sep 2017)  -- End

                'S.SANDEEP [04-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_NATIONAL_BANK_OF_MALAWI Then
                    lnkEFTNationalBankMalawi.Visible = True
                Else
                    lnkEFTNationalBankMalawi.Visible = False
                End If
                'S.SANDEEP [04-May-2018] -- END

                'Sohail (04 Nov 2019) -- Start
                'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_FNB_BANK Then
                    lnkEFT_FNB_Bank_Export.Visible = True
                Else
                    lnkEFT_FNB_Bank_Export.Visible = False
                End If
                'Sohail (04 Nov 2019) -- End

                'Hemant (11 Dec 2019) -- Start
                'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_STANDARD_CHARTERED_BANK_S2B Then
                    lnkEFTStandardCharteredBank_S2B.Visible = True
                    'Sohail (15 Apr 2020) -- Start
                    'SANLAM LIFE INSURANCE Enhancement # 0004324 : Roll back to XLS format and additional column Posting date in MM/DD/YYYY.
                    lblPostingDate.Enabled = True
                    dtpPostingDate.Enabled = True
                    'Sohail (15 Apr 2020) -- End

                Else
                    lnkEFTStandardCharteredBank_S2B.Visible = False
                End If
                'Hemant (11 Dec 2019) -- End

                'Hemant (29 Jun 2020) -- Start
                'ENHANCEMENT(Benchmark) # 0004751: ABSA BANK EFT
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_ABSA_BANK Then
                    lnkEFT_ABSA_Bank.Visible = True
                Else
                    lnkEFT_ABSA_Bank.Visible = False
                End If
                'Hemant (29 Jun 2020) -- End

                'Hemant (24 Dec 2020) -- Start
                'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_NATIONAL_BANK_OF_MALAWI_XLSX Then
                    lnkEFTNationalBankMalawiXLSX.Visible = True

                    lblPostingDate.Enabled = True
                    dtpPostingDate.Enabled = True
                Else
                    lnkEFTNationalBankMalawiXLSX.Visible = False
                End If
                'Hemant (24 Dec 2020) -- End

                'Sohail (08 Jul 2021) -- Start
                'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_EQUITY_BANK_OF_KENYA Then
                    lnkEFTEquityBankKenya.Visible = True

                    lblMembershipRepo.Visible = True
                    cboMembershipRepo.Visible = True
                Else
                    lnkEFTEquityBankKenya.Visible = False
                End If

                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_NATIONAL_BANK_OF_KENYA Then
                    lnkEFTNationalBankKenya.Visible = True
                Else
                    lnkEFTNationalBankKenya.Visible = False
                End If
                'Sohail (08 Jul 2021) -- End

                'Sohail (27 Jul 2021) -- Start
                'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_CITI_BANK_KENYA Then
                    lnkEFTCitiBankKenya.Visible = True
                Else
                    lnkEFTCitiBankKenya.Visible = False
                End If
                'Sohail (27 Jul 2021) -- End

                'Hemant (29 Mar 2023) -- Start
                'ENHANCEMENT : A1X-711 -  ZSSF - Dynamics Nav database integration for Staff Payments 
                If CInt(Session("EFTIntegration")) = enEFTIntegration.DYNAMICS_NAVISION Then
                    lnkDynamicNavisionExport.Visible = True
                    lnkDynamicNavisionPaymentPostDB.Visible = True
                    'Hemant (18 Aug 2023) -- Start
                    'ENHANCEMENT(ZSSF): A1X-1194 - Real time posting of approved loans and leave expenses to Navision
                    lnkDynamicNavisionPaymentPostDB.Enabled = False
                    'Hemant (18 Aug 2023) -- End
                    lblPostingDate.Enabled = True
                    dtpPostingDate.Enabled = True
                Else
                    lnkDynamicNavisionExport.Visible = False
                    lnkDynamicNavisionPaymentPostDB.Visible = False
                End If
                'Hemant (29 Mar 2023) -- End  
                'Hemant (14 Apr 2023) -- Start
                'ENHANCEMENT(TRA) : A1X-802 - Create a generic EFT file with custom columns in CSV
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_GENERIC_CSV_WEB Then
                    lnkEFTGenericCSV.Visible = True
                    lnkEFTGenericCSVPostWeb.Visible = True
                    lnkEFTPaymentBatchApproval.Visible = True
                    lnkEFTPaymentBatchReconciliation.Visible = True
                Else
                    lnkEFTGenericCSV.Visible = False
                    lnkEFTGenericCSVPostWeb.Visible = False
                    lnkEFTPaymentBatchApproval.Visible = False
                    lnkEFTPaymentBatchReconciliation.Visible = False
                End If
                'Hemant (14 Apr 2023) -- End
                'Hemant (12 May 2023) -- Start
                'ENHANCEMENT : A1X-881 - Maj Consulting - Bank of Kigali CSV EFT
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_BANK_OF_KIGALI Then
                    lnkEFTBankOfKigali.Visible = True
                Else
                    lnkEFTBankOfKigali.Visible = False
                End If
                'Hemant (12 May 2023) -- End
                'Hemant (12 May 2023) -- Start
                'ENHANCEMENT : A1X-882 - Maj Consulting - NCBA EFT
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_NCBA Then
                    lnkEFT_NCBA.Visible = True
                Else
                    lnkEFT_NCBA.Visible = False
                End If
                'Hemant (12 May 2023) -- End
                'Hemant (07 Jul 2023) -- Start
                'ENHANCEMENT(TRA) : A1X-1034 - NBC Bank payment EFT
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_NBC Then
                    lnkEFT_NBC.Visible = True
                    lblPostingDate.Enabled = True
                    dtpPostingDate.Enabled = True
                Else
                    lnkEFT_NBC.Visible = False
                End If
                'Hemant (07 Jul 2023) -- End
            Else
                chkShowFNameSeparately.Enabled = False
                lnkEFTCityBankExport.Visible = False
                'Sohail (08 Dec 2014) -- Start
                'Enhancement - New EFT integration EFT CBA || New Mobile Money EFT integration with MPESA.
                lnkEFT_CBA_Export.Visible = False
                lnkMobileMoneyEFTMPesaExport.Visible = False
                'Sohail (08 Dec 2014) -- End
                lnkEFT_EXIM_Export.Visible = False 'Sohail (24 Dec 2014)
                'Sohail (24 Dec 2013) -- Start
                'Enhancement - Oman
                chkShowPayrollPeriod.Enabled = False
                chkShowSortCode.Enabled = False
                chkShowPayrollPeriod.Checked = False
                chkShowSortCode.Checked = False
                'Sohail (24 Dec 2013) -- End

                'SHANI (13 Mar 2015) -- Start
                'Enhancement - New EFT Report Custom CSV Report.
                lnkEFT_Custom_CSV_Export.Visible = False
                lnkEFT_Custom_XLS_Export.Visible = False
                'SHANI (13 Mar 2015) -- End
                'Sohail (01 Jun 2016) -- Start
                'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.

                'Nilay (10-Nov-2016) -- Start
                'Enhancement : New EFT report : EFT ECO bank for OFFGRID
                'lblPostingDate.Visible = False
                'dtpPostingDate.Visible = False
                lblPostingDate.Enabled = False
                dtpPostingDate.Enabled = False
                'Nilay (10-Nov-2016) -- End

                lnkEFT_FlexCubeRetailGEFU_Export.Visible = False
                'Sohail (01 Jun 2016) -- End

                'Nilay (10-Nov-2016) -- Start
                'Enhancement : New EFT report : EFT ECO bank for OFFGRID
                lnkEFT_ECO_Bank.Visible = False
                'Nilay (10-Nov-2016) -- End
                'Sohail (28 Feb 2017) -- Start
                'Enhancement - Add New Bank Payment List Integration for TAMA
                lnkBankPaymentLetter.Visible = False
                chkLetterhead.Visible = False
                chkAddresstoEmployeeBank.Visible = False
                'Sohail (28 Feb 2017) -- End

                'Varsha (20 Sep 2017) -- Start
                'Paytech Enhancement - 69.1 - EFT file for barclays bank should be in csv format. And contain the codes representing a specific item. A sample EFT file was shared with Rutta.
                lnkEFTBarclaysBankExport.Visible = False
                'Varsha (20 Sep 2017) -- End

                'S.SANDEEP [04-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
                lnkEFTNationalBankMalawi.Visible = False
                'S.SANDEEP [04-May-2018] -- END
                'Sohail (04 Nov 2019) -- Start
                'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
                lnkEFT_FNB_Bank_Export.Visible = False
                'Sohail (04 Nov 2019) -- End
                'Hemant (11 Dec 2019) -- Start
                'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
                lnkEFTStandardCharteredBank_S2B.Visible = False
                'Hemant (11 Dec 2019) -- End
                'Hemant (29 Jun 2020) -- Start
                'ENHANCEMENT(Benchmark) # 0004751: ABSA BANK EFT
                lnkEFT_ABSA_Bank.Visible = False
                'Hemant (29 Jun 2020) -- End
                'Hemant (24 Dec 2020) -- Start
                'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
                lnkEFTNationalBankMalawiXLSX.Visible = False
                'Hemant (24 Dec 2020) -- End

                'Sohail (08 Jul 2021) -- Start
                'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
                lnkEFTEquityBankKenya.Visible = False
                lnkEFTNationalBankKenya.Visible = False
                'Sohail (08 Jul 2021) -- End

                'Sohail (27 Jul 2021) -- Start
                'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
                lnkEFTCitiBankKenya.Visible = False
                'Sohail (27 Jul 2021) -- End

                'Hemant (29 Mar 2023) -- Start
                'ENHANCEMENT : A1X-711 -  ZSSF - Dynamics Nav database integration for Staff Payments 
                lnkDynamicNavisionExport.Visible = False
                lnkDynamicNavisionPaymentPostDB.Visible = False
                'Hemant (29 Mar 2023) -- End  
                'Hemant (14 Apr 2023) -- Start
                'ENHANCEMENT(TRA) : A1X-802 - Create a generic EFT file with custom columns in CSV
                lnkEFTGenericCSV.Visible = False
                lnkEFTGenericCSVPostWeb.Visible = False
                lnkEFTPaymentBatchApproval.Visible = False
                lnkEFTPaymentBatchReconciliation.Visible = False
                'Hemant (14 Apr 2023) -- End
                'Hemant (12 May 2023) -- Start
                'ENHANCEMENT : A1X-881 - Maj Consulting - Bank of Kigali CSV EFT
                lnkEFTBankOfKigali.Visible = False
                'Hemant (12 May 2023) -- End
                'Hemant (12 May 2023) -- Start
                'ENHANCEMENT : A1X-882 - Maj Consulting - NCBA EFT
                lnkEFT_NCBA.Visible = False
                'Hemant (12 May 2023) -- End
                'Hemant (07 Jul 2023) -- Start
                'ENHANCEMENT(TRA) : A1X-1034 - NBC Bank payment EFT
                lnkEFT_NBC.Visible = False
                'Hemant (07 Jul 2023) -- End
            End If

            If CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.EFT_SFIBANK Then

                cboEmployeeName.SelectedValue = "0"
                cboBankName.Enabled = False
                cboBranchName.SelectedValue = "0"
                cboBranchName.Enabled = False
                'txtEmpCode.Enabled = False

            ElseIf CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.EFT_StandardCharteredBank Then
                cboEmployeeName.SelectedValue = "0"
                cboBankName.Enabled = False
                cboBranchName.SelectedValue = "0"
                cboBranchName.Enabled = False

                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance Payment.
            ElseIf CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.EFT_ADVANCE_SALARY_CSV Then
                lnkEFT_Custom_CSV_Export.Visible = True
            ElseIf CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.EFT_ADVANCE_SALARY_XLS Then
                lnkEFT_Custom_XLS_Export.Visible = True
                'Sohail (25 Mar 2015) -- End
            End If

            'Sohail (15 Mar 2014) -- Start
            'Enhancement - Grouping on cut off amount.
            'Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            'If (CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.Electronic_Fund_Transfer _
            '  AndAlso CInt(Session("Compcountryid")) = 162) OrElse CInt(cboReportType.SelectedValue) = enBankPaymentReport.Bank_Payment_Voucher Then 'OMAN EFT 
            'Pinkal (12-Jun-2014) -- Start  'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]  OrElse CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Bank_Payment_Voucher
            If (CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Electronic_Fund_Transfer) _
              AndAlso CInt(Session("Compcountryid")) = 162) OrElse CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Bank_Payment_Voucher) Then 'OMAN EFT 
                'Sohail (12 Feb 2018) -- End

                chkEmployeeSign.Checked = False
                chkShowGroupByBankBranch.Checked = False
                chkSignatory1.Checked = False
                chkSignatory2.Checked = False
                chkSignatory3.Checked = False
                cboCountry.SelectedIndex = 0
                'cboCurrency.SelectedIndex = 0 'Sohail (01 Apr 2014)
                'mstrStringIds = ""
                'mstrStringName = ""
                'mintViewIdx = 0
                cboReportMode.Enabled = True
                chkShowFNameSeparately.Checked = False
                chkShowPayrollPeriod.Checked = True
                chkShowSortCode.Checked = True
                cboCountry.Enabled = False
                cboCurrency.Enabled = True
                chkEmployeeSign.Enabled = False
                chkShowGroupByBankBranch.Enabled = False
                chkSignatory1.Enabled = False
                chkSignatory2.Enabled = False
                chkSignatory3.Enabled = False
                chkShowPayrollPeriod.Checked = False
                chkShowSortCode.Checked = False
                chkShowPayrollPeriod.Enabled = False
                chkShowSortCode.Enabled = False
                chkShowFNameSeparately.Enabled = False
                chkDefinedSignatory.Enabled = False

                chkShowBankCode.Checked = False
                chkShowBranchCode.Checked = False
                chkDefinedSignatory2.Checked = False
                chkDefinedSignatory3.Checked = False

                chkShowBankCode.Enabled = False
                chkShowBranchCode.Enabled = False
                chkDefinedSignatory2.Enabled = False
                chkDefinedSignatory3.Enabled = False


                lblCutOffAmount.Visible = True
                txtCutOffAmount.Visible = True


                'Sohail (01 Apr 2014) -- Start
                'ENHANCEMENT - 
                'Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.Bank_payment_List)


                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                'Sohail (12 Feb 2018) -- Start
                'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                'If CInt(cboReportType.SelectedValue) = enBankPaymentReport.Bank_Payment_Voucher Then
                If CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Bank_Payment_Voucher) Then
                    'Sohail (12 Feb 2018) -- End
                    Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.Bank_Payment_Voucher)
                    chkShowReportHeader.Visible = True
                    chkShowReportHeader.Checked = True
                    chkShowEmployeeCode.Visible = False
                Else
                    Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.EFT_VOLTAMP)
                End If
                'Pinkal (12-Jun-2014) -- End
                'Sohail (01 Apr 2014) -- End
            Else
                lblCutOffAmount.Visible = False
                txtCutOffAmount.Visible = False
                txtCutOffAmount.Text = ""

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                chkShowEmployeeCode.Visible = True
                'Pinkal (12-Jun-2014) -- End

                Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            End If
            'Sohail (15 Mar 2014) -- End

            'Sohail (12 Feb 2015) -- Start
            'Enhancement - EFT CBA with New CSV Format.
            mstrOrderByDisplay = objBankPaymentList.OrderByDisplay
            mstrOrderByQuery = objBankPaymentList.OrderByQuery
            'Sohail (12 Feb 2015) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("cboReportType_SelectedIndexChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub cboCompanyBankName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCompanyBankName.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsList As New DataSet
        Try
            dsList = objCompanyBank.GetComboList(CInt(Session("CompanyUnkId")), 2, "List", " cfbankbranch_master.bankgroupunkid = " & CInt(cboCompanyBankName.SelectedValue) & " ")
            With cboCompanyBranchName
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            cboCompanyBranchName_SelectedIndexChanged(New Object(), New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError("cboCompanyBankName_SelectedIndexChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub cboCompanyBranchName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCompanyBranchName.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsCombos As New DataSet
        Try
            dsCombos = objCompanyBank.GetComboListBankAccount("Account", CInt(Session("CompanyUnkId")), True, CInt(cboCompanyBankName.SelectedValue), CInt(cboCompanyBranchName.SelectedValue))
            With cboCompanyAccountNo
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("Account")
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("cboCompanyBranchName_SelectedIndexChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub cboBankName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankName.SelectedIndexChanged
        Dim objBankBranch As New clsbankbranch_master
        Dim dsList As New DataSet
        Try
            dsList = objBankBranch.getListForCombo("List", True, CInt(cboBankName.SelectedValue))
            With cboBranchName
                .DataValueField = "branchunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("cboBankName_SelectedIndexChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPayment As New clsPayment_tran
        Dim dsCombos As DataSet
        Try
            dsCombos = objPayment.Get_DIST_ChequeNo("Cheque", True, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, Session("database_name").ToString, CInt(cboPeriod.SelectedValue))
            With cboChequeNo
                .DataValueField = "chequeno"
                .DataTextField = "chequeno"
                .DataSource = dsCombos.Tables("Cheque")
                .DataBind()
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (26 May 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
    Protected Sub cboRPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRPeriod.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Try
            dsCombos = objPaymentBatchPostingMaster.GetBatchNameList("List", CInt(cboRPeriod.SelectedValue), True, clspaymentbatchposting_master.enApprovalStatus.Approved, True, , Session("Database_Name").ToString())
            With cboRBatch
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                If mblnIsFromGrid = True Then
                    Dim drVoucher() As DataRow = dsCombos.Tables(0).Select("Name = '" & mstrVoucherNo & "'")
                    If drVoucher.Length > 0 Then
                        .SelectedValue = CInt(drVoucher(0).Item("id"))
                    End If
                Else
                    .SelectedValue = 0
                End If
            End With

            dgvDataList.DataSource = Nothing
            dgvDataList.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPaymentBatchPostingMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objBranch As New clsbankbranch_master
        Try
            dsCombos = objBranch.getListForCombo("Branch", True, CInt(cboBankGroup.SelectedValue))
            With cboBankBranch
                .DataValueField = "branchunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Branch")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboRBatch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRBatch.SelectedIndexChanged
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Try
            If CInt(cboRBatch.SelectedValue) > 0 Then
                objPaymentBatchPostingMaster._PaymentBatchPostingunkid = CInt(cboRBatch.SelectedValue)
                mstrVoucherNo = objPaymentBatchPostingMaster._VoucherNo
                btnReconciliation.Enabled = True
                dgvDataList.DataSource = Nothing
                dgvDataList.DataBind()
                mdtStatus = New DataTable
                Call FillStatusCombo(mdtStatus)

            ElseIf CInt(cboRBatch.SelectedValue) < 0 Then
                mstrVoucherNo = cboRBatch.SelectedItem.Text
                btnReconciliation.Enabled = True
                dgvDataList.DataSource = Nothing
                dgvDataList.DataBind()
                mdtStatus = New DataTable
                Call FillStatusCombo(mdtStatus)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPaymentBatchPostingMaster = Nothing
        End Try
    End Sub

    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    Private Sub cboRStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRStatus.SelectedIndexChanged
        Try
            If CInt(cboRStatus.SelectedIndex) > 0 Then
                btnSubmitForApproval.Visible = False
                dgvDataList.DataSource = Nothing
                dgvDataList.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (21 Jul 2023) -- End

    Protected Sub cboAPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAPeriod.SelectedIndexChanged, _
                                                                                                                cboAStatus.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Try
            dsCombos = objPaymentBatchPostingMaster.GetBatchNameList("List", CInt(cboAPeriod.SelectedValue), CInt(cboAStatus.SelectedValue), False, True, , Session("Database_Name").ToString)
            With cboABatch
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dgvDataList.DataSource = Nothing
            dgvDataList.DataBind()
            btnApproveReject.Visible = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPaymentBatchPostingMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub
#End Region

#Region "LinkButton Event"

    Protected Sub lnkEFTCityBankExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTCityBankExport.Click
        Try
            'Sohail (08 Dec 2014) -- Start
            'Enhancement - New Mobile Money EFT integration with MPESA.
            'If CInt(cboReportType.SelectedIndex) > 0 AndAlso CInt(cboCurrency.SelectedValue) <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 4, "Currency is mandatory information. Please select Currency."), Me)
            '    cboCurrency.Focus()
            '    Exit Try
            'ElseIf CInt(cboCompanyBankName.SelectedValue) <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 5, "Please select Company Bank."), Me)
            '    cboCompanyBankName.Focus()
            '    Exit Try
            'ElseIf CInt(cboCompanyBranchName.SelectedValue) <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 6, "Please select Company Bank Branch."), Me)
            '    cboCompanyBranchName.Focus()
            '    Exit Try
            'ElseIf CInt(cboCompanyAccountNo.SelectedValue) <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 7, "Please select Company Bank Account."), Me)
            '    cboCompanyAccountNo.Focus()
            '    Exit Try
            'ElseIf CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Sohail (08 Dec 2014) -- End
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            ElseIf Session("DatafileExportPath").ToString = "" Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 17, "Please Set Data File Export Path From Aruti Configuration -> Option -> Integration -> EFT Integration."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Please Set Data File Export Path From Aruti Configuration -> Option -> Integration -> EFT Integration."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            ElseIf Session("DatafileName").ToString = "" Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 18, "Please Set Data File Name From Aruti Configuration -> Option -> Integration -> EFT Integration."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Please Set Data File Name From Aruti Configuration -> Option -> Integration -> EFT Integration."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try

            End If

            'Sohail (08 Dec 2014) -- Start
            'Enhancement - New Mobile Money EFT integration with MPESA.
            'objBankPaymentList.SetDefaultValue()
            'If CInt(cboPeriod.SelectedValue) > 0 Then
            '    objBankPaymentList._PeriodId = CInt(cboPeriod.SelectedValue)
            'End If

            'If CInt(cboBankName.SelectedValue) > 0 Then
            '    objBankPaymentList._BankId = CInt(cboBankName.SelectedValue)
            '    objBankPaymentList._BankName = cboBankName.SelectedItem.Text
            'End If

            'If CInt(cboBranchName.SelectedValue) > 0 Then
            '    objBankPaymentList._BranchId = CInt(cboBranchName.SelectedValue)
            '    objBankPaymentList._BankBranchName = cboBranchName.SelectedItem.Text
            'End If

            'If CInt(cboCountry.SelectedValue) > 0 Then
            '    objBankPaymentList._CountryId = CInt(cboCountry.SelectedValue)
            '    objBankPaymentList._CountryName = cboCountry.SelectedItem.Text
            'End If

            'If CInt(cboEmployeeName.SelectedValue) > 0 Then
            '    objBankPaymentList._EmployeeUnkId = CInt(cboEmployeeName.SelectedValue)
            '    objBankPaymentList._EmpName = cboEmployeeName.SelectedItem.Text
            'End If

            ''Sohail (07 Dec 2013) -- Start
            ''Enhancement - OMAN
            ''If Trim(txtEmpCode.Text) <> "" Then
            ''    objBankPaymentList._EmpCode = txtEmpCode.Text
            ''End If
            'If cboChequeNo.SelectedIndex > 0 Then
            '    objBankPaymentList._ChequeNo = cboChequeNo.Text
            'Else
            '    objBankPaymentList._ChequeNo = ""
            'End If
            ''Sohail (07 Dec 2013) -- End


            'If Trim(txtAmount.Text) <> "" And Trim(txtAmountTo.Text) <> "" Then
            '    objBankPaymentList._Amount = txtAmount.Text
            '    objBankPaymentList._AmountTo = txtAmountTo.Text
            'End If

            'If chkEmployeeSign.Checked = True Then
            '    objBankPaymentList._IsEmployeeSign = True
            'End If
            'If chkSignatory1.Checked = True Then
            '    objBankPaymentList._IsSignatory1 = True
            'End If
            'If chkSignatory2.Checked = True Then
            '    objBankPaymentList._IsSignatory2 = True
            'End If
            'If chkSignatory3.Checked = True Then
            '    objBankPaymentList._IsSignatory3 = True
            'End If

            'objBankPaymentList._ShowFNameSurNameInSeparateColumn = chkShowFNameSeparately.Checked
            'Sohail (24 Dec 2013) -- Start
            'Enhancement - Oman
            'objBankPaymentList._ShowPayrollPeriod = chkShowPayrollPeriod.Checked
            'objBankPaymentList._ShowSortCode = chkShowSortCode.Checked
            ''Sohail (24 Dec 2013) -- End
            'objBankPaymentList._CurrencyId = CInt(cboCurrency.SelectedValue)
            'objBankPaymentList._CurrencyName = cboCurrency.SelectedItem.Text
            'objBankPaymentList._ShowGroupByBankBranch = chkShowGroupByBankBranch.Checked

            'objBankPaymentList._PeriodName = cboPeriod.Text

            'Dim objPeriod As New clscommom_period_Tran
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            'objBankPaymentList._PeriodStartDate = objPeriod._Start_Date
            'objBankPaymentList._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing

            'objBankPaymentList._ReportTypeId = CType(cboReportType.SelectedItem, ListItem).Value
            'objBankPaymentList._ReportTypeName = cboReportType.SelectedItem.Text

            'objBankPaymentList._ReportModeId = CInt(cboReportMode.SelectedIndex)
            'objBankPaymentList._ReportModeName = cboReportMode.SelectedItem.Text

            'objBankPaymentList._CompanyBranchId = CInt(cboCompanyBranchName.SelectedValue)
            'objBankPaymentList._CompanyBankBranchName = cboCompanyBranchName.SelectedItem.Text
            'objBankPaymentList._CompanyBankAccountNo = cboCompanyAccountNo.SelectedItem.Text

            'objBankPaymentList._UserUnkId = Session("UserId")
            'objBankPaymentList._CompanyUnkId = Session("CompanyUnkId")
            'objBankPaymentList._UserAccessFilter = Session("AccessLevelFilterString")
            'Sohail (08 Dec 2014) -- End

            'objBankPaymentList._CompanyBankGroupId = Session("CompanyBankGroupId")
            'objBankPaymentList._CompanyBranchId = Session("CompanyBankBranchId")
            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False
            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            'objBankPaymentList._EFTCompanyName = Session("gstrCompanyName")
            objBankPaymentList._EFTCompanyName = Session("CompName").ToString
            'Sohail (09 Jan 2016) -- End
            objBankPaymentList._CityBankDataExportPath = "InValid Path"

            Dim strConfigExpPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & Session("DatafileName").ToString & ".txt"

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Sohail (09 Jan 2016) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objBankPaymentList.EFT_CityDirect_Export_GenerateReport(strConfigExpPath) = True Then
            If objBankPaymentList.EFT_CityDirect_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                       CInt(Session("UserId")), _
                                                                       CInt(Session("Fin_year")), _
                                                                       CInt(Session("CompanyUnkId")), _
                                                                       objPeriod._Start_Date, _
                                                                       objPeriod._End_Date, _
                                                                       Session("UserAccessModeSetting").ToString, True, _
                                                                       CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                       strConfigExpPath, _
                                                                       Session("CompName").ToString, _
                                                                       ConfigParameter._Object._CurrentDateAndTime, _
                                                                       Session("DatafileExportPath").ToString, _
                                                                       Session("SMimeRunPath").ToString, _
                                                                       Session("fmtCurrency").ToString) = True Then
                'Shani(20-Nov-2015) -- End

                Session("ExFileName") = strConfigExpPath
                'Shani [ 10 DEC 2014 ] -- START
                'Issue : Chrome is not supporting ShowModalDialog option now."
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

                'Shani [ 10 DEC 2014 ] -- END

                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError("Data Exported Successfuly.", Me)
                DisplayMessage.DisplayMessage("Data Exported Successfuly.", Me)
                'Sohail (23 Mar 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkEFTCityBankExport_Click:- " & ex.Message, Me)
        End Try
    End Sub

    'Sohail (08 Dec 2014) -- Start
    'Enhancement - New EFT Integration EFT CBA || New Mobile Money EFT integration with MPESA.
    Protected Sub lnkEFT_CBA_Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_CBA_Export.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            'Sohail (12 Feb 2015) -- Start
            'Enhancement - EFT CBA with New CSV Format.
            'Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            'If objBankPaymentList.EFT_CBA_Export_GenerateReport(mstrPath) = True Then
            '    If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
            '        Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
            '        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
            '    End If
            'End If
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\CBA.csv"

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Sohail (09 Jan 2016) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objBankPaymentList.EFT_CBA_Export_GenerateReport(mstrPath) = True Then
            If objBankPaymentList.EFT_CBA_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                CInt(Session("UserId")), _
                                                                CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                objPeriod._Start_Date, _
                                                                objPeriod._End_Date, _
                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                                Session("fmtCurrency").ToString) = True Then
                'Shani(20-Nov-2015) -- End

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If
            'Sohail (12 Feb 2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkEFT_CBA_Export_Click:- " & ex.Message, Me)
        End Try
    End Sub

    'Sohail (24 Dec 2014) -- Start
    'AMI Enhancement - New EFT Report EFT EXIM.
    Protected Sub lnkEFT_EXIM_Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_EXIM_Export.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If


            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Sohail (09 Jan 2016) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objBankPaymentList.EFT_EXIM_Export_GenerateReport(mstrPath) = True Then
            If objBankPaymentList.EFT_EXIM_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 objPeriod._Start_Date, _
                                                                 objPeriod._End_Date, _
                                                                 Session("UserAccessModeSetting").ToString, True, _
                                                                 CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                 mstrPath, CDate(Session("fin_startdate")), _
                                                                 CDate(Session("fin_enddate")), _
                                                                 CBool(Session("OpenAfterExport")), _
                                                                 Session("fmtCurrency").ToString) = True Then
                'Shani(20-Nov-2015) -- End

                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End

                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "lnkEFT_EXIM_Export_Click", mstrModuleName)
            DisplayMessage.DisplayError("Procedure : lnkEFT_EXIM_Export_Click : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (24 Dec 2014) -- End

    Protected Sub lnkMobileMoneyEFTMPesaExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMobileMoneyEFTMPesaExport.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\MPesa.csv"

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Sohail (09 Jan 2016) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objBankPaymentList.EFT_MPesa_Export_GenerateReport(mstrPath) = True Then
            If objBankPaymentList.EFT_MPesa_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                  CInt(Session("UserId")), _
                                                                  CInt(Session("Fin_year")), _
                                                                  CInt(Session("CompanyUnkId")), _
                                                                  objPeriod._Start_Date, _
                                                                  objPeriod._End_Date, _
                                                                  Session("UserAccessModeSetting").ToString, True, _
                                                                  CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                  mstrPath, Session("fmtCurrency").ToString) = True Then
                'Shani(20-Nov-2015) -- End

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkMobileMoneyEFTMPesaExport_Click:- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (08 Dec 2014) -- End

    'SHANI [13 Mar 2015]-START
    'Enhancement - New EFT Report Custom CSV Report.

    Private Sub lnkEFT_Custom_CSV_Export_LinkClicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_Custom_CSV_Export.Click, _
                                                                                                                  lnkEFTGenericCSV.Click
        'Hemant (14 Apr 2023) -- [lnkEFTGenericCSV.Click]
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If
            mintModeId = -1
            mstrEFTCustomColumnsIds = String.Empty
            mblnShowColumnHeader = False
            marrEFTCustomColumnIds = New ArrayList
            dtEFTTable = New DataTable
            mintModeId = enEFT_Export_Mode.CSV
            Call GetEFTValue()
            Call FillEFTList()
            Call GetEFTValue() 'Sohail (13 Apr 2016)
            'Hemant (26 May 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
            mblnpopupReconciliation = False
            popupReconciliation.Hide()
            mblnpopupBatchApproval = False
            popupBatchApproval.Hide()
            'Hemant (26 May 2023) -- End
            popup_EFTCustom.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkEFT_Custom_CSV_Export_LinkClicked : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub lnkEFT_Custom_XLS_Export_LinkClicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_Custom_XLS_Export.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If
            mintModeId = -1
            mstrEFTCustomColumnsIds = String.Empty
            mblnShowColumnHeader = False
            marrEFTCustomColumnIds = New ArrayList
            dtEFTTable = New DataTable
            mintModeId = enEFT_Export_Mode.XLS
            Call GetEFTValue()
            Call FillEFTList()
            Call GetEFTValue() 'Sohail (13 Apr 2016)
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            chkSaveAsTXT.Visible = False
            chkSaveAsTXT.Checked = False
            chkTABDelimiter.Visible = False
            chkTABDelimiter.Checked = False
            'Hemant (27 June 2019) -- End
            popup_EFTCustom.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkEFT_Custom_XLS_Export_LinkClicked : " & ex.Message, Me)
        End Try
    End Sub

    'Sohail (01 Jun 2016) -- Start
    'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.
    Protected Sub lnkEFT_FlexCubeRetailGEFU_Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_FlexCubeRetailGEFU_Export.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then 'Nilay (10-Nov-2016) -- [dtpPostingDate.GetDate = Nothing]
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), Me)
                'Sohail (23 Mar 2019) -- End
                dtpPostingDate.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\FlexCubeRetailGEFU.txt"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_Flex_Cube_Retail_GEFU_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                                CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                objPeriod._Start_Date, _
                                                                                objPeriod._End_Date, _
                                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                                CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                                                Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("lnkEFT_FlexCubeRetailGEFU_Export_Click:- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (01 Jun 2016) -- End

    'Nilay (10-Nov-2016) -- Start
    'Enhancement : New EFT report : EFT ECO bank for OFFGRID
    Protected Sub lnkEFT_ECO_Bank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_ECO_Bank.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), Me)
                'Sohail (23 Mar 2019) -- End
                dtpPostingDate.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\ECOBank.csv"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_ECO_Bank(Session("Database_Name").ToString, _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               objPeriod._Start_Date, _
                                               objPeriod._End_Date, _
                                               Session("UserAccessModeSetting").ToString, True, _
                                               CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                               Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("lnkEFT_ECO_Bank_Click:- " & ex.Message, Me)
        End Try
    End Sub
    'Nilay (10-Nov-2016) -- End

    'Sohail (28 Feb 2017) -- Start
    'Enhancement - Add New Bank Payment List Integration for TAMA
    Protected Sub lnkBankPaymentLetter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBankPaymentLetter.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False


            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_Bank_Payment_Letter_Report(Session("Database_Name").ToString, _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 objPeriod._Start_Date, _
                                                                 objPeriod._End_Date, _
                                                                 Session("UserAccessModeSetting").ToString, True, _
                                                                 CBool(Session("IsIncludeInactiveEmp")), True, "", _
                                                                 Session("fmtCurrency").ToString, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = True Then

                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Session("objRpt") = objBankPaymentList._Rpt
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("lnkBankPaymentLetter_Click:- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (28 Feb 2017) -- End

    Protected Sub ChangeLocation(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim commandArgument As String = TryCast(sender, LinkButton).CommandArgument
            Dim rowIndex As Integer = TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex
            Dim xRow As DataRow = dtEFTTable.Rows(rowIndex)
            Dim xNewRow As DataRow = dtEFTTable.NewRow
            xNewRow.ItemArray = xRow.ItemArray
            If commandArgument = "down" Then
                If rowIndex >= lvEFTCustomColumns.Rows.Count - 1 Then Exit Sub
                dtEFTTable.Rows.RemoveAt(rowIndex)
                dtEFTTable.Rows.InsertAt(xNewRow, rowIndex + 1)
                dtEFTTable.Rows.IndexOf(xNewRow)
            ElseIf commandArgument = "up" Then
                If rowIndex <= 0 Then Exit Sub
                dtEFTTable.Rows.Remove(xRow)
                dtEFTTable.Rows.InsertAt(xNewRow, rowIndex - 1)
                dtEFTTable.Rows.IndexOf(xNewRow)
            End If
            With lvEFTCustomColumns
                .DataSource = dtEFTTable
                .DataBind()
            End With
            dtEFTTable.AcceptChanges()

        Catch ex As Exception
            DisplayMessage.DisplayError("ChangeAllocationLocation :- " & ex.Message, Me)
        Finally
            popup_EFTCustom.Show()
        End Try
    End Sub
    'SHANI [13 Mar 2015]--END 


    'Varsha (20 Sep 2017) -- Start
    'Paytech Enhancement - 69.1 - EFT file for barclays bank should be in csv format. And contain the codes representing a specific item. A sample EFT file was shared with Rutta.
    Private Sub lnkEFTBarclaysBankExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTBarclaysBankExport.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), Me)
                'Sohail (23 Mar 2019) -- End
                dtpPostingDate.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\BarclaysBank.csv"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_Barclays_Bank(Session("Database_Name").ToString, _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               objPeriod._Start_Date, _
                                               objPeriod._End_Date, _
                                               Session("UserAccessModeSetting").ToString, True, _
                                               CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                               Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("lnkEFTBarclaysBankExport_Click:- " & ex.Message, Me)
        End Try
    End Sub
    'Varsha (20 Sep 2017) -- End

    'S.SANDEEP [04-May-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
    Protected Sub lnkEFTNationalBankMalawi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTNationalBankMalawi.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_National_Bank_Malawi_GenerateReport(Session("Database_Name").ToString, _
                                                                                CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                objPeriod._Start_Date, _
                                                                                objPeriod._End_Date, _
                                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                                CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                                                Session("fmtCurrency").ToString) = True Then

                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End

                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("lnkEFTNationalBankMalawi_Click:- " & ex.Message, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04-May-2018] -- END
    'Sohail (04 Nov 2019) -- Start
    'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
    Private Sub lnkEFT_FNB_Bank_Export_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkEFT_FNB_Bank_Export.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\FNB.csv"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_FNB_Bank_Export_GenerateReport(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               objPeriod._Start_Date, _
                                                               objPeriod._End_Date, _
                                                               Session("UserAccessModeSetting").ToString, True, _
                                                               CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                               Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = mstrPath
                Export.Show()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("lnkEFT_FNB_Bank_Export_Click:- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (04 Nov 2019) -- End

    'Hemant (11 Dec 2019) -- Start
    'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
    Private Sub lnkEFTStandardCharteredBank_S2B_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkEFTStandardCharteredBank_S2B.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
                'Sohail (15 Apr 2020) -- Start
                'SANLAM LIFE INSURANCE Enhancement # 0004324 : Roll back to XLS format and additional column Posting date in MM/DD/YYYY.
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), Me)
                dtpPostingDate.Focus()
                Exit Try
                'Sohail (15 Apr 2020) -- End
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            'Sohail (31 Mar 2020) -- Start
            'SANLAM LIFE INSURANCE Enhancement # 0004324 : Assist to change the File Extension from XLS to CSV.
            'Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            'Sohail (15 Apr 2020) -- Start
            'SANLAM LIFE INSURANCE Enhancement # 0004324 : Roll back to XLS format and additional column Posting date in MM/DD/YYYY.
            'Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\S2B.csv"
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            'Sohail (15 Apr 2020) -- End
            'Sohail (31 Mar 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_Standard_Chartered_Bank_S2B_Report(Session("Database_Name").ToString, _
                                                                                CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                objPeriod._Start_Date, _
                                                                                objPeriod._End_Date, _
                                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                                CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                                                Session("fmtCurrency").ToString) = True Then

                'Sohail (31 Mar 2020) -- Start
                'SANLAM LIFE INSURANCE Enhancement # 0004324 : Assist to change the File Extension from XLS to CSV.
                'If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                '    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                '    Export.Show()

                'End If
                'Sohail (15 Apr 2020) -- Start
                'SANLAM LIFE INSURANCE Enhancement # 0004324 : Roll back to XLS format and additional column Posting date in MM/DD/YYYY.
                'Session("ExFileName") = mstrPath
                'Export.Show()
                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                    Export.Show()
                End If
                'Sohail (15 Apr 2020) -- End
                'Sohail (31 Mar 2020) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("lnkEFTStandardCharteredBank_S2B_Click:- " & ex.Message, Me)
        End Try
    End Sub
    'Hemant (11 Dec 2019) -- End

    'Hemant (29 Jun 2020) -- Start
    'ENHANCEMENT(Benchmark) # 0004751: ABSA BANK EFT
    Private Sub lnkEFT_ABSA_Bank_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkEFT_ABSA_Bank.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\ABSA.csv"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_ABSA_Bank_Report(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               objPeriod._Start_Date, _
                                                               objPeriod._End_Date, _
                                                               Session("UserAccessModeSetting").ToString, True, _
                                                               CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                               Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = mstrPath
                Export.Show()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'Hemant (29 Jun 2020) -- End
    'Hemant (24 Dec 2020) -- Start
    'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
    Protected Sub lnkEFTNationalBankMalawiXLSX_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTNationalBankMalawiXLSX.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), Me)
                dtpPostingDate.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            Dim strConfigExpPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\EFT_National_Bank.xlsx"

            If objBankPaymentList.EFT_National_Bank_Malawi_XLSX_GenerateReport(Session("Database_Name").ToString, _
                                                                                CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                objPeriod._Start_Date, _
                                                                                objPeriod._End_Date, _
                                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                                CBool(Session("IsIncludeInactiveEmp")), True, strConfigExpPath, _
                                                                                Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = strConfigExpPath
                Export.Show()
                DisplayMessage.DisplayMessage("Data Exported Successfuly.", Me)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub
    'Hemant (24 Dec 2020) -- End

    'Sohail (08 Jul 2021) -- Start
    'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
    Protected Sub lnkEFTEquityBankKenya_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTEquityBankKenya.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            ElseIf CInt(cboMembershipRepo.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "Please select membership to generate report."), Me)
                cboMembershipRepo.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_Equity_Bank_Kenya_GenerateReport(Session("Database_Name").ToString, _
                                                                                CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                objPeriod._Start_Date, _
                                                                                objPeriod._End_Date, _
                                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                                CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                                                Session("fmtCurrency").ToString) = True Then

                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                    Export.Show()

                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkEFTNationalBankKenya_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTNationalBankKenya.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_National_Bank_Kenya_GenerateReport(Session("Database_Name").ToString, _
                                                                                CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                objPeriod._Start_Date, _
                                                                                objPeriod._End_Date, _
                                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                                CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                                                Session("fmtCurrency").ToString) = True Then

                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                    Export.Show()

                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub
    'Sohail (08 Jul 2021) -- End

    'Sohail (27 Jul 2021) -- Start
    'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
    Protected Sub lnkEFTCitiBankKenya_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTCitiBankKenya.Click
        Try

            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            ElseIf Session("DatafileExportPath").ToString = "" Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Please Set Data File Export Path From Aruti Configuration -> Option -> Integration -> EFT Integration."), Me)
                cboReportMode.Focus()
                Exit Try
            ElseIf Session("DatafileName").ToString = "" Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Please Set Data File Name From Aruti Configuration -> Option -> Integration -> EFT Integration."), Me)
                cboReportMode.Focus()
                Exit Try

            End If


            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False
            objBankPaymentList._EFTCompanyName = Session("CompName").ToString
            objBankPaymentList._CityBankDataExportPath = "InValid Path"

            Dim strConfigExpPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & Session("DatafileName").ToString & ".txt"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_CityBankKenya_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                       CInt(Session("UserId")), _
                                                                       CInt(Session("Fin_year")), _
                                                                       CInt(Session("CompanyUnkId")), _
                                                                       objPeriod._Start_Date, _
                                                                       objPeriod._End_Date, _
                                                                       Session("UserAccessModeSetting").ToString, True, _
                                                                       CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                       strConfigExpPath, _
                                                                       Session("CompName").ToString, _
                                                                       ConfigParameter._Object._CurrentDateAndTime, _
                                                                       Session("DatafileExportPath").ToString, _
                                                                       Session("SMimeRunPath").ToString, _
                                                                       Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = strConfigExpPath
                Export.Show()

                DisplayMessage.DisplayMessage("Data Exported Successfuly.", Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'Sohail (27 Jul 2021) -- End

    'Hemant (29 Mar 2023) -- Start
    'ENHANCEMENT : A1X-711 -  ZSSF - Dynamics Nav database integration for Staff Payments 
    Private Sub lnkDynamicNavisionExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDynamicNavisionExport.Click, lnkDynamicNavisionPaymentPostDB.Click
        Dim blnIsSQLPosting As Boolean = False
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), Me)
                dtpPostingDate.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            objBankPaymentList._Loginemployeeunkid = 0
            objBankPaymentList._Isweb = True
            objBankPaymentList._FormName = mstrModuleName
            objBankPaymentList._ClientIP = CStr(Session("IP_ADD"))
            objBankPaymentList._HostName = CStr(Session("HOST_NAME"))
            objBankPaymentList._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objBankPaymentList._LoginModeId = enLogin_Mode.MGR_SELF_SERVICE

            If CType(sender, LinkButton).ID = lnkDynamicNavisionPaymentPostDB.ID Then
                Dim strBatchNo As String = ""
                Dim objPaymentPosting As New clsPaymentPosting_Tran
                If objPaymentPosting.isExist(CInt(cboPeriod.SelectedValue), str_BatchNo:=strBatchNo) = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 38, "Sorry, Posting is already done for selected period."), Me)
                    Exit Try
                End If
                objPaymentPosting = Nothing
            End If


            If CType(sender, LinkButton).ID = lnkDynamicNavisionPaymentPostDB.ID Then
                blnIsSQLPosting = True
            Else
                blnIsSQLPosting = False
            End If

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.Dynamics_Navision_Report(Session("Database_Name").ToString, _
                                                           CInt(Session("UserId")), _
                                                           CInt(Session("Fin_year")), _
                                                           CInt(Session("CompanyUnkId")), _
                                                           objPeriod._Start_Date, _
                                                           objPeriod._End_Date, _
                                                           Session("UserAccessModeSetting").ToString, True, _
                                                           CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                           Session("fmtCurrency").ToString, _
                                                           Session("SQLDataSource").ToString, _
                                                           Session("SQLDatabaseName").ToString, _
                                                           Session("SQLDatabaseOwnerName").ToString, _
                                                           Session("SQLUserName").ToString, _
                                                           Session("SQLUserPassword").ToString, _
                                                           blnIsSQLPosting, _
                                                           Session("FinancialYear_Name").ToString() _
                                                           ) = True Then


                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                    Export.Show()
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Data Posted Successfuly."), Me)

                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (29 Mar 2023) -- End  
    'Hemant (26 May 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
    Private Sub lnkEFTGenericCSVPostWeb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkEFTGenericCSVPostWeb.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsCustomColumnsList As DataSet
        Dim strEFTCustomColumnsIds As String = String.Empty
        Dim strDateFormat As String = String.Empty
        Dim strError As String = ""
        Dim strPostedData As String = ""
        Dim strResponseData As String = ""
        Dim dtFinalTable = New DataTable("Batch")
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            End If

            Dim dCol As DataColumn

            dCol = New DataColumn("Batch")
            dCol.Caption = "Batch"
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Status")
            dCol.Caption = "Status"
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Export", Type.GetType("System.String"))
            dCol.Caption = "Export"
            dtFinalTable.Columns.Add(dCol)

            dCol = New DataColumn("Reconciliation", Type.GetType("System.String"))
            dCol.Caption = "Reconciliation"
            dtFinalTable.Columns.Add(dCol)

            dsCustomColumnsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList, enEFT_Export_Mode.CSV, CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            Dim drCustomColumnsList() As DataRow = dsCustomColumnsList.Tables(0).Select("headtypeid = 0 ")
            If drCustomColumnsList.Length > 0 Then
                strEFTCustomColumnsIds = drCustomColumnsList(0).Item("transactionheadid").ToString()
            End If
            Dim drDateFormat() As DataRow = dsCustomColumnsList.Tables(0).Select("headtypeid = 5 ")
            If drDateFormat.Length > 0 Then
                strDateFormat = drDateFormat(0).Item("transactionheadid").ToString()
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            mdsGenericCSVPostWeb = objBankPaymentList.Get_EFT_Generic_CSV_Post_Data(Session("Database_Name").ToString, _
                                                                                    CInt(Session("UserId")), _
                                                                                    CInt(Session("Fin_year")), _
                                                                                    CInt(Session("CompanyUnkId")), _
                                                                                    objPeriod._Start_Date, _
                                                                                    objPeriod._End_Date, _
                                                                                    Session("UserAccessModeSetting").ToString, _
                                                                                    True, CBool(Session("IsIncludeInactiveEmp")), _
                                                                                    True, _
                                                                                    Session("fmtCurrency").ToString, _
                                                                                    mintEFTMembershipUnkId _
                                                                                    )

            Dim dtvoucherno As DataTable = mdsGenericCSVPostWeb.Tables(0).DefaultView.ToTable(True, "voucherno")
            For Each drvoucher As DataRow In dtvoucherno.Rows
                'Hemant (21 Jul 2023) -- Start
                'ENHANCEMENT : A1X-1107 - Payment Gateway - Pass the total net pay going through mobile in one tranche on the gateway payload
                Dim decTotalAmount As Decimal = (From p In mdsGenericCSVPostWeb.Tables(0) Where p.Item("voucherno") = CStr(drvoucher.Item("voucherno")) And p.Item("payment_typeid") = CInt(enBankPaymentType.MobileNumber) Select (CDec(p.Item("Amount")))).Sum()
                If decTotalAmount > 0 AndAlso CInt(Session("EFTMobileMoneyBank")) > 0 AndAlso CInt(Session("EFTMobileMoneyBranch")) > 0 AndAlso Session("EFTMobileMoneyAccountName").Trim.Length > 0 AndAlso Session("EFTMobileMoneyAccountNo").Trim.Length > 0 Then
                    Dim objPayrollGroupMaster As New clspayrollgroup_master
                    Dim objBankbBranchMaster As New clsbankbranch_master
                    objPayrollGroupMaster._Groupmasterunkid = CInt(Session("EFTMobileMoneyBank"))
                    objBankbBranchMaster._Branchunkid = CInt(Session("EFTMobileMoneyBranch"))
                    Dim drMobileMoney As DataRow = mdsGenericCSVPostWeb.Tables(0).NewRow
                    drMobileMoney.Item("employeeunkid") = "-1"
                    drMobileMoney.Item("EmpCode") = "000000"
                    drMobileMoney.Item("EmpName") = Session("EFTMobileMoneyAccountName").ToString
                    drMobileMoney.Item("EMP_BGCode") = objPayrollGroupMaster._Groupcode
                    drMobileMoney.Item("EMP_BankName") = objPayrollGroupMaster._Groupname
                    drMobileMoney.Item("EMP_BRCode") = objBankbBranchMaster._Branchcode
                    drMobileMoney.Item("EMP_SwiftCode") = objPayrollGroupMaster._Swiftcode
                    drMobileMoney.Item("EMP_BranchName") = objBankbBranchMaster._Branchname
                    drMobileMoney.Item("EMP_SortCode") = objBankbBranchMaster._Sortcode
                    drMobileMoney.Item("EMP_AccountNo") = Session("EFTMobileMoneyAccountNo").ToString
                    drMobileMoney.Item("EMP_BGID") = CInt(Session("EFTMobileMoneyBank"))
                    drMobileMoney.Item("EMP_BRID") = CInt(Session("EFTMobileMoneyBranch"))
                    drMobileMoney.Item("currency_sign") = mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("currency_sign")
                    drMobileMoney.Item("Amount") = Format(decTotalAmount, ConfigParameter._Object._CurrencyFormat).Replace(",", "")
                    drMobileMoney.Item("CMP_BGID") = mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BGID")
                    drMobileMoney.Item("CMP_BRID") = mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BRID")
                    drMobileMoney.Item("CMP_BGCODE") = mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BGCODE")
                    drMobileMoney.Item("CMP_BankName") = mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BankName")
                    drMobileMoney.Item("CMP_BRCode") = mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BRCode")
                    drMobileMoney.Item("CMP_BranchName") = mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BranchName")
                    drMobileMoney.Item("CMP_Account") = mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_Account")
                    drMobileMoney.Item("CMP_SortCode") = mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_SortCode")
                    drMobileMoney.Item("CMP_SwiftCode") = mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_SwiftCode")
                    drMobileMoney.Item("voucherno") = drvoucher.Item("voucherno")
                    drMobileMoney.Item("periodunkid") = mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("periodunkid")
                    drMobileMoney.Item("paidcurrencyid") = mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("paidcurrencyid")
                    drMobileMoney.Item("payment_typeid") = CInt(enBankPaymentType.BankAccount)
                    drMobileMoney.Item("dpndtbeneficetranunkid") = 0
                    mdsGenericCSVPostWeb.Tables(0).Rows.Add(drMobileMoney)
                    mdsGenericCSVPostWeb.Tables(0).AcceptChanges()
                    objPayrollGroupMaster = Nothing
                    objBankbBranchMaster = Nothing
                End If
                'Hemant (21 Jul 2023) -- End
                Dim strbatchSignature As String = String.Empty

                Dim drVoucherData() As DataRow = mdsGenericCSVPostWeb.Tables(0).Select("voucherno = '" & drvoucher.Item("voucherno").ToString & "' " & " AND payment_typeid = " & CInt(enBankPaymentType.BankAccount) & "")
                strbatchSignature = Session("EFTRequestClientID").ToString() & drvoucher.Item("voucherno").ToString() & drVoucherData.Length & mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_Account").ToString() & mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BGCode").ToString() & CDate(ConfigParameter._Object._CurrentDateAndTime).ToString("yyyyMMdd")
                Dim strJSON As String = GenarateJSONGenericEFT(drvoucher.Item("voucherno").ToString(), drVoucherData.Length, Session("CompanyCode").ToString(), CDate(ConfigParameter._Object._CurrentDateAndTime).ToString("yyyyMMdd"), _
                                                              drVoucherData, mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_Account").ToString(), mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BankName").ToString(), mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BGCode").ToString(), _
                                                              mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_SortCode").ToString(), mdsGenericCSVPostWeb.Tables(0).Rows(0).Item("currency_sign").ToString(), SignData(strbatchSignature), strEFTCustomColumnsIds, ConfigParameter._Object._CurrencyFormat, _
                                                              CInt(cboPeriod.SelectedValue), strDateFormat)

                Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(Session("EFTRequestClientID").ToString() & ":" & Session("EFTRequestPassword").ToString())
                Dim strBase64Credential As String = Convert.ToBase64String(byt)


                Dim strMessage = objBankPaymentList.GetSetPaymentAPIRequest(Session("EFTRequestURL").ToString(), strBase64Credential, strJSON, strError, strPostedData)

                If strMessage IsNot Nothing Then
                    Dim drRow As DataRow = dtFinalTable.NewRow
                    drRow.Item("Batch") = drvoucher.Item("voucherno").ToString()
                    drRow.Item("Status") = strMessage("statusCode").ToString() & " : " & strMessage("message").ToString()
                    drRow.Item("Export") = "Export"
                    drRow.Item("Reconciliation") = "Reconciliation Query"
                    dtFinalTable.Rows.Add(drRow)

                    If strMessage("statusCode").ToString() = "4000" Then
                        Dim objPaymentBatchReconciliationTran As New clspaymentbatchreconciliation_tran
                        objPaymentBatchReconciliationTran._TranDataTable = drVoucherData.CopyToDataTable
                        objPaymentBatchReconciliationTran._BatchName = drvoucher.Item("voucherno").ToString()
                        objPaymentBatchReconciliationTran._BatchPostedDateTime = ConfigParameter._Object._CurrentDateAndTime
                        objPaymentBatchReconciliationTran._Isvoid = False
                        objPaymentBatchReconciliationTran._Voiduserunkid = -1
                        objPaymentBatchReconciliationTran._Voiddatetime = Nothing
                        objPaymentBatchReconciliationTran._Voidreason = ""
                        If objPaymentBatchReconciliationTran.InsertAll() = False And objPaymentBatchReconciliationTran._Message <> "" Then
                            DisplayMessage.DisplayMessage(objPaymentBatchReconciliationTran._Message, Me)
                            Exit Sub
                        End If
                        objPaymentBatchReconciliationTran = Nothing
                    End If

                End If

                'Dim drRow1 As DataRow = dtFinalTable.NewRow
                'drRow1.Item("Batch") = drvoucher.Item("voucherno").ToString()
                'drRow1.Item("Status") = " 4000: Successfull Posted "
                'drRow1.Item("Export") = "Export"
                'drRow1.Item("Reconciliation") = "Reconciliation Query"
                'dtFinalTable.Rows.Add(drRow1)
            Next

            dgBatch.DataSource = dtFinalTable
            dgBatch.DataBind()

            pnlGenericPanel.Visible = True
            mblnpopupReconciliation = False
            popupReconciliation.Hide()
            mblnpopupBatchApproval = False
            popupBatchApproval.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub lnkEFTPaymentBatchApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkEFTPaymentBatchApproval.Click
        Try
            mblnIsFromGrid = False
            mblnpopupReconciliation = False
            popupReconciliation.Hide()
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            dgvBatchApprovalList.DataSource = Nothing
            dgvBatchApprovalList.DataBind()
            'Hemant (21 Jul 2023) -- End
            Call FillBatchApprovalCombo()
            mblnpopupBatchApproval = True

            popupBatchApproval.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub lnkEFTPaymentBatchReconciliation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkEFTPaymentBatchReconciliation.Click
        Try
            mblnIsFromGrid = False
            mblnpopupBatchApproval = False
            popupBatchApproval.Hide()
            Call FillReconciliationCombo()
            mstrOldBatchName = ""
            mblnpopupReconciliation = True
            popupReconciliation.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cbodgcolhBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cbodgcolhBranch As DropDownList = CType(sender, DropDownList)
            Dim item As DataGridItem = CType(cbodgcolhBranch.NamingContainer, DataGridItem)
            If CInt(cbodgcolhBranch.SelectedValue) > 0 Then
                Dim intBankGroupid As Integer = -1
                Dim objBranch As New clsbankbranch_master
                Dim objBankGroup As New clspayrollgroup_master
                objBranch._Branchunkid = CInt(cbodgcolhBranch.SelectedValue)
                intBankGroupid = CInt(objBranch._Bankgroupunkid)
                objBankGroup._Groupmasterunkid = intBankGroupid
                item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvDataList, "dgcolhBankGroup", False, True)).Text = objBankGroup._Groupname
            Else
                item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvDataList, "dgcolhBankGroup", False, True)).Text = ""
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (26 May 2023) -- End
    'Hemant (12 May 2023) -- Start
    'ENHANCEMENT : A1X-881 - Maj Consulting - Bank of Kigali CSV EFT
    Protected Sub lnkEFTBankOfKigali_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTBankOfKigali.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\BankOfKigali.csv"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_Bank_Of_Kigali(Session("Database_Name").ToString, _
                                                   CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   objPeriod._Start_Date, _
                                                   objPeriod._End_Date, _
                                                   Session("UserAccessModeSetting").ToString, True, _
                                                   CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                   Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = mstrPath
                Export.Show()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (12 May 2023) -- End
    'Hemant (12 May 2023) -- Start
    'ENHANCEMENT : A1X-882 - Maj Consulting - NCBA EFT
    Protected Sub lnkEFT_NCBA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_NCBA.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\NCBA.txt"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_NCBA(Session("Database_Name").ToString, _
                                           CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           objPeriod._Start_Date, _
                                           objPeriod._End_Date, _
                                           Session("UserAccessModeSetting").ToString, True, _
                                           CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                           Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = mstrPath
                Export.Show()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (12 May 2023) -- End
    'Hemant (07 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1034 - NBC Bank payment EFT
    Protected Sub lnkEFT_NBC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_NBC.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Please set Posting Date."), Me)
                dtpPostingDate.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            Dim strConfigExpPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\EFT_NBC.xlsx"

            If objBankPaymentList.EFT_NBC_Report(Session("Database_Name").ToString, _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 objPeriod._Start_Date, _
                                                 objPeriod._End_Date, _
                                                 Session("UserAccessModeSetting").ToString, True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), True, strConfigExpPath, _
                                                 Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = strConfigExpPath
                Export.Show()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Hemant (07 Jul 2023) -- End
#End Region

#Region "CheckBox Event"

    Protected Sub ChkShowGroupByBankBranch_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowGroupByBankBranch.CheckedChanged
        Try
            objBankPaymentList._ShowGroupByBankBranch = chkShowGroupByBankBranch.Checked
            Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
        Catch ex As Exception
            DisplayMessage.DisplayError("ChkShowGroupByBankBranch_CheckedChanged:- " & ex.Message, Me)
        End Try
    End Sub

    'Sohail (01 Apr 2014) -- Start
    'ENHANCEMENT - 
    Protected Sub chkShowEmployeeCode_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowEmployeeCode.CheckedChanged
        Try
            objBankPaymentList._ShowEmployeeCode = chkShowEmployeeCode.Checked
            If chkShowGroupByBankBranch.Checked = True OrElse (CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.Electronic_Fund_Transfer AndAlso CInt(Session("Compcountryid")) = 162) Then
                objBankPaymentList.Create_OnDetailReportWithoutBankBranch()
            Else
                objBankPaymentList.Create_OnDetailReport()
            End If
            If CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.Electronic_Fund_Transfer _
               AndAlso CInt(Session("Compcountryid")) = 162 Then 'OMAN EFT 
                Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.EFT_VOLTAMP)
            Else
            Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("chkShowEmployeeCode_CheckedChanged:- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (01 Apr 2014) -- End


    'SHANI [13 Mar 2015]-START
    'Enhancement - New EFT Report Custom CSV Report.
    Public Sub lvEFTCustomColumns_ItemChecked(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Try
            If chk Is Nothing Then Exit Try
            If lvEFTCustomColumns.Rows.Count <= 0 Then Exit Sub
            Select Case chk.ToolTip.ToUpper
                Case "ALL"
                    For Each dgvRow As GridViewRow In lvEFTCustomColumns.Rows
                        Dim cb As CheckBox = CType(lvEFTCustomColumns.Rows(dgvRow.RowIndex).FindControl("chkSelect"), CheckBox)
                        cb.Checked = chk.Checked
                    Next
                    dtEFTTable.AsEnumerable().Cast(Of DataRow).ToList.ForEach(Function(x) CheckUncheckAll(x, chk.Checked))
                    dtEFTTable.AcceptChanges()

                    'Sohail (13 Apr 2016) -- Start
                    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                    If chk.Checked = True Then
                        cboMembership.Enabled = True
                    Else
                        cboMembership.SelectedValue = "0"
                        cboMembership.Enabled = False
                    End If
                    'Sohail (13 Apr 2016) -- End
                    'Hemant (02 Jul 2020) -- Start
                    'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                    If chk.Checked = True Then
                        txtDateFormat.Enabled = True
                    Else
                        txtDateFormat.Text = "ddMMyyyy"
                        txtDateFormat.Enabled = False
                    End If
                    'Hemant (02 Jul 2020) -- End
                Case "CHECKED"
                    Dim gvRow As GridViewRow = CType(chk.NamingContainer, GridViewRow)
                    Dim xRow() As DataRow = dtEFTTable.Select("ID=" & dtEFTTable.Rows(gvRow.RowIndex)("Id").ToString)
                    If xRow.Length > 0 Then
                        xRow(0).Item("IsChecked") = chk.Checked
                        xRow(0).AcceptChanges()
                    End If

                    'Sohail (13 Apr 2016) -- Start
                    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                    If CInt(dtEFTTable.Rows(gvRow.RowIndex).Item("Id").ToString) = enEFT_EFT_Custom_Columns.EMP_MEMBERSHIP_NO Then
                        If chk.Checked = True Then
                            cboMembership.Enabled = True
                        Else
                            cboMembership.SelectedValue = "0"
                            cboMembership.Enabled = False
                        End If
                    End If
                    'Sohail (13 Apr 2016) -- End

                    'Hemant (02 Jul 2020) -- Start
                    'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                    If CInt(dtEFTTable.Rows(gvRow.RowIndex).Item("Id").ToString) = enEFT_EFT_Custom_Columns.PAYMENT_DATE Then
                        If chk.Checked = True Then
                            txtDateFormat.Enabled = True
                        Else
                            txtDateFormat.Text = "ddMMyyyy"
                            txtDateFormat.Enabled = False
                        End If
                    End If
                    'Hemant (02 Jul 2020) -- End

            End Select
            marrEFTCustomColumnIds.AddRange((From p In dtEFTTable Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("ID").ToString)).ToArray)


        Catch ex As Exception
            DisplayMessage.DisplayError("lvEFTCustomColumns_ItemChecked:- " & ex.Message, Me)
        Finally
            popup_EFTCustom.Show()
        End Try
    End Sub

    Private Function CheckUncheckAll(ByVal x As DataRow, ByVal blnCheckAll As Boolean) As Boolean
        Try
            x.Item("ischecked") = blnCheckAll
            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "CheckUncheckAll", mstrModuleName)
            DisplayMessage.DisplayError("Procedure : CheckUncheckAll : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function
    'SHANI [13 Mar 2015]--END 

#End Region

#Region "Button Event"

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 1, "Period is mandatory information.Please select Period."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is mandatory information.Please select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Sub
            End If

            If Not SetFilter() Then Exit Sub

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 58.1 changes in Web.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Jan 2016) -- End

            'Sohail (15 Mar 2014) -- Start
            'Enhancement - Grouping on cut off amount.
            'objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            'objBankPaymentList.generateReport(CInt(CType(cboReportType.SelectedItem, ListItem).Value), enPrintAction.None, enExportAction.None)
            If cboReportType.SelectedIndex = 1 AndAlso CInt(Session("Compcountryid")) = 162 Then 'OMAN EFT
                Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.Bank_payment_List)
                'Sohail (21 Jan 2016) -- Start
                'Enhancement - 58.1 changes in Web.
                'objBankPaymentList.generateReport(enBankPaymentReport.EFT_VOLTAMP, enPrintAction.None, enExportAction.None)
                objBankPaymentList.generateReportNew(Session("Database_Name").ToString, _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       objPeriod._Start_Date, _
                                                       objPeriod._End_Date, _
                                                       Session("UserAccessModeSetting").ToString, True, _
                                                       Session("ExportReportPath").ToString, _
                                                       CBool(Session("OpenAfterExport")), _
                                                       enBankPaymentReport.EFT_VOLTAMP, _
                                                       enPrintAction.None, enExportAction.None, _
                                                       CInt(Session("Base_CurrencyId")))
                'Sohail (21 Jan 2016) -- End

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                'Sohail (12 Feb 2018) -- Start
                'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                'ElseIf CInt(cboReportType.SelectedValue) = enBankPaymentReport.Bank_Payment_Voucher Then 'BANK PAYMENT VOUCHER REPORT
            ElseIf CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Bank_Payment_Voucher) Then 'BANK PAYMENT VOUCHER REPORT
                'Sohail (12 Feb 2018) -- End
                objBankPaymentList._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Session("ExportReportPath") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
                'Pinkal (16-Apr-2016) -- End

                objBankPaymentList._OpenAfterExport = False

                Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.Bank_Payment_Voucher)
                'Sohail (21 Jan 2016) -- Start
                'Enhancement - 58.1 changes in Web.
                'objBankPaymentList.generateReport(enBankPaymentReport.Bank_Payment_Voucher, enPrintAction.None, enExportAction.ExcelExtra)
                objBankPaymentList.generateReportNew(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               objPeriod._Start_Date, _
                                                               objPeriod._End_Date, _
                                                               Session("UserAccessModeSetting").ToString, True, _
                                                               Session("ExportReportPath").ToString, _
                                                               CBool(Session("OpenAfterExport")), _
                                                               enBankPaymentReport.Bank_Payment_Voucher, enPrintAction.None, enExportAction.ExcelExtra, _
                                                               CInt(Session("Base_CurrencyId")))
                'Sohail (21 Jan 2016) -- End

                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objBankPaymentList._FileNameAfterExported
                    'Shani [ 10 DEC 2014 ] -- START
                    'Issue : Chrome is not supporting ShowModalDialog option now."
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End

                    'Shani [ 10 DEC 2014 ] -- END
                End If
                'Pinkal (12-Jun-2014) -- End

                'Sohail (12 Feb 2018) -- Start
                'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            ElseIf CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Salary_Format_WPS) Then 'Salary Format WPS

                objBankPaymentList._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

                'Sohail (24 Mar 2018) -- Start
                'Voltamp Enhancement - 70.2 - Changes in WPS report as per discussion with Prabhakar.
                'Session("ExportReportPath") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
                'Hemant (16 Jul 2020) -- Start
                'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
                If chkSaveAsTXT_WPS.Checked = True Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\SalaryFormatWPS.txt"
                Else
                    'Hemant (16 Jul 2020) -- End
                    'Hemant (25 Sep 2020) -- Start
                    'Enhancement (Allan Comments On JIRA) : This client wants this EFT in XLs and not CSV as provided.  Please assist. He miscommunicated this bit.
                    'Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\SalaryFormatWPS.csv"
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
                    'Hemant (25 Sep 2020) -- End
                End If 'Hemant (16 Jul 2020)
                'Sohail (24 Mar 2018) -- End
                objBankPaymentList._OpenAfterExport = False

                Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.Salary_Format_WPS)

                'Sohail (16 Apr 2018) -- Start
                'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
                'If objBankPaymentList.Generate_Salary_Format_WPS_Report(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, Session("UserAccessModeSetting").ToString, True, False, CInt(enBankPaymentReport.Salary_Format_WPS), True, Session("fmtCurrency").ToString, Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport"))) = True Then
                If objBankPaymentList.Generate_Salary_Format_WPS_Report(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, Session("UserAccessModeSetting").ToString, True, False, CInt(enBankPaymentReport.Salary_Format_WPS), True, Session("fmtCurrency").ToString, Session("ExFileName").ToString, CBool(Session("OpenAfterExport")), chkSaveAsTXT_WPS.Checked) = True Then
                    'Hemant (25 Sep 2020) -- [chkSaveAsTXT.Checked]
                    'Sohail (16 Apr 2018) -- End

                    'Sohail (24 Mar 2018) -- Start
                    'Voltamp Enhancement - 70.2 - Changes in WPS report as per discussion with Prabhakar.
                    'If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    '    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objBankPaymentList._FileNameAfterExported
                    '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    'End If
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End


                    'Sohail (24 Mar 2018) -- End

                End If
                'Hemant (25 Sep 2020) -- Start
                'Enhancement (Allan Comments On JIRA) : This client wants this EFT in XLs and not CSV as provided.  Please assist. He miscommunicated this bit.
                If chkSaveAsTXT.Checked = False Then
                    If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                        Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objBankPaymentList._FileNameAfterExported
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    End If
                End If
                'Hemant (25 Sep 2020) -- End

                'Sohail (12 Feb 2018) -- End

            Else
            objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
                'Sohail (21 Jan 2016) -- Start
                'Enhancement - 58.1 changes in Web.
                'objBankPaymentList.generateReport(CInt(CType(cboReportType.SelectedItem, ListItem).Value), enPrintAction.None, enExportAction.None)
                objBankPaymentList.generateReportNew(Session("Database_Name").ToString, _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       objPeriod._Start_Date, _
                                                       objPeriod._End_Date, _
                                                       Session("UserAccessModeSetting").ToString, True, _
                                                       Session("ExportReportPath").ToString, _
                                                       CBool(Session("OpenAfterExport")), _
                                                       CInt(CType(cboReportType.SelectedItem, ListItem).Value), enPrintAction.None, enExportAction.None, _
                                                       CInt(Session("Base_CurrencyId")))
                'Sohail (21 Jan 2016) -- End
            End If
            'Sohail (15 Mar 2014) -- End

            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            'If CInt(cboReportType.SelectedValue) <> enBankPaymentReport.Bank_Payment_Voucher Then 'BANK PAYMENT VOUCHER REPORT
            'Sohail (24 Mar 2018) -- Start
            'Voltamp Enhancement - 70.2 - Changes in WPS report as per discussion with Prabhakar.
            'If CInt(CType(cboReportType.SelectedItem, ListItem).Value) <> CInt(enBankPaymentReport.Bank_Payment_Voucher) Then 'BANK PAYMENT VOUCHER REPORT
            If Not (CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Bank_Payment_Voucher) OrElse CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Salary_Format_WPS)) Then 'BANK PAYMENT VOUCHER REPORT
                'Sohail (24 Mar 2018) -- End
                'Sohail (12 Feb 2018) -- End
            Session("objRpt") = objBankPaymentList._Rpt
                Response.Redirect("../Aruti Report Structure/Report.aspx", False)
            End If
            'Pinkal (12-Jun-2014) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("btnReport_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnReset_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.ViewState("FirstOpenPeriod") = Nothing
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
        End Try
    End Sub


    'SHANI [13 Mar 2015]-START
    'Enhancement - New EFT Report Custom CSV Report.
    Protected Sub btnEFTSaveSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEFTSaveSelection.Click
        Try
            If IsEFTValidate() = False Then Exit Sub
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            'For i As Integer = 0 To 2
            For i As Integer = 0 To GetEnumHighestValue(Of enReportMode)()
                'Hemant (27 June 2019) -- End
                objUserDefRMode = New clsUserDef_ReportMode
                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.BankPaymentList
                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                'objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reporttypeid = CInt(CType(cboReportType.SelectedItem, ListItem).Value)
                'Sohail (25 Mar 2015) -- End
                objUserDefRMode._Reportmodeid = mintModeId

                If i = enReportMode.CUSTOM_COLUMNS Then 'Custom Columns


                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mstrEFTCustomColumnsIds
                    'Sohail (25 Mar 2015) -- Start
                    'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                    'intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, mintModeId, i)
                    intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, CInt(CType(cboReportType.SelectedItem, ListItem).Value), mintModeId, i)
                    'Sohail (25 Mar 2015) -- End


                ElseIf i = enReportMode.SHOW_REPORT_HEADER Then 'Show Column Header
                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mblnShowColumnHeader.ToString
                    'Sohail (25 Mar 2015) -- Start
                    'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                    'intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, mintModeId, i)
                    intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, CInt(CType(cboReportType.SelectedItem, ListItem).Value), mintModeId, i)
                    'Sohail (25 Mar 2015) -- End

                    'Sohail (13 Apr 2016) -- Start
                    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                ElseIf i = enReportMode.MEMBERSHIP Then

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mintMembershipUnkId.ToString

                    intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, CInt(CType(cboReportType.SelectedItem, ListItem).Value), mintModeId, i)
                    'Sohail (13 Apr 2016) -- End

                    'Hemant (27 June 2019) -- Start
                    'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                ElseIf i = enReportMode.SAVE_AS_TXT Then 'SAVE AS TXT

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mblnSaveAsTXT.ToString

                    intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, CInt(CType(cboReportType.SelectedItem, ListItem).Value), mintModeId, i)

                ElseIf i = enReportMode.TAB_DELIMITER Then 'TAB DELIMITER

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mblnTabDelimiter.ToString

                    intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, CInt(CType(cboReportType.SelectedItem, ListItem).Value), mintModeId, i)
                    'Hemant (27 June 2019) -- End

                    'Hemant (02 Jul 2020) -- Start
                    'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                ElseIf i = enReportMode.DATE_FORMAT Then 'DATE FORMAT

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mstrDateFormat.ToString

                    intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, CInt(CType(cboReportType.SelectedItem, ListItem).Value), mintModeId, i)
                    'Hemant (02 Jul 2020) -- End

                End If
                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
            Next
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Information Successfully Saved."), Me)
            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "Information Successfully Saved."), Me)
            'Hemant (27 June 2019) -- End

            Call Export_CSV_XLS()
        Catch ex As Exception
            popup_EFTCustom.Show()
            DisplayMessage.DisplayError("btnEFTSaveSelection_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEFTOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEFTOK.Click
        Try
            Call Export_CSV_XLS()
        Catch ex As Exception
            popup_EFTCustom.Show()
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayError(" :" & ex.Message, Me)
            DisplayMessage.DisplayError("btnEFTOK_Click :" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End

        End Try
    End Sub
    'SHANI [13 Mar 2015]--END 

    'Hemant (26 May 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
    Protected Sub btnReconciliation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReconciliation.Click
        Dim objBankPaymentList As New clsBankPaymentList(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Dim objPaymentBatchPostingTran As New clspaymentbatchposting_tran
        Dim objPaymentBatchReconciliationTran As New clspaymentbatchreconciliation_tran
        Dim strError As String = ""
        Dim strPostedData As String = ""
        Try

            If CInt(cboRBatch.SelectedValue) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName3, 8, "Please select atleast one Batch for Reconciliation process."), Me)
                cboRBatch.Focus()
                Exit Sub
            End If

            Dim dsRenciliationData As DataSet = objPaymentBatchReconciliationTran.GetList("List", cboRBatch.SelectedItem.Text)
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            If dsRenciliationData.Tables(0).Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName3, 9, "Sorry, Selected Batch is not Posted yet."), Me)
                cboRBatch.Focus()
                Exit Sub
            End If
            'Hemant (21 Jul 2023) -- End

            'Hemant (04 Aug 2023) -- Start
            Dim drRecon() As DataRow = dsRenciliationData.Tables(0).Select("reconciliation_date IS NOT NULL OR STATUS <> ''")
            If drRecon.Length > 0 Then
                popupReconciliationYesNo.Message = Language.getMessage(mstrModuleName3, 16, "Reconciliation has been already done for Selected Batch, Are you sure you want to Reconciliation again for Selected Batch?")
                popupReconciliationYesNo.Title = "Aruti"
                popupReconciliationYesNo.Show()
            Else
                Call ReconciliationProcess()
            End If
            'Hemant (04 Aug 2023) -- End

            'Hemant (04 Aug 2023) -- Start
            'Dim dsBatchData As DataSet
            'mdtReconciliationData = objPaymentBatchPostingTran._TranDataTable
            'If CInt(cboRBatch.SelectedValue) < 0 Then
            '    Dim intEFTMembershipUnkId As Integer = 0
            '    Dim objPeriod As New clscommom_period_Tran
            '    objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboRPeriod.SelectedValue)
            '    objBankPaymentList._PeriodId = CStr(cboRPeriod.SelectedValue)
            '    objBankPaymentList._BankId = CInt(cboBankName.SelectedValue)
            '    objBankPaymentList._BranchId = CInt(cboBranchName.SelectedValue)
            '    objBankPaymentList._CompanyBranchId = CInt(cboCompanyBranchName.SelectedValue)
            '    objBankPaymentList._CompanyBankAccountNo = cboCompanyAccountNo.SelectedItem.Text
            '    objBankPaymentList._CurrencyId = CInt(cboCurrency.SelectedValue)
            '    objBankPaymentList._ReportModeId = CInt(cboReportMode.SelectedIndex)
            '    dsBatchData = objBankPaymentList.Get_EFT_Generic_CSV_Post_Data(Session("Database_Name").ToString, _
            '                                                                   CInt(Session("UserId")), _
            '                                                                   CInt(Session("Fin_year")), _
            '                                                                   CInt(Session("CompanyUnkId")), _
            '                                                                   objPeriod._Start_Date, _
            '                                                                   objPeriod._End_Date, _
            '                                                                   Session("UserAccessModeSetting").ToString, _
            '                                                                   True, True, _
            '                                                                   True, _
            '                                                                   Session("fmtCurrency").ToString, _
            '                                                                   intEFTMembershipUnkId, _
            '                                                                   "", _
            '                                                                   " AND (prpayment_tran.voucherno = '" & cboRBatch.SelectedItem.Text & "' OR prglobalvoc_master.globalvocno = '" & cboRBatch.SelectedItem.Text & "') " _
            '                                                                   )
            '    'Hemant (21 Jul 2023) -- Start
            '    'ENHANCEMENT : A1X-1107 - Payment Gateway - Pass the total net pay going through mobile in one tranche on the gateway payload
            '    Dim decTotalAmount As Decimal = (From p In dsBatchData.Tables(0) Where p.Item("voucherno") = CStr(cboRBatch.SelectedItem.Text) And p.Item("payment_typeid") = CInt(enBankPaymentType.MobileNumber) Select (CDec(p.Item("Amount")))).Sum()
            '    If CInt(Session("EFTMobileMoneyBank")) > 0 AndAlso CInt(Session("EFTMobileMoneyBranch")) > 0 AndAlso Session("EFTMobileMoneyAccountName").Trim.Length > 0 AndAlso Session("EFTMobileMoneyAccountNo").Trim.Length > 0 Then
            '        Dim objPayrollGroupMaster As New clspayrollgroup_master
            '        Dim objBankbBranchMaster As New clsbankbranch_master
            '        objPayrollGroupMaster._Groupmasterunkid = CInt(Session("EFTMobileMoneyBank"))
            '        objBankbBranchMaster._Branchunkid = CInt(Session("EFTMobileMoneyBranch"))
            '        Dim drMobileMoney As DataRow = dsBatchData.Tables(0).NewRow
            '        drMobileMoney.Item("employeeunkid") = "-1"
            '        drMobileMoney.Item("EmpCode") = "000000"
            '        drMobileMoney.Item("EmpName") = Session("EFTMobileMoneyAccountName").ToString
            '        drMobileMoney.Item("EMP_BGCode") = objPayrollGroupMaster._Groupcode
            '        drMobileMoney.Item("EMP_BankName") = objPayrollGroupMaster._Groupname
            '        drMobileMoney.Item("EMP_BRCode") = objBankbBranchMaster._Branchcode
            '        drMobileMoney.Item("EMP_SwiftCode") = objPayrollGroupMaster._Swiftcode
            '        drMobileMoney.Item("EMP_BranchName") = objBankbBranchMaster._Branchname
            '        drMobileMoney.Item("EMP_SortCode") = objBankbBranchMaster._Sortcode
            '        drMobileMoney.Item("EMP_AccountNo") = Session("EFTMobileMoneyAccountNo").ToString
            '        drMobileMoney.Item("EMP_BGID") = CInt(Session("EFTMobileMoneyBank"))
            '        drMobileMoney.Item("EMP_BRID") = CInt(Session("EFTMobileMoneyBranch"))
            '        drMobileMoney.Item("currency_sign") = dsBatchData.Tables(0).Rows(0).Item("currency_sign")
            '        drMobileMoney.Item("Amount") = Format(decTotalAmount, ConfigParameter._Object._CurrencyFormat).Replace(",", "")
            '        drMobileMoney.Item("CMP_BGID") = dsBatchData.Tables(0).Rows(0).Item("CMP_BGID")
            '        drMobileMoney.Item("CMP_BRID") = dsBatchData.Tables(0).Rows(0).Item("CMP_BRID")
            '        drMobileMoney.Item("CMP_BGCODE") = dsBatchData.Tables(0).Rows(0).Item("CMP_BGCODE")
            '        drMobileMoney.Item("CMP_BankName") = dsBatchData.Tables(0).Rows(0).Item("CMP_BankName")
            '        drMobileMoney.Item("CMP_BRCode") = dsBatchData.Tables(0).Rows(0).Item("CMP_BRCode")
            '        drMobileMoney.Item("CMP_BranchName") = dsBatchData.Tables(0).Rows(0).Item("CMP_BranchName")
            '        drMobileMoney.Item("CMP_Account") = dsBatchData.Tables(0).Rows(0).Item("CMP_Account")
            '        drMobileMoney.Item("CMP_SortCode") = dsBatchData.Tables(0).Rows(0).Item("CMP_SortCode")
            '        drMobileMoney.Item("CMP_SwiftCode") = dsBatchData.Tables(0).Rows(0).Item("CMP_SwiftCode")
            '        drMobileMoney.Item("voucherno") = cboRBatch.SelectedItem.Text
            '        drMobileMoney.Item("periodunkid") = dsBatchData.Tables(0).Rows(0).Item("periodunkid")
            '        drMobileMoney.Item("paidcurrencyid") = dsBatchData.Tables(0).Rows(0).Item("paidcurrencyid")
            '        drMobileMoney.Item("payment_typeid") = CInt(enBankPaymentType.BankAccount)
            '        drMobileMoney.Item("dpndtbeneficetranunkid") = 0
            '        dsBatchData.Tables(0).Rows.Add(drMobileMoney)
            '        dsBatchData.Tables(0).AcceptChanges()
            '        objPayrollGroupMaster = Nothing
            '        objBankbBranchMaster = Nothing
            '    End If
            '    Dim dtBatch As DataTable = dsBatchData.Tables(0).Select("voucherno = '" & cboRBatch.SelectedItem.Text & "' AND payment_typeid = " & CInt(enBankPaymentType.BankAccount) & " ").CopyToDataTable
            '    dsBatchData.Tables.RemoveAt(0)
            '    dsBatchData.Tables.Add(dtBatch)
            '    'Hemant (21 Jul 2023) -- End
            'Else
            '    dsBatchData = objPaymentBatchPostingTran.GetList("Batch", CInt(cboRBatch.SelectedValue))
            'End If
            'Hemant (04 Aug 2023) -- End

            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            'Dim dsStatus As DataSet = (New clspaymentbatchposting_master).getStatusComboList("List", True)
            'For Each drEmployeeData As DataRow In dsBatchData.Tables(0).Rows
            '    Dim strJSON As String = GenarateJSONReconciliation(drEmployeeData, CInt(cboRBatch.SelectedValue))

            '    Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(Session("EFTRequestClientID").ToString() & ":" & Session("EFTRequestPassword").ToString())
            '    Dim strBase64Credential As String = Convert.ToBase64String(byt)

            '    Dim strMessage = objBankPaymentList.GetSetPaymentAPIRequest(Session("EFTReconciliationURL").ToString(), strBase64Credential, strJSON, strError, strPostedData, True, SignData(strJSON))

            '    If strMessage IsNot Nothing Then
            '        DisplayMessage.DisplayMessage(" Request Response : " & strMessage("statusCode").ToString() & " - " & strMessage("message").ToString(), Me)
            '    End If
            '    'Dim strMessage = "{""statusCode"":4000,""message"":""Success"",""payload"":{""batchId"":""PV13496"",""accountNumber"":""0321101809097"",""amount"":""306500.00"",""status"":""FAILED"",""reference"":null,""timestamp"":""2023-06-06 10:21:46""}}"
            '    Dim ResultJSON = JsonConvert.DeserializeObject(strMessage)
            '    Dim objObject As Object = ResultJSON
            '    Dim strPayload = objObject("payload").ToString
            '    Dim PayloadJSON = JsonConvert.DeserializeObject(strPayload)
            '    Dim strStatus = PayloadJSON("status").ToString()

            '    Dim drRow As DataRow = mdtReconciliationData.NewRow
            '    drRow.Item("IsCheck") = "False"
            '    drRow.Item("employeeunkid") = drEmployeeData.Item("employeeunkid").ToString
            '    If cboRBatch.SelectedValue < 0 Then
            '        drRow.Item("batchname") = drEmployeeData.Item("voucherno").ToString
            '        drRow.Item("employeeCode") = drEmployeeData.Item("empcode").ToString
            '        drRow.Item("branchunkid") = drEmployeeData.Item("EMP_BRID").ToString
            '        drRow.Item("accountNo") = drEmployeeData.Item("EMP_accountNo").ToString
            '    Else
            '        drRow.Item("batchname") = drEmployeeData.Item("batchname").ToString
            '        drRow.Item("employeeCode") = drEmployeeData.Item("employeecode").ToString
            '        drRow.Item("branchunkid") = drEmployeeData.Item("branchunkid").ToString
            '        drRow.Item("accountNo") = drEmployeeData.Item("accountNo").ToString
            '    End If
            '    drRow.Item("status") = strStatus.ToString

            '    drRow.Item("status") = strStatus.ToString
            '    Dim drReconciliation() As DataRow = dsRenciliationData.Tables(0).Select("employeeunkid = " & CInt(drEmployeeData.Item("employeeunkid").ToString) & " AND periodunkid = " & CInt(cboPeriod.SelectedValue) & " AND batchname = '" & cboRBatch.SelectedItem.Text & "' ")
            '    If drReconciliation.Length > 0 Then
            '        Dim drStatus() As DataRow = dsStatus.Tables(0).Select("name = '" & strStatus.ToString & "'")
            '        If drStatus.Length > 0 Then
            '            drReconciliation(0).Item("statusunkid") = CInt(drStatus(0).Item("Id"))
            '        End If
            '        drReconciliation(0).Item("reconciliation_date") = CDate(ConfigParameter._Object._CurrentDateAndTime)
            '        dsRenciliationData.Tables(0).AcceptChanges()
            '    End If

            '    drRow.Item("dpndtbeneficetranunkid") = drEmployeeData.Item("dpndtbeneficetranunkid").ToString
            '    mdtReconciliationData.Rows.Add(drRow)
            'Next
            'Dim decTotalBatchAmount As Decimal = (From p In dsRenciliationData.Tables(0) Select (CDec(p.Item("Amount")))).Sum()
            'Dim strJSON As String = GenarateJSONReconciliation(cboRBatch.SelectedItem.Text, dsRenciliationData.Tables(0).Rows.Count, Session("CompanyCode").ToString(), CDate(dsRenciliationData.Tables(0).Rows(0).Item("batchposted_date")).ToString("yyyyMMdd"), cboCompanyAccountNo.SelectedItem.Text, cboCurrency.SelectedItem.Text, decTotalBatchAmount)
            'Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(Session("EFTRequestClientID") & ":" & Session("EFTRequestPassword"))
            'Dim strBase64Credential As String = Convert.ToBase64String(byt)

            'Dim strMessage = objBankPaymentList.GetSetPaymentAPIRequest(Session("EFTReconciliationURL").ToString(), strBase64Credential, strJSON, strError, strPostedData, True, SignData(strJSON))

            'If strMessage IsNot Nothing Then
            '    eZeeMsgBox.Show(" Request Response : " & strMessage("statusCode").ToString() & " - " & strMessage("message").ToString(), enMsgBoxStyle.Information)
            'End If

            'Dim strMessage = "{""statusCode"":4000,""message"":""Success"",""payload"":{""batchId"":""PV13496"",""accountNumber"":""0321101809097"",""amount"":""306500.00"",""status"":""FAILED"",""reference"":null,""timestamp"":""2023-06-06 10:21:46""}}"
            'Dim strMessage = "{""statusCode"":4000,""message"":""Success"",""payload"":[{""clientReference"":""2106611011000584000001"",""status"":""failed""},{""clientReference"":""210661103000000000001"",""status"":""success""},{""clientReference"":""2106611011000025000003"",""status"":""failed""},{""clientReference"":""2106611021000584000002"",""status"":""success""},{""clientReference"":""210661102000000000003"",""status"":""failed""}]}"
            'Dim strMessage = "{""statusCode"":4000,""message"":""Success"",""payload"":[{""clientReference"": ""2106618011000584000001"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""P"",""bankRef"": null,""paymentStatusDesc"":""Pending"",""resv1"":null,""resv2"":null}, " & _
            '                                                                          "{""clientReference"": ""2106618011000009000002"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""S"",""bankRef"": null,""paymentStatusDesc"":""Success"",""resv1"":null,""resv2"":null}, " & _
            '                                                                          "{""clientReference"": ""2106618011000025000003"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""U"",""bankRef"": null,""paymentStatusDesc"":""Unknown"",""resv1"":null,""resv2"":null}," & _
            '                                                                          "{""clientReference"": ""2106618011000584000004"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""S"",""bankRef"": null,""paymentStatusDesc"":""Submitted"",""resv1"":null,""resv2"":null}, " & _
            '                                                                           "{""clientReference"": ""210661801000000000005"",""transactionType"":""INTERNAL"",""amount"": 50100,""paymentStatus"":""F"",""bankRef"": null,""paymentStatusDesc"":""Failed"",""resv1"":null,""resv2"":null}]}"
            'Dim ResultJSON = JsonConvert.DeserializeObject(strMessage)
            'Dim objObject As Object = ResultJSON
            'Dim strPayload = objObject("payload").ToString
            ''Dim strPayload = strMessage("payload").ToString
            'Dim PayloadJSON = JsonConvert.DeserializeObject(strPayload.Trim)
            'Dim dtResponse As DataTable = JsonConvert.DeserializeObject(Of DataTable)(PayloadJSON.ToString())
            'Hemant (04 Aug 2023) -- Start
            'mdtStatus = dtResponse.DefaultView.ToTable(True, "PaymentStatusDesc")
            'For Each drEmployeeData As DataRow In dsBatchData.Tables(0).Rows
            '    Dim drRow As DataRow = mdtReconciliationData.NewRow
            '    drRow.Item("IsCheck") = "False"
            '    drRow.Item("employeeunkid") = drEmployeeData.Item("employeeunkid").ToString
            '    If cboRBatch.SelectedValue < 0 Then
            '        drRow.Item("batchname") = drEmployeeData.Item("voucherno").ToString
            '        drRow.Item("employeeCode") = drEmployeeData.Item("empcode").ToString
            '        drRow.Item("branchunkid") = drEmployeeData.Item("EMP_BRID").ToString
            '        drRow.Item("accountNo") = drEmployeeData.Item("EMP_accountNo").ToString
            '        drRow.Item("bankname") = drEmployeeData.Item("EMP_BankName").ToString
            '        drRow.Item("periodunkid") = drEmployeeData.Item("periodunkid").ToString
            '    Else
            '        drRow.Item("batchname") = drEmployeeData.Item("batchname").ToString
            '        drRow.Item("employeeCode") = drEmployeeData.Item("employeecode").ToString
            '        drRow.Item("branchunkid") = drEmployeeData.Item("branchunkid").ToString
            '        drRow.Item("accountNo") = drEmployeeData.Item("accountNo").ToString
            '        drRow.Item("bankname") = drEmployeeData.Item("bankname").ToString
            '        drRow.Item("periodunkid") = CInt(cboRPeriod.SelectedValue)
            '    End If

            '    Dim drReconciliation() As DataRow = dsRenciliationData.Tables(0).Select("employeeunkid = " & CInt(drEmployeeData.Item("employeeunkid").ToString) & " AND periodunkid = " & CInt(cboRPeriod.SelectedValue) & " AND batchname = '" & cboRBatch.SelectedItem.Text & "' AND accountno = '" & drRow.Item("accountNo") & "' ")
            '    If drReconciliation.Length > 0 Then
            '        If drReconciliation(0).Item("clientReference").ToString.Trim.Length > 0 Then
            '            Dim drResponse() As DataRow = dtResponse.Select("clientReference = '" & drReconciliation(0).Item("clientReference") & "'")
            '            If drResponse.Length > 0 Then
            '                'Dim drStatus() As DataRow = dsStatus.Tables(0).Select("name = '" & drResponse(0).Item("status").ToString & "'")
            '                'If drStatus.Length > 0 Then
            '                '    drReconciliation(0).Item("statusunkid") = CInt(drStatus(0).Item("Id"))
            '                '    drRow.Item("statusunkid") = CInt(drStatus(0).Item("Id"))
            '                '    drRow.Item("status") = drResponse(0).Item("status").ToString
            '                'End If
            '                drReconciliation(0).Item("status") = drResponse(0).Item("PaymentStatusDesc").ToString
            '                drRow.Item("status") = drResponse(0).Item("PaymentStatusDesc").ToString
            '                drReconciliation(0).Item("reconciliation_date") = CDate(ConfigParameter._Object._CurrentDateAndTime)
            '                dsRenciliationData.Tables(0).AcceptChanges()
                            'End If
            '        End If
                            'End If

            '    drRow.Item("dpndtbeneficetranunkid") = drEmployeeData.Item("dpndtbeneficetranunkid").ToString

            '    mdtReconciliationData.Rows.Add(drRow)
            'Next
            'objPaymentBatchReconciliationTran._TranDataTable = dsRenciliationData.Tables(0)
            'If objPaymentBatchReconciliationTran.UpdateAll() = False And objPaymentBatchReconciliationTran._Message <> "" Then
            '    DisplayMessage.DisplayMessage(objPaymentBatchReconciliationTran._Message, Me)
                            'End If

            'btnReconciliation.Enabled = False
            'Call FillReconciliationList()
            'Call FillStatusCombo(mdtStatus)
            'Hemant (04 Aug 2023) -- End
            'Hemant (21 Jul 2023) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objBankPaymentList = Nothing
            objPaymentBatchPostingTran = Nothing
            objPaymentBatchReconciliationTran = Nothing
        End Try
    End Sub

    Protected Sub btnRSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRSearch.Click
        Try
            Call FillReconciliationList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSubmitForApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitForApproval.Click
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Dim objPaymentBatchPostingTran As New clspaymentbatchposting_tran
        Dim dtPaymentBatchPostingTran As DataTable
        Dim blnFlag As Boolean = False
        Try

            SetValueReconciliation(objPaymentBatchPostingMaster)

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvDataList.Items.Cast(Of DataGridItem)()

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName3, 1, "Atleast one Employee in the list is required for Approval."), Me)
                Exit Sub
            ElseIf objPaymentBatchPostingMaster.IsBatchPostingPendingForApproval(mstrVoucherNo) = True Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName3, 2, "Sorry, You can not submit for Approval. Because Already Batch is submitted for Approval Process"), Me)
                Exit Sub
            End If
            dtPaymentBatchPostingTran = objPaymentBatchPostingTran._TranDataTable

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                For Each dr As DataGridItem In gRow
                    Dim drRow As DataRow = dtPaymentBatchPostingTran.NewRow
                    drRow("batchname") = CStr(dr.Cells(GetColumnIndex.getColumnId_Datagrid(dgvDataList, "dgcolhBatchId", False, True)).Text)
                    drRow("employeeunkid") = CInt(dr.Cells(GetColumnIndex.getColumnId_Datagrid(dgvDataList, "objdgcolhEmployeeUnkid", False, True)).Text)
                    drRow("branchunkid") = CType(dr.FindControl("cbodgcolhBranch"), DropDownList).SelectedValue
                    drRow("accountno") = CType(dr.FindControl("txtdgcolhAccountNo"), TextBox).Text
                    drRow("Isvoid") = False
                    drRow("Voiddatetime") = DBNull.Value
                    drRow("Voidreason") = ""
                    drRow("Voiduserunkid") = -1
                    drRow("dpndtbeneficetranunkid") = CInt(dr.Cells(GetColumnIndex.getColumnId_Datagrid(dgvDataList, "objdgcolhDpndtBeneficeTranUnkid", False, True)).Text)
                    dtPaymentBatchPostingTran.Rows.Add(drRow)
                Next
            End If

            objPaymentBatchPostingMaster._BatchPostingEmpTran = dtPaymentBatchPostingTran
            blnFlag = objPaymentBatchPostingMaster.Insert()

            If blnFlag = False And objPaymentBatchPostingMaster._Message <> "" Then
                DisplayMessage.DisplayMessage(objPaymentBatchPostingMaster._Message, Me)
            End If

            If blnFlag = True Then
                mblnpopupReconciliation = False
                popupReconciliation.Hide()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPaymentBatchPostingMaster = Nothing
        End Try
    End Sub

    Protected Sub btnASearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnASearch.Click
        Try
            Call FillBatchApprovalList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApproveReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveReject.Click
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Dim blnFlag As Boolean = False
        Dim objEmpBankTran As New clsEmployeeBanks
        Dim objEmpSalaryTran As New clsEmpSalaryTran
        Dim objPaymentBatchPostingTran As New clspaymentbatchposting_tran
        Dim objGarnisheesBankTran As New clsGarnishees_Bank_Tran
        Dim dtPaymentBatchPostingTran As DataTable
        Try
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvBatchApprovalList.Rows.Cast(Of GridViewRow)()

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName4, 1, "Atleast one Employee in the list is required for Approval."), Me)
                Exit Sub
            End If

            Dim xPeriodStart As Date
            Dim xPeriodEnd As Date
            If CInt(cboAPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboAPeriod.SelectedValue)
                xPeriodStart = objPeriod._Start_Date
                xPeriodEnd = objPeriod._End_Date
                objPeriod = Nothing
            Else
                xPeriodStart = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                xPeriodEnd = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
            End If

            dtPaymentBatchPostingTran = objPaymentBatchPostingTran._TranDataTable
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                For Each dr As GridViewRow In gRow
                    Dim drRow As DataRow = dtPaymentBatchPostingTran.NewRow
                    drRow("employeeunkid") = CInt(dgvBatchApprovalList.DataKeys(dr.RowIndex)("employeeunkid").ToString())
                    drRow("branchunkid") = CInt(dgvBatchApprovalList.DataKeys(dr.RowIndex)("branchunkid").ToString())
                    drRow("accountno") = CStr(dgvBatchApprovalList.DataKeys(dr.RowIndex)("accountno").ToString())
                    drRow("Isvoid") = False
                    drRow("Voiddatetime") = DBNull.Value
                    drRow("Voidreason") = ""
                    drRow("Voiduserunkid") = -1
                    drRow("dpndtbeneficetranunkid") = CInt(dgvBatchApprovalList.DataKeys(dr.RowIndex)("dpndtbeneficetranunkid").ToString())
                    dtPaymentBatchPostingTran.Rows.Add(drRow)
                Next
            End If

            Dim dsEmpBankList As DataSet = objEmpBankTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), xPeriodStart, xPeriodEnd, Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), "EmployeeBank", , "", , xPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC", "")

            Dim dsEmpSalaryList As DataSet = objEmpSalaryTran.GetSalaryPaymentData(CInt(cboAPeriod.SelectedValue))
            Dim dsGarnisheeBankList As DataSet = objGarnisheesBankTran.GetList(xPeriodEnd, xPeriodEnd)
            objPaymentBatchPostingMaster._PaymentBatchPostingunkid = CInt(cboABatch.SelectedValue)
            objPaymentBatchPostingMaster._Statusunkid = clspaymentbatchposting_master.enApprovalStatus.Approved
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            objPaymentBatchPostingMaster._CompanyUnkid = CInt(Session("CompanyUnkId"))
            'Hemant (21 Jul 2023) -- End
            blnFlag = objPaymentBatchPostingMaster.UpdateApprovalStatus(dtPaymentBatchPostingTran, dsEmpBankList, dsEmpSalaryList, dsGarnisheeBankList, CInt(cboAPeriod.SelectedValue), ConfigParameter._Object._CurrentDateAndTime)

            If blnFlag = False And objPaymentBatchPostingMaster._Message <> "" Then
                DisplayMessage.DisplayMessage(objPaymentBatchPostingMaster._Message, Me)
            End If

            If blnFlag = True Then
                Dim objConfig As New clsConfigOptions
                objConfig._Companyunkid = Session("Companyunkid")
                Session("EFTMobileMoneyBank") = objConfig._EFTMobileMoneyBank
                Session("EFTMobileMoneyBranch") = objConfig._EFTMobileMoneyBranch
                Session("EFTMobileMoneyAccountName") = objConfig._EFTMobileMoneyAccountName
                Session("EFTMobileMoneyAccountNo") = objConfig._EFTMobileMoneyAccountNo
                objConfig = Nothing
                mblnpopupBatchApproval = False
                popupBatchApproval.Hide()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPaymentBatchPostingMaster = Nothing
            objEmpBankTran = Nothing
            objEmpSalaryTran = Nothing
        End Try
    End Sub

    Protected Sub btnSendRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendRequest.Click
        Dim objBankPaymentList As New clsBankPaymentList(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Dim objPaymentBatchPostingTran As New clspaymentbatchposting_tran
        Dim dtPaymentBatchPostingTran As DataTable
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsCustomColumnsList As DataSet
        Dim strEFTCustomColumnsIds As String = String.Empty
        Dim strDateFormat As String = String.Empty
        Dim intEFTMembershipUnkId As Integer = 0
        Dim strVoucherNo As String = String.Empty
        Dim strError As String = ""
        Dim strPostedData As String = ""
        Dim strResponseData As String = ""
        Try
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            If (New clspaymentbatchreconciliation_tran).GetList("List", cboABatch.SelectedItem.Text).Tables(0).Rows.Count > 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName4, 4, "Sorry, Selected Batch is already Posted."), Me)
                cboABatch.Focus()
                Exit Sub
            End If
            'Hemant (21 Jul 2023) -- End

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvBatchApprovalList.Rows.Cast(Of GridViewRow)()

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Atleast one Employee in the list is required for Send Request.", Me)
                Exit Sub
            End If

            dtPaymentBatchPostingTran = objPaymentBatchPostingTran._TranDataTable
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                For Each dr As GridViewRow In gRow
                    Dim drRow As DataRow = dtPaymentBatchPostingTran.NewRow
                    drRow("batchname") = CStr(dgvBatchApprovalList.DataKeys(dr.RowIndex)("batchname").ToString())
                    drRow("employeeunkid") = CInt(dgvBatchApprovalList.DataKeys(dr.RowIndex)("employeeunkid").ToString())
                    drRow("branchunkid") = CInt(dgvBatchApprovalList.DataKeys(dr.RowIndex)("branchunkid").ToString())
                    drRow("accountno") = CStr(dgvBatchApprovalList.DataKeys(dr.RowIndex)("accountno").ToString())
                    drRow("Isvoid") = False
                    drRow("Voiddatetime") = DBNull.Value
                    drRow("Voidreason") = ""
                    drRow("Voiduserunkid") = -1
                    drRow("dpndtbeneficetranunkid") = CInt(dgvBatchApprovalList.DataKeys(dr.RowIndex)("dpndtbeneficetranunkid").ToString())
                    dtPaymentBatchPostingTran.Rows.Add(drRow)
                Next
            End If

            dsCustomColumnsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList, enEFT_Export_Mode.CSV, 2)
            Dim drCustomColumnsList() As DataRow = dsCustomColumnsList.Tables(0).Select("headtypeid = 0 ")
            If drCustomColumnsList.Length > 0 Then
                strEFTCustomColumnsIds = drCustomColumnsList(0).Item("transactionheadid").ToString()
            End If
            Dim drDateFormat() As DataRow = dsCustomColumnsList.Tables(0).Select("headtypeid = 5 ")
            If drDateFormat.Length > 0 Then
                strDateFormat = drDateFormat(0).Item("transactionheadid").ToString()
            End If

            Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
            If CInt(cboABatch.SelectedValue) > 0 Then
                objPaymentBatchPostingMaster._PaymentBatchPostingunkid = CInt(cboABatch.SelectedValue)
                strVoucherNo = objPaymentBatchPostingMaster._VoucherNo
            End If
            objPaymentBatchPostingMaster = Nothing

            Dim strBatchEmployeelist As String = String.Join(",", (From p In dtPaymentBatchPostingTran Where CInt(p.Item("dpndtbeneficetranunkid")) <= 0 Select (p.Item("employeeunkid").ToString)).ToArray())
            'Hemant (07 Jul 2023) -- Start
            'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
            Dim strDpndtBeneficeTranUnkIDs As String = String.Join(",", (From p In dtPaymentBatchPostingTran Where CInt(p.Item("dpndtbeneficetranunkid")) > 0 Select (p.Item("dpndtbeneficetranunkid").ToString)).ToArray())

            If strBatchEmployeelist.Trim.Length <= 0 Then strBatchEmployeelist = "-1"
            If strDpndtBeneficeTranUnkIDs.Trim.Length <= 0 Then strDpndtBeneficeTranUnkIDs = "-1"
            'Hemant (07 July 2023) -- End
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboAPeriod.SelectedValue)
            objBankPaymentList._PeriodId = CStr(cboAPeriod.SelectedValue)
            objBankPaymentList._BankId = CInt(cboBankName.SelectedValue)
            objBankPaymentList._BranchId = CInt(cboBranchName.SelectedValue)
            objBankPaymentList._CompanyBranchId = CInt(cboCompanyBranchName.SelectedValue)
            objBankPaymentList._CompanyBankAccountNo = cboCompanyAccountNo.SelectedItem.Text
            objBankPaymentList._CurrencyId = CInt(cboCurrency.SelectedValue)
            objBankPaymentList._ReportModeId = CInt(cboReportMode.SelectedIndex)

            Dim dsGenericCSVPostWeb As DataSet = objBankPaymentList.Get_EFT_Generic_CSV_Post_Data(Session("Database_Name").ToString, _
                                                                                                    CInt(Session("UserId")), _
                                                                                                    CInt(Session("Fin_year")), _
                                                                                                    CInt(Session("CompanyUnkId")), _
                                                                                                    objPeriod._Start_Date, _
                                                                                                    objPeriod._End_Date, _
                                                                                                    Session("UserAccessModeSetting").ToString, _
                                                                                                    True, True, _
                                                                                                    True, _
                                                                                                    Session("fmtCurrency").ToString, _
                                                                                                    intEFTMembershipUnkId, _
                                                                                                    strBatchEmployeelist, _
                                                                                                    "", _
                                                                                                    strDpndtBeneficeTranUnkIDs _
                                                                                                  )
            'Hemant (07 Jul 2023) -- [strDpndtBeneficeTranUnkIDs]
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            For Each drMobileMoneyData As DataRow In dtPaymentBatchPostingTran.Select("employeeunkid = -1")
                Dim dsRenciliationData As DataSet = (New clspaymentbatchreconciliation_tran).GetList("List", strVoucherNo)
                Dim decTotalAmount As Decimal = (From p In dsRenciliationData.Tables(0) Where CInt(p.Item("employeeunkid")) = -1 Select (CDec(p.Item("Amount")))).Sum()
                If decTotalAmount > 0 AndAlso CInt(Session("EFTMobileMoneyBank")) > 0 AndAlso CInt(Session("EFTMobileMoneyBranch")) > 0 AndAlso Session("EFTMobileMoneyAccountName").Trim.Length > 0 AndAlso Session("EFTMobileMoneyAccountNo").Trim.Length > 0 Then
                    Dim objPayrollGroupMaster As New clspayrollgroup_master
                    Dim objBankbBranchMaster As New clsbankbranch_master
                    Dim strEMP_BGCode As String = "", strEMP_BankName As String = "", strEMP_BRCode As String = "", strEMP_SwiftCode As String = "", strEMP_BranchName As String = "", strEMP_SortCode As String = ""
                    objPayrollGroupMaster._Groupmasterunkid = CInt(Session("EFTMobileMoneyBank"))
                    strEMP_BGCode = objPayrollGroupMaster._Groupcode
                    strEMP_BankName = objPayrollGroupMaster._Groupname
                    strEMP_SwiftCode = objPayrollGroupMaster._Swiftcode
                    objBankbBranchMaster._Branchunkid = CInt(Session("EFTMobileMoneyBranch"))
                    strEMP_BRCode = objBankbBranchMaster._Branchcode
                    strEMP_BranchName = objBankbBranchMaster._Branchname
                    strEMP_SortCode = objBankbBranchMaster._Sortcode
                    Dim strCMP_BGID As String = "", strCMP_BRID As String = "", strCMP_BGCODE As String = "", strCMP_BankName As String = "", strCMP_BRCode As String = "", strCMP_BranchName As String = "", strCMP_Account As String = "", strCMP_SortCode = "", strCMP_SwiftCode = ""
                    objBankbBranchMaster._Branchunkid = CInt(cboCompanyBranchName.SelectedValue)
                    strCMP_BRCode = objBankbBranchMaster._Branchcode
                    strCMP_BranchName = objBankbBranchMaster._Branchname
                    strCMP_SortCode = objBankbBranchMaster._Sortcode
                    objPayrollGroupMaster._Groupmasterunkid = CInt(objBankbBranchMaster._Bankgroupunkid)
                    strCMP_BGID = objPayrollGroupMaster._Groupmasterunkid
                    strCMP_BGCODE = objPayrollGroupMaster._Groupcode
                    strCMP_BankName = objPayrollGroupMaster._Groupname
                    strCMP_SwiftCode = objPayrollGroupMaster._Swiftcode
                    Dim drMobileMoney As DataRow = dsGenericCSVPostWeb.Tables(0).NewRow
                    drMobileMoney.Item("employeeunkid") = "-1"
                    drMobileMoney.Item("EmpCode") = "000000"
                    drMobileMoney.Item("EmpName") = Session("EFTMobileMoneyAccountName").ToString
                    drMobileMoney.Item("EMP_BGCode") = strEMP_BGCode
                    drMobileMoney.Item("EMP_BankName") = strEMP_BankName
                    drMobileMoney.Item("EMP_BRCode") = strEMP_BRCode
                    drMobileMoney.Item("EMP_SwiftCode") = strEMP_SwiftCode
                    drMobileMoney.Item("EMP_BranchName") = strEMP_BranchName
                    drMobileMoney.Item("EMP_SortCode") = strEMP_SortCode
                    drMobileMoney.Item("EMP_AccountNo") = Session("EFTMobileMoneyAccountNo").ToString
                    drMobileMoney.Item("EMP_BGID") = CInt(Session("EFTMobileMoneyBank"))
                    drMobileMoney.Item("EMP_BRID") = CInt(Session("EFTMobileMoneyBranch"))
                    drMobileMoney.Item("currency_sign") = cboCurrency.SelectedItem.Text
                    drMobileMoney.Item("Amount") = Format(decTotalAmount, ConfigParameter._Object._CurrencyFormat).Replace(",", "")
                    drMobileMoney.Item("CMP_BGID") = strCMP_BGID
                    drMobileMoney.Item("CMP_BRID") = CInt(cboCompanyBranchName.SelectedValue)
                    drMobileMoney.Item("CMP_BGCODE") = strCMP_BGCODE
                    drMobileMoney.Item("CMP_BankName") = strCMP_BankName
                    drMobileMoney.Item("CMP_BRCode") = strCMP_BRCode
                    drMobileMoney.Item("CMP_BranchName") = strCMP_BranchName
                    drMobileMoney.Item("CMP_Account") = CStr(cboCompanyAccountNo.SelectedItem.Text)
                    drMobileMoney.Item("CMP_SortCode") = strCMP_SortCode
                    drMobileMoney.Item("CMP_SwiftCode") = strCMP_SwiftCode
                    drMobileMoney.Item("voucherno") = strVoucherNo
                    drMobileMoney.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                    drMobileMoney.Item("paidcurrencyid") = dsRenciliationData.Tables(0).Rows(0).Item("paidcurrencyid")
                    drMobileMoney.Item("payment_typeid") = CInt(enBankPaymentType.BankAccount)
                    drMobileMoney.Item("dpndtbeneficetranunkid") = 0
                    dsGenericCSVPostWeb.Tables(0).Rows.Add(drMobileMoney)
                    dsGenericCSVPostWeb.Tables(0).AcceptChanges()
                    objPayrollGroupMaster = Nothing
                    objBankbBranchMaster = Nothing
                End If
                Dim dtBatch As DataTable = dsGenericCSVPostWeb.Tables(0).Select("payment_typeid = " & CInt(enBankPaymentType.BankAccount) & " ").CopyToDataTable
                dsGenericCSVPostWeb.Tables.RemoveAt(0)
                dsGenericCSVPostWeb.Tables.Add(dtBatch)
                Exit For
            Next
            'Hemant (21 Jul 2023) -- End

            Dim dtvoucherno As DataTable = dsGenericCSVPostWeb.Tables(0).DefaultView.ToTable(True, "voucherno")
            For Each drvoucher As DataRow In dtvoucherno.Rows
                Dim strbatchSignature As String = String.Empty

                Dim drVoucherData() As DataRow = dsGenericCSVPostWeb.Tables(0).Select("voucherno = '" & drvoucher.Item("voucherno").ToString() & "'")
                Dim drBatchData() As DataRow = dtPaymentBatchPostingTran.Select("1=1")
                strbatchSignature = Session("EFTRequestClientID").ToString & drBatchData(0).Item("batchname").ToString & drVoucherData.Length & dsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_Account").ToString() & dsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BGCode").ToString() & CDate(ConfigParameter._Object._CurrentDateAndTime).ToString("yyyyMMdd")
                Dim strJSON As String = GenarateJSONGenericEFT(drBatchData(0).Item("batchname").ToString, drVoucherData.Length, Session("CompanyCode").ToString(), CDate(ConfigParameter._Object._CurrentDateAndTime).ToString("yyyyMMdd"), _
                                                              drVoucherData, dsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_Account").ToString(), dsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BankName").ToString(), dsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BGCode").ToString(), _
                                                              dsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_SortCode").ToString(), dsGenericCSVPostWeb.Tables(0).Rows(0).Item("currency_sign").ToString(), SignData(strbatchSignature), strEFTCustomColumnsIds, ConfigParameter._Object._CurrencyFormat, _
                                                              CInt(cboAPeriod.SelectedValue), strDateFormat)

                Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(Session("EFTRequestClientID").ToString & ":" & Session("EFTRequestPassword"))
                Dim strBase64Credential As String = Convert.ToBase64String(byt)

                Dim strMessage = objBankPaymentList.GetSetPaymentAPIRequest(Session("EFTRequestURL").ToString, strBase64Credential, strJSON, strError, strPostedData)

                If strMessage IsNot Nothing Then
                    DisplayMessage.DisplayMessage(" Request Response : " & strMessage("statusCode").ToString() & " - " & strMessage("message").ToString(), Me)

                    If strMessage("statusCode").ToString() = "4000" Then
                        Dim objPaymentBatchReconciliationTran As New clspaymentbatchreconciliation_tran
                        objPaymentBatchReconciliationTran._TranDataTable = drVoucherData.CopyToDataTable
                        objPaymentBatchReconciliationTran._BatchName = drBatchData(0).Item("batchname").ToString()
                        objPaymentBatchReconciliationTran._BatchPostedDateTime = ConfigParameter._Object._CurrentDateAndTime
                        objPaymentBatchReconciliationTran._Isvoid = False
                        objPaymentBatchReconciliationTran._Voiduserunkid = -1
                        objPaymentBatchReconciliationTran._Voiddatetime = Nothing
                        objPaymentBatchReconciliationTran._Voidreason = ""
                        If objPaymentBatchReconciliationTran.InsertAll() = False And objPaymentBatchReconciliationTran._Message <> "" Then
                            DisplayMessage.DisplayMessage(objPaymentBatchReconciliationTran._Message, Me)
                        End If
                        objPaymentBatchReconciliationTran = Nothing

                    End If
                End If
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnRApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRApply.Click
        Try
            If dgvDataList.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName3, 3, "No Data records found."), Me)
                Exit Sub
            End If

            If radApplyToAll.Checked = False AndAlso radApplyToChecked.Checked = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName3, 4, "Please either tick Apply To Checked OR Apply To All for further process."), Me)
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvDataList.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName3, 5, "Please check atleast one record from the list for further process."), Me)
                Exit Sub
            End If

            If CInt(cboBankGroup.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName3, 6, "Please select atleast one Bank Group for further process."), Me)
                cboBankGroup.Focus()
                Exit Sub
            End If

            If CInt(cboBankBranch.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName3, 7, "Please select atleast one Bank Branch for further process."), Me)
                cboBankBranch.Focus()
                Exit Sub
            End If

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                For Each dgRow As DataGridItem In gRow
                    CType(dgRow.FindControl("cbodgcolhBranch"), DropDownList).SelectedValue = CInt(cboBankBranch.SelectedValue)
                    cbodgcolhBranch_SelectedIndexChanged(CType(dgRow.FindControl("cbodgcolhBranch"), DropDownList), New EventArgs())
                Next
            End If



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (26 May 2023) -- End
    'Hemant (04 Aug 2023) -- Start
    Protected Sub btnRExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRExport.Click
        Try
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvDataList.Items.Cast(Of DataGridItem)()

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName3, 10, "There is no Employee to export."), Me)
                dgvDataList.Focus()
                Exit Sub
            End If

            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then

                Dim strBuilder As New System.Text.StringBuilder
                strBuilder.Append(" <HTML> " & vbCrLf)
                strBuilder.Append(" <BODY><FONT FACE = TAHOMA FONT SIZE=2> " & vbCrLf)

                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' COLSPAN='2' BGCOLOR='LightBlue'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName3, 11, "Reconciliation Report") & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName3, 12, "Period : ") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & CStr(IIf(CInt(cboPeriod.SelectedValue) > 0, cboPeriod.SelectedItem.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName3, 13, "Batch : ") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & CStr(IIf(CInt(cboRBatch.SelectedValue) <> 0, cboRBatch.SelectedItem.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                If cboRStatus.SelectedIndex > 0 Then
                    strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName3, 14, "Status : ") & "</B></FONT></TD> " & vbCrLf)
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & CStr(IIf(CInt(cboRStatus.SelectedIndex) > 0, cboRStatus.SelectedItem.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                    strBuilder.Append(" </TR> " & vbCrLf)
                End If

                strBuilder.Append(" </TABLE> " & vbCrLf)

                strBuilder.Append(" <BR/> " & vbCrLf)
                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)
                'strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                'strBuilder.Append(" <TD BORDER = 1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN='" & dtReader.Columns.Count & "' BGCOLOR='LightBlue'><FONT SIZE=2><B>" & cboBatch.Text & "</B></FONT></TD>" & vbCrLf)
                'strBuilder.Append(" </TR> " & vbCrLf)


                Dim gColumn As IEnumerable(Of DataGridColumn) = Nothing
                gColumn = dgvDataList.Columns.Cast(Of DataGridColumn)()
                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)

                For Each dr As DataGridColumn In gColumn
                    If dr.FooterText = "objdgcolhSelect" OrElse dr.FooterText = "objdgcolhBranchUnkid" _
                       OrElse dr.FooterText = "objdgcolhEmployeeUnkid" OrElse dr.FooterText = "objdgcolhDpndtBeneficeTranUnkid" _
                      OrElse dr.FooterText = "dgcolhBranch" Then Continue For
                    If dr.FooterText = "objdgcolhBranchName" Then
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' BGCOLOR='LightBlue'><FONT SIZE=2><B>" & dgvDataList.Columns(GetColumnIndex.getColumnId_Datagrid(dgvDataList, "dgcolhBranch", False, True)).HeaderText & "</B></FONT></TD> " & vbCrLf)
                    Else
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' BGCOLOR='LightBlue'><FONT SIZE=2><B>" & dr.HeaderText & "</B></FONT></TD> " & vbCrLf)

                    End If
                Next
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                For Each dr As DataGridItem In gRow
                    For Each dCol As DataGridColumn In gColumn
                        If dCol.FooterText = "objdgcolhSelect" OrElse dCol.FooterText = "objdgcolhBranchUnkid" _
                       OrElse dCol.FooterText = "objdgcolhEmployeeUnkid" OrElse dCol.FooterText = "objdgcolhDpndtBeneficeTranUnkid" _
                      OrElse dCol.FooterText = "dgcolhBranch" Then Continue For
                        If dCol.FooterText = "dgcolhAmount" Then
                            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & Format(CDbl(dr.Cells(GetColumnIndex.getColumnId_Datagrid(dgvDataList, dCol.FooterText, False, True)).Text), "###########################0.#0").ToString & "</FONT></TD> " & vbCrLf)
                        ElseIf dCol.FooterText = "dgcolhAccountNo" Then
                            Dim txtAccNo As TextBox = CType(dr.FindControl("txtdgcolhAccountNo"), TextBox)
                            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & txtAccNo.Text & "</FONT></TD> " & vbCrLf)
                        Else
                            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dr.Cells(GetColumnIndex.getColumnId_Datagrid(dgvDataList, dCol.FooterText, False, True)).Text & "</FONT></TD> " & vbCrLf)
                        End If

                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)
                Next

                strBuilder.Append(" </TABLE> " & vbCrLf)

                strBuilder.Append(" </BODY> " & vbCrLf)
                strBuilder.Append(" </HTML> " & vbCrLf)

                Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & Language._Object.getCaption(mstrModuleName, lblPageHeader.Text).Replace(" ", "_") & "_" & Now.ToString("yyyyMMddhhmmss") & ".xls"

                Dim fsFile As New IO.FileStream(strFilePath, IO.FileMode.Create, IO.FileAccess.Write)
                Dim strWriter As New IO.StreamWriter(fsFile)
                With strWriter
                    .BaseStream.Seek(0, IO.SeekOrigin.End)
                    .WriteLine(strBuilder)
                    .Close()
                End With

                Session("ExFileName") = strFilePath
                Export.Show()

            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupReconciliationYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupReconciliationYesNo.buttonYes_Click

        Try
            Call ReconciliationProcess()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub
    'Hemant (04 Aug 2023) -- End
#End Region

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebutton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Me.ViewState("FirstOpenPeriod") = Nothing
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton_CloseButton_click : -  " & ex.Message, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

#Region "GridView Event"
    'Hemant (26 May 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
    Protected Sub dgBatch_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgBatch.RowCommand
        Try
            If e.CommandName = "Export" Then
                Dim strBatch As String = CStr(dgBatch.DataKeys(CInt(e.CommandArgument)).Value)
                Dim dsCustomColumnsList As DataSet
                Dim strEFTCustomColumnsIds As String = String.Empty
                Dim strDateFormat As String = String.Empty
                Dim strDilimiter As String

                dsCustomColumnsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList, enEFT_Export_Mode.CSV, CInt(CType(cboReportType.SelectedItem, ListItem).Value))
                Dim drCustomColumnsList() As DataRow = dsCustomColumnsList.Tables(0).Select("headtypeid = 0 ")
                If drCustomColumnsList.Length > 0 Then
                    strEFTCustomColumnsIds = drCustomColumnsList(0).Item("transactionheadid").ToString()
                End If
                Dim drDateFormat() As DataRow = dsCustomColumnsList.Tables(0).Select("headtypeid = 5 ")
                If drDateFormat.Length > 0 Then
                    strDateFormat = drDateFormat(0).Item("transactionheadid").ToString()
                End If

                strDilimiter = ","
                GUI.fmtCurrency = Session("fmtCurrency").ToString
                objBankPaymentList._OpenAfterExport = False

                Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\Batch_" & strBatch & ".csv"

                Dim drBatchData() As DataRow = mdsGenericCSVPostWeb.Tables(0).Select("voucherno = '" & strBatch & "' AND payment_typeid = " & CInt(enBankPaymentType.BankAccount) & "")
                If objBankPaymentList.EFT_Generic_CSV_Post_Web_Export_GenerateReport(drBatchData, strEFTCustomColumnsIds, mstrPath, ConfigParameter._Object._CurrencyFormat, strDilimiter, strDateFormat) Then
                    Session("ExFileName") = mstrPath
                    Export.Show()
                End If

            ElseIf e.CommandName = "Reconciliation" Then
                Dim strBatch As String = CStr(dgBatch.DataKeys(CInt(e.CommandArgument)).Value)
                mstrVoucherNo = strBatch
                mblnIsFromGrid = True
                Call FillReconciliationCombo()
                mblnpopupReconciliation = True
                popupReconciliation.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvDataList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvDataList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                If e.Item.ItemIndex >= 0 Then
                    Dim objBranch As New clsbankbranch_master
                    Dim dsBranch As DataSet = objBranch.getListForCombo("Branch", True)
                    If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvDataList, "objdgcolhBranchUnkid", False, True)).Text) > 0 Then
                        Dim cboDeductionPeriod As DropDownList = CType(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvDataList, "dgcolhBranch", False, True)).FindControl("cbodgcolhBranch"), DropDownList)
                        With cboDeductionPeriod
                            .DataValueField = "branchunkid"
                            .DataTextField = "name"
                            .DataSource = dsBranch.Tables("Branch")
                            .DataBind()
                            .SelectedValue = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvDataList, "objdgcolhBranchUnkid", False, True)).Text
                        End With
                    End If
                    objBranch = Nothing

                    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvDataList, "dgcolhAmount", False, True)).Text = Format(CDec(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvDataList, "dgcolhAmount", False, True)).Text), Session("fmtCurrency"))
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (26 May 2023) -- End

#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebutton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.ID, Me.lblEmployeeName.Text)
            Me.lblCountryName.Text = Language._Object.getCaption(Me.lblCountryName.ID, Me.lblCountryName.Text)
            Me.lblBranchName.Text = Language._Object.getCaption(Me.lblBranchName.ID, Me.lblBranchName.Text)
            Me.lblBankName.Text = Language._Object.getCaption(Me.lblBankName.ID, Me.lblBankName.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.ID, Me.lblAmount.Text)
            Me.lblAmountTo.Text = Language._Object.getCaption(Me.lblAmountTo.ID, Me.lblAmountTo.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.chkEmployeeSign.Text = Language._Object.getCaption(Me.chkEmployeeSign.ID, Me.chkEmployeeSign.Text)
            Me.chkSignatory3.Text = Language._Object.getCaption(Me.chkSignatory3.ID, Me.chkSignatory3.Text)
            Me.chkSignatory2.Text = Language._Object.getCaption(Me.chkSignatory2.ID, Me.chkSignatory2.Text)
            Me.chkSignatory1.Text = Language._Object.getCaption(Me.chkSignatory1.ID, Me.chkSignatory1.Text)
            Me.chkShowGroupByBankBranch.Text = Language._Object.getCaption(Me.chkShowGroupByBankBranch.ID, Me.chkShowGroupByBankBranch.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.ID, Me.lblReportType.Text)
            Me.lblReportMode.Text = Language._Object.getCaption(Me.lblReportMode.ID, Me.lblReportMode.Text)
            Me.lblCompanyBankName.Text = Language._Object.getCaption(Me.lblCompanyBankName.ID, Me.lblCompanyBankName.Text)
            Me.lblCompanyBranchName.Text = Language._Object.getCaption(Me.lblCompanyBranchName.ID, Me.lblCompanyBranchName.Text)
            Me.lblCompanyAccountNo.Text = Language._Object.getCaption(Me.lblCompanyAccountNo.ID, Me.lblCompanyAccountNo.Text)
            Me.chkShowFNameSeparately.Text = Language._Object.getCaption(Me.chkShowFNameSeparately.ID, Me.chkShowFNameSeparately.Text)
            Me.lnkEFTCityBankExport.Text = Language._Object.getCaption(Me.lnkEFTCityBankExport.ID, Me.lnkEFTCityBankExport.Text)
            Me.chkDefinedSignatory.Text = Language._Object.getCaption(Me.chkDefinedSignatory.ID, Me.chkDefinedSignatory.Text)
            Me.lblChequeNo.Text = Language._Object.getCaption(Me.lblChequeNo.ID, Me.lblChequeNo.Text)
            Me.chkShowSortCode.Text = Language._Object.getCaption(Me.chkShowSortCode.ID, Me.chkShowSortCode.Text)
            Me.chkShowPayrollPeriod.Text = Language._Object.getCaption(Me.chkShowPayrollPeriod.ID, Me.chkShowPayrollPeriod.Text)
            Me.chkDefinedSignatory3.Text = Language._Object.getCaption(Me.chkDefinedSignatory3.ID, Me.chkDefinedSignatory3.Text)
            Me.chkDefinedSignatory2.Text = Language._Object.getCaption(Me.chkDefinedSignatory2.ID, Me.chkDefinedSignatory2.Text)
            Me.chkShowBranchCode.Text = Language._Object.getCaption(Me.chkShowBranchCode.ID, Me.chkShowBranchCode.Text)
            Me.chkShowBankCode.Text = Language._Object.getCaption(Me.chkShowBankCode.ID, Me.chkShowBankCode.Text)
            Me.chkShowEmployeeCode.Text = Language._Object.getCaption(Me.chkShowEmployeeCode.ID, Me.chkShowEmployeeCode.Text)
            Me.chkShowAccountType.Text = Language._Object.getCaption(Me.chkShowAccountType.ID, Me.chkShowAccountType.Text)
            Me.chkShowReportHeader.Text = Language._Object.getCaption(Me.chkShowReportHeader.ID, Me.chkShowReportHeader.Text)
            Me.lnkEFT_CBA_Export.Text = Language._Object.getCaption(Me.lnkEFT_CBA_Export.ID, Me.lnkEFT_CBA_Export.Text).Replace("&", "")
            Me.lnkMobileMoneyEFTMPesaExport.Text = Language._Object.getCaption(Me.lnkMobileMoneyEFTMPesaExport.ID, Me.lnkMobileMoneyEFTMPesaExport.Text).Replace("&", "")
            Me.lnkEFT_EXIM_Export.Text = Language._Object.getCaption(Me.lnkEFT_EXIM_Export.ID, Me.lnkEFT_EXIM_Export.Text).Replace("&", "")
            Me.lnkEFT_Custom_CSV_Export.Text = Language._Object.getCaption(Me.lnkEFT_Custom_CSV_Export.ID, Me.lnkEFT_Custom_CSV_Export.Text).Replace("&", "")
            Me.lnkEFT_Custom_XLS_Export.Text = Language._Object.getCaption(Me.lnkEFT_Custom_XLS_Export.ID, Me.lnkEFT_Custom_XLS_Export.Text).Replace("&", "")
            Me.lblCutOffAmount.Text = Language._Object.getCaption(Me.lblCutOffAmount.ID, Me.lblCutOffAmount.Text)
            Me.lnkEFT_FlexCubeRetailGEFU_Export.Text = Language._Object.getCaption(Me.lnkEFT_FlexCubeRetailGEFU_Export.ID, Me.lnkEFT_FlexCubeRetailGEFU_Export.Text).Replace("&", "")
            Me.lnkEFT_ECO_Bank.Text = Language._Object.getCaption(Me.lnkEFT_ECO_Bank.ID, Me.lnkEFT_ECO_Bank.Text).Replace("&", "")
            Me.lnkEFTBarclaysBankExport.Text = Language._Object.getCaption(Me.lnkEFTBarclaysBankExport.ID, Me.lnkEFTBarclaysBankExport.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblPresentDays.Text = Language._Object.getCaption(Me.lblPresentDays.ID, Me.lblPresentDays.Text)
            Me.lblBasicSalary.Text = Language._Object.getCaption(Me.lblBasicSalary.ID, Me.lblBasicSalary.Text)
            Me.lblSocialSecurity.Text = Language._Object.getCaption(Me.lblSocialSecurity.ID, Me.lblSocialSecurity.Text)
            Me.lblExtraIncome.Text = Language._Object.getCaption(Me.lblExtraIncome.ID, Me.lblExtraIncome.Text)
            Me.lblIdentityType.Text = Language._Object.getCaption(Me.lblIdentityType.ID, Me.lblIdentityType.Text)
            Me.lblPaymentType.Text = Language._Object.getCaption(Me.lblPaymentType.ID, Me.lblPaymentType.Text)
            Me.lblAbsentDays.Text = Language._Object.getCaption(Me.lblAbsentDays.ID, Me.lblAbsentDays.Text)
            Me.lblCustomCurrFormat.Text = Language._Object.getCaption(Me.lblCustomCurrFormat.ID, Me.lblCustomCurrFormat.Text)
            Me.lnkEFTBarclaysBankExport.Text = Language._Object.getCaption(Me.lnkEFTBarclaysBankExport.ID, Me.lnkEFTBarclaysBankExport.Text).Replace("&", "")
            'S.SANDEEP [04-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
            Me.lnkEFTNationalBankMalawi.Text = Language._Object.getCaption(Me.lnkEFTNationalBankMalawi.ID, Me.lnkEFTNationalBankMalawi.Text).Replace("&", "")
            'S.SANDEEP [04-May-2018] -- END
            'Sohail (04 Nov 2019) -- Start
            'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
            Me.btnReport.Text = Language._Object.getCaption(Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
            Me.lnkEFT_FNB_Bank_Export.Text = Language._Object.getCaption(Me.lnkEFT_FNB_Bank_Export.ID, Me.lnkEFT_FNB_Bank_Export.Text).Replace("&", "")
            'Sohail (04 Nov 2019) -- End
            'Hemant (11 Dec 2019) -- Start
            'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
            Me.lnkEFTStandardCharteredBank_S2B.Text = Language._Object.getCaption(Me.lnkEFTStandardCharteredBank_S2B.ID, Me.lnkEFTStandardCharteredBank_S2B.Text).Replace("&", "")
            'Hemant (11 Dec 2019) -- End
            'Hemant (29 Jun 2020) -- Start
            'ENHANCEMENT(Benchmark) # 0004751: ABSA BANK EFT
            Me.lnkEFT_ABSA_Bank.Text = Language._Object.getCaption(Me.lnkEFT_ABSA_Bank.ID, Me.lnkEFT_ABSA_Bank.Text).Replace("&", "")
            'Hemant (29 Jun 2020) -- End
            Me.lnkEFTNationalBankMalawiXLSX.Text = Language._Object.getCaption(Me.lnkEFTNationalBankMalawiXLSX.ID, Me.lnkEFTNationalBankMalawiXLSX.Text).Replace("&", "")
            Me.lblMembershipRepo.Text = Language._Object.getCaption(Me.lblMembershipRepo.ID, Me.lblMembershipRepo.Text).Replace("&", "")
            Me.lnkEFTEquityBankKenya.Text = Language._Object.getCaption(Me.lnkEFTEquityBankKenya.ID, Me.lnkEFTEquityBankKenya.Text).Replace("&", "")
            Me.lnkEFTNationalBankKenya.Text = Language._Object.getCaption(Me.lnkEFTNationalBankKenya.ID, Me.lnkEFTNationalBankKenya.Text).Replace("&", "")
            Me.lnkEFTCitiBankKenya.Text = Language._Object.getCaption(Me.lnkEFTCitiBankKenya.ID, Me.lnkEFTCitiBankKenya.Text).Replace("&", "")
            'Hemant (29 Mar 2023) -- Start
            'ENHANCEMENT : A1X-711 -  ZSSF - Dynamics Nav database integration for Staff Payments 
            Me.lnkDynamicNavisionExport.Text = Language._Object.getCaption(Me.lnkDynamicNavisionExport.ID, Me.lnkDynamicNavisionExport.Text).Replace("&", "")
            Me.lnkDynamicNavisionPaymentPostDB.Text = Language._Object.getCaption(Me.lnkDynamicNavisionPaymentPostDB.ID, Me.lnkDynamicNavisionPaymentPostDB.Text).Replace("&", "")
            'Hemant (29 Mar 2023) -- End  
            'Hemant (14 Apr 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-802 - Create a generic EFT file with custom columns in CSV
            Me.lnkEFTGenericCSV.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTGenericCSV.ID, Me.lnkEFTGenericCSV.Text).Replace("&", "")
            Me.lnkEFTGenericCSVPostWeb.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTGenericCSVPostWeb.ID, Me.lnkEFTGenericCSVPostWeb.Text).Replace("&", "")
            Me.lnkEFTPaymentBatchApproval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTPaymentBatchApproval.ID, Me.lnkEFTPaymentBatchApproval.Text).Replace("&", "")
            Me.lnkEFTPaymentBatchReconciliation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTPaymentBatchReconciliation.ID, Me.lnkEFTPaymentBatchReconciliation.Text).Replace("&", "")
            'Hemant (14 Apr 2023) -- End
            'Hemant (12 May 2023) -- Start
            'ENHANCEMENT : A1X-881 - Maj Consulting - Bank of Kigali CSV EFT
            Me.lnkEFTBankOfKigali.Text = Language._Object.getCaption(Me.lnkEFTBankOfKigali.ID, Me.lnkEFTBankOfKigali.Text).Replace("&", "")
            'Hemant (12 May 2023) -- End
            'Hemant (12 May 2023) -- Start
            'ENHANCEMENT : A1X-882 - Maj Consulting - NCBA EFT
            Me.lnkEFT_NCBA.Text = Language._Object.getCaption(Me.lnkEFT_NCBA.ID, Me.lnkEFT_NCBA.Text).Replace("&", "")
            'Hemant (12 May 2023) -- End
            'Hemant (07 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1034 - NBC Bank payment EFT
            Me.lnkEFT_NBC.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFT_NBC.ID, Me.lnkEFT_NBC.Text).Replace("&", "")
            'Hemant (07 Jul 2023) -- End

            'Hemant (20 Jul 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            Me.chkShowSelectedBankInfo.Text = Language._Object.getCaption(Me.chkShowSelectedBankInfo.ID, Me.chkShowSelectedBankInfo.Text)
            Me.chkShowCompanyGrpInfo.Text = Language._Object.getCaption(Me.chkShowCompanyGrpInfo.ID, Me.chkShowCompanyGrpInfo.Text)
            'Hemant (20 Jul 2019) -- End

            'EFT CUSTOM COLUMNS
            Language.setLanguage(mstrModuleName1) 'Hemant (27 June 2019)
            lvEFTCustomColumns.Columns(1).HeaderText = Language._Object.getCaption(lvEFTCustomColumns.Columns(2).FooterText, lvEFTCustomColumns.Columns(2).HeaderText)
            Me.btnEFTSaveSelection.Text = Language._Object.getCaption(Me.btnEFTSaveSelection.ID, Me.btnEFTSaveSelection.Text)
            Me.btnEFTOK.Text = Language._Object.getCaption(Me.btnEFTOK.ID, Me.btnEFTOK.Text)
            Me.btnEFTClose.Text = Language._Object.getCaption(Me.btnEFTClose.ID, Me.btnEFTClose.Text)
            Me.chkShowColumnHeader.Text = Language._Object.getCaption(Me.chkShowColumnHeader.ID, Me.chkShowColumnHeader.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.ID, Me.lblMembership.Text)
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            Me.chkSaveAsTXT.Text = Language._Object.getCaption(Me.chkSaveAsTXT.ID, Me.chkSaveAsTXT.Text)
            Me.chkTABDelimiter.Text = Language._Object.getCaption(Me.chkTABDelimiter.ID, Me.chkTABDelimiter.Text)
            'Hemant (27 June 2019) -- End
            'Hemant (02 Jul 2020) -- Start
            'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
            Me.lblDateFormat.Text = Language._Object.getCaption(Me.lblDateFormat.ID, Me.lblDateFormat.Text)
            'Hemant (02 Jul 2020) -- End
        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("Procedure : SetLanguage : " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period is mandatory information.Please select Period.")
            Language.setMessage(mstrModuleName, 2, "Bank Payment List")
            Language.setMessage(mstrModuleName, 3, "Electronic Funds Transfer")
            Language.setMessage(mstrModuleName, 4, "Currency is mandatory information. Please select Currency.")
            Language.setMessage(mstrModuleName, 5, "Please select Company Bank.")
            Language.setMessage(mstrModuleName, 6, "Please select Company Bank Branch.")
            Language.setMessage(mstrModuleName, 7, "Please select Company Bank Account.")
            Language.setMessage(mstrModuleName, 8, "DRAFT")
            Language.setMessage(mstrModuleName, 9, "AUTHORIZED")
            Language.setMessage(mstrModuleName, 10, "EFT Standard Bank")
            Language.setMessage(mstrModuleName, 12, "EFT Standard Chartered Bank")
            Language.setMessage(mstrModuleName, 13, "EFT SFI Bank")
            Language.setMessage(mstrModuleName, 14, "Please select Authorize mode to generate report.")
            Language.setMessage(mstrModuleName, 15, "Show Group By Bank / Branch")
            Language.setMessage(mstrModuleName, 16, "Show Group By Bank")
            Language.setMessage(mstrModuleName, 17, "Please Set Data File Export Path From Aruti Configuration -> Option -> Integration -> EFT Integration.")
            Language.setMessage(mstrModuleName, 18, "Please Set Data File Name From Aruti Configuration -> Option -> Integration -> EFT Integration.")
            Language.setMessage(mstrModuleName, 21, "EFT Advance Salary CSV")
            Language.setMessage(mstrModuleName, 22, "EFT Advance Salary XSV")
            Language.setMessage(mstrModuleName, 23, "Please set Posting Date.")
            Language.setMessage(mstrModuleName, 27, "Salary Format WPS")
            Language.setMessage(mstrModuleName, 31, "Please select membership to generate report.")
            Language.setMessage(mstrModuleName, 32, "Data Posted Successfuly.")
            Language.setMessage(mstrModuleName1, 1, "Please Select atleast one column to export EFT report.")
            Language.setMessage(mstrModuleName1, 2, "Information Successfully Saved.")
            Language.setMessage(mstrModuleName1, 3, "Please Select Membership.")

        Catch Ex As Exception
            DisplayMessage.DisplayError("SetMessages : " & Ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
