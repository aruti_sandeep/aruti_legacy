﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_Paymentapprovedisapprove.aspx.vb" Inherits="Payroll_wPg_Paymentapprovedisapprove"
    Title="Approve Payment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>

    <script type="text/javascript">
    function SetGeidScrolls()
    {
        var arrPnl=$('.gridscroll');
        for(j = 0; j < arrPnl.length; j++)
        {
            var trtag=$(arrPnl[j]).find('.gridview').children('tbody').children();
            if (trtag.length>52)
            {
                var trheight=0;
                for (i = 0; i < 52; i++) { 
                    trheight = trheight + $(trtag[i]).height();
                }
                $(arrPnl[j]).css("overflow", "auto");
                $(arrPnl[j]).css("height", trheight+"px"); 
            }
            else{
                $(arrPnl[j]).css("overflow", "auto"); 
                $(arrPnl[j]).css("height", "100%"); 
            }
        }
    }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Approve Payment"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 50%; vertical-align: top;">
                                                <div id="Div1" class="panel-default">
                                                    <div id="Div2" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <div id="scrollable-container" class="gridscroll" style="vertical-align: top" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                                        <asp:Panel ID="pnl_GvApprovePayment" Height="470px" ScrollBars="Auto" runat="server">
                                                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:GridView ID="GvApprovePayment" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                                                        HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                                                        <Columns>
                                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged" />
                                                                                                </HeaderTemplate>
                                                                                                <ItemTemplate>
                                                                                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                                        OnCheckedChanged="chkSelect_OnCheckedChanged" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:BoundField DataField="paymentapprovaltranunkid" HeaderText="paymentapprovaltranunkid"
                                                                                                ReadOnly="True" Visible="false" />
                                                                                            <asp:BoundField DataField="paymenttranunkid" HeaderText="paymenttranunkid" ReadOnly="True"
                                                                                                Visible="false" />
                                                                                            <asp:BoundField DataField="employeeunkid" HeaderText="employeeunkid" ReadOnly="True"
                                                                                                Visible="false" />
                                                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="True" HeaderStyle-HorizontalAlign="Left"
                                                                                                FooterText="colhCode" />
                                                                                            <asp:BoundField DataField="EmpName" HeaderText="Name" ReadOnly="True" HeaderStyle-HorizontalAlign="Left"
                                                                                                FooterText="colhName" />
                                                                                            <asp:BoundField DataField="expaidamt" HeaderText="Paid Amount" ReadOnly="True" HeaderStyle-HorizontalAlign="Right"
                                                                                                ItemStyle-HorizontalAlign="Right" FooterText="colhAmount" />
                                                                                            <asp:BoundField DataField="expaidamt_tag" HeaderText="expaidamt_tag" ReadOnly="True"
                                                                                                Visible="false" />
                                                                                            <asp:BoundField DataField="paidcurrency" HeaderText="Currency" ReadOnly="True" HeaderStyle-HorizontalAlign="Left"
                                                                                                FooterText="colhCurrency" />
                                                                                            <asp:BoundField DataField="paymentmode" HeaderText="paymentmode" ReadOnly="True"
                                                                                                Visible="false" />
                                                                                        </Columns>
                                                                                    </asp:GridView>
                                                                                </ContentTemplate>
                                                                                <Triggers>
                                                                                    <asp:AsyncPostBackTrigger ControlID="cboPayPeriod" EventName="SelectedIndexChanged" />
                                                                                    <asp:AsyncPostBackTrigger ControlID="cboCurrency" EventName="SelectedIndexChanged" />
                                                                                </Triggers>
                                                                            </asp:UpdatePanel>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <h3 style="border-bottom: 1px solid #DDD; color: #333; margin-bottom: 10px">
                                                                        <asp:Label ID="gbAmountInfo" runat="server" Text="Total Amount"></asp:Label>
                                                                    </h3>
                                                                    <table style="width: 100%">
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 30%">
                                                                                <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:Label ID="lblTotalAmount" runat="server" Text="Total Amt. (Tsh)"></asp:Label>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboCurrency" EventName="SelectedIndexChanged" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                            <td style="width: 70%">
                                                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtTotalAmount" Width="300px" Style="text-align: right;" runat="server"
                                                                                            ReadOnly="true" Text="0"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboPayPeriod" EventName="SelectedIndexChanged" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="width: 50%; vertical-align: top;">
                                                <div class="panel-body">
                                                    <div id="Div3" class="panel-default">
                                                        <div id="Div4" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="gbPaymentInfo" runat="server" Text="Payment Information"></asp:Label>
                                                            </div>
                                                            <div style="text-align: right">
                                                                <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" CssClass="lnkhover"></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div id="Div5" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="popupConfirm" EventName="buttonYes_Click" />
                                                                                <asp:AsyncPostBackTrigger ControlID="popupDisApprove" EventName="buttonDelReasonYes_Click" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblCurrency" runat="server" Text="Currency"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblPmtVoucher" runat="server" Text="Voucher #"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:DropDownList ID="cboPmtVoucher" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="objbtnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                                                <asp:Button ID="objbtnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div11" class="panel-default">
                                                        <div id="Div6" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="gbPaymentModeInfo" runat="server" Text="Payment Mode Summary"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div14" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblbankPaid" runat="server" Text="Bank Payment"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:Label ID="objBankpaidVal" runat="server" Text="Bank Payment #"></asp:Label>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="cboPayPeriod" EventName="SelectedIndexChanged" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblCashPaid" runat="server" Text="Cash Payment"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:Label ID="objCashpaidVal" runat="server" Text="Cash Payment #"></asp:Label>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="cboPayPeriod" EventName="SelectedIndexChanged" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%" colspan="2">
                                                                        <div style="border-bottom: 1px solid #DDD">
                                                                        </div>
                                                                        <%--<asp:Label ID="LblLine"  runat="server" Text="-----------------------------------------------------------------------------"></asp:Label>--%>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblTotalPaid" runat="server" Text="Total Payment"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:Label ID="objTotalpaidVal" runat="server" Text="Total Payment #"></asp:Label>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="cboPayPeriod" EventName="SelectedIndexChanged" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div10" class="panel-default">
                                                        <div id="Div7" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="gbPaymentApprovalnfo" runat="server" Text="Payment Approval Information"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div12" class="panel-body-default" style="position: relative">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:Label ID="objApproverName" runat="server" Text="0"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblApproverLevel" runat="server" Text="Approver Level"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:Label ID="objApproverLevelVal" runat="server" Text="0"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblLevelPriority" runat="server" Text="Level Priority"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:Label ID="objLevelPriorityVal" runat="server" Text="0"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td colspan="2">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div style="border-bottom: 1px solid #DDD">
                                                            </div>
                                                            <table style="width: 100%; margin-top: 10px">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%; vertical-align: top">
                                                                        <asp:Label ID="lblRemarks" runat="server" Text="Approval Remark"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="popupConfirm" EventName="buttonYes_Click" />
                                                                                <asp:AsyncPostBackTrigger ControlID="popupDisApprove" EventName="buttonDelReasonYes_Click" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <h4 class="heading1" style="width: 100%; text-align: left;">
                                                            </h4>
                                                            <div id="btnfixedbottom" class="btn-default">
                                                                <table style="width: 100%;">
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 37%; vertical-align: top">
                                                                            <table>
                                                                                <tr style="width: 100%">
                                                                                    <td>
                                                                                        <asp:LinkButton ID="lnkPayrollVarianceReport" runat="server" Text="Show Payroll Variance Report"
                                                                                            CssClass="lnkhover" Style="color: Blue; vertical-align: top; float: left; font-size: 10px;"></asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:LinkButton ID="lnkPayrollTotalVarianceReport" runat="server" Text="Show Payroll Total Variance Report"
                                                                                            CssClass="lnkhover" Style="color: Blue; vertical-align: top; float: left; font-size: 10px;"></asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td style="width: 60%; vertical-align: top">
                                                                            <table>
                                                                                <tr style="width: 100%">
                                                                                    <td>
                                                                <asp:Button ID="btnProcess" runat="server" Text="Approve Payment" CssClass="btnDefault" />
                                                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc9:ConfirmYesNo ID="popupConfirm" runat="server" Title="Are You Sure?" />
                    <uc9:DeleteReason ID="popupDisApprove" runat="server" Title="Are you sure you want to Disapprove listed Payments?" />
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
