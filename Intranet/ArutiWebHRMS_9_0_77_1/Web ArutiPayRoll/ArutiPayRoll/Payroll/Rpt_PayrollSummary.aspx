﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_PayrollSummary.aspx.vb" Inherits="Payroll_rpt_PayrollSummary" Title="Payroll Summary Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll2 = {
            Y: '#<%= hfScrollPosition2.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container2").scrollTop($(scroll2.Y).val());
            }
        }
    </script>
    
    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Payroll Summary Report"></asp:Label>
                        </div>
                        <div class="panel-body" style="text-align:left;">
                            <div id="FilterCriteria" class="panel-default" style="width:48%;float:left;">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 40%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" />
                                            </td>
                                            <td style="width:40%"></td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblCurrency" runat="server" Text="Currency"></asp:Label>
                                            </td>
                                            <td style="width: 40%">
                                                <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true" />
                                            </td>
                                            <td style="width: 40%">
                                                <asp:Label ID="lblExRate" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%" colspan="2" >
                                                <asp:CheckBox ID="chkInactiveemp" Text="Include Inactive Employee" runat="server" />
                                            </td>
                                            
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%" colspan="2" >
                                                <asp:CheckBox ID="chkIgnorezeroHead" Text="Ignore Zero Value Heads" runat="server" />
                                            </td>
                                            
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%" colspan="2" >
                                                <asp:CheckBox ID="chkShowEmployersContribution" Text="Show Employers Contribution" runat="server" />
                                            </td>                                            
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%" colspan="2" >
                                                <asp:CheckBox ID="chkShowInformational" Text="Show Informational Heads" runat="server" AutoPostBack="true" />
                                            </td>                                            
                                        </tr> 
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%" colspan="2" >
                                                <asp:CheckBox ID="chkShowEachAnalysisOnNewPage" Text="Show Analysis On New Page" runat="server" />
                                            </td>                                            
                                        </tr>                                        
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            
                            <asp:Panel ID="pnlInfoheads" runat="server" class="panel-default" style="width:48%;float:left;">
                                <div id="infoheadsTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblInfoHeadHeader" runat="server" Text="Informational Heads"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkSave" runat="server" Text="Save Settings" CssClass="lnkhover" Visible="false"></asp:LinkButton>
                                    </div>
                                </div>
                                
                                <div id="infoheadsBody" class="panel-body-default">
                                    <div style="margin: 3px 3px;">
                                        <asp:TextBox ID="txtSearchTranHeads" runat="server" AutoPostBack="true" placeholder="Search Heads"></asp:TextBox>
                                    </div>
                                    <div id="scrollable-container2" style="max-height: 250px; overflow: auto" onscroll="$(scroll2.Y).val(this.scrollTop);">
                                        <asp:GridView ID="dgHeads" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                            ShowFooter="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                            DataKeyNames="tranheadunkid">
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                    ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeadSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkHeadSelectAll_OnCheckedChanged" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkHeadSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                            OnCheckedChanged="chkHeadSelect_OnCheckedChanged" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Code" HeaderText="Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                    FooterText="dgColhHeadCode" />
                                                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                    FooterText="dgColhHeadName" />
                                                <asp:BoundField DataField="tranheadunkid" ReadOnly="true" Visible="false" />
                                                <%--<asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkUp" CssClass="button" CommandArgument="up" runat="server"
                                                            Text="&#x25B2;" OnClick="ChangeTranHeadLocation" />
                                                        <asp:LinkButton ID="lnkDown" CssClass="button" CommandArgument="down" runat="server"
                                                            Text="&#x25BC;" OnClick="ChangeTranHeadLocation" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />
                            </asp:Panel>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
