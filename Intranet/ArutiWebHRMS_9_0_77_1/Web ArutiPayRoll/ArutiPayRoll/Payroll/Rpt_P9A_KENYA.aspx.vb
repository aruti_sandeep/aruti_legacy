﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Payroll_Rpt_P9A_KENYA
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmP9AReport"
    Dim DisplayMessage As New CommonCodes
    Private objP9AReport As clsP9AReport
    Private mintFirstPeriodId As Integer = 0
    Private mintLastPeriodId As Integer = 0 'Sohail (29 Mar 2017)
    Private mdtPeriod As DataTable

#End Region

#Region " Page's Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            SetLanguage()

            If Not IsPostBack Then
                Call FillCombo()
                Call ResetValue()
                'Sohail (20 Apr 2017) -- Start
                'CCK Enhancement - 65.2 - Disable Mappings for P9A Report for ESS.
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    cboQuarterValue.Enabled = False
                    cboE1.Enabled = False
                    cboE2.Enabled = False
                    cboE3.Enabled = False
                    cboAmountOfInterest.Enabled = False
                    cboTaxCharged.Enabled = False
                    cboPersonalRelief.Enabled = False
                    cboMembership.Enabled = False
                    cboInsuranceRelief.Enabled = False
                    'Sohail (20 Mar 2021) -- Start
                    'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                    cboTaxableIcome.Enabled = False
                    txtBasicSalaryFormula.Enabled = False
                    txtTaxChargedFormula.Enabled = False
                    'Sohail (20 Mar 2021) -- End
                Else
                    cboQuarterValue.Enabled = True
                    cboE1.Enabled = True
                    cboE2.Enabled = True
                    cboE3.Enabled = True
                    cboAmountOfInterest.Enabled = True
                    cboTaxCharged.Enabled = True
                    cboPersonalRelief.Enabled = True
                    cboMembership.Enabled = True
                    cboInsuranceRelief.Enabled = True
                    'Sohail (20 Mar 2021) -- Start
                    'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                    cboTaxableIcome.Enabled = True
                    txtBasicSalaryFormula.Enabled = True
                    txtTaxChargedFormula.Enabled = True
                    'Sohail (20 Mar 2021) -- End
                End If
                'Sohail (20 Apr 2017) -- End
            Else
                mintFirstPeriodId = CInt(Me.ViewState("FirstPeriodId"))
                mintLastPeriodId = CInt(Me.ViewState("mintLastPeriodId")) 'Sohail (29 Mar 2017)
                mdtPeriod = CType(Me.ViewState("PeriodTable"), DataTable)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load1" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("FirstPeriodId") = mintFirstPeriodId
            Me.ViewState("mintLastPeriodId") = mintLastPeriodId 'Sohail (29 Mar 2017)
            Me.ViewState("PeriodTable") = mdtPeriod
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Contructor "

    Public Sub New()
        'objP9AReport = New clsP9AReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'objP9AReport.SetDefaultValue()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        QUARTER_VALUE = 1
        E1 = 2
        E2 = 3
        E3 = 4
        AMOUNT_OF_INTEREST = 5
        TAX_CHARGED = 6
        PERSONAL_RELIEF = 7
        MEMBERSHIP = 8
        'Sohail (18 Jan 2017) -- Start
        'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
        Insurance_Relief = 9
        'Sohail (18 Jan 2017) -- End
        'Sohail (08 May 2020) -- Start
        'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
        Taxable_Income = 10
        'Sohail (08 May 2020) -- End
        'Sohail (20 Mar 2021) -- Start
        'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
        BasicSalaryFormula = 11
        TaxChargedFormula = 12
        'Sohail (20 Mar 2021) -- End
    End Enum
#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsCombos As New DataSet
            Dim objEmp As New clsEmployee_Master
            Dim objPeriod As New clscommom_period_Tran
            Dim objTranHead As New clsTransactionHead
            Dim objMaster As New clsMasterData
            Dim objMember As New clsmembership_master

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                dsCombos = objEmp.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                  Session("UserAccessModeSetting").ToString, True, True, "Employee", False, CInt(Session("Employeeunkid")), _
                                                  , , , , , , , , , , , , , , , False, True)
                'Sohail (15 Feb 2017) -- Start
                'CCK Enhancement - 64.1 - Allow to access P9A report from MSS.
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsCombos = objEmp.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                             Session("UserAccessModeSetting").ToString, True, True, "Employee", True)
                'Sohail (15 Feb 2017) -- End
            End If
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombos.Tables("Employee")
                .DataBind()
            End With

            mintFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)

            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to view previous closed year report on P9A Report.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString, _
            '                                     CDate(Session("fin_startdate").ToString).Date, "Period", True)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 0, Session("Database_Name").ToString, _
                                                 CDate(Session("fin_startdate").ToString).Date, "Period", True)
            'Sohail (18 Jan 2017) -- End
            'Sohail (15 Feb 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to select periods from previous year and current year only on ESS P9A Report.
            'mdtPeriod = dsCombos.Tables("Period")
            'Sohail (02 May 2017) -- Start
            'HJF Enhancement - 66.1 - Allow to select only previous jan to dec calender / KRA financial year periods only even fy does not start from jan.
            'Dim dsYear As DataSet = objMaster.Get_Database_Year_List("Year", True, CInt(Session("companyunkid")))
            'Sohail (10 Mar 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to select periods from previous year only and not from current year on MSS and ESS P9A Report.
            'Dim dt As DataTable = New DataView(dsYear.Tables(0), "start_date >= '" & eZeeDate.convertDate(CDate(Session("fin_startdate")).AddYears(-1)) & "' ", "", DataViewRowState.CurrentRows).ToTable
            'Dim dt As DataTable = New DataView(dsYear.Tables(0), "start_date >= '" & eZeeDate.convertDate(CDate(Session("fin_startdate")).AddYears(-1)) & "' AND end_date <= '" & eZeeDate.convertDate(CDate(Session("fin_enddate")).AddYears(-1)) & "' ", "", DataViewRowState.CurrentRows).ToTable
            ''Sohail (10 Mar 2017) -- End
            'Dim strYear As String = String.Join(",", (From p In dt Select (p.Item("yearunkid").ToString)).ToArray)
            'mdtPeriod = New DataView(dsCombos.Tables("Period"), "yearunkid IN (0, " & strYear & ") ", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (28 Oct 2021) -- Start
            'FFK Enhancement : OLD-512 : Kenya P9 Report Enhancement to show all Periods for the last 2 closed FYs on ESS.
            'mdtPeriod = New DataView(dsCombos.Tables("Period"), "periodunkid = 0 OR (start_date >= '" & eZeeDate.convertDate(CDate("01/Jan/" & Year(CDate(Session("fin_enddate")).AddYears(-1)))) & "' AND end_date <= '" & eZeeDate.convertDate(CDate("31/Dec/" & Year(CDate(Session("fin_enddate")).AddYears(-1)))) & "') ", "", DataViewRowState.CurrentRows).ToTable
            mdtPeriod = New DataView(dsCombos.Tables("Period"), "periodunkid = 0 OR (start_date >= '" & eZeeDate.convertDate(CDate("01/Jan/" & Year(CDate(Session("fin_enddate")).AddYears(-2)))) & "' AND end_date <= '" & eZeeDate.convertDate(CDate("31/Dec/" & Year(CDate(Session("fin_enddate")).AddYears(-1)))) & "') ", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (28 Oct 2021) -- End
            'Sohail (02 May 2017) -- End
            'Sohail (29 Mar 2017) -- Start
            'CCK Enhancement - 65.2 - 2. End of year P9A – Currently system open to February 2017, therefore employees can generate reports from Jan 16 to Feb 2017. In fact, this report should not have period option. Should be specific from January to December 2016. (by default first period in From period and last period in to period) 
            If mdtPeriod.Rows.Count > 1 Then
                If CInt(mdtPeriod.Rows(1).Item("periodunkid")) > 0 Then mintFirstPeriodId = CInt(mdtPeriod.Rows(1).Item("periodunkid"))
                'Sohail (28 Oct 2021) -- Start
                'FFK Enhancement : OLD-512 : Kenya P9 Report Enhancement to show all Periods for the last 2 closed FYs on ESS.
                mintFirstPeriodId = (From p In mdtPeriod Where (Year(eZeeDate.convertDate(p.Item("start_date").ToString)) = Year(CDate(Session("fin_enddate")).AddYears(-1))) Select (CInt(p.Item("periodunkid")))).FirstOrDefault
                'Sohail (28 Oct 2021) -- End
                mintLastPeriodId = CInt(mdtPeriod.Rows(mdtPeriod.Rows.Count - 1).Item("periodunkid"))
            Else
                mintFirstPeriodId = 0
            End If
            If mintLastPeriodId <= 0 Then mintLastPeriodId = mintFirstPeriodId
            'Sohail (29 Mar 2017) -- End
            'Sohail (15 Feb 2017) -- End
            With cboFromPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                'Sohail (15 Feb 2017) -- Start
                'CCK Enhancement - 64.1 - Allow to select periods from previous year and current year only on ESS P9A Report.
                '.DataSource = dsCombos.Tables("Period")
                .DataSource = mdtPeriod
                'Sohail (15 Feb 2017) -- End
                'Sohail (10 Mar 2017) -- Start
                'CCK Enhancement - 64.1 - Allow to select periods from previous year only and not from current year on MSS and ESS P9A Report.
                '.SelectedValue = CStr(mintFirstPeriodId)
                'Sohail (29 Mar 2017) -- Start
                'CCK Enhancement - 65.2 - 2. End of year P9A – Currently system open to February 2017, therefore employees can generate reports from Jan 16 to Feb 2017. In fact, this report should not have period option. Should be specific from January to December 2016. (by default first period in From period and last period in to period) 
                '.SelectedValue = "0"
                .SelectedValue = mintFirstPeriodId.ToString
                'Sohail (29 Mar 2017) -- End
                'Sohail (10 Mar 2017) -- End
                .DataBind()
            End With
            With cboToPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                'Sohail (15 Feb 2017) -- Start
                'CCK Enhancement - 64.1 - Allow to select periods from previous year and current year only on ESS P9A Report.
                '.DataSource = dsCombos.Tables("Period").Copy
                .DataSource = mdtPeriod.Copy
                'Sohail (15 Feb 2017) -- End
                'Sohail (10 Mar 2017) -- Start
                'CCK Enhancement - 64.1 - Allow to select periods from previous year only and not from current year on MSS and ESS P9A Report.
                '.SelectedValue = CStr(mintFirstPeriodId)
                'Sohail (29 Mar 2017) -- Start
                'CCK Enhancement - 65.2 - 2. End of year P9A – Currently system open to February 2017, therefore employees can generate reports from Jan 16 to Feb 2017. In fact, this report should not have period option. Should be specific from January to December 2016. (by default first period in From period and last period in to period) 
                '.SelectedValue = "0"
                .SelectedValue = mintLastPeriodId.ToString
                'Sohail (29 Mar 2017) -- End
                'Sohail (10 Mar 2017) -- End
                .DataBind()
            End With

            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to view previous closed year report on P9A Report.
            'dsCombos = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", True, , , enTypeOf.Informational)
            dsCombos = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " ")
            'Sohail (18 Jan 2017) -- End
            With cboQuarterValue
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Heads")
                .SelectedValue = "0"
                .DataBind()
            End With
            With cboE1
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Heads").Copy
                .SelectedValue = "0"
                .DataBind()
            End With
            With cboE2
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Heads").Copy
                .SelectedValue = "0"
                .DataBind()
            End With
            With cboE3
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Heads").Copy
                .SelectedValue = "0"
                .DataBind()
            End With
            With cboAmountOfInterest
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Heads").Copy
                .SelectedValue = "0"
                .DataBind()
            End With

            'Hemant (04 Jul 2020) -- Start
            'ENHANCEMENT #0004771: P9 Statutory Report Changes - Tax Charged Column to Allow Mapping to All Informational Heads
            'dsCombos = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", True, , enCalcType.AsComputedOnWithINEXCESSOFTaxSlab)
            dsCombos = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", True, , , , , , " ( calctype_id = " & CInt(enCalcType.AsComputedOnWithINEXCESSOFTaxSlab) & " OR trnheadtype_id = " & CInt(enTranHeadType.Informational) & "  )")
            'Hemant (04 Jul 2020) -- End

            With cboTaxCharged
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Heads")
                .SelectedValue = "0"
                .DataBind()
            End With

            dsCombos = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " ")
            With cboPersonalRelief
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Heads")
                .SelectedValue = "0"
                .DataBind()
            End With
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to view previous closed year report on P9A Report.
            With cboInsuranceRelief
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Heads").Copy
                .SelectedValue = "0"
                .DataBind()
            End With
            'Sohail (18 Jan 2017) -- End

            'Sohail (08 May 2020) -- Start
            'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
            dsCombos = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", True, enTranHeadType.Informational, , , , , "typeof_id <> " & enTypeOf.Salary & " ")
            With cboTaxableIcome
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = "0"
                .DataBind()
            End With
            'Sohail (08 May 2020) -- End

            dsCombos = objMember.getListForCombo("Membership", True)
            With cboMembership
                .DataValueField = "membershipunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Membership")
                .SelectedValue = "0"
                .DataBind()
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.P9A_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Quarter_Value
                            cboQuarterValue.SelectedValue = CStr(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.E1
                            cboE1.SelectedValue = CStr(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.E2
                            cboE2.SelectedValue = CStr(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.E3
                            cboE3.SelectedValue = CStr(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Amount_Of_Interest
                            cboAmountOfInterest.SelectedValue = CStr(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Tax_Charged
                            cboTaxCharged.SelectedValue = CStr(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Personal_Relief
                            cboPersonalRelief.SelectedValue = CStr(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.MEMBERSHIP
                            cboMembership.SelectedValue = CStr(dsRow.Item("transactionheadid"))

                            'Sohail (18 Jan 2017) -- Start
                            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
                        Case enHeadTypeId.Insurance_Relief
                            cboInsuranceRelief.SelectedValue = CStr(dsRow.Item("transactionheadid"))
                            'Sohail (18 Jan 2017) -- End

                            'Sohail (08 May 2020) -- Start
                            'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
                        Case enHeadTypeId.Taxable_Income
                            cboTaxableIcome.SelectedValue = CStr(dsRow.Item("transactionheadid"))
                            'Sohail (08 May 2020) -- End

                            'Sohail (20 Mar 2021) -- Start
                            'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                        Case enHeadTypeId.BasicSalaryFormula
                            txtBasicSalaryFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.TaxChargedFormula
                            txtTaxChargedFormula.Text = dsRow.Item("transactionheadid").ToString
                            'Sohail (20 Mar 2021) -- End
                    End Select
                Next

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        'Sohail (18 Jan 2017) -- Start
        'CCK Enhancement - 64.1 - Allow to view previous closed year report on P9A Report.
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim intPrevYearID As Integer = 0
        Dim mdicYearDBName As New Dictionary(Of Integer, String)
        'Sohail (18 Jan 2017) -- End
        objP9AReport = New clsP9AReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            objP9AReport.SetDefaultValue()

            If CInt(cboFromPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "From Period is mandatory information. Please select From Period."), Me)
                cboFromPeriod.Focus()
                Return False
            ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "To Period is mandatory information. Please select To Period."), Me)
                cboToPeriod.Focus()
                Return False
            ElseIf CInt(cboFromPeriod.SelectedIndex) > CInt(cboToPeriod.SelectedIndex) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period."), Me)
                cboToPeriod.Focus()
                Return False
            ElseIf CInt(cboQuarterValue.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please select head for Value of Quarters."), Me)
                cboQuarterValue.Focus()
                Return False
            ElseIf CInt(cboE1.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please select head for E1."), Me)
                cboE1.Focus()
                Return False
            ElseIf CInt(cboE2.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Please select head for E2."), Me)
                cboE2.Focus()
                Return False
            ElseIf CInt(cboE3.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please select head for E3."), Me)
                cboE3.Focus()
                Return False
            ElseIf CInt(cboAmountOfInterest.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Please select head for Amount Of Interest."), Me)
                cboAmountOfInterest.Focus()
                Return False
            ElseIf CInt(cboTaxCharged.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Please select head for Tax Charged."), Me)
                cboTaxCharged.Focus()
                Return False
            ElseIf CInt(cboPersonalRelief.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Please select head for Personal Relief."), Me)
                cboPersonalRelief.Focus()
                Return False
                'Sohail (18 Jan 2017) -- Start
                'CCK Enhancement - 64.1 - Allow to view previous closed year report on P9A Report.
            ElseIf CInt(cboInsuranceRelief.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Please select head for Insurance Relief."), Me)
                cboInsuranceRelief.Focus()
                Return False
                'Sohail (18 Jan 2017) -- End
                'Sohail (08 May 2020) -- Start
                'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
            ElseIf CInt(cboTaxableIcome.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please select head for Taxable Income."), Me)
                cboTaxableIcome.Focus()
                Return False
                'Sohail (08 May 2020) -- End
            End If

            'Sohail (20 Apr 2017) -- Start
            'Issue - 65.2 - Report is coming for all employees even if one employee selected on MSS.
            'objP9AReport._EmpId = CInt(Session("Employeeunkid"))
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            objP9AReport._EmpId = CInt(Session("Employeeunkid"))
            Else
                objP9AReport._EmpId = CInt(cboEmployee.SelectedValue)
            End If
            'Sohail (20 Apr 2017) -- End
            objP9AReport._EmpName = cboEmployee.SelectedItem.Text

            Dim i As Integer = 0
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to view previous closed year report on P9A Report.
            'Dim strPreriodIds As String = ""
            'Dim strPeriodsName As String = ""
            'For Each dsRow As DataRow In mdtPeriod.Rows
            '    If i >= cboFromPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
            '        strPreriodIds &= ", " & dsRow.Item("periodunkid").ToString
            '        strPeriodsName &= ", " & dsRow.Item("name").ToString
            '    End If
            '    i += 1
            'Next
            'If strPreriodIds.Trim <> "" Then
            '    strPreriodIds = strPreriodIds.Substring(2)
            'End If
            'If strPeriodsName.Trim <> "" Then
            '    strPeriodsName = strPeriodsName.Substring(2)
            'End If
            Dim strPreriodIds As String = cboFromPeriod.SelectedValue.ToString
            Dim strPeriodsName As String = cboFromPeriod.SelectedItem.Text
            dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", CInt(mdtPeriod.Select("periodunkid = " & CInt(cboFromPeriod.SelectedValue) & " ")(0).Item("yearunkid")))
            If dsList.Tables("Database").Rows.Count > 0 Then
                intPrevYearID = CInt(mdtPeriod.Select("periodunkid = " & CInt(cboFromPeriod.SelectedValue) & " ")(0).Item("yearunkid"))
                If mdicYearDBName.ContainsKey(intPrevYearID) = False Then
                    mdicYearDBName.Add(intPrevYearID, dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                End If
            End If
            
            For Each dsRow As DataRow In mdtPeriod.Rows
                If i > cboFromPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                    strPreriodIds &= ", " & dsRow.Item("periodunkid").ToString
                    strPeriodsName &= ", " & dsRow.Item("name").ToString

                    If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                        dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", CInt(dsRow.Item("yearunkid")))
                        If dsList.Tables("Database").Rows.Count > 0 Then
                            If mdicYearDBName.ContainsKey(CInt(dsRow.Item("yearunkid"))) = False Then
                                mdicYearDBName.Add(CInt(dsRow.Item("yearunkid")), dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                            End If
                        End If
                    End If
                    intPrevYearID = CInt(dsRow.Item("yearunkid"))
                End If
                i += 1
            Next
            objP9AReport._dic_YearDBName = mdicYearDBName
            'Sohail (18 Jan 2017) -- End
            objP9AReport._PeriodIds = strPreriodIds
            objP9AReport._PeriodNames = strPeriodsName

            'Sohail (23 Jan 2021) -- Start
            'FFK Issue : OLD-281 - P9 report reads wrong year when FY is not Jan-Dec. Pick year of the last period selected
            'Dim objYear As New clsCompany_Master
            'Dim dRow As DataRow() = mdtPeriod.Select("periodunkid=" & CInt(cboFromPeriod.SelectedValue))
            'If dRow.Length > 0 Then
            '    objYear._YearUnkid = CInt(dRow(0).Item("yearunkid"))
            '    objP9AReport._FirstPeriodYearName = objYear._FinancialYear_Name.Substring(0, objYear._FinancialYear_Name.IndexOf("-"))
            'End If
            'objYear = Nothing
            objP9AReport._FirstPeriodYearName = eZeeDate.convertDate(mdtPeriod.Select("periodunkid = " & CInt(cboToPeriod.SelectedValue) & " ")(0).Item("end_date").ToString).Year.ToString
            'Sohail (23 Jan 2021) -- End

            objP9AReport._PayeHeadId = CInt(cboQuarterValue.SelectedValue)
            objP9AReport._PayeHeadName = cboQuarterValue.SelectedItem.Text

            objP9AReport._E1HeadId = CInt(cboE1.SelectedValue)
            objP9AReport._E1HeadName = cboE1.SelectedItem.Text

            objP9AReport._E2HeadId = CInt(cboE2.SelectedValue)
            objP9AReport._E2HeadName = cboE2.SelectedItem.Text

            objP9AReport._E3HeadId = CInt(cboE3.SelectedValue)
            objP9AReport._E3HeadName = cboE3.SelectedItem.Text

            objP9AReport._AmountOfInterestHeadId = CInt(cboAmountOfInterest.SelectedValue)
            objP9AReport._AmountOfInterestHeadName = cboAmountOfInterest.SelectedItem.Text

            objP9AReport._TaxChargedHeadId = CInt(cboTaxCharged.SelectedValue)
            objP9AReport._TaxChargedHeadName = cboTaxCharged.SelectedItem.Text

            objP9AReport._PersonalReliefHeadId = CInt(cboPersonalRelief.SelectedValue)
            objP9AReport._PersonalReliefHeadName = cboPersonalRelief.SelectedItem.Text

            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
            objP9AReport._InsuranceReliefHeadId = CInt(cboInsuranceRelief.SelectedValue)
            objP9AReport._InsuranceReliefHeadName = cboInsuranceRelief.SelectedItem.Text
            'Sohail (18 Jan 2017) -- End

            'Sohail (08 May 2020) -- Start
            'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
            objP9AReport._TaxbleIncomeHeadId = CInt(cboTaxableIcome.SelectedValue)
            objP9AReport._TaxbleIncomeHeadName = cboTaxableIcome.SelectedItem.Text
            'Sohail (08 May 2020) -- End

            'Sohail (20 Mar 2021) -- Start
            'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
            objP9AReport._BasicSalaryFormula = txtBasicSalaryFormula.Text
            objP9AReport._TaxChargedFormula = txtTaxChargedFormula.Text
            'Sohail (20 Mar 2021) -- End

            objP9AReport._MembershipId = CInt(cboMembership.SelectedValue)

            objP9AReport._ViewIndex = 0

            'Sohail (15 Feb 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to access P9A report from MSS.
            'objP9AReport._ApplyUserAccessFilter = False
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            objP9AReport._ApplyUserAccessFilter = False
            Else
                objP9AReport._ApplyUserAccessFilter = True
            End If
            'Sohail (15 Feb 2017) -- End

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetFilter:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Sub ResetValue()
        Try
            cboQuarterValue.SelectedValue = "0"
            'Sohail (10 Mar 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to select periods from previous year only and not from current year on MSS and ESS P9A Report.
            'cboFromPeriod.SelectedValue = CStr(mintFirstPeriodId)
            'cboToPeriod.SelectedValue = CStr(mintFirstPeriodId)
            'Sohail (29 Mar 2017) -- Start
            'CCK Enhancement - 65.2 - 2. End of year P9A – Currently system open to February 2017, therefore employees can generate reports from Jan 16 to Feb 2017. In fact, this report should not have period option. Should be specific from January to December 2016. (by default first period in From period and last period in to period) 
            'cboFromPeriod.SelectedValue = "0"
            'cboToPeriod.SelectedValue = "0"
            cboFromPeriod.SelectedValue = CStr(mintFirstPeriodId)
            cboToPeriod.SelectedValue = CStr(mintLastPeriodId)
            'Sohail (29 Mar 2017) -- End
            'Sohail (10 Mar 2017) -- End
            cboE1.SelectedValue = "0"
            cboE2.SelectedValue = "0"
            cboE3.SelectedValue = "0"
            cboAmountOfInterest.SelectedValue = "0"
            cboTaxCharged.SelectedValue = "0"
            cboPersonalRelief.SelectedValue = "0"
            cboMembership.SelectedValue = "0"
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
            cboInsuranceRelief.SelectedValue = "0"
            'Sohail (18 Jan 2017) -- End
            'Sohail (08 May 2020) -- Start
            'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
            cboTaxableIcome.SelectedValue = "0"
            'Sohail (08 May 2020) -- End
            'Sohail (20 Mar 2021) -- Start
            'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
            txtBasicSalaryFormula.Text = ""
            txtTaxChargedFormula.Text = ""
            'Sohail (20 Mar 2021) -- End

            Call GetValue()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Function GetAllControls(ByVal controls As List(Of Control), ByVal t As Type, ByVal parent As Control) As List(Of Control)
        Try
            For Each c As Control In parent.Controls
                If c.GetType() Is t Then
                    controls.Add(c)
                End If
                If c.HasControls() Then
                    controls = GetAllControls(controls, t, c)
                End If
            Next
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetAllControls:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GetAllControls:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return controls
    End Function

#End Region

#Region " ComboBox's Events "
  

    Protected Sub cboAmountOfInterest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAmountOfInterest.SelectedIndexChanged, _
                                                                                                                        cboE1.SelectedIndexChanged, _
                                                                                                                        cboE2.SelectedIndexChanged, _
                                                                                                                        cboE3.SelectedIndexChanged, _
                                                                                                                        cboTaxCharged.SelectedIndexChanged, _
                                                                                                                        cboPersonalRelief.SelectedIndexChanged, _
                                                                                                                        cboQuarterValue.SelectedIndexChanged, _
                                                                                                                        cboInsuranceRelief.SelectedIndexChanged
        'Sohail (18 Jan 2017) - [cboQuarterValue.SelectedIndexChanged, cboInsuranceRelief.SelectedIndexChanged]
        Try
            Dim cbo As DropDownList = CType(sender, DropDownList)

            If CInt(cbo.SelectedValue) > 0 Then
                Dim a As List(Of Control) = GetAllControls(New List(Of Control), GetType(System.Web.UI.WebControls.DropDownList), tblP9A)
                Dim lst As IEnumerable(Of DropDownList) = a.OfType(Of System.Web.UI.WebControls.DropDownList)().Where(Function(t) t.ID <> cbo.ID AndAlso t.ID <> cboEmployee.ID AndAlso t.ID <> cboFromPeriod.ID AndAlso t.ID <> cboToPeriod.ID AndAlso CInt(CType(t, DropDownList).SelectedValue) = CInt(cbo.SelectedValue))
                If lst.Count > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, This transaction head is already mapped."), Me)
                    cbo.SelectedValue = "0"
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboAmountOfInterest_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboAmountOfInterest_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        objP9AReport = New clsP9AReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            Call SetDateFormat()

            If Not SetFilter() Then Exit Sub

            objP9AReport.generateReportNew(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                           CStr(Session("UserAccessModeSetting")), True, Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport")), _
                                           0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))

            Session("objRpt") = objP9AReport._Rpt
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReport_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)

            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblFromPeriod.Text = Language._Object.getCaption(Me.lblFromPeriod.ID, Me.lblFromPeriod.Text)
            Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.ID, Me.lblToPeriod.Text)
            Me.lblQuarterValue.Text = Language._Object.getCaption(Me.lblQuarterValue.ID, Me.lblQuarterValue.Text)
            Me.lblE1.Text = Language._Object.getCaption(Me.lblE1.ID, Me.lblE1.Text)
            Me.lblE2.Text = Language._Object.getCaption(Me.lblE2.ID, Me.lblE2.Text)
            Me.lblE3.Text = Language._Object.getCaption(Me.lblE3.ID, Me.lblE3.Text)
            Me.lblAmountOfInterest.Text = Language._Object.getCaption(Me.lblAmountOfInterest.ID, Me.lblAmountOfInterest.Text)
            Me.lblTaxCharged.Text = Language._Object.getCaption(Me.lblTaxCharged.ID, Me.lblTaxCharged.Text)
            Me.lblPersonalRelief.Text = Language._Object.getCaption(Me.lblPersonalRelief.ID, Me.lblPersonalRelief.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.ID, Me.lblMembership.Text)
            Me.lblInsuranceRelief.Text = Language._Object.getCaption(Me.lblInsuranceRelief.ID, Me.lblInsuranceRelief.Text)
            Me.lblTaxableIcome.Text = Language._Object.getCaption(Me.lblTaxableIcome.ID, Me.lblTaxableIcome.Text)
            Me.lblBasicSalaryFormula.Text = Language._Object.getCaption(Me.lblBasicSalaryFormula.ID, Me.lblBasicSalaryFormula.Text)
            Me.lblTaxChargedFormula.Text = Language._Object.getCaption(Me.lblTaxChargedFormula.ID, Me.lblTaxChargedFormula.Text)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetLanguage:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    
End Class
