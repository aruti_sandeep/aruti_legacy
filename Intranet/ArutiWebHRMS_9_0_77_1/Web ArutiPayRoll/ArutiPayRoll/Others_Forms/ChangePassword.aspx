﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="ChangePassword.aspx.vb"
    Inherits="ChangePassword" Title="Change Password" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 40%; min-width: 350px;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblTitle" runat="server" Text="Change Password"></asp:Label>
                            <asp:Label ID="lblResetTitle" runat="server" Text="Reset Password" Visible="false"></asp:Label>
                        </div>
                        <div class="panel-body" style="margin-left: 10px; margin-right: 10px; margin-bottom: 11px;">
                                    <table style="width: 100%;">
                                <asp:Panel ID="oldPwd" runat="server">
                                        <tr style="width: 100%;">
                                            <td style="width: 40%">
                                                <asp:Label ID="lblOldPwd" runat="server" Text="Old Password"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 60%">
                                                <asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password" ValidationGroup="password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="None"
                                                    ControlToValidate="txtOldPassword" ErrorMessage="Old Password is compulsory information. Please enter Old Password to continue."
                                                    CssClass="ErrorControl" ForeColor="White" ValidationGroup="password" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                                    TargetControlID="RequiredFieldValidator4" Width="300px" />
                                            </td>
                                        </tr>
                                </asp:Panel>
                                        <tr style="width: 100%;">
                                            <td style="width: 40%">
                                                <asp:Label ID="lblNewPwd" runat="server" Text="New Password"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 60%">
                                                <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" ValidationGroup="password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                                                    ControlToValidate="txtNewPassword" ErrorMessage="New Password is compulsory information. Please enter New Password to continue."
                                                    CssClass="ErrorControl" ForeColor="White" ValidationGroup="password" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 40%">
                                                <asp:Label ID="lblConfirmPwd" runat="server" Text="Confirm Password"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 60%">
                                                <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" ValidationGroup="password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="None"
                                                    ControlToValidate="txtConfirmPassword" ErrorMessage="Confirmation Password is compulsory information. Please enter Confirmation Password to continue."
                                                    CssClass="ErrorControl" ForeColor="White" ValidationGroup="password" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
                                                    TargetControlID="RequiredFieldValidator2" Width="300px" />
                                                <asp:CompareValidator ID="CompareValidator1" ControlToValidate="txtConfirmPassword"
                                                    ControlToCompare="txtNewPassword" Type="String" Operator="Equal" SetFocusOnError="true"
                                                    ErrorMessage="Password does not match. Please enter correct password." runat="Server"
                                                    Display="None" ForeColor="White" CssClass="ErrorControl" ValidationGroup="password" />
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                                    TargetControlID="CompareValidator1" Width="300px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btndefault" Text="Save" ValidationGroup="password"
                                            CausesValidation="true" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="btndefault" Text="Cancel" />
                                    </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
