<%@ Page Title="Owners Goals List" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgAllocationLvlGoalsList.aspx.vb" Inherits="wPgAllocationLvlGoalsList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <link href="../../App_Themes/PA_Style.css" type="text/css" />

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        $(".objAddBtn").live("mousedown", function(e) {
            $("#<%= hdf_locationx.ClientID %>").val(e.clientX);
            $("#<%= hdf_locationy.ClientID %>").val(e.clientY);
        });
    </script>

    <script type="text/javascript">
        $(".popMenu").live("click", function(e) {
            var id = jQuery(this).attr('id').replace('_backgroundElement', '');
            $find(id).hide();
        });
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <script type="text/javascript">
    function setscrollPosition(sender){
       sender.scrollLeft =  document.getElementById("<%=hdf_leftposition.ClientID%>").value;
       sender.scrollTop =  document.getElementById("<%=hdf_topposition.ClientID%>").value;
    }
    function getscrollPosition(){
        document.getElementById("<%=hdf_topposition.ClientID%>").value= document.getElementById("<%=pnl_dgvData.ClientID%>").scrollTop;
        document.getElementById("<%=hdf_leftposition.ClientID%>").value=document.getElementById("<%=pnl_dgvData.ClientID%>").scrollLeft;
    }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
            var scroll1 = {
                Y: '#<%= hfScrollPosition1.ClientID %>'
            };
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
                SetGeidScrolls();
                if (args.get_error() == undefined) {
                        $("#scrollable-container1").scrollTop($(scroll1.Y).val());
                }
                $(".updatedata123").html("<i class='fa fa-tasks'></i>");
            }
            $(document).ready(function(){
                $(".updatedata123").html("<i class='fa fa-tasks'></i>");
            });

    </script>

    <script type="text/javascript">
    function SetGeidScrolls()
    {
        var arrPnl=$('.gridscroll');
        for(j = 0; j < arrPnl.length; j++)
        {
            var trtag=$(arrPnl[j]).find('.gridview').children('tbody').children();
            if (trtag.length>52)
            {
                var trheight=0;
                for (i = 0; i < 52; i++) { 
                    trheight = trheight + $(trtag[i]).height();
                }
                $(arrPnl[j]).css("overflow", "auto");
                $(arrPnl[j]).css("height", trheight+"px"); 
            }
            else{
                $(arrPnl[j]).css("overflow", "auto"); 
                $(arrPnl[j]).css("height", "100%"); 
            }
        }
    }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Owners Goals List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 33%;" align="left">
                                                <asp:Label ID="lblPeriod" runat="server" Width="100%" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                <asp:Label ID="objlblField1" runat="server" Width="100%" Text="#Caption"></asp:Label>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                <asp:Label ID="objlblField4" runat="server" Width="100%" Text="#Caption"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 33%;" align="left">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                <asp:DropDownList ID="cboFieldValue1" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                <asp:DropDownList ID="cboFieldValue4" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 33%;">
                                                <asp:Label ID="lblOwnerType" runat="server" Text="Owner Type"></asp:Label>
                                            </td>
                                            <td style="width: 33%;">
                                                <asp:Label ID="objlblField2" runat="server" Text="#Caption"></asp:Label>
                                            </td>
                                            <td style="width: 33%;">
                                                <asp:Label ID="objlblField5" runat="server" Text="#Caption"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 33%;" align="left">
                                                <asp:DropDownList ID="cboOwnerType" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                <asp:DropDownList ID="cboFieldValue2" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                <asp:DropDownList ID="cboFieldValue5" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 33%;" align="left">
                                                <asp:Label ID="lblOwner" runat="server" Text="Owner"></asp:Label>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                <asp:Label ID="objlblField3" runat="server" Text="#Caption"></asp:Label>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 33%;" align="left">
                                                <asp:DropDownList ID="cboOwner" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                <asp:DropDownList ID="cboFieldValue3" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td colspan="3" style="text-align: right; padding-right: 15px; width: 99%">
                                                <asp:Label ID="objlblTotalWeight" runat="server" Text="" Font-Bold="true"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <div style="float: left">
                                            <asp:Panel ID="pnl_Button" runat="server">
                                                <asp:Button ID="btnCommit" runat="server" CssClass="btndefault" Text="Commit" />
                                                <asp:Button ID="btnUnlock" runat="server" CssClass="btndefault" Text="Unlock Committed" />
                                                <asp:Button ID="btnGlobalAssign" runat="server" CssClass="btndefault" Text="Global Assign" />
                                                <asp:Button ID="btnUpdatePercentage" runat="server" CssClass="btndefault" Text="Update % Completed"
                                                    Visible="false" />
                                            </asp:Panel>
                                        </div>
                                        <div>
                                            <asp:Button ID="BtnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                            <asp:Button ID="BtnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);" style="width: 99%;overflow:auto"
                                class="gridscroll">
                                <asp:Panel ID="pnl_dgvData" runat="server" style="text-align:left" Width="100%">
                                <asp:GridView ID="dgvData" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                    RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>
                                        <%--<asp:TemplateField HeaderText="Add" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgAdd" runat="server" CommandName="objAdd" ImageUrl="~/images/add_16.png"
                                                ToolTip="New" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CommandName="objEdit" ImageUrl="~/images/edit.png"
                                                ToolTip="Edit" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDelete" runat="server" CommandName="objDelete" ImageUrl="~/images/remove.png"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                            </div>
                            <table style="">
                                <tr id="trimageadd">
                                    <td colspan="3">
                                        <asp:HiddenField ID="hdfv" runat="server" />
                                        <cc1:ModalPopupExtender ID="popAddButton" BehaviorID="MPE" runat="server" TargetControlID="hdfv"
                                            CancelControlID="btnAddCancel" DropShadow="true" PopupControlID="pnlImageAdd"
                                            BackgroundCssClass="popMenu" Drag="True">
                                        </cc1:ModalPopupExtender>
                                        <%-- <asp:Panel ID="" runat="server" Style="cursor: move; display: none; padding: 10px;
                                            width: 140px; border-style: solid; border-width: 1px; -moz-box-shadow: 5px 5px 10px #000000;
                                            -webkit-box-shadow: 5px 5px 10px #000000; box-shadow: 5px 5px 10px #000000; -moz-border-radius: 10px;
                                            -webkit-border-radius: 10px; border-radius: 10px; -khtml-border-radius: 10px;"
                                            BackColor="#CCCCCC">
                                            <table style="vertical-align: middle; overflow: auto; margin-left: 5px; margin-top: 5px;
                                                margin-bottom: 10px;">
                                                <asp:DataList ID="" runat="server">
                                                    <AlternatingItemStyle BackColor="#DCDCDC" />
                                                    <ItemStyle BackColor="#EEEEEE" ForeColor="Black" />
                                                    <SelectedItemStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 100%; height: 25px" class="mnuTools">
                                                                <asp:LinkButton ID='objadd' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                                                    OnClick="lnkAdd_Click" CssClass="lnkhover" />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <tr>
                                                    <td style="text-align: right">
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnlImageAdd" runat="server" CssClass="" Style="display: none; width: 100%">
                                            <div class="panel-primary" style="margin-bottom: 0px;">
                                                <div class="panel-body" style="background-color: #EDF1F4; padding: 2px;">
                                                    <asp:DataList ID="dlmnuAdd" runat="server">
                                                        <AlternatingItemStyle BackColor="Transparent" />
                                                        <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                                        <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="width: 100%; height: 25px" class="mnuTools">
                                                                    <asp:LinkButton ID='objadd' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                                                        OnClick="lnkAdd_Click" CssClass="lnkhover" />
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                    <asp:HiddenField ID="btnAddCancel" runat="server" />
                                                    <asp:HiddenField ID="btndlmnuAdd" runat="server" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="trimageEdit">
                                    <td colspan="3">
                                        <cc1:ModalPopupExtender ID="popEditButton" runat="server" TargetControlID="btndlmnuEdit"
                                            CancelControlID="btnEditCancel" DropShadow="true" BackgroundCssClass="popMenu"
                                            PopupControlID="pnlImageEdit" Drag="True">
                                        </cc1:ModalPopupExtender>
                                        <%--<asp:Panel ID="pnlImageEdit" runat="server" Style="cursor: move; display: none; padding: 10px;
                                            width: 140px; border-style: solid; border-width: 1px; -moz-box-shadow: 5px 5px 10px #000000;
                                            -webkit-box-shadow: 5px 5px 10px #000000; box-shadow: 5px 5px 10px #000000; -moz-border-radius: 10px;
                                            -webkit-border-radius: 10px; border-radius: 10px; -khtml-border-radius: 10px;"
                                            BackColor="#CCCCCC">
                                            <table style="vertical-align: middle; overflow: auto; margin-left: 5px; margin-top: 5px;
                                                margin-bottom: 10px;">
                                                <asp:DataList ID="dlmnuEdit" runat="server">
                                                    <AlternatingItemStyle BackColor="#DCDCDC" />
                                                    <ItemStyle BackColor="#EEEEEE" ForeColor="Black" />
                                                    <SelectedItemStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 100%; height: 25px" class="mnuTools">
                                                                <asp:LinkButton ID='objEdit' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                                                    CssClass="lnkhover" OnClick="lnkEdit_Click" />
                                                                <asp:HiddenField ID="objEditOrignalName" runat="server" Value='<%# Eval("lnkOriginal") %>' />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <tr>
                                                    <td style="text-align: right">
                                                        <asp:HiddenField ID="btnEditCancel" runat="server" />
                                                        <asp:HiddenField ID="btndlmnuEdit" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnlImageEdit" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 100%">
                                            <div class="panel-primary" style="margin-bottom: 0px;">
                                                <div class="panel-body" style="background-color: #EDF1F4; padding: 2px;">
                                                    <asp:DataList ID="dlmnuEdit" runat="server">
                                                        <AlternatingItemStyle BackColor="Transparent" />
                                                        <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                                        <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="width: 100%; height: 25px" class="mnuTools">
                                                                    <asp:LinkButton ID='objEdit' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                                                        CssClass="lnkhover" OnClick="lnkEdit_Click" />
                                                                    <asp:HiddenField ID="objEditOrignalName" runat="server" Value='<%# Eval("lnkOriginal") %>' />
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                    <asp:HiddenField ID="btnEditCancel" runat="server" />
                                                    <asp:HiddenField ID="btndlmnuEdit" runat="server" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="trimageDelete">
                                    <td colspan="3">
                                        <cc1:ModalPopupExtender ID="popDeleteButton" runat="server" TargetControlID="btndlmnuDelete"
                                            CancelControlID="btnDeleteCancel" DropShadow="true" BackgroundCssClass="popMenu"
                                            PopupControlID="pnlImageDelete" Drag="True">
                                        </cc1:ModalPopupExtender>
                                        <%--<asp:Panel ID="pnlImageDelete" runat="server" Style="cursor: move; display: none;
                                            padding: 10px; width: 150px; border-style: solid; border-width: 1px; -moz-box-shadow: 5px 5px 10px #000000;
                                            -webkit-box-shadow: 5px 5px 10px #000000; box-shadow: 5px 5px 10px #000000; -moz-border-radius: 10px;
                                            -webkit-border-radius: 10px; border-radius: 10px; -khtml-border-radius: 10px;"
                                            BackColor="#CCCCCC">
                                            <table style="vertical-align: middle; overflow: auto; margin-left: 5px; margin-top: 5px;
                                                margin-bottom: 10px;">
                                                <asp:DataList ID="dlmnuDelete" runat="server">
                                                    <AlternatingItemStyle BackColor="#DCDCDC" />
                                                    <ItemStyle BackColor="#EEEEEE" ForeColor="Black" />
                                                    <SelectedItemStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 100%; height: 25px" class="mnuTools">
                                                                <asp:LinkButton ID='objDelete' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                                                    CssClass="lnkhover" OnClick="lnkDelete_Click" />
                                                                <asp:HiddenField ID="objDeleteOrignalName" runat="server" Value='<%# Eval("lnkOriginal") %>' />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <tr>
                                                    <td style="text-align: right">
                                                        <asp:HiddenField ID="btnDeleteCancel" runat="server" />
                                                        <asp:HiddenField ID="btndlmnuDelete" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnlImageDelete" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 100%">
                                            <div class="panel-primary" style="margin-bottom: 0px;">
                                                <div class="panel-body" style="background-color: #EDF1F4; padding: 2px;">
                                                    <asp:DataList ID="dlmnuDelete" runat="server">
                                                        <AlternatingItemStyle BackColor="Transparent" />
                                                        <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                                        <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td style="width: 100%; height: 25px" class="mnuTools">
                                                                    <asp:LinkButton ID='objDelete' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                                                        CssClass="lnkhover" OnClick="lnkDelete_Click" />
                                                                    <asp:HiddenField ID="objDeleteOrignalName" runat="server" Value='<%# Eval("lnkOriginal") %>' />
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                    <asp:HiddenField ID="btnDeleteCancel" runat="server" />
                                                    <asp:HiddenField ID="btndlmnuDelete" runat="server" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="objfrmAddEditOwrField1">
                                    <td colspan="3">
                                        <cc1:ModalPopupExtender ID="pop_objfrmAddEditOwrField1" runat="server" TargetControlID="hdf_btnOwrField1Save"
                                            CancelControlID="hdf_btnOwrField1Save" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                            PopupControlID="pnl_objfrmAddEditOwrField1" Drag="True">
                                        </cc1:ModalPopupExtender>
                                        <%--<asp:Panel ID="" runat="server" Width="750px" Height="460px"
                                            Style="display: none; background-color: #FFFFFF; border-style: solid; border-width: thin;
                                            margin-left: auto; margin-right: auto;">
                                            <div style="margin-left: 25px; margin-right: 25px; border-width: 1px; border-style: solid;
                                                float: left; background-color: #FFFFFF; margin-top: 25px; margin-bottom: 25px;">
                                                <h4 class="heading1" style="width: 100%; text-align: left">
                                                    &nbsp;
                                                </h4>
                                            </div>
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnl_objfrmAddEditOwrField1" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 100%">
                                            <div class="panel-primary" style="margin-bottom: 0px;">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblheaderOwrField1" runat="server" Text="AddEditOwrField1"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div1" class="panel-default">
                                                        <div id="Div2" class="panel-heading-default">
                                                            <div style="float: left;">
                                                            </div>
                                                        </div>
                                                        <div id="Div3" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 650px">
                                                                    <td style="width: 230px; vertical-align: top">
                                                                        <table style="width: 230px">
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:Label ID="lblOwrField1Period" runat="server" Text="Period"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 100%">
                                                                                    <asp:TextBox ID="txtOwrField1Period" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 300px">
                                                                                <td style="width: 300px">
                                                                                    <asp:Label ID="lblOwrField1GoalOwner" runat="server" Text="Owner Category"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:DropDownList ID="cboOwrField1Allocations" runat="server" Width="230px" Height="20px">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:Label ID="lblOwrField1Owner" runat="server" Text="Owner"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:DropDownList ID="cboOwrField1Owner" runat="server" Height="20px" Width="230px">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:Label ID="objOwrField1lblField1" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:DropDownList ID="cboOwrField1FieldValue1" runat="server" Height="20px" Width="230px">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:Label ID="objOwrField1lblOwrField1" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td>
                                                                                    <asp:TextBox ID="objOwrField1txtOwrField1" runat="server" TextMode="MultiLine" Height="130px"
                                                                                        Width="230px" Style="resize: none;"></asp:TextBox>
                                                                                    <asp:HiddenField ID="objOwrField1txtOwrField1Tag" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 420px; vertical-align: top">
                                                                        <asp:Panel ID="pnl_RightOwrField1" runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField1StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField1EndDate" runat="server" Text="End Date"></asp:Label>
                                                                                    </td>
                                                                                    <td rowspan="6" style="background-color: White; width: 215px">
                                                                                        <cc1:TabContainer ID="objOwrField1tabcRemarks" runat="server" Width="215px" Height="90px">
                                                                                            <cc1:TabPanel ID="objOwrField1tabpRemark1" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField1Remark1" runat="server"></asp:Label>
                                                                                                    <asp:HiddenField ID="hdfOwrField1Tag1" runat="server" />
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField1Remark1" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;" AutoPostBack="True"></asp:TextBox>
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                            <cc1:TabPanel ID="objOwrField1tabpRemark2" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField1Remark2" runat="server"></asp:Label>
                                                                                                    <asp:HiddenField ID="hdfOwrField1Tag2" runat="server" />
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField1Remark2" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                            <cc1:TabPanel ID="objOwrField1tabpRemark3" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField1Remark3" runat="server"></asp:Label>
                                                                                                    <asp:HiddenField ID="hdfOwrField1Tag3" runat="server" />
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField1Remark3" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                        </cc1:TabContainer>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <uc2:DateCtrl ID="dtpOwrField1StartDate" runat="server" Width="75" AutoPostBack="false" />
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <uc2:DateCtrl ID="dtpOwrField1EndDate" runat="server" Width="75" AutoPostBack="false" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 200px">
                                                                                        <asp:Label ID="lblOwrField1Status" runat="server" Text="Status"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 200px">
                                                                                        <asp:DropDownList ID="cboOwrField1Status" runat="server" Enabled="false" Height="25px" Width="200px">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField1Weight" runat="server" Text="Weight"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField1Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:TextBox ID="txtOwrField1Weight" runat="server" Style="text-align: right" Text="0.00"
                                                                                            Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:TextBox ID="txtOwrField1Percent" runat="server" Enabled="false" Style="text-align: right" Text="0.00"
                                                                                            Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3" style="width: 420px">
                                                                                        <asp:TextBox ID="txtOwrField1SearchEmp" runat="server" Width="420px" AutoPostBack="true"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 420px">
                                                                                    <td colspan="3" style="background-color: White">
                                                                                        <asp:Panel ID="pnl_OwrField1dgvower" runat="server" Style="height: 160px; overflow: auto;
                                                                                            border-bottom: solid 2px #DDD">
                                                                                            <asp:DataGrid ID="dgvOwrField1Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                                HeaderStyle-Font-Bold="false">
                                                                                                <Columns>
                                                                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                                                        HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                                        <HeaderTemplate>
                                                                                                            <asp:CheckBox ID="chkOwrField1allSelect" runat="server" OnCheckedChanged="chkOwrField1AllSelect_CheckedChanged"
                                                                                                                AutoPostBack="true" />
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="chkOwrField1select" runat="server" Checked='<%# Eval("ischeck") %>'
                                                                                                                AutoPostBack="true" OnCheckedChanged="chkOwrField1select_CheckedChanged" />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                                                        HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                                    <asp:BoundColumn DataField="name" HeaderText="Employee" FooterText="dgcolhEName"
                                                                                                        HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                                                                    </asp:BoundColumn>
                                                                                                </Columns>
                                                                                            </asp:DataGrid>
                                                                                        </asp:Panel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:HiddenField ID="hdf_btnOwrField1Save" runat="server" />
                                                                <asp:Button ID="btnOwrField1Save" runat="server" Text="Save" CssClass="btndefault" />
                                                                <asp:Button ID="btnOwrField1Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="objfrmAddEditOwrField2">
                                    <td colspan="3">
                                        <cc1:ModalPopupExtender ID="pop_objfrmAddEditOwrField2" runat="server" TargetControlID="hdf_btnOwrField2Save"
                                            CancelControlID="hdf_btnOwrField2Save" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                            PopupControlID="pnl_objfrmAddEditOwrField2" Drag="True">
                                        </cc1:ModalPopupExtender>
                                        <asp:Panel ID="pnl_objfrmAddEditOwrField2" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 100%">
                                            <div class="panel-primary" style="margin-bottom: 0px;">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblHeaderOwrField2" runat="server" Text="AddEditOwrField2"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div4" class="panel-default">
                                                        <div id="Div5" class="panel-heading-default">
                                                            <div style="float: left;">
                                                            </div>
                                                        </div>
                                                        <div id="Div6" class="panel-body-default">
                                                            <table style="vertical-align: middle;">
                                                                <tr style="width: 650px">
                                                                    <td style="width: 230px; vertical-align: top">
                                                                        <table style="width: 230px">
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:Label ID="lblOwrField2Period" runat="server" Text="Period"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 100%">
                                                                                    <asp:TextBox ID="txtOwrField2Period" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 300px">
                                                                                <td style="width: 300px">
                                                                                    <asp:Label ID="objOwrField2lblOwrField1" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:DropDownList ID="cboOwrField2OwrFieldValue1" runat="server" Width="230px" Height="20px"
                                                                                        AutoPostBack="true">
                                                                                    </asp:DropDownList>
                                                                                    <asp:HiddenField ID="hdf_cboOwrField2OwrFieldValue1" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:Label ID="objOwrField2lblOwrField2" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td>
                                                                                    <asp:TextBox ID="objOwrField2txtOwrField2" runat="server" TextMode="MultiLine" Width="230px"
                                                                                        Height="220px" Style="resize: none"></asp:TextBox>
                                                                                    <asp:HiddenField ID="hdf_objOwrField2txtOwrField2" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 420px; vertical-align: top">
                                                                        <asp:Panel ID="pnl_RightOwrField2" runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblStartDate" runat="server" Text="Start Date"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblEndDate" runat="server" Text="End Date"></asp:Label>
                                                                                    </td>
                                                                                    <td rowspan="6" style="background-color: White; width: 215px">
                                                                                        <cc1:TabContainer ID="objOwrField2tabcRemarks" runat="server" Width="215px" Height="90px">
                                                                                            <cc1:TabPanel ID="objOwrField2tabpRemark1" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField2Remark1" runat="server"></asp:Label>
                                                                                                    <asp:HiddenField ID="hdfOwrField2Tag1" runat="server" />
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField2Remark1" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;" AutoPostBack="True"></asp:TextBox>
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                            <cc1:TabPanel ID="objOwrField2tabpRemark2" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField2Remark2" runat="server"></asp:Label>
                                                                                                    <asp:HiddenField ID="hdfOwrField2Tag2" runat="server" />
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField2Remark2" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                            <cc1:TabPanel ID="objOwrField2tabpRemark3" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField2Remark3" runat="server"></asp:Label>
                                                                                                    <asp:HiddenField ID="hdfOwrField2Tag3" runat="server" />
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField2Remark3" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                        </cc1:TabContainer>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <uc2:DateCtrl ID="dtpOwrField2StartDate" runat="server" Width="75" />
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <uc2:DateCtrl ID="dtpOwrField2EndDate" runat="server" Width="75" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 200px">
                                                                                        <asp:Label ID="lblOwrField2Status" runat="server" Text="Status"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 200px">
                                                                                        <asp:DropDownList ID="cboOwrField2Status" runat="server" Enabled="false" Height="25px" Width="200px">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField2Weight" runat="server" Text="Weight"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField2Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:TextBox ID="txtOwrField2Weight" runat="server" Style="text-align: right" Text="0.00"
                                                                                            Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:TextBox ID="txtOwrField2Percent" runat="server" Enabled="false" Style="text-align: right" Text="0.00"
                                                                                            Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3" style="width: 420px">
                                                                                        <asp:TextBox ID="txtOwrField2SearchEmp" runat="server" AutoPostBack="true" Width="420px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 420px">
                                                                                    <td colspan="3" style="background-color: White">
                                                                                        <asp:Panel ID="pnl_OwrField2dgvower" runat="server" Style="height: 160px; overflow: auto;
                                                                                            border-bottom: solid 2px #DDD">
                                                                                            <asp:DataGrid ID="dgvOwrField2Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                                HeaderStyle-Font-Bold="false">
                                                                                                <Columns>
                                                                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                                                        HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                                        <HeaderTemplate>
                                                                                                            <asp:CheckBox ID="chkOwrField2allSelect" runat="server" OnCheckedChanged="chkOwrField2AllSelect_CheckedChanged"
                                                                                                                AutoPostBack="true" />
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="chkOwrField2select" runat="server" Checked='<%# Eval("ischeck") %>'
                                                                                                                AutoPostBack="true" OnCheckedChanged="chkOwrField2select_CheckedChanged" />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                                                        HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                                    <asp:BoundColumn DataField="name" HeaderText="Employee" FooterText="dgcolhEName"
                                                                                                        HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                                                                    </asp:BoundColumn>
                                                                                                </Columns>
                                                                                            </asp:DataGrid>
                                                                                        </asp:Panel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:HiddenField ID="hdf_btnOwrField2Save" runat="server" />
                                                                <asp:Button ID="btnOwrField2Save" runat="server" Text="Save" CssClass="btndefault" />
                                                                <asp:Button ID="btnOwrField2Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="objfrmAddEditOwrField3">
                                    <td colspan="3">
                                        <cc1:ModalPopupExtender ID="pop_objfrmAddEditOwrField3" runat="server" TargetControlID="hdf_btnOwrField3Save"
                                            CancelControlID="hdf_btnOwrField3Save" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                            PopupControlID="pnl_objfrmAddEditOwrField3" Drag="True">
                                        </cc1:ModalPopupExtender>
                                        <%-- <asp:Panel ID="" runat="server" Width="760px" Height="470px" Style="display: none;
                                            background-color: #FFFFFF; border-style: solid; border-width: thin; margin-left: auto;
                                            margin-right: auto;">
                                            <div style="margin-left: 25px; margin-right: 25px; border-width: 1px; border-style: solid;
                                                float: left; background-color: #FFFFFF; margin-top: 25px; margin-bottom: 25px;">
                                                <h4 class="heading1" style="width: 100%; text-align: left">
                                                    &nbsp;
                                                </h4>
                                            </div>
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnl_objfrmAddEditOwrField3" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 100%">
                                            <div class="panel-primary" style="margin-bottom: 0px;">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblHeaderOwrField3" runat="server" Text="AddEditOwrField3"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div7" class="panel-default">
                                                        <div id="Div8" class="panel-heading-default">
                                                            <div style="float: left;">
                                                            </div>
                                                        </div>
                                                        <div id="Div9" class="panel-body-default">
                                                            <table style="vertical-align: middle;">
                                                                <tr style="width: 650px">
                                                                    <td style="width: 230px; vertical-align: top">
                                                                        <table style="width: 230px">
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:Label ID="lblOwrField3Period" runat="server" Text="Period"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 100%">
                                                                                    <asp:TextBox ID="txtOwrField3Period" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 300px">
                                                                                <td style="width: 300px">
                                                                                    <asp:Label ID="objOwrField3lblOwrField1" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 100%">
                                                                                    <asp:TextBox ID="objOwrField3txtOwrField1" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 300px">
                                                                                <td style="width: 300px">
                                                                                    <asp:Label ID="objOwrField3lblOwrField2" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:DropDownList ID="cboOwrField3OwrFieldValue2" runat="server" Width="230px" Height="20px"
                                                                                        AutoPostBack="true">
                                                                                    </asp:DropDownList>
                                                                                    <asp:HiddenField ID="hdf_cboOwrField3OwrFieldValue2" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:Label ID="objOwrField3lblOwrField3" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td>
                                                                                    <asp:TextBox ID="objOwrField3txtOwrField3" runat="server" TextMode="MultiLine" Rows="5"
                                                                                        Width="230px" Height="170px"></asp:TextBox>
                                                                                    <asp:HiddenField ID="hdf_objOwrField3txtOwrField3" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 420px; vertical-align: top">
                                                                        <asp:Panel ID="pnl_RightOwrField3" runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField3StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField3EndDate" runat="server" Text="End Date"></asp:Label>
                                                                                    </td>
                                                                                    <td rowspan="6" style="background-color: White; width: 215px">
                                                                                        <cc1:TabContainer ID="objOwrField3tabcRemarks" runat="server" Width="215px" Height="90px">
                                                                                            <cc1:TabPanel ID="objOwrField3tabpRemark1" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField3Remark1" runat="server"></asp:Label>
                                                                                                    <asp:HiddenField ID="hdf_lblOwrField3Remark1" runat="server" />
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField3Remark1" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;"></asp:TextBox>
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                            <cc1:TabPanel ID="objOwrField3tabpRemark2" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField3Remark2" runat="server"></asp:Label>
                                                                                                    <asp:HiddenField ID="hdf_lblOwrField3Remark2" runat="server" />
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField3Remark2" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;"></asp:TextBox>
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                            <cc1:TabPanel ID="objOwrField3tabpRemark3" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField3Remark3" runat="server"></asp:Label>
                                                                                                    <asp:HiddenField ID="hdf_lblOwrField3Remark3" runat="server" />
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField3Remark3" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;"></asp:TextBox>
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                        </cc1:TabContainer>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <uc2:DateCtrl ID="dtpOwrField3StartDate" runat="server" Width="75" />
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <uc2:DateCtrl ID="dtpOwrField3EndDate" runat="server" Width="75" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 200px">
                                                                                        <asp:Label ID="lblOwrField3Status" runat="server" Text="Status"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 200px">
                                                                                        <asp:DropDownList ID="cboOwrField3Status" runat="server" Enabled="false" Height="25px" Width="200px">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField3Weight" runat="server" Text="Weight"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField3Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:TextBox ID="txtOwrField3Weight" runat="server" Style="text-align: right" Text="0.00"
                                                                                            Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:TextBox ID="txtOwrField3Percent" runat="server" Enabled="false" Style="text-align: right" Text="0.00"
                                                                                            Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3" style="width: 420px">
                                                                                        <asp:TextBox ID="txtOwrField3SearchEmp" runat="server" AutoPostBack="true" Width="420px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 420px">
                                                                                    <td colspan="3" style="background-color: White">
                                                                                        <asp:Panel ID="pnl_OwrField3dgvOwner" runat="server" Style="height: 160px; overflow: auto;
                                                                                            border-bottom: solid 2px #DDD">
                                                                                            <asp:DataGrid ID="dgvOwrField3Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                                HeaderStyle-Font-Bold="false">
                                                                                                <Columns>
                                                                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                                        <HeaderTemplate>
                                                                                                            <asp:CheckBox ID="chkOwrField3allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkOwrField3AllSelect_CheckedChanged" />
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="chkOwrField3select" runat="server" Checked='<%# Eval("ischeck")  %>'
                                                                                                                AutoPostBack="true" OnCheckedChanged="chkOwrField3select_CheckedChanged" />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                                                        HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                                    <asp:BoundColumn DataField="name" HeaderText="Employee" FooterText="dgcolhEName"
                                                                                                        HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                                                                    </asp:BoundColumn>
                                                                                                </Columns>
                                                                                            </asp:DataGrid>
                                                                                        </asp:Panel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:HiddenField ID="hdf_btnOwrField3Save" runat="server" />
                                                                <asp:Button ID="btnOwrField3Save" runat="server" Text="Save" CssClass="btndefault" />
                                                                <asp:Button ID="btnOwrField3Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="objfrmAddEditOwrField4">
                                    <td colspan="3">
                                        <cc1:ModalPopupExtender ID="pop_objfrmAddEditOwrField4" runat="server" TargetControlID="hdf_btnOwrField4Save"
                                            CancelControlID="hdf_btnOwrField4Save" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                            PopupControlID="pnl_objfrmAddEditOwrField4" Drag="True">
                                        </cc1:ModalPopupExtender>
                                        <%--<asp:Panel ID="" runat="server" Width="760px" Height="470px" Style="display: none;
                                            background-color: #FFFFFF; border-style: solid; border-width: thin; margin-left: auto;
                                            margin-right: auto;">
                                            <div style="margin-left: 25px; margin-right: 25px; border-width: 1px; border-style: solid;
                                                float: left; background-color: #FFFFFF; margin-top: 25px; margin-bottom: 25px;">
                                                <h4 class="heading1" style="width: 100%; text-align: left">
                                                    &nbsp;
                                                </h4>
                                            </div>
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnl_objfrmAddEditOwrField4" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 100%">
                                            <div class="panel-primary" style="margin-bottom: 0px;">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblHeaderOwrField4" runat="server" Text="AddEditOwrField3"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div10" class="panel-default">
                                                        <div id="Div11" class="panel-heading-default">
                                                            <div style="float: left;">
                                                            </div>
                                                        </div>
                                                        <div id="Div12" class="panel-body-default">
                                                            <table style="vertical-align: middle;">
                                                                <tr style="width: 650px">
                                                                    <td style="width: 230px; vertical-align: top">
                                                                        <table style="width: 230px">
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:Label ID="lblOwrField4Period" runat="server" Text="Period"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 100%">
                                                                                    <asp:TextBox ID="txtOwrField4Period" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 300px">
                                                                                <td style="width: 300px">
                                                                                    <asp:Label ID="objOwrField4lblOwrField1" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 100%">
                                                                                    <asp:TextBox ID="objOwrField4txtOwrField1" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 300px">
                                                                                <td style="width: 300px">
                                                                                    <asp:Label ID="objOwrField4lblOwrField2" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 100%">
                                                                                    <asp:TextBox ID="objOwrField4txtOwrField2" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 300px">
                                                                                <td style="width: 300px">
                                                                                    <asp:Label ID="objOwrField4lblOwrField3" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:DropDownList ID="cboOwrField4OwrFieldValue3" runat="server" Width="230px" Height="20px"
                                                                                        AutoPostBack="true">
                                                                                    </asp:DropDownList>
                                                                                    <asp:HiddenField ID="hdf_cboOwrField4OwrFieldValue3" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:Label ID="objOwrField4lblOwrField4" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td>
                                                                                    <asp:TextBox ID="objOwrField4txtOwrField4" runat="server" TextMode="MultiLine" Style="resize: none"
                                                                                        Width="230px" Height="150px"></asp:TextBox>
                                                                                    <asp:HiddenField ID="hdf_objOwrField4txtOwrField4" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 420px; vertical-align: top">
                                                                        <asp:Panel ID="pnl_RightOwrField4" runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField4StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField4EndDate" runat="server" Text="End Date"></asp:Label>
                                                                                    </td>
                                                                                    <td rowspan="6" style="background-color: White; width: 215px">
                                                                                        <cc1:TabContainer ID="objOwrField4tabcRemarks" runat="server" Width="215px" Height="90px">
                                                                                            <cc1:TabPanel ID="objOwrField4tabpRemark1" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField4Remark1" runat="server"></asp:Label>
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField4Remark1" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdf_txtOwrField4Remark1" runat="server" />
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                            <cc1:TabPanel ID="objOwrField4tabpRemark2" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField4Remark2" runat="server"></asp:Label>
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField4Remark2" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdf_txtOwrField4Remark2" runat="server" />
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                            <cc1:TabPanel ID="objOwrField4tabpRemark3" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField4Remark3" runat="server"></asp:Label>
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField4Remark3" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdf_txtOwrField4Remark3" runat="server" />
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                        </cc1:TabContainer>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <uc2:DateCtrl ID="dtpOwrField4StartDate" runat="server" Width="75" />
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <uc2:DateCtrl ID="dtpOwrField4EndDate" runat="server" Width="75" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 200px">
                                                                                        <asp:Label ID="lblOwrField4Status" runat="server" Text="Status"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 200px">
                                                                                        <asp:DropDownList ID="cboOwrField4Status" Enabled="false" runat="server" Height="25px" Width="200px">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField4Weight" runat="server" Text="Weight"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField4Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:TextBox ID="txtOwrField4Weight" runat="server" Style="text-align: right" Text="0.00"
                                                                                            Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:TextBox ID="txtOwrField4Percent" runat="server" Enabled="false" Style="text-align: right" Text="0.00"
                                                                                            Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3" style="width: 420px">
                                                                                        <asp:TextBox ID="txtOwrField4SearchEmp" runat="server" AutoPostBack="true" Width="420px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 420px">
                                                                                    <td colspan="3" style="background-color: White">
                                                                                        <asp:Panel ID="pnl_OwrField4dgvOwner" runat="server" Style="height: 185px; overflow: auto;
                                                                                            border-bottom: solid 2px #DDD">
                                                                                            <asp:DataGrid ID="dgvOwrField4Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                                HeaderStyle-Font-Bold="false">
                                                                                                <Columns>
                                                                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                                        <HeaderTemplate>
                                                                                                            <asp:CheckBox ID="chkOwrField4allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkOwrField4AllSelect_CheckedChanged" />
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="chkOwrField4select" runat="server" Checked='<%# Eval("ischeck") %>'
                                                                                                                AutoPostBack="true" OnCheckedChanged="chkOwrField4select_CheckedChanged" />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                                                        HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                                    <asp:BoundColumn DataField="name" HeaderText="Employee" FooterText="dgcolhEName"
                                                                                                        HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                                                                    </asp:BoundColumn>
                                                                                                </Columns>
                                                                                            </asp:DataGrid>
                                                                                        </asp:Panel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:HiddenField ID="hdf_btnOwrField4Save" runat="server" />
                                                                <asp:Button ID="btnOwrField4Save" runat="server" Text="Save" CssClass="btndefault" />
                                                                <asp:Button ID="btnOwrField4Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="objfrmAddEditOwrField5">
                                    <td colspan="3">
                                        <cc1:ModalPopupExtender ID="pop_objfrmAddEditOwrField5" runat="server" TargetControlID="hdf_btnOwrField5Save"
                                            CancelControlID="hdf_btnOwrField5Save" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                            PopupControlID="pnl_objfrmAddEditOwrField5" Drag="True">
                                        </cc1:ModalPopupExtender>
                                        <%--<asp:Panel ID="" runat="server" Width="765px" Height="550px" Style="display: none;
                                            background-color: #FFFFFF; border-style: solid; border-width: thin; margin-left: auto;
                                            margin-right: auto;">
                                            
                                            <div style="margin-left: 25px; margin-right: 25px; border-width: 1px; border-style: solid;
                                                float: left; background-color: #FFFFFF; margin-top: 25px; margin-bottom: 25px;">
                                                <h4 class="heading1" style="width: 100%; text-align: left">
                                                    &nbsp;
                                                </h4>
                                            </div>
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnl_objfrmAddEditOwrField5" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 100%">
                                            <div class="panel-primary" style="margin-bottom: 0px;">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblHeaderOwrField5" runat="server" Text="AddEditOwrField5"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div13" class="panel-default">
                                                        <div id="Div14" class="panel-heading-default">
                                                            <div style="float: left;">
                                                            </div>
                                                        </div>
                                                        <div id="Div15" class="panel-body-default">
                                                            <table style="vertical-align: middle;">
                                                                <tr style="width: 650px">
                                                                    <td style="width: 230px; vertical-align: top">
                                                                        <table style="width: 230px">
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:Label ID="lblOwrField5Period" runat="server" Text="Period"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 100%">
                                                                                    <asp:TextBox ID="txtOwrField5Period" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 300px">
                                                                                <td style="width: 300px">
                                                                                    <asp:Label ID="objOwrField5lblOwrField1" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 100%">
                                                                                    <asp:TextBox ID="objOwrField5txtOwrField1" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 300px">
                                                                                <td style="width: 300px">
                                                                                    <asp:Label ID="objOwrField5lblOwrField2" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 100%">
                                                                                    <asp:TextBox ID="objOwrField5txtOwrField2" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 300px">
                                                                                <td style="width: 300px">
                                                                                    <asp:Label ID="objOwrField5lblOwrField3" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 100%">
                                                                                    <asp:TextBox ID="objOwrField5txtOwrField3" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 300px">
                                                                                <td style="width: 300px">
                                                                                    <asp:Label ID="objOwrField5lblOwrField4" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:DropDownList ID="cboOwrField5OwrFieldValue4" runat="server" Width="230px" Height="20px"
                                                                                        AutoPostBack="true">
                                                                                    </asp:DropDownList>
                                                                                    <asp:HiddenField ID="hdf_cboOwrField5OwrFieldValue4" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td style="width: 230px">
                                                                                    <asp:Label ID="objOwrField5lblOwrField5" runat="server" Text="#Caption"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 230px">
                                                                                <td>
                                                                                    <asp:TextBox ID="objOwrField5txtOwrField5" runat="server" TextMode="MultiLine" Width="230px"
                                                                                        Height="155px" Style="resize: none"></asp:TextBox>
                                                                                    <asp:HiddenField ID="hdf_objOwrField5txtOwrField5" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="width: 420px; vertical-align: top">
                                                                        <asp:Panel ID="pnl_RightOwrField5" runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField5StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField5EndDate" runat="server" Text="End Date"></asp:Label>
                                                                                    </td>
                                                                                    <td rowspan="6" style="background-color: White; width: 215px">
                                                                                        <cc1:TabContainer ID="objOwrField5tabcRemarks" runat="server" Width="215px" Height="90px">
                                                                                            <cc1:TabPanel ID="objOwrField5tabpRemark1" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField5Remark1" runat="server"></asp:Label>
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField5Remark1" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdf_txtOwrField5Remark1" runat="server" />
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                            <cc1:TabPanel ID="objOwrField5tabpRemark2" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField5Remark2" runat="server"></asp:Label>
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField5Remark2" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdf_txtOwrField5Remark2" runat="server" />
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                            <cc1:TabPanel ID="objOwrField5tabpRemark3" runat="server" HeaderText="">
                                                                                                <HeaderTemplate>
                                                                                                    <asp:Label ID="lblOwrField5Remark3" runat="server"></asp:Label>
                                                                                                </HeaderTemplate>
                                                                                                <ContentTemplate>
                                                                                                    <asp:TextBox ID="txtOwrField5Remark3" runat="server" TextMode="MultiLine" Width="97%"
                                                                                                        Height="75px" Style="resize: none;"></asp:TextBox>
                                                                                                    <asp:HiddenField ID="hdf_txtOwrField5Remark3" runat="server" />
                                                                                                </ContentTemplate>
                                                                                            </cc1:TabPanel>
                                                                                        </cc1:TabContainer>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <uc2:DateCtrl ID="dtpOwrField5StartDate" runat="server" Width="75" />
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <uc2:DateCtrl ID="dtpOwrField5EndDate" runat="server" Width="75" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 200px">
                                                                                        <asp:Label ID="lblOwrField5Status" runat="server" Text="Status"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" style="width: 200px">
                                                                                        <asp:DropDownList ID="cboOwrField5Status" Enabled="false" runat="server" Height="25px" Width="200px">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField5Weight" runat="server" Text="Weight"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:Label ID="lblOwrField5Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">
                                                                                        <asp:TextBox ID="txtOwrField5Weight" runat="server" Style="text-align: right" Text="0.00"
                                                                                            Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                    <td style="width: 100px">
                                                                                        <asp:TextBox ID="txtOwrField5Percent" runat="server" Enabled="false" Style="text-align: right" Text="0.00"
                                                                                            Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3" style="width: 420px">
                                                                                        <asp:TextBox ID="txtOwrField5SearchEmp" runat="server" AutoPostBack="true" Width="420px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 420px">
                                                                                    <td colspan="3" style="background-color: White">
                                                                                        <asp:Panel ID="pnl_OwrField5dgvOwner" runat="server" Style="height: 240px; overflow: auto;
                                                                                            border-bottom: solid 2px #DDD">
                                                                                            <asp:DataGrid ID="dgvOwrField5Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                                HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                                HeaderStyle-Font-Bold="false">
                                                                                                <Columns>
                                                                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                                        <HeaderTemplate>
                                                                                                            <asp:CheckBox ID="chkOwrField5allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkOwrField5AllSelect_CheckedChanged" />
                                                                                                        </HeaderTemplate>
                                                                                                        <ItemTemplate>
                                                                                                            <asp:CheckBox ID="chkOwrField5select" runat="server" AutoPostBack="true" OnCheckedChanged="chkOwrField5select_CheckedChanged" />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateColumn>
                                                                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                                                        HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                                    <asp:BoundColumn DataField="name" HeaderText="Employee" FooterText="dgcolhEName"
                                                                                                        HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                                                                    </asp:BoundColumn>
                                                                                                </Columns>
                                                                                            </asp:DataGrid>
                                                                                        </asp:Panel>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:HiddenField ID="hdf_btnOwrField5Save" runat="server" />
                                                                <asp:Button ID="btnOwrField5Save" runat="server" Text="Save" CssClass="btndefault" />
                                                                <asp:Button ID="btnOwrField5Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="objfrmUpdateFieldValue">
                                    <td colspan="3">
                                        <cc1:ModalPopupExtender ID="popup_objfrmUpdateFieldValue" runat="server" BackgroundCssClass="ModalPopupBG"
                                            PopupControlID="pnl_objfrmUpdateFieldValue" TargetControlID="hdf_btnUpdateFieldvalue"
                                            CancelControlID="hdf_btnUpdateFieldvalue" Drag="true" PopupDragHandleControlID="pnl_objfrmUpdateFieldValue">
                                        </cc1:ModalPopupExtender>
                                        <asp:Panel ID="pnl_objfrmUpdateFieldValue" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 700px">
                                            <div class="panel-primary" style="margin: 0px">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblpopupHeader" runat="server" Text="Update Progress"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div22" class="panel-default">
                                                        <div id="Div23" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblDetailUpdateHeader" runat="server" Text="Update Progress"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div24" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 14%">
                                                                        <asp:Label ID="lblUpdatePeriod" runat="server" Text="Period"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 46%" colspan="3">
                                                                        <asp:TextBox ID="txtUpdatePeriod" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 40%">
                                                                        <asp:Label ID="lblUpdateGoals" runat="server" Text="Goal"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 14%">
                                                                        <asp:Label ID="lblUpdateOwner" runat="server" Text="Owner"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 46%" colspan="3">
                                                                        <asp:TextBox ID="txtUpdateOwner" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 40%" rowspan="3">
                                                                        <asp:TextBox ID="txtUpdateGoals" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 14%">
                                                                        <asp:Label ID="lblUpdateDate" runat="server" Text="Change Date"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <uc2:DateCtrl ID="dtpUpdateChangeDate" Width="80" runat="server" />
                                                                    </td>
                                                                    <td style="width: 15%">
                                                                        <asp:Label ID="lblUpdatePercentage" runat="server" Text="% Completed"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 11%">
                                                                        <asp:TextBox ID="txtUpdatePercent" runat="server" Text="0.00" onKeypress="return onlyNumbers(this, event);"
                                                                            Style="text-align: right"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 40%">
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 14%">
                                                                        <asp:Label ID="lblUpdateStatus" runat="server" Text="Status"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 46%" colspan="3">
                                                                        <asp:DropDownList ID="cboUpdateStatus" runat="server" Width="295px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 40%">
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 14%" valign="top">
                                                                        <asp:Label ID="lblUpdateRemark" runat="server" Text="Remark"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 86%" colspan="4">
                                                                        <asp:TextBox ID="txtUpdateRemark" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnUpdateSave" runat="server" Text="Save" CssClass="btndefault" />
                                                                <asp:Button ID="btnUpdateClose" runat="server" Text="Close" CssClass="btndefault" />
                                                                <asp:HiddenField ID="hdf_btnUpdateFieldvalue" runat="server" />
                                                            </div>
                                                        </div>
                                                        <asp:Panel ID="pnldgvHistoryUpdateProgress" ScrollBars="Auto" Style="max-height: 250px;
                                                            margin-top: 10px; margin-bottom: 10px; margin-left: 10px; margin-right: 10px"
                                                            runat="server">
                                                            <asp:DataGrid ID="dgvHistory" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                HeaderStyle-Font-Bold="false" Width="99%">
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                        FooterText="brnEdit">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit"></asp:LinkButton>
                                                                            </span>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                        FooterText="btnDelete">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                                    CommandName="Delete" CssClass="griddelete"></asp:LinkButton>
                                                                            </span>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="ddate" HeaderText="Date" FooterText="dgcolhDate"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="pct_completed" HeaderText="% Completed" HeaderStyle-HorizontalAlign="Right"
                                                                        ItemStyle-HorizontalAlign="Right" FooterText="dgcolhPercent"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="dstatus" HeaderText="Status" FooterText="dgcolhStatus">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="remark" HeaderText="Remark" FooterText="dgcolhRemark">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="owrupdatetranunkid" HeaderText="objdgcolhUnkid" Visible="false"
                                                                        FooterText="objdgcolhUnkid"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="UpdateYesNO">
                                    <td colspan="3">
                                        <cc1:ModalPopupExtender ID="popup_UpdateYesNO" runat="server" BackgroundCssClass="ModalPopupBG"
                                            CancelControlID="hdf_UpdateYesNo" PopupControlID="pnl_UpdateYesNO" TargetControlID="hdf_UpdateYesNo">
                                        </cc1:ModalPopupExtender>
                                        <asp:Panel ID="pnl_UpdateYesNO" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 450px">
                                            <div class="panel-primary" style="margin-bottom: 0px;">
                                                <div class="panel-heading">
                                                    <asp:Label ID="Label1" runat="server" Text="Aruti"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div33" class="panel-default">
                                                        <div id="Div35" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:Label ID="lblUpdateMessages" runat="server" Text="Message :" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnupdateYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                                <asp:Button ID="btnupdateNo" runat="server" Text="No" CssClass="btnDefault" />
                                                                <asp:HiddenField ID="hdf_UpdateYesNo" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="hiddenFiled">
                                    <td colspan="3">
                                        <asp:HiddenField ID="hdf_locationx" runat="server" />
                                        <asp:HiddenField ID="hdf_locationy" runat="server" />
                                    </td>
                                </tr>
                                <tr id="YesNoPopup">
                                    <td colspan="3">
                                        <cc1:ModalPopupExtender ID="popup_YesNo" runat="server" BackgroundCssClass="ModalPopupBG"
                                            CancelControlID="btnNo" PopupControlID="pnl_YesNo" TargetControlID="hdf_popupYesNo">
                                        </cc1:ModalPopupExtender>
                                        <%--<asp:Panel ID="" runat="server" Style="display: none; padding: 10px; width: ;
                                            border-style: solid; border-width: 1px; -moz-box-shadow: 5px 5px 10px #000000;
                                            -webkit-box-shadow: 5px 5px 10px #000000; box-shadow: 5px 5px 10px #000000; -moz-border-radius: 10px;
                                            -webkit-border-radius: 10px; border-radius: 10px; -khtml-border-radius: 10px;"
                                            BackColor="#5377A9" DefaultButton="btnYes">
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnl_YesNo" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 450px">
                                            <div class="panel-primary" style="margin-bottom: 0px">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div16" class="panel-default">
                                                        <div id="Div17" class="panel-heading-default" style="height: 50px">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblMessage" runat="server" Text="Message :"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div18" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="3" Style="resize: none;
                                                                            width: 98%"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                                <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btnDefault" />
                                                                <asp:HiddenField ID="hdf_popupYesNo" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="popupCommited">
                                    <td colspan="3">
                                        <%--<cc1:ModalPopupExtender ID="" runat="server" BackgroundCssClass="ModalPopupBG"
                                            CancelControlID="btnCmtNo" PopupControlID="pnl_Commit" TargetControlID="hdf_cmtYesNo">
                                        </cc1:ModalPopupExtender>
                                        <asp:Panel ID="pnl_Commit" runat="server" Style="display: none; padding: 10px; width: 300px;
                                            border-style: solid; border-width: 1px; -moz-box-shadow: 5px 5px 10px #000000;
                                            -webkit-box-shadow: 5px 5px 10px #000000; box-shadow: 5px 5px 10px #000000; -moz-border-radius: 10px;
                                            -webkit-border-radius: 10px; border-radius: 10px; -khtml-border-radius: 10px;"
                                            BackColor="#5377A9" DefaultButton="btnYes">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblCmtTitle" runat="server" Text="Title" Width="270px" ForeColor="White"
                                                            Style="margin-left: 5px;" />
                                                        <hr style="display: block; -webkit-margin-before: 0.5em; -webkit-margin-after: 0.5em;
                                                            -webkit-margin-start: auto; -webkit-margin-end: auto; border-style: inset; border-width: 1px;" />
                                                    </td>
                                                </tr>
                                                <tr style="vertical-align: middle; overflow: auto; margin-left: 5px; margin-top: 5px;
                                                    margin-bottom: 20px;">
                                                    <td>
                                                        <asp:Label ID="lblCmtMessage" runat="server" Text="Message :" Width="270px" ForeColor="White" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right;">
                                                        <asp:Button ID="btnCmtYes" runat="server" Text="Yes" Width="70px" Style="margin-right: 10px"
                                                            CssClass="btnDefault" />
                                                        <asp:Button ID="btnCmtNo" runat="server" Text="No" Width="70px" Style="margin-right: 10px"
                                                            CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdf_cmtYesNo" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>--%>
                                        <ucCfnYesno:Confirmation ID="popup_Commit" runat="server" Message="" Title="Confirmation" />
                                    </td>
                                </tr>
                                <tr id="popupUnCommited">
                                    <td colspan="3">
                                        <cc1:ModalPopupExtender ID="popup_UnCommit" runat="server" BackgroundCssClass="ModalPopupBG"
                                            CancelControlID="btnUnCmtNo" PopupControlID="pnl_UnCommit" TargetControlID="hdf_UnCmtYesNo">
                                        </cc1:ModalPopupExtender>
                                        <%--<asp:Panel ID="" runat="server" Style="display: none; padding: 10px; width: 300px;
                                            border-style: solid; border-width: 1px; -moz-box-shadow: 5px 5px 10px #000000;
                                            -webkit-box-shadow: 5px 5px 10px #000000; box-shadow: 5px 5px 10px #000000; -moz-border-radius: 10px;
                                            -webkit-border-radius: 10px; border-radius: 10px; -khtml-border-radius: 10px;"
                                            BackColor="#5377A9" DefaultButton="btnYes">
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnl_UnCommit" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 450px">
                                            <div class="panel-primary" style="margin-bottom: 0px;">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblUnCmtTitle" runat="server" Text="Title"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div19" class="panel-default">
                                                        <div id="Div20" class="panel-heading-default" style="height: 50px">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblUnCmtMessage" runat="server" Text="Message :"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div21" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtUnCmtMessage" runat="server" TextMode="MultiLine" Rows="3" Style="resize: none;
                                                                            width: 98%"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnUnCmtYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                                <asp:Button ID="btnUnCmtNo" runat="server" Text="No" CssClass="btnDefault" />
                                                                <asp:HiddenField ID="hdf_UnCmtYesNo" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="HiddenFieldSetScroll">
                                    <td colspan="3">
                                        <asp:HiddenField ID="hdf_topposition" runat="server" />
                                        <asp:HiddenField ID="hdf_leftposition" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_UpdateProgressDeleteReason" runat="server" Title="Aruti"
                        CancelControlName="hdf_btnUpdateFieldvalue" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
