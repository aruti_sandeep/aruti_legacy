﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgUnlockLockedEmployee.aspx.vb" Inherits="Assessment_New_Performance_Goals_wPgUnlockLockedEmployee" %>

<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        $("[id*=chkHeder1]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    //debugger;
                    $(this).attr("checked", "checked");

                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        $("[id*=chkbox1]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];
            //debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");

            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }

        });
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server" EnableViewState="true">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Unlock Employee(s)"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div49" class="panel-default ib" style="width: 66%; vertical-align: top">
                                <div id="Div50" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblUnlockFC" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="Div51" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 22%">
                                            <asp:Label ID="lblUnlockPeriod" runat="server" Width="100%" Text="Period"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:DropDownList ID="cboUnlockPeriod" runat="server" Width="99%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 22%">
                                            <asp:Label ID="lblLockType" runat="server" Width="100%" Text="Select Lock Type"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:DropDownList ID="cboLockType" runat="server" Width="99%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:LinkButton ID="objlblPeriodDates" runat="server" Text="" Style="float: left"
                                            Font-Underline="false" Enabled="false" Font-Bold="true"></asp:LinkButton>
                                        <asp:Button ID="btnUSearch" runat="server" Text="Search" class="btndefault" />
                                        <asp:Button ID="btnUReset" runat="server" Text="Reset" class="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div55" class="panel-default ib" style="width: 30%">
                                <div id="Div56" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblUSetInfo" runat="server" Text="Set Information"></asp:Label>
                                    </div>
                                </div>
                                <div id="Div57" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 58%">
                                            <asp:Label ID="lblSetDays" runat="server" Width="100%" Text="Set Next Lock Days"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 30%">
                                            <asp:TextBox ID="txtNextDays" runat="server" Enabled="false" Style="text-align: right;
                                                background-color: White"></asp:TextBox>
                                            <cc1:NumericUpDownExtender ID="nudNextDays" Width="80" runat="server" Minimum="0"
                                                TargetControlID="txtNextDays">
                                            </cc1:NumericUpDownExtender>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 80%">
                                            <asp:RadioButton ID="radApplyTochecked" runat="server" GroupName="setinfo" Text="Apply To Checked" />
                                            <asp:RadioButton ID="radApplyToAll" runat="server" GroupName="setinfo" Text="Apply To All" />
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnUpdateEmp" runat="server" Text="Update" class="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div42" class="panel-default">
                                <div id="Div46" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                        <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Height="150px">
                                            <asp:GridView ID="dgvUnlockEmp" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="lockunkid,employeeunkid,unlockdays,oName,emp_email">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-Width="10">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Employee" HeaderText="Emplyoee" FooterText="dgcolhEmployee" />
                                                    <asp:BoundField DataField="lockunlockdatetime" HeaderText="Lock Date" FooterText="dgcolhlockunlockdatetime" />
                                                    <asp:BoundField DataField="udays" HeaderText="Unlock Day(s)" FooterText="dgcolhudays" />
                                                    <asp:BoundField DataField="nextlockdatetime" HeaderText="Next Lock Date" FooterText="dgcolhnextlockdatetime" />
                                                    <asp:BoundField DataField="message" HeaderText="Message" FooterText="dgcolhmessage" />
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <div id="Div38" class="panel-default">
                                <div id="Div39" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                            <asp:Label ID="lblUnlockEmpRemark" runat="server" Width="100%" Text="Remark"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 80%">
                                            <asp:TextBox ID="txtUnlockEmpRemark" runat="server" Width="124%" TextMode="MultiLine"
                                                Rows="3"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="btnfixedbottom" class="btn-default">
                                <asp:Button ID="btnUProcess" runat="server" Text="Process" CssClass="btnDefault" />
                                <asp:Button ID="btnUClose" runat="server" Text="Close" CssClass="btnDefault" />
                                <asp:HiddenField ID="hdf_UnlockEmp" runat="server" />
                            </div>
                        </div>
                    </div>
                    <ucCfnYesno:Confirmation ID="cnfUnlockDays" runat="server" Title="Aruti" />
                    <ucCfnYesno:Confirmation ID="cnfUnlockCheck" runat="server" Title="Aruti" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
