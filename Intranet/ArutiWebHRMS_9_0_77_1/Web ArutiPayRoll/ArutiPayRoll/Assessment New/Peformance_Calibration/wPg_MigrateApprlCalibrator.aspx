﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_MigrateApprlCalibrator.aspx.vb" Inherits="Assessment_New_Peformance_Calibration_wPg_MigrateApprlCalibrator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        var scroll2 = {
            Y: '#<%= hfScrollPosition2.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            SetGeidScrolls();
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
                $("#scrollable-container2").scrollTop($(scroll2.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("overflow", "auto");
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("overflow", "none");
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>

    <script language="javascript" type="text/javascript">
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtFrmSearch').val().length > 0) {
                $('#<%= dgvFrmEmp.ClientID %> tbody tr').hide();
                $('#<%= dgvFrmEmp.ClientID %> tbody tr:first').show();
                $('#<%= dgvFrmEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtFrmSearch').val() + '\')').parent().show();
            }
            else if ($('#txtFrmSearch').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= dgvFrmEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
                //$('#<%= dgvFrmEmp.ClientID %>').append('<tr class="norecords"><td colspan="3" class="Normal" style="text-align: center">No records were found</td></tr>');
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtFrmSearch').val('');
            $('#<%= dgvFrmEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtFrmSearch').focus();
        }

        function ToSearching() {
            if ($('#txtToSearch').val().length > 0) {
                $('#<%= dgvToEmp.ClientID %> tbody tr').hide();
                $('#<%= dgvToEmp.ClientID %> tbody tr:first').show();
                $('#<%= dgvToEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtToSearch').val() + '\')').parent().show();
            }
            else if ($('#txtToSearch').val().length == 0) {
                resetToSearchValue();
            }
            if ($('#<%= dgvToEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
                //$('#<%= dgvToEmp.ClientID %>').append('<tr class="norecords"><td colspan="3" class="Normal" style="text-align: center">No records were found</td></tr>');
            }

            if (event.keyCode == 27) {
                resetToSearchValue();
            }
        }
        function resetToSearchValue() {
            $('#txtToSearch').val('');
            $('#<%= dgvToEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtToSearch').focus();
        }

        //        function clonerow() {
        //            $("#<%=dgvFrmEmp.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
        //                if ($(this).is(':checked')) {
        //                    var grd = document.getElementById("<%=dgvToEmp.ClientID%>");
        //                    var tbod = grd.rows[0].parentNode;
        //                    var newRow = grd.rows[grd.rows.length - 1].cloneNode(true);
        //                    tbod.appendChild(newRow);
        //                    $(newRow).closest("tr").find("td:eq(0)").html($(this).closest("tr").find("td:eq(1)").text());
        //                    $(newRow).closest("tr").find("td:eq(1)").html($(this).closest("tr").find("td:eq(2)").text());
        //                    $(this).closest("tr").remove();
        //                }
        //            });
        //        }

        function isvalidTransfer() {
            var pfid = document.getElementById("<%=cboFromValue.ClientID%>");
            var ptid = document.getElementById("<%=cboToValue.ClientID%>");
            var radioButtons = document.getElementsByName("<%=radMode.UniqueID%>");
            var radOpr = document.getElementsByName("<%=radOperation.UniqueID%>");
            var oprtypid = 1;
            if (radOpr != null) {
                for (var x = 0; x < radioButtons.length; x++) {
                    if (radioButtons[x].checked) {
                        oprtypid = radOpr[x].value;
                    }
                }
            }
            var blncalibrator = false;
            if (radioButtons != null) {
                for (var x = 0; x < radioButtons.length; x++) {
                    if (radioButtons[x].checked) {
                        if (radioButtons[x].value == "1") {
                            if ($(pfid).val() <= 0 || $(ptid).val() <= 0) {
                                swal({ title: '', text: "Sorry, Please select from and to approver in order to perform migration operation." });
                                return;
                            }
                            if ($(pfid).val() == $(ptid).val()) {
                                swal({ title: '', text: "Sorry you cannot map same from and to approver." });
                                return;
                            }
                            blncalibrator = false;
                        }
                        else if (radioButtons[x].value == "2") {
                            if ($(pfid).val() <= 0 || $(ptid).val() <= 0) {
                                swal({ title: '', text: "Sorry, Please select from and to calibrator in order to perform migration operation." });
                                return;
                            }
                            if ($(pfid).val() == $(ptid).val()) {
                                swal({ title: '', text: "Sorry you cannot map same from and to calibrator." });
                                return;
                            }
                            blncalibrator = true;
                        }
                    }
                }
            }
            if (blncalibrator == false) {
                var txtlvl1 = document.getElementById("<%=txtFrmLevel.ClientID%>");
                var txtlvl2 = document.getElementById("<%=txtToLevel.ClientID%>");

                if (txtlvl1.value != txtlvl2.value) {
                    swal({ title: '', text: "Sorry, You cannot transfer employee to different level approver. Level should be same." });
                    return;
                }
            }
            var grid = document.getElementById("<%=dgvFrmEmp.ClientID%>");
            if (grid == null) {
                swal({ title: '', text: "Sorry, No records found in order to perform migration operation." });
                return;
            }
            var arr = [];
            $("#<%=dgvFrmEmp.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                if ($(this).is(':checked')) {
                    var guid = $(this).closest("tr").find("td:eq(1)").text();
                    arr.push(guid);
                }
            });
            if (arr == null) {
                swal({ title: '', text: "Sorry, Please check atleast one employee in order to migrate." });
                return;
            }
            if (arr != null && arr.length <= 0) {
                swal({ title: '', text: "Sorry, Please check atleast one employee in order to migrate." });
                return;
            }
            var ipadd = '<%= Session("IP_ADD") %>';
            var hstname = '<%= Session("HOST_NAME") %>';
            var usrid = '<%= Session("UserId") %>';
            var usrnm = '<%= Session("UserName") %>'
            var sndrs = '<%= Session("Senderaddress") %>'
            var iCoyId = '<%= Session("CompanyUnkId") %>'

            swal({ title: '', type: "info", text: "Performing selected operation and sending notification(s), Please wait for a while. This dailog will close in some time.", showCancelButton: false, showConfirmButton: false });
            
            PageMethods.TransferEmployee($(pfid).val(), $(ptid).val(), arr, blncalibrator, usrid, ipadd, hstname, "frmMigrateApprlCalibrator", oprtypid, usrnm, sndrs, iCoyId, onSuccess, onFailure);

            function onSuccess(str) {
                if (str == "1") {
                    swal({ title: '', type: "success", text: "Selected employee(s) are migrated successfully." });
                    $("#<%=dgvFrmEmp.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                        if ($(this).is(':checked')) {
                            var grd = document.getElementById("<%=dgvToEmp.ClientID%>");
                            var tbod = grd.rows[0].parentNode;
                            var newRow = grd.rows[grd.rows.length - 1].cloneNode(true);
                            tbod.appendChild(newRow);
                            $(newRow).closest("tr").find("td:eq(0)").html($(this).closest("tr").find("td:eq(1)").text());
                            $(newRow).closest("tr").find("td:eq(1)").html($(this).closest("tr").find("td:eq(2)").text());
                            $(this).closest("tr").remove();
                        }
                    });
                }
                else if (str == "0") {
                    swal({ title: '', type: "warning", text: "Problem in migrating employee(s)." });
                }
            }

            function onFailure(err) {
                swal({ title: '', text: err.get_message() });
                return;
            }
        }

        $("[id*=chkHeder1]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    if ($(this).is(":visible")) {
                        $(this).attr("checked", "checked");
                    }
                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        $("[id*=chkbox1]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");

            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }

        });
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Transfer Calibrator/Approver"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="panel-heading-default">
                                        <asp:Label ID="lblOperation" runat="server" Text="Select Opertion" Font-Bold="true"></asp:Label>
                                        <div style="float: right; margin-top: -5px">
                                            <asp:RadioButtonList ID="radMode" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                                <asp:ListItem Text="Approver" Value="1" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Calibrator" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="panel-body" style="width: 100%">
                                        <div class="row2" style="vertical-align: top">
                                            <div class="ib" style="width: 48%;">
                                                <div class="row2">
                                                    <div class="ib" style="width: 18%">
                                                        <asp:Label ID="objlblFromValue" runat="server" Text="#Value"></asp:Label></div>
                                                    <div class="ib" style="width: 75%">
                                                        <asp:DropDownList ID="cboFromValue" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <asp:Panel ID="pnllvel" runat="server">
                                                    <div class="row2">
                                                        <div class="ib" style="width: 18%">
                                                            <asp:Label ID="lblFromLevel" runat="server" Text="Level"></asp:Label></div>
                                                        <div class="ib" style="width: 74%">
                                                            <%--<asp:DropDownList ID="cboFromLevel" runat="server">
                                                        </asp:DropDownList>--%>
                                                            <asp:TextBox ID="txtFrmLevel" ReadOnly="true" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <div class="row2">
                                                    <div class="ib" style="width: 94.5%">
                                                        <input type="text" id="txtFrmSearch" name="txtSearch" placeholder="type search text"
                                                            maxlength="50" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ib" style="width: 94.5%">
                                                        <asp:Panel ID="pnl_dgvFrmEmp" runat="server" Width="99%" Height="350px" ScrollBars="Auto">
                                                            <asp:GridView ID="dgvFrmEmp" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                                HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                                HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="mappingtranunkid,Id">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-Width="25">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="ecode" HeaderText="Code" />
                                                                    <asp:BoundField DataField="ename" HeaderText="Employee" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ib" style="width: 48%;">
                                                <div class="row2">
                                                    <div class="ib" style="width: 15%">
                                                        <asp:Label ID="objlblToValue" runat="server" Text="#Value"></asp:Label></div>
                                                    <div class="ib" style="width: 75%">
                                                        <asp:DropDownList ID="cboToValue" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <asp:Panel ID="Panel1" runat="server">
                                                    <div class="row2">
                                                        <div class="ib" style="width: 15%">
                                                            <asp:Label ID="lblToLevel" runat="server" Text="Level"></asp:Label></div>
                                                        <div class="ib" style="width: 74%">
                                                            <%--<asp:DropDownList ID="cboToLevel" runat="server">
                                                        </asp:DropDownList>--%>
                                                            <asp:TextBox ID="txtToLevel" ReadOnly="true" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <div class="row2">
                                                    <div class="ib" style="width: 91.5%">
                                                        <input type="text" id="txtToSearch" name="txtSearch" placeholder="type search text"
                                                            maxlength="50" style="height: 25px; font: 100" onkeyup="ToSearching();" />
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ib" style="width: 91.5%">
                                                        <asp:Panel ID="pnl_dgvAssignedEmp" runat="server" Width="99%" Height="350px" ScrollBars="Auto">
                                                            <asp:GridView ID="dgvToEmp" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                                HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                                HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="mappingtranunkid,Id">
                                                                <Columns>
                                                                    <asp:BoundField DataField="ecode" HeaderText="Code" />
                                                                    <asp:BoundField DataField="ename" HeaderText="Employee" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row2" style="display: none">
                                            <asp:RadioButtonList runat="server" ID="radOperation" ForeColor="Red">
                                                <asp:ListItem Text="Overwrite calibration done for migrating approver/calibrator for the selected employee for all open assessment period."
                                                    Value="1" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Void calibration done for migrating approver/calibrator for the selected employee for all open assessment period."
                                                    Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <div style="float: left">
                                            <asp:Label ID="lblNote" runat="server" Text="Please tick desired employee(s) and press transfer button in order to do migration process."
                                                Font-Bold="true" ForeColor="Red"></asp:Label>
                                        </div>
                                        <asp:Button ID="btnTransfer" runat="server" Text="Transfer" CssClass="btndefault"
                                            OnClientClick="isvalidTransfer(); return false;" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
