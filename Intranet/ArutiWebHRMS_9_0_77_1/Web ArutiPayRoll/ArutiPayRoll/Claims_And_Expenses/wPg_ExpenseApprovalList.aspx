﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_ExpenseApprovalList.aspx.vb" Inherits="Claims_And_Expenses_wPg_ExpenseApprovalList"
    Title="Expense Approval List" %>


<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>

<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Expense Approval List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                   <%-- <div style="float: right">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" CssClass="lnkhover"
                                            Visible="false"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkReset" runat="server" Text="Reset" CssClass="lnkhover" Visible="false"></asp:LinkButton>
                                    </div>--%>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblExpCategory" runat="server" Text="Expense Cat."></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboExpCategory" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblClaimNo" runat="server" Text="Claim No"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtClaimNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 5%; text-align: center">
                                                <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 45%">
                                                            <uc2:DateCtrl ID="dtpFDate" AutoPostBack="false" runat="server" />
                                                        </td>
                                                        <td style="width: 10%">
                                                            <asp:Label ID="lblTo" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 45%">
                                                            <uc2:DateCtrl ID="dtpTDate" AutoPostBack="false" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee"  runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblStatus"  runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Priod" Visible="false"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" Visible="false">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblApprover"  runat="server" Text="Approver"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtExpApprover"  runat="server" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td style="width: 70%" colspan="4">
                                                <asp:CheckBox ID="ChkMyApprovals" runat="server" Text="My Approvals" Checked="true" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnSearch" CssClass="btndefault" runat="server" Text="Search" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <asp:Panel ID="pnl_lvClaimRequestList" ScrollBars="Auto" Height="300px" runat="server">
                                                    <asp:DataGrid ID="lvClaimRequestList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        AllowPaging="true" PagerStyle-Mode="NumericPages" PageSize = "250" PagerStyle-HorizontalAlign="Left" 
                                                        PagerStyle-Position="Top" PagerStyle-Wrap="true"   HeaderStyle-Font-Bold="false" Width="99.5%">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Change Status" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="btnChangeStatus">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Approval" Text="Change Status"
                                                                        CssClass="lnkhover"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="claimrequestno" HeaderText="Claim No" ReadOnly="true"
                                                                FooterText="colhclaimno" />
                                                            <asp:BoundColumn DataField="employeecode" HeaderText="Emp. Code" ReadOnly="true"  FooterText="colhEmployeecode" />
                                                            <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="true" FooterText="colhemployee" />
                                                            <asp:BoundColumn DataField="approvername" HeaderText="Approver" ReadOnly="true" FooterText="colhExpenseApprover" />
                                                            <asp:BoundColumn DataField="period" HeaderText="Period" ReadOnly="true" FooterText="colhperiod"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="expensetype" HeaderText="Expense Cat." ReadOnly="true"
                                                                FooterText="colhExpType" />
                                                            <asp:BoundColumn DataField="approvaldate" HeaderText="Approval Date" ReadOnly="true"
                                                                FooterText="colhdate" />
                                                            <asp:BoundColumn DataField="amount" HeaderText="Amount" ReadOnly="true" FooterText="colhamount"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                                  
                                                                  <%--'Pinkal (13-Aug-2020) -- Start
                                                                  'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                                                                   <asp:BoundColumn DataField="status" HeaderText="Status" ReadOnly="true" FooterText="colhStatus" />--%>
                                                                   <asp:BoundColumn DataField="AppprovalStatus" HeaderText="Status" ReadOnly="true" FooterText="colhStatus" />
                                                                   <%--'Pinkal (13-Aug-2020) -- End--%>
                                                                   
                                                            <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeunkId" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="crapproverunkid" HeaderText="approverunkid" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="approveremployeeunkid" HeaderText="approverEmpID" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="statusunkid" HeaderText="Statusunkid" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="iscancel" HeaderText="IsCancel" ReadOnly="true" Visible="false" />
                                                            <asp:BoundColumn DataField="mapuserunkid" HeaderText="Map UserId" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="crpriority" HeaderText="Priority" ReadOnly="true" Visible="false" />
                                                            <asp:BoundColumn DataField="crmasterunkid" HeaderText="crmasterunkid" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="crlevelname" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="tdate" HeaderText="objcolhTranscationDate" Visible="false" />
                                                            <asp:BoundColumn DataField="isexternalapprover" Visible="false" />
                                                            <asp:BoundColumn DataField="IsGrp" Visible="false"  FooterText="objcolhIsGrp"  />
                                                               <%--'Pinkal (13-Aug-2020) -- Start
                                                                  'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.--%>
                                                            <asp:BoundColumn DataField="crstatusunkid" Visible="false"  FooterText="objcolhcrstatusunkid" />
                                                             <%--'Pinkal (13-Aug-2020) -- End--%>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<%--                    <uc5:AdvanceFilter ID="popupAdvanceFilter" runat="server" />--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
