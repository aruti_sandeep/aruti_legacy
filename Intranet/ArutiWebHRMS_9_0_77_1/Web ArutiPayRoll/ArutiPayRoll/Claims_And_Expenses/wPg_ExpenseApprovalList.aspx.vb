﻿Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
#End Region
Partial Class Claims_And_Expenses_wPg_ExpenseApprovalList
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmExpenseApprovalList"
    Private DisplayMessage As New CommonCodes

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objClaimReqApproverTran As New clsclaim_request_approval_tran
    'Private objExpApprover As New clsExpenseApprover_Master
    'Pinkal (05-Sep-2020) -- End

    Dim dsTemp As DataSet = Nothing
#End Region

#Region "Page Event"
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            SetLanguage()

            If IsPostBack = False Then

                'Pinkal (16-Jul-2020) -- Start
                'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
                'If Session("crdtTable") IsNot Nothing Then
                '    Session("crdtTable") = Nothing
                'End If
                'Pinkal (16-Jul-2020) -- End
               
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (05-Sep-2020) -- End

                Call FillCombo()
                Dim objMapping As New clsapprover_Usermapping
                objMapping.GetData(enUserType.crApprover, , CInt(Session("UserId")), )


                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Dim objExpApprover As New clsExpenseApprover_Master
                'Pinkal (05-Sep-2020) -- End

                objExpApprover._crApproverunkid = objMapping._Approverunkid


                Dim dsAppr As New DataSet
                dsAppr = objExpApprover.GetList("List", Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                , CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString(), Session("UserAccessModeSetting").ToString() _
                                                                , True, CBool(Session("IsIncludeInactiveEmp")), True, "", True, objExpApprover._crApproverunkid)
                If dsAppr.Tables("List").Rows.Count > 0 Then
                    txtExpApprover.Text = CStr(dsAppr.Tables("List").Rows(0)("ename"))
                End If

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'dsAppr.Dispose()
                If dsAppr IsNot Nothing Then dsAppr.Clear()
                dsAppr = Nothing
                'Pinkal (05-Sep-2020) -- End



                Dim dsPedingList As DataSet = Nothing
                Me.ViewState.Add("dsPedingList", dsPedingList)
                Me.ViewState.Add("mstrAdvanceFilter", "")
                Me.ViewState.Add("mintClaimFormunkid", 0)
                Me.ViewState.Add("crApproverunkid", objExpApprover._crApproverunkid)
                lvClaimRequestList.DataSource = New List(Of String)
                lvClaimRequestList.DataBind()
                SetVisibility()


                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objMapping = Nothing
                objExpApprover = Nothing
                'Pinkal (05-Sep-2020) -- End


                'Pinkal (03-Mar-2021)-- Start
                'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
                If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ChkMyApprovals.Checked = False
                    ChkMyApprovals.Visible = False
                    txtExpApprover.Text = ""
                End If
                'Pinkal (03-Mar-2021) -- End

            Else

                'Pinkal (16-Jul-2020) -- Start
                'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
                'Pinkal (10-Jun-2020) -- Start
                'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                'If Session("crdtTable") IsNot Nothing Then
                '    lvClaimRequestList.DataSource = CType(Session("crdtTable"), DataTable)
                '    lvClaimRequestList.DataBind()
                'End If
                'Pinkal (10-Jun-2020) -- End
                'Pinkal (16-Jul-2020) -- End
            End If
        Catch ex As ArgumentException
            DisplayMessage.DisplayError(ex.Message, Me)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim objEMaster As New clsEmployee_Master
        Dim objMasterData As New clsMasterData
        Dim dsCombo As New DataSet
        Try

            dsCombo = objPrd.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_Year")), Session("Database_Name").ToString(), CDate(Session("fin_startdate")), "List", True, 1)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            With cboExpCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If
            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                            True, "List", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With



            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsCombo = objMasterData.getLeaveStatusList("List")
            dsCombo = objMasterData.getLeaveStatusList("List", "")
            'Pinkal (03-Jan-2020) -- End


            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombo.Tables(0), "statusunkid IN (0,1,2,3,6)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "Name"
                .DataSource = dtab
                .DataBind()

                'Pinkal (10-Jun-2020) -- Start
                'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                '.SelectedValue = "0"
                .SelectedValue = "2"
                'Pinkal (10-Jun-2020) -- End
            End With


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtab IsNot Nothing Then dtab.Clear()
            dtab = Nothing
            'Pinkal (05-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objPrd = Nothing
            objEMaster = Nothing
            objMasterData = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub Fill_List()
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim dTable As DataTable = Nothing
        Dim StrSearch As String = String.Empty
        Dim dsPedingList As DataSet = Nothing
        Dim objClaimReqApproverTran As New clsclaim_request_approval_tran
        Dim objExpApprover As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End
        Try
            If CBool(Session("AllowtoViewProcessClaimExpenseFormList")) = False AndAlso CInt(Session("LoginBy")) = Global.User.en_loginby.User Then Exit Sub

            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
            'Dim dsPedingList As DataSet = CType(Me.ViewState("dsPedingList"), DataSet)
            'Pinkal (16-Jul-2020) -- End

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If CInt(cboExpCategory.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.expensetypeid = '" & CInt(cboExpCategory.SelectedValue) & "' "
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                StrSearch &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
            End If


            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'If CInt(IIf(IsNumeric(txtClaimNo.Text), txtClaimNo.Text.Trim.Length, 0)) > 0 Then
            If txtClaimNo.Text.Trim.Length > 0 Then
                'Pinkal (13-Aug-2020) -- End
                StrSearch &= "AND cmclaim_request_master.claimrequestno LIKE '%" & txtClaimNo.Text & "%' "
            End If

            If IsDate(dtpFDate.GetDate) AndAlso IsDate(dtpTDate.GetDate) Then
                If dtpFDate.GetDate.Date <> Nothing AndAlso dtpTDate.GetDate.Date <> Nothing Then
                    StrSearch &= "AND cmclaim_request_master.transactiondate >= '" & eZeeDate.convertDate(dtpFDate.GetDate) & "' AND cmclaim_request_master.transactiondate <= '" & eZeeDate.convertDate(dtpTDate.GetDate) & "' "
                End If
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                If CInt(cboStatus.SelectedValue) = 6 Then
                    StrSearch &= "AND cmclaim_approval_tran.iscancel = 1"
                Else
                    StrSearch &= "AND cmclaim_approval_tran.iscancel = 0 AND cmclaim_request_master.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
                End If
            End If


            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                StrSearch &= "AND visibleid <> -1 "
            End If

            If CStr(Me.ViewState("mstrAdvanceFilter")).Length > 0 Then
                StrSearch &= "AND " & CStr(Me.ViewState("mstrAdvanceFilter"))
            End If
            'Pinkal (16-Jul-2020) -- End

            If StrSearch.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
            End If


            dsPedingList = objClaimReqApproverTran.GetApproverExpesneList("List", True, CBool(Session("PaymentApprovalwithLeaveApproval")), _
                                                                          Session("Database_Name").ToString(), _
                                                                          CInt(Session("UserId")), _
                                                                          Session("EmployeeAsOnDate").ToString(), _
                                                                          CInt(cboExpCategory.SelectedValue), True, True, -1, StrSearch, -1, Nothing)


            'Me.ViewState("dsPedingList") = dsPedingList

            StrSearch = ""

            'START FOR SET FILTER THAT ONLY LOGIN USER CAN SEE HIS AND LOWER LEVEL APPROVER

            If dsPedingList.Tables("List").Rows.Count > 0 Then
                Dim drRow As DataRow() = dsPedingList.Tables("List").Select("mapuserunkid = " & CInt(Session("UserId")), "")
                If drRow.Length > 0 Then
                    Dim objmapuser As New clsapprover_Usermapping
                    objmapuser.GetData(enUserType.crApprover, CInt(drRow(0)("crapproverunkid")), , )
                    objExpApprover._crApproverunkid = objmapuser._Approverunkid
                    Me.ViewState("crApproverunkid") = objmapuser._Approverunkid
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objmapuser = Nothing
                    'Pinkal (05-Sep-2020) -- End
                End If
            End If

            Dim objExpAppLevel As New clsExApprovalLevel
            objExpAppLevel._Crlevelunkid = objExpApprover._crLevelunkid

            'END FOR SET FILTER THAT ONLY LOGIN USER CAN SEE HIS AND LOWER LEVEL APPROVER

            If ChkMyApprovals.Checked AndAlso objExpAppLevel._Crlevelunkid > 0 Then
                StrSearch &= "AND mapuserunkid = " & CInt(Session("UserId"))
            Else
                ChkMyApprovals.Checked = False
            End If


            If StrSearch.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
                dTable = New DataView(dsPedingList.Tables("List"), StrSearch, "claimrequestno", DataViewRowState.CurrentRows).ToTable
            Else
                dTable = New DataView(dsPedingList.Tables("List"), "", "claimrequestno", DataViewRowState.CurrentRows).ToTable
            End If

            Dim dtTable As DataTable = dTable.Clone
            Dim strClaimNo As String = ""
            dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))

            Dim dtRow As DataRow = Nothing
            For Each drow As DataRow In dTable.Rows
                If CStr(drow("claimrequestno")).Trim <> strClaimNo.Trim Then
                    dtRow = dtTable.NewRow
                    strClaimNo = drow("claimrequestno").ToString()
                    dtRow("IsGrp") = True
                    dtRow("claimrequestno") = strClaimNo
                    dtTable.Rows.Add(dtRow)
                End If

                dtRow = dtTable.NewRow
                For Each dtcol As DataColumn In dTable.Columns
                    dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                Next
                dtRow("IsGrp") = False
                dtTable.Rows.Add(dtRow)
            Next

            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
            'Session("crdtTable") = dtTable
            'Pinkal (16-Jul-2020) -- End
            lvClaimRequestList.DataSource = dtTable
            lvClaimRequestList.DataBind()



            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dTable.Clear()
            dtTable = Nothing
            'Pinkal (05-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsPedingList IsNot Nothing Then dsPedingList.Clear()
            dsPedingList = Nothing
            If dTable IsNot Nothing Then dTable.Clear()
            dTable = Nothing
            objExpApprover = Nothing
            objClaimReqApproverTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                lvClaimRequestList.Columns(0).Visible = CBool(Session("AllowtoProcessClaimExpenseForm"))
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                lvClaimRequestList.Columns(0).Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub


    'Pinkal (16-Jul-2020) -- Start
    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report

    Private Sub AddGroup(ByVal rw As DataGridItem, ByVal title As String, ByVal gd As DataGrid, ByVal enLoginBy As Global.User.en_loginby)
        Try
            If enLoginBy = Global.User.en_loginby.User Then
                For i = 1 To lvClaimRequestList.Columns.Count - 1
                    rw.Cells(i).Visible = False
                Next
                rw.Cells(0).Text = title
                rw.Cells(0).HorizontalAlign = HorizontalAlign.Left
                rw.Cells(0).Font.Bold = True
                rw.Cells(0).ColumnSpan = lvClaimRequestList.Columns.Count
                rw.Cells(0).CssClass = "GroupHeaderStyle"

            ElseIf enLoginBy = Global.User.en_loginby.Employee Then
                For i = 2 To lvClaimRequestList.Columns.Count - 1
                    rw.Cells(i).Visible = False
                Next
                rw.Cells(1).Text = title
                rw.Cells(1).HorizontalAlign = HorizontalAlign.Left
                rw.Cells(1).Font.Bold = True
                rw.Cells(1).ColumnSpan = lvClaimRequestList.Columns.Count
                rw.Cells(1).CssClass = "GroupHeaderStyle"
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    'Pinkal (16-Jul-2020) -- End


#End Region

#Region "LinkButton Event"

    'Pinkal (16-Jul-2020) -- Start
    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
    'Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
    '    popupAdvanceFilter.Show()
    'End Sub

    'Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
    '    Try
    '        Me.ViewState("mstrAdvanceFilter") = ""
    '        Fill_List()
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("lnkReset_Click:-" & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Pinkal (16-Jul-2020) -- End


#End Region

#Region "Button Event"

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Expense Category is compulsory information.Please Select Expense Category."), Me)
                cboExpCategory.Focus()
                Exit Sub
            End If


            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug if any issue contact to allan.
            'If CInt(cboStatus.SelectedValue) <= 0 Then
            If CInt(cboStatus.SelectedValue) <= 0 AndAlso CInt(Session("Employeeunkid")) <= 0 Then
                'Pinkal (10-Feb-2021) -- End
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Status is compulsory information.Please Select Status. "), Me)
                cboStatus.Focus()
                Exit Sub
            End If
            'Pinkal (13-Aug-2020) -- End


            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            lvClaimRequestList.CurrentPageIndex = 0
            'Pinkal (13-Aug-2020) -- End

            Fill_List()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub


    'Pinkal (16-Jul-2020) -- Start
    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
    'Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
    '    Dim strAdvanceSearch As String
    '    Try
    '        strAdvanceSearch = popupAdvanceFilter._GetFilterString
    '        Me.ViewState("mstrAdvanceFilter") = strAdvanceSearch
    '        Fill_List()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex.Message, Me)
    '    End Try
    'End Sub
    'Pinkal (16-Jul-2020) -- End



    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
            'Session("crdtTable") = Nothing
            'Pinkal (16-Jul-2020) -- End
            Response.Redirect(Session("rootpath").ToString() & "UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "GridView Event"

    Protected Sub lvClaimRequestList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvClaimRequestList.ItemCommand
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objClaimReqApproverTran As New clsclaim_request_approval_tran
        Dim objExpApprover As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End
        Try
            If e.CommandName = "Approval" Then

                'Pinkal (13-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                'Dim dsPedingList As DataSet = CType(Me.ViewState("dsPedingList"), DataSet)
                Dim strSearch = "cmclaim_request_master.employeeunkid = " & CInt(e.Item.Cells(10).Text) & " AND cmclaim_approval_tran.crmasterunkid = " & CInt(e.Item.Cells(17).Text) & " AND  cmclaim_approval_tran.crapproverunkid <> " & CInt(e.Item.Cells(11).Text)

                Dim dsPedingList As DataSet = objClaimReqApproverTran.GetApproverExpesneList("List", True, CBool(Session("PaymentApprovalwithLeaveApproval")), _
                                                                          Session("Database_Name").ToString(), _
                                                                          CInt(Session("UserId")), _
                                                                          Session("EmployeeAsOnDate").ToString(), _
                                                                          CInt(cboExpCategory.SelectedValue), True, True, -1, StrSearch, -1, Nothing)
                'Pinkal (13-Aug-2020) -- End



                Dim dsList As DataSet
                Dim objMapping As New clsapprover_Usermapping
                dsList = objMapping.GetList("Mapping", enUserType.crApprover, "userunkid= " & CInt(Session("UserId")))

                If dsList.Tables("Mapping").Rows.Count > 0 Then

                    'Pinkal (16-Jul-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
                    'Dim dtList1 As DataTable = New DataView(dsList.Tables("Mapping"), "userunkid= " & CInt(Session("UserId")), "", DataViewRowState.CurrentRows).ToTable
                    'If dtList1.Rows.Count > 0 Then
                    'Me.ViewState("crApproverunkid") = CInt(e.Item.Cells(11).Text)
                    'objExpApprover._crApproverunkid = CInt(e.Item.Cells(11).Text)
                    'End If
                    Me.ViewState("crApproverunkid") = CInt(e.Item.Cells(11).Text)
                    objExpApprover._crApproverunkid = CInt(e.Item.Cells(11).Text)
                    'Pinkal (16-Jul-2020) -- End
                End If

                If CInt(Session("UserId")) <> CInt(e.Item.Cells(15).Text) Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "You can't Edit this Expense detail. Reason: You are logged in into another user login."), Me)
                    Exit Sub
                End If

                If CBool(e.Item.Cells(14).Text) = True Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "You can't Edit this Expense detail. Reason: This Expense is already Cancelled."), Me)
                    Exit Sub
                End If

                If CInt(e.Item.Cells(13).Text) = 1 Then 'FOR APPROVED
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Expense detail. Reason: This Expense is already Approved."), Me)
                    Exit Sub
                End If

                If CInt(e.Item.Cells(13).Text) = 3 Then 'FOR REJECTED
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already Rejected."), Me)
                    Exit Sub
                End If

                'START FOR CHECK WHETHER LOWER LEVEL APPROV LEAVE OR NOT
                Dim mintApproverId As Integer = 0

                'Pinkal (13-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                'Dim dtList As DataTable = New DataView(dsPedingList.Tables("List"), "employeeunkid = " & CInt(e.Item.Cells(10).Text) & " AND crmasterunkid = " & CInt(e.Item.Cells(17).Text) _
                '                                        & " AND crapproverunkid <> " & CInt(e.Item.Cells(11).Text), "statusunkid desc", DataViewRowState.CurrentRows).ToTable
                Dim dtList As DataTable = New DataView(dsPedingList.Tables("List"), "", "statusunkid desc", DataViewRowState.CurrentRows).ToTable
                'Pinkal (13-Aug-2020) -- End


                If dtList.Rows.Count > 0 Then
                    Dim objClaimMst As New clsclaim_request_master
                    Dim objExpapprlevel As New clsExApprovalLevel
                    Dim objLeaveApprover As New clsleaveapprover_master
                    Dim objLeaveLevel As New clsapproverlevel_master
                    objClaimMst._Crmasterunkid = CInt(dtList.Rows(0)("crmasterunkid"))
                    If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                        objLeaveApprover = New clsleaveapprover_master
                        objLeaveLevel = New clsapproverlevel_master
                        objLeaveApprover._Approverunkid = CInt(e.Item.Cells(11).Text)
                        objLeaveLevel._Levelunkid = objLeaveApprover._Levelunkid
                        mintApproverId = objLeaveApprover._Approverunkid
                    Else
                        mintApproverId = CInt(Me.ViewState("crApproverunkid"))
                    End If

                    objExpapprlevel._Crlevelunkid = objExpApprover._crLevelunkid
                    For i As Integer = 0 To dtList.Rows.Count - 1
                        'Pinkal (20-APR-2018) -- Start
                        'Bug - [0002189] Unable to apply for overtime on self service,We have an issue with pacra where they can apply for overtime normally when applying using desktop application but when the attempt to use self service
                        'If CBool(Session("PaymentApprovalwithLeaveApproval"))  Then
                        If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                            'Pinkal (20-APR-2018) -- End
                            If objLeaveLevel._Priority > CInt(dtList.Rows(i)("crpriority")) Then
                                'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING
                                Dim dList As DataTable = New DataView(dtList, "crlevelunkid = " & CInt(dtList.Rows(i)("crlevelunkid")) & " AND statusunkid = 1", "", DataViewRowState.CurrentRows).ToTable
                                If dList.Rows.Count > 0 Then Continue For
                                'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                                If CInt(dtList.Rows(i)("statusunkid")) = 2 Then
                                    Language.setLanguage(mstrModuleName)
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "You can't Edit this Expense detail. Reason: This Expesne approval is still pending."), Me)
                                    Exit Sub
                                ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                                    Language.setLanguage(mstrModuleName)
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already rejected."), Me)
                                    Exit Sub
                                End If
                            ElseIf objLeaveLevel._Priority <= CInt(dtList.Rows(i)("crpriority")) Then
                                If CInt(dtList.Rows(i)("statusunkid")) = 1 Then
                                    Language.setLanguage(mstrModuleName)
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Expense detail. Reason: This Expense is already Approved."), Me)
                                    Exit Sub
                                ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                                    Language.setLanguage(mstrModuleName)
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already rejected."), Me)
                                    Exit Sub

                                End If
                            End If
                        Else
                            If objExpapprlevel._Crpriority > CInt(dtList.Rows(i)("crpriority")) Then
                                'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING
                                Dim dList As DataTable = New DataView(dtList, "crlevelunkid = " & CInt(dtList.Rows(i)("crlevelunkid")) & " AND statusunkid = 1", "", DataViewRowState.CurrentRows).ToTable
                                If dList.Rows.Count > 0 Then Continue For
                                'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                                If CInt(dtList.Rows(i)("statusunkid")) = 2 Then
                                    Language.setLanguage(mstrModuleName)
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "You can't Edit this Expense detail. Reason: This Expesne approval is still pending."), Me)
                                    Exit Sub
                                ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                                    Language.setLanguage(mstrModuleName)
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already rejected."), Me)
                                    Exit Sub
                                End If
                            ElseIf objExpapprlevel._Crpriority <= CInt(dtList.Rows(i)("crpriority")) Then
                                If CInt(dtList.Rows(i)("statusunkid")) = 1 Then
                                    Language.setLanguage(mstrModuleName)
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Expense detail. Reason: This Expense is already Approved."), Me)
                                    Exit Sub
                                ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                                    Language.setLanguage(mstrModuleName)
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already rejected."), Me)
                                    Exit Sub
                                End If
                            End If
                        End If
                    Next
                    objClaimMst = Nothing
                    objLeaveApprover = Nothing
                    objLeaveLevel = Nothing
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objExpapprlevel = Nothing
                    'Pinkal (05-Sep-2020) -- End
                End If

                'END FOR CHECK WHETHER LOWER LEVEL APPROV LEAVE OR NOT
                If CDate(e.Item.Cells(19).Text).Date < ConfigParameter._Object._CurrentDateAndTime.Date And mintApproverId = CInt(e.Item.Cells(11).Text) Then
                    If CInt(e.Item.Cells(13).Text) = 1 Then   'FOR APPROVED
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Expense detail. Reason: This Expense is already Approved."), Me)
                        Exit Sub
                    ElseIf CInt(e.Item.Cells(13).Text) = 3 Then   'FOR REJECTED
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already rejected."), Me)
                        Exit Sub
                    End If
                ElseIf CDate(e.Item.Cells(19).Text).Date > ConfigParameter._Object._CurrentDateAndTime.Date And mintApproverId = CInt(e.Item.Cells(11).Text) Then
                    If CInt(e.Item.Cells(13).Text) = 3 Then   'FOR REJECTED
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "You can't Edit this Expense detail. Reason: This Expense is already rejected."), Me)
                        Exit Sub
                    End If

                End If

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtList IsNot Nothing Then dtList.Clear()
                dtList = Nothing
                'Pinkal (05-Sep-2020) -- End

                Session.Add("crmasterunkid", e.Item.Cells(17).Text)
                Session.Add("approverunkid", e.Item.Cells(11).Text)
                Session.Add("approverEmpID", e.Item.Cells(12).Text)
                Session.Add("Priority", e.Item.Cells(16).Text)
                Session.Add("IsExternalApprover", e.Item.Cells(20).Text)

                Response.Redirect("~/Claims_And_Expenses/wPg_ExpenseApproval.aspx", False)
                'Pinkal (16-Jul-2020) -- Start
                'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
                'Session("crdtTable") = Nothing
                'Pinkal (16-Jul-2020) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub lvClaimRequestList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles lvClaimRequestList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then

                'Pinkal (13-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                Call SetDateFormat()
                'Pinkal (13-Aug-2020) -- End

                e.Item.Cells(1).Text = e.Item.Cells(3).Text
                e.Item.Cells(1).ColumnSpan = 3
                e.Item.Cells(2).Visible = False
                e.Item.Cells(3).Visible = False

            End If


            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                'Pinkal (13-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                'If CBool(e.Item.Cells(e.Item.Cells.Count - 1).Text) Then
                If CBool(e.Item.Cells(getColumnId_Datagrid(lvClaimRequestList, "objcolhIsGrp", False, True)).Text) Then
                    'Pinkal (13-Aug-2020) -- End
                    CType(e.Item.Cells(0).FindControl("lnkEdit"), LinkButton).Visible = False

                    'Pinkal (16-Jul-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report

                    'If CBool(Session("AllowtoProcessClaimExpenseForm")) AndAlso CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    '    e.Item.Cells(0).Text = e.Item.Cells(1).Text
                    '    e.Item.Cells(0).ColumnSpan = e.Item.Cells.Count - 1
                    '    e.Item.Cells(0).Style.Add("text-align", "left")
                    '    e.Item.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                    '    e.Item.Cells(1).Visible = False
                    'Else
                    '    e.Item.Cells(1).Text = e.Item.Cells(1).Text
                    '    e.Item.Cells(1).ColumnSpan = e.Item.Cells.Count - 1
                    '    e.Item.Cells(1).Style.Add("text-align", "left")
                    '    e.Item.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                    'End If
                    'For i = 2 To e.Item.Cells.Count - 1
                    '    e.Item.Cells(i).Visible = False
                    'Next
                    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                    If CBool(Session("AllowtoProcessClaimExpenseForm")) Then
                        AddGroup(e.Item, info1.ToTitleCase(e.Item.Cells(1).Text.ToLower()), lvClaimRequestList, CType(CInt(Session("LoginBy")), Global.User.en_loginby))
                    Else
                        AddGroup(e.Item, info1.ToTitleCase(e.Item.Cells(1).Text.ToLower()), lvClaimRequestList, CType(CInt(Session("LoginBy")), Global.User.en_loginby))
                    End If
                    'Pinkal (16-Jul-2020) -- End

                Else

                    'Pinkal (13-Aug-2020) -- Start
                    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

                    'Dim mstrStaus As String = ""

                    'Dim dsPedingList As DataSet = CType(Me.ViewState("dsPedingList"), DataSet)

                    'Dim dList As DataTable = Nothing
                    'If e.Item.Cells.Count > 0 AndAlso e.Item.ItemIndex >= 0 AndAlso dsPedingList IsNot Nothing Then
                    '    If CInt(Me.ViewState("mintClaimFormunkid")) <> CInt(e.Item.Cells(17).Text) Then
                    '        dList = New DataView(dsPedingList.Tables("List"), "employeeunkid = " & CInt(e.Item.Cells(10).Text) & " AND crmasterunkid = " & CInt(e.Item.Cells(17).Text), "", DataViewRowState.CurrentRows).ToTable
                    '        Me.ViewState("mintClaimFormunkid") = CInt(e.Item.Cells(17).Text)
                    '    Else
                    '        dList = New DataView(dsPedingList.Tables("List"), "employeeunkid = " & CInt(e.Item.Cells(10).Text) & " AND crmasterunkid = " & CInt(e.Item.Cells(17).Text), "", DataViewRowState.CurrentRows).ToTable
                    '    End If

                    '    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                    '        Dim dr As DataRow() = dList.Select("crpriority >= " & CInt(e.Item.Cells(16).Text))
                    '        If dr.Length > 0 Then
                    '            For i As Integer = 0 To dr.Length - 1
                    '                If CInt(e.Item.Cells(13).Text) = 1 Then
                    '                    mstrStaus = Language.getMessage(mstrModuleName, 8, "Approved By :-  ") & e.Item.Cells(4).Text.ToString()
                    '                    Exit For
                    '                ElseIf CInt(e.Item.Cells(13).Text) = 2 Then
                    '                    If CInt(dr(i)("statusunkid")) = 1 Then
                    '                        mstrStaus = Language.getMessage(mstrModuleName, 8, "Approved By :-  ") & dr(i)("approvername").ToString()
                    '                        Exit For
                    '                    ElseIf CInt(dr(i)("statusunkid")) = 3 Then
                    '                        mstrStaus = Language.getMessage(mstrModuleName, 9, "Rejected By :-  ") & dr(i)("approvername").ToString()
                    '                        Exit For
                    '                    End If
                    '                ElseIf CInt(e.Item.Cells(13).Text) = 3 Then
                    '                    mstrStaus = Language.getMessage(mstrModuleName, 9, "Rejected By :-  ") & e.Item.Cells(4).Text.ToString()
                    '                    Exit For
                    '                End If
                    '            Next
                    '        End If

                    '        'Pinkal (28-Apr-2020) -- Start
                    '        'Optimization  - Working on Process Optimization and performance for require module.	
                    '        'If mstrStaus = "" Then
                    '        '    If CInt(e.Item.Cells(13).Text) = 2 Then
                    '        '        For i As Integer = 0 To dr(0).Table.Rows.Count - 1
                    '        '            If CInt(dr(0).Table.Rows(i)("statusunkid")) = 3 Then
                    '        '                mstrStaus = Language.getMessage(mstrModuleName, 9, "Rejected By :-  ") & dr(0).Table.Rows(i)("approvername").ToString()
                    '        '                Exit For
                    '        '            End If
                    '        '        Next
                    '        '    End If
                    '        'End If
                    '        'Pinkal (28-Apr-2020) -- End
                    '    End If

                    '    If mstrStaus <> "" Then
                    '        e.Item.Cells(9).Text = mstrStaus
                    '    End If
                    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                    If e.Item.Cells(getColumnId_Datagrid(lvClaimRequestList, "colhStatus", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(lvClaimRequestList, "colhStatus", False, True)).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(getColumnId_Datagrid(lvClaimRequestList, "colhStatus", False, True)).Text = info1.ToTitleCase(e.Item.Cells(getColumnId_Datagrid(lvClaimRequestList, "colhStatus", False, True)).Text.ToLower())
                    End If

                    If e.Item.Cells(19).Text.Trim <> "" AndAlso e.Item.Cells(19).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(19).Text = eZeeDate.convertDate(e.Item.Cells(19).Text).Date.ToShortDateString
                    End If

                    e.Item.Cells(4).Text = e.Item.Cells(4).Text & " - " & e.Item.Cells(18).Text

                    If e.Item.Cells(7).Text.Trim <> "" AndAlso e.Item.Cells(7).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(7).Text = eZeeDate.convertDate(e.Item.Cells(7).Text).Date.ToShortDateString
                    End If

                    e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(8).Text), Session("fmtCurrency").ToString())

                    'Dim lnkChangeStatus As LinkButton = CType(e.Item.Cells(0).FindControl("lnkEdit"), LinkButton)

                    'Pinkal (13-Aug-2020) -- End


                    'Pinkal (28-Apr-2020) -- Start
                    'Optimization  - Working on Process Optimization and performance for require module.	
                    ' Language.setLanguage(mstrModuleName)
                    'Pinkal (28-Apr-2020) -- End

                    'lnkChangeStatus.Text = Language._Object.getCaption("btnChangeStatus", lnkChangeStatus.Text).Replace("&", "")

                    e.Item.Cells(1).Attributes.Add("ClaimNo", e.Item.Cells(1).Text)
                    e.Item.Cells(1).Attributes.Add("EmpCode", e.Item.Cells(2).Text)
                    e.Item.Cells(1).Attributes.Add("EmpName", e.Item.Cells(3).Text)
                    e.Item.Cells(1).Text = e.Item.Cells(2).Text & " - " & e.Item.Cells(3).Text
                    e.Item.Cells(1).ColumnSpan = 3
                    e.Item.Cells(2).Visible = False
                    e.Item.Cells(3).Visible = False



                    'Pinkal (13-Aug-2020) -- Start
                    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                    If CInt(e.Item.Cells(getColumnId_Datagrid(lvClaimRequestList, "objcolhcrstatusunkid", False, True)).Text) = 1 Then
                        CType(e.Item.Cells(0).FindControl("lnkEdit"), LinkButton).Enabled = False
                        CType(e.Item.Cells(0).FindControl("lnkEdit"), LinkButton).CssClass = ""
                    Else
                        CType(e.Item.Cells(0).FindControl("lnkEdit"), LinkButton).Enabled = True
                    End If
                    'End If
                    'Pinkal (13-Aug-2020) -- End

                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lvClaimRequestList_ItemDataBound:-" & ex.Message, Me)
            DisplayMessage.DisplayError("lvClaimRequestList_ItemDataBound:-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lvClaimRequestList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles lvClaimRequestList.PageIndexChanged
        Try
            lvClaimRequestList.CurrentPageIndex = e.NewPageIndex
            Fill_List()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)


            Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.ID, Me.lblClaimNo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.ID, Me.lblTo.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)

            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
            'Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            'Pinkal (16-Jul-2020) -- End

            Me.ChkMyApprovals.Text = Language._Object.getCaption(Me.ChkMyApprovals.ID, Me.ChkMyApprovals.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")


            Me.lvClaimRequestList.Columns(0).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(0).FooterText, Me.lvClaimRequestList.Columns(0).HeaderText).Replace("&", "")
            Me.lvClaimRequestList.Columns(1).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(1).FooterText, Me.lvClaimRequestList.Columns(1).HeaderText)
            Me.lvClaimRequestList.Columns(2).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(2).FooterText, Me.lvClaimRequestList.Columns(2).HeaderText)
            Me.lvClaimRequestList.Columns(3).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(3).FooterText, Me.lvClaimRequestList.Columns(3).HeaderText)
            Me.lvClaimRequestList.Columns(4).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(4).FooterText, Me.lvClaimRequestList.Columns(4).HeaderText)
            Me.lvClaimRequestList.Columns(5).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(5).FooterText, Me.lvClaimRequestList.Columns(5).HeaderText)
            Me.lvClaimRequestList.Columns(6).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(6).FooterText, Me.lvClaimRequestList.Columns(6).HeaderText)
            Me.lvClaimRequestList.Columns(7).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(7).FooterText, Me.lvClaimRequestList.Columns(7).HeaderText)
            Me.lvClaimRequestList.Columns(8).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(8).FooterText, Me.lvClaimRequestList.Columns(8).HeaderText)
            Me.lvClaimRequestList.Columns(9).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(9).FooterText, Me.lvClaimRequestList.Columns(9).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

End Class
