﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data

#End Region

Partial Class wPgEmployeeFeedback
    Inherits Basepage

#Region " Private Variable(s) "

    Private objFMaster As clshrtnafeedback_master
    Private objFTran As clshrtnafeedback_tran
    Private clsuser As New User
    Private msg As New CommonCodes
    Private mdtFeedback As DataTable
    Private dsItems As New DataSet


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private mstrModuleName As String = "frmEmployeeFeedback"
    'Anjan [04 June 2014] -- End

#End Region

#Region " Private Method(s) & Function(s) "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objTSchedule As New clsTraining_Scheduling
        Dim objFdbkGrp As New clshrtnafdbk_group_master
        Try


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End
            dsCombo = objTSchedule.getComboList("List", True, Session("Fin_year"), True)
            With cboCourseTitle
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
            Me.ViewState.Add("Title", dsCombo.Tables("List"))

            dsCombo = objFdbkGrp.getComboList("List", True)
            With cboFeedbackGrp
                .DataValueField = "fdbkgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            msg.DisplayError("Procedure FillCombo : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboCourseTitle.SelectedValue = objFMaster._Trainingschduleunkid
            Call cboCourseTitle_SelectedIndexChanged(Nothing, Nothing)

            cboEmployee.SelectedValue = objFMaster._Employeeunkid
        Catch ex As Exception
            msg.DisplayError("Procedure GetValue : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objFMaster._Feedbackmasterunkid = Me.ViewState("Unkid")
            Dim dTemp() As DataRow = CType(Me.ViewState("Title"), DataTable).Select("Id ='" & CInt(cboCourseTitle.SelectedValue) & "'")
            If dTemp.Length > 0 Then
                objFMaster._Courseunkid = CInt(dTemp(0)("courseunkid"))
            End If
            objFMaster._Feedbackdate = ConfigParameter._Object._CurrentDateAndTime
            objFMaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objFMaster._Isvoid = False
            objFMaster._Trainingschduleunkid = CInt(cboCourseTitle.SelectedValue)
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objFMaster._Userunkid = Aruti.Data.User._Object._Userunkid
            objFMaster._Userunkid = Session("UserId")
            'Sohail (23 Apr 2012) -- End
            objFMaster._Voiddatetime = Nothing
            objFMaster._Voidreason = ""
            objFMaster._Voiduserunkid = -1
        Catch ex As Exception
            msg.DisplayError("Procedure SetValue : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub ResetCombos()
        Try
            cboResult.SelectedValue = 0 : cboSubItem.SelectedValue = 0
        Catch ex As Exception
            msg.DisplayError("Procedure ResetCombos : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            Language.setLanguage(mstrModuleName)  'Anjan [04 June 2014] -- Start

            If CInt(cboCourseTitle.SelectedValue) <= 0 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Course Title is mandatory information. Please select Course Title to continue."), Me)
                'Anjan [04 June 2014] -- End


                Return False
            End If

            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(Language.getMessage(mstrModuleName, 3, "Employee is mandatory information. Please select Employee to continue."), Me)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Employee is mandatory information. Please select Employee to continue."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End
                    Return False
                End If
            Else
                If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError("Sorry, you have not enrolled for this particular scheduled course so you can not give feedback.", Me)
                    msg.DisplayMessage("Sorry, you have not enrolled for this particular scheduled course so you can not give feedback.", Me)
                    'Sohail (23 Mar 2019) -- End
                    Return False
                End If
            End If


            If CInt(cboFeedbackGrp.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(Language.getMessage(mstrModuleName, 4, "Feedback Group is mandatory information. Please select Feedback Group to continue."), Me)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Feedback Group is mandatory information. Please select Feedback Group to continue."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End
                Return False
            End If

            If CInt(IIf(cboFeedbackItem.SelectedValue = "", 0, cboFeedbackItem.SelectedValue)) <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(Language.getMessage(mstrModuleName, 5, "Feedback Item is mandatory information. Please select Feedback Item to continue."), Me)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Feedback Item is mandatory information. Please select Feedback Item to continue."), Me)
                'Sohail (23 Mar 2019) -- End
                Return False
            End If

            If Me.ViewState("dsItems") IsNot Nothing Then dsItems = Me.ViewState("dsItems")
            Dim dtTemp() As DataRow = dsItems.Tables(0).Select("Id = '" & CInt(cboFeedbackItem.SelectedValue) & "'")
            If dtTemp.Length > 0 Then
                If CInt(dtTemp(0).Item("Cnt")) > 0 AndAlso CInt(IIf(cboSubItem.SelectedValue = "", 0, cboSubItem.SelectedValue)) <= 0 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(Language.getMessage(mstrModuleName, 5, "Feedback Sub Item compulsory information.Please Select Feedback Sub Item."), Me)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Feedback Sub Item compulsory information.Please Select Feedback Sub Item."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            msg.DisplayError("Procedure Validation : " & ex.Message, Me)
        Finally
        End Try
    End Function

    Private Function Is_AllFeedback(Optional ByVal blnFlag As Boolean = False) As Boolean
        Try
            If Me.ViewState("mdtFeedback") IsNot Nothing Then mdtFeedback = Me.ViewState("mdtFeedback")
            Dim dView As DataView = mdtFeedback.DefaultView
            Dim intCount As Integer = objFMaster.GetItems_BasedGroup()

            Language.setLanguage(mstrModuleName)

            If intCount <> dView.ToTable(True, "fdbkitemunkid").Rows.Count Then
                If blnFlag = True Then

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot do Save Complete Operation as you have not provided the feedback all items."), Me)
                    'Anjan [04 June 2014] -- End


                End If
                Return False
            End If

            dsItems = objFMaster.GetSubItemCount()

            If dsItems.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsItems.Tables(0).Rows
                    Dim dtTemp() As DataRow = mdtFeedback.Select("fdbkitemunkid = '" & CInt(dRow.Item("Id")) & "'")
                    If dtTemp.Length > 0 AndAlso CInt(dtTemp(0)("fdbksubitemunkid")) <> 0 Then
                        If dtTemp.Length <> CInt(dRow.Item("Cnt")) Then
                            If blnFlag = True Then
                                'Anjan [04 June 2014] -- Start
                                'ENHANCEMENT : Implementing Language,requested by Andrew
                                msg.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot do Save Complete Operation as you have not provided the feedback all subitems in some item(s)."), Me)
                                'Anjan [04 June 2014] -- End
                            End If
                            Return False
                        End If
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            msg.DisplayError("Procedure Is_AllFeedback : " & ex.Message, Me)
        Finally
        End Try
    End Function

    Public Function b64decode(ByVal StrDecode As String) As String
        Dim decodedString As String = ""
        Try
            decodedString = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(StrDecode))
        Catch ex As Exception
            msg.DisplayError("Procedure b64decode : " & ex.Message, Me)
        End Try
        Return decodedString
    End Function

    Private Sub SetDataSource()
        Try
            If Me.ViewState("mdtFeedback") IsNot Nothing Then
                mdtFeedback = Me.ViewState("mdtFeedback")
                Dim dView As New DataView
                dView = mdtFeedback.DefaultView
                dView.Sort = "feedback_group"
                mdtFeedback = dView.ToTable

                If mdtFeedback.Rows.Count <= 0 Then
                    Dim r As DataRow = mdtFeedback.NewRow
                    r.Item("feedback_group") = "None"
                    mdtFeedback.Rows.Add(r)
                    mdtFeedback.AcceptChanges()
                End If
                Me.ViewState.Add("mdtFeedback", mdtFeedback)
            End If
            dgvFeedback.DataSource = mdtFeedback
            dgvFeedback.DataBind()
        Catch ex As Exception
            msg.DisplayError("Procedure SetDataSource : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Public Function IsFromFeedback_List() As Boolean
        Try
            Me.ViewState("mdtFeedback") = Nothing

            If (Request.QueryString("ProcessId") <> "") Then
                Dim mintFeedbackUnkid As Integer = -1
                mintFeedbackUnkid = Val(b64decode(Request.QueryString("ProcessId")))
                Me.ViewState.Add("Unkid", mintFeedbackUnkid)
                objFMaster._Feedbackmasterunkid = mintFeedbackUnkid
                objFTran._Feedbackmasterunkid = mintFeedbackUnkid
                Call GetValue()
                cboCourseTitle.Enabled = False
                cboEmployee.Enabled = False
            Else
                Me.ViewState.Add("Unkid", -1)
            End If

            mdtFeedback = objFTran._DataTable

            If Me.ViewState("mdtFeedback") IsNot Nothing Then
                mdtFeedback = Me.ViewState("mdtFeedback")
            Else
                Me.ViewState.Add("mdtFeedback", mdtFeedback)
            End If

            Call SetDataSource()

        Catch ex As Exception
            msg.DisplayError("Function IsFromFeedback_List : " & ex.Message, Me)
        End Try
    End Function

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                'Anjan [20 February 2016] -- End
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End



            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End

            objFMaster = New clshrtnafeedback_master
            objFTran = New clshrtnafeedback_tran

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmEmployeeFeedback"
            'StrModuleName2 = "mnuTrainingInformation"
            'StrModuleName3 = "mnuTrainingEvaluation"
            'clsCommonATLog._WebClientIP = Session("IP_ADD")
            'clsCommonATLog._WebHostName = Session("HOST_NAME")

            ''Pinkal (24-Aug-2012) -- Start
            ''Enhancement : TRA Changes

            'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Form)
            'SHANI [01 FEB 2015]--END
            'Pinkal (22-Mar-2012) -- End


            If (Page.IsPostBack = False) Then
                clsuser = CType(Session("clsuser"), User)

                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    'Aruti.Data.User._Object._Userunkid = Session("UserId") 'Sohail (23 Apr 2012)
                Else
                    'Aruti.Data.User._Object._Userunkid = -1 'Sohail (23 Apr 2012)
                End If

                Call FillCombo()

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If (Request.QueryString("ProcessId") = "") Then
                    cboEmployee.SelectedValue = Session("EmpFeedBack_EmpUnkID")
                End If
                'SHANI [09 Mar 2015]--END 

            End If




            'Pinkal (12-Feb-2015) -- Start
            'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.

            If Session("LoginBy") = Global.User.en_loginby.User Then
                btnAddFeedback.Visible = CBool(Session("AllowToAddLevelIEvaluation"))
                btnEditFeedback.Visible = CBool(Session("AllowToEditLevelIEvaluation"))
                btnSave.Visible = CBool(Session("AllowToSave_CompleteLevelIEvaluation"))
                btnSaveComplete.Visible = CBool(Session("AllowToSave_CompleteLevelIEvaluation"))
                dgvFeedback.Columns(0).Visible = CBool(Session("AllowToEditLevelIEvaluation"))
                dgvFeedback.Columns(1).Visible = CBool(Session("AllowToDeleteLevelIEvaluation"))
            End If

            'Pinkal (12-Feb-2015) -- End


        Catch ex As Exception
            msg.DisplayError("Event Page_Load : " & ex.Message, Me)
        Finally

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END

        End Try
    End Sub

    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        Try
            If Not IsPostBack Then
                IsFromFeedback_List()
            End If
        Catch ex As Exception
            msg.DisplayError("Page_PreRenderComplete Event : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'ToolbarEntry1.VisibleSaveButton(False)
        'ToolbarEntry1.VisibleDeleteButton(False)
        'ToolbarEntry1.VisibleCancelButton(False)
        'ToolbarEntry1.VisibleExitButton(False)
        'SHANI [01 FEB 2015]--END

    End Sub

    'Pinkal (22-Mar-2012) -- End

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button(s) Events "

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\UserHome.aspx")
            Response.Redirect("~\Training\wPgEmployeeFeedbackList.aspx")
            'SHANI [09 Mar 2015]--END
        Catch ex As Exception
            msg.DisplayError("btnClose_Click : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Me.ViewState("mdtFeedback") IsNot Nothing Then mdtFeedback = Me.ViewState("mdtFeedback")
            Dim dRow() As DataRow = mdtFeedback.Select("feedback_group = 'None'")
            If dRow.Length > 0 Then
                mdtFeedback.Rows.Remove(dRow(0))
            End If
            Dim dDel() As DataRow = mdtFeedback.Select("AUD <> 'D'")
            If dDel.Length <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Please add atleast one Feedback Item in order to save."), Me)
                'Anjan [04 June 2014] -- End
                Exit Sub
            End If

            Call SetValue()

            mdtFeedback = Me.ViewState("mdtFeedback")

            With objFMaster
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            If Me.ViewState("Unkid") <= 0 Then
                blnFlag = objFMaster.Insert(mdtFeedback)
            Else
                blnFlag = objFMaster.Update(mdtFeedback)
            End If

            If blnFlag = True Then
                Me.ViewState("mdtFeedback") = Nothing
                Me.ViewState("Unkid") = 0
                Response.Redirect("~\Training\wPgEmployeeFeedbackList.aspx")
            End If

        Catch ex As Exception
            msg.DisplayError("Procdure btnSave_Click : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub btnSaveComplete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveComplete.Click
        Try
            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            If Is_AllFeedback(False) = False Then

                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'LblDelete.Text = Language.getMessage(mstrModuleName, 12, "There are some Feedback Items which are still not given any answers.If you Press Yes,you will Complete this Feedback.Do you want to continue?")
                popupConfirm.Title = Language.getMessage(mstrModuleName, 12, "There are some Feedback Items which are still not given any answers.If you Press Yes,you will Complete this Feedback.Do you want to continue?")
                'SHANI [01 FEB 2015]--END
            Else
                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'LblDelete.Text = Language.getMessage(mstrModuleName, 13, "Are you sure you want to Finally save this feedback.After this you won't be allowed to EDIT this feedback.Do you want to continue?")
                popupConfirm.Title = Language.getMessage(mstrModuleName, 13, "Are you sure you want to Finally save this feedback.After this you won't be allowed to EDIT this feedback.Do you want to continue?")
                'SHANI [01 FEB 2015]--END

            End If
            'Anjan [04 June 2014] -- End
            popupConfirm.Show()
        Catch ex As Exception
            msg.DisplayError("Procedure btnSaveComplete_Click : " & ex.Message, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirm.buttonYes_Click
        'SHANI [01 FEB 2015]--END
        Dim blnFlag As Boolean = False
        Try
            Call SetValue()
            objFMaster._Iscomplete = True

            mdtFeedback = Me.ViewState("mdtFeedback")

            With objFMaster
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            If Me.ViewState("Unkid") <= 0 Then
                blnFlag = objFMaster.Insert(mdtFeedback)
            Else
                blnFlag = objFMaster.Update(mdtFeedback)
            End If

            'S.SANDEEP [06-MAR-2017] -- START
            'ISSUE/ENHANCEMENT : Training Module Notification
            If Session("Ntf_TrainingLevelI_EvalUserIds").ToString.Trim.Length > 0 Then
                If blnFlag Then
                    If (Session("LoginBy") = Global.User.en_loginby.User) Then
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objFMaster.SendNotification(CInt(cboCourseTitle.SelectedValue), CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), enLogin_Mode.MGR_SELF_SERVICE, Session("Userunkid"), Session("Ntf_TrainingLevelI_EvalUserIds"), 0, mstrModuleName)
                        objFMaster.SendNotification(CInt(cboCourseTitle.SelectedValue), CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), enLogin_Mode.MGR_SELF_SERVICE, Session("Userunkid"), Session("Ntf_TrainingLevelI_EvalUserIds"), 0, mstrModuleName, CInt(Session("CompanyUnkId")))
                        'Sohail (30 Nov 2017) -- End
                    ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objFMaster.SendNotification(CInt(cboCourseTitle.SelectedValue), CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), enLogin_Mode.EMP_SELF_SERVICE, Session("Userunkid"), Session("Ntf_TrainingLevelI_EvalUserIds"), Session("Employeeunkid"), mstrModuleName)
                        objFMaster.SendNotification(CInt(cboCourseTitle.SelectedValue), CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), enLogin_Mode.EMP_SELF_SERVICE, Session("Userunkid"), Session("Ntf_TrainingLevelI_EvalUserIds"), Session("Employeeunkid"), mstrModuleName, CInt(Session("CompanyUnkId")))
                        'Sohail (30 Nov 2017) -- End
                    End If
                End If
            End If
            'S.SANDEEP [06-MAR-2017] -- END



            If blnFlag = True Then
                Me.ViewState("mdtFeedback") = Nothing
                Me.ViewState("Unkid") = 0
                Response.Redirect("~\Training\wPgEmployeeFeedbackList.aspx")
            End If

        Catch ex As Exception
            msg.DisplayError("Procedure btnYes_Click : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnAddFeedback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddFeedback.Click
        Try
            If Validation() = False Then Exit Sub

            Language.setLanguage(mstrModuleName) 'Anjan [04 June 2014] -- Start

            If objFTran.IsExists(CInt(cboCourseTitle.SelectedValue), _
                     CInt(cboEmployee.SelectedValue), _
                     CInt(cboFeedbackGrp.SelectedValue), _
                     CInt(cboFeedbackItem.SelectedValue), _
                     CInt(cboSubItem.SelectedValue)) = True Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Feedback is already present for selected selection."), Me)
                'Anjan [04 June 2014] -- End


                Exit Sub
            End If

            If Me.ViewState("mdtFeedback") IsNot Nothing Then mdtFeedback = Me.ViewState("mdtFeedback")

            If CInt(IIf(cboSubItem.SelectedValue = "", 0, cboSubItem.SelectedValue)) > 0 Then
                Dim dtRow As DataRow() = mdtFeedback.Select("fdbksubitemunkid = '" & CInt(cboSubItem.SelectedValue) & "' AND AUD <> 'D' ")
                If dtRow.Length > 0 Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot add same item again in the list."), Me)
                    'Anjan [04 June 2014] -- End
                    Exit Sub
                End If
            Else
                If CInt(IIf(cboFeedbackItem.SelectedValue = "", 0, cboFeedbackItem.SelectedValue)) > 0 Then
                    Dim dtRow As DataRow() = mdtFeedback.Select("fdbkitemunkid = '" & CInt(cboFeedbackItem.SelectedValue) & "' AND AUD <> 'D' ")
                    If dtRow.Length > 0 Then
                        msg.DisplayMessage("Sorry, you cannot add same item again in the list.", Me)
                        Exit Sub
                    End If
                End If
            End If

            Dim dRow As DataRow = mdtFeedback.NewRow

            dRow.Item("feedbacktranunkid") = -1
            dRow.Item("feedbackmasterunkid") = Me.ViewState("Unkid")
            dRow.Item("fdbkgroupunkid") = cboFeedbackGrp.SelectedValue
            dRow.Item("fdbkitemunkid") = cboFeedbackItem.SelectedValue
            dRow.Item("fdbksubitemunkid") = cboSubItem.SelectedValue
            dRow.Item("resultunkid") = cboResult.SelectedValue
            dRow.Item("other_result") = txtTextResult.Text
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("feedback_group") = cboFeedbackGrp.SelectedItem.Text
            dRow.Item("feedback_item") = cboFeedbackItem.SelectedItem.Text
            If CInt(cboSubItem.SelectedValue) > 0 Then
                dRow.Item("feedback_subitem") = cboSubItem.SelectedItem.Text
            Else
                dRow.Item("feedback_subitem") = ""
            End If
            If CInt(cboResult.SelectedValue) > 0 Then
                dRow.Item("feedback_result") = cboResult.SelectedItem.Text
            Else
                dRow.Item("feedback_result") = ""
            End If
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString

            mdtFeedback.Rows.Add(dRow)

            Call SetDataSource()
            Call ResetCombos()

            cboCourseTitle.Enabled = False : cboEmployee.Enabled = False

        Catch ex As Exception
            msg.DisplayError("Procedure btnAddFeedback_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEditFeedback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditFeedback.Click
        Try
            If Me.ViewState("intRowNum") >= 0 Then
                If Me.ViewState("mdtFeedback") IsNot Nothing Then mdtFeedback = Me.ViewState("mdtFeedback")
                Dim dtTemp() As DataRow = Nothing

                If CInt(IIf(cboSubItem.SelectedValue = "", 0, cboSubItem.SelectedValue)) > 0 Then
                    dtTemp = mdtFeedback.Select("fdbksubitemunkid = '" & CInt(cboSubItem.SelectedValue) & "'AND GUID <> '" & mdtFeedback.Rows(Me.ViewState("intRowNum")).Item("GUID") & "' AND AUD <> 'D' ")
                Else
                    If CInt(IIf(cboFeedbackItem.SelectedValue = "", 0, cboFeedbackItem.SelectedValue)) > 0 Then
                        dtTemp = mdtFeedback.Select("fdbkitemunkid = '" & CInt(cboFeedbackItem.SelectedValue) & "'AND GUID <> '" & mdtFeedback.Rows(Me.ViewState("intRowNum")).Item("GUID") & "' AND AUD <> 'D' ")
                    End If
                End If

                If dtTemp.Length > 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError("Sorry, you cannot add same item again in the list.", Me)
                    msg.DisplayMessage("Sorry, you cannot add same item again in the list.", Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If

                If Validation() = False Then Exit Sub

                With mdtFeedback.Rows(Me.ViewState("intRowNum"))

                    .Item("feedbacktranunkid") = .Item("feedbacktranunkid")
                    .Item("feedbackmasterunkid") = .Item("feedbackmasterunkid")
                    .Item("fdbkgroupunkid") = cboFeedbackGrp.SelectedValue
                    .Item("fdbkitemunkid") = cboFeedbackItem.SelectedValue
                    .Item("fdbksubitemunkid") = cboSubItem.SelectedValue
                    .Item("resultunkid") = cboResult.SelectedValue
                    .Item("other_result") = txtTextResult.Text
                    .Item("isvoid") = False
                    .Item("voiduserunkid") = -1
                    .Item("voiddatetime") = DBNull.Value
                    .Item("voidreason") = ""
                    .Item("feedback_group") = cboFeedbackGrp.SelectedItem.Text
                    .Item("feedback_item") = cboFeedbackItem.Text
                    If CInt(cboSubItem.SelectedValue) > 0 Then
                        .Item("feedback_subitem") = cboSubItem.SelectedItem.Text
                    Else
                        .Item("feedback_subitem") = ""
                    End If
                    If CInt(cboResult.SelectedValue) > 0 Then
                        .Item("feedback_result") = cboResult.SelectedItem.Text
                    Else
                        .Item("feedback_result") = ""
                    End If
                    If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                        .Item("AUD") = "U"
                    End If
                    .Item("GUID") = Guid.NewGuid.ToString
                    .AcceptChanges()

                End With
                Call SetDataSource()
                Call ResetCombos()
            End If
        Catch ex As Exception
            msg.DisplayError("Procedure btnEditFeedback_Click : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub cboCourseTitle_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCourseTitle.SelectedIndexChanged
        Try
            If CInt(cboCourseTitle.SelectedValue) > 0 Then
                Dim objTEnroll As New clsTraining_Enrollment_Tran
                Dim dsList As New DataSet
                dsList = objTEnroll.Get_EnrolledEmp(CInt(cboCourseTitle.SelectedValue))
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    With cboEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dsList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    'S.SANDEEP [ 18 JUL 2014 ] -- START
                    'Dim objglobalassess = New GlobalAccess
                    'objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                    'Dim dRow() As DataRow = dsList.Tables(0).Select("employeeunkid = '" & objglobalassess.employeeid & "'")
                    'If dRow.Length > 0 Then
                    '    With cboEmployee
                    '        .DataSource = objglobalassess.ListOfEmployee.Copy
                    '        .DataTextField = "loginname"
                    '        .DataValueField = "employeeunkid"
                    '        .DataBind()
                    '    End With
                    'Else
                    '    dsList.Tables(0).Rows.Clear()
                    '    With cboEmployee
                    '        .DataSource = dsList.Tables(0)
                    '        .DataTextField = "employeename"
                    '        .DataValueField = "employeeunkid"
                    '        .DataBind()
                    '    End With
                    'End If
                    Dim iDataTable As DataTable = New DataView(dsList.Tables(0), "employeeunkid = '" & Session("Employeeunkid") & "'", "", DataViewRowState.CurrentRows).ToTable
                    If iDataTable.Rows.Count > 0 Then
                        With cboEmployee
                            .DataSource = iDataTable
                            'Nilay (09-Aug-2016) -- Start
                            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                            .DataTextField = "EmpCodeName"
                            'Nilay (09-Aug-2016) -- End
                            .DataValueField = "employeeunkid"
                            .DataBind()
                        End With
                    Else
                        dsList.Tables(0).Rows.Clear()
                        With cboEmployee
                            .DataSource = dsList.Tables(0)
                            'Nilay (09-Aug-2016) -- Start
                            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                            .DataTextField = "EmpCodeName"
                            'Nilay (09-Aug-2016) -- End
                            .DataValueField = "employeeunkid"
                            .DataBind()
                        End With
                    End If
                    'S.SANDEEP [ 18 JUL 2014 ] -- END
                End If
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            msg.DisplayError("cboCourseTitle_SelectedIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub cboFeedbackGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFeedbackGrp.SelectedIndexChanged
        Try
            If CInt(cboFeedbackGrp.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objfdbkItems As New clshrtnafdbk_item_master
                dsList = objfdbkItems.getComboList("List", True, CInt(cboFeedbackGrp.SelectedValue))
                With cboFeedbackItem
                    .DataValueField = "fdbkitemunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
                dsItems = objFMaster.GetSubItemCount(CInt(cboFeedbackGrp.SelectedValue))
                Me.ViewState.Add("dsItems", dsItems)
            Else
                cboFeedbackItem.DataSource = Nothing
            End If
        Catch ex As Exception
            msg.DisplayError("cboFeedbackGrp_SelectedIndexChanged", Me)
        Finally
        End Try
    End Sub

    Private Sub cboFeedbackItem_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFeedbackItem.SelectedIndexChanged
        Try
            If CInt(cboFeedbackItem.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                Dim objfdbkItem As New clshrtnafdbk_item_master
                Dim objfdbkSItem As New clshrtnafdbk_subitem_master
                Dim objResult As New clsresult_master
                objfdbkItem._Fdbkitemunkid = CInt(cboFeedbackItem.SelectedValue)

                dsList = objfdbkSItem.getComboList("List", True, CInt(cboFeedbackItem.SelectedValue))
                With cboSubItem
                    .DataValueField = "fdbksubitemunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                    .SelectedValue = 0
                End With

                dsList = objResult.getComboList("List", True, objfdbkItem._Resultgroupunkid)
                With cboResult
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                    .SelectedValue = 0
                End With

                If objfdbkItem._Resultgroupunkid > 0 Then
                    txtTextResult.Text = ""
                    txtTextResult.Enabled = False
                    cboResult.SelectedValue = 0
                    cboResult.Enabled = True
                Else
                    txtTextResult.Text = ""
                    txtTextResult.Enabled = True
                    cboResult.SelectedValue = 0
                    cboResult.Enabled = False
                End If

                objfdbkItem = Nothing : objfdbkSItem = Nothing : objResult = Nothing
            Else
                txtTextResult.Text = ""
                txtTextResult.Enabled = True
                cboResult.DataSource = Nothing : cboSubItem.DataSource = Nothing
            End If
        Catch ex As Exception
            msg.DisplayError("cboFeedbackItem_SelectedIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgvFeedback_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvFeedback.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If mdtFeedback.Rows(e.Row.RowIndex).Item("AUD") = "D" Then
                    e.Row.Visible = False
                End If

                If e.Row.Cells(2).Text = "None" Then
                    e.Row.Visible = False
                End If

            End If
        Catch ex As Exception
            msg.DisplayError("dgvFeedback_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub dgvFeedback_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvFeedback.RowCommand
        Try
            If e.CommandName = "Change" Then
                Dim intRowNum As Integer = -1
                intRowNum = CInt(e.CommandArgument)
                Me.ViewState.Add("intRowNum", intRowNum)
                If Me.ViewState("mdtFeedback") IsNot Nothing Then mdtFeedback = Me.ViewState("mdtFeedback")

                cboFeedbackGrp.SelectedValue = mdtFeedback.Rows(intRowNum).Item("fdbkgroupunkid")
                Call cboFeedbackGroup_SelectedIndexChanged(Nothing, Nothing)

                cboFeedbackItem.SelectedValue = mdtFeedback.Rows(intRowNum).Item("fdbkitemunkid")
                Call cboFeedbackItem_SelectedIndexChanged(Nothing, Nothing)

                cboSubItem.SelectedValue = IIf(mdtFeedback.Rows(intRowNum).Item("fdbksubitemunkid") <= 0, 0, mdtFeedback.Rows(intRowNum).Item("fdbksubitemunkid"))
                cboResult.SelectedValue = IIf(mdtFeedback.Rows(intRowNum).Item("resultunkid") <= 0, 0, mdtFeedback.Rows(intRowNum).Item("resultunkid"))
                txtTextResult.Text = mdtFeedback.Rows(intRowNum).Item("other_result")

                cboCourseTitle.Enabled = False : cboEmployee.Enabled = False

            ElseIf e.CommandName = "Remove" Then
                Dim intDelRowNum As Integer = CInt(e.CommandArgument)
                Me.ViewState.Add("intDelRowNum", intDelRowNum)
                If Me.ViewState("mdtFeedback") IsNot Nothing Then mdtFeedback = Me.ViewState("mdtFeedback")
                mdtFeedback.Rows(intDelRowNum).Item("AUD") = "D"
                mdtFeedback.Rows(intDelRowNum).Item("isvoid") = True
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'mdtFeedback.Rows(intDelRowNum).Item("voiduserunkid") = Aruti.Data.User._Object._Userunkid
                mdtFeedback.Rows(intDelRowNum).Item("voiduserunkid") = Session("UserId")
                'Sohail (23 Apr 2012) -- End
                mdtFeedback.Rows(intDelRowNum).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                mdtFeedback.Rows(intDelRowNum).Item("voidreason") = "Void From Self Service."

                mdtFeedback.AcceptChanges() : Me.ViewState("mdtFeedback") = mdtFeedback
                Dim dDel() As DataRow = mdtFeedback.Select("AUD <> 'D'")
                Call SetDataSource()
                If dDel.Length <= 0 Then
                    cboCourseTitle.Enabled = True
                    cboEmployee.Enabled = True
                End If
            End If
        Catch ex As Exception
            msg.DisplayError("Procedure dgvFeedback_RowCommand : " & ex.Message, Me)
        Finally
        End Try
    End Sub

#End Region

    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes

#Region "ToolBar Event"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect("~\Training\wPgEmployeeFeedbackList.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError("ToolbarEntry1_GridMode_Click :- " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region

    'Pinkal (22-Mar-2012) -- End


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebutton.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END

            Me.lblResult.Text = Language._Object.getCaption(Me.lblResult.ID, Me.lblResult.Text)
            Me.lblFeedbackGroup.Text = Language._Object.getCaption(Me.lblFeedbackGroup.ID, Me.lblFeedbackGroup.Text)
            Me.lblFeedbackItems.Text = Language._Object.getCaption(Me.lblFeedbackItems.ID, Me.lblFeedbackItems.Text)
            Me.lblFeedbackSubItems.Text = Language._Object.getCaption(Me.lblFeedbackSubItems.ID, Me.lblFeedbackSubItems.Text)
            Me.lblCourseTitle.Text = Language._Object.getCaption(Me.lblCourseTitle.ID, Me.lblCourseTitle.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.ID, Me.lblRemark.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnEditFeedback.Text = Language._Object.getCaption(Me.btnEditFeedback.ID, Me.btnEditFeedback.Text).Replace("&", "")
            Me.btnAddFeedback.Text = Language._Object.getCaption(Me.btnAddFeedback.ID, Me.btnAddFeedback.Text).Replace("&", "")
            Me.btnSaveComplete.Text = Language._Object.getCaption(Me.btnSaveComplete.ID, Me.btnSaveComplete.Text).Replace("&&", "")

            Me.dgvFeedback.Columns(0).HeaderText = Language._Object.getCaption("btnEditFeedback", Me.dgvFeedback.Columns(0).HeaderText).Replace("&", "")
            Me.dgvFeedback.Columns(1).HeaderText = Language._Object.getCaption("btnDeleteFeedback", Me.dgvFeedback.Columns(1).HeaderText).Replace("&", "")
            Me.dgvFeedback.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvFeedback.Columns(2).FooterText, Me.dgvFeedback.Columns(2).HeaderText)
            Me.dgvFeedback.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvFeedback.Columns(3).FooterText, Me.dgvFeedback.Columns(3).HeaderText)
            Me.dgvFeedback.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvFeedback.Columns(4).FooterText, Me.dgvFeedback.Columns(4).HeaderText)
            Me.dgvFeedback.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvFeedback.Columns(5).FooterText, Me.dgvFeedback.Columns(5).HeaderText)
            Me.dgvFeedback.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvFeedback.Columns(6).FooterText, Me.dgvFeedback.Columns(6).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError("SetLanguage :-" & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End


End Class