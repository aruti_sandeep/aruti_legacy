﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DateCtrl.ascx.vb" Inherits="Controls_DateCtrl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:TextBox runat="server" ID="TxtDate" AutoPostBack="True" Width="60%" Style="max-width: 100px"
    class="form-control" />
<asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/images/PopUpCalendar.gif"
    AlternateText="Click to show calendar" Width="20px" Height="20px" ImageAlign="Top" />
<asp:ImageButton runat="Server" ID="objbtnDateReminder" ImageUrl="~/images/Reminder_32.png"
    Width="25px" Height="25px" ImageAlign="Top" Visible="false" />
<br />
<ajaxToolkit:CalendarExtender ID="calendarButtonExtender" runat="server" TargetControlID="TxtDate"
    PopupButtonID="Image1" PopupPosition="BottomLeft" Enabled="True" CssClass="cal_Theme1" />
