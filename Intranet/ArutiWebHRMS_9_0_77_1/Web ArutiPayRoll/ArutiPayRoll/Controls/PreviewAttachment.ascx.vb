﻿Imports Aruti.Data
Imports System.Data
Imports System.IO
Imports System.IO.Packaging
Imports System.Net

Partial Class Controls_PreviewAttachment
    Inherits System.Web.UI.UserControl

    Public Event btnPreviewClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)

#Region "Private Varible"
    Private mstrScanTranUnkidCsv As String = ""
    Private DisplayMessage As New CommonCodes
    Private wpg_WebPage As UI.Page
    Private blnShowPopup As Boolean = False
    'S.SANDEEP |25-JAN-2019| -- START
    'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
    Private mstrZipFileName As String = String.Empty
    Dim dtTranTable As New DataTable
    'S.SANDEEP |25-JAN-2019| -- END
#End Region

#Region "Private Method(S)"

    Public Sub Show()
        Try
        If lvPreviewList.Items.Count <= 0 Then
            lvPreviewList.DataSource = New List(Of String)
            lvPreviewList.DataBind()
        End If
        'If Not Page.ClientScript.IsStartupScriptRegistered("OpenNew") Then
        '    Page.ClientScript.RegisterStartupScript(Me.GetType(), "OpenNew", "opennewtab('');", True)
        'End If
        Call FillList()
        imgAttchment.ImageUrl = ""
        blnShowPopup = True
        popup_ShowAttchment.Show()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Show :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Hide()
        Try
        blnShowPopup = False
        popup_ShowAttchment.Hide()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Hide :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim objDocument As New clsScan_Attach_Documents
        Dim dtTranTable As New DataTable
        Try
            'S.SANDEEP |04-SEP-2021| -- START
            'ISSUE : TAKING CARE FROM SLOWNESS QUERY
            'Call objDocument.GetList(Session("Document_Path"), "List", mstrScanTranUnkidCsv)
            Call objDocument.GetList(Session("Document_Path"), "List", mstrScanTranUnkidCsv, , , , , , CBool(IIf(mstrScanTranUnkidCsv.Trim.Length <= 0, True, False)))
            'S.SANDEEP |04-SEP-2021| -- START

            dtTranTable = objDocument._Datatable.Copy

            If dtTranTable IsNot Nothing AndAlso dtTranTable.Rows.Count > 0 Then
                lvPreviewList.AutoGenerateColumns = False
                lvPreviewList.DataSource = dtTranTable
                lvPreviewList.DataBind()
            End If

            'S.SANDEEP |16-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
            If lvPreviewList.Items.Count <= 0 Then
                btnDownloadView.Visible = False
            End If
            'S.SANDEEP |16-MAY-2019| -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, wpg_WebPage)


            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.
    Public Sub ClearPreviewDataSource()
        Try
            lvPreviewList.DataSource = New List(Of String)
            lvPreviewList.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ClearPreviewDataSource:- " & ex.Message, wpg_WebPage)

            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End

            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Pinkal (10-Jan-2017) -- End

    'S.SANDEEP |25-JAN-2019| -- START
    'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
    Private Sub AddFileToZip(ByVal zipFilename As String, ByVal fileToAdd As List(Of String), Optional ByVal compression As CompressionOption = CompressionOption.Normal)
        Try
            If zipFilename.Trim.Length <= 0 Then zipFilename = "file_" + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
            Using ms As New MemoryStream()
                Using pkg As Package = Package.Open(ms, FileMode.Create)
                    For Each item As String In fileToAdd
                        Dim destFilename As String = "/" & Path.GetFileName(item)
                        Dim part As PackagePart = pkg.CreatePart(New Uri(destFilename, UriKind.Relative), "", CompressionOption.Normal)
                        Dim byt As Byte() = File.ReadAllBytes(item)

                        Using fileStream As FileStream = New FileStream(item, FileMode.Open)
                            CopyStream(CType(fileStream, Stream), part.GetStream())
                        End Using
                    Next
                    pkg.Close()
                End Using
                Response.Clear()
                Response.ContentType = "application/zip"
                Response.AddHeader("content-disposition", "attachment; filename=" + zipFilename + "")
                Response.OutputStream.Write(ms.GetBuffer(), 0, CInt(ms.Length))
                Response.Flush()
                Response.End()
            End Using
        Catch ex As Threading.ThreadAbortException

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
            'If fileToAdd.Count > 0 Then
            '    For Each fl As String In fileToAdd
            '        Try
            '            System.IO.File.Delete(fl)
            '        Catch ex As Exception

            '        End Try
            '    Next
            'End If
        End Try
    End Sub

    Private Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Try
            Const bufSize As Integer = (5859 * 1024)
            Dim buf(bufSize - 1) As Byte
            Dim bytesRead As Integer = 0

            bytesRead = source.Read(buf, 0, bufSize)
            Do While bytesRead > 0
                target.Write(buf, 0, bytesRead)
                bytesRead = source.Read(buf, 0, bufSize)
            Loop
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CopyStream :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
    'S.SANDEEP |25-JAN-2019| -- END

#End Region

#Region "Public Property(S)"

    Public Property ScanTranIds() As String
        Get
            Return mstrScanTranUnkidCsv
        End Get
        Set(ByVal value As String)
            mstrScanTranUnkidCsv = value
        End Set
    End Property

    Public WriteOnly Property _Webpage() As UI.Page
        Set(ByVal value As UI.Page)
            wpg_WebPage = value
        End Set
    End Property

    'S.SANDEEP |25-JAN-2019| -- START
    'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
    Public Property _ZipFileName() As String
        Get
            Return mstrZipFileName
        End Get
        Set(ByVal value As String)
            If value.Trim.Length <= 0 Then
                mstrZipFileName = "file_" + DateTime.Now.ToString("MMyyyyddsshhmm").ToString()
            Else
                mstrZipFileName = value
            End If
        End Set
    End Property
    'S.SANDEEP |25-JAN-2019| -- END

#End Region

#Region "page Event(S)"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                'If Not Page.ClientScript.IsStartupScriptRegistered("OpenNew") Then
                '    Page.ClientScript.RegisterStartupScript(Me.GetType(), "OpenNew", "opennewtab('');", True)
                'End If
            Else
                mstrScanTranUnkidCsv = Me.ViewState("mstrScanTranUnkidCsv").ToString
                blnShowPopup = CBool(Me.ViewState("blnShowPopup"))
                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                dtTranTable = CType(Me.ViewState("dtTranTable"), DataTable)
                mstrZipFileName = Me.ViewState("mstrZipFileName")
                'S.SANDEEP |25-JAN-2019| -- END
                If blnShowPopup Then
                    popup_ShowAttchment.Show()
                End If
            End If
            imgAttchment.Visible = False
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Page_Load :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("blnShowPopup") = blnShowPopup
            Me.ViewState("mstrScanTranUnkidCsv") = mstrScanTranUnkidCsv
            'S.SANDEEP |25-JAN-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
            Me.ViewState("dtTranTable") = dtTranTable
            Me.ViewState("mstrZipFileName") = mstrZipFileName
            'S.SANDEEP |25-JAN-2019| -- END
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Page_PreRender :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
#End Region

#Region "Button Event(S)"

    'S.SANDEEP |03-APR-2019| -- START
    Protected Sub btnDownloadView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadView.Click
        Try
            If dtTranTable.Rows.Count <= 0 Then
                Dim objDocument As New clsScan_Attach_Documents
                'S.SANDEEP |04-SEP-2021| -- START
                'ISSUE : TAKING CARE FROM SLOWNESS QUERY
                Call objDocument.GetList(Session("Document_Path"), "List", mstrScanTranUnkidCsv, , , , , , CBool(IIf(mstrScanTranUnkidCsv.Trim.Length <= 0, True, False)))
                'S.SANDEEP |04-SEP-2021| -- END

                dtTranTable = objDocument._Datatable.Copy
                objDocument = Nothing
            End If
            If dtTranTable.Columns.Contains("temppath") = False Then dtTranTable.Columns.Add("temppath", Type.GetType("System.String"))
            If dtTranTable.Columns.Contains("error") = False Then dtTranTable.Columns.Add("error", Type.GetType("System.String"))
            Dim strError As String = String.Empty : Dim strLocalPath As String = String.Empty
            Dim fileToAdd As New List(Of String)
            For Each xRow As DataRow In dtTranTable.Rows
                If IsDBNull(xRow("file_data")) = False Then
                    strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                    Dim ms As New MemoryStream(CType(xRow("file_data"), Byte()))
                    Dim fs As New FileStream(strLocalPath, FileMode.Create)
                    ms.WriteTo(fs)
                    ms.Close()
                    fs.Close()
                    fs.Dispose()
                    If strLocalPath <> "" Then
                        fileToAdd.Add(strLocalPath)
                    End If
                ElseIf IsDBNull(xRow("file_data")) = True Then
                    If xRow("temppath").ToString.Trim <> "" Then
                        If System.IO.File.Exists(xRow("temppath").ToString.Trim) Then
                            strLocalPath = xRow("temppath").ToString.Trim
                        End If
                    Else
                        strLocalPath = xRow("filepath").ToString
                        strLocalPath = strLocalPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(strLocalPath, 1) <> "/" Then
                            strLocalPath = "~/" & strLocalPath
                        Else
                            strLocalPath = "~" & strLocalPath
                        End If
                        strLocalPath = Server.MapPath(strLocalPath)
                        If strLocalPath <> "" Then
                            If IO.File.Exists(strLocalPath) Then
                                fileToAdd.Add(strLocalPath)
                            End If
                        End If
                    End If
                End If
            Next
            If fileToAdd.Count > 0 Then
                AddFileToZip(mstrZipFileName, fileToAdd)
            Else
                DisplayMessage.DisplayMessage("No Files to download.", wpg_WebPage)
            End If
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
    'S.SANDEEP |03-APR-2019| -- END

    Protected Sub btnAttchmentClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttchmentClose.Click
        RaiseEvent btnPreviewClose_Click(sender, e)
    End Sub

#End Region

#Region "Control Event(S)"

    Protected Sub lvPreviewList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvPreviewList.ItemCommand
        Try

            If e.CommandName.ToUpper = "PREVIEW" Then
                If e.Item.ItemIndex >= 0 Then
                    Dim strFile As String = CStr(e.Item.Cells(6).Text)
                    If strFile.Contains(Session("ArutiSelfServiceURL").ToString) Then
                        strFile = strFile.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(strFile, 1) <> "/" Then
                            strFile = "~/" & strFile
                        Else
                            strFile = "~" & strFile
                        End If

                        If IO.File.Exists(Server.MapPath(strFile)) Then
                            If clsScan_Attach_Documents.IsValidImage(Server.MapPath(strFile)) Then
                                imgAttchment.ImageUrl = strFile
                            Else
                                Dim url As String = Session("ArutiSelfServiceURL").ToString & strFile.Replace("~", "")
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenNew", "opennewtab('" & url & "')", True)
                            End If
                        Else
                            'Dim StrMessage As String = "Unable to preview. Reson : File [ " & e.Item.Cells(4).Text & " ] has been " & _
                            '                            "removed or renamed or moved to other location."
                            'Response.Write("<script><alert>" & StrMessage & "</alert></script>")
                            imgAttchment.ImageUrl = "../images/Not_File.jpg"
                        End If
                    Else
                        'Dim StrMessage As String = "Unable to preview. Reson : File [ " & e.Item.Cells(4).Text & " ] has been " & _
                        '                               "removed or renamed or moved to other location."
                        'Response.Write("<script><alert>" & StrMessage & "</alert></script>")
                        imgAttchment.ImageUrl = "../images/Not_File.jpg"
                    End If
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lvPreviewList_ItemCommand:- " & ex.Message, wpg_WebPage)

            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End

            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

End Class
