﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PreviewAttachment.ascx.vb"
    Inherits="Controls_PreviewAttachment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--<script type="text/javascript">
    function opennewtab(url) {
        if (url != '') {
            window.open(url);
        }
    }
    
</script>--%>
<ajaxToolkit:ModalPopupExtender ID="popup_ShowAttchment" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="hdf_ScanAttchment" PopupControlID="pnl_ShowAttchment" TargetControlID="hdf_ScanAttchment">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnl_ShowAttchment" runat="server" CssClass="newpopup csspreview" Style="width: 600px">
    <div class="panel-primary" style="margin-bottom: 0px">
        <div class="panel-heading">
            <asp:Label ID="lblAttchmentheader" runat="server" Text="View Scan/Attchment"></asp:Label>
        </div>
        <div class="panel-body">
            <div id="Div4" class="panel-default">
                <div id="Div5" class="panel-heading-default">
                    <div style="float: left;">
                        <asp:Label ID="lblHeader" runat="server" Text="View Scan/Attchment"></asp:Label>
                    </div>
                </div>
                <div id="Div6" class="panel-body-default">
                    <table style="width: 100%">
                        <tr style="width: 100%; display: none">
                            <td style="width: 100%; height: 300px">
                                <asp:Image ID="imgAttchment" runat="server" Width="100%" Height="100%" Visible="true" />
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 100%;">
                                <asp:Panel ID="pnl_lvPreviewList" runat="server" Style="width: 550px; max-height: 150px"
                                    ScrollBars="Auto">
                                    <asp:DataGrid ID="lvPreviewList" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                        HeaderStyle-Font-Bold="false" Width="99%">
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" Visible="false">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPreview" runat="server" ToolTip="Preview" Font-Underline="false"
                                                        CommandName="Preview">
                                                        <i class="fa fa-eye" aria-hidden="true" style="font-size:20px;"></i>                                                                                              
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="code" HeaderText="Code" FooterText="colhCode" />
                                            <asp:BoundColumn DataField="names" HeaderText="Employee/Applicant" FooterText="colhName" />
                                            <asp:BoundColumn DataField="document" HeaderText="Document" FooterText="colhDocument" />
                                            <asp:BoundColumn DataField="filename" HeaderText="Filename" FooterText="colhFileName" />
                                            <asp:BoundColumn DataField="doctype" Visible="false" FooterText="objcolhDocType" />
                                            <asp:BoundColumn DataField="filepath" Visible="false" FooterText="objcolhFullPath" />
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <div class="btn-default">
                        <asp:Button ID="btnAttchmentClose" runat="server" Text="Close" CssClass="btnDefault" />
                        <asp:Button ID="btnDownloadView" runat="server" Text="Download & View" CssClass="btnDefault" />
                        <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>

<script>
    var prm;
    prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(endRequestHandler);
    function endRequestHandler(sender, evemt) {
        $(".csspreview").css("z-index", "100013");
    }
</script>

