﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_EmployeeSkill.aspx.vb"
    Inherits="HR_wPg_EmployeeSkill" MasterPageFile="~/home.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 45%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Skill"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Skills Add/Edit"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <asp:Panel ID="pnlEntry" Width="100%" runat="server">
                                        <table style="width: 100%">
                                            <tr style="width:100%">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                </td>
                                                <td style="width: 80%;" colspan="3">
                                                    <asp:DropDownList ID="drpEmployee" Width="100%" runat="server"></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width:100%">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblSkillcategory" runat="server" Text="Skill Category"></asp:Label>
                                                </td>
                                                <td style="width: 30%">
                                                    <asp:DropDownList ID="drpSkillCategory" Width="100%" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 20%; text-align:center">
                                                    <asp:Label ID="lblSkill" runat="server" Text="Skill"></asp:Label>
                                                </td>
                                                <td style="width: 30%">
                                                    <asp:DropDownList ID="drpSkill" Width="100%" runat="server"></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width:100%">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblSkillDescription" runat="server" Text="Description"></asp:Label>
                                                </td>
                                                <td style="width: 80%" colspan="3" >
                                                    <asp:TextBox ID="txtDescription" runat="server"  TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                </td>
                                            </tr>
                                           
                                        </table>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSaveInfo" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
