﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_EmpRecategorize.aspx.vb" Inherits="HR_wPg_EmpRecategorize" Title="Re-Categorize Employee" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewMovementApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

        <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

        <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
    
    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>

    <style>
        .fixPopup
        {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 9999;
            visibility: hidden;
            opacity: 0;
            transition: all .8s 3s;
        }
        .fixPopup.open
        {
            visibility: visible;
            opacity: 1;
        }
        .newpopup::before
        {
            content: '';
            position: fixed;
            top: 0;
            left: 0;
            background-color: rgba(0,0,0,.5);
            z-index: -1;
            width: 100%;
            height: 100%;
            transform: translate(-50%, -50%) scale(5000);
        }
    </style>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Re-Categorize Information"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Re-Categorize Information"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" Style="color: Blue;
                                            vertical-align: top"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkJobfilter" runat="server" Text="Job Filter" Style="color: Blue;
                                            vertical-align: top"></asp:LinkButton>
</div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%; margin-bottom: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <uc1:DateCtrl ID="dtEffectiveDate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 60%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="border: 1px solid #DDD">
                                    </div>
                                    <table style="width: 100%; margin-top: 10px; margin-bottom: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblJob" runat="server" Text="Job"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboJob" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblChangeReason" runat="server" Text="Change Reason"></asp:Label>
                                            </td>
                                            <td style="width: 35%;">
                                                <asp:DropDownList ID="cboChangeReason" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 50%" align="right">
                                                        <td style="width: 10%">
                                                            <asp:Label ID="lblAppointmentdate" runat="server" Style="margin-left: 10px" Visible="false"
                                                                Text="Appoint Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <asp:TextBox ID="txtAppointDate" runat="server" Visible="false" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 15%" align="right">
                                                    <asp:Button ID="btnSave" runat="server" CssClass="btndefault" Text="Save Changes" />
                                            </td>
                                        </tr>
                                    </table>
                                    </td> </tr> </table>
                                    <div style="border: 1px solid #DDD">
                                    </div>
                                    <table style="width: 100%; margin-top: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto;
                                                    max-height: 350px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                    <asp:DataGrid ID="dgvHistory" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                        <ItemStyle CssClass="griviewitem" />
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" FooterText="brnView">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="imgView" runat="server" ToolTip="View" CommandName="ViewReport">
                                                                        <i class="fa fa-exclamation-circle" style="font-size: 18px;"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn FooterText="brnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                                HeaderText="" ItemStyle-Width="30px">
                                                                <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" CommandName="Edit" CssClass="gridedit"
                                                                            ToolTip="Edit"></asp:LinkButton>
                                                                        <asp:LinkButton ID="imgDetail" runat="server" ToolTip="View Detail" CommandName="View"
                                                                            Visible="false">
                                                                            <i class="fa fa-eye" style="font-size: 18px;"></i> 
                                                                        </asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                                HeaderText="" ItemStyle-Width="30px">
                                                                <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" CssClass="griddelete"
                                                                            ToolTip="Delete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="effectivedate" FooterText="dgcolhChangeDate" HeaderText="Effective Date"
                                                                ReadOnly="true"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="JobGroup" FooterText="dgcolhJobGroup" HeaderText="Job Group"
                                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Job" FooterText="dgcolhJob" HeaderText="Job" ReadOnly="true">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Reason" FooterText="dgcolhReason" HeaderText="Reason"
                                                                ReadOnly="true"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="categorizationtranunkid" FooterText="objdgcolhrecategorizeunkid"
                                                                HeaderText="objdgcolhrecategorizeunkid" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="isfromemployee" FooterText="objdgcolhFromEmp" HeaderText="objdgcolhFromEmp"
                                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="adate" FooterText="objdgcolhAppointdate" HeaderText="objdgcolhAppointdate"
                                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                                 <asp:BoundColumn DataField="rehiretranunkid" HeaderText="rehiretranunkid" Visible="false">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" Visible="false"
                                                                FooterText="objdgcolhOperationType"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="operationtypeid" HeaderText="Operationtype Id" Visible="false"
                                                                FooterText="objdgcolhOperationTypeId"></asp:BoundColumn>
                                                            <%--13--%>
                                                        </Columns>
                                                        <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                    </asp:DataGrid>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Label ID="objlblCaption" runat="server" Text="Employee Rehired" BackColor="Orange"
                                            Style="padding: 2px; font-weight: bold"></asp:Label>
                                        <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"
                                            Style="padding: 2px; font-weight: bold"></asp:Label>
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To Delete?:" />
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <asp:Panel ID="pnlJobFilter" runat="server" CssClass="newpopup fixPopup" Style="width: 650px;">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblCancelText1" Text="Job Filter" runat="server" />
                            </div>
                            <div class="panel-body">
                                <div id="Div20" class="panel-body-default">
                                    <table style="width: 100%; margin-bottom: 10px; margin-top: 10px">
                                        <tr style="width: 100%; margin-top: 10px">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblbranch" runat="server" Text="Branch"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cbobranch" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblunitgroup" Style="margin-left: 10px" runat="server" Text="Unit Group"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cbounitgroup" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%; margin-top: 10px">
                                            <td style="width: 15%">
                                                <asp:Label ID="lbldeptgroup" runat="server" Text="Dept. Group"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cbodeptgroup" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblunit" Style="margin-left: 10px" runat="server" Text="Units"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cbounit" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%; margin-top: 10px">
                                            <td style="width: 15%">
                                                <asp:Label ID="lbldepartment" runat="server" Text="Department"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cbodepartment" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblteam" Style="margin-left: 10px" runat="server" Text="Team"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboteam" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%; margin-top: 10px">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblsectiongroup" runat="server" Text="Sec. Group"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cbosectiongroup" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblclassgroup" Style="margin-left: 10px" runat="server" Text="Class Group"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboclassgroup" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%; margin-top: 10px">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblsection" runat="server" Text="Sections"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cbosection" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblclass" Style="margin-left: 10px" runat="server" Text="Class"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboclass" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%; margin-top: 10px">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblJobGroup" runat="server" Text="Job Group"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboJobGroup" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblGrade" Style="margin-left: 10px" runat="server" Text="Grade"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboGrade" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblGradeLevel" Style="margin-left: 10px" runat="server" Text="Grade Level"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboGradeLevel" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnOkFilter" runat="server" CssClass="btndefault" Text="Ok" />
                                        <asp:Button ID="btnCloseFilter" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
