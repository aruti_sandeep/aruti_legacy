﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wpg_AppRejEmpMovement.aspx.vb"
    Inherits="HR_Employee_Movement_wpg_AppRejEmpMovement" Title="Appove/Reject Employee Movement" %>

<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ViewMovementApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="panel-primary">
                    <div class="panel-heading">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Appove/Reject Employee Movement"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                <div class="row2">
                                    <div class="ib" style="width: 12%">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 20%">
                                        <asp:DropDownList ID="drpEmployee" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ib" style="width: 10%">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 20%">
                                        <asp:TextBox ID="txtApprover" runat="server" Enabled="false"></asp:TextBox>
                                        <asp:HiddenField ID="hfApprover" runat="server" />
                                    </div>
                                    <div class="ib" style="width: 10%">
                                        <asp:Label ID="lblLevel" runat="server" Text="Level"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 20%">
                                        <asp:TextBox ID="txtLevel" runat="server" Enabled="false"></asp:TextBox>
                                        <asp:HiddenField ID="hfLevel" runat="server" />
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ib" style="width: 12%">
                                        <asp:Label ID="lblMovement" runat="server" Text="Movement"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 20%">
                                        <asp:DropDownList ID="drpMovement" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    
                                     <div class="ib" style="width: 10%">
                                        <asp:Label ID="lblSelectOperationType" runat="server" Text="Opration Type"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 20%">
                                        <asp:DropDownList ID="cboOperationType" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    
                                </div>
                                <div class="btn-default">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="Div1" class="panel-default">
                            <div id="Div3" class="panel-body-default" style="position: relative">
                             <div class="row2">
                                    <div class="ib" style="width: 100%">
                                        <asp:TextBox ID="txtsearch" runat="server" AutoPostBack="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ib" style="width: 100%; height: 230px; overflow: auto">
                                        <asp:GridView ID="gvApproveRejectMovement" runat="server" AutoGenerateColumns="False"
                                            Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="icheck">
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                    ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="ChkAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAll_CheckedChanged" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="ChkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkSelect_CheckedChanged" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Code" DataField="ecode" FooterText="dgcolhecode" />
                                                <asp:BoundField HeaderText="Employee" DataField="ename" FooterText="dgcolhename" />
                                                <asp:BoundField HeaderText="Effective Date" HeaderStyle-Width="130px" DataField="EffDate"
                                                    FooterText="dgcolhEffDate" />
                                                <asp:BoundField HeaderText="Branch" DataField="branch" FooterText="dgcolhbranch" />
                                                <asp:BoundField HeaderText="Department Group" DataField="deptgroup" FooterText="dgcolhdeptgroup" />
                                                <asp:BoundField HeaderText="Department" DataField="dept" FooterText="dgcolhdept" />
                                                <asp:BoundField HeaderText="Section Group" DataField="secgroup" FooterText="dgcolhsecgroup" />
                                                <asp:BoundField HeaderText="Section" DataField="section" FooterText="dgcolhsection" />
                                                <asp:BoundField HeaderText="Unit Group" DataField="unitgrp" FooterText="dgcolhunitgrp" />
                                                <asp:BoundField HeaderText="Unit" DataField="unit" FooterText="dgcolhunit" />
                                                <asp:BoundField HeaderText="Team" DataField="team" FooterText="dgcolhteam" />
                                                <asp:BoundField HeaderText="Class Group" DataField="classgrp" FooterText="dgcolhclassgrp" />
                                                <asp:BoundField HeaderText="Class" DataField="class" FooterText="dgcolhclass" />
                                                <asp:BoundField HeaderText="Reason" DataField="CReason" FooterText="dgcolhCReason" />
                                                <asp:BoundField HeaderText="Job Group" DataField="JobGroup" FooterText="dgcolhJobGroup" />
                                                <asp:BoundField HeaderText="Job" DataField="Job" FooterText="dgcolhJob" />
                                                <asp:BoundField HeaderText="Date1" DataField="dDate1" FooterText="dgcolhdDate1" />
                                                <asp:BoundField HeaderText="Date2" DataField="dDate2" FooterText="dgcolhdDate2" />
                                                <asp:BoundField HeaderText="Permit No." DataField="work_permit_no" FooterText="dgcolhwork_permit_no" />
                                                <asp:BoundField HeaderText="Issue Place" DataField="issue_place" FooterText="dgcolhissue_place" />
                                                <asp:BoundField HeaderText="Issue Date" DataField="IDate" FooterText="dgcolhIDate" />
                                                <asp:BoundField HeaderText="Expiry Date" DataField="EDate" FooterText="dgcolhExDate" />
                                                <asp:BoundField HeaderText="Country" DataField="Country" FooterText="dgcolhCountry" />
                                                <asp:BoundField HeaderText="CostCenter" DataField="DispValue" FooterText="dgcolhDispValue" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ib" style="width: 12%">
                                        <asp:Label ID="lblRemark" runat="server" Text="Remark"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 75%">
                                        <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="btn-default">
                                    <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btndefault" />
                                    <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btndefault" />
                                    <asp:Button ID="btnShowMyReport" runat="server" Text="My Report" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc2:Cnf_YesNo ID="Cnf_WithoutRemark" runat="server" Title="Aruti" />
                    <uc3:Pop_report ID="Pop_report" runat="server" />
                    <uc2:Cnf_YesNo ID="Cnf_LeaveIssued" runat="server" Title="Aruti" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
