﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_EmployeeTransfers.aspx.vb" Inherits="HR_wPg_EmployeeTransfers"
    Title="Employee Transfers" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewMovementApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Transfer Information"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    
                                     <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" CssClass="lnkhover"
                                          Style="color: Blue; vertical-align: top"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <uc1:DateCtrl ID="dtEffectiveDate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 50%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; margin-bottom: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 15%; font-size: 12px; color: #333;">
                                                <asp:Label ID="lblallocation" runat="server" Style="font-weight: bold" Text="Allocations"></asp:Label>
                                            </td>
                                            <td style="width: 85%;" colspan="3" align="left">
                                                <div style="border: 1px solid #ddd">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%; margin-top: 10px">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboBranch" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblUnitGroup" Style="margin-left: 10px" runat="server" Text="Unit Group"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboUnitGroup" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblDepartmentGroup" runat="server" Text="Dept. Group"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboDeptGroup" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblUnits" Style="margin-left: 10px" runat="server" Text="Units"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboUnits" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboDepartment" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblTeam" Style="margin-left: 10px" runat="server" Text="Team"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboTeam" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblSectionGroup" runat="server" Text="Sec. Group"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboSecGroup" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblClassGroup" Style="margin-left: 10px" runat="server" Text="Class Group"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboClassGroup" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblSection" runat="server" Text="Sections"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboSections" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblClass" Style="margin-left: 10px" runat="server" Text="Class"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboClass" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="border: 1px solid #DDD">
                                    </div>
                                    <table style="width: 100%; margin-top: 10px;">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblChangeReason" runat="server" Text="Change Reason"></asp:Label>
                                            </td>
                                            <td style="width: 35%;">
                                                <asp:DropDownList ID="cboChangeReason" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 50%" align="right">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <asp:Panel ID="pnldata" runat="server">
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblAppointmentdate" runat="server" Style="margin-left: 10px" Text="Appoint Date"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtAppointDate" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </td>
                                                        </asp:Panel>
                                                        <td style="width: 20%" align="right">
                                                            <asp:Button ID="btnSaveChanges" runat="server" CssClass="btndefault" Text="Save Changes" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; margin-top: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <div id="scrollable-container" class="gridscroll" style="width: 100%; vertical-align: top;
                                                    overflow: auto; max-height: 350px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                    <asp:DataGrid ID="dgTransfersList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" FooterText="brnView">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="imgView" runat="server" ToolTip="View" CommandName="ViewReport" >
                                                                        <i class="fa fa-exclamation-circle" style="font-size: 18px;"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" FooterText="brnEdit">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit" ></asp:LinkButton>
                                                                        <asp:LinkButton ID="imgDetail" runat="server" ToolTip="View Detail" CommandName="View" Visible="false"><i class="fa fa-eye" style="font-size: 18px;"></i> </asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete"
                                                                            CssClass="griddelete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn  HeaderStyle-Width="100px" DataField="EffDate" HeaderText="Effective Date" ReadOnly="true"
                                                                FooterText="dgcolhChangeDate"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="branch" HeaderText="Branch" ReadOnly="true" FooterText="dgcolhBranch">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="deptgroup" HeaderText="Dept. Group" ReadOnly="true" FooterText="dgcolhDepartmentGrp">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="dept" HeaderText="Department" ReadOnly="true" FooterText="dgcolhDepartment">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="secgroup" HeaderText="Sec. Group" ReadOnly="true" FooterText="dgcolhSecGroup">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="section" HeaderText="Sections" ReadOnly="true" FooterText="dgcolhSection">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="unitname" HeaderText="Unit Group" ReadOnly="true" FooterText="dgcolhUnitGrp">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="unit" HeaderText="Unit" ReadOnly="true" FooterText="dgcolhUnit">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="team" HeaderText="Team" ReadOnly="true" FooterText="dgcolhTeam">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="classgrp" HeaderText="Class Group" ReadOnly="true" FooterText="dgcolhClsGrp">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="class" HeaderText="Class" ReadOnly="true" FooterText="dgcolhClass">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="CReason" HeaderText="Change Reason" ReadOnly="true" FooterText="dgcolhReason">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="transferunkid" HeaderText="objdgcolhtransferunkid" Visible="false"
                                                                FooterText="objdgcolhtransferunkid"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="isfromemployee" HeaderText="objdgcolhFromEmp" Visible="false"
                                                                FooterText="objdgcolhFromEmp"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="adate" HeaderText="dgcolhAppointdate" Visible="false"
                                                                FooterText="objdgcolhAppointdate"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="rehiretranunkid" HeaderText="rehiretranunkid" Visible="false">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" Visible="false" FooterText="objdgcolhOperationType"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="operationtypeid" HeaderText="Operationtype Id" Visible="false" FooterText="objdgcolhOperationTypeId"></asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Label ID="objlblCaption" runat="server" Text="Employee Rehired" BackColor="Orange" style="padding: 2px;font-weight:bold"></asp:Label>
                                        <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"  style="padding: 2px;font-weight:bold"></asp:Label>
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To Delete?:" />
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
