<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_EmployeeSkillList.aspx.vb"
    Inherits="HR_wPg_EmployeeSkillList" MasterPageFile="~/home.master" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="pnlData" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Skill List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <asp:Panel ID="pnlFilter" runat="server">
                                        <table width="100%">
                                            <%--<tr style="width: 100%">
                                                <td colspan="6">
                                                    <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Pending Employee" AutoPostBack="true" />
                                                </td>
                                            </tr>--%>
                                            <tr style="width: 100%">
                                                <td style="width: 10%">
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                </td>
                                                <td style="width: 25%">
                                                    <asp:DropDownList ID="drpEmployee" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 10%">
                                                    <asp:Label ID="lblSkillCategory" runat="server" Text="Skill Category"></asp:Label>
                                                </td>
                                                <td style="width: 25%">
                                                    <asp:DropDownList ID="drpCategory" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:Label ID="lblSkill" runat="server" Text="Skill"></asp:Label>
                                                </td>
                                                <td style="width: 25%">
                                                    <asp:DropDownList ID="drpSkill" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnNew" runat="server" Text="New" CssClass="btndefault" Width="77px"
                                            Visible="False" />
                                        <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                            <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btndefault" Text="View Detail" />
                                        <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                            <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" BackColor="LightCoral"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="PnlGrid" runat="server" ScrollBars="Auto">
                                <asp:DataGrid ID="gvSkill" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                    AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" ToolTip="Select"
                                                    CommandName="Select"></asp:LinkButton>
                                                <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" ToolTip="Select"
                                                    CommandName="Select" />--%>
                                                    
                                                    <%--'Gajanan [17-DEC-2018] -- Start--%>
                                                    <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                    <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details" CommandName="View"
                                                        Visible="false"><i class="fa fa-eye" style="font-size: 18px;"></i> </asp:LinkButton>
                                                <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                    <%--'Gajanan [17-DEC-2018] -- End--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn >
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete"
                                                        CssClass="griddelete"></asp:LinkButton>
                                                <asp:HiddenField ID="hfemployeeid" runat="server" Value='<%#Eval("empid") %>' />
                                                </span>
                                                <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                    CommandName="Delete" />--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="NAME" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Category" HeaderText="Skill Category" ReadOnly="True"
                                            FooterText="colhSkillCategory"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="SkillName" HeaderText="Skill" ReadOnly="True" FooterText="colhSkill">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Description" HeaderText="Description" ReadOnly="True"
                                            FooterText="colhDescription"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="EmpId" HeaderText="EmpId" Visible="false" FooterText="objdgcolhempid">
                                        </asp:BoundColumn>
                                        <%--'Gajanan [17-DEC-2018] -- Start--%>
                                            <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                        <asp:BoundColumn DataField="SkillTranId" HeaderText="SkillTranId" Visible="false"
                                            FooterText="objdgcolhSkillTranId"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" FooterText="colhOperationType">
                                            </asp:BoundColumn>
                                            <%--<11>--%>
                                            <%--'Gajanan [17-DEC-2018] -- End--%>
                                    </Columns>
                                </asp:DataGrid>
                            </asp:Panel>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are ou Sure You Want To delete?:" />
                    <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
<%--<div>
    <asp:Panel ID="" runat="server">
        <table width="100%" class="ContentCantroller">
            <tr style="width: 100%">
                <td style="width: 100%">
                </td>
            </tr>
            <tr style="width: 100%">
                <td style="width: 100%">
                </td>
            </tr>
        </table>
    </asp:Panel>
</div>--%>
<%--<tr style="width:100%">
                                                <td colspan="6" align="left">
                                                    <asp:Panel ID="pnlpopup" Width="100%" Height="100%" runat="server" Style="display: none"
                                                        CssClass="modalPopup">
                                                        <asp:Panel ID="pnl2" runat="server" Style="cursor: move; background-color: #DDDDDD;
                                                            border: solid 1px Gray; color: Black">
                                                            <div>
                                                                <p>
                                                                    Are ou Sure You Want To delete?:</p>
                                                            </div>
                                                        </asp:Panel>
                                                        <table style="width: 180px;">
                                                            <tr>
                                                                <td>
                                                                    Reason:
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtreason" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td align="right">
                                                                    <asp:Button ID="ButDel" runat="server" Text="Delete" />
                                                                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <cc1:ModalPopupExtender ID="popup1" runat="server" BackgroundCssClass="modalBackground"
                                                        CancelControlID="CancelButton" DropShadow="true" PopupControlID="pnlpopup" TargetControlID="txtreason">
                                                    </cc1:ModalPopupExtender>
                                                </td>
                                            </tr>--%>