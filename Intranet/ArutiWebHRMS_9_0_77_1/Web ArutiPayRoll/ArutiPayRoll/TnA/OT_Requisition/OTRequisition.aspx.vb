﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports System.Globalization
Imports System.IO
Imports System.Drawing
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Net.Dns

Partial Class OT_OTRequisition
    Inherits Basepage

#Region " Private Variable "
    Private DisplayMessage As New CommonCodes

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private objclsOT_Requisition_Tran As New clsOT_Requisition_Tran
    'Pinkal (03-Sep-2020) -- End


    Private ReadOnly mstrModuleName As String = "frmEmployeeOTRequisitionAddEdit"
    Private ReadOnly mstrModuleName1 As String = "frmEmployeeOTRequisitionList"

    Private mintEmployeeUnkId As Integer = -1
    Private mintShiftID As Integer = -1
    Private mintOTRequisitiontranunkid As Integer = 0
    Private mblnPopupOTRequisition As Boolean = False
    Private mdtPeriodStartDate As Date = Nothing
    Private mdtPeriodEndDate As Date = Nothing
    Private blnissubmited As Boolean = False
    Private mdtOTRequisitionList As DataTable
    Private currentId As String = ""
    Private mblnIsWeekend As Boolean = False
    Private mdtStarttime As DateTime = Nothing
    Private mdtEndtime As DateTime = Nothing
    'Private mintPeriodStatusid As Integer
    Private mblnIncludeCapApprover As Boolean = False
    Private mblnIsholiday As Boolean = False
    Private mblnIsDayOff As Boolean = False


    'Pinkal (27-Feb-2020) -- Start
    'Enhancement - Changes related To OT NMB Testing.
    Private objCONN As SqlConnection
    Private mintStatusId As Integer = 0
    'Pinkal (27-Feb-2020) -- End


#End Region

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

                If Request.QueryString.Count <= 0 Then Exit Sub
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END


                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End


                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If

                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                If arr.Length > 0 Then
                    mdtPeriodStartDate = eZeeDate.convertDate(arr(0).ToString()) 'Start Date
                    mdtPeriodEndDate = eZeeDate.convertDate(arr(1).ToString())   'End Date
                    HttpContext.Current.Session("Employeeunkid") = CInt(arr(2))    'Employee
                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(3))    'Company
                    mintStatusId = CInt(arr(4))    'Status


                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    'gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    'Pinkal (03-Sep-2020) -- End
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))

                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    'Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    'Pinkal (03-Sep-2020) -- End

                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    'Dim clsConfig As New clsConfigOptions
                    'clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    'Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp.ToString
                    'Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                    'Session("fmtCurrency") = clsConfig._CurrencyFormat

                    'If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                    '    Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    'Else
                    '    Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    'End If

                    'Session("UserAccessModeSetting") = clsConfig._UserAccessModeSetting.Trim()

                    Session("IsIncludeInactiveEmp") = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                    Session("EmployeeAsOnDate") = ConfigParameter._Object._EmployeeAsOnDate
                    Session("fmtCurrency") = ConfigParameter._Object._CurrencyFormat

                    If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", ConfigParameter._Object._ArutiSelfServiceURL)
                    End If

                    Session("UserAccessModeSetting") = ConfigParameter._Object._UserAccessModeSetting.Trim()

                    'clsConfig = Nothing
                    'Pinkal (03-Sep-2020) -- End


                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    Dim base As New Basepage
                    If base.IsAccessGivenUserEmp(strError, Global.User.en_loginby.Employee, CInt(Session("Employeeunkid"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page)
                        Exit Try
                    End If

                    Call GetDatabaseVersion()

                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), Nothing) = CInt(Session("Employeeunkid"))

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                    HttpContext.Current.Session("UserId") = -1
                    HttpContext.Current.Session("Employeeunkid") = CInt(Session("Employeeunkid"))
                    HttpContext.Current.Session("UserName") = "ID " & " : " & objEmployee._Employeecode & vbCrLf & "Employee : " & objEmployee._Firstname & " " & objEmployee._Surname 'objEmp._Displayname
                    HttpContext.Current.Session("Password") = objEmployee._Password
                    HttpContext.Current.Session("LeaveBalances") = 0
                    HttpContext.Current.Session("MemberName") = "Emp. : (" & objEmployee._Employeecode & ") " & objEmployee._Firstname & " " & objEmployee._Surname
                    HttpContext.Current.Session("RoleID") = 0
                    HttpContext.Current.Session("LangId") = 1
                    HttpContext.Current.Session("Firstname") = objEmployee._Firstname
                    HttpContext.Current.Session("Surname") = objEmployee._Surname
                    HttpContext.Current.Session("DisplayName") = objEmployee._Displayname
                    HttpContext.Current.Session("Theme_id") = objEmployee._Theme_Id
                    HttpContext.Current.Session("Lastview_id") = objEmployee._LastView_Id


                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    objEmployee = Nothing
                    'Pinkal (03-Sep-2020) -- End


                    strError = ""
                    If base.SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "Index.aspx")
                        Exit Try
                    End If

                    strError = ""
                    If base.SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "Index.aspx")
                        Exit Try
                    End If


                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    base = Nothing
                    'Pinkal (03-Sep-2020) -- End

                End If

            End If
            'Pinkal (27-Feb-2020) -- End



            If Session("clsuser") Is Nothing AndAlso Request.QueryString.Count <= 0 Then
                Exit Sub
            End If

            If Page.IsPostBack = False Then
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()
                Call FillListCombo()
                Call FillList(True)


                'Pinkal (27-Feb-2020) -- Start
                'Enhancement - Changes related To OT NMB Testing.
                If Request.QueryString.Count <= 0 Then
                    dtpListOTReqFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    dtpListOTReqToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                Else
                    dtpListOTReqFromDate.SetDate = mdtPeriodStartDate.Date
                    dtpListOTReqToDate.SetDate = mdtPeriodEndDate.Date
                    dtpListOTReqFromDate.Enabled = False
                    dtpListOTReqToDate.Enabled = False

                    cboListStatus.SelectedValue = mintStatusId.ToString()
                    cboListStatus.Enabled = False

                    btnListSearch_Click(btnListSearch, New EventArgs())

                    btnListNew.Visible = False
                    btnListReset.Visible = False

                End If
                'Pinkal (27-Feb-2020) -- End

            Else
                mblnPopupOTRequisition = CBool(ViewState("mblnPopupOTRequisition"))
                mintOTRequisitiontranunkid = CInt(ViewState("mintOTRequisitiontranunkid"))
                mintEmployeeUnkId = CInt(Me.ViewState("mintEmployeeUnkId"))

                mdtPeriodStartDate = CDate(Me.ViewState("PeriodStartDate"))
                mdtPeriodEndDate = CDate(Me.ViewState("PeriodEndDate"))


                ''If ViewState("mdtOTRequisitionList") IsNot Nothing Then
                ''    mdtOTRequisitionList = CType(ViewState("mdtOTRequisitionList"), DataTable)
                ''    If mdtOTRequisitionList.Rows.Count > 0 AndAlso CInt(mdtOTRequisitionList.Rows(0)("otrequisitiontranunkid").ToString) > 0 Then
                ''        gvOTRequisitionList.DataSource = mdtOTRequisitionList
                ''        gvOTRequisitionList.DataBind()
                ''    End If
                ''End If

                mblnIsWeekend = Convert.ToBoolean(Me.ViewState("mblnIsWeekend"))
                mdtStarttime = Convert.ToDateTime(Me.ViewState("mdtStarttime"))
                mdtEndtime = Convert.ToDateTime(Me.ViewState("mdtEndtime"))
                'mintPeriodStatusid = Convert.ToInt32(Me.ViewState("mintPeriodStatusid"))
                mintShiftID = CInt(Me.ViewState("shiftId"))
                blnissubmited = CBool(Me.ViewState("IsSubmitted"))
                mblnIncludeCapApprover = CBool(Me.ViewState("IncludeCapApprover"))
                mblnIsholiday = CBool(Me.ViewState("Isholiday"))
                mblnIsDayOff = CBool(Me.ViewState("IsDayOff"))
            End If

            If mblnPopupOTRequisition Then
                popupOTRequisition.Show()
            End If

            SetControlVisibility()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mblnPopupOTRequisition") = mblnPopupOTRequisition
            ViewState("mintOTRequisitiontranunkid") = mintOTRequisitiontranunkid
            If (CInt(Session("loginBy")) = CInt(Global.User.en_loginby.Employee)) Then
                pnlActualTime.Visible = False
            Else
                pnlActualTime.Visible = False
            End If
            Me.ViewState.Add("mintEmployeeUnkId", mintEmployeeUnkId)
            Me.ViewState("PeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("PeriodEndDate") = mdtPeriodEndDate

            ''Me.ViewState("mdtOTRequisitionList") = mdtOTRequisitionList

            Me.ViewState("mblnIsWeekend") = mblnIsWeekend
            Me.ViewState("mdtStarttime") = mdtStarttime
            Me.ViewState("mdtEndtime") = mdtEndtime
            ' Me.ViewState("mintPeriodStatusid") = mintPeriodStatusid
            Me.ViewState("shiftId") = mintShiftID
            Me.ViewState("IsSubmitted") = blnissubmited
            Me.ViewState("IncludeCapApprover") = mblnIncludeCapApprover
            Me.ViewState("Isholiday") = mblnIsholiday
            Me.ViewState("IsDayOff") = mblnIsDayOff

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Pinkal (27-Feb-2020) -- Start
        'Enhancement - Changes related To OT NMB Testing.
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
        'Pinkal (27-Feb-2020) -- End
    End Sub

#End Region

#Region " Private Method "

    Private Sub SetControlVisibility()
        Try
            cboActualStartTimehr.SelectedIndex = 0
            cboActualStartTimehr.Enabled = False

            cboActualStartTimemin.SelectedIndex = 0
            cboActualStartTimemin.Enabled = False

            cboActualEndTimehr.SelectedIndex = 0
            cboActualEndTimehr.Enabled = False

            cboActualEndTimemin.SelectedIndex = 0
            cboActualEndTimemin.Enabled = False

            txtReasonForAdjustment.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    'Private Sub FillAddEditCombo()
    'Dim objEmployee As New clsEmployee_Master
    'Dim objMaster As New clsMasterData
    'Dim objPeriod As New clscommom_period_Tran
    'Dim dsCombo As DataSet
    'Dim intFirstOpenPeriodID As Integer = 0
    'Try


    'Pinkal (08-Jan-2020) -- Start
    'Enhancement - NMB - Working on NMB OT Requisition Requirement.

    'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
    '                               CDate(Session("fin_startdate").ToString).Date, "Period", True)


    'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
    '                                                        CDate(Session("fin_startdate").ToString).Date, "Period", True, enStatusType.OPEN)

    'intFirstOpenPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), enStatusType.OPEN, False, True)

    ''Pinkal (08-Jan-2020) -- End

    'With cboPeriod
    '    .DataValueField = "periodunkid"
    '    .DataTextField = "name"
    '    .DataSource = dsCombo.Tables("Period")
    '    .DataBind()
    '    .SelectedValue = intFirstOpenPeriodID.ToString
    'End With

    'Pinkal (08-Jan-2020) -- Start
    'Enhancement - NMB - Working on NMB OT Requisition Requirement.
    'If (CInt(Session("loginBy")) = CInt(Global.User.en_loginby.Employee)) Then
    '    cboPeriod.Enabled = False
    'End If
    'Pinkal (08-Jan-2020) -- End

    'cboPeriod_SelectedIndexChanged(Nothing, Nothing)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex.Message, Me)
    '    End Try
    'End Sub

    Private Sub FillListCombo()

        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        'Dim objEmployee As New clsEmployee_Master
        'Dim objMaster As New clsMasterData
        'Dim objPeriod As New clscommom_period_Tran
        'Dim dsCombo As DataSet
        'Dim dtStatus As DataTable
        'Dim intFirstOpenPeriodID As Integer = 0
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Dim dtStatus As DataTable = Nothing
        'Pinkal (03-Sep-2020) -- End
        Try


            'Pinkal (08-Jan-2020) -- Start
            'Enhancement - NMB - Working on NMB OT Requisition Requirement.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
            '                          CDate(Session("fin_startdate").ToString).Date, "Period", True)


            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
            '                       CDate(Session("fin_startdate").ToString).Date, "Period", True, enStatusType.OPEN)

            'intFirstOpenPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), enStatusType.OPEN, False, True)

            'Pinkal (08-Jan-2020) -- End



            'With cboListPeriod
            '    .DataValueField = "periodunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombo.Tables("Period")
            '    .DataBind()
            '    .SelectedValue = intFirstOpenPeriodID.ToString
            'End With


            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If


            Dim objEmpAssignOT As New clsassignemp_ot
            dsCombo = objEmpAssignOT.GetList(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
                                                                 , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, CStr(Session("UserAccessModeSetting")) _
                                                                 , True, True, False, intEmpId, blnApplyFilter, "", blnSelect)

            If dsCombo IsNot Nothing AndAlso dsCombo.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsCombo.Tables(0).NewRow()
                drRow("employeecode") = Language.getMessage(mstrModuleName1, 3, "Select")
                drRow("employee") = Language.getMessage(mstrModuleName1, 3, "Select")
                drRow("EmpcodeName") = Language.getMessage(mstrModuleName1, 3, "Select")
                drRow("employeeunkid") = 0
                dsCombo.Tables(0).Rows.Add(drRow)
            End If

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objEmpAssignOT = Nothing
            'Pinkal (03-Sep-2020) -- End

            With cboListEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpcodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
            objEmpAssignOT = Nothing


            dtStatus = objMaster.getLeaveStatusList("Status", "", False, False).Tables(0).Select("statusunkid IN (0,1,2,3,6)").CopyToDataTable
            With cboListStatus
                .DataValueField = "statusunkid"
                .DataTextField = "Name"
                .DataSource = dtStatus
                .DataBind()
                .SelectedIndex = 2
            End With


            'cboListPeriod_SelectedIndexChanged(Nothing, Nothing)


            For I As Integer = 0 To 23
                Dim strHour As String = I.ToString
                If (Len(strHour) = 1) Then
                    strHour = "0" + strHour
                End If
                cboPlannedStartTimehr.Items.Add(strHour)
                cboPlannedEndTimehr.Items.Add(strHour)
                cboActualStartTimehr.Items.Add(strHour)
                cboActualEndTimehr.Items.Add(strHour)
            Next

            For I As Integer = 0 To 59
                Dim strMins As String = I.ToString
                If (Len(strMins) = 1) Then
                    strMins = "0" + strMins
                End If
                cboPlannedStartTimemin.Items.Add(strMins)
                cboPlannedEndTimemin.Items.Add(strMins)
                cboActualStartTimemin.Items.Add(strMins)
                cboActualEndTimemin.Items.Add(strMins)
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'objEmployee = Nothing
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo.Dispose()
            If dtStatus IsNot Nothing Then dtStatus.Clear()
            dtStatus.Dispose()
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    'Private Sub FillOTReqisitionList()
    '    Dim objOTReq As New clsOT_Requisition_Tran
    '    Dim dsList As New DataSet
    '    Try
    '        '    Dim strFilter As String = ""
    '        '    Dim glFilter As New clsOT_Requisition_Tran.GetListFilter

    '        '    With glFilter
    '        '        .strDatabaseName = Session("Database_Name").ToString
    '        '        .intUserUnkid = CInt(Session("UserId"))
    '        '        .intYearUnkId = CInt(Session("Fin_year"))
    '        '        .intCompanyUnkid = CInt(Session("CompanyUnkId"))
    '        '        .dtPeriodStart = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
    '        '        .dtPeriodEnd = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
    '        '        .strUserModeSetting = Session("UserAccessModeSetting").ToString
    '        '        .blnOnlyApproved = True
    '        '        .blnIncludeIn_ActiveEmployee = CBool(Session("IsIncludeInactiveEmp"))
    '        '        .strAdvanceFilter = ""
    '        '        If (CInt(Session("loginBy")) = CInt(Global.User.en_loginby.Employee)) Then
    '        '            .blnApplyUserAccessFilter = False
    '        '        Else
    '        '            .blnApplyUserAccessFilter = True
    '        '        End If
    '        '        .objDataOp = Nothing

    '        '        .intEmployeeUnkId = CInt(cboListEmployee.SelectedValue)
    '        '        .strEmpIDs = ""
    '        '        If dtpListOTReqFromDate.IsNull = False Then
    '        '            .dtRequisitionFromDate = dtpListOTReqFromDate.GetDate()
    '        '        End If
    '        '        If dtpListOTReqToDate.IsNull = False Then
    '        '            .dtRequisitionToDate = dtpListOTReqToDate.GetDate()
    '        '        End If
    '        '        .intStatusID = CInt(cboListStatus.SelectedValue)
    '        '    End With

    '        '    dsList = objOTReq.GetList("List", glFilter)

    '        '    Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
    '        '    If intRowCount <= 0 Then
    '        '        Dim dr As DataRow = dsList.Tables(0).NewRow
    '        '        dsList.Tables(0).Rows.Add(dr)
    '        '    End If

    '        '    gvOTRequisitionList.DataSource = dsList.Tables(0)
    '        '    gvOTRequisitionList.DataBind()

    '        '    If intRowCount <= 0 Then
    '        '        gvOTRequisitionList.Rows(0).Visible = False
    '        '    End If
    '        FillList(False)
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("FillOTReqisitionList:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    Finally
    '        objOTReq = Nothing
    '    End Try
    'End Sub

    Public Sub ResetFilter()
        Try
            cboListEmployee.SelectedIndex = 0
            cboListStatus.SelectedIndex = 0
            dtpListOTReqFromDate.SetDate = Nothing
            dtpListOTReqToDate.SetDate = Nothing
            FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub ResetAddedit()
        Try
            txtRequestReason.Text = ""
            cboEmployee.DataSource = Nothing
            cboEmployee.DataBind()
            txtReasonForAdjustment.Text = ""
            mintOTRequisitiontranunkid = 0
            dtpRequestDate.Enabled = True
            cboEmployee.Enabled = True
            cboPlannedStartTimehr.SelectedValue = "00"
            cboPlannedStartTimemin.SelectedValue = "00"
            cboPlannedEndTimehr.SelectedValue = "00"
            cboPlannedEndTimemin.SelectedValue = "00"
            mblnIncludeCapApprover = False
            mintShiftID = 0
            mblnIsWeekend = False
            mblnIsholiday = False
            mblnIsDayOff = False
            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            dtpRequestDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpRequestDate_TextChanged(dtpRequestDate, New EventArgs())
            'Pinkal (27-Feb-2020) -- End

            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), Me)
                Return False

                'ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Period is compulsory information.Please Select Period."), Me)
                '    cboPeriod.Focus()
                '    Return False

            ElseIf dtpRequestDate.IsNull Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Request Date is compulsory information.Please Select Request Date."), Me)
                Return False

            ElseIf IsDate(dtpRequestDate.GetDate.Date) = False OrElse IsDate(dtpRequestDate.GetDate.Date) = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, Date format(s) are not valid. Please set the correct date format(s)."), Me)
                Return False

            ElseIf cboPlannedStartTimehr.SelectedIndex = -1 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please Select Planned Start Time in Hour."), Me)
                Return False

            ElseIf cboPlannedEndTimehr.SelectedIndex = -1 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Please Select Planned End Time in Hour."), Me)
                Return False

            ElseIf cboPlannedStartTimemin.SelectedIndex = -1 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Planned Start Time in Miniutes."), Me)
                Return False

            ElseIf cboPlannedEndTimemin.SelectedIndex = -1 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Please Select Planned End Time in Miniutes."), Me)
                Return False

            ElseIf GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedStartTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedStartTimemin.SelectedItem.ToString)) = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedEndTimemin.SelectedItem.ToString)) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, Planned Start Time Should not be Same as Planned End Time ."), Me)
                Return False

                'ElseIf GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedStartTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedStartTimemin.SelectedItem.ToString)) > GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedEndTimemin.SelectedItem.ToString)) Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, Planned End Time Should not be Less than Planned Start Time ."), Me)
                '    Return False

                'ElseIf dtpRequestDate.IsNull OrElse (dtpRequestDate.GetDate.Date < mdtPeriodStartDate OrElse dtpRequestDate.GetDate.Date > mdtPeriodEndDate) Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Please Select proper Date Range."), Me)
                '    Return False

            ElseIf dtpRequestDate.IsNull Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Please Select proper Date Range."), Me)
                Return False

            ElseIf txtRequestReason.Text Is Nothing Or txtRequestReason.Text.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Reason for Request is compulsory information.Please Enter Reason for Request."), Me)
                Return False

                'ElseIf mintPeriodStatusid = enStatusType.CLOSE Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, Request Date should not be in closed period ."), Me)
                '    Return False

            End If

            'Pinkal (27-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

            'If (CInt(Session("loginBy")) = CInt(Global.User.en_loginby.User)) Then
            'If cboActualStartTimehr.SelectedIndex = -1 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Please select Actual start time in Hour."), Me)
            '    Return False

            'ElseIf cboActualEndTimehr.SelectedIndex = -1 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Please select actual end time in Hour."), Me)
            '    Return False

            'ElseIf cboActualStartTimemin.SelectedIndex = -1 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Please Select Actual Start Time in Miniutes."), Me)
            '    Return False

            'ElseIf cboActualEndTimemin.SelectedIndex = -1 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Please Select Actual End Time in Miniutes."), Me)
            '    Return False

            'ElseIf GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboActualStartTimehr.SelectedItem.ToString), Convert.ToInt32(cboActualStartTimemin.SelectedItem.ToString)) = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboActualEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboActualEndTimemin.SelectedItem.ToString)) Then
            '    If (cboActualStartTimehr.SelectedValue = "00" AndAlso cboActualStartTimemin.SelectedValue = "00" AndAlso cboActualEndTimehr.SelectedValue = "00" AndAlso cboActualEndTimemin.SelectedValue = "00") Then
            '    Else
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, Actual Start Time Should not be Same as Actual End Time ."), Me)
            '        Return False
            '    End If

            'ElseIf GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboActualStartTimehr.SelectedItem.ToString), Convert.ToInt32(cboActualStartTimemin.SelectedItem.ToString)) > GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboActualEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboActualEndTimemin.SelectedItem.ToString)) Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Sorry, Actual End Time Should not be Less than Actual Start Time ."), Me)
            '    Return False

            'ElseIf txtReasonForAdjustment.Text Is Nothing Or txtReasonForAdjustment.Text.Length <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Reason For Adjustment is compulsory information.Please Enter Reason For Adjustment."), Me)
            '    Return False

            'End If

            'End If

            'Pinkal (27-Aug-2020) -- End

            If GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedStartTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedStartTimemin.SelectedItem.ToString)) > GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedEndTimemin.SelectedItem.ToString)) Then
                Dim objEmpshiftTran As New clsEmployee_Shift_Tran
                Dim objshiftTran As New clsshift_tran
                Dim mintShiftunkId As Integer = objEmpshiftTran.GetEmployee_Current_ShiftId(dtpRequestDate.GetDate.AddDays(1).Date, CInt(cboEmployee.SelectedValue))
                objshiftTran.GetShiftTran(mintShiftunkId)
                If objshiftTran._dtShiftday IsNot Nothing AndAlso objshiftTran._dtShiftday.Rows.Count > 0 Then
                    Dim IStartTime As IEnumerable(Of DateTime) = objshiftTran._dtShiftday.AsEnumerable().Where(Function(x) x.Field(Of Integer)("dayid") = GetWeekDayNumber(WeekdayName(Weekday(dtpRequestDate.GetDate.AddDays(1).Date), False, FirstDayOfWeek.Sunday))).Select(Function(x) x.Field(Of DateTime)("starttime"))
                    If IStartTime.Count > 0 Then
                        If GetDateTimeFromCombos(dtpRequestDate.GetDate.AddDays(1).Date, IStartTime(0).Hour, IStartTime(0).Minute) <= GetDateTimeFromCombos(dtpRequestDate.GetDate.AddDays(1).Date, Convert.ToInt32(cboPlannedEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedEndTimemin.SelectedItem.ToString)) Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 30, "Sorry, You cannot apply for this OT.Reason : Planned End Time should not be overlap the next day shift start time."), Me)
                            'Pinkal (03-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                            objshiftTran = Nothing
                            objEmpshiftTran = Nothing
                            'Pinkal (03-Sep-2020) -- End
                            Return False
                        End If
                    End If
                End If
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objshiftTran = Nothing
                objEmpshiftTran = Nothing
                'Pinkal (03-Sep-2020) -- End
            End If

            Dim objApprover As New clsTnaapprover_master
            Dim dtApprover As DataTable = objApprover.GetEmployeeApprover(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                                 , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, False, -1, cboEmployee.SelectedValue.ToString() _
                                                                                                 , -1, Nothing, False, "")
            If dtApprover Is Nothing OrElse dtApprover.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Please Assign OT Approver to this employee."), Me)
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objApprover = Nothing
                'Pinkal (03-Sep-2020) -- End
                Return False
            End If
            objApprover = Nothing

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            'If mintOTRequisitiontranunkid <= 0 Then
            If mintOTRequisitiontranunkid <= 0 AndAlso (dtpRequestDate.GetDate.Date >= mdtPeriodStartDate.Date AndAlso dtpRequestDate.GetDate.Date <= mdtPeriodStartDate.AddDays(Date.DaysInMonth(mdtPeriodStartDate.Year, mdtPeriodStartDate.Month) - 1).Date) Then
                'Pinkal (10-Jun-2020) -- End

                'Pinkal (29-Jan-2020) -- Start
                'Enhancement - Changes related To OT NMB Testing.
                'If objclsOT_Requisition_Tran.getPeriodwise_PendingEmpOtStatus(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), mintOTRequisitiontranunkid, True) Then

                'Pinkal (10-Jun-2020) -- Start
                'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                'If objclsOT_Requisition_Tran.getPeriodwise_PendingEmpOtStatus(mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, CInt(cboEmployee.SelectedValue), mintOTRequisitiontranunkid, True) Then

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                Dim objclsOT_Requisition_Tran As New clsOT_Requisition_Tran
                'Pinkal (03-Sep-2020) -- End
                If objclsOT_Requisition_Tran.getPeriodwise_PendingEmpOtStatus(mdtPeriodStartDate.Date, mdtPeriodStartDate.AddDays(Date.DaysInMonth(mdtPeriodStartDate.Year, mdtPeriodStartDate.Month)).Date, CInt(cboEmployee.SelectedValue), mintOTRequisitiontranunkid, True) Then
                    'Pinkal (10-Jun-2020) -- End
                    'Pinkal (29-Jan-2020) -- End
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 29, "Sorry, you cannot apply for this ot now as your previous ot requisition has not been approved yet."), Me)
                    'Pinkal (03-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                    objclsOT_Requisition_Tran = Nothing
                    'Pinkal (03-Sep-2020) -- End
                    Return False
                End If
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objclsOT_Requisition_Tran = Nothing
                'Pinkal (03-Sep-2020) -- End
            End If


            Dim mdtplannedStartTime As DateTime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedStartTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedStartTimemin.SelectedItem.ToString))
            Dim mdtplannedEndTime As DateTime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedEndTimemin.SelectedItem.ToString))

            If mdtplannedStartTime > mdtplannedEndTime Then
                mdtplannedEndTime = mdtplannedEndTime.AddDays(1)
            End If



            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.

            'If mdtplannedStartTime < mdtStarttime AndAlso mdtplannedEndTime >= mdtStarttime Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Sorry, You cannot apply for this OT.Reason : Planned End Time should not be between the shift start time and end time."), Me)
            '    Return False
            'ElseIf mdtplannedStartTime < mdtEndtime AndAlso mdtplannedEndTime >= mdtEndtime Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Sorry, You cannot apply for this OT.Reason : Planned Start Time should be greater than shift end time."), Me)
            '    Return False
            'End If

            'Pinkal (01-Apr-2020) -- Start
            'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
            'If (mdtplannedStartTime >= mdtStarttime AndAlso mdtplannedStartTime <= mdtEndtime) OrElse (mdtplannedEndTime >= mdtStarttime AndAlso mdtplannedEndTime <= mdtEndtime) Then
            If (mblnIsWeekend = False AndAlso mblnIsDayOff = False AndAlso mblnIsholiday = False) AndAlso ((mdtplannedStartTime >= mdtStarttime AndAlso mdtplannedStartTime <= mdtEndtime) OrElse (mdtplannedEndTime >= mdtStarttime AndAlso mdtplannedEndTime <= mdtEndtime)) Then
                'Pinkal (01-Apr-2020) -- End
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Sorry, You cannot apply for this OT.Reason : Planned Start\End Time should not be between the shift start time and end time."), Me)
                Return False

                'Pinkal (28-May-2020) -- Start
                'BUG NMB:  problem solved on TnA OT Requisition whenever employee apply for OT on holiday. 
                ' ElseIf mdtplannedStartTime < mdtEndtime AndAlso mdtplannedEndTime >= mdtEndtime Then
            ElseIf (mblnIsWeekend = False AndAlso mblnIsDayOff = False AndAlso mblnIsholiday = False) AndAlso mdtplannedStartTime < mdtEndtime AndAlso mdtplannedEndTime >= mdtEndtime Then
                'Pinkal (28-May-2020) -- End
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Sorry, You cannot apply for this OT.Reason : Planned Start Time should be greater than shift end time."), Me)
                Return False
            End If


            'Pinkal (27-Feb-2020) -- End


            'If CInt(Session("AllowApplyOTForEachMonthDay")) > 0 AndAlso CInt(Session("AllowApplyOTForEachMonthDay")) < ConfigParameter._Object._CurrentDateAndTime.Day Then
            '    'If CInt(Session("AllowApplyOTForEachMonthDay")) < ConfigParameter._Object._CurrentDateAndTime.Day Then
            '    'Dim objMaster As New clsMasterData
            '    'Dim mintSelectedPeriodId As Integer = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, mdtPeriodStartDate.Date, 0, Nothing)
            '    'Dim mintCurrentPeriodId As Integer = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, ConfigParameter._Object._CurrentDateAndTime.Date, 0, Nothing)
            '    'If mintSelectedPeriodId <> mintCurrentPeriodId Then
            '    '    Dim objPeriod As New clscommom_period_Tran
            '    '    objPeriod._Periodunkid(Session("Database_Name").ToString()) = mintCurrentPeriodId
            '    '    If objPeriod._TnA_StartDate.Date > mdtPeriodEndDate.Date Then
            '    '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "Sorry, you cannot apply for this ot.Reason : The date set on configuration is already passed."), Me)
            '    '        objPeriod = Nothing
            '    '        Return False
            '    '    End If
            '    '    objPeriod = Nothing
            '    'End If
            '    'objMaster = Nothing

            '    Dim mdtDate As DateTime = New DateTime(mdtPeriodEndDate.AddMonths(1).Year, mdtPeriodEndDate.AddMonths(1).Month, CInt(Session("AllowApplyOTForEachMonthDay")))
            '    If ConfigParameter._Object._CurrentDateAndTime.Date > mdtDate Then
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "Sorry, you cannot apply for this ot.Reason : The date set on configuration is already passed."), Me)
            '        Return False
            '    End If

            'End If


            'Pinkal (02-Jun-2020) -- Start
            'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
            'If mdtPeriodStartDate.Date > dtpRequestDate.GetDate.Date Then

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            'If (ConfigParameter._Object._CurrentDateAndTime.Subtract(mdtPeriodStartDate).Days + 1) > CInt(Session("OTTenureDays")) Then
            If mdtPeriodEndDate.Date < ConfigParameter._Object._CurrentDateAndTime.Date AndAlso dtpRequestDate.GetDate.Date <= mdtPeriodStartDate.AddDays(Date.DaysInMonth(mdtPeriodStartDate.Year, mdtPeriodStartDate.Month) - 1).Date Then
                'Pinkal (10-Jun-2020) -- End
                'Pinkal (02-Jun-2020) -- End
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "Sorry, you cannot apply for this ot.Reason : The date set on configuration is already passed."), Me)
                Return False
            ElseIf dtpRequestDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 36, "Sorry, you cannot apply for this ot.Reason : you cannot apply ot requisition for future date."), Me)
                Return False
            End If


            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            Return False
        End Try
    End Function


    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private Sub SetValue()
    Private Sub SetValue(ByVal objclsOT_Requisition_Tran As clsOT_Requisition_Tran)
        'Pinkal (03-Sep-2020) -- End
        Try
            If mintOTRequisitiontranunkid > 0 Then
                objclsOT_Requisition_Tran._Otrequisitiontranunkid = mintOTRequisitiontranunkid
            End If

            objclsOT_Requisition_Tran._Employeeunkid = Convert.ToInt32(cboEmployee.SelectedValue)
            objclsOT_Requisition_Tran._Transactiondate = ConfigParameter._Object._CurrentDateAndTime.Date
            objclsOT_Requisition_Tran._Requestdate = dtpRequestDate.GetDate.Date
            objclsOT_Requisition_Tran._Ottypeunkid = 0
            'objclsOT_Requisition_Tran._Periodunkid = Convert.ToInt32(cboPeriod.SelectedValue)
            objclsOT_Requisition_Tran._Plannedstart_Time = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedStartTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedStartTimemin.SelectedItem.ToString))

            If GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedStartTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedStartTimemin.SelectedItem.ToString)) > GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedEndTimemin.SelectedItem.ToString)) Then
                objclsOT_Requisition_Tran._Plannedend_Time = GetDateTimeFromCombos(dtpRequestDate.GetDate.AddDays(1).Date, Convert.ToInt32(cboPlannedEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedEndTimemin.SelectedItem.ToString))
            Else
                objclsOT_Requisition_Tran._Plannedend_Time = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedEndTimemin.SelectedItem.ToString))
            End If

            objclsOT_Requisition_Tran._PlannedNight_Hours = CalculateNightHrs(mintShiftID, objclsOT_Requisition_Tran._Plannedstart_Time, objclsOT_Requisition_Tran._Plannedend_Time)

            Dim wkPlannedmins As Double = DateDiff(DateInterval.Second, objclsOT_Requisition_Tran._Plannedstart_Time, objclsOT_Requisition_Tran._Plannedend_Time)
            objclsOT_Requisition_Tran._Plannedot_Hours = CInt(wkPlannedmins)
            objclsOT_Requisition_Tran._Request_Reason = txtRequestReason.Text.Trim
            objclsOT_Requisition_Tran._Statusunkid = 2

            If (CInt(Session("loginBy")) = CInt(Global.User.en_loginby.User)) Then
                objclsOT_Requisition_Tran._Actualstart_Time = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboActualStartTimehr.SelectedItem.ToString), Convert.ToInt32(cboActualStartTimemin.SelectedItem.ToString))
                objclsOT_Requisition_Tran._Actualend_Time = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboActualEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboActualEndTimemin.SelectedItem.ToString))
                Dim wkActualmins As Double = DateDiff(DateInterval.Second, objclsOT_Requisition_Tran._Actualstart_Time, objclsOT_Requisition_Tran._Actualend_Time)
                objclsOT_Requisition_Tran._Actualot_Hours = CInt(wkActualmins)
                objclsOT_Requisition_Tran._Adjustment_Reason = txtReasonForAdjustment.Text.Trim
                If cboActualStartTimehr.SelectedItem.ToString = "00" AndAlso cboActualStartTimemin.SelectedItem.ToString = "00" AndAlso cboActualEndTimehr.SelectedItem.ToString = "00" AndAlso cboActualEndTimemin.SelectedItem.ToString = "00" Then
                    objclsOT_Requisition_Tran._Actualstart_Time = Nothing
                    objclsOT_Requisition_Tran._Actualend_Time = Nothing
                    objclsOT_Requisition_Tran._Actualot_Hours = 0
                End If
            End If
            objclsOT_Requisition_Tran._Finalmappedapproverunkid = 0
            objclsOT_Requisition_Tran._Issubmit_Approval = blnissubmited
            objclsOT_Requisition_Tran._ShiftId = mintShiftID
            objclsOT_Requisition_Tran._Isweekend = mblnIsWeekend
            objclsOT_Requisition_Tran._Isholiday = mblnIsholiday
            objclsOT_Requisition_Tran._IsDayOff = mblnIsDayOff


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'Call SetAtValue(mstrModuleName)
            Call SetAtValue(mstrModuleName, objclsOT_Requisition_Tran)
            'Pinkal (03-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub


    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private Sub GetValue()
    Private Sub GetValue(ByVal objclsOT_Requisition_Tran As clsOT_Requisition_Tran)
        'Pinkal (03-Sep-2020) -- End
        Try
            mintOTRequisitiontranunkid = objclsOT_Requisition_Tran._Otrequisitiontranunkid

            dtpRequestDate.SetDate = objclsOT_Requisition_Tran._Requestdate
            dtpRequestDate_TextChanged(dtpRequestDate, New EventArgs())

            mintEmployeeUnkId = objclsOT_Requisition_Tran._Employeeunkid
            'cboPeriod.SelectedValue = Convert.ToString(objclsOT_Requisition_Tran._Periodunkid)
            cboEmployee.SelectedValue = Convert.ToString(objclsOT_Requisition_Tran._Employeeunkid)
            'cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())

            mintShiftID = objclsOT_Requisition_Tran._ShiftId
            mblnIsWeekend = objclsOT_Requisition_Tran._Isweekend
            mblnIsholiday = objclsOT_Requisition_Tran._Isholiday
            mblnIsDayOff = objclsOT_Requisition_Tran._IsDayOff

            If mintOTRequisitiontranunkid > 0 Then
                dtpRequestDate.Enabled = False
                cboEmployee.Enabled = False
            End If

            If Len(objclsOT_Requisition_Tran._Plannedstart_Time.Hour.ToString) = 1 Then
                cboPlannedStartTimehr.SelectedValue = "0" + objclsOT_Requisition_Tran._Plannedstart_Time.Hour.ToString
            Else
                cboPlannedStartTimehr.SelectedValue = objclsOT_Requisition_Tran._Plannedstart_Time.Hour.ToString
            End If
            If Len(objclsOT_Requisition_Tran._Plannedstart_Time.Minute.ToString) = 1 Then
                cboPlannedStartTimemin.SelectedValue = "0" + objclsOT_Requisition_Tran._Plannedstart_Time.Minute.ToString
            Else
                cboPlannedStartTimemin.SelectedValue = objclsOT_Requisition_Tran._Plannedstart_Time.Minute.ToString
            End If


            If Len(objclsOT_Requisition_Tran._Plannedend_Time.Hour.ToString) = 1 Then
                cboPlannedEndTimehr.SelectedValue = "0" + objclsOT_Requisition_Tran._Plannedend_Time.Hour.ToString
            Else
                cboPlannedEndTimehr.SelectedValue = objclsOT_Requisition_Tran._Plannedend_Time.Hour.ToString
            End If

            If Len(objclsOT_Requisition_Tran._Plannedend_Time.Minute.ToString) = 1 Then
                cboPlannedEndTimemin.SelectedValue = "0" + objclsOT_Requisition_Tran._Plannedend_Time.Minute.ToString
            Else
                cboPlannedEndTimemin.SelectedValue = objclsOT_Requisition_Tran._Plannedend_Time.Minute.ToString
            End If
            txtRequestReason.Text = objclsOT_Requisition_Tran._Request_Reason

            If (CInt(Session("loginBy")) = CInt(Global.User.en_loginby.User)) Then

                If Len(objclsOT_Requisition_Tran._Actualstart_Time.Hour.ToString) = 1 Then
                    cboActualStartTimehr.SelectedValue = "0" + objclsOT_Requisition_Tran._Actualstart_Time.Hour.ToString
                Else
                    cboActualStartTimehr.SelectedValue = objclsOT_Requisition_Tran._Actualstart_Time.Hour.ToString
                End If

                If Len(objclsOT_Requisition_Tran._Actualstart_Time.Minute.ToString) = 1 Then
                    cboActualStartTimemin.SelectedValue = "0" + objclsOT_Requisition_Tran._Actualstart_Time.Minute.ToString
                Else
                    cboActualStartTimemin.SelectedValue = objclsOT_Requisition_Tran._Actualstart_Time.Minute.ToString
                End If

                If Len(objclsOT_Requisition_Tran._Actualend_Time.Hour.ToString) = 1 Then
                    cboActualEndTimehr.SelectedValue = "0" + objclsOT_Requisition_Tran._Actualend_Time.Hour.ToString
                Else
                    cboActualEndTimehr.SelectedValue = objclsOT_Requisition_Tran._Actualend_Time.Hour.ToString
                End If

                If Len(objclsOT_Requisition_Tran._Actualend_Time.Minute.ToString) = 1 Then
                    cboActualEndTimemin.SelectedValue = "0" + objclsOT_Requisition_Tran._Actualend_Time.Minute.ToString
                Else
                    cboActualEndTimemin.SelectedValue = objclsOT_Requisition_Tran._Actualend_Time.Minute.ToString
                End If
                txtReasonForAdjustment.Text = objclsOT_Requisition_Tran._Adjustment_Reason
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub


    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private Sub SetAtValue(ByVal xFormName As String)
    Private Sub SetAtValue(ByVal xFormName As String, ByVal objclsOT_Requisition_Tran As clsOT_Requisition_Tran)
        'Pinkal (03-Sep-2020) -- End
        Try

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objclsOT_Requisition_Tran._Userunkid = CInt(Session("UserId"))
            Else
                objclsOT_Requisition_Tran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objclsOT_Requisition_Tran._WebClientIP = CStr(Session("IP_ADD"))
            objclsOT_Requisition_Tran._WebHostName = CStr(Session("HOST_NAME"))
            objclsOT_Requisition_Tran._WebFormName = xFormName
            objclsOT_Requisition_Tran._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Function GetDateTimeFromCombos(ByVal dtCombo As DateTime, ByVal intHour As Integer, ByVal intMins As Integer) As DateTime
        Try
            If (intHour > 0) Then
                dtCombo = dtCombo.AddHours(intHour)
            End If
            If (intMins > 0) Then
                dtCombo = dtCombo.AddMinutes(intMins)
            End If
            Return dtCombo
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Function

    'Pinkal (12-Oct-2021)-- Start
    'NMB OT Requisition Performance Issue.

    'Private Sub FillList(ByVal isblank As Boolean)
    '    Dim strfilter As String = String.Empty
    '    Dim dsList As New DataSet

    '    'Pinkal (03-Sep-2020) -- Start
    '    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '    Dim objclsOT_Requisition_Tran As clsOT_Requisition_Tran
    '    'Pinkal (03-Sep-2020) -- End

    '    Try

    '        gvOTRequisitionList.DataSource = Nothing
    '        gvOTRequisitionList.DataBind()


    '        If isblank Then
    '            strfilter = "AND 1 = 2 "
    '        End If


    '        'Pinkal (27-Feb-2020) -- Start
    '        'Enhancement - Changes related To OT NMB Testing.
    '        Dim xEmployeeId As Integer = -1
    '        If CInt(cboListEmployee.SelectedValue) > 0 Then
    '            xEmployeeId = CInt(cboListEmployee.SelectedValue)
    '        End If
    '        'Pinkal (27-Feb-2020) -- End

    '        Dim blnApplyAccessFilter As Boolean = True
    '        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
    '            blnApplyAccessFilter = False
    '            'Pinkal (27-Feb-2020) -- Start
    '            'Enhancement - Changes related To OT NMB Testing.
    '            If CInt(cboListEmployee.SelectedValue) <= 0 Then
    '                xEmployeeId = 0
    '            End If
    '            'Pinkal (27-Feb-2020) -- End
    '        End If


    '        'Pinkal (29-Jan-2020) -- Start
    '        'Enhancement - Changes related To OT NMB Testing.

    '        Dim mdtStartDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    '        Dim mdtEndDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date

    '        If dtpListOTReqFromDate.IsNull = False Then
    '            mdtStartDate = dtpListOTReqFromDate.GetDate.Date
    '        End If

    '        If dtpListOTReqToDate.IsNull = False Then
    '            mdtEndDate = dtpListOTReqToDate.GetDate.Date
    '        End If



    '        'Pinkal (03-Sep-2020) -- Start
    '        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '        objclsOT_Requisition_Tran = New clsOT_Requisition_Tran
    '        'Pinkal (03-Sep-2020) -- End


    '        'Pinkal (27-Feb-2020) -- Start
    '        'Enhancement - Changes related To OT NMB Testing.

    '        'dsList = objclsOT_Requisition_Tran.GetList("List", Convert.ToString(Session("Database_Name")), Convert.ToInt32(Session("UserId")), Convert.ToInt32(Session("Fin_year")) _
    '        '                                                            , Convert.ToInt32(Session("CompanyUnkId")), Convert.ToBoolean(Session("IsIncludeInactiveEmp")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) _
    '        '                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Convert.ToString(Session("UserAccessModeSetting")), True, blnApplyAccessFilter, "", Nothing _
    '        '                                                           , Convert.ToInt32(cboListEmployee.SelectedValue), "", mdtStartDate.Date, mdtEndDate.Date _
    '        '                                                           , Convert.ToInt32(cboListStatus.SelectedValue), 0, False, -1, True, False)

    '        dsList = objclsOT_Requisition_Tran.GetList("List", Convert.ToString(Session("Database_Name")), Convert.ToInt32(Session("UserId")), Convert.ToInt32(Session("Fin_year")) _
    '                                                                   , Convert.ToInt32(Session("CompanyUnkId")), Convert.ToBoolean(Session("IsIncludeInactiveEmp")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) _
    '                                                                   , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Convert.ToString(Session("UserAccessModeSetting")), True, blnApplyAccessFilter, "", Nothing _
    '                                                                 , xEmployeeId, "", mdtStartDate.Date, mdtEndDate.Date _
    '                                                                   , Convert.ToInt32(cboListStatus.SelectedValue), 0, False, -1, True, False)
    '        'Pinkal (27-Feb-2020) -- End


    '        'Pinkal (29-Jan-2020) -- End



    '        If dsList.Tables(0).Rows.Count <= 0 Then


    '            'Pinkal (27-Feb-2020) -- Start
    '            'Enhancement - Changes related To OT NMB Testing.

    '            'dsList = objclsOT_Requisition_Tran.GetList("List", Convert.ToString(Session("Database_Name")), Convert.ToInt32(Session("UserId")), Convert.ToInt32(Session("Fin_year")) _
    '            '                                                            , Convert.ToInt32(Session("CompanyUnkId")), Convert.ToBoolean(Session("IsIncludeInactiveEmp")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) _
    '            '                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Convert.ToString(Session("UserAccessModeSetting")), True, blnApplyAccessFilter, "", Nothing _
    '            '                                                        , Convert.ToInt32(cboListEmployee.SelectedValue), "", mdtStartDate.Date, mdtEndDate.Date _
    '            '                                                        , Convert.ToInt32(cboListStatus.SelectedValue), 0, True, -1, True, False)

    '            dsList = objclsOT_Requisition_Tran.GetList("List", Convert.ToString(Session("Database_Name")), Convert.ToInt32(Session("UserId")), Convert.ToInt32(Session("Fin_year")) _
    '                                                                    , Convert.ToInt32(Session("CompanyUnkId")), Convert.ToBoolean(Session("IsIncludeInactiveEmp")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) _
    '                                                                    , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Convert.ToString(Session("UserAccessModeSetting")), True, blnApplyAccessFilter, "", Nothing _
    '                                                                   , xEmployeeId, "", mdtStartDate.Date, mdtEndDate.Date _
    '                                                                    , Convert.ToInt32(cboListStatus.SelectedValue), 0, True, -1, True, False)

    '            'Pinkal (27-Feb-2020) -- End


    '            isblank = True

    '        Else
    '            isblank = False
    '        End If

    '        If Not dsList.Tables("List").Columns.Contains("IsGrp") Then
    '            dsList.Tables("List").Columns.Add("IsGrp", Type.GetType("System.Boolean"))
    '        End If

    '        If Not dsList.Tables("List").Columns.Contains("PlannedworkedDuration") Then
    '            dsList.Tables("List").Columns.Add("PlannedworkedDuration", GetType(String))
    '        End If

    '        If Not dsList.Tables("List").Columns.Contains("ActualworkedDuration") Then
    '            dsList.Tables("List").Columns.Add("ActualworkedDuration", GetType(String))
    '        End If


    '        Dim TotalSec As Integer = 0
    '        For Each dtRow As DataRow In dsList.Tables("List").Rows
    '            dtRow("IsGrp") = False
    '            dtRow("PlannedworkedDuration") = CalculateTime(True, CInt(dtRow("plannedot_hours"))).ToString("#00.00").Replace(".", ":")
    '            dtRow("ActualworkedDuration") = CalculateTime(True, CInt(dtRow("actualot_hours"))).ToString("#00.00").Replace(".", ":")
    '        Next

    '        dsList.Tables("List").AcceptChanges()

    '        Dim mdtTran As DataTable ''= dsList.Tables(0)
    '        'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso isblank = False Then
    '        mdtTran = New DataView(dsList.Tables("List"), "", "Employeeunkid", DataViewRowState.CurrentRows).ToTable()
    '        Dim strEmployeeunkid As String = ""
    '        Dim dtTable As DataTable = mdtTran.Clone
    '        Dim dtRow As DataRow = Nothing

    '        For Each drow As DataRow In mdtTran.Rows
    '            If CStr(drow("Employeeunkid")).Trim <> strEmployeeunkid.Trim Then
    '                dtRow = dtTable.NewRow
    '                dtRow("IsGrp") = True
    '                dtRow("otrequisitiontranunkid") = drow("otrequisitiontranunkid")
    '                dtRow("empcodename") = drow("empcodename")
    '                dtRow("issubmit_approval") = drow("issubmit_approval")
    '                strEmployeeunkid = drow("Employeeunkid").ToString()
    '                dtTable.Rows.Add(dtRow)
    '            End If
    '            dtRow = dtTable.NewRow
    '            For Each dtcol As DataColumn In mdtTran.Columns
    '                dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
    '            Next
    '            dtRow("IsGrp") = False
    '            dtTable.Rows.Add(dtRow)
    '        Next

    '        dtTable.AcceptChanges()

    '        'Pinkal (03-Sep-2020) -- Start
    '        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '        'gvOTRequisitionList.DataSource = dtTable
    '        'gvOTRequisitionList.DataBind()
    '        'mdtOTRequisitionList = dtTable
    '        gvOTRequisitionList.DataSource = dtTable.Copy()
    '        gvOTRequisitionList.DataBind()
    '        mdtOTRequisitionList = dtTable.Copy
    '        If dtTable IsNot Nothing Then dtTable.Clear()
    '        dtTable.Dispose()
    '        'Pinkal (03-Sep-2020) -- End

    '        'Else
    '        'gvOTRequisitionList.DataSource = dsList.Tables("List")
    '        'gvOTRequisitionList.DataBind()
    '        'End If

    '        If isblank Then
    '            gvOTRequisitionList.Rows(0).Visible = False
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex.Message, Me)
    '        'Pinkal (03-Sep-2020) -- Start
    '        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    '    Finally
    '        If dsList IsNot Nothing Then dsList.Clear()
    '        dsList.Dispose()
    '        objclsOT_Requisition_Tran = Nothing
    '        'Pinkal (03-Sep-2020) -- End 
    '    End Try
    'End Sub

    Private Sub FillList(ByVal isblank As Boolean)
        Dim strfilter As String = String.Empty
        Dim dsList As New DataSet
        Dim objclsOT_Requisition_Tran As clsOT_Requisition_Tran
        Try


            gvOTRequisitionList.DataSource = Nothing
            gvOTRequisitionList.DataBind()

            Dim xEmployeeId As Integer = -1
            If CInt(cboListEmployee.SelectedValue) > 0 Then
                xEmployeeId = CInt(cboListEmployee.SelectedValue)
            End If

            Dim blnApplyAccessFilter As Boolean = True
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnApplyAccessFilter = False
                If CInt(cboListEmployee.SelectedValue) <= 0 Then
                    xEmployeeId = 0
                End If
            End If


            Dim mdtStartDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
            Dim mdtEndDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date

            If dtpListOTReqFromDate.IsNull = False Then
                mdtStartDate = dtpListOTReqFromDate.GetDate.Date
            End If

            If dtpListOTReqToDate.IsNull = False Then
                mdtEndDate = dtpListOTReqToDate.GetDate.Date
            End If

            objclsOT_Requisition_Tran = New clsOT_Requisition_Tran

            dsList = objclsOT_Requisition_Tran.GetList("List", Convert.ToString(Session("Database_Name")), Convert.ToInt32(Session("UserId")), Convert.ToInt32(Session("Fin_year")) _
                                                                    , Convert.ToInt32(Session("CompanyUnkId")), Convert.ToBoolean(Session("IsIncludeInactiveEmp")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) _
                                                                    , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Convert.ToString(Session("UserAccessModeSetting")), True, blnApplyAccessFilter, "", Nothing _
                                                                   , xEmployeeId, "", mdtStartDate.Date, mdtEndDate.Date _
                                                                    , Convert.ToInt32(cboListStatus.SelectedValue), 0, True, -1, True, False)

            If dsList.Tables(0).Rows.Count <= 0 Then isblank = True


            If Not dsList.Tables("List").Columns.Contains("PlannedworkedDuration") Then
                dsList.Tables("List").Columns.Add("PlannedworkedDuration", GetType(String))
            End If

            If Not dsList.Tables("List").Columns.Contains("ActualworkedDuration") Then
                dsList.Tables("List").Columns.Add("ActualworkedDuration", GetType(String))
            End If


            Dim TotalSec As Integer = 0

            Dim dtRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = False)

            If dtRow IsNot Nothing AndAlso dtRow.Count > 0 Then
                For Each dRow In dtRow
                    dRow("PlannedworkedDuration") = CalculateTime(True, CInt(dRow("plannedot_hours"))).ToString("#00.00").Replace(".", ":")
                    dRow("ActualworkedDuration") = CalculateTime(True, CInt(dRow("actualot_hours"))).ToString("#00.00").Replace(".", ":")
                    dRow.AcceptChanges()
                Next
            End If

            dsList.Tables("List").AcceptChanges()

            Dim mdtTran As DataTable ''= dsList.Tables(0)
            mdtTran = New DataView(dsList.Tables("List"), "", "Employeeunkid", DataViewRowState.CurrentRows).ToTable()
            gvOTRequisitionList.DataSource = mdtTran.Copy()
            gvOTRequisitionList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("IsGrp") = True
                drRow("otrequisitiontranunkid") = 0
                drRow("issubmit_approval") = False
                dsList.Tables(0).Rows.Add(drRow)
                gvOTRequisitionList.DataSource = dsList.Tables(0).Copy()
                gvOTRequisitionList.DataBind()

                gvOTRequisitionList.Rows(0).Visible = False

            End If

            If dsList IsNot Nothing Then dsList.Clear()
            dsList.Dispose()
            objclsOT_Requisition_Tran = Nothing
        End Try
    End Sub

    'Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
    '    Try
    '        rw.Visible = False
    '        Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
    '        'row.BackColor = ColorTranslator.FromHtml("#DDD")
    '        row.BackColor = ColorTranslator.FromHtml("#ECECEC")
    '        Dim cell As TableCell = New TableCell()
    '        cell.Text = title
    '        cell.Font.Bold = True
    '        cell.Style.Add("text-align", "left")
    '        cell.ColumnSpan = gd.Columns.Count
    '        row.Cells.Add(cell)
    '        gd.Controls(0).Controls.Add(row)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex.Message, Me)
    '    End Try

    'End Sub

    'Pinkal (12-Oct-2021)-- End


    'Pinkal (23-Nov-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.

    Private Sub SaveOTRequisition(ByVal sender As Object)
        Dim blnFlag As Boolean = False
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsOT_Requisition_Tran As clsOT_Requisition_Tran
        'Pinkal (03-Sep-2020) -- End

        Try
            If CType(sender, Button).ID.ToUpper() = btnSaveSubmitOTRequiApprover.ID.ToUpper() Then
                blnissubmited = True
            Else
                blnissubmited = False
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'SetValue()
            objclsOT_Requisition_Tran = New clsOT_Requisition_Tran
            SetValue(objclsOT_Requisition_Tran)
            'Pinkal (03-Sep-2020) -- End

            If mintOTRequisitiontranunkid > 0 Then
                blnFlag = objclsOT_Requisition_Tran.Update(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, mblnIncludeCapApprover, Nothing)
            Else
                blnFlag = objclsOT_Requisition_Tran.Insert(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, mblnIncludeCapApprover)
            End If

            If blnFlag = False And objclsOT_Requisition_Tran._Message <> "" Then
                DisplayMessage.DisplayMessage(objclsOT_Requisition_Tran._Message, Me)
                Exit Sub
            End If

            If blnFlag Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "OT Requisition saved Successfully."), Me)

                If (objclsOT_Requisition_Tran._Otrequisitiontranunkid > 0) AndAlso blnissubmited Then

                    Dim mblnApplyAccessFilter As Boolean = True
                    If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        mblnApplyAccessFilter = False
                    End If


                    'Pinkal (12-Oct-2021)-- Start
                    'NMB OT Requisition Performance Issue.

                    'Dim dsList As DataSet = objclsOT_Requisition_Tran.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                    '                                                 , False, mdtPeriodStartDate, mdtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, mblnApplyAccessFilter, "" _
                    '                                                 , Nothing, CInt(cboEmployee.SelectedValue), "", mdtPeriodStartDate, mdtPeriodEndDate, 2, 0 _
                    '                                                 , False, 1)


                    'If dsList.Tables("List").Columns.Contains("IsGrp") = False Then
                    '    Dim dc As New DataColumn("IsGrp", Type.GetType("System.Boolean"))
                    '    dc.DefaultValue = False
                    '    dsList.Tables(0).Columns.Add(dc)
                    '    dc = Nothing
                    'End If

                    'If dsList.Tables("List").Columns.Contains("IsChecked") = False Then
                    '    Dim dc As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                    '    dc.DefaultValue = True
                    '    dsList.Tables(0).Columns.Add(dc)
                    '    dc = Nothing
                    'End If

                    'Dim mdtOTTimesheet As DataTable = dsList.Tables(0).Clone()
                    'Dim mdtTran As DataTable = Nothing

                    'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    '    mdtTran = New DataView(dsList.Tables("List"), "", "Employeeunkid", DataViewRowState.CurrentRows).ToTable()
                    '    Dim dtTable As DataTable = mdtTran.Clone
                    '    Dim strEmployeeunkid As String = ""
                    '    Dim dtRow As DataRow = Nothing

                    '    For Each drow As DataRow In mdtTran.Rows
                    '        If CStr(drow("Employeeunkid")).Trim <> strEmployeeunkid.Trim Then
                    '            dtRow = dtTable.NewRow
                    '            dtRow("IsGrp") = True
                    '            dtRow("otrequisitiontranunkid") = -1
                    '            dtRow("empcodename") = drow("empcodename")
                    '            dtRow("issubmit_approval") = drow("issubmit_approval")

                    '            If IsDBNull(drow("requestdate")) = False Then
                    '                dtRow("requestdate") = CDate(drow("requestdate"))
                    '            End If

                    '            dtRow("employeeunkid") = CInt(drow("employeeunkid"))
                    '            strEmployeeunkid = drow("Employeeunkid").ToString()
                    '            dtTable.Rows.Add(dtRow)
                    '        End If
                    '        dtRow = dtTable.NewRow
                    '        For Each dtcol As DataColumn In mdtTran.Columns
                    '            If dtcol.ColumnName.Contains("empcodename") Then
                    '                dtRow(dtcol.ColumnName) = ""
                    '            ElseIf dtcol.ColumnName.Contains("ischecked") Then
                    '                dtRow(dtcol.ColumnName) = True
                    '            Else
                    '                dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    '            End If
                    '        Next
                    '        dtTable.Rows.Add(dtRow)
                    '    Next


                    '    mdtOTTimesheet = dtTable.Copy()
                    '    If dtTable IsNot Nothing Then dtTable.Clear()
                    '    dtTable = Nothing
                    '    If mdtTran IsNot Nothing Then mdtTran.Clear()
                    '    mdtTran = Nothing


                    Dim dsList As DataSet = objclsOT_Requisition_Tran.GetList("List", CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                     , False, mdtPeriodStartDate, mdtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, mblnApplyAccessFilter, "" _
                                                                     , Nothing, CInt(cboEmployee.SelectedValue), "", mdtPeriodStartDate, mdtPeriodEndDate, 2, 0 _
                                                                    , True, 1, True, False)



                    If dsList.Tables("List").Columns.Contains("IsChecked") = False Then
                        Dim dc As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                        dc.DefaultValue = True
                        dsList.Tables(0).Columns.Add(dc)
                        dc = Nothing
                    End If

                    Dim mdtOTTimesheet As DataTable = dsList.Tables(0).Copy()

                    'Pinkal (12-Oct-2021)-- End

                    Dim mintLoginTypeID As Integer = 0
                    Dim mintLoginEmployeeID As Integer = 0
                    Dim mintUserID As Integer = 0
                    If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                        mintLoginTypeID = enLogin_Mode.MGR_SELF_SERVICE
                        mintLoginEmployeeID = 0
                        mintUserID = CInt(Session("UserId"))
                    ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        mintLoginTypeID = enLogin_Mode.EMP_SELF_SERVICE
                        mintLoginEmployeeID = CInt(Session("Employeeunkid"))
                        mintUserID = 0
                    End If

                    Dim strEmployeeIDs As String = String.Join(",", mdtOTTimesheet.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray)

                    'Pinkal (12-Oct-2021)-- Start
                    'NMB OT Requisition Performance Issue.

                    'Dim dtSelectedData As DataTable = mdtOTTimesheet.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsChecked") = True).ToList().CopyToDataTable()


                    'objclsOT_Requisition_Tran.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                    '                                                                 , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), False, strEmployeeIDs _
                    '                                                               , mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, -1, dtSelectedData, mintLoginTypeID, mintLoginEmployeeID _
                    '                                                              , mintUserID, CStr(Session("ArutiSelfServiceURL")), False, mblnIncludeCapApprover, "")


                    'If mdtOTTimesheet IsNot Nothing Then mdtOTTimesheet.Clear()
                    'mdtOTTimesheet = Nothing
                    'If dtSelectedData IsNot Nothing Then dtSelectedData.Clear()
                    'dtSelectedData = Nothing
                    'If dsList IsNot Nothing Then dsList.Clear()
                    'dsList = Nothing

                    objclsOT_Requisition_Tran.Send_Notification_Approver(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                   , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), False, strEmployeeIDs _
                                                                                   , mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, -1, Nothing, mintLoginTypeID, mintLoginEmployeeID _
                                                                                   , mintUserID, CStr(Session("ArutiSelfServiceURL")), False, mblnIncludeCapApprover, "")


                    If mdtOTTimesheet IsNot Nothing Then mdtOTTimesheet.Clear()
                    mdtOTTimesheet = Nothing
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing

                    'Pinkal (12-Oct-2021)-- End

                End If

                If mintOTRequisitiontranunkid > 0 Then
                    mblnPopupOTRequisition = False
                    popupOTRequisition.Hide()
                Else
                    mblnPopupOTRequisition = True
                End If

                ResetAddedit()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsOT_Requisition_Tran = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    'Pinkal (23-Nov-2019) -- End



#End Region

#Region " Button Event "

    Protected Sub btnListSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListSearch.Click
        Try
            'Call FillOTReqisitionList()
            Call FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnListReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListReset.Click
        Try
            ResetFilter()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListNew.Click
        Try
            ResetAddedit()
            'FillAddEditCombo()
            mblnPopupOTRequisition = True
            popupOTRequisition.Show()
            mintOTRequisitiontranunkid = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnCloseOTRequiApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseOTRequiApprover.Click
        Try
            ResetAddedit()
            mblnPopupOTRequisition = False
            popupOTRequisition.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnSaveOTRequiApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveOTRequiApprover.Click, btnSaveSubmitOTRequiApprover.Click
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsOT_Requisition_Tran As clsOT_Requisition_Tran
        'Pinkal (03-Sep-2020) -- End
        Try
            If Validation() = False Then Exit Sub


            Dim strCapOTHrsForHODApprovers As String = Session("CapOTHrsForHODApprovers").ToString()

            If strCapOTHrsForHODApprovers.Length > 0 Then
                Dim arrCapOTHrsForHODApprovers As String() = strCapOTHrsForHODApprovers.Split(New Char() {":"c})
                Dim intTotalMins As Integer = 0
                If arrCapOTHrsForHODApprovers.Length > 0 Then
                    intTotalMins = (Convert.ToInt32(arrCapOTHrsForHODApprovers(0)) * 60) + Convert.ToInt32(arrCapOTHrsForHODApprovers(1))
                End If

                Dim mdtPlannedStartTime As DateTime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedStartTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedStartTimemin.SelectedItem.ToString))
                Dim mdtPlannedEndTime As DateTime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(cboPlannedEndTimehr.SelectedItem.ToString), Convert.ToInt32(cboPlannedEndTimemin.SelectedItem.ToString))

                If mdtPlannedStartTime > mdtPlannedEndTime Then
                    mdtPlannedEndTime = mdtPlannedEndTime.AddDays(1)
                End If


                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objclsOT_Requisition_Tran = New clsOT_Requisition_Tran
                'Pinkal (03-Sep-2020) -- End


                'Pinkal (29-Jan-2020) -- Start
                'Enhancement - Changes related To OT NMB Testing.
                'Dim intAppliedOThours As Double = CalculateTime(False, objclsOT_Requisition_Tran.GetEmpTotalOThours(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue).ToString(), Nothing, True, True))
                Dim intAppliedOThours As Double = CalculateTime(False, objclsOT_Requisition_Tran.GetEmpTotalOThours(mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, CInt(cboEmployee.SelectedValue).ToString(), Nothing, True, True))
                'Pinkal (29-Jan-2020) -- End

                If intTotalMins <= (intAppliedOThours + DateDiff(DateInterval.Minute, mdtPlannedStartTime, mdtPlannedEndTime)) Then
                    popupConfirmationForOTCap.Title = Language.getMessage(mstrModuleName, 34, "Confirmation")
                    popupConfirmationForOTCap.Message = Language.getMessage(mstrModuleName, 35, "Are you sure you want to apply for this OT Requisition as this employee already exceeded limit for OT Cap which is configured on configuration ?")
                    popupConfirmationForOTCap.Show()

                    If CType(sender, Button).ID.ToUpper() = btnSaveSubmitOTRequiApprover.ID.ToUpper() Then
                        blnissubmited = True
                    Else
                        blnissubmited = False
                    End If
                Else
                    SaveOTRequisition(sender)
                End If '  If intTotalMins >= DateDiff(DateInterval.Minute, mdtPlannedStartTime, mdtPlannedEndTime) Then
            Else
                SaveOTRequisition(sender)
            End If 'If strCapOTHrsForHODApprovers.Length > 0 Then

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsOT_Requisition_Tran = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub popupConfirmationOTRequiReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmationOTRequiReason.buttonYes_Click
        Try
            popupDeleteOTRequiReason.Title = Language.getMessage(mstrModuleName1, 1, "Are you sure you want to delete this OT Requisition?")
            popupDeleteOTRequiReason.Reason = ""
            popupDeleteOTRequiReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupDeleteOTRequiReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteOTRequiReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsOT_Requisition_Tran As clsOT_Requisition_Tran
        'Pinkal (03-Sep-2020) -- End
        Try
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsOT_Requisition_Tran = New clsOT_Requisition_Tran
            'Pinkal (03-Sep-2020) -- End

            objclsOT_Requisition_Tran._Otrequisitiontranunkid = mintOTRequisitiontranunkid
            objclsOT_Requisition_Tran._Isvoid = True
            objclsOT_Requisition_Tran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objclsOT_Requisition_Tran._Voiduserunkid = CInt(Session("UserId"))
                objclsOT_Requisition_Tran._VoidLoginemployeeunkid = 0
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objclsOT_Requisition_Tran._Voiduserunkid = 0
                objclsOT_Requisition_Tran._VoidLoginemployeeunkid = CInt(Session("employeeunkid"))
            End If
            objclsOT_Requisition_Tran._Voidreason = popupDeleteOTRequiReason.Reason


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'SetAtValue(mstrModuleName1)
            SetAtValue(mstrModuleName1, objclsOT_Requisition_Tran)
            'Pinkal (03-Sep-2020) -- End



            blnFlag = objclsOT_Requisition_Tran.Delete(mintOTRequisitiontranunkid)
            If blnFlag = False And objclsOT_Requisition_Tran._Message <> "" Then
                DisplayMessage.DisplayMessage(objclsOT_Requisition_Tran._Message, Me)
                Exit Sub
            Else

                'Pinkal (07-Nov-2019) -- Start
                'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.

                objclsOT_Requisition_Tran._Otrequisitiontranunkid(Nothing) = mintOTRequisitiontranunkid

                Dim mintLoginTypeID As Integer = 0
                Dim mintLoginEmployeeID As Integer = 0
                Dim mintUserID As Integer = 0
                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    mintLoginTypeID = enLogin_Mode.MGR_SELF_SERVICE
                    mintLoginEmployeeID = 0
                    mintUserID = CInt(Session("UserId"))
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    mintLoginTypeID = enLogin_Mode.EMP_SELF_SERVICE
                    mintLoginEmployeeID = CInt(Session("Employeeunkid"))
                    mintUserID = 0
                End If

                objclsOT_Requisition_Tran.Send_Notification_Employee(CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                                                                                    CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                    CBool(Session("IsIncludeInactiveEmp")), Session("UserAccessModeSetting").ToString(), _
                                                                                    CStr(objclsOT_Requisition_Tran._Employeeunkid), mintOTRequisitiontranunkid.ToString(), _
                                                                                     mdtPeriodStartDate.Date, mdtPeriodEndDate.Date, 4, mintLoginTypeID, mintLoginEmployeeID, _
                                                                                    mintUserID, popupDeleteOTRequiReason.Reason)
                'Pinkal (07-Nov-2019) -- End


                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objclsOT_Requisition_Tran = Nothing
                'Pinkal (03-Sep-2020) -- End


                ResetAddedit()
                FillList(False)
                mintOTRequisitiontranunkid = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsOT_Requisition_Tran = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub


    'Pinkal (23-Nov-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.
    Protected Sub popupConfirmationForOTCap_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmationForOTCap.buttonYes_Click
        Try
            If blnissubmited Then
                mblnIncludeCapApprover = True
                SaveOTRequisition(btnSaveSubmitOTRequiApprover)
            Else
                SaveOTRequisition(btnSaveOTRequiApprover)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'Pinkal (23-Nov-2019) -- End


#End Region

#Region "Gridview Event"

    Protected Sub gvOTRequisitionList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOTRequisitionList.RowDataBound
        ''Dim oldid As String
        Try
            SetDateFormat()
            If e.Row.RowType = DataControlRowType.DataRow Then
                ''Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                'Pinkal (04-Oct-2021)-- Start
                'Leave Planner Repor Enhancement : Creating Planned Leave Report. 
                'If gvOTRequisitionList.DataKeys(e.Row.RowIndex)("empcodename").ToString.Trim <> "" AndAlso CInt(gvOTRequisitionList.DataKeys(e.Row.RowIndex)("otrequisitiontranunkid").ToString) > 0 Then
                'Pinkal (04-Oct-2021) -- End
                ''oldid = dt.Rows(e.Row.RowIndex)("empcodename").ToString()
                ''If Convert.ToBoolean(dt.Rows(e.Row.RowIndex)("IsGrp")) = True Then
                If Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then
                    ''Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                    ''Me.AddGroup(e.Row, info1.ToTitleCase(dt.Rows(e.Row.RowIndex)("empcodename").ToString().ToLower()), gvOTRequisitionList)
                    ''currentId = oldid

                    Dim lnkEdit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                    lnkEdit.Visible = False

                    Dim lnkDelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                    lnkDelete.Visible = False

                    e.Row.Cells(3).Text = DataBinder.Eval(e.Row.DataItem, "empcodename").ToString
                    e.Row.Cells(3).ColumnSpan = e.Row.Cells.Count - 1
                    e.Row.BackColor = ColorTranslator.FromHtml("#ECECEC")
                    e.Row.Font.Bold = True

                    For i As Integer = 4 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next


                Else
                    If e.Row.Cells(3).Text <> "&nbsp;" Then
                        e.Row.Cells(3).Text = CDate(e.Row.Cells(3).Text).ToShortDateString()
                        e.Row.Cells(4).Text = CDate(e.Row.Cells(4).Text).ToShortDateString() & " " & Format(CDate(e.Row.Cells(4).Text), "HH:mm")
                        e.Row.Cells(5).Text = CDate(e.Row.Cells(5).Text).ToShortDateString() & " " & Format(CDate(e.Row.Cells(5).Text), "HH:mm")
                    End If

                    If e.Row.Cells(7).Text <> "&nbsp;" Then
                        e.Row.Cells(7).Text = CDate(e.Row.Cells(7).Text).ToShortDateString() & " " & Format(CDate(e.Row.Cells(7).Text), "HH:mm")
                        e.Row.Cells(8).Text = CDate(e.Row.Cells(8).Text).ToShortDateString() & " " & Format(CDate(e.Row.Cells(8).Text), "HH:mm")
                    End If

                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                    Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)

                    If CBool(gvOTRequisitionList.DataKeys(e.Row.RowIndex)("issubmit_approval").ToString) Then
                        lnkedit.Visible = False
                        lnkdelete.Visible = False
                    Else
                        lnkedit.Visible = True
                        lnkdelete.Visible = True
                        lnkedit.ToolTip = Language.getMessage(mstrModuleName, 26, "Edit")
                        lnkdelete.ToolTip = Language.getMessage(mstrModuleName, 27, "Delete")
                    End If
                End If

            End If

            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsOT_Requisition_Tran As clsOT_Requisition_Tran
        'Pinkal (03-Sep-2020) -- End

        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            'FillAddEditCombo()
            mintOTRequisitiontranunkid = Convert.ToInt32(lnk.CommandArgument.ToString())

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsOT_Requisition_Tran = New clsOT_Requisition_Tran
            'Pinkal (03-Sep-2020) -- End
            objclsOT_Requisition_Tran._Otrequisitiontranunkid = mintOTRequisitiontranunkid

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'GetValue()
            GetValue(objclsOT_Requisition_Tran)
            'Pinkal (03-Sep-2020) -- End

            mblnPopupOTRequisition = True
            popupOTRequisition.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsOT_Requisition_Tran = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsOT_Requisition_Tran As clsOT_Requisition_Tran
        'Pinkal (03-Sep-2020) -- End
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mintOTRequisitiontranunkid = Convert.ToInt32(lnk.CommandArgument.ToString())

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsOT_Requisition_Tran = New clsOT_Requisition_Tran
            'Pinkal (03-Sep-2020) -- End

            objclsOT_Requisition_Tran._Otrequisitiontranunkid = mintOTRequisitiontranunkid
            popupConfirmationOTRequiReason.Title = Language.getMessage(mstrModuleName1, 2, "Confirmation")
            popupConfirmationOTRequiReason.Message = Language.getMessage(mstrModuleName1, 1, "Are you sure you want to delete this OT Requisition?")
            popupConfirmationOTRequiReason.Show()


            'Pinkal (02-Jun-2020) -- Start
            'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
            dtpRequestDate.SetDate = objclsOT_Requisition_Tran._Requestdate
            dtpRequestDate_TextChanged(dtpRequestDate, New EventArgs())
            'Pinkal (02-Jun-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsOT_Requisition_Tran = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region " Dropdown Event "

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objEmpShift As New clsEmployee_Shift_Tran
        Dim objShiftTran As New clsshift_tran
        Dim objholiday As New clsemployee_holiday
        'Pinkal (03-Sep-2020) -- End
        Try
            If Convert.ToInt32(cboEmployee.SelectedValue) > 0 Then
                mintEmployeeUnkId = Convert.ToInt32(cboEmployee.SelectedValue)
            End If


            mintShiftID = objEmpShift.GetEmployee_Current_ShiftId(dtpRequestDate.GetDate.Date, mintEmployeeUnkId)
            objShiftTran.GetShiftTran(mintShiftID)

            If mintShiftID <= 0 Then Exit Sub

            mblnIsholiday = False
            mblnIsWeekend = False
            mblnIsDayOff = False

            '/*  START TO CHECK FOR HOLIDAY 

            Dim drWeekend() As DataRow = Nothing

            Dim dsData As DataSet = objholiday.GetList("List", Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                         , dtpRequestDate.GetDate.Date, dtpRequestDate.GetDate.Date, Session("UserAccessModeSetting").ToString(), True, False, False, CInt(cboEmployee.SelectedValue), , _
                                                                                         , " AND convert(char(8),lvholiday_master.holidaydate,112) >= '" & eZeeDate.convertDate(dtpRequestDate.GetDate.Date) & "' AND convert(char(8),lvholiday_master.holidaydate,112) <= '" & eZeeDate.convertDate(dtpRequestDate.GetDate.Date) & "'")

            If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
                mblnIsholiday = True
                mblnIsWeekend = False
                mblnIsDayOff = False

                drWeekend = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(dtpRequestDate.GetDate), False, FirstDayOfWeek.Sunday).ToString()) & " ")
                mdtStarttime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(Format(CDate(drWeekend(0)("starttime")), "HH")), Convert.ToInt32(Format(CDate(drWeekend(0)("starttime")), "mm")))
                mdtEndtime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(Format(CDate(drWeekend(0)("endtime")), "HH")), Convert.ToInt32(Format(CDate(drWeekend(0)("endtime")), "mm")))

                If mdtStarttime > mdtEndtime Then
                    mdtEndtime = mdtEndtime.AddDays(1)
                End If

                cboPlannedStartTimehr.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "HH")
                cboPlannedStartTimemin.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "mm")
                cboPlannedEndTimehr.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "HH")
                cboPlannedEndTimemin.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "mm")
                If (CInt(Session("loginBy")) = CInt(Global.User.en_loginby.User)) Then
                    cboActualStartTimehr.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "HH")
                    cboActualStartTimemin.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "mm")
                    cboActualEndTimehr.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "HH")
                    cboActualEndTimemin.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "mm")
                End If
            End If
            dsData.Clear()
            dsData = Nothing
            objholiday = Nothing
            '/*  END TO CHECK FOR HOLIDAY 



            '/*  START TO CHECK FOR DAYOFF 
            Dim objDayOff As New clsemployee_dayoff_Tran
            Dim dsDayOff As DataSet = objDayOff.GetList("List", Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                                        , Session("UserAccessModeSetting").ToString(), dtpRequestDate.GetDate.Date, "", CInt(cboEmployee.SelectedValue) _
                                                                                        , dtpRequestDate.GetDate.Date, dtpRequestDate.GetDate.Date)
            If dsDayOff IsNot Nothing AndAlso dsDayOff.Tables(0).Rows.Count > 0 Then

                mblnIsholiday = False
                mblnIsWeekend = False
                mblnIsDayOff = True

                drWeekend = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(dtpRequestDate.GetDate), False, FirstDayOfWeek.Sunday).ToString()) & " ")
                mdtStarttime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(Format(CDate(drWeekend(0)("starttime")), "HH")), Convert.ToInt32(Format(CDate(drWeekend(0)("starttime")), "mm")))
                mdtEndtime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(Format(CDate(drWeekend(0)("endtime")), "HH")), Convert.ToInt32(Format(CDate(drWeekend(0)("endtime")), "mm")))

                If mdtStarttime > mdtEndtime Then
                    mdtEndtime = mdtEndtime.AddDays(1)
                End If

                cboPlannedStartTimehr.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "HH")
                cboPlannedStartTimemin.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "mm")
                cboPlannedEndTimehr.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "HH")
                cboPlannedEndTimemin.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "mm")
                If (CInt(Session("loginBy")) = CInt(Global.User.en_loginby.User)) Then
                    cboActualStartTimehr.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "HH")
                    cboActualStartTimemin.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "mm")
                    cboActualEndTimehr.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "HH")
                    cboActualEndTimemin.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "mm")
                End If
            End If
            dsDayOff.Clear()
            dsDayOff = Nothing
            objDayOff = Nothing
            '/*  END TO CHECK FOR DAYOFF 




            'Pinkal (01-Apr-2020) -- Start
            'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 

            '/*  START TO CHECK FOR OT HOLIDAY
            Dim drOTHoliday As DataRow() = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(dtpRequestDate.GetDate), False, FirstDayOfWeek.Sunday).ToString()) & " AND isotholiday = 1")
            If drOTHoliday.Length > 0 Then
                mblnIsWeekend = False
                mblnIsholiday = True
                mblnIsDayOff = False

                mdtStarttime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(Format(CDate(drOTHoliday(0)("starttime")), "HH")), Convert.ToInt32(Format(CDate(drOTHoliday(0)("starttime")), "mm")))
                mdtEndtime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(Format(CDate(drOTHoliday(0)("endtime")), "HH")), Convert.ToInt32(Format(CDate(drOTHoliday(0)("endtime")), "mm")))

                If mdtStarttime > mdtEndtime Then
                    mdtEndtime = mdtEndtime.AddDays(1)
                End If

                cboPlannedStartTimehr.SelectedValue = Format(CDate(drOTHoliday(0)("starttime")), "HH")
                cboPlannedStartTimemin.SelectedValue = Format(CDate(drOTHoliday(0)("starttime")), "mm")
                cboPlannedEndTimehr.SelectedValue = Format(CDate(drOTHoliday(0)("starttime")), "HH")
                cboPlannedEndTimemin.SelectedValue = Format(CDate(drOTHoliday(0)("starttime")), "mm")
                If (CInt(Session("loginBy")) = CInt(Global.User.en_loginby.User)) Then
                    cboActualStartTimehr.SelectedValue = Format(CDate(drOTHoliday(0)("starttime")), "HH")
                    cboActualStartTimemin.SelectedValue = Format(CDate(drOTHoliday(0)("starttime")), "mm")
                    cboActualEndTimehr.SelectedValue = Format(CDate(drOTHoliday(0)("starttime")), "HH")
                    cboActualEndTimemin.SelectedValue = Format(CDate(drOTHoliday(0)("starttime")), "mm")
                End If
            End If
            '/*  END TO CHECK FOR OT HOLIDAY


            '/*  START TO CHECK FOR WEEKEND 
            'drWeekend = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(dtpRequestDate.GetDate), False, FirstDayOfWeek.Sunday).ToString()) & " AND isweekend = 1 ")
            drWeekend = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(dtpRequestDate.GetDate), False, FirstDayOfWeek.Sunday).ToString()) & " AND isweekend = 1 AND isotholiday = 0")
            'Pinkal (01-Apr-2020) -- End

            If drWeekend.Length > 0 Then
                mblnIsWeekend = True
                mblnIsholiday = False
                mblnIsDayOff = False

                mdtStarttime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(Format(CDate(drWeekend(0)("starttime")), "HH")), Convert.ToInt32(Format(CDate(drWeekend(0)("starttime")), "mm")))
                mdtEndtime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(Format(CDate(drWeekend(0)("endtime")), "HH")), Convert.ToInt32(Format(CDate(drWeekend(0)("endtime")), "mm")))

                If mdtStarttime > mdtEndtime Then
                    mdtEndtime = mdtEndtime.AddDays(1)
                End If

                cboPlannedStartTimehr.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "HH")
                cboPlannedStartTimemin.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "mm")
                cboPlannedEndTimehr.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "HH")
                cboPlannedEndTimemin.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "mm")
                If (CInt(Session("loginBy")) = CInt(Global.User.en_loginby.User)) Then
                    cboActualStartTimehr.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "HH")
                    cboActualStartTimemin.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "mm")
                    cboActualEndTimehr.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "HH")
                    cboActualEndTimemin.SelectedValue = Format(CDate(drWeekend(0)("starttime")), "mm")
                End If

                'Pinkal (01-Apr-2020) -- Start
                'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
            ElseIf mblnIsDayOff = False AndAlso mblnIsholiday = False AndAlso mblnIsWeekend = False Then
                'Pinkal (01-Apr-2020) -- End

                mblnIsWeekend = False
                drWeekend = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(dtpRequestDate.GetDate), False, FirstDayOfWeek.Sunday).ToString()) & " AND isweekend = 0 ")
                mdtStarttime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(Format(CDate(drWeekend(0)("starttime")), "HH")), Convert.ToInt32(Format(CDate(drWeekend(0)("starttime")), "mm")))
                mdtEndtime = GetDateTimeFromCombos(dtpRequestDate.GetDate.Date, Convert.ToInt32(Format(CDate(drWeekend(0)("endtime")), "HH")), Convert.ToInt32(Format(CDate(drWeekend(0)("endtime")), "mm")))

                If mdtStarttime > mdtEndtime Then
                    mdtEndtime = mdtEndtime.AddDays(1)
                End If


                'Pinkal (27-Feb-2020) -- Start
                'Enhancement - Changes related To OT NMB Testing.

                cboPlannedStartTimehr.SelectedValue = Format(CDate(drWeekend(0)("endtime")), "HH")
                cboPlannedStartTimemin.SelectedValue = Format(CDate(drWeekend(0)("endtime")).AddMinutes(1), "mm")
                cboPlannedEndTimehr.SelectedValue = Format(CDate(drWeekend(0)("endtime")), "HH")
                cboPlannedEndTimemin.SelectedValue = Format(CDate(drWeekend(0)("endtime")).AddMinutes(1), "mm")
                If (CInt(Session("loginBy")) = CInt(Global.User.en_loginby.User)) Then
                    cboActualStartTimehr.SelectedValue = Format(CDate(drWeekend(0)("endtime")), "HH")
                    cboActualStartTimemin.SelectedValue = Format(CDate(drWeekend(0)("endtime")).AddMinutes(1), "mm")
                    cboActualEndTimehr.SelectedValue = Format(CDate(drWeekend(0)("endtime")), "HH")
                    cboActualEndTimemin.SelectedValue = Format(CDate(drWeekend(0)("endtime")).AddMinutes(1), "mm")
                End If

                'Pinkal (27-Feb-2020) -- End

            End If
            '/*  END TO CHECK FOR WEEKEND

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objEmpShift = Nothing
            objShiftTran = Nothing
            objholiday = Nothing
        End Try
    End Sub

    'Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
    '    Try
    '        Dim objPeriod As New clscommom_period_Tran
    '        Dim objEmployee As New clsEmployee_Master
    '        Dim dsCombo As DataSet
    '        If CInt(cboPeriod.SelectedValue) > 0 Then
    '            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
    '            mintPeriodStatusid = Convert.ToInt32(objPeriod._Statusid)
    '            mdtPeriodStartDate = CDate(objPeriod._TnA_StartDate).Date
    '            mdtPeriodEndDate = CDate(objPeriod._TnA_EndDate).Date
    '        Else
    '            mdtPeriodStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
    '            mdtPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
    '        End If


    '        Dim blnApplyFilter As Boolean = True
    '        Dim blnSelect As Boolean = True
    '        Dim intEmpId As Integer = 0
    '        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
    '            blnSelect = False
    '            intEmpId = CInt(Session("Employeeunkid"))
    '            blnApplyFilter = False
    '        End If


    '        Dim objEmpAssignOT As New clsassignemp_ot
    '        dsCombo = objEmpAssignOT.GetList(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
    '                                                             , mdtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, True, False, intEmpId, blnApplyFilter, "", blnSelect)

    '        If dsCombo IsNot Nothing AndAlso dsCombo.Tables(0).Rows.Count <= 0 Then
    '            Dim drRow As DataRow = dsCombo.Tables(0).NewRow()
    '            drRow("employeecode") = Language.getMessage(mstrModuleName1, 3, "Select")
    '            drRow("employee") = Language.getMessage(mstrModuleName1, 3, "Select")
    '            drRow("EmpcodeName") = Language.getMessage(mstrModuleName1, 3, "Select")
    '            drRow("employeeunkid") = 0
    '            dsCombo.Tables(0).Rows.Add(drRow)
    '        End If


    '        With cboEmployee
    '            .DataValueField = "employeeunkid"
    '            .DataTextField = "EmpcodeName"
    '            .DataSource = dsCombo.Tables(0)
    '            .DataBind()
    '        End With
    '        objEmpAssignOT = Nothing


    '        mintEmployeeUnkId = CInt(cboEmployee.SelectedValue)

    '        lblPeriodStartDate.Text = Language.getMessage(mstrModuleName, 24, "Start Date : ") + Format(CDate(mdtPeriodStartDate), "dd-MMM-yyyy")
    '        lblPeriodEndDate.Text = Language.getMessage(mstrModuleName, 25, "End Date : ") + Format(CDate(mdtPeriodEndDate), "dd-MMM-yyyy")
    '        dtpRequestDate.SetDate = mdtPeriodStartDate

    '        dtpRequestDate_TextChanged(Nothing, Nothing)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub cboListPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboListPeriod.SelectedIndexChanged
    '    Try
    '        Dim objPeriod As New clscommom_period_Tran

    '        If CInt(cboListPeriod.SelectedValue) > 0 Then
    '            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboListPeriod.SelectedValue)
    '            dtpListOTReqFromDate.SetDate = CDate(objPeriod._TnA_StartDate).Date
    '            dtpListOTReqToDate.SetDate = CDate(objPeriod._TnA_EndDate).Date
    '            mdtOTRequisitionList = Nothing
    '            gvOTRequisitionList.DataSource = mdtOTRequisitionList
    '            gvOTRequisitionList.DataBind()
    '        Else
    '            dtpListOTReqFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
    '            dtpListOTReqToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
    '        End If


    '        Dim blnApplyFilter As Boolean = True
    '        Dim blnSelect As Boolean = True
    '        Dim intEmpId As Integer = 0
    '        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
    '            blnSelect = False
    '            intEmpId = CInt(Session("Employeeunkid"))
    '            blnApplyFilter = False
    '        End If


    '        Dim objEmpAssignOT As New clsassignemp_ot
    '        Dim dsCombo As DataSet = objEmpAssignOT.GetList(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
    '                                                             , mdtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, True, False, intEmpId, blnApplyFilter, "", blnSelect)
    '        objEmpAssignOT = Nothing


    '        If dsCombo IsNot Nothing AndAlso dsCombo.Tables(0).Rows.Count <= 0 Then
    '            Dim drRow As DataRow = dsCombo.Tables(0).NewRow()
    '            drRow("employeecode") = Language.getMessage(mstrModuleName1, 3, "Select")
    '            drRow("employee") = Language.getMessage(mstrModuleName1, 3, "Select")
    '            drRow("EmpcodeName") = Language.getMessage(mstrModuleName1, 3, "Select")
    '            drRow("employeeunkid") = 0
    '            dsCombo.Tables(0).Rows.Add(drRow)
    '        End If

    '        With cboListEmployee
    '            .DataValueField = "employeeunkid"
    '            .DataTextField = "empcodename"
    '            .DataSource = dsCombo.Tables(0)
    '            .DataBind()
    '        End With

    '        objPeriod = Nothing

    '        FillList(False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex.Message, Me)
    '    End Try
    'End Sub

#End Region

#Region "DatePicker Methods"

    Protected Sub dtpRequestDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpRequestDate.TextChanged
        Try

            'Pinkal (02-Jun-2020) -- Start
            'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
            If Session("OTTenureDays") IsNot Nothing AndAlso CInt(Session("OTTenureDays")) > 0 Then
                'If Session("AllowApplyOTForEachMonthDay") IsNot Nothing AndAlso CInt(Session("AllowApplyOTForEachMonthDay")) > 0 Then
                'mdtPeriodStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, CInt(Session("AllowApplyOTForEachMonthDay")) + 1)
                mdtPeriodStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
                'Pinkal (02-Jun-2020) -- End

                If mdtPeriodStartDate.Day > ConfigParameter._Object._CurrentDateAndTime.Day Then
                    'Pinkal (02-Jun-2020) -- Start
                    'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
                    'mdtPeriodStartDate = mdtPeriodStartDate.AddDays(-31)
                    mdtPeriodStartDate = mdtPeriodStartDate.AddDays(-1 * CInt(Session("OTTenureDays")) + 1)
                ElseIf mdtPeriodStartDate.Date > dtpRequestDate.GetDate.Date Then
                    Dim mdtTempStartDate As DateTime = New DateTime(dtpRequestDate.GetDate.Year, dtpRequestDate.GetDate.Month, 1)
                    If mdtTempStartDate <= dtpRequestDate.GetDate.Date Then
                        mdtPeriodStartDate = mdtTempStartDate.Date
                    End If
                Else
                    Dim mdtTempDate As DateTime = mdtPeriodStartDate.AddMonths(-1)
                    If (dtpRequestDate.GetDate.Subtract(mdtTempDate).Days + 1) <= CInt(Session("OTTenureDays")) Then
                        mdtPeriodStartDate = mdtTempDate
                    End If
                End If
                'mdtPeriodEndDate = mdtPeriodStartDate.AddDays(30)
                mdtPeriodEndDate = mdtPeriodStartDate.AddDays(CInt(Session("OTTenureDays")) - 1)
                'Pinkal (02-Jun-2020) -- End
            Else
                mdtPeriodStartDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, 1)
                mdtPeriodEndDate = New DateTime(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month, DateTime.DaysInMonth(ConfigParameter._Object._CurrentDateAndTime.Year, ConfigParameter._Object._CurrentDateAndTime.Month))
            End If

            Dim dsCombo As DataSet = Nothing
            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If


            Dim objEmpAssignOT As New clsassignemp_ot
            dsCombo = objEmpAssignOT.GetList(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
                                                                 , dtpRequestDate.GetDate.Date, CStr(Session("UserAccessModeSetting")), True, True, False, intEmpId, blnApplyFilter, "", blnSelect)

            If dsCombo IsNot Nothing AndAlso dsCombo.Tables(0).Rows.Count <= 0 Then
                Dim drRow As DataRow = dsCombo.Tables(0).NewRow()
                drRow("employeecode") = Language.getMessage(mstrModuleName1, 3, "Select")
                drRow("employee") = Language.getMessage(mstrModuleName1, 3, "Select")
                drRow("EmpcodeName") = Language.getMessage(mstrModuleName1, 3, "Select")
                drRow("employeeunkid") = 0
                dsCombo.Tables(0).Rows.Add(drRow)
            End If


            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpcodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
            objEmpAssignOT = Nothing


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'Pinkal (03-Sep-2020) -- End


            lblPeriodStartDate.Text = Language.getMessage(mstrModuleName, 24, "Start Date : ") + Format(CDate(mdtPeriodStartDate), "dd-MMM-yyyy")
            lblPeriodEndDate.Text = Language.getMessage(mstrModuleName, 25, "End Date : ") + Format(CDate(mdtPeriodEndDate), "dd-MMM-yyyy")

            cboPlannedStartTimehr.SelectedValue = "00"
            cboPlannedStartTimemin.SelectedValue = "00"
            cboPlannedEndTimehr.SelectedValue = "00"
            cboPlannedEndTimemin.SelectedValue = "00"


            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            If CInt(cboEmployee.SelectedValue) > 0 Then
                cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())
            End If
            'Pinkal (27-Feb-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(Me.lbladdeditPageHeader.ID, Me.lbladdeditPageHeader.Text)
            Language._Object.setCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Language._Object.setCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Language._Object.setCaption(Me.lblRequestDate.ID, Me.lblRequestDate.Text)
            Language._Object.setCaption(Me.lblPlannedStartTime.ID, Me.lblPlannedStartTime.Text)
            Language._Object.setCaption(Me.lblPlannedEndTime.ID, Me.lblPlannedEndTime.Text)
            Language._Object.setCaption(Me.lblRequestReason.ID, Me.lblRequestReason.Text)
            Language._Object.setCaption(Me.lblActualStartTime.ID, Me.lblActualStartTime.Text)
            Language._Object.setCaption(Me.lblActualEndTime.ID, Me.lblActualEndTime.Text)
            Language._Object.setCaption(Me.lblReasonForAdjustment.ID, Me.lblReasonForAdjustment.Text)
            Language._Object.setCaption(Me.lblPeriodStartDate.ID, Me.lblPeriodStartDate.Text)
            Language._Object.setCaption(Me.lblPeriodEndDate.ID, Me.lblPeriodEndDate.Text)
            Language._Object.setCaption(Me.btnSaveOTRequiApprover.ID, Me.btnSaveOTRequiApprover.Text.Trim.Replace("&", ""))
            Language._Object.setCaption(Me.btnSaveSubmitOTRequiApprover.ID, Me.btnSaveSubmitOTRequiApprover.Text.Trim.Replace("&", ""))
            Language._Object.setCaption(Me.btnCloseOTRequiApprover.ID, Me.btnCloseOTRequiApprover.Text.Trim.Replace("&", ""))


            Language.setLanguage(mstrModuleName1)
            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Language._Object.setCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            'Language._Object.setCaption(Me.lblListPeriod.ID, Me.lblListPeriod.Text)
            Language._Object.setCaption(Me.lblListEmployee.ID, Me.lblListEmployee.Text)
            Language._Object.setCaption(Me.lblListOTReqFromDate.ID, Me.lblListOTReqFromDate.Text)
            Language._Object.setCaption(Me.lblListOTReqToDate.ID, Me.lblListOTReqToDate.Text)
            Language._Object.setCaption(Me.lblListStatus.ID, Me.lblListStatus.Text)
            Language._Object.setCaption(Me.btnListNew.ID, Me.btnListNew.Text.Trim.Replace("&", ""))
            Language._Object.setCaption(Me.btnListSearch.ID, Me.btnListSearch.Text.Trim.Replace("&", ""))
            Language._Object.setCaption(Me.btnListReset.ID, Me.btnListReset.Text.Trim.Replace("&", ""))

            Language._Object.setCaption(Me.gvOTRequisitionList.Columns(2).FooterText, Me.gvOTRequisitionList.Columns(2).HeaderText)
            Language._Object.setCaption(Me.gvOTRequisitionList.Columns(3).FooterText, Me.gvOTRequisitionList.Columns(3).HeaderText)
            Language._Object.setCaption(Me.gvOTRequisitionList.Columns(4).FooterText, Me.gvOTRequisitionList.Columns(4).HeaderText)
            Language._Object.setCaption(Me.gvOTRequisitionList.Columns(5).FooterText, Me.gvOTRequisitionList.Columns(5).HeaderText)
            Language._Object.setCaption(Me.gvOTRequisitionList.Columns(6).FooterText, Me.gvOTRequisitionList.Columns(6).HeaderText)
            Language._Object.setCaption(Me.gvOTRequisitionList.Columns(7).FooterText, Me.gvOTRequisitionList.Columns(7).HeaderText)
            Language._Object.setCaption(Me.gvOTRequisitionList.Columns(8).FooterText, Me.gvOTRequisitionList.Columns(8).HeaderText)
            Language._Object.setCaption(Me.gvOTRequisitionList.Columns(9).FooterText, Me.gvOTRequisitionList.Columns(9).HeaderText)
            Language._Object.setCaption(Me.gvOTRequisitionList.Columns(10).FooterText, Me.gvOTRequisitionList.Columns(10).HeaderText)


        Catch Ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.lbladdeditPageHeader.Text = Language._Object.getCaption(Me.lbladdeditPageHeader.ID, Me.lbladdeditPageHeader.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblRequestDate.Text = Language._Object.getCaption(Me.lblRequestDate.ID, Me.lblRequestDate.Text)
            Me.lblPlannedStartTime.Text = Language._Object.getCaption(Me.lblPlannedStartTime.ID, Me.lblPlannedStartTime.Text)
            Me.lblPlannedEndTime.Text = Language._Object.getCaption(Me.lblPlannedEndTime.ID, Me.lblPlannedEndTime.Text)
            Me.lblRequestReason.Text = Language._Object.getCaption(Me.lblRequestReason.ID, Me.lblRequestReason.Text)
            Me.lblActualStartTime.Text = Language._Object.getCaption(Me.lblActualStartTime.ID, Me.lblActualStartTime.Text)
            Me.lblActualEndTime.Text = Language._Object.getCaption(Me.lblActualEndTime.ID, Me.lblActualEndTime.Text)
            Me.lblReasonForAdjustment.Text = Language._Object.getCaption(Me.lblReasonForAdjustment.ID, Me.lblReasonForAdjustment.Text)
            Me.lblPeriodStartDate.Text = Language._Object.getCaption(Me.lblPeriodStartDate.ID, Me.lblPeriodStartDate.Text)
            Me.lblPeriodEndDate.Text = Language._Object.getCaption(Me.lblPeriodEndDate.ID, Me.lblPeriodEndDate.Text)
            Me.btnSaveOTRequiApprover.Text = Language._Object.getCaption(Me.btnSaveOTRequiApprover.ID, Me.btnSaveOTRequiApprover.Text.Trim.Replace("&", ""))
            Me.btnSaveSubmitOTRequiApprover.Text = Language._Object.getCaption(Me.btnSaveSubmitOTRequiApprover.ID, Me.btnSaveSubmitOTRequiApprover.Text.Trim.Replace("&", ""))
            Me.btnCloseOTRequiApprover.Text = Language._Object.getCaption(Me.btnCloseOTRequiApprover.ID, Me.btnCloseOTRequiApprover.Text.Trim.Replace("&", ""))


            Language.setLanguage(mstrModuleName1)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            'Me.lblListPeriod.Text = Language._Object.getCaption(Me.lblListPeriod.ID, Me.lblListPeriod.Text)
            Me.lblListEmployee.Text = Language._Object.getCaption(Me.lblListEmployee.ID, Me.lblListEmployee.Text)
            Me.lblListOTReqFromDate.Text = Language._Object.getCaption(Me.lblListOTReqFromDate.ID, Me.lblListOTReqFromDate.Text)
            Me.lblListOTReqToDate.Text = Language._Object.getCaption(Me.lblListOTReqToDate.ID, Me.lblListOTReqToDate.Text)
            Me.lblListStatus.Text = Language._Object.getCaption(Me.lblListStatus.ID, Me.lblListStatus.Text)
            Me.btnListNew.Text = Language._Object.getCaption(Me.btnListNew.ID, Me.btnListNew.Text.Trim.Replace("&", ""))
            Me.btnListSearch.Text = Language._Object.getCaption(Me.btnListSearch.ID, Me.btnListSearch.Text.Trim.Replace("&", ""))
            Me.btnListReset.Text = Language._Object.getCaption(Me.btnListReset.ID, Me.btnListReset.Text.Trim.Replace("&", ""))

            Me.gvOTRequisitionList.Columns(2).HeaderText = Language._Object.getCaption(Me.gvOTRequisitionList.Columns(2).FooterText, Me.gvOTRequisitionList.Columns(2).HeaderText)
            Me.gvOTRequisitionList.Columns(3).HeaderText = Language._Object.getCaption(Me.gvOTRequisitionList.Columns(3).FooterText, Me.gvOTRequisitionList.Columns(3).HeaderText)
            Me.gvOTRequisitionList.Columns(4).HeaderText = Language._Object.getCaption(Me.gvOTRequisitionList.Columns(4).FooterText, Me.gvOTRequisitionList.Columns(4).HeaderText)
            Me.gvOTRequisitionList.Columns(5).HeaderText = Language._Object.getCaption(Me.gvOTRequisitionList.Columns(5).FooterText, Me.gvOTRequisitionList.Columns(5).HeaderText)
            Me.gvOTRequisitionList.Columns(6).HeaderText = Language._Object.getCaption(Me.gvOTRequisitionList.Columns(6).FooterText, Me.gvOTRequisitionList.Columns(6).HeaderText)
            Me.gvOTRequisitionList.Columns(7).HeaderText = Language._Object.getCaption(Me.gvOTRequisitionList.Columns(7).FooterText, Me.gvOTRequisitionList.Columns(7).HeaderText)
            Me.gvOTRequisitionList.Columns(8).HeaderText = Language._Object.getCaption(Me.gvOTRequisitionList.Columns(8).FooterText, Me.gvOTRequisitionList.Columns(8).HeaderText)
            Me.gvOTRequisitionList.Columns(9).HeaderText = Language._Object.getCaption(Me.gvOTRequisitionList.Columns(9).FooterText, Me.gvOTRequisitionList.Columns(9).HeaderText)
            Me.gvOTRequisitionList.Columns(10).HeaderText = Language._Object.getCaption(Me.gvOTRequisitionList.Columns(10).FooterText, Me.gvOTRequisitionList.Columns(10).HeaderText)



        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee.")
            Language.setMessage(mstrModuleName, 3, "Request Date is compulsory information.Please Select Request Date.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Date format(s) are not valid. Please set the correct date format(s).")
            Language.setMessage(mstrModuleName, 5, "Please Select Planned Start Time in Hour.")
            Language.setMessage(mstrModuleName, 6, "Please Select Planned End Time in Hour.")
            Language.setMessage(mstrModuleName, 7, "Please Select Planned Start Time in Miniutes.")
            Language.setMessage(mstrModuleName, 8, "Please Select Planned End Time in Miniutes.")
            Language.setMessage(mstrModuleName, 9, "Sorry, Planned Start Time Should not be Same as Planned End Time .")
            Language.setMessage(mstrModuleName, 11, "Please Select proper Date Range.")
            Language.setMessage(mstrModuleName, 12, "Reason for Request is compulsory information.Please Enter Reason for Request.")
            Language.setMessage(mstrModuleName, 15, "Please select Actual start time in Hour.")
            Language.setMessage(mstrModuleName, 16, "Please select actual end time in Hour.")
            Language.setMessage(mstrModuleName, 17, "Please Select Actual Start Time in Miniutes.")
            Language.setMessage(mstrModuleName, 18, "Please Select Actual End Time in Miniutes.")
            Language.setMessage(mstrModuleName, 19, "Sorry, Actual Start Time Should not be Same as Actual End Time .")
            Language.setMessage(mstrModuleName, 20, "Sorry, Actual End Time Should not be Less than Actual Start Time .")
            Language.setMessage(mstrModuleName, 21, "Reason For Adjustment is compulsory information.Please Enter Reason For Adjustment.")
            Language.setMessage(mstrModuleName, 23, "OT Requisition saved Successfully.")
            Language.setMessage(mstrModuleName, 24, "Start Date :")
            Language.setMessage(mstrModuleName, 25, "End Date :")
            Language.setMessage(mstrModuleName, 26, "Edit")
            Language.setMessage(mstrModuleName, 27, "Delete")
            Language.setMessage(mstrModuleName, 28, "Please Assign OT Approver to this employee.")
            Language.setMessage(mstrModuleName, 29, "Sorry, you cannot apply for this ot now as your previous ot requisition has not been approved yet.")
            Language.setMessage(mstrModuleName, 30, "Sorry, You cannot apply for this OT.Reason : Planned End Time should not be overlap the next day shift start time.")
            Language.setMessage(mstrModuleName, 31, "Sorry, you cannot apply for this ot.Reason : The date set on configuration is already passed.")
            Language.setMessage(mstrModuleName, 32, "Sorry, You cannot apply for this OT.Reason : Planned End Time should not be between the shift start time and end time.")
            Language.setMessage(mstrModuleName, 33, "Sorry, You cannot apply for this OT.Reason : Planned Start Time should be greater than shift end time.")
            Language.setMessage(mstrModuleName, 34, "Confirmation")
            Language.setMessage(mstrModuleName, 35, "Are you sure you want to apply for this OT Requisition as this employee already exceeded limit for OT Cap which is configured on configuration ?")
            Language.setMessage(mstrModuleName, 36, "Sorry, you cannot apply for this ot.Reason : you cannot apply ot requisition for future date.")

            Language.setMessage(mstrModuleName1, 1, "Are you sure you want to delete this OT Requisition?")
            Language.setMessage(mstrModuleName1, 2, "Confirmation")
            Language.setMessage(mstrModuleName1, 3, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
