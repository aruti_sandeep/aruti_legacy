﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_ShiftGlobalAssign.aspx.vb" Inherits="TnA_Shift_Policy_Global_Assign_wPg_ShiftGlobalAssign"
    Title="Global Shift Assign" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
            var scroll1 = {
                Y: '#<%= hfScrollPosition1.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll1.Y).val());
    }
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 75%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Shift/Policy Assignment"></asp:Label>
                        </div>
                        <div class="panel-body">
                        
                         <%--  Pinkal (06-Nov-2017) -- Start
                        Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.--%>
                        
                        <div id="FilterShiftCriteria" class="panel-default">
                                <div id="Div1" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="LblFilterShiftinfo" runat="server" Text="Filter Shift"></asp:Label>
                                    </div>
                                </div>
                                
                                <div id="FilterShiftCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="LblFilterShiftType" runat="server" Text="Shift Type"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                 <asp:DropDownList ID="cboFilterShiftType" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="LblFilterShift" runat="server" Style="margin-left: 10px" Text="Shift"></asp:Label>
                                            </td>
                                            <td style="width: 40%; text-align: left; padding-right: 15px">
                                                <asp:DropDownList ID="cboFilterShift" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                 </table>
                                 </div>
                            </div>
                            
                          <%--  Pinkal (06-Nov-2017) -- End--%>
                        
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Shift Assignment"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="LblEffectiveDate" runat="server" Text="Effective Date"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <uc2:DateCtrl ID="dtpEffectiveDate" runat="server" />
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="LblShift" runat="server" Style="margin-left: 10px" Text="Shift"></asp:Label>
                                            </td>
                                            <td style="width: 40%; text-align: left; padding-right: 15px">
                                                <asp:DropDownList ID="cboShift" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%; padding-bottom: 10px">
                                            <td style="width: 100%" colspan="4" />
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%" colspan="2">
                                                <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                            <td style="width: 50%" colspan="2" align="right">
                                                <asp:Button ID="btnAssignShift" runat="server" Text="Add" CssClass="btnDefault" Style="margin-bottom: 5px;" />
                                                <asp:Button ID="btnDeleteShift" runat="server" Text="Delete" CssClass="btnDefault"
                                                    Style="margin-bottom: 5px;" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%; padding-right: 5px" colspan="2">
                                                <div id="scrollable-container" style="overflow: auto; height: 400px; vertical-align: top"
                                                    onscroll="$(scroll.Y).val(this.scrollTop);">
                                                    <asp:UpdatePanel ID="UpdateEmployeeList" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="dgvEmp" runat="server" Style="margin: auto" AutoGenerateColumns="false"
                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkEmpSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpSelectAll_CheckedChanged" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkEmpSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpSelect_CheckedChanged" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="True" FooterText="dgcolhEcode" />
                                                                    <asp:BoundField DataField="name" HeaderText="Employee" ReadOnly="True" FooterText="dgcolhEName" />
                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="Employeeunkid" ReadOnly="True"
                                                                        Visible="false" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </td>
                                            <td style="width: 50%; padding-left: 5px" colspan="2">
                                                <div id="scrollable-container1" style="overflow: auto; height: 400px; vertical-align: top"
                                                    onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                    <asp:UpdatePanel ID="UpdateAssigedShift" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="dgvAssignedShift" runat="server" Style="margin: auto" AutoGenerateColumns="false"
                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkShiftSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkShiftSelectAll_CheckedChanged" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkShiftSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkShiftSelect_CheckedChanged" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="effectivedate" HeaderText="Effective Date" ReadOnly="True"
                                                                        FooterText="colhEffectiveDate" />
                                                                    <asp:BoundField DataField="shifttype" HeaderText="Shift Type" ReadOnly="True" FooterText="colhShiftType" />
                                                                    <asp:BoundField DataField="shiftname" HeaderText="Shift" ReadOnly="True" FooterText="colhShiftName" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
