﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.Data

#End Region


Partial Class TnA_Shift_Policy_Global_Assign_wPg_ShiftGlobalAssign
    Inherits Basepage

#Region " Private Variable "
    Dim DisplayMessage As New CommonCodes
    Dim objShiftTran As clsEmployee_Shift_Tran


    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private mstrModuleName As String = "frmShiftPolicyGlobalAssign"
    'Pinkal (06-May-2014) -- End

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objShiftTran = New clsEmployee_Shift_Tran


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            If Not IsPostBack Then
                Fill_Combo()
                objShiftTran._EmployeeUnkid = 0
                Dim mdtShiftTran As DataTable = objShiftTran._SDataTable.Copy
                dtpEffectiveDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpEffectiveDate_TextChanged(New Object, New EventArgs())
                Me.ViewState.Add("ShiftList", mdtShiftTran)
                Fill_Shift_List()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub Fill_Combo()
        Dim dsCombo As New DataSet
        Dim objShift As New clsNewshift_master
        Try
            dsCombo = objShift.getListForCombo("List", True)
            With cboShift
                .DataValueField = "shiftunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            Me.ViewState.Add("GetShiftList", dsCombo.Tables(0))

            'Pinkal (06-Nov-2017) -- Start
            'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.

            Dim objCommonMst As New clsCommon_Master
            dsCombo = Nothing
            dsCombo = objCommonMst.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "List")
            With cboFilterShiftType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            dsCombo = Nothing
            objCommonMst = Nothing

            cboFilterShiftType_SelectedIndexChanged(New Object(), New EventArgs())

            'Pinkal (06-Nov-2017) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Combo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Fill_Combo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objShift = Nothing
        End Try
    End Sub

    Private Function isValid_Data() As Boolean
        Try
            Dim mdtEmployee As DataTable = Nothing
            Dim objMaster As New clsMasterData
            Dim iPeriod As Integer = 0
            Dim objPrd As New clscommom_period_Tran

            iPeriod = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpEffectiveDate.GetDate.Date)

            If iPeriod > 0 Then
                objPrd._Periodunkid(Session("Database_Name").ToString()) = iPeriod
                If objPrd._Statusid = 2 Then  ' FOR ENSTATUSTYPE = 1 OPEN AND ENSTATUSTYPE  = 2 CLOSE
                    DisplayMessage.DisplayMessage("Sorry, you cannot assign shift. Reason : Period is already closed for the selected date.", Me)
                    Return False
                End If
            End If
            objMaster = Nothing


            mdtEmployee = CType(Me.ViewState("EmployeeList"), DataTable)
            Dim dtmp() As DataRow = mdtEmployee.Select("ischeck=true")

            If dtmp.Length <= 0 Then
                DisplayMessage.DisplayMessage("Please check atleast one employee in order to assign shift to employee(s).", Me)
                Return False
            End If

            If iPeriod > 0 Then
                Dim objTnALvTran As New clsTnALeaveTran
                Dim sValue As String = String.Empty
                Dim selectedTags As List(Of Integer) = dtmp.Select(Function(x) x.Field(Of Integer)("employeeunkid")).Distinct.ToList
                Dim result() As String = selectedTags.Select(Function(x) x.ToString()).ToArray()
                sValue = String.Join(", ", result)

                Dim mdtTnADate As DateTime = Nothing
                If objPrd._TnA_EndDate.Date < dtpEffectiveDate.GetDate.Date Then
                    mdtTnADate = dtpEffectiveDate.GetDate.Date
                Else
                    mdtTnADate = objPrd._TnA_EndDate.Date
                End If

                If objTnALvTran.IsPayrollProcessDone(iPeriod, sValue, mdtTnADate.Date, enModuleReference.TnA) = True Then
                    DisplayMessage.DisplayMessage("Sorry, You cannot assigned shift/policy for the selected date.Reason : Payroll is already processed for the last date of selected date period.", Me)
                    Return False
                End If

            End If

            If CBool(Session("PolicyManagementTNA")) = True Then
                Dim objEmp_Policy As New clsemployee_policy_tran
                Dim iAssigned As String = objEmp_Policy.Assigned_EmpIds()
                If iAssigned.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage("Sorry, you cannot assign shift to selected employee(s). Reason, There is not policy assigned to the employee(s). " & _
                                                                   "To assign policy, please enable setting from Aruti Configuration -> Option -> Leave & Time Settings -> Time and Attendance Setting -> Policy Management for TnA." & _
                                                                   "After this define policy from Time and Attendance and then assign policy to employee(s) from this screen.", Me)
                    Return False
                End If
                If iAssigned.Trim.Length > 0 Then
                    dtmp = mdtEmployee.Select("ischeck = true AND employeeunkid NOT IN(" & iAssigned & ")")
                    If dtmp.Length > 0 Then
                        DisplayMessage.DisplayMessage("Sorry, there are some selected employee(s) to whom policy is not assigned. These employee(s) will be highlighted in red and shift will not be assigned.", Me)
                        For i As Integer = 0 To dtmp.Length - 1
                            dgvEmp.Rows(mdtEmployee.Rows.IndexOf(dtmp(i))).ForeColor = Drawing.Color.Red
                        Next
                        Return False
                    End If
                End If
            End If

            If dgvAssignedShift.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage("Please add atleast one shift in order to assign shift to employee(s).", Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("isVaild_Data :- " & ex.Message, Me)
            DisplayMessage.DisplayError("isVaild_Data :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function

    Private Sub Fill_Emp_Grid()
        Dim dView As DataView = Nothing
        Dim mdtEmployee As DataTable = Nothing
        Dim objEmp As New clsEmployee_Master
        Try
            Dim dsEmp As New DataSet


            'Pinkal (06-Nov-2017) -- Start
            'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.


            'dsEmp = objEmp.GetList(Session("Database_Name").ToString(), _
            '                     CInt(Session("UserId")), _
            '                     CInt(Session("Fin_year")), _
            '                     CInt(Session("CompanyUnkId")), _
            '                     dtpEffectiveDate.GetDate.Date, _
            '                     dtpEffectiveDate.GetDate.Date, _
            '                     Session("UserAccessModeSetting").ToString(), True, _
            '                     False, "List", _
            '                     CBool(Session("ShowFirstAppointmentDate")), _
            '                     CInt(Session("Employeeunkid")), , _
            '                     CStr(Session("AccessLevelFilterString")).Substring(4))

            Dim StrCheck_Fields As String = String.Empty
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Appointed_Date & "," & clsEmployee_Master.EmpColEnum.Col_ShiftName

            dsEmp = objEmp.GetListForDynamicField(StrCheck_Fields, _
                                           Session("Database_Name").ToString(), _
                                   CInt(Session("UserId")), _
                                   CInt(Session("Fin_year")), _
                                   CInt(Session("CompanyUnkId")), _
                                   dtpEffectiveDate.GetDate.Date, _
                                   dtpEffectiveDate.GetDate.Date, _
                                   Session("UserAccessModeSetting").ToString(), True, _
                                            False, "List", , , )


            If CInt(cboFilterShift.SelectedValue) > 0 Then
                mdtEmployee = New DataView(dsEmp.Tables("List"), "shiftunkid = " & CInt(cboFilterShift.SelectedValue), "", DataViewRowState.CurrentRows).ToTable()
            Else
            mdtEmployee = New DataView(dsEmp.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            mdtEmployee.Columns(1).ColumnName = "employeecode"
            mdtEmployee.Columns(2).ColumnName = "name"

            'Pinkal (06-Nov-2017) -- End


            mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
            mdtEmployee.Columns("ischeck").DefaultValue = False
            
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Emp_Grid :- " & ex.Message, Me)
            DisplayMessage.DisplayError("Fill_Emp_Grid :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally

            If mdtEmployee Is Nothing OrElse mdtEmployee.Rows.Count <= 0 Then
                mdtEmployee = New DataTable("List")
                mdtEmployee.Columns.Add("employeecode", Type.GetType("System.String"))
                mdtEmployee.Columns.Add("name", Type.GetType("System.String"))
                mdtEmployee.Columns.Add("employeeunkid", Type.GetType("System.Int32"))

                Dim drRow As DataRow = mdtEmployee.NewRow
                drRow("employeecode") = "None"
                drRow("name") = "None"
                drRow("employeeunkid") = -1
                mdtEmployee.Rows.Add(drRow)
            End If

            If mdtEmployee.Columns.Contains("IsCheck") = False Then
                Dim dc As New DataColumn
                dc.ColumnName = "IsCheck"
                dc.DataType = Type.GetType("System.Boolean")
                dc.DefaultValue = False
                mdtEmployee.Columns.Add(dc)
            End If

            dView = mdtEmployee.DefaultView
            dgvEmp.DataSource = dView
            dgvEmp.DataBind()

            If Me.ViewState("EmployeeList") Is Nothing Then
                Me.ViewState.Add("EmployeeList", dView.ToTable)
            Else
                Me.ViewState("EmployeeList") = dView.ToTable
            End If

            dView = Nothing
            objEmp = Nothing
        End Try
    End Sub

    Private Sub Fill_Shift_List()
        Dim mdtShiftTran As DataTable = Nothing
        Try

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            mdtShiftTran = CType(Me.ViewState("ShiftList"), DataTable)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Shift_List :- " & ex.Message, Me)
            DisplayMessage.DisplayError("Fill_Shift_List :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally

            If mdtShiftTran Is Nothing OrElse mdtShiftTran.Rows.Count <= 0 Then
                mdtShiftTran = New DataTable("SList")
                mdtShiftTran.Columns.Add("shifttranunkid", Type.GetType("System.Int32"))
                mdtShiftTran.Columns.Add("employeeunkid", Type.GetType("System.Int32"))
                mdtShiftTran.Columns.Add("shiftunkid", Type.GetType("System.Int32"))
                mdtShiftTran.Columns.Add("isdefault", Type.GetType("System.Boolean"))
                mdtShiftTran.Columns.Add("userunkid", Type.GetType("System.Int32"))
                mdtShiftTran.Columns.Add("isvoid", Type.GetType("System.Boolean"))
                mdtShiftTran.Columns.Add("voiduserunkid", Type.GetType("System.Int32"))
                mdtShiftTran.Columns.Add("voiddatetime", Type.GetType("System.DateTime"))
                mdtShiftTran.Columns.Add("voidreason", Type.GetType("System.String"))
                mdtShiftTran.Columns.Add("AUD", Type.GetType("System.String"))
                mdtShiftTran.Columns.Add("GUID", Type.GetType("System.String"))
                mdtShiftTran.Columns.Add("effectivedate", Type.GetType("System.String"))
                mdtShiftTran.Columns.Add("shifttype", Type.GetType("System.String"))
                mdtShiftTran.Columns.Add("shiftname", Type.GetType("System.String"))

                'Pinkal (07-Oct-2015) -- Start
                'Enhancement - WORKING ON HYATT URGENT CHANGES REGARDING SHIFT.
                mdtShiftTran.Columns.Add("effdate", Type.GetType("System.String"))
                'Pinkal (07-Oct-2015) -- End

                Dim drRow As DataRow = mdtShiftTran.NewRow
                drRow("shifttranunkid") = -1
                drRow("employeeunkid") = -1
                drRow("shiftunkid") = -1
                drRow("isdefault") = False
                drRow("userunkid") = -1
                drRow("isvoid") = False
                drRow("voiduserunkid") = -1
                drRow("voiddatetime") = DBNull.Value
                drRow("voidreason") = ""
                drRow("AUD") = ""
                drRow("GUID") = ""
                drRow("effectivedate") = ""
                drRow("shifttype") = "None"
                drRow("shiftname") = "None"

                'Pinkal (07-Oct-2015) -- Start
                'Enhancement - WORKING ON HYATT URGENT CHANGES REGARDING SHIFT.
                drRow("effdate") = ""
                'Pinkal (07-Oct-2015) -- End

                mdtShiftTran.Rows.Add(drRow)
            End If

            If mdtShiftTran.Columns.Contains("IsCheck") = False Then
                Dim dc As New DataColumn
                dc.ColumnName = "IsCheck"
                dc.DataType = Type.GetType("System.Boolean")
                dc.DefaultValue = False
                mdtShiftTran.Columns.Add(dc)
            End If


            mdtShiftTran = New DataView(mdtShiftTran, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable

            dgvAssignedShift.DataSource = mdtShiftTran
            dgvAssignedShift.DataBind()

            If Me.ViewState("ShiftList") Is Nothing Then
                Me.ViewState.Add("ShiftList", mdtShiftTran)
            Else
                Me.ViewState("ShiftList") = mdtShiftTran
            End If
        End Try
    End Sub

#End Region

#Region "Button Event"
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~/TnA/Shift_Policy_Global_Assign/wPgViewAssignShiftPolicy.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnAssignShift_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssignShift.Click
        Try

            If CInt(cboShift.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Shift is mandatory information. Please select Shift.", Me)
                Exit Sub
            End If

            Dim mdtShiftTran As DataTable = CType(Me.ViewState("ShiftList"), DataTable)

            'Pinkal (07-Oct-2015) -- Start
            'Enhancement - WORKING ON HYATT URGENT CHANGES REGARDING SHIFT.
            'Dim dTemp() As DataRow = mdtShiftTran.Select("shiftunkid = '" & CInt(cboShift.SelectedValue) & "' AND AUD <> 'D' ")

            If mdtShiftTran IsNot Nothing AndAlso mdtShiftTran.Rows.Count > 0 Then
                Dim intCount As Integer = -1
                intCount = (From n In mdtShiftTran.AsEnumerable() Where (n.Field(Of String)("AUD") <> "D" And n.Field(Of String)("AUD") <> "") Select n.Field(Of String)("effdate")).Count()

                If intCount > 0 Then
                    Dim dtMaxdate As DateTime = eZeeDate.convertDate((From n In mdtShiftTran.AsEnumerable() Where n.Field(Of String)("AUD") <> "D" Select n.Field(Of String)("effdate")).Max())
                    Dim query = (From n In mdtShiftTran.AsEnumerable() Where n.Field(Of String)("AUD") <> "D" AndAlso n.Field(Of String)("effdate") = eZeeDate.convertDate(dtMaxdate) Select n)
                    Dim intshiftId As Integer = CInt(query(0)("shiftunkid"))

                    If eZeeDate.convertDate(dtMaxdate.Date) = eZeeDate.convertDate(dtpEffectiveDate.GetDate.Date) Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot assign this shift.") & vbCrLf & _
                                                     Language.getMessage(mstrModuleName, 31, "Reason : shift is already assign on this date."), Me)
                        Exit Sub
                    ElseIf eZeeDate.convertDate(dtMaxdate.Date) > eZeeDate.convertDate(dtpEffectiveDate.GetDate.Date) Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Sorry, you cannot assign the past date to the employee(s). Reason : future date entry is present which is") & " " & dtMaxdate.Date & ".", Me)
                        Exit Sub
                    End If

                    If intshiftId = CInt(cboShift.SelectedValue) Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "This Shift is already added to the list. Please add another shift."), Me)
                Exit Sub
            End If
                End If

            End If

            If mdtShiftTran.Rows.Count > 0 Then
                Dim iShiftId As Integer = -1
                Dim dtOldDate As Date = Nothing
                Dim drTemp() As DataRow = mdtShiftTran.Select("AUD <> 'D' AND shiftunkid >0")
                If drTemp.Length > 0 Then
                    iShiftId = CInt(drTemp(drTemp.Length - 1).Item("shiftunkid"))
                    dtOldDate = eZeeDate.convertDate((drTemp(drTemp.Length - 1).Item("effdate")).ToString()).Date 
                    'If clsEmployee_Shift_Tran.Shift_Assignment(iShiftId, CInt(cboShift.SelectedValue)) = False Then
Dim clsEmployee_Shift_Tran As New clsEmployee_Shift_Tran
                    With clsEmployee_Shift_Tran
                        ._FormName = mstrModuleName
                        If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                            ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                        Else
                            ._AuditUserId = Convert.ToInt32(Session("UserId"))
                        End If
                        ._ClientIP = Session("IP_ADD").ToString()
                        ._HostName = Session("HOST_NAME").ToString()
                        ._FromWeb = True
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                    End With
                    If clsEmployee_Shift_Tran.Shift_Assignment(iShiftId, CInt(cboShift.SelectedValue), dtOldDate, dtpEffectiveDate.getdate.date) = False Then
                        DisplayMessage.DisplayMessage("Sorry, you cannot assign this shift.The possible reason is that current shift timings overlap with some existing shift timings.", Me)
                        Exit Sub
                    End If
                End If
            End If

            'Pinkal (07-Oct-2015) -- End

            Dim dtSRow As DataRow
            dtSRow = mdtShiftTran.NewRow

            dtSRow.Item("shifttranunkid") = -1
            dtSRow.Item("employeeunkid") = 0
            dtSRow.Item("shiftunkid") = CInt(cboShift.SelectedValue)
            dtSRow.Item("isdefault") = False
            dtSRow.Item("userunkid") = Session("UserId")
            dtSRow.Item("isvoid") = False
            dtSRow.Item("voiduserunkid") = -1
            dtSRow.Item("voiddatetime") = DBNull.Value
            dtSRow.Item("voidreason") = ""
            dtSRow.Item("AUD") = "A"
            dtSRow.Item("GUID") = Guid.NewGuid.ToString
            dtSRow.Item("effectivedate") = dtpEffectiveDate.GetDate.ToShortDateString
            If CInt(cboShift.SelectedValue) > 0 Then
                Dim dtShift As DataTable = CType(Me.ViewState("GetShiftList"), DataTable)
                Dim dSType() As DataRow = dtShift.Select("shiftunkid = '" & CInt(cboShift.SelectedValue) & "'")
                If dSType.Length > 0 Then
                    dtSRow.Item("shifttype") = dSType(0).Item("shifttype")
                End If
            End If
            dtSRow.Item("shiftname") = cboShift.SelectedItem.Text

            'Pinkal (07-Oct-2015) -- Start
            'Enhancement - WORKING ON HYATT URGENT CHANGES REGARDING SHIFT.
            dtSRow.Item("effdate") = eZeeDate.convertDate(dtpEffectiveDate.GetDate.Date)
            'Pinkal (07-Oct-2015) -- End

            mdtShiftTran.Rows.Add(dtSRow)

            Me.ViewState("ShiftList") = mdtShiftTran

            Call Fill_Shift_List()

            'Pinkal (07-Oct-2015) -- Start
            'Enhancement - WORKING ON HYATT URGENT CHANGES REGARDING SHIFT.
            'dtpEffectiveDate.Enabled = False
            'Pinkal (07-Oct-2015) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnAssignShift_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnAssignShift_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnDeleteShift_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteShift.Click
        Try
            Dim drTemp As DataRow() = Nothing
            Dim mdtShiftTran As DataTable

            mdtShiftTran = CType(Me.ViewState("ShiftList"), DataTable)
            drTemp = mdtShiftTran.Select("ischeck = true  AND shiftunkid > 0")
            If drTemp.Length > 0 Then
                drTemp(0).Item("AUD") = "D"
                Call Fill_Shift_List()
                mdtShiftTran = CType(Me.ViewState("ShiftList"), DataTable)
                drTemp = mdtShiftTran.Select("ischeck = true  AND shiftunkid > 0")
                If drTemp.Length <= 0 Then
                    dtpEffectiveDate.Enabled = True
                End If
            Else
                DisplayMessage.DisplayMessage("There is no row to delete shift.", Me)
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnDeleteShift_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnDeleteShift_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If isValid_Data() = False Then Exit Sub

            Dim blnFlag As Boolean = False
            Dim iShiftIds As String = String.Empty
            Dim objleaveForm As New clsleaveform
            Dim iCurrShiftId As Integer = 0
            Dim iSameShift As Boolean = False
            Dim mblnInsertFlag As Boolean = False
            Dim mblnLeaveAssigned As Boolean = False

            Dim mdtEmployee As DataTable = CType(Me.ViewState("EmployeeList"), DataTable)
            Dim mdtShifttran As DataTable = CType(Me.ViewState("ShiftList"), DataTable)
            mdtShifttran = New DataView(mdtShifttran, "effectivedate <> ''", "", DataViewRowState.CurrentRows).ToTable
            Dim dtmp() As DataRow = mdtEmployee.Select("ischeck=true")

            'Dim lRows As IEnumerable(Of GridViewRow) = From row As GridViewRow In dgvEmp.Rows Select row
            Dim lRows As IEnumerable(Of GridViewRow) = From row In dgvEmp.Rows Select CType(row, GridViewRow)

            Dim idx As Integer = -1

            For iEmp As Integer = 0 To dtmp.Length - 1

                Dim iLoop As Integer = iEmp
                Dim rows = lRows.Cast(Of GridViewRow)().Where(Function(row) row.Cells(1).Text.ToString().Equals(dtmp(iLoop).Item("employeecode")))

                For Each drrow As DataRow In mdtShifttran.Rows
                    If drrow.Item("AUD").ToString = "D" Then Continue For
                    iCurrShiftId = objShiftTran.GetEmployee_Current_ShiftId(CDate(drrow.Item("effectivedate")).Date, CInt(dtmp(iEmp).Item("employeeunkid")))

                    If iCurrShiftId > 0 Then
                        If iCurrShiftId = CInt(drrow.Item("shiftunkid")) Then
                            If rows.Count > 0 Then
                                idx = rows(0).RowIndex
                                If idx >= 0 Then
                                    dgvEmp.Rows(idx).ForeColor = Drawing.Color.Red
                                End If
                            End If
                            iSameShift = True
                            Continue For
                        End If
                    End If

                    If objShiftTran.isExist(CDate(drrow.Item("effectivedate")).Date, CInt(dtmp(iEmp).Item("employeeunkid")), , CInt(drrow.Item("shiftunkid"))).ToString.Trim.Length > 0 Then
                        If blnFlag = False Then
                            DisplayMessage.DisplayMessage("Sorry, shift cannot be assigned to some of the selected employee(s). Reason : shift is already assigned to them for the selected or future date and will be highlighted in red.", Me)

                            If rows.Count > 0 Then
                                idx = rows(0).RowIndex
                                If idx >= 0 Then
                                    dgvEmp.Rows(idx).ForeColor = Drawing.Color.Red
                                End If
                            End If

                            blnFlag = True : Continue For
                        Else

                            If rows.Count > 0 Then
                                idx = rows(0).RowIndex
                                If idx >= 0 Then
                                    dgvEmp.Rows(idx).ForeColor = Drawing.Color.Red
                                End If
                            End If

                            Continue For
                        End If
                    End If

                    Dim iOldSftid As Integer = objShiftTran.GetEmployee_Old_ShiftId(CDate(drrow.Item("effectivedate")).Date, CInt(dtmp(iEmp).Item("employeeunkid")))
                    If iOldSftid > 0 Then
                        'YOU CANNOT ASSIGN OTHER SHIFT FOR THE SAME DATE EVEN IF THE TIME IS NOT OVERLAPPING REASON SOME OTHER SHIFT IS ASSIGNED FOR PARTICULAR DATE.
                        DisplayMessage.DisplayMessage("Sorry, shift cannot be assigned to some of the selected employee(s). Reason : shift is already assigned to them for the selected date.", Me)
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) Then
                        If objleaveForm.ValidShiftForEmployee(CInt(dtmp(iEmp).Item("employeeunkid")), CDate(drrow.Item("effectivedate")).Date) Then

                            If rows.Count > 0 Then
                                idx = rows(0).RowIndex
                                If idx >= 0 Then
                                    dgvEmp.Rows(idx).ForeColor = Drawing.Color.Red
                                End If
                            End If

                            mblnLeaveAssigned = True
                            Continue For
                        End If
                    End If

                    'Pinkal (07-Oct-2015) -- Start
                    'Enhancement - WORKING ON HYATT URGENT CHANGES REGARDING SHIFT.
                    'Dim iTemp() = mdtShifttran.Select("shiftunkid = '" & CInt(drrow.Item("shiftunkid")) & "' AND AUD <> 'D'")
                    Dim iTemp() = mdtShifttran.Select("shiftunkid = '" & CInt(drrow.Item("shiftunkid")) & "' AND effdate = '" & drrow.Item("effdate").ToString() & "' AND AUD <> 'D'")
                    'Pinkal (07-Oct-2015) -- End
                    objShiftTran._EmployeeUnkid = CInt(dtmp(iEmp).Item("employeeunkid"))
                    objShiftTran._SDataTable = iTemp.CopyToDataTable().Copy
                    With objShiftTran
                        ._FormName = mstrModuleName
                        If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                            ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                        Else
                            ._AuditUserId = Convert.ToInt32(Session("UserId"))
                        End If
                        ._ClientIP = Session("IP_ADD").ToString()
                        ._HostName = Session("HOST_NAME").ToString()
                        ._FromWeb = True
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                    End With
                    mblnInsertFlag = objShiftTran.InsertUpdateDelete()
                    If mblnInsertFlag = False Then
                        DisplayMessage.DisplayMessage("Problem in assigning shift.", Me)
                        Exit For
                    End If

                Next

            Next

            If iSameShift Then
                DisplayMessage.DisplayMessage("Sorry, shift cannot be assigned to some of the selected employee(s). Reason : Same shift is assigned to them for the selected or previous date and will be highlighted in red.", Me)
                mdtShifttran.Rows.Clear()
                Me.ViewState("ShiftList") = mdtShifttran
                Fill_Shift_List()
                CType(dgvEmp.HeaderRow.FindControl("chkEmpSelectAll"), CheckBox).Checked = False
                chkEmpSelectAll_CheckedChanged(New CheckBox(), New EventArgs())
            End If

            If mblnLeaveAssigned Then
                DisplayMessage.DisplayMessage("Sorry, shift cannot be assigned to some of the selected employee(s). Reason : selected employee(s) was already applied leave for the selected or future date and will be highlighted in red.", Me)
                mdtShifttran.Rows.Clear()
                Me.ViewState("ShiftList") = mdtShifttran
                Fill_Shift_List()
                CType(dgvEmp.HeaderRow.FindControl("chkEmpSelectAll"), CheckBox).Checked = False
                chkEmpSelectAll_CheckedChanged(New CheckBox(), New EventArgs())

            ElseIf mblnInsertFlag = True Then
                DisplayMessage.DisplayMessage("shift assigned successfully.", Me)
                mdtShifttran.Rows.Clear()
                Me.ViewState("ShiftList") = mdtShifttran
                Fill_Shift_List()
                dtpEffectiveDate.Enabled = True
                cboShift.SelectedValue = "0"
                Fill_Emp_Grid()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSave_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~/TnA/Shift_Policy_Global_Assign/wPgViewAssignShiftPolicy.aspx")
            Response.Redirect(Session("rootpath").ToString() & "TnA/Shift_Policy_Global_Assign/wPgViewAssignShiftPolicy.aspx")
            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "DatePicker Event"

    Protected Sub dtpEffectiveDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpEffectiveDate.TextChanged
        Try
            Fill_Emp_Grid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dtpEffectiveDate_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("dtpEffectiveDate_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Textbox Event"

    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim dvTable As DataView = Nothing
        Try
            dvTable = CType(Me.ViewState("EmployeeList"), DataTable).DefaultView
            If dvTable IsNot Nothing Then
                If dvTable.Table.Rows.Count > 0 Then
                    dvTable.RowFilter = "employeecode like '%" & txtSearch.Text.Trim & "%'  OR name like '%" & txtSearch.Text.Trim & "%' "
                    dgvEmp.DataSource = dvTable
                    dgvEmp.DataBind()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtSearch_TextChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("txtSearch_TextChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            Dim mdtemployee As DataTable = dvTable.ToTable
            If mdtemployee.Rows.Count <= 0 Then
                Dim drRow As DataRow = mdtemployee.NewRow
                drRow("employeecode") = "None"
                drRow("name") = "None"
                drRow("employeeunkid") = -1
                mdtemployee.Rows.Add(drRow)
            End If
            dvTable = mdtemployee.DefaultView
            dgvEmp.DataSource = dvTable
            dgvEmp.DataBind()
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Public Sub chkEmpSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try

            Dim dtTab As DataTable = CType(Me.ViewState("EmployeeList"), DataTable)

            For i As Integer = 0 To dgvEmp.Rows.Count - 1
                Dim gvRow As GridViewRow = dgvEmp.Rows(i)
                CType(gvRow.FindControl("chkEmpSelect"), CheckBox).Checked = cb.Checked
                Dim drRow() As DataRow = dtTab.Select("employeecode = '" & dgvEmp.Rows(i).Cells(1).Text.Trim & "'")
                If drRow.Length > 0 Then
                    drRow(0)("ischeck") = cb.Checked
                End If
            Next
            dtTab.AcceptChanges()

            Me.ViewState("EmployeeList") = dtTab

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkEmpSelectAll_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkEmpSelectAll_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub chkEmpSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dvTable As DataView = Nothing
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try


            Dim dtTab As DataTable = CType(Me.ViewState("EmployeeList"), DataTable)

            Dim gvRow As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            CType(gvRow.FindControl("chkEmpSelect"), CheckBox).Checked = cb.Checked

            Dim drRow() As DataRow = dtTab.Select("employeecode = '" & gvRow.Cells(1).Text.Trim & "'")

            If drRow.Length > 0 Then
                drRow(0)("IsCheck") = cb.Checked
                drRow(0).AcceptChanges()
            End If

            Me.ViewState("EmployeeList") = dtTab

            dvTable = CType(Me.ViewState("EmployeeList"), DataTable).DefaultView

            If txtSearch.Text.Trim.Length > 0 Then
                If dvTable IsNot Nothing Then
                    If dvTable.Table.Rows.Count > 0 Then
                        dvTable.RowFilter = "employeecode like '%" & txtSearch.Text.Trim & "%'  OR name like '%" & txtSearch.Text.Trim & "%' "
                    End If
                End If
            End If

            Dim drcheckedRow() As DataRow = dvTable.ToTable.Select("Ischeck = True")

            If drcheckedRow.Length = dvTable.ToTable.Rows.Count Then
                CType(dgvEmp.HeaderRow.FindControl("chkEmpSelectAll"), CheckBox).Checked = True
            Else
                CType(dgvEmp.HeaderRow.FindControl("chkEmpSelectAll"), CheckBox).Checked = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkEmpSelect_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkEmpSelect_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub chkShiftSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try

            Dim dtTab As DataTable = CType(Me.ViewState("ShiftList"), DataTable)

            For i As Integer = 0 To dtTab.Rows.Count - 1
                Dim gvRow As GridViewRow = dgvAssignedShift.Rows(i)
                CType(gvRow.FindControl("chkShiftSelect"), CheckBox).Checked = cb.Checked
                dtTab.Rows(i)("IsCheck") = cb.Checked
            Next
            dtTab.AcceptChanges()

            Me.ViewState("ShiftList") = dtTab

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkShiftSelectAll_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkShiftSelectAll_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub chkShiftSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try


            Dim dtTab As DataTable = CType(Me.ViewState("ShiftList"), DataTable)

            Dim gvRow As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            CType(gvRow.FindControl("chkShiftSelect"), CheckBox).Checked = cb.Checked
            dtTab.Rows(gvRow.RowIndex)("IsCheck") = cb.Checked
            dtTab.AcceptChanges()

            Me.ViewState("ShiftList") = dtTab

            Dim drcheckedRow() As DataRow = dtTab.Select("Ischeck = True AND shiftunkid >0 ")
            Dim drRow() As DataRow = dtTab.Select("effectivedate <> ''")

            If drcheckedRow.Length = drRow.Length Then
                CType(dgvAssignedShift.HeaderRow.FindControl("chkShiftSelectAll"), CheckBox).Checked = True
            Else
                CType(dgvAssignedShift.HeaderRow.FindControl("chkShiftSelectAll"), CheckBox).Checked = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkShiftSelect_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkShiftSelect_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Gridview Event"

    Protected Sub dgvEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvEmp.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            If e.Row.Cells(1).Text = "None" AndAlso e.Row.Cells(2).Text = "None" Then
                e.Row.Visible = False
                CType(dgvEmp.HeaderRow.FindControl("chkEmpSelectAll"), CheckBox).Enabled = False
            Else
                CType(dgvEmp.HeaderRow.FindControl("chkEmpSelectAll"), CheckBox).Enabled = True
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvEmp_RowDataBound :- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgvEmp_RowDataBound :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvAssignedShift_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvAssignedShift.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            If e.Row.Cells(1).Text = "&nbsp;" AndAlso e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "None" Then
                e.Row.Visible = False
                CType(dgvAssignedShift.HeaderRow.FindControl("chkShiftSelectAll"), CheckBox).Enabled = False
            Else
                CType(dgvAssignedShift.HeaderRow.FindControl("chkShiftSelectAll"), CheckBox).Enabled = True
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvAssignedShift_RowDataBound :- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgvAssignedShift_RowDataBound :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Pinkal (06-Nov-2017) -- Start
    'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.

#Region "ComboBox Events"

    Private Sub cboFilterShiftType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFilterShiftType.SelectedIndexChanged
        Try
            Dim objShift As New clsNewshift_master
            Dim dsList As DataSet = objShift.getListForCombo("List", True, CInt(cboFilterShiftType.SelectedValue))
            With cboFilterShift
                .DataValueField = "shiftunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedIndex = 0
            End With
            cboFilterShift_SelectedIndexChanged(New Object(), New EventArgs())
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboFilterShiftType_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError("cboFilterShiftType_SelectedIndexChanged : " + ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub cboFilterShift_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFilterShift.SelectedIndexChanged
        Try
            Fill_Emp_Grid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboFilterShift_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError("cboFilterShift_SelectedIndexChanged :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Pinkal (06-Nov-2017) -- End


    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)

        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End
        Me.LblEffectiveDate.Text = Language._Object.getCaption(Me.LblEffectiveDate.ID, Me.LblEffectiveDate.Text)
        Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.ID, Me.LblShift.Text)
        Me.dgvEmp.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvEmp.Columns(1).FooterText, Me.dgvEmp.Columns(1).HeaderText)
        Me.dgvEmp.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvEmp.Columns(2).FooterText, Me.dgvEmp.Columns(2).HeaderText)

        Me.dgvAssignedShift.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvAssignedShift.Columns(1).FooterText, Me.dgvAssignedShift.Columns(1).HeaderText)
        Me.dgvAssignedShift.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvAssignedShift.Columns(2).FooterText, Me.dgvAssignedShift.Columns(2).HeaderText)
        Me.dgvAssignedShift.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvAssignedShift.Columns(3).FooterText, Me.dgvAssignedShift.Columns(3).HeaderText)

        Me.btnDeleteShift.Text = Language._Object.getCaption(Me.btnDeleteShift.ID, Me.btnDeleteShift.Text).Replace("&", "")
        Me.btnAssignShift.Text = Language._Object.getCaption(Me.btnAssignShift.ID, Me.btnAssignShift.Text).Replace("&", "")
        Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

End Class
