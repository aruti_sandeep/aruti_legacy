﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region

Partial Class Leave_Viewer
    Inherits Basepage

#Region " Private Variables "

    Dim ds As DataSet
    Dim ClsEmplist As New Aruti.Data.clsEmployee_Master
    Dim msql As String
    Dim Employeeunkid As Integer
    Dim objDataOperation As clsDataOperation
    Dim clsuser As New User
    Dim DisplayMessage As New CommonCodes
    Dim StrIds As String = ""
    Dim strYear As String = ""



    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmLeaveIssue_AddEdit"
    'Pinkal (06-May-2014) -- End

#End Region

#Region " Page Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            objDataOperation = New clsDataOperation()


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            If IsPostBack = False Then
                BindEmployeeList()
                BindLeaveType()
                dttodate.SetDate = Nothing
                dtfromdate.SetDate = Nothing
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.UserID
                    Me.ViewState("Empunkid") = clsuser.UserID
                    'Sohail (30 Mar 2015) -- End
                Else
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                    'Me.ViewState("Employeeunkid") = clsuser.Employeeunkid
                    Me.ViewState("Empunkid") = Session("Employeeunkid")
                    Me.ViewState("Employeeunkid") = Session("Employeeunkid")
                    'Sohail (30 Mar 2015) -- End
                End If
            Else
                Employeeunkid = CInt(Me.ViewState("Employeeunkid"))
            End If
            dtfromdate.AutoPostBack = False
            dttodate.AutoPostBack = False
        Catch ex As Exception
            DisplayMessage.DisplayError("Page Load : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("Employeeunkid", Employeeunkid)
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreInit : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Private Funtions & Methods "

    Public Sub BindEmployeeList()
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            'If (Session("LoginBy") = Global.User.en_loginby.User) Then

            '    'Pinkal (25-APR-2012) -- Start
            '    'Enhancement : TRA Changes
            '    'ds = ClsEmplist.GetEmployeeList("Employee", False, True)


            '    'Pinkal (5-MAY-2012) -- Start
            '    'Enhancement : TRA Changes

            '    If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '        ds = ClsEmplist.GetEmployeeList("Employee", False, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            '    Else
            '        ds = ClsEmplist.GetEmployeeList("Employee", False, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            '    End If
            '    'Pinkal (5-MAY-2012) -- End



            '    'Pinkal (25-APR-2012) -- End
            '    dgViewEmployeelist.DataSource = ds
            '    dgViewEmployeelist.DataKeyField = "employeeunkid"
            '    dgViewEmployeelist.DataBind()
            '    Me.ViewState.Add("Source", ds)
            'Else

            '    'Pinkal (25-APR-2012) -- Start
            '    'Enhancement : TRA Changes
            '    'ds = ClsEmplist.GetEmployeeList("Employee", False, True, Session("Employeeunkid"))


            '    'Pinkal (5-MAY-2012) -- Start
            '    'Enhancement : TRA Changes

            '    If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '        ds = ClsEmplist.GetEmployeeList("Employee", False, , Session("Employeeunkid"), , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            '    Else
            '        ds = ClsEmplist.GetEmployeeList("Employee", False, , Session("Employeeunkid"), , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            '    End If

            '    'Pinkal (5-MAY-2012) -- End

            '    'Pinkal (25-APR-2012) -- End

            '    dgViewEmployeelist.DataSource = ds
            '    dgViewEmployeelist.DataKeyField = "employeeunkid"
            '    dgViewEmployeelist.DataBind()
            '    Me.ViewState.Add("Source", ds)
            'End If


            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            Dim blnApplyFilter As Boolean = True
            Dim intEmpId As Integer = 0
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If
            ds = ClsEmplist.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                            False, "Employee", False, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            'Pinkal (22-Mar-2016) -- End

            With dgViewEmployeelist
                .DataSource = ds
                .DataKeyField = "employeeunkid"
                .DataBind()
            End With
                Me.ViewState.Add("Source", ds)

            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("BindEmployeeList : " & ex.Message, Me)
        End Try
    End Sub

    Public Sub BindLeaveType()
        Try
            Dim objLeaveType As New clsleavetype_master

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                ds = objLeaveType.GetList("LeaveType", True, True, "ISNULL(isshowoness,1) = 1")
            Else
                ds = objLeaveType.GetList("LeaveType", True, True, "")
            End If
            'Pinkal (25-May-2019) -- End


            dgViewLeaveType.DataSource = ds
            dgViewLeaveType.DataKeyField = "leavetypeunkid"
            dgViewLeaveType.DataBind()
            objLeaveType = Nothing
            For Each mItm As DataGridItem In dgViewLeaveType.Items
                If mItm.Cells(2).Text <> "0" Then
                    mItm.Cells(1).ForeColor = Color.White
                    mItm.Cells(1).BackColor = Color.FromArgb(CInt(mItm.Cells(2).Text.Trim))
                    mItm.Cells(1).CssClass = "LeaveCss" 'SHANI [01 FEB 2015]-START 'Enhancement - REDESIGN SELF SERVICE.
                End If
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError("BindLeaveType : " & ex.Message, Me)
        End Try
    End Sub

    Public Function EntryValidation() As Boolean
        Try
            Dim mValid As Boolean
            mValid = True
            If dtfromdate.GetDate > dttodate.GetDate Then
                DisplayMessage.DisplayMessage("End date cannot be less or equal to start date", Me)
                dttodate.Focus()
                mValid = False
                Exit Function
            End If
            If Employeeunkid = 0 Then
                DisplayMessage.DisplayMessage("Select  Employee id", Me)
                dgViewEmployeelist.Focus()
                mValid = False
                Exit Function
            End If
            If dtfromdate.GetDate > dttodate.GetDate Then
                DisplayMessage.DisplayMessage("End date Can not Be greater than Start date", Me)
                dtfromdate.Focus()
                mValid = False
                Exit Function
            End If
            Return mValid
        Catch ex As Exception
            DisplayMessage.DisplayError("EntryValidation : " & ex.Message, Me)
        End Try
    End Function

    Public Sub BindGrid(Optional ByVal blnInValidDate As Boolean = False)
        Try


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            If StrIds.Length > 0 Or strYear.Length > 0 Then

            msql = "select CalendarTableID,MonthYear,startday,MonthDays,MonthId,YearId,sun_1 ,Mon_1,Tue_1,Wed_1 ,Thu_1 ,Fri_1 ,Sat_1 " & vbCrLf
            msql &= ",sun_2 ,Mon_2 ,Tue_2 ,Wed_2 ,Thu_2 ,Fri_2 ,Sat_2 " & vbCrLf
            msql &= ",sun_3 ,Mon_3 ,Tue_3 ,Wed_3 ,Thu_3 ,Fri_3 ,Sat_3 " & vbCrLf
            msql &= ",sun_4 ,Mon_4 ,Tue_4 ,Wed_4 ,Thu_4 ,Fri_4 ,Sat_4 " & vbCrLf
            msql &= ",sun_5 ,Mon_5 ,Tue_5 ,Wed_5 ,Thu_5 ,Fri_5 ,Sat_5" & vbCrLf
            msql &= ",sun_6 ,Mon_6 ,Tue_6 ,Wed_6 ,Thu_6 ,Fri_6 ,Sat_6 " & vbCrLf
                msql &= " from  CalMonth_" & Employeeunkid & "  WHERE 1 = 1 "

            If StrIds.Length > 0 Then
                    msql &= " AND MonthId IN (" & StrIds & ")"
                    'Else
                    '    msql &= " WHERE YearId <> (" & CDate(dtfromdate.GetDate).ToString("yyyy") & ")"
                End If


                If strYear.Length > 0 Then
                    msql &= " AND YearId IN (" & strYear & ")"
            End If

            ds = objDataOperation.WExecQuery(msql, "calendartable")
            If blnInValidDate = True Then
                ds.Tables(0).Rows.Clear()
            End If
            dgViewLeave.DataSource = ds.Tables(0)
            dgViewLeave.DataBind()

            Else
                dgViewLeave.DataSource = ds.Tables(0).Clone
                dgViewLeave.DataBind()
            End If

            'Pinkal (18-Dec-2012) -- End

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." And dgViewLeave.CurrentPageIndex > 0 Then
                dgViewLeave.CurrentPageIndex = 0
                dgViewLeave.DataBind()
            Else
                Throw ex
                DisplayMessage.DisplayError("BindGrid : " & ex.Message, Me)
            End If
        End Try
    End Sub

#End Region

#Region " DataGrid Events "

    Protected Sub dgViewEmployeelist_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgViewEmployeelist.ItemCommand
        Try
            If e.CommandName = "Select" Then
                Employeeunkid = CInt(dgViewEmployeelist.DataKeys(e.Item.ItemIndex))
                btnSearch_Click(New Object, New System.EventArgs())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dgViewEmployeelist_ItemCommand : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub dgViewEmployeelist_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgViewEmployeelist.PageIndexChanged
        Try
            dgViewEmployeelist.CurrentPageIndex = e.NewPageIndex
            If dgViewEmployeelist.SelectedIndex >= 0 Then dgViewEmployeelist.SelectedIndex = -1


            'Pinkal (09-Jan-2013) -- Start
            'Enhancement : TRA Changes

            If TxtSearch.Text.Trim.Length > 0 Then
                TxtSearch_TextChanged(source, e)
                dgViewEmployeelist.CurrentPageIndex = e.NewPageIndex
                dgViewEmployeelist.DataBind()
            Else
            BindEmployeeList()
            End If

            'Pinkal (09-Jan-2013) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("dgViewEmployeelist_PageIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub dgViewLeaveType_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgViewLeaveType.ItemDataBound
        Try
            e.Item.Cells(0).Height = 10
            e.Item.Cells(1).Height = 10
            e.Item.Cells(2).Height = 10
            e.Item.Cells(0).Width = 80
        Catch ex As Exception
            DisplayMessage.DisplayError("dgViewLeaveType_ItemDataBound : " & ex.Message, Me)
        End Try
    End Sub


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Protected Sub dgViewLeaveType_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgViewLeaveType.PageIndexChanged
        Try
            dgViewLeaveType.CurrentPageIndex = e.NewPageIndex
            BindLeaveType()
        Catch ex As Exception
            DisplayMessage.DisplayError("dgViewLeaveType_PageIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

    'Pinkal (06-May-2014) -- End


    Protected Sub dgViewLeave_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgViewLeave.PageIndexChanged
        Try
            dgViewLeave.CurrentPageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError("dgViewLeave_PageIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub dgViewLeave_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgViewLeave.ItemDataBound
        Try
            Dim arr() As String
            For Each cell As TableCell In e.Item.Cells
                cell.HorizontalAlign = HorizontalAlign.Center
                e.Item.Cells(1).HorizontalAlign = HorizontalAlign.Left
                e.Item.Cells(1).Wrap = False


                'Pinkal (09-Jan-2013) -- Start
                'Enhancement : TRA Changes
                Dim mstrYear As String = e.Item.Cells(0).Text
                'Pinkal (09-Jan-2013) -- End


                arr = Split(cell.Text, "~")
                If IsNumeric(arr(0)) AndAlso CInt(arr(0)) < 32 Then

                    If eZeeDate.convertDate(CInt(e.Item.Cells(5).Text) & CInt(e.Item.Cells(4).Text).ToString("#00") & CInt(arr(0)).ToString("#00")) >= eZeeDate.convertDate(Me.ViewState("statdate").ToString) _
                        And eZeeDate.convertDate(CInt(e.Item.Cells(5).Text) & CInt(e.Item.Cells(4).Text).ToString("#00") & CInt(arr(0)).ToString("#00")) <= eZeeDate.convertDate(Me.ViewState("enddate").ToString) Then

                        If Val(arr(0)) <> 0 Then
                            If arr.Count > 1 Then
                                Dim str As String() = arr(1).Split(CChar("/"))
                                If str.Length > 1 Then
                                    cell.Text = arr(0).ToString() & " / " & str(1).ToString()
                                    cell.BackColor = (Color.FromArgb(CInt(str(0))))
                                cell.ForeColor = Color.White
                                Else
                                    cell.Text = arr(0).ToString()

                                    'Pinkal (01-Feb-2014) -- Start
                                    'Enhancement : TRA Changes
                                    cell.BackColor = (Color.FromArgb(CInt(arr(1))))
                                    cell.ForeColor = Color.White
                                    'Pinkal (01-Feb-2014) -- End

                                End If
                            End If
                        End If
                    Else
                        cell.Text = arr(0).ToString()
                    End If
                End If
            Next

            e.Item.Cells(0).Visible = False
            e.Item.Cells(2).Visible = False
            e.Item.Cells(3).Visible = False
            e.Item.Cells(4).Visible = False
            e.Item.Cells(5).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError("dgViewLeave_ItemDataBound : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If EntryValidation() = False Then
                Exit Sub
            End If

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                If CBool(Session("ViewLeaveViewer")) = False Then
                    Exit Sub
                End If
            End If

            Dim objIssueMaster As New Aruti.Data.clsleaveissue_master
            Dim dsList As New DataSet
            dsList = objIssueMaster.GetEmployeeIssueData(Employeeunkid)
            Dim statdate, enddate As String
            statdate = Nothing : enddate = Nothing
            Dim blnflag As Boolean = False

            'If dtfromdate.GetDate = CDate("1/1/2000") And dttodate.GetDate = CDate("1/1/2000") Then
            If dtfromdate.IsNull = True AndAlso dttodate.IsNull = True Then
                If dsList.Tables(0).Rows.Count > 0 Then
                    statdate = dsList.Tables(0).Compute("MIN(leavedate)", "").ToString()
                    enddate = dsList.Tables(0).Compute("MAX(leavedate)", "").ToString()
                Else
                    statdate = Format(CDate("01/01/1900"), "yyyyMMdd")
                    enddate = Format(CDate("01/01/1900"), "yyyyMMdd")
                    blnflag = True
                End If
            Else
                statdate = Format(CDate(dtfromdate.GetDate), "yyyyMMdd")
                enddate = Format(CDate(dttodate.GetDate), "yyyyMMdd")
            End If


            'Pinkal (09-Jan-2013) -- Start
            'Enhancement : TRA Changes

            'Me.ViewState.Add("statdate", eZeeDate.convertDate(statdate.ToString).ToString("MMdd"))
            'Me.ViewState.Add("enddate", eZeeDate.convertDate(enddate.ToString).ToString("MMdd"))

            Me.ViewState.Add("statdate", eZeeDate.convertDate(statdate.ToString).ToString("yyyyMMdd"))
            Me.ViewState.Add("enddate", eZeeDate.convertDate(enddate.ToString).ToString("yyyyMMdd"))

            'Pinkal (09-Jan-2013) -- End

            Dim TblName As String = ""
            TblName = "CalMonth_" & Employeeunkid & ""
            Dim ds2 As New DataSet
            msql = "exec CreateMonthCalendar  '" & TblName & "' , '" & statdate & "','" & enddate & "'"
            objDataOperation.ExecNonQuery(msql)

            Dim flag As Boolean = False
            Dim totday As Integer
            msql = "select * from " & TblName & ""
            ds = objDataOperation.WExecQuery(msql, "Tblname")
            Dim objEmpHoliday As New Aruti.Data.clsemployee_holiday
            Dim objLeaveFraction As New clsleaveday_fraction


            For i = 0 To ds.Tables(0).Rows.Count - 1 'month loop
                Dim mth As String = ds.Tables(0).Rows(i)("Monthyear").ToString
                Dim startday As String = ds.Tables(0).Rows(i)("startday").ToString()
                totday = CInt(ds.Tables(0).Rows(i)("MonthDays"))
                Dim d As Integer = ds.Tables(0).Columns(startday).Ordinal

                For j As Integer = 1 To totday
                    Dim mCol As String = ds.Tables(0).Columns(d).ToString
                    Dim mdtdate As New Date(CInt(ds.Tables(0).Rows(i)("YearId")), CInt(ds.Tables(0).Rows(i)("MonthId")), j)
                    Dim dtHoliday As DataTable = objEmpHoliday.GetEmployeeHoliday(Employeeunkid, mdtdate)
                    flag = False
                    If dsList.Tables(0).Rows.Count > 0 Then
                        For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                            Dim dtDate As Date = eZeeDate.convertDate(dsList.Tables(0).Rows(k).Item("leavedate").ToString).Date

                            If dtHoliday IsNot Nothing And dtHoliday.Rows.Count > 0 Then
                                Dim mVal As String = j & "~" & dtHoliday.Rows(0)("color").ToString
                                msql = "update " & TblName & " set " & mCol & " = '" & mVal & "' where MonthYear = '" & ds.Tables(0).Rows(i)("monthyear").ToString & "' "
                                objDataOperation.WExecQuery(msql, "TblName")
                            End If


                            If dtDate.Month = CInt(ds.Tables(0).Rows(i)("MonthId")) And dtDate.Year = CInt(ds.Tables(0).Rows(i)("YearId")) And dtDate.Day = j Then
                                Dim mVal As String = j & "~" & dsList.Tables(0).Rows(k)("color").ToString
                                Dim strDate As String = ds.Tables(0).Rows(i)("YearId").ToString() & CInt(ds.Tables(0).Rows(i)("MonthId")).ToString("#00") & CInt(j).ToString("#00")

                                'Pinkal (27-May-2014) -- Start
                                'Enhancement : OMAN Changes [Adding Leave Day Fraction In Login Summary FOR Getting Proper Leave Day Fraction]
                                'Dim mdclFraction As Decimal = objLeaveFraction.GetEmployeeLeaveDay_Fraction(strDate, CInt(dsList.Tables(0).Rows(k)("formunkid")), Employeeunkid)
                                Dim objLeaveissue As New clsleaveissue_Tran
                                Dim mdclFraction As Decimal = objLeaveissue.GetIssuedDayFractionForViewer(Employeeunkid, strDate)
                                'Pinkal (27-May-2014) -- End


                                msql = "update " & TblName & " set " & mCol & " = '" & mVal.ToString() & " / " & mdclFraction.ToString & " ' where MonthYear = '" & ds.Tables(0).Rows(i)("monthyear").ToString & "' "
                                objDataOperation.WExecQuery(msql, "TblName")
                                StrIds &= "," & ds.Tables(0).Rows(i)("MonthId").ToString()
                                strYear &= "," & ds.Tables(0).Rows(i)("YearId").ToString()
                                Exit For
                            Else
                                If flag = False Then
                                    msql = "update " & TblName & " set " & mCol & " = " & j & " where MonthYear = '" & ds.Tables(0).Rows(i)("monthyear").ToString & "' "
                                    objDataOperation.WExecQuery(msql, "TblName")
                                    flag = True
                                End If
                            End If
                        Next
                    Else
                        msql = "update " & TblName & " set " & mCol & " = " & j & " where MonthYear = '" & ds.Tables(0).Rows(i)("monthyear").ToString & "' "
                        objDataOperation.WExecQuery(msql, "TblName")
                    End If
                    d = d + 1
                Next
            Next
            If StrIds.Length > 0 Then
                StrIds = Mid(StrIds, 2)
            End If

            If strYear.Length > 0 Then
                strYear = Mid(strYear, 2)
            End If


            BindGrid(blnflag)
            msql = "drop table " & TblName & ""
            ds = objDataOperation.WExecQuery(msql, "Tblname")
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSearch_Click : " & ex.Message, Me)
        End Try
    End Sub

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region


    'Pinkal (09-Jan-2013) -- Start
    'Enhancement : TRA Changes

#Region "TextBox Event"

    Protected Sub TxtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtSearch.TextChanged
        Try
            Dim dvEmployee As DataView = CType(Me.ViewState("Source"), DataSet).Tables(0).DefaultView
            If dvEmployee IsNot Nothing Then
                If dvEmployee.Table.Rows.Count > 0 Then
                    dgViewEmployeelist.CurrentPageIndex = 0
                    dvEmployee.RowFilter = "employeecode like '%" & TxtSearch.Text.Trim & "%'  OR employeename like '%" & TxtSearch.Text.Trim & "%' "
                    dgViewEmployeelist.DataSource = dvEmployee
                    dgViewEmployeelist.DataBind()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("TxtSearch_TextChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("TxtSearch_TextChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Pinkal (09-Jan-2013) -- End


    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption("gbLeaveViewer", Me.Title)
        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption("gbLeaveViewer", Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption("gbLeaveViewer", Me.lblPageHeader.Text)
        'SHANI [01 FEB 2015]--END
        Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.ID, Me.lblFromDate.Text)
        Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.ID, Me.lblToDate.Text)

        Me.dgViewEmployeelist.Columns(2).HeaderText = Language._Object.getCaption(Me.dgViewEmployeelist.Columns(2).FooterText, Me.dgViewEmployeelist.Columns(2).HeaderText)
        Me.dgViewLeaveType.Columns(1).HeaderText = Language._Object.getCaption(Me.dgViewLeaveType.Columns(1).FooterText, Me.dgViewLeaveType.Columns(1).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    
End Class

'Dim clsLeaveType As New Aruti.Data.clsleavetype_master
'Dim cls As New Aruti.Data.clsMasterData
'Dim cls1 As New Aruti.Data.clsleaveissue_master
'Dim lv As New Aruti.Data.clsleaveform
'Dim ds1 As New DataSet
'Protected Sub dgViewEmployeelist_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgViewEmployeelist.ItemDataBound
'    Try
'        e.Item.Cells(3).Visible = False
'    Catch ex As Exception
'        DisplayMessage.DisplayError("Error in dgViewEmployeelist_ItemDataBound Event", Me)
'    End Try

'End Sub