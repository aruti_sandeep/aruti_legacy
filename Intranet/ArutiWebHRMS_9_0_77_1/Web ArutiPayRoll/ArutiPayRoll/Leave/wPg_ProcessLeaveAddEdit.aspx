﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_ProcessLeaveAddEdit.aspx.vb"
    Inherits="Leave_wPg_ProcessLeaveAddEdit" Title="Leave Approval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/PreviewAttachment.ascx" TagName="PreviewAttachment"
    TagPrefix="PrviewAttchmet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
			var scroll1 = {
					Y: '#<%= hfScrollPosition1.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
	             if (args.get_error() == undefined) {
                        $("#scrollable-container").scrollTop($(scroll.Y).val());

            }
        }
    </script>

    <script>
        function IsValidAttach() {
            var cboEmployee = $('#<%= cboApprovalExpEmployee.ClientID %>');

            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }

            if (parseInt(cboApprovalExpEmployee.val()) <= 0) {
                alert('Employee Name is compulsory information. Please select Employeee to continue.');
                cboApprovalExpEmployee.focus();
                return false;
            }
        }    
                      
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_YesNo.ClientID %>_Panel1").css("z-index", "100005");
                }
        
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave Approval"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 40%; vertical-align: top">
                                                <div id="Div4" class="panel-default">
                                                    <div id="Div5" class="panel-body-default">
                                                        <table style="width: 100%; height: 330px">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%; vertical-align: top;">
                                                                    <table style="width: 100%">
                                                                        <div id="Div1" class="panel-heading-default" style="margin-bottom: 5px">
                                                                            <div style="float: left;">
                                                                                <asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label>
                                                                            </div>
                                                                            <div style="float: right;">
                                                                                <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                        <tr style="width: 100%; display: none">
                                                                            <td style="width: 70%">
                                                                                <asp:Label ID="lblLastYearAccrued" runat="server" Text="Total Accrued Upto Last Year:"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 30%; text-align: right">
                                                                                <asp:Label ID="objlblLastYearAccruedValue" runat="server" Text="0"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 70%">
                                                                                    <asp:Label ID="LblLeaveBF" runat="server" Text="Leave BF"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 30%; text-align: right">
                                                                                    <asp:Label ID="objlblLeaveBFvalue" runat="server" Text="0.00"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 70%">
                                                                                <asp:Label ID="lblToDateAccrued" runat="server" Text="Total Accrued To Date:"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 30%; text-align: right">
                                                                                <asp:Label ID="objlblAccruedToDateValue" runat="server" Text="0"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%; display: none">
                                                                            <td style="width: 70%">
                                                                                <asp:Label ID="lblLastYearIssued" runat="server" Text="Total Issued Upto Last Year:"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 30%; text-align: right">
                                                                                <asp:Label ID="objlblLastYearIssuedValue" runat="server" Text="0"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 70%">
                                                                                <asp:Label ID="lblTotalIssuedToDate" runat="server" Text="Total Issued Upto Date:"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 30%; text-align: right">
                                                                                <asp:Label ID="objlblIssuedToDateValue" runat="server" Text="0"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 70%">
                                                                                    <asp:Label ID="LblTotalAdjustment" runat="server" Text="Total Adjustment"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 30%; text-align: right">
                                                                                   <asp:Label ID="objlblTotalAdjustment" runat="server" Text="0.00"></asp:Label>
                                                                                </td>
                                                                        </tr>
                                                                           <%--'Pinkal (15-Apr-2019) -- 'Enhancement - Working on Leave & CR Changes for NMB.--%>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 70%">
                                                                                <asp:Label ID="LblBalanceAsonDate" runat="server" Text="Balance As on Date"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 30%; text-align: right">
                                                                                <asp:Label ID="objlblBalanceAsonDateValue" runat="server" align="left" Text="0"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                                                                                                
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 70%">
                                                                                <asp:Label ID="lblBalance" runat="server" Text="Total Balance"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 30%; text-align: right">
                                                                                <asp:Label ID="objlblBalanceValue" runat="server" align="left" Text="0"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 100%" colspan="2">
                                                                                <div style="border-bottom: 1px solid #DDD; margin-top: 10px; margin-bottom: 10px" />
                                                                          </td>
                                                                        </tr>
                                                                        
                                                                          <%--'Pinkal (15-Apr-2019) --.--%>
                                                                        
                                                                       <tr style="width: 100%;">
                                                                            <td style="width: 70%">
                                                                                <asp:Label ID="LblAsonDateAccrue" runat="server" Text="Leave Accrue As on End Date"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 30%; text-align: right">
                                                                                <asp:Label ID="objAsonDateAccrue" runat="server" align="left" Text="0"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 70%">
                                                                                <asp:Label ID="lblAsonDateBalance" runat="server" Text="Leave Balance As on End Date"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 30%; text-align: right">
                                                                                <asp:Label ID="objAsonDateBalance" runat="server" align="left" Text="0"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%;">
                                                                <td style="width: 100%; vertical-align: bottom;">
                                                                    <table style="width: 100%;">
                                                                        <tr style="width: 100%;">
                                                                            <td style="width: 100%;" colspan="2">
                                                                                <asp:Label ID="lblRemark" runat="server" Text="Remarks:"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%;">
                                                                            <td colspan="2" style="width: 100%;">
                                                                                <asp:TextBox ID="txtRemark" runat="server" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%;">
                                                                            <td style="width: 50%;">
                                                                                <asp:LinkButton ID="lnkShowLeaveForm" runat="server" Text="Leave Form" Font-Underline="false" />
                                                                            </td>
                                                                            <td style="width: 50%;">
                                                                                <asp:LinkButton ID="lnkShowLeaveExpense" runat="server" Text="Leave Expense" Font-Underline="false" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%;">
                                                                            <td style="width: 50%;">
                                                                                <asp:LinkButton ID="lnkLeaveDayCount" runat="server" Text="Leave Day Fraction" Font-Underline="false" />
                                                                            </td>
                                                                            <td style="width: 50%">
                                                                                <asp:LinkButton ID="lnkShowScanDocuementList" runat="server" Text="Show Scan Documents"
                                                                                    Font-Underline="false"></asp:LinkButton>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                    <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                    <PrviewAttchmet:PreviewAttachment ID="popup_ShowAttchment" runat="server" />
                                                    <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                </div>
                                            </td>
                                            <td style="width: 60%; vertical-align: top">
                                                <div id="Div8" class="panel-default">
                                                    <div id="Div9" class="panel-body-default">
                                                        <table style="width: 70%;">
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%;">
                                                                    <asp:Label ID="lblFormNo" runat="server" Text="Application No:"></asp:Label>
                                                                </td>
                                                                <td style="width: 60%;">
                                                                    <asp:TextBox ID="txtFormNo" runat="server" AutoPostBack="True" ReadOnly="True" Enabled="true"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                             <%-- 'Pinkal (25-May-2019) -- Start
                                                              'Enhancement - NMB FINAL LEAVE UAT CHANGES.--%>
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%;">
                                                                    <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type"></asp:Label>
                                                                </td>
                                                                <td style="width: 30%;">
                                                                    <asp:TextBox ID="txtLeaveType" runat="server" Enabled="true" ReadOnly="true" Visible="true"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <%--'Pinkal (25-May-2019) -- End--%>
                                                            
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%;">
                                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee:"></asp:Label>
                                                                </td>
                                                                <td style="width: 30%;">
                                                                    <asp:TextBox ID="txtEmployee" runat="server" Enabled="true" ReadOnly="true" Visible="true"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%;">
                                                                    <asp:Label ID="lblDepartment" runat="server" Text="Department:"></asp:Label>
                                                                </td>
                                                                <td style="width: 60%;">
                                                                    <asp:TextBox ID="txtDepartment" runat="server" Enabled="True" ReadOnly="True"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%;">
                                                                    <asp:Label ID="lblReportTo" runat="server" Text="Report To:"></asp:Label>
                                                                </td>
                                                                <td style="width: 60%;">
                                                                    <asp:TextBox ID="txtReportTo" runat="server" ReadOnly="True" Enabled="true"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            
                                                            <%--'Pinkal (03-May-2019) -- Start
                                                            'Enhancement - Working on Leave UAT Changes for NMB.--%>
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%;"/>
                                                                 <td style="width: 60%">
                                                                    <asp:CheckBox ID = "chkIncludeAllStaff" runat = "server" Text= "Include All Staff" AutoPostBack ="true" />
                                                                </td>
                                                            </tr>
                                                            
                                                           <%-- 'Pinkal (03-May-2019) -- End--%>
                                                            
                                                            
                                                             <%--'Pinkal (01-Oct-2018) -- Start Enhancement - Leave Enhancement for NMB.--%>
                                                              <tr style="width: 100%;">
                                                                <td style="width: 40%;">
                                                                   <%-- 'Pinkal (25-May-2019) -- Start
                                                                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.--%>
                                                                    <asp:Label ID="LblReliever" runat="server" CssClass="mandatory_field" Text="Reliever"></asp:Label>
                                                                    <%--'Pinkal (25-May-2019) -- End--%>
                                                                </td>
                                                                <td style="width: 60%">
                                                                    <asp:DropDownList ID="cboReliever" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <%--Pinkal (01-Oct-2018) -- End--%>
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%;">
                                                                    <asp:Label ID="lblApprover" runat="server" Text="Approver:"></asp:Label>
                                                                </td>
                                                                <td style="width: 60%">
                                                                    <asp:DropDownList ID="cboApprover" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%">
                                                                    <asp:Label ID="lblStartdate" runat="server" Text="Start Date:"></asp:Label>
                                                                </td>
                                                                <td style="width: 60%">
                                                                    <uc2:DateCtrl ID="dtpStartdate" AutoPostBack="True" Enabled="True" runat="server"
                                                                        ReadonlyDate="False" />
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%">
                                                                    <asp:Label ID="lblEnddate" runat="server" Text="End Date:"></asp:Label>
                                                                </td>
                                                                <td style="width: 60%">
                                                                    <div style="float: left; padding-right: 10px">
                                                                        <uc2:DateCtrl ID="dtpEnddate" runat="server" AutoPostBack="True" Enabled="True" ReadonlyDate="False" />
                                                                    </div>
                                                                    <asp:Label ID="objNoofDays" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%">
                                                                    <asp:Label ID="lblApprovalDate" runat="server" Text="ApprovalDate:"></asp:Label>
                                                                </td>
                                                                <td style="width: 60%">
                                                                    <uc2:DateCtrl ID="dtpApplyDate" AutoPostBack="false" Enabled="True" runat="server"
                                                                        ReadonlyDate="True" />
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%">
                                                                    <asp:Label ID="lblAllowance" runat="server" Text="Allowance:"></asp:Label>
                                                                </td>
                                                                <td style="width: 60%">
                                                                    <asp:DropDownList ID="cboAllowance" runat="server" AutoPostBack="True">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%">
                                                                    <asp:Label ID="lblValue" runat="server" Text="Value:"></asp:Label>
                                                                </td>
                                                                <td style="width: 60%">
                                                                    <asp:TextBox ID="txtAllowance" runat="server" Enabled="true" ReadOnly="true"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                             <%--  'Pinkal (25-May-2019) -- Start
                                                                 'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                                                            <tr style="width: 100%;">
                                                                <td style="width: 40%">
                                                                    <asp:Label ID="lblStatus" runat="server" Text="Status:"></asp:Label>
                                                                </td>
                                                                <td style="width: 60%">
                                                                    <asp:DropDownList ID="cboStatus" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            'Pinkal (25-May-2019) -- End   --%>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <%-- 'Pinkal (25-May-2019) -- Start
                                                 'Enhancement - NMB FINAL LEAVE UAT CHANGES.--%>
                                        <%--<asp:Button ID="btnSave" runat="server" CssClass=" btnDefault" Text="Save" />--%>
                                        <asp:Button ID="btnApprove" runat="server" CssClass=" btnDefault" Text="Approve" />
                                        <asp:Button ID="btnReject" runat="server" CssClass=" btnDefault" Text="Reject" />
                                        <asp:Button ID="btnReschedule" runat="server" CssClass=" btnDefault" Text="Re-schedule" />
                                         <%--'Pinkal (25-May-2019) -- End--%>
                                        <asp:Button ID="Btnclose" runat="server" CssClass=" btnDefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <uc7:Confirmation ID="LeaveApprovalConfirmation" runat="server" Title="Confirmation" Message="" />
                        <uc7:Confirmation ID="Confirmation" runat="server" Title="Confirmation" Message="Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?" />
                    </div>
                    
                    <cc1:ModalPopupExtender ID="popupClaimRequest" runat="server" BackgroundCssClass="modalBackground"
                        TargetControlID="hdf_ClaimRequest" PopupControlID="pnl_ClaimRequest" DropShadow="true"
                        CancelControlID="hdf_ClaimRequest" />
                    <%--   'Pinkal (04-Feb-2019) -- Start 
                                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                    <asp:Panel ID="pnl_ClaimRequest" runat="server" CssClass="newpopup" Style="display: none;
                        width: 990px">
                        <%--   'Pinkal (04-Feb-2019) -- End  --%>
                        <div class="panel-primary" style="margin-bottom: 0px;">
                            <div class="panel-heading">
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Claim & Request"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div6" class="panel-default">
                                    <div id="Div7" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblExpenseHeader" runat="server" Text="Claim & Request Add/Edit"></asp:Label>
                                        </div>
                                        <%--'Pinkal (20-Nov-2018) -- Start
                                'Enhancement - Working on P2P Integration for NMB.--%>
                                        <div style="text-align: right">
                                            <asp:LinkButton ID="lnkClaimDepedents" runat="server" Text="View Depedents List"
                                                CssClass="lnkhover" Font-Bold="true"></asp:LinkButton>
                                        </div>
                                        <%--'Pinkal (20-Nov-2018) -- End--%>
                                    </div>
                                    <div id="Div10" class="panel-body-default">
                                        <table style="width: 100%;">
                                            <tr style="width: 100%">
                                                <td style="border-right: solid 1px #DDD; width: 40%;">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lblExpCategory" runat="server" Text="Exp.Cat."></asp:Label>
                                                            </td>
                                                            <td style="width: 75%" colspan="3">
                                                                <asp:DropDownList ID="cboExpCategory" runat="server" AutoPostBack="true" Width="263px">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblPeriod" runat="server" Visible="false" Text="Period"></asp:Label>
                                                                <asp:DropDownList ID="cboPeriod" runat="server" Visible="false" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lblClaimNo" runat="server" Text="Claim No"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:TextBox ID="txtClaimNo" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>
                                                            </td>
                                                            <td style="width: 35%">
                                                                <uc2:DateCtrl ID="dtpDate" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lblEmpAddEdit" runat="server" Text="Employee"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:DropDownList ID="cboClaimEmployee" runat="server" AutoPostBack="true" Width="263px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lblLeaveTypeAddEdit" runat="server" Text="Leave Type"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:DropDownList ID="cboLeaveType" runat="server" AutoPostBack="true" Enabled="False"
                                                                    Width="263px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="objlblValue" runat="server" Text="Leave Form"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:DropDownList ID="cboReference" runat="server" AutoPostBack="false" Enabled="False"
                                                                    Width="263px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <%--'Pinkal (20-Nov-2018) -- Start
                                                        'Enhancement - Working on P2P Integration for NMB.--%>
                                                        <%--<tr>
                                                            <td>
                                                            </td>
                                                            <td colspan="3" style="font-weight: bold">
                                                                <asp:LinkButton ID="lnkClaimDepedents" runat="server" ForeColor="#006699" Text="View Depedents List"
                                                                    CssClass="lnkhover"></asp:LinkButton>
                                                            </td>
                                                        </tr>--%>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                                    ReadOnly="true"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <%--'Pinkal (20-Nov-2018) -- End--%>
                                                    </table>
                                                </td>
                                                <td style="width: 60%">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblExpense" runat="server" Text="Expense"></asp:Label>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- Start 
                                                                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <td colspan="3" style="width: 90%">
                                                                <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="true" Width="265px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; padding: 0">
                                                                <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- End --%>
                                                        </tr>
                                                        <%--'Pinkal (20-Nov-2018) -- Start
                                                        'Enhancement - Working on P2P Integration for NMB.--%>
                                                       <%--   'Pinkal (30-Apr-2018) - Start
                                                          'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.--%>
                                                        <%-- <tr style="width: 100%">
                                                           <td style="width: 13%">
                                                                <asp:Label ID="lblClaimBalanceasondate" runat="server" Text="Balance As on Date"></asp:Label>
                                                            </td>
                                                            <td style="width: 23%">
                                                                <asp:TextBox ID="txtClaimBalanceAsOnDate" runat="server" CssClass="txttextalignright"
                                                                    Enabled="false" Style="text-align: right"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                            </td>
                                                            <td style="width: 42%">
                                                                <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 12%">
                                                                <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtCalimQty" runat="server" CssClass="txttextalignright" Text="0"
                                                                    AutoPostBack="true" Style="text-align: right"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 11%">
                                                                <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtCosting" runat="server" CssClass="txttextalignright" Text="0.00"
                                                                    Enabled="false" Visible="false"></asp:TextBox>
                                                                <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 13%">
                                                                <asp:Label ID="lblBalanceAddEdit" runat="server" Text="Balance"></asp:Label>
                                                            </td>
                                                            <td style="width: 23%">
                                                                <asp:TextBox ID="txtClaimBalance" runat="server" CssClass="txttextalignright" Enabled="false"
                                                                    Style="text-align: right"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblSector" runat="server" Text="Sector/Route"></asp:Label>
                                                            </td>
                                                            <td style="width: 42%">
                                                                <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" Width="118px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 12%">
                                                                <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="txttextalignright" Text="0"
                                                                    Style="text-align: right"></asp:TextBox>
                                                            </td>
                                                        </tr>--%>
                                                        <tr style="width: 100%">
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblSector" runat="server" Text="Sector/Route"></asp:Label>
                                                            </td>
                                                            <td style="width: 50%" colspan="3">
                                                                <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" Width="265px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- Start 
                                                                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <%--   <td style="width: 10%">
                                                                <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; padding: 0" colspan="1">
                                                                <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                            </td>--%>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; padding: 0">
                                                                <asp:TextBox ID="txtCalimQty" runat="server" CssClass="txttextalignright" Text="0"
                                                                    AutoPostBack="true" Style="text-align: right"></asp:TextBox>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- End  --%>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center"></asp:Label>
                                                            </td>
                                                            <td style="width: 50%" colspan="3">
                                                                <asp:DropDownList ID="cboCostCenter" runat="server" AutoPostBack="true" Width="265px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- Start 
                                                                      'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <%--  <td style="width: 10%">
                                                                <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; padding: 0" colspan="1">
                                                                <asp:TextBox ID="txtCalimQty" runat="server" CssClass="txttextalignright" Text="0"
                                                                    AutoPostBack="true" Style="text-align: right"></asp:TextBox>
                                                            </td>--%>
                                                            <td style="width: 13%">
                                                                <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                                <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                              <%--'Pinkal (07-Feb-2020) -- Start
                                                                      'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB 
                                                                <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="txttextalignright" Text="0" Style="text-align: right"></asp:TextBox>--%>
                                                                <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="txttextalignright" Text="1.00" Style="text-align: right"></asp:TextBox>
                                                                <%--  'Pinkal (07-Feb-2019) -- End --%>
                                                                <asp:TextBox ID="txtCosting" runat="server" Style="text-align: right" Text="0.00"
                                                                    Enabled="false" Visible="false"></asp:TextBox>
                                                                <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- End  --%>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 14%">
                                                                <asp:Label ID="lblClaimBalanceasondate" runat="server" Text="Balance As on Date"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtClaimBalanceAsOnDate" runat="server" CssClass="txttextalignright"
                                                                    Enabled="false" Style="text-align: right"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 11%">
                                                                <asp:Label ID="lblBalanceAddEdit" runat="server" Text="Balance"></asp:Label>
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:TextBox ID="txtClaimBalance" runat="server" CssClass="txttextalignright" Enabled="false"
                                                                    Style="text-align: right"></asp:TextBox>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- Start 
                                                                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <%-- <td style="width: 13%">
                                                                <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                                <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="txttextalignright" Text="0"
                                                                    Style="text-align: right"></asp:TextBox>
                                                                <asp:TextBox ID="txtCosting" runat="server" Style="text-align: right" Text="0.00"
                                                                    Enabled="false" Visible="false"></asp:TextBox>
                                                                <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                            </td>--%>
                                                            <td style="width: 13%">
                                                                <asp:Label ID="LblClaimCurrency" runat="server" Text="Currency"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:DropDownList ID="cboClaimCurrency" runat="server" Width="115px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- End --%>
                                                        </tr>
                                                          <%--  'Pinkal (30-Apr-2018) - End--%>
                                                        <tr style="width: 100%">
                                                            <td colspan="5">
                                                                <%--   'Pinkal (04-Feb-2019) -- Start
                                                                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                                <cc1:TabContainer ID="tabmain" runat="server" ActiveTabIndex="0">
                                                                    <%--'Pinkal (04-Feb-2019) -- End--%>
                                                                    <cc1:TabPanel ID="tbExpenseRemark" runat="server" HeaderText="Expense Remark">
                                                                        <HeaderTemplate>
                                                                            Expense Remark
                                                                        </HeaderTemplate>
                                                                        <ContentTemplate>
                                                                            <asp:TextBox ID="txtExpRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                        </ContentTemplate>
                                                                    </cc1:TabPanel>
                                                                    <cc1:TabPanel ID="tbClaimRemark" runat="server" HeaderText="Claim Remark">
                                                                        <HeaderTemplate>
                                                                            Claim Remark
                                                                        </HeaderTemplate>
                                                                        <ContentTemplate>
                                                                            <asp:TextBox ID="txtClaimRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                        </ContentTemplate>
                                                                    </cc1:TabPanel>
                                                                </cc1:TabContainer>
                                                            </td>
                                                            <%--  <td colspan="3" style="width: 20%" valign="top">
                                                                <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address"></asp:Label>
                                                                <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                                    Style="height: 56px;" ReadOnly="true"></asp:TextBox>
                                                            </td>--%>
                                                            <td style="width: 15%; vertical-align: bottom;" align="right">
                                                                <asp:Button ID="btnClaimAdd" runat="server" Text="Add" CssClass="btnDefault" />
                                                                <asp:Button ID="btnClaimEdit" runat="server" Text="Edit" CssClass="btnDefault" Visible="false" />
                                                                <asp:HiddenField ID="hdf_ClaimRequest" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <%--'Pinkal (20-Nov-2018) -- End--%>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div id="Div11" class="panel-default">
                                    <%--   'Pinkal (04-Feb-2019) -- Start 
                                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                    <%--   <div id="Div12" class="panel-body-default">--%>
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                <asp:Panel ID="pnl_dgvdata" runat="server" Height="170px" ScrollBars="Auto">
                                                    <%--   'Pinkal (04-Feb-2019) -- End  --%>
                                                        <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                            Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                    FooterText="brnEdit">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="imgEdit" runat="server" CssClass="gridedit" ToolTip="Edit" CommandName="Edit"></asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--0--%>
                                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                    FooterText="btnDelete">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                                                CommandName="Delete"></asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--1--%>
                                                                <asp:TemplateColumn HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                                                                    ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkPreview" runat="server" ToolTip="Preview" Font-Underline="false"
                                                                            CommandName="Preview">
                                                                            <i class="fa fa-eye" aria-hidden="true" style="font-size:20px;"></i>                                                                                              
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--2--%>
                                                                <asp:BoundColumn DataField="expense" HeaderText="Claim/Expense Desc" ReadOnly="true"
                                                                    FooterText="dgcolhExpense" />
                                                                <%--3--%>
                                                                <asp:BoundColumn DataField="sector" HeaderText="Sector / Route" ReadOnly="true" FooterText="dgcolhSectorRoute" />
                                                                <%--4--%>
                                                                <asp:BoundColumn DataField="uom" HeaderText="UoM" ReadOnly="true" FooterText="dgcolhUoM" />
                                                                <%--5--%>
                                                                <asp:BoundColumn DataField="quantity" HeaderText="Quantity" ReadOnly="true" FooterText="dgcolhQty"
                                                                    ItemStyle-CssClass="txttextalignright" />
                                                                <%--6--%>
                                                                <asp:BoundColumn DataField="unitprice" HeaderText="Unit Price" ReadOnly="true" FooterText="dgcolhUnitPrice"
                                                                    ItemStyle-CssClass="txttextalignright" />
                                                                <%--7--%>
                                                                <asp:BoundColumn DataField="amount" HeaderText="Amount" ReadOnly="true" FooterText="dgcolhAmount"
                                                                    ItemStyle-CssClass="txttextalignright" />
                                                                <%--8--%>
                                                                <asp:BoundColumn DataField="expense_remark" HeaderText="Expense Remark" ReadOnly="true"
                                                                    Visible="true" FooterText="dgcolhExpenseRemark" />
                                                                <%--9--%>
                                                                <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" ReadOnly="true"
                                                                    Visible="false" />
                                                                <%--10--%>
                                                                <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" ReadOnly="true"
                                                                    Visible="false" />
                                                                <%--11--%>
                                                                <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" ReadOnly="true" Visible="false" />
                                                                <%--12--%>
                                                            <asp:BoundColumn DataField="crapprovaltranunkid" HeaderText="approvaltranunkid" ReadOnly="true"
                                                                Visible="false" />
                                                                <%--13--%>
                                                            </Columns>
                                                            <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                                        </asp:DataGrid>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="text-align: right">
                                                    <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                                                    <asp:TextBox ID="txtGrandTotal" runat="server" Enabled="False" CssClass="txttextalignright"
                                                        Width="20%"></asp:TextBox>
                                                    <asp:HiddenField ID="hdf_Claim" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <div style="position: absolute; text-align: left;">
                                                <asp:Button ID="btnViewScanAttchment" runat="server" Visible="false" Text="View Scan/Attchment"
                                                    CssClass="btnDefault" />
                                            </div>
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Button ID="btnClaimSave" runat="server" Text="Save" CssClass="btnDefault" ValidationGroup="Step" />
                                                    <asp:Button ID="btnClaimClose" runat="server" Text="Close" CssClass="btnDefault"
                                                        CausesValidation="false" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    <%--   'Pinkal (04-Feb-2019) -- Start
                                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                    <%--  </div>--%>
                                    <%--   'Pinkal (04-Feb-2019) -- End --%>
                                    </div>
                                </div>
                            </div>
                       <%--  'Pinkal (22-Oct-2018) -- Start
                         'Enhancement - Implementing Claim & Request changes For NMB .--%>
                        <uc7:Confirmation ID="popup_UnitPriceYesNo" runat="server" Title="Confirmation" />
                         <%--  'Pinkal (22-Oct-2018) -- End --%>
                        <%--     'Pinkal (04-Feb-2019) -- Start
                                         'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                        <uc7:Confirmation ID="popup_ExpRemarkYesNo" runat="server" Title="Confirmation" />
                        <%--   'Pinkal (04-Feb-2019) -- End --%>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupEmpDepedents" runat="server" BackgroundCssClass="modalBackground"
                        TargetControlID="hdf_claimDeletePopup" PopupControlID="pnlEmpDepedents" DropShadow="true"
                        CancelControlID="btnEmppnlEmpDepedentsClose">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlEmpDepedents" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px;">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="LblEmpDependentsList" runat="server" Text="Dependents List"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div19" class="panel-default">
                                    <div id="Div20" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="Label6" runat="server" Text="Dependents List"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div21" class="panel-body-default">
                                        <asp:Panel ID="pvl_dgvDependance" runat="server" ScrollBars="Auto" Height="200px"
                                            Width="100%">
                                            <asp:DataGrid ID="dgDepedent" runat="server" AutoGenerateColumns="false" Width="99%"
                                                DataKeyField="" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                <Columns>
                                                    <asp:BoundColumn DataField="dependants" HeaderText="Name" ReadOnly="true" FooterText="dgcolhName" />
                                                    <asp:BoundColumn DataField="gender" HeaderText="Gender" ReadOnly="true" FooterText="dgcolhGender" />
                                                    <asp:BoundColumn DataField="age" HeaderText="Age" ReadOnly="true" FooterText="dgcolhAge" />
                                                    <asp:BoundColumn DataField="Months" HeaderText="Month" ReadOnly="true" FooterText="dgcolhMonth" />
                                                    <asp:BoundColumn DataField="relation" HeaderText="Relation" ReadOnly="true" FooterText="dgcolhRelation" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </asp:Panel>
                                        <div class="btn-default">
                                            <asp:Button ID="btnEmppnlEmpDepedentsClose" runat="server" Text="Cancel" CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdf_claimDeletePopup" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupClaimDelete" runat="server" BackgroundCssClass="modalBackground"
                        TargetControlID="hdf_ClaimDelete" PopupControlID="pnl_popupClaimDelete" DropShadow="true"
                        CancelControlID="btnDelReasonNo">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_popupClaimDelete" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px;">
                        <div class="panel-primary" style="margin-bottom: 0px;">
                            <div class="panel-heading">
                                <asp:Label ID="lblClaimDelete" runat="server" Text="Aruti"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div22" class="panel-default">
                                    <div id="Div23" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblClaimTitle" runat="server" Text="Are you Sure You Want To delete?:"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtClaimDelReason" runat="server" TextMode="MultiLine" Width="97%"
                                                        Height="50px" Style="margin-bottom: 5px;" />
                                                    <asp:RequiredFieldValidator ID="rqfv_txtClaimDelReason" runat="server" Display="None"
                                                        ControlToValidate="txtClaimDelReason" ErrorMessage="Delete Reason can not be blank. "
                                                        CssClass="ErrorControl" ForeColor="White" Style="" SetFocusOnError="True" ValidationGroup="ClaimDelete"></asp:RequiredFieldValidator>
                                                    <cc1:ValidatorCalloutExtender runat="Server" ID="PNReqE" TargetControlID="rqfv_txtClaimDelReason"
                                                        Width="300px" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnDelReasonYes" runat="server" Text="Yes" Width="70px" Style="margin-right: 10px"
                                                CssClass="btnDefault" ValidationGroup="ClaimDelete" />
                                            <asp:Button ID="btnDelReasonNo" runat="server" Text="No" Width="70px" Style="margin-right: 10px"
                                                CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdf_ClaimDelete" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupLeaveForm" BackgroundCssClass="modalBackground"
                        TargetControlID="txtLeaveFormNo" runat="server" PopupControlID="pnlLeaveFormpopup"
                        DropShadow="true" CancelControlID="btnLeaveFormOk" />
                    <asp:Panel ID="pnlLeaveFormpopup" runat="server" CssClass="newpopup" Style="display: none;
                        width: 650px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="LblLeaveFormText" Text="Leave Application Form" runat="server" /></b>
                            </div>
                            <div class="panel-body">
                                <div id="Div2" class="panel-default">
                                    <div id="Div15" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="LblLeaveFormText1" Text="Leave Application Form" runat="server" /></b>
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default">
                                        <table width="100%">
                                            <tr width="100%">
                                                <td width="20%">
                                                    <asp:Label ID="LblLeaveFormNo" Text="Leave Application No " runat="server" />
                                                </td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtLeaveFormNo" runat="server" ReadOnly="true" />
                                                </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblLeaveFormEmpCode" Text="Employee Code " runat="server" />
                                                </td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtLeaveFormEmpCode" ReadOnly="true" runat="server" />
                                                </td>
                                            </tr>
                                            <tr width="100%">
                                                <td width="20%">
                                                    <asp:Label ID="lblLeaveFormEmployee" Text="Employee" runat="server" />
                                                </td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtLeaveFormEmployee" runat="server" ReadOnly="true" />
                                                </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblLeaveFormLeaveType" Text="Leave Type" runat="server" />
                                                </td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtLeaveFormLeaveType" runat="server" ReadOnly="true" />
                                                </td>
                                            </tr>
                                            <tr width="100%">
                                                <td width="20%">
                                                    <asp:Label ID="lblLeaveFormApplydate" Text="Apply Date" runat="server" />
                                                </td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtLeaveFormApplydate" ReadOnly="true" runat="server" />
                                                </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblLeaveFormStartdate" Text="Start Date" runat="server" />
                                                </td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtLeaveFormStartdate" runat="server" ReadOnly="true" />
                                                </td>
                                            </tr>
                                            <tr width="100%">
                                                <td width="20%">
                                                    <asp:Label ID="lblLeaveFormReturndate" Text="End Date" runat="server" />
                                                </td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtLeaveFormReturndate" ReadOnly="true" runat="server" />
                                                </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblLeaveFormDayApply" Text="Days To Apply" runat="server" />
                                                </td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtLeaveFormDayApply" ReadOnly="true" runat="server" />
                                                </td>
                                            </tr>
                                            <tr width="100%">
                                                <td colspan="2" width="50%">
                                                    <asp:Label ID="lblLeaveAddress" Text="Address and Telephone Number While On leave"
                                                        runat="server" />
                                                </td>
                                                <td colspan="2" width="50%">
                                                    <asp:Label ID="lblLeaveFormRemark" Text="Remark" runat="server" />
                                                </td>
                                            </tr>
                                            <tr width="100%">
                                                <td colspan="2" width="50%">
                                                    <asp:TextBox ID="txtLeaveFormAddress" runat="server" ReadOnly="true" TextMode="MultiLine"
                                                        Rows="3" />
                                                </td>
                                                <td colspan="2" width="50%">
                                                    <asp:TextBox ID="txtLeaveFormRemark" runat="server" ReadOnly="true" TextMode="MultiLine"
                                                        Rows="3" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnLeaveFormOk" runat="server" CssClass="btndefault" Text="Close" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupFraction" BackgroundCssClass="modalBackground" TargetControlID="LblFraction"
                        runat="server" PopupControlID="pnlFractionPopup" DropShadow="true" CancelControlID="btnFractionCancel">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlFractionPopup" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px">
                        <div class="panel-primary" style="margin-bottom: 0px;">
                            <div class="panel-heading">
                                <asp:Label ID="LblFraction" Text="Leave Day Count" runat="server" Font-Bold="true"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div16" class="panel-default">
                                    <div id="Div17" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="LblFraction1" Text="Leave Day Count" runat="server" Font-Bold="true"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div18" class="panel-body-default">
                                        <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);" style="width: 100%;
                                            height: 350px; overflow: auto">
                                            <asp:Panel ID="pbl_gvfucnation" runat="server">
                                                <asp:GridView ID="dgFraction" runat="server" AutoGenerateColumns="False" Width="98%"
                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                    <Columns>
                                                        <asp:CommandField ShowEditButton="true" ItemStyle-HorizontalAlign="Center" ControlStyle-Font-Underline="false" />
                                                        <asp:BoundField DataField="leavedate" HeaderText="Date" ReadOnly="True" FooterText="dgColhDate" />
                                                        <asp:BoundField DataField="dayfraction" HeaderText="Fractions" ItemStyle-HorizontalAlign="Right"
                                                            HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhFraction" />
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                        <div class="btn-default">
                                            <asp:Button ID="btnFractionOK" runat="server" Width="20%" CssClass="btndefault" Text="Ok" />
                                            <asp:Button ID="btnFractionCancel" runat="server" Width="20%" CssClass="btndefault"
                                                Text="Cancel" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    
                    <cc1:ModalPopupExtender ID="popupExpense" runat="server" BackgroundCssClass="modalBackground"
                        TargetControlID="hdf_ApprovalClaim" PopupControlID="pnlExpensePopup" DropShadow="true"
                        CancelControlID="hdf_ApprovalClaim" BehaviorID="btnApprovalSaveAddEdit">
                    </cc1:ModalPopupExtender>
                    <%--   'Pinkal (04-Feb-2019) -- Start
                                 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                    <asp:Panel ID="pnlExpensePopup" runat="server" CssClass="newpopup" Style="display: none;
                        width: 990px">
                        <%--  'Pinkal (22-Oct-2018) -- Start
                                 'Enhancement - Implementing Claim & Request changes For NMB .--%>
                        <div class="panel-primary" style="margin-bottom: 0px;">
                            <div class="panel-heading">
                                <asp:Label ID="lblApprovalpopupHeader" runat="server" Text="Claim &amp; Request"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div13" class="panel-default">
                                    <div id="Div14" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="Label2" runat="server" Text="Claim &amp; Request Add/Edit"></asp:Label>
                                        </div>
                                        <%--'Pinkal (20-Nov-2018) -- Start
                                        'Enhancement - Working on P2P Integration for NMB.--%>
                                        <div style="text-align: right">
                                            <asp:LinkButton ID="lnkApprovalViewDependants" runat="server" CssClass="lnkhover"
                                                Font-Bold="true" Text="View Depedents List"></asp:LinkButton>
                                        </div>
                                        <%--'Pinkal (20-Nov-2018) -- End--%>
                                    </div>
                                    <div id="Div24" class="panel-body-default">
                                        <table style="width: 100%;">
                                            <tr style="width: 100%">
                                                <td style="border-right: solid 1px #DDD; width: 40%;">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lblApprovalExpCategory" runat="server" Text="Exp.Cat."></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:DropDownList ID="cboApprovalExpCategory" runat="server" AutoPostBack="true"
                                                                    Width="263px">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblApprovalPeriod" runat="server" Text="Period" Visible="false"></asp:Label>
                                                                <asp:DropDownList ID="cboApprovalPeriod" runat="server" AutoPostBack="true" Visible="false">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lblApprovalClaimNo" runat="server" Text="Claim No"></asp:Label>
                                                            </td>
                                                            <td style="width: 30%">
                                                                <asp:TextBox ID="txtApprovalClaimNo" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lbApprovalClaimlDate" runat="server" Text="Date"></asp:Label>
                                                            </td>
                                                            <td style="width: 35%">
                                                                <uc2:DateCtrl ID="dtpApprovalClaimDate" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lblAppvoalExpEmployee" runat="server" Text="Employee"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:DropDownList ID="cboApprovalExpEmployee" runat="server" AutoPostBack="true"
                                                                    Width="263px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="lblApprovalExpLeaveType" runat="server" Text="Leave Type"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:DropDownList ID="cboApprovalExpLeaveType" runat="server" AutoPostBack="true"
                                                                    Enabled="False" Width="263px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="objApprovallblValue" runat="server" Text="Leave Form"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:DropDownList ID="cboApprovalReference" runat="server" AutoPostBack="false" Enabled="False"
                                                                    Width="263px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <%--'Pinkal (20-Nov-2018) -- Start
                                                'Enhancement - Working on P2P Integration for NMB.--%>
                                                        <%--  <tr>
                                                            <td>
                                                            </td>
                                                            <td colspan="3" style="font-weight: bold">
                                                                <asp:LinkButton ID="lnkApprovalViewDependants" runat="server" ForeColor="#006699"
                                                                    Text="View Depedents List" CssClass="lnkhover"></asp:LinkButton>
                                                            </td>
                                                        </tr>--%>
                                                        <tr style="width: 100%">
                                                            <td style="width: 25%">
                                                                <asp:Label ID="LblApprovalDomicileAdd" runat="server" Text="Domicile Address"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 75%">
                                                                <asp:TextBox ID="txtApprovalDomicileAddress" runat="server" ReadOnly="true" Rows="3"
                                                                    TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <%--'Pinkal (20-Nov-2018) -- End--%>
                                                    </table>
                                                </td>
                                                <td style="width: 60%">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblApprovalExpense" runat="server" Text="Expense"></asp:Label>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- Start
                                                                         'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <td colspan="3" style="width: 90%">
                                                                <asp:DropDownList ID="cboApprovalExpense" runat="server" AutoPostBack="true" Width="260px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 13%">
                                                                <asp:Label ID="lblApprovalUoM" runat="server" Text="UoM Type"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; padding: 0">
                                                                <asp:TextBox ID="txtApprovalUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- End --%>
                                                        </tr>
                                                        <%--'Pinkal (20-Nov-2018) -- Start
                                                        'Enhancement - Working on P2P Integration for NMB.--%>
                                                         <%--   'Pinkal (30-Apr-2018) - Start
                                                        'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.--%>
                                                        <%--<tr style="width: 100%">
                                                             <td style="width: 13%">
                                                                <asp:Label ID="lblApprovalExpBalAsonDate" runat="server" Text="Balance As on Date"></asp:Label>
                                                            </td>
                                                            <td style="width: 23%">
                                                                <asp:TextBox ID="txtApprovalExpBalAsonDate" runat="server" CssClass="txttextalignright"
                                                                    Enabled="false" Style="text-align: right"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblApprovalUoM" runat="server" Text="UoM Type"></asp:Label>
                                                            </td>
                                                            <td style="width: 42%">
                                                                <asp:TextBox ID="txtApprovalUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 12%">
                                                                <asp:Label ID="lblApprovalQty" runat="server" Text="Qty"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtApprovalQty" runat="server" CssClass="txttextalignright" Text="0"
                                                                    Style="text-align: right" AutoPostBack="true"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 11%">
                                                                <asp:Label ID="lblApprovalCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtApprovalCosting" runat="server" CssClass="txttextalignright"
                                                                    Text="0.00" Enabled="false" Visible="false"></asp:TextBox>
                                                                <asp:HiddenField ID="txtApprovalCostingTag" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                           <td style="width: 13%">
                                                                <asp:Label ID="lblApprovalExpBalance" runat="server" Text="Balance"></asp:Label>
                                                            </td>
                                                            <td style="width: 23%">
                                                                <asp:TextBox ID="txtApprovalBalance" runat="server" CssClass="txttextalignright"
                                                                    Style="text-align: right" Enabled="false"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblApprovalSector" runat="server" Text="Sector/Route"></asp:Label>
                                                            </td>
                                                            <td style="width: 42%">
                                                                <asp:DropDownList ID="cboApprovalSectorRoute" runat="server" AutoPostBack="true"
                                                                    Width="90px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 12%">
                                                                <asp:Label ID="lblApprovalUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtApprovalUnitPrice" runat="server" CssClass="txttextalignright"
                                                                    Style="text-align: right" Text="0"></asp:TextBox>
                                                            </td>
                                                        </tr>--%>
                                                        <tr style="width: 100%">
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblApprovalSector" runat="server" Text="Sector/Route"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 50%">
                                                                <asp:DropDownList ID="cboApprovalSectorRoute" runat="server" AutoPostBack="true"
                                                                    Width="260px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- Start
                                                                         'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <%--  <td style="width: 10%">
                                                                <asp:Label ID="lblApprovalUoM" runat="server" Text="UoM Type"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; padding: 0" colspan="1">
                                                                <asp:TextBox ID="txtApprovalUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                            </td>--%>
                                                            <td style="width: 13%">
                                                                <asp:Label ID="lblApprovalQty" runat="server" Text="Qty"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; padding: 0">
                                                                <asp:TextBox ID="txtApprovalQty" runat="server" AutoPostBack="true" CssClass="txttextalignright"
                                                                    Style="text-align: right" Text="0"></asp:TextBox>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- End --%>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblApprovalCostCenter" runat="server" Text="Cost Center"></asp:Label>
                                                            </td>
                                                            <td colspan="3" style="width: 50%">
                                                                <asp:DropDownList ID="cboApprovalCostCenter" runat="server" AutoPostBack="true" Width="260px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- Start
                                                                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <%--<td style="width: 10%">
                                                                <asp:Label ID="lblApprovalQty" runat="server" Text="Qty"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; padding: 0" colspan="1">
                                                                <asp:TextBox ID="txtApprovalQty" runat="server" CssClass="txttextalignright" Text="0"
                                                                    Style="text-align: right" AutoPostBack="true"></asp:TextBox>
                                                            </td>--%>
                                                            <td style="width: 13%">
                                                                <asp:Label ID="lblApprovalUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                                <asp:Label ID="lblApprovalCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%;padding:0">
                                                                <%--'Pinkal (07-Feb-2020) -- Start
                                                                        'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB 
                                                                <asp:TextBox ID="txtApprovalUnitPrice" runat="server" CssClass="txttextalignright"  Style="text-align: right" Text="0"></asp:TextBox>--%>
                                                                <asp:TextBox ID="txtApprovalUnitPrice" runat="server" CssClass="txttextalignright"  Style="text-align: right" Text="1.00"></asp:TextBox>
                                                                  <%--   'Pinkal (07-Feb-2019) -- End --%> 
                                                                <asp:TextBox ID="txtApprovalCosting" runat="server" CssClass="txttextalignright"
                                                                    Enabled="false" Text="0.00" Visible="false"></asp:TextBox>
                                                                <asp:HiddenField ID="txtApprovalCostingTag" runat="server" />
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- End --%>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 14%">
                                                                <asp:Label ID="lblApprovalExpBalAsonDate" runat="server" Text="Balance As on Date"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtApprovalExpBalAsonDate" runat="server" CssClass="txttextalignright"
                                                                    Enabled="false" Style="text-align: right"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 11%">
                                                                <asp:Label ID="lblApprovalExpBalance" runat="server" Text="Balance"></asp:Label>
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:TextBox ID="txtApprovalBalance" runat="server" CssClass="txttextalignright"
                                                                    Enabled="false" Style="text-align: right"></asp:TextBox>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- Start
                                                                   'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                            <%--   <td style="width: 13%">
                                                                <asp:Label ID="lblApprovalUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                                <asp:Label ID="lblApprovalCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%">
                                                                <asp:TextBox ID="txtApprovalUnitPrice" runat="server" CssClass="txttextalignright"
                                                                    Style="text-align: right" Text="0"></asp:TextBox>
                                                                <asp:TextBox ID="txtApprovalCosting" runat="server" CssClass="txttextalignright"
                                                                    Text="0.00" Enabled="false" Visible="false"></asp:TextBox>
                                                                <asp:HiddenField ID="txtApprovalCostingTag" runat="server" />
                                                            </td>--%>
                                                            <td style="width: 13%">
                                                                <asp:Label ID="LblApprovalCurrency" runat="server" Text="Currency"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%;padding:0">
                                                                <asp:DropDownList ID="cboApprovalCurrency" runat="server" Width="112px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <%--   'Pinkal (04-Feb-2019) -- End --%>
                                                        </tr>
                                                         <%--   'Pinkal (30-Apr-2018) - End --%>
                                                        <tr style="width: 100%">
                                                            <td colspan="5">
                                                                <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                                                                    <cc1:TabPanel ID="tbApprovalExpenseRemark" runat="server" HeaderText="Expense Remark">
                                                                        <HeaderTemplate>
                                                                            Expense Remark
                                                                        </HeaderTemplate>
                                                                        <ContentTemplate>
                                                                            <asp:TextBox ID="txtApprovalExpRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                        </ContentTemplate>
                                                                    </cc1:TabPanel>
                                                                    <cc1:TabPanel ID="tbApprovalClaimRemark" runat="server" HeaderText="Claim Remark">
                                                                        <ContentTemplate>
                                                                            <asp:TextBox ID="txtApprovalClaimRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                        </ContentTemplate>
                                                                    </cc1:TabPanel>
                                                                </cc1:TabContainer>
                                                            </td>
                                                            <%--<td colspan="3" style="width: 20%" valign="top">
                                                                <asp:Label ID="LblApprovalDomicileAdd" runat="server" Text="Domicile Address"></asp:Label>
                                                                <asp:TextBox ID="txtApprovalDomicileAddress" runat="server" TextMode="MultiLine"
                                                                    Rows="3" Style="height: 56px;" ReadOnly="true"></asp:TextBox>
                                                            </td>--%>
                                                            <td align="right" style="width: 15%; vertical-align: bottom;">
                                                                <asp:Button ID="btnApprovalAdd" runat="server" CssClass="btnDefault" Text="Add" />
                                                                <asp:Button ID="btnApprovalEdit" runat="server" CssClass="btnDefault" Text="Edit"
                                                                    Visible="false" />
                                                            </td>
                                                        </tr>
                                                        <%--'Pinkal (20-Nov-2018) -- End--%>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div id="Div25" class="panel-default">
                                    <%--   'Pinkal (04-Feb-2019) -- Start
                                             'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                    <%--<div id="Div26" class="panel-body-default">--%>
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:Panel ID="pnl_Approvaldgvdata" runat="server" Height="170px" ScrollBars="Auto">
                                                        <%--   'Pinkal (04-Feb-2019) -- End --%>
                                                        <asp:DataGrid ID="dgvApprovalData" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" HeaderStyle-Font-Bold="false"
                                                            ItemStyle-CssClass="griviewitem" ShowFooter="False" Width="99%">
                                                            <Columns>
                                                                <asp:TemplateColumn FooterText="brnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                                    ItemStyle-Width="30px">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="imgEdit" runat="server" CommandName="Edit" CssClass="gridedit"
                                                                                ToolTip="Edit"></asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                                    ItemStyle-Width="30px">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" CssClass="griddelete"
                                                                                ToolTip="Delete"></asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn FooterText="objcolhAttachment" HeaderStyle-HorizontalAlign="Center"
                                                                    HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="imgView" runat="server" CommandName="attachment" ToolTip="Attachment">
                                                                                        <i class="fa fa-paperclip" aria-hidden="true" 
                                                                            style="font-size:20px;font-weight:bold"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="expense" FooterText="dgcolhExpense" HeaderText="Claim/Expense Desc"
                                                                    ReadOnly="true" />
                                                                <asp:BoundColumn DataField="sector" FooterText="dgcolhSectorRoute" HeaderText="Sector / Route"
                                                                    ReadOnly="true" />
                                                                <asp:BoundColumn DataField="uom" FooterText="dgcolhUoM" HeaderText="UoM" ReadOnly="true" />
                                                                <asp:BoundColumn DataField="quantity" FooterText="dgcolhQty" HeaderText="Quantity"
                                                                    ItemStyle-CssClass="txttextalignright" ReadOnly="true" />
                                                                <asp:BoundColumn DataField="unitprice" FooterText="dgcolhUnitPrice" HeaderText="Unit Price"
                                                                    ItemStyle-CssClass="txttextalignright" ReadOnly="true" />
                                                                <asp:BoundColumn DataField="amount" FooterText="dgcolhAmount" HeaderText="Amount"
                                                                    ItemStyle-CssClass="txttextalignright" ReadOnly="true" />
                                                                <asp:BoundColumn DataField="expense_remark" FooterText="dgcolhExpenseRemark" HeaderText="Expense Remark"
                                                                    ReadOnly="true" Visible="true" />
                                                                <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" ReadOnly="true"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" ReadOnly="true"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" ReadOnly="true" Visible="false" />
                                                            </Columns>
                                                            <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                                        </asp:DataGrid>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="text-align: right">
                                                    <asp:Label ID="lblApprovalGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                                                    <asp:TextBox ID="txtApprovalGrandTotal" runat="server" CssClass="txttextalignright"
                                                        Enabled="False" Width="20%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnApprovalSaveAddEdit" runat="server" CssClass="btnDefault" Text="Save" />
                                            <asp:Button ID="btnApprovalCloseAddEdit" runat="server" CssClass="btnDefault" Text="Close" />
                                            <asp:HiddenField ID="hdf_ApprovalClaim" runat="server" />
                                        </div>
                                        <%--   'Pinkal (04-Feb-2019) -- Start
                                           'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                    <%--</div>--%>
                                    <%--   'Pinkal (04-Feb-2019) -- End --%>
                                    </div>
                                </div>
                                <%--  'Pinkal (22-Oct-2018) -- Start
                                 'Enhancement - Implementing Claim & Request changes For NMB .--%>
                                <uc7:Confirmation ID="popup_ApprovalUnitPriceYesNo" runat="server" Title="Confirmation" />
                                 <%--  'Pinkal (22-Oct-2018) -- End --%>
                            <%--     'Pinkal (04-Feb-2019) -- Start
                                 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                            <uc7:Confirmation ID="popup_ApprovalExpRemarkYesNo" runat="server" Title="Confirmation" />
                            <%--   'Pinkal (04-Feb-2019) -- End --%>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupApprovalEmpDepedents" runat="server" BackgroundCssClass="modalBackground"
                        TargetControlID="btnApprovalEmppnlEmpDepedentsClose" PopupControlID="pnlApprovalEmpDepedents"
                        DropShadow="true" CancelControlID="btnApprovalEmppnlEmpDepedentsClose">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlApprovalEmpDepedents" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="LblApprovalEmpDependentsList" runat="server" Text="Dependents List" />
                            </div>
                            <div class="panel-body">
                                <div id="Div27" class="panel-default">
                                    <div id="Div28" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="Label3" runat="server" Text="Dependents List" />
                                        </div>
                                    </div>
                                    <div id="Div29" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:DataGrid ID="dgApprovalDepedent" runat="server" AutoGenerateColumns="false"
                                                        Width="99%" DataKeyField="" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                        RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:BoundColumn DataField="dependants" HeaderText="Name" ReadOnly="true" FooterText="dgcolhName" />
                                                            <asp:BoundColumn DataField="gender" HeaderText="Gender" ReadOnly="true" FooterText="dgcolhGender" />
                                                            <asp:BoundColumn DataField="age" HeaderText="Age" ReadOnly="true" FooterText="dgcolhAge" />
                                                            <asp:BoundColumn DataField="Months" HeaderText="Month" ReadOnly="true" FooterText="dgcolhMonth" />
                                                            <asp:BoundColumn DataField="relation" HeaderText="Relation" ReadOnly="true" FooterText="dgcolhRelation" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnApprovalEmppnlEmpDepedentsClose" runat="server" Text="Cancel"
                                                Width="70px" CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdnApprovalEmpDepedents" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="ModalPopupBG"
                        TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" DropShadow="true"
                        CancelControlID="hdf_ScanAttchment">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="newpopup" Width="600px"
                        Style="display: none;">
                        <div class="panel-primary" style="margin: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div30" class="panel-default">
                                    <div id="Div31" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                </td>
                                                <td style="width: 40%">
                                                    <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server"
                                                        Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 30%">
                                                    <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                        <div id="fileuploader">
                                                            <input type="button" id="btnAddFile" runat="server" class="btndefault" value="Browse" />
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                                        Text="Browse" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td colspan="3" style="width: 100%">
                                                    <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                            ToolTip="Delete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <%--0--%>
                                                            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                            <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <%--1--%>
                                                            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                            <%--2--%>
                                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                            <%--3--%>
                                                            <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                            <%--4--%>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                            <div style="float: left">
                                                <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                            </div>
                                            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                            <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btnDefault" />
                                            <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                           <uc7:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation"  />
                    </asp:Panel>
                    <%--<uc7:Confirmation ID="LeaveApprovalConfirmation" runat="server" Title="Confirmation" Message="" />--%>
                    <%--<uc7:Confirmation ID="Confirmation" runat="server" Title="Confirmation" Message="Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?" />--%>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="popup_ShowAttchment" />
                    <asp:PostBackTrigger ControlID="dgv_Attchment" />
                    <asp:PostBackTrigger ControlID="btnDownloadAll" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <script>
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPg_ProcessLeaveAddEdit.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $('input[type=file]').live("click", function() {
            return IsValidAttach();
        });
    </script>

</asp:Content>
