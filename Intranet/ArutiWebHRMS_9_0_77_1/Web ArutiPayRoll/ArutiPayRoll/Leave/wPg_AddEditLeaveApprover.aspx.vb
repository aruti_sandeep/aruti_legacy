﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region

Partial Class Leave_wPg_AddEditLeaveApprover
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objLeaveApprover As New clsleaveapprover_master
    'Private objLeaveApproverTran As New clsleaveapprover_Tran
    'Pinkal (11-Sep-2020) -- End


    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private Shared ReadOnly mstrModuleName As String = "frmLeaveApprover_AddEdit"
    'Pinkal (06-May-2014) -- End

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
          
            SetLanguage()

            If IsPostBack = False Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (11-Sep-2020) -- End

                FillCombo()


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Dim objLeaveApprover As New clsleaveapprover_master
                Dim objLeaveApproverTran As New clsleaveapprover_Tran
                'Pinkal (11-Sep-2020) -- End

                If Session("ApproverId") IsNot Nothing Then
                    objLeaveApprover._Approverunkid = CInt(Session("ApproverId"))
                    objLeaveApproverTran._Approverunkid = objLeaveApprover._Approverunkid

                    'Pinkal (12-Oct-2020) -- Start
                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                    'objLeaveApproverTran.GetApproverTran(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, objLeaveApprover._Approverunkid, objLeaveApprover._Departmentunkid, objLeaveApprover._Sectionunkid, objLeaveApprover._Jobunkid)
                    objLeaveApproverTran.GetApproverTran(CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, objLeaveApprover._Approverunkid, objLeaveApprover._Departmentunkid, objLeaveApprover._Sectionunkid, objLeaveApprover._Jobunkid, chkIncludeInactiveEmp.Checked)
                    'Pinkal (12-Oct-2020) -- End


                    Me.ViewState.Add("SelectedEmployee", objLeaveApproverTran._DataList)
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'Getvalue()
                    Getvalue(objLeaveApprover)
                    'Pinkal (11-Sep-2020) -- End
                    chkExternalApprover.Enabled = False
                Else
                    Me.ViewState.Add("SelectedEmployee", objLeaveApproverTran._DataList)
                    chkExternalApprover.Checked = False
                    Call chkExternalApprover_CheckedChanged(New Object, New EventArgs())
                End If


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objLeaveApproverTran = Nothing
                objLeaveApprover = Nothing
                'Pinkal (11-Sep-2020) -- End


                Me.ViewState.Add("FirstRecordNo", 0)
                Me.ViewState.Add("LastRecordNo", GvEmployee.PageSize)

                Me.ViewState.Add("FirstSelectedEmpRNo", 0)
                Me.ViewState.Add("LastSelectedEmpRNo", GvSelectedEmployee.PageSize)

                drpUser.Enabled = CBool(Session("AllowMapApproverWithUser"))
                lnkMapLeaveType.Enabled = CBool(Session("AllowtoMapLeaveType"))
                lnkMapLeaveType.Visible = CBool(Session("LeaveApproverForLeaveType"))

                If GvEmployee.Rows.Count <= 0 Then
                    GvEmployee.DataSource = New List(Of String)
                    GvEmployee.DataBind()
                End If
                If GvSelectedEmployee.Rows.Count <= 0 Then
                    GvSelectedEmployee.DataSource = New List(Of String)
                    GvSelectedEmployee.DataBind()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub


    'Pinkal (30-Nov-2013) -- Start
    'Enhancement : Oman Changes

    'Nilay (01-July-2015) -- Start
    'Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
    '    Try
    '        Session("ApproverId") = Nothing
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("Page_Unload :- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-July-2015) -- End

    'Pinkal (30-Nov-2013) -- End

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim objEmployee As New clsEmployee_Master
            'Dim objDepartment As New clsDepartment
            'Dim objSection As New clsSections
            'Dim objJob As New clsJobs
            'Pinkal (11-Sep-2020) -- End

            Dim objApproverLevel As New clsapproverlevel_master

            Dim dsList As DataSet = Nothing

            dsList = objApproverLevel.getListForCombo("Level", True)
            With drpLevel
                .DataTextField = "name"
                .DataValueField = "levelunkid"
                .DataSource = dsList.Tables("Level")
                .DataBind()
            End With
            objApproverLevel = Nothing

            'PRIVILEGE NO 264 :- ALLOW TO APPROVE LEAVE
            Dim mblnAdUser As Boolean = False
            Dim objUser As New clsUserAddEdit
            Dim objOption As New clsPassowdOptions
            Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

            If drOption.Length > 0 Then
                If objOption._UserLogingModeId <> enAuthenticationMode.BASIC_AUTHENTICATION Then mblnAdUser = True
            End If

            dsList = objUser.getNewComboList("User", 0, True, CInt(Session("CompanyUnkId")), CStr(264), CInt(Session("Fin_year")))
            drpUser.DataTextField = "name"
            drpUser.DataValueField = "userunkid"
            drpUser.DataSource = dsList.Tables("User")
            drpUser.DataBind()


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.ViewState.Add("UserList", dsList.Tables("User"))
            Me.ViewState.Add("UserList", dsList.Tables("User").Copy())

            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing

            'Pinkal (11-Sep-2020) -- End



            objOption = Nothing
            objUser = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillEmployee(ByVal mstrEmployeeID As String)
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim dsEmployee As DataSet = Nothing
            Dim strSearch As String = String.Empty


            If chkExternalApprover.Checked = False Then
                If CInt(drpApprover.SelectedValue) > 0 Then
                    strSearch &= "AND hremployee_master.employeeunkid <> " & CInt(drpApprover.SelectedValue) & " "
                End If
            End If

            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	
            'If mstrEmployeeID.Trim.Length > 0 Then
            '    strSearch &= " AND hremployee_master.employeeunkid not in ( " & mstrEmployeeID.Trim & " )"
            'End If
            'Pinkal (28-Apr-2020) -- End

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

            'Dim blnInActiveEmp As Boolean = False
            'If (Session("ApproverId") Is Nothing OrElse CInt(Session("ApproverId")) <= 0) Then
            '    blnInActiveEmp = False
            'Else
            '    blnInActiveEmp = True
            'End If
            Dim blnInActiveEmp As Boolean = chkIncludeInactiveEmp.Checked
            'Pinkal (12-Oct-2020) -- End


            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	
            'dsEmployee = objEmployee.GetList(Session("Database_Name").ToString(), CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
            '                                Session("UserAccessModeSetting").ToString(), True, _
            '                                blnInActiveEmp, _
            '                                "Employee", _
            '                                CBool(Session("ShowFirstAppointmentDate")), -1, False, strSearch, False, False)

            Dim StrCheck_Fields As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & _
                                                          "," & clsEmployee_Master.EmpColEnum.Col_Section & "," & clsEmployee_Master.EmpColEnum.Col_Job

            dsEmployee = objEmployee.GetListForDynamicField(StrCheck_Fields, Session("Database_Name").ToString(), CInt(Session("UserId")) _
                                                                                     , CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                     , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, Session("UserAccessModeSetting").ToString() _
                                                                                     , True, blnInActiveEmp, "Employee", -1, False, strSearch, CBool(Session("ShowFirstAppointmentDate")) _
                                                                                     , False, False, True, Nothing, False)


            'If dsEmployee IsNot Nothing Then
            '    dsEmployee.Tables(0).Columns.Add("Select", Type.GetType("System.Boolean"))
            '    dsEmployee.Tables(0).Columns("Select").DefaultValue = False
            'End If

            If dsEmployee IsNot Nothing Then
                Dim dcColumn As New DataColumn("Select", Type.GetType("System.Boolean"))
                dcColumn.DefaultValue = False
                dsEmployee.Tables(0).Columns.Add(dcColumn)
            End If

            'Pinkal (28-Apr-2020) -- End

            Dim dtEmployee As DataTable = Nothing

            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	
            'dtEmployee = dsEmployee.Tables("Employee")
            If mstrEmployeeID.Trim.Length > 0 Then
                dtEmployee = New DataView(dsEmployee.Tables(0), "employeeunkid not in ( " & mstrEmployeeID.Trim & " )", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtEmployee = dsEmployee.Tables("Employee")
            End If
            'Pinkal (28-Apr-2020) -- End



            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Session.Add("EmpList", dtEmployee)
            Session.Add("EmpList", dtEmployee.Copy())
            'Pinkal (11-Sep-2020) -- End


            GvEmployee.DataSource = dtEmployee
            GvEmployee.DataBind()


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtEmployee IsNot Nothing Then dtEmployee.Clear()
            dtEmployee = Nothing
            If dsEmployee IsNot Nothing Then dsEmployee.Clear()
            dsEmployee = Nothing
            objEmployee = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                GvEmployee.PageIndex = 0
                GvEmployee.DataBind()
            Else
                Throw ex
                DisplayMessage.DisplayError(ex.Message, Me)
            End If
        End Try
    End Sub

    Private Sub SelectedEmplyeeList()
        Dim strSearch As String = String.Empty
        Dim dtEmployee As DataTable = Nothing
        Try


            'Pinkal (30-Nov-2013) -- Start
            'Enhancement : Oman Changes

            If Me.ViewState("SelectedEmployee") Is Nothing Then Exit Sub

            'Pinkal (30-Nov-2013) -- End

            Dim dtSelectedEmp As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)

            If Session("ApproverId") IsNot Nothing AndAlso dtSelectedEmp.Columns.Contains("Select") = False Then
                dtSelectedEmp.Columns.Add("Select", Type.GetType("System.Boolean"))
                dtSelectedEmp.Columns("Select").DefaultValue = False
            End If

            strSearch = "AND AUD <> 'D' "

            'S.SANDEEP [30 JAN 2016] -- START
            'If CInt(drpDepartment.SelectedValue) > 0 Then
            '    strSearch &= "AND departmentunkid=" & CInt(drpDepartment.SelectedValue) & " "
            'End If
            'If CInt(drpSection.SelectedValue) > 0 Then
            '    strSearch &= "AND sectionunkid=" & CInt(drpSection.SelectedValue)
            'End If
            'If CInt(drpJob.SelectedValue) > 0 Then
            '    strSearch &= "AND jobunkid=" & CInt(drpJob.SelectedValue)
            'End If
            'S.SANDEEP [30 JAN 2016] -- END



            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                dtEmployee = New DataView(dtSelectedEmp, strSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtEmployee = dtSelectedEmp
            End If

            GvSelectedEmployee.DataSource = dtEmployee
            GvSelectedEmployee.DataBind()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.ViewState("SelectedEmp") = dtEmployee
            Me.ViewState("SelectedEmp") = dtEmployee.Copy()
            'Pinkal (11-Sep-2020) -- End



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SelectedEmplyeeList :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If dtEmployee IsNot Nothing Then dtEmployee.Dispose()
            If dtEmployee IsNot Nothing Then dtEmployee.Clear()
            dtEmployee = Nothing
            'Pinkal (11-Sep-2020) -- End

        End Try

    End Sub

    Private Function Validation() As Boolean
        Try

            If CInt(drpApprover.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Approver Name cannot be blank. Approver Name is required information."), Me)
                drpApprover.Focus()
                Return False

            ElseIf CInt(drpLevel.SelectedValue) = 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Approver Level is compulsory information.Please Select Approver Level."), Me)
                drpLevel.Focus()
                Return False


                'Pinkal (06-Apr-2013) -- Start
                'Enhancement : TRA Changes

            ElseIf CInt(drpUser.SelectedValue) <= 0 Then
                'S.SANDEEP [30 JAN 2016] -- START
                'Language.setLanguage(mstrModuleName)
                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "User is compulsory information.Please map user to this approver."), Me)
                'drpUser.Focus()
                'Return False
                If chkExternalApprover.Checked = False Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "User is compulsory information.Please map user to this approver."), Me)
                    drpUser.Focus()
                    Return False
                End If
                'S.SANDEEP [30 JAN 2016] -- END


                'Nilay (30-Jun-2015) -- Start
                'ElseIf CBool(Session("AllowtoMapLeaveType")) = True AndAlso (lnkMapLeaveType.Enabled = False And (Session("ApproverId") Is Nothing OrElse CInt(Session("ApproverId") <= 0))) Then
            ElseIf CBool(Session("LeaveApproverForLeaveType")) AndAlso CBool(Session("AllowtoMapLeaveType")) = True AndAlso (lnkMapLeaveType.Enabled = False And (Session("ApproverId") Is Nothing OrElse CInt(Session("ApproverId")) <= 0)) Then
                'Nilay (30-Jun-2015) -- End
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Leave Type Mapping is compulsory information.Please map leave type to this approver."), Me)
                lnkMapLeaveType.Focus()
                Return False

                'Pinkal (06-Apr-2013) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
        Return True
    End Function


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub SetValue()
    Private Sub SetValue(ByRef objLeaveApprover As clsleaveapprover_master)
        'Pinkal (11-Sep-2020) -- End
        Try
            objLeaveApprover._Levelunkid = CInt(drpLevel.SelectedValue)
            objLeaveApprover._leaveapproverunkid = CInt(drpApprover.SelectedValue)
            objLeaveApprover._Departmentunkid = -1
            objLeaveApprover._Sectionunkid = -1
            objLeaveApprover._Userumkid = CInt(Session("UserId"))

            If chkExternalApprover.Checked = True Then
                objLeaveApprover._MapUserId = CInt(drpApprover.SelectedValue)
            Else
                objLeaveApprover._MapUserId = CInt(drpUser.SelectedValue)
            End If

            objLeaveApprover._dtLeaveTypeMapping = CType(Me.ViewState("LeaveTypeMapping"), DataTable)

            objLeaveApprover._Isexternalapprover = chkExternalApprover.Checked
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillApprover()
        Try
            Dim dsApprover As New DataSet
            If chkExternalApprover.Checked = False Then
                Dim objEmployee As New clsEmployee_Master


                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                'Dim blnInActiveEmp As Boolean = False
                'If (Session("ApproverId") Is Nothing OrElse CInt(Session("ApproverId")) <= 0) Then
                '    blnInActiveEmp = False
                'Else
                '    blnInActiveEmp = True
                'End If

                Dim blnInActiveEmp As Boolean = chkIncludeInactiveEmp.Checked

                'Pinkal (12-Oct-2020) -- End


                'Pinkal (28-Apr-2020) -- Start
                'Optimization  - Working on Process Optimization and performance for require module.


                'dsApprover = objEmployee.GetList(Session("Database_Name").ToString(), CInt(Session("UserId")), _
                '                               CInt(Session("Fin_year")), _
                '                               CInt(Session("CompanyUnkId")), _
                '                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                '                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                '                               Session("UserAccessModeSetting").ToString(), True, _
                '                               blnInActiveEmp, _
                '                               "Employee", _
                '                               CBool(Session("ShowFirstAppointmentDate")), -1, False, "hremployee_master.isapproved=1", , )

                Dim StrCheck_Fields As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name

                dsApprover = objEmployee.GetListForDynamicField(StrCheck_Fields, Session("Database_Name").ToString(), CInt(Session("UserId")) _
                                                                                         , CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                         , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, Session("UserAccessModeSetting").ToString() _
                                                                                         , True, blnInActiveEmp, "Employee", -1, False, "", CBool(Session("ShowFirstAppointmentDate")) _
                                                                                         , False, False, True, Nothing, True)

                'Pinkal (28-Apr-2020) -- End



                'Pinkal (28-Apr-2020) -- Start
                'Optimization  - Working on Process Optimization and performance for require module.	

                'Dim drRow As DataRow = dsApprover.Tables(0).NewRow
                'drRow("name") = "Select"
                'drRow("employeeunkid") = 0
                'dsApprover.Tables(0).Rows.InsertAt(drRow, 0)

                'With drpApprover
                '    .DataTextField = "name"
                '    .DataValueField = "employeeunkid"
                '    .DataSource = dsApprover.Tables("Employee")
                '    .DataBind()
                'End With

                Dim drRow As DataRow = dsApprover.Tables(0).NewRow
                drRow("Employee Name") = "Select"
                drRow("employeeunkid") = 0
                dsApprover.Tables(0).Rows.InsertAt(drRow, 0)

                With drpApprover
                    .DataTextField = "Employee Name"
                    .DataValueField = "employeeunkid"
                    .DataSource = dsApprover.Tables("Employee")
                    .DataBind()
                End With
                'Pinkal (28-Apr-2020) -- End


                objEmployee = Nothing

            Else

                Dim objUser As New clsUserAddEdit
                dsApprover = objUser.GetExternalApproverList("List", CInt(Session("CompanyUnkId")), _
                                                         CInt(Session("Fin_year")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, "264")

                Dim drRow As DataRow = dsApprover.Tables(0).NewRow
                drRow("name") = "Select"
                drRow("userunkid") = 0
                dsApprover.Tables(0).Rows.InsertAt(drRow, 0)

                With drpApprover
                    .DataTextField = "name"
                    .DataValueField = "userunkid"
                    .DataSource = dsApprover.Tables("List")
                    .DataBind()
                End With
                objUser = Nothing
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsApprover IsNot Nothing Then dsApprover.Clear()
            dsApprover = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillApprover :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub Getvalue()
    Private Sub Getvalue(ByRef objLeaveApprover As clsleaveapprover_master)
        'Pinkal (11-Sep-2020) -- End
        Try

            'S.SANDEEP [30 JAN 2016] -- START
            chkExternalApprover.Checked = objLeaveApprover._Isexternalapprover
            Call chkExternalApprover_CheckedChanged(New Object, New EventArgs)
            'S.SANDEEP [30 JAN 2016] -- END

            drpApprover.SelectedValue = objLeaveApprover._leaveapproverunkid.ToString()
            'drpApprover_SelectedIndexChanged(New Object(), New EventArgs())
            Call drpApprover_SelectedIndexChanged(Nothing, Nothing)
            drpLevel.SelectedValue = objLeaveApprover._Levelunkid.ToString()

            SelectedEmplyeeList()

            If Session("ApproverId") IsNot Nothing AndAlso CInt(Session("ApproverId")) > 0 Then
                drpUser.SelectedValue = objLeaveApprover._MapUserId.ToString()
                Dim objLeaveTypeMapping As New clsapprover_leavetype_mapping
                Me.ViewState("LeaveTypeMapping") = objLeaveTypeMapping.GetLeaveTypeForMapping(CInt(Session("ApproverId")))
                objLeaveTypeMapping = Nothing
            Else
                drpUser.SelectedValue = "0"
            End If


            drpApprover.Enabled = False
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Getvalue :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master
    'Private Sub GetEmployeePageRecordNo()
    '    Try
    '        If GvEmployee.PageIndex > 0 Then
    '            Me.ViewState("FirstRecordNo") = (((GvEmployee.PageIndex + 1) * GvEmployee.Rows.Count) - GvEmployee.Rows.Count)
    '        Else
    '            If GvEmployee.Rows.Count < GvEmployee.PageSize Then
    '                Me.ViewState("FirstRecordNo") = ((1 * GvEmployee.Rows.Count) - GvEmployee.Rows.Count)
    '            Else
    '                Me.ViewState("FirstRecordNo") = ((1 * GvEmployee.PageSize) - GvEmployee.Rows.Count)
    '            End If

    '        End If
    '        Me.ViewState("LastRecordNo") = ((GvEmployee.PageIndex + 1) * GvEmployee.Rows.Count)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("GetEmployeePageRecordNo:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Private Sub GetSelectedEmpPageRecordNo()
    '    Try
    '        If GvSelectedEmployee.PageIndex > 0 Then
    '            Me.ViewState("FirstSelectedEmpRNo") = (((GvSelectedEmployee.PageIndex + 1) * GvSelectedEmployee.Rows.Count) - GvSelectedEmployee.Rows.Count)
    '        Else
    '            If GvSelectedEmployee.Rows.Count < GvSelectedEmployee.PageSize Then
    '                Me.ViewState("FirstSelectedEmpRNo") = ((1 * GvSelectedEmployee.Rows.Count) - GvSelectedEmployee.Rows.Count)
    '            Else
    '                Me.ViewState("FirstSelectedEmpRNo") = ((1 * GvSelectedEmployee.PageSize) - GvSelectedEmployee.Rows.Count)
    '            End If
    '        End If
    '        Me.ViewState("LastSelectedEmpRNo") = ((GvSelectedEmployee.PageIndex + 1) * GvSelectedEmployee.Rows.Count)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("GetSelectedEmpPageRecordNo:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Shani(17-Aug-2015) -- End
#End Region

#Region "ComboBox Event"

    Protected Sub drpApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpApprover.SelectedIndexChanged
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveApprover As New clsleaveapprover_master
        'Pinkal (11-Sep-2020) -- End
        Try
            If CInt(drpApprover.SelectedValue) > 0 Then
                Dim mstrEmployeeID As String = objLeaveApprover.GetApproverEmployeeId(CInt(drpApprover.SelectedValue), chkExternalApprover.Checked)

                Me.ViewState.Add("EmployeeIDs", mstrEmployeeID)
                FillEmployee(mstrEmployeeID)


                Dim objOption As New clsPassowdOptions
                If objOption._IsEmployeeAsUser Then
                    Dim objUser As New clsUserAddEdit
                    Dim mintUserID As Integer = objUser.Return_UserId(CInt(drpApprover.SelectedValue), CInt(Session("CompanyUnkId")))
                    Dim drRow() As DataRow = CType(Me.ViewState("UserList"), DataTable).Select("userunkid = " & mintUserID)
                    If drRow.Length > 0 Then
                        drpUser.SelectedValue = mintUserID.ToString()
                    Else
                        drpUser.SelectedValue = "0"
                    End If
                End If
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objOption = Nothing
                'Pinkal (11-Sep-2020) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpApprover_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveApprover = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "TextBox Event"


    'Pinkal (28-Apr-2020) -- Start
    'Optimization  - Working on Process Optimization and performance for require module.	

    'Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
    '    Try
    '        If Session("EmpList") IsNot Nothing Then
    '            Dim dvEmployee As DataView = CType(Session("EmpList"), DataTable).DefaultView
    '            If dvEmployee IsNot Nothing Then
    '                If dvEmployee.Table.Rows.Count > 0 Then
    '                    dvEmployee.RowFilter = "employeecode like '%" & txtSearch.Text.Trim & "%'  OR name like '%" & txtSearch.Text.Trim & "%' "
    '                    GvEmployee.DataSource = dvEmployee
    '                    GvEmployee.DataBind()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("txtSearch_TextChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Pinkal (28-Apr-2020) -- End



#End Region

#Region "CheckBox Event"


    'Pinkal (28-Apr-2020) -- Start
    'Optimization  - Working on Process Optimization and performance for require module.	

    'Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If GvEmployee.Rows.Count <= 0 Then Exit Sub
    '        Dim dtEmployee As DataTable = CType(Session("EmpList"), DataTable)

    '        'SHANI [01 FEB 2015]-START
    '        'Enhancement - REDESIGN SELF SERVICE.
    '        'GetEmployeePageRecordNo()
    '        'If Me.ViewState("FirstRecordNo") IsNot Nothing AndAlso Me.ViewState("LastRecordNo") IsNot Nothing Then
    '        If GvEmployee.Rows.Count > 0 Then
    '            'Dim mintFirstRecord As Integer = Me.ViewState("FirstRecordNo")
    '            'Dim mintLastRecord As Integer = Me.ViewState("LastRecordNo")
    '            'Dim mintrowindex As Integer = 0
    '            'For i As Integer = mintFirstRecord To mintLastRecord - 1
    '            For i As Integer = 0 To GvEmployee.Rows.Count - 1
    '                If dtEmployee.Rows.Count - 1 < i Then Exit For
    '                'SHANI [01 FEB 2015]--END
    '                Dim drRow As DataRow() = dtEmployee.Select("employeecode = '" & GvEmployee.Rows(i).Cells(1).Text.Trim & "'")  'SHANI [01 FEB 2015]--GvEmployee.Rows(mintrowindex).Cells(1).Text.Trim 
    '                If drRow.Length > 0 Then
    '                    drRow(0)("Select") = cb.Checked
    '                    Dim gvRow As GridViewRow = GvEmployee.Rows(i) 'SHANI [01 FEB 2015]--GvEmployee.Rows(mintrowindex)
    '                    CType(gvRow.FindControl("ChkgvSelect"), CheckBox).Checked = cb.Checked
    '                End If
    '                dtEmployee.AcceptChanges()
    '                'SHANI [01 FEB 2015]-START
    '                'Enhancement - REDESIGN SELF SERVICE.
    '                'mintrowindex += 1
    '                'SHANI [01 FEB 2015]--END
    '            Next
    '            Session("EmpList") = dtEmployee

    '        End If

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkAll_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub ChkgvSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
    '        If gvRow.Cells.Count > 0 Then
    '            Dim drRow() As DataRow = CType(Session("EmpList"), DataTable).Select("employeecode = '" & gvRow.Cells(1).Text & "'")
    '            If drRow.Length > 0 Then
    '                drRow(0)("Select") = cb.Checked
    '                drRow(0).AcceptChanges()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("ChkgvSelect_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub


    'Protected Sub ChkSelectedEmpAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If GvSelectedEmployee.Rows.Count <= 0 Then Exit Sub
    '        Dim dtSelectedEmp As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)

    '        'SHANI [01 FEB 2015]-START
    '        'Enhancement - REDESIGN SELF SERVICE.
    '        'GetSelectedEmpPageRecordNo()
    '        'If Me.ViewState("FirstSelectedEmpRNo") IsNot Nothing AndAlso Me.ViewState("LastSelectedEmpRNo") IsNot Nothing Then
    '        If GvSelectedEmployee.Rows.Count > 0 Then
    '            'Dim mintFirstRecord As Integer = Me.ViewState("FirstSelectedEmpRNo")
    '            'Dim mintLastRecord As Integer = Me.ViewState("LastSelectedEmpRNo")
    '            'Dim mintrowindex As Integer = 0
    '            'For i As Integer = mintFirstRecord To mintLastRecord - 1
    '            For i As Integer = 0 To GvSelectedEmployee.Rows.Count - 1
    '                If dtSelectedEmp.Rows.Count - 1 < i Then Exit For
    '                'SHANI [01 FEB 2015]--END
    '                Dim drRow As DataRow() = dtSelectedEmp.Select("employeecode = '" & GvSelectedEmployee.Rows(i).Cells(1).Text.Trim & "'") 'SHANI [01 FEB 2015]--GvSelectedEmployee.Rows(mintrowindex).Cells(1).Text.Trim
    '                If drRow.Length > 0 Then
    '                    drRow(0)("Select") = cb.Checked
    '                    Dim gvRow As GridViewRow = GvSelectedEmployee.Rows(i) 'SHANI [01 FEB 2015]--GvSelectedEmployee.Rows(mintrowindex)
    '                    CType(gvRow.FindControl("ChkgvSelectedEmp"), CheckBox).Checked = cb.Checked
    '                End If
    '                dtSelectedEmp.AcceptChanges()
    '                'SHANI [01 FEB 2015]-START
    '                'Enhancement - REDESIGN SELF SERVICE.
    '                'mintrowindex += 1
    '                'SHANI [01 FEB 2015]--END
    '            Next
    '            Me.ViewState("SelectedEmployee") = dtSelectedEmp
    '        End If

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("ChkSelectedEmpAll_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub ChkgvSelectedEmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
    '        If gvRow.Cells.Count > 0 Then
    '            Dim drRow() As DataRow = CType(Me.ViewState("SelectedEmployee"), DataTable).Select("employeecode = '" & gvRow.Cells(1).Text & "'")
    '            If drRow.Length > 0 Then
    '                drRow(0)("Select") = cb.Checked
    '                drRow(0).AcceptChanges()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("ChkgvSelectedEmp_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Pinkal (28-Apr-2020) -- End

#End Region

#Region "GridView Event"

    'Pinkal (28-Apr-2020) -- Start
    'Optimization  - Working on Process Optimization and performance for require module.	

    'Protected Sub GvEmployee_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvEmployee.RowDataBound
    '    Try
    '        If e.Row.Cells.Count > 1 Then
    '            Dim dRow() As DataRow = CType(Session("EmpList"), DataTable).Select("employeecode = '" & e.Row.Cells(1).Text & "'")
    '            If dRow.Length > 0 Then
    '                If IsDBNull(dRow(0).Item("select")) Then dRow(0).Item("select") = False
    '                If CBool(dRow(0).Item("select")) = True Then
    '                    Dim gvRow As GridViewRow = e.Row
    '                    CType(gvRow.FindControl("ChkgvSelect"), CheckBox).Checked = CBool(dRow(0).Item("select"))
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("GvEmployee_RowDataBound:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub


    'Protected Sub GvSelectedEmployee_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvSelectedEmployee.RowDataBound
    '    Try
    '        If e.Row.Cells.Count > 1 Then
    '            Dim dRow() As DataRow = CType(Me.ViewState("SelectedEmployee"), DataTable).Select("employeecode = '" & e.Row.Cells(1).Text & "'")
    '            If dRow.Length > 0 Then
    '                If IsDBNull(dRow(0).Item("select")) Then dRow(0).Item("select") = False
    '                If CBool(dRow(0).Item("select")) = True Then
    '                    Dim gvRow As GridViewRow = e.Row
    '                    CType(gvRow.FindControl("ChkgvSelectedEmp"), CheckBox).Checked = CBool(dRow(0).Item("select"))
    '                End If
    '            End If
    '        End If

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("GvSelectedEmployee_RowDataBound:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Pinkal (28-Apr-2020) -- End

#End Region

#Region "Button's Event"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try

            If Validation() = False Then Exit Sub


            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	

            'Dim dtEmployee As DataTable = CType(Session("EmpList"), DataTable)

            'Dim drRow() As DataRow = dtEmployee.Select("Select = true")
            'If drRow.Length <= 0 Then
            '    DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
            '    Exit Sub
            'End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = GvEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                Exit Sub
            End If

            Dim dtSelectedEmp As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)

            'If dtSelectedEmp.Columns.Contains("Select") = False Then
            '    dtSelectedEmp.Columns.Add("Select", Type.GetType("System.Boolean"))
            '    dtSelectedEmp.Columns("Select").DefaultValue = False
            '    dtSelectedEmp.Columns.Add("employeecode", Type.GetType("System.String"))
            '    dtSelectedEmp.Columns("employeecode").DefaultValue = ""
            'End If

            'Dim dtRow As DataRow = Nothing
            'Dim count As Integer
            'For i As Integer = 0 To drRow.Length - 1
            '    count = CInt(dtSelectedEmp.Compute("count(employeeunkid)", "employeeunkid=" & CInt(drRow(i)("employeeunkid"))))
            '    If count > 0 Then
            '        Dim dr() As DataRow = Nothing
            '        dr = dtSelectedEmp.Select("employeeunkid=" & CInt(drRow(i)("employeeunkid")))
            '        If dr.Length > 0 Then
            '            dr(0)("approverunkid") = -1
            '            dr(0)("approvername") = drpApprover.SelectedItem.Text 'Shani [01 FEB 2015] -- drpApprover.SelectedText.Trim
            '            dr(0)("AUD") = "A"
            '        End If
            '    Else
            '        dtRow = dtSelectedEmp.NewRow
            '        dtRow("leaveapprovertranunkid") = -1
            '        dtRow("approverunkid") = -1
            '        dtRow("approvername") = drpApprover.SelectedItem.Text 'Shani [01 FEB 2015] -- drpApprover.SelectedText.Trim
            '        dtRow("employeeunkid") = CInt(drRow(i)("employeeunkid"))
            '        dtRow("employeecode") = drRow(i)("employeecode").ToString
            '        dtRow("name") = drRow(i)("name").ToString().Trim
            '        dtRow("departmentunkid") = CInt(drRow(i)("departmentunkid").ToString().Trim)
            '        dtRow("departmentname") = drRow(i)("deptName").ToString().Trim
            '        dtRow("sectionunkid") = CInt(drRow(i)("sectionunkid").ToString().Trim)
            '        dtRow("sectionname") = drRow(i)("section").ToString().Trim
            '        dtRow("jobunkid") = CInt(drRow(i)("jobunkid").ToString().Trim)
            '        dtRow("jobname") = drRow(i)("job_name").ToString().Trim
            '        dtRow("AUD") = "A"
            '        dtRow("GUID") = Guid.NewGuid.ToString
            '        dtSelectedEmp.Rows.Add(dtRow)
            '    End If
            'Next

            If dtSelectedEmp.Columns.Contains("Select") = False Then
                Dim dcColumn As New DataColumn("Select", Type.GetType("System.Boolean"))
                dcColumn.DefaultValue = False
                dtSelectedEmp.Columns.Add(dcColumn)

                dcColumn = Nothing
                dcColumn = New DataColumn("employeecode", Type.GetType("System.String"))
                dcColumn.DefaultValue = ""
                dtSelectedEmp.Columns.Add(dcColumn)
            End If

            Dim dtRow As DataRow = Nothing
            Dim count As Integer
            Dim xIndex As Integer = -1
            For i As Integer = 0 To gRow.Count - 1
                count = CInt(dtSelectedEmp.Compute("count(employeeunkid)", "employeeunkid=" & CInt(GvEmployee.DataKeys(gRow(i).DataItemIndex)("employeeunkid"))))
                If count > 0 Then
                    Dim dr() As DataRow = Nothing
                    dr = dtSelectedEmp.Select("employeeunkid=" & CInt(GvEmployee.DataKeys(gRow(i).DataItemIndex)("employeeunkid")))
                    If dr.Length > 0 Then
                        dr(0)("approverunkid") = -1
                        dr(0)("approvername") = drpApprover.SelectedItem.Text
                        dr(0)("AUD") = "A"
                    End If
                Else
                    dtRow = dtSelectedEmp.NewRow
                    dtRow("leaveapprovertranunkid") = -1
                    dtRow("approverunkid") = -1
                    dtRow("approvername") = drpApprover.SelectedItem.Text
                    dtRow("employeeunkid") = CInt(GvEmployee.DataKeys(gRow(i).DataItemIndex)("employeeunkid"))
                    dtRow("employeecode") = GvEmployee.DataKeys(gRow(i).DataItemIndex)("Code").ToString()
                    dtRow("name") = GvEmployee.DataKeys(gRow(i).DataItemIndex)("Employee Name").ToString()
                    dtRow("departmentunkid") = -1
                    dtRow("departmentname") = GvEmployee.DataKeys(gRow(i).DataItemIndex)("Department").ToString()
                    dtRow("sectionunkid") = -1
                    dtRow("sectionname") = GvEmployee.DataKeys(gRow(i).DataItemIndex)("Section").ToString()
                    dtRow("jobunkid") = -1
                    dtRow("jobname") = GvEmployee.DataKeys(gRow(i).DataItemIndex)("Job").ToString()
                    dtRow("AUD") = "A"
                    dtRow("GUID") = Guid.NewGuid.ToString
                    dtSelectedEmp.Rows.Add(dtRow)
                End If
            Next
            'Pinkal (28-Apr-2020) -- End


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.ViewState("SelectedEmployee") = dtSelectedEmp
            Me.ViewState("SelectedEmployee") = dtSelectedEmp.Copy()
            'Pinkal (11-Sep-2020) -- End


            GvSelectedEmployee.DataSource = dtSelectedEmp
            GvSelectedEmployee.DataBind()


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing

            If dtSelectedEmp IsNot Nothing Then dtSelectedEmp.Clear()
            dtSelectedEmp = Nothing

            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnAdd_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim dtSelectedEmp As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)
            Dim dtSelectedEmp As DataTable = Nothing
            If Me.ViewState("SelectedEmployee") IsNot Nothing Then
                dtSelectedEmp = CType(Me.ViewState("SelectedEmployee"), DataTable).Copy()
            End If

            'Pinkal (11-Sep-2020) -- End


            If dtSelectedEmp Is Nothing OrElse dtSelectedEmp.Columns.Contains("Select") = False Then Exit Sub


            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	

            'Dim drRow() As DataRow = dtSelectedEmp.Select("Select = true")
            'If drRow.Length <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Selected Employee is compulsory information.Please Check atleast One Selected Employee."), Me)
            '    Exit Sub
            'End If

            'Dim drTemp As DataRow() = Nothing
            'Dim objLeaveForm As New clsleaveform

            'For i As Integer = 0 To drRow.Length - 1

            '    If CInt(drRow(i)("leaveapprovertranunkid")) = -1 Then
            '        drTemp = dtSelectedEmp.Select("GUID = '" & drRow(i)("GUID").ToString() & "'")
            '    Else

            '        If objLeaveForm.GetApproverPendingLeaveFormCount(objLeaveApprover._Approverunkid, drRow(i)("employeeunkid").ToString()) <= 0 Then
            '            drTemp = dtSelectedEmp.Select("employeeunkid = " & CInt(drRow(i)("employeeunkid")))
            '        Else
            '            Language.setLanguage(mstrModuleName)
            '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "This Employee has Pending Leave Application Form.You cannot delete this employee."), Me)
            '            Exit For
            '        End If

            '    End If

            '    If drTemp IsNot Nothing AndAlso drTemp.Length > 0 Then
            '        drTemp(0).Item("AUD") = "D"
            '        drTemp(0).AcceptChanges()
            '    End If

            'Next


            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = GvSelectedEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectedEmp"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Selected Employee is compulsory information.Please Check atleast One Selected Employee."), Me)
                Exit Sub
            End If

            Dim drTemp As DataRow() = Nothing
            Dim objLeaveForm As New clsleaveform

            For i As Integer = 0 To gRow.Count - 1

                If CInt(GvSelectedEmployee.DataKeys(gRow(i).DataItemIndex)("leaveapprovertranunkid")) <= 0 Then
                    drTemp = dtSelectedEmp.Select("GUID = '" & GvSelectedEmployee.DataKeys(gRow(i).DataItemIndex)("GUID").ToString() & "'")
                Else


                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'If objLeaveForm.GetApproverPendingLeaveFormCount(objLeaveApprover._Approverunkid, GvSelectedEmployee.DataKeys(gRow(i).DataItemIndex)("employeeunkid").ToString()) <= 0 Then
                    If objLeaveForm.GetApproverPendingLeaveFormCount(CInt(Session("ApproverId")), GvSelectedEmployee.DataKeys(gRow(i).DataItemIndex)("employeeunkid").ToString()) <= 0 Then
                        'Pinkal (11-Sep-2020) -- End
                        drTemp = dtSelectedEmp.Select("employeeunkid = " & CInt(GvSelectedEmployee.DataKeys(gRow(i).DataItemIndex)("employeeunkid")))
                    Else
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "This Employee has Pending Leave Application Form.You cannot delete this employee."), Me)
                        Exit For
                    End If

                End If

                If drTemp IsNot Nothing AndAlso drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    drTemp(0).AcceptChanges()
                End If

            Next
            'Pinkal (28-Apr-2020) -- End

            Me.ViewState("SelectedEmployee") = dtSelectedEmp.Copy()

            SelectedEmplyeeList()


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            If dtSelectedEmp IsNot Nothing Then dtSelectedEmp.Clear()
            dtSelectedEmp = Nothing
            objLeaveForm = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnDelete_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveApprover As New clsleaveapprover_master
        'Pinkal (11-Sep-2020) -- End

        Try
            If Validation() = False Then Exit Sub

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)
            Dim mdtTran As DataTable = Nothing
            If Me.ViewState("SelectedEmployee") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("SelectedEmployee"), DataTable).Copy()
            End If
            'Pinkal (11-Sep-2020) -- End


            Dim count As Integer = CInt(mdtTran.Compute("Count(employeeunkid)", "AUD <> 'D'"))
            If mdtTran.Rows.Count = 0 Or count = 0 Or GvSelectedEmployee.Rows.Count <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee."), Me)
                Exit Sub
            End If


            If CBool(Session("LeaveApproverForLeaveType")) AndAlso CBool(Session("AllowtoMapLeaveType")) = True AndAlso Me.ViewState("LeaveTypeMapping") Is Nothing Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Please map leave type for this approver."), Me)
                Exit Sub
            End If

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmLeaveApprover_AddEdit"
            'StrModuleName2 = "mnuLeaveInformation"
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            If Session("ApproverId") IsNot Nothing AndAlso CInt(Session("ApproverId")) > 0 Then
                objLeaveApprover._Approverunkid = CInt(Session("ApproverId"))
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'SetValue()
                SetValue(objLeaveApprover)
                'Pinkal (11-Sep-2020) -- End
                blnFlag = objLeaveApprover.Update(mdtTran)
            Else
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'SetValue()
                SetValue(objLeaveApprover)
                'Pinkal (11-Sep-2020) -- End
                blnFlag = objLeaveApprover.Insert(mdtTran)
            End If

            If blnFlag = False And objLeaveApprover._Message <> "" Then
                DisplayMessage.DisplayMessage("btnSave_Click:- " & objLeaveApprover._Message, Me)
            End If

            If blnFlag Then
                Session("EmpList") = Nothing
                Dim dtTable As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)
                Me.ViewState("SelectedEmployee") = dtTable.Clone
                dtTable = Nothing
                Me.ViewState("LeaveTypeMapping") = Nothing
                drpApprover.SelectedIndex = 0
                drpLevel.SelectedIndex = 0

                'Pinkal (28-Apr-2020) -- Start
                'Optimization  - Working on Process Optimization and performance for require module.	
                'txtSearch.Text = ""
                chkExternalApprover.Checked = False

                If Session("ApproverId") Is Nothing OrElse CInt(Session("ApproverId")) <= 0 Then
                    Call chkExternalApprover_CheckedChanged(New Object, New EventArgs)
                End If
                'Pinkal (28-Apr-2020) -- End

                GvEmployee.DataSource = Nothing
                GvEmployee.DataBind()
                GvSelectedEmployee.DataSource = Nothing
                GvSelectedEmployee.DataBind()
                If Session("ApproverId") IsNot Nothing Then
                    Session("ApproverId") = Nothing
                    Response.Redirect("~/Leave/wPg_LeaveApproverList.aspx", False)
                End If
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (11-Sep-2020) -- End`


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveApprover = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("EmpList") = Nothing
            Session("ApproverId") = Nothing
            Me.ViewState("SelectedEmployee") = Nothing
            Me.ViewState("FirstRecordNo") = Nothing
            Me.ViewState("LastRecordNo") = Nothing
            Me.ViewState("FirstSelectedEmpRNo") = Nothing
            Me.ViewState("LastSelectedEmpRNo") = Nothing
            Response.Redirect("~\Leave\wPg_LeaveApproverList.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Protected Sub lnkLeaveTypeMapping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMapLeaveType.Click
        Try

            If CInt(drpApprover.SelectedValue) <= 0 Or CInt(drpLevel.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Name or Level to do futher operation on it."), Me)
                Exit Sub
            End If
            LblApproverLeaveMapVal.Text = drpApprover.SelectedItem.Text.ToString()
            LblLevelLeaveMapVal.Text = drpLevel.SelectedItem.Text.ToString()

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'GvLeaveTypeMapping.PageIndex = 0
            'SHANI [01 FEB 2015]--END
            FillLeaveType()
            popupLeaveMapping.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkLeaveTypeMapping_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Leave Type Mapping"

#Region "Private Methods"

    Private Sub FillLeaveType()
        Dim dsFill As DataSet = Nothing
        Try
            Dim objLeaveTypeMapping As New clsapprover_leavetype_mapping
            Dim objLeaveType As New clsleavetype_master

            If Me.ViewState("LeaveTypeMapping") Is Nothing Then
                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                'dsFill = objLeaveType.GetList("List", True, True)
                dsFill = objLeaveType.GetList("List", True, True, "ISNULL(skipapproverflow,0) = 0")
                'Pinkal (01-Oct-2018) -- End
            Else
                Dim dsLeaveType As DataSet = Nothing
                dsLeaveType = New DataSet
                dsLeaveType.Tables.Add(CType(Me.ViewState("LeaveTypeMapping"), DataTable))
                dsFill = dsLeaveType
            End If


            For Each dr As DataRow In dsFill.Tables(0).Rows
                Dim drRow As DataRow = objLeaveTypeMapping._dtLeaveType.NewRow()
                drRow("leavetypemappingunkid") = objLeaveTypeMapping.GetLeaveTypeMappingUnkId(CInt(Session("ApproverId")), CInt(dr("leavetypeunkid")))
                If Session("ApproverId") IsNot Nothing AndAlso CInt(Session("ApproverId")) > 0 Then
                    drRow("ischecked") = objLeaveTypeMapping.isExist(CInt(Session("ApproverId")), CInt(dr("leavetypeunkid")))
                Else
                    If dsFill.Tables(0).Columns.Contains("ischecked") Then
                        drRow("ischecked") = dr("ischecked")
                    Else
                        drRow("ischecked") = False
                    End If
                End If

                drRow("approverunkid") = CInt(Session("ApproverId"))
                drRow("leavetypeunkid") = dr("leavetypeunkid").ToString()
                drRow("leavetypecode") = dr("leavetypecode").ToString()
                drRow("leavename") = dr("leavename").ToString()
                drRow("ispaid") = dr("ispaid").ToString()
                objLeaveTypeMapping._dtLeaveType.Rows.Add(drRow)
            Next

            Me.ViewState.Add("LeaveTypeMapping", objLeaveTypeMapping._dtLeaveType)

            GvLeaveTypeMapping.DataSource = objLeaveTypeMapping._dtLeaveType
            GvLeaveTypeMapping.DataBind()
            SetCheckBox()


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveTypeMapping = Nothing
            objLeaveType = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillLeaveType:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsFill IsNot Nothing Then dsFill.Clear()
            dsFill = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master
    'Private Sub GetPageRecordNo()
    '    Try
    '        If GvLeaveTypeMapping.PageIndex > 0 Then
    '            Me.ViewState("FirstRecordNo") = (((GvLeaveTypeMapping.PageIndex + 1) * GvLeaveTypeMapping.Rows.Count) - GvLeaveTypeMapping.Rows.Count)
    '        Else
    '            Me.ViewState("FirstRecordNo") = ((1 * GvLeaveTypeMapping.PageSize) - GvLeaveTypeMapping.Rows.Count)
    '        End If
    '        Me.ViewState("LastRecordNo") = ((GvLeaveTypeMapping.PageIndex + 1) * GvLeaveTypeMapping.Rows.Count)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("GetPageRecordNo:- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Shani(17-Aug-2015) -- End

    Private Sub SetCheckBox()
        Try

            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            'GetPageRecordNo()
            'Shani(17-Aug-2015) -- End
            If GvLeaveTypeMapping.Rows.Count <= 0 Then Exit Sub


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim dtLeaveTypeMapping As DataTable = CType(Me.ViewState("LeaveTypeMapping"), DataTable)
            Dim dtLeaveTypeMapping As DataTable = Nothing

            If Me.ViewState("LeaveTypeMapping") IsNot Nothing Then
                dtLeaveTypeMapping = CType(Me.ViewState("LeaveTypeMapping"), DataTable).Copy()
            End If
            'Pinkal (11-Sep-2020) -- End



            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.

            'If Me.ViewState("FirstRecordNo") IsNot Nothing AndAlso Me.ViewState("LastRecordNo") IsNot Nothing Then
            'Dim mintFirstRecord As Integer = Me.ViewState("FirstRecordNo")
            'Dim mintLastRecord As Integer = Me.ViewState("LastRecordNo")
            Dim mintrowindex As Integer = 0
            'Pinkal (06-Jan-2016) -- End


            'If mintFirstRecord = 0 Then mintLastRecord = mintLastRecord - 1


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.

            'For i As Integer = mintFirstRecord To mintLastRecord
            '    If dtLeaveTypeMapping.Rows.Count < i Then Exit For
            '    Dim drRow As DataRow() = dtLeaveTypeMapping.Select("leavetypecode = '" & GvLeaveTypeMapping.Rows(mintrowindex).Cells(1).Text.Trim & "' AND ischecked = true")
            '    If drRow.Length > 0 Then
            '        Dim gvRow As GridViewRow = GvLeaveTypeMapping.Rows(mintrowindex)
            '        CType(gvRow.FindControl("ChkgvSelectAll"), CheckBox).Checked = True
            '    End If
            '    mintrowindex += 1
            'Next

            ' End  if 
            If dtLeaveTypeMapping Is Nothing Then Exit Sub

            For i As Integer = 0 To dtLeaveTypeMapping.Rows.Count - 1
                If dtLeaveTypeMapping.Rows.Count < i Then Exit For
                Dim drRow As DataRow() = dtLeaveTypeMapping.Select("leavetypecode = '" & GvLeaveTypeMapping.Rows(i).Cells(1).Text.Trim & "' AND ischecked = true")
                If drRow.Length > 0 Then
                    Dim gvRow As GridViewRow = GvLeaveTypeMapping.Rows(mintrowindex)
                    CType(gvRow.FindControl("ChkgvSelectAll"), CheckBox).Checked = True
                End If
                mintrowindex += 1
            Next

            'Pinkal (06-Jan-2016) -- End


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtLeaveTypeMapping IsNot Nothing Then dtLeaveTypeMapping.Clear()
            dtLeaveTypeMapping = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetCheckBox :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Event"


    'Pinkal (28-Apr-2020) -- Start
    'Optimization  - Working on Process Optimization and performance for require module.	

    'Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If GvLeaveTypeMapping.Rows.Count <= 0 Then Exit Sub
    '        Dim dtLeaveTypeMapping As DataTable = CType(Me.ViewState("LeaveTypeMapping"), DataTable)

    '        'SHANI [01 FEB 2015]-START
    '        'Enhancement - REDESIGN SELF SERVICE.

    '        'GetPageRecordNo()
    '        'If Me.ViewState("FirstRecordNo") IsNot Nothing AndAlso Me.ViewState("LastRecordNo") IsNot Nothing Then
    '        If GvLeaveTypeMapping.Rows.Count > 0 Then
    '            'Dim mintFirstRecord As Integer = Me.ViewState("FirstRecordNo")
    '            'Dim mintLastRecord As Integer = Me.ViewState("LastRecordNo")
    '            'Dim mintrowindex As Integer = 0


    '            'Pinkal (30-Apr-2013) -- Start
    '            'Enhancement : TRA Changes

    '            'Pinkal (30-Nov-2013) -- Start
    '            'Enhancement : Oman Changes

    '            'SHANI [01 FEB 2015]-START
    '            'Enhancement - REDESIGN SELF SERVICE.
    '            'If mintFirstRecord = 0 Then mintLastRecord = mintLastRecord - 1
    '            'SHANI [01 FEB 2015]--END
    '            'Pinkal (30-Nov-2013) -- End


    '            'For i As Integer = mintFirstRecord To mintLastRecord - 1
    '            'For i As Integer = mintFirstRecord To mintLastRecord
    '            For i As Integer = 0 To GvLeaveTypeMapping.Rows.Count - 1
    '                'If dtLeaveTypeMapping.Rows.Count - 1 < i Then Exit For
    '                If dtLeaveTypeMapping.Rows.Count < i Then Exit For
    '                'SHANI [01 FEB 2015]--END
    '                Dim drRow As DataRow() = dtLeaveTypeMapping.Select("leavetypecode = '" & GvLeaveTypeMapping.Rows(i).Cells(1).Text.Trim & "'") 'SHANI [01 FEB 2015]--GvLeaveTypeMapping.Rows(mintrowindex).Cells(1).Text.Trim
    '                If drRow.Length > 0 Then
    '                    drRow(0)("ischecked") = cb.Checked
    '                    Dim gvRow As GridViewRow = GvLeaveTypeMapping.Rows(i) 'SHANI [01 FEB 2015]--GvLeaveTypeMapping.Rows(mintrowindex)
    '                    CType(gvRow.FindControl("ChkgvSelectAll"), CheckBox).Checked = cb.Checked
    '                End If
    '                dtLeaveTypeMapping.AcceptChanges()

    '                'SHANI [01 FEB 2015]-START
    '                'Enhancement - REDESIGN SELF SERVICE.
    '                'mintrowindex += 1
    '                'SHANI [01 FEB 2015]--END
    '            Next

    '            'Pinkal (30-Apr-2013) -- End

    '            Me.ViewState("LeaveTypeMapping") = dtLeaveTypeMapping
    '            popupLeaveMapping.Show()
    '        End If

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub ChkgvSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
    '        If gvRow.Cells.Count > 0 Then
    '            Dim drRow() As DataRow = CType(Me.ViewState("LeaveTypeMapping"), DataTable).Select("leavetypecode = '" & gvRow.Cells(1).Text & "'")
    '            If drRow.Length > 0 Then
    '                drRow(0)("ischecked") = cb.Checked
    '                drRow(0).AcceptChanges()
    '            End If
    '            popupLeaveMapping.Show()
    '        End If

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("ChkgvSelect_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Pinkal (28-Apr-2020) -- End

#End Region

#Region " GridView's Event(s) "

    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master
    'Protected Sub GvLeaveTypeMapping_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvLeaveTypeMapping.PageIndexChanging
    '    Try
    '        GvLeaveTypeMapping.PageIndex = e.NewPageIndex
    '        GvLeaveTypeMapping.DataSource = CType(Me.ViewState("LeaveTypeMapping"), DataTable)
    '        GvLeaveTypeMapping.DataBind()
    '        SetCheckBox()
    '        popupLeaveMapping.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("GvLeaveTypeMapping_PageIndexChanging :- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Shani(17-Aug-2015) -- End
#End Region

#Region "Button's Event(s)"

    Protected Sub btnLeaveMapSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeaveMapSave.Click
        Try

            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim dtLeaveTypeMapping As DataTable = CType(Me.ViewState("LeaveTypeMapping"), DataTable)
            Dim dtLeaveTypeMapping As DataTable = Nothing

            If Me.ViewState("LeaveTypeMapping") IsNot Nothing Then
                dtLeaveTypeMapping = CType(Me.ViewState("LeaveTypeMapping"), DataTable).Copy()
            End If
            'Pinkal (11-Sep-2020) -- End


            'Dim drRow As DataRow() = dtLeaveTypeMapping.Select("ischecked = true")
            'If drRow.Length <= 0 Then
            '    DisplayMessage.DisplayMessage("Leave Type is compulsory information.Please Select one Leave Type.", Me)
            '    popupLeaveMapping.Show()
            '    Exit Sub
            'End If

            Dim gchekcedRow As IEnumerable(Of GridViewRow) = Nothing
            gchekcedRow = GvLeaveTypeMapping.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAll"), CheckBox).Checked = True)

            If gchekcedRow Is Nothing OrElse gchekcedRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Leave Type is compulsory information.Please Select one Leave Type.", Me)
                popupLeaveMapping.Show()
                Exit Sub
            End If

            For i As Integer = 0 To gchekcedRow.Count - 1
                Dim drRow() As DataRow = dtLeaveTypeMapping.Select("leavetypeunkid = " & CInt(GvLeaveTypeMapping.DataKeys(gchekcedRow(i).DataItemIndex)("leavetypeunkid")))
                If drRow.Length > 0 Then
                    drRow(0)("ischecked") = True
                    drRow(0).AcceptChanges()
                End If
            Next

            Dim gunchekcedRow As IEnumerable(Of GridViewRow) = GvLeaveTypeMapping.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelectAll"), CheckBox).Checked = False)
            If gunchekcedRow IsNot Nothing AndAlso gunchekcedRow.Count > 0 Then
                For i As Integer = 0 To gunchekcedRow.Count - 1
                    Dim drRow() As DataRow = dtLeaveTypeMapping.Select("leavetypeunkid = " & CInt(GvLeaveTypeMapping.DataKeys(gunchekcedRow(i).DataItemIndex)("leavetypeunkid")))
                    If drRow.Length > 0 Then
                        drRow(0)("ischecked") = False
                        drRow(0).AcceptChanges()
                    End If
                Next
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.ViewState("LeaveTypeMapping") = dtLeaveTypeMapping
            Me.ViewState("LeaveTypeMapping") = dtLeaveTypeMapping.Copy()

            If gchekcedRow IsNot Nothing Then gchekcedRow.ToList.Clear()
            gchekcedRow = Nothing

            If gunchekcedRow IsNot Nothing Then gunchekcedRow.ToList.Clear()
            gunchekcedRow = Nothing

            If dtLeaveTypeMapping IsNot Nothing Then dtLeaveTypeMapping.Clear()
            dtLeaveTypeMapping = Nothing

            'Pinkal (11-Sep-2020) -- End



            'Pinkal (28-Apr-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnLeaveMapSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnLeaveMapClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeaveMapClose.Click
        Try
            If Session("ApproverId") Is Nothing Then
                Me.ViewState("LeaveTypeMapping") = Nothing
            End If
            popupLeaveMapping.Hide()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnLeaveMapClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


#End Region

#Region "ImageButton Events"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Session("ApproverId") = Nothing
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '    End Try

    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region

#Region " Checkbox Event(s) "

    Protected Sub chkExternalApprover_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkExternalApprover.CheckedChanged
        Try
            LblUser.Visible = Not chkExternalApprover.Checked
            drpUser.Visible = Not chkExternalApprover.Checked
            If chkExternalApprover.Checked = False Then
                drpUser.SelectedValue = "0"
            End If
            Call FillApprover()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkExternalApprover_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'Pinkal (12-Oct-2020) -- Start
    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

    Protected Sub chkIncludeInactiveEmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactiveEmp.CheckedChanged
        Dim objLeaveApprover As New clsleaveapprover_master
        Try
            If CInt(drpApprover.SelectedValue) <= 0 Then
                FillApprover()
            Else
                Dim mstrEmployeeID As String = objLeaveApprover.GetApproverEmployeeId(CInt(drpApprover.SelectedValue), chkExternalApprover.Checked)
                FillEmployee(mstrEmployeeID)
            End If

            If Session("ApproverId") IsNot Nothing AndAlso CInt(Session("ApproverId")) > 0 Then
                Dim objLeaveApproverTran As New clsleaveapprover_Tran
                objLeaveApprover._Approverunkid = CInt(Session("ApproverId"))
                objLeaveApproverTran._Approverunkid = objLeaveApprover._Approverunkid
                objLeaveApproverTran.GetApproverTran(CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, objLeaveApprover._Approverunkid, objLeaveApprover._Departmentunkid, objLeaveApprover._Sectionunkid, objLeaveApprover._Jobunkid, chkIncludeInactiveEmp.Checked)
                Me.ViewState.Add("SelectedEmployee", objLeaveApproverTran._DataList)

                SelectedEmplyeeList()

                objLeaveApproverTran = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objLeaveApprover = Nothing
        End Try
    End Sub

    'Pinkal (12-Oct-2020) -- End

#End Region


    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)
            'SHANI [01 FEB 2015]--END

            Me.lblCaption.Text = Language._Object.getCaption(mstrModuleName, Me.lblCaption.Text)

            Me.lblApproverName.Text = Language._Object.getCaption(Me.lblApproverName.ID, Me.lblApproverName.Text)
            Me.lblApproveLevel.Text = Language._Object.getCaption(Me.lblApproveLevel.ID, Me.lblApproveLevel.Text)
            Me.LblUser.Text = Language._Object.getCaption(Me.LblUser.ID, Me.LblUser.Text)
            Me.lnkMapLeaveType.Text = Language._Object.getCaption(Me.lnkMapLeaveType.ID, Me.lnkMapLeaveType.Text)

            'S.SANDEEP [30 JAN 2016] -- START
            Me.chkExternalApprover.Text = Language._Object.getCaption(Me.chkExternalApprover.ID, Me.chkExternalApprover.Text)
            'Me.lblFilterDepartment.Text = Language._Object.getCaption(Me.lblFilterDepartment.ID, Me.lblFilterDepartment.Text)
            'Me.lblFilterjob.Text = Language._Object.getCaption(Me.lblFilterjob.ID, Me.lblFilterjob.Text)
            'Me.lblFilterSection.Text = Language._Object.getCaption(Me.lblFilterSection.ID, Me.lblFilterSection.Text)
            'S.SANDEEP [30 JAN 2016] -- END


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            chkIncludeInactiveEmp.Text = Language._Object.getCaption(chkIncludeInactiveEmp.ID, chkIncludeInactiveEmp.Text)
            'Pinkal (12-Oct-2020) -- End


            Me.GvEmployee.Columns(1).HeaderText = Language._Object.getCaption(Me.GvEmployee.Columns(1).FooterText, Me.GvEmployee.Columns(1).HeaderText)
            Me.GvEmployee.Columns(2).HeaderText = Language._Object.getCaption(Me.GvEmployee.Columns(2).FooterText, Me.GvEmployee.Columns(2).HeaderText)

            Me.GvSelectedEmployee.Columns(1).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(1).FooterText, Me.GvSelectedEmployee.Columns(1).HeaderText)
            Me.GvSelectedEmployee.Columns(2).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(2).FooterText, Me.GvSelectedEmployee.Columns(2).HeaderText)
            Me.GvSelectedEmployee.Columns(3).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(3).FooterText, Me.GvSelectedEmployee.Columns(3).HeaderText)
            Me.GvSelectedEmployee.Columns(4).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(4).FooterText, Me.GvSelectedEmployee.Columns(4).HeaderText)
            Me.GvSelectedEmployee.Columns(5).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(5).FooterText, Me.GvSelectedEmployee.Columns(5).HeaderText)

            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.ID, Me.btnDelete.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub



End Class
