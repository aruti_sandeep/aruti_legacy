﻿namespace ExcelWriter
{
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Xml;

    public sealed class PTLineItemCollection : CollectionBase, IWriter
    {
        internal PTLineItemCollection()
        {
        }

        public int Add(PTLineItem pTLineItem)
        {
            return base.InnerList.Add(pTLineItem);
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "PTLineItems", "urn:schemas-microsoft-com:office:excel");
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public bool Contains(PTLineItem item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(PTLineItem[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(PTLineItem item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, PTLineItem item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(PTLineItem item)
        {
            base.InnerList.Remove(item);
        }

        public PTLineItem this[int index]
        {
            get
            {
                return (PTLineItem) base.InnerList[index];
            }
        }
    }
}

