﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Collections;
    using System.Reflection;
    using System.Xml;

    public sealed class NumberCollection : CollectionBase, IWriter, ICodeWriter
    {
        internal NumberCollection()
        {
        }

        public int Add(string item)
        {
            return base.InnerList.Add(item);
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                string str = this[i];
                CodeMethodInvokeExpression expression = new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(targetObject, "Numbers"), "Add", new CodeExpression[] { new CodePrimitiveExpression(str) });
                method.Statements.Add(expression);
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                string str = this[i];
                writer.WriteElementString("Number", "urn:schemas-microsoft-com:office:excel", str);
            }
        }

        public bool Contains(string link)
        {
            return base.InnerList.Contains(link);
        }

        public void CopyTo(string[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(string item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, string item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(string item)
        {
            base.InnerList.Remove(item);
        }

        public string this[int index]
        {
            get
            {
                return (string) base.InnerList[index];
            }
        }
    }
}

