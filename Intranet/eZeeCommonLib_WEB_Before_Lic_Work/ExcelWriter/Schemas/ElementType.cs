﻿namespace ExcelWriter.Schemas
{
    using ExcelWriter;
    using System;
    using System.Globalization;
    using System.Xml;

    public sealed class ElementType : SchemaType, IWriter
    {
        private AttributeCollection _attributes;
        private SchemaContent _content;
        private string _name;

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "ElementType", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
            if (this._name != null)
            {
                writer.WriteAttributeString("s", "name", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882", this._name);
            }
            if (this._content != SchemaContent.NotSet)
            {
                writer.WriteAttributeString("s", "content", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882", this._content.ToString());
            }
            if (this._attributes != null)
            {
                ((IWriter) this._attributes).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public AttributeCollection Attributes
        {
            get
            {
                if (this._attributes == null)
                {
                    this._attributes = new AttributeCollection();
                }
                return this._attributes;
            }
        }

        public SchemaContent Content
        {
            get
            {
                return this._content;
            }
            set
            {
                this._content = value;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }
    }
}

