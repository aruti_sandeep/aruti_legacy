﻿Imports System.Data.SqlClient


Public Class UserCredentials
    Inherits System.Web.Services.Protocols.SoapHeader

    'Private strUserName As String
    Private strUserPassword As String
    Private intWebClientID As Integer
    Private strCompCode As String
    Private strAuthenticationCode As String

    'Public Property UserName() As String
    '    Get
    '        Return strUserName
    '    End Get
    '    Set(ByVal value As String)
    '        strUserName = value
    '    End Set
    'End Property

    Public Property Password() As String
        Get
            Return strUserPassword
        End Get
        Set(ByVal value As String)
            strUserPassword = value
        End Set
    End Property

    Public Property WebClientID() As Integer
        Get
            Return intWebClientID
        End Get
        Set(ByVal value As Integer)
            intWebClientID = value
        End Set
    End Property

    Public Property CompCode() As String
        Get
            Return strCompCode
        End Get
        Set(ByVal value As String)
            strCompCode = value
        End Set
    End Property

    Public Property AuthenticationCode() As String
        Get
            Return strAuthenticationCode
        End Get
        Set(ByVal value As String)
            strAuthenticationCode = value
        End Set
    End Property


    Public Function IsUserValid() As Boolean
        'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
        Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()
        Dim dsList As New DataSet
        Dim strQ As String = ""

        strQ = "SELECT * FROM cfcompany_info Where companyunkid = " & intWebClientID & " AND company_code = '" & strCompCode & "' AND authentication_code = '" & strAuthenticationCode & "'"

        'If objDataoperation.RecordCount(strQ) <= 0 OrElse strUserPassword <> clsSecurity.Decrypt(ConfigurationManager.AppSettings("password").ToString, "ezee") Then
        '    Return False
        'Else
        '    Return True
        'End If
        Using con As New SqlConnection(strConn)

            con.Open()

            Using da As New SqlDataAdapter()
                Using cmd As New SqlCommand(strQ, con)

                    cmd.CommandType = CommandType.Text
                    cmd.CommandTimeout = 0
                    da.SelectCommand = cmd
                    da.Fill(dsList)

                End Using
            End Using
        End Using

        'If dsList.Tables(0).Rows.Count <= 0 OrElse strUserPassword <> clsSecurity.Decrypt(ConfigurationManager.AppSettings("password").ToString, "ezee") Then
        If dsList.Tables(0).Rows.Count <= 0 OrElse strUserPassword <> "CNjh9qOZUGhuqVpVp/LjZoYbJJoNgPiyC/P/vxPGfWo=" Then
            Return False
        Else
            Return True
        End If

    End Function

End Class
