﻿Imports System.Runtime.CompilerServices
Imports System.Security.Cryptography

Module modGlobal

    <Extension()>
    Public Function ReplaceQuotes(ByVal str As String)
        Try
            str = str.Replace("'", "''").Replace("’", "''")
        Catch ex As Exception

        End Try
        Return str
    End Function

End Module



'Pinkal (12-Feb-2018) -- Start
'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.

Public Class clsCrypto
    Private Shared strKey As String = "@%^"

    Public Shared Function Encrypt(ByVal strText As String) As String
        Dim Crypt As New MD5CryptoServiceProvider
        Dim b() As Byte = Encoding.UTF8.GetBytes(strText)
        Dim key() As Byte

        Try
            key = Crypt.ComputeHash(Encoding.UTF8.GetBytes(strKey))
            Crypt.Clear()

            Dim t As New TripleDESCryptoServiceProvider
            t.Key = key
            t.Mode = CipherMode.ECB
            t.Padding = PaddingMode.PKCS7

            Dim it As ICryptoTransform = t.CreateEncryptor()
            Dim rslt() As Byte = it.TransformFinalBlock(b, 0, b.Length)
            t.Clear()

            Return Convert.ToBase64String(rslt, Base64FormattingOptions.None, rslt.Length)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Shared Function Dicrypt(ByVal strEncodedText As String) As String
        Dim Crypt As New MD5CryptoServiceProvider
        Dim b() As Byte = Convert.FromBase64String(strEncodedText)
        Dim key() As Byte

        Try

            key = Crypt.ComputeHash(Encoding.UTF8.GetBytes(strKey))
            Crypt.Clear()

            Dim t As New TripleDESCryptoServiceProvider
            t.Key = key
            t.Mode = CipherMode.ECB
            t.Padding = PaddingMode.PKCS7

            Dim it As ICryptoTransform = t.CreateDecryptor()
            Dim rslt() As Byte = it.TransformFinalBlock(b, 0, b.Length)
            t.Clear()

            Return Encoding.UTF8.GetString(rslt)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class

'Pinkal (12-Feb-2018) -- End

