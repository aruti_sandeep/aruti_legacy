﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.IO
'Imports eZeeCommonLib
Imports Microsoft.Exchange.WebServices.Data

Imports System.Net.Mail
Imports System.Web.Hosting
Imports System.Data.SqlClient
Imports Aruti_Online_Recruitment
Imports Newtonsoft.Json


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="https://www.arutihr.com/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class ArutiRecruitmentService
    Inherits System.Web.Services.WebService

    Public objUser As UserCredentials
    Private m_strLogFile As String = IO.Path.Combine(Server.MapPath("~") & "/UploadImage", "ApplicantServiceError_LOG.txt")

#Region " GET Methods "

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function ValidateUser(username As String, password As String) As String
        Dim strErrorMessage As String
        Try
            strErrorMessage = ""
            'If objUser.IsUserValid() = False Then
            '    'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
            '    strErrorMessage = "Cannot autheticate user. Please contact Aruti Support Team. Procedure : GetApplicants"
            '    Return Nothing
            '    Exit Function
            'End If
            If Membership.GetUser(username) Is Nothing Then
                Return "Sorry, This Email does not exist!"
            End If

            If Membership.GetUser(username).IsLockedOut = True Then
                Dim dtLastLockOut As Date = Membership.GetUser(username).LastLockoutDate
                Dim dtUnlockDate As Date = dtLastLockOut.AddMinutes(10)
                'Dim dtUnlockDate As Date = dtLastLockOut.AddHours(3)

                If DateTime.Now > dtUnlockDate Then
                    Membership.GetUser(username).UnlockUser()
                Else
                    If DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) <= 59 Then
                        Return "Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Minute, DateTime.Now, dtUnlockDate) & " minutes."
                    Else
                        Return "Sorry, Your account is locked. You can unlock your account after " & DateDiff(DateInterval.Hour, DateTime.Now, dtUnlockDate) & " hour(s)."
                    End If
                    'e.Cancel = True
                    Exit Try
                End If
            End If

            If Membership.GetUser(username).IsApproved = True Then

                Dim strUserId As String = Membership.GetUser(username).ProviderUserKey.ToString


            Else
                Return "Sorry, This email is not activated. Please activate your email from your mailbox.\n\n Please click on Resend Actication Link to get Activation link again in your Mailbox."

                Exit Try
                '
            End If

            If Membership.ValidateUser(username, password) = False Then
                'Dim objApplicant As New clsApplicant
                'Dim intCount As Integer = objApplicant.GetFailedPasswordAttemptCount(Membership.GetUser(username.Text).ProviderUserKey.ToString)

                'FailureText.Text = "Sorry, Password does not match."
                Return "Sorry, Password does not match. You have maximum " & (Membership.MaxInvalidPasswordAttempts).ToString() & " attempts."
                'e.Cancel = True
                Exit Try
            End If

            Dim profile As UserProfile = UserProfile.GetUserProfile(username)

            Dim result = New With {.UserName = Membership.GetUser(username).UserName,
                .FirstName = profile.FirstName,
                .MiddleName = profile.MiddleName,
                .LastName = profile.LastName,
                .Gender = profile.Gender,
                .MobileNo = profile.MobileNo,
                .BirthDate = profile.BirthDate,
                .Password = "",
                .Role = "",
                .Token = ""
                }
            Return JsonConvert.SerializeObject(result)

        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function GetApplicants(ByRef strErrorMessage As String, ByVal blnOnlyPendingApplicants As Boolean, ByVal intApplicantID As Integer, ByVal strApplicantUnkIDs As String) As DataSet
        Dim strQ As String = ""
        Dim strTopApplicantQuery As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot autheticate user. Please contact Aruti Support Team. Procedure : GetApplicants"
                Return Nothing
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If CInt(ConfigurationManager.AppSettings("maxapplicantsforimport").ToString) > 0 Then
                strTopApplicantQuery = " TOP " & CInt(ConfigurationManager.AppSettings("maxapplicantsforimport").ToString) + 1 & " "
            End If

            strQ = "SELECT DISTINCT  " & strTopApplicantQuery & " rcapplicant_master.applicantunkid  " &
                                  ", rcapplicant_master.applicant_code " &
                                  ", rcapplicant_master.titleunkid " &
                                  ", rcapplicant_master.firstname " &
                                  ", rcapplicant_master.surname " &
                                  ", rcapplicant_master.othername " &
                                  ", rcapplicant_master.gender " &
                                  ", rcapplicant_master.email " &
                                  ", rcapplicant_master.present_address1 " &
                                  ", rcapplicant_master.present_address2 " &
                                  ", rcapplicant_master.present_countryunkid " &
                                  ", rcapplicant_master.present_stateunkid " &
                                  ", rcapplicant_master.present_province " &
                                  ", rcapplicant_master.present_post_townunkid " &
                                  ", rcapplicant_master.present_zipcode " &
                                  ", rcapplicant_master.present_road " &
                                  ", rcapplicant_master.present_estate " &
                                  ", rcapplicant_master.present_plotno " &
                                  ", rcapplicant_master.present_mobileno " &
                                  ", rcapplicant_master.present_alternateno " &
                                  ", rcapplicant_master.present_tel_no " &
                                  ", rcapplicant_master.present_fax " &
                                  ", rcapplicant_master.perm_address1 " &
                                  ", rcapplicant_master.perm_address2 " &
                                  ", rcapplicant_master.perm_countryunkid " &
                                  ", rcapplicant_master.perm_stateunkid " &
                                  ", rcapplicant_master.perm_province " &
                                  ", rcapplicant_master.perm_post_townunkid " &
                                  ", rcapplicant_master.perm_zipcode " &
                                  ", rcapplicant_master.perm_road " &
                                  ", rcapplicant_master.perm_estate " &
                                  ", rcapplicant_master.perm_plotno " &
                                  ", rcapplicant_master.perm_mobileno " &
                                  ", rcapplicant_master.perm_alternateno " &
                                  ", rcapplicant_master.perm_tel_no " &
                                  ", rcapplicant_master.perm_fax " &
                                  ", rcapplicant_master.birth_date " &
                                  ", rcapplicant_master.marital_statusunkid " &
                                  ", rcapplicant_master.anniversary_date " &
                                  ", rcapplicant_master.language1unkid " &
                                  ", rcapplicant_master.language2unkid " &
                                  ", rcapplicant_master.language3unkid " &
                                  ", rcapplicant_master.language4unkid " &
                                  ", rcapplicant_master.nationality " &
                                  ", rcapplicant_master.userunkid " &
                                  ", rcapplicant_master.vacancyunkid " &
                                  ", rcapplicant_master.isimport " &
                                  ", rcapplicant_master.other_skill " &
                                  ", rcapplicant_master.other_qualification " &
                                  ", rcapplicant_master.employeecode " &
                                  ", rcapplicant_master.referenceno " &
                                  ", ISNULL(rcapplicant_master.memberships, '') AS memberships " &
                                  ", ISNULL(rcapplicant_master.achievements, '') AS achievements " &
                                  ", ISNULL(rcapplicant_master.journalsresearchpapers, '') AS journalsresearchpapers " &
                                  ", ISNULL(rcapplicant_master.UserId, '') AS UserId " &
                                  ", ISNULL(rcapplicant_master.mother_tongue,'') AS mother_tongue " &
                                  ", rcapplicant_master.isimpaired " &
                                  ", rcapplicant_master.impairment " &
                                  ", ISNULL(rcapplicant_master.current_salary,0) AS current_salary " &
                                  ", ISNULL(rcapplicant_master.expected_salary,0) AS expected_salary " &
                                  ", ISNULL(rcapplicant_master.expected_benefits,'') AS expected_benefits " &
                                  ", rcapplicant_master.willing_to_relocate " &
                                  ", rcapplicant_master.willing_to_travel " &
                                  ", rcapplicant_master.notice_period_days " &
                                  ", rcapplicant_master.current_salary_currency_id " &
                                  ", rcapplicant_master.expected_salary_currency_id " &
                                  ", rcapplicant_master.applicant_photo " &
                                  ", rcapplicant_master.applicant_signature " &
                                  ", rcapplicant_master.nin " &
                                  ", rcapplicant_master.tin " &
                                  ", rcapplicant_master.ninmobile " &
                                  ", rcapplicant_master.village " &
                                  ", rcapplicant_master.phonenum " &
                                  ", rcapplicant_master.residency " &
                                  ", rcapplicant_master.indexno " &
                    "FROM    rcapplicant_master " &
                    "LEFT JOIN rcapp_vacancy_mapping ON rcapp_vacancy_mapping.applicantunkid = rcapplicant_master.applicantunkid " &
                    "WHERE   rcapplicant_master.Comp_Code = '" & objUser.CompCode & "' " &
                            "AND rcapplicant_master.companyunkid = " & objUser.WebClientID & " " &
                            "AND rcapp_vacancy_mapping.isvoid = 0 " &
                            "AND rcapp_vacancy_mapping.Comp_Code = '" & objUser.CompCode & "' " &
                            "AND rcapp_vacancy_mapping.companyunkid = " & objUser.WebClientID & " " &
                            "AND rcapplicant_master.UserId IS NOT NULL " &
                            "/*AND ISNULL(rcapplicant_master.referenceno, '') <> '' */"
            'S.SANDEEP |04-MAY-2023| -- START {A1X-833} [applicant_photo,applicant_signature,nin,tin,ninmobile,village,phonenum] -- END

            'Hemant (09 July 2018) -- [current_salary_currency_id, expected_salary_currency_id]

            'Nilay (13 Apr 2017) -- [mother_tongue, isimpaired, impairment, current_salary, expected_salary, expected_benefits, willing_to_relocate, willing_to_travel, notice_period_days]

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(rcapplicant_master.Syncdatetime, ' ') = ' ' "
            End If

            If intApplicantID > 0 Then
                strQ &= "AND rcapplicant_master.applicantunkid = " & intApplicantID & " "
            End If

            If strApplicantUnkIDs.Trim.Length > 0 Then
                strQ &= "AND rcapplicant_master.applicantunkid IN (" & strApplicantUnkIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : GetApplicants"
            '    Return Nothing
            '    Exit Function
            'End If
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsList)
            'Sohail (08 Oct 2018) -- End
            'System.IO.File.AppendAllText(m_strLogFile, "GetApplicants... : " & dsList.Tables(0).Rows.Count.ToString & vbCrLf)
            'System.IO.File.AppendAllText(m_strLogFile, dsList.GetXml().ToString & vbCrLf & vbCrLf)

            If dsList.Tables(0).Rows.Count > CInt(ConfigurationManager.AppSettings("maxapplicantsforimport").ToString) Then
                Dim lstRows As List(Of DataRow) = (From p In dsList.Tables(0).AsEnumerable() Select (p)).Take(CInt(ConfigurationManager.AppSettings("maxapplicantsforimport").ToString)).ToList
                Dim dt As DataTable = lstRows.CopyToDataTable()
                dt.Columns.Add("noerror", System.Type.GetType("System.Int32")).DefaultValue = 0
                dsList.Tables.Clear()
                dsList.Tables.Add(dt)
            End If

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : GetApplicants"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
        Return dsList
    End Function

    '<WebMethod()>
    '<SoapHeader("objUser", Required:=True)>
    'Public Function GetApplicantsNew(ByRef strErrorMessage As String, ByVal blnOnlyPendingApplicants As Boolean, ByVal intApplicantID As Integer) As String
    '    Dim strQ As String = ""
    '    'Dim dsList As DataSet = Nothing
    '    Dim dsList As New DataSet

    '    Try

    '        strErrorMessage = ""
    '        If objUser.IsUserValid() = False Then
    '            'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
    '            strErrorMessage = "Cannot autheticate user. Please contact Aruti Support Team. Procedure : GetApplicants"
    '            Return Nothing
    '            Exit Function
    '        End If

    '        'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
    '        Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

    '        strQ = "SELECT DISTINCT  rcapplicant_master.applicantunkid  " &
    '                              ", rcapplicant_master.applicant_code " &
    '                              ", rcapplicant_master.titleunkid " &
    '                              ", rcapplicant_master.firstname " &
    '                              ", rcapplicant_master.surname " &
    '                              ", rcapplicant_master.othername " &
    '                              ", rcapplicant_master.gender " &
    '                              ", rcapplicant_master.email " &
    '                              ", rcapplicant_master.present_address1 " &
    '                              ", rcapplicant_master.present_address2 " &
    '                              ", rcapplicant_master.present_countryunkid " &
    '                              ", rcapplicant_master.present_stateunkid " &
    '                              ", rcapplicant_master.present_province " &
    '                              ", rcapplicant_master.present_post_townunkid " &
    '                              ", rcapplicant_master.present_zipcode " &
    '                              ", rcapplicant_master.present_road " &
    '                              ", rcapplicant_master.present_estate " &
    '                              ", rcapplicant_master.present_plotno " &
    '                              ", rcapplicant_master.present_mobileno " &
    '                              ", rcapplicant_master.present_alternateno " &
    '                              ", rcapplicant_master.present_tel_no " &
    '                              ", rcapplicant_master.present_fax " &
    '                              ", rcapplicant_master.perm_address1 " &
    '                              ", rcapplicant_master.perm_address2 " &
    '                              ", rcapplicant_master.perm_countryunkid " &
    '                              ", rcapplicant_master.perm_stateunkid " &
    '                              ", rcapplicant_master.perm_province " &
    '                              ", rcapplicant_master.perm_post_townunkid " &
    '                              ", rcapplicant_master.perm_zipcode " &
    '                              ", rcapplicant_master.perm_road " &
    '                              ", rcapplicant_master.perm_estate " &
    '                              ", rcapplicant_master.perm_plotno " &
    '                              ", rcapplicant_master.perm_mobileno " &
    '                              ", rcapplicant_master.perm_alternateno " &
    '                              ", rcapplicant_master.perm_tel_no " &
    '                              ", rcapplicant_master.perm_fax " &
    '                              ", rcapplicant_master.birth_date " &
    '                              ", rcapplicant_master.marital_statusunkid " &
    '                              ", rcapplicant_master.anniversary_date " &
    '                              ", rcapplicant_master.language1unkid " &
    '                              ", rcapplicant_master.language2unkid " &
    '                              ", rcapplicant_master.language3unkid " &
    '                              ", rcapplicant_master.language4unkid " &
    '                              ", rcapplicant_master.nationality " &
    '                              ", rcapplicant_master.userunkid " &
    '                              ", rcapplicant_master.vacancyunkid " &
    '                              ", rcapplicant_master.isimport " &
    '                              ", rcapplicant_master.other_skill " &
    '                              ", rcapplicant_master.other_qualification " &
    '                              ", rcapplicant_master.employeecode " &
    '                              ", rcapplicant_master.referenceno " &
    '                              ", ISNULL(rcapplicant_master.memberships, '') AS memberships " &
    '                              ", ISNULL(rcapplicant_master.achievements, '') AS achievements " &
    '                              ", ISNULL(rcapplicant_master.journalsresearchpapers, '') AS journalsresearchpapers " &
    '                              ", ISNULL(rcapplicant_master.UserId, '') AS UserId " &
    '                              ", ISNULL(rcapplicant_master.mother_tongue,'') AS mother_tongue " &
    '                              ", rcapplicant_master.isimpaired " &
    '                              ", rcapplicant_master.impairment " &
    '                              ", ISNULL(rcapplicant_master.current_salary,0) AS current_salary " &
    '                              ", ISNULL(rcapplicant_master.expected_salary,0) AS expected_salary " &
    '                              ", ISNULL(rcapplicant_master.expected_benefits,'') AS expected_benefits " &
    '                              ", rcapplicant_master.willing_to_relocate " &
    '                              ", rcapplicant_master.willing_to_travel " &
    '                              ", rcapplicant_master.notice_period_days " &
    '                              ", rcapplicant_master.current_salary_currency_id " &
    '                              ", rcapplicant_master.expected_salary_currency_id " &
    '                "FROM    rcapplicant_master " &
    '                "LEFT JOIN rcapp_vacancy_mapping ON rcapp_vacancy_mapping.applicantunkid = rcapplicant_master.applicantunkid " &
    '                "WHERE   rcapplicant_master.Comp_Code = '" & objUser.CompCode & "' " &
    '                        "AND rcapplicant_master.companyunkid = " & objUser.WebClientID & " " &
    '                        "AND rcapp_vacancy_mapping.isvoid = 0 " &
    '                        "AND rcapp_vacancy_mapping.Comp_Code = '" & objUser.CompCode & "' " &
    '                        "AND rcapp_vacancy_mapping.companyunkid = " & objUser.WebClientID & " " &
    '                        "AND rcapplicant_master.UserId IS NOT NULL " &
    '                        "/*AND ISNULL(rcapplicant_master.referenceno, '') <> '' */"

    '        'Hemant (09 July 2018) -- [current_salary_currency_id, expected_salary_currency_id]

    '        'Nilay (13 Apr 2017) -- [mother_tongue, isimpaired, impairment, current_salary, expected_salary, expected_benefits, willing_to_relocate, willing_to_travel, notice_period_days]

    '        If blnOnlyPendingApplicants = True Then
    '            strQ &= "AND ISNULL(rcapplicant_master.Syncdatetime, ' ') = ' ' "
    '        End If

    '        If intApplicantID > 0 Then
    '            strQ &= "AND rcapplicant_master.applicantunkid = " & intApplicantID & " "
    '        End If

    '        'dsList = objDataoperation.ExecQuery(strQ, "List")

    '        'If objDataoperation.ErrorMessage <> "" Then
    '        '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : GetApplicants"
    '        '    Return Nothing
    '        '    Exit Function
    '        'End If
    '        Using con As New SqlConnection(strConn)

    '            con.Open()

    '            Using da As New SqlDataAdapter()
    '                Using cmd As New SqlCommand(strQ, con)

    '                    cmd.CommandType = CommandType.Text
    '                    da.SelectCommand = cmd
    '                    da.Fill(dsList)

    '                End Using
    '            End Using
    '        End Using

    '        'Sohail (08 Oct 2018) -- Start
    '        'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
    '        Call RemoveDateTimeZones(dsList)
    '        'Sohail (08 Oct 2018) -- End
    '        'System.IO.File.AppendAllText(m_strLogFile, "GetApplicants... : " & dsList.Tables(0).Rows.Count.ToString & vbCrLf)
    '        'System.IO.File.AppendAllText(m_strLogFile, dsList.GetXml().ToString & vbCrLf & vbCrLf)


    '        ' Create a compressed stream in memory to hold the XML schema and data.
    '        Dim memStream As System.IO.MemoryStream = New System.IO.MemoryStream()
    '        Dim compressedStream As System.IO.Compression.DeflateStream = New System.IO.Compression.DeflateStream(memStream, System.IO.Compression.CompressionMode.Compress, True)

    '        '' Let the System.Data.DataSet serialize the XML directly to the compressed stream.
    '        'dsList.WriteXml(compressedStream, XmlWriteMode.WriteSchema)
    '        'compressedStream.Close()
    '        'memStream.Seek(0, System.IO.SeekOrigin.Begin)

    '        '' Return a base64 encoded string representation of the compressed stream.
    '        'Return Convert.ToBase64String(memStream.GetBuffer(), Base64FormattingOptions.None)
    '        'Return memStream.GetBuffer()
    '        'Return compressedStream

    '        Dim inputArray As Byte() = Encoding.UTF8.GetBytes(dsList.GetXml)
    '        compressedStream.Write(inputArray, 0, inputArray.Length)
    '        Dim rslt As String = ""
    '        memStream.Position = 0
    '        Using sr As StreamReader = New StreamReader(memStream)
    '            rslt = sr.ReadToEnd()
    '        End Using
    '        'Dim rslt As String = Convert.ToBase64String.ToArray)
    '        Return rslt

    '    Catch ex As Exception
    '        strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : GetApplicants"
    '        If ex.InnerException IsNot Nothing Then
    '            strErrorMessage &= "; " & ex.InnerException.Message
    '        End If
    '    End Try
    '    'Return ""

    'End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function GetJobHistory(ByRef strErrorMessage As String, ByVal blnOnlyPendingApplicants As Boolean, ByVal intApplicantID As Integer, ByVal strApplicantUnkIDs As String) As DataSet
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : GetJobHistory"
                Return Nothing
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            strQ = "SELECT  applicantunkid  " &
                          ", employername " &
                          ", companyname " &
                          ", designation " &
                          ", responsibility " &
                          ", joiningdate " &
                          ", terminationdate " &
                          ", officephone " &
                          ", leavingreason " &
                          ", ISNULL(achievements, '') AS achievements " &
                          ", jobhistorytranunkid " &
                          ", jobhistorytranunkid AS serverjobhistorytranunkid " &
                          ", '' AS AUD " &
                          ", isvoid " &
                          ", 0 AS voiduserunkid " &
                          ", voiddatetime " &
                          ", voidreason " &
                          ", ISNULL(created_date, '19000101') AS created_date " &
                          ", isgovjob " &
                    "FROM    rcjobhistory " &
                    "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                        "AND companyunkid = " & objUser.WebClientID & " "
            'S.SANDEEP |04-MAY-2023| -- START {A1X-833} [isgovjob] -- END
            'Sohail (26 May 2017) - [jobhistorytranunkid, serverjobhistorytranunkid, AUD, isvoid, voiduserunkid, voiddatetime, voidreason]

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If intApplicantID > 0 Then
                strQ &= "AND applicantunkid = " & intApplicantID & " "
            End If

            If strApplicantUnkIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantUnkIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : GetJobHistory"
            '    Return Nothing
            '    Exit Function
            'End If
            Using con As New SqlConnection(strConn)
                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using

                End Using

            End Using

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsList)
            'Sohail (08 Oct 2018) -- End

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : GetJobHistory"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
        Return dsList
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function GetSkill(ByRef strErrorMessage As String, ByVal blnOnlyPendingApplicants As Boolean, ByVal intApplicantID As Integer, ByVal strApplicantUnkIDs As String) As DataSet
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : GetSkill"
                Return Nothing
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            strQ = "SELECT  applicantunkid  " &
                          ", skillcategoryunkid " &
                          ", skillunkid " &
                          ", remark " &
                          ", other_skillcategory " &
                          ", other_skill " &
                          ", skillexpertiseunkid " &
                          ", skilltranunkid " &
                          ", skilltranunkid AS serverskilltranunkid " &
                          ", '' AS AUD " &
                          ", isvoid " &
                          ", 0 AS voiduserunkid " &
                          ", voiddatetime " &
                          ", voidreason " &
                          ", ISNULL(created_date, '19000101') AS created_date " &
                    "FROM    rcapplicantskill_tran " &
                    "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                        "AND companyunkid = " & objUser.WebClientID & " "
            'Sohail (26 May 2017) - [skilltranunkid, serverskilltranunkid, AUD, isvoid, voiduserunkid, voiddatetime, voidreason]
            'Nilay (13 Apr 2017) -- [skillexpertiseunkid]

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If intApplicantID > 0 Then
                strQ &= "AND applicantunkid = " & intApplicantID & " "
            End If

            If strApplicantUnkIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantUnkIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : GetSkill"
            '    Return Nothing
            '    Exit Function
            'End If
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsList)
            'Sohail (08 Oct 2018) -- End

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : GetSkill"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
        Return dsList
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function GetQualification(ByRef strErrorMessage As String, ByVal blnOnlyPendingApplicants As Boolean, ByVal intApplicantID As Integer, ByVal strApplicantUnkIDs As String) As DataSet
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : GetQualification"
                Return Nothing
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            strQ = "SELECT  applicantunkid  " &
                          ", qualificationgroupunkid " &
                          ", qualificationunkid " &
                          ", transaction_date " &
                          ", reference_no " &
                          ", award_start_date " &
                          ", award_end_date " &
                          ", instituteunkid " &
                          ", remark " &
                          ", resultunkid " &
                          ", gpacode " &
                          ", other_qualificationgrp " &
                          ", other_qualification " &
                          ", other_institute " &
                          ", other_resultcode " &
                          ", ISNULL(certificateno, '') AS certificateno " &
                          ", ishighestqualification " &
                          ", qualificationtranunkid " &
                          ", qualificationtranunkid AS serverqualificationtranunkid " &
                          ", '' AS AUD " &
                          ", isvoid " &
                          ", 0 AS voiduserunkid " &
                          ", voiddatetime " &
                          ", voidreason " &
                          ", ISNULL(created_date, '19000101') AS created_date " &
                    "FROM    rcapplicantqualification_tran " &
                    "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                        "AND companyunkid = " & objUser.WebClientID & " "
            'Sohail (26 May 2017) - [qualificationtranunkid, serverqualificationtranunkid, AUD, isvoid, voiduserunkid, voiddatetime, voidreason]
            'Nilay (13 Apr 2017) -- [ishighestqualification]

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If intApplicantID > 0 Then
                strQ &= "AND applicantunkid = " & intApplicantID & " "
            End If

            If strApplicantUnkIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantUnkIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : GetQualification"
            '    Return Nothing
            '    Exit Function
            'End If
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsList)
            'Sohail (08 Oct 2018) -- End

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : GetQualification"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
        Return dsList
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function GetReference(ByRef strErrorMessage As String, ByVal blnOnlyPendingApplicants As Boolean, ByVal intApplicantID As Integer, ByVal strApplicantUnkIDs As String) As DataSet
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : GetReference"
                Return Nothing
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            strQ = "SELECT  applicantunkid  " &
                          ", name " &
                          ", address " &
                          ", countryunkid " &
                          ", stateunkid " &
                          ", cityunkid " &
                          ", email " &
                          ", gender AS genderunkid " &
                          ", position " &
                          ", telephone_no " &
                          ", mobile_no " &
                          ", relationunkid " &
                          ", referencetranunkid " &
                          ", referencetranunkid AS serverreferencetranunkid " &
                          ", '' AS AUD " &
                          ", isvoid " &
                          ", 0 AS voiduserunkid " &
                          ", voiddatetime " &
                          ", voidreason " &
                          ", ISNULL(created_date, '19000101') AS created_date " &
                    "FROM    rcapp_reference_tran " &
                    "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                        "AND companyunkid = " & objUser.WebClientID & " "
            'Sohail (26 May 2017) - [referencetranunkid, serverreferencetranunkid, AUD, isvoid, voiduserunkid, voiddatetime, voidreason]

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If intApplicantID > 0 Then
                strQ &= "AND applicantunkid = " & intApplicantID & " "
            End If

            If strApplicantUnkIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantUnkIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : GetReference"
            '    Return Nothing
            '    Exit Function
            'End If
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsList)
            'Sohail (08 Oct 2018) -- End

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : GetReference"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
        Return dsList
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function GetVacancy(ByRef strErrorMessage As String, ByVal blnOnlyPendingApplicants As Boolean, ByVal intApplicantID As Integer, ByVal strApplicantUnkIDs As String) As DataSet
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : GetVacancy"
                Return Nothing
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            strQ = "SELECT  applicantunkid  " &
                          ", vacancyunkid " &
                          ", 1 AS userunkid " &
                          ", 1 AS isactive " &
                          ", earliest_possible_startdate " &
                          ", comments " &
                          ", vacancy_found_out_from " &
                          ", CAST(0 AS BIT) AS isimport " &
                          ", 0 AS importuserunkid " &
                          ", CAST(NULL AS Datetime) AS importdatetime " &
                          ", CAST(1 AS BIT) AS isfromonline " &
                          ", appvacancytranunkid " &
                          ", appvacancytranunkid AS serverappvacancytranunkid " &
                          ", ISNULL(vacancy_found_out_from_unkid, 0) AS vacancy_found_out_from_unkid " &
                          ", '' AS AUD " &
                          ", isvoid " &
                          ", 0 AS voiduserunkid " &
                          ", voiddatetime " &
                          ", voidreason " &
                          ", ISNULL(loginemployeeunkid, 0) AS loginemployeeunkid " &
                    "FROM    rcapp_vacancy_mapping " &
                    "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                        "AND companyunkid = " & objUser.WebClientID & " "
            'Sohail (03 Oct 2019) - [vacancy_found_out_from_unkid]
            'Sohail (09 Oct 2018) - [isimport, importuserunkid, importdatetime, isfromonline, appvacancytranunkid, serverappvacancytranunkid, AUD, isvoid, voiduserunkid, voiddatetime, voidreason]
            'Hemant (08 Sept 2018) -- Start
            'Enhancement - Issue : To Stop Importing Deleted Applied Job Vancancy From HRMS Database Server to Local Database Server
            strQ &= " AND isvoid = 0 "
            'Hemant (08 Sept 2018)) -- End
            'Nilay (13 Apr 2017) -- [earliest_possible_startdate, comments, vacancy_found_out_from]

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If intApplicantID > 0 Then
                strQ &= "AND applicantunkid = " & intApplicantID & " "
            End If

            If strApplicantUnkIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantUnkIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : GetVacancy"
            '    Return Nothing
            '    Exit Function
            'End If
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsList)
            'Sohail (08 Oct 2018) -- End

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : GetVacancy"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
        Return dsList
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function GetImages(ByRef strErrorMessage As String, ByVal blnOnlyPendingApplicants As Boolean, ByVal intApplicantID As Integer, ByVal strApplicantUnkIDs As String) As DataSet
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : GetImages"
                Return Nothing
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            strQ = "SELECT  employeeunkid  " &
                          ", imagename " &
                          ", employeeunkid " &
                          ", referenceid " &
                          ", isapplicant " &
                          ", imagetranunkid " &
                    "FROM    hr_images_tran " &
                    "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                            "AND companyunkid = " & objUser.WebClientID & " "

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If intApplicantID > 0 Then
                strQ &= "AND employeeunkid = " & intApplicantID & " "
            End If

            If strApplicantUnkIDs.Trim.Length > 0 Then
                strQ &= "AND employeeunkid IN (" & strApplicantUnkIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : GetImages"
            '    Return Nothing
            '    Exit Function
            'End If
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsList)
            'Sohail (08 Oct 2018) -- End

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : GetImages"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
        Return dsList
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function GetAttachments(ByRef strErrorMessage As String, ByVal blnOnlyPendingApplicants As Boolean, ByVal intApplicantID As Integer, ByVal strApplicantUnkIDs As String) As DataSet
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : GetAttachments"
                Return Nothing
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            strQ = "SELECT  applicantunkid  " &
                          ", documentunkid " &
                          ", modulerefid " &
                          ", attachrefid " &
                          ", filepath " &
                          ", filename " &
                          ", fileuniquename " &
                          ", attached_date " &
                          ", file_size " &
                          ", attachfiletranunkid " &
                          ", attachfiletranunkid AS serverattachfiletranunkid " &
                          ", '' AS AUD " &
                          ", isvoid " &
                          ", 0 AS voiduserunkid " &
                          ", voiddatetime " &
                          ", voidreason " &
                          ", appvacancytranunkid " &
                          ", jobhistorytranunkid " &
                          ", applicantqualiftranunkid " &
                    "FROM    rcattachfiletran " &
                    "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                        "AND companyunkid = " & objUser.WebClientID & " " &
                        "AND (appvacancytranunkid > 0 OR jobhistorytranunkid > 0 OR applicantqualiftranunkid > 0) "

            'Pinkal (30-Sep-2023) -- (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. [", applicantqualiftranunkid " &]

            'S.SANDEEP |04-MAY-2023| -- START {A1X-833} [jobhistorytranunkid] -- END

            'Sohail (26 May 2017) - [attachfiletranunkid, serverattachfiletranunkid, AUD, isvoid, voiduserunkid, voiddatetime, voidreason]

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If intApplicantID > 0 Then
                strQ &= "AND applicantunkid = " & intApplicantID & " "
            End If

            If strApplicantUnkIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantUnkIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : GetAttachments"
            '    Return Nothing
            '    Exit Function
            'End If
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsList)
            'Sohail (08 Oct 2018) -- End

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : GetAttachments"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
        Return dsList
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function DownloadFile(ByVal strFilePath As String, ByRef strErrorMessage As String) As Byte()
        'Public Function DownloadFile(ByVal strFilePath As String, ByRef strErrorMessage As String) As Stream
        'Public Function DownloadFile(ByVal strFolderName As String, ByVal strFileName As String, ByRef strErrorMessage As String) As Byte()
        Dim filebytes As Byte() = Nothing
        'Dim filebytes As Stream = Nothing
        Try
            'System.IO.File.AppendAllText(m_strLogFile, "Downloading... : " & strFilePath & vbCrLf)
            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot autheticate user. Please contact Aruti Support Team. Procedure : DownloadFile"
                Return Nothing
                Exit Function
            End If

            'Dim strFilePath As String = "" 'System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & strFolderName & "/") & strFileName
            'If strFolderName.Trim = "" Then
            '    strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/") & strFileName
            'Else
            '    strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & strFolderName & "/") & strFileName
            'End If
            If File.Exists(strFilePath) = True Then
                'System.IO.File.AppendAllText(m_strLogFile, "File exist : " & strFilePath & vbCrLf)
                filebytes = File.ReadAllBytes(strFilePath)
                'filebytes = File.OpenRead(strFilePath)
                'System.IO.File.AppendAllText(m_strLogFile, "File length : " & filebytes.Length & " : " & strFilePath & vbCrLf)
            Else
                'System.IO.File.AppendAllText(m_strLogFile, "File not found : " & strFilePath & vbCrLf)
                'strErrorMessage = "File not found. Please contact Aruti Support Team. Procedure : DownloadFile"
                strErrorMessage = "File not found. Please contact Aruti Support Team. Procedure : DownloadFile; Path : " & strFilePath
            End If
            'System.IO.File.AppendAllText(m_strLogFile, vbCrLf)

        Catch ex As Exception
            strErrorMessage = "File: " & strFilePath & "; " & ex.Message & "; " & ex.StackTrace & ",  Procedure : DownloadFile"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
        Return filebytes
    End Function

    'Pinkal (12-Feb-2018) -- Start
    'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.

    '<WebMethod()>
    '<SoapHeader("objUser", Required:=True)>
    Public Function GetNotificationDetails(ByRef strErrorMessage As String, ByVal strbody As String, ByVal mstrCompanyName As String _
                                                        , ByVal mstrApplicant As String, ByVal dsNotificationDetail As DataSet _
                                                        , ByVal strExternallink As String, ByVal strSubscribelink As String _
                                                        , intTemplateFound As Boolean _
                                                        , dr As DataRow
                                                        ) As String
        Try
            If dsNotificationDetail IsNot Nothing AndAlso dsNotificationDetail.Tables(0).Rows.Count > 0 Then
                If intTemplateFound = False Then

                    strbody = strbody.Trim.Replace("#ApplicantName", mstrApplicant)
                    strbody = strbody.Trim.Replace("#Vacancy", dsNotificationDetail.Tables(0).Rows(0)("vacancytitle").ToString())
                    strbody = strbody.Trim.Replace("#Company", mstrCompanyName)

                    If dsNotificationDetail.Tables(0).Rows(0)("experience").ToString().Length > 0 Then
                        strbody = strbody.Trim.Replace("#Experience", Math.Round(CDec(CInt(dsNotificationDetail.Tables(0).Rows(0)("experience").ToString()) / 12), 1) & " Year(s).")
                    Else
                        strbody = strbody.Trim.Replace("<b>Experience :</b> #Experience", "")
                    End If

                    If dsNotificationDetail.Tables(0).Rows(0)("noposition").ToString().Length > 0 Then
                        strbody = strbody.Trim.Replace("#Position", dsNotificationDetail.Tables(0).Rows(0)("noposition").ToString())
                    Else
                        strbody = strbody.Trim.Replace("<b> No. of Position :</b>  #Position", "")
                    End If

                    If dsNotificationDetail.Tables(0).Rows(0)("pay_from").ToString().Length > 0 AndAlso dsNotificationDetail.Tables(0).Rows(0)("pay_to").ToString().Length > 0 Then
                        strbody = strbody.Trim.Replace("#Scale", Math.Round(CDec(dsNotificationDetail.Tables(0).Rows(0)("pay_from")), 2) & " - " & Math.Round(CDec(dsNotificationDetail.Tables(0).Rows(0)("pay_to")), 2))
                    Else
                        strbody = strbody.Trim.Replace("<b>Scale :</b> #Scale", "")
                    End If

                    If dsNotificationDetail.Tables(0).Rows(0)("skill").ToString().Length > 0 Then
                        strbody = strbody.Trim.Replace("#Skills", dsNotificationDetail.Tables(0).Rows(0)("skill").ToString())
                    Else
                        strbody = strbody.Trim.Replace("<b>Skill :</b> #Skills", "")
                    End If

                    If IsDBNull(dsNotificationDetail.Tables(0).Rows(0)("openingdate")) = False AndAlso dsNotificationDetail.Tables(0).Rows(0)("openingdate") <> Nothing Then
                        strbody = strbody.Trim.Replace("#OpeningDate", CDate(dsNotificationDetail.Tables(0).Rows(0)("openingdate")).ToString("dd-MMM-yyyy"))
                    Else
                        strbody = strbody.Trim.Replace("<b>Job Opening date :</b> #OpeningDate", "")
                    End If

                    If IsDBNull(dsNotificationDetail.Tables(0).Rows(0)("closingdate")) = False AndAlso dsNotificationDetail.Tables(0).Rows(0)("closingdate") <> Nothing Then
                        strbody = strbody.Trim.Replace("#ClosingDate", CDate(dsNotificationDetail.Tables(0).Rows(0)("closingdate")).ToString("dd-MMM-yyyy"))
                    Else
                        strbody = strbody.Trim.Replace("<b>Job closing date :</b> #ClosingDate", "")
                    End If

                    If IsDBNull(dsNotificationDetail.Tables(0).Rows(0)("remark")) = False AndAlso dsNotificationDetail.Tables(0).Rows(0)("remark") <> Nothing Then
                        strbody = strbody.Trim.Replace("#Description", dsNotificationDetail.Tables(0).Rows(0)("remark").ToString().Replace(vbCrLf, "<br />").Replace(vbLf, "<br />"))
                    Else
                        strbody = strbody.Trim.Replace("<b>Job Description :</b> #Description", "")
                    End If

                    If dsNotificationDetail.Tables(0).Rows(0)("duties").ToString().Length > 0 Then
                        strbody = strbody.Trim.Replace("#Responsibility", dsNotificationDetail.Tables(0).Rows(0)("duties").ToString().Replace(vbCrLf, "<br />").Replace(vbLf, "<br />"))
                    Else
                        strbody = strbody.Trim.Replace("<b>Responsblity :</b> #Responsibility", "")
                    End If

                    If dsNotificationDetail.Tables(0).Rows(0)("Qualification").ToString().Length > 0 Then
                        strbody = strbody.Trim.Replace("#Qualifications", dsNotificationDetail.Tables(0).Rows(0)("Qualification").ToString())
                    Else
                        strbody = strbody.Trim.Replace("<b>Qualification Required :</b> #Qualifications", "")
                    End If

                    strbody = strbody.Trim.Replace("#Externallink", strExternallink)
                    strbody = strbody.Trim.Replace("#unsubscribelink", strSubscribelink)

                Else

                    strbody = strbody.Trim.Replace("#Title#", "") 'TODO -> Get Title from Applicant master
                    strbody = strbody.Trim.Replace("#Firstname#", dr.Item("firstname").ToString)
                    strbody = strbody.Trim.Replace("#Surname#", dr.Item("surname").ToString)
                    strbody = strbody.Trim.Replace("#Othername#", dr.Item("othername").ToString)
                    strbody = strbody.Trim.Replace("#VacancyName#", dsNotificationDetail.Tables(0).Rows(0)("vacancytitle").ToString())
                    strbody = strbody.Trim.Replace("#Vacancy_Without_Dates#", dsNotificationDetail.Tables(0).Rows(0)("vacancytitle").ToString())
                    Dim strVacWithDates As String = dsNotificationDetail.Tables(0).Rows(0)("vacancytitle").ToString()
                    If IsDBNull(dsNotificationDetail.Tables(0).Rows(0)("openingdate")) = False AndAlso dsNotificationDetail.Tables(0).Rows(0)("openingdate") <> Nothing Then
                        strVacWithDates &= " [" & CDate(dsNotificationDetail.Tables(0).Rows(0)("openingdate")).ToString("dd-MMM-yyyy").ToString
                    End If
                    If IsDBNull(dsNotificationDetail.Tables(0).Rows(0)("closingdate")) = False AndAlso dsNotificationDetail.Tables(0).Rows(0)("closingdate") <> Nothing Then
                        strVacWithDates &= " - " & CDate(dsNotificationDetail.Tables(0).Rows(0)("closingdate")).ToString("dd-MMM-yyyy").ToString & "]"
                    End If
                    strbody = strbody.Trim.Replace("#Vacancy_With_Dates#", strVacWithDates)
                    strbody = strbody.Trim.Replace("#CompanyName#", mstrCompanyName)
                    strbody = strbody.Trim.Replace("#experience#", Math.Round(CDec(CInt(dsNotificationDetail.Tables(0).Rows(0)("experience").ToString()) / 12), 1) & " Year(s).")
                    strbody = strbody.Trim.Replace("#noofposition#", dsNotificationDetail.Tables(0).Rows(0)("noposition").ToString())
                    strbody = strbody.Trim.Replace("#Pay_Range_From#", Math.Round(CDec(dsNotificationDetail.Tables(0).Rows(0)("pay_from")), 2).ToString)
                    strbody = strbody.Trim.Replace("#Pay_Range_To#", Math.Round(CDec(dsNotificationDetail.Tables(0).Rows(0)("pay_to")), 2).ToString)
                    strbody = strbody.Trim.Replace("#SkillRequired#", dsNotificationDetail.Tables(0).Rows(0)("skill").ToString())
                    strbody = strbody.Trim.Replace("#QualificationRequired#", dsNotificationDetail.Tables(0).Rows(0)("Qualification").ToString())
                    strbody = strbody.Trim.Replace("#VacancyOpeningDate#", CDate(dsNotificationDetail.Tables(0).Rows(0)("openingdate")).ToString("dd-MMM-yyyy"))
                    strbody = strbody.Trim.Replace("#VacancyClosingDate#", CDate(dsNotificationDetail.Tables(0).Rows(0)("closingdate")).ToString("dd-MMM-yyyy"))
                    strbody = strbody.Trim.Replace("#remark#", dsNotificationDetail.Tables(0).Rows(0)("remark").ToString().Replace(vbCrLf, "<br />").Replace(vbLf, "<br />"))
                    strbody = strbody.Trim.Replace("#responsibilities_duties#", dsNotificationDetail.Tables(0).Rows(0)("duties").ToString().Replace(vbCrLf, "<br />").Replace(vbLf, "<br />"))


                    strbody = strbody.Trim.Replace("#Externallink#", strExternallink)
                    strbody = strbody.Trim.Replace("#unsubscribelink#", strSubscribelink)

                End If
            End If
        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : GetNotificationDetails"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
        Return strbody
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function SendJobAlertNotification(ByRef strErrorMessage As String) As Boolean
        Dim message As MailMessage
        Dim smtp As SmtpClient
        strErrorMessage = ""
        Dim strLog As String = ""
        Try

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            strLog &= "client id : " & objUser.WebClientID & vbCrLf

            Dim strQ As String = " SELECT senderaddress,mailserverip " &
                                           " ,mailserverport, username,password " &
                                           " ,isloginssl,sendername,email_type,ews_url,ews_domain  " &
                                           ",maxvacancyalert,isvacancyAlert,issendjobconfirmationemail " &
                                           " FROM cfcompany_info  " &
                                           " WHERE  cfcompany_info.companyunkid = " & objUser.WebClientID & " AND cfcompany_info.company_code = '" & objUser.CompCode & "' "

            'Dim dsData As DataSet = objDataoperation.ExecQuery(strQ, "List")
            Dim dsData As New DataSet
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsData)

                    End Using
                End Using
            End Using

            Dim strSenderAddress As String = ""
            Dim strMailServerIP As String = ""
            Dim intMailServerPort As Integer = 0
            Dim strUserName As String = ""
            Dim strPassword As String = ""
            Dim blnIsloginssl As Boolean = False
            Dim strSenderName As String = ""
            Dim intEmail_Type As Integer = 0
            Dim strEWs_URL As String = ""
            Dim strEWs_Domain As String = ""
            Dim mintMaxVacacnyAlert As Integer = 0
            Dim mblnVacancyAlert As Boolean = False

            strLog &= "dsData Rows : " & dsData.Tables(0).Rows.Count & vbCrLf

            If dsData IsNot Nothing AndAlso dsData.Tables(0).Rows.Count > 0 Then
                strSenderAddress = dsData.Tables(0).Rows(0)("senderaddress").ToString.Trim
                strLog &= "strSenderAddress : " & strSenderAddress & " <br> "
                strMailServerIP = dsData.Tables(0).Rows(0)("mailserverip").ToString
                strLog &= "strMailServerIP : " & strMailServerIP & " <br> "
                intMailServerPort = CInt(dsData.Tables(0).Rows(0)("mailserverport"))
                strLog &= "intMailServerPort : " & intMailServerPort.ToString & " <br> "
                strUserName = dsData.Tables(0).Rows(0)("username").ToString
                strLog &= "strUserName : " & strUserName & " <br> "
                strPassword = clsSecurity.Decrypt(dsData.Tables(0).Rows(0)("password").ToString(), "ezee")
                'strLog &= "strPassword : " & strPassword & " <br> "
                blnIsloginssl = CBool(dsData.Tables(0).Rows(0)("isloginssl"))
                strLog &= "blnIsloginssl : " & blnIsloginssl & " <br> "
                strSenderName = dsData.Tables(0).Rows(0)("sendername").ToString
                strLog &= "strSenderName : " & strSenderName & " <br> "
                intEmail_Type = CInt(dsData.Tables(0).Rows(0)("email_type"))
                strLog &= "intEmail_Type : " & intEmail_Type.ToString & " <br> "
                strEWs_URL = dsData.Tables(0).Rows(0)("ews_url").ToString()
                strLog &= "strEWs_URL " & strEWs_URL & " <br> "
                strEWs_Domain = dsData.Tables(0).Rows(0)("ews_domain").ToString()
                strLog &= "strEWs_Domain : " & strEWs_Domain & " <br> "
                strLog &= "mintMaxVacacnyAlert : " & dsData.Tables(0).Rows(0)("maxvacancyalert").ToString() & " <br> "
                mintMaxVacacnyAlert = CInt(dsData.Tables(0).Rows(0)("maxvacancyalert").ToString())

                mblnVacancyAlert = CBool(dsData.Tables(0).Rows(0)("isvacancyAlert"))
                strLog &= "mblnVacancyAlert : " & dsData.Tables(0).Rows(0)("isvacancyAlert").ToString & " <br> "

            End If

            strLog &= "dsData2 Rows : " & dsData.Tables(0).Rows.Count & vbCrLf
            'objDataoperation.ClearParameters()
            'objDataoperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objUser.WebClientID)
            'objDataoperation.AddParameter("@compcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, objUser.CompCode)
            'objDataoperation.AddParameter("@maxalert", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxVacacnyAlert)
            'objDataoperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            'objDataoperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            'objDataoperation.AddParameter("@isemailsent", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            'Dim dsNotifiction As DataSet = objDataoperation.ExecStoredProcedure("procGetApplicantForJobAlerts", "List")
            Dim dsNotifiction As New DataSet
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand("procGetApplicantForJobAlerts", con)

                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = objUser.WebClientID
                        cmd.Parameters.Add(New SqlParameter("@compcode", SqlDbType.NVarChar)).Value = objUser.CompCode
                        cmd.Parameters.Add(New SqlParameter("@maxalert", SqlDbType.Int)).Value = mintMaxVacacnyAlert
                        cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = 0
                        cmd.Parameters.Add(New SqlParameter("@vacancyunkid", SqlDbType.Int)).Value = 0
                        cmd.Parameters.Add(New SqlParameter("@isemailsent", SqlDbType.Int)).Value = 0
                        da.SelectCommand = cmd
                        da.Fill(dsNotifiction)

                    End Using
                End Using
            End Using


            If dsNotifiction IsNot Nothing AndAlso dsNotifiction.Tables(0).Rows.Count <> 0 Then

                strLog &= "dsNotifiction Rows : " & dsNotifiction.Tables(0).Rows.Count & vbCrLf

                Dim strSenderEmail As String = strSenderAddress
                For Each dr As DataRow In dsNotifiction.Tables(0).Rows

                    strLog &= "applicantid : " & CInt(dr("applicantunkid")) & ";  " & CInt(dr("appnotificationpriorityunkid")) & ";; " & CInt(dr("vacancyunkid")) & vbCrLf

                    Dim strToEmail As String = dr("email").ToString()
                    Dim strSubject As String = dr("subject").ToString()

                    If strToEmail.Trim = "" OrElse strToEmail.Trim = "" Then Continue For

                    If intEmail_Type = 0 Then '0 = SMTP, 1 = EWS

                        'message = New MailMessage(strSenderEmail, strToEmail)
                        Dim strSenderDispName As String = strSenderName
                        If strSenderDispName.Trim.Length <= 0 Then
                            strSenderDispName = strSenderEmail
                        End If

                        message = New MailMessage(New MailAddress(strSenderEmail, strSenderDispName), New MailAddress(strToEmail))

                        message.To.Add(strToEmail)
                        message.Subject = strSubject
                        message.IsBodyHtml = True
                        Dim strBody As String = dr("message").ToString()
                        Dim HTMLEmail As AlternateView = AlternateView.CreateAlternateViewFromString(strBody, Nothing, "text/html")
                        message.AlternateViews.Add(HTMLEmail)

                        smtp = New SmtpClient
                        smtp.Host = strMailServerIP
                        smtp.Port = intMailServerPort
                        smtp.Credentials = New System.Net.NetworkCredential(strUserName, strPassword)
                        smtp.EnableSsl = blnIsloginssl
                        'smtp.Send(message)
                        strErrorMessage &= SendSMTP(smtp, message)

                    ElseIf intEmail_Type = 1 Then '0 = SMTP, 1 = EWS

                        'Creating the ExchangeService Object 
                        Dim objService As New ExchangeService()

                        'Seting Credentials to be used
                        objService.Credentials = New WebCredentials(strSenderName, strPassword, strEWs_Domain)

                        ''Setting the EWS Uri
                        objService.Url = New Uri(strEWs_URL)

                        ''Creating an Email Service and passing in the service
                        Dim objMessage As New EmailMessage(objService)

                        'Setting Email message properties
                        objMessage.Subject = strSubject
                        objMessage.ToRecipients.Add(strToEmail)
                        objMessage.Body = dr("message").ToString()

                        'objMessage.SendAndSaveCopy()
                        strErrorMessage &= SendEWS(objMessage)

                    End If

                    'objDataoperation.ClearParameters()
                    'objDataoperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objUser.WebClientID)
                    'objDataoperation.AddParameter("@compcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, objUser.CompCode)
                    'objDataoperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("applicantunkid")))
                    'objDataoperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("vacancyunkid")))
                    'objDataoperation.AddParameter("@appnotificationpriorityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("appnotificationpriorityunkid")))
                    'objDataoperation.AddParameter("@link", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr("link").ToString())
                    'objDataoperation.AddParameter("@subject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dr("subject").ToString())
                    'objDataoperation.AddParameter("@message", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, dr("message").ToString())
                    'objDataoperation.AddParameter("@isemailsent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    'objDataoperation.ExecStoredProcedure("procInsertUpdateApplicantVacancyNotfications", "List")
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand("procInsertUpdateApplicantVacancyNotfications", con)

                                cmd.CommandType = CommandType.StoredProcedure
                                cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = objUser.WebClientID
                                cmd.Parameters.Add(New SqlParameter("@compcode", SqlDbType.NVarChar)).Value = objUser.CompCode
                                cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = CInt(dr("applicantunkid"))
                                cmd.Parameters.Add(New SqlParameter("@vacancyunkid", SqlDbType.Int)).Value = CInt(dr("vacancyunkid"))
                                cmd.Parameters.Add(New SqlParameter("@appnotificationpriorityunkid", SqlDbType.Int)).Value = CInt(dr("appnotificationpriorityunkid"))
                                cmd.Parameters.Add(New SqlParameter("@link", SqlDbType.NVarChar)).Value = dr("link").ToString()
                                cmd.Parameters.Add(New SqlParameter("@subject", SqlDbType.NVarChar)).Value = dr("subject").ToString()
                                cmd.Parameters.Add(New SqlParameter("@message", SqlDbType.NVarChar)).Value = dr("message").ToString()
                                cmd.Parameters.Add(New SqlParameter("@isemailsent", SqlDbType.Bit)).Value = True

                                'da.SelectCommand = cmd
                                'da.Fill(dsNotifiction)
                                cmd.ExecuteNonQuery()

                            End Using
                        End Using
                    End Using

                Next

            End If

            If strErrorMessage.Trim <> "" Then
                strErrorMessage = strLog & vbCrLf & "smtp error : " & strErrorMessage
                Return False
            Else
                Return True
            End If


        Catch ex As Exception
            strErrorMessage = strLog & vbCrLf & ex.Message & ";" & ex.StackTrace.ToString
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= ";" & ex.InnerException.Message
                If ex.InnerException.InnerException IsNot Nothing Then
                    strErrorMessage &= ";" & ex.InnerException.InnerException.Message
                End If
            End If
            'Throw New Exception(strMsg)
            Return False
        End Try
    End Function

    'Pinkal (12-Feb-2018) -- End

    'Sohail (06 Nov 2019) -- Start
    'NMB Recruitment UAT # 20: Email sent back to employee after application of a vacancy should read as below. And should not have Powered by Aruti Signature.
    Private Function SendSMTP(smtp As SmtpClient, Message As MailMessage) As String
        Dim strErrorMessage As String = ""
        Try
            smtp.Send(Message)
        Catch ex As Exception
            strErrorMessage = ex.Message & ";" & ex.StackTrace.ToString
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= ";" & ex.InnerException.Message
                If ex.InnerException.InnerException IsNot Nothing Then
                    strErrorMessage &= ";" & ex.InnerException.InnerException.Message
                End If
            End If
            strErrorMessage = strErrorMessage & vbCrLf
        End Try
        Return strErrorMessage
    End Function

    Private Function SendEWS(objMessage As EmailMessage) As String
        Dim strErrorMessage As String = ""
        Try
            objMessage.SendAndSaveCopy()
        Catch ex As Exception
            strErrorMessage = ex.Message & ";" & ex.StackTrace.ToString
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= ";" & ex.InnerException.Message
                If ex.InnerException.InnerException IsNot Nothing Then
                    strErrorMessage &= ";" & ex.InnerException.InnerException.Message
                End If
            End If
            strErrorMessage = strErrorMessage & vbCrLf
        End Try
        Return strErrorMessage
    End Function
    'Sohail (06 Nov 2019) -- End

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function GetJob(ByRef strErrorMessage As String, ByVal blnOnlyPendingApplicants As Boolean, ByVal intApplicantID As Integer) As DataSet
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : GetSkill"
                Return Nothing
                Exit Function
            End If

            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            strQ = "SELECT  applicantunkid  " &
                          ", skillcategoryunkid " &
                          ", skillunkid " &
                          ", remark " &
                          ", other_skillcategory " &
                          ", other_skill " &
                          ", skillexpertiseunkid " &
                          ", skilltranunkid " &
                          ", skilltranunkid AS serverskilltranunkid " &
                          ", '' AS AUD " &
                          ", isvoid " &
                          ", 0 AS voiduserunkid " &
                          ", voiddatetime " &
                          ", voidreason " &
                    "FROM    rcapplicantskill_tran " &
                    "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                        "AND companyunkid = " & objUser.WebClientID & " "

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If intApplicantID > 0 Then
                strQ &= "AND applicantunkid = " & intApplicantID & " "
            End If

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsList)
            'Sohail (08 Oct 2018) -- End

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : GetSkill"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
        Return dsList
    End Function
#End Region

#Region " POST Methods "

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UpdateSyncDateTimeForImport(ByRef strErrorMessage As String, ByVal blnOnlyPendingApplicants As Boolean, ByVal strApplicantIDs As String) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UpdateSyncDateTimeForImport"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            '*** Applicant Master
            strQ = "UPDATE  rcapplicant_master " &
                    "SET     Syncdatetime = GETDATE() " &
                    "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                            "AND companyunkid = " & objUser.WebClientID & " "

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If strApplicantIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UpdateSyncDateTimeForImport [Applicant Master]"
            '    Return False
            '    Exit Function
            'End If
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            ''*** Job History
            strQ = "UPDATE  rcjobhistory " &
                    "SET     Syncdatetime = GETDATE() " &
                    "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                            "AND companyunkid = " & objUser.WebClientID & " "

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If strApplicantIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UpdateSyncDateTimeForImport [Applicant Job History]"
            '    Return False
            '    Exit Function
            'End If
            dsList.Tables.Clear()
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            '*** Applicant Skill Tran
            strQ = "UPDATE  rcapplicantskill_tran " &
                   "SET     Syncdatetime = GETDATE() " &
                   "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                           "AND companyunkid = " & objUser.WebClientID & " "

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If strApplicantIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UpdateSyncDateTimeForImport [Applicant Skill Tran]"
            '    Return False
            '    Exit Function
            'End If
            dsList.Tables.Clear()
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            '*** Applicant Qualification Tran
            strQ = "UPDATE  rcapplicantqualification_tran " &
                  "SET     Syncdatetime = GETDATE() " &
                  "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                          "AND companyunkid = " & objUser.WebClientID & " "

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If strApplicantIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UpdateSyncDateTimeForImport [Applicant Qualification Tran]"
            '    Return False
            '    Exit Function
            'End If
            dsList.Tables.Clear()
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            '*** Applicant Reference Tran
            strQ = "UPDATE  rcapp_reference_tran " &
                  "SET     Syncdatetime = GETDATE() " &
                  "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                          "AND companyunkid = " & objUser.WebClientID & " "

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If strApplicantIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UpdateSyncDateTimeForImport [Applicant Reference Tran]"
            '    Return False
            '    Exit Function
            'End If
            dsList.Tables.Clear()
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            '*** Applicant Vacancy Mapping Tran
            strQ = "UPDATE  rcapp_vacancy_mapping " &
                 "SET     Syncdatetime = GETDATE() " &
                 "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                         "AND companyunkid = " & objUser.WebClientID & " "

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If strApplicantIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UpdateSyncDateTimeForImport [Applicant Vacancy Mapping Tran]"
            '    Return False
            '    Exit Function
            'End If
            dsList.Tables.Clear()
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            '*** Applicant Image Tran
            strQ = "UPDATE  hr_images_tran " &
                 "SET     Syncdatetime = GETDATE() " &
                 "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                         "AND companyunkid = " & objUser.WebClientID & " "

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If strApplicantIDs.Trim.Length > 0 Then
                strQ &= "AND employeeunkid IN (" & strApplicantIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UpdateSyncDateTimeForImport [Applicant Image Tran]"
            '    Return False
            '    Exit Function
            'End If
            dsList.Tables.Clear()
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            '*** Applicant Attachments Tran
            strQ = "UPDATE  rcattachfiletran " &
                 "SET     Syncdatetime = GETDATE() " &
                 "WHERE   Comp_Code = '" & objUser.CompCode & "' " &
                         "AND companyunkid = " & objUser.WebClientID & " "

            If blnOnlyPendingApplicants = True Then
                strQ &= "AND ISNULL(Syncdatetime, ' ') = ' ' "
            End If

            If strApplicantIDs.Trim.Length > 0 Then
                strQ &= "AND applicantunkid IN (" & strApplicantIDs & ") "
            End If

            'dsList = objDataoperation.ExecQuery(strQ, "List")

            'If objDataoperation.ErrorMessage <> "" Then
            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UpdateSyncDateTimeForImport [Applicant Attachments Tran]"
            '    Return False
            '    Exit Function
            'End If
            dsList.Tables.Clear()
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UpdateSyncDateTimeForImport"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadCityMaster(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadCityMaster" & ";" & objUser.WebClientID.ToString & ";" & objUser.CompCode & ";" & objUser.AuthenticationCode & ";" ' & ConfigurationManager.ConnectionStrings("paydb").ToString()
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then

                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For

                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM cfcity_master WHERE cfcity_master.companyunkid = " & objUser.WebClientID & " AND cfcity_master.Comp_Code = '" & objUser.CompCode & "' AND cfcity_master.cityunkid = " & CInt(dRow.Item("cityunkid")) & ") " &
                                "BEGIN " &
                                    "INSERT INTO cfcity_master (Comp_Code, companyunkid, cityunkid, stateunkid, countryunkid, code, name, isactive, name1, name2) " &
                                    "SELECT '" & objUser.CompCode & "' " &
                                            ", " & objUser.WebClientID & " " &
                                            ", " & CInt(dRow.Item("cityunkid")) & " " &
                                            ", " & CInt(dRow.Item("stateunkid")) & " " &
                                            ", " & CInt(dRow.Item("countryunkid")) & " " &
                                            ", '" & dRow.Item("code").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("name").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(Int(dRow.Item("isactive"))) & " " &
                                            ", '" & dRow.Item("name1").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("name2").ToString.ReplaceQuotes().ToString() & "' " &
                                    "WHERE  " & CInt(Int(dRow.Item("isactive"))) & " = 1 " &
                                "END " &
                            "ELSE " &
                                "BEGIN " &
                                    "UPDATE  cfcity_master " &
                                    "SET     cfcity_master.countryunkid = " & CInt(dRow.Item("cityunkid")) & " " &
                                          ", cfcity_master.stateunkid = " & CInt(dRow.Item("stateunkid")) & " " &
                                          ", cfcity_master.code = '" & dRow.Item("code").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfcity_master.name = '" & dRow.Item("name").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfcity_master.name1 = '" & dRow.Item("name1").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfcity_master.name2 = '" & dRow.Item("name2").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfcity_master.isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                                    "WHERE   cfcity_master.companyunkid = " & objUser.WebClientID & " " &
                                            "AND cfcity_master.Comp_Code = '" & objUser.CompCode & "' " &
                                            "AND cfcity_master.cityunkid = " & CInt(dRow.Item("cityunkid")) & " " &
                                "END "

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadCityMaster"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using

                Next

            End If


            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadCityMaster"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadStateMaster(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadStateMaster"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then

                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For

                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM cfstate_master WHERE cfstate_master.companyunkid = " & objUser.WebClientID & " AND cfstate_master.Comp_Code = '" & objUser.CompCode & "' AND cfstate_master.stateunkid = " & CInt(dRow.Item("stateunkid")) & ") " &
                                "BEGIN " &
                                    "INSERT INTO cfstate_master (Comp_Code, companyunkid, stateunkid, countryunkid, code, name, isactive, name1, name2) " &
                                    "SELECT '" & objUser.CompCode & "' " &
                                            ", " & objUser.WebClientID & " " &
                                            ", " & CInt(dRow.Item("stateunkid")) & " " &
                                            ", " & CInt(dRow.Item("countryunkid")) & " " &
                                            ", '" & dRow.Item("code").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("name").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(Int(dRow.Item("isactive"))) & " " &
                                            ", '" & dRow.Item("name1").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("name2").ToString.ReplaceQuotes().ToString() & "' " &
                                    "WHERE  " & CInt(Int(dRow.Item("isactive"))) & " = 1 " &
                                "END " &
                            "ELSE " &
                                "BEGIN " &
                                    "UPDATE  cfstate_master " &
                                    "SET     cfstate_master.countryunkid = " & CInt(dRow.Item("countryunkid")) & " " &
                                          ", cfstate_master.code = '" & dRow.Item("code").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfstate_master.name = '" & dRow.Item("name").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfstate_master.name1 = '" & dRow.Item("name1").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfstate_master.name2 = '" & dRow.Item("name2").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfstate_master.isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                                    "WHERE   cfstate_master.companyunkid = " & objUser.WebClientID & " " &
                                            "AND cfstate_master.Comp_Code = '" & objUser.CompCode & "' " &
                                            "AND cfstate_master.stateunkid = " & CInt(dRow.Item("stateunkid")) & " " &
                                "END "

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadStateMaster"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using

                Next

            End If


            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadStateMaster"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadZipCodeMaster(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadZipCodeMaster"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then

                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For

                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM cfzipcode_master WHERE cfzipcode_master.companyunkid = " & objUser.WebClientID & " AND cfzipcode_master.Comp_Code = '" & objUser.CompCode & "' AND cfzipcode_master.zipcodeunkid = " & CInt(dRow.Item("zipcodeunkid")) & ") " &
                                "BEGIN " &
                                    "INSERT INTO cfzipcode_master (Comp_Code, companyunkid, zipcodeunkid, countryunkid, stateunkid, cityunkid, zipcode_code, zipcode_no, isactive) " &
                                    "SELECT '" & objUser.CompCode & "' " &
                                            ", " & objUser.WebClientID & " " &
                                            ", " & CInt(dRow.Item("zipcodeunkid")) & " " &
                                            ", " & CInt(dRow.Item("countryunkid")) & " " &
                                            ", " & CInt(dRow.Item("stateunkid")) & " " &
                                            ", " & CInt(dRow.Item("cityunkid")) & " " &
                                            ", '" & dRow.Item("zipcode_code").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("zipcode_no").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(Int(dRow.Item("isactive"))) & " " &
                                    "WHERE  " & CInt(Int(dRow.Item("isactive"))) & " = 1 " &
                                "END " &
                            "ELSE " &
                                "BEGIN " &
                                    "UPDATE  cfzipcode_master " &
                                    "SET     cfzipcode_master.countryunkid = " & CInt(dRow.Item("countryunkid")) & " " &
                                          ", cfzipcode_master.stateunkid = " & CInt(dRow.Item("stateunkid")) & " " &
                                          ", cfzipcode_master.cityunkid = " & CInt(dRow.Item("cityunkid")) & " " &
                                          ", cfzipcode_master.zipcode_code = '" & dRow.Item("zipcode_code").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfzipcode_master.zipcode_no = '" & dRow.Item("zipcode_no").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfzipcode_master.isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                                    "WHERE   cfzipcode_master.companyunkid = " & objUser.WebClientID & " " &
                                            "AND cfzipcode_master.Comp_Code = '" & objUser.CompCode & "' " &
                                            "AND cfzipcode_master.zipcodeunkid = " & CInt(dRow.Item("zipcodeunkid")) & " " &
                                "END "

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadZipCodeMaster"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using

                Next

            End If


            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadZipCodeMaster"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadCommonMaster(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadCommonMaster"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then

                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For

                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM cfcommon_master WHERE cfcommon_master.companyunkid = " & objUser.WebClientID & " AND cfcommon_master.Comp_Code = '" & objUser.CompCode & "' AND cfcommon_master.masterunkid = " & CInt(dRow.Item("masterunkid")) & ") " &
                                "BEGIN " &
                                    "INSERT INTO cfcommon_master (Comp_Code, companyunkid, masterunkid, code, alias, name, name1, name2, mastertype, isactive, isrefreeallowed, issyncwithrecruitment) " &
                                    "SELECT '" & objUser.CompCode & "' " &
                                            ", " & objUser.WebClientID & " " &
                                            ", " & CInt(dRow.Item("masterunkid")) & " " &
                                            ", '" & dRow.Item("code").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("alias").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("name").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("name1").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("name2").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(dRow.Item("mastertype")) & " " &
                                            ", " & CInt(Int(dRow.Item("isactive"))) & " " &
                                            ", " & CInt(Int(dRow.Item("isrefreeallowed"))) & " " &
                                            ", " & CInt(Int(dRow.Item("issyncwithrecruitment"))) & " " &
                                    "WHERE " & CInt(Int(dRow.Item("isactive"))) & " = 1 " &
                                    "AND NOT ( " & CInt(dRow.Item("mastertype")) & " = 33 AND " & CInt(Int(dRow.Item("issyncwithrecruitment"))) & " = 0) " &
                                "END " &
                            "ELSE " &
                                "BEGIN " &
                                    "UPDATE  cfcommon_master " &
                                    "SET     cfcommon_master.code = '" & dRow.Item("code").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfcommon_master.alias = '" & dRow.Item("alias").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfcommon_master.name = '" & dRow.Item("name").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfcommon_master.name1 = '" & dRow.Item("name1").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfcommon_master.name2 = '" & dRow.Item("name2").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", cfcommon_master.mastertype = " & CInt(dRow.Item("mastertype")) & " " &
                                          ", cfcommon_master.isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                                          ", cfcommon_master.isrefreeallowed = " & CInt(Int(dRow.Item("isrefreeallowed"))) & " " &
                                          ", cfcommon_master.issyncwithrecruitment = " & CInt(Int(dRow.Item("issyncwithrecruitment"))) & " " &
                                    "WHERE   cfcommon_master.companyunkid = " & objUser.WebClientID & " " &
                                            "AND cfcommon_master.Comp_Code = '" & objUser.CompCode & "' " &
                                            "AND cfcommon_master.masterunkid = " & CInt(dRow.Item("masterunkid")) & " " &
                                "END "

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadCommonMaster"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using

                Next

            End If


            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadCommonMaster"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadSkillMaster(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadSkillMaster"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then

                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For

                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM hrskill_master WHERE hrskill_master.companyunkid = " & objUser.WebClientID & " AND hrskill_master.Comp_Code = '" & objUser.CompCode & "' AND hrskill_master.skillunkid = " & CInt(dRow.Item("skillunkid")) & ") " &
                                "BEGIN " &
                                    "INSERT INTO hrskill_master (Comp_Code, companyunkid, skillunkid, skillcode, skillname, skillcategoryunkid, description, isactive, skillname1, skillname2) " &
                                    "SELECT '" & objUser.CompCode & "' " &
                                            ", " & objUser.WebClientID & " " &
                                            ", " & CInt(dRow.Item("skillunkid")) & " " &
                                            ", '" & dRow.Item("skillcode").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("skillname").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(dRow.Item("skillcategoryunkid")) & " " &
                                            ", '" & dRow.Item("description").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(Int(dRow.Item("isactive"))) & " " &
                                            ", '" & dRow.Item("skillname1").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("skillname2").ToString.ReplaceQuotes().ToString() & "' " &
                                    "WHERE " & CInt(Int(dRow.Item("isactive"))) & " = 1 " &
                                "END " &
                            "ELSE " &
                                "BEGIN " &
                                    "UPDATE  hrskill_master " &
                                    "SET     hrskill_master.skillcode = '" & dRow.Item("skillcode").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrskill_master.skillname = '" & dRow.Item("skillname").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrskill_master.skillcategoryunkid = " & CInt(dRow.Item("skillcategoryunkid")) & " " &
                                          ", hrskill_master.description = '" & dRow.Item("description").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrskill_master.skillname1 = '" & dRow.Item("skillname1").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrskill_master.skillname2 = '" & dRow.Item("skillname2").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrskill_master.isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                                    "WHERE   hrskill_master.companyunkid = " & objUser.WebClientID & " " &
                                            "AND hrskill_master.Comp_Code = '" & objUser.CompCode & "' " &
                                            "AND hrskill_master.skillunkid = " & CInt(dRow.Item("skillunkid")) & " " &
                                "END "

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadSkillMaster"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using

                Next

            End If


            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadSkillMaster"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadInstituteMaster(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadInstituteMaster"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then

                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For

                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM hrinstitute_master WHERE hrinstitute_master.companyunkid = " & objUser.WebClientID & " AND hrinstitute_master.Comp_Code = '" & objUser.CompCode & "' AND hrinstitute_master.instituteunkid = " & CInt(dRow.Item("instituteunkid")) & ") " &
                                "BEGIN " &
                                    "INSERT INTO hrinstitute_master (Comp_Code, companyunkid, instituteunkid, institute_code, institute_name, institute_address, telephoneno, institute_fax, institute_email, description, contact_person, stateunkid, cityunkid, countryunkid, ishospital, isactive) " &
                                    "SELECT '" & objUser.CompCode & "' " &
                                            ", " & objUser.WebClientID & " " &
                                            ", " & CInt(dRow.Item("instituteunkid")) & " " &
                                            ", '" & dRow.Item("institute_code").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("institute_name").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("institute_address").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("telephoneno").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("institute_fax").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("institute_email").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("description").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("contact_person").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(dRow.Item("stateunkid")) & " " &
                                            ", " & CInt(dRow.Item("cityunkid")) & " " &
                                            ", " & CInt(dRow.Item("countryunkid")) & " " &
                                            ", " & CInt(Int(dRow.Item("ishospital"))) & " " &
                                            ", " & CInt(Int(dRow.Item("isactive"))) & " " &
                                    "WHERE " & CInt(Int(dRow.Item("isactive"))) & " = 1 " &
                                            "AND " & CInt(Int(dRow.Item("ishospital"))) & " = 0 " &
                                            "AND " & CInt(Int(dRow.Item("issync_recruit"))) & " = 1 " &
                                "END " &
                            "ELSE " &
                                "BEGIN " &
                                    "UPDATE  hrinstitute_master " &
                                    "SET     hrinstitute_master.institute_code = '" & dRow.Item("institute_code").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrinstitute_master.institute_name = '" & dRow.Item("institute_name").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrinstitute_master.institute_address = '" & dRow.Item("institute_address").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrinstitute_master.telephoneno = '" & dRow.Item("telephoneno").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrinstitute_master.institute_fax = '" & dRow.Item("institute_fax").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrinstitute_master.institute_email = '" & dRow.Item("institute_email").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrinstitute_master.description = '" & dRow.Item("description").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrinstitute_master.contact_person = '" & dRow.Item("contact_person").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrinstitute_master.stateunkid = " & CInt(dRow.Item("stateunkid")) & " " &
                                          ", hrinstitute_master.cityunkid = " & CInt(dRow.Item("cityunkid")) & " " &
                                          ", hrinstitute_master.countryunkid = " & CInt(dRow.Item("countryunkid")) & " " &
                                          ", hrinstitute_master.ishospital = " & CInt(Int(dRow.Item("ishospital"))) & " " &
                                          ", hrinstitute_master.isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                                    "WHERE   hrinstitute_master.companyunkid = " & objUser.WebClientID & " " &
                                            "AND hrinstitute_master.Comp_Code = '" & objUser.CompCode & "' " &
                                            "AND hrinstitute_master.instituteunkid = " & CInt(dRow.Item("instituteunkid")) & " " &
                                "END "

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadInstituteMaster"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using

                Next

            End If


            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadInstituteMaster"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadQualificationMaster(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadQualificationMaster"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then

                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For

                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM hrqualification_master WHERE hrqualification_master.companyunkid = " & objUser.WebClientID & " AND hrqualification_master.Comp_Code = '" & objUser.CompCode & "' AND hrqualification_master.qualificationunkid = " & CInt(dRow.Item("qualificationunkid")) & ") " &
                                "BEGIN " &
                                    "INSERT INTO hrqualification_master (Comp_Code, companyunkid, qualificationunkid, qualificationcode, qualificationname, qualificationgroupunkid, description, isactive, qualificationname1, qualificationname2, resultgroupunkid) " &
                                    "SELECT '" & objUser.CompCode & "' " &
                                            ", " & objUser.WebClientID & " " &
                                            ", " & CInt(dRow.Item("qualificationunkid")) & " " &
                                            ", '" & dRow.Item("qualificationcode").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("qualificationname").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(dRow.Item("qualificationgroupunkid")) & " " &
                                            ", '" & dRow.Item("description").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(Int(dRow.Item("isactive"))) & " " &
                                            ", '" & dRow.Item("qualificationname1").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("qualificationname2").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(dRow.Item("resultgroupunkid")) & " " &
                                    "WHERE " & CInt(Int(dRow.Item("isactive"))) & " = 1 " &
                                            "AND " & CInt(Int(dRow.Item("issync_recruit"))) & " = 1 " &
                                "END " &
                            "ELSE " &
                                "BEGIN " &
                                    "UPDATE  hrqualification_master " &
                                    "SET     hrqualification_master.qualificationcode = '" & dRow.Item("qualificationcode").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrqualification_master.qualificationname = '" & dRow.Item("qualificationname").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrqualification_master.qualificationgroupunkid = " & CInt(dRow.Item("qualificationgroupunkid")) & " " &
                                          ", hrqualification_master.description = '" & dRow.Item("description").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrqualification_master.qualificationname1 = '" & dRow.Item("qualificationname1").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrqualification_master.qualificationname2 = '" & dRow.Item("qualificationname2").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrqualification_master.resultgroupunkid = " & CInt(dRow.Item("resultgroupunkid")) & " " &
                                          ", hrqualification_master.isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                                    "WHERE   hrqualification_master.companyunkid = " & objUser.WebClientID & " " &
                                            "AND hrqualification_master.Comp_Code = '" & objUser.CompCode & "' " &
                                            "AND hrqualification_master.qualificationunkid = " & CInt(dRow.Item("qualificationunkid")) & " " &
                                "END "

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadQualificationMaster"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using

                Next

            End If


            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadQualificationMaster"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadVacancyMaster(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean _
                                                             , ByVal xVacancyMasterType As Integer, ByVal xEmployeementMasterType As Integer)
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet
        Dim dsList1 As New DataSet
        Dim strPath As String = ""
        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadVacancyMaster"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()


            'Pinkal (12-Feb-2018) -- Start
            'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.
            Dim mdtCurrentDate As Date = Nothing
            Dim mintMaxVacanyalert As Integer = 0
            Dim mstrCompany As String = ""

            strQ = " SELECT ISNULL(cfcompany_info.company_name,'') AS Company, ISNULL(maxvacancyalert,0) AS maxvacancyalert, GETDATE() AS TodayDate FROM cfcompany_info WHERE companyunkid = " & objUser.WebClientID
            'dsList = objDataoperation.ExecQuery(strQ, "DateInfo")
            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        da.SelectCommand = cmd
                        da.Fill(dsList)

                    End Using
                End Using
            End Using

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrCompany = dsList.Tables(0).Rows(0)("Company").ToString()
                mdtCurrentDate = CDate(dsList.Tables(0).Rows(0)("TodayDate"))
                mintMaxVacanyalert = CInt(dsList.Tables(0).Rows(0)("maxvacancyalert"))
            End If
            'Pinkal (12-Feb-2018) -- End


            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then

                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For

                    End If

                    'Shani (02-Nov-2016) -- Start
                    'Enhancement - Add Employee Configuration Settings form in Aruti Configuration [Assign by Sohail]



                    'strQ = "IF NOT EXISTS(SELECT * FROM rcvacancy_master WHERE rcvacancy_master.companyunkid = " & objUser.WebClientID & " AND rcvacancy_master.Comp_Code = '" & objUser.CompCode & "' AND rcvacancy_master.vacancyunkid = " & cint(dRow.Item("vacancyunkid")) & ") " & _
                    '            "BEGIN " & _
                    '                "INSERT INTO rcvacancy_master (Comp_Code, companyunkid, vacancyunkid, vacancytitle, openingdate, closingdate, interview_startdate, interview_closedate, noofposition, experience, responsibilities_duties, remark, isvoid, isexternalvacancy) " & _
                    '                "SELECT '" & objUser.CompCode & "' " & _
                    '                        ", " & objUser.WebClientID & " " & _
                    '                        ", " & cint(dRow.Item("vacancyunkid")) & " " & _
                    '                        ", '" & dRow.Item("vacancytitle").ToString.ReplaceQuotes().ToString()& "' " & _
                    '                        ", '" & CDate(dRow.Item("openingdate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " & _
                    '                        ", '" & CDate(dRow.Item("closingdate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " & _
                    '                        ", '" & CDate(dRow.Item("interview_startdate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " & _
                    '                        ", '" & CDate(dRow.Item("interview_closedate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " & _
                    '                        ", " & cint(dRow.Item("noofposition")) & " " & _
                    '                        ", " & cint(dRow.Item("experience")) & " " & _
                    '                        ", '" & dRow.Item("responsibilities_duties").ToString.ReplaceQuotes().ToString()& "' " & _
                    '                        ", '" & dRow.Item("remark").ToString.ReplaceQuotes().ToString()& "' " & _
                    '                        ", " & CInt(Int(dRow.Item("isvoid"))) & " " & _
                    '                        ", " & CInt(Int(dRow.Item("isexternalvacancy"))) & " " & _
                    '                "WHERE " & CInt(Int(dRow.Item("isvoid"))) & " = 0 " & _
                    '                        "AND " & CInt(Int(dRow.Item("iswebexport"))) & " = 1 " & _
                    '            "END " & _
                    '        "ELSE " & _
                    '            "BEGIN " & _
                    '                "UPDATE  rcvacancy_master " & _
                    '                "SET     rcvacancy_master.vacancytitle = '" & dRow.Item("vacancytitle".ToString.ReplaceQuotes()) & "' " & _
                    '                      ", rcvacancy_master.openingdate = '" & CDate(dRow.Item("openingdate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " & _
                    '                      ", rcvacancy_master.closingdate = '" & CDate(dRow.Item("closingdate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " & _
                    '                      ", rcvacancy_master.interview_startdate = '" & CDate(dRow.Item("interview_startdate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " & _
                    '                      ", rcvacancy_master.interview_closedate = '" & CDate(dRow.Item("interview_closedate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " & _
                    '                      ", rcvacancy_master.noofposition = " & cint(dRow.Item("noofposition")) & " " & _
                    '                      ", rcvacancy_master.experience = " & cint(dRow.Item("experience")) & " " & _
                    '                      ", rcvacancy_master.responsibilities_duties = '" & dRow.Item("responsibilities_duties").ToString.ReplaceQuotes().ToString()& "' " & _
                    '                      ", rcvacancy_master.remark = '" & dRow.Item("remark").ToString.ReplaceQuotes().ToString()& "' " & _
                    '                      ", rcvacancy_master.isvoid = " & CInt(Int(dRow.Item("isvoid"))) & " " & _
                    '                      ", rcvacancy_master.isexternalvacancy = " & CInt(Int(dRow.Item("isexternalvacancy"))) & " " & _
                    '                "WHERE   rcvacancy_master.companyunkid = " & objUser.WebClientID & " " & _
                    '                        "AND rcvacancy_master.Comp_Code = '" & objUser.CompCode & "' " & _
                    '                        "AND rcvacancy_master.vacancyunkid = " & cint(dRow.Item("vacancyunkid")) & " " & _
                    '            "END "
                    strQ = "IF NOT EXISTS(SELECT * FROM rcvacancy_master WHERE rcvacancy_master.companyunkid = " & objUser.WebClientID & " AND rcvacancy_master.Comp_Code = '" & objUser.CompCode & "' AND rcvacancy_master.vacancyunkid = " & CInt(dRow.Item("vacancyunkid")) & ") " &
                                "BEGIN " &
                       "    INSERT INTO rcvacancy_master (" &
                       "        Comp_Code" &
                       "        , companyunkid" &
                       "        , vacancyunkid" &
                       "        , vacancytitle" &
                       "        , openingdate" &
                       "        , closingdate" &
                       "        , interview_startdate" &
                       "        , interview_closedate" &
                       "        , noofposition" &
                       "        , experience" &
                       "        , responsibilities_duties" &
                       "        , remark" &
                       "        , isvoid" &
                       "        , isexternalvacancy" &
                       "        ,stationunkid " &
                       "        ,deptgroupunkid " &
                       "        ,departmentunkid " &
                       "        ,sectionunkid " &
                       "        ,unitunkid " &
                       "        ,jobgroupunkid " &
                       "        ,jobunkid " &
                       "        ,employeementtypeunkid " &
                       "        ,paytypeunkid " &
                       "        ,shifttypeunkid " &
                       "        ,pay_from " &
                       "        ,pay_to " &
                       "        ,isskillbold " &
                       "        ,isskillitalic " &
                       "        ,isqualibold " &
                       "        ,isqualiitalic " &
                       "        ,isexpbold " &
                       "        ,isexpitalic " &
                       "        ,isbothintext " &
                       "        ,other_qualification " &
                       "        ,other_skill " &
                       "        ,other_language " &
                       "        ,other_experience " &
                       "        ,classgroupunkid " &
                       "        ,classunkid " &
                       "        ) " &
                       "    SELECT " &
                       "        '" & objUser.CompCode & "' " &
                                            ", " & objUser.WebClientID & " " &
                                            ", " & CInt(dRow.Item("vacancyunkid")) & " " &
                                            ", '" & dRow.Item("vacancytitle").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & CDate(dRow.Item("openingdate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " &
                                            ", '" & CDate(dRow.Item("closingdate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " &
                                            ", '" & CDate(dRow.Item("interview_startdate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " &
                                            ", '" & CDate(dRow.Item("interview_closedate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " &
                                            ", " & CInt(dRow.Item("noofposition")) & " " &
                                            ", " & CInt(dRow.Item("experience")) & " " &
                                            ", '" & dRow.Item("responsibilities_duties").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("remark").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(Int(dRow.Item("isvoid"))) & " " &
                                            ", " & CInt(Int(dRow.Item("isexternalvacancy"))) & " " &
                       "        ," & CInt(dRow.Item("stationunkid")) & " " &
                       "        ," & CInt(dRow.Item("deptgroupunkid")) & " " &
                       "        ," & CInt(dRow.Item("departmentunkid")) & " " &
                       "        ," & CInt(dRow.Item("sectionunkid")) & " " &
                       "        ," & CInt(dRow.Item("unitunkid")) & " " &
                       "        ," & CInt(dRow.Item("jobgroupunkid")) & " " &
                       "        ," & CInt(dRow.Item("jobunkid")) & " " &
                       "        ," & CInt(dRow.Item("employeementtypeunkid")) & " " &
                       "        ," & CInt(dRow.Item("paytypeunkid")) & " " &
                       "        ," & CInt(dRow.Item("shifttypeunkid")) & " " &
                       "        ," & CDec(dRow.Item("pay_from")) & " " &
                       "        ," & CDec(dRow.Item("pay_to")) & " " &
                       ", " & CInt(Int(dRow.Item("isskillbold"))) & " " &
                       ", " & CInt(Int(dRow.Item("isskillitalic"))) & " " &
                       ", " & CInt(Int(dRow.Item("isqualibold"))) & " " &
                       ", " & CInt(Int(dRow.Item("isqualiitalic"))) & " " &
                       ", " & CInt(Int(dRow.Item("isexpbold"))) & " " &
                       ", " & CInt(Int(dRow.Item("isexpitalic"))) & " " &
                       ", " & CInt(Int(dRow.Item("isbothintext"))) & " " &
                       ", '" & dRow.Item("other_qualification").ToString.ReplaceQuotes().ToString() & "' " &
                       ", '" & dRow.Item("other_skill").ToString.ReplaceQuotes().ToString() & "' " &
                       ", '" & dRow.Item("other_language").ToString.ReplaceQuotes().ToString() & "' " &
                       ", '" & dRow.Item("other_experience").ToString.ReplaceQuotes().ToString() & "' " &
                        ", " & CInt(dRow.Item("classgroupunkid")) & " " &
                         ", " & CInt(dRow.Item("classunkid")) & " " &
                                    "WHERE " & CInt(Int(dRow.Item("isvoid"))) & " = 0 " &
                                            "AND " & CInt(Int(dRow.Item("iswebexport"))) & " = 1 " &
                                "END " &
                            "ELSE " &
                                "BEGIN " &
                                    "UPDATE  rcvacancy_master " &
                                    "SET     rcvacancy_master.vacancytitle = '" & dRow.Item("vacancytitle").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", rcvacancy_master.openingdate = '" & CDate(dRow.Item("openingdate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " &
                                          ", rcvacancy_master.closingdate = '" & CDate(dRow.Item("closingdate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " &
                                          ", rcvacancy_master.interview_startdate = '" & CDate(dRow.Item("interview_startdate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " &
                                          ", rcvacancy_master.interview_closedate = '" & CDate(dRow.Item("interview_closedate")).ToString("yyyy-MM-dd hh:mm:ss") & "' " &
                                          ", rcvacancy_master.noofposition = " & CInt(dRow.Item("noofposition")) & " " &
                                          ", rcvacancy_master.experience = " & CInt(dRow.Item("experience")) & " " &
                                          ", rcvacancy_master.responsibilities_duties = '" & dRow.Item("responsibilities_duties").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", rcvacancy_master.remark = '" & dRow.Item("remark").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", rcvacancy_master.isvoid = " & CInt(Int(dRow.Item("isvoid"))) & " " &
                                          ", rcvacancy_master.isexternalvacancy = " & CInt(Int(dRow.Item("isexternalvacancy"))) & " " &
                       "    , rcvacancy_master.stationunkid = " & CInt(dRow.Item("stationunkid")) & " " &
                       "    , rcvacancy_master.deptgroupunkid = " & CInt(dRow.Item("deptgroupunkid")) & " " &
                       "    , rcvacancy_master.departmentunkid = " & CInt(dRow.Item("departmentunkid")) & " " &
                       "    , rcvacancy_master.sectionunkid = " & CInt(dRow.Item("sectionunkid")) & " " &
                       "    , rcvacancy_master.unitunkid = " & CInt(dRow.Item("unitunkid")) & " " &
                       "    , rcvacancy_master.jobgroupunkid = " & CInt(dRow.Item("jobgroupunkid")) & " " &
                       "    , rcvacancy_master.jobunkid = " & CInt(dRow.Item("jobunkid")) & " " &
                       "    , rcvacancy_master.employeementtypeunkid = " & CInt(dRow.Item("employeementtypeunkid")) & " " &
                       "    , rcvacancy_master.paytypeunkid = " & CInt(dRow.Item("paytypeunkid")) & " " &
                       "    , rcvacancy_master.shifttypeunkid = " & CInt(dRow.Item("shifttypeunkid")) & " " &
                       "    , rcvacancy_master.pay_from = " & CInt(dRow.Item("pay_from")) & " " &
                       "    , rcvacancy_master.pay_to = " & CInt(dRow.Item("pay_to")) & " " &
                       "    , rcvacancy_master.isskillbold = " & CInt(Int(dRow.Item("isskillbold"))) & " " &
                       "    , rcvacancy_master.isskillitalic = " & CInt(Int(dRow.Item("isskillitalic"))) & " " &
                       "    , rcvacancy_master.isqualibold = " & CInt(Int(dRow.Item("isqualibold"))) & " " &
                       "    , rcvacancy_master.isqualiitalic = " & CInt(Int(dRow.Item("isqualiitalic"))) & " " &
                       "    , rcvacancy_master.isexpbold = " & CInt(Int(dRow.Item("isexpbold"))) & " " &
                       "    , rcvacancy_master.isexpitalic = " & CInt(Int(dRow.Item("isexpitalic"))) & " " &
                       "    , rcvacancy_master.isbothintext = " & CInt(Int(dRow.Item("isbothintext"))) & " " &
                       "    , rcvacancy_master.other_qualification = '" & dRow.Item("other_qualification").ToString.ReplaceQuotes().ToString() & "' " &
                       "    , rcvacancy_master.other_skill = '" & dRow.Item("other_skill").ToString.ReplaceQuotes().ToString() & "' " &
                       "    , rcvacancy_master.other_language = '" & dRow.Item("other_language").ToString.ReplaceQuotes().ToString() & "' " &
                       "    , rcvacancy_master.other_experience = '" & dRow.Item("other_experience").ToString.ReplaceQuotes().ToString() & "' " &
                       "    , rcvacancy_master.classgroupunkid = " & CInt(dRow.Item("classgroupunkid")) & " " &
                       "    , rcvacancy_master.classunkid = " & CInt(dRow.Item("classunkid")) & " " &
                                    "WHERE   rcvacancy_master.companyunkid = " & objUser.WebClientID & " " &
                                            "AND rcvacancy_master.Comp_Code = '" & objUser.CompCode & "' " &
                                            "AND rcvacancy_master.vacancyunkid = " & CInt(dRow.Item("vacancyunkid")) & " " &
                                "END "
                    'Sohail (18 Feb 2020) -- [isbothintext]
                    'Shani (02-Nov-2016) -- End

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadVacancyMaster"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                cmd.ExecuteNonQuery()
                                'da.SelectCommand = cmd
                                'da.Fill(dsList)

                            End Using
                        End Using
                    End Using

                    'Pinkal (12-Feb-2018) -- Start
                    'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.

                    If CDate(dRow.Item("closingdate")) >= CDate(mdtCurrentDate.AddMinutes(CInt(dRow.Item("TimeZone")))) Then

                        strQ = " SELECT appnotificationpriorityunkid, rc_appnotificationpriority.companyunkid,rc_appnotificationpriority.comp_code,rc_appnotificationpriority.applicantunkid " &
                                  ", applicant_code,ISNULL(firstname,'') + ' ' + ISNULL(surname,'') As applicant " &
                                  ", applicant_code,ISNULL(firstname,'') AS firstname " &
                                  ", applicant_code,ISNULL(surname,'') AS surname " &
                                  ", applicant_code,ISNULL(othername,'') AS othername " &
                                  " FROM " &
                                  " rc_appnotificationpriority " &
                                  " LEFT JOIN rcapplicant_master ON rcapplicant_master.applicantunkid = rc_appnotificationpriority.applicantunkid " &
                                  " WHERE rc_appnotificationpriority.comp_code = '" & objUser.CompCode & "' AND rc_appnotificationpriority.companyunkid = " & objUser.WebClientID &
                                  " AND vacancytitleunkid = " & CInt(dRow.Item("vacancytitle")) & " And isactive = 1 And priority > 0 "

                        'dsList = objDataoperation.ExecQuery(strQ, "List")

                        'If objDataoperation.ErrorMessage <> "" Then
                        '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadVacancyMaster"
                        '    Return False
                        '    Exit Function
                        'End If
                        dsList1 = New DataSet
                        Using con As New SqlConnection(strConn)

                            con.Open()

                            Using da As New SqlDataAdapter()
                                Using cmd As New SqlCommand(strQ, con)

                                    cmd.CommandType = CommandType.Text
                                    cmd.CommandTimeout = 0
                                    da.SelectCommand = cmd
                                    da.Fill(dsList1)

                                End Using
                            End Using
                        End Using

                        If dsList1 IsNot Nothing AndAlso dsList1.Tables(0).Rows.Count > 0 Then

                            'objDataoperation.ClearParameters()
                            'objDataoperation.AddParameter("@Comp_Code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, objUser.CompCode)
                            'objDataoperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objUser.WebClientID)
                            'objDataoperation.AddParameter("@mastertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xVacancyMasterType)
                            'objDataoperation.AddParameter("@EType", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeementMasterType)
                            'objDataoperation.AddParameter("@VacancyType", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(dRow.Item("isexternalvacancy")))
                            'objDataoperation.AddParameter("@AllVacancy", SqlDbType.Int, eZeeDataType.INT_SIZE, False)
                            'objDataoperation.AddParameter("@DateZoneDifference", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dRow.Item("TimeZone")))
                            'objDataoperation.AddParameter("@VacancyUnkIdList", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dRow.Item("vacancyunkid").ToString())

                            'Dim dsNotificationData As DataSet = objDataoperation.ExecStoredProcedure("procGetVacancy", "List")

                            'If objDataoperation.ErrorMessage <> "" Then
                            '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadVacancyMaster"
                            '    Return False
                            '    Exit Function
                            'End If
                            Dim dsNotificationData As New DataSet
                            Using con As New SqlConnection(strConn)

                                con.Open()

                                Using da As New SqlDataAdapter()
                                    Using cmd As New SqlCommand("procGetVacancy", con)

                                        cmd.CommandType = CommandType.StoredProcedure
                                        cmd.Parameters.Add(New SqlParameter("@Comp_Code", SqlDbType.NVarChar)).Value = objUser.CompCode
                                        'cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.NVarChar)).Value = objUser.WebClientID
                                        cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = objUser.WebClientID
                                        'cmd.Parameters.Add(New SqlParameter("@mastertypeid", SqlDbType.Bit)).Value = xVacancyMasterType
                                        cmd.Parameters.Add(New SqlParameter("@mastertypeid", SqlDbType.Int)).Value = xVacancyMasterType
                                        cmd.Parameters.Add(New SqlParameter("@EType", SqlDbType.Int)).Value = xEmployeementMasterType
                                        'cmd.Parameters.Add(New SqlParameter("@VacancyType", SqlDbType.NVarChar)).Value = CBool(dRow.Item("isexternalvacancy"))
                                        cmd.Parameters.Add(New SqlParameter("@VacancyType", SqlDbType.Bit)).Value = CBool(dRow.Item("isexternalvacancy"))
                                        'cmd.Parameters.Add(New SqlParameter("@AllVacancy", SqlDbType.Int)).Value = False
                                        cmd.Parameters.Add(New SqlParameter("@AllVacancy", SqlDbType.Bit)).Value = False
                                        'cmd.Parameters.Add(New SqlParameter("@DateZoneDifference", SqlDbType.NVarChar)).Value = CInt(dRow.Item("TimeZone"))
                                        cmd.Parameters.Add(New SqlParameter("@DateZoneDifference", SqlDbType.Int)).Value = CInt(dRow.Item("TimeZone"))
                                        cmd.Parameters.Add(New SqlParameter("@VacancyUnkIdList", SqlDbType.NVarChar)).Value = dRow.Item("vacancyunkid").ToString()
                                        da.SelectCommand = cmd
                                        da.Fill(dsNotificationData)

                                    End Using
                                End Using
                            End Using

                            'Sohail (25 Mar 2019) -- Start
                            'Issue : There is no row at position 0
                            If dsNotificationData.Tables(0).Rows.Count <= 0 Then Continue For
                            'Sohail (25 Mar 2019) -- End

                            Dim strMainBody As String = ""
                            Dim strSubject As String = ""
                            'Sohail (06 Nov 2019) -- Start
                            'NMB Recruitment UAT # 20: Email sent back to employee after application of a vacancy should read as below. And should not have Powered by Aruti Signature.
                            Dim sKeyName As String = "JOBALERTTEMPLATEID"
                            If CBool(dRow.Item("isexternalvacancy")) = False Then
                                sKeyName = "INTERNALJOBALERTTEMPLATEID"
                            End If
                            Dim strValue As String = clsConfiguration.GetConfigurationValue(strCompCode:=objUser.CompCode _
                                                                  , intCompanyUnkID:=objUser.WebClientID _
                                                                  , strKeyName:=sKeyName
                                                                  )

                            Dim intID As Integer
                            Integer.TryParse(strValue, intID)
                            Dim intTemplateFound As Boolean = False
                            If intID > 0 Then
                                Dim objLetterType As New clsLetterType
                                Dim lstLetter As List(Of clsLetterType) = objLetterType.GetList(intCompanyId:=objUser.WebClientID _
                                                                                          , strCompanyCode:=objUser.CompCode _
                                                                                          , intUnkId:=intID
                                                                                          )

                                If lstLetter IsNot Nothing AndAlso lstLetter.Count > 0 Then
                                    Dim strLetterContent As String = lstLetter(0)._lettercontent
                                    Dim intFIdx As Integer = strLetterContent.IndexOf(CChar("{")) 'to remove xml start and end tags
                                    Dim intLIdx As Integer = strLetterContent.LastIndexOf(CChar("}")) 'to remove xml start and end tags
                                    strLetterContent = strLetterContent.Substring(intFIdx, intLIdx - intFIdx + 1) 'to remove xml start and end tags

                                    If strLetterContent.Trim.Length > 0 Then
                                        'Dim objLF As New clsLetterFields
                                        'Dim dsField As DataSet = objLF.GetList(lstLetter(0)._fieldtypeid)
                                        intTemplateFound = True
                                        'Dim strDataName As String = ""
                                        'Dim StrCol As String = ""

                                        'strLetterContent = strLetterContent.Replace("#CompanyCode#", objUser.CompCode)
                                        'strLetterContent = strLetterContent.Replace("#CompanyName#", Session("CompName").ToString)

                                        'For Each dsRow As DataRow In dsField.Tables(0).Rows
                                        '    StrCol = dsRow.Item("Name").ToString

                                        '    If strLetterContent.Contains("#" & StrCol & "#") Then

                                        '        If StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                                        '            strDataName = strLetterContent.Replace("#" & StrCol & "#", "#imgcmp#")
                                        '        Else
                                        '            If dsAppVac.Tables(0).Columns.Contains(StrCol) = True Then
                                        '                strDataName = strLetterContent.Replace("#" & StrCol & "#", dsAppVac.Tables(0).Rows(0)(StrCol).ToString)
                                        '            Else
                                        '                strDataName = strLetterContent.Replace("#" & StrCol & "#", "")
                                        '            End If
                                        '        End If

                                        '        strLetterContent = strDataName
                                        '    End If
                                        'Next

                                        Dim htmlOutput = "Template.html"
                                        Dim contentUriPrefix = Path.GetFileNameWithoutExtension(htmlOutput)
                                        Dim htmlResult = RtfToHtmlConverter.RtfToHtml(strLetterContent, contentUriPrefix)

                                        strMainBody = htmlResult._HTML

                                    End If
                                End If
                            End If
                            'Sohail (06 Nov 2019) -- End

                            If intTemplateFound = False Then
                                strPath = HostingEnvironment.ApplicationPhysicalPath & "EmailTemplate\JobAlerts.html"
                                Dim reader As New StreamReader(strPath)
                                'Dim reader As New StreamReader(HttpContext.Current.Server.MapPath("~") & "/EmailTemplate/JobAlerts.html")
                                If reader IsNot Nothing Then
                                    strMainBody = reader.ReadToEnd
                                End If
                                reader.Close()
                            End If

                            Dim strExternallink As String = dRow.Item("link").ToString().Trim() & IIf(CBool(dRow.Item("isexternalvacancy")), "7", "9") & "&cc=" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(objUser.WebClientID.ToString & "?" & objUser.CompCode.Trim))
                            Dim strUnsubscribeLink As String = dRow.Item("link").ToString().Trim() & IIf(CBool(dRow.Item("isexternalvacancy")), "7", "9") & "&cc=" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(objUser.WebClientID.ToString & "?" & objUser.CompCode.Trim)) & "&refid=" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt("notification"))


                            strSubject = "Job Alert : " & dsNotificationData.Tables(0).Rows(0)("vacancytitle").ToString().Trim() & " at " & mstrCompany.ToString().Trim()


                            For Each dr As DataRow In dsList1.Tables(0).Rows

                                Dim mstrMessage As String = GetNotificationDetails(strErrorMessage, strMainBody, mstrCompany, dr("applicant").ToString(), dsNotificationData, strExternallink, strUnsubscribeLink, intTemplateFound, dr)

                                'objDataoperation.ClearParameters()
                                'objDataoperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objUser.WebClientID)
                                'objDataoperation.AddParameter("@compcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, objUser.CompCode)
                                'objDataoperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("applicantunkid")))
                                'objDataoperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dRow.Item("vacancyunkid")))
                                'objDataoperation.AddParameter("@appnotificationpriorityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("appnotificationpriorityunkid")))
                                'objDataoperation.AddParameter("@link", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strExternallink)
                                'objDataoperation.AddParameter("@subject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strSubject)
                                'objDataoperation.AddParameter("@message", SqlDbType.NVarChar, mstrMessage.Trim.Length, mstrMessage.Trim)
                                'objDataoperation.AddParameter("@isemailsent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                'Dim dsData As DataSet = objDataoperation.ExecStoredProcedure("procInsertUpdateApplicantVacancyNotfications", "List")

                                'If objDataoperation.ErrorMessage <> "" Then
                                '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadVacancyMaster"
                                '    Return False
                                'End If
                                Using con As New SqlConnection(strConn)

                                    con.Open()

                                    Using da As New SqlDataAdapter()
                                        Using cmd As New SqlCommand("procInsertUpdateApplicantVacancyNotfications", con)

                                            cmd.CommandType = CommandType.StoredProcedure
                                            cmd.Parameters.Add(New SqlParameter("@companyunkid", SqlDbType.Int)).Value = objUser.WebClientID
                                            cmd.Parameters.Add(New SqlParameter("@compcode", SqlDbType.NVarChar)).Value = objUser.CompCode
                                            cmd.Parameters.Add(New SqlParameter("@applicantunkid", SqlDbType.Int)).Value = CInt(dr("applicantunkid"))
                                            cmd.Parameters.Add(New SqlParameter("@vacancyunkid", SqlDbType.Int)).Value = CInt(dRow("vacancyunkid"))
                                            cmd.Parameters.Add(New SqlParameter("@appnotificationpriorityunkid", SqlDbType.Int)).Value = CInt(dr("appnotificationpriorityunkid"))
                                            cmd.Parameters.Add(New SqlParameter("@link", SqlDbType.NVarChar)).Value = strExternallink
                                            cmd.Parameters.Add(New SqlParameter("@subject", SqlDbType.NVarChar)).Value = strSubject
                                            cmd.Parameters.Add(New SqlParameter("@message", SqlDbType.NVarChar)).Value = mstrMessage.Trim
                                            cmd.Parameters.Add(New SqlParameter("@isemailsent", SqlDbType.Bit)).Value = False

                                            'da.SelectCommand = cmd
                                            'da.Fill(dsNotifiction)
                                            cmd.ExecuteNonQuery()

                                        End Using
                                    End Using
                                End Using

                            Next

                        End If



                    End If

                    'Pinkal (12-Feb-2018) -- End 

                Next

            End If

            Return True
        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & strPath & "; " & ex.StackTrace & "; Procedure : UploadVacancyMaster"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadResultMaster(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadResultMaster"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then

                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For

                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM hrresult_master WHERE hrresult_master.companyunkid = " & objUser.WebClientID & " AND hrresult_master.Comp_Code = '" & objUser.CompCode & "' AND hrresult_master.resultunkid = " & CInt(dRow.Item("resultunkid")) & ") " &
                                "BEGIN " &
                                    "INSERT INTO hrresult_master (Comp_Code, companyunkid, resultunkid, resultcode, resultname, resultgroupunkid, description, isactive, resultname1, resultname2) " &
                                    "SELECT '" & objUser.CompCode & "' " &
                                            ", " & objUser.WebClientID & " " &
                                            ", " & CInt(dRow.Item("resultunkid")) & " " &
                                            ", '" & dRow.Item("resultcode").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("resultname").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(dRow.Item("resultgroupunkid")) & " " &
                                            ", '" & dRow.Item("description").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(Int(dRow.Item("isactive"))) & " " &
                                            ", '" & dRow.Item("resultname1").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("resultname2").ToString.ReplaceQuotes().ToString() & "' " &
                                    "WHERE " & CInt(Int(dRow.Item("isactive"))) & " = 1 " &
                                "END " &
                            "ELSE " &
                                "BEGIN " &
                                    "UPDATE  hrresult_master " &
                                    "SET     hrresult_master.resultcode = '" & dRow.Item("resultcode").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrresult_master.resultname = '" & dRow.Item("resultname").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrresult_master.resultgroupunkid = " & CInt(dRow.Item("resultgroupunkid")) & " " &
                                          ", hrresult_master.description = '" & dRow.Item("description").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrresult_master.resultname1 = '" & dRow.Item("resultname1").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrresult_master.resultname2 = '" & dRow.Item("resultname2").ToString.ReplaceQuotes().ToString() & "' " &
                                          ", hrresult_master.isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                                    "WHERE   hrresult_master.companyunkid = " & objUser.WebClientID & " " &
                                            "AND hrresult_master.Comp_Code = '" & objUser.CompCode & "' " &
                                            "AND hrresult_master.resultunkid = " & CInt(dRow.Item("resultunkid")) & " " &
                                "END "

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadResultMaster"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using

                Next

            End If


            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadResultMaster"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    'Nilay (13 Apr 2017) -- Start
    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadSkillExpertiseMaster(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try
            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadSkillExpertiseMaster"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then
                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For
                    End If

                    strQ = " IF NOT EXISTS(SELECT * FROM rcskillexpertise_master WHERE rcskillexpertise_master.companyunkid = " & objUser.WebClientID & " AND rcskillexpertise_master.Comp_Code = '" & objUser.CompCode & "' AND rcskillexpertise_master.skillexpertiseunkid = " & CInt(dRow.Item("skillexpertiseunkid")) & ") " &
                                " BEGIN " &
                                    " INSERT INTO rcskillexpertise_master (skillexpertiseunkid, code, name, isactive, name1, name2, Comp_Code, companyunkid) " &
                                    " SELECT " &
                                            "  " & CInt(dRow.Item("skillexpertiseunkid")) & " " &
                                            ", '" & dRow.Item("code").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("name").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(Int(dRow.Item("isactive"))) & " " &
                                            ", '" & dRow.Item("name1").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("name2").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & objUser.CompCode & "' " &
                                            ", " & objUser.WebClientID & " " &
                                    " WHERE " & CInt(Int(dRow.Item("isactive"))) & " = 1 " &
                                " END " &
                            " ELSE " &
                                " BEGIN " &
                                    " UPDATE rcskillexpertise_master SET " &
                                            "  rcskillexpertise_master.code = '" & dRow.Item("code").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", rcskillexpertise_master.name = '" & dRow.Item("name").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", rcskillexpertise_master.isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                                            ", rcskillexpertise_master.name1 = '" & dRow.Item("name1").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", rcskillexpertise_master.name2 = '" & dRow.Item("name2").ToString.ReplaceQuotes().ToString() & "' " &
                                    " WHERE rcskillexpertise_master.companyunkid = " & objUser.WebClientID & " " &
                                            " AND rcskillexpertise_master.Comp_Code = '" & objUser.CompCode & "' " &
                                            " AND rcskillexpertise_master.skillexpertiseunkid = " & CInt(dRow.Item("skillexpertiseunkid")) & " " &
                                " END "

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadSkillExpertiseMaster"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using
                Next
            End If

            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadSkillExpertiseMaster"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function
    'Nilay (13 Apr 2017) -- End


    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadCompanyInfo(ByRef strErrorMessage As String, ByVal dsTable As DataSet) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                'Throw (New UnauthorizedAccessException("Cannot autheticate user"))
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadCompanyInfo"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    Dim strIsPwdEncryp As String = ""
                    If dsTable.Tables(0).Columns.Contains("isemailpwd_encrypted") = True Then
                        strIsPwdEncryp = ", cfcompany_info.isemailpwd_encrypted = " & CInt(Int(dRow.Item("isemailpwd_encrypted"))) & " "
                    End If

                    strQ = "UPDATE  cfcompany_info " &
                            "SET     cfcompany_info.sendername = '" & dRow.Item("sendername").ToString.ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.senderaddress = '" & dRow.Item("senderaddress").ToString.ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.reference = '" & dRow.Item("reference").ToString.ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.mailserverip = '" & dRow.Item("mailserverip").ToString.ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.mailserverport = " & CInt(dRow.Item("mailserverport")) & " " &
                                  ", cfcompany_info.username = '" & dRow.Item("username").ToString.ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.password = '" & dRow.Item("password").ToString.ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.isloginssl = " & CInt(Int(dRow.Item("isloginssl"))) & " " &
                                  ", cfcompany_info.mail_body = '" & dRow.Item("mail_body").ToString.ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.applicant_declaration = '" & dRow.Item("applicant_declaration").ToString.ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.timezone_minute_diff = " & CInt(dRow.Item("timezone_minute_diff")) & " " &
                                  ", cfcompany_info.[image] = @image " &
                                  ", cfcompany_info.[isQualiCertAttachMandatory] = " & CInt(Int(dRow.Item("isQualiCertAttachMandatory"))) & " " &
                                  ", cfcompany_info.Date_Format = '" & dRow.Item("Date_Format").ToString.ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.DateSeparator = '" & dRow.Item("DateSeparator").ToString.ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.admin_email = '" & dRow.Item("admin_email").ToString.ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.email_type = " & CInt(dRow.Item("email_type")) & " " &
                                  ", cfcompany_info.ews_url = '" & dRow.Item("ews_url").ToString().ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.ews_domain = '" & dRow.Item("ews_domain").ToString().ReplaceQuotes().ToString() & "' " &
                                  ", cfcompany_info.MiddleNameMandatory = " & CInt(Int(dRow.Item("MiddleNameMandatory"))) & " " &
                                  ", cfcompany_info.GenderMandatory = " & CInt(Int(dRow.Item("GenderMandatory"))) & " " &
                                  ", cfcompany_info.BirthDateMandatory = " & CInt(Int(dRow.Item("BirthDateMandatory"))) & " " &
                                  ", cfcompany_info.Address1Mandatory = " & CInt(Int(dRow.Item("Address1Mandatory"))) & " " &
                                  ", cfcompany_info.Address2Mandatory = " & CInt(Int(dRow.Item("Address2Mandatory"))) & " " &
                                  ", cfcompany_info.CountryMandatory = " & CInt(Int(dRow.Item("CountryMandatory"))) & " " &
                                  ", cfcompany_info.StateMandatory = " & CInt(Int(dRow.Item("StateMandatory"))) & " " &
                                  ", cfcompany_info.CityMandatory = " & CInt(Int(dRow.Item("CityMandatory"))) & " " &
                                  ", cfcompany_info.PostCodeMandatory = " & CInt(Int(dRow.Item("PostCodeMandatory"))) & " " &
                                  ", cfcompany_info.MaritalStatusMandatory = " & CInt(Int(dRow.Item("MaritalStatusMandatory"))) & " " &
                                  ", cfcompany_info.Language1Mandatory = " & CInt(Int(dRow.Item("Language1Mandatory"))) & " " &
                                  ", cfcompany_info.NationalityMandatory = " & CInt(Int(dRow.Item("NationalityMandatory"))) & " " &
                                  ", cfcompany_info.OneSkillMandatory = " & CInt(Int(dRow.Item("OneSkillMandatory"))) & " " &
                                  ", cfcompany_info.OneQualificationMandatory = " & CInt(Int(dRow.Item("OneQualificationMandatory"))) & " " &
                                  ", cfcompany_info.OneJobExperienceMandatory = " & CInt(Int(dRow.Item("OneJobExperienceMandatory"))) & " " &
                                  ", cfcompany_info.OneReferenceMandatory = " & CInt(Int(dRow.Item("OneReferenceMandatory"))) & " " &
                                  ", cfcompany_info.appqualisortbyid = " & CInt(dRow.Item("ApplicantQualificationSortBy")) & " " &
                                  ", cfcompany_info.RegionMandatory = " & CInt(Int(dRow.Item("RegionMandatory"))) & " " &
                                  ", cfcompany_info.HighestQualificationMandatory = " & CInt(Int(dRow.Item("HighestQualificationMandatory"))) & " " &
                                  ", cfcompany_info.EmployerMandatory = " & CInt(Int(dRow.Item("EmployerMandatory"))) & " " &
                                  ", cfcompany_info.MotherTongueMandatory = " & CInt(Int(dRow.Item("MotherTongueMandatory"))) & " " &
                                  ", cfcompany_info.CurrentSalaryMandatory = " & CInt(Int(dRow.Item("CurrentSalaryMandatory"))) & " " &
                                  ", cfcompany_info.ExpectedSalaryMandatory = " & CInt(Int(dRow.Item("ExpectedSalaryMandatory"))) & " " &
                                  ", cfcompany_info.ExpectedBenefitsMandatory = " & CInt(Int(dRow.Item("ExpectedBenefitsMandatory"))) & " " &
                                  ", cfcompany_info.NoticePeriodMandatory = " & CInt(Int(dRow.Item("NoticePeriodMandatory"))) & " " &
                                  ", cfcompany_info.EarliestPossibleStartDateMandatory = " & CInt(Int(dRow.Item("EarliestPossibleStartDateMandatory"))) & " " &
                                  ", cfcompany_info.CommentsMandatory = " & CInt(Int(dRow.Item("CommentsMandatory"))) & " " &
                                  ", cfcompany_info.VacancyFoundOutFromMandatory = " & CInt(Int(dRow.Item("VacancyFoundOutFromMandatory"))) & " " &
                                  ", cfcompany_info.CompanyNameJobHistoryMandatory = " & CInt(Int(dRow.Item("CompanyNameJobHistoryMandatory"))) & " " &
                                  ", cfcompany_info.PositionHeldMandatory = " & CInt(Int(dRow.Item("PositionHeldMandatory"))) & " " &
                                  ", cfcompany_info.company_last_updated = GETDATE() " &
                                  strIsPwdEncryp &
                                  ", cfcompany_info.issendjobconfirmationemail =  " & CInt(Int(dRow.Item("SendJobConfirmationEmail"))) & " " &
                                  ", cfcompany_info.isvacancyAlert =  " & CInt(Int(dRow.Item("isVacancyalert"))) & " " &
                                  ", cfcompany_info.maxvacancyalert =  " & CInt(Int(dRow.Item("MaxVacancyAlert"))) & " " &
                                  ", cfcompany_info.isattachapplicantcv =  " & CInt(Int(dRow.Item("AttachApplicantCV"))) & " " &
                                  ", cfcompany_info.OneCurriculamVitaeMandatory =  " & CInt(Int(dRow.Item("OneCurriculamVitaeMandatory"))) & " " &
                                  ", cfcompany_info.OneCoverLetterMandatory =  " & CInt(Int(dRow.Item("OneCoverLetterMandatory"))) & " " &
                                  ", cfcompany_info.QualificationStartDateMandatory =  " & CInt(Int(dRow.Item("QualificationStartDateMandatory"))) & " " &
                                  ", cfcompany_info.QualificationAwardDateMandatory =  " & CInt(Int(dRow.Item("QualificationAwardDateMandatory"))) & " " &
                            "WHERE   cfcompany_info.companyunkid = " & objUser.WebClientID & " " &
                                    "AND cfcompany_info.company_code = '" & objUser.CompCode & "' "
                    'Sohail (30 Aug 2019) - [QualificationStartDateMandatory, QualificationAwardDateMandatory]
                    'Sohail (04 Jul 2019) - [OneCurriculamVitaeMandatory, isOneCoverLetterMandatory]

                    'Pinkal (18-May-2018) --  'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.[", cfcompany_info.isattachapplicantcv =  " & CInt(Int(dRow.Item("AttachApplicantCV"))) & " " & _]

                    'Pinkal (12-Feb-2018) -- 'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.[ ", cfcompany_info.issendjobconfirmationemail =  " & CInt(Int(dRow.Item("SendJobConfirmationEmail"))) & " " & _]


                    'Sohail (24 Jul 2017) - [CompanyNameJobHistoryMandatory, PositionHeldMandatory]
                    'Nilay (13 Apr 2017) -- [appqualisortbyid, RegionMandatory, HighestQualificationMandatory, EmployerMandatory, MotherTongueMandatory, 
                    'CurrentSalaryMandatory, ExpectedSalaryMandatory, ExpectedBenefitsMandatory, NoticePeriodMandatory, EarliestPossibleStartDateMandatory,
                    'CommentsMandatory, VacancyFoundOutFromMandatory]
                    'Sohail (05 Dec 2016) - [Date_Format, DateSeparator to OneReferenceMandatory]

                    'If IsDBNull(dRow.Item("image")) = True Then
                    '    objDataoperation.AddParameter("@image", SqlDbType.Binary, 0, DBNull.Value)
                    'Else
                    '    Dim imgLogo() As Byte = dRow.Item("image")
                    '    objDataoperation.AddParameter("@image", SqlDbType.Binary, imgLogo.Length, imgLogo)
                    'End If

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadCompanyInfo"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                If IsDBNull(dRow.Item("image")) = True Then
                                    cmd.Parameters.Add(New SqlParameter("@image", SqlDbType.Binary)).Value = DBNull.Value
                                Else
                                    Dim imgLogo() As Byte = DirectCast(dRow.Item("image"), Byte())
                                    cmd.Parameters.Add(New SqlParameter("@image", SqlDbType.Binary)).Value = imgLogo
                                End If

                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using

                Next

            End If


            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadCompanyInfo"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadFile(ByVal filebytes As Byte(), ByVal strFolderName As String, ByVal strFileName As String, ByRef strErrorMessage As String) As Boolean
        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot autheticate user. Please contact Aruti Support Team. Procedure : UploadFile"
                Return False
                Exit Function
            End If

            Dim strFilePath As String = ""
            If strFolderName.Trim = "" Then
                strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/") & strFileName
            Else
                If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & strFolderName)) = False Then
                    Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & strFolderName))
                End If
                'If Directory.Exists(Server.MapPath("~/UploadImage/" & strFolderName)) = False Then
                '    Directory.CreateDirectory(Server.MapPath("~/UploadImage/" & strFolderName))
                'End If
                strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & strFolderName & "/") & strFileName
            End If

            Dim ms As New MemoryStream(filebytes)
            Dim fs As New FileStream(strFilePath, FileMode.Create)
            ms.WriteTo(fs)

            ms.Close()
            fs.Close()
            fs.Dispose()

            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadFile"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function DeleteFile(ByVal strFolderName As String, ByVal strFileName As String, ByRef strErrorMessage As String) As Boolean
        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot autheticate user. Please contact Aruti Support Team. Procedure : DeleteFile"
                Return Nothing
                Exit Function
            End If

            Dim strFilePath As String = ""
            If strFolderName.Trim = "" Then
                strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/") & strFileName
            Else
                strFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & strFolderName & "/") & strFileName
            End If

            If File.Exists(strFilePath) = True Then
                File.Delete(strFilePath)
            End If

            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : DeleteFile"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    'Shani (02-Nov-2016) -- Start
    'Enhancement - Add Employee Configuration Settings form in Aruti Configuration [Assign by Sohail]
    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadJobQualificationTran(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadJobQualificationTran"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows
                    If blnOnlyPendingTransactions = True Then
                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For
                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM hrjob_qualification_tran WHERE hrjob_qualification_tran.companyunkid = " & objUser.WebClientID & " AND hrjob_qualification_tran.Comp_Code = '" & objUser.CompCode & "' AND hrjob_qualification_tran.jobqualificationtranunkid = " & CInt(dRow.Item("jobqualificationtranunkid")) & ") " &
                           "BEGIN " &
                           "    INSERT INTO hrjob_qualification_tran (" &
                           "         jobqualificationtranunkid " &
                           "        ,jobunkid" &
                           "        ,qualificationgroupunkid" &
                           "        ,qualificationunkid" &
                           "        ,isactive" &
                           "        ,Comp_Code" &
                           "        ,companyunkid" &
                           "        ) " &
                           "    SELECT " &
                           "         " & CInt(Int(dRow.Item("jobqualificationtranunkid"))) & " " &
                           "        ," & CInt(Int(dRow.Item("jobunkid"))) & " " &
                           "        ," & CInt(Int(dRow.Item("qualificationgroupunkid"))) & " " &
                           "        ," & CInt(Int(dRow.Item("qualificationunkid"))) & " " &
                           "        ," & CInt(Int(dRow.Item("isactive"))) & " " &
                           "        ,'" & objUser.CompCode & "' " &
                           "        ," & objUser.WebClientID & " " &
                           "END " &
                           "ELSE " &
                           "BEGIN " &
                           "    UPDATE  hrjob_qualification_tran SET " &
                           "     jobunkid = " & CInt(Int(dRow.Item("jobunkid"))) & " " &
                           "    ,qualificationgroupunkid = " & CInt(Int(dRow.Item("qualificationgroupunkid"))) & " " &
                           "    ,qualificationunkid = " & CInt(Int(dRow.Item("qualificationunkid"))) & " " &
                           "    ,isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                           "    WHERE   companyunkid = " & objUser.WebClientID & " " &
                           "        AND Comp_Code = '" & objUser.CompCode & "' " &
                           "        AND jobqualificationtranunkid = " & CInt(dRow.Item("jobqualificationtranunkid")) & " " &
                           "END "
                    'Sohail (23 Mar 2018) - [isactive]

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadJobQualificationTran"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using
                Next
            End If


            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadJobQualificationTran"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadJobSkillTran(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadJobSkillTran"
                Return False
                Exit Function
            End If

            'Dim objDataoperation As New eZeeCommonLib.clsDataOperation(True)
            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then
                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For
                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM hrjob_skill_tran WHERE hrjob_skill_tran.companyunkid = " & objUser.WebClientID & " AND hrjob_skill_tran.Comp_Code = '" & objUser.CompCode & "' AND hrjob_skill_tran.jobskilltranunkid = " & CInt(dRow.Item("jobskilltranunkid")) & ") " &
                           "BEGIN " &
                           "    INSERT INTO hrjob_skill_tran (" &
                           "         jobskilltranunkid " &
                           "        ,jobunkid" &
                           "        ,skillcategoryunkid" &
                           "        ,skillunkid" &
                           "        ,isactive" &
                           "        ,Comp_Code" &
                           "        ,companyunkid" &
                           "        ) " &
                           "    SELECT " &
                           "         " & CInt(dRow.Item("jobskilltranunkid")) & " " &
                           "        ," & CInt(dRow.Item("jobunkid")) & " " &
                           "        ," & CInt(dRow.Item("skillcategoryunkid")) & " " &
                           "        ," & CInt(dRow.Item("skillunkid")) & " " &
                           "        ," & CInt(Int(dRow.Item("isactive"))) & " " &
                           "        ,'" & objUser.CompCode & "' " &
                           "        ," & objUser.WebClientID & " " &
                           "END " &
                           "ELSE " &
                           "BEGIN " &
                           "    UPDATE  hrjob_skill_tran SET " &
                           "         jobunkid = " & CInt(dRow.Item("jobunkid")) & " " &
                           "        ,skillcategoryunkid = " & CInt(dRow.Item("skillcategoryunkid")) & " " &
                           "        ,skillunkid = " & CInt(dRow.Item("skillunkid")) & " " &
                           "        ,isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                           "    WHERE   companyunkid = " & objUser.WebClientID & " " &
                           "        AND Comp_Code = '" & objUser.CompCode & "' " &
                           "        AND jobskilltranunkid = " & CInt(dRow.Item("jobskilltranunkid")) & " " &
                           "END "
                    'Sohail (23 Mar 2018) - [isactive]

                    'dsList = objDataoperation.ExecQuery(strQ, "List")

                    'If objDataoperation.ErrorMessage <> "" Then
                    '    strErrorMessage = objDataoperation.ErrorMessage & ",  Procedure : UploadJobSkillTran"
                    '    Return False
                    '    Exit Function
                    'End If
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using
                Next
            End If

            Return True
        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadJobSkillTran"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function
    'Shani (02-Nov-2016) -- End

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadQualificationGroup(ByRef strErrorMessage As String, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        Dim strQ1 As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try
            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadQualificationGroup"
                Return False
                Exit Function
            End If

            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            strQ = "UPDATE cfcommon_master " &
                    "SET isactive = 1 " &
                    "WHERE mastertype = 18 " &
                          "AND masterunkid IN (SELECT DISTINCT qualificationgroupunkid FROM hrqualification_master WHERE isactive = 1 ) "

            strQ1 = "UPDATE cfcommon_master " &
                    "SET isactive = 0 " &
                    "WHERE mastertype = 18 " &
                          "AND masterunkid NOT IN (SELECT DISTINCT qualificationgroupunkid FROM hrqualification_master WHERE isactive = 1 ) "

            Using con As New SqlConnection(strConn)

                con.Open()

                Using da As New SqlDataAdapter()
                    Using cmd As New SqlCommand(strQ, con)

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        cmd.ExecuteNonQuery()

                        If con.State = ConnectionState.Closed Then
                            con.ConnectionString = strConn
                            con.Open()
                        End If

                        cmd.CommandType = CommandType.Text
                        cmd.CommandTimeout = 0
                        cmd.Connection = con
                        cmd.CommandText = strQ1
                        cmd.ExecuteNonQuery()

                    End Using
                End Using
            End Using

            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadQualificationGroup"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    'Sohail (18 Feb 2020) -- Start
    'NMB Enhancement # : On job master,  Need a tab to define the language(s) attached to the job. Those languages will be displayed on job report.
    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadJobLanguageTran(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadJobLanguageTran"
                Return False
                Exit Function
            End If

            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then
                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For
                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM hrjob_language_tran WHERE hrjob_language_tran.companyunkid = " & objUser.WebClientID & " AND hrjob_language_tran.Comp_Code = '" & objUser.CompCode & "' AND hrjob_language_tran.joblanguagetranunkid = " & CInt(dRow.Item("joblanguagetranunkid")) & ") " &
                           "BEGIN " &
                           "    INSERT INTO hrjob_language_tran (" &
                           "         joblanguagetranunkid " &
                           "        ,jobunkid" &
                           "        ,masterunkid" &
                           "        ,isactive" &
                           "        ,Comp_Code" &
                           "        ,companyunkid" &
                           "        ) " &
                           "    SELECT " &
                           "         " & CInt(dRow.Item("joblanguagetranunkid")) & " " &
                           "        ," & CInt(dRow.Item("jobunkid")) & " " &
                           "        ," & CInt(dRow.Item("masterunkid")) & " " &
                           "        ," & CInt(Int(dRow.Item("isactive"))) & " " &
                           "        ,'" & objUser.CompCode & "' " &
                           "        ," & objUser.WebClientID & " " &
                           "END " &
                           "ELSE " &
                           "BEGIN " &
                           "    UPDATE  hrjob_language_tran SET " &
                           "         jobunkid = " & CInt(dRow.Item("jobunkid")) & " " &
                           "        ,masterunkid = " & CInt(dRow.Item("masterunkid")) & " " &
                           "        ,isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                           "    WHERE   companyunkid = " & objUser.WebClientID & " " &
                           "        AND Comp_Code = '" & objUser.CompCode & "' " &
                           "        AND joblanguagetranunkid = " & CInt(dRow.Item("joblanguagetranunkid")) & " " &
                           "END "

                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using
                Next
            End If

            Return True
        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadJobLanguageTran"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
            Return False
        End Try
    End Function
    'Sohail (18 Feb 2020) -- End

    'Sohail (06 Nov 2019) -- Start
    'NMB Enhancement # TC029: On the recruitment portal, Vacancy found out from section should be a drop down menu and the items on the drop down should be definable in a master.
    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadConfiguration(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadConfiguration"
                Return False
                Exit Function
            End If

            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then
                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For
                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM cfconfiguration WHERE cfconfiguration.companyunkid = " & objUser.WebClientID & " AND cfconfiguration.Comp_Code = '" & objUser.CompCode & "' AND cfconfiguration.configunkid = " & CInt(dRow.Item("configunkid")) & ") " &
                           "BEGIN " &
                           "    INSERT INTO cfconfiguration (" &
                           "         configunkid " &
                           "       , key_name" &
                           "       , key_value" &
                           "       , companyunkid" &
                           "       , Comp_Code" &
                           "        ) " &
                           "    SELECT " &
                           "         " & CInt(dRow.Item("configunkid")) & " " &
                           "       , '" & dRow.Item("key_name").ToString & "' " &
                           "       , '" & dRow.Item("key_value").ToString & "' " &
                           "       , " & objUser.WebClientID & " " &
                           "       , '" & objUser.CompCode & "' " &
                           "END " &
                           "ELSE " &
                           "BEGIN " &
                           "    UPDATE  cfconfiguration SET " &
                           "         configunkid = " & CInt(dRow.Item("configunkid")) & " " &
                           "        ,key_name = '" & dRow.Item("key_name").ToString & "' " &
                           "        ,key_value = '" & dRow.Item("key_value").ToString & "' " &
                           "    WHERE   companyunkid = " & objUser.WebClientID & " " &
                           "        AND Comp_Code = '" & objUser.CompCode & "' " &
                           "        AND configunkid = " & CInt(dRow.Item("configunkid")) & " " &
                           "END "

                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using
                Next
            End If

            Return True
        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadConfiguration"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
            Return False
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadLetterType(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadLetterType"
                Return False
                Exit Function
            End If

            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then
                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For
                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM hrlettertype_master WHERE hrlettertype_master.companyunkid = " & objUser.WebClientID & " AND hrlettertype_master.Comp_Code = '" & objUser.CompCode & "' AND hrlettertype_master.lettertypeunkid = " & CInt(dRow.Item("lettertypeunkid")) & ") " &
                           "BEGIN " &
                           "    INSERT INTO hrlettertype_master (" &
                           "         lettertypeunkid " &
                           "       , alias" &
                           "       , lettermasterunkid " &
                           "       , lettername" &
                           "       , lettername1" &
                           "       , lettername2" &
                           "       , lettercontent" &
                           "       , isactive" &
                           "       , fieldtypeid" &
                           "       , companyunkid" &
                           "       , Comp_Code" &
                           "        ) " &
                           "    SELECT " &
                           "         " & CInt(dRow.Item("lettertypeunkid")) & " " &
                           "       , '" & dRow.Item("alias").ToString & "' " &
                           "       , " & CInt(dRow.Item("lettermasterunkid")) & " " &
                           "       , '" & dRow.Item("lettername").ToString & "' " &
                           "       , '" & dRow.Item("lettername1").ToString & "' " &
                           "       , '" & dRow.Item("lettername2").ToString & "' " &
                           "       , '" & dRow.Item("lettercontent").ToString.Replace("'", "''") & "' " &
                           "       , " & CInt(Int(dRow.Item("isactive"))) & " " &
                           "       , " & CInt(dRow.Item("fieldtypeid")) & " " &
                           "       , " & objUser.WebClientID & " " &
                           "       , '" & objUser.CompCode & "' " &
                           "END " &
                           "ELSE " &
                           "BEGIN " &
                           "    UPDATE  hrlettertype_master SET " &
                           "         lettertypeunkid = " & CInt(dRow.Item("lettertypeunkid")) & " " &
                           "        ,alias = '" & dRow.Item("alias").ToString & "' " &
                           "        ,lettermasterunkid = " & CInt(dRow.Item("lettermasterunkid")) & " " &
                           "        ,lettername = '" & dRow.Item("lettername").ToString & "' " &
                           "        ,lettername1 = '" & dRow.Item("lettername1").ToString & "' " &
                           "        ,lettername2 = '" & dRow.Item("lettername2").ToString & "' " &
                           "        ,lettercontent = '" & dRow.Item("lettercontent").ToString.Replace("'", "''") & "' " &
                           "        ,isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                           "        ,fieldtypeid = " & CInt(dRow.Item("fieldtypeid")) & " " &
                           "    WHERE   companyunkid = " & objUser.WebClientID & " " &
                           "        AND Comp_Code = '" & objUser.CompCode & "' " &
                           "        AND lettertypeunkid = " & CInt(dRow.Item("lettertypeunkid")) & " " &
                           "END "

                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using
                Next
            End If

            Return True
        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadLetterType"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
            Return False
        End Try
    End Function
    'Sohail (06 Nov 2019) -- End

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadJobTran(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadJobSkillTran"
                Return False
                Exit Function
            End If

            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then
                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For
                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM hrjob_skill_tran WHERE hrjob_skill_tran.companyunkid = " & objUser.WebClientID & " AND hrjob_skill_tran.Comp_Code = '" & objUser.CompCode & "' AND hrjob_skill_tran.jobskilltranunkid = " & CInt(dRow.Item("jobskilltranunkid")) & ") " &
                           "BEGIN " &
                           "    INSERT INTO hrjob_skill_tran (" &
                           "         jobskilltranunkid " &
                           "        ,jobunkid" &
                           "        ,skillcategoryunkid" &
                           "        ,skillunkid" &
                           "        ,isactive" &
                           "        ,Comp_Code" &
                           "        ,companyunkid" &
                           "        ) " &
                           "    SELECT " &
                           "         " & CInt(dRow.Item("jobskilltranunkid")) & " " &
                           "        ," & CInt(dRow.Item("jobunkid")) & " " &
                           "        ," & CInt(dRow.Item("skillcategoryunkid")) & " " &
                           "        ," & CInt(dRow.Item("skillunkid")) & " " &
                           "        ," & CInt(Int(dRow.Item("isactive"))) & " " &
                           "        ,'" & objUser.CompCode & "' " &
                           "        ," & objUser.WebClientID & " " &
                           "END " &
                           "ELSE " &
                           "BEGIN " &
                           "    UPDATE  hrjob_skill_tran SET " &
                           "         jobunkid = " & CInt(dRow.Item("jobunkid")) & " " &
                           "        ,skillcategoryunkid = " & CInt(dRow.Item("skillcategoryunkid")) & " " &
                           "        ,skillunkid = " & CInt(dRow.Item("skillunkid")) & " " &
                           "        ,isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                           "    WHERE   companyunkid = " & objUser.WebClientID & " " &
                           "        AND Comp_Code = '" & objUser.CompCode & "' " &
                           "        AND jobskilltranunkid = " & CInt(dRow.Item("jobskilltranunkid")) & " " &
                           "END "
                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using
                Next
            End If

            Return True
        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadJobSkillTran"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function



    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadJobMaster(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""

        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then

                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadSkillMaster"
                Return False
                Exit Function
            End If

            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then

                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For

                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM hrjob_master WHERE hrjob_master.companyunkid = " & objUser.WebClientID & " AND hrjob_master.Comp_Code = '" & objUser.CompCode & "' AND hrjob_master.jobunkid = " & CInt(dRow.Item("jobunkid")) & ") " &
                                "BEGIN " &
                                    "INSERT INTO hrjob_master (Comp_Code, companyunkid,jobunkid " &
                                    ",jobgroupunkid,jobheadunkid,jobunitunkid,jobsectionunkid,jobgradeunkid,job_code,job_name, " &
                                    "report_tounkid,create_date,total_position,terminate_date,  " &
                                    "desciription,isactive,job_name1,job_name2,job_level,teamunkid,working_hrs,experience_comment, " &
                                    "experience_month,experience_year,jobclassgroupunkid,jobdepartmentunkid,indirectreport_tounkid,  " &
                                    "jobbranchunkid,jobdepartmentgrpunkid,jobunitgrpunkid,jobclassunkid,jobgradelevelunkid,jobsectiongrpunkid,critical  " &
                                    ",jobtypeunkid ) " &
                                    "SELECT '" & objUser.CompCode & "' " &
                                            ", " & objUser.WebClientID & " " &
                                            ", " & CInt(dRow.Item("jobunkid")) & " " &
                                            ", " & CInt(dRow.Item("jobgroupunkid")) & " " &
                                            ", " & CInt(dRow.Item("jobheadunkid")) & " " &
                                            ", " & CInt(dRow.Item("jobunitunkid")) & " " &
                                            ", " & CInt(dRow.Item("jobsectionunkid")) & " " &
                                            ", " & CInt(dRow.Item("jobgradeunkid")) & " " &
                                            ", '" & dRow.Item("job_code").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("job_name").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(dRow.Item("report_tounkid")) & " " &
                                            ", '" & If(IsDBNull(dRow.Item("create_date")), DBNull.Value, CDate(dRow.Item("create_date")).ToString("yyyy-MM-dd hh:mm:ss")) & "' " &
                                            ", " & CInt(dRow.Item("total_position")) & " " &
                                            ", '" & If(IsDBNull(dRow.Item("terminate_date")), DBNull.Value, CDate(dRow.Item("terminate_date")).ToString("yyyy-MM-dd hh:mm:ss")) & "' " &
                                            ", '" & dRow.Item("desciription").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(dRow.Item("isactive")) & " " &
                                            ", '" & dRow.Item("job_name1").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("job_name2").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(dRow.Item("job_level")) & " " &
                                            ", " & If(IsDBNull(dRow.Item("teamunkid")), 0, CInt(dRow.Item("teamunkid"))) & " " &
                                            ", '" & dRow.Item("working_hrs").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("experience_comment").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(dRow.Item("experience_month")) & " " &
                                            ", " & CInt(dRow.Item("experience_year")) & " " &
                                            ", " & CInt(dRow.Item("jobclassgroupunkid")) & " " &
                                            ", " & CInt(dRow.Item("jobdepartmentunkid")) & " " &
                                            ", " & CInt(dRow.Item("indirectreport_tounkid")) & " " &
                                            ", " & CInt(dRow.Item("jobbranchunkid")) & " " &
                                            ", " & CInt(dRow.Item("jobdepartmentgrpunkid")) & " " &
                                            ", " & CInt(dRow.Item("jobunitgrpunkid")) & " " &
                                            ", " & CInt(dRow.Item("jobclassunkid")) & " " &
                                            ", " & CInt(dRow.Item("jobgradelevelunkid")) & " " &
                                            ", " & CInt(dRow.Item("jobsectiongrpunkid")) & " " &
                                            ", " & If(IsDBNull(dRow.Item("critical")), 0, CInt(dRow.Item("critical"))) & " " &
                                            ", " & CInt(dRow.Item("jobtypeunkid")) & " " &
                                    "WHERE " & CInt(Int(dRow.Item("isactive"))) & " = 1 " &
                                "END " &
                            "ELSE " &
                                "BEGIN " &
                                    "UPDATE  hrjob_master " &
                                    "SET    hrjob_master.jobgroupunkid= " & CInt(dRow.Item("jobgroupunkid")) & " " &
                                            ",hrjob_master.jobheadunkid= " & CInt(dRow.Item("jobheadunkid")) & " " &
                                            ",hrjob_master.jobunitunkid= " & CInt(dRow.Item("jobunitunkid")) & " " &
                                            ",hrjob_master.jobsectionunkid=" & CInt(dRow.Item("jobsectionunkid")) & " " &
                                            ",hrjob_master.jobgradeunkid= " & CInt(dRow.Item("jobgradeunkid")) & " " &
                                            ",hrjob_master.job_code= '" & dRow.Item("job_code").ToString.ReplaceQuotes().ToString() & "' " &
                                            ",hrjob_master.job_name= '" & dRow.Item("job_name").ToString.ReplaceQuotes().ToString() & "' " &
                                            ",hrjob_master.report_tounkid= " & CInt(dRow.Item("report_tounkid")) & " " &
                                            ",hrjob_master.create_date= '" & If(IsDBNull(dRow.Item("create_date")), DBNull.Value, CDate(dRow.Item("create_date")).ToString("yyyy-MM-dd hh:mm:ss")) & "' " &
                                            ",hrjob_master.total_position= " & CInt(dRow.Item("total_position")) & " " &
                                            ",hrjob_master.terminate_date= '" & If(IsDBNull(dRow.Item("terminate_date")), DBNull.Value, CDate(dRow.Item("terminate_date")).ToString("yyyy-MM-dd hh:mm:ss")) & "' " &
                                            ",hrjob_master.desciription= '" & dRow.Item("desciription").ToString.ReplaceQuotes().ToString() & "' " &
                                            ",hrjob_master.isactive= " & CInt(dRow.Item("isactive")) & " " &
                                            ",hrjob_master.job_name1= '" & dRow.Item("job_name1").ToString.ReplaceQuotes().ToString() & "' " &
                                            ",hrjob_master.job_name2= '" & dRow.Item("job_name2").ToString.ReplaceQuotes().ToString() & "' " &
                                            ",hrjob_master.job_level= " & CInt(dRow.Item("job_level")) & " " &
                                            ",hrjob_master.teamunkid= " & If(IsDBNull(dRow.Item("teamunkid")), 0, CInt(dRow.Item("teamunkid"))) & " " &
                                            ",hrjob_master.working_hrs= '" & dRow.Item("working_hrs").ToString.ReplaceQuotes().ToString() & "' " &
                                            ",hrjob_master.experience_comment= '" & dRow.Item("experience_comment").ToString.ReplaceQuotes().ToString() & "' " &
                                            ",hrjob_master.experience_month= " & CInt(dRow.Item("experience_month")) & " " &
                                            ",hrjob_master.experience_year= " & CInt(dRow.Item("experience_year")) & " " &
                                            ",hrjob_master.jobclassgroupunkid= " & CInt(dRow.Item("jobclassgroupunkid")) & " " &
                                            ",hrjob_master.jobdepartmentunkid= " & CInt(dRow.Item("jobdepartmentunkid")) & " " &
                                            ",hrjob_master.indirectreport_tounkid= " & CInt(dRow.Item("indirectreport_tounkid")) & " " &
                                            ",hrjob_master.jobbranchunkid= " & CInt(dRow.Item("jobbranchunkid")) & " " &
                                            ",hrjob_master.jobdepartmentgrpunkid= " & CInt(dRow.Item("jobdepartmentgrpunkid")) & " " &
                                            ",hrjob_master.jobunitgrpunkid= " & CInt(dRow.Item("jobunitgrpunkid")) & " " &
                                            ",hrjob_master.jobclassunkid= " & CInt(dRow.Item("jobclassunkid")) & " " &
                                            ",hrjob_master.jobgradelevelunkid= " & CInt(dRow.Item("jobgradelevelunkid")) & " " &
                                            ",hrjob_master.jobsectiongrpunkid= " & CInt(dRow.Item("jobsectiongrpunkid")) & " " &
                                            ",hrjob_master.critical = " & If(IsDBNull(dRow.Item("critical")), 0, CInt(dRow.Item("critical"))) & " " &
                                            ",hrjob_master.jobtypeunkid =" & CInt(dRow.Item("jobtypeunkid")) & " " &
                                            "WHERE hrjob_master.companyunkid = " & objUser.WebClientID & " " &
                                            "AND hrjob_master.Comp_Code = '" & objUser.CompCode & "' " &
                                            "AND hrjob_master.jobunkid = " & CInt(dRow.Item("jobunkid")) & " " &
                                            "END "

                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using

                Next

            End If


            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadJobMaster"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadEmailSetup(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadConfiguration"
                Return False
                Exit Function
            End If

            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then
                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For
                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM cfemail_setup WHERE cfemail_setup.companyunkid = " & objUser.WebClientID & " AND cfemail_setup.Comp_Code = '" & objUser.CompCode & "' AND cfemail_setup.emailsetupunkid = " & CInt(dRow.Item("emailsetupunkid")) & ") " &
                           "BEGIN " &
                           "    INSERT INTO cfemail_setup (" &
                           "         emailsetupunkid " &
                           "       , sendername" &
                           "       , senderaddress" &
                           "       , reference" &
                           "       , mailserverip" &
                           "       , mailserverport" &
                           "       , username" &
                           "       , password" &
                           "       , isloginssl" &
                           "       , iscrt_authenticated" &
                           "       , isbypassproxy" &
                           "       , mail_body" &
                           "       , email_type" &
                           "       , ews_url" &
                           "       , ews_domain" &
                           "       , isvoid " &
                           "       , companyunkid" &
                           "       , Comp_Code" &
                           "        ) " &
                           "    SELECT " &
                           "         " & CInt(dRow.Item("emailsetupunkid")) & " " &
                           "       , '" & dRow.Item("sendername").ToString & "' " &
                           "       , '" & dRow.Item("senderaddress").ToString & "' " &
                           "       , '" & dRow.Item("reference").ToString & "' " &
                           "       , '" & dRow.Item("mailserverip").ToString & "' " &
                           "       , " & CInt(dRow.Item("mailserverport").ToString) & " " &
                           "       , '" & dRow.Item("username").ToString & "' " &
                           "       , '" & dRow.Item("password").ToString & "' " &
                           "       , " & CInt(Int(dRow.Item("isloginssl"))) & " " &
                           "       , " & CInt(Int(dRow.Item("iscrt_authenticated"))) & " " &
                           "       , " & CInt(Int(dRow.Item("isbypassproxy"))) & " " &
                           "       , '" & dRow.Item("mail_body").ToString & "' " &
                           "       , " & CInt(dRow.Item("email_type").ToString) & " " &
                           "       , '" & dRow.Item("ews_url").ToString & "' " &
                           "       , '" & dRow.Item("ews_domain").ToString & "' " &
                           "       , " & CInt(Int(dRow.Item("isvoid"))) & " " &
                           "       , " & objUser.WebClientID & " " &
                           "       , '" & objUser.CompCode & "' " &
                           "END " &
                           "ELSE " &
                           "BEGIN " &
                           "    UPDATE  cfemail_setup SET " &
                           "         emailsetupunkid = " & CInt(dRow.Item("emailsetupunkid")) & " " &
                           "        ,sendername = '" & dRow.Item("sendername").ToString & "' " &
                           "        ,senderaddress = '" & dRow.Item("senderaddress").ToString & "' " &
                           "        ,reference = '" & dRow.Item("reference").ToString & "' " &
                           "        ,mailserverip = '" & dRow.Item("mailserverip").ToString & "' " &
                           "        ,mailserverport = " & CInt(dRow.Item("mailserverport").ToString) & " " &
                           "        ,username = '" & dRow.Item("username").ToString & "' " &
                           "        ,password = '" & dRow.Item("password").ToString & "' " &
                           "        ,isloginssl = " & CInt(Int(dRow.Item("isloginssl"))) & " " &
                           "        ,iscrt_authenticated = " & CInt(Int(dRow.Item("iscrt_authenticated"))) & " " &
                           "        ,isbypassproxy = " & CInt(Int(dRow.Item("isbypassproxy"))) & " " &
                           "        ,mail_body = '" & dRow.Item("mail_body").ToString & "' " &
                           "        ,email_type = " & CInt(dRow.Item("email_type").ToString) & " " &
                           "        ,ews_url = '" & dRow.Item("ews_url").ToString & "' " &
                           "        ,ews_domain = '" & dRow.Item("ews_domain").ToString & "' " &
                           "        ,isvoid = " & CInt(Int(dRow.Item("isvoid"))) & " " &
                           "    WHERE   companyunkid = " & objUser.WebClientID & " " &
                           "        AND Comp_Code = '" & objUser.CompCode & "' " &
                           "        AND emailsetupunkid = " & CInt(dRow.Item("emailsetupunkid")) & " " &
                           "END "

                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using
                Next
            End If

            Return True
        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadEmailSetup"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
            Return False
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadClassGroup(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadConfiguration"
                Return False
                Exit Function
            End If

            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then
                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For
                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM hrclassgroup_master WHERE hrclassgroup_master.companyunkid = " & objUser.WebClientID & " AND hrclassgroup_master.Comp_Code = '" & objUser.CompCode & "' AND hrclassgroup_master.classgroupunkid = " & CInt(dRow.Item("classgroupunkid")) & ") " &
                           "BEGIN " &
                           "    INSERT INTO hrclassgroup_master (" &
                           "         classgroupunkid " &
                           "       , code " &
                           "       , name " &
                           "       , description " &
                           "       , isactive " &
                           "       , name1 " &
                           "       , name2 " &
                           "       , companyunkid" &
                           "       , Comp_Code" &
                           "        ) " &
                           "    SELECT " &
                           "         " & CInt(dRow.Item("classgroupunkid")) & " " &
                           "       , '" & dRow.Item("code").ToString & "' " &
                           "       , '" & dRow.Item("name").ToString & "' " &
                           "       , '" & dRow.Item("description").ToString & "' " &
                           "       , " & CInt(Int(dRow.Item("isactive"))) & " " &
                           "       , '" & dRow.Item("name1").ToString & "' " &
                           "       , '" & dRow.Item("name2").ToString & "' " &
                           "       , " & objUser.WebClientID & " " &
                           "       , '" & objUser.CompCode & "' " &
                           "END " &
                           "ELSE " &
                           "BEGIN " &
                           "    UPDATE  hrclassgroup_master SET " &
                           "         classgroupunkid = " & CInt(dRow.Item("classgroupunkid")) & " " &
                           "        ,code = '" & dRow.Item("code").ToString & "' " &
                           "        ,name = '" & dRow.Item("name").ToString & "' " &
                           "        ,description = '" & dRow.Item("description").ToString & "' " &
                           "        ,isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                           "        ,name1 = '" & dRow.Item("name1").ToString & "' " &
                           "        ,name2 = '" & dRow.Item("name2").ToString & "' " &
                           "    WHERE   companyunkid = " & objUser.WebClientID & " " &
                           "        AND Comp_Code = '" & objUser.CompCode & "' " &
                           "        AND classgroupunkid = " & CInt(dRow.Item("classgroupunkid")) & " " &
                           "END "

                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using
                Next
            End If

            Return True
        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadClassGroup"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
            Return False
        End Try
    End Function

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadClass(ByRef strErrorMessage As String, ByVal dsTable As DataSet, ByVal blnOnlyPendingTransactions As Boolean) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadConfiguration"
                Return False
                Exit Function
            End If

            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows

                    If blnOnlyPendingTransactions = True Then
                        If IsDBNull(dRow.Item("Syncdatetime")) = False Then Continue For
                    End If

                    strQ = "IF NOT EXISTS(SELECT * FROM hrclasses_master WHERE hrclasses_master.companyunkid = " & objUser.WebClientID & " AND hrclasses_master.Comp_Code = '" & objUser.CompCode & "' AND hrclasses_master.classesunkid = " & CInt(dRow.Item("classesunkid")) & ") " &
                           "BEGIN " &
                           "    INSERT INTO hrclasses_master (" &
                           "         classesunkid " &
                           "       , classgroupunkid " &
                           "       , code " &
                           "       , name " &
                           "       , description " &
                           "       , isactive " &
                           "       , name1 " &
                           "       , name2 " &
                           "       , companyunkid" &
                           "       , Comp_Code" &
                           "        ) " &
                           "    SELECT " &
                           "         " & CInt(dRow.Item("classesunkid")) & " " &
                           "       , " & CInt(dRow.Item("classgroupunkid")) & " " &
                           "       , '" & dRow.Item("code").ToString & "' " &
                           "       , '" & dRow.Item("name").ToString & "' " &
                           "       , '" & dRow.Item("description").ToString & "' " &
                           "       , " & CInt(Int(dRow.Item("isactive"))) & " " &
                           "       , '" & dRow.Item("name1").ToString & "' " &
                           "       , '" & dRow.Item("name2").ToString & "' " &
                           "       , " & objUser.WebClientID & " " &
                           "       , '" & objUser.CompCode & "' " &
                           "END " &
                           "ELSE " &
                           "BEGIN " &
                           "    UPDATE  hrclasses_master SET " &
                           "         classesunkid = " & CInt(dRow.Item("classesunkid")) & " " &
                           "        ,classgroupunkid = " & CInt(dRow.Item("classgroupunkid")) & " " &
                           "        ,code = '" & dRow.Item("code").ToString & "' " &
                           "        ,name = '" & dRow.Item("name").ToString & "' " &
                           "        ,description = '" & dRow.Item("description").ToString & "' " &
                           "        ,isactive = " & CInt(Int(dRow.Item("isactive"))) & " " &
                           "        ,name1 = '" & dRow.Item("name1").ToString & "' " &
                           "        ,name2 = '" & dRow.Item("name2").ToString & "' " &
                           "    WHERE   companyunkid = " & objUser.WebClientID & " " &
                           "        AND Comp_Code = '" & objUser.CompCode & "' " &
                           "        AND classesunkid = " & CInt(dRow.Item("classesunkid")) & " " &
                           "END "

                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using
                Next
            End If

            Return True
        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadClass"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
            Return False
        End Try
    End Function


    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1400) NMB - Post assigned employee assets to P2P

    <WebMethod()>
    <SoapHeader("objUser", Required:=True)>
    Public Function UploadApplicantFeedback(ByRef strErrorMessage As String, ByVal dsTable As DataSet) As Boolean
        Dim strQ As String = ""
        'Dim dsList As DataSet = Nothing
        Dim dsList As New DataSet

        Try

            strErrorMessage = ""
            If objUser.IsUserValid() = False Then
                strErrorMessage = "Cannot authenticate user. Please contact Aruti Support Team. Procedure : UploadApplicantFeedback" & ";" & objUser.WebClientID.ToString & ";" & objUser.CompCode & ";" & objUser.AuthenticationCode & ";" ' & ConfigurationManager.ConnectionStrings("paydb").ToString()
                Return False
                Exit Function
            End If

            Dim strConn As String = ConfigurationManager.ConnectionStrings("paydb").ToString()

            If dsTable IsNot Nothing Then
                For Each dRow As DataRow In dsTable.Tables(0).Rows


                    '" IF NOT EXISTS(SELECT * FROM rcapplicantfeedback_tran WHERE rcapplicantfeedback_tran.isvoid = 0 AND rcapplicantfeedback_tran.companyunkid = " & objUser.WebClientID & " AND rcapplicantfeedback_tran.Comp_Code = '" & objUser.CompCode & "' AND rcapplicantfeedback_tran.recipient_address = '" & dRow.Item("recipient_address").ToString() & "' AND rcapplicantfeedback_tran.vacancyunkid = " & CInt(dRow.Item("vacancyunkid")) & ") " &
                    '          " BEGIN " &

                    strQ = " INSERT INTO rcapplicantfeedback_tran (Comp_Code,companyunkid,sender_address,recipient_address,send_datetime,sent_data,subject,vacancytypeunkid,vacancyunkid,viewtypeunkid,isvoid,voidreason) " &
                              "     SELECT '" & objUser.CompCode & "' " &
                                            ", " & objUser.WebClientID & " " &
                                            ", '" & dRow.Item("sender_address").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("recipient_address").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & IIf(IsDBNull(dRow.Item("send_datetime")), DBNull.Value, CDate(dRow.Item("send_datetime"))) & "' " &
                                            ", '" & dRow.Item("sent_data").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", '" & dRow.Item("subject").ToString.ReplaceQuotes().ToString() & "' " &
                                            ", " & CInt(Int(dRow.Item("vacancytypeunkid"))) & " " &
                                            ", " & CInt(Int(dRow.Item("vacancyunkid"))) & " " &
                                            ", " & CInt(Int(dRow.Item("viewtypeunkid"))) & " " &
                                            ", " & CInt(Int(dRow.Item("isvoid"))) & " " &
                                            ", '" & dRow.Item("voidreason").ToString.ReplaceQuotes().ToString() & "' "
                    '" END "
                    '" ELSE " &
                    '          " BEGIN " &
                    '          "       UPDATE rcapplicantfeedback_tran " &
                    '          "       SET  rcapplicantfeedback_tran.sender_address = '" & dRow.Item("sender_address").ToString.ReplaceQuotes().ToString() & "' " &
                    '          ",      rcapplicantfeedback_tran.recipient_address =  '" & dRow.Item("recipient_address").ToString.ReplaceQuotes().ToString() & "' " &
                    '          ",      rcapplicantfeedback_tran.send_datetime = '" & CDate(dRow.Item("send_datetime")) & "' " &
                    '          ",      rcapplicantfeedback_tran.sent_data = '" & dRow.Item("sent_data").ToString.ReplaceQuotes().ToString() & "' " &
                    '          ",      rcapplicantfeedback_tran.subject = '" & dRow.Item("subject").ToString.ReplaceQuotes().ToString() & "' " &
                    '          ",      rcapplicantfeedback_tran.vacancytypeunkid = " & CInt(Int(dRow.Item("vacancytypeunkid"))) & " " &
                    '          ",      rcapplicantfeedback_tran.vacancyunkid = " & CInt(Int(dRow.Item("vacancyunkid"))) & " " &
                    '          ",      rcapplicantfeedback_tran.viewtypeunkid = " & CInt(Int(dRow.Item("viewtypeunkid"))) & " " &
                    '          "       WHERE rcapplicantfeedback_tran.isvoid = 0   " &
                    '          "       AND  rcapplicantfeedback_tran.companyunkid = " & objUser.WebClientID & " " &
                    '          "       AND rcapplicantfeedback_tran.Comp_Code = '" & objUser.CompCode & "' " &
                    '          "       AND rcapplicantfeedback_tran.recipient_address = '" & dRow.Item("recipient_address").ToString() & "' " &
                    '          "       AND rcapplicantfeedback_tran.vacancyunkid = " & CInt(dRow.Item("vacancyunkid")) & " " &
                    '          " END "


                    Using con As New SqlConnection(strConn)

                        con.Open()

                        Using da As New SqlDataAdapter()
                            Using cmd As New SqlCommand(strQ, con)

                                cmd.CommandType = CommandType.Text
                                cmd.CommandTimeout = 0
                                da.SelectCommand = cmd
                                da.Fill(dsList)

                            End Using
                        End Using
                    End Using

                Next

            End If


            Return True

        Catch ex As Exception
            strErrorMessage = ex.Message & "; " & ex.StackTrace & ",  Procedure : UploadApplicantFeedback"
            If ex.InnerException IsNot Nothing Then
                strErrorMessage &= "; " & ex.InnerException.Message
            End If
        End Try
    End Function

    'Pinkal (16-Oct-2023) -- End


#End Region

    'Sohail (08 Oct 2018) -- Start
    'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
#Region " Methods "

    Public Sub RemoveDateTimeZones(ByVal ds As DataSet)
        Try
            If ds IsNot Nothing Then
                For Each dt As DataTable In ds.Tables
                    For Each Col As DataColumn In dt.Columns
                        If Col.DataType Is GetType(Date) Then
                            Col.DateTimeMode = DataSetDateTime.Unspecified
                        End If
                    Next
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

#End Region
    'Sohail (08 Oct 2018) -- End

End Class