﻿Imports System.Data.SqlClient

Public Class clsPolicy_tran

    Private Shared ReadOnly mstrModuleName As String = "clsPolicy_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintPolicyTranunkid As Integer = -1
    Private mintPolicyunkid As Integer = -1
    Private mintUserunkid As Integer = -1
    Private mblnIsvoid As Boolean = False
    Private mintVoidUserunkid As Integer = -1
    Private mdtVoiddatetime As DateTime = Nothing
    Private mstrVoidReason As String = ""
    Private mdtTran As DataTable
#End Region

#Region " Properties "

    Public Property _PolicyTranunkid() As Integer
        Get
            Return mintPolicyTranunkid
        End Get
        Set(ByVal value As Integer)
            mintPolicyTranunkid = value
        End Set
    End Property

    Public Property _Policyunkid() As Integer
        Get
            Return mintPolicyunkid
        End Get
        Set(ByVal value As Integer)
            mintPolicyunkid = value
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _VoidUserunkid() As Integer
        Get
            Return mintVoidUserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidUserunkid = value
        End Set
    End Property

    Public Property _Voiddatetime() As DateTime
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As DateTime)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _VoidReason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("PolicyTran")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("policytranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("policyunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("dayid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isweekend")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("basehours")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot1")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot2")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot3")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot4")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("elrcoming_grace")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ltcoming_grace")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("elrgoing_grace")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ltgoing_grace")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("basehoursinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot1insec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot2insec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot3insec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot4insec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("elrcome_graceinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ltcome_graceinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("elrgoing_graceinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ltgoing_graceinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            mdtTran.Columns.Add("isvoid", Type.GetType("System.Boolean"))
            mdtTran.Columns.Add("voiduserunkid", Type.GetType("System.Int32"))
            mdtTran.Columns.Add("voiddatetime", Type.GetType("System.DateTime"))
            mdtTran.Columns.Add("voidreason", Type.GetType("System.String"))


            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("breaktime")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("breaktimeinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("countbreaktimeafter")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("countbreaktimeaftersec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)
          
            dCol = New DataColumn("teatime")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("teatimeinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("countteatimeafter")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("countteatimeaftersec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

    Public Sub GetPolicyTran(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String, Optional ByVal intPolicyId As Integer = 0, Optional ByVal dayId As Integer = -1)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim sqlCmd As SqlCommand = Nothing
      
        Try

            strQ = " SELECT  " & _
                       " tnapolicy_tran.policytranunkid " & _
                       ",tnapolicy_tran.policyunkid " & _
                       ",tnapolicy_tran.dayid " & _
                       ",'' AS DAYS " & _
                       ",0.00 as basehours " & _
                       ",tnapolicy_tran.isweekend " & _
                       ",0.00 as ot1 " & _
                       ",0.00 as ot2 " & _
                       ",0.00 as ot3 " & _
                       ",0.00 as ot4 " & _
                       ",0 as elrcoming_grace " & _
                       ",0 as ltcoming_grace " & _
                       ",0 as elrgoing_grace " & _
                       ",0 as ltgoing_grace " & _
                       ",tnapolicy_tran.basehours as  basehoursinsec" & _
                       ",tnapolicy_tran.ot1 as ot1insec " & _
                       ",tnapolicy_tran.ot2 as ot2insec " & _
                       ",tnapolicy_tran.ot3 as ot3insec  " & _
                       ",tnapolicy_tran.ot4 as ot4insec  " & _
                       ",tnapolicy_tran.elrcoming_grace as  elrcome_graceinsec " & _
                       ",tnapolicy_tran.ltcoming_grace  as ltcome_graceinsec " & _
                       ",tnapolicy_tran.elrgoing_grace as elrgoing_graceinsec " & _
                       ",tnapolicy_tran.ltgoing_grace  as ltgoing_graceinsec " & _
                       ",isvoid " & _
                       ",voiduserunkid " & _
                       ",voiddatetime " & _
                       ",voidreason " & _
                       " ,'' AS AUD " & _
                       ",0 AS breaktime " & _
                       ",tnapolicy_tran.breaktime AS breaktimeinsec " & _
                       ",0 AS countbreaktimeafter " & _
                       ",tnapolicy_tran.countbreaktimeafter AS countbreaktimeaftersec " & _
                       ",0 AS teatime " & _
                       ",tnapolicy_tran.teatime AS teatimeinsec " & _
                       ",0 AS countteatimeafter " & _
                       ",tnapolicy_tran.countteatimeafter AS countteatimeaftersec " & _
                       " FROM " & xDatabaseName & "..tnapolicy_tran"


            If intPolicyId > 0 Then
                strQ &= " WHERE tnapolicy_tran.policyunkid = @policyunkid"
            End If

            If dayId > 0 Then
                strQ &= "  AND tnapolicy_tran.dayid = @dayid"
            End If


            If SqlTran Is Nothing Then
                sqlCmd = New SqlCommand(strQ, SqlCn)
            Else
                sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            End If
            sqlCmd.Parameters.Clear()

            If intPolicyId > 0 Then
                sqlCmd.Parameters.AddWithValue("@policyunkid", intPolicyId)
            End If

            If dayId > 0 Then
                sqlCmd.Parameters.AddWithValue("@dayid", dayId)
            End If

            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

            mdtTran = dsList.Tables("List")
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPolicyTran; Module Name: " & mstrModuleName)
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub


End Class
