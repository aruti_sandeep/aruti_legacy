﻿Imports System.Globalization
Imports System.Data.SqlClient
Imports MySql.Data.MySqlClient


''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsdeviceconnection_setting
    Private Shared ReadOnly mstrModuleName As String = "clsdeviceconnection_setting"


#Region "Enum"

    Public Enum enDeviceConnectionType
        SQLServer = 1
        MYSQL = 2
    End Enum

#End Region

    Public Function GetList(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xCompanyId As Integer, ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim SqlCmd As SqlCommand = Nothing

        Try
            strQ = "SELECT " & _
                      "  deviceconnectionunkid " & _
                      ", companyunkid " & _
                      ", connectiontypeunkid " & _
                      ", CASE WHEN connectiontypeunkid = 1 THEN @SQLSERVER " & _
                      "           WHEN connectiontypeunkid = 2 THEN @MYSQL " & _
                      "  ELSE '' END AS connectiontype " & _
                      ", devicetypeunkid " & _
                      ", CASE WHEN devicetypeunkid = " & enFingerPrintDevice.BioStar & " THEN @Biostar " & _
                      "           WHEN devicetypeunkid = " & enFingerPrintDevice.BioStar2 & " THEN @Biostar2 " & _
                      "           WHEN devicetypeunkid = " & enFingerPrintDevice.BioStar3 & " THEN @Biostar3 " & _
                      " END AS biostartype " & _
                      ", database_name " & _
                      ", db_servername " & _
                      ", db_username " & _
                      ", db_password " & _
                      ", db_port " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      " FROM hrmsconfiguration..cfdeviceconnection_setting "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            If xCompanyId > 0 Then
                strQ &= " AND companyunkid = @companyunkid "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            SqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            SqlCmd.Parameters.Clear()
            SqlCmd.Parameters.AddWithValue("@companyunkid", xCompanyId)
            SqlCmd.Parameters.AddWithValue("@SQLSERVER", "SQL Server")
            SqlCmd.Parameters.AddWithValue("@MYSQL", "MYSQL")
            SqlCmd.Parameters.AddWithValue("@Biostar", "Biostar")
            SqlCmd.Parameters.AddWithValue("@Biostar2", "Biostar 2")
            SqlCmd.Parameters.AddWithValue("@Biostar3", "Biostar 3")

            Using sqlDataadp As New SqlDataAdapter(SqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    Public Function GetBioStarAttendanceData(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xConnectiontypeunkid As Integer, ByVal mstrDb_Servername As String _
                                                                , ByVal mstrDatabase_Name As String, ByVal mstrDb_Username As String, ByVal mstrDb_Password As String, ByVal mstrDb_Port As String _
                                                                , ByVal mdtFromdate As Date, ByVal mdtTodate As Date) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            If xConnectiontypeunkid = enDeviceConnectionType.SQLServer Then

                Dim StrConn As String = ""
                Dim sqlcmd As SqlClient.SqlCommand = Nothing
                Dim adp As SqlClient.SqlDataAdapter = Nothing

                Using oSql As SqlClient.SqlConnection = New SqlClient.SqlConnection
                    StrConn = "Data Source=" & mstrDb_Servername & "; Initial Catalog=" & mstrDatabase_Name & "; user id =" & mstrDb_Username & "; Password=" & mstrDb_Password & "; MultipleActiveResultSets=true; "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                              " TB_USER.sUserID AS UserId  " & _
                              " ,TB_USER.sUserName AS UserName" & _
                              " ,TB_EVENT_LOG.nReaderIdn AS Device " & _
                              " ,TB_READER.sIP AS IPAddress" & _
                              " ,DATEADD(s,nDateTime,'1970-01-01') AS Logindate " & _
                              " FROM " & mstrDatabase_Name & "..TB_EVENT_LOG " & _
                              " JOIN " & mstrDatabase_Name & "..TB_USER on TB_EVENT_LOG.nUserID = TB_USER.sUserID " & _
                              " LEFT JOIN " & mstrDatabase_Name & "..TB_READER ON TB_READER.nReaderIdn = TB_EVENT_LOG.nReaderIdn " & _
                              " WHERE TB_EVENT_LOG.nUserID > 0 AND CONVERT(CHAR(8),DATEADD(s,nDateTime,'1970-01-01'),112) BETWEEN @FromDate AND @ToDate " & _
                              " ORDER BY CONVERT(CHAR(8),DATEADD(s,nDateTime,'1970-01-01'),112),TB_USER.sUserID "

                    sqlcmd = New SqlClient.SqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", SqlDbType.NVarChar).Value = convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", SqlDbType.NVarChar).Value = convertDate(mdtTodate).ToString()
                    adp = New SqlClient.SqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using

            ElseIf xConnectiontypeunkid = enDeviceConnectionType.MYSQL Then

                Dim StrConn As String = ""
                Dim sqlcmd As MySqlCommand = Nothing
                Dim adp As MySqlDataAdapter = Nothing

                Using oSql As MySqlConnection = New MySqlConnection
                    StrConn = "Server=" & mstrDb_Servername & ";Port=" & mstrDb_Port & ";Database=" & mstrDatabase_Name & ";Uid=" & mstrDb_Username & ";Pwd = " & mstrDb_Password & " "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                              " TB_USER.sUserID AS UserId  " & _
                              " ,TB_USER.sUserName AS UserName" & _
                              " ,TB_EVENT_LOG.nReaderIdn AS Device " & _
                              " ,TB_READER.sIP AS IPAddress" & _
                              " ,DATEADD('1970-01-01' ,INTERVAL nDateTime SECOND) AS Logindate " & _
                              " FROM " & mstrDatabase_Name & ".TB_EVENT_LOG " & _
                              " JOIN " & mstrDatabase_Name & ".TB_USER on TB_EVENT_LOG.nUserID = TB_USER.sUserID " & _
                              " LEFT JOIN " & mstrDatabase_Name & ".TB_READER ON TB_READER.nReaderIdn = TB_EVENT_LOG.nReaderIdn " & _
                              " WHERE TB_EVENT_LOG.nUserID > 0 AND DATE_FORMAT(DATE_ADD('1970-01-01', INTERVAL nDateTime SECOND), '%Y%m%d') BETWEEN @FromDate AND @ToDate " & _
                              " ORDER BY DATE_FORMAT(DATE_ADD('1970-01-01', INTERVAL nDateTime SECOND), '%Y%m%d') ,TB_USER.sUserID "


                    sqlcmd = New MySqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", MySqlDbType.VarChar).Value = convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", MySqlDbType.VarChar).Value = convertDate(mdtTodate).ToString()
                    adp = New MySqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBioStarAttendanceData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return dtTable
    End Function

    Public Function GetBioStar2AttendanceData(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xConnectiontypeunkid As Integer, ByVal mstrDb_Servername As String _
                                                                , ByVal mstrDatabase_Name As String, ByVal mstrDb_Username As String, ByVal mstrDb_Password As String, ByVal mstrDb_Port As String _
                                                                , ByVal mdtFromdate As Date, ByVal mdtTodate As Date) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            If xConnectiontypeunkid = enDeviceConnectionType.SQLServer Then

                Dim StrConn As String = ""
                Dim sqlcmd As SqlClient.SqlCommand = Nothing
                Dim adp As SqlClient.SqlDataAdapter = Nothing

                Using oSql As SqlClient.SqlConnection = New SqlClient.SqlConnection
                    StrConn = "Data Source=" & mstrDb_Servername & "; Initial Catalog=" & mstrDatabase_Name & "; user id =" & mstrDb_Username & "; Password=" & mstrDb_Password & "; MultipleActiveResultSets=true; "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                              " p.user_id AS UserId " & _
                              ", p.user_name AS UserName " & _
                              ", p.devnm AS Device " & _
                              ", d.ip_address AS IPAddress " & _
                              ", p.bsevtdt AS LoginDate " & _
                              " FROM  " & mstrDatabase_Name & "..punchlog AS p " & _
                              " JOIN  " & mstrDatabase_Name & "..device AS d ON d.id = p.devid " & _
                              " WHERE p.user_id > 0 AND CONVERT(CHAR(8),p.bsevtdt,112) BETWEEN @FromDate AND @ToDate " & _
                              " ORDER BY CONVERT(CHAR(8),p.bsevtdt,112),p.user_id "


                    sqlcmd = New SqlClient.SqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", SqlDbType.NVarChar).Value = convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", SqlDbType.NVarChar).Value = convertDate(mdtTodate).ToString()
                    adp = New SqlClient.SqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using

            ElseIf xConnectiontypeunkid = enDeviceConnectionType.MYSQL Then

                Dim StrConn As String = ""
                Dim sqlcmd As MySqlCommand = Nothing
                Dim adp As MySqlDataAdapter = Nothing

                Using oSql As MySqlConnection = New MySqlConnection
                    StrConn = "Server=" & mstrDb_Servername & ";Port=" & mstrDb_Port & ";Database=" & mstrDatabase_Name & ";Uid=" & mstrDb_Username & ";Pwd = " & mstrDb_Password & " "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                            " p.user_id AS UserId " & _
                            ", p.user_name AS UserName " & _
                            ", p.devnm AS Device " & _
                            ", d.ip_address AS IPAddress " & _
                            ", p.bsevtdt AS LoginDate " & _
                            " FROM  " & mstrDatabase_Name & ".punchlog AS p " & _
                            " JOIN  " & mstrDatabase_Name & ".device AS d ON d.id = p.devid " & _
                            " WHERE p.user_id > 0 AND DATE_FORMAT(p.bsevtdt, '%Y%m%d') BETWEEN @FromDate AND @ToDate " & _
                            " ORDER BY DATE_FORMAT(p.bsevtdt, '%Y%m%d'),p.user_id "

                    sqlcmd = New MySqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", MySqlDbType.VarChar).Value = convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", MySqlDbType.VarChar).Value = convertDate(mdtTodate).ToString()
                    adp = New MySqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBioStar2AttendanceData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return dtTable
    End Function

    Public Function GetBioStar3AttendanceData(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xConnectiontypeunkid As Integer, ByVal mstrDb_Servername As String _
                                                                , ByVal mstrDatabase_Name As String, ByVal mstrDb_Username As String, ByVal mstrDb_Password As String, ByVal mstrDb_Port As String _
                                                                , ByVal mdtFromdate As Date, ByVal mdtTodate As Date) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            If xConnectiontypeunkid = enDeviceConnectionType.SQLServer Then

                Dim StrConn As String = ""
                Dim sqlcmd As SqlClient.SqlCommand = Nothing
                Dim adp As SqlClient.SqlDataAdapter = Nothing

                Using oSql As SqlClient.SqlConnection = New SqlClient.SqlConnection
                    StrConn = "Data Source=" & mstrDb_Servername & "; Initial Catalog=" & mstrDatabase_Name & "; user id =" & mstrDb_Username & "; Password=" & mstrDb_Password & "; MultipleActiveResultSets=true; "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                              " p.user_id AS UserId " & _
                              ", p.user_name AS UserName " & _
                              ", p.devnm AS Device " & _
                              ", d.ip_address AS IPAddress " & _
                              ", p.devdt AS LoginDate " & _
                              " FROM  " & mstrDatabase_Name & "..punchlog AS p " & _
                              " JOIN  " & mstrDatabase_Name & "..device AS d ON d.id = p.devid " & _
                              " WHERE p.user_id > 0 AND CONVERT(CHAR(8),p.devdt,112) BETWEEN @FromDate AND @ToDate " & _
                              " ORDER BY CONVERT(CHAR(8),p.devdt,112),p.user_id "


                    sqlcmd = New SqlClient.SqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", SqlDbType.NVarChar).Value = convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", SqlDbType.NVarChar).Value = convertDate(mdtTodate).ToString()
                    adp = New SqlClient.SqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using

            ElseIf xConnectiontypeunkid = enDeviceConnectionType.MYSQL Then

                Dim StrConn As String = ""
                Dim sqlcmd As MySqlCommand = Nothing
                Dim adp As MySqlDataAdapter = Nothing

                Using oSql As MySqlConnection = New MySqlConnection
                    StrConn = "Server=" & mstrDb_Servername & ";Port=" & mstrDb_Port & ";Database=" & mstrDatabase_Name & ";Uid=" & mstrDb_Username & ";Pwd = " & mstrDb_Password & " "
                    oSql.ConnectionString = StrConn
                    oSql.Open()

                    strQ = " SELECT  " & _
                            " p.user_id AS UserId " & _
                            ", p.user_name AS UserName " & _
                            ", p.devnm AS Device " & _
                            ", d.ip_address AS IPAddress " & _
                            ", p.devdt AS LoginDate " & _
                            " FROM  " & mstrDatabase_Name & ".punchlog AS p " & _
                            " JOIN  " & mstrDatabase_Name & ".device AS d ON d.id = p.devid " & _
                            " WHERE p.user_id > 0 AND DATE_FORMAT(p.devdt, '%Y%m%d') BETWEEN @FromDate AND @ToDate " & _
                            " ORDER BY DATE_FORMAT(p.devdt, '%Y%m%d'),p.user_id "

                    sqlcmd = New MySqlCommand(strQ)
                    sqlcmd.Connection = oSql
                    sqlcmd.CommandTimeout = 0
                    sqlcmd.Parameters.Clear()
                    sqlcmd.Parameters.Add("@FromDate", MySqlDbType.VarChar).Value = convertDate(mdtFromdate).ToString()
                    sqlcmd.Parameters.Add("@ToDate", MySqlDbType.VarChar).Value = convertDate(mdtTodate).ToString()
                    adp = New MySqlDataAdapter(sqlcmd)
                    Dim dsList As New DataSet
                    adp.Fill(dsList)
                    If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

                    oSql.Close()
                End Using

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBioStar3AttendanceData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return dtTable
    End Function


End Class