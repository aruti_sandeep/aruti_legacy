﻿Imports System.Data.SqlClient

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsshift_tran

    Private Shared ReadOnly mstrModuleName As String = "clsshift_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintShifttranunkid As Integer
    Private mintShiftunkid As Integer
    Private mdtShiftDays As DataTable = Nothing

#End Region

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Shifttranunkid() As Integer
        Get
            Return mintShifttranunkid
        End Get
        Set(ByVal value As Integer)
            mintShifttranunkid = value
        End Set
    End Property

    Public Property _Shiftunkid() As Integer
        Get
            Return mintShiftunkid
        End Get
        Set(ByVal value As Integer)
            mintShiftunkid = value
        End Set
    End Property

    Public Property _dtShiftday() As DataTable
        Get
            Return mdtShiftDays
        End Get
        Set(ByVal value As DataTable)
            mdtShiftDays = value
        End Set
    End Property

#End Region

#Region "Methods"

    Public Sub GetShiftTran(ByVal SqlCn As SqlConnection, ByVal SqlTran As SqlTransaction, ByVal xDatabaseName As String, Optional ByVal intShiftunkid As Integer = 0)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim sqlCmd As SqlCommand = Nothing

        Try

            strQ = " SELECT  " & _
                       " tnashift_tran.shifttranunkid " & _
                       ",tnashift_tran.shiftunkid " & _
                       ",tnashift_tran.dayid " & _
                       ",'' AS DAYS " & _
                       ",tnashift_tran.isweekend " & _
                       ",tnashift_tran.starttime " & _
                       ",tnashift_tran.endtime " & _
                       ",(tnashift_tran.breaktime / 60) AS breaktime " & _
                       ",CONVERT(DECIMAL(5, 2), FLOOR(((tnashift_tran.workinghrs) / 60 )/ 60) + ( CONVERT(DECIMAL(5,2), ( tnashift_tran.workinghrs / 60 ) % 60)/ 100 )) AS workinghrs  " & _
                       ",(tnashift_tran.calcovertimeafter / 60) AS calcovertimeafter " & _
                       ",(tnashift_tran.calcshorttimebefore / 60)  AS calcshorttimebefore " & _
                       ",0.00 as halffromhrs " & _
                       ",0.00 as halftohrs " & _
                       ",tnashift_tran.halfdayfromhrs as halffromhrsinsec " & _
                       ",tnashift_tran.halfdaytohrs as halftohrsinsec " & _
                       ",tnashift_tran.nightfromhrs " & _
                       ",tnashift_tran.nighttohrs " & _
                       ",tnashift_tran.breaktime AS breaktimeinsec " & _
                       ",tnashift_tran.workinghrs AS workinghrsinsec " & _
                       ",tnashift_tran.calcovertimeafter AS calcovertimeafterinsec " & _
                       ",tnashift_tran.calcshorttimebefore AS calcshorttimebeforeinsec " & _
                       ",tnashift_tran.daystart_time " & _
                       " ,'' AS AUD " & _
                       ",ISNULL(tnashift_tran.countotmins_aftersftendtime/60,0) AS countotmins_aftersftendtime " & _
                       ",ISNULL(tnashift_tran.countotmins_aftersftendtime,0) AS countotmins_aftersftendtimeinsec " & _
                       ",ISNULL(tnashift_tran.isotholiday,0) AS isotholiday  " & _
                       " FROM " & xDatabaseName & "..tnashift_tran WITH (NOLOCK) "

            If intShiftunkid > 0 Then
                strQ &= " WHERE tnashift_tran.Shiftunkid = @Shiftunkid"
            End If

            If SqlTran Is Nothing Then
                sqlCmd = New SqlCommand(strQ, SqlCn)
            Else
                sqlCmd = New SqlCommand(strQ, SqlCn, SqlTran)
            End If

            sqlCmd.Parameters.Clear()
            If intShiftunkid > 0 Then
                sqlCmd.Parameters.AddWithValue("@Shiftunkid", intShiftunkid)
            End If

            Using sqlDataadp As New SqlDataAdapter(sqlCmd)
                dsList = New DataSet
                sqlDataadp.Fill(dsList)
            End Using

            mdtShiftDays = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetShiftTran; Module Name: " & mstrModuleName)
        Finally
            sqlCmd.Dispose()
            sqlCmd = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Sub

#End Region

End Class
