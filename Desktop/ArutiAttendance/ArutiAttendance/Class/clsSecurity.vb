﻿Imports System.Security.Cryptography
Imports System.IO
Imports System.Text

Public Class clsSecurity

    Private Shared bytIV() As Byte = {21, 54, 9, 13, 78, 19, 62, 45, 121, 195, 245, 35, 5, 1, 121, 212}

    Public Shared Function Decrypt(ByVal vstrStringToBeDecrypted As String, ByVal vstrDecryptionKey As String) As String

        Dim bytDataToBeDecrypted() As Byte = Nothing
        Dim bytTemp() As Byte = Nothing
        Dim bytDecryptionKey() As Byte = Nothing

        Dim objRijndaelManaged As New RijndaelManaged()
        Dim objMemoryStream As MemoryStream = Nothing
        Dim objCryptoStream As CryptoStream = Nothing

        Dim strReturnString As String = String.Empty

        Try
            bytDataToBeDecrypted = Convert.FromBase64String(vstrStringToBeDecrypted)

            vstrDecryptionKey = vstrDecryptionKey.PadRight(32, "X"c)
            bytDecryptionKey = Encoding.ASCII.GetBytes(vstrDecryptionKey.ToCharArray())

            bytTemp = New Byte(bytDataToBeDecrypted.Length) {}

            objMemoryStream = New MemoryStream(bytDataToBeDecrypted)

            objCryptoStream = New CryptoStream(objMemoryStream, objRijndaelManaged.CreateDecryptor(bytDecryptionKey, bytIV), CryptoStreamMode.Read)
            objCryptoStream.Read(bytTemp, 0, bytTemp.Length)

            Return StripNullCharacters(Encoding.Unicode.GetString(bytTemp, 0, bytTemp.Length))

        Catch ex As Exception
            Dim S_dispmsg As String = ex.Message & "; " & ex.StackTrace.ToString()
            If ex.InnerException IsNot Nothing Then
                S_dispmsg &= "; " & ex.InnerException.Message
            End If
            S_dispmsg = S_dispmsg.Replace("'", "")
            S_dispmsg = S_dispmsg.Replace(Environment.NewLine, "")
            Throw New Exception(S_dispmsg & "Decrypt")
        Finally
            bytDataToBeDecrypted = Nothing
            bytTemp = Nothing
            bytDecryptionKey = Nothing

            If objMemoryStream IsNot Nothing Then
                objMemoryStream.Close()
            End If
            objMemoryStream = Nothing

            If objCryptoStream IsNot Nothing Then
                objCryptoStream.Close()
            End If
            objCryptoStream = Nothing

            If objRijndaelManaged IsNot Nothing Then
                objRijndaelManaged.Clear()
            End If
            objRijndaelManaged = Nothing
        End Try
    End Function

    Private Shared Function StripNullCharacters(ByVal vstrStringWithNulls As String) As String
        Dim intPosition As Integer = 0
        Dim strStringWithOutNulls As String = Nothing
        intPosition = 1
        strStringWithOutNulls = vstrStringWithNulls

        Do While intPosition > 0
            intPosition = (vstrStringWithNulls.IndexOf(ControlChars.NullChar, intPosition - 1) + 1)

            If intPosition > 0 Then
                strStringWithOutNulls = strStringWithOutNulls.Substring(0, intPosition - 1) & strStringWithOutNulls.Substring(strStringWithOutNulls.Length - (strStringWithOutNulls.Length - intPosition))
            End If

            If intPosition > strStringWithOutNulls.Length Then
                Exit Do
            End If
        Loop
        Return strStringWithOutNulls
    End Function

End Class
