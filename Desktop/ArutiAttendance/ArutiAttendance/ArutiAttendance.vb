﻿#Region " Import "

Imports System
Imports System.IO
Imports System.Timers
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.ServiceProcess
Imports System.Security.Cryptography
Imports System.Text
Imports System.Collections.Specialized
Imports System.Net
Imports System.Text.RegularExpressions
Imports System.Globalization
Imports System.Threading
Imports System.Runtime.InteropServices
Imports iTextSharp.text
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf

#End Region


Public Class ArutiAttendance

#Region "Private Variable"
    Private timer As New System.Timers.Timer()
    Dim sqlCn As SqlConnection
    Dim sqlCmd As SqlCommand
    Private mintDatabaseVersion As Integer = 57
    Private mblnIsDatabaseAccessible As Boolean = True
    Dim strIp As String = ""
    Dim intPort As Integer = 0
    Dim mintMachineSrNo As Integer = 0
    Dim mstrDeviceCode As String = ""
    Dim intDeviceType As Integer = -1
    Dim mstrCommunicationKey As String = ""
    Private mstrUserID As String = ""
    Private mstrPassword As String = ""
    Private mstrDeviceModel As String = ""
    Private mstrSendTnAEarlyLateReportsUserIds As String = ""
    Private mstrTnAFailureNotificationUserIds As String = ""
    Dim t As Thread
    Dim mstrError As String = ""
    Dim xCompanyId As Integer = 0
    Dim xYearId As Integer = 0

    Dim anviz_handle As IntPtr

    <DllImport("Kernel32.dll")> _
 Public Shared Function RtlMoveMemory(ByRef Destination As clsAnviz.CLOCKINGRECORD, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Function RtlMoveMemory(ByRef Destination As clsAnviz.PERSONINFO, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Function RtlMoveMemory(ByRef Destination As clsAnviz.PERSONINFOEX, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Function RtlMoveMemory(ByRef Destination As Integer, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Function RtlMoveMemory(ByRef Destination As Byte, ByVal Source As Integer, ByVal Length As Integer) As Boolean
    End Function
    <DllImport("Kernel32.dll")> _
    Public Shared Sub GetLocalTime(ByRef lpSystemTime As clsAnviz.SYSTEMTIME)
    End Sub

    Private ReaderNo As Integer
    Private ReaderIpAddress As String
    Private clocking As New clsAnviz.CLOCKINGRECORD()

    Private Structure SYSTEMTIME
        Dim wYear As Short
        Dim wMonth As Short
        Dim wDayOfWeek As Short
        Dim wDay As Short
        Dim wHour As Short
        Dim wMinute As Short
        Dim wSecond As Short
        Dim wMilliseconds As Short
    End Structure
    Dim IDNumber As Int32

    Private Declare Sub GetLocalTime Lib "kernel32" (ByRef lpSystemTime As SYSTEMTIME)

    'Pinkal (21-Jul-2023) -- Start
    '(A1X-1108) Voltamp/TRA -- Bio star device integration.
    Dim mintLeaveBalanceSetting As Integer = 1
    Dim mblnIsHolidayConsiderOnWeekend As Boolean = True
    Dim mblnIsDayOffConsiderOnWeekend As Boolean = True
    Dim mblnIsHolidayConsiderOnDayoff As Boolean = True
    Dim mstrUserAccessModeSettings As String = ""
    Dim mblnPolicyManagementTNA As Boolean = False
    Dim mblnDonotAttendanceinSeconds As Boolean = False
    Dim mblnFirstCheckInLastCheckOut As Boolean = False
    Dim mblnCountAttendanceNotLVIfLVapplied As Boolean = True
    Private dtAttendanceData As DataTable = Nothing
    'Pinkal (21-Jul-2023) -- End

    'Pinkal (15-Sep-2023) -- Start
    '(A1X-158) Mbeya Cement: HIK Device Integration - Automatic attendance data download
    Dim mintHIKUserID As Integer = -1
    Dim m_lGetAcsEventHandle As Integer = -1
    'Pinkal (15-Sep-2023) -- End
#End Region

#Region "Service Method"

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            'System.Diagnostics.Debugger.Launch()
            AddHandler timer.Elapsed, AddressOf OnElapsedTime
            timer.Interval = 60000   '1 Minute
            timer.Enabled = True
            EmbeddedAssembly.Load("ArutiAttendance.itextsharp.dll", "itextsharp.dll")
        Catch ex As Exception
            WriteLog("OnStart:- " & ex.Message)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        Try
            timer.Enabled = False
        Catch ex As Exception
            WriteLog("OnStop:- " & ex.Message)
        End Try
    End Sub

    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA.
    'Private Sub OnElapsedTime(ByVal source As Object, ByVal e As ElapsedEventArgs)
    '    Dim dtCompany As DataTable = Nothing
    '    Dim xCompanyId As Integer = 0
    '    Dim xYearId As Integer = 0
    '    Dim objLogin As New clslogin_Tran()
    '    Try
    '        'System.Diagnostics.Debugger.Launch()
    '        timer.Enabled = False
    '        If IsConnect() = False Then Exit Sub
    '        dtCompany = IsArutiAttendanceConfigured()

    '        If IsDBNull(dtCompany) = False AndAlso dtCompany.Rows.Count > 0 Then

    '            For Each dr As DataRow In dtCompany.Rows
    '                xCompanyId = Convert.ToInt32(dr("companyunkid"))
    '                xYearId = Convert.ToInt32(dr("yearunkid"))
    '                mblnIsDatabaseAccessible = False
    '                IsDatabaseAccessible(dr("database_name").ToString())
    '                If mblnIsDatabaseAccessible = False Then Continue For
    '                Dim dtParams As New DataTable()
    '                dtParams = GetAttendanceParameters(Convert.ToInt32(dr("companyunkid")))

    '                If IsDBNull(dtParams) = False AndAlso dtParams.Rows.Count > 0 Then

    '                    Dim objSendmail As New clsSendMail
    '                    Dim dtCompanyDetails As DataTable = GetCompanyDetails(xCompanyId)

    '                    If dtCompanyDetails IsNot Nothing AndAlso dtCompanyDetails.Rows.Count > 0 Then
    '                        objSendmail._SenderAddress = dtCompanyDetails.Rows(0)("senderaddress").ToString()
    '                        objSendmail._MailserverIP = dtCompanyDetails.Rows(0)("mailserverip").ToString()
    '                        objSendmail._MailserverPort = dtCompanyDetails.Rows(0)("mailserverport").ToString()
    '                        objSendmail._UserName = dtCompanyDetails.Rows(0)("username").ToString()
    '                        objSendmail._Password = dtCompanyDetails.Rows(0)("password").ToString()
    '                        objSendmail._Isloginssl = CBool(dtCompanyDetails.Rows(0)("isloginssl"))
    '                    End If

    '                    Dim drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "SendTnAEarlyLateReportsUserIds").ToList()
    '                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
    '                        mstrSendTnAEarlyLateReportsUserIds = drRow(0)("key_value").ToString()
    '                    End If

    '                    drRow = Nothing
    '                    drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "TnAFailureNotificationUserIds").ToList()
    '                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
    '                        mstrTnAFailureNotificationUserIds = drRow(0)("key_value").ToString()
    '                    End If

    '                    drRow = Nothing
    '                    drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "RunDailyAttendanceServiceTime").ToList()
    '                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then

    '                        mstrError = ""

    '                        If DateTime.Now.ToString("HH:mm") = CDate(drRow(0)("key_value")).ToString("HH:mm") Then

    '                            Dim path As String = AppDomain.CurrentDomain.BaseDirectory + "\DeviceSetting.xml"

    '                            If System.IO.File.Exists(path) Then

    '                                Dim dsMachineSetting As New DataSet()
    '                                dsMachineSetting.ReadXml(path)

    '                                If IsDBNull(dsMachineSetting) = False AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then
    '                                    Dim drMachineDetail = dsMachineSetting.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("companyunkid") = xCompanyId.ToString()).ToList()

    '                                    If drMachineDetail IsNot Nothing AndAlso drMachineDetail.Count() > 0 Then

    '                                        'If objLogin.DeleteDeviceAttendanceData(sqlCn, dr("database_name").ToString()) = False Then
    '                                        '    Dim mstrError As String = objLogin._Message
    '                                        '    SendFailedNotificationToUser(dr("database_name").ToString(), xCompanyId, False, mstrError, objSendmail)
    '                                        '    Continue For
    '                                        'End If

    '                                        For i As Integer = 0 To drMachineDetail.Count - 1

    '                                            intDeviceType = CInt(drMachineDetail(i)("commdeviceid").ToString())

    '                                            mstrDeviceCode = CStr(drMachineDetail(i)("devicecode").ToString()).Trim.Remove(drMachineDetail(i)("devicecode").ToString().IndexOf("||"), drMachineDetail(i)("devicecode").ToString().Trim.Length - drMachineDetail(i)("devicecode").ToString().IndexOf("||"))

    '                                            strIp = drMachineDetail(i)("ip").ToString()

    '                                            If IsDBNull(drMachineDetail(i)("port")) = False AndAlso drMachineDetail(i)("port").ToString() <> "" Then
    '                                                intPort = CInt(drMachineDetail(i)("port").ToString())
    '                                            End If

    '                                            mintMachineSrNo = CInt(drMachineDetail(i)("machinesrno").ToString())

    '                                            If dsMachineSetting.Tables(0).Columns.Contains("commkey") Then
    '                                                If IsDBNull(drMachineDetail(i)("commkey")) = False AndAlso drMachineDetail(i)("commkey").ToString.Trim.Length > 0 Then
    '                                                    mstrCommunicationKey = drMachineDetail(i)("commkey").ToString()
    '                                                End If
    '                                            End If

    '                                            If dsMachineSetting.Tables(0).Columns.Contains("userid") Then
    '                                                If IsDBNull(drMachineDetail(i)("userid")) = False AndAlso drMachineDetail(i)("userid").ToString.Trim.Length > 0 Then
    '                                                    mstrUserID = drMachineDetail(i)("userid").ToString()
    '                                                End If
    '                                            End If

    '                                            If dsMachineSetting.Tables(0).Columns.Contains("password") Then
    '                                                If IsDBNull(drMachineDetail(i)("password")) = False AndAlso drMachineDetail(i)("password").ToString.Trim.Length > 0 Then
    '                                                    mstrPassword = drMachineDetail(i)("password").ToString()
    '                                                End If
    '                                            End If

    '                                            If dsMachineSetting.Tables(0).Columns.Contains("devicemodel") Then
    '                                                If IsDBNull(drMachineDetail(i)("devicemodel")) = False AndAlso drMachineDetail(i)("devicemodel").ToString.Trim.Length > 0 Then
    '                                                    mstrDeviceModel = drMachineDetail(i)("devicemodel").ToString()
    '                                                End If
    '                                            End If


    '                                            If intDeviceType <> enFingerPrintDevice.Handpunch AndAlso intDeviceType <> enFingerPrintDevice.SAGEM Then
    '                                                Dim Parameters = New Object() {dr("database_name").ToString(), xCompanyId, objSendmail}
    '                                                t = New Thread(AddressOf DeviceIntegration)
    '                                                t.Start(Parameters)
    '                                                t.Join()


    '                                            ElseIf intDeviceType = enFingerPrintDevice.Handpunch Then
    '                                                'DownloadHandpunch_Data()
    '                                                Exit For

    '                                            ElseIf intDeviceType = enFingerPrintDevice.SAGEM Then
    '                                                'DownloadSAGEM_Data()
    '                                                Exit For

    '                                            End If

    '                                            If mstrError.Trim.Length > 0 Then
    '                                                objLogin.DeleteDeviceAttendanceData(sqlCn, dr("database_name").ToString(), Date.Now.AddDays(-1).Date)
    '                                                mstrError = ""
    '                                                Exit For
    '                                            End If

    '                                        Next


    '                                    End If  'If drMachineDetail IsNot Nothing AndAlso drMachineDetail.Count() > 0 Then

    '                                End If  'If IsDBNull(dsMachineSetting) = False AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

    '                            Else
    '                                mstrError = "Device setting file does not exist on application path.Please put device setting file on application folder."
    '                                SendFailedNotificationToUser(dr("database_name").ToString(), xCompanyId, False, mstrError, objSendmail)
    '                                Continue For
    '                            End If  'If System.IO.File.Exists(path) Then


    '                            'Dim mstrUserAccessModeSetting As String = ""
    '                            'Dim mblnFirstCheckInLastCheckOut As Boolean = False
    '                            'Dim mblnPolicyManagementTNA As Boolean = False
    '                            'Dim mblnDonotAttendanceinSeconds As Boolean = False
    '                            'Dim mblnIsHolidayConsiderOnWeekend As Boolean = False
    '                            'Dim mblnIsDayOffConsiderOnWeekend As Boolean = False
    '                            'Dim mblnIsHolidayConsiderOnDayoff As Boolean = False

    '                            'Dim drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "UserAccessModeSetting").ToList()
    '                            'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
    '                            '    mstrUserAccessModeSetting = drOption(0)("key_value").ToString()
    '                            'End If

    '                            'drOption = Nothing
    '                            'drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "FirstCheckInLastCheckOut").ToList()
    '                            'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
    '                            '    mblnFirstCheckInLastCheckOut = CBool(drOption(0)("key_value"))
    '                            'End If

    '                            'drOption = Nothing
    '                            'drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "PolicyManagementTNA").ToList()
    '                            'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
    '                            '    mblnPolicyManagementTNA = CBool(drOption(0)("key_value"))
    '                            'End If

    '                            'drOption = Nothing
    '                            'drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "DonotAttendanceinSeconds").ToList()
    '                            'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
    '                            '    mblnDonotAttendanceinSeconds = CBool(drOption(0)("key_value"))
    '                            'End If

    '                            'drOption = Nothing
    '                            'drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "IsHolidayConsiderOnWeekend").ToList()
    '                            'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
    '                            '    mblnIsHolidayConsiderOnWeekend = CBool(drOption(0)("key_value"))
    '                            'End If

    '                            'drOption = Nothing
    '                            'drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "IsDayOffConsiderOnWeekend").ToList()
    '                            'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
    '                            '    mblnIsDayOffConsiderOnWeekend = CBool(drOption(0)("key_value"))
    '                            'End If

    '                            'drOption = Nothing
    '                            'drOption = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "IsHolidayConsiderOnDayoff").ToList()
    '                            'If drOption IsNot Nothing AndAlso drRow.Count > 0 Then
    '                            '    mblnIsHolidayConsiderOnDayoff = CBool(drOption(0)("key_value"))
    '                            'End If


    '                            '' START TO SAVE DATA FROM TEMP TABLE TO MAIN TABLE
    '                            'Dim dtTable As DataTable = objLogin.GetDeviceAttendanceData(sqlCn, dr("database_name").ToString(), True, convertDate(Now.Date.AddDays(-1).Date), convertDate(Now.Date.AddDays(-1).Date))

    '                            'If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
    '                            '    SaveAttendanceData(dr("database_name").ToString(), xCompanyId, xYearId, mstrUserAccessModeSetting, mblnFirstCheckInLastCheckOut _
    '                            '                                  , mblnPolicyManagementTNA, CBool(dr("DonotAttendanceinSeconds")), mblnIsHolidayConsiderOnWeekend _
    '                            '                                  , mblnIsDayOffConsiderOnWeekend, mblnIsHolidayConsiderOnDayoff, dtTable)
    '                            'End If
    '                            '' END TO SAVE DATA FROM TEMP TABLE TO MAIN TABLE

    '                            Thread.Sleep(60000)

    '                        End If  ' If DateTime.Now.ToString("HH:mm") = CDate(drRow(0)("key_value")).ToString("HH:mm") Then

    '                    End If  'If drRow IsNot Nothing AndAlso drRow.Count > 0 Then

    '                End If ' If IsDBNull(dtParams) = False AndAlso dtParams.Rows.Count > 0 Then

    '            Next

    '        End If
    '    Catch ex As Exception
    '        WriteLog("OnElapsedTime:- " & ex.Message)
    '    Finally
    '        t = Nothing
    '        objLogin = Nothing
    '        If sqlCn.State = ConnectionState.Open Or sqlCn.State = ConnectionState.Broken Then
    '            sqlCn.Close()
    '        End If
    '        timer.Start()
    '    End Try
    'End Sub
    Private Sub OnElapsedTime(ByVal source As Object, ByVal e As ElapsedEventArgs)
        Dim dtCompany As DataTable = Nothing
        Dim objLogin As New clslogin_Tran()
        Try
            'System.Diagnostics.Debugger.Launch()
            timer.Enabled = False

            If IsConnect() = False Then
                WriteLog("Database is not connected.")
                Exit Sub
            Else
                WriteLog("Database is connected.")
            End If

            dtCompany = IsArutiAttendanceConfigured()

            If IsDBNull(dtCompany) = False AndAlso dtCompany.Rows.Count > 0 Then

                For Each dr As DataRow In dtCompany.Rows
                    xCompanyId = Convert.ToInt32(dr("companyunkid"))
                    xYearId = Convert.ToInt32(dr("yearunkid"))
                    mblnIsDatabaseAccessible = False
                    IsDatabaseAccessible(dr("database_name").ToString())
                    If mblnIsDatabaseAccessible = False Then Continue For
                    Dim dtParams As New DataTable()
                    dtParams = GetAttendanceParameters(Convert.ToInt32(dr("companyunkid")))

                    If IsDBNull(dtParams) = False AndAlso dtParams.Rows.Count > 0 Then

                            Dim objSendmail As New clsSendMail
                            Dim dtCompanyDetails As DataTable = GetCompanyDetails(xCompanyId)

                            If dtCompanyDetails IsNot Nothing AndAlso dtCompanyDetails.Rows.Count > 0 Then
                                objSendmail._SenderAddress = dtCompanyDetails.Rows(0)("senderaddress").ToString()
                                objSendmail._MailserverIP = dtCompanyDetails.Rows(0)("mailserverip").ToString()
                                objSendmail._MailserverPort = dtCompanyDetails.Rows(0)("mailserverport").ToString()
                                objSendmail._UserName = dtCompanyDetails.Rows(0)("username").ToString()
                                objSendmail._Password = dtCompanyDetails.Rows(0)("password").ToString()
                                objSendmail._Isloginssl = CBool(dtCompanyDetails.Rows(0)("isloginssl"))
                            End If

                            Dim drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "SendTnAEarlyLateReportsUserIds").ToList()
                            If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                                mstrSendTnAEarlyLateReportsUserIds = drRow(0)("key_value").ToString()
                            End If

                            drRow = Nothing
                            drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "TnAFailureNotificationUserIds").ToList()
                            If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                                mstrTnAFailureNotificationUserIds = drRow(0)("key_value").ToString()
                            End If
                        drRow = Nothing


                        'Pinkal (21-Jul-2023) -- Start
                        '(A1X-1108) Voltamp/TRA -- Bio star device integration.

                        drRow = Nothing
                        drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "LeaveBalanceSetting").ToList()
                        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then mintLeaveBalanceSetting = CInt(drRow(0)("key_value"))
                        drRow = Nothing

                        drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "IsHolidayConsiderOnWeekend").ToList()
                        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then mblnIsHolidayConsiderOnWeekend = CBool(drRow(0)("key_value"))
                        drRow = Nothing

                        drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "IsDayOffConsiderOnWeekend").ToList()
                        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then mblnIsDayOffConsiderOnWeekend = CBool(drRow(0)("key_value"))
                        drRow = Nothing

                        drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "IsHolidayConsiderOnDayoff").ToList()
                        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then mblnIsHolidayConsiderOnDayoff = CBool(drRow(0)("key_value"))
                        drRow = Nothing

                        drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "PolicyManagementTNA").ToList()
                        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then mblnPolicyManagementTNA = CBool(drRow(0)("key_value").ToString())
                        drRow = Nothing

                        drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "DonotAttendanceinSeconds").ToList()
                        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then mblnDonotAttendanceinSeconds = CBool(drRow(0)("key_value").ToString())
                        drRow = Nothing

                        drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "FirstCheckInLastCheckOut").ToList()
                        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then mblnFirstCheckInLastCheckOut = CBool(drRow(0)("key_value").ToString())
                        drRow = Nothing

                        drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "CountAttendanceNotLVIfLVapplied").ToList()
                        If drRow IsNot Nothing AndAlso drRow.Count > 0 Then mblnCountAttendanceNotLVIfLVapplied = CBool(drRow(0)("key_value").ToString())
                        drRow = Nothing

                        Dim strGroupName As String = ""
                        strGroupName = GetGroupName()

                        '   If strGroupName.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then

                        'Pinkal (21-Jul-2023) -- End

                            drRow = Nothing
                            drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "RunDailyAttendanceServiceTime").ToList()


                            If drRow IsNot Nothing AndAlso drRow.Count > 0 Then

                                mstrError = ""

                            'Pinkal (18-Aug-2023) -- Start
                            '(A1X -1189) St. Judes - Automatic download of attendance data from Anviz device. 
                            If strGroupName.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then
                                IntializeAttendanceDataTable()
                            End If
                            'Pinkal (18-Aug-2023) -- End

                                If DateTime.Now.ToString("HH:mm") = CDate(drRow(0)("key_value")).ToString("HH:mm") Then

                                    Dim path As String = AppDomain.CurrentDomain.BaseDirectory + "\DeviceSetting.xml"

                                    If System.IO.File.Exists(path) Then

                                        Dim dsMachineSetting As New DataSet()
                                        dsMachineSetting.ReadXml(path)

                                        If IsDBNull(dsMachineSetting) = False AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then
                                            Dim drMachineDetail = dsMachineSetting.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("companyunkid") = xCompanyId.ToString()).ToList()

                                        'Pinkal (15-Sep-2023) -- Start
                                        '(A1X-158) Mbeya Cement: HIK Device Integration - Automatic attendance data download
                                        Dim dHIKRow As DataRow() = dsMachineSetting.Tables(0).Select("companyunkid = '" & xCompanyId & "' AND  commdeviceid = " & enFingerPrintDevice.HIKVision)
                                        If dHIKRow IsNot Nothing AndAlso dHIKRow.Length > 0 Then
                                            CHCNetSDK.NET_DVR_Init()
                                        End If
                                        dHIKRow = Nothing
                                        'Pinkal (15-Sep-2023) -- End

                                            If drMachineDetail IsNot Nothing AndAlso drMachineDetail.Count() > 0 Then

                                                For i As Integer = 0 To drMachineDetail.Count - 1

                                                    intDeviceType = CInt(drMachineDetail(i)("commdeviceid").ToString())

                                                    mstrDeviceCode = CStr(drMachineDetail(i)("devicecode").ToString()).Trim.Remove(drMachineDetail(i)("devicecode").ToString().IndexOf("||"), drMachineDetail(i)("devicecode").ToString().Trim.Length - drMachineDetail(i)("devicecode").ToString().IndexOf("||"))

                                                    strIp = drMachineDetail(i)("ip").ToString()

                                                    If IsDBNull(drMachineDetail(i)("port")) = False AndAlso drMachineDetail(i)("port").ToString() <> "" Then
                                                        intPort = CInt(drMachineDetail(i)("port").ToString())
                                                    End If

                                                    mintMachineSrNo = CInt(drMachineDetail(i)("machinesrno").ToString())

                                                    If dsMachineSetting.Tables(0).Columns.Contains("commkey") Then
                                                        If IsDBNull(drMachineDetail(i)("commkey")) = False AndAlso drMachineDetail(i)("commkey").ToString.Trim.Length > 0 Then
                                                            mstrCommunicationKey = drMachineDetail(i)("commkey").ToString()
                                                        End If
                                                    End If

                                                    If dsMachineSetting.Tables(0).Columns.Contains("userid") Then
                                                        If IsDBNull(drMachineDetail(i)("userid")) = False AndAlso drMachineDetail(i)("userid").ToString.Trim.Length > 0 Then
                                                            mstrUserID = drMachineDetail(i)("userid").ToString()
                                                        End If
                                                    End If

                                                    If dsMachineSetting.Tables(0).Columns.Contains("password") Then
                                                        If IsDBNull(drMachineDetail(i)("password")) = False AndAlso drMachineDetail(i)("password").ToString.Trim.Length > 0 Then
                                                            mstrPassword = drMachineDetail(i)("password").ToString()
                                                        End If
                                                    End If

                                                    If dsMachineSetting.Tables(0).Columns.Contains("devicemodel") Then
                                                        If IsDBNull(drMachineDetail(i)("devicemodel")) = False AndAlso drMachineDetail(i)("devicemodel").ToString.Trim.Length > 0 Then
                                                            mstrDeviceModel = drMachineDetail(i)("devicemodel").ToString()
                                                        End If
                                                    End If


                                                'Pinkal (21-Jul-2023) -- Start
                                                '(A1X-1108) Voltamp/TRA -- Bio star device integration.
                                                If intDeviceType <> enFingerPrintDevice.Handpunch AndAlso intDeviceType <> enFingerPrintDevice.SAGEM AndAlso _
                                                    intDeviceType <> enFingerPrintDevice.BioStar AndAlso intDeviceType <> enFingerPrintDevice.BioStar2 AndAlso _
                                                    intDeviceType <> enFingerPrintDevice.BioStar3 Then
                                                    'Pinkal (21-Jul-2023) -- End

                                                        Dim Parameters = New Object() {dr("database_name").ToString(), xCompanyId, objSendmail}
                                                        t = New Thread(AddressOf DeviceIntegration)
                                                        t.Start(Parameters)
                                                        t.Join()


                                                    ElseIf intDeviceType = enFingerPrintDevice.Handpunch Then
                                                        'DownloadHandpunch_Data()
                                                        Exit For

                                                    ElseIf intDeviceType = enFingerPrintDevice.SAGEM Then
                                                        'DownloadSAGEM_Data()
                                                        Exit For

                                                    'Pinkal (21-Jul-2023) -- Start
                                                    '(A1X-1108) Voltamp/TRA -- Bio star device integration.
                                                ElseIf intDeviceType = enFingerPrintDevice.BioStar OrElse intDeviceType = enFingerPrintDevice.BioStar2 OrElse intDeviceType = enFingerPrintDevice.BioStar3 Then
                                                    DownloadBiostar_Data(intDeviceType, strIp)
                                                    'Pinkal (21-Jul-2023) -- Start
                                                    End If


                                                    If mstrError.Trim.Length > 0 Then
                                                        objLogin.DeleteDeviceAttendanceData(sqlCn, dr("database_name").ToString(), Date.Now.AddDays(-1).Date)
                                                        mstrError = ""
                                                        Exit For
                                                    End If

                                                Next

                                            'Pinkal (18-Aug-2023) -- Start
                                            '(A1X -1189) St. Judes - Automatic download of attendance data from Anviz device. 

                                                If dtAttendanceData IsNot Nothing AndAlso dtAttendanceData.Rows.Count > 0 Then

                                                    dtAttendanceData = New DataView(dtAttendanceData, "", "UserId,Logindate", DataViewRowState.CurrentRows).ToTable()

                                                    SaveAttendanceData(dr("database_name").ToString(), xCompanyId, xYearId, mblnFirstCheckInLastCheckOut, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds _
                                                                                  , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend, mblnIsHolidayConsiderOnDayoff, dtAttendanceData)

                                                    If dtAttendanceData IsNot Nothing Then dtAttendanceData.Rows.Clear()
                                                    dtAttendanceData = Nothing
                                                End If

                                            'Pinkal (18-Aug-2023) -- End

                                        End If  'If drMachineDetail IsNot Nothing AndAlso drMachineDetail.Count() > 0 Then

                                    End If  'If IsDBNull(dsMachineSetting) = False AndAlso dsMachineSetting.Tables(0).Rows.Count > 0 Then

                                Else
                                    mstrError = "Device setting file does not exist on application path.Please put device setting file on application folder."
                                    SendFailedNotificationToUser(dr("database_name").ToString(), xCompanyId, False, mstrError, objSendmail)
                                    Continue For
                                End If  'If System.IO.File.Exists(path) Then

                                Thread.Sleep(60000)

                            End If  ' If DateTime.Now.ToString("HH:mm") = CDate(drRow(0)("key_value")).ToString("HH:mm") Then

                            'Pinkal (15-Sep-2023) -- Start
                            '(A1X-158) Mbeya Cement: HIK Device Integration - Automatic attendance data download

                            If mstrSendTnAEarlyLateReportsUserIds.Trim.Length > 0 OrElse mstrTnAFailureNotificationUserIds.Trim.Length > 0 Then

                            If strGroupName.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                                If DateTime.Now.ToString("HH:mm") = CDate(drRow(0)("key_value")).AddMinutes(10).ToString("HH:mm") Then
                                    SendNotificationTRA(xCompanyId, dr("database_name").ToString(), xYearId, dtCompanyDetails)
                                End If
                            Else
                                If DateTime.Now.ToString("HH:mm") = CDate(drRow(0)("key_value")).AddMinutes(45).ToString("HH:mm") Then
                                    SendNotificationTRA(xCompanyId, dr("database_name").ToString(), xYearId, dtCompanyDetails)
                                End If
                            End If  ' If strGroupName.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then

                            End If

                            'Pinkal (15-Sep-2023) -- End

                        End If  'If drRow IsNot Nothing AndAlso drRow.Count > 0 Then    RunDailyAttendanceServiceTime

                        'Pinkal (16-Aug-2023) -- Start
                        'IN TRA Absent Process didn't work for each day after system download data.

                        drRow = Nothing
                                                drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "RunDailyAbsentProcessTime").ToList()

                                                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then

                            If strGroupName.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then

                                                    If DateTime.Now.ToString("HH:mm") = CDate(drRow(0)("key_value")).ToString("HH:mm") Then

                                                        Dim xStartDate As DateTime = Nothing
                                                        Dim xEndDate As DateTime = Nothing

                                                        getCurrentPeriodTenure(dr("database_name").ToString(), xYearId, Now.Date, xStartDate, xEndDate)

                                                        ' START AS TRA IS HAVING WHOLE YEAR AS PERIOD SO WE ARE DOING EVERY DAY ABSENT PROCESS FOR ALL EMPLOYEES
                                                        xStartDate = Now.Date
                                                        xEndDate = Now.Date
                                                        ' END AS TRA IS HAVING WHOLE YEAR AS PERIOD SO WE ARE DOING EVERY DAY ABSENT PROCESS FOR ALL EMPLOYEES

                                                        StartAbsentProcess(dr("database_name").ToString(), xStartDate.Date, xEndDate.Date, mintLeaveBalanceSetting, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                                     , mblnIsHolidayConsiderOnDayoff, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                                                                                     , mblnCountAttendanceNotLVIfLVapplied, objSendmail)


                                                    End If  'If DateTime.Now.ToString("HH:mm") = CDate(drRow(0)("key_value")).ToString("HH:mm") Then

                                            End If  ' If strGroupName.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then

                        End If   ' If drRow IsNot Nothing AndAlso drRow.Count > 0 Then  RunDailyAbsentProcessTime

                        'Pinkal (16-Aug-2023) -- End


                        'Pinkal (21-Jul-2023) -- Start
                        '(A1X-1108) Voltamp/TRA -- Bio star device integration.

                        'Else

                        '    'Pinkal (14-Feb-2022) -- Start
                        '    'Enhancement TRA : TnA Module Enhancement for TRA.

                        '    'Dim drRow As List(Of DataRow) = Nothing
                        '    drRow = Nothing
                        '    drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "RunDailyAttendanceServiceTime").ToList()
                        '    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                        '        If DateTime.Now.ToString("HH:mm") = CDate(drRow(0)("key_value")).AddMinutes(30).ToString("HH:mm") Then
                        '            SendNotificationTRA(xCompanyId, dr("database_name").ToString(), xYearId, dtCompanyDetails)
                        '        End If
                        '    End If

                        '    drRow = Nothing
                        '    Dim mintLeaveBalanceSetting As Integer = 1
                        '    Dim mblnIsHolidayConsiderOnWeekend As Boolean = True
                        '    Dim mblnIsDayOffConsiderOnWeekend As Boolean = True
                        '    Dim mblnIsHolidayConsiderOnDayoff As Boolean = True
                        '    Dim mstrUserAccessModeSettings As String = ""
                        '    Dim mblnPolicyManagementTNA As Boolean = False
                        '    Dim mblnDonotAttendanceinSeconds As Boolean = False
                        '    Dim mblnFirstCheckInLastCheckOut As Boolean = False
                        '    Dim mblnCountAttendanceNotLVIfLVapplied As Boolean = True

                        '    drRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "RunDailyAbsentProcessTime").ToList()
                        '    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                        '        If DateTime.Now.ToString("HH:mm") = CDate(drRow(0)("key_value")).ToString("HH:mm") Then
                        '            Dim drConfigRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "LeaveBalanceSetting").ToList()
                        '            If drConfigRow IsNot Nothing AndAlso drConfigRow.Count > 0 Then mintLeaveBalanceSetting = CInt(drConfigRow(0)("key_value"))
                        '            drConfigRow = Nothing

                        '            drConfigRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "IsHolidayConsiderOnWeekend").ToList()
                        '            If drConfigRow IsNot Nothing AndAlso drConfigRow.Count > 0 Then mblnIsHolidayConsiderOnWeekend = CBool(drConfigRow(0)("key_value"))
                        '            drConfigRow = Nothing

                        '            drConfigRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "IsDayOffConsiderOnWeekend").ToList()
                        '            If drConfigRow IsNot Nothing AndAlso drConfigRow.Count > 0 Then mblnIsDayOffConsiderOnWeekend = CBool(drConfigRow(0)("key_value"))
                        '            drConfigRow = Nothing

                        '            drConfigRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "IsHolidayConsiderOnDayoff").ToList()
                        '            If drConfigRow IsNot Nothing AndAlso drConfigRow.Count > 0 Then mblnIsHolidayConsiderOnDayoff = CBool(drConfigRow(0)("key_value"))
                        '            drConfigRow = Nothing

                        '            drConfigRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "PolicyManagementTNA").ToList()
                        '            If drConfigRow IsNot Nothing AndAlso drConfigRow.Count > 0 Then mblnPolicyManagementTNA = CBool(drConfigRow(0)("key_value").ToString())
                        '            drConfigRow = Nothing

                        '            drConfigRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "DonotAttendanceinSeconds").ToList()
                        '            If drConfigRow IsNot Nothing AndAlso drConfigRow.Count > 0 Then mblnDonotAttendanceinSeconds = CBool(drConfigRow(0)("key_value").ToString())
                        '            drConfigRow = Nothing

                        '            drConfigRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "FirstCheckInLastCheckOut").ToList()
                        '            If drConfigRow IsNot Nothing AndAlso drConfigRow.Count > 0 Then mblnFirstCheckInLastCheckOut = CBool(drConfigRow(0)("key_value").ToString())
                        '            drConfigRow = Nothing

                        '            drConfigRow = dtParams.AsEnumerable().Where(Function(x) x.Field(Of String)("key_name") = "CountAttendanceNotLVIfLVapplied").ToList()
                        '            If drConfigRow IsNot Nothing AndAlso drConfigRow.Count > 0 Then mblnCountAttendanceNotLVIfLVapplied = CBool(drConfigRow(0)("key_value").ToString())
                        '            drConfigRow = Nothing

                        '            Dim xStartDate As DateTime = Nothing
                        '            Dim xEndDate As DateTime = Nothing

                        '            getCurrentPeriodTenure(dr("database_name").ToString(), xYearId, Now.Date, xStartDate, xEndDate)

                        '            ' START AS TRA IS HAVING WHOLE YEAR AS PERIOD SO WE ARE DOING EVERY DAY ABSENT PROCESS FOR ALL EMPLOYEES
                        '            xStartDate = Now.Date
                        '            xEndDate = Now.Date
                        '            ' END AS TRA IS HAVING WHOLE YEAR AS PERIOD SO WE ARE DOING EVERY DAY ABSENT PROCESS FOR ALL EMPLOYEES

                        '            StartAbsentProcess(dr("database_name").ToString(), xStartDate.Date, xEndDate.Date, mintLeaveBalanceSetting, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                        '                                         , mblnIsHolidayConsiderOnDayoff, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                        '                                         , mblnCountAttendanceNotLVIfLVapplied, objSendmail)
                        '        End If
                        '    End If
                        '    drRow = Nothing
                        '    'Pinkal (14-Feb-2022) -- End


                        'End If 'If strGroupName.ToUpper() <> "TANZANIA REVENUE AUTHORITY" Then

                        'Pinkal (21-Jul-2023) -- End

                    End If ' If IsDBNull(dtParams) = False AndAlso dtParams.Rows.Count > 0 Then

                Next

            End If
        Catch ex As Exception
            WriteLog("OnElapsedTime:- " & ex.Message)
        Finally
            'Pinkal (15-Sep-2023) -- Start
            '(A1X-158) Mbeya Cement: HIK Device Integration - Automatic attendance data download
            If mintHIKUserID >= 0 Then
                CHCNetSDK.NET_DVR_Logout_V30(mintHIKUserID)
                mintHIKUserID = -1
                CHCNetSDK.NET_DVR_Cleanup()
            End If
            'Pinkal (15-Sep-2023) -- End

            t = Nothing
            objLogin = Nothing
            If sqlCn.State = ConnectionState.Open Or sqlCn.State = ConnectionState.Broken Then
                sqlCn.Close()
            End If
            timer.Start()
        End Try
    End Sub
    'Pinkal (14-Feb-2022) -- End

#End Region

#Region "Enum"

    Public Enum enModuleReference
        Payroll = 1
        Leave = 2
        Medical = 3
        Training = 4
        Assessment = 5
        TnA = 6
        PDP = 7
        Talent = 8
        Succession = 9
        Miscellaneous = 999
    End Enum

    Public Enum enStatusType
        OPEN = 1
        CLOSE = 2
    End Enum

#End Region

#Region "Private Method"

    Private Function IsConnect() As Boolean
        Try
            sqlCn = New SqlConnection("Data Source=.\Apayroll;Initial Catalog=hrmsConfiguration;User ID=sa;Password=pRofessionalaRuti999")
            sqlCn.Open()
            sqlCmd = New SqlCommand
            sqlCmd.CommandTimeout = 0
            sqlCmd.Connection = sqlCn
            GetDatabaseVersion()
            Return True
        Catch ex As Exception
            Try
                sqlCn = New SqlConnection("Data Source=.\Apayroll;Initial Catalog=hrmsConfiguration;User ID=aruti_sa;Password=pRofessionalaRuti999")
                sqlCn.Open()
                sqlCmd = New SqlCommand
                sqlCmd.CommandTimeout = 0
                sqlCmd.Connection = sqlCn
                GetDatabaseVersion()
                Return True
            Catch ex1 As Exception
                WriteLog("IsConnect:- " & ex1.Message)
            End Try

            WriteLog("IsConnect : " & ex.Message)
        End Try
    End Function

    Private Sub GetDatabaseVersion()
        Dim strQ As String = ""
        Try
            strQ = "DECLARE @tbl_version TABLE (dbversion NVARCHAR(100)) " & _
                   "DECLARE @SP_CHECK_VERSION AS NVARCHAR(MAX) " & _
                   "    INSERT INTO @tbl_version (dbversion) EXEC version " & _
                   "    SET @SP_CHECK_VERSION = (SELECT dbversion FROM @tbl_version) " & _
                   "DECLARE @POS_START INT SET @POS_START = 1 " & _
                   "DECLARE @POS_END INT SET @POS_END = CHARINDEX('.', @SP_CHECK_VERSION, @POS_START) " & _
                   "DECLARE @COLUMN AS INT SET @COLUMN = 3 " & _
                   "   WHILE (@COLUMN > 1 AND @POS_END > 0) " & _
                   "      BEGIN " & _
                   "         SET @POS_START = @POS_END + 1 " & _
                   "         SET @POS_END = CHARINDEX('.', @SP_CHECK_VERSION, @POS_START) " & _
                   "         SET @COLUMN = @COLUMN - 1 " & _
                   "      END " & _
                   "   IF @COLUMN > 1  SET @POS_START = LEN(@SP_CHECK_VERSION) + 1 " & _
                   "   IF @POS_END = 0 SET @POS_END = LEN(@SP_CHECK_VERSION) + 1 " & _
                   "SELECT SUBSTRING (@SP_CHECK_VERSION, @POS_START, @POS_END - @POS_START) "

            Dim Qcmd As New SqlCommand(strQ, sqlCn)
            mintDatabaseVersion = CInt(Qcmd.ExecuteScalar())
        Catch ex As Exception
            WriteLog("GetDatabaseVersion:- " & ex.Message)
        End Try
    End Sub

    Private Function GetCompanyDetails(ByVal xCompanyId As Integer) As DataTable
        Dim dtList As DataTable = Nothing
        Dim strQ As String = ""
        Try

            strQ = " SELECT " & _
                      " cfcompany_master.companyunkid " & _
                      ",senderaddress " & _
                      ",sendername " & _
                      ",mailserverip " & _
                      ",mailserverport " & _
                      ",username " & _
                      ",password  " & _
                      ",isloginssl " & _
                      ",cffinancial_year_tran.yearunkid " & _
                      ",cffinancial_year_tran.database_name " & _
                      ",cffinancial_year_tran.financialyear_name " & _
                      ",cffinancial_year_tran.start_date  " & _
                      ",cffinancial_year_tran.end_date " & _
                      ",hrmsconfiguration..cfcompany_master.name " & _
                      " FROM hrmsconfiguration..cfcompany_master " & _
                      " JOIN hrmsconfiguration..cffinancial_year_tran ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                      " WHERE isactive = 1 AND cfcompany_master.companyunkid = @companyunkid "
            'Pinkal (14-Feb-2022) -- Start {name} -- End

            Dim sqlDataadp As New SqlDataAdapter(strQ, sqlCn)
            sqlDataadp.SelectCommand.Parameters.AddWithValue("@companyunkid", xCompanyId)
            dtList = New DataTable
            sqlDataadp.Fill(dtList)
        Catch ex As Exception
            WriteLog("GetCompanyDetails:- " & ex.Message)
            Return Nothing
        End Try
        Return dtList
    End Function

    Private Function IsArutiAttendanceConfigured() As DataTable
        Dim dt As New DataTable()
        Dim StrQ As String = ""
        Try

            'Pinkal (14-Feb-2022) -- Start
            'Enhancement TRA : TnA Module Enhancement for TRA.

            StrQ = " SELECT " & _
                       "     CF.companyunkid " & _
                       "    ,FT.database_name " & _
                       "    ,FT.yearunkid " & _
                       " FROM hrmsConfiguration..cfconfiguration AS CF " & _
                       " JOIN hrmsConfiguration..cffinancial_year_tran AS FT ON FT.companyunkid = CF.companyunkid " & _
                       " WHERE UPPER(CF.[key_name]) = 'RUNDAILYATTENDANCESERVICETIME'  AND (CF.key_value <> '00:00:00' AND CF.key_value <> '00:00' AND CF.key_value <> '')  AND FT.isclosed = 0 " & _
                       " UNION " & _
                       " SELECT " & _
                       "     CF.companyunkid " & _
                       "    ,FT.database_name " & _
                       "    ,FT.yearunkid " & _
                       " FROM hrmsConfiguration..cfconfiguration AS CF " & _
                       " JOIN hrmsConfiguration..cffinancial_year_tran AS FT ON FT.companyunkid = CF.companyunkid " & _
                       " WHERE UPPER(CF.[key_name]) = 'RUNDAILYABSENTPROCESSTIME'  AND (CF.key_value <> '00:00:00' AND CF.key_value <> '00:00' AND CF.key_value <> '')  AND FT.isclosed = 0 "

            'Pinkal (14-Feb-2022) -- End


            If sqlCmd Is Nothing Then
                sqlCmd = New SqlCommand()
                sqlCmd.Connection = sqlCn
            End If
            sqlCmd.Parameters.Clear()
            sqlCmd.CommandText = StrQ

            Dim da As New SqlDataAdapter(sqlCmd)
            da.Fill(dt)
        Catch ex As SqlException
            WriteLog("IsArutiAttendanceConfigured : " & ex.Message)
        Catch ex As Exception
            WriteLog("IsArutiAttendanceConfigured : " & ex.Message)
        End Try
        Return dt
    End Function

    Private Sub IsDatabaseAccessible(ByVal strDBName As String)
        Dim StrQ As String = ""
        Dim intDBId As Integer = 0
        If sqlCmd Is Nothing Then
            sqlCmd = New SqlCommand()
            sqlCmd.Connection = sqlCn
        End If
        Try
            StrQ = "SELECT database_id " & "FROM sys.databases " & "WHERE name = '" & strDBName & "' AND user_access_desc = 'MULTI_USER' "
            sqlCmd.Parameters.Clear()
            sqlCmd.CommandText = StrQ

            intDBId = CInt(sqlCmd.ExecuteScalar())
            If intDBId > 0 Then
                mblnIsDatabaseAccessible = True
            End If

        Catch ex As Exception
            WriteLog("IsDatabaseAccessible " & Date.Now & " : " & ex.Message)
        End Try
    End Sub

    Private Function GetAttendanceParameters(ByVal xCompanyId As Integer) As DataTable
        Dim dt As New DataTable()
        Dim StrQ As String = ""
        Dim StrCols As String = ""
        Try

            'Pinkal (14-Feb-2022) -- Enhancement TRA : TnA Module Enhancement for TRA.['RUNDAILYABSENTPROCESSTIME']

            StrCols = "'USERACCESSMODESETTING', 'FIRSTCHECKINLASTCHECKOUT', 'POLICYMANAGEMENTTNA', 'DONOTATTENDANCEINSECONDS', 'ISHOLIDAYCONSIDERONWEEKEND', 'ISDAYOFFCONSIDERONWEEKEND','ISHOLIDAYCONSIDERONDAYOFF', " & _
                           "'RUNDAILYATTENDANCESERVICETIME','SENDTNAEARLYLATEREPORTSUSERIDS','TNAFAILURENOTIFICATIONUSERIDS', 'RUNDAILYABSENTPROCESSTIME','LEAVEBALANCESETTING', " & _
                           "'ISDAYOFFCONSIDERONWEEKEND','ISHOLIDAYCONSIDERONWEEKEND','ISHOLIDAYCONSIDERONDAYOFF','COUNTATTENDANCENOTLVIFLVAPPLIED' "


            StrQ = "SELECT " & _
                       "     CF.key_name " & _
                       "    ,CF.key_value " & _
                       " FROM hrmsConfiguration..cfconfiguration AS CF " & _
                       " WHERE CF.[key_name] IN (" & StrCols & ") AND CF.companyunkid = '" & xCompanyId & "' " & " AND CF.key_value <> '' "

            If sqlCmd Is Nothing Then
                sqlCmd = New SqlCommand()
                sqlCmd.Connection = sqlCn
            End If
            sqlCmd.Parameters.Clear()
            sqlCmd.CommandText = StrQ
            Dim da As New SqlDataAdapter(sqlCmd)
            da.Fill(dt)
        Catch ex As Exception
            WriteLog("GetAttendanceParameters " & Date.Now & " : " & ex.Message)
        End Try
        Return dt
    End Function

    Public Function GetConfigKeyValue(ByVal intCompanyUnkId As Integer, ByVal strKeyName As String, Optional ByVal sqlcmd As SqlCommand = Nothing) As String
        Dim strQ As String = ""
        Dim strKeyValue As String = ""
        Dim oCmd As SqlCommand = Nothing
        Try

            strQ = " SELECT  " & _
                      " key_value " & _
                      " FROM hrmsConfiguration..cfconfiguration " & _
                      " WHERE key_name = @key_name "

            If sqlcmd Is Nothing Then
                oCmd = New SqlCommand(strQ, sqlCn)
            Else
                oCmd = sqlcmd
            End If

            oCmd.Parameters.Clear()

            If intCompanyUnkId > 0 Then
                strQ &= " AND companyunkid =  @CompanyId "
                oCmd.Parameters.AddWithValue("@CompanyId", intCompanyUnkId)
            Else
                strQ &= " AND companyunkid IS NULL "
            End If

            oCmd.Parameters.AddWithValue("@key_name", strKeyName)
            oCmd.CommandText = strQ

            strKeyValue = oCmd.ExecuteScalar()

        Catch ex As Exception
            WriteLog("GetConfigKeyValue : " & ex.Message)
        End Try
        Return strKeyValue
    End Function

    Public Sub DeviceIntegration(ByVal objParameters As Object())
        Try
            Dim xDatabaseName As String = ""
            Dim xCompanyId As Integer = 0
            Dim objSendmail As clsSendMail = Nothing

            If objParameters IsNot Nothing Then
                xDatabaseName = objParameters(0).ToString()
                xCompanyId = CInt(objParameters(1))
                objSendmail = CType(objParameters(2), clsSendMail)
            End If


            If intDeviceType = Convert.ToInt16(enFingerPrintDevice.ZKSoftware) Then
                'DownloadZkData()    ;

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.BioStar) Then
                'DownloadBioStarData();    

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.ZKAcessControl) Then
                'DownloadZkAccessControlData()    ;

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.Anviz) Then
                DownloadAnvizData(xDatabaseName.ToString(), CInt(xCompanyId), objSendmail)

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.ACTAtek) Then
                'DownloadACTAtekData();    

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.BioStar2) Then
                'DownloadBioStar2Data();    

            ElseIf intDeviceType = Convert.ToInt16(enFingerPrintDevice.FingerTec) Then
                'DownloadFingerTecData();   

                'Pinkal (15-Sep-2023) -- Start
                '(A1X-158) Mbeya Cement: HIK Device Integration - Automatic attendance data download
            ElseIf intDeviceType = enFingerPrintDevice.HIKVision Then
                DownloadHIKVisionData()
                'Pinkal (15-Sep-2023) -- End

            End If
        Catch ex As Exception
            WriteLog("DeviceIntegration " & Date.Now & " : " & ex.Message)
        End Try
    End Sub

    Private Sub DownloadAnvizData(ByVal xDatabaseName As String, ByVal xCompanyId As Integer, ByVal objSendmail As clsSendMail)
        Try
            Dim mintErrorNo As Integer = 0

            mintErrorNo = clsAnviz.CKT_RegisterNet(mintMachineSrNo, strIp.Trim())

            If mintErrorNo <> clsAnviz.CKT_RESULT_OK Then
                If mintErrorNo = clsAnviz.CKT_ERROR_INVPARAM Then
                    mstrError = "Invalid Parameter." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_NETDAEMONREADY Then
                    mstrError = "Error in Netdaemonreday." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_CHECKSUMERR Then
                    mstrError = "Checksum Error." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_MEMORYFULL Then
                    mstrError = "Memory Full." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_INVFILENAME Then
                    mstrError = "Invalid File Name." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_FILECANNOTOPEN Then
                    mstrError = "File Cannot Open." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_FILECONTENTBAD Then
                    mstrError = "File Content Bad." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_FILECANNOTCREATED Then
                    mstrError = "File Cannot Create." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                ElseIf mintErrorNo = clsAnviz.CKT_ERROR_NOTHISPERSON Then
                    mstrError = "Not this Person." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub

                Else
                    mstrError = "Unable to connect the device." & mstrDeviceCode & " [" & strIp & "]."
                    clsAnviz.CKT_Disconnect()
                    SendFailedNotificationToUser(xDatabaseName, xCompanyId, True, mstrError, objSendmail)
                    Exit Sub
                End If

            End If


            Dim i As Integer = 0
            Dim RecordCount As New Integer()
            Dim RetCount As New Integer()
            Dim pClockings As New Integer()
            Dim pLongRun As New Integer()
            Dim ptemp As Integer = 0
            Dim tempptr As Integer = 0
            Dim ret As Integer = 0
            Dim j As Integer = 0


            If clsAnviz.CKT_GetClockingRecordEx(mintMachineSrNo, pLongRun) = 1 Then

                While True
                    ret = clsAnviz.CKT_GetClockingRecordProgress(pLongRun, RecordCount, RetCount, pClockings)
                    If ret <> 0 Then
                        ptemp = Marshal.SizeOf(clocking)
                        tempptr = pClockings
                        For i = 0 To RetCount - 1
                            RtlMoveMemory(clocking, pClockings, ptemp)
                            pClockings = pClockings + ptemp
                            If clocking.PersonID < 0 Then
                                Continue For
                            End If

                            'Pinkal (18-Aug-2023) -- Start
                            '(A1X -1189) St. Judes - Automatic download of attendance data from Anviz device. 

                            'If Convert.ToInt32(convertDate(Convert.ToDateTime(mstrTime).Date)) >= Convert.ToInt32(convertDate(Date.Now.AddDays(-1).Date)) AndAlso Convert.ToInt32(convertDate(Convert.ToDateTime(mstrTime))) <= Convert.ToInt32(convertDate(Date.Now.AddDays(-1).Date)) Then
                            '    If InsertDeviceData(xDatabaseName, mstrDeviceCode, strIp, clocking.PersonID.ToString(), Convert.ToDateTime(mstrTime).Date, Convert.ToDateTime(mstrTime), "0", "-1", mstrError) = False Then
                            '        SendFailedNotificationToUser(xDatabaseName, xCompanyId, False, mstrError, objSendmail)
                            '        Exit Sub
                            '    End If
                            'Else
                            '    Continue For
                            'End If

                            Dim mstrTime As String = Encoding.Default.GetString(clocking.Time).ToString()

                            'WriteLog("mstrTime : " & mstrTime)
                            'WriteLog("clocking.PersonID : " & clocking.PersonID)

                            If Convert.ToInt32(convertDate(Convert.ToDateTime(mstrTime).Date)) >= Convert.ToInt32(convertDate(Date.Now.AddDays(-1).Date)) AndAlso Convert.ToInt32(convertDate(Convert.ToDateTime(mstrTime))) <= Convert.ToInt32(convertDate(Date.Now.AddDays(-1).Date)) Then

                                If dtAttendanceData Is Nothing Then IntializeAttendanceDataTable()

                                If dtAttendanceData IsNot Nothing Then
                                    Dim drRow As DataRow = dtAttendanceData.NewRow()
                                    drRow("UserId") = clocking.PersonID.ToString()
                                    drRow("UserName") = ""
                                    drRow("Device") = mstrDeviceCode
                                    drRow("IPAddress") = strIp
                                    drRow("LoginDate") = Convert.ToDateTime(mstrTime)
                                    dtAttendanceData.Rows.Add(drRow)
                                    dtAttendanceData.AcceptChanges()
                                End If
                            Else
                                Continue For
                            End If

                            'Pinkal (18-Aug-2023) -- End 

                        Next

                        If tempptr <> 0 Then
                            clsAnviz.CKT_FreeMemory(tempptr)
                        End If

                        If ret = 1 Then
                            Exit While
                        End If

                    End If

                End While

            End If
        Catch ex1 As ThreadAbortException
            WriteLog("DownloadAnvizData ThreadAbort : " & Date.Now & " : " & ex1.Message)
            SendFailedNotificationToUser(xDatabaseName, xCompanyId, False, ex1.Message.ToString(), objSendmail)
        Catch ex As Exception
            WriteLog("DownloadAnvizData " & Date.Now & " : " & ex.Message)
            SendFailedNotificationToUser(xDatabaseName, xCompanyId, False, ex.Message.ToString(), objSendmail)
        Finally
            clsAnviz.CKT_Disconnect()
        End Try
    End Sub

    Private Function InsertDeviceData(ByVal xDatabaseName As String, ByVal mstrDeviceCode As String, ByVal mstrIPAddress As String, ByVal mstrEnrollNo As String, ByVal mdtLoginDate As Date, ByVal mdtLoginTime As Date, ByVal mstrVerifyCode As String, ByVal mstrInOutMode As String, ByRef mstrError As String) As Boolean
        Dim objLogin As New clslogin_Tran()
        Try

            objLogin._DeviceAtt_DeviceNo = mstrDeviceCode

            objLogin._DeviceAtt_EnrollNo = mstrEnrollNo
            objLogin._DeviceAtt_IPAddress = mstrIPAddress
            objLogin._DeviceAtt_LoginDate = mdtLoginDate.Date
            objLogin._DeviceAtt_LoginTime = mdtLoginTime
            objLogin._DeviceAtt_VerifyMode = mstrVerifyCode
            objLogin._DeviceAtt_InOutMode = mstrInOutMode
            objLogin._DeviceAtt_IsError = False

            If objLogin.InsertDeviceAttendanceData(sqlCn, xDatabaseName) = False Then
                mstrError = objLogin._Message
                Return False
            End If
        Catch ex As Exception
            WriteLog("InsertDeviceData " & Date.Now & " : " & ex.Message)
            Return False
        Finally
            objLogin = Nothing
        End Try
        Return True
    End Function

    Private Sub SaveAttendanceData(ByVal xDatabaseName As String, ByVal xCompanyId As Integer, ByVal xYearId As Integer, ByVal xFirstInLastout As Boolean, ByVal xPolicyManagement As Boolean _
                                                  , ByVal xDonotAttendanceinSeconds As Boolean, ByVal IsHolidayConsiderOnWeekend As Boolean, ByVal IsDayOffConsiderOnWeekend As Boolean _
                                                  , ByVal IsHolidayConsiderOnDayoff As Boolean, ByVal dtTable As DataTable)

        Dim dsList As DataSet = Nothing
        Dim xEmployeeId As Integer = -1
        Dim intShiftId As Integer = -1
        Dim dtLogindate As Date = Nothing
        Dim dtCheckIntime As DateTime = Nothing
        Dim dtCheckOuttime As DateTime = Nothing
        Dim objLogin As clslogin_Tran = Nothing
        Dim xUserId As Integer = 1

        Try

            WriteLog("SaveAttendanceData : Start Saving")

            If dtTable IsNot Nothing Then  ' dtTable IsNot Nothing 

                If dtTable.Rows.Count > 0 Then   'dtTable.Rows.Count > 0

                    For Each dr As DataRow In dtTable.Rows

                        objLogin = New clslogin_Tran

                        Dim objShiftTran As New clsshift_tran

                        Dim mblnAttendanceOnDeviceInOutStatus As Boolean = False
                        Dim mintshiftId As Integer = 0

                        Dim mdtLoginDate As DateTime = CDate(dr("Logindate"))
                        xEmployeeId = objLogin.GetEmployeeUnkID(sqlCn, Nothing, xDatabaseName, dr("UserId").ToString())

                        If xEmployeeId.ToString().Trim.Length <= 0 Then Exit Sub

                        dsList = objLogin.GetEmployeeLoginDate(sqlCn, xDatabaseName, xEmployeeId, mdtLoginDate.Date)

                        dtCheckIntime = mdtLoginDate
                        dtCheckOuttime = mdtLoginDate

                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then   'dsList.Tables("List").Rows.Count <= 0

                            objLogin._Employeeunkid = xEmployeeId
                            objLogin._SourceType = enInOutSource.Import
                            objLogin._Userunkid = xUserId

                            If xFirstInLastout = False Then

                                Dim intCurrentShiftID As Integer = 0
                                intCurrentShiftID = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, mdtLoginDate.Date, xEmployeeId)

                                objLogin._Shiftunkid = intCurrentShiftID

                                intShiftId = intCurrentShiftID

                                objLogin._Logindate = mdtLoginDate.Date

                                objShiftTran.GetShiftTran(sqlCn, Nothing, xDatabaseName, objLogin._Shiftunkid)

                                Dim drShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLoginDate.Date), False, FirstDayOfWeek.Sunday)))
                                If drShiftTran.Length > 0 Then

                                    Dim objShift As New clsNewshift_master
                                    objShift._Shiftunkid(sqlCn, Nothing, xDatabaseName) = objLogin._Shiftunkid

                                    mblnAttendanceOnDeviceInOutStatus = objShift._AttOnDeviceInOutStatus

                                    If objShift._IsOpenShift = False Then
                                        Dim mdtDayStartTime As DateTime = CDate(mdtLoginDate.Date & " " & CDate(drShiftTran(0)("daystart_time")).ToString("hh:mm:ss tt"))

                                        If DateDiff(DateInterval.Minute, mdtDayStartTime, mdtLoginDate) <= 0 Then   ' FOR NIGHT SHIFT PROBLEM
                                            Dim mintPreviousShift As Integer = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, mdtLoginDate.Date.AddDays(-1).Date, xEmployeeId)

                                            Dim objPrevShift As New clsNewshift_master
                                            objPrevShift._Shiftunkid(sqlCn, Nothing, xDatabaseName) = mintPreviousShift

                                            If objPrevShift._IsOpenShift = False Then

                                                If mintPreviousShift <> intShiftId Then
                                                    objShiftTran.GetShiftTran(sqlCn, Nothing, xDatabaseName, mintPreviousShift)
                                                    Dim drPreviousShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLoginDate.Date.AddDays(-1).Date), False, FirstDayOfWeek.Sunday)))
                                                    If drPreviousShiftTran.Length > 0 Then
                                                        Dim mdtPreviousDayStartTime As DateTime = CDate(mdtLoginDate.Date & " " & CDate(drPreviousShiftTran(0)("daystart_time")).ToString("hh:mm tt"))
                                                        If DateDiff(DateInterval.Hour, mdtPreviousDayStartTime, mdtLoginDate) >= 24 Then
                                                            objLogin._Logindate = mdtLoginDate.Date
                                                        Else
                                                            objLogin._Employeeunkid = xEmployeeId
                                                            If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, convertDate(mdtLoginDate.Date.AddDays(-1).Date).ToString(), "", xEmployeeId, -1) Then
                                                                objLogin._Logindate = mdtLoginDate.Date.AddDays(-1)
                                                            Else
                                                                objLogin._Logindate = mdtLoginDate.Date
                                                            End If
                                                        End If
                                                    End If
                                                Else
                                                    objLogin._Logindate = mdtLoginDate.Date.AddDays(-1)
                                                End If

                                            Else
                                                objLogin._Logindate = mdtLoginDate.Date
                                            End If
                                            objPrevShift = Nothing

                                        ElseIf DateDiff(DateInterval.Minute, mdtDayStartTime, mdtLoginDate) >= 0 Then
                                            objLogin._Logindate = mdtLoginDate.Date
                                        End If
                                    Else
                                        objLogin._Logindate = mdtLoginDate.Date
                                    End If
                                    objShift = Nothing
                                End If


                                If mblnAttendanceOnDeviceInOutStatus Then

                                    intCurrentShiftID = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, mdtLoginDate.Date, xEmployeeId)

                                    objLogin._Shiftunkid = intCurrentShiftID

                                    intShiftId = objLogin._Shiftunkid

                                    If objLogin.GetLoginType(sqlCn, xDatabaseName, xEmployeeId, mdtLoginDate.Date) = 0 Then 'IN STATUS

                                        objLogin._checkintime = mdtLoginDate
                                        objLogin._Original_InTime = objLogin._checkintime
                                        objLogin._CheckInDevice = dr("Device").ToString()
                                        objLogin._Checkouttime = Nothing
                                        objLogin._Original_OutTime = Nothing
                                        objLogin._InOutType = 0
                                        objLogin._Workhour = 0
                                        objLogin._Holdunkid = 0
                                        objLogin._Voiddatetime = Nothing
                                        objLogin._Isvoid = False
                                        objLogin._Voidreason = ""

                                        If objLogin.Insert(sqlCn, Nothing, xDatabaseName, xUserId, xYearId, xCompanyId, objLogin._Logindate.Date, objLogin._Logindate.Date, True _
                                                                 , xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                 , IsHolidayConsiderOnDayoff) = False Then

                                            If objLogin._Message.Trim.Length > 0 Then
                                                WriteLog("SaveAttendanceData Insert :- " & objLogin._Message)
                                            End If
                                        End If  '  If objLogin.Insert

                                    Else 'OUT STATUS

                                        Dim intLoginId As Integer = -1
                                        If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, convertDate(objLogin._Logindate.Date), "", xEmployeeId, intLoginId) Then

                                            objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = intLoginId
                                            objLogin._Checkouttime = mdtLoginDate
                                            objLogin._CheckOutDevice = dr("Device").ToString()
                                            objLogin._Original_OutTime = objLogin._Checkouttime
                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                            objLogin._Workhour = CInt(wkmins)
                                            objLogin._InOutType = 1
                                            objLogin._SourceType = enInOutSource.Import
                                            objLogin._Shiftunkid = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, objLogin._Logindate.Date, xEmployeeId)


                                            If objLogin.Update(sqlCn, Nothing, xDatabaseName, xUserId, xYearId, xCompanyId, objLogin._Logindate.Date, objLogin._Logindate.Date _
                                                                      , True, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                      , IsHolidayConsiderOnDayoff) = False Then


                                                If objLogin._Message.Trim.Length > 0 Then
                                                    WriteLog("SaveAttendanceData Update :- " & objLogin._Message)
                                                End If

                                            End If  'objLogin.Update

                                        End If  'objLogin.IsEmployeeLoginExist

                                    End If  'mintLoginTypeId = 0

                                ElseIf mblnAttendanceOnDeviceInOutStatus = False Then

                                    If objLogin.GetLoginType(sqlCn, xDatabaseName, xEmployeeId, objLogin._Logindate.Date) = 0 Then

                                        objLogin._Employeeunkid = xEmployeeId
                                        objLogin._Logindate = mdtLoginDate.Date
                                        objLogin._checkintime = mdtLoginDate
                                        objLogin._Original_InTime = objLogin._checkintime
                                        objLogin._CheckInDevice = dr("Device").ToString()

                                        intCurrentShiftID = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, mdtLoginDate.Date, xEmployeeId)

                                        objLogin._Shiftunkid = intCurrentShiftID
                                        intShiftId = objLogin._Shiftunkid

                                        objLogin._Checkouttime = Nothing
                                        objLogin._Original_OutTime = Nothing
                                        objLogin._InOutType = 0
                                        objLogin._Workhour = 0
                                        objLogin._Holdunkid = 0
                                        objLogin._Voiddatetime = Nothing
                                        objLogin._Isvoid = False
                                        objLogin._Voidreason = ""

                                        If objLogin.Insert(sqlCn, Nothing, xDatabaseName, xUserId, xYearId, xCompanyId, objLogin._Logindate.Date, objLogin._Logindate.Date, True _
                                                             , xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                             , IsHolidayConsiderOnDayoff) = False Then

                                            If objLogin._Message.Trim.Length > 0 Then
                                                WriteLog("SaveAttendanceData Insert :- " & objLogin._Message)
                                            End If

                                        End If  'objLogin.Insert

                                    Else

                                        objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName)
                                        objLogin._Checkouttime = mdtLoginDate
                                        objLogin._Original_OutTime = objLogin._Checkouttime
                                        objLogin._CheckOutDevice = dr("Device").ToString()

                                        intCurrentShiftID = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, objLogin._Logindate.Date, xEmployeeId)

                                        objLogin._Shiftunkid = intCurrentShiftID

                                        intShiftId = objLogin._Shiftunkid


                                        If objLogin._checkintime > objLogin._Checkouttime Then
                                            Continue For
                                        End If


                                        If objLogin._checkintime < objLogin._Checkouttime Then
                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                            objLogin._Workhour = CInt(wkmins)
                                            objLogin._InOutType = 1
                                            objLogin._SourceType = enInOutSource.Import


                                            If objLogin.Update(sqlCn, Nothing, xDatabaseName, xUserId, xYearId, xCompanyId, objLogin._Logindate.Date, objLogin._Logindate.Date _
                                                              , True, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                              , IsHolidayConsiderOnDayoff) = False Then

                                                If objLogin._Message.Trim.Length > 0 Then
                                                    WriteLog("SaveAttendanceData Insert :- " & objLogin._Message)
                                                End If

                                            End If  'objLogin.Update

                                        End If  'If objLogin._checkintime < objLogin._Checkouttime Then

                                    End If  'If objLogin.GetLoginType(sqlCn, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate.Date) = 0 Then

                                End If  'If mblnAttendanceOnDeviceInOutStatus Then


                            ElseIf xFirstInLastout Then   'ConfigParameter._Object._FirstCheckInLastCheckOut

                                Dim intCurrentShiftID As Integer = 0

                                If IsDBNull(mdtLoginDate) Then Continue For

                                intCurrentShiftID = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, mdtLoginDate.Date, xEmployeeId)
                                objLogin._Shiftunkid = intCurrentShiftID
                                intShiftId = objLogin._Shiftunkid

                                objShiftTran.GetShiftTran(sqlCn, Nothing, xDatabaseName, objLogin._Shiftunkid)
                                Dim drShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLoginDate.Date), False, FirstDayOfWeek.Sunday)))
                                intShiftId = objLogin._Shiftunkid

                                dtLogindate = mdtLoginDate.Date
                                dtCheckIntime = mdtLoginDate
                                dtCheckOuttime = mdtLoginDate

                                If drShiftTran.Length > 0 Then   'drShiftTran.Length > 0

                                    Dim objShift As New clsNewshift_master
                                    objShift._Shiftunkid(sqlCn, Nothing, xDatabaseName) = objLogin._Shiftunkid

                                    mblnAttendanceOnDeviceInOutStatus = objShift._AttOnDeviceInOutStatus

                                    If objShift._IsOpenShift = False Then
                                        If IsDBNull(drShiftTran(0)("daystart_time")) Then Continue For
                                        Dim mdtDayStartTime As DateTime = CDate(mdtLoginDate.Date & " " & CDate(drShiftTran(0)("daystart_time")).ToString("hh:mm tt"))

                                        If DateDiff(DateInterval.Minute, mdtDayStartTime, mdtLoginDate) <= 0 Then   ' FOR NIGHT SHIFT PROBLEM

                                            Dim mintPreviousShift As Integer = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, mdtLoginDate.Date.AddDays(-1).Date, xEmployeeId)

                                            Dim objPrevShift As New clsNewshift_master
                                            objPrevShift._Shiftunkid(sqlCn, Nothing, xDatabaseName) = mintPreviousShift

                                            If objPrevShift._IsOpenShift = False Then

                                                If mintPreviousShift <> intShiftId Then
                                                    objShiftTran.GetShiftTran(sqlCn, Nothing, xDatabaseName, mintPreviousShift)
                                                    Dim drPreviousShiftTran() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(mdtLoginDate.Date.AddDays(-1).Date), False, FirstDayOfWeek.Sunday)))
                                                    If drPreviousShiftTran.Length > 0 Then
                                                        Dim mdtPreviousDayStartTime As DateTime = CDate(mdtLoginDate.Date & " " & CDate(drPreviousShiftTran(0)("daystart_time")).ToString("hh:mm tt"))
                                                        If DateDiff(DateInterval.Hour, mdtPreviousDayStartTime, mdtLoginDate) >= 24 Then
                                                            objLogin._Logindate = mdtLoginDate.Date
                                                        Else
                                                            objLogin._Employeeunkid = xEmployeeId
                                                            If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, convertDate(mdtLoginDate.Date.AddDays(-1).Date).ToString(), "", xEmployeeId, -1) Then
                                                                objLogin._Logindate = mdtLoginDate.Date.AddDays(-1)
                                                            Else
                                                                objLogin._Logindate = mdtLoginDate.Date
                                                            End If
                                                        End If
                                                    End If
                                                Else
                                                    objLogin._Logindate = mdtLoginDate.Date.AddDays(-1)
                                                End If
                                            Else
                                                objLogin._Logindate = mdtLoginDate.Date
                                            End If  'If objPrevShift._IsOpenShift = False Then

                                        ElseIf DateDiff(DateInterval.Minute, mdtDayStartTime, mdtLoginDate) >= 0 Then
                                            objLogin._Logindate = mdtLoginDate.Date
                                        End If
                                    Else
                                        objLogin._Logindate = mdtLoginDate.Date
                                    End If
                                    objShift = Nothing


                                    If mblnAttendanceOnDeviceInOutStatus Then  'ConfigParameter._Object._SetDeviceInOutStatus

                                        If objLogin.GetLoginType(sqlCn, xDatabaseName, xEmployeeId, objLogin._Logindate.Date) = 0 Then 'IN STATUS

                                            Dim mintLoginId As Integer = -1

                                            If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, convertDate(mdtLoginDate.Date), "", xEmployeeId, mintLoginId) Then   'objLogin.IsEmployeeLoginExist

                                                objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = mintLoginId

                                                If objLogin._checkintime = Nothing AndAlso objLogin._Checkouttime <> Nothing Then  ' objLogin._checkintime = Nothing AndAlso objLogin._Checkouttime <> Nothing
                                                    objLogin._checkintime = mdtLoginDate
                                                    objLogin._Original_InTime = objLogin._checkintime

                                                    Dim objShiftInMst As New clsNewshift_master
                                                    objShiftInMst._Shiftunkid(sqlCn, Nothing, xDatabaseName) = intCurrentShiftID
                                                    If objShiftInMst._MaxHrs > 0 Then  'objShiftInMst._MaxHrs > 0 
DeviceInStatus:
                                                        If DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs) Then  'DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs)
                                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)

                                                            objLogin._CheckOutDevice = dr("Device").ToString()
                                                            objLogin._Workhour = CInt(wkmins)
                                                            objLogin._InOutType = 1
                                                            objLogin._SourceType = enInOutSource.Import

                                                            If objLogin.Update(sqlCn, Nothing, xDatabaseName, xUserId, xYearId, xCompanyId, objLogin._Logindate.Date, objLogin._Logindate.Date _
                                                                                       , True, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                                       , IsHolidayConsiderOnDayoff) = False Then

                                                                If objLogin._Message.Trim.Length > 0 Then
                                                                    WriteLog("SaveAttendanceData Update :- " & objLogin._Message)
                                                                End If

                                                            End If  'objLogin.Update() = False

                                                        ElseIf DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) > CInt(objShiftInMst._MaxHrs) Then  'DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs)
                                                            GoTo DeviceInStatus1
                                                        End If   'DateDiff(DateInterval.Minute, objLogin._checkintime, objLogin._Checkouttime) <= CInt(objShiftInMst._MaxHrs)

                                                    ElseIf objShiftInMst._MaxHrs <= 0 Then  'objShiftInMst._MaxHrs > 0 
                                                        objLogin._checkintime = mdtLoginDate
                                                        objLogin._Original_InTime = objLogin._checkintime
                                                        GoTo DeviceInStatus
                                                    End If  'objShiftInMst._MaxHrs > 0 

                                                End If 'objLogin._checkintime = Nothing AndAlso objLogin._Checkouttime <> Nothing

                                            Else
DeviceInStatus1:
                                                objLogin._Logindate = mdtLoginDate.Date
                                                objLogin._Employeeunkid = xEmployeeId

                                                objLogin._checkintime = mdtLoginDate
                                                objLogin._Original_InTime = objLogin._checkintime
                                                objLogin._Checkouttime = Nothing
                                                objLogin._Original_OutTime = Nothing
                                                objLogin._CheckInDevice = dr("Device").ToString()
                                                objLogin._InOutType = 0
                                                objLogin._Workhour = 0
                                                objLogin._Holdunkid = 0
                                                objLogin._Voiddatetime = Nothing
                                                objLogin._Isvoid = False
                                                objLogin._Voidreason = ""

                                                If objLogin.Insert(sqlCn, Nothing, xDatabaseName, xUserId, xYearId, xCompanyId, objLogin._Logindate.Date, objLogin._Logindate.Date, True _
                                                      , xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                      , IsHolidayConsiderOnDayoff) = False Then

                                                    If objLogin._Message.Trim.Length > 0 Then
                                                        WriteLog("SaveAttendanceData Insert :- " & objLogin._Message)
                                                    End If

                                                End If 'objLogin.Insert

                                            End If  'objLogin.IsEmployeeLoginExist


                                        Else  'OUT STATUS

                                            Dim intLoginId As Integer = -1

                                            If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, mdtLoginDate.Date, "", xEmployeeId, intLoginId) Then

                                                objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = intLoginId
                                                objLogin._Checkouttime = mdtLoginDate
                                                objLogin._Original_OutTime = objLogin._Checkouttime

                                                If objLogin._checkintime <> Nothing Then
                                                    Dim objShiftMst As New clsNewshift_master
                                                    objShiftMst._Shiftunkid(sqlCn, Nothing, xDatabaseName) = intCurrentShiftID

                                                    If objShiftMst._MaxHrs > 0 Then
                                                        If DateDiff(DateInterval.Minute, objLogin._checkintime, mdtLoginDate) <= CInt(objShiftMst._MaxHrs) Then  'objShiftMst._MaxHrs > 0 
DeviceOutStatus:
                                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                            objLogin._Workhour = CInt(wkmins)
                                                            objLogin._InOutType = 1
                                                            objLogin._SourceType = enInOutSource.Import
                                                            objLogin._CheckOutDevice = dr("Device").ToString()

                                                            If objLogin.Update(sqlCn, Nothing, xDatabaseName, xUserId, xYearId, xCompanyId, objLogin._Logindate.Date, objLogin._Logindate.Date _
                                                                                  , True, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                                  , IsHolidayConsiderOnDayoff) = False Then

                                                                If objLogin._Message.Trim.Length > 0 Then
                                                                    WriteLog("SaveAttendanceData Update :- " & objLogin._Message)
                                                                End If

                                                            End If  'objLogin.Update

                                                        ElseIf DateDiff(DateInterval.Minute, objLogin._checkintime, mdtLoginDate) > CInt(objShiftMst._MaxHrs) Then  ''objShiftMst._MaxHrs > 0 
DeviceOutStatus1:
                                                            objLogin._Employeeunkid = xEmployeeId
                                                            objLogin._CheckInDevice = dr("Device").ToString()
                                                            objLogin._checkintime = Nothing
                                                            objLogin._Original_InTime = Nothing
                                                            objLogin._InOutType = 0
                                                            objLogin._Workhour = 0
                                                            objLogin._Holdunkid = 0
                                                            objLogin._Voiddatetime = Nothing
                                                            objLogin._Isvoid = False
                                                            objLogin._Voidreason = ""

                                                            If objLogin.Insert(sqlCn, Nothing, xDatabaseName, xUserId, xYearId, xCompanyId, objLogin._Logindate.Date, objLogin._Logindate.Date, True _
                                                                                  , xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                                  , IsHolidayConsiderOnDayoff) = False Then

                                                                If objLogin._Message.Trim.Length > 0 Then
                                                                    WriteLog("SaveAttendanceData Insert :- " & objLogin._Message)
                                                                End If

                                                            End If  'objLogin.Insert

                                                        End If   'If DateDiff(DateInterval.Minute, objLogin._checkintime, mdtLoginDate) <= CInt(objShiftMst._MaxHrs) Then 

                                                    ElseIf objShiftMst._MaxHrs <= 0 Then   'objShiftMst._MaxHrs <= 0 

                                                        If objLogin._checkintime <> Nothing Then   'objShiftMst._MaxHrs <= 0 
DeviceOutStatus2:
                                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                            objLogin._Workhour = CInt(wkmins)
                                                            objLogin._InOutType = 1
                                                            objLogin._SourceType = enInOutSource.Import
                                                            objLogin._CheckOutDevice = dr("Device").ToString()

                                                            If objLogin.Update(sqlCn, Nothing, xDatabaseName, xUserId, xYearId, xCompanyId, objLogin._Logindate.Date, objLogin._Logindate.Date _
                                                                             , True, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                             , IsHolidayConsiderOnDayoff) = False Then

                                                                If objLogin._Message.Trim.Length > 0 Then
                                                                    WriteLog("SaveAttendanceData Update :- " & objLogin._Message)
                                                                End If

                                                            End If  'objLogin.Update

                                                        ElseIf objLogin._checkintime = Nothing Then   'objShift._MaxHrs <= 0 

                                                            Dim dtDeviceTable As DataTable = objLogin.GetMaxLoginDate(sqlCn, xDatabaseName, mdtLoginDate.Date, xEmployeeId)
                                                            If dtDeviceTable IsNot Nothing And dtDeviceTable.Rows.Count > 0 Then
                                                                objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = CInt(dtDeviceTable.Rows(0)("loginunkid"))
                                                                objLogin._Checkouttime = mdtLoginDate
                                                                objLogin._Original_OutTime = objLogin._Checkouttime
                                                                Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                                                objLogin._Workhour = CInt(wkmins)
                                                                objLogin._InOutType = 1
                                                                objLogin._SourceType = enInOutSource.Import
                                                                objLogin._CheckOutDevice = dr("Device").ToString()

                                                                If objLogin.Update(sqlCn, Nothing, xDatabaseName, xUserId, xYearId, xCompanyId, objLogin._Logindate.Date, objLogin._Logindate.Date _
                                                                      , True, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                      , IsHolidayConsiderOnDayoff) = False Then

                                                                    If objLogin._Message.Trim.Length > 0 Then
                                                                        WriteLog("SaveAttendanceData Update :- " & objLogin._Message)
                                                                    End If

                                                                End If  'objLogin.Update
                                                            Else
                                                                Continue For
                                                            End If  '  If dtDeviceTable IsNot Nothing And dtDeviceTable.Rows.Count > 0 Then

                                                        End If   'objShiftMst._MaxHrs <= 0 

                                                    End If   'objShiftMst._MaxHrs <= 0 

                                                ElseIf objLogin._checkintime = Nothing Then  'objLogin._checkintime <> Nothing
                                                    GoTo DeviceOutStatus1
                                                End If

                                            Else  'IF employee login is not exist.

                                                Dim dtDeviceTable As DataTable = objLogin.GetMaxLoginDate(sqlCn, xDatabaseName, mdtLoginDate.Date, objLogin._Employeeunkid)

                                                If dtDeviceTable IsNot Nothing And dtDeviceTable.Rows.Count > 0 Then  'dtDeviceTable.Rows.Count > 0
                                                    objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = CInt(dtDeviceTable.Rows(0)("loginunkid"))
                                                    objLogin._Checkouttime = mdtLoginDate
                                                    objLogin._Original_OutTime = objLogin._Checkouttime
                                                    objLogin._CheckOutDevice = dr("Device").ToString()

                                                    Dim objShiftMst As New clsNewshift_master
                                                    objShiftMst._Shiftunkid(sqlCn, Nothing, xDatabaseName) = intCurrentShiftID
                                                    If objShiftMst._MaxHrs > 0 Then   'objShiftMst._MaxHrs > 0
                                                        If objLogin._checkintime <> Nothing AndAlso DateDiff(DateInterval.Minute, objLogin._checkintime, mdtLoginDate) <= CInt(objShiftMst._MaxHrs) Then
                                                            GoTo DeviceOutStatus
                                                        ElseIf objLogin._checkintime <> Nothing AndAlso DateDiff(DateInterval.Minute, objLogin._checkintime, mdtLoginDate) > CInt(objShiftMst._MaxHrs) Then
                                                            objLogin._Logindate = mdtLoginDate.Date
                                                            GoTo DeviceOutStatus1
                                                        End If 'objLogin._checkintime <> Nothing AndAlso DateDiff(DateInterval.Minute, objLogin._checkintime, CDate(dr("Logintime"))) <= CInt(objShiftMst._MaxHrs)
                                                    ElseIf objShiftMst._MaxHrs <= 0 Then   'objShiftMst._MaxHrs <= 0
                                                        GoTo DeviceOutStatus2
                                                    End If  'objShiftMst._MaxHrs > 0
                                                Else  ' 'dtDeviceTable.Rows.Count > 0
                                                    objLogin._Logindate = mdtLoginDate.Date
                                                    objLogin._Checkouttime = mdtLoginDate
                                                    objLogin._Original_OutTime = objLogin._Checkouttime
                                                    GoTo DeviceOutStatus1
                                                End If  'dtDeviceTable.Rows.Count > 0

                                            End If   'IF employee login is not exist.


                                        End If   'CInt(dr("InOutMode")) = 1


                                    ElseIf mblnAttendanceOnDeviceInOutStatus = False Then  'ConfigParameter._Object._SetDeviceInOutStatus = False

                                        Dim intLoginId As Integer = -1

                                        If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, convertDate(objLogin._Logindate.Date), "", xEmployeeId, intLoginId) Then

                                            objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = intLoginId
                                            objLogin._Checkouttime = mdtLoginDate
                                            objLogin._Original_OutTime = objLogin._Checkouttime
                                            Dim wkmins As Double = DateDiff(DateInterval.Second, objLogin._checkintime, objLogin._Checkouttime)
                                            objLogin._Workhour = CInt(wkmins)
                                            objLogin._InOutType = 1
                                            objLogin._SourceType = enInOutSource.Import
                                            objLogin._Shiftunkid = GetEmployee_Current_ShiftId(sqlCn, xDatabaseName, objLogin._Logindate.Date, xEmployeeId)
                                            objLogin._CheckOutDevice = dr("Device").ToString()


                                            If objLogin.Update(sqlCn, Nothing, xDatabaseName, xUserId, xYearId, xCompanyId, objLogin._Logindate.Date, objLogin._Logindate.Date _
                                                                      , True, xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                      , IsHolidayConsiderOnDayoff) = False Then

                                                If objLogin._Message.Trim.Length > 0 Then
                                                    WriteLog("SaveAttendanceData Update :- " & objLogin._Message)
                                                End If

                                            End If  'objLogin.Update


                                        Else

                                            objLogin._Employeeunkid = xEmployeeId
                                            objLogin._checkintime = mdtLoginDate
                                            objLogin._Original_InTime = objLogin._checkintime
                                            objLogin._Checkouttime = Nothing
                                            objLogin._Original_OutTime = Nothing
                                            objLogin._InOutType = 0
                                            objLogin._Workhour = 0
                                            objLogin._Holdunkid = 0
                                            objLogin._Voiddatetime = Nothing
                                            objLogin._Isvoid = False
                                            objLogin._Voidreason = ""
                                            objLogin._CheckInDevice = dr("Device").ToString()

                                            If objLogin.Insert(sqlCn, Nothing, xDatabaseName, xUserId, xYearId, xCompanyId, objLogin._Logindate.Date, objLogin._Logindate.Date, True _
                                                                             , xPolicyManagement, xDonotAttendanceinSeconds, xFirstInLastout, IsHolidayConsiderOnWeekend, IsDayOffConsiderOnWeekend _
                                                                             , IsHolidayConsiderOnDayoff) = False Then

                                                If objLogin._Message.Trim.Length > 0 Then
                                                    WriteLog("SaveAttendanceData Insert :- " & objLogin._Message)
                                                End If

                                            End If

                                        End If

                                    End If  'ConfigParameter._Object._SetDeviceInOutStatus

                                End If   'drShiftTran.Length > 0

                            End If  'ConfigParameter._Object._FirstCheckInLastCheckOut

                        End If  'dsList.Tables("List").Rows.Count <= 0

                    Next


                End If   'dtTable.Rows.Count > 0

            End If   ' dtTable IsNot Nothing 
        Catch ex As Exception
            Dim strMessage As String = "Employee : " & xEmployeeId & ",Shift : " & intShiftId & ",LoginDate : " & dtLogindate & ",CheckIntime : " & dtCheckIntime & ",CheckOuttime : " & dtCheckOuttime
            WriteLog("SaveAttendanceData " & Date.Now & " : " & strMessage & vbCrLf & ex.Message)
        Finally
        End Try
    End Sub

    Public Function GetEmployee_Current_ShiftId(ByVal SqlCn As SqlConnection, ByVal xDatabaseName As String, ByVal iDate As DateTime, ByVal iEmployId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim iValue As Integer = 0
        Dim sqlCmd As SqlCommand = Nothing
        Try
            strQ = "SELECT TOP 1 shiftunkid	AS iShift FROM " & xDatabaseName & "..hremployee_shift_tran WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & convertDate(iDate) & "' AND employeeunkid = '" & iEmployId & "' AND effectivedate IS NOT NULL ORDER BY CONVERT(CHAR(8),effectivedate,112) DESC "
            sqlCmd = New SqlCommand(strQ, SqlCn)
            sqlCmd.Parameters.Clear()
            iValue = CInt(sqlCmd.ExecuteScalar())
            Return iValue
        Catch ex As Exception
            WriteLog("GetEmployee_Current_ShiftId " & Date.Now & " : " & ex.Message)
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Sub SendFailedNotificationToUser(ByVal xDatabaseName As String, ByVal xCompanyunkid As Integer, ByVal mblnIsdisconnect As Boolean, ByVal mstrError As String, ByVal objSendmail As clsSendMail)
        Try
            Dim mstrModuleName As String = "clsEmployee_Master"
            Dim xUserLanguageId As Integer = -1
            Dim strMessage As String = ""

            If mstrTnAFailureNotificationUserIds.Trim.Length > 0 Then

                Dim NewCulture As CultureInfo = CType(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
                NewCulture.DateTimeFormat.ShortDatePattern = "dd-MMM-yyyy"
                NewCulture.DateTimeFormat.DateSeparator = "-"
                System.Threading.Thread.CurrentThread.CurrentCulture = NewCulture


                Dim mstrQuery As String = " SELECT " & _
                                                        " cfuser_master.userunkid " & _
                                                        ",ISNULL(cfuser_master.username,'') AS username " & _
                                                        ",ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') AS UName " & _
                                                        ",ISNULL(cfuser_master.languageunkid,1) AS languageunkid " & _
                                                        ",ISNULL(cfuser_master.email,'') AS email " & _
                                                        " FROM hrmsConfiguration..cfuser_master " & _
                                                        " WHERE cfuser_master.isactive = 1 AND cfuser_master.userunkid = @userunkid "


                For Each sId As String In mstrTnAFailureNotificationUserIds.Trim.Split(CChar(","))
                    strMessage = ""
                    Dim mstrUserName As String = ""
                    Dim mstrUserEmail As String = ""
                    Dim dtUser As New DataTable

                    Dim oCmd As New SqlCommand(mstrQuery, sqlCn)
                    oCmd.Parameters.Clear()
                    oCmd.Parameters.Add("@userunkid", SqlDbType.Int).Value = CInt(sId)
                    Dim adp As New SqlDataAdapter(oCmd)
                    adp.Fill(dtUser)


                    If dtUser IsNot Nothing AndAlso dtUser.Rows.Count > 0 Then

                        If dtUser.Rows(0)("UName").ToString().Trim.Length > 0 Then
                            mstrUserName = dtUser.Rows(0)("UName").ToString().Trim.ToLower()
                        Else
                            mstrUserName = dtUser.Rows(0)("username").ToString().Trim.ToLower()
                        End If

                        xUserLanguageId = CInt(dtUser.Rows(0)("languageunkid"))
                        mstrUserEmail = dtUser.Rows(0)("email").ToString()

                        strMessage = "<HTML> <BODY>"
                        strMessage &= "Dear " & CultureInfo.CurrentCulture.TextInfo.ToTitleCase(mstrUserName) & ", <BR></BR>"
                        If mblnIsdisconnect Then
                            strMessage &= "This is to notify you that device code <B>" & mstrDeviceCode & "</B> with IP Adress <B>(" & strIp & ")</B> was not able to connect." & "<BR></BR>"
                        Else
                            strMessage &= "This is to notify you that the process of downloading Attendance Data failed for <B> (" & Now.AddDays(-1).Date & ")</B>  due to below error." & "<BR></BR>"
                        End If

                        strMessage &= "<B>" & "Error Message :" & mstrError & "</B><BR></BR>"
                        strMessage &= "Regards."

                        Dim blnValue As Boolean = True
                        Dim mstrArutisignature As String = ""
                        mstrArutisignature = GetConfigKeyValue(xCompanyunkid, "ShowArutisignature")
                        blnValue = CBool(IIf(mstrArutisignature.ToString().Length > 0, mstrArutisignature.ToString(), True))
                        If blnValue Then
                            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        End If
                        strMessage &= "</BODY></HTML>"


                        objSendmail._Message = strMessage.ToString()
                        objSendmail._Form_Name = "ArutiReflex"
                        objSendmail._LogEmployeeUnkid = -1
                        objSendmail._OperationModeId = enLogin_Mode.DESKTOP
                        objSendmail._UserUnkid = 1
                        objSendmail._ModuleRefId = enAT_VIEW_TYPE.TNA_MGT

                        Dim mstrEmailError As String = ""
                        If mstrUserEmail.Trim.Length > 0 Then
                            objSendmail._Subject = "Notification for Failure of Attendance Data"
                            objSendmail._ToEmail = mstrUserEmail.Trim
                            mstrEmailError = objSendmail.SendMail()

                            Insert_Email_Tran_Log(enAT_VIEW_TYPE.TNA_MGT, xDatabaseName, objSendmail._SenderAddress, objSendmail._ToEmail, objSendmail._Subject, objSendmail._Message, 1, enLogin_Mode.DESKTOP, mstrEmailError, "")

                        End If

                    End If

                Next
            End If
        Catch ex As Exception
            WriteLog("SendFailedNotificationToUser : " & ex.Message)
        End Try
    End Sub

    Public Sub Insert_Email_Tran_Log(ByVal mintModuleRefId As enAT_VIEW_TYPE, ByVal mstrDatabaseName As String, ByVal strSenderaddress As String _
                                                     , ByVal mstrToEmail As String, ByVal mstrSubject As String, ByVal mstrMessage As String, ByVal xUserUnkid As Integer _
                                                     , ByVal xOperationModeId As Integer, Optional ByVal strReason As String = "", Optional ByVal mstrAttachedFiles As String = "")

        Dim StrQ As String = String.Empty

        Try
            Dim iTableName As String = String.Empty

            Select Case mintModuleRefId
                Case enAT_VIEW_TYPE.TNA_MGT
                    iTableName = mstrDatabaseName & "..attna_email_ntf_tran"
            End Select

            StrQ = "INSERT INTO " & iTableName & " " & _
                   "( " & _
                   "	 row_guid " & _
                   "	,sender_address " & _
                   "	,recipient_address " & _
                   "	,send_datetime " & _
                   "	,sent_data " & _
                   "	,subject " & _
                   "	,ip " & _
                   "	,host " & _
                   "	,form_name " & _
                   "	,module_name1 " & _
                   "	,module_name2 " & _
                   "	,module_name3 " & _
                   "	,module_name4 " & _
                   "	,module_name5 " & _
                   "	,operationmodeid " & _
                   "	,userunkid " & _
                   "	,loginemployeeunkid " & _
                   "	,fail_reason " & _
                   "	,filename " & _
                   "	,gateway " & _
                   ", isweb " & _
                   ") " & _
                   "VALUES " & _
                   "( " & _
                   "	 @row_guid " & _
                   "	,@sender_address " & _
                   "	,@recipient_address " & _
                   "	, getdate() " & _
                   "	,@sent_data " & _
                   "	,@subject " & _
                   "	,@ip " & _
                   "	,@host " & _
                   "	,@form_name " & _
                   "	,@module_name1 " & _
                   "	,@module_name2 " & _
                   "	,@module_name3 " & _
                   "	,@module_name4 " & _
                   "	,@module_name5 " & _
                   "	,@operationmodeid " & _
                   "	,@userunkid " & _
                   "	,@loginemployeeunkid " & _
                   "	,@fail_reason "

            If mstrAttachedFiles.Trim.Length > 0 Then
                StrQ &= ",'" & mstrAttachedFiles & "' "
            Else
                StrQ &= ",'' "
            End If
            StrQ &= "	,@gateway " & _
                    ", @isweb " & _
                    ") "

            Dim oCmd As New SqlCommand(StrQ, sqlCn)
            oCmd.Parameters.Clear()

            oCmd.Parameters.AddWithValue("@row_guid", Guid.NewGuid.ToString())
            oCmd.Parameters.AddWithValue("@sender_address", strSenderaddress)
            oCmd.Parameters.AddWithValue("@recipient_address", mstrToEmail)
            oCmd.Parameters.AddWithValue("@sent_data", mstrMessage)
            oCmd.Parameters.AddWithValue("@subject", mstrSubject)
            oCmd.Parameters.AddWithValue("@ip", getIP())
            oCmd.Parameters.AddWithValue("@host", getHostName())
            oCmd.Parameters.AddWithValue("@form_name", "ArutiReflex")
            oCmd.Parameters.AddWithValue("@isweb", False)
            oCmd.Parameters.AddWithValue("@module_name1", "")
            oCmd.Parameters.AddWithValue("@module_name2", "")
            oCmd.Parameters.AddWithValue("@module_name3", "")
            oCmd.Parameters.AddWithValue("@module_name4", "")
            oCmd.Parameters.AddWithValue("@module_name5", "")
            oCmd.Parameters.AddWithValue("@operationmodeid", xOperationModeId)
            oCmd.Parameters.AddWithValue("@userunkid", xUserUnkid)
            oCmd.Parameters.AddWithValue("@loginemployeeunkid", -1)
            oCmd.Parameters.AddWithValue("@fail_reason", strReason)
            oCmd.Parameters.AddWithValue("@gateway", strSenderaddress)

            oCmd.ExecuteNonQuery()

        Catch ex As Exception
            WriteLog("Insert_Email_Tran_Log : " & ex.Message)
            Insert_Email_Tran_Log(mintModuleRefId, mstrDatabaseName, strSenderaddress, mstrToEmail, mstrSubject, mstrMessage, xUserUnkid, xOperationModeId, ex.Message.ToString(), "")
        End Try
    End Sub

    Private Sub WriteLog(ByVal strMessage As String)
        Dim m_strLogFile As String = ""
        Dim m_strFileName As String = "ArutiAttendance_LOG_" & Now.ToString("yyyyMMdd")
        Dim file As System.IO.StreamWriter
        m_strLogFile = System.IO.Path.Combine(My.Application.Info.DirectoryPath, m_strFileName & ".txt")
        file = New System.IO.StreamWriter(m_strLogFile, True)
        file.BaseStream.Seek(0, SeekOrigin.End)
        file.WriteLine(strMessage)
        file.Close()
    End Sub

    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA. 'TANZANIA REVENUE AUTHORITY

    Private Function GetGroupName() As String
        Dim StrGrpName As String = ""
        Dim StrQ As String = ""
        Try
            StrQ = "SELECT groupname FROM hrmsConfiguration..cfgroup_master "

            If sqlCmd Is Nothing Then
                sqlCmd = New SqlCommand()
                sqlCmd.Connection = sqlCn
            End If

            sqlCmd.CommandText = StrQ
            StrGrpName = CStr(sqlCmd.ExecuteScalar())

        Catch ex As Exception
            WriteLog("GetGroupName:- " & ex.Message)
        End Try
        Return StrGrpName
    End Function

    Private Sub SendNotificationTRA(ByVal intCompanyId As Integer, _
                                 ByVal xStrDatabaseName As String, _
                                 ByVal intYearId As Integer, _
                                 ByVal dtCoyEmailSetup As DataTable)
        Dim StrQ As String = ""
        Dim StrSendTnAEarlyLateReportsUserIds As String = ""
        Dim StrTnAFailureNotificationUserIds As String = ""
        Dim StrDate As String = "" : Dim blnShowSig As Boolean = False
        Dim StrUserAccessModeSetting As String = ""
        Try
            StrQ = "SELECT key_value FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = " & intCompanyId.ToString() & " AND UPPER(key_name) = 'SENDTNAEARLYLATEREPORTSUSERIDS' "

            If sqlCmd Is Nothing Then
                sqlCmd = New SqlCommand()
                sqlCmd.Connection = sqlCn
            End If

            sqlCmd.CommandText = StrQ
            StrSendTnAEarlyLateReportsUserIds = CStr(sqlCmd.ExecuteScalar())
            If StrSendTnAEarlyLateReportsUserIds Is Nothing Then StrSendTnAEarlyLateReportsUserIds = ""

            'StrQ = "SELECT key_value FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = " & intCompanyId.ToString() & " AND UPPER(key_name) = 'TNAFAILURENOTIFICATIONUSERIDS' "
            'If sqlCmd Is Nothing Then
            '    sqlCmd = New SqlCommand()
            '    sqlCmd.Connection = sqlCn
            'End If

            'sqlCmd.CommandText = StrQ
            'StrTnAFailureNotificationUserIds = CStr(sqlCmd.ExecuteScalar())

            StrQ = "SELECT key_value FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = " & intCompanyId.ToString() & " AND UPPER(key_name) = 'USERACCESSMODESETTING' "

            If sqlCmd Is Nothing Then
                sqlCmd = New SqlCommand()
                sqlCmd.Connection = sqlCn
            End If

            sqlCmd.CommandText = StrQ
            StrUserAccessModeSetting = CStr(sqlCmd.ExecuteScalar())
            If StrUserAccessModeSetting Is Nothing Then StrUserAccessModeSetting = "3"

            'Pinkal (29-Mar-2023) -- Start
            'As Per TRA Requirements - TRA Wants to get report yesterday's Report.
            'StrQ = "SELECT CONVERT(NVARCHAR(8),GETDATE(),112) "
            StrQ = "SELECT CONVERT(NVARCHAR(8),GETDATE() - 1,112) "
            'Pinkal (20-Mar-2023) -- End

            If sqlCmd Is Nothing Then
                sqlCmd = New SqlCommand()
                sqlCmd.Connection = sqlCn
            End If

            sqlCmd.CommandText = StrQ
            StrDate = CStr(sqlCmd.ExecuteScalar())

            StrQ = "SELECT key_value FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = " & intCompanyId.ToString() & " AND UPPER(key_name) = 'SHOWARUTISIGNATURE' "
            If sqlCmd Is Nothing Then
                sqlCmd = New SqlCommand()
                sqlCmd.Connection = sqlCn
            End If

            sqlCmd.CommandText = StrQ
            blnShowSig = CBool(sqlCmd.ExecuteScalar())

            Dim xFile As New List(Of String)
            If StrSendTnAEarlyLateReportsUserIds.Trim.Length > 0 Then
                Dim StrUser As String = "" : Dim strEmail As String = "" : Dim objSendmail As New clsSendMail
                For Each xUserId As String In StrSendTnAEarlyLateReportsUserIds.Trim.Split(CChar(","))

                    StrQ = "SELECT CASE WHEN firstname <> '' and lastname <> '' THEN firstname+' '+lastname WHEN firstname <> '' and lastname = '' THEN firstname ELSE username END AS suser FROM hrmsConfiguration..cfuser_master WHERE userunkid = '" & xUserId & "' "
                    If sqlCmd Is Nothing Then
                        sqlCmd = New SqlCommand()
                        sqlCmd.Connection = sqlCn
                    End If

                    sqlCmd.CommandText = StrQ
                    StrUser = CStr(sqlCmd.ExecuteScalar())

                    StrQ = "SELECT email FROM hrmsConfiguration..cfuser_master WHERE userunkid = '" & xUserId & "' "
                    If sqlCmd Is Nothing Then
                        sqlCmd = New SqlCommand()
                        sqlCmd.Connection = sqlCn
                    End If

                    sqlCmd.CommandText = StrQ
                    strEmail = CStr(sqlCmd.ExecuteScalar())

                    StrQ = " SELECT " & _
                           "     hremployee_master.employeeunkid  " & _
                           "    ,ISNULL(hremployee_master.employeecode,'') AS Empcode " & _
                           "    ,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                           "    ,ISNULL(dept.name, '') AS department " & _
                           "    ,ISNULL(jb.job_name, '') AS job " & _
                           "    ,ISNULL(CONVERT(VARCHAR(5), tnashift_tran.starttime, 108),'') + ' - ' + ISNULL(CONVERT(VARCHAR(5), tnashift_tran.endtime, 108),'') As Shifthr " & _
                           "    ,CONVERT(NVARCHAR(5),loginIntime.checkintime,108) AS Late_Intime " & _
                           "    ,CONVERT(NVARCHAR(5),loginOuttime.checkouttime,108) AS Early_Goingtime " & _
                           " FROM " & xStrDatabaseName & "..tnalogin_summary " & _
                           " JOIN " & xStrDatabaseName & "..hremployee_master ON hremployee_master.employeeunkid = tnalogin_summary.employeeunkid " & _
                           " JOIN ( " & _
                            "SELECT  Trf_AS.TrfEmpId " & _
                            "FROM    ( SELECT    ETT.employeeunkid AS TrfEmpId  " & _
                                              ", ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                                              ", ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                                              ", ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                                              ", ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                                              ", ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                                              ", ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                                              ", ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                                              ", ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                                              ", ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                                              ", ISNULL(ETT.classunkid, 0) AS classunkid " & _
                                              ", CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
                                              ", ETT.employeeunkid " & _
                                              ", ROW_NUMBER() OVER ( PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC ) AS Rno " & _
                                      "FROM      " & xStrDatabaseName & "..hremployee_transfer_tran AS ETT " & _
                                      "WHERE     isvoid = 0 " & _
                                                "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & StrDate & "' " & _
                                    ") AS Trf_AS " & _
                                    "JOIN ( SELECT   ECT.employeeunkid AS CatEmpId  " & _
                                                  ", ECT.jobgroupunkid " & _
                                                  ", ECT.jobunkid " & _
                                                  ", CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
                                                  ", ECT.employeeunkid " & _
                                                  ", ROW_NUMBER() OVER ( PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC ) AS Rno " & _
                                           "FROM     " & xStrDatabaseName & "..hremployee_categorization_tran AS ECT " & _
                                           "WHERE    isvoid = 0 " & _
                                                    "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & StrDate & "' " & _
                                         ") AS RUCat ON Trf_AS.employeeunkid = RUCat.employeeunkid " & _
                            "WHERE   Trf_AS.Rno = 1 " & _
                                    "AND RUCat.Rno = 1 "
                    Dim arrID As String() = StrUserAccessModeSetting.Split(",")
                    For i = 0 To arrID.Length - 1
                        Dim iStrQ As String = "" : Dim xAccessLevel As String = ""
                        iStrQ = "SELECT " & _
                                "    STUFF((SELECT ',' + CAST(allocationunkid AS NVARCHAR(50)) " & _
                                "FROM hrmsConfiguration..cfuseraccess_privilege_tran " & _
                                "JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid " & _
                                "WHERE userunkid = " & xUserId & " AND companyunkid = " & intCompanyId & " AND yearunkid = " & intYearId & " AND referenceunkid = " & arrID(i).ToString & " " & _
                                "ORDER BY hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid FOR XML PATH('')),1,1,'') AS CSV "

                        If sqlCmd Is Nothing Then
                            sqlCmd = New SqlCommand()
                            sqlCmd.Connection = sqlCn
                        End If

                        sqlCmd.CommandText = iStrQ
                        xAccessLevel = CStr(sqlCmd.ExecuteScalar())
                        If xAccessLevel.Trim.Length <= 0 Then xAccessLevel = "-999"

                        StrQ &= " AND "

                        If i = 0 Then
                            StrQ &= " (( "
                        End If
                        If CInt(arrID(i)) = 3 Then
                            StrQ &= " Trf_AS.departmentunkid IN (" & xAccessLevel & ") "
                        ElseIf CInt(arrID(i)) = 13 Then
                            StrQ &= " Trf_AS.classgroupunkid IN (" & xAccessLevel & ") "
                        ElseIf CInt(arrID(i)) = 14 Then
                            StrQ &= " Trf_AS.classunkid IN (" & xAccessLevel & ") "
                        ElseIf CInt(arrID(i)) = 10 Then
                            StrQ &= " RUCat.jobunkid IN (" & xAccessLevel & ") "
                        ElseIf CInt(arrID(i)) = 9 Then
                            StrQ &= " RUCat.jobgroupunkid IN (" & xAccessLevel & ") "
                        ElseIf CInt(arrID(i)) = 1 Then
                            StrQ &= " Trf_AS.stationunkid IN (" & xAccessLevel & ") "
                        ElseIf CInt(arrID(i)) = 2 Then
                            StrQ &= " Trf_AS.deptgroupunkid IN (" & xAccessLevel & ") "
                        ElseIf CInt(arrID(i)) = 4 Then
                            StrQ &= " Trf_AS.sectiongroupunkid IN (" & xAccessLevel & ") "
                        ElseIf CInt(arrID(i)) = 5 Then
                            StrQ &= " Trf_AS.sectionunkid IN (" & xAccessLevel & ") "
                        ElseIf CInt(arrID(i)) = 6 Then
                            StrQ &= " Trf_AS.unitgroupunkid IN (" & xAccessLevel & ") "
                        ElseIf CInt(arrID(i)) = 7 Then
                            StrQ &= " Trf_AS.unitunkid IN (" & xAccessLevel & ") "
                        ElseIf CInt(arrID(i)) = 8 Then
                            StrQ &= " Trf_AS.teamunkid IN (" & xAccessLevel & ") "
                        End If
                    Next
                    StrQ &= " ))) AS UA ON hremployee_master.employeeunkid = UA.TrfEmpId AND hremployee_master.isapproved = 1 "

                    StrQ &= " LEFT JOIN " & _
                            "   ( " & _
                            "       SELECT " & _
                            "           jobunkid " & _
                            "           ,employeeunkid " & _
                            "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "       FROM " & xStrDatabaseName & "..hremployee_categorization_tran " & _
                            "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & StrDate & "' " & _
                            "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                            " LEFT JOIN " & xStrDatabaseName & "..hrjob_master jb ON Jobs.jobunkid = jb.jobunkid " & _
                            " LEFT JOIN " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            departmentunkid " & _
                            "           ,employeeunkid " & _
                            "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "       FROM " & xStrDatabaseName & "..hremployee_transfer_tran " & _
                            "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & StrDate & "' " & _
                            "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                            " LEFT JOIN " & xStrDatabaseName & "..hrdepartment_master dept ON Alloc.departmentunkid = dept.departmentunkid " & _
                            " JOIN " & xStrDatabaseName & "..tnashift_master ON tnashift_master.shiftunkid = tnalogin_summary.shiftunkid AND tnashift_master.isactive = 1 " & _
                            " JOIN " & xStrDatabaseName & "..tnashift_tran ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid AND tnashift_tran.dayid = datepart (dw,tnalogin_summary.login_date) - 1 " & _
                            " LEFT JOIN ( SELECT  tnalogin_tran.logindate,employeeunkid " & _
                            ", CASE WHEN MIN(tnalogin_tran.roundoff_intime) IS NULL THEN MIN(tnalogin_tran.checkintime) " & _
                            "           WHEN MIN(tnalogin_tran.checkintime) <>  MIN(tnalogin_tran.roundoff_intime) THEN  " & _
                            "     MIN(tnalogin_tran.roundoff_intime) " & _
                            "  ELSE  MIN(tnalogin_tran.roundoff_intime) END checkintime  " & _
                            " FROM " & xStrDatabaseName & "..tnalogin_Tran " & _
                            " WHERE CONVERT(VARCHAR(8),logindate,112) BETWEEN  '" & StrDate & "' AND '" & StrDate & "' AND isvoid = 0 " & _
                            " GROUP BY logindate,employeeunkid " & _
                            " ) AS loginIntime ON loginIntime.employeeunkid = tnalogin_summary.employeeunkid " & _
                            " AND CONVERT(VARCHAR(8),loginIntime.logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)  " & _
                            " AND DATEDIFF(MINUTE,CONVERT(VARCHAR(10),tnashift_tran.starttime,108),CONVERT(VARCHAR(10),loginIntime.checkintime,108)) > 0 " & _
                            " LEFT JOIN ( SELECT tnalogin_tran.logindate,employeeunkid " & _
                            "                          , CASE WHEN Max(tnalogin_tran.roundoff_outtime) IS NULL THEN MAX(tnalogin_tran.checkouttime)  " & _
                            "                                     WHEN MAX(tnalogin_tran.checkouttime) <> ISNULL(MAX(tnalogin_tran.roundoff_outtime), '') THEN  " & _
                            "                                MAX(tnalogin_tran.roundoff_outtime) " & _
                            "                            ELSE MAX(tnalogin_tran.roundoff_outtime) END checkouttime  " & _
                            "                    FROM " & xStrDatabaseName & "..tnalogin_Tran " & _
                            "                    WHERE isvoid = 0 AND CONVERT(VARCHAR(8),logindate,112) BETWEEN '" & StrDate & "' AND '" & StrDate & "' AND isvoid = 0 " & _
                            "					GROUP BY logindate,employeeunkid  " & _
                            " ) AS loginOuttime ON loginOuttime.employeeunkid = tnalogin_summary.employeeunkid  " & _
                            " AND CONVERT(VARCHAR(8),loginOuttime.logindate,112) = CONVERT(VARCHAR(8),tnalogin_summary.login_date,112)   " & _
                            " AND DATEDIFF(MINUTE,CONVERT(VARCHAR(10),tnashift_tran.endtime,108),CONVERT(VARCHAR(10),loginOuttime.checkouttime,108)) < 0 " & _
                            " LEFT JOIN " & _
                            " ( " & _
                            "    SELECT " & _
                            "         T.EmpId " & _
                            "        ,T.EOC " & _
                            "        ,T.LEAVING " & _
                            "        ,T.isexclude_payroll AS IsExPayroll " & _
                            "    FROM " & _
                            "    ( " & _
                            "        SELECT " & _
                            "             employeeunkid AS EmpId " & _
                            "            ,date1 AS EOC " & _
                            "            ,date2 AS LEAVING " & _
                            "            ,effectivedate " & _
                            "            ,isexclude_payroll " & _
                            "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                            "        FROM " & xStrDatabaseName & "..hremployee_dates_tran WHERE datetypeunkid IN(4) AND isvoid = 0 " & _
                            "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & StrDate & "' " & _
                            "    ) AS T WHERE T.xNo = 1 " & _
                            ") AS TRM ON TRM.EmpId = hremployee_master.employeeunkid " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         R.EmpId " & _
                            "        ,R.RETIRE " & _
                            "        ,R.isexclude_payroll AS IsExPayroll " & _
                            "    FROM " & _
                            "    ( " & _
                            "        SELECT " & _
                            "             employeeunkid AS EmpId " & _
                            "            ,date1 AS RETIRE " & _
                            "            ,effectivedate " & _
                            "            ,isexclude_payroll " & _
                            "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                            "        FROM " & xStrDatabaseName & "..hremployee_dates_tran WHERE datetypeunkid IN(6) AND isvoid = 0 " & _
                            "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & StrDate & "' " & _
                            "    ) AS R WHERE R.xNo = 1 " & _
                            ") AS RET ON RET.EmpId = hremployee_master.employeeunkid " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         RH.EmpId " & _
                            "        ,RH.REHIRE " & _
                            "    FROM " & _
                            "    ( " & _
                            "        SELECT " & _
                            "             employeeunkid AS EmpId " & _
                            "            ,reinstatment_date AS REHIRE " & _
                            "            ,effectivedate " & _
                            "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                            "        FROM " & xStrDatabaseName & "..hremployee_rehire_tran WHERE isvoid = 0 " & _
                            "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & StrDate & "' " & _
                            "    ) AS RH WHERE RH.xNo = 1 " & _
                            ") AS HIRE ON HIRE.EmpId = hremployee_master.employeeunkid " & _
                            " WHERE 1 = 1 AND loginintime.checkintime IS NOT NULL OR loginOuttime.checkouttime IS NOT NULL " & _
                            " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= '" & StrDate & "' " & _
                            " AND (CASE WHEN CONVERT(CHAR(8), TRM.LEAVING, 112) IS NULL THEN '" & StrDate & "' ELSE CONVERT(CHAR(8), TRM.LEAVING, 112) END) >= '" & StrDate & "' " & _
                            " AND (CASE WHEN CONVERT(CHAR(8), RET.RETIRE, 112) IS NULL THEN '" & StrDate & "' ELSE CONVERT(CHAR(8), RET.RETIRE, 112) END) >= '" & StrDate & "' " & _
                            " AND (CASE WHEN CONVERT(CHAR(8), TRM.EOC, 112) IS NULL THEN '" & StrDate & "' ELSE CONVERT(CHAR(8), TRM.EOC, 112) END) >= '" & StrDate & "' " & _
                            " AND (CASE WHEN CONVERT(CHAR(8), HIRE.REHIRE, 112) IS NULL THEN '" & StrDate & "' ELSE CONVERT(CHAR(8), HIRE.REHIRE, 112) END)  <= '" & StrDate & "' "

                    'Pinkal (29-Mar-2023) -- TRA Late coming and Early going Report Issue.[ AND DATEDIFF(MINUTE,CONVERT(VARCHAR(10),tnashift_tran.endtime,108),CONVERT(VARCHAR(10),loginOuttime.checkouttime,108)) < 0 AND DATEDIFF(MINUTE,CONVERT(VARCHAR(10),tnashift_tran.endtime,108),CONVERT(VARCHAR(10),loginOuttime.checkouttime,108)) < 0 ]
                    '" AND tnalogin_summary.ltcoming_grace > 60 AND tnalogin_summary.elrgoing_grace > 60 " & _


                    Dim sqlDataadp As New SqlDataAdapter(StrQ, sqlCn)
                    Dim dtList As New DataTable
                    sqlDataadp.Fill(dtList)

                    objSendmail._SenderAddress = dtCoyEmailSetup.Rows(0)("senderaddress").ToString()
                    objSendmail._MailserverIP = dtCoyEmailSetup.Rows(0)("mailserverip").ToString()
                    objSendmail._MailserverPort = dtCoyEmailSetup.Rows(0)("mailserverport").ToString()
                    objSendmail._UserName = dtCoyEmailSetup.Rows(0)("username").ToString()
                    objSendmail._Password = dtCoyEmailSetup.Rows(0)("password").ToString()
                    objSendmail._Isloginssl = CBool(dtCoyEmailSetup.Rows(0)("isloginssl"))

                    If dtList.Rows.Count > 0 Then
                        'Dim strBuilder As New StringBuilder
                        'strBuilder.AppendLine("<HTML>")
                        'strBuilder.AppendLine("<HEAD></HEAD>")
                        'strBuilder.AppendLine("<BODY>")

                        'strBuilder.AppendLine("<TABLE BORDER = 0 WIDTH=100% CELLSPACING =0 CELLPADDING =3 style='font-family:Verdana;'>")
                        'strBuilder.AppendLine("<TR>")
                        'strBuilder.AppendLine("<TD COLSPAN='7' style='font-size:20px; color:steelblue'><center><b>" & dtCoyEmailSetup.Rows(0)("name").ToString & "</center></b></TD>")
                        'strBuilder.AppendLine("</TR>")
                        'strBuilder.AppendLine("<TR>")
                        'strBuilder.AppendLine("<TD COLSPAN='7' style='font-size:15px; color:purple'><center><b>Late Arrival Early Departure Report</center></b></TD>")
                        'strBuilder.AppendLine("</TR>")
                        'strBuilder.AppendLine("</TABLE>")
                        'strBuilder.AppendLine("<BR>")
                        'strBuilder.AppendLine("<TABLE BORDER = 1 WIDTH=100% CELLSPACING =0 CELLPADDING =3 style='font-family:Verdana;'>")
                        'strBuilder.AppendLine("<TR style='font-size:12px; color:white;background-color:steelblue'>")
                        'strBuilder.AppendLine("<TD><B>Employee Code</B></TD>")
                        'strBuilder.AppendLine("<TD><B>Employee</B></TD>")
                        'strBuilder.AppendLine("<TD><B>Department</B></TD>")
                        'strBuilder.AppendLine("<TD><B>Job</B></TD>")
                        'strBuilder.AppendLine("<TD><B>Shift Hrs</B></TD>")
                        'strBuilder.AppendLine("<TD><B>Late In Time</B></TD>")
                        'strBuilder.AppendLine("<TD><B>Early Going Time</B></TD>")
                        'strBuilder.AppendLine("</TR>")
                        'For Each dtRow As DataRow In dtList.Rows
                        '    strBuilder.AppendLine("<TR style='font-size:13px'>")
                        '    strBuilder.AppendLine("<TD>" & dtRow.Item("Empcode").ToString().Trim() & "</TD>")
                        '    strBuilder.AppendLine("<TD>" & dtRow.Item("Employee").ToString().Trim() & "</TD>")
                        '    strBuilder.AppendLine("<TD>" & dtRow.Item("department").ToString().Trim() & "</TD>")
                        '    strBuilder.AppendLine("<TD>" & dtRow.Item("job").ToString().Trim() & "</TD>")
                        '    strBuilder.AppendLine("<TD>" & dtRow.Item("Shifthr").ToString().Trim() & "</TD>")
                        '    If Not IsDBNull(dtRow.Item("Late_Intime")) Then
                        '        strBuilder.AppendLine("<TD>" & dtRow.Item("Late_Intime").ToString() & "</TD>")
                        '    Else
                        '        strBuilder.AppendLine("<TD></TD>")
                        '    End If
                        '    If Not IsDBNull(dtRow.Item("Early_Goingtime")) Then
                        '        strBuilder.AppendLine("<TD>" & dtRow.Item("Early_Goingtime").ToString() & "</TD>")
                        '    Else
                        '        strBuilder.AppendLine("<TD></TD>")
                        '    End If
                        '    strBuilder.AppendLine("</TR>")
                        'Next
                        'strBuilder.AppendLine("</TABLE>")
                        'strBuilder.AppendLine("<BR>")
                        'strBuilder.AppendLine("</BODY>")
                        'strBuilder.AppendLine("</HTML>")
                        Dim tpath As String = My.Computer.FileSystem.SpecialDirectories.Temp
                        Dim flName As String = xUserId.ToString() & "_" & Format(Now, "hhmmssyyyydd").ToString() & ".pdf"
                        Dim xpath = System.IO.Path.Combine(tpath, flName)
                        If System.IO.File.Exists(xpath) Then System.IO.File.Delete(xpath)
                        'System.IO.File.WriteAllText(xpath, strBuilder.ToString())


                        'strBuilder = New StringBuilder()

                        'Generating PDF -- START
                        Dim doc As Document = New Document()
                        Dim tableLayout As PdfPTable = New PdfPTable(7)
                        PdfWriter.GetInstance(doc, New FileStream(xpath, FileMode.Create))
                        doc.Open()

                        'Pinkal (01-Mar-2023) -- Start
                        '(A1X-677) TRA - Late Arrival Early Departure Report enhancement to include Date info.
                        'Dim headers As Integer() = {10, 20, 20, 20, 10, 10, 10}
                        Dim headers As Integer() = {10, 20, 20, 20, 13, 10, 10}
                        'Pinkal (01-Mar-2023) -- End
                        tableLayout.SetWidths(headers)
                        tableLayout.WidthPercentage = 95
                        tableLayout.AddCell(New PdfPCell(New Phrase(dtCoyEmailSetup.Rows(0)("name").ToString, New Font(Font.HELVETICA, 13, 1, New iTextSharp.text.Color(0, 0, 128)))) With {.Colspan = 7, .Border = 0, .PaddingBottom = 20, .HorizontalAlignment = Element.ALIGN_CENTER})
                        tableLayout.AddCell(New PdfPCell(New Phrase("Late Arrival Early Departure Report", New Font(Font.HELVETICA, 11, 1, New iTextSharp.text.Color(128, 0, 128)))) With {.Colspan = 7, .Border = 0, .PaddingBottom = 20, .HorizontalAlignment = Element.ALIGN_CENTER})
                        'Pinkal (01-Mar-2023) -- Start
                        '(A1X-677) TRA - Late Arrival Early Departure Report enhancement to include Date info.
                        tableLayout.AddCell(New PdfPCell(New Phrase("Date : " & Format(convertDate(StrDate.ToString()), "dd-MMM-yyyy"), New Font(Font.HELVETICA, 11, 1, New iTextSharp.text.Color(System.Drawing.Color.Black)))) With {.Colspan = 7, .Border = 0, .PaddingBottom = 20, .HorizontalAlignment = Element.ALIGN_CENTER})
                        'Pinkal (01-Mar-2023) -- End

                        AddCellToHeader(tableLayout, "Employee Code")
                        AddCellToHeader(tableLayout, "Employee")
                        AddCellToHeader(tableLayout, "Department")
                        AddCellToHeader(tableLayout, "Job")
                        'Pinkal (01-Mar-2023) -- Start
                        '(A1X-677) TRA - Late Arrival Early Departure Report enhancement to include Date info.
                        'AddCellToHeader(tableLayout, "Shift Hrs")
                        AddCellToHeader(tableLayout, "Working Hrs")
                        'Pinkal (01-Mar-2023) -- End
                        AddCellToHeader(tableLayout, "Late In Time")
                        AddCellToHeader(tableLayout, "Early Going Time")

                        For Each dtRow As DataRow In dtList.Rows
                            AddCellToBody(tableLayout, dtRow.Item("Empcode").ToString().Trim())
                            AddCellToBody(tableLayout, dtRow.Item("Employee").ToString().Trim())
                            AddCellToBody(tableLayout, dtRow.Item("department").ToString().Trim())
                            AddCellToBody(tableLayout, dtRow.Item("job").ToString().Trim())
                            AddCellToBody(tableLayout, dtRow.Item("Shifthr").ToString().Trim())
                            If Not IsDBNull(dtRow.Item("Late_Intime")) Then
                                AddCellToBody(tableLayout, dtRow.Item("Late_Intime").ToString())
                            Else
                                AddCellToBody(tableLayout, "")
                            End If
                            If Not IsDBNull(dtRow.Item("Early_Goingtime")) Then
                                AddCellToBody(tableLayout, dtRow.Item("Early_Goingtime").ToString())
                            Else
                                AddCellToBody(tableLayout, "")
                            End If
                        Next
                        doc.Add(tableLayout)
                        doc.Close()
                        'Generating PDF -- END


                        'Dim styles As StyleSheet = New StyleSheet()
                        'Dim element = HTMLWorker.ParseToList(New StringReader(strBuilder.ToString()), styles)

                        'Dim document As Document = New Document()
                        'PdfWriter.GetInstance(document, New FileStream(xpath, FileMode.Create))
                        'document.Open()

                        'Dim hw As iTextSharp.text.html.simpleparser.HTMLWorker = New iTextSharp.text.html.simpleparser.HTMLWorker(document)
                        'hw.Parse(New StringReader(strBuilder.ToString()))
                        'document.Close()


                        xFile.Add(xpath)

                        Dim strMessage As String = ""
                        strMessage = "<HTML> <BODY>"
                        Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                        strMessage &= "Dear" & " " & info1.ToTitleCase(StrUser.ToLower()) & ", <BR></BR><BR></BR>"
                        strMessage &= "Please find the attached attendance report for your action. For more details, you may contact your system administrator.<BR></BR><BR></BR>"
                        strMessage &= "Regards."
                        If blnShowSig Then
                            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        End If
                        strMessage &= "</BODY></HTML>"
                        Dim mstrEmailError As String = ""
                        objSendmail._Message = strMessage.ToString()
                        objSendmail._Form_Name = "ArutiAttendance"
                        objSendmail._LogEmployeeUnkid = -1
                        objSendmail._OperationModeId = enLogin_Mode.DESKTOP
                        objSendmail._UserUnkid = xUserId
                        objSendmail._ModuleRefId = enAT_VIEW_TYPE.TNA_MGT
                        objSendmail._AttachedFiles = xpath
                        objSendmail._Subject = "Notification for Late Arrival/Early Departure"
                        objSendmail._ToEmail = strEmail.Trim
                        mstrEmailError = objSendmail.SendMail()
                        Insert_Email_Tran_Log(enAT_VIEW_TYPE.TNA_MGT, xStrDatabaseName, objSendmail._SenderAddress, objSendmail._ToEmail, objSendmail._Subject, objSendmail._Message, 1, enLogin_Mode.DESKTOP, mstrEmailError, "")
                    Else
                        Dim strMessage As String = ""
                        strMessage = "<HTML> <BODY>"
                        Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                        strMessage &= "Dear" & " " & info1.ToTitleCase(StrUser.ToLower()) & ", <BR></BR><BR></BR>"
                        strMessage &= "Please note that no employee was registered as a late comer or early goer on " & convertDate(StrDate.ToString()) & " .<BR></BR><BR></BR>"
                        strMessage &= "Regards."
                        If blnShowSig Then
                            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        End If
                        strMessage &= "</BODY></HTML>"
                        Dim mstrEmailError As String = ""
                        objSendmail._Message = strMessage.ToString()
                        objSendmail._Form_Name = "ArutiAttendance"
                        objSendmail._LogEmployeeUnkid = -1
                        objSendmail._OperationModeId = enLogin_Mode.DESKTOP
                        objSendmail._UserUnkid = xUserId
                        objSendmail._ModuleRefId = enAT_VIEW_TYPE.TNA_MGT
                        objSendmail._AttachedFiles = ""
                        objSendmail._Subject = "Notification for Late Arrival/Early Departure"
                        objSendmail._ToEmail = strEmail.Trim
                        mstrEmailError = objSendmail.SendMail()
                        Insert_Email_Tran_Log(enAT_VIEW_TYPE.TNA_MGT, xStrDatabaseName, objSendmail._SenderAddress, objSendmail._ToEmail, objSendmail._Subject, objSendmail._Message, 1, enLogin_Mode.DESKTOP, mstrEmailError, "")
                    End If
                Next

                For Each f As String In xFile
                    If System.IO.File.Exists(f) Then System.IO.File.Delete(f)
                Next

            End If

        Catch ex As Exception
            WriteLog("SendNotificationTRA:- " & ex.Message)
        End Try
    End Sub

    Public Sub SendAbsentProcessFailedNotificationToUser(ByVal xDatabaseName As String, ByVal xCompanyunkid As Integer, ByVal objSendmail As clsSendMail)
        Try
            Dim mstrModuleName As String = "clsEmployee_Master"
            Dim xUserLanguageId As Integer = -1
            Dim strMessage As String = ""

            If mstrTnAFailureNotificationUserIds.Trim.Length > 0 Then

                Dim NewCulture As CultureInfo = CType(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
                NewCulture.DateTimeFormat.ShortDatePattern = "dd-MMM-yyyy"
                NewCulture.DateTimeFormat.DateSeparator = "-"
                System.Threading.Thread.CurrentThread.CurrentCulture = NewCulture


                Dim mstrQuery As String = " SELECT " & _
                                                        " cfuser_master.userunkid " & _
                                                        ",ISNULL(cfuser_master.username,'') AS username " & _
                                                        ",ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') AS UName " & _
                                                        ",ISNULL(cfuser_master.languageunkid,1) AS languageunkid " & _
                                                        ",ISNULL(cfuser_master.email,'') AS email " & _
                                                        " FROM hrmsConfiguration..cfuser_master " & _
                                                        " WHERE cfuser_master.isactive = 1 AND cfuser_master.userunkid = @userunkid "


                For Each sId As String In mstrTnAFailureNotificationUserIds.Trim.Split(CChar(","))
                    strMessage = ""
                    Dim mstrUserName As String = ""
                    Dim mstrUserEmail As String = ""
                    Dim dtUser As New DataTable

                    Dim oCmd As New SqlCommand(mstrQuery, sqlCn)
                    oCmd.Parameters.Clear()
                    oCmd.Parameters.Add("@userunkid", SqlDbType.Int).Value = CInt(sId)
                    Dim adp As New SqlDataAdapter(oCmd)
                    adp.Fill(dtUser)


                    If dtUser IsNot Nothing AndAlso dtUser.Rows.Count > 0 Then

                        If dtUser.Rows(0)("UName").ToString().Trim.Length > 0 Then
                            mstrUserName = dtUser.Rows(0)("UName").ToString().Trim.ToLower()
                        Else
                            mstrUserName = dtUser.Rows(0)("username").ToString().Trim.ToLower()
                        End If

                        xUserLanguageId = CInt(dtUser.Rows(0)("languageunkid"))
                        mstrUserEmail = dtUser.Rows(0)("email").ToString()

                        strMessage = "<HTML> <BODY>"
                        strMessage &= "Dear " & CultureInfo.CurrentCulture.TextInfo.ToTitleCase(mstrUserName) & ", <BR></BR>"
                        strMessage &= "This is notify you that the automatic absent process failed to run today,<B> (" & Now.Date & ")</B>. Please run this process manually from Aruti." & "<BR></BR>"
                        strMessage &= "Regards."

                        Dim blnValue As Boolean = True
                        Dim mstrArutisignature As String = ""
                        mstrArutisignature = GetConfigKeyValue(xCompanyunkid, "ShowArutisignature")
                        blnValue = CBool(IIf(mstrArutisignature.ToString().Length > 0, mstrArutisignature.ToString(), True))
                        If blnValue Then
                            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        End If
                        strMessage &= "</BODY></HTML>"


                        objSendmail._Message = strMessage.ToString()
                        objSendmail._Form_Name = "ArutiAttendance"
                        objSendmail._LogEmployeeUnkid = -1
                        objSendmail._OperationModeId = enLogin_Mode.DESKTOP
                        objSendmail._UserUnkid = 1
                        objSendmail._ModuleRefId = enAT_VIEW_TYPE.TNA_MGT

                        Dim mstrEmailError As String = ""
                        If mstrUserEmail.Trim.Length > 0 Then
                            objSendmail._Subject = "Notification for Failure of Absent Process"
                            objSendmail._ToEmail = mstrUserEmail.Trim
                            mstrEmailError = objSendmail.SendMail()

                            Insert_Email_Tran_Log(enAT_VIEW_TYPE.TNA_MGT, xDatabaseName, objSendmail._SenderAddress, objSendmail._ToEmail, objSendmail._Subject, objSendmail._Message, 1, enLogin_Mode.DESKTOP, mstrEmailError, "")

                        End If

                    End If

                Next
            End If
        Catch ex As Exception
            WriteLog("SendAbsentProcessFailedNotificationToUser : " & ex.Message)
        End Try
    End Sub

    Private Shared Sub AddCellToHeader(ByVal tableLayout As PdfPTable, ByVal cellText As String)
        tableLayout.AddCell(New PdfPCell(New Phrase(cellText, New Font(Font.HELVETICA, 8, 1, iTextSharp.text.Color.WHITE))) With {.HorizontalAlignment = Element.ALIGN_LEFT, .Padding = 5, .BackgroundColor = New iTextSharp.text.Color(70, 130, 180)})
    End Sub

    Private Shared Sub AddCellToBody(ByVal tableLayout As PdfPTable, ByVal cellText As String)
        tableLayout.AddCell(New PdfPCell(New Phrase(cellText, New Font(Font.HELVETICA, 8, 1, iTextSharp.text.Color.BLACK))) With {.HorizontalAlignment = Element.ALIGN_CENTER, .Padding = 5, .BackgroundColor = iTextSharp.text.Color.WHITE})
    End Sub

    Private Function GetActiveEmployeeList(ByVal xDatabaseName As String, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date) As DataTable
        Dim dtEmployee As DataTable = Nothing
        Dim StrQ As String = ""
        Try

            StrQ = " SELECT " & _
                       "     hremployee_master.employeeunkid  " & _
                       "    ,ISNULL(hremployee_master.employeecode,'') AS Empcode " & _
                       "    ,ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                       "    ,hremployee_master.appointeddate " & _
                       "    ,ECNF.confirmation_date " & _
                       "    ,TRM.EOC " & _
                       "    ,TRM.LEAVING " & _
                       "    ,RET.RETIRE " & _
                       "    ,HIRE.REHIRE " & _
                       " FROM " & xDatabaseName & "..hremployee_master " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "    SELECT " & _
                       "         T.EmpId " & _
                       "        ,T.EOC " & _
                       "        ,T.LEAVING " & _
                       "        ,T.isexclude_payroll AS IsExPayroll " & _
                       "    FROM " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             employeeunkid AS EmpId " & _
                       "            ,date1 AS EOC " & _
                       "            ,date2 AS LEAVING " & _
                       "            ,effectivedate " & _
                       "            ,isexclude_payroll " & _
                       "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                       "        FROM " & xDatabaseName & "..hremployee_dates_tran WHERE datetypeunkid IN(4) AND isvoid = 0 " & _
                       "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & convertDate(mdtEndDate) & "' " & _
                       "    ) AS T WHERE T.xNo = 1 " & _
                       " ) AS TRM ON TRM.EmpId = hremployee_master.employeeunkid " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "    SELECT " & _
                       "         R.EmpId " & _
                       "        ,R.RETIRE " & _
                       "        ,R.isexclude_payroll AS IsExPayroll " & _
                       "    FROM " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             employeeunkid AS EmpId " & _
                       "            ,date1 AS RETIRE " & _
                       "            ,effectivedate " & _
                       "            ,isexclude_payroll " & _
                       "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                       "        FROM " & xDatabaseName & "..hremployee_dates_tran WHERE datetypeunkid IN(6) AND isvoid = 0 " & _
                       "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & convertDate(mdtEndDate) & "' " & _
                       "    ) AS R WHERE R.xNo = 1 " & _
                       " ) AS RET ON RET.EmpId = hremployee_master.employeeunkid " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "    SELECT " & _
                       "         RH.EmpId " & _
                       "        ,RH.REHIRE " & _
                       "    FROM " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             employeeunkid AS EmpId " & _
                       "            ,reinstatment_date AS REHIRE " & _
                       "            ,effectivedate " & _
                       "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                       "        FROM " & xDatabaseName & "..hremployee_rehire_tran WHERE isvoid = 0 " & _
                       "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & convertDate(mdtEndDate) & "' " & _
                       "    ) AS RH WHERE RH.xNo = 1 " & _
                       " ) AS HIRE ON HIRE.EmpId = hremployee_master.employeeunkid " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "    SELECT " & _
                       "    	 CNF.CEmpId " & _
                       "    	,CNF.confirmation_date " & _
                       "    FROM " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             CNF.employeeunkid AS CEmpId " & _
                       "            ,CNF.date1 AS confirmation_date " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY CNF.employeeunkid ORDER BY CNF.effectivedate DESC) AS Rno " & _
                       "        FROM " & xDatabaseName & "..hremployee_dates_tran AS CNF " & _
                       "        WHERE isvoid = 0 AND CNF.datetypeunkid IN (2) AND CONVERT(CHAR(8),CNF.effectivedate,112) <= '" & convertDate(mdtEndDate) & "' " & _
                       "    ) AS CNF WHERE CNF.Rno = 1 " & _
                       " ) AS ECNF ON ECNF.CEmpId = hremployee_master.employeeunkid " & _
                       " WHERE CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= '" & convertDate(mdtEndDate) & "' " & _
                       " AND (CASE WHEN CONVERT(CHAR(8), TRM.LEAVING, 112) IS NULL THEN '" & convertDate(mdtEndDate) & "' ELSE CONVERT(CHAR(8), TRM.LEAVING, 112) END) >= '" & convertDate(mdtEndDate) & "' " & _
                       " AND (CASE WHEN CONVERT(CHAR(8), RET.RETIRE, 112) IS NULL THEN '" & convertDate(mdtEndDate) & "' ELSE CONVERT(CHAR(8), RET.RETIRE, 112) END) >= '" & convertDate(mdtEndDate) & "' " & _
                       " AND (CASE WHEN CONVERT(CHAR(8), TRM.EOC, 112) IS NULL THEN '" & convertDate(mdtEndDate) & "' ELSE CONVERT(CHAR(8), TRM.EOC, 112) END) >= '" & convertDate(mdtEndDate) & "' " & _
                       " AND (CASE WHEN CONVERT(CHAR(8), HIRE.REHIRE, 112) IS NULL THEN '" & convertDate(mdtEndDate) & "' ELSE CONVERT(CHAR(8), HIRE.REHIRE, 112) END)  <= '" & convertDate(mdtEndDate) & "' "

            Dim sqlDataadp As New SqlDataAdapter(StrQ, sqlCn)
            dtEmployee = New DataTable
            sqlDataadp.Fill(dtEmployee)

        Catch ex As Exception
            WriteLog("GetActiveEmployeeList " & Date.Now & " : " & ex.Message)
        End Try
        Return dtEmployee
    End Function

    Private Sub StartAbsentProcess(ByVal xDatabaseName As String, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date, ByVal xLeaveBalanceSetting As Integer, ByVal mblnIsHolidayConsiderOnWeekend As Boolean _
                                                , ByVal mblnIsDayOffConsiderOnWeekend As Boolean, ByVal mblnIsHolidayConsiderOnDayoff As Boolean, ByVal mblnPolicyManagementTNA As Boolean _
                                                , ByVal mblnDonotAttendanceinSeconds As Boolean, ByVal mblnFirstCheckInLastCheckOut As Boolean, ByVal mblnCountAttendanceNotLVIfLVapplied As Boolean _
                                                , ByVal objSendmail As clsSendMail)
        Dim objLogin As New clslogin_Tran
        Dim xUserId As Integer = 1
        Dim SqlTran As SqlTransaction = Nothing
        Try


            Dim dtEmployee As DataTable = GetActiveEmployeeList(xDatabaseName, mdtStartDate, mdtEndDate)

            If dtEmployee IsNot Nothing AndAlso dtEmployee.Rows.Count > 0 Then

                For i As Integer = 0 To dtEmployee.Rows.Count - 1    '------------------------------------------'START FOR EMPLOYEE LOOP

                    DoAbsentProcess(dtEmployee.Rows(i), xDatabaseName, mdtStartDate, mdtEndDate, xLeaveBalanceSetting, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                            , mblnIsHolidayConsiderOnDayoff, mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut, mblnCountAttendanceNotLVIfLVapplied)
                Next

            End If

        Catch ex As Exception
            WriteLog("StartAbsentProcess:- " & ex.Message)
            SendAbsentProcessFailedNotificationToUser(xDatabaseName, xCompanyId, objSendmail)
        Finally
            objLogin = Nothing
        End Try
    End Sub

    Private Function DoAbsentProcess(ByVal drRow As DataRow, ByVal xDatabaseName As String, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date, ByVal xLeaveBalanceSetting As Integer, ByVal mblnIsHolidayConsiderOnWeekend As Boolean _
                                                   , ByVal mblnIsDayOffConsiderOnWeekend As Boolean, ByVal mblnIsHolidayConsiderOnDayoff As Boolean, ByVal mblnPolicyManagementTNA As Boolean _
                                                    , ByVal mblnDonotAttendanceinSeconds As Boolean, ByVal mblnFirstCheckInLastCheckOut As Boolean, ByVal mblnCountAttendanceNotLVIfLVapplied As Boolean) As Boolean

        Dim mblnFlag As Boolean = True
        Dim xUserId As Integer = 1
        Dim objLogin As New clslogin_Tran
        Dim SqlTran As SqlTransaction = Nothing
        Try

            GC.Collect()

            Dim mdtCurrentDate As Date = mdtStartDate.Date

            Dim xCount As Integer = DateDiff(DateInterval.Day, mdtStartDate.Date, mdtEndDate.Date) + 1

            Dim dtEmpShift As DataTable = GetEmpShiftHolidayWeekendDayOffList(xDatabaseName, CInt(drRow("employeeunkid")), mdtStartDate, mdtEndDate, mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend, mblnIsHolidayConsiderOnDayoff)

            For j As Integer = 0 To xCount - 1

                Dim mintLoginId As Integer = 0
                Dim mintLoginSummaryId As Integer = 0

                objLogin._Loginunkid(sqlCn, Nothing, xDatabaseName) = mintLoginId
                objLogin._LoginSummaryunkid = mintLoginSummaryId

                'START FOR CHECK EMPLOYEE APPOINTDATE 
                If IsDBNull(drRow("appointeddate")) = False AndAlso drRow("appointeddate") <> Nothing AndAlso CDate(drRow("appointeddate")).Date > mdtCurrentDate.AddDays(j).Date Then
                    Continue For
                End If
                'END FOR CHECK EMPLOYEE APPOINTDATE 

                'START FOR CHECK EMPLOYEE EOC DATE 
                If IsDBNull(drRow("EOC")) = False AndAlso drRow("EOC") <> Nothing AndAlso CDate(drRow("EOC")).Date < mdtCurrentDate.AddDays(j).Date Then
                    Continue For
                End If
                'END FOR CHECK EMPLOYEE EOC DATE 

                'START FOR CHECK EMPLOYEE LEAVING DATE 
                If IsDBNull(drRow("LEAVING")) = False AndAlso drRow("LEAVING") <> Nothing AndAlso CDate(drRow("LEAVING")).Date < mdtCurrentDate.AddDays(j).Date Then
                    Continue For
                End If
                'END FOR CHECK EMPLOYEE LEAVING DATE 

                'START FOR CHECK EMPLOYEE RETIREMENT DATE 
                If IsDBNull(drRow("RETIRE")) = False AndAlso drRow("RETIRE") <> Nothing AndAlso CDate(drRow("RETIRE")).Date < mdtCurrentDate.AddDays(j).Date Then
                    Continue For
                End If
                'END FOR CHECK EMPLOYEE RETIREMENT DATE 

                Dim mintshiftId As Integer = 0
                Dim mblnIsWeekend As Boolean = False
                Dim mblnDayOff As Boolean = False
                Dim mblnHoliday As Boolean = False

                If dtEmpShift IsNot Nothing AndAlso dtEmpShift.Rows.Count > 0 Then
                    Dim drShift As DataRow() = dtEmpShift.Select("ddate = '" & convertDate(mdtCurrentDate.AddDays(j).Date) & "'")
                    If drShift IsNot Nothing AndAlso drShift.Count > 0 Then
                        mintshiftId = CInt(drShift(0)("shiftunkid"))
                        mblnIsWeekend = CBool(drShift(0)("isweekend"))
                        mblnHoliday = CBool(drShift(0)("isholiday"))
                        mblnDayOff = CBool(drShift(0)("isdayoff"))
                    End If
                End If

                Dim sqlcmd As New SqlCommand
                sqlcmd.Connection = sqlCn
                sqlcmd.CommandText = "SET ARITHABORT ON"
                sqlcmd.ExecuteNonQuery()

                SqlTran = sqlCn.BeginTransaction()

                objLogin._Employeeunkid = CInt(drRow("employeeunkid"))
                objLogin._Logindate = mdtCurrentDate.AddDays(j).Date
                objLogin._Shiftunkid = mintshiftId
                objLogin._IsUnPaidLeave = False
                objLogin._IsPaidLeave = False
                objLogin._IsWeekend = False
                objLogin._IsHoliday = False
                objLogin._IsDayOffShift = False
                objLogin._LeaveTypeId = 0
                objLogin._LeaveDayFraction = objLogin.GetLeaveDayFraction(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, objLogin._LeaveTypeId)

                Dim LoginStatus As Integer = objLogin.GetEmployeeLeaveData(sqlCn, SqlTran, xDatabaseName, CInt(drRow("employeeunkid")), mdtCurrentDate.AddDays(j).Date, _
                                                                mintshiftId, xLeaveBalanceSetting, mblnIsWeekend)

                If LoginStatus = 7 Then  'Weekend 

                    If objLogin.IsEmployeeLoginExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, "", objLogin._Employeeunkid, mintLoginId) Then
                        Dim dtLoginSummary As DataTable = objLogin.IsEmployeeLoginSummaryExist(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, mintLoginSummaryId)
                        If dtLoginSummary IsNot Nothing AndAlso dtLoginSummary.Rows.Count <= 0 Then
                            objLogin._DayType = 0
                            objLogin._InOutType = 1
                            objLogin._TotalOvertimehr = 0
                            objLogin._Shorthr = 0
                            objLogin._Totalhr = 0
                            objLogin._TotalNighthr = 0
                            objLogin._IsUnPaidLeave = False
                            objLogin._IsPaidLeave = False
                            objLogin._IsWeekend = True
                            objLogin._IsAbsentProcess = True
                            objLogin._SourceType = -1 'FOR SYSTEM GENERATED ENTRY
                            objLogin._Ot2 = 0
                            objLogin._Ot3 = 0
                            objLogin._Ot4 = 0
                            objLogin._Early_Coming = 0
                            objLogin._Late_Coming = 0
                            objLogin._Early_Going = 0
                            objLogin._Late_Going = 0

                            'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                            '    objLogin._IsDayOffShift = True
                            '    If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                            '        objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                            '        objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                            '    End If
                            'End If
                            If mblnDayOff Then
                                objLogin._IsDayOffShift = True
                                If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                                    objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                                    objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                                End If
                            End If


                            Dim mintStatusId As Integer = objLogin.GetEmployeeLeaveData(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate.Date, objLogin._Shiftunkid, xLeaveBalanceSetting, mblnIsWeekend)

                            If mintStatusId = 1 Then
                                objLogin._IsUnPaidLeave = False
                                objLogin._IsPaidLeave = True
                                objLogin._DayType = 1
                            ElseIf mintStatusId = 2 Then
                                objLogin._IsUnPaidLeave = True
                                objLogin._IsPaidLeave = False
                            ElseIf mintStatusId = 3 OrElse mintStatusId = 4 Then
                                objLogin._IsUnPaidLeave = True
                                objLogin._IsPaidLeave = False
                            ElseIf mintStatusId = 5 Then
                                objLogin._IsUnPaidLeave = False
                                objLogin._IsPaidLeave = False
                                objLogin._IsHoliday = True
                                If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                    objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                    objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                                End If
                            End If

                            If objLogin._IsPaidLeave OrElse objLogin._IsUnPaidLeave Then
                                objLogin._IsHoliday = False
                                objLogin._IsDayOffShift = False
                                objLogin._IsWeekend = False
                            End If

                            objLogin.InsertLoginSummary(sqlCn, SqlTran, xDatabaseName, xUserId _
                                                                    , xYearId, xCompanyId, mdtCurrentDate.AddDays(j).Date _
                                                                    , mdtCurrentDate.AddDays(j).Date, True _
                                                                    , mblnPolicyManagementTNA _
                                                                    , mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                                                                    , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                    , mblnIsHolidayConsiderOnDayoff, 0, True)

                        Else
                            objLogin._IsDayOffShift = False
                            objLogin._IsUnPaidLeave = False
                            objLogin._IsPaidLeave = False
                            objLogin._IsWeekend = True

                            'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                            '    objLogin._IsDayOffShift = True
                            'End If

                            'Dim dtHoliday As DataTable = objLogin.GetEmployeeHolidayList(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, mdtCurrentDate.AddDays(j).Date)
                            'If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                            '    objLogin._IsHoliday = True
                            'End If
                            'dtHoliday.Clear()
                            'dtHoliday.Dispose()
                            'dtHoliday = Nothing


                            If mblnDayOff Then objLogin._IsDayOffShift = True
                            If mblnHoliday Then objLogin._IsHoliday = True

                            Dim mintStatusId As Integer = objLogin.GetEmployeeLeaveData(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate.Date, objLogin._Shiftunkid, xLeaveBalanceSetting, mblnIsWeekend)

                            If mintStatusId = 1 Then
                                objLogin._IsUnPaidLeave = False
                                objLogin._IsPaidLeave = True
                                objLogin._DayType = 1
                            ElseIf mintStatusId = 2 Then
                                objLogin._IsUnPaidLeave = True
                                objLogin._IsPaidLeave = False
                            ElseIf mintStatusId = 3 OrElse mintStatusId = 4 Then
                                objLogin._IsUnPaidLeave = True
                                objLogin._IsPaidLeave = False
                            ElseIf mintStatusId = 5 Then
                                objLogin._IsUnPaidLeave = False
                                objLogin._IsPaidLeave = False
                                objLogin._IsHoliday = True
                            End If

                            If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                            End If

                            If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                                objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                                objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                            End If

                            If objLogin._IsPaidLeave OrElse objLogin._IsUnPaidLeave Then
                                objLogin._IsHoliday = False
                                objLogin._IsDayOffShift = False
                                objLogin._IsWeekend = False
                            End If

                            objLogin._IsAbsentProcess = True

                            objLogin.UpdateLoginSummaryForAbsent(sqlCn, SqlTran, xDatabaseName)

                        End If     'If dtLoginSummary IsNot Nothing AndAlso dtLoginSummary.Rows.Count <= 0 Then

                        dtLoginSummary.Clear()
                        dtLoginSummary.Dispose()
                        dtLoginSummary = Nothing

                    Else
                        objLogin._checkintime = Nothing
                        objLogin._Holdunkid = -1
                        objLogin._Checkouttime = Nothing
                        objLogin._Original_InTime = Nothing
                        objLogin._Original_OutTime = Nothing
                        objLogin._Workhour = 0
                        objLogin._DayType = 0
                        objLogin._InOutType = 1
                        objLogin._TotalOvertimehr = 0
                        objLogin._Shorthr = 0
                        objLogin._Totalhr = 0
                        objLogin._TotalNighthr = 0
                        objLogin._IsUnPaidLeave = False
                        objLogin._IsPaidLeave = False
                        objLogin._IsWeekend = True
                        objLogin._IsAbsentProcess = True
                        objLogin._SourceType = -1 'FOR SYSTEM GENERATED ENTRY
                        objLogin._Ot2 = 0
                        objLogin._Ot3 = 0
                        objLogin._Ot4 = 0
                        objLogin._Early_Coming = 0
                        objLogin._Late_Coming = 0
                        objLogin._Early_Going = 0
                        objLogin._Late_Going = 0

                        'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                        '    objLogin._IsDayOffShift = True
                        '    If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                        '        objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                        '        objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                        '    End If
                        'End If

                        'Dim dtHoliday As DataTable = objLogin.GetEmployeeHolidayList(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, mdtCurrentDate.AddDays(j).Date)
                        'If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                        '    objLogin._IsHoliday = True
                        '    If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                        '        objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                        '        objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                        '    End If
                        'End If
                        'dtHoliday.Clear()
                        'dtHoliday.Dispose()
                        'dtHoliday = Nothing


                        If mblnDayOff Then
                            objLogin._IsDayOffShift = True
                            If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                                objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                                objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                            End If
                        End If

                        If mblnHoliday Then
                            objLogin._IsHoliday = True
                            If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                            End If
                        End If


                        Dim mintStatusId As Integer = objLogin.GetEmployeeLeaveData(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate.Date, objLogin._Shiftunkid, xLeaveBalanceSetting, mblnIsWeekend)

                        If mintStatusId = 1 Then
                            objLogin._IsUnPaidLeave = False
                            objLogin._IsPaidLeave = True
                            objLogin._IsWeekend = False
                            objLogin._DayType = 1
                        ElseIf mintStatusId = 2 Then
                            objLogin._IsUnPaidLeave = True
                            objLogin._IsPaidLeave = False
                            objLogin._IsWeekend = False
                        ElseIf mintStatusId = 3 OrElse mintStatusId = 4 Then
                            objLogin._IsUnPaidLeave = True
                            objLogin._IsPaidLeave = False
                            objLogin._IsWeekend = False
                        ElseIf mintStatusId = 5 Then
                            objLogin._IsUnPaidLeave = False
                            objLogin._IsPaidLeave = False
                            objLogin._IsHoliday = True
                            If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                            End If
                        End If

                        If objLogin._IsPaidLeave OrElse objLogin._IsUnPaidLeave Then
                            objLogin._IsHoliday = False
                            objLogin._IsDayOffShift = False
                            objLogin._IsWeekend = False
                        End If

                        objLogin.InsertLoginSummary(sqlCn, SqlTran, xDatabaseName, xUserId _
                                                                 , xYearId, xCompanyId, mdtCurrentDate.AddDays(j).Date _
                                                                 , mdtCurrentDate.AddDays(j).Date, True _
                                                                 , mblnPolicyManagementTNA _
                                                                 , mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                                                                 , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                 , mblnIsHolidayConsiderOnDayoff, 0, True)


                    End If  'If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, objLogin._Logindate.Date, "", objLogin._Employeeunkid, mintLoginSummaryId) Then


                ElseIf LoginStatus = 1 Then  'START FOR PAID LEAVE 

                    If objLogin.IsEmployeeLoginExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, "", objLogin._Employeeunkid, mintLoginId) Then
                        Dim dtLoginSummary As DataTable = objLogin.IsEmployeeLoginSummaryExist(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, mintLoginSummaryId)
                        If dtLoginSummary IsNot Nothing AndAlso dtLoginSummary.Rows.Count <= 0 Then
                            objLogin._DayType = 0
                            objLogin._InOutType = 1
                            objLogin._TotalOvertimehr = 0
                            objLogin._Shorthr = 0
                            objLogin._Totalhr = 0
                            objLogin._TotalNighthr = 0
                            objLogin._IsUnPaidLeave = False
                            objLogin._IsPaidLeave = True
                            objLogin._IsWeekend = False
                            objLogin._IsAbsentProcess = True
                            objLogin._SourceType = -1 'FOR SYSTEM GENERATED ENTRY
                            objLogin._Ot2 = 0
                            objLogin._Ot3 = 0
                            objLogin._Ot4 = 0
                            objLogin._Early_Coming = 0
                            objLogin._Late_Coming = 0
                            objLogin._Early_Going = 0
                            objLogin._Late_Going = 0

                            objLogin.InsertLoginSummary(sqlCn, SqlTran, xDatabaseName, xUserId _
                                                              , xYearId, xCompanyId, mdtCurrentDate.AddDays(j).Date _
                                                              , mdtCurrentDate.AddDays(j).Date, True _
                                                              , mblnPolicyManagementTNA _
                                                              , mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                                                              , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                              , mblnIsHolidayConsiderOnDayoff, 0, True)

                        Else
                            objLogin._IsUnPaidLeave = False
                            objLogin._IsPaidLeave = True
                            objLogin._IsWeekend = False
                            objLogin._IsDayOffShift = False

                            If GetGroupName().ToUpper.Trim() = "COLAS LTD" AndAlso mintLoginId > 0 Then
                                objLogin._Loginunkid(sqlCn, SqlTran, xDatabaseName) = mintLoginId
                                objLogin._Shorthr = 0
                                objLogin._TotalNighthr = 0
                                objLogin._Ot2 = 0
                                objLogin._Ot3 = 0
                                objLogin._Ot4 = 0
                                objLogin._Early_Coming = 0
                                objLogin._Late_Coming = 0
                                objLogin._Early_Going = 0
                                objLogin._Late_Going = 0

                                objLogin.InsertLoginSummary(sqlCn, SqlTran, xDatabaseName, xUserId _
                                                              , xYearId, xCompanyId, mdtCurrentDate.AddDays(j).Date _
                                                              , mdtCurrentDate.AddDays(j).Date, True _
                                                              , mblnPolicyManagementTNA _
                                                              , mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                                                              , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                              , mblnIsHolidayConsiderOnDayoff, 0, True)

                                mintLoginId = 0
                                'objLogin._Loginunkid(sqlCn, SqlTran, xDatabaseName) = mintLoginId

                            End If  ' If GetGroupName().ToUpper.Trim() = "COLAS LTD" AndAlso mintLoginId > 0 Then

                            objLogin._IsAbsentProcess = True
                            objLogin.UpdateLoginSummaryForAbsent(sqlCn, SqlTran, xDatabaseName)

                        End If 'If dtLoginSummary IsNot Nothing AndAlso dtLoginSummary.Rows.Count <= 0 Then

                        dtLoginSummary.Clear()
                        dtLoginSummary.Dispose()
                        dtLoginSummary = Nothing

                    Else
                        objLogin._DayType = 1
                        objLogin._InOutType = 1
                        objLogin._TotalOvertimehr = 0
                        objLogin._Shorthr = 0
                        objLogin._IsHoliday = False
                        objLogin._IsWeekend = False
                        objLogin._IsUnPaidLeave = False
                        objLogin._IsPaidLeave = True
                        objLogin._IsAbsentProcess = True
                        objLogin._SourceType = -1 'FOR SYSTEM GENERATED ENTRY
                        objLogin._Ot2 = 0
                        objLogin._Ot3 = 0
                        objLogin._Ot4 = 0
                        objLogin._Early_Coming = 0
                        objLogin._Late_Coming = 0
                        objLogin._Early_Going = 0
                        objLogin._Late_Going = 0

                        'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                        '    objLogin._IsDayOffShift = False
                        'End If

                        If mblnDayOff Then objLogin._IsDayOffShift = False

                        objLogin.InsertLoginSummary(sqlCn, SqlTran, xDatabaseName, xUserId _
                                                                 , xYearId, xCompanyId, mdtCurrentDate.AddDays(j).Date _
                                                                 , mdtCurrentDate.AddDays(j).Date, True _
                                                                 , mblnPolicyManagementTNA _
                                                                 , mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                                                                 , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                 , mblnIsHolidayConsiderOnDayoff, 0, True)

                    End If  'If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, objLogin._Logindate.Date, "", objLogin._Employeeunkid, mintLoginId) Then


                ElseIf LoginStatus = 2 Then   'FOR UNPAID LEAVE 

                    If objLogin.IsEmployeeLoginExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, "", objLogin._Employeeunkid, mintLoginId) Then
                        Dim dtLoginSummary As DataTable = objLogin.IsEmployeeLoginSummaryExist(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, mintLoginSummaryId)
                        If dtLoginSummary IsNot Nothing AndAlso dtLoginSummary.Rows.Count <= 0 Then
                            objLogin._DayType = 0
                            objLogin._InOutType = 1
                            objLogin._TotalOvertimehr = 0
                            objLogin._Shorthr = 0
                            objLogin._Totalhr = 0
                            objLogin._TotalNighthr = 0
                            objLogin._IsUnPaidLeave = True
                            objLogin._IsDayOffShift = False
                            objLogin._IsPaidLeave = False
                            objLogin._IsWeekend = False
                            objLogin._IsAbsentProcess = True
                            objLogin._IsHoliday = False
                            objLogin._SourceType = -1 'FOR SYSTEM GENERATED ENTRY
                            objLogin._Ot2 = 0
                            objLogin._Ot3 = 0
                            objLogin._Ot4 = 0
                            objLogin._Early_Coming = 0
                            objLogin._Late_Coming = 0
                            objLogin._Early_Going = 0
                            objLogin._Late_Going = 0

                            If objLogin._LeaveDayFraction <= 0 Then
                                objLogin._LeaveDayFraction = objLogin.SetEmployeeDayFraction(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate, objLogin._Shiftunkid)
                            End If


                            objLogin.InsertLoginSummary(sqlCn, SqlTran, xDatabaseName, xUserId _
                                                            , xYearId, xCompanyId, mdtCurrentDate.AddDays(j).Date _
                                                            , mdtCurrentDate.AddDays(j).Date, True _
                                                            , mblnPolicyManagementTNA _
                                                            , mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                                                            , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                            , mblnIsHolidayConsiderOnDayoff, 0, True)



                        Else

                            objLogin._IsDayOffShift = False
                            objLogin._IsUnPaidLeave = True
                            objLogin._IsPaidLeave = False
                            objLogin._IsWeekend = False
                            objLogin._IsHoliday = False

                            If objLogin._LeaveDayFraction <= 0 Then
                                objLogin._LeaveDayFraction = objLogin.SetEmployeeDayFraction(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate, objLogin._Shiftunkid)
                            End If

                            objLogin._IsAbsentProcess = True
                            objLogin.UpdateLoginSummaryForAbsent(sqlCn, SqlTran, xDatabaseName)

                        End If  'If dtLoginSummary IsNot Nothing AndAlso dtLoginSummary.Rows.Count <= 0 Then

                        dtLoginSummary.Clear()
                        dtLoginSummary.Dispose()
                        dtLoginSummary = Nothing

                    Else
                        objLogin._Holdunkid = 0
                        objLogin._DayType = 0
                        objLogin._IsWeekend = False
                        objLogin._IsHoliday = False
                        objLogin._IsDayOffShift = False
                        objLogin._IsPaidLeave = False
                        objLogin._IsUnPaidLeave = True
                        objLogin._IsAbsentProcess = True
                        objLogin._TotalOvertimehr = 0
                        objLogin._Shorthr = 0
                        objLogin._Ot2 = 0
                        objLogin._Ot3 = 0
                        objLogin._Ot4 = 0
                        objLogin._Early_Coming = 0
                        objLogin._Late_Coming = 0
                        objLogin._Early_Going = 0
                        objLogin._Late_Going = 0

                        'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                        '    objLogin._IsDayOffShift = False
                        'End If

                        If mblnDayOff Then objLogin._IsDayOffShift = False

                        If objLogin._LeaveDayFraction <= 0 Then
                            objLogin._LeaveDayFraction = objLogin.SetEmployeeDayFraction(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate, objLogin._Shiftunkid)
                        End If

                        objLogin.InsertLoginSummary(sqlCn, SqlTran, xDatabaseName, xUserId _
                                                                 , xYearId, xCompanyId, mdtCurrentDate.AddDays(j).Date _
                                                                 , mdtCurrentDate.AddDays(j).Date, True _
                                                                 , mblnPolicyManagementTNA _
                                                                 , mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                                                                 , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                                 , mblnIsHolidayConsiderOnDayoff, 0, True)

                    End If  'If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, objLogin._Logindate.Date, "", objLogin._Employeeunkid, mintLoginId) Then

                ElseIf mblnIsWeekend = False And LoginStatus = 3 Then   'FOR UNPLANNED LEAVE 
                    objLogin._Holdunkid = 0
                    objLogin._DayType = 0
                    objLogin._IsHoliday = False
                    objLogin._IsWeekend = False
                    objLogin._IsDayOffShift = False
                    objLogin._IsPaidLeave = False
                    objLogin._IsUnPaidLeave = True
                    objLogin._IsAbsentProcess = True
                    objLogin._TotalOvertimehr = 0
                    objLogin._Shorthr = 0
                    objLogin._Ot2 = 0
                    objLogin._Ot3 = 0
                    objLogin._Ot4 = 0
                    objLogin._Early_Coming = 0
                    objLogin._Late_Coming = 0
                    objLogin._Early_Going = 0
                    objLogin._Late_Going = 0

                    'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                    '    objLogin._IsDayOffShift = True
                    '    objLogin._IsUnPaidLeave = False
                    'End If

                    If mblnDayOff Then
                        objLogin._IsDayOffShift = True
                        objLogin._IsUnPaidLeave = False
                    End If

                    If objLogin._LeaveDayFraction <= 0 Then
                        objLogin._LeaveDayFraction = objLogin.SetEmployeeDayFraction(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate, objLogin._Shiftunkid)
                    End If

                    Dim dtLoginSummary As DataTable = objLogin.IsEmployeeLoginSummaryExist(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, mintLoginSummaryId)
                    If dtLoginSummary IsNot Nothing AndAlso dtLoginSummary.Rows.Count > 0 Then
                        objLogin._IsAbsentProcess = True
                        objLogin.UpdateLoginSummaryForAbsent(sqlCn, SqlTran, xDatabaseName)
                    Else
                        objLogin.InsertLoginSummary(sqlCn, SqlTran, xDatabaseName, xUserId _
                                                               , xYearId, xCompanyId, mdtCurrentDate.AddDays(j).Date _
                                                               , mdtCurrentDate.AddDays(j).Date, True _
                                                               , mblnPolicyManagementTNA _
                                                               , mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                                                               , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                               , mblnIsHolidayConsiderOnDayoff, 0, True)

                    End If 'If dtLoginSummary IsNot Nothing AndAlso dtLoginSummary.Rows.Count <= 0 Then

                    dtLoginSummary.Clear()
                    dtLoginSummary.Dispose()
                    dtLoginSummary = Nothing

                ElseIf LoginStatus = 4 Then  'FOR PAID LEAVE BUT LEAVE BALANCE IS IN MINUS...

                    If objLogin.IsEmployeeLoginExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, "", objLogin._Employeeunkid, mintLoginId) Then
                        objLogin._IsUnPaidLeave = True
                        objLogin._IsPaidLeave = True
                        objLogin._IsWeekend = False
                        objLogin._IsHoliday = False
                        'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                        '    objLogin._IsDayOffShift = False
                        'End If
                        If mblnDayOff Then objLogin._IsDayOffShift = False

                        objLogin._IsAbsentProcess = True
                        objLogin.UpdateLoginSummaryForAbsent(sqlCn, SqlTran, xDatabaseName)

                    Else
                        objLogin._checkintime = CDate(objLogin._Logindate.Date & " " & CDate(drRow("starttime")).ToLongTimeString)
                        objLogin._Holdunkid = 0
                        objLogin._Checkouttime = CDate(objLogin._Logindate.Date & " " & CDate(drRow("endtime")).ToLongTimeString)
                        objLogin._Workhour = CInt(drRow("workinghrsinsec"))
                        objLogin._Original_InTime = objLogin._checkintime
                        objLogin._Original_OutTime = objLogin._Checkouttime
                        objLogin._DayType = 0
                        objLogin._InOutType = 1
                        objLogin._TotalOvertimehr = 0
                        objLogin._Shorthr = 0
                        objLogin._IsHoliday = False
                        objLogin._IsWeekend = False
                        objLogin._IsDayOffShift = False
                        objLogin._IsUnPaidLeave = True
                        objLogin._IsPaidLeave = True
                        objLogin._IsAbsentProcess = True
                        objLogin._SourceType = -1 'FOR SYSTEM GENERATED ENTRY
                        objLogin._Ot2 = 0
                        objLogin._Ot3 = 0
                        objLogin._Ot4 = 0
                        objLogin._Early_Coming = 0
                        objLogin._Late_Coming = 0
                        objLogin._Early_Going = 0
                        objLogin._Late_Going = 0

                        'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                        '    objLogin._IsDayOffShift = False
                        'End If

                        If mblnDayOff Then objLogin._IsDayOffShift = False

                        objLogin.Insert(sqlCn, SqlTran, xDatabaseName, xUserId, xUserId, xCompanyId, mdtCurrentDate.AddDays(j).Date, mdtCurrentDate.AddDays(j).Date, True _
                                              , mblnPolicyManagementTNA, mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut, mblnIsHolidayConsiderOnWeekend _
                                              , mblnIsDayOffConsiderOnWeekend, mblnIsHolidayConsiderOnDayoff)


                        'objLogin.InsertLoginSummary(sqlCn, SqlTran, xDatabaseName, xUserId _
                        '                                             , xYearId, xCompanyId, mdtCurrentDate.AddDays(j).Date _
                        '                                             , mdtCurrentDate.AddDays(j).Date, True _
                        '                                             , mblnPolicyManagementTNA _
                        '                                             , mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                        '                                             , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                        '                                             , mblnIsHolidayConsiderOnDayoff, 0, True)


                    End If   '   If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, objLogin._Logindate.Date, "", objLogin._Employeeunkid, mintLoginId) Then


                ElseIf LoginStatus = 5 Then   'FOR HOLIDAY NO ENTRY IF EMPLOYEE IS NOT PRESENT ON HOLIDAY

                    objLogin._DayType = 0
                    objLogin._IsDayOffShift = False
                    objLogin._IsUnPaidLeave = False
                    objLogin._IsPaidLeave = False
                    objLogin._IsWeekend = False
                    objLogin._IsHoliday = True
                    objLogin._IsAbsentProcess = True

                    'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                    '    objLogin._IsDayOffShift = True
                    'End If

                    If mblnDayOff Then objLogin._IsDayOffShift = True


                    Dim dtLoginSummary As DataTable = objLogin.IsEmployeeLoginSummaryExist(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, mintLoginSummaryId)
                    If dtLoginSummary IsNot Nothing AndAlso dtLoginSummary.Rows.Count > 0 Then
                        If objLogin._IsDayOffShift AndAlso objLogin._IsHoliday Then
                            objLogin._IsHoliday = mblnIsHolidayConsiderOnDayoff
                            objLogin._IsDayOffShift = Not mblnIsHolidayConsiderOnDayoff
                        End If

                        objLogin.UpdateLoginSummaryForAbsent(sqlCn, SqlTran, xDatabaseName)

                    Else
                        objLogin.InsertLoginSummary(sqlCn, SqlTran, xDatabaseName, xUserId _
                                                             , xYearId, xCompanyId, mdtCurrentDate.AddDays(j).Date _
                                                             , mdtCurrentDate.AddDays(j).Date, True _
                                                             , mblnPolicyManagementTNA _
                                                             , mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                                                             , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                             , mblnIsHolidayConsiderOnDayoff, 0, True)

                    End If   'If dtLoginSummary IsNot Nothing AndAlso dtLoginSummary.Rows.Count > 0 Then

                    dtLoginSummary.Clear()
                    dtLoginSummary.Dispose()
                    dtLoginSummary = Nothing

                ElseIf LoginStatus = 0 Then   'FOR PRESENT EMPLOYEE, UPDATE ONLY ABSENT PROCESS TRUE

                    Dim dtEmployeeLeaveInfo As DataTable = objLogin.GetEmployeeLeaveInfoForAbsent(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, xLeaveBalanceSetting)

                    If mblnCountAttendanceNotLVIfLVapplied Then  'CONSIDER ATTENDANCE NOT LEAVE IF LEAVE APPLIED AND EMPLOYEE PRESENT ON THIS DAY

                        If objLogin._LeaveDayFraction = 1 Then

                            If dtEmployeeLeaveInfo IsNot Nothing AndAlso dtEmployeeLeaveInfo.Rows.Count > 0 AndAlso objLogin.GetLoginSourceType(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date) <> -1 Then

                                If GetGroupName().ToUpper.Trim() <> "COLAS LTD" Then

                                End If  ' If GetGroupName().ToUpper.Trim() <> "COLAS LTD" Then

                                objLogin._LeaveDayFraction = objLogin.SetEmployeeDayFraction(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate, objLogin._Shiftunkid)

                            End If  'If dtEmployeeLeaveInfo IsNot Nothing AndAlso dtEmployeeLeaveInfo.Rows.Count > 0 AndAlso objLogin.GetLoginSourceType(sqlCn, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date) <> -1 Then

                        ElseIf objLogin._LeaveDayFraction < 1 AndAlso objLogin._LeaveDayFraction > 0 Then

                            If mblnIsWeekend Then
                                objLogin._IsWeekend = True

                                Dim mintStatusId As Integer = objLogin.GetEmployeeLeaveData(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate.Date, objLogin._Shiftunkid, xLeaveBalanceSetting, mblnIsWeekend)

                                If mintStatusId = 1 Then
                                    objLogin._IsUnPaidLeave = False
                                    objLogin._IsPaidLeave = True
                                    objLogin._DayType = 1
                                ElseIf mintStatusId = 2 Then
                                    objLogin._IsUnPaidLeave = True
                                    objLogin._IsPaidLeave = False
                                ElseIf mintStatusId = 3 OrElse mintStatusId = 4 Then
                                    objLogin._IsUnPaidLeave = True
                                    objLogin._IsPaidLeave = False
                                ElseIf mintStatusId = 5 Then
                                    objLogin._IsUnPaidLeave = False
                                    objLogin._IsPaidLeave = False
                                    objLogin._IsHoliday = True
                                    If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                        objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                        objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                                    End If
                                End If  'If mintStatusId = 1 Then

                            End If '  If mblnIsWeekend Then

                            'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                            '    objLogin._IsDayOffShift = True
                            '    If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                            '        objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                            '        objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                            '    End If
                            'End If

                            'Dim dtHoliday As DataTable = objLogin.GetEmployeeHolidayList(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, mdtCurrentDate.AddDays(j).Date)
                            'If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                            '    objLogin._IsHoliday = True
                            '    If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                            '        objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                            '        objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                            '    End If
                            'End If
                            'dtHoliday.Clear()
                            'dtHoliday.Dispose()
                            'dtHoliday = Nothing

                            If mblnDayOff Then
                                objLogin._IsDayOffShift = True
                                If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                                    objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                                    objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                                End If
                            End If

                            If mblnHoliday Then
                                objLogin._IsHoliday = True
                                If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                    objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                    objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                                End If
                            End If

                            If dtEmployeeLeaveInfo IsNot Nothing AndAlso dtEmployeeLeaveInfo.Rows.Count > 0 AndAlso objLogin.GetLoginSourceType(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date) <> -1 Then
                                If CBool(dtEmployeeLeaveInfo.Rows(0)("ispaid")) Then
                                    objLogin._IsUnPaidLeave = False
                                    objLogin._IsPaidLeave = True
                                ElseIf CBool(dtEmployeeLeaveInfo.Rows(0)("ispaid")) = False Then
                                    objLogin._IsUnPaidLeave = True
                                    objLogin._IsPaidLeave = False
                                End If
                            End If
                            dtEmployeeLeaveInfo.Clear()
                            dtEmployeeLeaveInfo.Dispose()
                            dtEmployeeLeaveInfo = Nothing

                            If objLogin._IsPaidLeave OrElse objLogin._IsUnPaidLeave Then
                                objLogin._IsHoliday = False
                                objLogin._IsDayOffShift = False
                                objLogin._IsWeekend = False
                            End If

                            If GetGroupName().ToUpper.Trim() = "COLAS LTD" Then
                                If objLogin.IsEmployeeLoginExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, "", objLogin._Employeeunkid, mintLoginId) Then

                                    If mintLoginId > 0 Then
                                        objLogin._Loginunkid(sqlCn, SqlTran, xDatabaseName) = mintLoginId
                                        objLogin._Shorthr = 0
                                        objLogin._TotalNighthr = 0
                                        objLogin._Ot2 = 0
                                        objLogin._Ot3 = 0
                                        objLogin._Ot4 = 0
                                        objLogin._Early_Coming = 0
                                        objLogin._Late_Coming = 0
                                        objLogin._Early_Going = 0
                                        objLogin._Late_Going = 0

                                        objLogin.InsertLoginSummary(sqlCn, SqlTran, xDatabaseName, xUserId _
                                                               , xYearId, xCompanyId, mdtCurrentDate.AddDays(j).Date _
                                                               , mdtCurrentDate.AddDays(j).Date, True _
                                                               , mblnPolicyManagementTNA _
                                                               , mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                                                               , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                               , mblnIsHolidayConsiderOnDayoff, 0, True)

                                        mintLoginId = 0
                                        'objLogin._Loginunkid(sqlCn, SqlTran, xDatabaseName) = mintLoginId

                                    End If  'If mintLoginId > 0 Then

                                End If  '   If objLogin.IsEmployeeLoginExist(sqlCn, Nothing, xDatabaseName, objLogin._Logindate.Date, "", objLogin._Employeeunkid, mintLoginId) Then

                            End If  '  If GetGroupName().ToUpper.Trim() = "COLAS LTD" Then

                        ElseIf objLogin._LeaveDayFraction <= 0 Then

                            If dtEmployeeLeaveInfo IsNot Nothing AndAlso dtEmployeeLeaveInfo.Rows.Count > 0 AndAlso objLogin.GetLoginSourceType(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date) <> -1 Then
                                objLogin._LeaveDayFraction = objLogin.SetEmployeeDayFraction(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate, objLogin._Shiftunkid)
                            Else
                                If mblnIsWeekend Then
                                    objLogin._IsWeekend = True
                                    Dim mintStatusId As Integer = objLogin.GetEmployeeLeaveData(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate.Date, objLogin._Shiftunkid, xLeaveBalanceSetting, mblnIsWeekend)

                                    If mintStatusId = 1 Then
                                        objLogin._IsUnPaidLeave = False
                                        objLogin._IsPaidLeave = True
                                        objLogin._DayType = 1
                                    ElseIf mintStatusId = 2 Then
                                        objLogin._IsUnPaidLeave = True
                                        objLogin._IsPaidLeave = False
                                    ElseIf mintStatusId = 3 OrElse mintStatusId = 4 Then
                                        objLogin._IsUnPaidLeave = True
                                        objLogin._IsPaidLeave = False
                                        objLogin._IsWeekend = False
                                    ElseIf mintStatusId = 5 Then
                                        objLogin._IsUnPaidLeave = False
                                        objLogin._IsPaidLeave = False
                                        objLogin._IsHoliday = True
                                        If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                            objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                            objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                                        End If
                                    End If

                                End If  'If mblnIsWeekend Then

                                'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                                '    objLogin._IsDayOffShift = True
                                '    If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                                '        objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                                '        objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                                '    End If
                                'End If

                                'Dim dtHoliday As DataTable = objLogin.GetEmployeeHolidayList(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, mdtCurrentDate.AddDays(j).Date)
                                'If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                                '    objLogin._IsHoliday = True
                                '    If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                '        objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                '        objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                                '    End If
                                'End If
                                'dtHoliday.Clear()
                                'dtHoliday.Dispose()
                                'dtHoliday = Nothing

                                If mblnDayOff Then
                                    objLogin._IsDayOffShift = True
                                    If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                                        objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                                        objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                                    End If
                                End If

                                If mblnHoliday Then
                                    objLogin._IsHoliday = True
                                    If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                        objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                        objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                                    End If
                                End If

                                If objLogin._IsPaidLeave OrElse objLogin._IsUnPaidLeave Then
                                    objLogin._IsHoliday = False
                                    objLogin._IsDayOffShift = False
                                    objLogin._IsWeekend = False
                                End If

                            End If 'If dtEmployeeLeaveInfo IsNot Nothing AndAlso dtEmployeeLeaveInfo.Rows.Count > 0 AndAlso objLogin.GetLoginSourceType(sqlCn, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date) <> -1 Then

                        End If   '  If objLogin._LeaveDayFraction = 1 Then 


                    ElseIf mblnCountAttendanceNotLVIfLVapplied = False Then  'CONSIDER LEAVE  NOT ATTENDANCE IF LEAVE APPLIED AND EMPLOYEE PRESENT ON THIS DAY

                        If dtEmployeeLeaveInfo IsNot Nothing AndAlso dtEmployeeLeaveInfo.Rows.Count > 0 AndAlso objLogin.GetLoginSourceType(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date) <> -1 Then

                            objLogin.VoidAttendance(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate, objLogin._Shiftunkid, mblnDonotAttendanceinSeconds)

                            If mblnIsWeekend Then
                                objLogin._IsWeekend = True

                                Dim mintStatusId As Integer = objLogin.GetEmployeeLeaveData(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate.Date, objLogin._Shiftunkid, xLeaveBalanceSetting, mblnIsWeekend)

                                If mintStatusId = 1 Then
                                    objLogin._IsUnPaidLeave = False
                                    objLogin._IsPaidLeave = True
                                    objLogin._IsWeekend = False
                                    objLogin._DayType = 1
                                ElseIf mintStatusId = 2 Then
                                    objLogin._IsUnPaidLeave = True
                                    objLogin._IsPaidLeave = False
                                    objLogin._IsWeekend = False
                                ElseIf mintStatusId = 3 OrElse mintStatusId = 4 Then
                                    objLogin._IsUnPaidLeave = True
                                    objLogin._IsPaidLeave = False
                                    objLogin._IsWeekend = False
                                ElseIf mintStatusId = 5 Then
                                    objLogin._IsUnPaidLeave = False
                                    objLogin._IsPaidLeave = False
                                    objLogin._IsHoliday = True
                                    If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                        objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                        objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                                    End If
                                End If  '    If mintStatusId = 1 Then

                            End If  ' If mblnIsWeekend Then

                            'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                            '    objLogin._IsDayOffShift = True
                            '    If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                            '        objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                            '        objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                            '    End If
                            'End If

                            'Dim dtHoliday As DataTable = objLogin.GetEmployeeHolidayList(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, mdtCurrentDate.AddDays(j).Date)
                            'If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                            '    objLogin._IsHoliday = True
                            '    If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                            '        objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                            '        objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                            '    End If
                            'End If
                            'dtHoliday.Clear()
                            'dtHoliday.Dispose()
                            'dtHoliday = Nothing

                            If mblnDayOff Then
                                objLogin._IsDayOffShift = True
                                If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                                    objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                                    objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                                End If
                            End If

                            If mblnHoliday Then
                                objLogin._IsHoliday = True
                                If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                    objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                    objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                                End If
                            End If

                            If dtEmployeeLeaveInfo IsNot Nothing AndAlso dtEmployeeLeaveInfo.Rows.Count > 0 AndAlso objLogin.GetLoginSourceType(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date) <> -1 Then
                                If CBool(dtEmployeeLeaveInfo.Rows(0)("ispaid")) Then
                                    objLogin._IsUnPaidLeave = False
                                    objLogin._IsPaidLeave = True
                                ElseIf CBool(dtEmployeeLeaveInfo.Rows(0)("ispaid")) = False Then
                                    objLogin._IsUnPaidLeave = True
                                    objLogin._IsPaidLeave = False
                                End If
                            End If
                            dtEmployeeLeaveInfo.Clear()
                            dtEmployeeLeaveInfo.Dispose()
                            dtEmployeeLeaveInfo = Nothing

                            objLogin._IsAbsentProcess = True
                            If objLogin._IsPaidLeave OrElse objLogin._IsUnPaidLeave Then
                                objLogin._IsHoliday = False
                                objLogin._IsDayOffShift = False
                                objLogin._IsWeekend = False
                            End If

                            objLogin.InsertLoginSummary(sqlCn, SqlTran, xDatabaseName, xUserId _
                                                              , xYearId, xCompanyId, mdtCurrentDate.AddDays(j).Date _
                                                              , mdtCurrentDate.AddDays(j).Date, True _
                                                              , mblnPolicyManagementTNA _
                                                              , mblnDonotAttendanceinSeconds, mblnFirstCheckInLastCheckOut _
                                                              , mblnIsHolidayConsiderOnWeekend, mblnIsDayOffConsiderOnWeekend _
                                                              , mblnIsHolidayConsiderOnDayoff, 0, True)

                            Continue For

                        Else

                            If mblnIsWeekend Then

                                objLogin._IsWeekend = True

                                Dim mintStatusId As Integer = objLogin.GetEmployeeLeaveData(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, objLogin._Logindate.Date, objLogin._Shiftunkid, xLeaveBalanceSetting, mblnIsWeekend)

                                If mintStatusId = 1 Then
                                    objLogin._IsUnPaidLeave = False
                                    objLogin._IsPaidLeave = True
                                    objLogin._IsWeekend = False
                                    objLogin._DayType = 1
                                ElseIf mintStatusId = 2 Then
                                    objLogin._IsUnPaidLeave = True
                                    objLogin._IsPaidLeave = False
                                    objLogin._IsWeekend = False
                                ElseIf mintStatusId = 3 OrElse mintStatusId = 4 Then
                                    objLogin._IsUnPaidLeave = True
                                    objLogin._IsPaidLeave = False
                                    objLogin._IsWeekend = False
                                ElseIf mintStatusId = 5 Then
                                    objLogin._IsUnPaidLeave = False
                                    objLogin._IsPaidLeave = False
                                    objLogin._IsHoliday = True
                                    If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                        objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                        objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                                    End If

                                End If '   If mintStatusId = 1 Then

                            End If '  If mblnIsWeekend Then

                            'If objLogin.IsDayOffExist(sqlCn, SqlTran, xDatabaseName, objLogin._Logindate.Date, objLogin._Employeeunkid) Then
                            '    objLogin._IsDayOffShift = True
                            '    If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                            '        objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                            '        objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                            '    End If
                            'End If

                            'Dim dtHoliday As DataTable = objLogin.GetEmployeeHolidayList(sqlCn, SqlTran, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date, mdtCurrentDate.AddDays(j).Date)
                            'If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                            '    objLogin._IsHoliday = True
                            '    If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                            '        objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                            '        objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                            '    End If
                            'End If
                            'dtHoliday.Clear()
                            'dtHoliday.Dispose()
                            'dtHoliday = Nothing

                            If mblnDayOff Then
                                objLogin._IsDayOffShift = True
                                If objLogin._IsDayOffShift AndAlso objLogin._IsWeekend Then
                                    objLogin._IsDayOffShift = mblnIsDayOffConsiderOnWeekend
                                    objLogin._IsWeekend = Not mblnIsDayOffConsiderOnWeekend
                                End If
                            End If

                            If mblnHoliday Then
                                objLogin._IsHoliday = True
                                If objLogin._IsWeekend AndAlso objLogin._IsHoliday Then
                                    objLogin._IsHoliday = mblnIsHolidayConsiderOnWeekend
                                    objLogin._IsWeekend = Not mblnIsHolidayConsiderOnWeekend
                                End If
                            End If

                            If objLogin._IsPaidLeave OrElse objLogin._IsUnPaidLeave Then
                                objLogin._IsHoliday = False
                                objLogin._IsDayOffShift = False
                                objLogin._IsWeekend = False
                            End If

                        End If 'If dtEmployeeLeaveInfo IsNot Nothing AndAlso dtEmployeeLeaveInfo.Rows.Count > 0 AndAlso objLogin.GetLoginSourceType(sqlCn, xDatabaseName, objLogin._Employeeunkid, mdtCurrentDate.AddDays(j).Date) <> -1 Then

                    End If   ' If mblnCountAttendanceNotLVIfLVapplied Then

                    objLogin._IsAbsentProcess = True
                    objLogin.UpdateLoginSummaryForAbsent(sqlCn, SqlTran, xDatabaseName)

                End If   'If LoginStatus = 7 Then  'Weekend 

                SqlTran.Commit()

            Next       'For j As Integer = 0 To xCount - 1

            dtEmpShift.Rows.Clear()
            dtEmpShift.Dispose()
            GC.SuppressFinalize(dtEmpShift)

        Catch ex As Exception
            SqlTran.Rollback()
            WriteLog("DoAbsentProcess " & Date.Now & " : " & ex.Message)
            mblnFlag = False
        Finally
            SqlTran.Dispose()
            GC.SuppressFinalize(SqlTran)
            GC.SuppressFinalize(objLogin)
            objLogin = Nothing
        End Try
        Return mblnFlag
    End Function

    Private Sub getCurrentPeriodTenure(ByVal xDatabaseName As String, ByVal xYearId As Integer, ByVal mdtCurrentDate As Date, ByRef mdtStartDate As Date, ByRef mdtEndDate As Date)
        Dim StrQ As String = ""
        Dim sqlCmd As SqlCommand = Nothing
        Try

            StrQ = "SELECT  TOP 1 cfcommon_period_tran.periodunkid " & _
                       ", cfcommon_period_tran.period_code " & _
                       ", cfcommon_period_tran.period_name " & _
                       ", cfcommon_period_tran.yearunkid " & _
                       ", cfcommon_period_tran.start_date AS start_date " & _
                       ", cfcommon_period_tran.end_date AS end_date " & _
                       ", cfcommon_period_tran.statusid " & _
                       " FROM " & xDatabaseName & "..cfcommon_period_tran " & _
                       " WHERE   isactive = 1 " & _
                       " AND modulerefid = @modulerefid AND yearunkid = @yearunkid  AND statusid = @statusid "

            Dim strQCondition As String = StrQ & " " & " AND CONVERT(CHAR(8),cfcommon_period_tran.start_date,112) <= @Date AND  CONVERT(CHAR(8),cfcommon_period_tran.end_date,112) >= @Date "


            sqlCmd = New SqlCommand(strQCondition, sqlCn)
            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@modulerefid", enModuleReference.Payroll)
            sqlCmd.Parameters.AddWithValue("@yearunkid", xYearId)
            sqlCmd.Parameters.AddWithValue("@statusid", enStatusType.OPEN)
            sqlCmd.Parameters.AddWithValue("@Date", convertDate(mdtCurrentDate))

            Dim sqlDataadp As New SqlDataAdapter()
            sqlDataadp.SelectCommand = sqlCmd
            Dim dtTable As New DataTable
            sqlDataadp.Fill(dtTable)

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mdtStartDate = dtTable.Rows(0)("start_date").ToString()
                mdtEndDate = dtTable.Rows(0)("end_date").ToString()
                If mdtCurrentDate.Date < mdtEndDate.Date Then
                    mdtEndDate = mdtCurrentDate
                End If
            Else

                StrQ &= " ORDER BY start_date "

                sqlCmd = New SqlCommand(strQCondition, sqlCn)
                sqlCmd.Parameters.Clear()
                sqlCmd.Parameters.AddWithValue("@modulerefid", enModuleReference.Payroll)
                sqlCmd.Parameters.AddWithValue("@yearunkid", xYearId)
                sqlCmd.Parameters.AddWithValue("@statusid", enStatusType.OPEN)

                sqlDataadp = New SqlDataAdapter()
                sqlDataadp.SelectCommand = sqlCmd
                Dim dtPeriod As New DataTable
                sqlDataadp.Fill(dtPeriod)
                If dtPeriod IsNot Nothing AndAlso dtPeriod.Rows.Count > 0 Then
                    mdtStartDate = dtPeriod.Rows(0)("start_date").ToString()
                    mdtEndDate = dtPeriod.Rows(0)("end_date").ToString()
                    If mdtCurrentDate.Date < mdtEndDate.Date Then
                        mdtEndDate = mdtCurrentDate
                    End If
                End If
            End If

        Catch ex As Exception
            WriteLog("getCurrentPeriodTenure " & Date.Now & " : " & ex.Message)
        End Try
    End Sub

    Public Function GetEmpShiftHolidayWeekendDayOffList(ByVal xDatabaseName As String, ByVal mintEmployeeId As Integer, ByVal dtStartDate As Date, ByVal dtEndDate As Date, ByVal mblnIsHolidayConsiderOnWeekend As Boolean _
                                                   , ByVal mblnIsDayOffConsiderOnWeekend As Boolean, ByVal mblnIsHolidayConsiderOnDayoff As Boolean) As DataTable
        Dim strQ As String = ""
        Dim dtEmpShiftList As DataTable = Nothing
        Dim sqlCmd As SqlCommand = Nothing
        Try
            strQ &= " DECLARE @days TABLE (ddate DATETIME) " & _
                        " DECLARE @dw AS INT " & _
                        " DECLARE @daystable TABLE " & _
                        " ( " & _
                        "     employeeunkid INT  " & _
                        "   , ddate DATETIME " & _
                        "   , isweekend BIT " & _
                        "   , shiftid INTEGER " & _
                        " ) " & _
                        " SET @dw = DATEPART(dw, @StartDate); " & _
                        " WITH mdate AS  " & _
                        " ( " & _
                        "   SELECT CAST(@StartDate AS DATETIME) AS Dvalue " & _
                        "   UNION ALL " & _
                        "   SELECT Dvalue + 1 FROM mdate WHERE Dvalue + 1 <= @EndDate " & _
                        " ) " & _
                        " INSERT INTO @days " & _
                        " SELECT * FROM mdate " & _
                        " OPTION (MAXRECURSION 0); " & _
                        " DECLARE @date AS CHAR(8) " & _
                        " WHILE (@StartDate <=  (@EndDate + 1)) " & _
                        " BEGIN " & _
                        "     SET @date = (SELECT CONVERT(CHAR(8), ddate, 112) FROM @days WHERE CONVERT(CHAR(8), ddate, 112) = @StartDate) " & _
                        "     INSERT INTO @daystable " & _
                        "     SELECT " & _
                        "         employeeunkid  " & _
                        "       , CAST(@StartDate AS DATETIME) " & _
                        "       , isweekend " & _
                        "       , shiftunkid " & _
                        "     FROM " & _
                        "     ( " & _
                        "       SELECT " & _
                        "             hremployee_shift_tran.employeeunkid  " & _
                        "           , CONVERT(CHAR(8),hremployee_shift_tran.effectivedate,112) AS effectivedate " & _
                        "           , hremployee_shift_tran.shiftunkid " & _
                        "           , tnashift_tran.isweekend " & _
                        "           , ROW_NUMBER() OVER ( PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY effectivedate DESC ) Rowno " & _
                        "       FROM " & xDatabaseName & "..hremployee_shift_tran " & _
                        "       JOIN " & xDatabaseName & "..tnashift_tran ON tnashift_tran.shiftunkid = hremployee_shift_tran.shiftunkid " & _
                        "       WHERE hremployee_shift_tran.isvoid = 0 " & _
                        "           AND hremployee_shift_tran.employeeunkid = @employeeunkid " & _
                        "           AND CONVERT(CHAR(8), effectivedate, 112) <= @date " & _
                        "           AND DATEPART(dw, @date) = tnashift_tran.dayid + 1 " & _
                        "     ) AS A " & _
                        "     WHERE A.Rowno = 1 " & _
                         "     SET @StartDate =CONVERT(CHAR(8), CAST(@StartDate AS DATETIME) + 1,112) " & _
                        " END " & _
                        " SELECT " & _
                        "      [@daystable].employeeunkid  " & _
                        "    , CONVERT(CHAR(8),[@daystable].ddate,112) AS ddate " & _
                        "    , shiftid as Shiftunkid " & _
                        "    , [@daystable].isweekend AS isweekend " & _
                        "    , 0 AS isholiday " & _
                        "    , 0 AS isdayoff " & _
                        " FROM @daystable " & _
                        " UNION ALL " & _
                         " SELECT " & _
                        "      lvemployee_holiday.employeeunkid  " & _
                        "    , CONVERT(CHAR(8),lvholiday_master.holidaydate,112) AS ddate " & _
                        "    , dd.shiftid " & _
                        "    , 0 AS isweekend " & _
                        "    , 1 AS isholiday " & _
                        "    , 0 AS isdayoff " & _
                        " FROM " & xDatabaseName & "..lvemployee_holiday " & _
                        " JOIN " & xDatabaseName & "..lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid " & _
                        " JOIN @daystable dd ON dd.employeeunkid = lvemployee_holiday.employeeunkid " & _
                        " AND CONVERT(CHAR(8),dd.ddate,112) = CONVERT(CHAR(8),lvholiday_master.holidaydate,112) " & _
                        " WHERE  lvholiday_master.isactive=1 " & _
                        " AND lvemployee_holiday.employeeunkid = @employeeunkid " & _
                        " AND CONVERT(CHAR(8), lvholiday_master.holidaydate, 112) BETWEEN @StartDate AND @EndDate " & _
                        " UNION ALL " & _
                        " SELECT " & _
                        "      hremployee_dayoff_tran.employeeunkid  " & _
                        "    , CONVERT(CHAR(8),hremployee_dayoff_tran.dayoffdate,112) AS ddate " & _
                        "    , dd.shiftid " & _
                        "    , 0 AS isweekend " & _
                        "    , 0 AS isholiday " & _
                        "    , 1 AS isdayoff " & _
                        " FROM " & xDatabaseName & "..hremployee_dayoff_tran " & _
                        " JOIN @daystable dd ON dd.employeeunkid = hremployee_dayoff_tran.employeeunkid " & _
                        " AND CONVERT(CHAR(8),dd.ddate,112) = CONVERT(CHAR(8),hremployee_dayoff_tran.dayoffdate,112) " & _
                        " WHERE hremployee_dayoff_tran.isvoid = 0 " & _
                        " AND hremployee_dayoff_tran.employeeunkid = @employeeunkid " & _
                        " AND CONVERT(CHAR(8), dayoffdate, 112) BETWEEN @StartDate AND @EndDate " & _
                        " ORDER BY ddate "


            sqlCmd = New SqlCommand(strQ, sqlCn)
            sqlCmd.Parameters.Clear()
            sqlCmd.Parameters.AddWithValue("@StartDate", convertDate(dtStartDate.Date).ToString())
            sqlCmd.Parameters.AddWithValue("@EndDate", convertDate(dtEndDate.Date).ToString())
            sqlCmd.Parameters.AddWithValue("@employeeunkid", mintEmployeeId)

            Dim sqlDataadp As New SqlDataAdapter()
            sqlDataadp.SelectCommand = sqlCmd
            dtEmpShiftList = New DataTable
            sqlDataadp.Fill(dtEmpShiftList)


            If mblnIsHolidayConsiderOnWeekend OrElse mblnIsDayOffConsiderOnWeekend Then   'Remove Weekend
                Dim xEnum = From dr In dtEmpShiftList Where CBool(dr("isweekend")) = False Select dr
                If xEnum IsNot Nothing AndAlso xEnum.Count > 0 Then
                    Dim drRow = (From xwkRow In xEnum Join xRow In dtEmpShiftList On xwkRow("ddate") Equals xRow("ddate") Where CBool(xRow("isweekend")) = True Select xRow).Distinct()
                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                        drRow.ToList.ForEach(Function(x) RemoveRowFromTable(x, dtEmpShiftList))
                    End If
                    drRow = Nothing
                End If
            End If


            If mblnIsHolidayConsiderOnDayoff Then   'Remove DayOff
                Dim xEnum = From dr In dtEmpShiftList Where CBool(dr("isdayoff")) = False Select dr
                If xEnum IsNot Nothing AndAlso xEnum.Count > 0 Then
                    Dim drRow = (From xwkRow In xEnum Join xRow In dtEmpShiftList On xwkRow("ddate") Equals xRow("ddate") Where CBool(xRow("isdayoff")) = True Select xRow).Distinct()
                    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                        drRow.ToList.ForEach(Function(x) RemoveRowFromTable(x, dtEmpShiftList))
                    End If
                    drRow = Nothing
                End If
            End If

            dtEmpShiftList.AcceptChanges()

            sqlDataadp.Dispose()
            sqlDataadp = Nothing

        Catch ex As Exception
            WriteLog("GetEmpShiftHolidayWeekendDayOffList " & Date.Now & " : " & ex.Message)
        Finally
            sqlCmd.Dispose()
            sqlCmd = Nothing
        End Try
        Return dtEmpShiftList
    End Function

    Private Function RemoveRowFromTable(ByVal dr As DataRow, ByVal mdtTable As DataTable) As Boolean
        Try
            If mdtTable IsNot Nothing Then
                mdtTable.Rows.Remove(dr)
            End If
        Catch ex As Exception
            WriteLog("RemoveRowFromTable " & Date.Now & " : " & ex.Message)
        End Try
        Return True
    End Function

    'Pinkal (14-Feb-2022) -- End

    'Pinkal (21-Jul-2023) -- Start
    '(A1X-1108) Voltamp/TRA -- Bio star device integration.

    Private Sub DownloadBiostar_Data(ByVal xDeviceType As Integer, ByVal xDatabaseAddress As String)
        Dim dtDownloadData As DataTable = Nothing
        Dim objBiostar As New clsdeviceconnection_setting
        Try

            Dim dsList As DataSet = objBiostar.GetList(sqlCn, Nothing, xCompanyId, "List", True, "devicetypeunkid = " & xDeviceType & " AND db_servername = '" & xDatabaseAddress & "'")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dsList.Tables(0).Rows

                    If xDeviceType = enFingerPrintDevice.BioStar Then
                        dtDownloadData = objBiostar.GetBioStarAttendanceData(sqlCn, Nothing, CInt(dr("connectiontypeunkid")), dr("db_servername").ToString(), dr("database_name").ToString() _
                                                                                                        , dr("db_username").ToString(), clsSecurity.Decrypt(dr("db_password").ToString(), "ezee").ToString(), dr("db_port").ToString(), Now.AddDays(-1).Date, Now.AddDays(-1).Date)

                    ElseIf xDeviceType = enFingerPrintDevice.BioStar2 Then
                        dtDownloadData = objBiostar.GetBioStar2AttendanceData(sqlCn, Nothing, CInt(dr("connectiontypeunkid")), dr("db_servername").ToString(), dr("database_name").ToString() _
                                                                                                        , dr("db_username").ToString(), clsSecurity.Decrypt(dr("db_password").ToString(), "ezee").ToString(), dr("db_port").ToString(), Now.AddDays(-1).Date, Now.AddDays(-1).Date)

                    ElseIf xDeviceType = enFingerPrintDevice.BioStar3 Then
                        dtDownloadData = objBiostar.GetBioStar3AttendanceData(sqlCn, Nothing, CInt(dr("connectiontypeunkid")), dr("db_servername").ToString(), dr("database_name").ToString() _
                                                                                                        , dr("db_username").ToString(), clsSecurity.Decrypt(dr("db_password").ToString(), "ezee").ToString(), dr("db_port").ToString(), Now.AddDays(-1).Date, Now.AddDays(-1).Date)
                    End If

                    If dtAttendanceData Is Nothing Then
                        dtAttendanceData = dtDownloadData.Copy()
                    Else
                        If dtDownloadData IsNot Nothing AndAlso dtDownloadData.Rows.Count > 0 Then
                            dtAttendanceData.Merge(dtDownloadData)
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            WriteLog("DownloadBiostar_Data : " & Date.Now & " : " & ex.Message)
        Finally
            objBiostar = Nothing
        End Try
    End Sub

    'Pinkal (21-Jul-2023) -- End


    'Pinkal (18-Aug-2023) -- Start
    '(A1X -1189) St. Judes - Automatic download of attendance data from Anviz device. 
    Private Sub IntializeAttendanceDataTable()
        Try
            If dtAttendanceData Is Nothing Then

                dtAttendanceData = New DataTable()

                Dim dcColumn As New DataColumn("UserId")
                dcColumn.DataType = Type.GetType("System.String")
                dcColumn.DefaultValue = ""
                dtAttendanceData.Columns.Add(dcColumn)

                dcColumn = Nothing
                dcColumn = New DataColumn("UserName")
                dcColumn.DataType = Type.GetType("System.String")
                dcColumn.DefaultValue = ""
                dtAttendanceData.Columns.Add(dcColumn)

                dcColumn = Nothing
                dcColumn = New DataColumn("Device")
                dcColumn.DataType = Type.GetType("System.String")
                dcColumn.DefaultValue = ""
                dtAttendanceData.Columns.Add(dcColumn)

                dcColumn = Nothing
                dcColumn = New DataColumn("IPAddress")
                dcColumn.DataType = Type.GetType("System.String")
                dcColumn.DefaultValue = ""
                dtAttendanceData.Columns.Add(dcColumn)

                dcColumn = Nothing
                dcColumn = New DataColumn("LoginDate")
                dcColumn.DataType = Type.GetType("System.DateTime")
                dcColumn.DefaultValue = DBNull.Value
                dtAttendanceData.Columns.Add(dcColumn)

                dcColumn = Nothing
            End If
        Catch ex As Exception
            WriteLog("IntializeAttendanceDataTable : " & Date.Now & " : " & ex.Message)
        End Try
    End Sub
    'Pinkal (18-Aug-2023) -- End

    'Pinkal (15-Sep-2023) -- Start
    '(A1X-158) Mbeya Cement: HIK Device Integration - Automatic attendance data download

    Private Sub DownloadHIKVisionData()
        Try
            mintHIKUserID = -1
            m_lGetAcsEventHandle = -1


            Dim struLoginInfo As New CHCNetSDK.NET_DVR_USER_LOGIN_INFO()
            Dim struDeviceInfoV40 As New CHCNetSDK.NET_DVR_DEVICEINFO_V40()
            struDeviceInfoV40.struDeviceV30.sSerialNumber = New Byte(CHCNetSDK.SERIALNO_LEN - 1) {}

            struLoginInfo.sDeviceAddress = strIp
            struLoginInfo.sUserName = mstrUserID
            struLoginInfo.sPassword = mstrPassword
            UShort.TryParse(intPort.ToString(), struLoginInfo.wPort)

            mintHIKUserID = CHCNetSDK.NET_DVR_Login_V40(struLoginInfo, struDeviceInfoV40)

            If mintHIKUserID >= 0 Then

                Dim CsTemp As String = ""
                Dim MinorType As String = ""
                Dim MajorType As String = ""

                Dim struCond As New CHCNetSDK.NET_DVR_ACS_EVENT_COND()
                struCond.Init()
                struCond.dwSize = CUInt(Marshal.SizeOf(struCond))


                If GetAcsEventType.MajorTypeDictionary.Keys.Count <= 0 Then
                    GetAcsEventType.enMajorTypeDictionary()
                End If

                If GetAcsEventType.MinorTypeDictionary.Keys.Count <= 0 Then
                    GetAcsEventType.enMinorTypeDictionary()
                End If

                If GetAcsEventType.ComInductiveEvent.Keys.Count <= 0 Then
                    GetAcsEventType.enComInductiveEvent()
                End If

                struCond.dwMajor = GetAcsEventType.ReturnMajorTypeValue("Event")
                struCond.dwMinor = GetAcsEventType.ReturnMinorTypeValue("All")

                Dim mdtDate As Date = Now.Date.AddDays(-1)

                struCond.struStartTime.dwYear = mdtDate.Date.Year
                struCond.struStartTime.dwMonth = mdtDate.Date.Month
                struCond.struStartTime.dwDay = mdtDate.Date.Day
                struCond.struStartTime.dwHour = 0
                struCond.struStartTime.dwMinute = 0
                struCond.struStartTime.dwSecond = 0

                struCond.struEndTime.dwYear = Now.Date.Year
                struCond.struEndTime.dwMonth = Now.Date.Month
                struCond.struEndTime.dwDay = Now.Date.Day
                struCond.struEndTime.dwHour = 23
                struCond.struEndTime.dwMinute = 59
                struCond.struEndTime.dwSecond = 0

                struCond.byPicEnable = 0
                struCond.szMonitorID = ""
                struCond.wInductiveEventType = 65535

                Dim dwSize As UInteger = struCond.dwSize
                Dim ptrCond As IntPtr = Marshal.AllocHGlobal(CInt(dwSize))
                Marshal.StructureToPtr(struCond, ptrCond, False)

                m_lGetAcsEventHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(mintHIKUserID, CHCNetSDK.NET_DVR_GET_ACS_EVENT, ptrCond, CInt(dwSize), Nothing, IntPtr.Zero)

                If m_lGetAcsEventHandle = -1 Then
                    Marshal.FreeHGlobal(ptrCond)
                    WriteLog("DownloadHIKVisionData : IP " & strIp & " :  NET_DVR_StartRemoteConfig FAIL, ERROR CODE : " & Date.Now & " : " & CHCNetSDK.NET_DVR_GetLastError().ToString())
                    Exit Sub
                End If

                ProcessEvent()
                Marshal.FreeHGlobal(ptrCond)

            Else
                Dim nErr As UInteger = CHCNetSDK.NET_DVR_GetLastError()

                If nErr = CHCNetSDK.NET_DVR_PASSWORD_ERROR Then
                    WriteLog("DownloadHIKVisionData : IP " & strIp & " : " & Date.Now & " : Invalid User Name or password.")
                    If 1 = struDeviceInfoV40.bySupportLock Then
                        WriteLog("DownloadHIKVisionData : IP " & strIp & " : " & Date.Now & " : you have left" & " " & struDeviceInfoV40.byRetryLoginTime & " " & "try opportunity.")
                    End If
                    Exit Sub
                ElseIf nErr = CHCNetSDK.NET_DVR_USER_LOCKED Then
                    If 1 = struDeviceInfoV40.bySupportLock Then
                        WriteLog("DownloadHIKVisionData : : IP " & strIp & " : " & Date.Now & " : User is locked now.The Remaining Lock Time is" & " " & struDeviceInfoV40.dwSurplusLockTime)
                        Exit Sub
                    End If
                Else
                    WriteLog("DownloadHIKVisionData : IP " & strIp & " : " & Date.Now & " : net error or dvr is busy!")
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            WriteLog("DownloadHIKVisionData : IP " & strIp & " : " & Date.Now & " : " & ex.Message)
        End Try
    End Sub

    Public Sub ProcessEvent()
        Dim dwStatus As Integer = 0
        Dim Flag As Boolean = True
        Dim struCFG As New CHCNetSDK.NET_DVR_ACS_EVENT_CFG()
        struCFG.dwSize = CUInt(Marshal.SizeOf(struCFG))
        Dim dwOutBuffSize As Integer = CInt(Math.Truncate(struCFG.dwSize))
        struCFG.init()


        Do While Flag

            dwStatus = CHCNetSDK.NET_DVR_GetNextRemoteConfig(m_lGetAcsEventHandle, struCFG, dwOutBuffSize)

            Select Case dwStatus
                Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_SUCCESS
                    AddAcsEventToList(struCFG)

                Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_NEED_WAIT
                    Thread.Sleep(10)

                Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FAILED
                    CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                    WriteLog("ProcessEvent : IP " & strIp & " :  NET_SDK_GET_NEXT_STATUS_FAILED : " & Date.Now & " : " & CHCNetSDK.NET_DVR_GetLastError().ToString())
                    Flag = False

                Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FINISH
                    CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                    Flag = False
                Case Else
                    WriteLog("ProcessEvent : IP " & strIp & " : NET_SDK_GET_NEXT_STATUS_UNKOWN : " & Date.Now & " : " & CHCNetSDK.NET_DVR_GetLastError().ToString())
                    Flag = False
                    CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
            End Select
        Loop
    End Sub

    Private Sub AddAcsEventToList(ByVal struEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG)
        Try
            If struEventCfg.dwMinor <> CHCNetSDK.MINOR_FINGERPRINT_COMPARE_PASS AndAlso struEventCfg.dwMinor <> CHCNetSDK.MINOR_CARD_FINGERPRINT_PASSWD_VERIFY_PASS _
              AndAlso struEventCfg.dwMinor <> CHCNetSDK.MINOR_CARD_FINGERPRINT_VERIFY_PASS AndAlso struEventCfg.dwMinor <> CHCNetSDK.MINOR_FINGERPRINT_PASSWD_VERIFY_PASS Then Exit Sub

            Dim mdtLogTime As String = GetStrLogTime(struEventCfg.struTime)

            If dtAttendanceData Is Nothing Then IntializeAttendanceDataTable()

            If dtAttendanceData IsNot Nothing Then
                Dim drRow As DataRow = dtAttendanceData.NewRow()
                drRow("UserId") = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byEmployeeNo).ToString()
                drRow("UserName") = ""
                drRow("Device") = mstrDeviceCode
                drRow("IPAddress") = strIp
                drRow("LoginDate") = CDate(mdtLogTime)
                dtAttendanceData.Rows.Add(drRow)
                dtAttendanceData.AcceptChanges()
            End If

        Catch ex As Exception
            WriteLog("AddAcsEventToList : IP " & strIp & " : " & Date.Now & " : " & ex.Message)
        End Try
    End Sub

    Private Function GetStrLogTime(ByRef time As CHCNetSDK.NET_DVR_TIME) As String
        Dim res As String = ""
        Try
            res = time.dwYear.ToString() & "-" & time.dwMonth.ToString() & "-" & time.dwDay.ToString() & " " & time.dwHour.ToString("#00") & ":" & time.dwMinute.ToString("#00") & ":" & time.dwSecond.ToString("#00")
        Catch ex As Exception
            WriteLog("GetStrLogTime : " & Date.Now & " : " & ex.Message)
        End Try
        Return res
    End Function

    'Pinkal (15-Sep-2023) -- End

#End Region

End Class

