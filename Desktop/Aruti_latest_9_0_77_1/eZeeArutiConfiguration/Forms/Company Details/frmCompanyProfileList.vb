﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmCompanyProfileList

#Region " Private Varaibles "
    Private objCompany As clsCompany_Master
    Private ReadOnly mstrModuleName As String = "frmCompanyProfileList"
#End Region

#Region " Private Function "

    'S.SANDEEP [ 30 May 2011 ] -- START
    'ISSUE : FINCA REQ.
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowAddCompany
            btnEdit.Enabled = User._Object.Privilege._AllowEditCompany
            btnDelete.Enabled = User._Object.Privilege._AllowDeleteCompany
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : SetVisibility ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 30 May 2011 ] -- END 

    Private Sub fillList()
        Dim dsCompany As New DataSet
        Dim strSearching As String = ""
        'Sandeep | 04 JAN 2010 | -- Start
        'Dim dtClassTable As New DataTable
        Dim dsCountry As New DataSet
        Dim objMaster As New clsMasterData
        'Sandeep | 04 JAN 2010 | -- END 
        Try


            'Anjan [03 Jan 2014] -- Start
            If User._Object.Privilege._CompanyCreation = False Then Exit Sub
            'Anjan [03 Jan 2014 ] -- End

            dsCompany = objCompany.GetList("Company")
            'Sandeep | 04 JAN 2010 | -- Start
            dsCountry = objMaster.getCountryList("List", False)
            'Sandeep | 04 JAN 2010 | -- END 

            Dim lvItem As ListViewItem

            lvCompanyProfile.Items.Clear()
            For Each drRow As DataRow In dsCompany.Tables(0).Rows
                lvItem = New ListViewItem
                'Sandeep | 04 JAN 2010 | -- Start
                'lvItem.Text = drRow("name").ToString
                lvItem.Text = drRow("code").ToString
                lvItem.SubItems.Add(drRow("name").ToString)
                Dim dtTemp() As DataRow = dsCountry.Tables("List").Select("countryunkid ='" & CInt(drRow("countryunkid")) & "'")
                If dtTemp.Length > 0 Then
                    lvItem.SubItems.Add(dtTemp(0)("country_name").ToString)
                Else
                    lvItem.SubItems.Add("")
                End If
                'Sandeep | 04 JAN 2010 | -- END 
                lvItem.Tag = drRow("companyunkid")
                lvCompanyProfile.Items.Add(lvItem)
            Next


            'Sandeep | 04 JAN 2010 | -- Start
            'If lvCompanyProfile.Items.Count > 16 Then
            '    colhCompanyName.Width = 655 - 18
            'Else
            '    colhCompanyName.Width = 655
            'End If

            If lvCompanyProfile.Items.Count > 16 Then
                colhCompanyName.Width = 405 - 18
            Else
                colhCompanyName.Width = 405
            End If
            'Sandeep | 04 JAN 2010 | -- END 

            'Call setAlternateColor(lvAirline)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsCompany.Dispose()
        End Try
    End Sub

    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "

    Private Sub frmCompanyProfileList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvCompanyProfile.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGradeList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCompanyProfileList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGradeList_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub frmCompanyProfileList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objCompany = New clsCompany_Master
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()
            'S.SANDEEP [ 30 May 2011 ] -- START
            'ISSUE : FINCA REQ.
            Call SetVisibility()
            'S.SANDEEP [ 30 May 2011 ] -- END 
            Call fillList()

            If lvCompanyProfile.Items.Count > 0 Then lvCompanyProfile.Items(0).Selected = True
            lvCompanyProfile.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmClassesList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGradeList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objCompany = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCompany_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsCompany_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

    'Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    '    Dim objfrm As New frmLanguage
    '    Try
    '        If User._Object._RightToLeft = True Then
    '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            objfrm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(objfrm)
    '        End If

    '        Call SetMessages()

    '        clsAirLine.SetMessages()
    '        objfrm._Other_ModuleNames = "clsAirLine"
    '        objfrm.displayDialog(Me)

    '        Call SetLanguage()

    '    Catch ex As System.Exception
    '        Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
    '    Finally
    '        objfrm.Dispose()
    '        objfrm = Nothing
    '    End Try
    'End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvCompanyProfile.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Company from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvCompanyProfile.Select()
            Exit Sub
        End If
        'If objCompany.isUsed(CInt(lvCompanyProfile.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Company. Reason: This Company is in use."), enMsgBoxStyle.Information) '?2
        '    lvCompanyProfile.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvCompanyProfile.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Company?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

'S.SANDEEP [28-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Audit Trails} 
                objCompany._FormName = mstrModuleName
                objCompany._LoginEmployeeunkid = 0
                objCompany._ClientIP = getIP()
                objCompany._HostName = getHostName()
                objCompany._FromWeb = False
                objCompany._AuditUserId = User._Object._Userunkid
objCompany._CompanyUnkid = Company._Object._Companyunkid
                objCompany._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'S.SANDEEP [28-May-2018] -- END

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objCompany.Delete(CInt(lvCompanyProfile.SelectedItems(0).Tag))
                objCompany.Delete(CInt(lvCompanyProfile.SelectedItems(0).Tag), User._Object._Userunkid)
                'Shani(24-Aug-2015) -- End


                If objCompany._Message <> "" Then
                    eZeeMsgBox.Show(objCompany._Message, enMsgBoxStyle.Information)
                Else
                    lvCompanyProfile.SelectedItems(0).Remove()
                End If

                If lvCompanyProfile.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvCompanyProfile.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvCompanyProfile.Items.Count - 1
                    lvCompanyProfile.Items(intSelectedIndex).Selected = True
                    lvCompanyProfile.EnsureVisible(intSelectedIndex)
                ElseIf lvCompanyProfile.Items.Count <> 0 Then
                    lvCompanyProfile.Items(intSelectedIndex).Selected = True
                    lvCompanyProfile.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvCompanyProfile.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvCompanyProfile.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Company from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvCompanyProfile.Select()
            Exit Sub
        End If
        Dim frm As New frmCompany_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvCompanyProfile.SelectedItems(0).Index

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If frm.displayDialog(CInt(lvCompanyProfile.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frm = Nothing

            lvCompanyProfile.Items(intSelectedIndex).Selected = True
            lvCompanyProfile.EnsureVisible(intSelectedIndex)
            lvCompanyProfile.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmCompany_AddEdit
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If
            If frm.displayDialog(-1, enAction.ADD_ONE) Then
                Call fillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
          
            Call SetLanguage()

          
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhCompanyName.Text = Language._Object.getCaption(CStr(Me.colhCompanyName.Tag), Me.colhCompanyName.Text)
            Me.colhCompanyCode.Text = Language._Object.getCaption(CStr(Me.colhCompanyCode.Tag), Me.colhCompanyCode.Text)
            Me.colhCompanyCountry.Text = Language._Object.getCaption(CStr(Me.colhCompanyCountry.Tag), Me.colhCompanyCountry.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Company from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Company?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class