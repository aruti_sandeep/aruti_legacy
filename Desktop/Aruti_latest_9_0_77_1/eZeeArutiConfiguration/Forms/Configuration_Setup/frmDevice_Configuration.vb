﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Globalization

#End Region

Public Class frmDevice_Configuration

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDevice_Configuration"
    Private mblnCancel As Boolean = True
    Dim mintConnectionType As Integer = 0
    Dim mintBiostarType As Integer = 0
    Dim mstrDatabaseServer As String = ""
    Dim mstrDatabaseName As String = ""
    Dim mstrDatabaseUserName As String = ""
    Dim mstrDatabasePassword As String = ""
    Dim mstrDatabasePortNo As String = ""
    Dim objBiostar As New clsdeviceconnection_setting

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef _ConnectionType As Integer, ByRef _BioStarType As Integer, ByRef _DatabaseServer As String, ByRef _DatabaseName As String, ByRef _DatabaseUserName As String, ByRef _DatabasePassword As String, ByRef _DatabasePortNo As String) As Boolean
        Try
            mintConnectionType = _ConnectionType
            mintBiostarType = _BioStarType
            mstrDatabaseServer = _DatabaseServer
            mstrDatabaseName = _DatabaseName
            mstrDatabaseUserName = _DatabaseUserName
            mstrDatabasePassword = _DatabasePassword
            mstrDatabasePortNo = _DatabasePortNo

            Me.ShowDialog()
            _ConnectionType = mintConnectionType
            _BioStarType = mintBiostarType
            _DatabaseServer = mstrDatabaseServer
            _DatabaseName = mstrDatabaseName
            _DatabaseUserName = mstrDatabaseUserName
            _DatabasePassword = mstrDatabasePassword
            _DatabasePortNo = mstrDatabasePortNo
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmDevice_Configuration_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            FillCombo()
            GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDevice_Configuration_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Function Validation() As Boolean
        Dim mblnFlag As Boolean = False
        Try

            If CInt(cboConnectionType.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Connection Type is compulsory information.Please define connection type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboConnectionType.Focus()
                Return False
            ElseIf CInt(cboDeviceType.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Device Type is compulsory information.Please define Device type."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboDeviceType.Focus()
                Return False
            ElseIf txtDbServer.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Database Server is compulsory information.Please define database server."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtDbServer.Focus()
                Return False
            ElseIf txtDBName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Database Name is compulsory information.Please define database name."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtDBName.Focus()
                Return False
            ElseIf txtDBUserName.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Database User Name is compulsory information.Please define database user name."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtDBUserName.Focus()
                Return False
            ElseIf txtDBUserPwd.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Database User Password is compulsory information.Please define database user password."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtDBUserPwd.Focus()
                Return False
            End If

            mblnFlag = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    Private Sub GetValue()
        Try
            If mintConnectionType > 0 Then cboConnectionType.Enabled = False
            cboConnectionType.SelectedIndex = mintConnectionType
            cboDeviceType.SelectedIndex = mintBiostarType
            cboDeviceType.Enabled = False
            txtDbServer.Text = mstrDatabaseServer
            txtDBName.Text = mstrDatabaseName
            txtDBUserName.Text = mstrDatabaseUserName
            txtDBUserPwd.Text = mstrDatabasePassword
            txtPortNo.Text = mstrDatabasePortNo
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            dsList = objBiostar.getListForComboForConnectionType("List", True)
            With cboConnectionType
                .ValueMember = "Id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With

            dsList = Nothing
            Dim objMasterData As New clsMasterData
            dsList = objMasterData.GetCommunictionDevice()
            With cboDeviceType
                .ValueMember = "Id"
                .DisplayMember = "Device"
                .DataSource = dsList.Tables(0)
            End With
            objMasterData = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnTestConnection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTestConnection.Click
        Try
            If Validation() = False Then Exit Sub
            Dim objConfig As New clsConfigOptions

            If CInt(cboConnectionType.SelectedIndex) = 1 Then   'SQL Server

                If objConfig.TestConnection_SQL(txtDbServer.Text.Trim, txtDBName.Text.Trim, "", txtDBUserName.Text.Trim, txtDBUserPwd.Text) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the database server."), enMsgBoxStyle.Information)
                End If
            ElseIf CInt(cboConnectionType.SelectedIndex) = 2 Then   'MYSQL 

                If objConfig.TestConnection_MySQL(txtDbServer.Text.Trim, txtDBName.Text.Trim, txtPortNo.Text.Trim, txtDBUserName.Text.Trim, txtDBUserPwd.Text) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Connection is done successfully."), enMsgBoxStyle.Information)
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Unable to connect the database server."), enMsgBoxStyle.Information)
                End If
            End If
            objConfig = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnTestConnection_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If Validation() = False Then Exit Sub

            mintConnectionType = CInt(cboConnectionType.SelectedValue)
            mintBiostarType = CInt(cboDeviceType.SelectedValue)
            mstrDatabaseServer = txtDbServer.Text.Trim
            mstrDatabaseName = txtDBName.Text.Trim
            mstrDatabaseUserName = txtDBUserName.Text.Trim
            mstrDatabasePassword = txtDBUserPwd.Text
            If txtPortNo.Text.Trim.Length > 0 Then
                mstrDatabasePortNo = txtPortNo.Text.Trim
            End If
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboConnectionType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboConnectionType.SelectedIndexChanged
        Try
            txtDbServer.Text = ""
            txtDBUserName.Text = ""
            txtDBUserPwd.Text = ""
            txtPortNo.Text = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboConnectionType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDeviceType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDeviceType.SelectedIndexChanged
        Try
            If cboDeviceType.SelectedIndex <= 0 Then
                mstrDatabaseName = ""
                Exit Sub
            End If

            If CInt(cboDeviceType.SelectedIndex) = enFingerPrintDevice.BioStar Then
                mstrDatabaseName = "BioStar"
            ElseIf CInt(cboDeviceType.SelectedIndex) = enFingerPrintDevice.BioStar2 Then
                mstrDatabaseName = "biostar_tna"
            ElseIf CInt(cboDeviceType.SelectedIndex) = enFingerPrintDevice.BioStar3 Then
                mstrDatabaseName = "BiostarTA"
            ElseIf CInt(cboDeviceType.SelectedIndex) = enFingerPrintDevice.Cosec Then
                mstrDatabaseName = "Cosec"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDeviceType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.btnTestConnection.GradientBackColor = GUI._ButttonBackColor
            Me.btnTestConnection.GradientForeColor = GUI._ButttonFontColor

            Me.btnCancel.GradientBackColor = GUI._ButttonBackColor
            Me.btnCancel.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblDBUserPwd.Text = Language._Object.getCaption(Me.lblDBUserPwd.Name, Me.lblDBUserPwd.Text)
            Me.lblDBUserName.Text = Language._Object.getCaption(Me.lblDBUserName.Name, Me.lblDBUserName.Text)
            Me.lblDBName.Text = Language._Object.getCaption(Me.lblDBName.Name, Me.lblDBName.Text)
            Me.lblDbServer.Text = Language._Object.getCaption(Me.lblDbServer.Name, Me.lblDbServer.Text)
            Me.LblPortNo.Text = Language._Object.getCaption(Me.LblPortNo.Name, Me.LblPortNo.Text)
            Me.btnTestConnection.Text = Language._Object.getCaption(Me.btnTestConnection.Name, Me.btnTestConnection.Text)
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.Name, Me.btnCancel.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.LblConnectionType.Text = Language._Object.getCaption(Me.LblConnectionType.Name, Me.LblConnectionType.Text)
            Me.LblDeviceType.Text = Language._Object.getCaption(Me.LblDeviceType.Name, Me.LblDeviceType.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Connection Type is compulsory information.Please define connection type.")
            Language.setMessage(mstrModuleName, 2, "Device Type is compulsory information.Please define Device type.")
            Language.setMessage(mstrModuleName, 3, "Database Server is compulsory information.Please define database server.")
            Language.setMessage(mstrModuleName, 4, "Database Name is compulsory information.Please define database name.")
            Language.setMessage(mstrModuleName, 5, "Database User Name is compulsory information.Please define database user name.")
            Language.setMessage(mstrModuleName, 6, "Database User Password is compulsory information.Please define database user password.")
            Language.setMessage(mstrModuleName, 7, "Connection is done successfully.")
            Language.setMessage(mstrModuleName, 8, "Unable to connect the database server.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class