﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUser_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUser_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.tabcPrivilegeInformation = New System.Windows.Forms.TabControl
        Me.tabpCompanySelection = New System.Windows.Forms.TabPage
        Me.tvCompanyPrivilege = New System.Windows.Forms.TreeView
        Me.tabpOperationalPrivilege = New System.Windows.Forms.TabPage
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.TxtPrivilegeSearch = New eZee.TextBox.AlphanumericTextBox
        Me.tvOperationalPrivilege = New System.Windows.Forms.TreeView
        Me.gbUserDetails = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlUserInfo = New System.Windows.Forms.Panel
        Me.objpnlPersonal = New System.Windows.Forms.Panel
        Me.txtFirstName = New eZee.TextBox.AlphanumericTextBox
        Me.txtAddress2 = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress2 = New System.Windows.Forms.Label
        Me.lblFirstname = New System.Windows.Forms.Label
        Me.txtAddress1 = New eZee.TextBox.AlphanumericTextBox
        Me.lblEmail = New System.Windows.Forms.Label
        Me.lblPhone = New System.Windows.Forms.Label
        Me.txtLastName = New eZee.TextBox.AlphanumericTextBox
        Me.lblAddress1 = New System.Windows.Forms.Label
        Me.txtEmail = New eZee.TextBox.AlphanumericTextBox
        Me.txtPhone = New eZee.TextBox.AlphanumericTextBox
        Me.lblLastName = New System.Windows.Forms.Label
        Me.objpnlPrivacy = New System.Windows.Forms.Panel
        Me.txtUser_Name = New eZee.TextBox.AlphanumericTextBox
        Me.txtPassword = New eZee.TextBox.AlphanumericTextBox
        Me.lblPassword = New System.Windows.Forms.Label
        Me.lblUserName = New System.Windows.Forms.Label
        Me.txtRetypePassword = New eZee.TextBox.AlphanumericTextBox
        Me.lblVerifyPassword = New System.Windows.Forms.Label
        Me.dtpReloginDate = New System.Windows.Forms.DateTimePicker
        Me.lblReloginDate = New System.Windows.Forms.Label
        Me.chkIsManager = New System.Windows.Forms.CheckBox
        Me.elPersonalDetails = New eZee.Common.eZeeLine
        Me.dtpExpirydate = New System.Windows.Forms.DateTimePicker
        Me.objbtnAddRole = New eZee.Common.eZeeGradientButton
        Me.pnlExPassword = New System.Windows.Forms.Panel
        Me.radYes = New System.Windows.Forms.RadioButton
        Me.radNo = New System.Windows.Forms.RadioButton
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblExpirePassword = New System.Windows.Forms.Label
        Me.cboLanguage = New System.Windows.Forms.ComboBox
        Me.lblLanguage = New System.Windows.Forms.Label
        Me.EZeeLine2 = New eZee.Common.eZeeLine
        Me.chkRightToLeft = New System.Windows.Forms.CheckBox
        Me.EZeeLine1 = New eZee.Common.eZeeLine
        Me.lblAbilityLevel = New System.Windows.Forms.Label
        Me.cboUserRole = New System.Windows.Forms.ComboBox
        Me.EZeeLine3 = New eZee.Common.eZeeLine
        Me.elUserDetails = New eZee.Common.eZeeLine
        Me.chkActiveUser = New System.Windows.Forms.CheckBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMainInfo.SuspendLayout()
        Me.tabcPrivilegeInformation.SuspendLayout()
        Me.tabpCompanySelection.SuspendLayout()
        Me.tabpOperationalPrivilege.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbUserDetails.SuspendLayout()
        Me.pnlUserInfo.SuspendLayout()
        Me.objpnlPersonal.SuspendLayout()
        Me.objpnlPrivacy.SuspendLayout()
        Me.pnlExPassword.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.tabcPrivilegeInformation)
        Me.pnlMainInfo.Controls.Add(Me.gbUserDetails)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(675, 403)
        Me.pnlMainInfo.TabIndex = 0
        '
        'tabcPrivilegeInformation
        '
        Me.tabcPrivilegeInformation.Controls.Add(Me.tabpCompanySelection)
        Me.tabcPrivilegeInformation.Controls.Add(Me.tabpOperationalPrivilege)
        Me.tabcPrivilegeInformation.Location = New System.Drawing.Point(373, 12)
        Me.tabcPrivilegeInformation.Name = "tabcPrivilegeInformation"
        Me.tabcPrivilegeInformation.SelectedIndex = 0
        Me.tabcPrivilegeInformation.Size = New System.Drawing.Size(293, 331)
        Me.tabcPrivilegeInformation.TabIndex = 1
        '
        'tabpCompanySelection
        '
        Me.tabpCompanySelection.Controls.Add(Me.tvCompanyPrivilege)
        Me.tabpCompanySelection.Location = New System.Drawing.Point(4, 22)
        Me.tabpCompanySelection.Name = "tabpCompanySelection"
        Me.tabpCompanySelection.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpCompanySelection.Size = New System.Drawing.Size(285, 305)
        Me.tabpCompanySelection.TabIndex = 2
        Me.tabpCompanySelection.Text = "Company Privilege"
        Me.tabpCompanySelection.UseVisualStyleBackColor = True
        '
        'tvCompanyPrivilege
        '
        Me.tvCompanyPrivilege.CheckBoxes = True
        Me.tvCompanyPrivilege.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvCompanyPrivilege.Location = New System.Drawing.Point(3, 3)
        Me.tvCompanyPrivilege.Name = "tvCompanyPrivilege"
        Me.tvCompanyPrivilege.Size = New System.Drawing.Size(279, 299)
        Me.tvCompanyPrivilege.TabIndex = 0
        '
        'tabpOperationalPrivilege
        '
        Me.tabpOperationalPrivilege.Controls.Add(Me.objbtnReset)
        Me.tabpOperationalPrivilege.Controls.Add(Me.objbtnSearch)
        Me.tabpOperationalPrivilege.Controls.Add(Me.TxtPrivilegeSearch)
        Me.tabpOperationalPrivilege.Controls.Add(Me.tvOperationalPrivilege)
        Me.tabpOperationalPrivilege.Location = New System.Drawing.Point(4, 22)
        Me.tabpOperationalPrivilege.Name = "tabpOperationalPrivilege"
        Me.tabpOperationalPrivilege.Padding = New System.Windows.Forms.Padding(3)
        Me.tabpOperationalPrivilege.Size = New System.Drawing.Size(285, 305)
        Me.tabpOperationalPrivilege.TabIndex = 0
        Me.tabpOperationalPrivilege.Text = "Operational Privilege"
        Me.tabpOperationalPrivilege.UseVisualStyleBackColor = True
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(255, 6)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 166
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(232, 6)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 165
        Me.objbtnSearch.TabStop = False
        '
        'TxtPrivilegeSearch
        '
        Me.TxtPrivilegeSearch.Flags = 0
        Me.TxtPrivilegeSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtPrivilegeSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.TxtPrivilegeSearch.Location = New System.Drawing.Point(3, 7)
        Me.TxtPrivilegeSearch.Name = "TxtPrivilegeSearch"
        Me.TxtPrivilegeSearch.Size = New System.Drawing.Size(224, 21)
        Me.TxtPrivilegeSearch.TabIndex = 164
        Me.TxtPrivilegeSearch.Tag = "user_name"
        '
        'tvOperationalPrivilege
        '
        Me.tvOperationalPrivilege.CheckBoxes = True
        Me.tvOperationalPrivilege.Location = New System.Drawing.Point(3, 33)
        Me.tvOperationalPrivilege.Name = "tvOperationalPrivilege"
        Me.tvOperationalPrivilege.Size = New System.Drawing.Size(279, 269)
        Me.tvOperationalPrivilege.TabIndex = 0
        '
        'gbUserDetails
        '
        Me.gbUserDetails.BorderColor = System.Drawing.Color.Black
        Me.gbUserDetails.Checked = False
        Me.gbUserDetails.CollapseAllExceptThis = False
        Me.gbUserDetails.CollapsedHoverImage = Nothing
        Me.gbUserDetails.CollapsedNormalImage = Nothing
        Me.gbUserDetails.CollapsedPressedImage = Nothing
        Me.gbUserDetails.CollapseOnLoad = False
        Me.gbUserDetails.Controls.Add(Me.pnlUserInfo)
        Me.gbUserDetails.Controls.Add(Me.chkActiveUser)
        Me.gbUserDetails.ExpandedHoverImage = Nothing
        Me.gbUserDetails.ExpandedNormalImage = Nothing
        Me.gbUserDetails.ExpandedPressedImage = Nothing
        Me.gbUserDetails.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbUserDetails.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbUserDetails.HeaderHeight = 25
        Me.gbUserDetails.HeaderMessage = ""
        Me.gbUserDetails.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbUserDetails.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbUserDetails.HeightOnCollapse = 0
        Me.gbUserDetails.LeftTextSpace = 0
        Me.gbUserDetails.Location = New System.Drawing.Point(12, 12)
        Me.gbUserDetails.Name = "gbUserDetails"
        Me.gbUserDetails.OpenHeight = 300
        Me.gbUserDetails.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbUserDetails.ShowBorder = True
        Me.gbUserDetails.ShowCheckBox = False
        Me.gbUserDetails.ShowCollapseButton = False
        Me.gbUserDetails.ShowDefaultBorderColor = True
        Me.gbUserDetails.ShowDownButton = False
        Me.gbUserDetails.ShowHeader = True
        Me.gbUserDetails.Size = New System.Drawing.Size(355, 331)
        Me.gbUserDetails.TabIndex = 0
        Me.gbUserDetails.Temp = 0
        Me.gbUserDetails.Text = "User Details"
        Me.gbUserDetails.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlUserInfo
        '
        Me.pnlUserInfo.AutoScroll = True
        Me.pnlUserInfo.Controls.Add(Me.objpnlPersonal)
        Me.pnlUserInfo.Controls.Add(Me.objpnlPrivacy)
        Me.pnlUserInfo.Controls.Add(Me.dtpReloginDate)
        Me.pnlUserInfo.Controls.Add(Me.lblReloginDate)
        Me.pnlUserInfo.Controls.Add(Me.chkIsManager)
        Me.pnlUserInfo.Controls.Add(Me.elPersonalDetails)
        Me.pnlUserInfo.Controls.Add(Me.dtpExpirydate)
        Me.pnlUserInfo.Controls.Add(Me.objbtnAddRole)
        Me.pnlUserInfo.Controls.Add(Me.pnlExPassword)
        Me.pnlUserInfo.Controls.Add(Me.lblDate)
        Me.pnlUserInfo.Controls.Add(Me.lblExpirePassword)
        Me.pnlUserInfo.Controls.Add(Me.cboLanguage)
        Me.pnlUserInfo.Controls.Add(Me.lblLanguage)
        Me.pnlUserInfo.Controls.Add(Me.EZeeLine2)
        Me.pnlUserInfo.Controls.Add(Me.chkRightToLeft)
        Me.pnlUserInfo.Controls.Add(Me.EZeeLine1)
        Me.pnlUserInfo.Controls.Add(Me.lblAbilityLevel)
        Me.pnlUserInfo.Controls.Add(Me.cboUserRole)
        Me.pnlUserInfo.Controls.Add(Me.EZeeLine3)
        Me.pnlUserInfo.Controls.Add(Me.elUserDetails)
        Me.pnlUserInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlUserInfo.Location = New System.Drawing.Point(2, 26)
        Me.pnlUserInfo.Name = "pnlUserInfo"
        Me.pnlUserInfo.Size = New System.Drawing.Size(351, 303)
        Me.pnlUserInfo.TabIndex = 141
        '
        'objpnlPersonal
        '
        Me.objpnlPersonal.Controls.Add(Me.txtFirstName)
        Me.objpnlPersonal.Controls.Add(Me.txtAddress2)
        Me.objpnlPersonal.Controls.Add(Me.lblAddress2)
        Me.objpnlPersonal.Controls.Add(Me.lblFirstname)
        Me.objpnlPersonal.Controls.Add(Me.txtAddress1)
        Me.objpnlPersonal.Controls.Add(Me.lblEmail)
        Me.objpnlPersonal.Controls.Add(Me.lblPhone)
        Me.objpnlPersonal.Controls.Add(Me.txtLastName)
        Me.objpnlPersonal.Controls.Add(Me.lblAddress1)
        Me.objpnlPersonal.Controls.Add(Me.txtEmail)
        Me.objpnlPersonal.Controls.Add(Me.txtPhone)
        Me.objpnlPersonal.Controls.Add(Me.lblLastName)
        Me.objpnlPersonal.Location = New System.Drawing.Point(9, 368)
        Me.objpnlPersonal.Name = "objpnlPersonal"
        Me.objpnlPersonal.Size = New System.Drawing.Size(319, 166)
        Me.objpnlPersonal.TabIndex = 173
        '
        'txtFirstName
        '
        Me.txtFirstName.Flags = 0
        Me.txtFirstName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFirstName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFirstName.Location = New System.Drawing.Point(113, 3)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(179, 21)
        Me.txtFirstName.TabIndex = 162
        Me.txtFirstName.Tag = "user_name"
        '
        'txtAddress2
        '
        Me.txtAddress2.Flags = 0
        Me.txtAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress2.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress2.Location = New System.Drawing.Point(113, 84)
        Me.txtAddress2.Name = "txtAddress2"
        Me.txtAddress2.Size = New System.Drawing.Size(179, 21)
        Me.txtAddress2.TabIndex = 167
        Me.txtAddress2.Tag = "user_name"
        '
        'lblAddress2
        '
        Me.lblAddress2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress2.Location = New System.Drawing.Point(20, 87)
        Me.lblAddress2.Name = "lblAddress2"
        Me.lblAddress2.Size = New System.Drawing.Size(87, 14)
        Me.lblAddress2.TabIndex = 168
        Me.lblAddress2.Text = "Address2"
        '
        'lblFirstname
        '
        Me.lblFirstname.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFirstname.Location = New System.Drawing.Point(20, 6)
        Me.lblFirstname.Name = "lblFirstname"
        Me.lblFirstname.Size = New System.Drawing.Size(87, 14)
        Me.lblFirstname.TabIndex = 161
        Me.lblFirstname.Text = "First Name"
        '
        'txtAddress1
        '
        Me.txtAddress1.Flags = 0
        Me.txtAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress1.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtAddress1.Location = New System.Drawing.Point(113, 57)
        Me.txtAddress1.Name = "txtAddress1"
        Me.txtAddress1.Size = New System.Drawing.Size(179, 21)
        Me.txtAddress1.TabIndex = 166
        Me.txtAddress1.Tag = "user_name"
        '
        'lblEmail
        '
        Me.lblEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.Location = New System.Drawing.Point(20, 141)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(87, 14)
        Me.lblEmail.TabIndex = 172
        Me.lblEmail.Text = "Email"
        '
        'lblPhone
        '
        Me.lblPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhone.Location = New System.Drawing.Point(20, 114)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(87, 14)
        Me.lblPhone.TabIndex = 169
        Me.lblPhone.Text = "Phone / Mobile"
        '
        'txtLastName
        '
        Me.txtLastName.Flags = 0
        Me.txtLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLastName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLastName.Location = New System.Drawing.Point(113, 30)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(179, 21)
        Me.txtLastName.TabIndex = 163
        Me.txtLastName.Tag = "user_name"
        '
        'lblAddress1
        '
        Me.lblAddress1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAddress1.Location = New System.Drawing.Point(20, 60)
        Me.lblAddress1.Name = "lblAddress1"
        Me.lblAddress1.Size = New System.Drawing.Size(87, 14)
        Me.lblAddress1.TabIndex = 165
        Me.lblAddress1.Text = "Address1"
        '
        'txtEmail
        '
        Me.txtEmail.Flags = 0
        Me.txtEmail.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmail.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmail.Location = New System.Drawing.Point(113, 138)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(179, 21)
        Me.txtEmail.TabIndex = 171
        Me.txtEmail.Tag = "user_name"
        '
        'txtPhone
        '
        Me.txtPhone.Flags = 0
        Me.txtPhone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhone.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPhone.Location = New System.Drawing.Point(113, 111)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(179, 21)
        Me.txtPhone.TabIndex = 170
        Me.txtPhone.Tag = "user_name"
        '
        'lblLastName
        '
        Me.lblLastName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastName.Location = New System.Drawing.Point(20, 33)
        Me.lblLastName.Name = "lblLastName"
        Me.lblLastName.Size = New System.Drawing.Size(87, 14)
        Me.lblLastName.TabIndex = 164
        Me.lblLastName.Text = "Last Name"
        '
        'objpnlPrivacy
        '
        Me.objpnlPrivacy.Controls.Add(Me.txtUser_Name)
        Me.objpnlPrivacy.Controls.Add(Me.txtPassword)
        Me.objpnlPrivacy.Controls.Add(Me.lblPassword)
        Me.objpnlPrivacy.Controls.Add(Me.lblUserName)
        Me.objpnlPrivacy.Controls.Add(Me.txtRetypePassword)
        Me.objpnlPrivacy.Controls.Add(Me.lblVerifyPassword)
        Me.objpnlPrivacy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlPrivacy.Location = New System.Drawing.Point(9, 29)
        Me.objpnlPrivacy.Name = "objpnlPrivacy"
        Me.objpnlPrivacy.Size = New System.Drawing.Size(319, 81)
        Me.objpnlPrivacy.TabIndex = 175
        '
        'txtUser_Name
        '
        Me.txtUser_Name.Flags = 0
        Me.txtUser_Name.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUser_Name.InvalidChars = New Char(-1) {}
        Me.txtUser_Name.Location = New System.Drawing.Point(113, 3)
        Me.txtUser_Name.Name = "txtUser_Name"
        Me.txtUser_Name.Size = New System.Drawing.Size(179, 21)
        Me.txtUser_Name.TabIndex = 142
        Me.txtUser_Name.Tag = "user_name"
        '
        'txtPassword
        '
        Me.txtPassword.Flags = 0
        Me.txtPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtPassword.Location = New System.Drawing.Point(113, 30)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(179, 21)
        Me.txtPassword.TabIndex = 144
        Me.txtPassword.Tag = "password"
        '
        'lblPassword
        '
        Me.lblPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPassword.Location = New System.Drawing.Point(18, 33)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(87, 14)
        Me.lblPassword.TabIndex = 143
        Me.lblPassword.Text = "Password"
        '
        'lblUserName
        '
        Me.lblUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserName.Location = New System.Drawing.Point(18, 6)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(87, 14)
        Me.lblUserName.TabIndex = 141
        Me.lblUserName.Text = "User Name"
        '
        'txtRetypePassword
        '
        Me.txtRetypePassword.Flags = 0
        Me.txtRetypePassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRetypePassword.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtRetypePassword.Location = New System.Drawing.Point(113, 57)
        Me.txtRetypePassword.Name = "txtRetypePassword"
        Me.txtRetypePassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtRetypePassword.Size = New System.Drawing.Size(179, 21)
        Me.txtRetypePassword.TabIndex = 146
        Me.txtRetypePassword.Tag = ""
        '
        'lblVerifyPassword
        '
        Me.lblVerifyPassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVerifyPassword.Location = New System.Drawing.Point(18, 60)
        Me.lblVerifyPassword.Name = "lblVerifyPassword"
        Me.lblVerifyPassword.Size = New System.Drawing.Size(87, 14)
        Me.lblVerifyPassword.TabIndex = 145
        Me.lblVerifyPassword.Text = "Verify Password"
        '
        'dtpReloginDate
        '
        Me.dtpReloginDate.Checked = False
        Me.dtpReloginDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpReloginDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpReloginDate.Location = New System.Drawing.Point(122, 168)
        Me.dtpReloginDate.Name = "dtpReloginDate"
        Me.dtpReloginDate.ShowCheckBox = True
        Me.dtpReloginDate.Size = New System.Drawing.Size(112, 21)
        Me.dtpReloginDate.TabIndex = 174
        '
        'lblReloginDate
        '
        Me.lblReloginDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReloginDate.Location = New System.Drawing.Point(29, 171)
        Me.lblReloginDate.Name = "lblReloginDate"
        Me.lblReloginDate.Size = New System.Drawing.Size(87, 14)
        Me.lblReloginDate.TabIndex = 173
        Me.lblReloginDate.Text = "Relogin Date"
        '
        'chkIsManager
        '
        Me.chkIsManager.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkIsManager.BackColor = System.Drawing.Color.Transparent
        Me.chkIsManager.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsManager.Location = New System.Drawing.Point(122, 199)
        Me.chkIsManager.Name = "chkIsManager"
        Me.chkIsManager.Size = New System.Drawing.Size(179, 19)
        Me.chkIsManager.TabIndex = 143
        Me.chkIsManager.Text = "Manager/ Desktop"
        Me.chkIsManager.UseVisualStyleBackColor = False
        '
        'elPersonalDetails
        '
        Me.elPersonalDetails.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elPersonalDetails.Location = New System.Drawing.Point(6, 348)
        Me.elPersonalDetails.Name = "elPersonalDetails"
        Me.elPersonalDetails.Size = New System.Drawing.Size(322, 17)
        Me.elPersonalDetails.TabIndex = 160
        Me.elPersonalDetails.Text = "Personal Details"
        '
        'dtpExpirydate
        '
        Me.dtpExpirydate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpExpirydate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpExpirydate.Location = New System.Drawing.Point(122, 141)
        Me.dtpExpirydate.Name = "dtpExpirydate"
        Me.dtpExpirydate.Size = New System.Drawing.Size(112, 21)
        Me.dtpExpirydate.TabIndex = 159
        '
        'objbtnAddRole
        '
        Me.objbtnAddRole.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddRole.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddRole.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddRole.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddRole.BorderSelected = False
        Me.objbtnAddRole.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddRole.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Mini_Add
        Me.objbtnAddRole.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddRole.Location = New System.Drawing.Point(307, 239)
        Me.objbtnAddRole.Name = "objbtnAddRole"
        Me.objbtnAddRole.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddRole.TabIndex = 158
        '
        'pnlExPassword
        '
        Me.pnlExPassword.Controls.Add(Me.radYes)
        Me.pnlExPassword.Controls.Add(Me.radNo)
        Me.pnlExPassword.Location = New System.Drawing.Point(122, 114)
        Me.pnlExPassword.Name = "pnlExPassword"
        Me.pnlExPassword.Size = New System.Drawing.Size(179, 21)
        Me.pnlExPassword.TabIndex = 157
        '
        'radYes
        '
        Me.radYes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radYes.Location = New System.Drawing.Point(3, 3)
        Me.radYes.Name = "radYes"
        Me.radYes.Size = New System.Drawing.Size(79, 15)
        Me.radYes.TabIndex = 22
        Me.radYes.TabStop = True
        Me.radYes.Text = "Yes"
        Me.radYes.UseVisualStyleBackColor = True
        '
        'radNo
        '
        Me.radNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radNo.Location = New System.Drawing.Point(89, 2)
        Me.radNo.Name = "radNo"
        Me.radNo.Size = New System.Drawing.Size(85, 16)
        Me.radNo.TabIndex = 23
        Me.radNo.TabStop = True
        Me.radNo.Text = "No"
        Me.radNo.UseVisualStyleBackColor = True
        '
        'lblDate
        '
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(29, 144)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(87, 14)
        Me.lblDate.TabIndex = 156
        Me.lblDate.Text = "Date"
        '
        'lblExpirePassword
        '
        Me.lblExpirePassword.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpirePassword.Location = New System.Drawing.Point(29, 118)
        Me.lblExpirePassword.Name = "lblExpirePassword"
        Me.lblExpirePassword.Size = New System.Drawing.Size(87, 14)
        Me.lblExpirePassword.TabIndex = 155
        Me.lblExpirePassword.Text = "Expire Password"
        '
        'cboLanguage
        '
        Me.cboLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLanguage.FormattingEnabled = True
        Me.cboLanguage.Location = New System.Drawing.Point(122, 324)
        Me.cboLanguage.Name = "cboLanguage"
        Me.cboLanguage.Size = New System.Drawing.Size(179, 21)
        Me.cboLanguage.TabIndex = 154
        '
        'lblLanguage
        '
        Me.lblLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLanguage.Location = New System.Drawing.Point(29, 328)
        Me.lblLanguage.Name = "lblLanguage"
        Me.lblLanguage.Size = New System.Drawing.Size(87, 14)
        Me.lblLanguage.TabIndex = 153
        Me.lblLanguage.Text = "Language"
        '
        'EZeeLine2
        '
        Me.EZeeLine2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine2.Location = New System.Drawing.Point(6, 303)
        Me.EZeeLine2.Name = "EZeeLine2"
        Me.EZeeLine2.Size = New System.Drawing.Size(322, 17)
        Me.EZeeLine2.TabIndex = 152
        Me.EZeeLine2.Text = "Language Setting"
        '
        'chkRightToLeft
        '
        Me.chkRightToLeft.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRightToLeft.Location = New System.Drawing.Point(122, 283)
        Me.chkRightToLeft.Name = "chkRightToLeft"
        Me.chkRightToLeft.Size = New System.Drawing.Size(179, 17)
        Me.chkRightToLeft.TabIndex = 151
        Me.chkRightToLeft.Text = "Right to Left Interfaces"
        Me.chkRightToLeft.UseVisualStyleBackColor = True
        '
        'EZeeLine1
        '
        Me.EZeeLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine1.Location = New System.Drawing.Point(6, 263)
        Me.EZeeLine1.Name = "EZeeLine1"
        Me.EZeeLine1.Size = New System.Drawing.Size(322, 17)
        Me.EZeeLine1.TabIndex = 150
        Me.EZeeLine1.Text = "Layout Setting"
        '
        'lblAbilityLevel
        '
        Me.lblAbilityLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAbilityLevel.Location = New System.Drawing.Point(29, 243)
        Me.lblAbilityLevel.Name = "lblAbilityLevel"
        Me.lblAbilityLevel.Size = New System.Drawing.Size(87, 14)
        Me.lblAbilityLevel.TabIndex = 148
        Me.lblAbilityLevel.Text = "User Role"
        '
        'cboUserRole
        '
        Me.cboUserRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUserRole.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUserRole.FormattingEnabled = True
        Me.cboUserRole.Location = New System.Drawing.Point(122, 239)
        Me.cboUserRole.Name = "cboUserRole"
        Me.cboUserRole.Size = New System.Drawing.Size(179, 21)
        Me.cboUserRole.TabIndex = 149
        '
        'EZeeLine3
        '
        Me.EZeeLine3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.EZeeLine3.Location = New System.Drawing.Point(6, 219)
        Me.EZeeLine3.Name = "EZeeLine3"
        Me.EZeeLine3.Size = New System.Drawing.Size(322, 17)
        Me.EZeeLine3.TabIndex = 147
        Me.EZeeLine3.Text = "Role Settings"
        '
        'elUserDetails
        '
        Me.elUserDetails.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elUserDetails.Location = New System.Drawing.Point(6, 9)
        Me.elUserDetails.Name = "elUserDetails"
        Me.elUserDetails.Size = New System.Drawing.Size(322, 17)
        Me.elUserDetails.TabIndex = 140
        Me.elUserDetails.Text = "User Privacy"
        '
        'chkActiveUser
        '
        Me.chkActiveUser.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkActiveUser.AutoSize = True
        Me.chkActiveUser.BackColor = System.Drawing.Color.Transparent
        Me.chkActiveUser.Checked = True
        Me.chkActiveUser.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkActiveUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkActiveUser.Location = New System.Drawing.Point(270, 4)
        Me.chkActiveUser.Name = "chkActiveUser"
        Me.chkActiveUser.Size = New System.Drawing.Size(81, 17)
        Me.chkActiveUser.TabIndex = 1
        Me.chkActiveUser.Text = "Active User"
        Me.chkActiveUser.UseVisualStyleBackColor = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 348)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(675, 55)
        Me.objFooter.TabIndex = 8
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(463, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 128
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(566, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmUser_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(675, 403)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUser_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Users Add / Edit"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.tabcPrivilegeInformation.ResumeLayout(False)
        Me.tabpCompanySelection.ResumeLayout(False)
        Me.tabpOperationalPrivilege.ResumeLayout(False)
        Me.tabpOperationalPrivilege.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbUserDetails.ResumeLayout(False)
        Me.gbUserDetails.PerformLayout()
        Me.pnlUserInfo.ResumeLayout(False)
        Me.objpnlPersonal.ResumeLayout(False)
        Me.objpnlPersonal.PerformLayout()
        Me.objpnlPrivacy.ResumeLayout(False)
        Me.objpnlPrivacy.PerformLayout()
        Me.pnlExPassword.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents gbUserDetails As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkActiveUser As System.Windows.Forms.CheckBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents tabcPrivilegeInformation As System.Windows.Forms.TabControl
    Friend WithEvents tabpOperationalPrivilege As System.Windows.Forms.TabPage
    Friend WithEvents tvOperationalPrivilege As System.Windows.Forms.TreeView
    Friend WithEvents tabpCompanySelection As System.Windows.Forms.TabPage
    Friend WithEvents tvCompanyPrivilege As System.Windows.Forms.TreeView
    Friend WithEvents pnlUserInfo As System.Windows.Forms.Panel
    Friend WithEvents dtpExpirydate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnAddRole As eZee.Common.eZeeGradientButton
    Friend WithEvents pnlExPassword As System.Windows.Forms.Panel
    Friend WithEvents radYes As System.Windows.Forms.RadioButton
    Friend WithEvents radNo As System.Windows.Forms.RadioButton
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblExpirePassword As System.Windows.Forms.Label
    Friend WithEvents cboLanguage As System.Windows.Forms.ComboBox
    Friend WithEvents lblLanguage As System.Windows.Forms.Label
    Friend WithEvents EZeeLine2 As eZee.Common.eZeeLine
    Friend WithEvents chkRightToLeft As System.Windows.Forms.CheckBox
    Friend WithEvents EZeeLine1 As eZee.Common.eZeeLine
    Friend WithEvents lblAbilityLevel As System.Windows.Forms.Label
    Friend WithEvents cboUserRole As System.Windows.Forms.ComboBox
    Friend WithEvents EZeeLine3 As eZee.Common.eZeeLine
    Friend WithEvents txtPassword As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents txtRetypePassword As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblVerifyPassword As System.Windows.Forms.Label
    Friend WithEvents txtUser_Name As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblUserName As System.Windows.Forms.Label
    Friend WithEvents elUserDetails As eZee.Common.eZeeLine
    Friend WithEvents elPersonalDetails As eZee.Common.eZeeLine
    Friend WithEvents lblAddress2 As System.Windows.Forms.Label
    Friend WithEvents txtAddress2 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtAddress1 As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAddress1 As System.Windows.Forms.Label
    Friend WithEvents lblLastName As System.Windows.Forms.Label
    Friend WithEvents txtLastName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFirstName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblFirstname As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents txtEmail As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtPhone As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents chkIsManager As System.Windows.Forms.CheckBox
    Friend WithEvents dtpReloginDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblReloginDate As System.Windows.Forms.Label
    Friend WithEvents objpnlPersonal As System.Windows.Forms.Panel
    Friend WithEvents objpnlPrivacy As System.Windows.Forms.Panel
    Friend WithEvents TxtPrivilegeSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
End Class
