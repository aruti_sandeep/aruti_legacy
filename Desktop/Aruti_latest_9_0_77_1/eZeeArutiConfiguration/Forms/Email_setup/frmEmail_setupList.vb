﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib
Public Class frmEmail_setupList

#Region " Private Varaibles "
    Private ReadOnly mstrModuleName As String = "frmEmail_setupList"
    Private objEmail As clsEmail_setup
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try
            cboEmail.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombo As DataSet
        Try
            dsCombo = objEmail.getListForCombo("List", True)
            With cboEmail
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet
        Try
            If User._Object.Privilege._AllowToViewEmailSetup = False Then Exit Try

            dsList = objEmail.GetList("List", CInt(cboEmail.SelectedValue))

            dgvList.AutoGenerateColumns = False

            objcolhUnkId.DataPropertyName = "emailsetupunkid"
            colhEmail.DataPropertyName = "username"
            colhMailServer.DataPropertyName = "mailserverip"
            colhMailServerPort.DataPropertyName = "mailserverport"
            colhEWSUrl.DataPropertyName = "ews_url"
            colhEmailType.DataPropertyName = "email_typename"
            objcolhEmailTypeId.DataPropertyName = "email_type"

            dgvList.DataSource = dsList.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowToAddEmailSetup
            btnEdit.Enabled = User._Object.Privilege._AllowToEditEmailSetup
            btnDelete.Enabled = User._Object.Privilege._AllowToDeleteEmailSetup

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEmail_setupList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmail = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmail_setupList_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmail_setupList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Escape
                    Me.Close()
                Case Keys.Delete
                    btnDelete.PerformClick()
                Case Keys.Return
                    SendKeys.Send("{TAB}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmail_setupList_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmail_setupList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmail = New clsEmail_setup
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmail_setupList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmail_setup.SetMessages()
            objfrm._Other_ModuleNames = "clsEmail_setup"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objFrm As New frmEmail_setup
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            If objFrm.displayDialog(-1, enAction.ADD_CONTINUE) = True Then
                Call FillCombo()
                Call FillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim frm As New frmEmail_setup
        Try
            If dgvList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one transaction for further operation."), enMsgBoxStyle.Information)
                dgvList.Focus()
                Exit Sub
            End If

            If frm.displayDialog(CInt(dgvList.SelectedRows(0).Cells(objcolhUnkId.Index).Value), enAction.EDIT_ONE) Then
                Call FillList()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try

            If dgvList.SelectedRows.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select atleast one transaction for further operation."), enMsgBoxStyle.Information)
                dgvList.Focus()
                Exit Sub
            End If

            If objEmail.isUsed(CInt(dgvList.SelectedRows(0).Cells(objcolhUnkId.Index).Value)) = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, This email is already in use to send email."), enMsgBoxStyle.Information)
                dgvList.Focus()
                Exit Sub
            End If

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to Delete this Email Setup?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then

                objEmail = New clsEmail_setup

                Dim frm As New frmReasonSelection
                Dim mstrVoidReason As String = String.Empty
                frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
                If mstrVoidReason.Length <= 0 Then
                    Exit Sub
                Else
                    objEmail._Voidreason = mstrVoidReason
                End If
                frm = Nothing

                objEmail._Isvoid = True
                objEmail._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objEmail._Voiduserunkid = User._Object._Userunkid

                If objEmail.Void(CInt(dgvList.SelectedRows(0).Cells(objcolhUnkId.Index).Value)) Then
                    Call FillCombo()
                    Call FillList()
                Else
                    If objEmail._Message <> "" Then
                        eZeeMsgBox.Show(objEmail._Message, enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        Finally
            
        End Try

    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmail.SelectedValue = 0
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Datagridview's Events "

    Private Sub dgvList_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvList.DataError

    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.Name, Me.lblEmail.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhEmail.HeaderText = Language._Object.getCaption(Me.colhEmail.Name, Me.colhEmail.HeaderText)
			Me.colhMailServer.HeaderText = Language._Object.getCaption(Me.colhMailServer.Name, Me.colhMailServer.HeaderText)
			Me.colhMailServerPort.HeaderText = Language._Object.getCaption(Me.colhMailServerPort.Name, Me.colhMailServerPort.HeaderText)
			Me.colhEWSUrl.HeaderText = Language._Object.getCaption(Me.colhEWSUrl.Name, Me.colhEWSUrl.HeaderText)
			Me.colhEmailType.HeaderText = Language._Object.getCaption(Me.colhEmailType.Name, Me.colhEmailType.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select atleast one transaction for further operation.")
			Language.setMessage(mstrModuleName, 2, "Sorry, This email is already in use to send email.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to Delete this Email Setup?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class