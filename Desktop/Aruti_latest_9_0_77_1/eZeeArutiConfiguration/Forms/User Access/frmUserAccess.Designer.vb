﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserAccess
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserAccess))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.objgbSearching = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.gbUserList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.tblpnlUser = New System.Windows.Forms.TableLayoutPanel
        Me.txtSearch = New eZee.TextBox.AlphanumericTextBox
        Me.pnlUserData = New System.Windows.Forms.Panel
        Me.objchkAll = New System.Windows.Forms.CheckBox
        Me.dgvUser = New System.Windows.Forms.DataGridView
        Me.objdgcolhUserCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhUserName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhUserId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbAllocation = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchCompany = New eZee.Common.eZeeGradientButton
        Me.lblSelectCompany = New System.Windows.Forms.Label
        Me.cboCompany = New System.Windows.Forms.ComboBox
        Me.dgvUserAccess = New System.Windows.Forms.DataGridView
        Me.objdgcolhCollaps = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhUserAccess = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCompId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhYearId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhJobId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhJobLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.radDepartment = New System.Windows.Forms.RadioButton
        Me.radJobs = New System.Windows.Forms.RadioButton
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtSearchAccess = New eZee.TextBox.AlphanumericTextBox
        Me.pnlMain.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.objgbSearching.SuspendLayout()
        Me.gbUserList.SuspendLayout()
        Me.tblpnlUser.SuspendLayout()
        Me.pnlUserData.SuspendLayout()
        CType(Me.dgvUser, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbAllocation.SuspendLayout()
        CType(Me.dgvUserAccess, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.SplitContainer1)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(813, 477)
        Me.pnlMain.TabIndex = 0
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.objgbSearching)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgvUserAccess)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtSearchAccess)
        Me.SplitContainer1.Size = New System.Drawing.Size(813, 422)
        Me.SplitContainer1.SplitterDistance = 308
        Me.SplitContainer1.SplitterWidth = 2
        Me.SplitContainer1.TabIndex = 14
        '
        'objgbSearching
        '
        Me.objgbSearching.BorderColor = System.Drawing.Color.Black
        Me.objgbSearching.Checked = False
        Me.objgbSearching.CollapseAllExceptThis = False
        Me.objgbSearching.CollapsedHoverImage = Nothing
        Me.objgbSearching.CollapsedNormalImage = Nothing
        Me.objgbSearching.CollapsedPressedImage = Nothing
        Me.objgbSearching.CollapseOnLoad = False
        Me.objgbSearching.Controls.Add(Me.gbUserList)
        Me.objgbSearching.Controls.Add(Me.gbAllocation)
        Me.objgbSearching.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objgbSearching.ExpandedHoverImage = Nothing
        Me.objgbSearching.ExpandedNormalImage = Nothing
        Me.objgbSearching.ExpandedPressedImage = Nothing
        Me.objgbSearching.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbSearching.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbSearching.HeaderHeight = 25
        Me.objgbSearching.HeaderMessage = ""
        Me.objgbSearching.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.objgbSearching.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbSearching.HeightOnCollapse = 0
        Me.objgbSearching.LeftTextSpace = 0
        Me.objgbSearching.Location = New System.Drawing.Point(0, 0)
        Me.objgbSearching.Name = "objgbSearching"
        Me.objgbSearching.OpenHeight = 300
        Me.objgbSearching.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbSearching.ShowBorder = True
        Me.objgbSearching.ShowCheckBox = False
        Me.objgbSearching.ShowCollapseButton = False
        Me.objgbSearching.ShowDefaultBorderColor = True
        Me.objgbSearching.ShowDownButton = False
        Me.objgbSearching.ShowHeader = False
        Me.objgbSearching.Size = New System.Drawing.Size(308, 422)
        Me.objgbSearching.TabIndex = 13
        Me.objgbSearching.Temp = 0
        Me.objgbSearching.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbUserList
        '
        Me.gbUserList.BorderColor = System.Drawing.Color.Black
        Me.gbUserList.Checked = False
        Me.gbUserList.CollapseAllExceptThis = False
        Me.gbUserList.CollapsedHoverImage = Nothing
        Me.gbUserList.CollapsedNormalImage = Nothing
        Me.gbUserList.CollapsedPressedImage = Nothing
        Me.gbUserList.CollapseOnLoad = True
        Me.gbUserList.Controls.Add(Me.tblpnlUser)
        Me.gbUserList.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.gbUserList.ExpandedHoverImage = Nothing
        Me.gbUserList.ExpandedNormalImage = Nothing
        Me.gbUserList.ExpandedPressedImage = Nothing
        Me.gbUserList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbUserList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbUserList.HeaderHeight = 25
        Me.gbUserList.HeaderMessage = ""
        Me.gbUserList.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbUserList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbUserList.HeightOnCollapse = 0
        Me.gbUserList.LeftTextSpace = 0
        Me.gbUserList.Location = New System.Drawing.Point(0, 73)
        Me.gbUserList.Name = "gbUserList"
        Me.gbUserList.OpenHeight = 402
        Me.gbUserList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbUserList.ShowBorder = True
        Me.gbUserList.ShowCheckBox = False
        Me.gbUserList.ShowCollapseButton = False
        Me.gbUserList.ShowDefaultBorderColor = True
        Me.gbUserList.ShowDownButton = False
        Me.gbUserList.ShowHeader = True
        Me.gbUserList.Size = New System.Drawing.Size(308, 349)
        Me.gbUserList.TabIndex = 4
        Me.gbUserList.Temp = 0
        Me.gbUserList.Text = "User List"
        Me.gbUserList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tblpnlUser
        '
        Me.tblpnlUser.ColumnCount = 1
        Me.tblpnlUser.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpnlUser.Controls.Add(Me.txtSearch, 0, 0)
        Me.tblpnlUser.Controls.Add(Me.pnlUserData, 0, 1)
        Me.tblpnlUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblpnlUser.Location = New System.Drawing.Point(11, 33)
        Me.tblpnlUser.Name = "tblpnlUser"
        Me.tblpnlUser.RowCount = 2
        Me.tblpnlUser.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me.tblpnlUser.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblpnlUser.Size = New System.Drawing.Size(287, 310)
        Me.tblpnlUser.TabIndex = 16
        '
        'txtSearch
        '
        Me.txtSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtSearch.Flags = 0
        Me.txtSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearch.Location = New System.Drawing.Point(3, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(281, 21)
        Me.txtSearch.TabIndex = 107
        '
        'pnlUserData
        '
        Me.pnlUserData.Controls.Add(Me.objchkAll)
        Me.pnlUserData.Controls.Add(Me.dgvUser)
        Me.pnlUserData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlUserData.Location = New System.Drawing.Point(3, 30)
        Me.pnlUserData.Name = "pnlUserData"
        Me.pnlUserData.Size = New System.Drawing.Size(281, 277)
        Me.pnlUserData.TabIndex = 108
        '
        'objchkAll
        '
        Me.objchkAll.AutoSize = True
        Me.objchkAll.Location = New System.Drawing.Point(7, 5)
        Me.objchkAll.Name = "objchkAll"
        Me.objchkAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkAll.TabIndex = 11
        Me.objchkAll.UseVisualStyleBackColor = True
        '
        'dgvUser
        '
        Me.dgvUser.AllowUserToAddRows = False
        Me.dgvUser.AllowUserToDeleteRows = False
        Me.dgvUser.AllowUserToResizeColumns = False
        Me.dgvUser.AllowUserToResizeRows = False
        Me.dgvUser.BackgroundColor = System.Drawing.Color.White
        Me.dgvUser.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvUser.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvUser.ColumnHeadersHeight = 22
        Me.dgvUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvUser.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhUserCheck, Me.dgcolhUserName, Me.objdgcolhUserId})
        Me.dgvUser.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvUser.Location = New System.Drawing.Point(0, 0)
        Me.dgvUser.MultiSelect = False
        Me.dgvUser.Name = "dgvUser"
        Me.dgvUser.RowHeadersVisible = False
        Me.dgvUser.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvUser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvUser.Size = New System.Drawing.Size(281, 277)
        Me.dgvUser.TabIndex = 15
        '
        'objdgcolhUserCheck
        '
        Me.objdgcolhUserCheck.HeaderText = ""
        Me.objdgcolhUserCheck.Name = "objdgcolhUserCheck"
        Me.objdgcolhUserCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhUserCheck.Width = 25
        '
        'dgcolhUserName
        '
        Me.dgcolhUserName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhUserName.HeaderText = "User's"
        Me.dgcolhUserName.Name = "dgcolhUserName"
        Me.dgcolhUserName.ReadOnly = True
        Me.dgcolhUserName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhUserName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhUserId
        '
        Me.objdgcolhUserId.HeaderText = "objdgcolhUserId"
        Me.objdgcolhUserId.Name = "objdgcolhUserId"
        Me.objdgcolhUserId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhUserId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhUserId.Visible = False
        '
        'gbAllocation
        '
        Me.gbAllocation.BorderColor = System.Drawing.Color.Black
        Me.gbAllocation.Checked = False
        Me.gbAllocation.CollapseAllExceptThis = False
        Me.gbAllocation.CollapsedHoverImage = Nothing
        Me.gbAllocation.CollapsedNormalImage = Nothing
        Me.gbAllocation.CollapsedPressedImage = Nothing
        Me.gbAllocation.CollapseOnLoad = True
        Me.gbAllocation.Controls.Add(Me.objbtnSearchCompany)
        Me.gbAllocation.Controls.Add(Me.lblSelectCompany)
        Me.gbAllocation.Controls.Add(Me.cboCompany)
        Me.gbAllocation.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbAllocation.ExpandedHoverImage = Nothing
        Me.gbAllocation.ExpandedNormalImage = Nothing
        Me.gbAllocation.ExpandedPressedImage = Nothing
        Me.gbAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbAllocation.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbAllocation.HeaderHeight = 25
        Me.gbAllocation.HeaderMessage = ""
        Me.gbAllocation.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbAllocation.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbAllocation.HeightOnCollapse = 0
        Me.gbAllocation.LeftTextSpace = 0
        Me.gbAllocation.Location = New System.Drawing.Point(0, 0)
        Me.gbAllocation.Name = "gbAllocation"
        Me.gbAllocation.OpenHeight = 92
        Me.gbAllocation.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbAllocation.ShowBorder = True
        Me.gbAllocation.ShowCheckBox = False
        Me.gbAllocation.ShowCollapseButton = False
        Me.gbAllocation.ShowDefaultBorderColor = True
        Me.gbAllocation.ShowDownButton = False
        Me.gbAllocation.ShowHeader = True
        Me.gbAllocation.Size = New System.Drawing.Size(308, 67)
        Me.gbAllocation.TabIndex = 0
        Me.gbAllocation.Temp = 0
        Me.gbAllocation.Text = "Allocation"
        Me.gbAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchCompany
        '
        Me.objbtnSearchCompany.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchCompany.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchCompany.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchCompany.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchCompany.BorderSelected = False
        Me.objbtnSearchCompany.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchCompany.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Mini_Search
        Me.objbtnSearchCompany.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchCompany.Location = New System.Drawing.Point(277, 34)
        Me.objbtnSearchCompany.Name = "objbtnSearchCompany"
        Me.objbtnSearchCompany.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchCompany.TabIndex = 141
        '
        'lblSelectCompany
        '
        Me.lblSelectCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectCompany.Location = New System.Drawing.Point(8, 36)
        Me.lblSelectCompany.Name = "lblSelectCompany"
        Me.lblSelectCompany.Size = New System.Drawing.Size(58, 16)
        Me.lblSelectCompany.TabIndex = 140
        Me.lblSelectCompany.Text = "Company"
        Me.lblSelectCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.DropDownWidth = 200
        Me.cboCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(72, 34)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(199, 21)
        Me.cboCompany.TabIndex = 139
        '
        'dgvUserAccess
        '
        Me.dgvUserAccess.AllowUserToAddRows = False
        Me.dgvUserAccess.AllowUserToDeleteRows = False
        Me.dgvUserAccess.AllowUserToResizeColumns = False
        Me.dgvUserAccess.AllowUserToResizeRows = False
        Me.dgvUserAccess.BackgroundColor = System.Drawing.Color.White
        Me.dgvUserAccess.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvUserAccess.ColumnHeadersHeight = 22
        Me.dgvUserAccess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvUserAccess.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollaps, Me.objdgcolhCheck, Me.dgcolhUserAccess, Me.objdgcolhCompId, Me.objdgcolhYearId, Me.objdgcolhJobId, Me.objdgcolhJobLevel, Me.objdgcolhIsGroup})
        Me.dgvUserAccess.Location = New System.Drawing.Point(2, 24)
        Me.dgvUserAccess.Name = "dgvUserAccess"
        Me.dgvUserAccess.RowHeadersVisible = False
        Me.dgvUserAccess.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvUserAccess.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvUserAccess.Size = New System.Drawing.Size(500, 395)
        Me.dgvUserAccess.TabIndex = 12
        '
        'objdgcolhCollaps
        '
        Me.objdgcolhCollaps.HeaderText = ""
        Me.objdgcolhCollaps.Name = "objdgcolhCollaps"
        Me.objdgcolhCollaps.ReadOnly = True
        Me.objdgcolhCollaps.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCollaps.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhCollaps.Width = 25
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhUserAccess
        '
        Me.dgcolhUserAccess.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhUserAccess.HeaderText = "User Access"
        Me.dgcolhUserAccess.Name = "dgcolhUserAccess"
        Me.dgcolhUserAccess.ReadOnly = True
        Me.dgcolhUserAccess.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhCompId
        '
        Me.objdgcolhCompId.HeaderText = "objdgcolhCompId"
        Me.objdgcolhCompId.Name = "objdgcolhCompId"
        Me.objdgcolhCompId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCompId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhCompId.Visible = False
        '
        'objdgcolhYearId
        '
        Me.objdgcolhYearId.HeaderText = "objdgcolhYearId"
        Me.objdgcolhYearId.Name = "objdgcolhYearId"
        Me.objdgcolhYearId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhYearId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhYearId.Visible = False
        '
        'objdgcolhJobId
        '
        Me.objdgcolhJobId.HeaderText = "objdgcolhJobId"
        Me.objdgcolhJobId.Name = "objdgcolhJobId"
        Me.objdgcolhJobId.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhJobId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhJobId.Visible = False
        '
        'objdgcolhJobLevel
        '
        Me.objdgcolhJobLevel.HeaderText = "objdgcolhJobLevel"
        Me.objdgcolhJobLevel.Name = "objdgcolhJobLevel"
        Me.objdgcolhJobLevel.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhJobLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhJobLevel.Visible = False
        '
        'objdgcolhIsGroup
        '
        Me.objdgcolhIsGroup.HeaderText = "objdgcolhIsGroup"
        Me.objdgcolhIsGroup.Name = "objdgcolhIsGroup"
        Me.objdgcolhIsGroup.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhIsGroup.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhIsGroup.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.radDepartment)
        Me.objFooter.Controls.Add(Me.radJobs)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 422)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(813, 55)
        Me.objFooter.TabIndex = 9
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(601, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 128
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'radDepartment
        '
        Me.radDepartment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDepartment.Location = New System.Drawing.Point(12, 20)
        Me.radDepartment.Name = "radDepartment"
        Me.radDepartment.Size = New System.Drawing.Size(91, 17)
        Me.radDepartment.TabIndex = 21
        Me.radDepartment.TabStop = True
        Me.radDepartment.Text = "Department"
        Me.radDepartment.UseVisualStyleBackColor = True
        Me.radDepartment.Visible = False
        '
        'radJobs
        '
        Me.radJobs.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radJobs.Location = New System.Drawing.Point(109, 20)
        Me.radJobs.Name = "radJobs"
        Me.radJobs.Size = New System.Drawing.Size(91, 17)
        Me.radJobs.TabIndex = 24
        Me.radJobs.TabStop = True
        Me.radJobs.Text = "Job"
        Me.radJobs.UseVisualStyleBackColor = True
        Me.radJobs.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(704, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 127
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtSearchAccess
        '
        Me.txtSearchAccess.Flags = 0
        Me.txtSearchAccess.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchAccess.Location = New System.Drawing.Point(2, 2)
        Me.txtSearchAccess.Name = "txtSearchAccess"
        Me.txtSearchAccess.Size = New System.Drawing.Size(500, 21)
        Me.txtSearchAccess.TabIndex = 108
        '
        'frmUserAccess
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(813, 477)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUserAccess"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Set User Access"
        Me.pnlMain.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.objgbSearching.ResumeLayout(False)
        Me.gbUserList.ResumeLayout(False)
        Me.tblpnlUser.ResumeLayout(False)
        Me.tblpnlUser.PerformLayout()
        Me.pnlUserData.ResumeLayout(False)
        Me.pnlUserData.PerformLayout()
        CType(Me.dgvUser, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbAllocation.ResumeLayout(False)
        CType(Me.dgvUserAccess, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objchkAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvUserAccess As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCollaps As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhUserAccess As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCompId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhYearId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhJobId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhJobLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objgbSearching As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbAllocation As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbUserList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents radDepartment As System.Windows.Forms.RadioButton
    Friend WithEvents radJobs As System.Windows.Forms.RadioButton
    Friend WithEvents tblpnlUser As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents dgvUser As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents pnlUserData As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents objdgcolhUserCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhUserName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhUserId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objbtnSearchCompany As eZee.Common.eZeeGradientButton
    Friend WithEvents lblSelectCompany As System.Windows.Forms.Label
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents txtSearchAccess As eZee.TextBox.AlphanumericTextBox
End Class
