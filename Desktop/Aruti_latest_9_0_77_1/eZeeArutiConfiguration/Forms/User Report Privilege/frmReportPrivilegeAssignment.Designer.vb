﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportPrivilegeAssignment
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportPrivilegeAssignment))
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblProcessValue = New System.Windows.Forms.LinkLabel
        Me.gbRoleUser = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlUserRoleList = New System.Windows.Forms.Panel
        Me.objchkURSelectAll = New System.Windows.Forms.CheckBox
        Me.dgUserRoleList = New System.Windows.Forms.DataGridView
        Me.objdgcolhUCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhUserRole = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchUserRole = New eZee.TextBox.AlphanumericTextBox
        Me.gbReports = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlReportList = New System.Windows.Forms.Panel
        Me.objchkRptSelectAll = New System.Windows.Forms.CheckBox
        Me.dgReportList = New System.Windows.Forms.DataGridView
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhRCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhReports = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCategory = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhCategoryId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhreportunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhAUD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhreportprivilegeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchReport = New eZee.TextBox.AlphanumericTextBox
        Me.cboCompany = New System.Windows.Forms.ComboBox
        Me.lblUser = New System.Windows.Forms.Label
        Me.lblReportPrivilege = New System.Windows.Forms.Label
        Me.objHeading = New eZee.Common.eZeeHeading
        Me.radByRole = New System.Windows.Forms.RadioButton
        Me.radByUser = New System.Windows.Forms.RadioButton
        Me.objefFormFooter.SuspendLayout()
        Me.gbRoleUser.SuspendLayout()
        Me.pnlUserRoleList.SuspendLayout()
        CType(Me.dgUserRoleList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbReports.SuspendLayout()
        Me.pnlReportList.SuspendLayout()
        CType(Me.dgReportList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objHeading.SuspendLayout()
        Me.SuspendLayout()
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnSave)
        Me.objefFormFooter.Controls.Add(Me.btnClose)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 402)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(746, 50)
        Me.objefFormFooter.TabIndex = 24
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(548, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(644, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'objlblProcessValue
        '
        Me.objlblProcessValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.objlblProcessValue.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objlblProcessValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProcessValue.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objlblProcessValue.Location = New System.Drawing.Point(0, 452)
        Me.objlblProcessValue.Margin = New System.Windows.Forms.Padding(0)
        Me.objlblProcessValue.Name = "objlblProcessValue"
        Me.objlblProcessValue.Size = New System.Drawing.Size(746, 19)
        Me.objlblProcessValue.TabIndex = 2
        Me.objlblProcessValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbRoleUser
        '
        Me.gbRoleUser.BorderColor = System.Drawing.Color.Black
        Me.gbRoleUser.Checked = False
        Me.gbRoleUser.CollapseAllExceptThis = False
        Me.gbRoleUser.CollapsedHoverImage = Nothing
        Me.gbRoleUser.CollapsedNormalImage = Nothing
        Me.gbRoleUser.CollapsedPressedImage = Nothing
        Me.gbRoleUser.CollapseOnLoad = False
        Me.gbRoleUser.Controls.Add(Me.pnlUserRoleList)
        Me.gbRoleUser.Controls.Add(Me.txtSearchUserRole)
        Me.gbRoleUser.ExpandedHoverImage = Nothing
        Me.gbRoleUser.ExpandedNormalImage = Nothing
        Me.gbRoleUser.ExpandedPressedImage = Nothing
        Me.gbRoleUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbRoleUser.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbRoleUser.HeaderHeight = 25
        Me.gbRoleUser.HeaderMessage = ""
        Me.gbRoleUser.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbRoleUser.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbRoleUser.HeightOnCollapse = 0
        Me.gbRoleUser.LeftTextSpace = 0
        Me.gbRoleUser.Location = New System.Drawing.Point(0, 26)
        Me.gbRoleUser.Name = "gbRoleUser"
        Me.gbRoleUser.OpenHeight = 300
        Me.gbRoleUser.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbRoleUser.ShowBorder = True
        Me.gbRoleUser.ShowCheckBox = False
        Me.gbRoleUser.ShowCollapseButton = False
        Me.gbRoleUser.ShowDefaultBorderColor = True
        Me.gbRoleUser.ShowDownButton = False
        Me.gbRoleUser.ShowHeader = True
        Me.gbRoleUser.Size = New System.Drawing.Size(367, 373)
        Me.gbRoleUser.TabIndex = 74
        Me.gbRoleUser.Temp = 0
        Me.gbRoleUser.Text = "User(s)"
        Me.gbRoleUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlUserRoleList
        '
        Me.pnlUserRoleList.Controls.Add(Me.objchkURSelectAll)
        Me.pnlUserRoleList.Controls.Add(Me.dgUserRoleList)
        Me.pnlUserRoleList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlUserRoleList.Location = New System.Drawing.Point(2, 48)
        Me.pnlUserRoleList.Name = "pnlUserRoleList"
        Me.pnlUserRoleList.Size = New System.Drawing.Size(363, 323)
        Me.pnlUserRoleList.TabIndex = 115
        '
        'objchkURSelectAll
        '
        Me.objchkURSelectAll.AutoSize = True
        Me.objchkURSelectAll.Location = New System.Drawing.Point(6, 5)
        Me.objchkURSelectAll.Name = "objchkURSelectAll"
        Me.objchkURSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkURSelectAll.TabIndex = 113
        Me.objchkURSelectAll.UseVisualStyleBackColor = True
        '
        'dgUserRoleList
        '
        Me.dgUserRoleList.AllowUserToAddRows = False
        Me.dgUserRoleList.AllowUserToDeleteRows = False
        Me.dgUserRoleList.AllowUserToResizeColumns = False
        Me.dgUserRoleList.AllowUserToResizeRows = False
        Me.dgUserRoleList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgUserRoleList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgUserRoleList.CausesValidation = False
        Me.dgUserRoleList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.dgUserRoleList.ColumnHeadersHeight = 22
        Me.dgUserRoleList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgUserRoleList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhUCheck, Me.objdgcolhUserRole, Me.objdgcolhUnkid})
        Me.dgUserRoleList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgUserRoleList.Location = New System.Drawing.Point(0, 0)
        Me.dgUserRoleList.MultiSelect = False
        Me.dgUserRoleList.Name = "dgUserRoleList"
        Me.dgUserRoleList.RowHeadersVisible = False
        Me.dgUserRoleList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgUserRoleList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgUserRoleList.Size = New System.Drawing.Size(363, 323)
        Me.dgUserRoleList.TabIndex = 112
        '
        'objdgcolhUCheck
        '
        Me.objdgcolhUCheck.HeaderText = ""
        Me.objdgcolhUCheck.Name = "objdgcolhUCheck"
        Me.objdgcolhUCheck.Width = 25
        '
        'objdgcolhUserRole
        '
        Me.objdgcolhUserRole.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.objdgcolhUserRole.HeaderText = ""
        Me.objdgcolhUserRole.Name = "objdgcolhUserRole"
        Me.objdgcolhUserRole.ReadOnly = True
        Me.objdgcolhUserRole.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhUserRole.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhUnkid
        '
        Me.objdgcolhUnkid.HeaderText = "objdgcolhUnkid"
        Me.objdgcolhUnkid.Name = "objdgcolhUnkid"
        Me.objdgcolhUnkid.Visible = False
        '
        'txtSearchUserRole
        '
        Me.txtSearchUserRole.Flags = 0
        Me.txtSearchUserRole.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchUserRole.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchUserRole.Location = New System.Drawing.Point(2, 26)
        Me.txtSearchUserRole.Name = "txtSearchUserRole"
        Me.txtSearchUserRole.Size = New System.Drawing.Size(363, 21)
        Me.txtSearchUserRole.TabIndex = 111
        '
        'gbReports
        '
        Me.gbReports.BorderColor = System.Drawing.Color.Black
        Me.gbReports.Checked = False
        Me.gbReports.CollapseAllExceptThis = False
        Me.gbReports.CollapsedHoverImage = Nothing
        Me.gbReports.CollapsedNormalImage = Nothing
        Me.gbReports.CollapsedPressedImage = Nothing
        Me.gbReports.CollapseOnLoad = False
        Me.gbReports.Controls.Add(Me.pnlReportList)
        Me.gbReports.Controls.Add(Me.txtSearchReport)
        Me.gbReports.ExpandedHoverImage = Nothing
        Me.gbReports.ExpandedNormalImage = Nothing
        Me.gbReports.ExpandedPressedImage = Nothing
        Me.gbReports.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbReports.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbReports.HeaderHeight = 25
        Me.gbReports.HeaderMessage = ""
        Me.gbReports.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbReports.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbReports.HeightOnCollapse = 0
        Me.gbReports.LeftTextSpace = 0
        Me.gbReports.Location = New System.Drawing.Point(371, 26)
        Me.gbReports.Name = "gbReports"
        Me.gbReports.OpenHeight = 300
        Me.gbReports.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbReports.ShowBorder = True
        Me.gbReports.ShowCheckBox = False
        Me.gbReports.ShowCollapseButton = False
        Me.gbReports.ShowDefaultBorderColor = True
        Me.gbReports.ShowDownButton = False
        Me.gbReports.ShowHeader = True
        Me.gbReports.Size = New System.Drawing.Size(374, 373)
        Me.gbReports.TabIndex = 75
        Me.gbReports.Temp = 0
        Me.gbReports.Text = "Report List"
        Me.gbReports.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlReportList
        '
        Me.pnlReportList.Controls.Add(Me.objchkRptSelectAll)
        Me.pnlReportList.Controls.Add(Me.dgReportList)
        Me.pnlReportList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlReportList.Location = New System.Drawing.Point(3, 48)
        Me.pnlReportList.Name = "pnlReportList"
        Me.pnlReportList.Size = New System.Drawing.Size(368, 323)
        Me.pnlReportList.TabIndex = 117
        '
        'objchkRptSelectAll
        '
        Me.objchkRptSelectAll.AutoSize = True
        Me.objchkRptSelectAll.Location = New System.Drawing.Point(32, 5)
        Me.objchkRptSelectAll.Name = "objchkRptSelectAll"
        Me.objchkRptSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkRptSelectAll.TabIndex = 115
        Me.objchkRptSelectAll.UseVisualStyleBackColor = True
        Me.objchkRptSelectAll.Visible = False
        '
        'dgReportList
        '
        Me.dgReportList.AllowUserToAddRows = False
        Me.dgReportList.AllowUserToDeleteRows = False
        Me.dgReportList.AllowUserToResizeColumns = False
        Me.dgReportList.AllowUserToResizeRows = False
        Me.dgReportList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgReportList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgReportList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgReportList.ColumnHeadersHeight = 22
        Me.dgReportList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgReportList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollapse, Me.objdgcolhRCheck, Me.dgcolhReports, Me.objdgcolhCategory, Me.objdgcolhCategoryId, Me.objdgcolhreportunkid, Me.objdgcolhAUD, Me.objdgcolhreportprivilegeunkid})
        Me.dgReportList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgReportList.Location = New System.Drawing.Point(0, 0)
        Me.dgReportList.MultiSelect = False
        Me.dgReportList.Name = "dgReportList"
        Me.dgReportList.RowHeadersVisible = False
        Me.dgReportList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgReportList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgReportList.Size = New System.Drawing.Size(368, 323)
        Me.dgReportList.TabIndex = 113
        '
        'objdgcolhCollapse
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objdgcolhCollapse.DefaultCellStyle = DataGridViewCellStyle4
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.ReadOnly = True
        Me.objdgcolhCollapse.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhCollapse.Width = 25
        '
        'objdgcolhRCheck
        '
        Me.objdgcolhRCheck.HeaderText = ""
        Me.objdgcolhRCheck.Name = "objdgcolhRCheck"
        Me.objdgcolhRCheck.Width = 25
        '
        'dgcolhReports
        '
        Me.dgcolhReports.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhReports.HeaderText = "Reports"
        Me.dgcolhReports.Name = "dgcolhReports"
        Me.dgcolhReports.ReadOnly = True
        Me.dgcolhReports.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhReports.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhCategory
        '
        Me.objdgcolhCategory.HeaderText = "Category"
        Me.objdgcolhCategory.Name = "objdgcolhCategory"
        Me.objdgcolhCategory.Visible = False
        Me.objdgcolhCategory.Width = 5
        '
        'objdgcolhCategoryId
        '
        Me.objdgcolhCategoryId.HeaderText = "CategoryId"
        Me.objdgcolhCategoryId.Name = "objdgcolhCategoryId"
        Me.objdgcolhCategoryId.Visible = False
        Me.objdgcolhCategoryId.Width = 5
        '
        'objdgcolhreportunkid
        '
        Me.objdgcolhreportunkid.HeaderText = "ReportUnkid"
        Me.objdgcolhreportunkid.Name = "objdgcolhreportunkid"
        Me.objdgcolhreportunkid.Visible = False
        '
        'objdgcolhAUD
        '
        Me.objdgcolhAUD.HeaderText = "objdgcolhAUD"
        Me.objdgcolhAUD.Name = "objdgcolhAUD"
        Me.objdgcolhAUD.Visible = False
        '
        'objdgcolhreportprivilegeunkid
        '
        Me.objdgcolhreportprivilegeunkid.HeaderText = "objdgcolhreportprivilegeunkid"
        Me.objdgcolhreportprivilegeunkid.Name = "objdgcolhreportprivilegeunkid"
        Me.objdgcolhreportprivilegeunkid.Visible = False
        '
        'txtSearchReport
        '
        Me.txtSearchReport.Flags = 0
        Me.txtSearchReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchReport.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93)}
        Me.txtSearchReport.Location = New System.Drawing.Point(2, 26)
        Me.txtSearchReport.Name = "txtSearchReport"
        Me.txtSearchReport.Size = New System.Drawing.Size(370, 21)
        Me.txtSearchReport.TabIndex = 109
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(485, 2)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(259, 21)
        Me.cboCompany.TabIndex = 22
        '
        'lblUser
        '
        Me.lblUser.BackColor = System.Drawing.Color.Transparent
        Me.lblUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.Location = New System.Drawing.Point(388, 4)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(91, 17)
        Me.lblUser.TabIndex = 21
        Me.lblUser.Text = "Select Company"
        Me.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblReportPrivilege
        '
        Me.lblReportPrivilege.BackColor = System.Drawing.Color.Transparent
        Me.lblReportPrivilege.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportPrivilege.Location = New System.Drawing.Point(5, 5)
        Me.lblReportPrivilege.Name = "lblReportPrivilege"
        Me.lblReportPrivilege.Size = New System.Drawing.Size(377, 15)
        Me.lblReportPrivilege.TabIndex = 3
        Me.lblReportPrivilege.Text = "Assign Report Privileges"
        Me.lblReportPrivilege.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objHeading
        '
        Me.objHeading.BorderColor = System.Drawing.Color.Black
        Me.objHeading.Controls.Add(Me.cboCompany)
        Me.objHeading.Controls.Add(Me.lblReportPrivilege)
        Me.objHeading.Controls.Add(Me.lblUser)
        Me.objHeading.Controls.Add(Me.radByRole)
        Me.objHeading.Controls.Add(Me.radByUser)
        Me.objHeading.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objHeading.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objHeading.Location = New System.Drawing.Point(0, 0)
        Me.objHeading.Name = "objHeading"
        Me.objHeading.Padding = New System.Windows.Forms.Padding(8, 0, 0, 0)
        Me.objHeading.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objHeading.ShowDefaultBorderColor = True
        Me.objHeading.Size = New System.Drawing.Size(746, 25)
        Me.objHeading.TabIndex = 2
        '
        'radByRole
        '
        Me.radByRole.BackColor = System.Drawing.Color.Transparent
        Me.radByRole.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.radByRole.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radByRole.Location = New System.Drawing.Point(218, 4)
        Me.radByRole.Name = "radByRole"
        Me.radByRole.Size = New System.Drawing.Size(72, 17)
        Me.radByRole.TabIndex = 5
        Me.radByRole.Text = "By Role"
        Me.radByRole.UseVisualStyleBackColor = False
        Me.radByRole.Visible = False
        '
        'radByUser
        '
        Me.radByUser.BackColor = System.Drawing.Color.Transparent
        Me.radByUser.Enabled = False
        Me.radByUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.radByUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radByUser.Location = New System.Drawing.Point(296, 4)
        Me.radByUser.Name = "radByUser"
        Me.radByUser.Size = New System.Drawing.Size(72, 17)
        Me.radByUser.TabIndex = 6
        Me.radByUser.Text = "By User"
        Me.radByUser.UseVisualStyleBackColor = False
        Me.radByUser.Visible = False
        '
        'frmReportPrivilegeAssignment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(746, 471)
        Me.Controls.Add(Me.objefFormFooter)
        Me.Controls.Add(Me.objlblProcessValue)
        Me.Controls.Add(Me.objHeading)
        Me.Controls.Add(Me.gbReports)
        Me.Controls.Add(Me.gbRoleUser)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReportPrivilegeAssignment"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Report Privilege Assignment"
        Me.objefFormFooter.ResumeLayout(False)
        Me.gbRoleUser.ResumeLayout(False)
        Me.gbRoleUser.PerformLayout()
        Me.pnlUserRoleList.ResumeLayout(False)
        Me.pnlUserRoleList.PerformLayout()
        CType(Me.dgUserRoleList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbReports.ResumeLayout(False)
        Me.gbReports.PerformLayout()
        Me.pnlReportList.ResumeLayout(False)
        Me.pnlReportList.PerformLayout()
        CType(Me.dgReportList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objHeading.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbRoleUser As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objchkURSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgUserRoleList As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearchUserRole As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents gbReports As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dgReportList As System.Windows.Forms.DataGridView
    Friend WithEvents txtSearchReport As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents lblReportPrivilege As System.Windows.Forms.Label
    Friend WithEvents objHeading As eZee.Common.eZeeHeading
    Friend WithEvents radByUser As System.Windows.Forms.RadioButton
    Friend WithEvents radByRole As System.Windows.Forms.RadioButton
    Friend WithEvents objchkRptSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents pnlUserRoleList As System.Windows.Forms.Panel
    Friend WithEvents pnlReportList As System.Windows.Forms.Panel
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents objdgcolhUCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhUserRole As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objlblProcessValue As System.Windows.Forms.LinkLabel
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhRCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhReports As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCategory As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhCategoryId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhreportunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhAUD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhreportprivilegeunkid As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
