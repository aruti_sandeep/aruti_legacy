﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMDI
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMDI))
        Me.StatusStrip = New System.Windows.Forms.StatusStrip
        Me.lblUserName = New System.Windows.Forms.ToolStripStatusLabel
        Me.objbtnUserName = New System.Windows.Forms.ToolStripStatusLabel
        Me.objlblSep3 = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblWorkingDate = New System.Windows.Forms.ToolStripStatusLabel
        Me.objbtnWorkingDate = New System.Windows.Forms.ToolStripStatusLabel
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.tblMDILayout = New System.Windows.Forms.TableLayoutPanel
        Me.objspc1 = New System.Windows.Forms.SplitContainer
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.pnlMenuItems = New System.Windows.Forms.Panel
        Me.pnlSummary = New System.Windows.Forms.Panel
        Me.fpnlAuditTrails = New System.Windows.Forms.FlowLayoutPanel
        Me.btnApplicationEventLog = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnUserAuthenticationLog = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnUserAttempts = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnLogView = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnUserOptionLog = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSystemErrorLog = New eZee.Common.eZeeLightButton(Me.components)
        Me.fpnlConfiguration = New System.Windows.Forms.FlowLayoutPanel
        Me.btnGroupMaster = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCompany = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnUserCreation = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnUserRole = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnAbilityLevel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnBackUp = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnRestore = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCountry = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnState = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCity = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnZipcode = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEmailSetup = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnVoidReason = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReminderType = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnPasswordOption = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnUnlockUser = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnUserLog = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnDeviceList = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEnableAutoUpdate = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnForceLogout = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCloneData = New eZee.Common.eZeeLightButton(Me.components)
        Me.mnuMDIConfiguration = New System.Windows.Forms.MenuStrip
        Me.mnuConfiguration = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuOption = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuAuditTrail = New System.Windows.Forms.ToolStripMenuItem
        Me.ReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ActiveEmployeesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUserDetailLogReport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem
        Me.tmrReminder = New System.Windows.Forms.Timer(Me.components)
        Me.mnuTroubleshoot = New System.Windows.Forms.ToolStripMenuItem
        Me.StatusStrip.SuspendLayout()
        Me.pnlMainInfo.SuspendLayout()
        Me.tblMDILayout.SuspendLayout()
        Me.objspc1.Panel1.SuspendLayout()
        Me.objspc1.Panel2.SuspendLayout()
        Me.objspc1.SuspendLayout()
        Me.pnlSummary.SuspendLayout()
        Me.fpnlAuditTrails.SuspendLayout()
        Me.fpnlConfiguration.SuspendLayout()
        Me.mnuMDIConfiguration.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblUserName, Me.objbtnUserName, Me.objlblSep3, Me.lblWorkingDate, Me.objbtnWorkingDate})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 431)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(782, 22)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'lblUserName
        '
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(71, 17)
        Me.lblUserName.Text = "User Name :"
        '
        'objbtnUserName
        '
        Me.objbtnUserName.Name = "objbtnUserName"
        Me.objbtnUserName.Size = New System.Drawing.Size(62, 17)
        Me.objbtnUserName.Text = "UserName"
        '
        'objlblSep3
        '
        Me.objlblSep3.Name = "objlblSep3"
        Me.objlblSep3.Size = New System.Drawing.Size(10, 17)
        Me.objlblSep3.Text = "|"
        '
        'lblWorkingDate
        '
        Me.lblWorkingDate.Name = "lblWorkingDate"
        Me.lblWorkingDate.Size = New System.Drawing.Size(85, 17)
        Me.lblWorkingDate.Text = "Working Date :"
        '
        'objbtnWorkingDate
        '
        Me.objbtnWorkingDate.Name = "objbtnWorkingDate"
        Me.objbtnWorkingDate.Size = New System.Drawing.Size(79, 17)
        Me.objbtnWorkingDate.Text = "Working Date"
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.tblMDILayout)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 24)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(782, 407)
        Me.pnlMainInfo.TabIndex = 9
        '
        'tblMDILayout
        '
        Me.tblMDILayout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.tblMDILayout.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset
        Me.tblMDILayout.ColumnCount = 2
        Me.tblMDILayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 191.0!))
        Me.tblMDILayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblMDILayout.Controls.Add(Me.objspc1, 0, 0)
        Me.tblMDILayout.Controls.Add(Me.pnlSummary, 1, 0)
        Me.tblMDILayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblMDILayout.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tblMDILayout.Location = New System.Drawing.Point(0, 0)
        Me.tblMDILayout.Name = "tblMDILayout"
        Me.tblMDILayout.RowCount = 1
        Me.tblMDILayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblMDILayout.Size = New System.Drawing.Size(782, 407)
        Me.tblMDILayout.TabIndex = 2
        '
        'objspc1
        '
        Me.objspc1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.objspc1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objspc1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.objspc1.IsSplitterFixed = True
        Me.objspc1.Location = New System.Drawing.Point(5, 5)
        Me.objspc1.Name = "objspc1"
        Me.objspc1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'objspc1.Panel1
        '
        Me.objspc1.Panel1.Controls.Add(Me.objlblCaption)
        '
        'objspc1.Panel2
        '
        Me.objspc1.Panel2.Controls.Add(Me.pnlMenuItems)
        Me.objspc1.Size = New System.Drawing.Size(185, 397)
        Me.objspc1.SplitterDistance = 43
        Me.objspc1.SplitterWidth = 2
        Me.objspc1.TabIndex = 18
        '
        'objlblCaption
        '
        Me.objlblCaption.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(0, 0)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(183, 41)
        Me.objlblCaption.TabIndex = 18
        Me.objlblCaption.Text = "objlblCaption"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlMenuItems
        '
        Me.pnlMenuItems.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMenuItems.Location = New System.Drawing.Point(0, 0)
        Me.pnlMenuItems.Name = "pnlMenuItems"
        Me.pnlMenuItems.Size = New System.Drawing.Size(183, 350)
        Me.pnlMenuItems.TabIndex = 0
        '
        'pnlSummary
        '
        Me.pnlSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSummary.Controls.Add(Me.fpnlAuditTrails)
        Me.pnlSummary.Controls.Add(Me.fpnlConfiguration)
        Me.pnlSummary.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlSummary.Location = New System.Drawing.Point(198, 5)
        Me.pnlSummary.Name = "pnlSummary"
        Me.pnlSummary.Size = New System.Drawing.Size(579, 397)
        Me.pnlSummary.TabIndex = 1
        '
        'fpnlAuditTrails
        '
        Me.fpnlAuditTrails.AutoScroll = True
        Me.fpnlAuditTrails.Controls.Add(Me.btnApplicationEventLog)
        Me.fpnlAuditTrails.Controls.Add(Me.btnUserAuthenticationLog)
        Me.fpnlAuditTrails.Controls.Add(Me.btnUserAttempts)
        Me.fpnlAuditTrails.Controls.Add(Me.btnLogView)
        Me.fpnlAuditTrails.Controls.Add(Me.btnUserOptionLog)
        Me.fpnlAuditTrails.Controls.Add(Me.btnSystemErrorLog)
        Me.fpnlAuditTrails.Location = New System.Drawing.Point(8, 53)
        Me.fpnlAuditTrails.Name = "fpnlAuditTrails"
        Me.fpnlAuditTrails.Size = New System.Drawing.Size(189, 42)
        Me.fpnlAuditTrails.TabIndex = 25
        Me.fpnlAuditTrails.Visible = False
        '
        'btnApplicationEventLog
        '
        Me.btnApplicationEventLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApplicationEventLog.BackColor = System.Drawing.Color.Transparent
        Me.btnApplicationEventLog.BackgroundImage = CType(resources.GetObject("btnApplicationEventLog.BackgroundImage"), System.Drawing.Image)
        Me.btnApplicationEventLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnApplicationEventLog.BorderColor = System.Drawing.Color.Empty
        Me.btnApplicationEventLog.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnApplicationEventLog.FlatAppearance.BorderSize = 0
        Me.btnApplicationEventLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnApplicationEventLog.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApplicationEventLog.ForeColor = System.Drawing.Color.Black
        Me.btnApplicationEventLog.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnApplicationEventLog.GradientForeColor = System.Drawing.Color.Black
        Me.btnApplicationEventLog.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApplicationEventLog.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnApplicationEventLog.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Application_Event_Log
        Me.btnApplicationEventLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnApplicationEventLog.Location = New System.Drawing.Point(3, 3)
        Me.btnApplicationEventLog.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnApplicationEventLog.Name = "btnApplicationEventLog"
        Me.btnApplicationEventLog.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnApplicationEventLog.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnApplicationEventLog.Size = New System.Drawing.Size(166, 35)
        Me.btnApplicationEventLog.TabIndex = 29
        Me.btnApplicationEventLog.Text = "Application Event Log"
        Me.btnApplicationEventLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnApplicationEventLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnApplicationEventLog.UseVisualStyleBackColor = False
        '
        'btnUserAuthenticationLog
        '
        Me.btnUserAuthenticationLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUserAuthenticationLog.BackColor = System.Drawing.Color.Transparent
        Me.btnUserAuthenticationLog.BackgroundImage = CType(resources.GetObject("btnUserAuthenticationLog.BackgroundImage"), System.Drawing.Image)
        Me.btnUserAuthenticationLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUserAuthenticationLog.BorderColor = System.Drawing.Color.Empty
        Me.btnUserAuthenticationLog.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnUserAuthenticationLog.FlatAppearance.BorderSize = 0
        Me.btnUserAuthenticationLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUserAuthenticationLog.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUserAuthenticationLog.ForeColor = System.Drawing.Color.Black
        Me.btnUserAuthenticationLog.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUserAuthenticationLog.GradientForeColor = System.Drawing.Color.Black
        Me.btnUserAuthenticationLog.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUserAuthenticationLog.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUserAuthenticationLog.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.User_Authentication_Log
        Me.btnUserAuthenticationLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUserAuthenticationLog.Location = New System.Drawing.Point(3, 44)
        Me.btnUserAuthenticationLog.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnUserAuthenticationLog.Name = "btnUserAuthenticationLog"
        Me.btnUserAuthenticationLog.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUserAuthenticationLog.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUserAuthenticationLog.Size = New System.Drawing.Size(166, 35)
        Me.btnUserAuthenticationLog.TabIndex = 31
        Me.btnUserAuthenticationLog.Text = "User Authentication Log"
        Me.btnUserAuthenticationLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUserAuthenticationLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUserAuthenticationLog.UseVisualStyleBackColor = False
        '
        'btnUserAttempts
        '
        Me.btnUserAttempts.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUserAttempts.BackColor = System.Drawing.Color.Transparent
        Me.btnUserAttempts.BackgroundImage = CType(resources.GetObject("btnUserAttempts.BackgroundImage"), System.Drawing.Image)
        Me.btnUserAttempts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUserAttempts.BorderColor = System.Drawing.Color.Empty
        Me.btnUserAttempts.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnUserAttempts.FlatAppearance.BorderSize = 0
        Me.btnUserAttempts.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUserAttempts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUserAttempts.ForeColor = System.Drawing.Color.Black
        Me.btnUserAttempts.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUserAttempts.GradientForeColor = System.Drawing.Color.Black
        Me.btnUserAttempts.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUserAttempts.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUserAttempts.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.User_Attempts_Log
        Me.btnUserAttempts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUserAttempts.Location = New System.Drawing.Point(3, 85)
        Me.btnUserAttempts.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnUserAttempts.Name = "btnUserAttempts"
        Me.btnUserAttempts.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUserAttempts.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUserAttempts.Size = New System.Drawing.Size(166, 35)
        Me.btnUserAttempts.TabIndex = 30
        Me.btnUserAttempts.Text = "User Attempts Log"
        Me.btnUserAttempts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUserAttempts.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUserAttempts.UseVisualStyleBackColor = False
        '
        'btnLogView
        '
        Me.btnLogView.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLogView.BackColor = System.Drawing.Color.Transparent
        Me.btnLogView.BackgroundImage = CType(resources.GetObject("btnLogView.BackgroundImage"), System.Drawing.Image)
        Me.btnLogView.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnLogView.BorderColor = System.Drawing.Color.Empty
        Me.btnLogView.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnLogView.FlatAppearance.BorderSize = 0
        Me.btnLogView.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLogView.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLogView.ForeColor = System.Drawing.Color.Black
        Me.btnLogView.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnLogView.GradientForeColor = System.Drawing.Color.Black
        Me.btnLogView.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLogView.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnLogView.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Log_View
        Me.btnLogView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLogView.Location = New System.Drawing.Point(3, 126)
        Me.btnLogView.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnLogView.Name = "btnLogView"
        Me.btnLogView.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnLogView.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnLogView.Size = New System.Drawing.Size(166, 35)
        Me.btnLogView.TabIndex = 32
        Me.btnLogView.Text = "Log View"
        Me.btnLogView.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLogView.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnLogView.UseVisualStyleBackColor = False
        '
        'btnUserOptionLog
        '
        Me.btnUserOptionLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUserOptionLog.BackColor = System.Drawing.Color.Transparent
        Me.btnUserOptionLog.BackgroundImage = CType(resources.GetObject("btnUserOptionLog.BackgroundImage"), System.Drawing.Image)
        Me.btnUserOptionLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUserOptionLog.BorderColor = System.Drawing.Color.Empty
        Me.btnUserOptionLog.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnUserOptionLog.FlatAppearance.BorderSize = 0
        Me.btnUserOptionLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUserOptionLog.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUserOptionLog.ForeColor = System.Drawing.Color.Black
        Me.btnUserOptionLog.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUserOptionLog.GradientForeColor = System.Drawing.Color.Black
        Me.btnUserOptionLog.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUserOptionLog.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUserOptionLog.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Log_View
        Me.btnUserOptionLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUserOptionLog.Location = New System.Drawing.Point(3, 167)
        Me.btnUserOptionLog.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnUserOptionLog.Name = "btnUserOptionLog"
        Me.btnUserOptionLog.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUserOptionLog.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUserOptionLog.Size = New System.Drawing.Size(166, 35)
        Me.btnUserOptionLog.TabIndex = 33
        Me.btnUserOptionLog.Text = "Configuration Option Log"
        Me.btnUserOptionLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUserOptionLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUserOptionLog.UseVisualStyleBackColor = False
        '
        'btnSystemErrorLog
        '
        Me.btnSystemErrorLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSystemErrorLog.BackColor = System.Drawing.Color.Transparent
        Me.btnSystemErrorLog.BackgroundImage = CType(resources.GetObject("btnSystemErrorLog.BackgroundImage"), System.Drawing.Image)
        Me.btnSystemErrorLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSystemErrorLog.BorderColor = System.Drawing.Color.Empty
        Me.btnSystemErrorLog.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnSystemErrorLog.FlatAppearance.BorderSize = 0
        Me.btnSystemErrorLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSystemErrorLog.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSystemErrorLog.ForeColor = System.Drawing.Color.Black
        Me.btnSystemErrorLog.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSystemErrorLog.GradientForeColor = System.Drawing.Color.Black
        Me.btnSystemErrorLog.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSystemErrorLog.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSystemErrorLog.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Application_Event_Log
        Me.btnSystemErrorLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSystemErrorLog.Location = New System.Drawing.Point(3, 208)
        Me.btnSystemErrorLog.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnSystemErrorLog.Name = "btnSystemErrorLog"
        Me.btnSystemErrorLog.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSystemErrorLog.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSystemErrorLog.Size = New System.Drawing.Size(166, 35)
        Me.btnSystemErrorLog.TabIndex = 34
        Me.btnSystemErrorLog.Text = "&System Error Log"
        Me.btnSystemErrorLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSystemErrorLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSystemErrorLog.UseVisualStyleBackColor = False
        '
        'fpnlConfiguration
        '
        Me.fpnlConfiguration.AutoScroll = True
        Me.fpnlConfiguration.Controls.Add(Me.btnGroupMaster)
        Me.fpnlConfiguration.Controls.Add(Me.btnCompany)
        Me.fpnlConfiguration.Controls.Add(Me.btnUserCreation)
        Me.fpnlConfiguration.Controls.Add(Me.btnUserRole)
        Me.fpnlConfiguration.Controls.Add(Me.btnAbilityLevel)
        Me.fpnlConfiguration.Controls.Add(Me.btnBackUp)
        Me.fpnlConfiguration.Controls.Add(Me.btnRestore)
        Me.fpnlConfiguration.Controls.Add(Me.btnCountry)
        Me.fpnlConfiguration.Controls.Add(Me.btnState)
        Me.fpnlConfiguration.Controls.Add(Me.btnCity)
        Me.fpnlConfiguration.Controls.Add(Me.btnZipcode)
        Me.fpnlConfiguration.Controls.Add(Me.btnEmailSetup)
        Me.fpnlConfiguration.Controls.Add(Me.btnVoidReason)
        Me.fpnlConfiguration.Controls.Add(Me.btnReminderType)
        Me.fpnlConfiguration.Controls.Add(Me.btnPasswordOption)
        Me.fpnlConfiguration.Controls.Add(Me.btnUnlockUser)
        Me.fpnlConfiguration.Controls.Add(Me.btnUserLog)
        Me.fpnlConfiguration.Controls.Add(Me.btnDeviceList)
        Me.fpnlConfiguration.Controls.Add(Me.btnEnableAutoUpdate)
        Me.fpnlConfiguration.Controls.Add(Me.btnForceLogout)
        Me.fpnlConfiguration.Controls.Add(Me.btnCloneData)
        Me.fpnlConfiguration.Location = New System.Drawing.Point(8, 6)
        Me.fpnlConfiguration.Name = "fpnlConfiguration"
        Me.fpnlConfiguration.Size = New System.Drawing.Size(189, 44)
        Me.fpnlConfiguration.TabIndex = 9
        Me.fpnlConfiguration.Visible = False
        '
        'btnGroupMaster
        '
        Me.btnGroupMaster.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGroupMaster.BackColor = System.Drawing.Color.Transparent
        Me.btnGroupMaster.BackgroundImage = CType(resources.GetObject("btnGroupMaster.BackgroundImage"), System.Drawing.Image)
        Me.btnGroupMaster.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGroupMaster.BorderColor = System.Drawing.Color.Empty
        Me.btnGroupMaster.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnGroupMaster.FlatAppearance.BorderSize = 0
        Me.btnGroupMaster.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGroupMaster.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGroupMaster.ForeColor = System.Drawing.Color.Black
        Me.btnGroupMaster.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnGroupMaster.GradientForeColor = System.Drawing.Color.Black
        Me.btnGroupMaster.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGroupMaster.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnGroupMaster.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Company_group_master
        Me.btnGroupMaster.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGroupMaster.Location = New System.Drawing.Point(3, 3)
        Me.btnGroupMaster.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnGroupMaster.Name = "btnGroupMaster"
        Me.btnGroupMaster.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnGroupMaster.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnGroupMaster.Size = New System.Drawing.Size(166, 35)
        Me.btnGroupMaster.TabIndex = 18
        Me.btnGroupMaster.Text = "Company &Group"
        Me.btnGroupMaster.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGroupMaster.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnGroupMaster.UseVisualStyleBackColor = False
        '
        'btnCompany
        '
        Me.btnCompany.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCompany.BackColor = System.Drawing.Color.Transparent
        Me.btnCompany.BackgroundImage = CType(resources.GetObject("btnCompany.BackgroundImage"), System.Drawing.Image)
        Me.btnCompany.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCompany.BorderColor = System.Drawing.Color.Empty
        Me.btnCompany.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnCompany.FlatAppearance.BorderSize = 0
        Me.btnCompany.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCompany.ForeColor = System.Drawing.Color.Black
        Me.btnCompany.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCompany.GradientForeColor = System.Drawing.Color.Black
        Me.btnCompany.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCompany.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCompany.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.company_creation
        Me.btnCompany.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCompany.Location = New System.Drawing.Point(3, 44)
        Me.btnCompany.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnCompany.Name = "btnCompany"
        Me.btnCompany.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCompany.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCompany.Size = New System.Drawing.Size(166, 35)
        Me.btnCompany.TabIndex = 7
        Me.btnCompany.Text = "&Company Creation"
        Me.btnCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCompany.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCompany.UseVisualStyleBackColor = False
        '
        'btnUserCreation
        '
        Me.btnUserCreation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUserCreation.BackColor = System.Drawing.Color.Transparent
        Me.btnUserCreation.BackgroundImage = CType(resources.GetObject("btnUserCreation.BackgroundImage"), System.Drawing.Image)
        Me.btnUserCreation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUserCreation.BorderColor = System.Drawing.Color.Empty
        Me.btnUserCreation.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnUserCreation.FlatAppearance.BorderSize = 0
        Me.btnUserCreation.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUserCreation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUserCreation.ForeColor = System.Drawing.Color.Black
        Me.btnUserCreation.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUserCreation.GradientForeColor = System.Drawing.Color.Black
        Me.btnUserCreation.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUserCreation.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUserCreation.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.user_creation
        Me.btnUserCreation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUserCreation.Location = New System.Drawing.Point(3, 85)
        Me.btnUserCreation.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnUserCreation.Name = "btnUserCreation"
        Me.btnUserCreation.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUserCreation.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUserCreation.Size = New System.Drawing.Size(166, 35)
        Me.btnUserCreation.TabIndex = 8
        Me.btnUserCreation.Text = "&User Creation"
        Me.btnUserCreation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUserCreation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUserCreation.UseVisualStyleBackColor = False
        '
        'btnUserRole
        '
        Me.btnUserRole.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUserRole.BackColor = System.Drawing.Color.Transparent
        Me.btnUserRole.BackgroundImage = CType(resources.GetObject("btnUserRole.BackgroundImage"), System.Drawing.Image)
        Me.btnUserRole.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUserRole.BorderColor = System.Drawing.Color.Empty
        Me.btnUserRole.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnUserRole.FlatAppearance.BorderSize = 0
        Me.btnUserRole.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUserRole.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUserRole.ForeColor = System.Drawing.Color.Black
        Me.btnUserRole.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUserRole.GradientForeColor = System.Drawing.Color.Black
        Me.btnUserRole.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUserRole.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUserRole.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.user_role
        Me.btnUserRole.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUserRole.Location = New System.Drawing.Point(3, 126)
        Me.btnUserRole.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnUserRole.Name = "btnUserRole"
        Me.btnUserRole.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUserRole.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUserRole.Size = New System.Drawing.Size(166, 35)
        Me.btnUserRole.TabIndex = 9
        Me.btnUserRole.Text = "User &Role"
        Me.btnUserRole.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUserRole.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUserRole.UseVisualStyleBackColor = False
        '
        'btnAbilityLevel
        '
        Me.btnAbilityLevel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAbilityLevel.BackColor = System.Drawing.Color.Transparent
        Me.btnAbilityLevel.BackgroundImage = CType(resources.GetObject("btnAbilityLevel.BackgroundImage"), System.Drawing.Image)
        Me.btnAbilityLevel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAbilityLevel.BorderColor = System.Drawing.Color.Empty
        Me.btnAbilityLevel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnAbilityLevel.FlatAppearance.BorderSize = 0
        Me.btnAbilityLevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAbilityLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAbilityLevel.ForeColor = System.Drawing.Color.Black
        Me.btnAbilityLevel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAbilityLevel.GradientForeColor = System.Drawing.Color.Black
        Me.btnAbilityLevel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAbilityLevel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAbilityLevel.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.abilitylevel
        Me.btnAbilityLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAbilityLevel.Location = New System.Drawing.Point(3, 167)
        Me.btnAbilityLevel.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnAbilityLevel.Name = "btnAbilityLevel"
        Me.btnAbilityLevel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAbilityLevel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAbilityLevel.Size = New System.Drawing.Size(166, 35)
        Me.btnAbilityLevel.TabIndex = 10
        Me.btnAbilityLevel.Text = "&Ability Level"
        Me.btnAbilityLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAbilityLevel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAbilityLevel.UseVisualStyleBackColor = False
        Me.btnAbilityLevel.Visible = False
        '
        'btnBackUp
        '
        Me.btnBackUp.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBackUp.BackColor = System.Drawing.Color.Transparent
        Me.btnBackUp.BackgroundImage = CType(resources.GetObject("btnBackUp.BackgroundImage"), System.Drawing.Image)
        Me.btnBackUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBackUp.BorderColor = System.Drawing.Color.Empty
        Me.btnBackUp.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnBackUp.FlatAppearance.BorderSize = 0
        Me.btnBackUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBackUp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBackUp.ForeColor = System.Drawing.Color.Black
        Me.btnBackUp.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnBackUp.GradientForeColor = System.Drawing.Color.Black
        Me.btnBackUp.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBackUp.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnBackUp.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Backup
        Me.btnBackUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBackUp.Location = New System.Drawing.Point(3, 208)
        Me.btnBackUp.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnBackUp.Name = "btnBackUp"
        Me.btnBackUp.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBackUp.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnBackUp.Size = New System.Drawing.Size(166, 35)
        Me.btnBackUp.TabIndex = 11
        Me.btnBackUp.Text = "&Back Up"
        Me.btnBackUp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBackUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnBackUp.UseVisualStyleBackColor = False
        '
        'btnRestore
        '
        Me.btnRestore.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRestore.BackColor = System.Drawing.Color.Transparent
        Me.btnRestore.BackgroundImage = CType(resources.GetObject("btnRestore.BackgroundImage"), System.Drawing.Image)
        Me.btnRestore.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRestore.BorderColor = System.Drawing.Color.Empty
        Me.btnRestore.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnRestore.FlatAppearance.BorderSize = 0
        Me.btnRestore.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRestore.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRestore.ForeColor = System.Drawing.Color.Black
        Me.btnRestore.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnRestore.GradientForeColor = System.Drawing.Color.Black
        Me.btnRestore.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRestore.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnRestore.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.restore
        Me.btnRestore.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRestore.Location = New System.Drawing.Point(3, 249)
        Me.btnRestore.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnRestore.Name = "btnRestore"
        Me.btnRestore.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnRestore.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnRestore.Size = New System.Drawing.Size(166, 35)
        Me.btnRestore.TabIndex = 12
        Me.btnRestore.Text = "Re&store"
        Me.btnRestore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRestore.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnRestore.UseVisualStyleBackColor = False
        '
        'btnCountry
        '
        Me.btnCountry.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCountry.BackColor = System.Drawing.Color.Transparent
        Me.btnCountry.BackgroundImage = CType(resources.GetObject("btnCountry.BackgroundImage"), System.Drawing.Image)
        Me.btnCountry.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCountry.BorderColor = System.Drawing.Color.Empty
        Me.btnCountry.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnCountry.FlatAppearance.BorderSize = 0
        Me.btnCountry.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCountry.ForeColor = System.Drawing.Color.Black
        Me.btnCountry.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCountry.GradientForeColor = System.Drawing.Color.Black
        Me.btnCountry.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCountry.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCountry.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.state
        Me.btnCountry.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCountry.Location = New System.Drawing.Point(3, 290)
        Me.btnCountry.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnCountry.Name = "btnCountry"
        Me.btnCountry.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCountry.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCountry.Size = New System.Drawing.Size(166, 35)
        Me.btnCountry.TabIndex = 136
        Me.btnCountry.Text = "Coun&try"
        Me.btnCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCountry.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCountry.UseVisualStyleBackColor = False
        '
        'btnState
        '
        Me.btnState.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnState.BackColor = System.Drawing.Color.Transparent
        Me.btnState.BackgroundImage = CType(resources.GetObject("btnState.BackgroundImage"), System.Drawing.Image)
        Me.btnState.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnState.BorderColor = System.Drawing.Color.Empty
        Me.btnState.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnState.FlatAppearance.BorderSize = 0
        Me.btnState.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnState.ForeColor = System.Drawing.Color.Black
        Me.btnState.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnState.GradientForeColor = System.Drawing.Color.Black
        Me.btnState.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnState.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnState.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.state
        Me.btnState.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnState.Location = New System.Drawing.Point(3, 331)
        Me.btnState.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnState.Name = "btnState"
        Me.btnState.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnState.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnState.Size = New System.Drawing.Size(166, 35)
        Me.btnState.TabIndex = 13
        Me.btnState.Text = "&State"
        Me.btnState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnState.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnState.UseVisualStyleBackColor = False
        '
        'btnCity
        '
        Me.btnCity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCity.BackColor = System.Drawing.Color.Transparent
        Me.btnCity.BackgroundImage = CType(resources.GetObject("btnCity.BackgroundImage"), System.Drawing.Image)
        Me.btnCity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCity.BorderColor = System.Drawing.Color.Empty
        Me.btnCity.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnCity.FlatAppearance.BorderSize = 0
        Me.btnCity.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCity.ForeColor = System.Drawing.Color.Black
        Me.btnCity.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCity.GradientForeColor = System.Drawing.Color.Black
        Me.btnCity.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCity.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCity.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.City
        Me.btnCity.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCity.Location = New System.Drawing.Point(3, 372)
        Me.btnCity.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnCity.Name = "btnCity"
        Me.btnCity.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCity.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCity.Size = New System.Drawing.Size(166, 35)
        Me.btnCity.TabIndex = 14
        Me.btnCity.Text = "C&ity"
        Me.btnCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCity.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCity.UseVisualStyleBackColor = False
        '
        'btnZipcode
        '
        Me.btnZipcode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnZipcode.BackColor = System.Drawing.Color.Transparent
        Me.btnZipcode.BackgroundImage = CType(resources.GetObject("btnZipcode.BackgroundImage"), System.Drawing.Image)
        Me.btnZipcode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnZipcode.BorderColor = System.Drawing.Color.Empty
        Me.btnZipcode.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnZipcode.FlatAppearance.BorderSize = 0
        Me.btnZipcode.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnZipcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnZipcode.ForeColor = System.Drawing.Color.Black
        Me.btnZipcode.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnZipcode.GradientForeColor = System.Drawing.Color.Black
        Me.btnZipcode.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnZipcode.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnZipcode.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.zipcode
        Me.btnZipcode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnZipcode.Location = New System.Drawing.Point(3, 413)
        Me.btnZipcode.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnZipcode.Name = "btnZipcode"
        Me.btnZipcode.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnZipcode.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnZipcode.Size = New System.Drawing.Size(166, 35)
        Me.btnZipcode.TabIndex = 15
        Me.btnZipcode.Text = "&Zipcode"
        Me.btnZipcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnZipcode.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnZipcode.UseVisualStyleBackColor = False
        '
        'btnEmailSetup
        '
        Me.btnEmailSetup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEmailSetup.BackColor = System.Drawing.Color.Transparent
        Me.btnEmailSetup.BackgroundImage = CType(resources.GetObject("btnEmailSetup.BackgroundImage"), System.Drawing.Image)
        Me.btnEmailSetup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEmailSetup.BorderColor = System.Drawing.Color.Empty
        Me.btnEmailSetup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnEmailSetup.FlatAppearance.BorderSize = 0
        Me.btnEmailSetup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEmailSetup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmailSetup.ForeColor = System.Drawing.Color.Black
        Me.btnEmailSetup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEmailSetup.GradientForeColor = System.Drawing.Color.Black
        Me.btnEmailSetup.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailSetup.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailSetup.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Mailbox
        Me.btnEmailSetup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEmailSetup.Location = New System.Drawing.Point(3, 454)
        Me.btnEmailSetup.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnEmailSetup.Name = "btnEmailSetup"
        Me.btnEmailSetup.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEmailSetup.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEmailSetup.Size = New System.Drawing.Size(166, 35)
        Me.btnEmailSetup.TabIndex = 135
        Me.btnEmailSetup.Text = "E&mail Setup"
        Me.btnEmailSetup.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEmailSetup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEmailSetup.UseVisualStyleBackColor = False
        '
        'btnVoidReason
        '
        Me.btnVoidReason.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVoidReason.BackColor = System.Drawing.Color.Transparent
        Me.btnVoidReason.BackgroundImage = CType(resources.GetObject("btnVoidReason.BackgroundImage"), System.Drawing.Image)
        Me.btnVoidReason.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVoidReason.BorderColor = System.Drawing.Color.Empty
        Me.btnVoidReason.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnVoidReason.FlatAppearance.BorderSize = 0
        Me.btnVoidReason.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVoidReason.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVoidReason.ForeColor = System.Drawing.Color.Black
        Me.btnVoidReason.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVoidReason.GradientForeColor = System.Drawing.Color.Black
        Me.btnVoidReason.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoidReason.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnVoidReason.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.voidreason
        Me.btnVoidReason.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVoidReason.Location = New System.Drawing.Point(3, 495)
        Me.btnVoidReason.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnVoidReason.Name = "btnVoidReason"
        Me.btnVoidReason.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoidReason.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnVoidReason.Size = New System.Drawing.Size(166, 35)
        Me.btnVoidReason.TabIndex = 16
        Me.btnVoidReason.Text = "&Reason"
        Me.btnVoidReason.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVoidReason.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnVoidReason.UseVisualStyleBackColor = False
        '
        'btnReminderType
        '
        Me.btnReminderType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReminderType.BackColor = System.Drawing.Color.Transparent
        Me.btnReminderType.BackgroundImage = CType(resources.GetObject("btnReminderType.BackgroundImage"), System.Drawing.Image)
        Me.btnReminderType.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReminderType.BorderColor = System.Drawing.Color.Empty
        Me.btnReminderType.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnReminderType.FlatAppearance.BorderSize = 0
        Me.btnReminderType.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReminderType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReminderType.ForeColor = System.Drawing.Color.Black
        Me.btnReminderType.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReminderType.GradientForeColor = System.Drawing.Color.Black
        Me.btnReminderType.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReminderType.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReminderType.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Reminder
        Me.btnReminderType.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReminderType.Location = New System.Drawing.Point(3, 536)
        Me.btnReminderType.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnReminderType.Name = "btnReminderType"
        Me.btnReminderType.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReminderType.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReminderType.Size = New System.Drawing.Size(166, 35)
        Me.btnReminderType.TabIndex = 17
        Me.btnReminderType.Text = "Reminder &Type"
        Me.btnReminderType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReminderType.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnReminderType.UseVisualStyleBackColor = False
        '
        'btnPasswordOption
        '
        Me.btnPasswordOption.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPasswordOption.BackColor = System.Drawing.Color.Transparent
        Me.btnPasswordOption.BackgroundImage = CType(resources.GetObject("btnPasswordOption.BackgroundImage"), System.Drawing.Image)
        Me.btnPasswordOption.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPasswordOption.BorderColor = System.Drawing.Color.Empty
        Me.btnPasswordOption.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnPasswordOption.FlatAppearance.BorderSize = 0
        Me.btnPasswordOption.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPasswordOption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPasswordOption.ForeColor = System.Drawing.Color.Black
        Me.btnPasswordOption.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPasswordOption.GradientForeColor = System.Drawing.Color.Black
        Me.btnPasswordOption.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPasswordOption.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPasswordOption.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.PasswordSettings
        Me.btnPasswordOption.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPasswordOption.Location = New System.Drawing.Point(3, 577)
        Me.btnPasswordOption.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnPasswordOption.Name = "btnPasswordOption"
        Me.btnPasswordOption.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPasswordOption.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPasswordOption.Size = New System.Drawing.Size(166, 35)
        Me.btnPasswordOption.TabIndex = 18
        Me.btnPasswordOption.Text = "&Other Options"
        Me.btnPasswordOption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPasswordOption.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPasswordOption.UseVisualStyleBackColor = False
        '
        'btnUnlockUser
        '
        Me.btnUnlockUser.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUnlockUser.BackColor = System.Drawing.Color.Transparent
        Me.btnUnlockUser.BackgroundImage = CType(resources.GetObject("btnUnlockUser.BackgroundImage"), System.Drawing.Image)
        Me.btnUnlockUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUnlockUser.BorderColor = System.Drawing.Color.Empty
        Me.btnUnlockUser.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnUnlockUser.FlatAppearance.BorderSize = 0
        Me.btnUnlockUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUnlockUser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnlockUser.ForeColor = System.Drawing.Color.Black
        Me.btnUnlockUser.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUnlockUser.GradientForeColor = System.Drawing.Color.Black
        Me.btnUnlockUser.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnlockUser.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUnlockUser.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.unlockuser
        Me.btnUnlockUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUnlockUser.Location = New System.Drawing.Point(3, 618)
        Me.btnUnlockUser.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnUnlockUser.Name = "btnUnlockUser"
        Me.btnUnlockUser.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUnlockUser.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUnlockUser.Size = New System.Drawing.Size(166, 35)
        Me.btnUnlockUser.TabIndex = 19
        Me.btnUnlockUser.Text = "&Unlock User"
        Me.btnUnlockUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUnlockUser.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUnlockUser.UseVisualStyleBackColor = False
        '
        'btnUserLog
        '
        Me.btnUserLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUserLog.BackColor = System.Drawing.Color.Transparent
        Me.btnUserLog.BackgroundImage = CType(resources.GetObject("btnUserLog.BackgroundImage"), System.Drawing.Image)
        Me.btnUserLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnUserLog.BorderColor = System.Drawing.Color.Empty
        Me.btnUserLog.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnUserLog.FlatAppearance.BorderSize = 0
        Me.btnUserLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnUserLog.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUserLog.ForeColor = System.Drawing.Color.Black
        Me.btnUserLog.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnUserLog.GradientForeColor = System.Drawing.Color.Black
        Me.btnUserLog.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUserLog.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnUserLog.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.UserLog
        Me.btnUserLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUserLog.Location = New System.Drawing.Point(3, 659)
        Me.btnUserLog.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnUserLog.Name = "btnUserLog"
        Me.btnUserLog.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnUserLog.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnUserLog.Size = New System.Drawing.Size(166, 35)
        Me.btnUserLog.TabIndex = 18
        Me.btnUserLog.Text = "&User Log"
        Me.btnUserLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUserLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnUserLog.UseVisualStyleBackColor = False
        Me.btnUserLog.Visible = False
        '
        'btnDeviceList
        '
        Me.btnDeviceList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeviceList.BackColor = System.Drawing.Color.Transparent
        Me.btnDeviceList.BackgroundImage = CType(resources.GetObject("btnDeviceList.BackgroundImage"), System.Drawing.Image)
        Me.btnDeviceList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDeviceList.BorderColor = System.Drawing.Color.Empty
        Me.btnDeviceList.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnDeviceList.FlatAppearance.BorderSize = 0
        Me.btnDeviceList.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeviceList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeviceList.ForeColor = System.Drawing.Color.Black
        Me.btnDeviceList.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDeviceList.GradientForeColor = System.Drawing.Color.Black
        Me.btnDeviceList.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeviceList.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDeviceList.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.Login
        Me.btnDeviceList.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeviceList.Location = New System.Drawing.Point(3, 700)
        Me.btnDeviceList.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnDeviceList.Name = "btnDeviceList"
        Me.btnDeviceList.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDeviceList.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDeviceList.Size = New System.Drawing.Size(166, 35)
        Me.btnDeviceList.TabIndex = 20
        Me.btnDeviceList.Text = "&Device Settings"
        Me.btnDeviceList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeviceList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDeviceList.UseVisualStyleBackColor = False
        '
        'btnEnableAutoUpdate
        '
        Me.btnEnableAutoUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEnableAutoUpdate.BackColor = System.Drawing.Color.White
        Me.btnEnableAutoUpdate.BackgroundImage = CType(resources.GetObject("btnEnableAutoUpdate.BackgroundImage"), System.Drawing.Image)
        Me.btnEnableAutoUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEnableAutoUpdate.BorderColor = System.Drawing.Color.Empty
        Me.btnEnableAutoUpdate.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEnableAutoUpdate.FlatAppearance.BorderSize = 0
        Me.btnEnableAutoUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEnableAutoUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnableAutoUpdate.ForeColor = System.Drawing.Color.Black
        Me.btnEnableAutoUpdate.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEnableAutoUpdate.GradientForeColor = System.Drawing.Color.Black
        Me.btnEnableAutoUpdate.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEnableAutoUpdate.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEnableAutoUpdate.Location = New System.Drawing.Point(3, 741)
        Me.btnEnableAutoUpdate.Name = "btnEnableAutoUpdate"
        Me.btnEnableAutoUpdate.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEnableAutoUpdate.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEnableAutoUpdate.Size = New System.Drawing.Size(166, 35)
        Me.btnEnableAutoUpdate.TabIndex = 131
        Me.btnEnableAutoUpdate.Text = "&Enable Auto Update"
        Me.btnEnableAutoUpdate.UseVisualStyleBackColor = True
        Me.btnEnableAutoUpdate.Visible = False
        '
        'btnForceLogout
        '
        Me.btnForceLogout.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnForceLogout.BackColor = System.Drawing.Color.Transparent
        Me.btnForceLogout.BackgroundImage = CType(resources.GetObject("btnForceLogout.BackgroundImage"), System.Drawing.Image)
        Me.btnForceLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnForceLogout.BorderColor = System.Drawing.Color.Empty
        Me.btnForceLogout.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnForceLogout.FlatAppearance.BorderSize = 0
        Me.btnForceLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnForceLogout.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnForceLogout.ForeColor = System.Drawing.Color.Black
        Me.btnForceLogout.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnForceLogout.GradientForeColor = System.Drawing.Color.Black
        Me.btnForceLogout.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnForceLogout.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnForceLogout.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.UserLog
        Me.btnForceLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnForceLogout.Location = New System.Drawing.Point(3, 782)
        Me.btnForceLogout.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnForceLogout.Name = "btnForceLogout"
        Me.btnForceLogout.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnForceLogout.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnForceLogout.Size = New System.Drawing.Size(166, 35)
        Me.btnForceLogout.TabIndex = 134
        Me.btnForceLogout.Text = "&Force Logout"
        Me.btnForceLogout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnForceLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnForceLogout.UseVisualStyleBackColor = False
        '
        'btnCloneData
        '
        Me.btnCloneData.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCloneData.BackColor = System.Drawing.Color.Transparent
        Me.btnCloneData.BackgroundImage = CType(resources.GetObject("btnCloneData.BackgroundImage"), System.Drawing.Image)
        Me.btnCloneData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCloneData.BorderColor = System.Drawing.Color.Empty
        Me.btnCloneData.ButtonType = eZee.Common.eZeeLightButton.enButtonType.CheckButton
        Me.btnCloneData.FlatAppearance.BorderSize = 0
        Me.btnCloneData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCloneData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCloneData.ForeColor = System.Drawing.Color.Black
        Me.btnCloneData.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCloneData.GradientForeColor = System.Drawing.Color.Black
        Me.btnCloneData.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCloneData.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCloneData.Image = Global.eZeeArutiConfiguration.My.Resources.Resources.autoimport
        Me.btnCloneData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCloneData.Location = New System.Drawing.Point(3, 823)
        Me.btnCloneData.MinimumSize = New System.Drawing.Size(150, 35)
        Me.btnCloneData.Name = "btnCloneData"
        Me.btnCloneData.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCloneData.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCloneData.Size = New System.Drawing.Size(166, 35)
        Me.btnCloneData.TabIndex = 135
        Me.btnCloneData.Text = "Clone &Data"
        Me.btnCloneData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCloneData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCloneData.UseVisualStyleBackColor = False
        '
        'mnuMDIConfiguration
        '
        Me.mnuMDIConfiguration.BackColor = System.Drawing.SystemColors.Control
        Me.mnuMDIConfiguration.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuConfiguration, Me.mnuOption, Me.mnuAuditTrail, Me.ReportToolStripMenuItem, Me.mnuHelp, Me.mnuTroubleshoot, Me.mnuExit})
        Me.mnuMDIConfiguration.Location = New System.Drawing.Point(0, 0)
        Me.mnuMDIConfiguration.Name = "mnuMDIConfiguration"
        Me.mnuMDIConfiguration.Size = New System.Drawing.Size(782, 24)
        Me.mnuMDIConfiguration.TabIndex = 10
        Me.mnuMDIConfiguration.Text = "MenuStrip1"
        '
        'mnuConfiguration
        '
        Me.mnuConfiguration.Name = "mnuConfiguration"
        Me.mnuConfiguration.Size = New System.Drawing.Size(93, 20)
        Me.mnuConfiguration.Text = "&Configuration"
        '
        'mnuOption
        '
        Me.mnuOption.Name = "mnuOption"
        Me.mnuOption.Size = New System.Drawing.Size(56, 20)
        Me.mnuOption.Text = "&Option"
        '
        'mnuAuditTrail
        '
        Me.mnuAuditTrail.Name = "mnuAuditTrail"
        Me.mnuAuditTrail.Size = New System.Drawing.Size(72, 20)
        Me.mnuAuditTrail.Text = "Audit Trail"
        '
        'ReportToolStripMenuItem
        '
        Me.ReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ActiveEmployeesToolStripMenuItem, Me.mnuUserDetailLogReport})
        Me.ReportToolStripMenuItem.Name = "ReportToolStripMenuItem"
        Me.ReportToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.ReportToolStripMenuItem.Text = "Report"
        '
        'ActiveEmployeesToolStripMenuItem
        '
        Me.ActiveEmployeesToolStripMenuItem.Name = "ActiveEmployeesToolStripMenuItem"
        Me.ActiveEmployeesToolStripMenuItem.Size = New System.Drawing.Size(167, 22)
        Me.ActiveEmployeesToolStripMenuItem.Text = "Active Employees"
        '
        'mnuUserDetailLogReport
        '
        Me.mnuUserDetailLogReport.Name = "mnuUserDetailLogReport"
        Me.mnuUserDetailLogReport.Size = New System.Drawing.Size(167, 22)
        Me.mnuUserDetailLogReport.Tag = "mnuUserDetailLogReport"
        Me.mnuUserDetailLogReport.Text = "User Details Log"
        '
        'mnuHelp
        '
        Me.mnuHelp.Name = "mnuHelp"
        Me.mnuHelp.Size = New System.Drawing.Size(44, 20)
        Me.mnuHelp.Text = "Help"
        '
        'mnuExit
        '
        Me.mnuExit.Name = "mnuExit"
        Me.mnuExit.Size = New System.Drawing.Size(38, 20)
        Me.mnuExit.Text = "&Exit"
        '
        'tmrReminder
        '
        Me.tmrReminder.Enabled = True
        Me.tmrReminder.Interval = 1000
        '
        'mnuTroubleshoot
        '
        Me.mnuTroubleshoot.Name = "mnuTroubleshoot"
        Me.mnuTroubleshoot.Size = New System.Drawing.Size(88, 20)
        Me.mnuTroubleshoot.Text = "Troubleshoot"
        '
        'frmMDI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(782, 453)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Controls.Add(Me.StatusStrip)
        Me.Controls.Add(Me.mnuMDIConfiguration)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IsMdiContainer = True
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.mnuMDIConfiguration
        Me.Name = "frmMDI"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aruti Configuration"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.pnlMainInfo.ResumeLayout(False)
        Me.tblMDILayout.ResumeLayout(False)
        Me.objspc1.Panel1.ResumeLayout(False)
        Me.objspc1.Panel2.ResumeLayout(False)
        Me.objspc1.ResumeLayout(False)
        Me.pnlSummary.ResumeLayout(False)
        Me.fpnlAuditTrails.ResumeLayout(False)
        Me.fpnlConfiguration.ResumeLayout(False)
        Me.mnuMDIConfiguration.ResumeLayout(False)
        Me.mnuMDIConfiguration.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents mnuMDIConfiguration As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuConfiguration As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOption As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tblMDILayout As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents objspc1 As System.Windows.Forms.SplitContainer
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents pnlMenuItems As System.Windows.Forms.Panel
    Friend WithEvents pnlSummary As System.Windows.Forms.Panel
    Friend WithEvents fpnlConfiguration As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCompany As eZee.Common.eZeeLightButton
    Friend WithEvents btnUserCreation As eZee.Common.eZeeLightButton
    Friend WithEvents btnUserRole As eZee.Common.eZeeLightButton
    Friend WithEvents btnAbilityLevel As eZee.Common.eZeeLightButton
    Friend WithEvents btnBackUp As eZee.Common.eZeeLightButton
    Friend WithEvents btnRestore As eZee.Common.eZeeLightButton
    Friend WithEvents btnState As eZee.Common.eZeeLightButton
    Friend WithEvents btnCity As eZee.Common.eZeeLightButton
    Friend WithEvents btnZipcode As eZee.Common.eZeeLightButton
    Friend WithEvents btnVoidReason As eZee.Common.eZeeLightButton
    Friend WithEvents btnReminderType As eZee.Common.eZeeLightButton
    Friend WithEvents tmrReminder As System.Windows.Forms.Timer
    Friend WithEvents btnPasswordOption As eZee.Common.eZeeLightButton
    Friend WithEvents btnUnlockUser As eZee.Common.eZeeLightButton
    Friend WithEvents btnUserLog As eZee.Common.eZeeLightButton
    Friend WithEvents btnGroupMaster As eZee.Common.eZeeLightButton
    Friend WithEvents btnDeviceList As eZee.Common.eZeeLightButton
    Friend WithEvents btnEnableAutoUpdate As eZee.Common.eZeeLightButton
    Friend WithEvents btnForceLogout As eZee.Common.eZeeLightButton
    Friend WithEvents mnuAuditTrail As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblUserName As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objbtnUserName As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objlblSep3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblWorkingDate As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents objbtnWorkingDate As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents fpnlAuditTrails As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnApplicationEventLog As eZee.Common.eZeeLightButton
    Friend WithEvents btnUserAuthenticationLog As eZee.Common.eZeeLightButton
    Friend WithEvents btnUserAttempts As eZee.Common.eZeeLightButton
    Friend WithEvents btnLogView As eZee.Common.eZeeLightButton
    Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnUserOptionLog As eZee.Common.eZeeLightButton
    Friend WithEvents ReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ActiveEmployeesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUserDetailLogReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnCloneData As eZee.Common.eZeeLightButton
    Friend WithEvents btnSystemErrorLog As eZee.Common.eZeeLightButton
    Friend WithEvents btnCountry As eZee.Common.eZeeLightButton
    Friend WithEvents btnEmailSetup As eZee.Common.eZeeLightButton
    Friend WithEvents mnuTroubleshoot As System.Windows.Forms.ToolStripMenuItem
End Class
