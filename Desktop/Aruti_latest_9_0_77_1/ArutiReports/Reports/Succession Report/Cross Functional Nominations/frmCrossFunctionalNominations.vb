﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region
Public Class frmCrossFunctionalNominations
#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCrossFunctionalNominations"
    Private objCrossFunctionalNominations As clsCrossFunctionalNominations

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty
    Private dvEmployee As DataView
    Private mstrSearchEmpText As String = ""
    Dim strEmployeeId As String = ""
#End Region

#Region " Constructor "
    Public Sub New()
        objCrossFunctionalNominations = New clsCrossFunctionalNominations(User._Object._Languageunkid, Company._Object._Companyunkid)
        objCrossFunctionalNominations.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmCrossFunctionalNominations_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objCrossFunctionalNominations = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCrossFunctionalNominations_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCrossFunctionalNominations_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Call Language.setLanguage(Me.Name)
            'OtherSettings()
            Call FillCombo()
            Call ResetValue()
            objbtnSearchEmployee.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCrossFunctionalNominations_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCrossFunctionalNominations_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCrossFunctionalNominations_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmCrossFunctionalNominations_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    SendKeys.Send("{TAB}")
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmCrossFunctionalNominations_KeyPress", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsCrossFunctionalNominations.SetMessages()
            objfrm._Other_ModuleNames = "clsCrossFunctionalNominations"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objJob As New clsJobs

        Try
            dsCombo = objJob.getComboList("keyJob", True, , , , , , , , , , , , , , , , True)
            With cboJob
                .ValueMember = "Jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("keyJob")
                .SelectedValue = 0
            End With



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            dsCombo.Dispose() : objJob = Nothing

        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboJob.SelectedIndex = 0
            chkIncludeInactiveEmp.Checked = False
            If cboEmployee.DataSource IsNot Nothing AndAlso cboEmployee.Items.Count > 0 Then cboEmployee.SelectedIndex = 0
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            objCrossFunctionalNominations.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objCrossFunctionalNominations.SetDefaultValue()

            objCrossFunctionalNominations._JobUnkid = cboJob.SelectedValue
            objCrossFunctionalNominations._JobName = cboJob.Text
            objCrossFunctionalNominations._EmployeeUnkid = cboEmployee.SelectedValue
            objCrossFunctionalNominations._EmployeeName = cboEmployee.Text
            objCrossFunctionalNominations._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            objCrossFunctionalNominations._ViewByIds = mstrStringIds
            objCrossFunctionalNominations._ViewIndex = mintViewIdx
            objCrossFunctionalNominations._ViewByName = mstrStringName
            objCrossFunctionalNominations._Analysis_Fields = mstrAnalysis_Fields
            objCrossFunctionalNominations._Analysis_Join = mstrAnalysis_Join
            objCrossFunctionalNominations._Analysis_OrderBy = mstrAnalysis_OrderBy
            objCrossFunctionalNominations._Report_GroupName = mstrReport_GroupName
            objCrossFunctionalNominations._Advance_Filter = mstrAdvanceFilter

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try

            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
                Next
                dvEmployee.ToTable.AcceptChanges()
            End If

            Dim drRow As DataRow() = dvEmployee.Table.Select("IsChecked = 1")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If SetFilter() = False Then Exit Sub


            objCrossFunctionalNominations.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                                                User._Object._Userunkid, _
                                                                FinancialYear._Object._YearUnkid, _
                                                                Company._Object._Companyunkid, _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString), _
                                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                                True, ConfigParameter._Object._ExportReportPath, _
                                                                ConfigParameter._Object._OpenAfterExport, _
                                                                0, enExportAction.None)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchJob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchJob.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboJob.DataSource
            frm.ValueMember = cboJob.ValueMember
            frm.DisplayMember = cboJob.DisplayMember
            If frm.DisplayDialog Then
                cboJob.SelectedValue = frm.SelectedValue
                cboJob.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchJob_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
#End Region

#Region "Combobox Events"
    Private Sub cboJob_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJob.SelectedIndexChanged
        Dim objsucpipeline_master As New clssucpipeline_master
        Dim objStage As New clssucstages_master
        Dim dsList As New DataSet
        Dim dsCombo As New DataSet
        Try

            dsList = objStage.GetList("list")
            Dim intmaxStage As Integer = -1
            Dim intminStage As Integer = -1
            Dim intmaxToLastStage As Integer = -1
            Dim mintStageUnkid As Integer = -1
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                objStage.Get_Min_Max_FlowOrder(intminStage, intmaxStage, intmaxToLastStage)
                Dim drRow As DataRow = dsList.Tables(0).Select("floworder = " & intmaxStage & "")(0)
                mintStageUnkid = CInt(drRow.Item("stageunkid"))
            End If

            Dim FilterType As enScreeningFilterType = enScreeningFilterType.ALL

            dsCombo = objsucpipeline_master.GetOtherStagesDetailList(FinancialYear._Object._DatabaseName, _
                                                                      User._Object._Userunkid, _
                                                                      FinancialYear._Object._YearUnkid, _
                                                                      Company._Object._Companyunkid, _
                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                      ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                      ConfigParameter._Object._IsIncludeInactiveEmp, CInt(cboJob.SelectedValue), _
                                                                      mintStageUnkid, "", intmaxStage, -1, FilterType, "Emp", False, mstrAdvanceFilter, True)

            Dim RemoveColumns As String() = {"isapproved", "age", "exyr", "TotalScreener", "stageunkid", "processmstunkid", "empImage", _
                                             "empbaseimage", "isdisapproved", "IsDone", "nominatedjobunkid", "nominatedjob", "color", _
                                             "ismatch", "ismanual", "potentialSuccessiontranunkid"}

            For Each colName As String In RemoveColumns
                dsCombo.Tables(0).Columns.Remove(colName)
            Next

            Dim dt As DataTable = dsCombo.Tables(0).DefaultView.ToTable(True, dsCombo.Tables(0).Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray())

            Dim r As DataRow = dt.NewRow

            r.Item(1) = "Select" ' 
            r.Item(2) = "0"

            dt.Rows.InsertAt(r, 0)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dt
                .SelectedValue = 0
            End With
            objbtnSearchEmployee.Enabled = True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboJob_SelectedIndexChanged", mstrModuleName)
        Finally
            dsCombo.Dispose()
            objsucpipeline_master = Nothing
            objStage = Nothing
        End Try
    End Sub
#End Region

#Region " Other Control's Events "

    'Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
    '    Dim frm As New frmViewAnalysis
    '    Try
    '        frm.displayDialog()
    '        mstrStringIds = frm._ReportBy_Ids
    '        mstrStringName = frm._ReportBy_Name
    '        mintViewIdx = frm._ViewIndex
    '        mstrAnalysis_Fields = frm._Analysis_Fields
    '        mstrAnalysis_Join = frm._Analysis_Join
    '        mstrAnalysis_OrderBy = frm._Analysis_OrderBy
    '        mstrReport_GroupName = frm._Report_GroupName
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
    '    Finally
    '        frm = Nothing
    '    End Try
    'End Sub

    Private Sub lnkAdvanceFilter_Clicked(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.LinkClicked
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select Calendar.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

   
End Class