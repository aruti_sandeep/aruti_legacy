#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region


Public Class frmDailyAttendance_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDailyAttendance_Report"
    Private objattendance As clsDailyAttendance_Report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (22-Dec-2022) -- Start
    '(A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
    Private mstrGroupName As String = ""
    'Pinkal (22-Dec-2022) -- End
#End Region

#Region " Constructor "
    Public Sub New()
        objattendance = New clsDailyAttendance_Report(User._Object._Languageunkid, Company._Object._Companyunkid)
        objattendance.SetDefaultValue()

        'Pinkal (22-Dec-2022) -- Start
        ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
        Dim objGrpMstr As New clsGroup_Master
        objGrpMstr._Groupunkid = 1
        mstrGroupName = objGrpMstr._Groupname
        objGrpMstr = Nothing
        'Pinkal (22-Dec-2022) -- End
        InitializeComponent()
    End Sub
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    dsList = objEmp.GetEmployeeList("Employee", True, False)
            'Else
            '    dsList = objEmp.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'End If

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                           dtpFromDate.Value.Date, _
                                           dtpToDate.Value.Date, _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Employee")
                .SelectedValue = 0
            End With
            objEmp = Nothing


            'Pinkal (22-Dec-2022) -- Start
            ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns
            If mstrGroupName.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                Dim objMData As New clsMasterData
                dsList = objMData.GetEAllocation_Notification("List")
                Dim drow As DataRow = dsList.Tables("List").NewRow
                drow.Item("Id") = 0
                drow.Item("Name") = Language.getMessage(mstrModuleName, 7, "Select Allocation for Report")
                dsList.Tables("List").Rows.InsertAt(drow, 0)

                Dim iRowIdx As Integer = -1
                iRowIdx = dsList.Tables("List").Rows.IndexOf(dsList.Tables("List").Select("Id = " & enAllocation.DEPARTMENT)(0))
                dsList.Tables("List").Rows.RemoveAt(iRowIdx)

                With cboReportColumn
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = 0
                End With
                cboReportColumn.Visible = True
                gbShowColumns.Visible = True
            Else
                cboReportColumn.Visible = False
                gbShowColumns.Visible = False
            End If
            'Pinkal (22-Dec-2022) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmployee.SelectedValue = 0
            chkInactiveemp.Checked = False
            chkIgnoreEmptyColumns.Checked = False
            mstrAdvanceFilter = ""
            mstrStringIds = ""
            mintViewIdx = -1
            mstrStringName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            'Pinkal (22-Dec-2022) -- Start
            '(A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
            If mstrGroupName.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                GetValue()
            End If
            'Pinkal (22-Dec-2022) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    'Pinkal (22-Dec-2022) -- Start
    '(A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Daily_Attendance_Checking_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                cboReportColumn.SelectedValue = dsList.Tables(0).Rows(0)("transactionheadid")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Pinkal (22-Dec-2022) -- End

#End Region

#Region " Form's Events "

    Private Sub frmDailyAttendance_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objattendance = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyAttendance_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDailyAttendance_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            OtherSettings()
            eZeeHeader.Title = objattendance._ReportName
            eZeeHeader.Message = objattendance._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDailyAttendance_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDailyAttendance_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmDailyAttendance_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDailyAttendance_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmDailyAttendance_Report_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsDailyAttendance_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsDailyAttendance_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try

            If ConfigParameter._Object._ExportReportPath = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths."), enMsgBoxStyle.Information)
                Exit Sub
            End If

            'Pinkal (22-Dec-2022) -- Start
            '(A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
            If mstrGroupName.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                If cboReportColumn.SelectedIndex <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Location is mandatory information. Please select location to continue."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'Pinkal (22-Dec-2022) -- End

            objattendance.SetDefaultValue()

            objattendance._EmployeeId = cboEmployee.SelectedValue
            objattendance._EmployeeName = cboEmployee.Text
            objattendance._FromDate = dtpFromDate.Value.Date
            objattendance._ToDate = dtpToDate.Value.Date
            objattendance._IncludeInactive = chkInactiveemp.Checked
            objattendance._Advance_Filter = mstrAdvanceFilter
            objattendance._ViewByIds = mstrStringIds
            objattendance._ViewIndex = mintViewIdx
            objattendance._ViewByName = mstrStringName
            objattendance._Analysis_Fields = mstrAnalysis_Fields
            objattendance._Analysis_Join = mstrAnalysis_Join
            objattendance._Report_GroupName = mstrReport_GroupName
            objattendance._IgnoreEmptyColumns = chkIgnoreEmptyColumns.Checked
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003634 {PAPAYE}.
            objattendance._LegendCode = cboLegends.SelectedValue
            objattendance._LegendName = cboLegends.Text
            'S.SANDEEP |29-MAR-2019| -- END

            'Pinkal (22-Dec-2022) -- Start
            ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
            If mstrGroupName.ToUpper() = "TANZANIA REVENUE AUTHORITY" Then
                objattendance._LocationId = CInt(cboReportColumn.SelectedValue)
                objattendance._Location = cboReportColumn.Text
            Else
                objattendance._LocationId = 0
                objattendance._Location = ""
            End If
            'Pinkal (22-Dec-2022) -- End

            objattendance.Generate_DailyAttendanceReport(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             dtpFromDate.Value.Date, _
                                             dtpToDate.Value.Date, _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Try
            With cboEmployee
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    'Pinkal (22-Dec-2022) -- Start
    ' (A1X-351) TRA - Daily attendance report changes to include new columns and hide other columns.
    Private Sub lnkSaveChanges_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSaveChanges.LinkClicked
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1
        Dim mblnFlag As Boolean = False
        Try
            objUserDefRMode._Reportunkid = enArutiReport.Daily_Attendance_Checking_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 1
            objUserDefRMode._EarningTranHeadIds = CInt(cboReportColumn.SelectedValue)
            intUnkid = objUserDefRMode.isExist(enArutiReport.Daily_Attendance_Checking_Report, 0, 0, objUserDefRMode._Headtypeid)

            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                mblnFlag = objUserDefRMode.Insert()
            Else
                mblnFlag = objUserDefRMode.Update()
            End If

            If mblnFlag Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully."), enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show(objUserDefRMode._Message)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSaveChanges_LinkClicked", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Pinkal (22-Dec-2022) -- End

#End Region

#Region " LinkButton Event "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003634 {PAPAYE}.
#Region " Date Event(s) "

    Private Sub dtpFromDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFromDate.ValueChanged, dtpToDate.ValueChanged
        Try
            Dim dttable As DataTable = Nothing
            objattendance._FromDate = dtpFromDate.Value.Date
            objattendance._ToDate = dtpToDate.Value.Date
            dttable = objattendance.GetLegends()
            Dim DrRow As DataRow = dttable.NewRow
            DrRow("Code") = Language.getMessage(mstrModuleName, 1, "Select")
            DrRow("Name") = Language.getMessage(mstrModuleName, 1, "Select")
            DrRow("uDisplay") = Language.getMessage(mstrModuleName, 1, "Select")
            DrRow("Color") = ""
            DrRow("uCode") = "0"
            dttable.Rows.InsertAt(DrRow, 0)
            With cboLegends
                .ValueMember = "uCode"
                .DisplayMember = "uDisplay"
                .DataSource = dttable
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpFromDate_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region
    'S.SANDEEP |29-MAR-2019| -- END





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbShowColumns.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbShowColumns.ForeColor = GUI._eZeeContainerHeaderForeColor



            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.Name, Me.lblToDate.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
            Me.chkIgnoreEmptyColumns.Text = Language._Object.getCaption(Me.chkIgnoreEmptyColumns.Name, Me.chkIgnoreEmptyColumns.Text)
            Me.lblLegends.Text = Language._Object.getCaption(Me.lblLegends.Name, Me.lblLegends.Text)
            Me.gbShowColumns.Text = Language._Object.getCaption(Me.gbShowColumns.Name, Me.gbShowColumns.Text)
            Me.lblLocation.Text = Language._Object.getCaption(Me.lblLocation.Name, Me.lblLocation.Text)
            Me.lnkSaveChanges.Text = Language._Object.getCaption(Me.lnkSaveChanges.Name, Me.lnkSaveChanges.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 4, "Please set the Export Report Path form Aruti Configuration -> Option -> Paths.")
            Language.setMessage(mstrModuleName, 5, "Sorry, Location is mandatory information. Please select location to continue.")
            Language.setMessage(mstrModuleName, 6, "Selection Saved Successfully.")
            Language.setMessage(mstrModuleName, 7, "Select Allocation for Report")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
