'************************************************************************************************************************************
'Class Name :clsEmpScheduleReport.vb
'Purpose    :
'Date       :09-Jan-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports Microsoft.VisualBasic
Imports ExcelWriter


#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsEmpScheduleReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEmpScheduleReport"
    Private mstrReportId As String = enArutiReport.Employee_Schedule_Report '142
    Private objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintSelectedReportTypeId As Integer = 0
    Private mstrSelectedReportName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = String.Empty
    Private mintShiftId As Integer = 0
    Private mstrShiftName As String = String.Empty

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 183)  Working on Employee Schedule Report in MSS [Coral Beach Club Limited].
    'Private mintPolicyId As String = String.Empty
    Private mintPolicyId As Integer = 0
    'Pinkal (28-Mar-2018) -- End
    Private mstrPolicyName As String = String.Empty
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mstrAdvance_Filter As String = String.Empty
    Private mblnIncludeInactive As Boolean = False
    Private mstrYearName As String = String.Empty
    Private mdtDate1 As Date = Nothing
    Private mdtDate2 As Date = Nothing

    'Pinkal (21-AUG-2014) -- Start
    'Enhancement - GIVING ERROR WHEN DAYS MORE THAN 31 DAYS.SO CONVERT IT FROM CRYSTAL REPORT TO EXCEL EXPORT REPORT.
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    'Pinkal (21-AUG-2014) -- End


    'Pinkal (08-Oct-2015) -- Start
    'Enhancement - WORKING ON DAY OFF OPTION FOR POWERSOFT.
    Private mblnShowDayOFF As Boolean = False
    'Pinkal (08-Oct-2015) -- End

    'Pinkal (30-Oct-2017) -- Start
    'Enhancement - Ref id 15 Working on Employee Scheduled Report.
    Private mdtFromDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    Private mdtToDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
    Private mintDepartmentId As Integer = 0
    Private mstrDepartment As String = 0
    Private mintSectionId As Integer = 0
    Private mstrSection As String = 0
    'Pinkal (30-Oct-2017) -- End

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003221 {PAPAYE}.
    Private mstrCompanyGroupName As String = String.Empty
    'S.SANDEEP |29-MAR-2019| -- END


#End Region

#Region " Properties "

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintSelectedReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrSelectedReportName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ShiftId() As Integer
        Set(ByVal value As Integer)
            mintShiftId = value
        End Set
    End Property

    Public WriteOnly Property _ShiftName() As String
        Set(ByVal value As String)
            mstrShiftName = value
        End Set
    End Property

    Public WriteOnly Property _PolicyId() As Integer
        Set(ByVal value As Integer)
            mintPolicyId = value
        End Set
    End Property

    Public WriteOnly Property _PolicyName() As String
        Set(ByVal value As String)
            mstrPolicyName = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactive() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactive = value
        End Set
    End Property

    Public WriteOnly Property _YearName() As String
        Set(ByVal value As String)
            mstrYearName = value
        End Set
    End Property

    'Pinkal (21-AUG-2014) -- Start
    'Enhancement - GIVING ERROR WHEN DAYS MORE THAN 31 DAYS.SO CONVERT IT FROM CRYSTAL REPORT TO EXCEL EXPORT REPORT.

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    'Pinkal (21-AUG-2014) -- End

    'Pinkal (08-Oct-2015) -- Start
    'Enhancement - WORKING ON DAY OFF OPTION FOR POWERSOFT.

    Public WriteOnly Property _ShowDayOFF() As Boolean
        Set(ByVal value As Boolean)
            mblnShowDayOFF = value
        End Set
    End Property

    'Pinkal (08-Oct-2015) -- End


    'Pinkal (30-Oct-2017) -- Start
    'Enhancement - Ref id 15 Working on Employee Scheduled Report.

    Public WriteOnly Property _FromDate() As Date
        Set(ByVal value As Date)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As Date
        Set(ByVal value As Date)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _DepartmentId() As Integer
        Set(ByVal value As Integer)
            mintDepartmentId = value
        End Set
    End Property

    Public WriteOnly Property _Department() As String
        Set(ByVal value As String)
            mstrDepartment = value
        End Set
    End Property

    Public WriteOnly Property _SectionId() As Integer
        Set(ByVal value As Integer)
            mintSectionId = value
        End Set
    End Property

    Public WriteOnly Property _Section() As String
        Set(ByVal value As String)
            mstrSection = value
        End Set
    End Property

    'Pinkal (30-Oct-2017) -- End

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003221 {PAPAYE}.
    Public WriteOnly Property _CompanyGroupName() As String
        Set(ByVal value As String)
            mstrCompanyGroupName = value
        End Set
    End Property
    'S.SANDEEP |29-MAR-2019| -- END

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintSelectedReportTypeId = 0
            mstrSelectedReportName = String.Empty
            mintEmployeeId = 0
            mstrEmployeeName = String.Empty
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mintShiftId = 0
            mstrShiftName = String.Empty

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 183)  Working on Employee Schedule Report in MSS [Coral Beach Club Limited].
            'mintPolicyId = String.Empty
            mintPolicyId = 0
            'Pinkal (28-Mar-2018) -- End

            mstrPolicyName = String.Empty
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            mintUserUnkid = -1
            mintCompanyUnkid = -1
            mstrUserAccessFilter = String.Empty
            mstrAdvance_Filter = String.Empty
            mblnIncludeInactive = False
            mdtDate1 = Nothing
            mdtDate2 = Nothing

            'Pinkal (08-Oct-2015) -- Start
            'Enhancement - WORKING ON DAY OFF OPTION FOR POWERSOFT.
            mblnShowDayOFF = False
            'Pinkal (08-Oct-2015) -- End


            'Pinkal (30-Oct-2017) -- Start
            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
            mintDepartmentId = 0
            mstrDepartment = ""
            mintSectionId = 0
            mstrSection = ""
            mdtDate1 = ConfigParameter._Object._CurrentDateAndTime
            mdtDate2 = ConfigParameter._Object._CurrentDateAndTime
            'Pinkal (30-Oct-2017) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try


            'Pinkal (30-Oct-2017) -- Start
            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate2))
            'Pinkal (30-Oct-2017) -- End

            If mblnIncludeInactive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "From Date :") & " " & mdtDate1.ToShortDateString & " " & _
                                   Language.getMessage(mstrModuleName, 2, "To Date :") & " " & mdtDate2.ToShortDateString & " "
            End If

            If mintEmployeeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Employee :") & " " & mstrEmployeeName & " "
                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                'Pinkal (30-Oct-2017) -- End
            End If

            If mintShiftId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Shift :") & " " & mstrShiftName & " "
                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftId)
                'Pinkal (30-Oct-2017) -- End
            End If

            If mintPolicyId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Policy :") & " " & mstrPolicyName & " "
                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPolicyId)
                'Pinkal (30-Oct-2017) -- End
            End If


            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003221 {PAPAYE}.
            ''Pinkal (30-Oct-2017) -- Start
            ''Enhancement - Ref id 15 Working on Employee Scheduled Report.
            'If mintDepartmentId > 0 AndAlso mintSectionId > 0 Then
            '    objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentId)
            '    objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionId)
            'End If
            ''Pinkal (30-Oct-2017) -- End

            If mintDepartmentId > 0 Then
                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentId)
            End If
            If mintSectionId > 0 Then
                objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionId)
            End If
            'S.SANDEEP |29-MAR-2019| -- END



            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= " ORDER BY employeecode,effectivedate DESC," & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    'Pinkal (21-AUG-2014) -- Start
        '    'Enhancement - GIVING ERROR WHEN DAYS MORE THAN 31 DAYS.SO CONVERT IT FROM CRYSTAL REPORT TO EXCEL EXPORT REPORT.
        '    'objRpt = Generate_DetailReport()
        '    'Rpt = objRpt
        '    'Pinkal (21-AUG-2014) -- End

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Shani(24-Aug-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (21-AUG-2014) -- Start
    'Enhancement - GIVING ERROR WHEN DAYS MORE THAN 31 DAYS.SO CONVERT IT FROM CRYSTAL REPORT TO EXCEL EXPORT REPORT. 

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim dsTran As New DataSet
    '    Dim blnFlag As Boolean = False
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Dim iTotDays As Integer = 0
    '    Try
    '        objDataOperation = New clsDataOperation

    '        '<START & END DATE OF GIVEN MONTH AND YEAR WITH TOTAL DAYS FOR LOOP PURPOSE> -- START

    '        'S.SANDEEP [ 01 MAR 2014 ] -- START
    '        'StrQ = "DECLARE @year INT, @month INT SET @year = '" & mstrYearName & "' SET @month = '" & mintPeriodId & "'; " & _
    '        '       "SELECT " & _
    '        '       " CONVERT(CHAR(8),(SELECT DATEADD(MONTH, @month - 1,DATEADD(YEAR, @year - 1900, 0))),112) AS sDate " & _
    '        '       ",CONVERT(CHAR(8),(SELECT DATEADD(MONTH, @month,DATEADD(YEAR,@year - 1900, -1))),112) AS eDate " & _
    '        '       ",DATEDIFF(dd,CONVERT(CHAR(8),(SELECT DATEADD(MONTH, @month - 1,DATEADD(YEAR, @year - 1900, 0))),112),CONVERT(CHAR(8),(SELECT DATEADD(MONTH, @month,DATEADD(YEAR,@year - 1900, -1))),112))+ 1 AS iDay "

    '        'dsTran = objDataOperation.ExecQuery(StrQ, "List")

    '        'mdtDate1 = eZeeDate.convertDate(dsTran.Tables(0).Rows(0).Item("sDate").ToString).ToShortDateString
    '        'mdtDate2 = eZeeDate.convertDate(dsTran.Tables(0).Rows(0).Item("eDate").ToString).ToShortDateString
    '        'iTotDays = dsTran.Tables(0).Rows(0).Item("iDay")

    '        'If objDataOperation.ErrorMessage <> "" Then
    '        '    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        'End If
    '        Dim objPrd As New clscommom_period_Tran
    '        objPrd._Periodunkid = mintPeriodId
    '        mdtDate1 = objPrd._TnA_StartDate
    '        mdtDate2 = objPrd._TnA_EndDate
    '        iTotDays = DateDiff(DateInterval.Day, mdtDate1, mdtDate2) + 1
    '        objPrd = Nothing
    '        'S.SANDEEP [ 01 MAR 2014 ] -- END

    '        '<START & END DATE OF GIVEN MONTH AND YEAR WITH TOTAL DAYS FOR LOOP PURPOSE> -- END

    '        '<EMPLOYEE SHIFT OR POLICY DATA GIVEN MONTH AND YEAR WITH EFFECTIVE DATE FOR MATCHING > -- START
    '        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then 'S.SANDEEP [ 01 MAR 2014 ] -- START -- END
    '            'If dsTran.Tables(0).Rows.Count > 0 Then 
    '            Select Case mintSelectedReportTypeId
    '                Case 0  'SHIFT SCHEDULE REPORT
    '                    StrQ = "SELECT " & _
    '                           "     employeecode AS ecode " & _
    '                           "    ,shiftcode AS code " & _
    '                           "    ,shiftname AS name " & _
    '                           "    ,CONVERT(CHAR(8),effectivedate,112) AS fdate " & _
    '                           "FROM hremployee_master " & _
    '                           "    LEFT JOIN hremployee_shift_tran ON hremployee_master.employeeunkid = hremployee_shift_tran.employeeunkid " & _
    '                           "    JOIN tnashift_master ON hremployee_shift_tran.shiftunkid = tnashift_master.shiftunkid "
    '                    If mintViewIndex > 0 Then
    '                        StrQ &= mstrAnalysis_Join & " "
    '                    End If

    '                    'S.SANDEEP [ 01 MAR 2014 ] -- START
    '                    'StrQ &= " WHERE tnashift_master.isactive = 1 AND CONVERT(CHAR(8),effectivedate,112) <= '" & dsTran.Tables(0).Rows(0).Item("eDate") & "' "
    '                    StrQ &= " WHERE tnashift_master.isactive = 1 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtDate2).ToString & "' "
    '                    'S.SANDEEP [ 01 MAR 2014 ] -- END

    '                    If mblnIncludeInactive = False Then
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                    End If

    '                    If mintEmployeeId > 0 Then
    '                        StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
    '                    End If

    '                    If mstrAdvance_Filter.Trim.Length > 0 Then
    '                        StrQ &= " AND " & mstrAdvance_Filter
    '                    End If

    '                    If mintShiftId > 0 Then
    '                        StrQ &= " AND tnashift_master.shiftunkid = '" & mintShiftId & "' "
    '                    End If

    '                Case 1  'POLICY SCHEDULE REPORT
    '                    StrQ = "SELECT " & _
    '                           "	 employeecode AS ecode " & _
    '                           "	,policycode AS code " & _
    '                           "	,policyname AS name " & _
    '                           "	,CONVERT(CHAR(8),effectivedate,112) AS fdate " & _
    '                           "FROM hremployee_master " & _
    '                           "	LEFT JOIN hremployee_policy_tran ON hremployee_master.employeeunkid = hremployee_policy_tran.employeeunkid " & _
    '                           "	JOIN tnapolicy_master ON hremployee_policy_tran.policyunkid = tnapolicy_master.policyunkid "
    '                    If mintViewIndex > 0 Then
    '                        StrQ &= mstrAnalysis_Join & " "
    '                    End If

    '                    'S.SANDEEP [ 01 MAR 2014 ] -- START
    '                    'StrQ &= " WHERE tnapolicy_master.isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & dsTran.Tables(0).Rows(0).Item("eDate") & "' "
    '                    StrQ &= " WHERE tnapolicy_master.isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtDate2).ToString & "' "
    '                    'S.SANDEEP [ 01 MAR 2014 ] -- END

    '                    If mblnIncludeInactive = False Then
    '                        StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                                " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '                    End If

    '                    If mintEmployeeId > 0 Then
    '                        StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
    '                    End If

    '                    If mstrAdvance_Filter.Trim.Length > 0 Then
    '                        StrQ &= " AND " & mstrAdvance_Filter
    '                    End If

    '                    If mintPolicyId > 0 Then
    '                        StrQ &= " AND tnapolicy_master.policyunkid = '" & mintPolicyId & "' "
    '                    End If

    '            End Select

    '            Call FilterTitleAndFilterQuery()

    '            dsList = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            End If
    '            '<EMPLOYEE SHIFT OR POLICY DATA GIVEN MONTH AND YEAR WITH EFFECTIVE DATE FOR MATCHING > -- END

    '            '<GENERATING LEGENDS TO DISPLAY IN REPORT> -- START
    '            Dim StrLegends As String = String.Empty
    '            Select Case mintSelectedReportTypeId
    '                Case 0  'SHIFT SCHEDULE REPORT
    '                    StrQ = "SELECT " & _
    '                           "    shiftcode + ' - ' + shiftname AS iLegend " & _
    '                           "FROM tnashift_master " & _
    '                           "WHERE isactive = 1 "

    '                    If mintShiftId > 0 Then
    '                        StrQ &= " AND shiftunkid = '" & mintShiftId & "' "
    '                    End If
    '                Case 1  'POLICY SCHEDULE REPORT
    '                    StrQ = "SELECT " & _
    '                           "    policycode +' - '+ policyname AS iLegend " & _
    '                           "FROM tnapolicy_master " & _
    '                           "WHERE isvoid = 0 "

    '                    If mintPolicyId > 0 Then
    '                        StrQ &= " AND policyunkid = '" & mintPolicyId & "' "
    '                    End If
    '            End Select
    '            Dim dsLegends As New DataSet
    '            dsLegends = objDataOperation.ExecQuery(StrQ, "Leg")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            End If
    '            If dsLegends.Tables("Leg").Rows.Count > 0 Then
    '                For Each iLRow As DataRow In dsLegends.Tables("Leg").Rows
    '                    StrLegends &= iLRow.Item("iLegend") & vbCrLf
    '                Next
    '            End If
    '            '<GENERATING LEGENDS TO DISPLAY IN REPORT> -- END


    '            '<EMPLOYEE DATA GIVEN MONTH AND YEAR WITH CODE FOR MATCHING > -- START
    '            StrQ = "SELECT employeecode AS ecode " & _
    '                   "    ,firstname+' '+ISNULL(othername,'')+''+surname AS ename " & _
    '                   "FROM hremployee_master "
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join & " "
    '            End If

    '            StrQ &= "WHERE isapproved = 1 "

    '            If mblnIncludeInactive = False Then
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                        " AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '            End If

    '            If mintEmployeeId > 0 Then
    '                StrQ &= " AND employeeunkid = '" & mintEmployeeId & "' "
    '            End If

    '            If mstrAdvance_Filter.Trim.Length > 0 Then
    '                StrQ &= " AND " & mstrAdvance_Filter
    '            End If
    '            StrQ &= "ORDER BY firstname+' '+ISNULL(othername,'')+''+surname "

    '            Dim dsEmp As New DataSet
    '            dsEmp = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            End If
    '            '<EMPLOYEE DATA GIVEN MONTH AND YEAR WITH CODE FOR MATCHING > -- END

    '            rpt_Data = New ArutiReport.Designer.dsArutiReport

    '            objRpt = New ArutiReport.Designer.rptEmpScheduleReport

    '            Dim iDate As String = ""
    '            For Each dtRow As DataRow In dsEmp.Tables("List").Rows
    '                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

    '                rpt_Rows.Item("Column1") = dtRow.Item("ecode")
    '                rpt_Rows.Item("Column2") = dtRow.Item("ename")

    '                '<DAY LOOP BASED ON MONTH SELECTION> -- START
    '                Dim iCol As Integer = 3

    '                'S.SANDEEP [ 01 MAR 2014 ] -- START
    '                Dim iNewDate As Date = mdtDate1
    '                'For iDay As Integer = 1 To dsTran.Tables(0).Rows(0).Item("iDay")
    '                For iDay As Integer = 1 To iTotDays
    '                    'iDate = "" : iDate = mstrYearName & mintPeriodId.ToString("0#") & iDay.ToString("0#")
    '                    iDate = ""
    '                    If iDay = 1 Then
    '                        iDate = eZeeDate.convertDate(iNewDate)
    '                    Else
    '                        iNewDate = iNewDate.AddDays(1)
    '                        iDate = eZeeDate.convertDate(iNewDate)
    '                    End If
    '                    Call ReportFunction.TextChange(objRpt, "txt" & iDay.ToString, iNewDate.Day.ToString)
    '                    'S.SANDEEP [ 01 MAR 2014 ] -- END
    '                    Dim dtmp() As DataRow = dsList.Tables(0).Select("ecode = '" & dtRow.Item("ecode") & "' AND fdate <= '" & iDate & "'", "fdate DESC")
    '                    If dtmp.Length > 0 Then
    '                        rpt_Rows.Item("Column" & iCol.ToString) = IIf(dtmp(0).Item("code") = "", dtmp(0).Item("name"), dtmp(0).Item("code"))
    '                    Else
    '                        rpt_Rows.Item("Column" & iCol.ToString) = ""
    '                    End If
    '                    iCol += 1
    '                Next
    '                '<DAY LOOP BASED ON MONTH SELECTION> -- END
    '                rpt_Rows.Item("Column34") = StrLegends
    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '            Next

    '            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

    '            Dim arrImageRow As DataRow = Nothing
    '            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '            ReportFunction.Logo_Display(objRpt, _
    '                                        ConfigParameter._Object._IsDisplayLogo, _
    '                                        ConfigParameter._Object._ShowLogoRightSide, _
    '                                        "arutiLogo1", _
    '                                        "arutiLogo2", _
    '                                        arrImageRow, _
    '                                        "txtCompanyName", _
    '                                        "txtReportName", _
    '                                        "txtFilterDescription", _
    '                                        ConfigParameter._Object._GetLeftMargin, _
    '                                        ConfigParameter._Object._GetRightMargin)

    '            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)


    '            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '                rpt_Data.Tables("ArutiTable").Rows.Add("")
    '            End If

    '            If ConfigParameter._Object._IsShowPreparedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 7, "Prepared By :"))
    '                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
    '            End If

    '            If ConfigParameter._Object._IsShowCheckedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 8, "Checked By :"))
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '            End If

    '            If ConfigParameter._Object._IsShowApprovedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 9, "Approved By :"))
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '            End If

    '            If ConfigParameter._Object._IsShowReceivedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 10, "Received By :"))
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '            End If


    '            Call ReportFunction.TextChange(objRpt, "txtECode", Language.getMessage(mstrModuleName, 11, "Code"))
    '            Call ReportFunction.TextChange(objRpt, "txtEName", Language.getMessage(mstrModuleName, 12, "Employee"))

    '            objRpt.SetDataSource(rpt_Data)

    '            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 13, "Printed By :"))
    '            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 14, "Printed Date :"))

    '            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName & " - [ " & mstrSelectedReportName & " ]")
    '            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '            Select Case iTotDays
    '                Case 28
    '                    ReportFunction.EnableSuppress(objRpt, "txt29", True) : ReportFunction.EnableSuppress(objRpt, "Column311", True)
    '                    ReportFunction.EnableSuppress(objRpt, "txt30", True) : ReportFunction.EnableSuppress(objRpt, "Column321", True)
    '                    ReportFunction.EnableSuppress(objRpt, "txt31", True) : ReportFunction.EnableSuppress(objRpt, "Column331", True)
    '                Case 29
    '                    ReportFunction.EnableSuppress(objRpt, "txt30", True) : ReportFunction.EnableSuppress(objRpt, "Column321", True)
    '                    ReportFunction.EnableSuppress(objRpt, "txt31", True) : ReportFunction.EnableSuppress(objRpt, "Column331", True)
    '                Case 30
    '                    ReportFunction.EnableSuppress(objRpt, "txt31", True) : ReportFunction.EnableSuppress(objRpt, "Column331", True)
    '            End Select

    '        End If

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Sub Generate_DetailReport()

    Public Sub Generate_DetailReport(ByVal strDatabaseName As String, _
                                     ByVal intUserUnkid As Integer, _
                                     ByVal intYearUnkid As Integer, _
                                     ByVal intCompanyUnkid As Integer, _
                                     ByVal dtPeriodStart As Date, _
                                     ByVal dtPeriodEnd As Date, _
                                     ByVal strUserModeSetting As String, _
                                     ByVal blnOnlyApproved As Boolean)
        'Shani(24-Aug-2015) -- End

        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsTran As New DataSet
        Dim blnFlag As Boolean = False
        Dim iTotDays As Integer = 0
        Try
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003221 {PAPAYE}.
            If mstrCompanyGroupName.Trim.Length <= 0 Then
                Dim objCfGroup As New clsGroup_Master(True)
                mstrCompanyGroupName = objCfGroup._Groupname.ToUpper
            End If
            'S.SANDEEP |29-MAR-2019| -- END

            objDataOperation = New clsDataOperation

            '<START & END DATE OF GIVEN MONTH AND YEAR WITH TOTAL DAYS FOR LOOP PURPOSE> -- START

            'Pinkal (30-Oct-2017) -- Start
            'Enhancement - Ref id 15 Working on Employee Scheduled Report.

            If mintSelectedReportTypeId = 0 OrElse mintSelectedReportTypeId = 2 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(strDatabaseName) = mintPeriodId
                mdtDate1 = objPrd._TnA_StartDate
                mdtDate2 = objPrd._TnA_EndDate
                objPrd = Nothing
            ElseIf mintSelectedReportTypeId = 1 OrElse mintSelectedReportTypeId = 3 Then
                mdtDate1 = mdtFromDate.Date
                mdtDate2 = mdtToDate.Date
            End If

            iTotDays = DateDiff(DateInterval.Day, mdtDate1, mdtDate2) + 1

            'Pinkal (30-Oct-2017) -- End

            '<START & END DATE OF GIVEN MONTH AND YEAR WITH TOTAL DAYS FOR LOOP PURPOSE> -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtDate1, mdtDate2, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtDate2, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtDate2, strDatabaseName)
            'Shani(24-Aug-2015) -- End

            '<EMPLOYEE SHIFT OR POLICY DATA GIVEN MONTH AND YEAR WITH EFFECTIVE DATE FOR MATCHING > -- START
            If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then 'S.SANDEEP [ 01 MAR 2014 ] -- START -- END
                Select Case mintSelectedReportTypeId

                    'Pinkal (30-Oct-2017) -- Start
                    'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                    'Case 0  'SHIFT SCHEDULE REPORT
                    Case 0, 1  'SHIFT SCHEDULE REPORT
                        'Pinkal (30-Oct-2017) -- End

                        'S.SANDEEP |29-MAR-2019| -- START
                        'ENHANCEMENT : 0003221 {PAPAYE}.
                        'StrQ = "SELECT " & _
                        '       "     employeecode AS ecode " & _
                        '       "    ,shiftcode AS code " & _
                        '       "    ,shiftname AS name " & _
                        '       "    ,CONVERT(CHAR(8),effectivedate,112) AS fdate " & _
                        '       "FROM hremployee_master " & _
                        '       "    LEFT JOIN hremployee_shift_tran ON hremployee_master.employeeunkid = hremployee_shift_tran.employeeunkid " & _
                        '       "    JOIN tnashift_master ON hremployee_shift_tran.shiftunkid = tnashift_master.shiftunkid "

                        StrQ = "SELECT " & _
                               "     employeecode AS ecode " & _
                               "    ,shiftcode AS code " & _
                               "    ,shiftname AS name " & _
                               "    ,CONVERT(CHAR(8),effectivedate,112) AS fdate "

                        'If mintViewIndex > 0 Then
                        '    StrQ &= mstrAnalysis_Fields
                        'Else
                        '    StrQ &= ", 0 AS Id, '' AS GName "
                        'End If

                        StrQ &= "FROM hremployee_master " & _
                               "    LEFT JOIN hremployee_shift_tran ON hremployee_master.employeeunkid = hremployee_shift_tran.employeeunkid " & _
                               "    JOIN tnashift_master ON hremployee_shift_tran.shiftunkid = tnashift_master.shiftunkid "
                        'S.SANDEEP |29-MAR-2019| -- END



                        'Pinkal (30-Oct-2017) -- Start
                        'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                        If mintSelectedReportTypeId = 1 Then  'SHIFT REPORT DATE WISE

                            'Pinkal (11-Dec-2018) --[Issue No 3221] 
                            'an employee has been transferred to another department, he keeps showing in the schedule report of his old department even for the dates when he isn’t in that department.]
                            '["  AND departmentunkid = @departmentunkid AND sectionunkid = @sectionunkid " & _]

                            'S.SANDEEP |29-MAR-2019| -- START
                            'ENHANCEMENT : 0003221 {PAPAYE}.
                            'StrQ &= " JOIN " & _
                            '              "( " & _
                            '              "    SELECT " & _
                            '              "         departmentunkid " & _
                            '              "        ,sectionunkid " & _
                            '              "        ,employeeunkid " & _
                            '              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            '              "    FROM hremployee_transfer_tran " & _
                            '              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                            '              " ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                            '              "    AND T.departmentunkid = @departmentunkid AND T.sectionunkid = @sectionunkid "

                            StrQ &= " JOIN " & _
                                          "( " & _
                                          "    SELECT " & _
                                          "         departmentunkid " & _
                                          "        ,sectionunkid " & _
                                          "        ,employeeunkid " & _
                                          "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                          "    FROM hremployee_transfer_tran " & _
                                          "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                                    " ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "
                            If mintDepartmentId > 0 Then
                                StrQ &= " AND T.departmentunkid = @departmentunkid "
                            End If

                            If mintSectionId > 0 Then
                                StrQ &= " AND T.sectionunkid = @sectionunkid "
                            End If
                            'S.SANDEEP |29-MAR-2019| -- END

                        End If
                        'Pinkal (30-Oct-2017) -- End



                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Join & " "
                        End If

                        If xDateJoinQry.Trim.Length > 0 Then
                            StrQ &= xDateJoinQry & " "
                        End If

                        If xUACQry.Trim.Length > 0 Then
                            StrQ &= xUACQry & " "
                        End If

                        If xAdvanceJoinQry.Trim.Length > 0 Then
                            StrQ &= xAdvanceJoinQry & " "
                        End If



                        'Pinkal (30-Oct-2017) -- Start
                        'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                        'StrQ &= " WHERE tnashift_master.isactive = 1 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtDate2).ToString & "' "
                        StrQ &= " WHERE tnashift_master.isactive = 1 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate "
                        'Pinkal (30-Oct-2017) -- End


                        If xUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= " AND " & xUACFiltrQry & " "
                        End If

                        If mblnIncludeInactive = False Then
                            If xDateFilterQry.Trim.Length > 0 Then
                                StrQ &= xDateFilterQry & " "
                            End If
                        End If

                        If mstrAdvance_Filter.Trim.Length > 0 Then
                            StrQ &= " AND " & mstrAdvance_Filter
                        End If

                        If mintEmployeeId > 0 Then

                            'Pinkal (30-Oct-2017) -- Start
                            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                            'StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
                            StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid"
                            'Pinkal (30-Oct-2017) -- End

                        End If

                        If mintShiftId > 0 Then
                            'Pinkal (30-Oct-2017) -- Start
                            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                            'StrQ &= " AND tnashift_master.shiftunkid = '" & mintShiftId & "' "
                            StrQ &= " AND tnashift_master.shiftunkid = @shiftunkid"
                            'Pinkal (30-Oct-2017) -- End
                        End If

                        StrQ &= " AND hremployee_shift_tran.isvoid = 0"


                        'Pinkal (30-Oct-2017) -- Start
                        'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                        'Case 1  'POLICY SCHEDULE REPORT
                    Case 2, 3  'POLICY SCHEDULE REPORT
                        'Pinkal (30-Oct-2017) -- End

                        'S.SANDEEP |29-MAR-2019| -- START
                        'ENHANCEMENT : 0003221 {PAPAYE}.
                        'StrQ = "SELECT " & _
                        '       "	 employeecode AS ecode " & _
                        '       "	,policycode AS code " & _
                        '       "	,policyname AS name " & _
                        '       "	,CONVERT(CHAR(8),effectivedate,112) AS fdate " & _
                        '       "FROM hremployee_master " & _
                        '       "	LEFT JOIN hremployee_policy_tran ON hremployee_master.employeeunkid = hremployee_policy_tran.employeeunkid " & _
                        '       "	JOIN tnapolicy_master ON hremployee_policy_tran.policyunkid = tnapolicy_master.policyunkid "
                        StrQ = "SELECT " & _
                               "	 employeecode AS ecode " & _
                               "	,policycode AS code " & _
                               "	,policyname AS name " & _
                               "	,CONVERT(CHAR(8),effectivedate,112) AS fdate "
                        'If mintViewIndex > 0 Then
                        '    StrQ &= mstrAnalysis_Fields
                        'Else
                        '    StrQ &= ", 0 AS Id, '' AS GName "
                        'End If
                        StrQ &= "FROM hremployee_master " & _
                               "	LEFT JOIN hremployee_policy_tran ON hremployee_master.employeeunkid = hremployee_policy_tran.employeeunkid " & _
                               "	JOIN tnapolicy_master ON hremployee_policy_tran.policyunkid = tnapolicy_master.policyunkid "
                        'S.SANDEEP |29-MAR-2019| -- END


                        'Pinkal (30-Oct-2017) -- Start
                        'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                        If mintSelectedReportTypeId = 3 Then  'POLICY REPORT DATE WISE

                            'Pinkal (11-Dec-2018) --[Issue No 3221] 
                            'an employee has been transferred to another department, he keeps showing in the schedule report of his old department even for the dates when he isn’t in that department.]
                            '["  AND departmentunkid = @departmentunkid AND sectionunkid = @sectionunkid " & _]

                            'S.SANDEEP |29-MAR-2019| -- START
                            'ENHANCEMENT : 0003221 {PAPAYE}.
                            'StrQ &= " JOIN " & _
                            '        "( " & _
                            '        "    SELECT " & _
                            '        "         departmentunkid " & _
                            '        "        ,sectionunkid " & _
                            '        "        ,employeeunkid " & _
                            '        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            '        "    FROM hremployee_transfer_tran " & _
                            '        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                            '        " ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                            '        "    AND T.departmentunkid = @departmentunkid AND T.sectionunkid = @sectionunkid "
                            StrQ &= " JOIN " & _
                                          "( " & _
                                          "    SELECT " & _
                                          "         departmentunkid " & _
                                          "        ,sectionunkid " & _
                                          "        ,employeeunkid " & _
                                          "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                          "    FROM hremployee_transfer_tran " & _
                                          "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                                    " ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                            If mintDepartmentId > 0 Then
                                StrQ &= "    AND T.departmentunkid = @departmentunkid "
                            End If

                            If mintSectionId > 0 Then
                                StrQ &= "    AND T.sectionunkid = @sectionunkid "
                            End If
                            'S.SANDEEP |29-MAR-2019| -- END

                        End If
                        'Pinkal (30-Oct-2017) -- End


                        If mintViewIndex > 0 Then
                            StrQ &= mstrAnalysis_Join & " "
                        End If

                        If xDateJoinQry.Trim.Length > 0 Then
                            StrQ &= xDateJoinQry & " "
                        End If

                        If xUACQry.Trim.Length > 0 Then
                            StrQ &= xUACQry & " "
                        End If

                        If xAdvanceJoinQry.Trim.Length > 0 Then
                            StrQ &= xAdvanceJoinQry & " "
                        End If


                        'Pinkal (30-Oct-2017) -- Start
                        'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                        'StrQ &= " WHERE tnapolicy_master.isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtDate2).ToString & "' "
                        StrQ &= " WHERE tnapolicy_master.isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate"
                        'Pinkal (30-Oct-2017) -- End


                        If xUACFiltrQry.Trim.Length > 0 Then
                            StrQ &= " AND " & xUACFiltrQry & " "
                        End If

                        If mblnIncludeInactive = False Then
                            If xDateFilterQry.Trim.Length > 0 Then
                                StrQ &= xDateFilterQry & " "
                            End If
                        End If

                        If mstrAdvance_Filter.Trim.Length > 0 Then
                            StrQ &= " AND " & mstrAdvance_Filter
                        End If


                        If mintEmployeeId > 0 Then
                            'Pinkal (30-Oct-2017) -- Start
                            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                            'StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmployeeId & "' "
                            StrQ &= " AND hremployee_master.employeeunkid =  @employeeunkid"
                            'Pinkal (30-Oct-2017) -- End
                        End If

                        If mintPolicyId > 0 Then
                            'Pinkal (30-Oct-2017) -- Start
                            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                            'StrQ &= " AND tnapolicy_master.policyunkid = '" & mintPolicyId & "' "
                            StrQ &= " AND tnapolicy_master.policyunkid = @policyunkid"
                            'Pinkal (30-Oct-2017) -- End
                        End If

                        StrQ &= " AND hremployee_policy_tran.isvoid = 0"


                End Select


                Call FilterTitleAndFilterQuery()

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If


                '<EMPLOYEE SHIFT OR POLICY DATA GIVEN MONTH AND YEAR WITH EFFECTIVE DATE FOR MATCHING > -- END

                '<GENERATING LEGENDS TO DISPLAY IN REPORT> -- START
                Dim StrLegends As String = String.Empty
                Select Case mintSelectedReportTypeId

                    'Pinkal (30-Oct-2017) -- Start
                    'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                    'Case 0  'SHIFT SCHEDULE REPORT
                    Case 0, 1  'SHIFT SCHEDULE REPORT
                        'Pinkal (30-Oct-2017) -- End


                        StrQ = "SELECT " & _
                               "    shiftcode + ' - ' + shiftname AS iLegend " & _
                               "FROM tnashift_master " & _
                               "WHERE isactive = 1 "

                        If mintShiftId > 0 Then

                            'Pinkal (30-Oct-2017) -- Start
                            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                            'StrQ &= " AND shiftunkid = '" & mintShiftId & "' "
                            StrQ &= " AND shiftunkid = @shiftunkid"
                            'Pinkal (30-Oct-2017) -- End

                        End If

                        'Pinkal (30-Oct-2017) -- Start
                        'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                        'Case 1  'POLICY SCHEDULE REPORT
                    Case 2, 3  'POLICY SCHEDULE REPORT
                        'Pinkal (30-Oct-2017) -- End
                        StrQ = "SELECT " & _
                               "    policycode +' - '+ policyname AS iLegend " & _
                               "FROM tnapolicy_master " & _
                               "WHERE isvoid = 0 "

                        If mintPolicyId > 0 Then

                            'Pinkal (30-Oct-2017) -- Start
                            'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                            'StrQ &= " AND policyunkid = '" & mintPolicyId & "' "
                            StrQ &= " AND policyunkid = @policyunkid"
                            'Pinkal (30-Oct-2017) -- End
                        End If
                End Select
                Dim dsLegends As New DataSet

                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftId)
                objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPolicyId)
                'Pinkal (30-Oct-2017) -- End

                dsLegends = objDataOperation.ExecQuery(StrQ, "Leg")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                If mblnShowDayOFF Then
                    If dsLegends IsNot Nothing AndAlso dsLegends.Tables(0).Rows.Count > 0 Then
                        Dim drRow As DataRow = dsLegends.Tables(0).NewRow
                        drRow("iLegend") = Language.getMessage(mstrModuleName, 14, "OFF - DAY OFF")
                        dsLegends.Tables(0).Rows.InsertAt(drRow, dsLegends.Tables(0).Rows.Count)
                    End If
                End If

                If dsLegends.Tables("Leg").Rows.Count > 0 Then
                    'S.SANDEEP |29-MAR-2019| -- START
                    'ENHANCEMENT : 0003221 {PAPAYE}.
                    'For Each iLRow As DataRow In dsLegends.Tables("Leg").Rows
                    '    StrLegends &= iLRow.Item("iLegend") & vbCrLf
                    'Next
                    StrLegends = String.Join(vbCrLf, dsLegends.Tables("Leg").AsEnumerable().Select(Function(x) x.Field(Of String)("iLegend")).ToArray())
                    'S.SANDEEP |29-MAR-2019| -- END
                End If
                '<GENERATING LEGENDS TO DISPLAY IN REPORT> -- END


                ''<EMPLOYEE DATA GIVEN MONTH AND YEAR WITH CODE FOR MATCHING > -- START
                StrQ = "SELECT employeecode AS ecode " & _
                       "    ,firstname+' '+ISNULL(othername,'')+''+surname AS ename " & _
                           "    ,hremployee_master.employeeunkid "

                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003221 {PAPAYE}.
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Fields
                Else
                    StrQ &= ", 0 AS Id, '' AS GName "
                End If
                'S.SANDEEP |29-MAR-2019| -- END


                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.

                If mintSelectedReportTypeId = 1 OrElse mintSelectedReportTypeId = 3 Then
                    'S.SANDEEP |29-MAR-2019| -- START
                    'ENHANCEMENT : 0003221 {PAPAYE}.
                    'StrQ &= ", hrjob_master.job_name AS Job, ISNULL(hrjob_master.desciription,'') AS Description,EmpQ.Qualification "

                    'S.SANDEEP |05-APR-2019| -- START
                    'StrQ &= ", hrjob_master.job_name AS Job "
                    StrQ &= ", ejb.job_name AS Job "
                    'S.SANDEEP |05-APR-2019| -- END

                    If mstrCompanyGroupName <> "PAPAYE FAST FOODS LTD" Then
                        StrQ &= ", ISNULL(ejb.desciription,'') AS Description " & _
                                ", EmpQ.Qualification "

                        'Pinkal (06-Dec-2019) -- 'PAPAYE [0004328] -   Error when generating employee schedule report by date range [ISNULL(hrjob_master.desciription,'') AS Description]

                    End If
                    'S.SANDEEP |29-MAR-2019| -- END
                End If

                'Pinkal (30-Oct-2017) -- End



                StrQ &= "FROM hremployee_master "
                If mintViewIndex > 0 Then
                    StrQ &= mstrAnalysis_Join & " "
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry & " "
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry & " "
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry & " "
                End If


                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                If mintSelectedReportTypeId = 1 OrElse mintSelectedReportTypeId = 3 Then  'SHIFT/POLICY REPORT DATE WISE


                    'Pinkal (11-Dec-2018) --[Issue No 3221] 
                    'an employee has been transferred to another department, he keeps showing in the schedule report of his old department even for the dates when he isn’t in that department.]
                    '["  AND departmentunkid = @departmentunkid AND sectionunkid = @sectionunkid " & _]


                    'S.SANDEEP |29-MAR-2019| -- START
                    'ENHANCEMENT : 0003221 {PAPAYE}.
                    'StrQ &= " JOIN " & _
                    '              "( " & _
                    '              "    SELECT " & _
                    '              "         departmentunkid " & _
                    '              "        ,sectionunkid " & _
                    '              "        ,employeeunkid " & _
                    '              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '              "    FROM hremployee_transfer_tran " & _
                    '              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                    '              " ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    '              "    AND T.departmentunkid = @departmentunkid AND T.sectionunkid = @sectionunkid " & _
                    '              " LEFT JOIN " & _
                    '              "( " & _
                    '              "    SELECT " & _
                    '              "         jobgroupunkid " & _
                    '              "        ,jobunkid " & _
                    '              "        ,employeeunkid " & _
                    '              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '              "    FROM hremployee_categorization_tran " & _
                    '              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                    '              " ) AS jb ON jb.employeeunkid = hremployee_master.employeeunkid AND jb.Rno = 1 " & _
                    '              " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = jb.jobunkid " & _
                    '              " LEFT JOIN " & _
                    '              " ( " & _
                    '                     " SELECT DISTINCT A.employeeunkid, ISNULL(STUFF( " & _
                    '                                   "(SELECT  DISTINCT ',' +  CAST(CASE WHEN hrqualification_master.qualificationname IS NULL THEN hremp_qualification_tran.other_qualification ELSE hrqualification_master.qualificationname END AS nvarchar(max)) " & _
                    '                                   "FROM hremp_qualification_tran " & _
                    '                                   "LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                    '                                   "WHERE hremp_qualification_tran.isvoid = 0  AND hremp_qualification_tran.employeeunkid = A.employeeunkid " & _
                    '                                   "FOR XML PATH('')),1,1,''),'0') AS Qualification " & _
                    '                     " FROM hremp_qualification_tran AS A WHERE A.isvoid = 0 " & _
                    '            ") AS EmpQ ON EmpQ.employeeunkid = hremployee_master.employeeunkid "

                    StrQ &= " JOIN " & _
                                  "( " & _
                                  "    SELECT " & _
                                  "         departmentunkid " & _
                                  "        ,sectionunkid " & _
                                  "        ,employeeunkid " & _
                                  "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                  "    FROM hremployee_transfer_tran " & _
                                  "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                            " ) AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "
                    If mintDepartmentId > 0 Then
                        StrQ &= "    AND T.departmentunkid = @departmentunkid "
                    End If

                    If mintSectionId > 0 Then
                        StrQ &= "    AND T.sectionunkid = @sectionunkid "
                    End If
                    'S.SANDEEP |05-APR-2019| -- START
                    'StrQ &= " LEFT JOIN " & _
                    '              "( " & _
                    '              "    SELECT " & _
                    '              "         jobgroupunkid " & _
                    '              "        ,jobunkid " & _
                    '              "        ,employeeunkid " & _
                    '              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    '              "    FROM hremployee_categorization_tran " & _
                    '              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                    '              " ) AS jb ON jb.employeeunkid = hremployee_master.employeeunkid AND jb.Rno = 1 " & _
                    '              " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = jb.jobunkid " & _
                    '              " LEFT JOIN " & _
                    '              " ( " & _
                    '                     " SELECT DISTINCT A.employeeunkid, ISNULL(STUFF( " & _
                    '        "       (SELECT  DISTINCT ',' +  CAST(CASE WHEN hrqualification_master.qualificationname IS NULL THEN hremp_qualification_tran.other_qualification ELSE hrqualification_master.qualificationname END AS nvarchar(max)) " & _
                    '        "        FROM hremp_qualification_tran " & _
                    '        "           LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                    '        "        WHERE hremp_qualification_tran.isvoid = 0  AND hremp_qualification_tran.employeeunkid = A.employeeunkid " & _
                    '        "        FOR XML PATH('')),1,1,''),'0') AS Qualification " & _
                    '                     " FROM hremp_qualification_tran AS A WHERE A.isvoid = 0 " & _
                    '            ") AS EmpQ ON EmpQ.employeeunkid = hremployee_master.employeeunkid "

                    StrQ &= " LEFT JOIN " & _
                                  "( " & _
                                  "    SELECT " & _
                                  "         jobgroupunkid " & _
                                  "        ,jobunkid " & _
                                  "        ,employeeunkid " & _
                                  "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                  "    FROM hremployee_categorization_tran " & _
                                  "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effectivedate " & _
                                  " ) AS jb ON jb.employeeunkid = hremployee_master.employeeunkid AND jb.Rno = 1 " & _
                            " LEFT JOIN hrjob_master AS ejb ON ejb.jobunkid = jb.jobunkid " & _
                                  " LEFT JOIN " & _
                                  " ( " & _
                                         " SELECT DISTINCT A.employeeunkid, ISNULL(STUFF( " & _
                            "       (SELECT  DISTINCT ',' +  CAST(CASE WHEN hrqualification_master.qualificationname IS NULL THEN hremp_qualification_tran.other_qualification ELSE hrqualification_master.qualificationname END AS nvarchar(max)) " & _
                            "        FROM hremp_qualification_tran " & _
                            "           LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                            "        WHERE hremp_qualification_tran.isvoid = 0  AND hremp_qualification_tran.employeeunkid = A.employeeunkid " & _
                            "        FOR XML PATH('')),1,1,''),'0') AS Qualification " & _
                                         " FROM hremp_qualification_tran AS A WHERE A.isvoid = 0 " & _
                                ") AS EmpQ ON EmpQ.employeeunkid = hremployee_master.employeeunkid "
                    'S.SANDEEP |05-APR-2019| -- END

                    'S.SANDEEP |29-MAR-2019| -- END



                End If
                'Pinkal (30-Oct-2017) -- End


                StrQ &= "WHERE isapproved = 1 "


                If mblnIncludeInactive = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry & " "
                    End If
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

                If mintEmployeeId > 0 Then
                    'Pinkal (30-Oct-2017) -- Start
                    'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                    'StrQ &= " AND employeeunkid = '" & mintEmployeeId & "' "
                    StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                    'Pinkal (30-Oct-2017) -- End
                End If


                StrQ &= " ORDER BY firstname+' '+ISNULL(othername,'')+''+surname "

                Dim dsEmp As New DataSet

                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate2))
                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentId)
                objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionId)
                'Pinkal (30-Oct-2017) -- End

                dsEmp = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If
                ''<EMPLOYEE DATA GIVEN MONTH AND YEAR WITH CODE FOR MATCHING > -- END


                If dsList IsNot Nothing Then
                    Dim intCount As Integer = 0


                    For i As Integer = 0 To iTotDays - 1
                        If dsEmp.Tables(0).Columns.Contains(eZeeDate.convertDate(mdtDate1.AddDays(i).Date)) = False Then
                            dsEmp.Tables(0).Columns.Add(eZeeDate.convertDate(mdtDate1.AddDays(i).Date), System.Type.GetType("System.String"))
                            dsEmp.Tables(0).Columns(eZeeDate.convertDate(mdtDate1.AddDays(i).Date)).Caption = eZeeDate.convertDate(mdtDate1.AddDays(i).Date)
                        Else
                            intCount += 1
                            dsEmp.Tables(0).Columns.Add(eZeeDate.convertDate(mdtDate1.AddDays(i).Date) & StrDup(intCount, "*"), System.Type.GetType("System.String"))
                            dsEmp.Tables(0).Columns(eZeeDate.convertDate(mdtDate1.AddDays(i).Date) & StrDup(intCount, "*")).Caption = eZeeDate.convertDate(mdtDate1.AddDays(i).Date)
                        End If
                        'S.SANDEEP |29-MAR-2019| -- START
                        'ENHANCEMENT : 0003221 {PAPAYE}.
                        If i = 0 Then
                            dsEmp.Tables(0).Columns(eZeeDate.convertDate(mdtDate1.AddDays(i).Date)).ExtendedProperties.Add("dt", "dt")
                        End If
                        'S.SANDEEP |29-MAR-2019| -- END
                    Next
                End If

                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003221 {PAPAYE}.
                dsEmp.Tables(0).Columns("Id").SetOrdinal(dsEmp.Tables(0).Columns.Count - 1)
                dsEmp.Tables(0).Columns("GName").SetOrdinal(dsEmp.Tables(0).Columns.Count - 1)
                'S.SANDEEP |29-MAR-2019| -- END


                Dim objEmpDayOff As New clsemployee_dayoff_Tran
                Dim iDate As String = ""

                For Each dtRow As DataRow In dsEmp.Tables(0).Rows
                    'Dim intColumnindex As Integer = 2

                    'Pinkal (30-Oct-2017) -- Start
                    'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                    'Dim intColumnindex As Integer = 3
                    Dim intColumnindex As Integer = 0
                    If mintSelectedReportTypeId = 0 OrElse mintSelectedReportTypeId = 2 Then  'SHIFT/POLICY PERIOD WISE REPRT
                        intColumnindex = 3
                    ElseIf mintSelectedReportTypeId = 1 OrElse mintSelectedReportTypeId = 3 Then  'SHIFT/POLICY DATE WISE REPRT
                        'S.SANDEEP |29-MAR-2019| -- START
                        'ENHANCEMENT : 0003221 {PAPAYE}.

                        ''Pinkal (30-Oct-2017) -- Start
                        ''Enhancement - Ref id 15 Working on Employee Scheduled Report.
                        ''intColumnindex = 8
                        'intColumnindex = 6
                        ''Pinkal (30-Oct-2017) -- End


                        'Dim rtext As New RichTextBox
                        'rtext.Rtf = dtRow("Description").ToString()
                        'dtRow("Description") = rtext.Text.Trim
                        'rtext = Nothing
                        If mstrCompanyGroupName <> "PAPAYE FAST FOODS LTD" Then
                            intColumnindex = 6
                            Dim rtext As New RichTextBox
                            rtext.Rtf = dtRow("Description").ToString()
                            dtRow("Description") = rtext.Text.Trim
                            rtext = Nothing
                        Else
                            intColumnindex = 4
                        End If
                        'S.SANDEEP |29-MAR-2019| -- END


                    End If

                    'Pinkal (30-Oct-2017) -- End


                    For iDay As Integer = 0 To iTotDays - 1
                        iDate = eZeeDate.convertDate(mdtDate1.AddDays(iDay).Date)
                        Dim dtmp() As DataRow = dsList.Tables(0).Select("ecode = '" & dtRow.Item("ecode") & "' AND fdate <= '" & iDate & "'", "fdate DESC")
                        If dtmp.Length > 0 Then
                            If dsEmp.Tables(0).Columns(intColumnindex).Caption = iDate Then
                                If mblnShowDayOFF Then
                                    If objEmpDayOff.isExist(eZeeDate.convertDate(dsEmp.Tables(0).Columns(intColumnindex).Caption), CInt(dtRow.Item("employeeunkid")), -1, Nothing) Then
                                        dtRow(intColumnindex) = Language.getMessage(mstrModuleName, 13, "OFF")
                                    Else
                                        dtRow(intColumnindex) = IIf(dtmp(0).Item("code") = "", dtmp(0).Item("name"), dtmp(0).Item("code"))
                                    End If
                                Else
                                    dtRow(intColumnindex) = IIf(dtmp(0).Item("code") = "", dtmp(0).Item("name"), dtmp(0).Item("code"))
                                End If
                            Else
                                dtRow(intColumnindex) = ""
                            End If
                            'Pinkal (19-Jan-2016) -- Start
                            'Enhancement - Working on Changes in close Year For Voltamp problem for transfering employee Assigned shift.
                            'intColumnindex += 1
                            'Pinkal (19-Jan-2016) -- End
                        End If
                        'Pinkal (19-Jan-2016) -- Start
                        'Enhancement - Working on Changes in close Year For Voltamp problem for transfering employee Assigned shift.
                        intColumnindex += 1
                        'Pinkal (19-Jan-2016) -- End
                    Next
                Next
                objEmpDayOff = Nothing


                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                'If dsEmp IsNot Nothing AndAlso dsEmp.Tables(0).Rows.Count > 0 Then
                If dsEmp IsNot Nothing Then
                    If dsEmp.Tables(0).Columns.Contains("employeeunkid") Then dsEmp.Tables(0).Columns.Remove("employeeunkid")
                End If
                'Pinkal (30-Oct-2017) -- End

                ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

                Dim strarrGroupColumns As String() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell

                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003221 {PAPAYE}.
                Dim mdtTableExcel As DataTable = dsEmp.Tables(0)
                Dim mView As DataView = mdtTableExcel.DefaultView
                If mintViewIndex <= 0 Then
                    mView.Table().Columns.Remove("Id")
                    mView.Table().Columns.Remove("GName")
                Else
                    mView.Sort = "GName"
                    mView.Table().Columns.Remove("Id")
                    mView.Table().Columns("GName").Caption = mstrReport_GroupName
                    Dim strGrpCols As String() = {"GName"}
                    strarrGroupColumns = strGrpCols
                End If
                mView.Table.AcceptChanges()
                mdtTableExcel = mView.ToTable()
                'S.SANDEEP |29-MAR-2019| -- END


                row = New WorksheetRow()

                If Me._FilterTitle.ToString.Length > 0 Then
                    wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    row.Cells.Add(wcell)
                End If
                rowsArrayHeader.Add(row)

                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                If mintSelectedReportTypeId = 1 OrElse mintSelectedReportTypeId = 3 Then
                    row = New WorksheetRow()

                    'S.SANDEEP |29-MAR-2019| -- START
                    'ENHANCEMENT : 0003221 {PAPAYE}.
                    'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Department :") & mstrDepartment & Space(15) & Language.getMessage(mstrModuleName, 16, "Section :") & mstrSection, "s10bw")
                    'wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    'row.Cells.Add(wcell)
                    'rowsArrayHeader.Add(row)

                    If mintDepartmentId > 0 AndAlso mintSectionId > 0 Then
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Department :") & mstrDepartment & Space(15) & Language.getMessage(mstrModuleName, 16, "Section :") & mstrSection, "s10bw")
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                        row.Cells.Add(wcell)
                        rowsArrayHeader.Add(row)
                    ElseIf mintDepartmentId > 0 Then
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 15, "Department :") & mstrDepartment, "s10bw")
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                        row.Cells.Add(wcell)
                        rowsArrayHeader.Add(row)
                    ElseIf mintSectionId > 0 Then
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 16, "Section :") & mstrSection, "s10bw")
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                        row.Cells.Add(wcell)
                        rowsArrayHeader.Add(row)
                    End If
                    'S.SANDEEP |29-MAR-2019| -- END
                End If
                'Pinkal (30-Oct-2017) -- End


                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)


                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)


                row = New WorksheetRow()

                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                If mintSelectedReportTypeId = 0 OrElse mintSelectedReportTypeId = 1 Then
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 17, "Shift Information: "), "s10bw")
                ElseIf mintSelectedReportTypeId = 2 OrElse mintSelectedReportTypeId = 3 Then
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Policy Information: "), "s10bw")
                End If
                'Pinkal (30-Oct-2017) -- End

                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)


                If dsLegends IsNot Nothing AndAlso dsLegends.Tables(0).Rows.Count > 0 Then

                    'Pinkal (30-Oct-2017) -- Start
                    'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)
                    'Pinkal (30-Oct-2017) -- End

                    For i As Integer = 0 To dsLegends.Tables(0).Rows.Count - 1
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(dsLegends.Tables(0).Rows(i)("iLegend").ToString(), "s10bw")

                        'Pinkal (30-Oct-2017) -- Start
                        'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                        'wcell.MergeAcross = 5
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                        'Pinkal (30-Oct-2017) -- End
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                    Next

                End If

                'Pinkal (30-Oct-2017) -- End

                '--------------------


                If ConfigParameter._Object._IsShowPreparedBy = True Then
                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                    wcell.MergeAcross = 4
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)
                End If



                If ConfigParameter._Object._IsShowCheckedBy = True Then
                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Checked By :"), "s8bw")
                    wcell.MergeAcross = 4
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)
                End If


                If ConfigParameter._Object._IsShowApprovedBy = True Then
                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Approved By"), "s8bw")
                    wcell.MergeAcross = 4
                    row.Cells.Add(wcell)

                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s10bw")
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)
                End If

                If ConfigParameter._Object._IsShowReceivedBy = True Then
                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 10, "Received By :"), "s8bw")
                    wcell.MergeAcross = 4
                    row.Cells.Add(wcell)
                    rowsArrayFooter.Add(row)
                End If

                mdtTableExcel.Columns("ecode").Caption = Language.getMessage(mstrModuleName, 11, "Code")
                mdtTableExcel.Columns("ename").Caption = Language.getMessage(mstrModuleName, 12, "Employee")

                Dim intDaysCount As Integer = 0

                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.
                Dim xDay As Integer = 0
                If mintSelectedReportTypeId = 0 OrElse mintSelectedReportTypeId = 2 Then
                    xDay = 2
                ElseIf mintSelectedReportTypeId = 1 OrElse mintSelectedReportTypeId = 3 Then
                    'S.SANDEEP |29-MAR-2019| -- START
                    'ENHANCEMENT : 0003221 {PAPAYE}.
                    'xDay = 5
                    If mstrCompanyGroupName <> "PAPAYE FAST FOODS LTD" Then
                        xDay = 5
                    Else
                        xDay = 3
                    End If
                    'S.SANDEEP |29-MAR-2019| -- END
                End If

                'For i As Integer = 2 To mdtTableExcel.Columns.Count - 1

                'S.SANDEEP |29-MAR-2019| -- START
                'ENHANCEMENT : 0003221 {PAPAYE}.
                'For i As Integer = xDay To mdtTableExcel.Columns.Count - 1
                Dim intLoopStep As Integer = 1
                If mintViewIndex > 0 Then
                    intLoopStep = 2
                End If
                For i As Integer = xDay To mdtTableExcel.Columns.Count - intLoopStep
                    'S.SANDEEP |29-MAR-2019| -- END
                    mdtTableExcel.Columns(i).Caption = MonthName(mdtDate1.AddDays(intDaysCount).Month, True) & "#10;" & mdtDate1.AddDays(intDaysCount).ToString("dd") & "#10;" & WeekdayName(Weekday(mdtDate1.AddDays(intDaysCount).Date, FirstDayOfWeek.Sunday), True, FirstDayOfWeek.Sunday)
                    intDaysCount += 1
                Next
                'Pinkal (30-Oct-2017) -- End

                ''SET EXCEL CELL WIDTH  
                Dim intArrayColumnWidth As Integer() = Nothing
                ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)


                'Pinkal (30-Oct-2017) -- Start
                'Enhancement - Ref id 15 Working on Employee Scheduled Report.

                If mintSelectedReportTypeId = 0 OrElse mintSelectedReportTypeId = 2 Then  'SHIFT / POLICY PERIOD WISE REPORT
                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        If i > 1 Then
                            intArrayColumnWidth(i) = 50
                        ElseIf i = 0 Then
                            intArrayColumnWidth(i) = 100
                        ElseIf i = 1 Then
                            intArrayColumnWidth(i) = 175
                        End If
                    Next

                ElseIf mintSelectedReportTypeId = 1 OrElse mintSelectedReportTypeId = 3 Then  'SHIFT / POLICY DATE WISE REPORT
                    'S.SANDEEP |29-MAR-2019| -- START
                    'ENHANCEMENT : 0003221 {PAPAYE}.
                    'ElseIf i <= 4 Then
                    'For i As Integer = 0 To intArrayColumnWidth.Length - 1
                    '    If i > 4 Then
                    Dim intDateColIndex As Integer = 4
                    Dim xCols = mdtTableExcel.Columns.Cast(Of DataColumn).AsEnumerable().Where(Function(x) x.ExtendedProperties("dt") <> "").FirstOrDefault
                    If xCols IsNot Nothing Then
                        intDateColIndex = mdtTableExcel.Columns.IndexOf(xCols)
                    End If
                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        If i >= intDateColIndex Then
                            'S.SANDEEP |29-MAR-2019| -- END
                            intArrayColumnWidth(i) = 50
                        ElseIf i = 0 Then
                            intArrayColumnWidth(i) = 100
                        ElseIf i < intDateColIndex Then
                            intArrayColumnWidth(i) = 175
                        End If
                    Next
                End If

                'Pinkal (30-Oct-2017) -- End

                'SET EXCEL CELL WIDTH

                Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName & " - (" & mstrSelectedReportName & ")", "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (21-AUG-2014) -- End

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "From Date :")
            Language.setMessage(mstrModuleName, 2, "To Date :")
            Language.setMessage(mstrModuleName, 3, "Employee :")
            Language.setMessage(mstrModuleName, 4, "Shift :")
            Language.setMessage(mstrModuleName, 5, "Policy :")
            Language.setMessage(mstrModuleName, 6, "Order By :")
            Language.setMessage(mstrModuleName, 7, "Prepared By :")
            Language.setMessage(mstrModuleName, 8, "Checked By :")
            Language.setMessage(mstrModuleName, 9, "Approved By")
            Language.setMessage(mstrModuleName, 10, "Received By :")
            Language.setMessage(mstrModuleName, 11, "Code")
            Language.setMessage(mstrModuleName, 12, "Employee")
            Language.setMessage(mstrModuleName, 13, "OFF")
            Language.setMessage(mstrModuleName, 14, "OFF - DAY OFF")
            Language.setMessage(mstrModuleName, 15, "Department :")
            Language.setMessage(mstrModuleName, 16, "Section :")
            Language.setMessage(mstrModuleName, 17, "Shift Information:")
            Language.setMessage(mstrModuleName, 18, "Policy Information:")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
