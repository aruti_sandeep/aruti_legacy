﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAttendanceTrackerSummaryReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAttendanceTrackerSummaryReport))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.objbtnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.objbtnLanguage = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnReset = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkConsiderZeroFilter = New System.Windows.Forms.CheckBox
        Me.nudDayTo = New System.Windows.Forms.NumericUpDown
        Me.nudDayFrom = New System.Windows.Forms.NumericUpDown
        Me.elActDaysNoOfWorked = New eZee.Common.eZeeLine
        Me.objlblPDate = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.lblYTo = New System.Windows.Forms.Label
        Me.lblDayFrom = New System.Windows.Forms.Label
        Me.chkShowLateArrivalhours = New System.Windows.Forms.CheckBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.lblExpetedDays = New System.Windows.Forms.Label
        Me.nudDaysWorked = New System.Windows.Forms.NumericUpDown
        Me.dgvLeave = New System.Windows.Forms.DataGridView
        Me.dgcolhOrgLeave = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCaption = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhLvTypId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbOtherSetting = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboAllocation = New System.Windows.Forms.ComboBox
        Me.lblAllocation = New System.Windows.Forms.Label
        Me.objpnlLvTypes = New System.Windows.Forms.Panel
        Me.lnkSaveSelection = New System.Windows.Forms.LinkLabel
        Me.EZeeFooter1.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.nudDayTo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDayFrom, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDaysWorked, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLeave, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbOtherSetting.SuspendLayout()
        Me.objpnlLvTypes.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(865, 60)
        Me.eZeeHeader.TabIndex = 32
        Me.eZeeHeader.Title = ""
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.objbtnAdvanceFilter)
        Me.EZeeFooter1.Controls.Add(Me.objbtnLanguage)
        Me.EZeeFooter1.Controls.Add(Me.btnReset)
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 515)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(865, 55)
        Me.EZeeFooter1.TabIndex = 33
        '
        'objbtnAdvanceFilter
        '
        Me.objbtnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.objbtnAdvanceFilter.BackgroundImage = CType(resources.GetObject("objbtnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.objbtnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Location = New System.Drawing.Point(469, 13)
        Me.objbtnAdvanceFilter.Name = "objbtnAdvanceFilter"
        Me.objbtnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.objbtnAdvanceFilter.TabIndex = 36
        Me.objbtnAdvanceFilter.Text = "Adv. Filter"
        Me.objbtnAdvanceFilter.UseVisualStyleBackColor = False
        '
        'objbtnLanguage
        '
        Me.objbtnLanguage.BackColor = System.Drawing.Color.White
        Me.objbtnLanguage.BackgroundImage = CType(resources.GetObject("objbtnLanguage.BackgroundImage"), System.Drawing.Image)
        Me.objbtnLanguage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnLanguage.BorderColor = System.Drawing.Color.Empty
        Me.objbtnLanguage.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnLanguage.FlatAppearance.BorderSize = 0
        Me.objbtnLanguage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnLanguage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnLanguage.ForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnLanguage.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Image = Global.ArutiReports.My.Resources.Resources.FONT_TXT
        Me.objbtnLanguage.Location = New System.Drawing.Point(12, 13)
        Me.objbtnLanguage.Name = "objbtnLanguage"
        Me.objbtnLanguage.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnLanguage.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnLanguage.Size = New System.Drawing.Size(30, 30)
        Me.objbtnLanguage.TabIndex = 35
        Me.objbtnLanguage.UseVisualStyleBackColor = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.White
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.BorderColor = System.Drawing.Color.Empty
        Me.btnReset.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnReset.FlatAppearance.BorderSize = 0
        Me.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReset.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReset.ForeColor = System.Drawing.Color.Black
        Me.btnReset.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnReset.GradientForeColor = System.Drawing.Color.Black
        Me.btnReset.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Location = New System.Drawing.Point(572, 13)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnReset.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnReset.Size = New System.Drawing.Size(90, 30)
        Me.btnReset.TabIndex = 34
        Me.btnReset.Text = "&Reset"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(668, 13)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(90, 30)
        Me.btnExport.TabIndex = 33
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(764, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.chkConsiderZeroFilter)
        Me.gbFilterCriteria.Controls.Add(Me.nudDayTo)
        Me.gbFilterCriteria.Controls.Add(Me.nudDayFrom)
        Me.gbFilterCriteria.Controls.Add(Me.elActDaysNoOfWorked)
        Me.gbFilterCriteria.Controls.Add(Me.objlblPDate)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblYTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblDayFrom)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowLateArrivalhours)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(367, 203)
        Me.gbFilterCriteria.TabIndex = 36
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkConsiderZeroFilter
        '
        Me.chkConsiderZeroFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkConsiderZeroFilter.Location = New System.Drawing.Point(82, 177)
        Me.chkConsiderZeroFilter.Name = "chkConsiderZeroFilter"
        Me.chkConsiderZeroFilter.Size = New System.Drawing.Size(238, 17)
        Me.chkConsiderZeroFilter.TabIndex = 145
        Me.chkConsiderZeroFilter.Text = "Consider Zero As Value && Add in Filter"
        Me.chkConsiderZeroFilter.UseVisualStyleBackColor = True
        '
        'nudDayTo
        '
        Me.nudDayTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDayTo.Location = New System.Drawing.Point(258, 148)
        Me.nudDayTo.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.nudDayTo.Name = "nudDayTo"
        Me.nudDayTo.Size = New System.Drawing.Size(57, 21)
        Me.nudDayTo.TabIndex = 144
        Me.nudDayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'nudDayFrom
        '
        Me.nudDayFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDayFrom.Location = New System.Drawing.Point(152, 148)
        Me.nudDayFrom.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.nudDayFrom.Name = "nudDayFrom"
        Me.nudDayFrom.Size = New System.Drawing.Size(57, 21)
        Me.nudDayFrom.TabIndex = 143
        Me.nudDayFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'elActDaysNoOfWorked
        '
        Me.elActDaysNoOfWorked.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elActDaysNoOfWorked.Location = New System.Drawing.Point(7, 128)
        Me.elActDaysNoOfWorked.Name = "elActDaysNoOfWorked"
        Me.elActDaysNoOfWorked.Size = New System.Drawing.Size(313, 17)
        Me.elActDaysNoOfWorked.TabIndex = 139
        Me.elActDaysNoOfWorked.Text = "Actual No. Of Days Worked"
        '
        'objlblPDate
        '
        Me.objlblPDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblPDate.Location = New System.Drawing.Point(82, 61)
        Me.objlblPDate.Name = "objlblPDate"
        Me.objlblPDate.Size = New System.Drawing.Size(238, 14)
        Me.objlblPDate.TabIndex = 117
        Me.objlblPDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 230
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(82, 37)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(238, 21)
        Me.cboPeriod.TabIndex = 116
        '
        'lblYTo
        '
        Me.lblYTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYTo.Location = New System.Drawing.Point(215, 151)
        Me.lblYTo.Name = "lblYTo"
        Me.lblYTo.Size = New System.Drawing.Size(37, 15)
        Me.lblYTo.TabIndex = 113
        Me.lblYTo.Text = "To"
        Me.lblYTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDayFrom
        '
        Me.lblDayFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDayFrom.Location = New System.Drawing.Point(79, 151)
        Me.lblDayFrom.Name = "lblDayFrom"
        Me.lblDayFrom.Size = New System.Drawing.Size(67, 15)
        Me.lblDayFrom.TabIndex = 112
        Me.lblDayFrom.Text = "From"
        Me.lblDayFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowLateArrivalhours
        '
        Me.chkShowLateArrivalhours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowLateArrivalhours.Location = New System.Drawing.Point(82, 108)
        Me.chkShowLateArrivalhours.Name = "chkShowLateArrivalhours"
        Me.chkShowLateArrivalhours.Size = New System.Drawing.Size(238, 17)
        Me.chkShowLateArrivalhours.TabIndex = 111
        Me.chkShowLateArrivalhours.Text = "Show Only Late Arrival hours"
        Me.chkShowLateArrivalhours.UseVisualStyleBackColor = True
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(9, 40)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(67, 15)
        Me.lblPeriod.TabIndex = 62
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 230
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(82, 81)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(238, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(326, 81)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 53
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(9, 84)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(67, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblExpetedDays
        '
        Me.lblExpetedDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpetedDays.Location = New System.Drawing.Point(12, 40)
        Me.lblExpetedDays.Name = "lblExpetedDays"
        Me.lblExpetedDays.Size = New System.Drawing.Size(133, 15)
        Me.lblExpetedDays.TabIndex = 143
        Me.lblExpetedDays.Text = "Number Of Working Days"
        Me.lblExpetedDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'nudDaysWorked
        '
        Me.nudDaysWorked.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDaysWorked.Location = New System.Drawing.Point(151, 37)
        Me.nudDaysWorked.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.nudDaysWorked.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudDaysWorked.Name = "nudDaysWorked"
        Me.nudDaysWorked.Size = New System.Drawing.Size(46, 21)
        Me.nudDaysWorked.TabIndex = 144
        Me.nudDaysWorked.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.nudDaysWorked.Value = New Decimal(New Integer() {20, 0, 0, 0})
        '
        'dgvLeave
        '
        Me.dgvLeave.AllowUserToAddRows = False
        Me.dgvLeave.AllowUserToDeleteRows = False
        Me.dgvLeave.BackgroundColor = System.Drawing.Color.White
        Me.dgvLeave.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvLeave.ColumnHeadersHeight = 24
        Me.dgvLeave.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLeave.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhOrgLeave, Me.dgcolhCaption, Me.objdgcolhLvTypId})
        Me.dgvLeave.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLeave.Location = New System.Drawing.Point(0, 0)
        Me.dgvLeave.Name = "dgvLeave"
        Me.dgvLeave.RowHeadersVisible = False
        Me.dgvLeave.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvLeave.Size = New System.Drawing.Size(448, 130)
        Me.dgvLeave.TabIndex = 145
        '
        'dgcolhOrgLeave
        '
        Me.dgcolhOrgLeave.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhOrgLeave.HeaderText = "Leave Name"
        Me.dgcolhOrgLeave.Name = "dgcolhOrgLeave"
        Me.dgcolhOrgLeave.ReadOnly = True
        '
        'dgcolhCaption
        '
        Me.dgcolhCaption.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhCaption.HeaderText = "Display Name"
        Me.dgcolhCaption.Name = "dgcolhCaption"
        '
        'objdgcolhLvTypId
        '
        Me.objdgcolhLvTypId.HeaderText = "objdgcolhLvTypId"
        Me.objdgcolhLvTypId.Name = "objdgcolhLvTypId"
        Me.objdgcolhLvTypId.Visible = False
        '
        'gbOtherSetting
        '
        Me.gbOtherSetting.BorderColor = System.Drawing.Color.Black
        Me.gbOtherSetting.Checked = False
        Me.gbOtherSetting.CollapseAllExceptThis = False
        Me.gbOtherSetting.CollapsedHoverImage = Nothing
        Me.gbOtherSetting.CollapsedNormalImage = Nothing
        Me.gbOtherSetting.CollapsedPressedImage = Nothing
        Me.gbOtherSetting.CollapseOnLoad = False
        Me.gbOtherSetting.Controls.Add(Me.cboAllocation)
        Me.gbOtherSetting.Controls.Add(Me.lblAllocation)
        Me.gbOtherSetting.Controls.Add(Me.objpnlLvTypes)
        Me.gbOtherSetting.Controls.Add(Me.lnkSaveSelection)
        Me.gbOtherSetting.Controls.Add(Me.nudDaysWorked)
        Me.gbOtherSetting.Controls.Add(Me.lblExpetedDays)
        Me.gbOtherSetting.ExpandedHoverImage = Nothing
        Me.gbOtherSetting.ExpandedNormalImage = Nothing
        Me.gbOtherSetting.ExpandedPressedImage = Nothing
        Me.gbOtherSetting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOtherSetting.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOtherSetting.HeaderHeight = 25
        Me.gbOtherSetting.HeaderMessage = ""
        Me.gbOtherSetting.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbOtherSetting.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOtherSetting.HeightOnCollapse = 0
        Me.gbOtherSetting.LeftTextSpace = 0
        Me.gbOtherSetting.Location = New System.Drawing.Point(385, 66)
        Me.gbOtherSetting.Name = "gbOtherSetting"
        Me.gbOtherSetting.OpenHeight = 300
        Me.gbOtherSetting.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOtherSetting.ShowBorder = True
        Me.gbOtherSetting.ShowCheckBox = False
        Me.gbOtherSetting.ShowCollapseButton = False
        Me.gbOtherSetting.ShowDefaultBorderColor = True
        Me.gbOtherSetting.ShowDownButton = False
        Me.gbOtherSetting.ShowHeader = True
        Me.gbOtherSetting.Size = New System.Drawing.Size(468, 203)
        Me.gbOtherSetting.TabIndex = 146
        Me.gbOtherSetting.Temp = 0
        Me.gbOtherSetting.Text = "Report Display Setting"
        Me.gbOtherSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboAllocation
        '
        Me.cboAllocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAllocation.DropDownWidth = 230
        Me.cboAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAllocation.FormattingEnabled = True
        Me.cboAllocation.Location = New System.Drawing.Point(276, 37)
        Me.cboAllocation.Name = "cboAllocation"
        Me.cboAllocation.Size = New System.Drawing.Size(182, 21)
        Me.cboAllocation.TabIndex = 149
        '
        'lblAllocation
        '
        Me.lblAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAllocation.Location = New System.Drawing.Point(203, 40)
        Me.lblAllocation.Name = "lblAllocation"
        Me.lblAllocation.Size = New System.Drawing.Size(67, 15)
        Me.lblAllocation.TabIndex = 148
        Me.lblAllocation.Text = "Allocation"
        Me.lblAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnlLvTypes
        '
        Me.objpnlLvTypes.Controls.Add(Me.dgvLeave)
        Me.objpnlLvTypes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlLvTypes.Location = New System.Drawing.Point(10, 64)
        Me.objpnlLvTypes.Name = "objpnlLvTypes"
        Me.objpnlLvTypes.Size = New System.Drawing.Size(448, 130)
        Me.objpnlLvTypes.TabIndex = 146
        '
        'lnkSaveSelection
        '
        Me.lnkSaveSelection.BackColor = System.Drawing.Color.Transparent
        Me.lnkSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSaveSelection.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSaveSelection.Location = New System.Drawing.Point(364, 4)
        Me.lnkSaveSelection.Name = "lnkSaveSelection"
        Me.lnkSaveSelection.Size = New System.Drawing.Size(94, 17)
        Me.lnkSaveSelection.TabIndex = 36
        Me.lnkSaveSelection.TabStop = True
        Me.lnkSaveSelection.Text = "Save Setting"
        Me.lnkSaveSelection.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmAttendanceTrackerSummaryReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(865, 570)
        Me.Controls.Add(Me.gbOtherSetting)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmAttendanceTrackerSummaryReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Attendance Tracker Summary Report"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        CType(Me.nudDayTo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDayFrom, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDaysWorked, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLeave, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbOtherSetting.ResumeLayout(False)
        Me.objpnlLvTypes.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents objbtnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents objbtnLanguage As eZee.Common.eZeeLightButton
    Friend WithEvents btnReset As eZee.Common.eZeeLightButton
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents chkShowLateArrivalhours As System.Windows.Forms.CheckBox
    Friend WithEvents lblDayFrom As System.Windows.Forms.Label
    Friend WithEvents lblYTo As System.Windows.Forms.Label
    Friend WithEvents objlblPDate As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents elActDaysNoOfWorked As eZee.Common.eZeeLine
    Friend WithEvents nudDaysWorked As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblExpetedDays As System.Windows.Forms.Label
    Friend WithEvents dgvLeave As System.Windows.Forms.DataGridView
    Friend WithEvents gbOtherSetting As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkSaveSelection As System.Windows.Forms.LinkLabel
    Friend WithEvents dgcolhOrgLeave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCaption As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhLvTypId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objpnlLvTypes As System.Windows.Forms.Panel
    Friend WithEvents nudDayTo As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudDayFrom As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkConsiderZeroFilter As System.Windows.Forms.CheckBox
    Friend WithEvents cboAllocation As System.Windows.Forms.ComboBox
    Friend WithEvents lblAllocation As System.Windows.Forms.Label
End Class
