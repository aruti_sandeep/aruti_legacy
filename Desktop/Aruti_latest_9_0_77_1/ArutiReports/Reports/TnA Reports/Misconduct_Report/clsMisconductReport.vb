﻿'************************************************************************************************************************************
'Class Name :clsMisconductReport.vb
'Purpose    :
'Date       :23 Mar 2022
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
'''Purpose: Report Generation Class 
''' Developer: Sandeep Sharma
''' </summary>`
Public Class clsMisconductReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsDailyTimeSheet"
    Private mstrReportId As String = enArutiReport.EmployeeMisconductReport
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrAdvanceFilter As String = ""
    Private mstrGroupName As String = ""
    Private mintLocationId As Integer = 0
    Private mstrLocation As String = ""
    Private mblnAddUACFilter As Boolean = True
    Private mintEmployeeId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mdtDateFrom As Date = Nothing
    Private mdtDateTo As Date = Nothing
    Private mintMisconductDays As Integer = 0

#End Region

#Region " Properties "

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _LocationId() As Integer
        Set(ByVal value As Integer)
            mintLocationId = value
        End Set
    End Property

    Public WriteOnly Property _Location() As String
        Set(ByVal value As String)
            mstrLocation = value
        End Set
    End Property

    Public WriteOnly Property _GroupName() As String
        Set(ByVal value As String)
            mstrGroupName = value
        End Set
    End Property

    Public WriteOnly Property _ApplyAddUACFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnAddUACFilter = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _DateFrom() As Date
        Set(ByVal value As Date)
            mdtDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _DateTo() As Date
        Set(ByVal value As Date)
            mdtDateTo = value
        End Set
    End Property

    Public WriteOnly Property _MisconductDays() As Integer
        Set(ByVal value As Integer)
            mintMisconductDays = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            Rpt = Nothing
            mintUserUnkid = -1
            mintCompanyUnkid = -1
            mstrAdvanceFilter = ""
            mstrGroupName = ""
            mintLocationId = 0
            mstrLocation = ""
            mblnAddUACFilter = True
            mintEmployeeId = 0
            mstrEmployeeName = ""
            mdtDateFrom = Nothing
            mdtDateTo = Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try
            objDataOperation.AddParameter("@start_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDateFrom))
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDateTo))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 101, "From Date: ") & " " & mdtDateFrom.ToShortDateString & " " & _
                               Language.getMessage(mstrModuleName, 102, "To Date: ") & " " & mdtDateTo.ToShortDateString & " "

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 103, " Employee : ") & " " & mstrEmployeeName & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, " ", False)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub IsAbsentProcessDone(ByRef sMsg As String, ByVal dt1 As Date, ByVal dt2 As Date, Optional ByVal intEmpId As Integer = 0)
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Dim iMsg As String = ""
        Try
            Using objOpr As New clsDataOperation

                StrQ = "SELECT DISTINCT login_date,isabsentprocess FROM tnalogin_summary WHERE CONVERT(NVARCHAR(8),login_date,112) BETWEEN @dt1 AND @dt2 "

                If intEmpId > 0 Then
                    StrQ &= " AND employeeunkid = @EId "
                    objOpr.AddParameter("@EId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                End If

                objOpr.AddParameter("@dt1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dt1.Date).ToString)
                objOpr.AddParameter("@dt2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dt2.Date).ToString)

                dsList = objOpr.ExecQuery(StrQ, "list")

                If objOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objOpr.ErrorNumber & " : " & objOpr.ErrorMessage)
                    Throw exForce
                End If

            End Using

            If dsList.Tables.Count > 0 AndAlso dsList.Tables(0).Rows.Count > 0 Then
                If dsList.Tables(0).AsEnumerable().Where(Function(x) CBool(x.Field(Of Boolean)("isabsentprocess")) = False).Count > 0 Then
                    iMsg = Language.getMessage(mstrModuleName, 105, "Sorry, Absent process is not done for the selected date range. Please do absent process in order to generate report.")
                End If
            Else
                iMsg = Language.getMessage(mstrModuleName, 105, "Sorry, Absent process is not done for the selected date range. Please do absent process in order to generate report.")
            End If

            sMsg = iMsg

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsAbsentProcessDone; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnAddUACFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            If mstrAdvanceFilter.Trim.Length > 0 Then Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            StrQ = "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                   "    DROP TABLE #Results " & _
                   " " & _
                   "IF OBJECT_ID('tempdb..#FResult') IS NOT NULL " & _
                   "    DROP TABLE #FResult " & _
                   " " & _
                   " " & _
                   " " & _
                   ";WITH " & _
                   "AllDays " & _
                   "AS " & _
                   "( " & _
                   "    SELECT CAST(@start_date AS DATETIME) AS [Date] " & _
                   "    UNION ALL " & _
                   "    SELECT DATEADD(DAY, 1, [Date]) " & _
                   "    FROM  AllDays " & _
                   "    WHERE [Date] < CAST(@end_date AS DATETIME) " & _
                   ") " & _
                   "SELECT " & _
                   "     [Date] " & _
                   "    ,hremployee_master.employeeunkid " & _
                   "    ,A.shiftunkid " & _
                   "    ,DATEPART(dw,[Date])-1 AS DayId " & _
                   "    ,B.isweekend " & _
                   "    ,ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                   "    ,ISNULL(dept.name, '') AS department "
            Select Case mintLocationId

                Case enAllocation.BRANCH

                    StrQ &= ", ISNULL(hrstation_master.name, '') AS location "

                Case enAllocation.DEPARTMENT_GROUP

                    StrQ &= ", ISNULL(hrdepartment_group_master.name, '') AS location "

                Case enAllocation.SECTION_GROUP

                    StrQ &= ", ISNULL(hrsectiongroup_master.name, '') AS location "

                Case enAllocation.SECTION

                    StrQ &= ", ISNULL(hrsection_master.name, '') AS location "

                Case enAllocation.UNIT_GROUP

                    StrQ &= ", ISNULL(hrunitgroup_master.name, '') AS location "

                Case enAllocation.UNIT

                    StrQ &= ", ISNULL(hrunit_master.name, '') AS location "

                Case enAllocation.TEAM

                    StrQ &= ", ISNULL(hrteam_master.name, '') AS location "

                Case enAllocation.JOB_GROUP

                    StrQ &= ", ISNULL(jg.name, '') AS location "

                Case enAllocation.JOBS

                    StrQ &= ", ISNULL(jb.job_name, '') AS location "

                Case enAllocation.CLASS_GROUP

                    StrQ &= ", ISNULL(hrclassgroup_master.name, '') AS location "

                Case enAllocation.CLASSES

                    StrQ &= ", ISNULL(hrclasses_master.name, '') AS location "

                Case enAllocation.COST_CENTER

                    StrQ &= ", ISNULL(prcostcenter_master.costcentername, '') AS location "

                Case Else
                    StrQ &= ", '' AS location "

            End Select

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= "INTO #Results " & _
                   " " & _
                   "FROM AllDays " & _
                   "JOIN hremployee_master ON 1 = 1 "
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If


            StrQ &= "JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         employeeunkid " & _
                    "        ,effectivedate " & _
                    "        ,shiftunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_shift_tran " & _
                    "    WHERE isvoid = 0 " & _
                    ") AS A ON A.effectivedate <= AllDays.[Date] AND rno = 1 AND hremployee_master.employeeunkid = A.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         shiftunkid " & _
                    "        ,dayid " & _
                    "        ,isweekend " & _
                    "    FROM tnashift_tran " & _
                    ") AS B ON B.shiftunkid=A.shiftunkid AND B.dayid = DATEPART(dw,[Date])-1 "

            StrQ &= " LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_transfer_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    " LEFT JOIN hrdepartment_master dept ON Alloc.departmentunkid = dept.departmentunkid "
            Select Case mintLocationId

                Case enAllocation.BRANCH

                    StrQ &= " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = Alloc.stationunkid "

                Case enAllocation.DEPARTMENT_GROUP

                    StrQ &= " LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = Alloc.deptgroupunkid "

                Case enAllocation.SECTION_GROUP

                    StrQ &= " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = Alloc.sectiongroupunkid "

                Case enAllocation.SECTION

                    StrQ &= " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = Alloc.sectionunkid "

                Case enAllocation.UNIT_GROUP

                    StrQ &= " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = Alloc.unitgroupunkid "

                Case enAllocation.UNIT

                    StrQ &= " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = Alloc.unitunkid "

                Case enAllocation.TEAM

                    StrQ &= " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = Alloc.teamunkid "

                Case enAllocation.JOB_GROUP

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           jobgroupunkid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_categorization_tran " & _
                                  "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS Jobgrp ON Jobgrp.employeeunkid = hremployee_master.employeeunkid AND Jobgrp.rno = 1 " & _
                                  " LEFT JOIN hrjobgroup_master jg ON Jobgrp.jobgroupunkid = jg.jobgroupunkid "

                Case enAllocation.JOBS

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           jobunkid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_categorization_tran " & _
                                  "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                  " LEFT JOIN hrjob_master jb ON Jobs.jobunkid = jb.jobunkid "

                Case enAllocation.CLASS_GROUP

                    StrQ &= " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid "

                Case enAllocation.CLASSES

                    StrQ &= " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = Alloc.classunkid "

                Case enAllocation.COST_CENTER

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           cctranheadvalueid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_cctranhead_tran " & _
                                  "       WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                                  " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = CC.cctranheadvalueid "

            End Select


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If


            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= " WHERE  1=1  "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If mintEmployeeId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = " & mintEmployeeId
            End If
            StrQ &= "ORDER BY hremployee_master.employeeunkid,[Date] " & _
                    "OPTION (MAXRECURSION 0) " & _
                    " " & _
                    " " & _
                    " " & _
                    "SELECT " & _
                    "     #Results.* " & _
                    "    ,CASE WHEN C.hDate = CONVERT(CHAR(8),[Date],112) THEN 1 ELSE 0 END AS isholiday " & _
                    "    ,CASE WHEN D.lDate = CONVERT(CHAR(8),[Date],112) AND aydate > strDate THEN 0 " & _
                    "          WHEN D.lDate = CONVERT(CHAR(8),[Date],112) AND aydate <= strDate THEN 1 " & _
                    "     ELSE 0 END AS isleave " & _
                    "    ,CASE WHEN E.dDate = CONVERT(CHAR(8),[Date],112) THEN 1 ELSE 0 END AS isdayoff " & _
                    "    ,CASE WHEN F.fDate = CONVERT(CHAR(8),[Date],112) THEN 1 ELSE 0 END AS fraction " & _
                    "INTO #FResult " & _
                    "FROM #Results " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         employeeunkid " & _
                    "        ,CONVERT(CHAR(8),holidaydate,112) AS hDate " & _
                    "    FROM lvemployee_holiday EH " & _
                    "        JOIN lvholiday_master AS HL ON HL.holidayunkid = EH.holidayunkid " & _
                    "    WHERE HL.isactive = 1 " & _
                    ") AS C ON C.employeeunkid = #Results.employeeunkid " & _
                    "AND C.hDate = CONVERT(CHAR(8),[Date],112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         lvleaveform.employeeunkid " & _
                    "        ,CONVERT(CHAR(8),leavedate,112) AS lDate " & _
                    "        ,CONVERT(NVARCHAR(8), applydate, 112) as aydate " & _
                    "        ,CONVERT(NVARCHAR(8), ISNULL(approve_stdate, lvleaveform.startdate), 112) as strDate " & _
                    "    FROM lvleaveIssue_tran " & _
                    "        JOIN lvleaveIssue_master on lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                    "        JOIN lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid " & _
                    "    WHERE lvleaveIssue_tran.isvoid = 0 and lvleaveIssue_master.isvoid = 0 " & _
                    "       AND CONVERT(NVARCHAR(8), applydate, 112) BETWEEN @start_date AND @end_date " & _
                    "UNION " & _
                    "    SELECT " & _
                    "         employeeunkid " & _
                    "        ,leavedate " & _
                    "        ,CONVERT(NVARCHAR(8), applydate, 112) as aydate " & _
                    "        ,CONVERT(NVARCHAR(8), ISNULL(approve_stdate, lvleaveform.startdate), 112) as strDate " & _
                    "    FROM lvleaveform " & _
                    "        JOIN lvleaveday_fraction ON lvleaveday_fraction.formunkid = lvleaveform.formunkid AND lvleaveday_fraction.isvoid = 0 AND lvleaveday_fraction.approverunkid < = 0 " & _
                    "    WHERE lvleaveform.isvoid = 0 AND CONVERT(NVARCHAR(8),applydate,112) BETWEEN @start_date AND @end_date " & _
                    "        AND CONVERT(NVARCHAR(8),ISNULL(approve_stdate,startdate),112) < CONVERT(NVARCHAR(8),applydate,112) " & _
                    "        AND CONVERT(NVARCHAR(8),ISNULL(approve_stdate,startdate),112) BETWEEN @start_date AND @end_date " & _
                    "        AND statusunkid = 2 " & _
                    ") AS D ON D.employeeunkid = #Results.employeeunkid " & _
                    "AND D.lDate = CONVERT(CHAR(8),[Date],112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         employeeunkid " & _
                    "        ,CONVERT(CHAR(8),dayoffdate,112) AS dDate " & _
                    "    FROM hremployee_dayoff_tran " & _
                    ") AS E ON E.employeeunkid  =  #Results.employeeunkid " & _
                    "AND E.dDate = CONVERT(CHAR(8),[Date],112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         ROW_NUMBER() OVER (PARTITION BY employeeunkid Order by login_date) as RowNumber " & _
                    "        ,CONVERT(CHAR(8),login_date,112) AS fDate " & _
                    "        ,login_date,employeeunkid,lvdayfraction,isabsentprocess " & _
                    "    FROM tnalogin_summary WHERE CONVERT(NVARCHAR(8),login_date,112) BETWEEN @start_date AND @end_date " & _
                    "    AND lvdayfraction > 0 AND isabsentprocess = 1 " & _
                    ") AS F ON F.employeeunkid  =  #Results.employeeunkid " & _
                    "AND F.fDate = CONVERT(CHAR(8),[Date],112) " & _
                    "WHERE 1 = 1 " & _
                    "AND CASE WHEN C.hDate = CONVERT(CHAR(8),[Date],112) THEN 1 ELSE 0 END = 0 " & _
                    "AND CASE WHEN D.lDate = CONVERT(CHAR(8),[Date],112) AND aydate > strDate THEN 0 " & _
                    "         WHEN D.lDate = CONVERT(CHAR(8),[Date],112) AND aydate <= strDate THEN 1 " & _
                    "    ELSE 0 END = 0 " & _
                    "AND CASE WHEN E.dDate = CONVERT(CHAR(8),[Date],112) THEN 1 ELSE 0 END = 0 " & _
                    "AND CASE WHEN F.fDate = CONVERT(CHAR(8),[Date],112) THEN 1 ELSE 0 END = 1 " & _
                    "ORDER BY #Results.employeeunkid,#Results.Date " & _
                    " " & _
                    " " & _
                    " " & _
                    "SELECT " & _
                    " ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY Date) AS RowNumber " & _
                    ",Date AS iDate " & _
                    ",* " & _
                    ",DATEPART(DD,DATE) AS iDay " & _
                    ",CAST(0 AS BIT) AS iVisible " & _
                    ",CAST(0 AS BIT) AS iMatched " & _
                    "FROM #FResult " & _
                    "ORDER BY employeeunkid,Date " & _
                    "/* ;WITH tblDifference AS " & _
                    "( " & _
                        "SELECT " & _
                          "ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY Date) AS RowNumber " & _
                         ",* " & _
                         "FROM #FResult " & _
                    ") " & _
                    " " & _
                    "SELECT " & _
                          "ISNULL(Prv.Date,Cur.Date) AS iDate " & _
                         ",DATEDIFF(DAY,ISNULL(Prv.Date,Cur.Date),Cur.Date) AS iMatch " & _
                         ",Cur.* " & _
                    "FROM tblDifference Cur " & _
                         "LEFT OUTER JOIN tblDifference Prv On Cur.RowNumber = Prv.RowNumber + 1 AND Cur.employeeunkid = Prv.employeeunkid " & _
                    "WHERE 1 = 1 AND DATEDIFF(DAY,ISNULL(Prv.Date,Cur.Date),Cur.Date) IN (1) " & _
                    "ORDER BY Cur.employeeunkid,Cur.Date */"

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim xTable As DataTable = dsList.Tables(0).Clone
            If dsList.Tables.Count > 0 AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim eEmpLst As New List(Of Integer)
                eEmpLst = dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid")).Distinct().ToList()

                'DISPLAY LOGIC : 1
                'For Each xEmpId In eEmpLst
                '    Dim iCount As Integer = 0 : Dim xFiNCnt As Integer = 0 : Dim xStIdx As Integer = -1
                '    Dim intMatchIdx As Integer = 0
                '    Dim iRow() As DataRow = dsList.Tables(0).Select("employeeunkid = " & xEmpId)
                '    For idx As Integer = 0 To iRow.Length - 1
                '        Dim dt1, dt2 As DateTime
                '        dt1 = iRow(idx)("iDate")
                '        Try
                '            dt2 = iRow(idx + 1)("iDate")
                '            If DateDiff(DateInterval.Day, dt1, dt2) <= 1 Then
                '                If xStIdx < 0 Then xStIdx = idx
                '                If iCount <= 0 Then
                '                    iCount += 2
                '                Else
                '                    iCount += 1
                '                End If
                '            Else
                '                iCount = 0
                '                xStIdx = -1
                '            End If
                '            If iCount = mintMisconductDays Then
                '                xFiNCnt = idx + 1
                '            End If
                '        Catch ex As Exception
                '            dt2 = dt1
                '            If DateDiff(DateInterval.Day, dt1, dt2) <= 1 Then
                '                If xStIdx < 0 Then xStIdx = idx
                '                iCount += 1
                '            Else
                '                iCount = 0
                '                xStIdx = -1
                '            End If
                '            If iCount = mintMisconductDays Then
                '                xFiNCnt = idx
                '            End If
                '        End Try
                '        If xFiNCnt > 0 Then
                '            For i As Integer = xStIdx To xFiNCnt
                '                xTable.ImportRow(iRow(i))
                '            Next
                '            xStIdx = -1
                '            xFiNCnt = 0
                '            iCount = 0
                '        End If
                '    Next
                'Next


                'DISPLAY LOGIC : 2 iVisible = True
                'For Each xEmpId In eEmpLst
                '    Dim intDays As Integer = DateDiff(DateInterval.Day, mdtDateFrom, mdtDateTo) + 1
                '    Dim iMatchDay As Integer
                '    For iDay As Integer = 1 To intDays
                '        iMatchDay = iDay
                '        If intDays = iMatchDay Then iMatchDay += 1
                '        If dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("iDay") = iMatchDay And x.Field(Of Integer)("employeeunkid") = xEmpId).Count <= 0 Then
                '            Dim itmp() As DataRow = dsList.Tables(0).Select("employeeunkid = " & xEmpId & "AND iDay <= " & iMatchDay & " AND iVisible = True AND iMatched = False")
                '            If itmp.Length < mintMisconductDays Then
                '                For Each trow As DataRow In itmp
                '                    trow("iVisible") = False
                '                Next
                '            Else
                '                For Each trow As DataRow In itmp
                '                    trow("iMatched") = True
                '                Next
                '            End If
                '        End If
                '    Next
                'Next

                'DISPLAY LOGIC : 3
                For Each xEmpId In eEmpLst                    
                    Dim iDays As List(Of Integer) = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmpId).Select(Function(x) x.Field(Of Integer)("iDay")).ToList()
                    If iDays.Count >= mintMisconductDays Then
                        Dim iMaxDays = iDays.Max() + 1
                        Dim iMissing As List(Of Integer) = Enumerable.Range(1, iMaxDays).Except(iDays).ToList()
                        For iDay As Integer = 0 To iMissing.Count - 1
                            Dim itmp() As DataRow = Nothing
                            If iDay <= 0 Then
                                itmp = dsList.Tables(0).Select("employeeunkid = " & xEmpId & "AND iDay <= " & iMissing(iDay) & " AND iMatched = False")
                            Else
                                itmp = dsList.Tables(0).Select("employeeunkid = " & xEmpId & "AND iDay > " & iMissing(iDay - 1) & " AND iDay <= " & iMissing(iDay) & " AND iMatched = False")
                            End If
                            If itmp.Length >= mintMisconductDays Then
                                For Each trow As DataRow In itmp
                                    trow("iMatched") = True
                                Next
                            End If
                        Next
                    End If
                Next
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            xTable = New DataView(dsList.Tables(0), "iMatched = True", "iDay", DataViewRowState.CurrentRows).ToTable.Copy()
            For Each dtRow As DataRow In xTable.Rows
                Dim rpt_Row As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("GName").ToString().Trim()
                rpt_Row.Item("Column2") = dtRow.Item("EmpCodeName").ToString().Trim()
                rpt_Row.Item("Column3") = CDate(dtRow.Item("iDate")).ToShortDateString()
                rpt_Row.Item("Column4") = dtRow.Item("department").ToString().Trim()
                rpt_Row.Item("Column5") = dtRow.Item("location").ToString().Trim()

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptEmployeeMisconductReport
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()


            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 15, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 16, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 17, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 13, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            'If mblnEachEmployeeOnPage = True Then
            '    ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", True)
            '    objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "if Previous ({ArutiTable.Column1}) <> {ArutiTable.Column1} then 0 Else 1 "
            'Else
            ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", False)
            objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "0"
            'End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 4, "Sr.No"))
            Call ReportFunction.TextChange(objRpt, "txtLoginDate", Language.getMessage(mstrModuleName, 5, "Misconduct Date"))
            Call ReportFunction.TextChange(objRpt, "txtLocation", Language.getMessage(mstrModuleName, 6, "Location"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 7, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)
            Call ReportFunction.TextChange(objRpt, "txtEmployee", Language.getMessage(mstrModuleName, 3, "Employee: "))
            Call ReportFunction.TextChange(objRpt, "txtTotalEmployee", Language.getMessage(mstrModuleName, 14, "Total Employee : "))

            If mintViewIndex < 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If


            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 18, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 19, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)
            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 3, "Employee:")
            Language.setMessage(mstrModuleName, 4, "Sr.No")
            Language.setMessage(mstrModuleName, 5, "Misconduct Date")
            Language.setMessage(mstrModuleName, 6, "Location")
            Language.setMessage(mstrModuleName, 7, "Department")
            Language.setMessage(mstrModuleName, 13, "Received By :")
            Language.setMessage(mstrModuleName, 14, "Total Employee :")
            Language.setMessage(mstrModuleName, 15, "Prepared By :")
            Language.setMessage(mstrModuleName, 16, "Checked By :")
            Language.setMessage(mstrModuleName, 17, "Approved By :")
            Language.setMessage(mstrModuleName, 18, "Printed By :")
            Language.setMessage(mstrModuleName, 19, "Printed Date :")
            Language.setMessage(mstrModuleName, 101, "From Date:")
            Language.setMessage(mstrModuleName, 102, "To Date:")
            Language.setMessage(mstrModuleName, 103, " Employee :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
