'************************************************************************************************************************************
'Class Name : clsTRAEmpMispunchReport.vb
'Purpose    :
'Date       :03-Mar-2022
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter
''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Pinkal
''' </summary>
''' 
Public Class clsTRAEmpTimesheetComplianceReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsTRAEmpTimesheetComplianceReport"
    Private mstrReportId As String = enArutiReport.Employee_Timesheet_Compliance_Report '287
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mdtFromDate As DateTime = Nothing
    Private mdtToDate As DateTime = Nothing
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintLocationId As Integer = 0
    Private mstrLocation As String = ""
    Private mstrOrderByQuery As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mblnIncludeWeekend As Boolean = False
    Private mblnIncludeHoliday As Boolean = False
    Private mblnIncludeDayOff As Boolean = False
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mintYearID As Integer = -1
    Private mblnIncludeAccessFilterQry As Boolean = True

#End Region

#Region " Properties "


    Public WriteOnly Property _FromDate() As DateTime
        Set(ByVal value As DateTime)
            mdtFromDate = value
        End Set
    End Property

    Public WriteOnly Property _ToDate() As DateTime
        Set(ByVal value As DateTime)
            mdtToDate = value
        End Set
    End Property

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _LocationId() As Integer
        Set(ByVal value As Integer)
            mintLocationId = value
        End Set
    End Property

    Public WriteOnly Property _Location() As String
        Set(ByVal value As String)
            mstrLocation = value
        End Set
    End Property

    Public WriteOnly Property _IncludeWeekend() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeWeekend = value
        End Set
    End Property

    Public WriteOnly Property _IncludeHoliday() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeHoliday = value
        End Set
    End Property

    Public WriteOnly Property _IncludeDayOff() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeDayOff = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _AdvanceFilter() As String
        Set(ByVal value As String)
            mstrAdvanceFilter = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _YearId() As Integer
        Set(ByVal value As Integer)
            mintYearID = value
        End Set
    End Property

    Public Property _IncludeAccessFilterQry() As Boolean
        Get
            Return mblnIncludeAccessFilterQry
        End Get
        Set(ByVal value As Boolean)
            mblnIncludeAccessFilterQry = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mdtFromDate = Nothing
            mdtToDate = Nothing
            mintEmpId = 0
            mstrEmpName = ""
            mintLocationId = 0
            mstrLocation = ""
            mblnIncludeWeekend = False
            mblnIncludeHoliday = False
            mblnIncludeDayOff = False
            mstrOrderByQuery = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrOrderByQuery = ""
            mstrAdvanceFilter = ""
            mstrAnalysis_OrderBy_GName = ""
            mstrReport_GroupName = ""
            mblnIncludeAccessFilterQry = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""

        Try


            objDataOperation.AddParameter("@TotalDays", SqlDbType.Int, eZeeDataType.INT_SIZE, DateDiff(DateInterval.Day, mdtFromDate.Date, mdtToDate.Date) + 1)

            objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtFromDate))
            objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtToDate))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 22, " From Date: ") & " " & mdtFromDate.ToShortDateString & " "
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 23, " To Date: ") & " " & mdtToDate.ToShortDateString & " "

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, "Employee : ") & " " & mstrEmpName & " "
            End If

            If Me.OrderByQuery <> "" Then
                If mintViewIndex > 0 Then
                    mstrOrderByQuery &= "  ORDER BY GName," & Me.OrderByQuery
                Else
                    mstrOrderByQuery &= "  ORDER BY " & Me.OrderByQuery
                End If

                Me._FilterTitle &= Language.getMessage(mstrModuleName, 20, " Order By : ") & " " & Me.OrderByDisplay
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'')", Language.getMessage(mstrModuleName, 1, "Employee")))
            iColumn_DetailReport.Add(New IColumn("CONVERT(VARCHAR(8),logintran.logindate,112)", Language.getMessage(mstrModuleName, 2, "Date")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub Generate_DetailReport(ByVal strDatabaseName As String, _
                                          ByVal intUserUnkid As Integer, _
                                          ByVal intYearUnkid As Integer, _
                                          ByVal intCompanyUnkid As Integer, _
                                          ByVal dtPeriodStart As Date, _
                                          ByVal dtPeriodEnd As Date, _
                                          ByVal strUserModeSetting As String, _
                                          ByVal blnOnlyApproved As Boolean, _
                                          ByVal xExportReportPath As String, _
                                          ByVal xOpenReportAfterExport As Boolean)
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try


            If intCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If intUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid


            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            If mblnIncludeAccessFilterQry Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)


            StrQ = " SELECT hremployee_master.employeeunkid  "

            If mintViewIndex > 0 Then
                Dim ar() As String = mstrAnalysis_Fields.Trim().Substring(1).Split(CChar(","))
                Dim mstrAnalysisFields As String = ""
                If ar.Length > 0 Then
                    mstrAnalysisFields = ar(1).ToString()
                End If
                StrQ &= ",  ROW_NUMBER() OVER (ORDER BY " & mstrAnalysisFields.Replace("AS GName", "") & ", ISNULL(hremployee_master.employeecode, '')) AS SrNo "
            Else
                StrQ &= ",  ROW_NUMBER() OVER (ORDER BY ISNULL(hremployee_master.employeecode, '')) AS SrNo "
            End If

            StrQ &= ", ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                       ", ISNULL(dept.name, '') AS department "

            Select Case mintLocationId

                Case enAllocation.BRANCH

                    StrQ &= ", ISNULL(hrstation_master.name, '') AS location "

                Case enAllocation.DEPARTMENT_GROUP

                    StrQ &= ", ISNULL(hrdepartment_group_master.name, '') AS location "

                Case enAllocation.SECTION_GROUP

                    StrQ &= ", ISNULL(hrsectiongroup_master.name, '') AS location "

                Case enAllocation.SECTION

                    StrQ &= ", ISNULL(hrsection_master.name, '') AS location "

                Case enAllocation.UNIT_GROUP

                    StrQ &= ", ISNULL(hrunitgroup_master.name, '') AS location "

                Case enAllocation.UNIT

                    StrQ &= ", ISNULL(hrunit_master.name, '') AS location "

                Case enAllocation.TEAM

                    StrQ &= ", ISNULL(hrteam_master.name, '') AS location "

                Case enAllocation.JOB_GROUP

                    StrQ &= ", ISNULL(jg.name, '') AS location "

                Case enAllocation.JOBS

                    StrQ &= ", ISNULL(jb.job_name, '') AS location "

                Case enAllocation.CLASS_GROUP

                    StrQ &= ", ISNULL(hrclassgroup_master.name, '') AS location "

                Case enAllocation.CLASSES

                    StrQ &= ", ISNULL(hrclasses_master.name, '') AS location "

                Case enAllocation.COST_CENTER

                    StrQ &= ", ISNULL(prcostcenter_master.costcentername, '') AS location "

            End Select


            StrQ &= ", CAST(ISNULL(pr.prcount,0) AS NVARCHAR(MAX)) + SPACE(2) +  '(' + CAST(CAST((100 *  ISNULL(pr.prcount,0)) / CAST((@TotalDays - ISNULL(wk.Wkcnt,0) - ISNULL(HL.HLcnt,0) - ISNULL(DOff.DayOffcnt,0)) AS DECIMAL(6,2)) AS DECIMAL(6,2)) AS NVARCHAR(MAX)) + '%)'   AS PrCnt " & _
                         ", CAST(ISNULL(ab.absentcnt,0) AS NVARCHAR(MAX)) + SPACE(2) +  '(' + CAST(CAST((100 *  ISNULL(ab.absentcnt,0)) / CAST((@TotalDays - ISNULL(wk.Wkcnt,0) - ISNULL(HL.HLcnt,0) - ISNULL(DOff.DayOffcnt,0)) AS DECIMAL(6,2)) AS DECIMAL(6,2)) AS NVARCHAR(MAX)) + '%)' AS absentcnt " & _
                         ", CAST(ISNULL(lt.LateComingCnt,0) AS NVARCHAR(MAX)) + SPACE(2) +  '(' + CAST(CAST((100 *  ISNULL(lt.LateComingCnt,0)) / CAST((@TotalDays - ISNULL(wk.Wkcnt,0) - ISNULL(HL.HLcnt,0) - ISNULL(DOff.DayOffcnt,0)) AS DECIMAL(6,2)) AS DECIMAL(6,2)) AS NVARCHAR(MAX)) + '%)' AS LateComingCnt " & _
                         ", CAST(ISNULL(er.ErgoingCnt,0) AS NVARCHAR(MAX)) + SPACE(2) + '(' + CAST(CAST((100 *  ISNULL(er.ErgoingCnt,0)) / CAST((@TotalDays - ISNULL(wk.Wkcnt,0) - ISNULL(HL.HLcnt,0) - ISNULL(DOff.DayOffcnt,0)) AS DECIMAL(6,2)) AS DECIMAL(6,2)) AS NVARCHAR(MAX)) + '%)' AS ErgoingCnt " & _
                         ", CAST(ISNULL(mp.Mispunchcnt,0) AS NVARCHAR(MAX)) + SPACE(2) + '(' + CAST(CAST((100 *  ISNULL(mp.Mispunchcnt,0)) / CAST((@TotalDays - ISNULL(wk.Wkcnt,0) - ISNULL(HL.HLcnt,0) - ISNULL(DOff.DayOffcnt,0)) AS DECIMAL(6,2)) AS DECIMAL(6,2)) AS NVARCHAR(MAX)) + '%)' AS Mispunchcnt " & _
                         ", CAST(ISNULL(lv.Leavecnt,0) AS NVARCHAR(MAX)) + SPACE(2) + '(' + CAST(CAST((100 *  ISNULL(lv.Leavecnt,0)) / CAST((@TotalDays - ISNULL(wk.Wkcnt,0) - ISNULL(HL.HLcnt,0) - ISNULL(DOff.DayOffcnt,0)) AS DECIMAL(6,2)) AS DECIMAL(6,2)) AS NVARCHAR(MAX)) + '%)' AS LeaveCnt " & _
                         ", CAST(ISNULL(Ex1.ExcuseCnt,0) AS NVARCHAR(MAX)) + SPACE(2) + '(' + CAST(CAST((100 *  ISNULL(Ex1.ExcuseCnt,0)) / CAST((@TotalDays - ISNULL(wk.Wkcnt,0) - ISNULL(HL.HLcnt,0) - ISNULL(DOff.DayOffcnt,0)) AS DECIMAL(6,2)) AS DECIMAL(6,2)) AS NVARCHAR(MAX)) + '%)' AS ExcuseCnt " & _
                         ", CAST(ISNULL(wk.Wkcnt,0) AS NVARCHAR(MAX)) + SPACE(2) + '(' + CAST(CAST((100 *  ISNULL(wk.Wkcnt,0)) / CAST((@TotalDays - ISNULL(HL.HLcnt,0) - ISNULL(DOff.DayOffcnt,0)) AS DECIMAL(6,2)) AS DECIMAL(6,2)) AS NVARCHAR(MAX)) + '%)' AS Weekendcnt " & _
                         ", CAST(ISNULL(HL.HLcnt,0) AS NVARCHAR(MAX)) + SPACE(2) + '(' + CAST(CAST((100 *  ISNULL(HL.HLcnt,0)) / CAST((@TotalDays - ISNULL(wk.Wkcnt,0) - ISNULL(DOff.DayOffcnt,0)) AS DECIMAL(6,2)) AS DECIMAL(6,2)) AS NVARCHAR(MAX)) + '%)' AS HolidayCnt " & _
                         ", CAST(ISNULL(DOff.DayOffcnt,0) AS NVARCHAR(MAX)) + SPACE(2) + '(' + CAST(CAST((100 *  ISNULL(DOff.DayOffcnt,0)) / CAST((@TotalDays - ISNULL(wk.Wkcnt,0) - ISNULL(HL.HLcnt,0)) AS DECIMAL(6,2)) AS DECIMAL(6,2)) AS NVARCHAR(MAX)) + '%)'  AS DayOffCnt "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", '' AS GName "
            End If

            StrQ &= " FROM hremployee_master "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " LEFT JOIN " & _
                         "   ( " & _
                         "       SELECT " & _
                         "         stationunkid " & _
                         "        ,deptgroupunkid " & _
                         "        ,departmentunkid " & _
                         "        ,sectiongroupunkid " & _
                         "        ,sectionunkid " & _
                         "        ,unitgroupunkid " & _
                         "        ,unitunkid " & _
                         "        ,teamunkid " & _
                         "        ,classgroupunkid " & _
                         "        ,classunkid " & _
                         "        ,employeeunkid " & _
                         "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "       FROM hremployee_transfer_tran " & _
                         "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                         "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                         " LEFT JOIN hrdepartment_master dept ON Alloc.departmentunkid = dept.departmentunkid "



            Select Case mintLocationId

                Case enAllocation.BRANCH

                    StrQ &= " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = Alloc.stationunkid "

                Case enAllocation.DEPARTMENT_GROUP

                    StrQ &= " LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = Alloc.deptgroupunkid "

                Case enAllocation.SECTION_GROUP

                    StrQ &= " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = Alloc.sectiongroupunkid "

                Case enAllocation.SECTION

                    StrQ &= " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = Alloc.sectionunkid "

                Case enAllocation.UNIT_GROUP

                    StrQ &= " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = Alloc.unitgroupunkid "

                Case enAllocation.UNIT

                    StrQ &= " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = Alloc.unitunkid "

                Case enAllocation.TEAM

                    StrQ &= " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = Alloc.teamunkid "

                Case enAllocation.JOB_GROUP

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           jobgroupunkid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_categorization_tran " & _
                                  "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS Jobgrp ON Jobgrp.employeeunkid = hremployee_master.employeeunkid AND Jobgrp.rno = 1 " & _
                                  " LEFT JOIN hrjobgroup_master jg ON Jobgrp.jobgroupunkid = jg.jobgroupunkid "

                Case enAllocation.JOBS

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           jobunkid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_categorization_tran " & _
                                  "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                  " LEFT JOIN hrjob_master jb ON Jobs.jobunkid = jb.jobunkid "

                Case enAllocation.CLASS_GROUP

                    StrQ &= " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = Alloc.classgroupunkid "

                Case enAllocation.CLASSES

                    StrQ &= " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = Alloc.classunkid "

                Case enAllocation.COST_CENTER

                    StrQ &= " LEFT JOIN " & _
                                  "   ( " & _
                                  "       SELECT " & _
                                  "           cctranheadvalueid " & _
                                  "           ,employeeunkid " & _
                                  "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "       FROM hremployee_cctranhead_tran " & _
                                  "       WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                  "   ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                                  " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = CC.cctranheadvalueid "

            End Select


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If


            StrQ &= " LEFT JOIN ( " & _
                         "                      SELECT employeeunkid " & _
                         "                          ,COUNT(total_hrs) As prcount " & _
                         "                      FROM tnalogin_summary " & _
                         "                      WHERE CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) BETWEEN @StartDate	AND @EndDate " & _
                         "                      AND total_hrs >= 60 AND isholiday=0 AND ispaidleave = 0 AND isunpaidleave = 0 AND isweekend = 0 AND isdayoff = 0	" & _
                         "                     GROUP BY employeeunkid,isholiday,ispaidleave,isunpaidleave ,isweekend , isdayoff " & _
                         " ) AS pr ON  pr.employeeunkid = hremployee_master.employeeunkid " & _
                         " LEFT JOIN ( " & _
                         "                  SELECT employeeunkid " & _
                         "                      ,COUNT(total_hrs) As absentcnt " & _
                         "                  FROM tnalogin_summary " & _
                         "                  WHERE CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) BETWEEN @StartDate	AND @EndDate " & _
                         "                  AND total_hrs < 60 AND isholiday=0 AND ispaidleave = 0 AND isunpaidleave = 1 AND isweekend = 0 AND isdayoff = 0 " & _
                         "                  GROUP BY employeeunkid,isholiday,ispaidleave,isunpaidleave ,isweekend , isdayoff 	" & _
                         " ) AS ab ON  ab.employeeunkid = hremployee_master.employeeunkid " & _
                         " LEFT JOIN ( " & _
                         "                  SELECT employeeunkid " & _
                         "                      ,COUNT(ltcoming_grace) As LateComingCnt " & _
                         "                  FROM tnalogin_summary " & _
                         "                  WHERE CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) BETWEEN @StartDate	AND @EndDate " & _
                         "                  AND ltcoming_grace >= 60 AND isholiday=0 AND ispaidleave = 0 AND isunpaidleave = 0 AND isweekend = 0 AND isdayoff = 0 " & _
                         "                  GROUP BY employeeunkid,isholiday,ispaidleave,isunpaidleave ,isweekend , isdayoff 	" & _
                         " ) AS lt ON  lt.employeeunkid = hremployee_master.employeeunkid " & _
                         " LEFT JOIN ( " & _
                         " 	                SELECT employeeunkid " & _
                         "                      ,COUNT(elrgoing_grace) As ErgoingCnt " & _
                         "                  FROM tnalogin_summary " & _
                         "                  WHERE CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) BETWEEN @StartDate	AND @EndDate " & _
                         "                  AND elrgoing_grace >= 60 AND isholiday=0 AND ispaidleave = 0 AND isunpaidleave = 0 AND isweekend = 0 AND isdayoff = 0	 " & _
                         "                  GROUP BY employeeunkid,isholiday,ispaidleave,isunpaidleave ,isweekend , isdayoff 	" & _
                         " ) AS er ON  er.employeeunkid = hremployee_master.employeeunkid " & _
                         " LEFT JOIN ( " & _
                         "                  SELECT employeeunkid " & _
                         "                      ,COUNT(ispaidleave) As Leavecnt " & _
                         "                  FROM tnalogin_summary " & _
                         "                  LEFT Join lvleavetype_master ON lvleavetype_master.leavetypeunkid = tnalogin_summary.leavetypeunkid " & _
                         "                  WHERE CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) BETWEEN @StartDate	AND @EndDate " & _
                         "                  AND ispaidleave = 1 AND isunpaidleave = 0  AND lvleavetype_master.isexcuseleave = 0 " & _
                         "                  GROUP BY employeeunkid,isholiday,ispaidleave,isunpaidleave ,isweekend , isdayoff 	" & _
                         " ) AS lv ON  lv.employeeunkid = hremployee_master.employeeunkid " & _
                         " LEFT JOIN ( " & _
                         "              	SELECT employeeunkid " & _
                         "                      ,COUNT(*) AS Mispunchcnt " & _
                         "                  FROM tnalogin_tran " & _
                         "                  WHERE isvoid= 0 AND CONVERT(VARCHAR(8), tnalogin_tran.logindate, 112) BETWEEN @StartDate	AND @EndDate " & _
                         "                  Group by employeeunkid " & _
                         "                  HAVING MAX(tnalogin_tran.checkouttime) IS NULL " & _
                         " ) AS mp on mp.employeeunkid = hremployee_master.employeeunkid " & _
                         " LEFT JOIN ( " & _
                         "                  SELECT employeeunkid " & _
                         "                      ,COUNT(leavedate) As ExcuseCnt " & _
                         "                  FROM lvleaveIssue_tran " & _
                         "                  JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid AND lvleaveIssue_master.isvoid = 0 " & _
                         "                  JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                         "                  WHERE lvleaveIssue_tran.isvoid = 0 AND CONVERT(VARCHAR(8), lvleaveIssue_tran.leavedate, 112) BETWEEN @StartDate	AND @EndDate " & _
                         "                  AND lvleavetype_master.isexcuseleave = 1 " & _
                         "                  GROUP BY employeeunkid" & _
                         " ) AS Ex1 ON  Ex1.employeeunkid = hremployee_master.employeeunkid " & _
                         " LEFT JOIN ( " & _
                         "                  SELECT employeeunkid " & _
                         "                      ,COUNT(isweekend) AS Wkcnt " & _
                         "                  FROM tnalogin_summary " & _
                         "                  WHERE CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) BETWEEN @StartDate	AND @EndDate " & _
                         "                  AND isweekend = 1 " & _
                         "                  Group by employeeunkid " & _
                         " ) AS wk on wk.employeeunkid = hremployee_master.employeeunkid " & _
                         " LEFT JOIN ( " & _
                         "                  SELECT employeeunkid " & _
                         "                      ,COUNT(isholiday) AS HLcnt " & _
                         "                  FROM tnalogin_summary " & _
                         "                  WHERE CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) BETWEEN @StartDate	AND @EndDate " & _
                         "                  AND isholiday = 1 " & _
                         "                  Group by employeeunkid  " & _
                         " ) AS HL on HL.employeeunkid = hremployee_master.employeeunkid " & _
                         " LEFT JOIN ( " & _
                         "                   SELECT employeeunkid " & _
                         "                    ,COUNT(isdayoff) AS DayOffcnt " & _
                         "                   FROM tnalogin_summary " & _
                         "                   WHERE CONVERT(VARCHAR(8), tnalogin_summary.login_date, 112) BETWEEN @StartDate	AND @EndDate " & _
                         "                   AND isdayoff = 1 " & _
                         "                   Group by employeeunkid " & _
                         " ) AS DOff on DOff.employeeunkid = hremployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If


            StrQ &= " WHERE  1=1  "

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvanceFilter
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If


            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))


            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell


            Dim mdtTableExcel As DataTable = dsList.Tables(0)

            If mdtTableExcel.Columns.Contains("employeeunkid") Then
                mdtTableExcel.Columns.Remove("employeeunkid")
            End If


            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, " From Date:") & " " & mdtFromDate.Date & _
                                                                           Space(5) & Language.getMessage(mstrModuleName, 23, " To Date:") & " " & mdtToDate.Date & _
                                                                           Space(5) & Language.getMessage(mstrModuleName, 24, "Employee :") & " " & IIf(mintEmpId > 0, mstrEmpName, ""), "s10bw")

            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)


            If mintViewIndex > 0 Then
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                mdtTableExcel.Columns("GName").Caption = mstrReport_GroupName
                Dim strGrpCols As String() = {"GName"}
                strarrGroupColumns = strGrpCols
            Else
                If mdtTableExcel.Columns.Contains("Id") Then
                    mdtTableExcel.Columns.Remove("Id")
                End If
                If mdtTableExcel.Columns.Contains("GName") Then
                    mdtTableExcel.Columns.Remove("GName")
                End If
            End If


            mdtTableExcel.Columns("SrNo").Caption = Language.getMessage(mstrModuleName, 4, "Sr.No")
            mdtTableExcel.Columns("EmpCodeName").Caption = Language.getMessage(mstrModuleName, 1, "Employee")
            mdtTableExcel.Columns("department").Caption = Language.getMessage(mstrModuleName, 7, "Department")
            mdtTableExcel.Columns("location").Caption = Language.getMessage(mstrModuleName, 6, "Location")
            mdtTableExcel.Columns("PrCnt").Caption = Language.getMessage(mstrModuleName, 8, "Present")
            mdtTableExcel.Columns("absentcnt").Caption = Language.getMessage(mstrModuleName, 9, "Absent")
            mdtTableExcel.Columns("LateComingCnt").Caption = Language.getMessage(mstrModuleName, 10, "Late Coming")
            mdtTableExcel.Columns("ErgoingCnt").Caption = Language.getMessage(mstrModuleName, 11, "Early Departure")
            mdtTableExcel.Columns("Mispunchcnt").Caption = Language.getMessage(mstrModuleName, 12, "Mispunch")
            mdtTableExcel.Columns("LeaveCnt").Caption = Language.getMessage(mstrModuleName, 13, "Leave")
            mdtTableExcel.Columns("ExcuseCnt").Caption = Language.getMessage(mstrModuleName, 14, "Absentees with Excuse")

            If mblnIncludeWeekend Then
                mdtTableExcel.Columns("Weekendcnt").Caption = Language.getMessage(mstrModuleName, 15, "Weekend")
            End If

            If mblnIncludeHoliday Then
                mdtTableExcel.Columns("HolidayCnt").Caption = Language.getMessage(mstrModuleName, 16, "Holiday")
            End If

            If mblnIncludeDayOff Then
                mdtTableExcel.Columns("DayOffCnt").Caption = Language.getMessage(mstrModuleName, 17, "Dayoff")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 18, "Prepared By :") & Space(10) & User._Object._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 19, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 20, "Approved By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If



            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 21, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            ''SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i = 0 Then
                    intArrayColumnWidth(i) = 50
                ElseIf i >= 1 And i <= 3 Then
                    intArrayColumnWidth(i) = 125
                Else
                    intArrayColumnWidth(i) = 80
                End If

            Next
            'SET EXCEL CELL WIDTH

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, Me._ReportName, "", " ", , "", False, rowsArrayHeader, rowsArrayFooter, Nothing, Nothing, False)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee")
			Language.setMessage(mstrModuleName, 2, "Date")
			Language.setMessage(mstrModuleName, 4, "Sr.No")
			Language.setMessage(mstrModuleName, 6, "Location")
			Language.setMessage(mstrModuleName, 7, "Department")
			Language.setMessage(mstrModuleName, 8, "Present")
			Language.setMessage(mstrModuleName, 9, "Absent")
			Language.setMessage(mstrModuleName, 10, "Late Coming")
			Language.setMessage(mstrModuleName, 11, "Early Departure")
			Language.setMessage(mstrModuleName, 12, "Mispunch")
			Language.setMessage(mstrModuleName, 13, "Leave")
			Language.setMessage(mstrModuleName, 14, "Absentees with Excuse")
			Language.setMessage(mstrModuleName, 15, "Weekend")
			Language.setMessage(mstrModuleName, 16, "Holiday")
			Language.setMessage(mstrModuleName, 17, "Dayoff")
			Language.setMessage(mstrModuleName, 18, "Prepared By :")
			Language.setMessage(mstrModuleName, 19, "Checked By :")
			Language.setMessage(mstrModuleName, 20, "Approved By :")
			Language.setMessage(mstrModuleName, 21, "Received By :")
			Language.setMessage(mstrModuleName, 22, " From Date:")
			Language.setMessage(mstrModuleName, 23, " To Date:")
			Language.setMessage(mstrModuleName, 24, "Employee :")
			Language.setMessage(mstrModuleName, 25, " Order By :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
