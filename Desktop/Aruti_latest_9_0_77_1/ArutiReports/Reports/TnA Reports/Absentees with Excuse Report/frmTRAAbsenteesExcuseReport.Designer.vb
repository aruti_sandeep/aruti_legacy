﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTRAAbsenteesExcuseReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkSave = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchLocation = New eZee.Common.eZeeGradientButton
        Me.LblLocation = New System.Windows.Forms.Label
        Me.cboLocation = New System.Windows.Forms.ComboBox
        Me.chkShowEachEmpOnNewPage = New System.Windows.Forms.CheckBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.lblToDate = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblFromDate = New System.Windows.Forms.Label
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.objbtnSearchExcuse = New eZee.Common.eZeeGradientButton
        Me.LblExcuse = New System.Windows.Forms.Label
        Me.cboExcuse = New System.Windows.Forms.ComboBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbSortBy.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 553)
        Me.NavPanel.Size = New System.Drawing.Size(767, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchExcuse)
        Me.gbFilterCriteria.Controls.Add(Me.LblExcuse)
        Me.gbFilterCriteria.Controls.Add(Me.cboExcuse)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSave)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchLocation)
        Me.gbFilterCriteria.Controls.Add(Me.LblLocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboLocation)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowEachEmpOnNewPage)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmpName)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpFromDate)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(9, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(398, 184)
        Me.gbFilterCriteria.TabIndex = 15
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSave
        '
        Me.lnkSave.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSave.Location = New System.Drawing.Point(295, 158)
        Me.lnkSave.Name = "lnkSave"
        Me.lnkSave.Size = New System.Drawing.Size(95, 17)
        Me.lnkSave.TabIndex = 225
        Me.lnkSave.TabStop = True
        Me.lnkSave.Text = "Save Settings"
        '
        'objbtnSearchLocation
        '
        Me.objbtnSearchLocation.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchLocation.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchLocation.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchLocation.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchLocation.BorderSelected = False
        Me.objbtnSearchLocation.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchLocation.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchLocation.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchLocation.Location = New System.Drawing.Point(369, 113)
        Me.objbtnSearchLocation.Name = "objbtnSearchLocation"
        Me.objbtnSearchLocation.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchLocation.TabIndex = 217
        '
        'LblLocation
        '
        Me.LblLocation.BackColor = System.Drawing.Color.Transparent
        Me.LblLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLocation.Location = New System.Drawing.Point(8, 116)
        Me.LblLocation.Name = "LblLocation"
        Me.LblLocation.Size = New System.Drawing.Size(62, 13)
        Me.LblLocation.TabIndex = 215
        Me.LblLocation.Text = "Location"
        '
        'cboLocation
        '
        Me.cboLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLocation.DropDownWidth = 120
        Me.cboLocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLocation.FormattingEnabled = True
        Me.cboLocation.Location = New System.Drawing.Point(76, 113)
        Me.cboLocation.Name = "cboLocation"
        Me.cboLocation.Size = New System.Drawing.Size(287, 21)
        Me.cboLocation.TabIndex = 216
        '
        'chkShowEachEmpOnNewPage
        '
        Me.chkShowEachEmpOnNewPage.Checked = True
        Me.chkShowEachEmpOnNewPage.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowEachEmpOnNewPage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEachEmpOnNewPage.Location = New System.Drawing.Point(76, 141)
        Me.chkShowEachEmpOnNewPage.Name = "chkShowEachEmpOnNewPage"
        Me.chkShowEachEmpOnNewPage.Size = New System.Drawing.Size(206, 18)
        Me.chkShowEachEmpOnNewPage.TabIndex = 213
        Me.chkShowEachEmpOnNewPage.Text = "Show Each Employee On New Page"
        Me.chkShowEachEmpOnNewPage.UseVisualStyleBackColor = True
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(298, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 23
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(369, 58)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 207
        '
        'lblToDate
        '
        Me.lblToDate.BackColor = System.Drawing.Color.Transparent
        Me.lblToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToDate.Location = New System.Drawing.Point(197, 35)
        Me.lblToDate.Name = "lblToDate"
        Me.lblToDate.Size = New System.Drawing.Size(51, 13)
        Me.lblToDate.TabIndex = 175
        Me.lblToDate.Text = "To Date"
        '
        'dtpToDate
        '
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpToDate.Location = New System.Drawing.Point(254, 31)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpToDate.TabIndex = 176
        '
        'lblEmpName
        '
        Me.lblEmpName.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(8, 61)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(62, 13)
        Me.lblEmpName.TabIndex = 167
        Me.lblEmpName.Text = "Employee"
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.DropDownWidth = 120
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(76, 58)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(287, 21)
        Me.cboEmployee.TabIndex = 168
        '
        'lblFromDate
        '
        Me.lblFromDate.BackColor = System.Drawing.Color.Transparent
        Me.lblFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromDate.Location = New System.Drawing.Point(8, 35)
        Me.lblFromDate.Name = "lblFromDate"
        Me.lblFromDate.Size = New System.Drawing.Size(62, 13)
        Me.lblFromDate.TabIndex = 151
        Me.lblFromDate.Text = "From Date"
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFromDate.Location = New System.Drawing.Point(76, 31)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(109, 21)
        Me.dtpFromDate.TabIndex = 152
        '
        'gbSortBy
        '
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(9, 258)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(398, 63)
        Me.gbSortBy.TabIndex = 16
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(369, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 2
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(68, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(76, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(287, 21)
        Me.txtOrderBy.TabIndex = 1
        '
        'objbtnSearchExcuse
        '
        Me.objbtnSearchExcuse.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchExcuse.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchExcuse.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchExcuse.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchExcuse.BorderSelected = False
        Me.objbtnSearchExcuse.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchExcuse.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchExcuse.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchExcuse.Location = New System.Drawing.Point(369, 85)
        Me.objbtnSearchExcuse.Name = "objbtnSearchExcuse"
        Me.objbtnSearchExcuse.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchExcuse.TabIndex = 229
        '
        'LblExcuse
        '
        Me.LblExcuse.BackColor = System.Drawing.Color.Transparent
        Me.LblExcuse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblExcuse.Location = New System.Drawing.Point(8, 89)
        Me.LblExcuse.Name = "LblExcuse"
        Me.LblExcuse.Size = New System.Drawing.Size(62, 13)
        Me.LblExcuse.TabIndex = 227
        Me.LblExcuse.Text = "Excuse"
        '
        'cboExcuse
        '
        Me.cboExcuse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboExcuse.DropDownWidth = 120
        Me.cboExcuse.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboExcuse.FormattingEnabled = True
        Me.cboExcuse.Location = New System.Drawing.Point(76, 85)
        Me.cboExcuse.Name = "cboExcuse"
        Me.cboExcuse.Size = New System.Drawing.Size(287, 21)
        Me.cboExcuse.TabIndex = 228
        '
        'frmTRAAbsenteesExcuseReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(767, 608)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbSortBy)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTRAAbsenteesExcuseReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Daily TimeSheet"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblEmpName As System.Windows.Forms.Label
    Public WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Public WithEvents lblFromDate As System.Windows.Forms.Label
    Public WithEvents dtpFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Public WithEvents lblToDate As System.Windows.Forms.Label
    Public WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchLocation As eZee.Common.eZeeGradientButton
    Private WithEvents LblLocation As System.Windows.Forms.Label
    Public WithEvents cboLocation As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowEachEmpOnNewPage As System.Windows.Forms.CheckBox
    Friend WithEvents lnkSave As System.Windows.Forms.LinkLabel
    Friend WithEvents objbtnSearchExcuse As eZee.Common.eZeeGradientButton
    Private WithEvents LblExcuse As System.Windows.Forms.Label
    Public WithEvents cboExcuse As System.Windows.Forms.ComboBox
End Class
