﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter
#End Region

Public Class clsPDPFormReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPDPFormReport"
    Private mstrReportId As String = enArutiReport.PDP_Form_Report '309
    Private objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mblnIsInImageDb As Boolean = False
    Private mstrPhotoPath As String = ""

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _IsImageInDb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsInImageDb = value
        End Set
    End Property

    Public WriteOnly Property _PhotoPath() As String
        Set(ByVal value As String)
            mstrPhotoPath = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmployeeUnkid = 0
            mstrEmployeeName = ""
            mblnIsInImageDb = False
            mstrPhotoPath = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.ClearParameters()
            If mintEmployeeUnkid > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid)
                Me._FilterQuery &= " AND hremployee_master.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 19, "Employee :") & " " & mstrEmployeeName & " "
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid


            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xExportReportPath, xOpenReportAfterExport, PrintAction, ExportAction, intBaseCurrencyUnkid)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Private Function Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0 _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim rpt_EmpHistory As New ArutiReport.Designer.dsArutiReport
        Dim rpt_Training As New ArutiReport.Designer.dsArutiReport
        Dim rpt_Status As New ArutiReport.Designer.dsArutiReport
        Dim rpt_Future As New ArutiReport.Designer.dsArutiReport
        Dim oDictData As New Dictionary(Of String, String)

        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT " & _
                   "     employeecode AS StaffID " & _
                   "    ,firstname + ' ' + othername +' '+ surname AS eName " & _
                   "    ,ISNULL(LineMgr,'') AS LineMgr " & _
                   "    ,ISNULL(SGM.name,'') AS Department " & _
                   "    ,ISNULL(JBM.job_name,'') AS Job " & _
                   "    ,ISNULL(CLM.name,'') AS Branch " & _
                   "    ,ISNULL(GGM.name,'') AS JobLevel " & _
                   "    ,ISNULL(eGrd.incrementdate,'') AS JobLvlDate " & _
                   "    ,ISNULL(eHistory.effectivedate,'') AS HistoryDate " & _
                   "    ,ISNULL(eHistory.JbHistory,'') AS HistoryJob " & _
                   "    ,ISNULL(eTrg.TrainingName,'') AS TrainingName " & _
                   "    ,ISNULL(ePF.CG,'') AS CG " & _
                   "    ,ISNULL(ePF.AoS,'') AS AoS " & _
                   "    ,ISNULL(ePF.AoD,'') AS AoD " & _
                   "    ,ISNULL(ePF.SoF,'') AS SoF " & _
                   "    ,ISNULL(eAP.DoG,'') AS DoG " & _
                   "    ,ISNULL(eAP.AoG,'') AS AoG " & _
                   "    ,ISNULL(eAP.DeL,'') AS DeL " & _
                   "    ,ISNULL(eAP.cHeader,'') AS cHeader " & _
                   "    ,ISNULL(eAP.sortorder,0) AS sortorder " & _
                   "FROM hremployee_master " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         PF.employeeunkid " & _
                   "        ,ISNULL(pJb.job_name,'') AS CG " & _
                   "        ,ISNULL(M1.fieldvalue,'') AS AoS " & _
                   "        ,ISNULL(M2.fieldvalue,'') AS AoD " & _
                   "        ,ISNULL(M3.fieldvalue,'') AS SoF " & _
                   "    FROM pdpformmaster PF " & _
                   "        LEFT JOIN hrjob_master AS pJb ON PF.nextjobid = pJb.jobunkid " & _
                   "        LEFT JOIN " & _
                   "        ( " & _
                   "            /*Areas of Strength (What are my Strengths)*/ " & _
                   "            SELECT " & _
                   "                 PF.pdpformunkid " & _
                   "                ,(CAST(ROW_NUMBER()OVER(Partition by PF.pdpformunkid ORDER BY DA.fieldvalue) AS NVARCHAR(50)) + '. ' +  DA.fieldvalue) AS fieldvalue " & _
                   "            FROM pdpitemdatatran AS DA " & _
                   "                JOIN pdpitem_master AS IT ON DA.itemunkid = IT.itemunkid " & _
                   "                JOIN pdpformmaster PF ON DA.pdpformunkid = PF.pdpformunkid " & _
                   "            WHERE DA.isvoid = 0 AND PF.isvoid = 0 AND LTRIM(RTRIM(IT.orgitem)) = 'Areas of Strength (What are my Strengths)' " & _
                   "        )AS M1 ON M1.pdpformunkid = PF.pdpformunkid " & _
                   "        LEFT JOIN " & _
                   "        ( " & _
                   "            /*Areas for further development*/ " & _
                   "            SELECT " & _
                   "                 PF.pdpformunkid " & _
                   "                ,(CAST(ROW_NUMBER()OVER(Partition by PF.pdpformunkid ORDER BY DA.fieldvalue) AS NVARCHAR(50)) + '. ' +  DA.fieldvalue) AS fieldvalue " & _
                   "            FROM pdpitemdatatran AS DA " & _
                   "                JOIN pdpitem_master AS IT ON DA.itemunkid = IT.itemunkid " & _
                   "                JOIN pdpformmaster PF ON DA.pdpformunkid = PF.pdpformunkid " & _
                   "            WHERE DA.isvoid = 0 AND PF.isvoid = 0 AND LTRIM(RTRIM(IT.orgitem)) = 'Areas for further development' " & _
                   "        )AS M2 ON M2.pdpformunkid = PF.pdpformunkid " & _
                   "        LEFT JOIN " & _
                   "        ( " & _
                   "            /*Opportunities (what strength(s) can be developed further)*/ " & _
                   "            SELECT " & _
                   "                 PF.pdpformunkid " & _
                   "                ,(CAST(ROW_NUMBER()OVER(Partition by PF.pdpformunkid ORDER BY DA.fieldvalue) AS NVARCHAR(50)) + '. ' +  DA.fieldvalue) AS fieldvalue " & _
                   "            FROM pdpitemdatatran AS DA " & _
                   "                JOIN pdpitem_master AS IT ON DA.itemunkid = IT.itemunkid " & _
                   "                JOIN pdpformmaster PF ON DA.pdpformunkid = PF.pdpformunkid " & _
                   "            WHERE DA.isvoid = 0 AND PF.isvoid = 0 AND LTRIM(RTRIM(IT.orgitem)) = 'Opportunities (what strength(s) can be developed further)' " & _
                   "        )AS M3 ON M3.pdpformunkid = PF.pdpformunkid " & _
                   "    WHERE PF.isvoid = 0 " & _
                   ") AS ePF ON ePF.employeeunkid = hremployee_master.employeeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         PF.employeeunkid " & _
                   "        ,PG.goal_name AS DoG " & _
                   "        ,PG.goal_description AS AoG " & _
                   "        ,CONVERT(NVARCHAR(8),PG.duedate,112) AS Del " & _
                   "        ,AP.category AS cHeader " & _
                   "        ,AP.sortorder " & _
                   "    FROM pdpgoals_master AS PG " & _
                   "        JOIN pdpformmaster AS PF ON PF.pdpformunkid = PG.pdpformunkid " & _
                   "        JOIN pdpaction_plan_category AS AP ON AP.actionplancategoryunkid = PG.actionplancategoryunkid " & _
                   "    WHERE PG.isvoid = 0 AND PF.isvoid = 0 " & _
                   ") AS eAP ON eAP.employeeunkid = hremployee_master.employeeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         TR.employeeunkid " & _
                   "        ,ISNULL(CM.name,'') AS TrainingName " & _
                   "    FROM trtraining_request_master AS TR " & _
                   "        JOIN cfcommon_master CM ON CM.masterunkid = TR.coursemasterunkid " & _
                   "    WHERE TR.isvoid = 0 AND TR.statusunkid = 2 AND CM.mastertype = 30 " & _
                   ") AS eTrg ON eTrg.employeeunkid = hremployee_master.employeeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         CT.employeeunkid " & _
                   "        ,CONVERT(NVARCHAR(8),CT.effectivedate,112) AS effectivedate " & _
                   "        ,ISNULL(eJB.job_name,'') AS JbHistory " & _
                   "    FROM hremployee_categorization_tran AS CT " & _
                   "        JOIN hrjob_master AS eJB ON CT.jobunkid = eJB.jobunkid " & _
                   "    WHERE CT.isvoid = 0 AND eJB.isactive = 1 " & _
                   ") AS eHistory ON eHistory.employeeunkid = hremployee_master.employeeunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         SI.gradegroupunkid " & _
                   "        ,SI.gradeunkid " & _
                   "        ,SI.gradelevelunkid " & _
                   "        ,CONVERT(NVARCHAR(8),SI.incrementdate,112) AS incrementdate " & _
                   "        ,SI.employeeunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY SI.employeeunkid ORDER BY SI.incrementdate DESC, SI.salaryincrementtranunkid DESC) AS xRno " & _
                   "    FROM prsalaryincrement_tran AS SI " & _
                   "    WHERE SI.isvoid = 0 AND SI.isapproved = 1 " & _
                   "    AND CONVERT(CHAR(8),SI.incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString() & "' " & _
                   ")AS eGrd ON eGrd.employeeunkid = hremployee_master.employeeunkid AND eGrd.xRno = 1 " & _
                   "LEFT JOIN hrgradegroup_master GGM ON eGrd.gradegroupunkid = GGM.gradegroupunkid AND GGM.isactive = 1 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         CT.employeeunkid " & _
                   "        ,CT.jobgroupunkid " & _
                   "        ,CT.jobunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY CT.employeeunkid ORDER BY CT.effectivedate DESC) AS xRno " & _
                   "    FROM hremployee_categorization_tran AS CT " & _
                   "    WHERE CT.isvoid = 0 AND CONVERT(CHAR(8),CT.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString() & "' " & _
                   ") AS eCat ON eCat.employeeunkid = hremployee_master.employeeunkid AND eCat.xRno = 1 " & _
                   "LEFT JOIN hrjob_master JBM ON eCat.jobunkid = JBM.jobunkid AND JBM.isactive = 1 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         TT.employeeunkid " & _
                   "        ,TT.stationunkid " & _
                   "        ,TT.deptgroupunkid " & _
                   "        ,TT.departmentunkid " & _
                   "        ,TT.sectiongroupunkid " & _
                   "        ,TT.sectionunkid " & _
                   "        ,TT.unitgroupunkid " & _
                   "        ,TT.unitunkid " & _
                   "        ,TT.teamunkid " & _
                   "        ,TT.classgroupunkid " & _
                   "        ,TT.classunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY TT.employeeunkid ORDER BY TT.effectivedate DESC) AS xRno " & _
                   "    FROM hremployee_transfer_tran AS TT " & _
                   "    WHERE TT.isvoid = 0 AND CONVERT(CHAR(8),TT.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString() & "' " & _
                   ") AS eAlloc ON eAlloc.employeeunkid = hremployee_master.employeeunkid AND eAlloc.xRno = 1 " & _
                   "LEFT JOIN hrsectiongroup_master SGM ON eAlloc.sectiongroupunkid = SGM.sectiongroupunkid AND SGM.isactive = 1 " & _
                   "LEFT JOIN hrclasses_master AS CLM ON eAlloc.classunkid = CLM.classesunkid AND CLM.isactive = 1 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         hremployee_reportto.employeeunkid " & _
                   "        ,reporttoemployeeunkid " & _
                   "        ,firstname + ' ' + othername +' '+ surname AS LineMgr " & _
                   "    FROM hremployee_reportto " & _
                   "        JOIN hremployee_master AS eR ON eR.employeeunkid = hremployee_reportto.reporttoemployeeunkid " & _
                   "    WHERE isvoid = 0 AND ishierarchy = 1 " & _
                   ") AS eLMgr ON eLMgr.employeeunkid = hremployee_master.employeeunkid " & _
                   "WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")


            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If
            Dim dtData As DataTable = dsList.Tables(0).Clone

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            '******************** Main Part ******************' START
            dtData = dsList.Tables(0).DefaultView.ToTable(True, "StaffID", "eName", "LineMgr", "Department", "Job", "Branch", "JobLevel", "JobLvlDate")
            For Each dtRow As DataRow In dtData.Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("StaffID")
                rpt_Rows.Item("Column2") = dtRow.Item("eName")
                rpt_Rows.Item("Column3") = dtRow.Item("LineMgr")
                rpt_Rows.Item("Column4") = dtRow.Item("Department")
                rpt_Rows.Item("Column5") = dtRow.Item("Job")
                rpt_Rows.Item("Column6") = dtRow.Item("Branch")
                rpt_Rows.Item("Column7") = dtRow.Item("JobLevel")
                If dtRow.Item("JobLvlDate").ToString().Trim.Length > 0 Then
                    rpt_Rows.Item("Column8") = eZeeDate.convertDate(dtRow.Item("JobLvlDate").ToString()).Date.ToShortDateString()
                Else
                    rpt_Rows.Item("Column8") = ""
                End If
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '******************** Main Part ******************' END

            '******************** Employement Status ******************' START
            dtData = dsList.Tables(0).DefaultView.ToTable(True, "StaffID", "HistoryDate", "HistoryJob")
            For Each dtRow As DataRow In dtData.Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_EmpHistory.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("StaffID")
                If dtRow.Item("HistoryDate").ToString().Trim.Length > 0 Then
                    rpt_Rows.Item("Column9") = eZeeDate.convertDate(dtRow.Item("HistoryDate").ToString()).Date.ToShortDateString()
                Else
                    rpt_Rows.Item("Column9") = ""
                End If
                rpt_Rows.Item("Column10") = dtRow.Item("HistoryJob")

                rpt_EmpHistory.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '******************** Employement Status ******************' END

            '******************** Training ******************' START
            dtData = dsList.Tables(0).DefaultView.ToTable(True, "StaffID", "TrainingName")
            For Each dtRow As DataRow In dtData.Rows
                If dtRow.Item("TrainingName").ToString.Trim().Length > 0 Then
                    Dim rpt_Rows As DataRow
                    rpt_Rows = rpt_Training.Tables("ArutiTable").NewRow

                    rpt_Rows.Item("Column1") = dtRow.Item("StaffID")
                    rpt_Rows.Item("Column11") = dtRow.Item("TrainingName")

                    rpt_Training.Tables("ArutiTable").Rows.Add(rpt_Rows)
                End If                
            Next
            '******************** Training ******************' END

            '******************** Actual Status ******************' START
            dtData = dsList.Tables(0).DefaultView.ToTable(True, "StaffID", "CG", "AoS", "AoD", "SoF")
            If dtData.Rows.Count > 0 Then
                Dim _strData As String = ""
                oDictData.Add("StaffID", dtData.Rows(0).Item("StaffID"))

                _strData = dtData.AsEnumerable().Select(Function(x) x.Field(Of String)("CG")).ToList().Distinct().FirstOrDefault()
                oDictData.Add("CG", _strData)

                _strData = String.Join(vbCrLf, dtData.AsEnumerable().Select(Function(x) x.Field(Of String)("AoS")).Distinct().ToArray())
                oDictData.Add("AoS", _strData)

                _strData = String.Join(vbCrLf, dtData.AsEnumerable().Select(Function(x) x.Field(Of String)("AoD")).Distinct().ToArray())
                oDictData.Add("AoD", _strData)

                _strData = String.Join(vbCrLf, dtData.AsEnumerable().Select(Function(x) x.Field(Of String)("SoF")).Distinct().ToArray())
                oDictData.Add("SoF", _strData)

                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Status.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = oDictData("StaffID")
                rpt_Rows.Item("Column12") = oDictData("CG")
                rpt_Rows.Item("Column13") = oDictData("AoS")
                rpt_Rows.Item("Column14") = oDictData("AoD")
                rpt_Rows.Item("Column15") = oDictData("SoF")
                rpt_Status.Tables("ArutiTable").Rows.Add(rpt_Rows)

                'For Each dtRow As DataRow In dtData.Rows
                '    Dim rpt_Rows As DataRow
                '    rpt_Rows = rpt_Status.Tables("ArutiTable").NewRow

                '    rpt_Rows.Item("Column1") = dtRow.Item("StaffID")
                '    rpt_Rows.Item("Column12") = dtRow.Item("CG")
                '    rpt_Rows.Item("Column13") = dtRow.Item("AoS")
                '    rpt_Rows.Item("Column14") = dtRow.Item("AoD")
                '    rpt_Rows.Item("Column15") = dtRow.Item("SoF")

                '    rpt_Status.Tables("ArutiTable").Rows.Add(rpt_Rows)
                'Next
            End If            
            '******************** Actual Status ******************' END

            '******************** Future Development ******************' START
            dtData = dsList.Tables(0).DefaultView.ToTable(True, "StaffID", "DoG", "AoG", "cHeader", "DeL", "sortorder")
            For Each dtRow As DataRow In dtData.Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Future.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("StaffID")
                rpt_Rows.Item("Column16") = dtRow.Item("DoG")
                rpt_Rows.Item("Column17") = dtRow.Item("AoG")
                If dtRow.Item("DeL").ToString().Trim.Length > 0 Then
                    rpt_Rows.Item("Column18") = eZeeDate.convertDate(dtRow.Item("DeL").ToString()).Date.ToShortDateString()
                Else
                    rpt_Rows.Item("Column18") = ""
                End If
                rpt_Rows.Item("Column19") = dtRow.Item("cHeader")
                rpt_Rows.Item("Column20") = dtRow.Item("sortorder")

                rpt_Future.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next
            '******************** Future Development ******************' END

            objRpt = New ArutiReport.Designer.rptPDPFormReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            'ReportFunction.Logo_Display(objRpt, _
            '                            ConfigParameter._Object._IsDisplayLogo, _
            '                            ConfigParameter._Object._ShowLogoRightSide, _
            '                            "arutiLogo1", _
            '                            "arutiLogo2", _
            '                            arrImageRow, _
            '                            "txtCompanyName", _
            '                            "txtReportName", _
            '                            "txtFilterDescription", _
            '                            ConfigParameter._Object._GetLeftMargin, _
            '                            ConfigParameter._Object._GetRightMargin)


            Dim objEmployeeData As New clsEmployee_Master
            objEmployeeData._Employeeunkid(xPeriodEnd) = mintEmployeeUnkid
            Dim objEImg(0) As Object
            Dim strEmployeeImage As String = ""
            Dim imgBlank As Image = Nothing
            If mblnIsInImageDb = False Then
                If objEmployeeData._ImagePath <> "" Then
                    If mstrPhotoPath.ToString.LastIndexOf("\") = mstrPhotoPath.ToString.Length - 1 Then
                        strEmployeeImage = mstrPhotoPath & objEmployeeData._ImagePath
                    Else
                        strEmployeeImage = mstrPhotoPath & "\" & objEmployeeData._ImagePath
                    End If
                    objEImg(0) = eZeeDataType.image2Data(System.Drawing.Image.FromFile(strEmployeeImage))
                Else
                    imgBlank = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                    objEImg(0) = eZeeDataType.image2Data(imgBlank)
                End If

            ElseIf mblnIsInImageDb = True Then

                Dim objEmpImg As New clsemp_Images
                objEmpImg.GetData(Company._Object._Companyunkid, mintEmployeeUnkid, True, Nothing)

                If objEmpImg._Photo IsNot Nothing Then
                    objEImg(0) = objEmpImg._Photo
                Else
                    imgBlank = ReportFunction.CreateBlankImage(1, 1, True, True, True)
                    objEImg(0) = eZeeDataType.image2Data(imgBlank)
                    arrImageRow.Item("arutiLogo") = objEImg(0)
                End If

            End If

            arrImageRow.Item("guestimage") = objEImg(0)
            imgBlank = ReportFunction.CreateBlankImage(1, 1, True, True, True)
            arrImageRow.Item("arutiLogo") = eZeeDataType.image2Data(imgBlank)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptEmplHistory").SetDataSource(rpt_EmpHistory)
            objRpt.Subreports("rptEmplTraining").SetDataSource(rpt_Training)
            objRpt.Subreports("rptActualStatus").SetDataSource(rpt_Status)
            objRpt.Subreports("rptFutureDevelopment").SetDataSource(rpt_Future)

            ReportFunction.TextChange(objRpt, "txtPDPForm", Language.getMessage(mstrModuleName, 301, "PERSONAL DEVELOPMENT PLAN FORM (PDP)"))
            ReportFunction.TextChange(objRpt, "lblPart1", Language.getMessage(mstrModuleName, 302, "PERSONAL DATA"))
            ReportFunction.TextChange(objRpt, "txtParticulars", Language.getMessage(mstrModuleName, 303, "Employee Particulars:"))
            ReportFunction.TextChange(objRpt, "txtName", Language.getMessage(mstrModuleName, 304, "Name"))
            ReportFunction.TextChange(objRpt, "txtStaffId", Language.getMessage(mstrModuleName, 305, "Staff ID"))
            ReportFunction.TextChange(objRpt, "txtJobTitle", Language.getMessage(mstrModuleName, 306, "Job Title"))
            ReportFunction.TextChange(objRpt, "txtJobLevel", Language.getMessage(mstrModuleName, 307, "Job Level"))
            ReportFunction.TextChange(objRpt, "txtLineMgr", Language.getMessage(mstrModuleName, 308, "Line Manager"))
            ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 309, "Department"))
            ReportFunction.TextChange(objRpt, "txtBranch", Language.getMessage(mstrModuleName, 310, "Branch"))
            ReportFunction.TextChange(objRpt, "txtDtJobLevel", Language.getMessage(mstrModuleName, 311, "Date into Job Level"))
            ReportFunction.TextChange(objRpt, "lblEmplHistory", Language.getMessage(mstrModuleName, 312, "EMPLOYMENT HISTORY"))
            ReportFunction.TextChange(objRpt, "lblTraining", Language.getMessage(mstrModuleName, 313, "EDUCATION/TRAINING COMPLETED"))
            ReportFunction.TextChange(objRpt, "lblFuturePlan", Language.getMessage(mstrModuleName, 314, "Future development action plan"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmplHistory"), "txtDate", Language.getMessage(mstrModuleName, 315, "Date"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmplHistory"), "txtJobName", Language.getMessage(mstrModuleName, 316, "Job"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmplTraining"), "txtTrainingName", Language.getMessage(mstrModuleName, 317, "Training Name"))
            ReportFunction.TextChange(objRpt.Subreports("rptActualStatus"), "txtActualStatus", Language.getMessage(mstrModuleName, 318, "Actual Status"))
            ReportFunction.TextChange(objRpt.Subreports("rptActualStatus"), "txtAreaOfStrength", Language.getMessage(mstrModuleName, 319, "Areas of Strengths"))
            ReportFunction.TextChange(objRpt.Subreports("rptActualStatus"), "txtAreaOfDevelopment", Language.getMessage(mstrModuleName, 320, "Areas of Development"))
            ReportFunction.TextChange(objRpt.Subreports("rptActualStatus"), "txtCareerGoal", Language.getMessage(mstrModuleName, 321, "Career Goal (s):"))
            ReportFunction.TextChange(objRpt.Subreports("rptActualStatus"), "txtStrengthFurther", Language.getMessage(mstrModuleName, 322, "What strengths are needed to be exploited further?"))
            ReportFunction.TextChange(objRpt.Subreports("rptFutureDevelopment"), "txtAction", Language.getMessage(mstrModuleName, 323, "Actions towards goal"))
            ReportFunction.TextChange(objRpt.Subreports("rptFutureDevelopment"), "txtDeadline", Language.getMessage(mstrModuleName, 324, "Deadline"))

            ReportFunction.TextChange(objRpt, "lblEmplSig", Language.getMessage(mstrModuleName, 325, "EMPLOYEE'S SIGNATURE"))
            ReportFunction.TextChange(objRpt, "lblLineMgrSig", Language.getMessage(mstrModuleName, 326, "LINE MANAGER'S SIGNATURE"))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 19, "Employee :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
