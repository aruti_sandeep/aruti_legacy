﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPDPFormReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPDPFormReport"
    Private objPDPForm As clsPDPFormReport

#End Region

#Region " Constructor "

    Public Sub New()
        objPDPForm = New clsPDPFormReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objPDPForm.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing
            dsList = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objPDPForm.SetDefaultValue()

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "Sorry, Emplyoee is mandatory information. Please select employee to continue."), enMsgBoxStyle.Information)
                Return False
            End If

            objPDPForm._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            objPDPForm._EmployeeName = cboEmployee.Text
            objPDPForm._IsImageInDb = ConfigParameter._Object._IsImgInDataBase
            objPDPForm._PhotoPath = ConfigParameter._Object._PhotoPath

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedIndex = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmPDPFormReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPDPForm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPFormReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPDPFormReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objPDPForm._ReportName
            Me._Message = objPDPForm._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPFormReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPDPFormReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPDPFormReport_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub frmPDPFormReport_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If SetFilter() = False Then Exit Sub
            objPDPForm.generateReportNew(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                               ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPFormReport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPDPFormReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If SetFilter() = False Then Exit Sub
            objPDPForm.generateReportNew(FinancialYear._Object._DatabaseName, _
                                               User._Object._Userunkid, _
                                               FinancialYear._Object._YearUnkid, _
                                               Company._Object._Companyunkid, _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                               ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, _
                                               ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPFormReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPDPFormReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPFormReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPDPFormReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPDPFormReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPDPFormReport.SetMessages()
            objfrm._Other_ModuleNames = "clsPDPFormReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class