'Class Name : clsSunAccountProjectJVReport.vb
'Purpose    :
'Date       :01/10/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsSunAccountProjectJVReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsSunAccountProjectJVReport"
    Private mstrReportId As String = enArutiReport.SUN_Account_Project_JV_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private Variables "

    Private mintBudgetId As Integer = -1
    Private mstrBudgetName As String = String.Empty
    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mintTranHeadUnkId As Integer = -1
    Private mstrTranHeadName As String = String.Empty

    Private mdtTableExcel As DataTable
    Private mstrMessage As String = ""

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderByGName As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_TableName As String = ""
    'Hemant (22 Mar 2019) -- Start
    'ISSUE#3620 : Add the "ignore zero rows" in the SUN JV account report for MST (Marie Stopes Tanzania).
    Private mblnIgnoreZero As Boolean = True
    'Hemant (22 Mar 2019) -- End
#End Region

#Region " Properties "

    Public WriteOnly Property _BudgetId() As Integer
        Set(ByVal value As Integer)
            mintBudgetId = value
        End Set
    End Property

    Public WriteOnly Property _BudgetName() As String
        Set(ByVal value As String)
            mstrBudgetName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _TranHeadId() As Integer
        Set(ByVal value As Integer)
            mintTranHeadUnkId = value
        End Set
    End Property

    Public WriteOnly Property _TranHeadName() As String
        Set(ByVal value As String)
            mstrTranHeadName = value
        End Set
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderByGName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderByGName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Hemant (22 Mar 2019) -- Start
    'ISSUE#3620 : Add the "ignore zero rows" in the SUN JV account report for MST (Marie Stopes Tanzania).
    Public WriteOnly Property _IgnoreZero() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnoreZero = value
        End Set
    End Property
    'Hemant (22 Mar 2019) -- End
#End Region

#Region " Public Function(s) And Procedures "

    Public Sub SetDefaultValue()
        Try

            mintBudgetId = -1
            mstrBudgetName = 0
            mintPeriodId = -1
            mstrPeriodName = 0
            mintTranHeadUnkId = -1
            mstrTranHeadName = 0
            mstrMessage = ""
            'mstrAdvance_Filter = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAnalysis_OrderByGName = ""
            mstrReport_GroupName = ""
            'Hemant (22 Mar 2019) -- Start
            'ISSUE#3620 : Add the "ignore zero rows" in the SUN JV account report for MST (Marie Stopes Tanzania).
            mblnIgnoreZero = True
            'Hemant (22 Mar 2019) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            'objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetId)
            'objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            Me._FilterTitle &= ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            'OrderByDisplay = iColumn_DetailReport.ColumnItem(1).DisplayName
            'OrderByQuery = iColumn_DetailReport.ColumnItem(1).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            'iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 7, "Employee Code")))
            'If mblnFirstNamethenSurname = True Then
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' '+ ISNULL(hremployee_master.surname, ' ')", Language.getMessage(mstrModuleName, 8, "Employee Name")))
            'Else
            '    iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '')+ ' ' + ISNULL(hremployee_master.othername, '')", Language.getMessage(mstrModuleName, 9, "Employee Name")))
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Export_Report(ByVal xUserUnkid As Integer _
                                  , ByVal xCompanyUnkid As Integer _
                                  , ByVal xDatabaseName As String _
                                  , ByVal xYearUnkid As String _
                                  , ByVal xPeriodStart As Date _
                                  , ByVal xPeriodEnd As Date _
                                  , ByVal xUserModeSetting As String _
                                  , ByVal xOnlyApproved As Boolean _
                                  , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                  , ByVal blnApplyUserAccessFilter As Boolean _
                                  , ByVal strfmtCurrency As String _
                                  , ByVal xExportReportPath As String _
                                  , ByVal xOpenAfterExport As Boolean _
                                  , ByVal mintBaseCurrencyId As Integer _
                                  )

        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim dsList As DataSet
        Dim objBudget As New clsBudget_MasterNew
        Dim objBudget_Tran As New clsBudget_TranNew
        Dim objBudgetCodes As New clsBudgetcodes_master
        Dim objFundProjectCode As New clsFundProjectCode
        Dim objActivity As New clsfundactivity_Tran
        Dim objBudgetPeriod As New clsBudgetperiod_Tran
        Dim objBudgetHeadMapping As New clsBudgetformulaheadmapping_Tran
        Dim objPeriod As New clscommom_period_Tran

        Dim mdicFund As New Dictionary(Of Integer, Decimal)
        Dim dsAllHeads As DataSet = Nothing
        Dim mintViewById As Integer
        Dim mstrPeriodIdList As String = ""
        Dim mdicHeadMapping As New Dictionary(Of Integer, Integer)
        Dim mstrAllocationTranUnkIDs As String = ""
        Dim mstrEmployeeIDs As String = ""

        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            Dim strBaseCurrencySign As String = ""
            If mintBaseCurrencyId <= 0 Then mintBaseCurrencyId = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = mintBaseCurrencyId
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            strBaseCurrencySign = objExchangeRate._Currency_Sign

            mstrPeriodIdList = objBudgetPeriod.GetPeriodIDs(mintBudgetId)
            mdicHeadMapping = objBudgetHeadMapping.GetHeadMapping(mintBudgetId)
            mstrAllocationTranUnkIDs = objBudget_Tran.GetAllocationTranUnkIDs(mintBudgetId)

            Dim objEmp As New clsEmployee_Master
            Dim intViewIdx As Integer
            Dim strAnalysis_Fields As String = ""
            Dim strAnalysis_TableName As String = ""
            Dim strAnalysis_OrderBy As String = ""
            Dim strAnalysis_OrderBy_GroupName As String = ""
            Dim strAnalysis_Join As String = ""
            Dim strCode As String = ""
            Dim strName As String = ""

            objBudget._Budgetunkid = mintBudgetId
            intViewIdx = objBudget._Allocationbyid
            mintViewById = objBudget._Viewbyid
            mstrAllocationTranUnkIDs = objBudget_Tran.GetAllocationTranUnkIDs(mintBudgetId)

            If CInt(mintViewById) = enBudgetViewBy.Allocation Then
                frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", intViewIdx, mstrAllocationTranUnkIDs, objBudget._Budget_date, "", strAnalysis_Fields, strAnalysis_Join, strAnalysis_OrderBy, strAnalysis_OrderBy_GroupName, "", strAnalysis_TableName)
                If intViewIdx = 0 Then intViewIdx = -1

            ElseIf CInt(mintViewById) = enBudgetViewBy.Employee Then
                mstrEmployeeIDs = objBudget_Tran.getEmployeeIds(mintBudgetId)
                frmEmpSelection.GetAnalysisByDetails("frmEmpSelection", mstrEmployeeIDs, "", strAnalysis_Fields, strAnalysis_Join, strAnalysis_OrderBy, strAnalysis_OrderBy_GroupName, "", strAnalysis_TableName)
                intViewIdx = 0
                mstrAllocationTranUnkIDs = mstrEmployeeIDs
            End If

            Call FilterTitleAndFilterQuery()

            Dim strAnalysisFld As String = strAnalysis_Fields.Substring(1) '

            objDataOperation = New clsDataOperation

            ' mstrAnalysis_Fields.Replace("AS Id,", ", bgbudget_tran.jobunkid) AS Id, ").Replace("AS GName", ", job.job_name + ' ' + CAST(bgbudget_tran.allocationtranunkid * -1 AS NVARCHAR(MAX))) AS GName").Replace(mstrAnalysis_Fields.Substring(0, 1), ", ISNULL(") & _
            StrQ = "SELECT DISTINCT " & _
                            "bgbudget_master.allocationbyid  " & _
                          ", bgbudget_tran.allocationtranunkid " & _
                          ", bgbudgetcodesfundsource_tran.fundprojectcodeunkid " & _
                          ", bgfundprojectcode_master.fundprojectcode " & _
                          ", bgbudgetcodesfundsource_tran.percentage " & _
                          ", bgfundprojectcode_master.fundsourceunkid " & _
                          ", bgfundsource_master.fundcode " & _
                          ", bgbudgetcodesfundsource_tran.fundactivityunkid " & _
                          ", bgfundactivity_tran.activity_code " & _
                          ", CASE WHEN bgbudget_tran.allocationtranunkid > 0 THEN 0 ELSE 1 END AS A " & _
                          ", ISNULL(" & strAnalysisFld.Replace("AS Id,", ", bgbudget_tran.jobunkid) AS Id, ISNULL(").Replace("AS GName", ", job.job_name + ' ' + CAST(bgbudget_tran.allocationtranunkid * -1 AS NVARCHAR(MAX))) AS GName")

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "AS AId").Replace("AS GName", "AS AGName")
            Else
                StrQ &= ", 0 AS AId, '' AS AGName "
            End If

            StrQ &= "FROM    bgbudgetcodes_master " & _
                            "LEFT JOIN bgbudget_master ON bgbudget_master.budgetunkid = bgbudgetcodes_master.budgetunkid " & _
                                                         "AND bgbudgetcodes_master.periodunkid = @periodunkid " & _
                            "LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodes_tran.budgetunkid = bgbudgetcodes_master.budgetunkid " & _
                                                            "AND bgbudgetcodes_tran.periodunkid = @periodunkid " & _
                            "LEFT JOIN bgbudget_tran ON bgbudget_tran.budgettranunkid = bgbudgetcodes_tran.budgettranunkid " & _
                                                       "AND bgbudget_tran.budgettranunkid = bgbudgetcodes_tran.budgettranunkid " & _
                            "LEFT JOIN bgbudgetcodesfundsource_tran ON bgbudgetcodesfundsource_tran.budgettranunkid = bgbudgetcodes_tran.budgettranunkid " & _
                                                                      "AND bgbudgetcodesfundsource_tran.periodunkid = @periodunkid " & _
                            "LEFT JOIN bgfundprojectcode_master ON bgbudgetcodesfundsource_tran.fundprojectcodeunkid = bgfundprojectcode_master.fundprojectcodeunkid " & _
                            "LEFT JOIN bgfundsource_master ON bgfundprojectcode_master.fundsourceunkid = bgfundsource_master.fundsourceunkid " & _
                            "LEFT JOIN bgfundactivity_tran ON bgbudgetcodesfundsource_tran.fundactivityunkid = bgfundactivity_tran.fundactivityunkid " & _
                            "LEFT JOIN hrjob_master AS job ON job.jobunkid = bgbudget_tran.jobunkid " & _
                                    "AND bgbudget_tran.jobunkid > 0 " & _
                            "LEFT JOIN " & strAnalysis_TableName & " ON bgbudget_tran.allocationtranunkid = " & strAnalysis_OrderBy & " " & _
                            mstrAnalysis_Join & _
                    "WHERE   bgbudgetcodes_master.isvoid = 0 " & _
                            "AND bgbudget_master.isvoid = 0 " & _
                            "AND bgbudgetcodes_tran.isvoid = 0 " & _
                            "AND bgbudget_tran.isvoid = 0 " & _
                            "AND bgbudgetcodesfundsource_tran.isvoid = 0 " & _
                            "AND bgfundsource_master.isvoid = 0 " & _
                            "AND bgfundactivity_tran.isvoid = 0 "
            'Hemant (22 Mar 2019) -- Start
            'ISSUE#3620 : Add the "ignore zero rows" in the SUN JV account report for MST (Marie Stopes Tanzania).
            If mblnIgnoreZero = True Then
                StrQ &= "AND bgbudgetcodesfundsource_tran.percentage <> 0 "
            End If
            'Hemant (22 Mar 2019) -- End

            StrQ &= "ORDER BY "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_OrderByGName & ", "
            End If

            StrQ &= "CASE WHEN bgbudget_tran.allocationtranunkid > 0 THEN 0 ELSE 1 END " & _
                        ", ISNULL(" & strAnalysis_OrderBy_GroupName & ", job.job_name + ' ' + CAST(bgbudget_tran.allocationtranunkid * -1 AS NVARCHAR(MAX)))"

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable1")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            ''*** Create Datatable
            Dim mdtExcelAdvance As New DataTable
            Dim dtCol As DataColumn

            mdtExcelAdvance = dsList.Tables("DataTable1")

            dtCol = New DataColumn("code", Type.GetType("System.String"))
            dtCol.DefaultValue = ""
            dtCol.AllowDBNull = False
            mdtExcelAdvance.Columns.Add(dtCol)

            dtCol = New DataColumn("name", Type.GetType("System.String"))
            dtCol.DefaultValue = ""
            dtCol.AllowDBNull = False
            mdtExcelAdvance.Columns.Add(dtCol)

            dtCol = New DataColumn("amount", Type.GetType("System.Decimal"))
            dtCol.DefaultValue = 0
            dtCol.AllowDBNull = False
            mdtExcelAdvance.Columns.Add(dtCol)

            dtCol = New DataColumn("cccode", Type.GetType("System.String"))
            dtCol.DefaultValue = ""
            dtCol.AllowDBNull = False
            mdtExcelAdvance.Columns.Add(dtCol)

            Dim objEmpCostCenter As New clsemployee_costcenter_Tran
            'Sohail (29 Mar 2017) -- Start
            'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
            'Dim dsCC As DataSet = objEmpCostCenter.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "EmpCC", blnApplyUserAccessFilter, , mintTranHeadUnkId)
            Dim dsCC As DataSet = objEmpCostCenter.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "EmpCC", blnApplyUserAccessFilter, , mintTranHeadUnkId, , xPeriodEnd)
            'Sohail (29 Mar 2017) -- End

            StrQ = "SELECT    SUM(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & "))) amount "

            StrQ &= strAnalysis_Fields

            StrQ &= "FROM    prpayrollprocess_tran " & _
                             "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid   " & _
                             "LEFT JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtnaleave_tran.payperiodunkid "

            StrQ &= "LEFT JOIN " & _
                           "( " & _
                           "    SELECT " & _
                           "         stationunkid " & _
                           "        ,deptgroupunkid " & _
                           "        ,departmentunkid " & _
                           "        ,sectiongroupunkid " & _
                           "        ,sectionunkid " & _
                           "        ,unitgroupunkid " & _
                           "        ,unitunkid " & _
                           "        ,teamunkid " & _
                           "        ,classgroupunkid " & _
                           "        ,classunkid " & _
                           "        ,employeeunkid " & _
                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                           "    FROM hremployee_transfer_tran " & _
                           "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                           ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                   "LEFT JOIN " & _
                           "( " & _
                           "    SELECT " & _
                           "         cctranheadvalueid AS costcenterunkid " & _
                           "        ,employeeunkid " & _
                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                           "    FROM hremployee_cctranhead_tran " & _
                           "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                           ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                   "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = C.costcenterunkid "

            StrQ &= strAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE  prtnaleave_tran.isvoid = 0 " & _
                            "AND prpayrollprocess_tran.isvoid = 0 " & _
                            "AND prtnaleave_tran.payperiodunkid = @periodunkid " & _
                            "AND prpayrollprocess_tran.tranheadunkid = @tranheadunkid  "


            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            StrQ &= "GROUP BY " & strAnalysis_Fields.Substring(1).Replace("AS Id", "").Replace("AS GName", "")

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranHeadUnkId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable1")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim dsAnalysis As DataSet = Nothing

            Select Case intViewIdx

                Case enAnalysisReport.Branch
                    Dim obj As New clsStation
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.Department
                    Dim obj As New clsDepartment
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.Section
                    Dim obj As New clsSections
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.Unit
                    Dim obj As New clsUnits
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.Job
                    Dim obj As New clsJobs
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.CostCenter
                    Dim obj As New clscostcenter_master
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.SectionGroup
                    Dim obj As New clsSectionGroup
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.UnitGroup
                    Dim obj As New clsUnitGroup
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.Team
                    Dim obj As New clsTeams
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.DepartmentGroup
                    Dim obj As New clsDepartmentGroup
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.JobGroup
                    Dim obj As New clsJobGroup
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.ClassGroup
                    Dim obj As New clsClassGroup
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.Classs
                    Dim obj As New clsClass
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.GradeGroup
                    Dim obj As New clsGradeGroup
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.Grade
                    Dim obj As New clsGrade
                    dsAnalysis = obj.GetList("List", True)

                Case enAnalysisReport.GradeLevel
                    Dim obj As New clsGradeLevel
                    dsAnalysis = obj.GetList("List", True)

            End Select

            Dim dr() As DataRow = Nothing
            Dim drAna() As DataRow = Nothing
            Dim drCC() As DataRow = Nothing
            Dim dsEmp As DataSet = objEmp.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "Emp")
            Dim objCC As New clscostcenter_master
            Dim dsECC As DataSet = objCC.GetList("ECC", True)
            Dim drEmp() As DataRow = Nothing
            Dim drECC() As DataRow = Nothing

            For Each dtRow As DataRow In mdtExcelAdvance.Rows
                strCode = ""
                strName = dtRow.Item("GName").ToString

                dr = dsList.Tables(0).Select("Id = " & CInt(dtRow.Item("allocationtranunkid")) & " ")

                Select Case intViewIdx

                    Case 0
                        drAna = dsEmp.Tables(0).Select("employeeunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("employeecode")
                            strName = drAna(0).Item("firstname") & " " & drAna(0).Item("othername") & " " & drAna(0).Item("surname")
                        End If

                    Case enAnalysisReport.Branch
                        drAna = dsAnalysis.Tables(0).Select("stationunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.Department
                        drAna = dsAnalysis.Tables(0).Select("departmentunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.Section
                        drAna = dsAnalysis.Tables(0).Select("sectionunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.Unit
                        drAna = dsAnalysis.Tables(0).Select("unitunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.Job
                        drAna = dsAnalysis.Tables(0).Select("jobunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("JobName")
                        End If

                    Case enAnalysisReport.CostCenter
                        drAna = dsAnalysis.Tables(0).Select("costcenterunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("costcentercode")
                            strName = drAna(0).Item("costcentername")
                        End If

                    Case enAnalysisReport.SectionGroup
                        drAna = dsAnalysis.Tables(0).Select("sectiongroupunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.UnitGroup
                        drAna = dsAnalysis.Tables(0).Select("unitgroupunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.Team
                        drAna = dsAnalysis.Tables(0).Select("teamunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.DepartmentGroup
                        drAna = dsAnalysis.Tables(0).Select("deptgroupunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.JobGroup
                        drAna = dsAnalysis.Tables(0).Select("jobgroupunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.ClassGroup
                        drAna = dsAnalysis.Tables(0).Select("classgroupunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.Classs
                        drAna = dsAnalysis.Tables(0).Select("classesunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.GradeGroup
                        drAna = dsAnalysis.Tables(0).Select("gradegroupunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.Grade
                        drAna = dsAnalysis.Tables(0).Select("gradeunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                    Case enAnalysisReport.GradeLevel
                        drAna = dsAnalysis.Tables(0).Select("gradelevelunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drAna.Length > 0 Then
                            strCode = drAna(0).Item("code")
                            strName = drAna(0).Item("name")
                        End If

                End Select

                dtRow.Item("code") = strCode
                dtRow.Item("name") = strName

                If dr.Length > 0 Then
                    dtRow.Item("amount") = (CDec(dr(0).Item("amount")) * CDec(dtRow.Item("percentage"))) / 100
                Else
                    dtRow.Item("amount") = 0
                End If

                dtRow.Item("cccode") = ""
                If intViewIdx <= 0 Then
                    drCC = dsCC.Tables(0).Select("employeeunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                    If drCC.Length > 0 Then
                        dtRow.Item("cccode") = drCC(0).Item("costcentercode")
                    Else
                        drEmp = dsEmp.Tables(0).Select("employeeunkid = " & CInt(dtRow.Item("allocationtranunkid")) & " ")
                        If drEmp.Length > 0 Then
                            drECC = dsECC.Tables(0).Select("costcenterunkid = " & CInt(drEmp(0).Item("costcenterunkid")) & " ")
                            If drECC.Length > 0 Then
                                dtRow.Item("cccode") = drECC(0).Item("costcentercode")
                            Else
                                dtRow.Item("cccode") = ""
                            End If
                        Else
                            dtRow.Item("cccode") = ""
                        End If
                    End If
                End If
            Next

            Dim mintColumn As Integer = 0

            mdtExcelAdvance.Columns.Add("fundprojectcode2", Type.GetType("System.String"), "fundprojectcode")

            mdtExcelAdvance.Columns("code").Caption = Language.getMessage(mstrModuleName, 1, "ID No.")
            mdtExcelAdvance.Columns("code").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtExcelAdvance.Columns("name").Caption = Language.getMessage(mstrModuleName, 2, "Name")
            mdtExcelAdvance.Columns("name").SetOrdinal(mintColumn)
            mintColumn += 1

            If mintViewIndex > 0 Then
                mdtExcelAdvance.Columns("AGName").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Replace(":", "").Trim, mstrReport_GroupName)
                mdtExcelAdvance.Columns("AGName").SetOrdinal(mintColumn)
                mintColumn += 1
            End If

            mdtExcelAdvance.Columns("fundprojectcode").Caption = Language.getMessage(mstrModuleName, 3, "FUNDER")
            mdtExcelAdvance.Columns("fundprojectcode").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtExcelAdvance.Columns("percentage").Caption = Language.getMessage(mstrModuleName, 4, "PERCENTAGE")
            mdtExcelAdvance.Columns("percentage").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtExcelAdvance.Columns("amount").Caption = Language.getMessage(mstrModuleName, 5, "AMOUNT")
            mdtExcelAdvance.Columns("amount").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtExcelAdvance.Columns("cccode").Caption = Language.getMessage(mstrModuleName, 6, "T0")
            mdtExcelAdvance.Columns("cccode").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtExcelAdvance.Columns("fundcode").Caption = Language.getMessage(mstrModuleName, 7, "T1")
            mdtExcelAdvance.Columns("fundcode").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtExcelAdvance.Columns("fundprojectcode2").Caption = Language.getMessage(mstrModuleName, 8, "T2")
            mdtExcelAdvance.Columns("fundprojectcode2").SetOrdinal(mintColumn)
            mintColumn += 1

            mdtExcelAdvance.Columns("activity_code").Caption = Language.getMessage(mstrModuleName, 9, "T3")
            mdtExcelAdvance.Columns("activity_code").SetOrdinal(mintColumn)
            mintColumn += 1

            For i = mintColumn To mdtExcelAdvance.Columns.Count - 1
                mdtExcelAdvance.Columns.RemoveAt(mintColumn)
            Next

            Dim intArrayColWidth() As Integer = {150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150, 150}
            Dim arrGroupColumn() As String = Nothing
            If mintViewIndex > 0 Then
                Dim strGrpCols As String() = {"AGName"}
                arrGroupColumn = strGrpCols
            End If

            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenAfterExport, mdtExcelAdvance, intArrayColWidth, False, True, False, arrGroupColumn, Language.getMessage(mstrModuleName, 10, "SunAccount_ProjectJV") & " (" & mstrPeriodName & ")", , " ", , , False, , , , , False)

            Return True

        Catch ex As Exception
            exForce = New Exception(ex.Message & "; Procedure Name: Export_Report; Module Name: " & mstrModuleName)
            Throw exForce
            Return False
        End Try
    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "ID No.")
            Language.setMessage(mstrModuleName, 2, "Name")
            Language.setMessage(mstrModuleName, 3, "FUNDER")
            Language.setMessage(mstrModuleName, 4, "PERCENTAGE")
            Language.setMessage(mstrModuleName, 5, "AMOUNT")
            Language.setMessage(mstrModuleName, 6, "T0")
            Language.setMessage(mstrModuleName, 7, "T1")
            Language.setMessage(mstrModuleName, 8, "T2")
            Language.setMessage(mstrModuleName, 9, "T3")
            Language.setMessage(mstrModuleName, 10, "SunAccount_ProjectJV")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
