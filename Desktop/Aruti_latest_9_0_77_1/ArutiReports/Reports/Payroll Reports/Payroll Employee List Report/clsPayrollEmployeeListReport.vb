﻿'Class Name : clsPayrollEmployeeListReport.vb
'Purpose    :
'Date       : 17-Jul-2021
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region
Public Class clsPayrollEmployeeListReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsPayrollEmployeeListReport"
    Private mstrReportId As String = enArutiReport.Payroll_Employee_List_Report
    Dim objDataOperation As clsDataOperation
    Private menExportAction As enExportAction
    Private mintCompanyUnkid As Integer = -1
    Private mintUserUnkid As Integer = -1

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region " Private variables "

    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = ""
    Private mDicAllocationDetails As Dictionary(Of Integer, String)

#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationDetails() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mDicAllocationDetails = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriodName = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal pintReportType As Integer, _
                                           Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                           Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                           Optional ByVal intBaseCurrencyUnkid As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = ""
            OrderByQuery = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Function GetAllocationList(Optional ByVal xFilter As String = "") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Dim objData As New clsDataOperation
            StrQ = " SELECT " & _
                   "     Id " & _
                   "    ,Name " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT 1 AS Id, @BRANCH AS Name " & _
                   "    UNION SELECT 2 AS Id,  @DEPARTMENT_GROUP AS Name " & _
                   "    UNION SELECT 3 AS Id,  @DEPARTMENT AS Name " & _
                   "    UNION SELECT 4 AS Id,  @SECTION_GROUP AS Name " & _
                   "    UNION SELECT 5 AS Id,  @SECTION AS Name " & _
                   "    UNION SELECT 6 AS Id,  @UNIT_GROUP AS Name " & _
                   "    UNION SELECT 7 AS Id,  @UNIT AS Name " & _
                   "    UNION SELECT 8 AS Id,  @TEAM AS Name " & _
                   "    UNION SELECT 9 AS Id,  @JOB_GROUP AS Name " & _
                   "    UNION SELECT 10 AS Id, @JOBS AS Name " & _
                   "    UNION SELECT 11 AS Id, @CLASS_GROUP AS Name " & _
                   "    UNION SELECT 12 AS Id, @CLASSES AS Name " & _
                   "    UNION SELECT 13 AS Id, @Appointmentdate AS Name " & _
                   "    UNION SELECT 14 AS Id, @Birthdate AS Name " & _
                   "    UNION SELECT 15 AS Id, @Confirmationdate AS Name " & _
                   "    UNION SELECT 16 AS Id, @Reitrementdate AS Name " & _
                   "    UNION SELECT 17 AS Id, @EOCdate AS Name " & _
                   "    UNION SELECT 18 AS Id, @Costcenter AS Name " & _
                   "    UNION SELECT 19 AS Id, @Gradegroup AS Name " & _
                   "    UNION SELECT 20 AS Id, @Grade AS Name " & _
                   "    UNION SELECT 21 AS Id, @Gradelevel AS Name " & _
                   "    UNION SELECT 22 AS Id, @Employmenttype AS Name " & _
                   "    UNION SELECT 23 AS Id, @Gender AS Name " & _
                   "    UNION SELECT 24 AS Id, @MaritalStatus AS Name " & _
                   "    UNION SELECT 25 AS Id, @CostcenterCode AS Name " & _
                   "    UNION SELECT 26 AS Id, @CostcenterCustomCode AS Name " & _
                   ") AS A WHERE 1 = 1 "

            If xFilter.Trim.Length > 0 Then
                StrQ &= "AND " & xFilter
            End If


            objData.AddParameter("@BRANCH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 430, "Branch"))
            objData.AddParameter("@DEPARTMENT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 429, "Department Group"))
            objData.AddParameter("@DEPARTMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 428, "Department"))
            objData.AddParameter("@SECTION_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 427, "Section Group"))
            objData.AddParameter("@SECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 426, "Section"))
            objData.AddParameter("@UNIT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 425, "Unit Group"))
            objData.AddParameter("@UNIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 424, "Unit"))
            objData.AddParameter("@TEAM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 423, "Team"))
            objData.AddParameter("@JOB_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 422, "Job Group"))
            objData.AddParameter("@JOBS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 421, "Jobs"))
            objData.AddParameter("@CLASS_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 420, "Class Group"))
            objData.AddParameter("@CLASSES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 419, "Classes"))
            objData.AddParameter("@COSTCENTER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 586, "Cost Center"))
            objData.AddParameter("@GradeGroup", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1063, "Grade Group"))
            objData.AddParameter("@Grade", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1064, "Grade"))
            objData.AddParameter("@GradeLevel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1065, "Grade Level"))
            objData.AddParameter("@Appointmentdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Appointment Date"))
            objData.AddParameter("@Birthdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Birthdate"))
            objData.AddParameter("@Confirmationdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Confirmation Date"))
            objData.AddParameter("@Reitrementdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Retirement Date"))
            objData.AddParameter("@EOCdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "End Of Contract Date"))
            objData.AddParameter("@Employmenttype", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Employment Type"))
            objData.AddParameter("@Gender", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Gender"))
            objData.AddParameter("@MaritalStatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "Marital Status"))
            objData.AddParameter("@CostcenterCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "Cost Center Code"))
            objData.AddParameter("@CostcenterCustomCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "Cost Center Custom Code"))

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAllocationList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
#End Region

#Region " Report Generation "

    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                          ByVal xUserUnkid As Integer, _
                                          ByVal xYearUnkid As Integer, _
                                          ByVal xCompanyUnkid As Integer, _
                                          ByVal xPeriodStart As Date, _
                                          ByVal xPeriodEnd As Date, _
                                          ByVal xUserModeSetting As String, _
                                          ByVal xOnlyApproved As Boolean, _
                                          ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                          ByVal xExportReportPath As String, _
                                          ByVal xOpenReportAfterExport As Boolean, _
                                          Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                          Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                          Optional ByVal intBaseCurrencyUnkid As Integer = 0, _
                                          Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                          Optional ByVal blnApplyEmpExemptionFilter As Boolean = False, _
                                          Optional ByVal blnApplyEmpExemptionFilterForTerminated As Boolean = False, _
                                          Optional ByVal strAdvanceFilterQuery As String = "" _
                                          )
        Dim dtCol As DataColumn
        Dim dtFinalTable As DataTable
        Dim mdtTableExcel As New DataTable
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim xTotalStartCol As Integer = 0
        Try
            objDataOperation = New clsDataOperation
            dtFinalTable = New DataTable("Payroll")

            dtCol = New DataColumn("Period", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Period")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeCode", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "Code")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("EmployeeName", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "Employee")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            If mDicAllocationDetails.Keys.Count > 0 Then
                For Each xKey As Integer In mDicAllocationDetails.Keys
                    dtCol = New DataColumn("Col_" & xKey.ToString)
                    dtCol.Caption = mDicAllocationDetails(xKey)
                    dtCol.DataType = System.Type.GetType("System.String")
                    dtFinalTable.Columns.Add(dtCol)
                    xTotalStartCol += 1
                Next
            End If

            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, , blnExcludeTermEmp_PayProcess, xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)


            If strAdvanceFilterQuery.Trim.Length > 0 Then
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            End If


            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If

            If blnApplyEmpExemptionFilter = True Then
                strQ &= "SELECT AA.employeeunkid, SUM(TotalDays) AS TotalDays " & _
                        "INTO #TableExempt " & _
                        "FROM ( " & _
                        "SELECT A.datestranunkid " & _
                             ", A.employeeunkid " & _
                             ", A.effectivedate " & _
                             ", A.date1 " & _
                             ", A.date2 " & _
                             ", (A.date2 + 1) - A.date1 AS TotalDays " & _
                        "FROM ( " & _
                                "SELECT datestranunkid " & _
                                     ", hremployee_dates_tran.employeeunkid " & _
                                     ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                                     ", CASE WHEN CONVERT(CHAR(8), date1, 112) < '" & eZeeDate.convertDate(xPeriodStart) & "' THEN '" & eZeeDate.convertDate(xPeriodStart) & "' ELSE CONVERT(CHAR(8), date1, 112) END AS date1 " & _
                                     ", CASE WHEN date2 IS NULL THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' WHEN CONVERT(CHAR(8), date2, 112) > '" & eZeeDate.convertDate(xPeriodEnd) & "' THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' ELSE CONVERT(CHAR(8), date2, 112) END AS date2 " & _
                                     ", DENSE_RANK() OVER ( PARTITION  BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC, hremployee_dates_tran.datestranunkid DESC ) AS ROWNO " & _
                                "FROM hremployee_dates_tran " & _
                                "WHERE isvoid = 0 " & _
                                      "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                                      "AND date1 IS NOT NULL " & _
                                      "AND CONVERT(CHAR(8), effectivedate, 112) < @startdate " & _
                        " ) AS A " & _
                        " WHERE A.ROWNO = 1 " & _
                        "UNION " & _
                        "SELECT datestranunkid " & _
                             ", hremployee_dates_tran.employeeunkid " & _
                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                             ", CASE WHEN CONVERT(CHAR(8), date1, 112) < '" & eZeeDate.convertDate(xPeriodStart) & "' THEN '" & eZeeDate.convertDate(xPeriodStart) & "' ELSE CONVERT(CHAR(8), date1, 112) END AS date1 " & _
                             ", CASE WHEN date2 IS NULL THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' WHEN CONVERT(CHAR(8), date2, 112) > '" & eZeeDate.convertDate(xPeriodEnd) & "' THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' ELSE CONVERT(CHAR(8), date2, 112) END AS date2 " & _
                             ", (CASE WHEN date2 IS NULL THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' WHEN CONVERT(CHAR(8), date2, 112) > '" & eZeeDate.convertDate(xPeriodEnd) & "' THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' ELSE CONVERT(CHAR(8), date2, 112) END  + 1) - CASE WHEN CONVERT(CHAR(8), date1, 112) < '" & eZeeDate.convertDate(xPeriodStart) & "' THEN '" & eZeeDate.convertDate(xPeriodStart) & "' ELSE CONVERT(CHAR(8), date1, 112) END AS TotalDays " & _
                        "FROM hremployee_dates_tran " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                              "AND date1 IS NOT NULL " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @enddate " & _
                              "AND CONVERT(CHAR(8), date1, 112) " & _
                              "BETWEEN @startdate AND @enddate " & _
                        "UNION " & _
                        "SELECT datestranunkid " & _
                             ", hremployee_dates_tran.employeeunkid " & _
                             ", CONVERT(CHAR(8), effectivedate, 112) AS effectivedate " & _
                             ", CASE WHEN CONVERT(CHAR(8), date1, 112) < '" & eZeeDate.convertDate(xPeriodStart) & "' THEN '" & eZeeDate.convertDate(xPeriodStart) & "' ELSE CONVERT(CHAR(8), date1, 112) END AS date1 " & _
                             ", CASE WHEN date2 IS NULL THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' WHEN CONVERT(CHAR(8), date2, 112) > '" & eZeeDate.convertDate(xPeriodEnd) & "' THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' ELSE CONVERT(CHAR(8), date2, 112) END AS date2 " & _
                             ", (CASE WHEN date2 IS NULL THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' WHEN CONVERT(CHAR(8), date2, 112) > '" & eZeeDate.convertDate(xPeriodEnd) & "' THEN '" & eZeeDate.convertDate(xPeriodEnd) & "' ELSE CONVERT(CHAR(8), date2, 112) END  + 1) - CASE WHEN CONVERT(CHAR(8), date1, 112) < '" & eZeeDate.convertDate(xPeriodStart) & "' THEN '" & eZeeDate.convertDate(xPeriodStart) & "' ELSE CONVERT(CHAR(8), date1, 112) END AS TotalDays " & _
                        "FROM hremployee_dates_tran " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                              "AND date2 IS NOT NULL " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= @enddate " & _
                              "AND CONVERT(CHAR(8), date2, 112) " & _
                              "BETWEEN @startdate AND @enddate " & _
                          " ) AS AA GROUP BY AA.employeeunkid "
            End If

            strQ &= "SELECT " & _
                    "   hremployee_master.employeecode AS employeecode " & _
                    ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                    ",  hremployee_master.employeeunkid As employeeunkid " & _
                    ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                    ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                    ",  ISNULL(SM.name,'') AS Col_1 " & _
                    ",  ISNULL(DGM.name,'') AS Col_2 " & _
                    ",  ISNULL(DM.name,'') AS Col_3 " & _
                    ",  ISNULL(SECG.name,'') AS Col_4 " & _
                    ",  ISNULL(SEC.name,'') AS Col_5 " & _
                    ",  ISNULL(UGM.name,'') AS Col_6 " & _
                    ",  ISNULL(UM.name,'') AS Col_7 " & _
                    ",  ISNULL(TM.name,'') AS Col_8 " & _
                    ",  ISNULL(JGM.name,'') AS Col_9 " & _
                    ",  ISNULL(JM.job_name,'') AS Col_10 " & _
                    ",  ISNULL(CGM.name,'') AS Col_11 " & _
                    ",  ISNULL(CM.name,'') AS Col_12 " & _
                    ",  ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') AS Col_13 " & _
                    ",  ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS Col_14  " & _
                    ",  ISNULL(CONVERT(CHAR(8),ECNF.confirmation_date,112),'') AS Col_15 " & _
                    ",  ISNULL(CONVERT(CHAR(8),ERET.termination_to_date,112),'') AS Col_16 " & _
                    ",  ISNULL(CONVERT(CHAR(8),ETERM.empl_enddate,112),'') AS Col_17 " & _
                    ",  ISNULL(CC.costcentername,'') AS Col_18 " & _
                    ",  ISNULL(GGM.name,'') AS Col_19 " & _
                    ",  ISNULL(GM.name,'') AS Col_20 " & _
                    ",  ISNULL(GLM.name,'') AS Col_21 " & _
                    ",  ISNULL(etype.name,'') AS Col_22 " & _
                    ",  CASE WHEN gender = 0 THEN '' " & _
                    "      WHEN gender = 1 THEN @Male " & _
                    "      WHEN gender = 2 THEN @Female " & _
                    "   END AS Col_23 " & _
                    ",  ISNULL(mtype.name,'') AS Col_24 " & _
                    ",  ISNULL(CC.costcentercode,'') AS Col_25 " & _
                    ",  ISNULL(CC.customcode,'') AS Col_26 " & _
                    "FROM " & strDBName & "hremployee_master "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "LEFT JOIN " & _
                 "( " & _
                 "    SELECT " & _
                 "         stationunkid " & _
                 "        ,deptgroupunkid " & _
                 "        ,departmentunkid " & _
                 "        ,sectiongroupunkid " & _
                 "        ,sectionunkid " & _
                 "        ,unitgroupunkid " & _
                 "        ,unitunkid " & _
                 "        ,teamunkid " & _
                 "        ,classgroupunkid " & _
                 "        ,classunkid " & _
                 "        ,employeeunkid " & _
                 "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                 "    FROM " & strDBName & "hremployee_transfer_tran " & _
                 "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                 ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                 "LEFT JOIN " & _
                 "( " & _
                 "    SELECT " & _
                 "         jobgroupunkid " & _
                 "        ,jobunkid " & _
                 "        ,employeeunkid " & _
                 "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                 "    FROM " & strDBName & "hremployee_categorization_tran " & _
                 "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                 ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                 "LEFT JOIN " & _
                 "( " & _
                 "    SELECT " & _
                 "         cctranheadvalueid AS costcenterunkid " & _
                 "        ,employeeunkid " & _
                 "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                 "    FROM " & strDBName & "hremployee_cctranhead_tran " & _
                 "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                 ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 " & _
                 "LEFT JOIN " & _
                 "( " & _
                 "	SELECT " & _
                 "		 gradegroupunkid " & _
                 "		,gradeunkid " & _
                 "		,gradelevelunkid " & _
                 "		,employeeunkid " & _
                 "		,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                 "	FROM " & strDBName & "prsalaryincrement_tran " & _
                 "	WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                 ") AS G ON G.employeeunkid = hremployee_master.employeeunkid AND G.Rno = 1 " & _
                 "LEFT JOIN " & _
                 "( " & _
                 "   SELECT " & _
                 "        hremployee_shift_tran.employeeunkid " & _
                 "       ,hremployee_shift_tran.shiftunkid " & _
                 "       ,ROW_NUMBER()OVER(PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS Rno " & _
                 "   FROM " & strDBName & "hremployee_shift_tran " & _
                 "   WHERE hremployee_shift_tran.isvoid = 0 " & _
                 "   AND hremployee_shift_tran.effectivedate IS NOT NULL AND CONVERT(NVARCHAR(8),hremployee_shift_tran.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                 ") AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.Rno = 1 " & _
                 "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    		 TERM.TEEmpId " & _
                    "    		,TERM.empl_enddate " & _
                    "    		,TERM.termination_from_date " & _
                    "    		,TERM.TEfDt " & _
                    "    		,TERM.isexclude_payroll " & _
                    "    		,TERM.TR_REASON " & _
                    "    		,TERM.changereasonunkid " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             TRM.employeeunkid AS TEEmpId " & _
                    "            ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                    "            ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                    "            ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                    "            ,TRM.isexclude_payroll " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                    "            ,TRM.changereasonunkid " & _
                    "        FROM hremployee_dates_tran AS TRM " & _
                    "            LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                    "        WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS TERM WHERE TERM.Rno = 1 " & _
                    ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                 "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    	 CNF.CEmpId " & _
                    "    	,CNF.confirmation_date " & _
                    "       ,CNF.CEfDt " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             CNF.employeeunkid AS CEmpId " & _
                    "            ,CONVERT(CHAR(8),CNF.date1,112) AS confirmation_date " & _
                    "            ,CONVERT(CHAR(8),CNF.effectivedate,112) AS CEfDt " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY CNF.employeeunkid ORDER BY CNF.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN CNF.rehiretranunkid > 0 THEN ISNULL(CTE.name,'') ELSE ISNULL(CEC.name,'') END AS CF_REASON " & _
                    "            ,CNF.changereasonunkid " & _
                    "        FROM hremployee_dates_tran AS CNF " & _
                    "            LEFT JOIN cfcommon_master AS CTE ON CTE.masterunkid = CNF.changereasonunkid AND CTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS CEC ON CEC.masterunkid = CNF.changereasonunkid AND CEC.mastertype = '" & clsCommon_Master.enCommonMaster.CONFIRMATION & "' " & _
                    "        WHERE isvoid = 0 AND CNF.datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),CNF.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS CNF WHERE CNF.Rno = 1 " & _
                    ") AS ECNF ON ECNF.CEmpId = hremployee_master.employeeunkid AND ECNF.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    	 RET.REmpId " & _
                    "    	,RET.termination_to_date " & _
                    "    	,RET.REfDt " & _
                    "    	,RET.RET_REASON " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             RTD.employeeunkid AS REmpId " & _
                    "            ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                    "            ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                    "        FROM hremployee_dates_tran AS RTD " & _
                    "            LEFT JOIN cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                    "        WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS RET WHERE RET.Rno = 1 " & _
                    ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                 " LEFT JOIN " & strDBName & "..hrstation_master AS SM on SM.stationunkid = T.stationunkid " & _
                 " LEFT JOIN " & strDBName & "..hrdepartment_group_master AS DGM on DGM.deptgroupunkid = T.deptgroupunkid " & _
                 " LEFT JOIN " & strDBName & "..hrdepartment_master AS DM ON DM.departmentunkid = T.departmentunkid " & _
                 " LEFT JOIN " & strDBName & "..hrsectiongroup_master AS SECG ON T.sectiongroupunkid = SECG.sectiongroupunkid " & _
                 " LEFT JOIN " & strDBName & "..hrsection_master AS SEC ON SEC.sectionunkid = T.sectionunkid  " & _
                 " LEFT JOIN " & strDBName & "..hrunitgroup_master AS UGM ON T.unitgroupunkid = UGM.unitgroupunkid " & _
                 " LEFT JOIN " & strDBName & "..hrunit_master AS UM on UM.unitunkid = T.unitunkid   " & _
                 " LEFT JOIN " & strDBName & "..hrteam_master AS TM ON T.teamunkid = TM.teamunkid " & _
                 " LEFT JOIN " & strDBName & "..hrjobgroup_master AS JGM on JGM.jobgroupunkid = J.jobgroupunkid  " & _
                 " LEFT JOIN " & strDBName & "..hrjob_master AS JM ON JM.jobunkid = J.jobunkid " & _
                 " LEFT JOIN " & strDBName & "..hrclassgroup_master AS CGM ON CGM.classgroupunkid = T.classgroupunkid   " & _
                 " LEFT JOIN " & strDBName & "..hrclasses_master AS CM on CM.classesunkid = T.classunkid   " & _
                 " LEFT JOIN " & strDBName & "..prcostcenter_master AS CC on CC.costcenterunkid = C.costcenterunkid   " & _
                 " LEFT JOIN " & strDBName & "..hrgradegroup_master AS GGM on GGM.gradegroupunkid = G.gradegroupunkid   " & _
                 " LEFT JOIN " & strDBName & "..hrgrade_master AS GM on GM.gradeunkid = G.gradeunkid   " & _
                 " LEFT JOIN " & strDBName & "..hrgradelevel_master AS GLM on GLM.gradelevelunkid = G.gradelevelunkid  " & _
                 " LEFT JOIN " & strDBName & "..cfcommon_master as mtype ON mtype.masterunkid = hremployee_master.maritalstatusunkid AND mtype.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
                 " LEFT JOIN " & strDBName & "..cfcommon_master AS etype ON etype.masterunkid = hremployee_master.employmenttypeunkid AND etype.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " "

            If blnApplyEmpExemptionFilter = True Then
                strQ &= " LEFT JOIN #TableExempt ON hremployee_master.employeeunkid = #TableExempt.employeeunkid "
            End If


            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            strQ &= " WHERE 1=1 "

            If blnApplyEmpExemptionFilter = True AndAlso blnApplyEmpExemptionFilterForTerminated = False Then
                strQ &= " AND (#TableExempt.employeeunkid IS NULL OR " & DateDiff(DateInterval.Day, xPeriodStart, xPeriodEnd.AddDays(1)) & " > #TableExempt.TotalDays ) "
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If

            If strAdvanceFilterQuery.Trim <> "" Then
                strQ &= " AND " & strAdvanceFilterQuery & " "
            End If

            If blnApplyEmpExemptionFilter = True Then
                strQ &= " DROP TABLE #TableExempt "
            End If

            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Female"))
            dsList = objDataOperation.ExecQuery(strQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intCount As Integer = 1
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            For ii As Integer = 0 To intRowCount - 1
                drRow = dsList.Tables(0).Rows(ii)
                rpt_Row = dtFinalTable.NewRow

                rpt_Row.Item("Period") = mstrPeriodName
                rpt_Row.Item("EmployeeCode") = drRow.Item("employeecode")
                rpt_Row.Item("EmployeeName") = drRow.Item("employeename")

                If dtFinalTable.Columns.Contains("Col_1") Then
                    rpt_Row.Item("Col_1") = drRow.Item("Col_1")
                End If

                If dtFinalTable.Columns.Contains("Col_2") Then
                    rpt_Row.Item("Col_2") = drRow.Item("Col_2")
                End If

                If dtFinalTable.Columns.Contains("Col_3") Then
                    rpt_Row.Item("Col_3") = drRow.Item("Col_3")
                End If

                If dtFinalTable.Columns.Contains("Col_4") Then
                    rpt_Row.Item("Col_4") = drRow.Item("Col_4")
                End If

                If dtFinalTable.Columns.Contains("Col_5") Then
                    rpt_Row.Item("Col_5") = drRow.Item("Col_5")
                End If

                If dtFinalTable.Columns.Contains("Col_6") Then
                    rpt_Row.Item("Col_6") = drRow.Item("Col_6")
                End If

                If dtFinalTable.Columns.Contains("Col_7") Then
                    rpt_Row.Item("Col_7") = drRow.Item("Col_7")
                End If

                If dtFinalTable.Columns.Contains("Col_8") Then
                    rpt_Row.Item("Col_8") = drRow.Item("Col_8")
                End If

                If dtFinalTable.Columns.Contains("Col_9") Then
                    rpt_Row.Item("Col_9") = drRow.Item("Col_9")
                End If

                If dtFinalTable.Columns.Contains("Col_10") Then
                    rpt_Row.Item("Col_10") = drRow.Item("Col_10")
                End If

                If dtFinalTable.Columns.Contains("Col_11") Then
                    rpt_Row.Item("Col_11") = drRow.Item("Col_11")
                End If

                If dtFinalTable.Columns.Contains("Col_12") Then
                    rpt_Row.Item("Col_12") = drRow.Item("Col_12")
                End If

                If dtFinalTable.Columns.Contains("Col_13") Then
                    If drRow.Item("Col_13").ToString.Trim.Length > 0 Then
                        rpt_Row.Item("Col_13") = eZeeDate.convertDate(drRow.Item("Col_13").ToString).ToShortDateString
                    End If
                End If

                If dtFinalTable.Columns.Contains("Col_14") Then
                    If drRow.Item("Col_14").ToString.Trim.Length > 0 Then
                        rpt_Row.Item("Col_14") = eZeeDate.convertDate(drRow.Item("Col_14").ToString).ToShortDateString
                    End If
                End If

                If dtFinalTable.Columns.Contains("Col_15") Then
                    If drRow.Item("Col_15").ToString.Trim.Length > 0 Then
                        rpt_Row.Item("Col_15") = eZeeDate.convertDate(drRow.Item("Col_15").ToString).ToShortDateString
                    End If
                End If

                If dtFinalTable.Columns.Contains("Col_16") Then
                    If drRow.Item("Col_16").ToString.Trim.Length > 0 Then
                        rpt_Row.Item("Col_16") = eZeeDate.convertDate(drRow.Item("Col_16").ToString).ToShortDateString
                    End If
                End If

                If dtFinalTable.Columns.Contains("Col_17") Then
                    If drRow.Item("Col_17").ToString.Trim.Length > 0 Then
                        rpt_Row.Item("Col_17") = eZeeDate.convertDate(drRow.Item("Col_17").ToString).ToShortDateString
                    End If
                End If

                If dtFinalTable.Columns.Contains("Col_18") Then
                    rpt_Row.Item("Col_18") = drRow.Item("Col_18")
                End If

                If dtFinalTable.Columns.Contains("Col_19") Then
                    rpt_Row.Item("Col_19") = drRow.Item("Col_19")
                End If

                If dtFinalTable.Columns.Contains("Col_20") Then
                    rpt_Row.Item("Col_20") = drRow.Item("Col_20")
                End If

                If dtFinalTable.Columns.Contains("Col_21") Then
                    rpt_Row.Item("Col_21") = drRow.Item("Col_21")
                End If

                If dtFinalTable.Columns.Contains("Col_22") Then
                    rpt_Row.Item("Col_22") = drRow.Item("Col_22")
                End If

                If dtFinalTable.Columns.Contains("Col_23") Then
                    rpt_Row.Item("Col_23") = drRow.Item("Col_23")
                End If

                If dtFinalTable.Columns.Contains("Col_24") Then
                    rpt_Row.Item("Col_24") = drRow.Item("Col_24")
                End If

                If dtFinalTable.Columns.Contains("Col_25") Then
                    rpt_Row.Item("Col_25") = drRow.Item("Col_25")
                End If

                If dtFinalTable.Columns.Contains("Col_26") Then
                    rpt_Row.Item("Col_26") = drRow.Item("Col_26")
                End If

                dtFinalTable.Rows.Add(rpt_Row)

                intCount = intCount + 1
            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable

            Dim strarrGroupColumns As String() = Nothing

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))

            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell

            Dim DistinctColumns As String() = mdtTableExcel.Columns.Cast(Of DataColumn)().[Select](Function(x) x.ColumnName).ToArray()
            mdtTableExcel = mdtTableExcel.DefaultView.ToTable(True, DistinctColumns)

            row = New WorksheetRow()

            If Me._FilterTitle.ToString.Length > 0 Then
                wcell = New WorksheetCell(Me._FilterTitle.ToString, "s10bw")
                wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                row.Cells.Add(wcell)
            End If
            rowsArrayHeader.Add(row)

            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayHeader.Add(row)


            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s10bw")
            row.Cells.Add(wcell)
            rowsArrayFooter.Add(row)

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                row = New WorksheetRow()

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = xUserUnkid
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 6, "Prepared By :") & Space(10) & objUser._Username, "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                objUser = Nothing

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowCheckedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 7, "Checked By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If


            If ConfigParameter._Object._IsShowApprovedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 8, "Approved By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)

                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)

                row = New WorksheetRow()
                wcell = New WorksheetCell("", "s10bw")
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                row = New WorksheetRow()
                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 9, "Received By :"), "s8bw")
                wcell.MergeAcross = 4
                row.Cells.Add(wcell)
                rowsArrayFooter.Add(row)
            End If

            'SET EXCEL CELL WIDTH  
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                If i = 2 Then
                    intArrayColumnWidth(i) = 200
                Else
                    intArrayColumnWidth(i) = 125
                End If

            Next
            'SET EXCEL CELL WIDTH


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, "", "", " ", Nothing, "", True, rowsArrayHeader)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage("clsMasterData", 419, "Classes")
			Language.setMessage("clsMasterData", 420, "Class Group")
			Language.setMessage("clsMasterData", 421, "Jobs")
			Language.setMessage("clsMasterData", 422, "Job Group")
			Language.setMessage("clsMasterData", 423, "Team")
			Language.setMessage("clsMasterData", 424, "Unit")
			Language.setMessage("clsMasterData", 425, "Unit Group")
			Language.setMessage("clsMasterData", 426, "Section")
			Language.setMessage("clsMasterData", 427, "Section Group")
			Language.setMessage("clsMasterData", 428, "Department")
			Language.setMessage("clsMasterData", 429, "Department Group")
			Language.setMessage("clsMasterData", 430, "Branch")
			Language.setMessage("clsMasterData", 586, "Cost Center")
			Language.setMessage("clsMasterData", 1063, "Grade Group")
			Language.setMessage("clsMasterData", 1064, "Grade")
			Language.setMessage("clsMasterData", 1065, "Grade Level")
			Language.setMessage(mstrModuleName, 1, "Period")
			Language.setMessage(mstrModuleName, 2, "Code")
			Language.setMessage(mstrModuleName, 3, "Employee")
			Language.setMessage(mstrModuleName, 4, "Male")
			Language.setMessage(mstrModuleName, 5, "Female")
			Language.setMessage(mstrModuleName, 6, "Prepared By :")
			Language.setMessage(mstrModuleName, 7, "Checked By :")
			Language.setMessage(mstrModuleName, 8, "Approved By :")
			Language.setMessage(mstrModuleName, 9, "Received By :")
			Language.setMessage(mstrModuleName, 10, "Appointment Date")
			Language.setMessage(mstrModuleName, 11, "Birthdate")
			Language.setMessage(mstrModuleName, 12, "Confirmation Date")
			Language.setMessage(mstrModuleName, 13, "Retirement Date")
			Language.setMessage(mstrModuleName, 14, "End Of Contract Date")
			Language.setMessage(mstrModuleName, 15, "Employment Type")
			Language.setMessage(mstrModuleName, 16, "Gender")
			Language.setMessage(mstrModuleName, 17, "Marital Status")
			Language.setMessage(mstrModuleName, 18, "Cost Center Code")
			Language.setMessage(mstrModuleName, 19, "Cost Center Custom Code")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
