'************************************************************************************************************************************
'Class Name : frmEDDetailReport.vb
'Purpose    : 
'Written By : Pinkal Jariwala
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmEDDetailReport

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmEDDetailReport"
    Private objEDDetail As clsEDDetailReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    'Sohail (16 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (16 May 2012) -- End



    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes
    Private dsPeriod As DataSet = Nothing
    'Pinkal (03-Sep-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

    'Sohail (25 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrTranDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0
    'Sohail (25 Mar 2013) -- End

#End Region

#Region " Constructor "

    Public Sub New()
        objEDDetail = New clsEDDetailReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEDDetail.SetDefaultValue()
        InitializeComponent()


        'Pinkal (09-Jan-2013) -- Start
        'Enhancement : TRA Changes
        _Show_ExcelExtra_Menu = True
        'Pinkal (09-Jan-2013) -- End

        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End

    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objperiod As New clscommom_period_Tran
            Dim dsList As New DataSet
            Dim objUserdefReport As New clsUserDef_ReportMode
            Dim objMaster As New clsMasterData 'Sohail (25 Mar 2013)
         
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, False)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, True, "Emp", True)
            'Nilay (10-Feb-2016) -- End

            'Anjan [10 June 2015] -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid) 'Sohail (25 Mar 2013)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1) 'Sohail (25 Mar 2013)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (03 Dec 2013) -- Start
            'Enhancement - TBC - Inclusion of Closed FY periods
            'dsPeriod = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsPeriod = objperiod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)
            dsPeriod = objperiod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            'Sohail (03 Dec 2013) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsPeriod.Tables(0)
                'Sohail (25 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                '.SelectedValue = 0
                .SelectedValue = mintFirstOpenPeriod
                'Sohail (25 Mar 2013) -- End
            End With


            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsPeriod.Tables(0).Copy
                'Sohail (25 Mar 2013) -- Start
                'TRA - ENHANCEMENT
                '.SelectedValue = 0
                .SelectedValue = mintFirstOpenPeriod
                'Sohail (25 Mar 2013) -- End
            End With
            objperiod = Nothing
            'Pinkal (03-Sep-2012) -- End



            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes

            'dsList = objUserdefReport.GetModeReportList
            'If dsList.Tables(0).Rows.Count > 1 Then dsList.Tables(0).Rows.RemoveAt(2)
            'dsList.AcceptChanges()
            'With cboMode
            '    .ValueMember = "Id"
            '    .DisplayMember = "Name"
            '    .DataSource = dsList.Tables(0)
            'End With

            'Dim objMaster As New clsMasterData 'Sohail (25 Mar 2013)
            dsList = objMaster.getComboListForHeadType("List")
            'Sohail (10 Sep 2014) -- Start
            'Enhancement - Pay Per Activity on E&D Detail Report.
            'Dim dr As DataRow = dsList.Tables(0).NewRow
            'dr.Item("Id") = 99999
            'dr.Item("Name") = Language.getMessage(mstrModuleName, 7, "PAY PER ACTIVITY")
            'dsList.Tables(0).Rows.Add(dr)
            'Sohail (10 Sep 2014) -- End
            'Sohail (23 Jun 2015) -- Start
            'Enhancement - Claim Request Expense on E&D Detail Reports.
            Dim dr As DataRow = dsList.Tables(0).NewRow
            dr.Item("Id") = 99998
            dr.Item("Name") = Language.getMessage(mstrModuleName, 8, "Claim Request Expense")
            dsList.Tables(0).Rows.Add(dr)
            'Sohail (23 Jun 2015) -- End
            With cboMode
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dsList.Tables(0)
            End With
            'Pinkal (05-Mar-2012) -- End


            'Pinkal (08-Jun-2013) -- Start
            'Enhancement : TRA Changes.

            Dim objMember As New clsmembership_master
            dsList = objMember.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            'Pinkal (08-Jun-2013) -- End




            dsList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        'Sohail (03 Dec 2013) -- Start
        'Enhancement - TBC
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim strPeriodIDs As String = ""
        Dim arrDatabaseName As New ArrayList
        Dim i As Integer = 0
        Dim intPrevYearID As Integer = 0
        'Sohail (03 Dec 2013) -- End
        Dim mdicYearDBName As New Dictionary(Of Integer, String) 'Sohail (21 Aug 2015)

        Try

            objEDDetail.SetDefaultValue()

            objEDDetail._EmployeeId = CInt(cboEmployee.SelectedValue)
            objEDDetail._EmployeeName = cboEmployee.Text

            objEDDetail._ModeId = CInt(cboMode.SelectedValue)
            objEDDetail._ModeName = cboMode.Text

            objEDDetail._HeadId = CInt(cboHeades.SelectedValue)
            objEDDetail._HeadName = cboHeades.Text
            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If cboHeades.DataSource IsNot Nothing Then
                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                'Dim dtRow() As DataRow = CType(cboHeades.DataSource, DataTable).Select("tranheadunkid = '" & CInt(cboHeades.SelectedValue) & "'")
                'If dtRow.Length > 0 Then
                '    objEDDetail._HeadCode = dtRow(0)("code").ToString
                'End If
                'If CInt(cboMode.SelectedValue) = 99999 Then
                '    objEDDetail._HeadCode = CType(cboHeades.SelectedItem, DataRowView).Item("code").ToString
                'Else
                    objEDDetail._HeadCode = CType(cboHeades.SelectedItem, DataRowView).Item("code").ToString
                'End If
                'Sohail (10 Sep 2014) -- End
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END

            objEDDetail._PeriodId = CInt(cboPeriod.SelectedValue)
            objEDDetail._PeriodName = cboPeriod.Text

            objEDDetail._ViewByIds = mstrStringIds
            objEDDetail._ViewIndex = mintViewIdx
            objEDDetail._ViewByName = mstrStringName
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            objEDDetail._Analysis_Fields = mstrAnalysis_Fields
            objEDDetail._Analysis_Join = mstrAnalysis_Join
            objEDDetail._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEDDetail._Report_GroupName = mstrReport_GroupName
            'Sohail (16 May 2012) -- End

            'Sohail (20 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            objEDDetail._IsActive = chkInactiveemp.Checked
            objEDDetail._IncludeNegativeHeads = chkIncludeNegativeHeads.Checked 'Sohail (19 Dec 2012)

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objEDDetail._PeriodStartDate = objPeriod._Start_Date


            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            'objEDDetail._PeriodEndDate = objPeriod._End_Date

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboToPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objEDDetail._PeriodEndDate = objPeriod._End_Date

            objEDDetail._ToPeriodId = CInt(cboToPeriod.SelectedValue)
            objEDDetail._ToPeriodName = cboToPeriod.Text

            'Sohail (03 Dec 2013) -- Start
            'Enhancement - TBC
            'If dsPeriod IsNot Nothing And dsPeriod.Tables.Count > 0 Then

            '    Dim drRow() As DataRow = dsPeriod.Tables(0).Select("periodunkid >= " & CInt(cboPeriod.SelectedValue) & " AND periodunkid<= " & CInt(cboToPeriod.SelectedValue))

            '    If drRow.Length > 0 Then
            '        Dim mstrPeriod As String = ""
            '        For i As Integer = 0 To drRow.Length - 1
            '            mstrPeriod &= drRow(i)("periodunkid").ToString() & ","
            '        Next

            '        If mstrPeriod.Trim.Length > 0 Then
            '            mstrPeriod = mstrPeriod.Substring(0, mstrPeriod.Trim.Length - 1)
            '        End If

            '        objEDDetail._mstrPeriodID = mstrPeriod

            '    End If

            'End If
            For Each dsRow As DataRow In dsPeriod.Tables(0).Rows
                If i >= cboPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                    strPeriodIDs &= ", " & dsRow.Item("periodunkid").ToString
                    If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                        dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(dsRow.Item("yearunkid")))
                        If dsList.Tables("Database").Rows.Count > 0 Then
                            arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            If mdicYearDBName.ContainsKey(CInt(dsRow.Item("yearunkid"))) = False Then
                                mdicYearDBName.Add(CInt(dsRow.Item("yearunkid")), dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                            End If
                            'Sohail (21 Aug 2015) -- End
                        End If
                End If
                    intPrevYearID = CInt(dsRow.Item("yearunkid"))
            End If
                i += 1
            Next
            If strPeriodIDs.Trim <> "" Then strPeriodIDs = strPeriodIDs.Substring(2)
            If arrDatabaseName.Count <= 0 Then Return False

            objEDDetail._mstrPeriodID = strPeriodIDs
            objEDDetail._Arr_DatabaseName = arrDatabaseName
            'Sohail (03 Dec 2013) -- End
            'Pinkal (03-Sep-2012) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            objEDDetail._mdicYearDBName = mdicYearDBName
            'Sohail (21 Aug 2015) -- End


            objPeriod = Nothing
            'Sohail (20 Apr 2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objEDDetail._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End


            'Pinkal (08-Jun-2013) -- Start
            'Enhancement : TRA Changes
            objEDDetail._MembershipId = CInt(cboMembership.SelectedValue)
            objEDDetail._MemberShipName = cboMembership.Text
            'Pinkal (08-Jun-2013) -- End

            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            objEDDetail._IsLogoCompanyInfo = radLogoCompanyInfo.Checked
            objEDDetail._IsOnlyLogo = radLogo.Checked
            objEDDetail._IsOnlyCompanyInfo = radCompanyDetails.Checked
            objEDDetail._PayslipTemplate = ConfigParameter._Object._PayslipTemplate
            'Sohail (09 Feb 2016) -- End

            'Sohail (13 Feb 2016) -- Start
            'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
            objEDDetail._ApplyUserAccessFilter = True
            'Sohail ((13 Feb 2016) -- End
            'Sohail (04 Aug 2016) -- Start
            'Enhancement - 63.1 - Consolidated Employee name on E&D Detail Report for ASP.
            objEDDetail._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            'Sohail (04 Aug 2016) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objEDDetail.setDefaultOrderBy(0)
            txtOrderBy.Text = objEDDetail.OrderByDisplay
            cboEmployee.SelectedIndex = 0
            cboMode.SelectedIndex = 0
            cboHeades.SelectedIndex = 0
            'Sohail (25 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'cboPeriod.SelectedIndex = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            'Sohail (25 Mar 2013) -- End
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'Sohail (16 May 2012) -- End

            'Sohail (20 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            chkInactiveemp.Checked = False
            'Sohail (20 Apr 2012) -- End
            chkIncludeNegativeHeads.Checked = True 'Sohail (19 Dec 2012)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            'Sohail (25 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'cboToPeriod.SelectedIndex = 0
            cboToPeriod.SelectedValue = mintFirstOpenPeriod
            'Sohail (25 Mar 2013) -- End
            'Pinkal (03-Sep-2012) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End


            'Pinkal (08-Jun-2013) -- Start
            'Enhancement : TRA Changes
            cboMembership.SelectedValue = 0
            'Pinkal (08-Jun-2013) -- End

            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            Select Case ConfigParameter._Object._LogoOnPayslip
                Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                    radLogoCompanyInfo.Checked = True
                Case enLogoOnPayslip.COMPANY_DETAILS
                    radCompanyDetails.Checked = True
                Case enLogoOnPayslip.LOGO
                    radLogo.Checked = True
            End Select
            'Sohail (09 Feb 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmEDDetailReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEDDetail = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDDetailReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDDetailReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objEDDetail._ReportName
            Me._Message = objEDDetail._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDSpreadSheetReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDDetailReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmEDDetailReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Buttons"

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    'Sohail (04 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub objbtnSearchTranHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTranHead.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboHeades
                objfrm.DataSource = .DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "code"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTranHead_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (04 Feb 2012) -- End

    Private Sub frmEDDetailReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try


            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes
            If CInt(cboMode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Mode."), enMsgBoxStyle.Information)
                cboMode.Select()
                Exit Sub
            ElseIf CInt(cboHeades.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Head."), enMsgBoxStyle.Information)
                cboHeades.Select()
                Exit Sub
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Sub
            End If

            'Pinkal (05-Mar-2012) -- End

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            If CInt(cboPeriod.SelectedValue) <= 0 And CInt(cboToPeriod.SelectedValue) > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select From Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Sub

            ElseIf CInt(cboPeriod.SelectedValue) > 0 And CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Select To Period."), enMsgBoxStyle.Information)
                cboToPeriod.Select()
                Exit Sub

            End If

            If cboToPeriod.SelectedIndex < cboPeriod.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, " To Period cannot be less than From Period"), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Sub
            End If

            'Pinkal (03-Sep-2012) -- End

            If SetFilter() = False Then Exit Sub

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEDDetail.generateReport(0, e.Type, enExportAction.None)
            Dim dtPeriodStart As Date
            Dim dtPeriodEnd As Date
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            dtPeriodStart = objPeriod._Start_Date
            objPeriod = New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            dtPeriodEnd = objPeriod._End_Date
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objEDDetail._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(dtPeriodEnd))
            'Sohail (03 Aug 2019) -- End
            objEDDetail.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDDetailReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDDetailReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes

            If CInt(cboMode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Mode."), enMsgBoxStyle.Information)
                cboMode.Select()
                Exit Sub
            ElseIf CInt(cboHeades.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Head."), enMsgBoxStyle.Information)
                cboHeades.Select()
                Exit Sub
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Sub
            End If

            'Pinkal (05-Mar-2012) -- End



            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            If CInt(cboPeriod.SelectedValue) <= 0 And CInt(cboToPeriod.SelectedValue) > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select From Period."), enMsgBoxStyle.Information)
                cboPeriod.Select()
                Exit Sub

            ElseIf CInt(cboPeriod.SelectedValue) > 0 And CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please Select To Period."), enMsgBoxStyle.Information)
                cboToPeriod.Select()
                Exit Sub

            End If

            If cboToPeriod.SelectedIndex < cboPeriod.SelectedIndex Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, " To Period cannot be less than From Period"), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Exit Sub
            End If

            'Pinkal (03-Sep-2012) -- End



            If SetFilter() = False Then Exit Sub
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEDDetail.generateReport(0, enPrintAction.None, e.Type)
            Dim dtPeriodStart As Date
            Dim dtPeriodEnd As Date
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            dtPeriodStart = objPeriod._Start_Date
            objPeriod = New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            dtPeriodEnd = objPeriod._End_Date
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objEDDetail._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(dtPeriodEnd))
            'Sohail (03 Aug 2019) -- End

            'Nilay (10-Feb-2016) -- Start
            'objEDDetail.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
            '                              dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, chkInactiveemp.Checked, _
            '                              ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)

            objEDDetail.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                          dtPeriodStart, dtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, _
                                          ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'Nilay (10-Feb-2016) -- End

            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDDetailReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDDetailReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDDetailReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDDetailReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDDetailReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub frmEDDetailReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEDDetailReport.SetMessages()
            objfrm._Other_ModuleNames = "clsEDDetailReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEDDetailReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    'Pinkal (27-Feb-2013) -- End


    'Pinkal (08-Jun-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub objbtnSearchMembership_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchMembership.Click
        Dim objfrm As New frmCommonSearch
        Try
            With cboMembership
                objfrm.DataSource = cboMembership.DataSource
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchMembership_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (08-Jun-2013) -- End



#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            'Sohail (16 May 2012) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
            'Sohail (16 May 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

#Region "Control"

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objEDDetail.setOrderBy(0)
            txtOrderBy.Text = objEDDetail.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub


#End Region

#Region "ComboBox Event"

    Private Sub cboMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Dim dsFill As DataSet = Nothing
        Try
            'Dim objTransactionhead As New clsTransactionHead 'Sohail (10 Sep 2014)


            'Pinkal (05-Mar-2012) -- Start
                'Enhancement : TRA Changes

            'If CInt(cboMode.SelectedIndex) = 0 Then

            '    'Pinkal (06-Feb-2012) -- Start
            '    'Enhancement : TRA Changes
            '    'dsFill = objTransactionhead.getComboList("List", True, enTranHeadType.EarningForEmployees, enCalcType.FlatRate_Others, , )
            '    dsFill = objTransactionhead.getComboList("List", True, enTranHeadType.EarningForEmployees)
            '    'Pinkal (06-Feb-2012) -- End


            'ElseIf CInt(cboMode.SelectedIndex) = 1 Then

            '    'Pinkal (06-Feb-2012) -- Start
            '    'Enhancement : TRA Changes
            '    'dsFill = objTransactionhead.getComboList("List", True, enTranHeadType.DeductionForEmployee, enCalcType.FlatRate_Others, , )
            '    dsFill = objTransactionhead.getComboList("List", True, enTranHeadType.DeductionForEmployee)
            '    'Pinkal (06-Feb-2012) -- End


            'End If


            'Sohail (10 Sep 2014) -- Start
            'Enhancement - Pay Per Activity on E&D Detail Report.
            'Sohail (17 Sep 2014) -- Start
            'Enhancement - System generated Base, OT and PH heads for PPA Activity.
            'dsFill = objTransactionhead.getComboList("List", True, CInt(cboMode.SelectedValue))
            'dsFill = objTransactionhead.getComboList("List", True, CInt(cboMode.SelectedValue), , , , , , , , True)
            'Sohail (17 Sep 2014) -- End
            'If CInt(cboMode.SelectedValue) = 99999 Then
            '    Dim objActivity As New clsActivity_Master
            '    dsFill = objActivity.getComboList("List", True)
            'Else
            '    Dim objTransactionhead As New clsTransactionHead
            '    dsFill = objTransactionhead.getComboList("List", True, CInt(cboMode.SelectedValue))
            'End If
            'cboHeades.DataSource = Nothing
            'Sohail (10 Sep 2014) -- End
            'Sohail (23 Jun 2015) -- Start
            'Enhancement - Claim Request Expense on E&D Detail Reports.
            If CInt(cboMode.SelectedValue) = 99998 Then
                Dim objExpense As New clsExpense_Master
                dsFill = objExpense.getComboList(0, True, "List")
            Else
                Dim objTransactionhead As New clsTransactionHead
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsFill = objTransactionhead.getComboList("List", True, CInt(cboMode.SelectedValue), , , , , , , , True)
                dsFill = objTransactionhead.getComboList(FinancialYear._Object._DatabaseName, "List", True, CInt(cboMode.SelectedValue), , , , , , , True)
                'Sohail (21 Aug 2015) -- End
            End If
            cboHeades.DataSource = Nothing
            'Sohail (23 Jun 2015) -- End

            'Pinkal (05-Mar-2012) -- End

            'Sohail (10 Sep 2014) -- Start
            'Enhancement - Pay Per Activity on E&D Detail Report.
            'cboHeades.ValueMember = "tranheadunkid"
            'If CInt(cboMode.SelectedValue) = 99999 Then
            '    cboHeades.ValueMember = "activityunkid"
            'Else
            'cboHeades.ValueMember = "tranheadunkid"
            'End If
            'Sohail (10 Sep 2014) -- End
            'Sohail (23 Jun 2015) -- Start
            'Enhancement - Claim Request Expense on E&D Detail Reports.
            If CInt(cboMode.SelectedValue) = 99998 Then
                cboHeades.ValueMember = "Id"
            Else
                cboHeades.ValueMember = "tranheadunkid"
            End If
            'Sohail (23 Jun 2015) -- End
            cboHeades.DisplayMember = "Name"
            cboHeades.DataSource = dsFill.Tables("List")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region
    

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblHeads.Text = Language._Object.getCaption(Me.lblHeads.Name, Me.lblHeads.Text)
			Me.lblSelectMode.Text = Language._Object.getCaption(Me.lblSelectMode.Name, Me.lblSelectMode.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.LblTo.Text = Language._Object.getCaption(Me.LblTo.Name, Me.LblTo.Text)
			Me.chkIncludeNegativeHeads.Text = Language._Object.getCaption(Me.chkIncludeNegativeHeads.Name, Me.chkIncludeNegativeHeads.Text)
			Me.LblMembership.Text = Language._Object.getCaption(Me.LblMembership.Name, Me.LblMembership.Text)
			Me.radLogo.Text = Language._Object.getCaption(Me.radLogo.Name, Me.radLogo.Text)
			Me.radCompanyDetails.Text = Language._Object.getCaption(Me.radCompanyDetails.Name, Me.radCompanyDetails.Text)
			Me.radLogoCompanyInfo.Text = Language._Object.getCaption(Me.radLogoCompanyInfo.Name, Me.radLogoCompanyInfo.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Mode.")
			Language.setMessage(mstrModuleName, 2, "Please Select Head.")
			Language.setMessage(mstrModuleName, 3, "Please Select Period.")
			Language.setMessage(mstrModuleName, 4, "Please Select From Period.")
			Language.setMessage(mstrModuleName, 5, "Please Select To Period.")
			Language.setMessage(mstrModuleName, 6, " To Period cannot be less than From Period")
                        Language.setMessage(mstrModuleName, 8, "Claim Request Expense")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
