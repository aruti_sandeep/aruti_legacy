'************************************************************************************************************************************
'Class Name : frmPayementVoucherReport.vb
'Purpose    : 
'Written By : Pinkal
'Created   : 26-Feb-2018
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPayementVoucherReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPayementVoucherReport"
    Private objPaymentVoucher As clsPaymentVoucherReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""

#End Region

#Region " Constructor "

    Public Sub New()
        objPaymentVoucher = New clsPaymentVoucherReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objPaymentVoucher.SetDefaultValue()
        _Show_AdvanceFilter = True
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmployee As New clsEmployee_Master

            Dim dsList As DataSet = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsList.Tables(0)
            cboEmployee.SelectedValue = 0
            objEmployee = Nothing

            dsList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objPaymentVoucher.SetDefaultValue()
            objPaymentVoucher._AdvanceVoucherNo = txtAdvanceVoucherNo.Text.Trim()
            objPaymentVoucher._EmployeeID = CInt(cboEmployee.SelectedValue)
            objPaymentVoucher._EmployeeName = cboEmployee.Text
            objPaymentVoucher._VoucherFromDate = dtpVoucherFromDate.Value.Date
            objPaymentVoucher._VouncherToDate = dtpVoucherToDate.Value.Date

            objPaymentVoucher._ViewByIds = mstrViewByIds
            objPaymentVoucher._ViewIndex = mintViewIndex
            objPaymentVoucher._ViewByName = mstrViewByName
            objPaymentVoucher._Analysis_Fields = mstrAnalysis_Fields
            objPaymentVoucher._Analysis_Join = mstrAnalysis_Join
            objPaymentVoucher._Analysis_OrderBy = mstrAnalysis_OrderBy
            objPaymentVoucher._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objPaymentVoucher._Advance_Filter = mstrAdvanceFilter

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            'objPaymentVoucher.setDefaultOrderBy(0)
            cboEmployee.SelectedValue = 0
            txtAdvanceVoucherNo.Text = ""
            dtpVoucherFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpVoucherToDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            mstrStringIds = ""
            mstrStringName = ""
            mstrViewByIds = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmPayementVoucherReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPaymentVoucher = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayementVoucherReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayementVoucherReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objPaymentVoucher._ReportName
            Me._Message = objPaymentVoucher._ReportDesc
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayementVoucherReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayementVoucherReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPayementVoucherReport_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Buttons"

    Private Sub objbtnSearchEmployeee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objfrm As New frmCommonSearch
        Try
            With cboEmployee
                objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployeee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmPayementVoucherReport_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If SetFilter() = False Then Exit Sub

            objPaymentVoucher.generateReportNew(FinancialYear._Object._DatabaseName, _
                                     User._Object._Userunkid, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     dtpVoucherFromDate.Value.Date, _
                                     dtpVoucherToDate.Value.Date, _
                                     ConfigParameter._Object._UserAccessModeSetting, True, _
                                     ConfigParameter._Object._ExportReportPath, _
                                     ConfigParameter._Object._OpenAfterExport, _
                                     0, e.Type, enExportAction.None)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayementVoucherReport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayementVoucherReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If SetFilter() = False Then Exit Sub
            objPaymentVoucher.generateReportNew(FinancialYear._Object._DatabaseName, _
                                     User._Object._Userunkid, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     dtpVoucherFromDate.Value.Date, _
                                     dtpVoucherToDate.Value.Date, _
                                     ConfigParameter._Object._UserAccessModeSetting, True, _
                                     ConfigParameter._Object._ExportReportPath, _
                                     ConfigParameter._Object._OpenAfterExport, _
                                     0, enPrintAction.None, e.Type)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayementVoucherReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayementVoucherReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayementVoucherReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayementVoucherReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayementVoucherReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPaymentVoucherReport.SetMessages()
            objfrm._Other_ModuleNames = "clsPaymentVoucherReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


#End Region

#Region "Link Button"

    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
        Try
            Dim objLnkAnalys As New frmViewAnalysis
            objLnkAnalys.displayDialog()
            mstrViewByIds = objLnkAnalys._ReportBy_Ids
            mstrViewByName = objLnkAnalys._ReportBy_Name
            mintViewIndex = objLnkAnalys._ViewIndex
            mstrAnalysis_Fields = objLnkAnalys._Analysis_Fields
            mstrAnalysis_Join = objLnkAnalys._Analysis_Join
            mstrAnalysis_OrderBy = objLnkAnalys._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = objLnkAnalys._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region





	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.LblAdvanceVoucherNo.Text = Language._Object.getCaption(Me.LblAdvanceVoucherNo.Name, Me.LblAdvanceVoucherNo.Text)
			Me.LblVoucherToDate.Text = Language._Object.getCaption(Me.LblVoucherToDate.Name, Me.LblVoucherToDate.Text)
			Me.LblFromPeriod.Text = Language._Object.getCaption(Me.LblFromPeriod.Name, Me.LblFromPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
