'************************************************************************************************************************************
'Class Name : frmOverDeduction_NetPay_Report.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmOverDeduction_NetPay_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmOverDeduction_NetPay_Report"
    Private objOverDeduction As clsOverDeduction_NetPay_Report
    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Constructor "

    Public Sub New()
        InitializeComponent()
        objOverDeduction = New clsOverDeduction_NetPay_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objOverDeduction.SetDefaultValue()
        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objperiod As New clscommom_period_Tran
        Dim dsList As New DataSet
        Try

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, False)
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'Anjan [10 June 2015] -- End


            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With
            objEmp = Nothing

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With
            objperiod = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objOverDeduction.SetDefaultValue()

            objOverDeduction._Employee_Id = cboEmployee.SelectedValue
            objOverDeduction._Employee_Name = cboEmployee.Text

            objOverDeduction._Period_Id = cboPeriod.SelectedValue
            objOverDeduction._Period_Name = cboPeriod.Text

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objOverDeduction._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            objOverDeduction.setDefaultOrderBy(0)
            txtOrderBy.Text = objOverDeduction.OrderByDisplay
            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmOverDeduction_NetPay_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOverDeduction_NetPay_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmOverDeduction_NetPay_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objOverDeduction._ReportName
            Me._Message = objOverDeduction._ReportDesc

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOverDeduction_NetPay_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmOverDeduction_NetPay_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmOverDeduction_NetPay_Report_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim objEmployee As New clsEmployee_Master
        Dim dtEmployee As DataTable
        Try
            dtEmployee = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtEmployee
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub frmOverDeduction_NetPay_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objOverDeduction.generateReport(0, e.Type, enExportAction.None)
            Dim dtPeriod_Start As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            Dim dtPeriod_End As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                dtPeriod_Start = objPeriod._Start_Date
                dtPeriod_End = objPeriod._End_Date
                objPeriod = Nothing
            End If
            objOverDeduction.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriod_Start, dtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOverDeduction_NetPay_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmOverDeduction_NetPay_Report_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objOverDeduction.generateReport(0, enPrintAction.None, e.Type)
            Dim dtPeriod_Start As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            Dim dtPeriod_End As Date = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                dtPeriod_Start = objPeriod._Start_Date
                dtPeriod_End = objPeriod._End_Date
                objPeriod = Nothing
            End If
            objOverDeduction.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, dtPeriod_Start, dtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOverDeduction_NetPay_Report_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmOverDeduction_NetPay_Report_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOverDeduction_NetPay_Report_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmOverDeduction_NetPay_Report_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOverDeduction_NetPay_Report_Cancel_Click", mstrModuleName)
        End Try

    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub frmOverDeduction_NetPay_Report_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsOverDeduction_NetPay_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsOverDeduction_NetPay_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (27-Feb-2013) -- End


#End Region

#Region "Control"

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objOverDeduction.setOrderBy(0)
            txtOrderBy.Text = objOverDeduction.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
