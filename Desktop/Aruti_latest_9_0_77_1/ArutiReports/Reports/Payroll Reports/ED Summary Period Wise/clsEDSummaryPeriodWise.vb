﻿'************************************************************************************************************************************
'Class Name : clsEDSummaryPeriodWise.vb
'Purpose    :
'Date       :27/05/2022
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports ExcelWriter


''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsEDSummaryPeriodWise
        Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsEDSummaryPeriodWise"
    Private mstrReportId As String = enArutiReport.ED_Summary_Period_Wise_Report
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mstrPeriodIdList As String
    Private mintFromPeriodId As Integer = -1
    Private mstrFromPeriodName As String = ""
    Private mintToPeriodId As Integer = -1
    Private mstrToPeriodName As String = ""

    Private mintModeId As Integer = -1
    Private mstrModeName As String = String.Empty
    Private mintTransactionHeadId As Integer = -1
    Private mstrTransactionHeadName As String = String.Empty
    Private mintAllocationId As Integer = -1
    Private mstrAllocationName As String = String.Empty
    

    Private mblnIsActive As Boolean = True

    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""



    Private mblnIgnorezeroHeads As Boolean = False

    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date

    Private mstrCurrency_Sign As String = String.Empty

    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable


    Private mstrAdvance_Filter As String = String.Empty

    Private mstrCurrentDatabaseName As String = FinancialYear._Object._DatabaseName
    Private mintBase_CurrencyId As Integer = ConfigParameter._Object._Base_CurrencyId
    Private mstrfmtCurrency As String = GUI.fmtCurrency
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport

    Private mstrUserAccessFilter As String = ""
    Private marrDatabaseName As New ArrayList
    Private mblnApplyUserAccessFilter As Boolean = True


#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIdList() As String
        Set(ByVal value As String)
            mstrPeriodIdList = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodId() As Integer
        Set(ByVal value As Integer)
            mintFromPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _FromPeriodName() As String
        Set(ByVal value As String)
            mstrFromPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodId() As Integer
        Set(ByVal value As Integer)
            mintToPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _ToPeriodName() As String
        Set(ByVal value As String)
            mstrToPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _ModeId() As Integer
        Set(ByVal value As Integer)
            mintModeId = value
        End Set
    End Property

    Public WriteOnly Property _ModeName() As String
        Set(ByVal value As String)
            mstrModeName = value
        End Set
    End Property

    Public WriteOnly Property _TransactionHeadId() As Integer
        Set(ByVal value As Integer)
            mintTransactionHeadId = value
        End Set
    End Property

    Public WriteOnly Property _TransactionHeadName() As String
        Set(ByVal value As String)
            mstrTransactionHeadName = value
        End Set
    End Property

    Public WriteOnly Property _AllocationId() As Integer
        Set(ByVal value As Integer)
            mintAllocationId = value
        End Set
    End Property

    Public WriteOnly Property _AllocationName() As String
        Set(ByVal value As String)
            mstrAllocationName = value
        End Set
    End Property


    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property



    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _IgnoreZeroHeads() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnorezeroHeads = value
        End Set
    End Property

    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _Currency_Sign() As String
        Set(ByVal value As String)
            mstrCurrency_Sign = value
        End Set
    End Property

    Public WriteOnly Property _FromDatabaseName() As String
        Set(ByVal value As String)
            mstrFromDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _ToDatabaseName() As String
        Set(ByVal value As String)
            mstrToDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    Public WriteOnly Property _CurrentDatabaseName() As String
        Set(ByVal value As String)
            mstrCurrentDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _Base_CurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBase_CurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _Arr_DatabaseName() As ArrayList
        Set(ByVal value As ArrayList)
            marrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _ApplyUserAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnApplyUserAccessFilter = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""

            mintFromPeriodId = -1
            mstrFromPeriodName = ""
            mintToPeriodId = -1
            mstrToPeriodName = ""
            mstrPeriodIdList = ""

            mintModeId = -1
            mstrModeName = ""

            mintTransactionHeadId = -1
            mstrTransactionHeadName = ""

            mintAllocationId = -1
            mstrAllocationName = ""

            mblnIsActive = True


            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""


            mblnIgnorezeroHeads = False

            mstrCurrency_Sign = ""

            mstrFromDatabaseName = FinancialYear._Object._DatabaseName
            mstrToDatabaseName = FinancialYear._Object._DatabaseName

            mstrAdvance_Filter = ""

            marrDatabaseName.Clear()

            mblnApplyUserAccessFilter = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            If mintFromPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 1, "Period : ") & " " & mstrFromPeriodName & " "
            End If

            If mintToPeriodId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, " To ") & " " & mstrToPeriodName & " "
            End If

            If mintAllocationId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 3, "Allocation : ") & " " & mstrAllocationName & " "
            End If

            If mintModeId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 4, "Mode : ") & " " & mstrModeName & " "
            End If

            If mintTransactionHeadId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 5, "Transaction Head :") & " " & mstrTransactionHeadName & " "
            End If


            If mstrCurrency_Sign.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Currency :") & " " & mstrCurrency_Sign & " "
            End If


            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= ""
                If mintViewIndex > 0 Then
                    Me._FilterQuery &= " ORDER BY " & mstrAnalysis_OrderBy & ", " & Me.OrderByQuery
                Else
                    Me._FilterQuery &= "ORDER BY " & Me.OrderByQuery
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overloads Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try

            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function



    Private Function IgnoreZeroHead(ByVal objDataReader As DataTable) As DataTable
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Dim intColCount As Integer = objDataReader.Columns.Count
        Dim intRowCount As Integer = objDataReader.Rows.Count
        Try

            Dim intZeroColumn As Integer = 0
            Dim arrZeroColIndx As New ArrayList

            'Report Column Caption
SetColumnCount:
            intColCount = objDataReader.Columns.Count
            For j As Integer = 0 To intColCount - 1
                If mblnIgnorezeroHeads Then

                    If objDataReader.Columns(j).ColumnName <> "EmpCode" AndAlso objDataReader.Columns(j).ColumnName <> "EmpName" AndAlso objDataReader.Columns(j).ColumnName <> "GrpID" AndAlso objDataReader.Columns(j).ColumnName <> "FirstName" AndAlso objDataReader.Columns(j).ColumnName <> "OtherName" AndAlso objDataReader.Columns(j).ColumnName <> "Surname" AndAlso objDataReader.Columns(j).ColumnName <> "MembershipName" _
                           AndAlso objDataReader.Columns(j).ColumnName <> "GrpName" AndAlso objDataReader.Columns(j).ColumnName <> "TDD" AndAlso objDataReader.Columns(j).ColumnName <> "NetPay" Then

                        Dim drRow As DataRow() = Nothing
                        If objDataReader.Columns(j).DataType Is Type.GetType("System.Decimal") Then
                            drRow = objDataReader.Select(objDataReader.Columns(j).ColumnName & " = 0 OR " & objDataReader.Columns(j).ColumnName & " = 0.00")
                            If drRow.Length = objDataReader.Rows.Count Then
                                objDataReader.Columns.RemoveAt(j)
                                GoTo SetColumnCount
                            End If
                        End If

                    End If
                End If

            Next

            Dim decCols() As String = (From p In objDataReader.Columns.Cast(Of DataColumn)() Where (p.DataType Is Type.GetType("System.Decimal")) Select (p.ColumnName.ToString & " = 0 AND ")).ToArray
            If decCols.Length > 0 Then
                Dim s As String = String.Join("", decCols)
                Dim r() As DataRow = objDataReader.Select(s.Substring(0, s.Length - 4))
                For Each dr As DataRow In r
                    objDataReader.Rows.Remove(dr)
                Next
            End If

            Return objDataReader

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IgnoreZeroHead; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("ISNULL(hremployee_master.employeecode,'')", Language.getMessage(mstrModuleName, 7, "Employee Code")))

            

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Sub Generate_CustomPayrollHeadsReport(ByVal mdicYearDBName As Dictionary(Of Integer, String) _
                                                 , ByVal xUserUnkid As Integer _
                                                 , ByVal xCompanyUnkid As Integer _
                                                 , ByVal xPeriodStart As Date _
                                                 , ByVal xPeriodEnd As Date _
                                                 , ByVal xUserModeSetting As String _
                                                 , ByVal xOnlyApproved As Boolean _
                                                 , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                 , ByVal blnApplyUserAccessFilter As Boolean _
                                                 , ByVal intBase_CurrencyId As Integer _
                                                 , ByVal strfmtCurrency As String _
                                                 , ByVal strExportReportPath As String _
                                                 , ByVal blnOpenAfterExport As Boolean _
                                                 )
        Dim StrQ As String = String.Empty
        Dim StrInnerQ As String = String.Empty
        Dim exForce As Exception
        Dim dtFinalTable As DataTable
        Dim dsPayroll As New DataSet
        Dim xTotalStartCol As Integer = 0
        Dim dsPaymetDetail As New DataSet
        Dim mdicEmpBank As New Dictionary(Of String, String)
        Dim mdicEmpAccount As New Dictionary(Of String, String)
        Dim mdicEmpPayment As New Dictionary(Of String, String)
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            mintBase_CurrencyId = intBase_CurrencyId
            mstrfmtCurrency = strfmtCurrency
            mstrExportReportPath = strExportReportPath
            mblnOpenAfterExport = blnOpenAfterExport

            If mstrUserAccessFilter.Trim = "" Then
                mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
            End If
            dtFinalTable = New DataTable("Payroll") : Dim dCol As DataColumn

            dCol = New DataColumn("PeriodId")
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Int32")
            dtFinalTable.Columns.Add(dCol)
            xTotalStartCol += 1

            dCol = New DataColumn("Period")
            dCol.Caption = Language.getMessage(mstrModuleName, 8, "Period")
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)
            xTotalStartCol += 1

            dCol = New DataColumn("TranHeadId")
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Int32")
            dtFinalTable.Columns.Add(dCol)
            xTotalStartCol += 1

            dCol = New DataColumn("TranHeadName")
            dCol.Caption = Language.getMessage(mstrModuleName, 9, "Details")
            dCol.DataType = System.Type.GetType("System.String")
            dtFinalTable.Columns.Add(dCol)
            xTotalStartCol += 1

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = mintBase_CurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            decDecimalPlaces = 6


            StrQ = ""
            StrQ &= "SELECT   hremployee_master.employeeunkid " & _
                            ", hremployee_master.employeecode " & _
                            ", hremployee_master.appointeddate  " & _
                            ",ISNULL(SM.name,'') AS Col_" & enAnalysisReport.Branch & " " & _
                            ",ISNULL(DGM.name,'') AS Col_" & enAnalysisReport.DepartmentGroup & " " & _
                            ",ISNULL(DM.name,'') AS Col_" & enAnalysisReport.Department & " " & _
                            ",ISNULL(SECG.name,'') AS Col_" & enAnalysisReport.SectionGroup & " " & _
                            ",ISNULL(SEC.name,'') AS Col_" & enAnalysisReport.Section & " " & _
                            ",ISNULL(UGM.name,'') AS Col_" & enAnalysisReport.UnitGroup & " " & _
                            ",ISNULL(UM.name,'') AS Col_" & enAnalysisReport.Unit & " " & _
                            ",ISNULL(TM.name,'') AS Col_" & enAnalysisReport.Team & " " & _
                            ",ISNULL(JGM.name,'') AS Col_" & enAnalysisReport.JobGroup & " " & _
                            ",ISNULL(JM.job_name,'') AS Col_" & enAnalysisReport.Job & " " & _
                            ",ISNULL(CGM.name,'') AS Col_" & enAnalysisReport.ClassGroup & " " & _
                            ",ISNULL(CM.name,'') AS Col_" & enAnalysisReport.Classs & " " & _
                            ",ISNULL(CC.costcentername,'') AS Col_" & enAnalysisReport.CostCenter & " " & _
                            ",ISNULL(GGM.name,'') AS Col_" & enAnalysisReport.GradeGroup & " " & _
                            ",ISNULL(GM.name,'') AS Col_" & enAnalysisReport.Grade & " " & _
                            ",ISNULL(GLM.name,'') AS Col_" & enAnalysisReport.GradeLevel & " "


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",'' AS Id, '' AS GName "
            End If

            StrQ &= "INTO #CurrEmp "
            StrQ &= "FROM  " & mstrCurrentDatabaseName & "..hremployee_master "
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 gradegroupunkid " & _
                    "		,gradeunkid " & _
                    "		,gradelevelunkid " & _
                    "		,employeeunkid AS GEmpId " & _
                    "	FROM " & _
                    "	( " & _
                    "		SELECT " & _
                    "			 gradegroupunkid " & _
                    "			,gradeunkid " & _
                    "			,gradelevelunkid " & _
                    "			,employeeunkid " & _
                    "			,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                    "		FROM prsalaryincrement_tran " & _
                    "		WHERE isvoid = 0 AND isapproved = 1 AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "	) AS GRD WHERE GRD.Rno = 1 " & _
                    ") AS EGRD ON EGRD.GEmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "        Trf.TrfEmpId " & _
                    "       ,ISNULL(Trf.stationunkid,0) AS stationunkid " & _
                    "       ,ISNULL(Trf.deptgroupunkid,0) AS deptgroupunkid " & _
                    "       ,ISNULL(Trf.departmentunkid,0) AS departmentunkid " & _
                    "       ,ISNULL(Trf.sectiongroupunkid,0) AS sectiongroupunkid " & _
                    "       ,ISNULL(Trf.sectionunkid,0) AS sectionunkid " & _
                    "       ,ISNULL(Trf.unitgroupunkid,0) AS unitgroupunkid " & _
                    "       ,ISNULL(Trf.unitunkid,0) AS unitunkid " & _
                    "       ,ISNULL(Trf.teamunkid,0) AS teamunkid " & _
                    "       ,ISNULL(Trf.classgroupunkid,0) AS classgroupunkid " & _
                    "       ,ISNULL(Trf.classunkid,0) AS classunkid " & _
                    "       ,ISNULL(Trf.EfDt,'') AS EfDt " & _
                    "       ,Trf.ETT_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            ETT.employeeunkid AS TrfEmpId " & _
                    "           ,ISNULL(ETT.stationunkid,0) AS stationunkid " & _
                    "           ,ISNULL(ETT.deptgroupunkid,0) AS deptgroupunkid " & _
                    "           ,ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
                    "           ,ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
                    "           ,ISNULL(ETT.sectionunkid,0) AS sectionunkid " & _
                    "           ,ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
                    "           ,ISNULL(ETT.unitunkid,0) AS unitunkid " & _
                    "           ,ISNULL(ETT.teamunkid,0) AS teamunkid " & _
                    "           ,ISNULL(ETT.classgroupunkid,0) AS classgroupunkid " & _
                    "           ,ISNULL(ETT.classunkid,0) AS classunkid " & _
                    "           ,CONVERT(CHAR(8),ETT.effectivedate,112) AS EfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                    "           ,CASE WHEN ETT.rehiretranunkid > 0 THEN RTT.name ELSE TTC.name END AS ETT_REASON " & _
                    "       FROM hremployee_transfer_tran AS ETT " & _
                    "           LEFT JOIN cfcommon_master AS RTT ON RTT.masterunkid = ETT.changereasonunkid AND RTT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "           LEFT JOIN cfcommon_master AS TTC ON TTC.masterunkid = ETT.changereasonunkid AND TTC.mastertype = '" & clsCommon_Master.enCommonMaster.TRANSFERS & "' " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS Trf WHERE Trf.Rno = 1 " & _
                    ") AS ETRF ON ETRF.TrfEmpId = hremployee_master.employeeunkid AND ETRF.EfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "     Cat.CatEmpId " & _
                    "    ,Cat.jobgroupunkid " & _
                    "    ,Cat.jobunkid " & _
                    "    ,Cat.CEfDt " & _
                    "    ,Cat.RECAT_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            ECT.employeeunkid AS CatEmpId " & _
                    "           ,ECT.jobgroupunkid " & _
                    "           ,ECT.jobunkid " & _
                    "           ,CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                    "           ,CASE WHEN ECT.rehiretranunkid > 0 THEN ISNULL(RRC.name,'') ELSE ISNULL(RCC.name,'') END AS RECAT_REASON " & _
                    "       FROM hremployee_categorization_tran AS ECT " & _
                    "           LEFT JOIN cfcommon_master AS RRC ON RRC.masterunkid = ECT.changereasonunkid AND RRC.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "           LEFT JOIN cfcommon_master AS RCC ON RCC.masterunkid = ECT.changereasonunkid AND RCC.mastertype = '" & clsCommon_Master.enCommonMaster.RECATEGORIZE & "' " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   ) AS Cat WHERE Cat.Rno = 1 " & _
                    ") AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid AND ERECAT.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "		 CCT.CCTEmpId " & _
                    "		,CCT.costcenterunkid " & _
                    "		,CCT.CTEfDt " & _
                    "		,CC_REASON " & _
                    "   FROM " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            CCT.employeeunkid AS CCTEmpId " & _
                    "           ,CCT.cctranheadvalueid AS costcenterunkid " & _
                    "           ,CONVERT(CHAR(8),CCT.effectivedate,112) AS CTEfDt " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC) AS Rno " & _
                    "			,CASE WHEN CCT.rehiretranunkid > 0 THEN ISNULL(RCS.name,'') ELSE ISNULL(CTC.name,'') END AS CC_REASON " & _
                    "       FROM hremployee_cctranhead_tran AS CCT " & _
                    "			LEFT JOIN cfcommon_master AS RCS ON RCS.masterunkid = CCT.changereasonunkid AND RCS.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "			LEFT JOIN cfcommon_master AS CTC ON CTC.masterunkid = CCT.changereasonunkid AND CTC.mastertype = '" & clsCommon_Master.enCommonMaster.COST_CENTER & "' " & _
                    "		WHERE CCT.isvoid = 0 AND CCT.istransactionhead = 0 AND CONVERT(CHAR(8),CCT.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "   )CCT WHERE CCT.Rno = 1 " & _
                    ")AS ECCT ON ECCT.CCTEmpId = hremployee_master.employeeunkid AND ECCT.CTEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    		 PROB.PDEmpId " & _
                    "    		,PROB.probation_from_date " & _
                    "    		,PROB.probation_to_date " & _
                    "    		,PROB.PDEfDt " & _
                    "    		,PROB.PB_REASON " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             PDT.employeeunkid AS PDEmpId " & _
                    "            ,CONVERT(CHAR(8),PDT.date1,112) AS probation_from_date " & _
                    "            ,CONVERT(CHAR(8),PDT.date2,112) AS probation_to_date " & _
                    "            ,CONVERT(CHAR(8),PDT.effectivedate,112) AS PDEfDt " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY PDT.employeeunkid ORDER BY PDT.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN PDT.rehiretranunkid > 0 THEN ISNULL(RPB.name,'') ELSE ISNULL(PBC.name,'') END AS PB_REASON " & _
                    "        FROM hremployee_dates_tran AS PDT " & _
                    "            LEFT JOIN cfcommon_master AS RPB ON RPB.masterunkid = PDT.changereasonunkid AND RPB.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS PBC ON PBC.masterunkid = PDT.changereasonunkid AND PBC.mastertype = '" & clsCommon_Master.enCommonMaster.PROBATION & "' " & _
                    "        WHERE isvoid = 0 AND PDT.datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' AND CONVERT(CHAR(8),PDT.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS PROB WHERE PROB.Rno = 1 " & _
                    ") AS EPROB ON EPROB.PDEmpId = hremployee_master.employeeunkid AND EPROB.PDEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    		 TERM.TEEmpId " & _
                    "    		,TERM.empl_enddate " & _
                    "    		,TERM.termination_from_date " & _
                    "    		,TERM.TEfDt " & _
                    "    		,TERM.isexclude_payroll " & _
                    "    		,TERM.TR_REASON " & _
                    "    		,TERM.changereasonunkid " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             TRM.employeeunkid AS TEEmpId " & _
                    "            ,CONVERT(CHAR(8),TRM.date1,112) AS empl_enddate " & _
                    "            ,CONVERT(CHAR(8),TRM.date2,112) AS termination_from_date " & _
                    "            ,CONVERT(CHAR(8),TRM.effectivedate,112) AS TEfDt " & _
                    "            ,TRM.isexclude_payroll " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN TRM.rehiretranunkid > 0 THEN ISNULL(RTE.name,'') ELSE ISNULL(TEC.name,'') END AS TR_REASON " & _
                    "            ,TRM.changereasonunkid " & _
                    "        FROM hremployee_dates_tran AS TRM " & _
                    "            LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = TRM.changereasonunkid AND RTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = TRM.changereasonunkid AND TEC.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                    "        WHERE isvoid = 0 AND TRM.datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),TRM.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS TERM WHERE TERM.Rno = 1 " & _
                    ") AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.TEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    	 CNF.CEmpId " & _
                    "    	,CNF.confirmation_date " & _
                    "       ,CNF.CEfDt " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             CNF.employeeunkid AS CEmpId " & _
                    "            ,CONVERT(CHAR(8),CNF.date1,112) AS confirmation_date " & _
                    "            ,CONVERT(CHAR(8),CNF.effectivedate,112) AS CEfDt " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY CNF.employeeunkid ORDER BY CNF.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN CNF.rehiretranunkid > 0 THEN ISNULL(CTE.name,'') ELSE ISNULL(CEC.name,'') END AS CF_REASON " & _
                    "            ,CNF.changereasonunkid " & _
                    "        FROM hremployee_dates_tran AS CNF " & _
                    "            LEFT JOIN cfcommon_master AS CTE ON CTE.masterunkid = CNF.changereasonunkid AND CTE.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS CEC ON CEC.masterunkid = CNF.changereasonunkid AND CEC.mastertype = '" & clsCommon_Master.enCommonMaster.CONFIRMATION & "' " & _
                    "        WHERE isvoid = 0 AND CNF.datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),CNF.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS CNF WHERE CNF.Rno = 1 " & _
                    ") AS ECNF ON ECNF.CEmpId = hremployee_master.employeeunkid AND ECNF.CEfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "    	 RET.REmpId " & _
                    "    	,RET.termination_to_date " & _
                    "    	,RET.REfDt " & _
                    "    	,RET.RET_REASON " & _
                    "    FROM " & _
                    "    ( " & _
                    "        SELECT " & _
                    "             RTD.employeeunkid AS REmpId " & _
                    "            ,CONVERT(CHAR(8),RTD.date1,112) AS termination_to_date " & _
                    "            ,CONVERT(CHAR(8),RTD.effectivedate,112) AS REfDt " & _
                    "            ,ROW_NUMBER()OVER(PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                    "            ,CASE WHEN RTD.rehiretranunkid > 0 THEN ISNULL(RRT.name,'') ELSE ISNULL(RTC.name,'') END AS RET_REASON " & _
                    "        FROM hremployee_dates_tran AS RTD " & _
                    "            LEFT JOIN cfcommon_master AS RRT ON RRT.masterunkid = RTD.changereasonunkid AND RRT.mastertype = '" & clsCommon_Master.enCommonMaster.RE_HIRE & "' " & _
                    "            LEFT JOIN cfcommon_master AS RTC ON RTC.masterunkid = RTD.changereasonunkid AND RTC.mastertype = '" & clsCommon_Master.enCommonMaster.RETIREMENTS & "' " & _
                    "        WHERE isvoid = 0 AND RTD.datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),RTD.effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    "    ) AS RET WHERE RET.Rno = 1 " & _
                    ") AS ERET ON ERET.REmpId = hremployee_master.employeeunkid AND ERET.REfDt >= CONVERT(CHAR(8),appointeddate,112) " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrstation_master AS SM on SM.stationunkid = ETRF.stationunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrdepartment_group_master AS DGM on DGM.deptgroupunkid = ETRF.deptgroupunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrdepartment_master AS DM ON DM.departmentunkid = ETRF.departmentunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrsectiongroup_master AS SECG ON ETRF.sectiongroupunkid = SECG.sectiongroupunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrsection_master AS SEC ON SEC.sectionunkid = ETRF.sectionunkid  " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrunitgroup_master AS UGM ON ETRF.unitgroupunkid = UGM.unitgroupunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrunit_master AS UM on UM.unitunkid = ETRF.unitunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrteam_master AS TM ON ETRF.teamunkid = TM.teamunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrjobgroup_master AS JGM on JGM.jobgroupunkid = ERECAT.jobgroupunkid  " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrjob_master AS JM ON JM.jobunkid = ERECAT.jobunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrclassgroup_master AS CGM ON CGM.classgroupunkid = ETRF.classgroupunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrclasses_master AS CM on CM.classesunkid = ETRF.classunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..prcostcenter_master AS CC on CC.costcenterunkid = ECCT.costcenterunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrgradegroup_master AS GGM on GGM.gradegroupunkid = EGRD.gradegroupunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrgrade_master AS GM on GM.gradeunkid = EGRD.gradeunkid   " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..hrgradelevel_master AS GLM on GLM.gradelevelunkid = EGRD.gradelevelunkid  " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..cfcommon_master as mtype ON mtype.masterunkid = hremployee_master.maritalstatusunkid AND mtype.mastertype = " & clsCommon_Master.enCommonMaster.MARRIED_STATUS & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..cfcommon_master AS etype ON etype.masterunkid = hremployee_master.employmenttypeunkid AND etype.mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & " "

            StrQ &= mstrAnalysis_Join

            For Each key In mdicYearDBName

                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , key.Value)
                If mblnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, key.Value, xUserUnkid, xCompanyUnkid, key.Key, xUserModeSetting)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, key.Value)

                StrQ &= "SELECT   hremployee_master.employeeunkid "
                StrQ &= "INTO #" & key.Value & " "
                StrQ &= "FROM  " & key.Value & "..hremployee_master "

                StrQ &= "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         stationunkid " & _
                                "        ,deptgroupunkid " & _
                                "        ,departmentunkid " & _
                                "        ,sectiongroupunkid " & _
                                "        ,sectionunkid " & _
                                "        ,unitgroupunkid " & _
                                "        ,unitunkid " & _
                                "        ,teamunkid " & _
                                "        ,classgroupunkid " & _
                                "        ,classunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                                "    FROM " & mstrFromDatabaseName & "..hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE 1 = 1 "

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If mstrAdvance_Filter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAdvance_Filter
                End If

            Next

            StrQ &= "; SELECT  PeriodId " & _
                   ", ISNULL(period_name, '') AS PeriodName " & _
                   ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                   ", EmpId "


            StrQ &= ",#CurrEmp.employeecode AS Code " & _
                    ",TranId " & _
                    ", TranHeadName " & _
                    ", CASE ismonetary WHEN 1 THEN Amount ELSE Amount END AS Amount " & _
                    ",Mid " & _
                    ", CASE ismonetary WHEN 1 THEN Openingbalance ELSE Openingbalance END AS Openingbalance " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.Branch & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.DepartmentGroup & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.Department & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.SectionGroup & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.Section & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.UnitGroup & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.Unit & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.Team & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.JobGroup & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.Job & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.ClassGroup & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.Classs & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.CostCenter & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.GradeGroup & "  " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.Grade & " " & _
                    ",#CurrEmp.Col_" & enAnalysisReport.GradeLevel & " "

            StrQ &= ",#CurrEmp.Id, #CurrEmp.GName "
            StrQ &= "FROM    ( "

            Dim i As Integer = -1
            For Each key In mdicYearDBName
                mstrFromDatabaseName = key.Value
                i += 1


                If i > 0 Then
                    StrInnerQ &= " UNION ALL "
                End If

                StrInnerQ &= "SELECT DISTINCT " & _
                             "  payperiodunkid AS PeriodId " & _
                             " ,prtnaleave_tran.employeeunkid AS EmpId " & _
                             " ,prpayrollprocess_tran.tranheadunkid AS TranId " & _
                             ", prtranhead_master.trnheadname AS TranHeadName " & _
                             " ,SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                             " ,10 AS Mid " & _
                             " ,openingbalance AS Openingbalance " & _
                             " , CASE trnheadtype_id WHEN " & CInt(enTranHeadType.Informational) & " THEN ismonetary ELSE 1 END AS ismonetary " & _
                             "FROM " & mstrFromDatabaseName & "..prpayrollprocess_tran " & _
                             "  LEFT JOIN " & mstrFromDatabaseName & "..prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                             "  JOIN #" & mstrFromDatabaseName & " ON prpayrollprocess_tran.employeeunkid = #" & mstrFromDatabaseName & ".employeeunkid " & _
                             "  LEFT JOIN " & mstrFromDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "


                StrInnerQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                             "  AND prtnaleave_tran.isvoid = 0 " & _
                             "  AND prtranhead_master.isvoid = 0 " & _
                             "  AND prtranhead_master.trnheadtype_id = @trnheadtype_id " & _
                             "  AND payperiodunkid IN ( " & mstrPeriodIdList & " ) "

                If mintTransactionHeadId > 0 Then
                    StrInnerQ &= " AND prtranhead_master.tranheadunkid = @tranheadunkid "
                End If

                StrInnerQ &= " GROUP BY payperiodunkid " & _
                             ", prtnaleave_tran.employeeunkid " & _
                             ", prpayrollprocess_tran.tranheadunkid " & _
                             ", prtranhead_master.trnheadname " & _
                             ", openingbalance " & _
                             ", trnheadtype_id " & _
                             ", ismonetary "

            Next
            StrQ &= StrInnerQ
            StrQ &= ") AS payroll " & _
                    " JOIN #CurrEmp ON payroll.EmpId = #CurrEmp.employeeunkid " & _
                    " LEFT JOIN " & mstrCurrentDatabaseName & "..cfcommon_period_tran ON cfcommon_period_tran.periodunkid = payroll.PeriodId " & _
                    "   AND cfcommon_period_tran.isactive = 1 AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " "

            Me.OrderByQuery = Me.OrderByQuery.Replace("hremployee_master", "#CurrEmp")

            If mintViewIndex > 0 Then
                If Me.OrderByQuery <> "" Then
                    StrQ &= " ORDER BY #CurrEmp.GName, cfcommon_period_tran.end_date, " & Me.OrderByQuery & ", TranId "
                Else

                    StrQ &= " ORDER BY #CurrEmp.GName, cfcommon_period_tran.end_date, #CurrEmp.employeecode " & ", TranId "
                End If
            Else
                If Me.OrderByQuery <> "" Then
                    StrQ &= "ORDER BY cfcommon_period_tran.end_date, " & Me.OrderByQuery & ", TranId "
                Else
                    StrQ &= "ORDER BY cfcommon_period_tran.end_date, #CurrEmp.employeecode " & ", TranId "
                End If
            End If

            For Each key In mdicYearDBName
                StrQ &= " DROP TABLE #" & key.Value & " "
            Next

            StrQ &= " DROP TABLE #CurrEmp "

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromPeriodId)

            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If

            If mintModeId > 0 Then
                objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeId)
            End If

            If mintTransactionHeadId > 0 Then
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionHeadId)
            End If

            dsPayroll = objDataOperation.ExecQuery(StrQ, "payroll")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtPayroll As DataTable = New DataView(dsPayroll.Tables(0), "", "end_date, tranheadname, Col_" & mintAllocationId, DataViewRowState.CurrentRows).ToTable
            Dim distinctAllocation As DataTable = New DataView(dtPayroll).ToTable(True, "Col_" & mintAllocationId)

            For Each dtRow As DataRow In distinctAllocation.Rows
                If dtRow.Item(0).ToString.Trim = "" Then
                    dCol = New DataColumn("NotAssigned")
                    dCol.Caption = dtRow.Item(0).ToString
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dtFinalTable.Columns.Add(dCol)
                Else
                    dCol = New DataColumn(dtRow.Item(0).ToString)
                    dCol.Caption = dtRow.Item(0).ToString
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dCol.DefaultValue = 0
                    dtFinalTable.Columns.Add(dCol)
                End If
            Next

            dCol = New DataColumn("Total")
            dCol.Caption = Language.getMessage(mstrModuleName, 10, "TOTALS")
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dCol) '

            Dim result = From p In dtPayroll.AsEnumerable Group p By G = New With { _
                            Key .periodunkid = CInt(p.Item("PeriodId")) _
                            , Key .periodname = p.Item("PeriodName").ToString _
                            , Key .tranheadunkid = CInt(p.Item("TranId")) _
                            , Key .tranheadname = p.Item("TranHeadName").ToString _
                            , Key .allocationname = p.Item("Col_" & mintAllocationId).ToString _
                        } _
                        Into grp = Group Select New With { _
                            Key .periodunkid = grp(0).Item("PeriodId") _
                            , Key .periodname = grp(0).Item("PeriodName").ToString _
                            , Key .tranheadunkid = CInt(grp(0).Item("TranId")) _
                            , Key .tranheadname = grp(0).Item("TranHeadName").ToString _
                            , Key .allocationname = grp(0).Item("Col_" & mintAllocationId).ToString _
                            , Key .amount = grp.Sum(Function(x) CDec(x.Item("amount"))) _
                        }

            Dim drRow As DataRow : Dim rpt_Row As DataRow = Nothing : Dim strKey As String = "" : Dim strPrevKey As String = ""
            For Each itm In result
                strKey = itm.periodunkid & "_" & itm.tranheadunkid

                If strPrevKey <> strKey Then
                    rpt_Row = dtFinalTable.NewRow
                    rpt_Row.Item("PeriodId") = itm.periodunkid
                    rpt_Row.Item("Period") = itm.periodname
                    rpt_Row.Item("TranHeadId") = itm.tranheadunkid
                    rpt_Row.Item("TranHeadName") = itm.tranheadname

                    If itm.allocationname.ToString = "" Then
                        rpt_Row.Item("NotAssigned") = itm.amount
                    Else
                        rpt_Row.Item(itm.allocationname) = itm.amount
                    End If
                   
                    rpt_Row.Item("Total") += itm.amount

                    dtFinalTable.Rows.Add(rpt_Row)

                Else
                    rpt_Row = dtFinalTable.Rows(dtFinalTable.Rows.Count - 1)
                    If itm.allocationname.ToString = "" Then
                        rpt_Row.Item("NotAssigned") = itm.amount
                    Else
                        rpt_Row.Item(itm.allocationname) = itm.amount
                    End If
                    rpt_Row.Item("Total") += itm.amount
                    dtFinalTable.AcceptChanges()
                End If

                strPrevKey = itm.periodunkid & "_" & itm.tranheadunkid
            Next

          

          

            Call FilterTitleAndFilterQuery()

            Dim strGTotal As String = Language.getMessage(mstrModuleName, 11, "Grand Total :")
            Dim strSubTotal As String = Language.getMessage(mstrModuleName, 12, "Sub Total :")
            Dim objDic As New Dictionary(Of Integer, Object)
            Dim strarrGroupColumns As String() = Nothing
            Dim rowsArrayHeader As New ArrayList
            Dim rowsArrayFooter As New ArrayList
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell = Nothing


         
            mdtTableExcel = dtFinalTable

            mdtTableExcel.Columns.Remove("PeriodId")
            mdtTableExcel.Columns.Remove("TranHeadId")
            



            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Report Filter :") & " " & Me._FilterTitle, "s8b")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
            rowsArrayHeader.Add(row)

            Dim strGrpCols As String() = {"Period"}
            strarrGroupColumns = strGrpCols

            

            Dim intColCount As Integer = mdtTableExcel.Columns.Count
            If strarrGroupColumns IsNot Nothing AndAlso strarrGroupColumns.Length > 0 Then
                intColCount -= strarrGroupColumns.Length
            End If

           
            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)
            For ii As Integer = 0 To intArrayColumnWidth.Length - 1
                intArrayColumnWidth(ii) = 125
            Next




            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, mstrExportReportPath, _
                               mblnOpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, strarrGroupColumns, _
                               "", "", " ", Nothing, strGTotal, True, rowsArrayHeader, rowsArrayFooter, _
                               Nothing)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_CustomPayrollHeadsReport; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period :")
			Language.setMessage(mstrModuleName, 2, " To")
			Language.setMessage(mstrModuleName, 3, "Allocation :")
			Language.setMessage(mstrModuleName, 4, "Mode :")
			Language.setMessage(mstrModuleName, 5, "Transaction Head :")
			Language.setMessage(mstrModuleName, 6, "Currency :")
			Language.setMessage(mstrModuleName, 7, "Employee Code")
			Language.setMessage(mstrModuleName, 8, "Period")
			Language.setMessage(mstrModuleName, 9, "Details")
			Language.setMessage(mstrModuleName, 10, "TOTALS")
			Language.setMessage(mstrModuleName, 11, "Grand Total :")
			Language.setMessage(mstrModuleName, 12, "Sub Total :")
			Language.setMessage(mstrModuleName, 13, "Report Filter :")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
