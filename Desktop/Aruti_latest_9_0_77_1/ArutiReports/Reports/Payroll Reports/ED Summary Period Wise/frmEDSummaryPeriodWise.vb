﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmEDSummaryPeriodWise

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEDSummaryPeriodWise"
    Private objPayroll As clsEDSummaryPeriodWise
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mdecEx_Rate As Decimal = 0
    Private mstrCurr_Sign As String = String.Empty
    Private mstrBaseCurrSign As String = String.Empty
    Private mintBaseCurrId As Integer = 0
    Private mstrCurrency_Rate As String = String.Empty
    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrAdvanceFilter As String = String.Empty


#End Region

#Region " Constructor "

    Public Sub New()
        objPayroll = New clsEDSummaryPeriodWise(User._Object._Languageunkid, Company._Object._Companyunkid)
        objPayroll.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Allocation = 1
        Mode = 2
        TransactionHead = 3
        IncludeInactiveEmployee = 4
        IgnoreZeroValueHeads = 5
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try

            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)

            With cboFromPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0).Copy
                .SelectedValue = mintFirstOpenPeriod
            End With

            dsList = objMaster.getComboListForHeadType("List")
            dtTable = New DataView(dsList.Tables(0), "ID IN (0, " & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ", " & enTranHeadType.EmployeesStatutoryDeductions & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboMode
                .ValueMember = "Id"
                .DisplayMember = "NAME"
                .DataSource = dtTable
            End With

            dsList = objMaster.GetReportAllocation("List")
            Dim dr As DataRow = dsList.Tables(0).NewRow
            dr.Item("Id") = 0
            dr.Item("Name") = Language.getMessage(mstrModuleName, 1, "Select")
            dsList.Tables(0).Rows.InsertAt(dr, 0)
            With cboAllocation
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objMaster = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try

            objPayroll.setDefaultOrderBy(0)
            txtOrderBy.Text = objPayroll.OrderByDisplay

            cboFromPeriod.SelectedValue = mintFirstOpenPeriod
            cboToPeriod.SelectedValue = mintFirstOpenPeriod
            If cboFromPeriod.SelectedIndex >= 2 Then cboFromPeriod.SelectedIndex = cboFromPeriod.SelectedIndex - 1
            cboMode.SelectedValue = 0
            cboTranHead.SelectedValue = 0
            cboAllocation.SelectedValue = 0

            chkInactiveemp.Checked = False

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            chkIgnorezeroHead.Checked = True

            mstrAdvanceFilter = ""


            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.ED_Summary_Period_Wise_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Allocation
                            cboAllocation.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Mode
                            cboMode.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.TransactionHead
                            cboTranHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.IncludeInactiveEmployee
                            chkInactiveemp.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.IgnoreZeroValueHeads
                            chkIgnorezeroHead.Checked = CBool(dsRow.Item("transactionheadid"))

                        

                    End Select
                Next
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            If cboFromPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "From Period is compulsory infomation. Please select From Period to continue."), enMsgBoxStyle.Information)
                cboFromPeriod.Focus()
                Return False
            ElseIf cboToPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "To Period is compulsory infomation. Please select To Period to continue."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            ElseIf cboAllocation.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Allocation is compulsory infomation. Please select Allocation to continue."), enMsgBoxStyle.Information)
                cboAllocation.Focus()
                Return False
            ElseIf cboMode.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Mode is compulsory infomation. Please select Mode to continue."), enMsgBoxStyle.Information)
                cboMode.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Form's Events "

    Private Sub frmEDSummaryPeriodWise_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPayroll = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDSummaryPeriodWise_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEDSummaryPeriodWise_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Call FillCombo()

            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEDSummaryPeriodWise_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboFromPeriod_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboFromPeriod.KeyPress _
                                                                                                                        , cboToPeriod.KeyPress _
                                                                                                                        , cboTranHead.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboTranHead.Name Then
                        .CodeMember = "code"
                    End If
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFromPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFromPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboFromPeriod.SelectedValue)
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrFromDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrFromDatabaseName = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFromPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try

    End Sub

    Private Sub cboToPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboToPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrToDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrToDatabaseName = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboToPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try
    End Sub

    Private Sub cboMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Dim objHead As New clsTransactionHead
        Dim dsCombo As DataSet
        Try
            dsCombo = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, CInt(cboMode.SelectedValue))
            With cboTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)
        Finally
            objHead = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim arrDatabaseName As New ArrayList
        Dim intPrevYearID As Integer = 0
        Dim mdtPeriod_Start As Date
        Dim mdtPeriod_End As Date
        Dim mdicYearDBName As New Dictionary(Of Integer, String)
        Try
            
            If SetFilter() = False Then Exit Try

            Dim i As Integer = 0
            Dim strList As String = cboFromPeriod.SelectedValue.ToString
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid")))
            If dsList.Tables("Database").Rows.Count > 0 Then
                arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                intPrevYearID = CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid"))
                If mdicYearDBName.ContainsKey(intPrevYearID) = False Then
                    mdicYearDBName.Add(intPrevYearID, dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                End If
            End If
            
            For Each dsRow As DataRow In CType(cboFromPeriod.DataSource, DataTable).Rows
                If i > cboFromPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                    strList &= ", " & dsRow.Item("periodunkid").ToString
                    If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                        dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(dsRow.Item("yearunkid")))
                        If dsList.Tables("Database").Rows.Count > 0 Then
                            arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                            If mdicYearDBName.ContainsKey(CInt(dsRow.Item("yearunkid"))) = False Then
                                mdicYearDBName.Add(CInt(dsRow.Item("yearunkid")), dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                            End If
                        End If
                    End If
                    intPrevYearID = CInt(dsRow.Item("yearunkid"))
                End If
                i += 1
            Next


            objPayroll.SetDefaultValue()

            objPayroll._ReportId = cboMode.SelectedIndex
            objPayroll._ReportTypeName = cboMode.Text

            objPayroll._FromPeriodId = cboFromPeriod.SelectedValue
            objPayroll._FromPeriodName = cboFromPeriod.Text
            objPayroll._ToPeriodId = CInt(cboToPeriod.SelectedValue)
            objPayroll._ToPeriodName = cboToPeriod.Text
            objPayroll._PeriodIdList = strList
            objPayroll._Arr_DatabaseName = arrDatabaseName

            objPayroll._AllocationId = cboAllocation.SelectedValue
            objPayroll._AllocationName = cboAllocation.Text

            objPayroll._ModeId = cboMode.SelectedValue
            objPayroll._ModeName = cboMode.Text

            objPayroll._TransactionHeadId = cboTranHead.SelectedValue
            objPayroll._TransactionHeadName = cboTranHead.Text

            objPayroll._IsActive = chkInactiveemp.Checked


            objPayroll._ViewByIds = mstrStringIds
            objPayroll._ViewIndex = mintViewIdx
            objPayroll._ViewByName = mstrStringName
            objPayroll._Analysis_Fields = mstrAnalysis_Fields
            objPayroll._Analysis_Join = mstrAnalysis_Join
            objPayroll._Analysis_OrderBy = mstrAnalysis_OrderBy
            objPayroll._Report_GroupName = mstrReport_GroupName


            objPayroll._IgnoreZeroHeads = chkIgnorezeroHead.Checked

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboFromPeriod.SelectedValue)
            objPayroll._PeriodStartDate = objPeriod._Start_Date
            mdtPeriod_Start = objPeriod._Start_Date


            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            objPayroll._PeriodEndDate = objPeriod._End_Date
            
            mdtPeriod_End = objPeriod._End_Date

            objPayroll._FromDatabaseName = mstrFromDatabaseName
            objPayroll._ToDatabaseName = mstrToDatabaseName

            objPayroll._Advance_Filter = mstrAdvanceFilter

            objPayroll._ApplyUserAccessFilter = True

            
            objPayroll._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(mdtPeriod_End))
            


            objPayroll.Generate_CustomPayrollHeadsReport(mdicYearDBName, User._Object._Userunkid, Company._Object._Companyunkid, mdtPeriod_Start, _
                                                         mdtPeriod_End, ConfigParameter._Object._UserAccessModeSetting, True, chkInactiveemp.Checked, _
                                                         True, ConfigParameter._Object._Base_CurrencyId, _
                                                         ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._ExportReportPath, _
                                                         ConfigParameter._Object._OpenAfterExport)


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEDSummaryPeriodWise.SetMessages()
            objfrm._Other_ModuleNames = "clsEDSummaryPeriodWise"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Other Control's Events "
    Private Sub lnkAnalysisBy_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAnalysisBy.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAnalysisBy_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objPayroll.setOrderBy(0)
            txtOrderBy.Text = objPayroll.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSave.LinkClicked
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()
                Dim intUnkid As Integer = -1
                objUserDefRMode._Reportunkid = enArutiReport.ED_Summary_Period_Wise_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Allocation
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboAllocation.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ED_Summary_Period_Wise_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Mode
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboMode.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ED_Summary_Period_Wise_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.TransactionHead
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboTranHead.SelectedValue)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ED_Summary_Period_Wise_Report, 0, 0, intHeadType)


                    Case enHeadTypeId.IncludeInactiveEmployee
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkInactiveemp.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ED_Summary_Period_Wise_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.IgnoreZeroValueHeads
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CBool(chkIgnorezeroHead.Checked)

                        intUnkid = objUserDefRMode.isExist(enArutiReport.ED_Summary_Period_Wise_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Settings saved successfully."), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSave_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region



    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.objLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.Name, Me.lblToPeriod.Text)
			Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
			Me.chkIgnorezeroHead.Text = Language._Object.getCaption(Me.chkIgnorezeroHead.Name, Me.chkIgnorezeroHead.Text)
			Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.Name, Me.lnkAnalysisBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)
			Me.lblFromPeriod.Text = Language._Object.getCaption(Me.lblFromPeriod.Name, Me.lblFromPeriod.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblTranHead.Text = Language._Object.getCaption(Me.lblTranHead.Name, Me.lblTranHead.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.chkShowCompanyLogoOnReport.Text = Language._Object.getCaption(Me.chkShowCompanyLogoOnReport.Name, Me.chkShowCompanyLogoOnReport.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.lblMode.Text = Language._Object.getCaption(Me.lblMode.Name, Me.lblMode.Text)
			Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.Name, Me.lnkSave.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "From Period is compulsory infomation. Please select From Period to continue.")
			Language.setMessage(mstrModuleName, 3, "To Period is compulsory infomation. Please select To Period to continue.")
			Language.setMessage(mstrModuleName, 4, "Allocation is compulsory infomation. Please select Allocation to continue.")
			Language.setMessage(mstrModuleName, 5, "Mode is compulsory infomation. Please select Mode to continue.")
			Language.setMessage(mstrModuleName, 6, "Settings saved successfully.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class