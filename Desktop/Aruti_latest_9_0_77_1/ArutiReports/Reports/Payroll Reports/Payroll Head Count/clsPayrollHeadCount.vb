'************************************************************************************************************************************
'Class Name : clsCompanyTotalsByAnalysisReport.vb
'Purpose    :
'Date       :08/11/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsPayrollHeadCount
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPayrollHeadCount"
    Private mstrReportId As String = enArutiReport.PayrollHeadCount
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private Variables "
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = ""
    Private mblnIncludeInactiveEmployee As Boolean = False

    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    Private mdtPreviousPeriodStartDate As Date
    Private mdtPreviousPeriodEndDate As Date
    Private mintPreviousPeriodUnkId As Integer
    Private mstrBFDatabaseName As String = ""


    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty

    'Pinkal (24-May-2013) -- End

#End Region

#Region " Properties "
    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmployee() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmployee = value
        End Set
    End Property


    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _PreviousPeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPreviousPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PreviousPeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPreviousPeriodEndDate = value
        End Set
    End Property

    Public WriteOnly Property _PreviousPeriodId() As Integer
        Set(ByVal value As Integer)
            mintPreviousPeriodUnkId = value
        End Set
    End Property

    Public WriteOnly Property _BF_DatabaseName() As String
        Set(ByVal value As String)
            mstrBFDatabaseName = value
        End Set
    End Property

    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    'Pinkal (24-May-2013) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""

            mintPeriodId = 0
            mstrPeriodName = ""
            mblnIncludeInactiveEmployee = False

            mdtPeriodStartDate = Nothing
            mdtPeriodEndDate = Nothing
            mdtPreviousPeriodStartDate = Nothing
            mdtPreviousPeriodEndDate = Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterTitle = ""
        Me._FilterQuery = ""

        Try

            objDataOperation.AddParameter("@prevperiodid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPreviousPeriodUnkId)

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            objDataOperation.AddParameter("@nextstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate.AddDays(1))) 'Sohail (22 Nov 2012)

            'If mblnIncludeInactiveEmployee = False Then
            objDataOperation.AddParameter("@prevstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPreviousPeriodStartDate))
            objDataOperation.AddParameter("@prevenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPreviousPeriodEndDate))
            'End If

            Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "PAYROLL HEAD COUNT FOR THE PERIOD OF ") & " " & mstrPeriodName & " "

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    'Pinkal (24-May-2013) -- Start
        '    'Enhancement : TRA Changes

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid
        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'Pinkal (24-May-2013) -- End

        '    'objRpt = Generate_DetailReport()
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            mintCompanyUnkid = xCompanyUnkid
            mintUserUnkid = xUserUnkid

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, mdtPreviousPeriodStartDate, mdtPreviousPeriodEndDate, xUserModeSetting, xOnlyApproved, mblnIncludeInactiveEmployee, True)
            Rpt = objRpt


            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function Generate_DetailReport(ByVal xDatabaseName As String _
                                           , ByVal xUserUnkid As Integer _
                                           , ByVal xYearUnkid As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal xPeriodStart As Date _
                                           , ByVal xPeriodEnd As Date _
                                           , ByVal xPrevPeriodStart As Date _
                                           , ByVal xPrevPeriodEnd As Date _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                           , ByVal blnApplyUserAccessFilter As Boolean _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xPrevPeriodStart, xPrevPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        'Dim strQInactiveEmployee As String 'Sohail (21 Aug 2015)
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry, xBFDateJoinQry, xBFDateFilterQry, xBFUACQry, xBFUACFiltrQry, xBFAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = "" : xBFDateJoinQry = "" : xBFDateFilterQry = "" : xBFUACQry = "" : xBFUACFiltrQry = "" : xBFAdvanceJoinQry = ""
            Call GetDatesFilterString(xBFDateJoinQry, xBFDateFilterQry, xPrevPeriodStart, xPrevPeriodEnd, , , mstrBFDatabaseName)
            Call NewAccessLevelFilterString(xBFUACQry, xBFUACFiltrQry, xPrevPeriodEnd, xOnlyApproved, mstrBFDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xBFAdvanceJoinQry, xPrevPeriodEnd, mstrBFDatabaseName)

            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'Sohail (21 Aug 2015) -- End

            objDataOperation = New clsDataOperation

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'strQInactiveEmployee = " AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= @prevenddate " & _
            '                                                 "AND ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112), " & _
            '                                                            "@prevstartdate) >= @prevstartdate " & _
            '                                                 "AND ISNULL(CONVERT(CHAR(8), hremployee_master.termination_to_date, 112), " & _
            '                                                            "@prevstartdate) >= @prevstartdate " & _
            '                                                 "AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate, 112), " & _
            '                                                            "@prevstartdate) >= @prevstartdate "
            'Sohail (21 Aug 2015) -- End

            '*** Head_Count_previous_period_BF FROM Employee Master Only.
            'StrQ = "SELECT  reason_action AS Reason " & _
            '              ", GrpId " & _
            '              ", Grp " & _
            '              ", TotalCount " & _
            '        "FROM    ( SELECT    @Head_Count_previous_period_BF AS reason_action " & _
            '                          ", 1 AS GrpId " & _
            '                          ", @Head_Count_previous_period_BF AS Grp " & _
            '                          ", COUNT(*) AS TotalCount " & _
            '                  "FROM      hremployee_master " & _
            '                  "WHERE     1 = 1 " & _
            '                            "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= @prevenddate " & _
            '                            "AND ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112), " & _
            '                                       "@prevstartdate) >= @prevstartdate " & _
            '                            "AND ISNULL(CONVERT(CHAR(8), hremployee_master.termination_to_date, 112), " & _
            '                                       "@prevstartdate) >= @prevstartdate " & _
            '                            "AND ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate, 112), " & _
            '                                       "@prevstartdate) >= @prevstartdate " & _
            '                  "UNION ALL " & _
            '                  "SELECT    @NewHiredEmployees AS reason_action " & _
            '                          ", 2 AS GrpId " & _
            '                          ", @AdditionalHeadCount AS Grp " & _
            '                          ", COUNT(*) AS TotalCount " & _
            '                  "FROM      hremployee_master " & _
            '                  "WHERE     1 = 1 " & _
            '                            "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) >= @startdate " & _
            '                            "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= @enddate " & _
            '                  "UNION ALL " & _
            '                  "SELECT    @Reinstated AS reason_action " & _
            '                          ", 2 AS GrpId " & _
            '                          ", @AdditionalHeadCount AS Grp " & _
            '                          ", COUNT(*) AS TotalCount " & _
            '                  "FROM      hremployee_master " & _
            '                  "WHERE     1 = 1 " & _
            '                            "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) < @startdate " & _
            '                            "AND CONVERT(CHAR(8), hremployee_master.reinstatement_date, 112) >= @startdate " & _
            '                            "AND CONVERT(CHAR(8), hremployee_master.reinstatement_date, 112) <= @enddate " & _
            '                            "AND reinstatement_date > appointeddate " & _
            '                            "AND reinstatement_date IS NOT NULL " & _
            '                  "UNION ALL " & _
            '                  "SELECT    ISNULL(hraction_reason_master.reason_action, @terminated) AS reason_action " & _
            '                          ", 3 AS GrpId " & _
            '                          ", @LessHeadCount AS Grp " & _
            '                          ", COUNT(*) AS TotalCount " & _
            '                  "FROM      hremployee_master " & _
            '                            "LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid " & _
            '                                                                    "AND hraction_reason_master.isreason = 1 " & _
            '                  "WHERE     1 = 1 " & _
            '                            "AND CONVERT(CHAR(8), hremployee_master.termination_to_date, 112) >= @startdate " & _
            '                            "AND CONVERT(CHAR(8), hremployee_master.termination_to_date, 112) <= @enddate " & _
            '                            "AND empl_enddate IS NULL " & _
            '                            "AND termination_from_date IS NULL " & _
            '                  "GROUP BY  reason_action " & _
            '                  "UNION ALL " & _
            '                  "SELECT    ISNULL(hraction_reason_master.reason_action, @leaving) AS reason_action " & _
            '                          ", 3 AS GrpId " & _
            '                          ", @LessHeadCount AS Grp " & _
            '                          ", COUNT(*) AS TotalCount " & _
            '                  "FROM      hremployee_master " & _
            '                            "LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid " & _
            '                                                                    "AND hraction_reason_master.isreason = 1 " & _
            '                  "WHERE     1 = 1 " & _
            '                            "AND CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) >= @startdate " & _
            '                            "AND CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) <= @enddate " & _
            '                            "AND empl_enddate IS NULL " & _
            '                  "GROUP BY  reason_action " & _
            '                  "UNION ALL " & _
            '                  "SELECT    ISNULL(hraction_reason_master.reason_action, '') AS reason_action " & _
            '                          ", 3 AS GrpId " & _
            '                          ", @LessHeadCount AS Grp " & _
            '                          ", COUNT(*) AS TotalCount " & _
            '                  "FROM      hremployee_master " & _
            '                            "LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid " & _
            '                                                                    "AND hraction_reason_master.isreason = 1 " & _
            '                  "WHERE     1 = 1 " & _
            '                            "AND CONVERT(CHAR(8), hremployee_master.empl_enddate, 112) >= @startdate " & _
            '                            "AND CONVERT(CHAR(8), hremployee_master.empl_enddate, 112) <= @enddate " & _
            '                  "GROUP BY  reason_action " & _
            '                ") AS TableLessHeadCount "

            '*** Head_Count_previous_period_BF FROM prtnaleave_tran Table and same as payroll summary count.
            StrQ = "SELECT  reason_action AS Reason " & _
                                     ", GrpId " & _
                                     ", Grp " & _
                                     ", TotalCount " & _
                               "FROM    ( SELECT    @Head_Count_previous_period_BF AS reason_action " & _
                                                 ", 1 AS GrpId " & _
                                                 ", @Head_Count_previous_period_BF AS Grp " & _
                                                 ", SUM(EmpCount) AS TotalCount " & _
                                          "FROM      ( SELECT    COUNT(DISTINCT prempsalary_tran.employeeunkid) AS EmpCount " & _
                                                      "FROM     " & mstrBFDatabaseName & "..prempsalary_tran " & _
                                                                "JOIN " & mstrBFDatabaseName & "..prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                                                "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                                                "JOIN " & mstrBFDatabaseName & "..hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xBFDateJoinQry.Trim.Length > 0 Then
                StrQ &= xBFDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xBFUACQry.Trim.Length > 0 Then
            '    StrQ &= xBFUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACQry.Trim.Length > 0 Then
                    StrQ &= xBFUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END
            'Sohail (21 Aug 2015) -- End

            StrQ &= "                                  WHERE     ISNULL(prempsalary_tran.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                                "AND prpayment_tran.paymentmode IN ( " & enPaymentMode.CHEQUE & ", " & enPaymentMode.TRANSFER & " ) " & _
                                                                "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                                                "AND prpayment_tran.periodunkid = @prevperiodid "
            'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmployee = False Then
            '    StrQ &= strQInactiveEmployee
            'End If
            'Sohail (21 Aug 2015) -- End


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xBFUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xBFDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xBFDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-May-2013) -- End

            StrQ &= "                                  UNION ALL " & _
                                                      "SELECT    COUNT(DISTINCT prpayment_tran.employeeunkid) AS EmpCount " & _
                                                      "FROM     " & mstrBFDatabaseName & "..prpayment_tran " & _
                                                                "JOIN " & mstrBFDatabaseName & "..hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xBFDateJoinQry.Trim.Length > 0 Then
                StrQ &= xBFDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xBFUACQry.Trim.Length > 0 Then
            '    StrQ &= xBFUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACQry.Trim.Length > 0 Then
                    StrQ &= xBFUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xBFAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xBFAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "                                  WHERE     ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                                "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                                                "AND prpayment_tran.paymentmode IN ( " & enPaymentMode.CASH & ", " & enPaymentMode.CASH_AND_CHEQUE & " ) " & _
                                                                "AND prpayment_tran.periodunkid = @prevperiodid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmployee = False Then
            '    StrQ &= strQInactiveEmployee
            'End If
            'Sohail (21 Aug 2015) -- End

            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xBFUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xBFDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xBFDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-May-2013) -- End

            StrQ &= "                                  UNION ALL " & _
                                                      "SELECT    COUNT(prtnaleave_tran.employeeunkid) AS EmpCount " & _
                                                      "FROM     " & mstrBFDatabaseName & "..prtnaleave_tran " & _
                                                                "JOIN " & mstrBFDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xBFDateJoinQry.Trim.Length > 0 Then
                StrQ &= xBFDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xBFUACQry.Trim.Length > 0 Then
            '    StrQ &= xBFUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACQry.Trim.Length > 0 Then
                    StrQ &= xBFUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xBFAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xBFAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "                                  WHERE     ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                                "AND prtnaleave_tran.balanceamount <> 0 " & _
                                                                "AND prtnaleave_tran.payperiodunkid = @prevperiodid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmployee = False Then
            '    StrQ &= strQInactiveEmployee
            'End If
            'Sohail (21 Aug 2015) -- End

            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xBFUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xBFDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xBFDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-May-2013) -- End

            'Sohail (22 Nov 2012) -- Start
            'TRA - ENHANCEMENT - Now Less Head Count with Payroll Included from Previous Period  (B/F) AND Less Head Count with Payroll Excluded from Current Period.
            'StrQ &= "                                ) AS TableBF " & _
            '                             "UNION ALL " & _
            '                             "SELECT    @NewHiredEmployees AS reason_action " & _
            '                                     ", 2 AS GrpId " & _
            '                                     ", @AdditionalHeadCount AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) >= @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= @enddate " & _
            '                             "UNION ALL " & _
            '                             "SELECT    @Reinstated AS reason_action " & _
            '                                     ", 2 AS GrpId " & _
            '                                     ", @AdditionalHeadCount AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) < @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.reinstatement_date, 112) >= @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.reinstatement_date, 112) <= @enddate " & _
            '                                       "AND reinstatement_date > appointeddate " & _
            '                                       "AND reinstatement_date IS NOT NULL " & _
            '                             "UNION ALL " & _
            '                             "SELECT    ISNULL(hraction_reason_master.reason_action, @retired) AS reason_action " & _
            '                                     ", 3 AS GrpId " & _
            '                                     ", @LessHeadCount AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                                       "LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid " & _
            '                                                                               "AND hraction_reason_master.isreason = 1 " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_to_date, 112) >= @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_to_date, 112) <= @enddate " & _
            '                                       "AND empl_enddate IS NULL " & _
            '                                       "AND termination_from_date IS NULL " & _
            '                             "GROUP BY  reason_action " & _
            '                             "UNION ALL " & _
            '                             "SELECT    ISNULL(hraction_reason_master.reason_action, @terminated) AS reason_action " & _
            '                                     ", 3 AS GrpId " & _
            '                                     ", @LessHeadCount AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                                       "LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid " & _
            '                                                                               "AND hraction_reason_master.isreason = 1 " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) >= @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) <= @enddate " & _
            '                                       "AND empl_enddate IS NULL " & _
            '                             "GROUP BY  reason_action " & _
            '                             "UNION ALL " & _
            '                             "SELECT    ISNULL(hraction_reason_master.reason_action, '') AS reason_action " & _
            '                                     ", 3 AS GrpId " & _
            '                                     ", @LessHeadCount AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                                       "LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid " & _
            '                                                                               "AND hraction_reason_master.isreason = 1 " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.empl_enddate, 112) >= @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.empl_enddate, 112) <= @enddate " & _
            '                             "GROUP BY  reason_action " & _
            '                           ") AS TableLessHeadCount "
            'Sohail (08 May 2013) -- Start
            'TRA - ENHANCEMENT - Now don't include those employees in REINSTATED COUNT who are already counted in Previous period B/F count (As per Rutta's Comment)
            'StrQ &= "                                ) AS TableBF " & _
            '                             "UNION ALL " & _
            '                             "SELECT    @NewHiredEmployees AS reason_action " & _
            '                                     ", 2 AS GrpId " & _
            '                                     ", @AdditionalHeadCount AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) >= @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= @enddate " & _
            '                                       " AND isapproved = 1 " & _
            '                             "UNION ALL " & _
            '                             "SELECT    @Reinstated AS reason_action " & _
            '                                     ", 2 AS GrpId " & _
            '                                     ", @AdditionalHeadCount AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) < @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.reinstatement_date, 112) >= @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.reinstatement_date, 112) <= @enddate " & _
            '                                       "AND reinstatement_date > appointeddate " & _
            '                                       "AND reinstatement_date IS NOT NULL " & _
            '                                       " AND isapproved = 1 " & _
            '                             "UNION ALL " & _
            '                             "SELECT    ISNULL(hraction_reason_master.reason_action, @retired) AS reason_action " & _
            '                                     ", 3 AS GrpId " & _
            '                                     ", @LessHeadCountPreviousPeriodPayrollIncluded AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                                       "LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid " & _
            '                                                                               "AND hraction_reason_master.isreason = 1 " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_to_date, 112) >= @prevstartdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_to_date, 112) <= @prevenddate " & _
            '                                       "AND empl_enddate IS NULL " & _
            '                                       "AND termination_from_date IS NULL " & _
            '                                       "AND isexclude_payroll = 0 " & _
            '                                       " AND isapproved = 1 " & _
            '                                       "AND ( ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) <= @prevenddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) <= @prevenddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) <= @prevenddate " & _
            '                                            ") " & _
            '                             "GROUP BY  reason_action " & _
            '                             "UNION ALL " & _
            '                             "SELECT    ISNULL(hraction_reason_master.reason_action, @terminated) AS reason_action " & _
            '                                     ", 3 AS GrpId " & _
            '                                     ", @LessHeadCountPreviousPeriodPayrollIncluded AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                                       "LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid " & _
            '                                                                               "AND hraction_reason_master.isreason = 1 " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) >= @prevstartdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) <= @prevenddate " & _
            '                                       "AND empl_enddate IS NULL " & _
            '                                       "AND isexclude_payroll = 0 " & _
            '                                       " AND isapproved = 1 " & _
            '                                       "AND ( ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) <= @prevenddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) <= @prevenddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) <= @prevenddate " & _
            '                                            ") " & _
            '                             "GROUP BY  reason_action " & _
            '                             "UNION ALL " & _
            '                             "SELECT    ISNULL(hraction_reason_master.reason_action, '') AS reason_action " & _
            '                                     ", 3 AS GrpId " & _
            '                                     ", @LessHeadCountPreviousPeriodPayrollIncluded AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                                       "LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid " & _
            '                                                                               "AND hraction_reason_master.isreason = 1 " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.empl_enddate, 112) >= @prevstartdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.empl_enddate, 112) <= @prevenddate " & _
            '                                       "AND isexclude_payroll = 0 " & _
            '                                       " AND isapproved = 1 " & _
            '                                       "AND ( ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) <= @prevenddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) <= @prevenddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) <= @prevenddate " & _
            '                                            ") " & _
            '                             "GROUP BY  reason_action " & _
            '                             "UNION ALL " & _
            '                             "SELECT    ISNULL(hraction_reason_master.reason_action, @retired) AS reason_action " & _
            '                                     ", 4 AS GrpId " & _
            '                                     ", @LessHeadCountPayrollExcluded AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                                       "LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid " & _
            '                                                                               "AND hraction_reason_master.isreason = 1 " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_to_date, 112) >= @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_to_date, 112) <= @enddate " & _
            '                                       "AND empl_enddate IS NULL " & _
            '                                       "AND termination_from_date IS NULL " & _
            '                                       "AND isexclude_payroll = 1 " & _
            '                                       " AND isapproved = 1 " & _
            '                                       "AND ( ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @nextstartdate) <= @enddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @nextstartdate) <= @enddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @nextstartdate) <= @enddate " & _
            '                                            ") " & _
            '                             "GROUP BY  reason_action " & _
            '                             "UNION ALL " & _
            '                             "SELECT    ISNULL(hraction_reason_master.reason_action, @terminated) AS reason_action " & _
            '                                     ", 4 AS GrpId " & _
            '                                     ", @LessHeadCountPayrollExcluded AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                                       "LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid " & _
            '                                                                               "AND hraction_reason_master.isreason = 1 " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) >= @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_from_date, 112) <= @enddate " & _
            '                                       "AND empl_enddate IS NULL " & _
            '                                       "AND isexclude_payroll = 1 " & _
            '                                       " AND isapproved = 1 " & _
            '                                       "AND ( ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @nextstartdate) <= @enddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @nextstartdate) <= @enddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @nextstartdate) <= @enddate " & _
            '                                            ") " & _
            '                             "GROUP BY  reason_action " & _
            '                             "UNION ALL " & _
            '                             "SELECT    ISNULL(hraction_reason_master.reason_action, '') AS reason_action " & _
            '                                     ", 4 AS GrpId " & _
            '                                     ", @LessHeadCountPayrollExcluded AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                                       "LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid " & _
            '                                                                               "AND hraction_reason_master.isreason = 1 " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.empl_enddate, 112) >= @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.empl_enddate, 112) <= @enddate " & _
            '                                       "AND isexclude_payroll = 1 " & _
            '                                       " AND isapproved = 1 " & _
            '                                       "AND ( ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @nextstartdate) <= @enddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @nextstartdate) <= @enddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @nextstartdate) <= @enddate " & _
            '                                            ") " & _
            '                             "GROUP BY  reason_action " & _
            '                           ") AS TableLessHeadCount "
            StrQ &= "                                ) AS TableBF " & _
                                         "UNION ALL " & _
                                         "SELECT    @NewHiredEmployees AS reason_action " & _
                                                 ", 2 AS GrpId " & _
                                                 ", @AdditionalHeadCount AS Grp " & _
                                                 ", COUNT(*) AS TotalCount " & _
                                         "FROM      hremployee_master "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "                     WHERE     1 = 1 " & _
                                                   "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) >= @startdate " & _
                                                   "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= @enddate " & _
                                                   " AND isapproved = 1 "

            'Sohail (23 May 2014) -- Start
            'Enhancement - To match this report with Newly Head Employee Report
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (23 May 2014) -- End

            StrQ &= "                     UNION ALL " & _
                                         "SELECT    @Reinstated AS reason_action " & _
                                                 ", 2 AS GrpId " & _
                                                 ", @AdditionalHeadCount AS Grp " & _
                                                 ", COUNT(*) AS TotalCount " & _
                                         "FROM      hremployee_master " & _
                                                    "LEFT JOIN ( SELECT    hremployee_master.employeeunkid " & _
                                                                 "FROM     " & mstrBFDatabaseName & "..prempsalary_tran " & _
                                                                           "JOIN " & mstrBFDatabaseName & "..prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                                                           "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                                                           "JOIN " & mstrBFDatabaseName & "..hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xBFDateJoinQry.Trim.Length > 0 Then
                StrQ &= xBFDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xBFUACQry.Trim.Length > 0 Then
            '    StrQ &= xBFUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACQry.Trim.Length > 0 Then
                    StrQ &= xBFUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xBFAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xBFAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "                                             WHERE     ISNULL(prempsalary_tran.isvoid, 0) = 0 " & _
                                                                           "AND ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                                           "AND prpayment_tran.paymentmode IN ( " & enPaymentMode.CHEQUE & ", " & enPaymentMode.TRANSFER & " ) " & _
                                                                           "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                                                           "AND prpayment_tran.periodunkid = @prevperiodid "
            'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmployee = False Then
            '    StrQ &= strQInactiveEmployee
            'End If
            'Sohail (21 Aug 2015) -- End

            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xBFUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xBFDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xBFDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-May-2013) -- End

            StrQ &= "                                             UNION ALL " & _
                                                                  "SELECT   hremployee_master.employeeunkid " & _
                                                                  "FROM     " & mstrBFDatabaseName & "..prpayment_tran " & _
                                                                            "JOIN " & mstrBFDatabaseName & "..hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xBFDateJoinQry.Trim.Length > 0 Then
                StrQ &= xBFDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xBFUACQry.Trim.Length > 0 Then
            '    StrQ &= xBFUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACQry.Trim.Length > 0 Then
                    StrQ &= xBFUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xBFAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xBFAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "                                                  WHERE     ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                                                                            "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                                                            "AND prpayment_tran.paymentmode IN ( " & enPaymentMode.CASH & ", " & enPaymentMode.CASH_AND_CHEQUE & " ) " & _
                                                                            "AND prpayment_tran.periodunkid = @prevperiodid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmployee = False Then
            '    StrQ &= strQInactiveEmployee
            'End If
            'Sohail (21 Aug 2015) -- End

            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xBFUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xBFDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xBFDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-May-2013) -- End

            StrQ &= "                                             UNION ALL " & _
                                                                  "SELECT   hremployee_master.employeeunkid " & _
                                                                  "FROM     " & mstrBFDatabaseName & "..prtnaleave_tran " & _
                                                                            "JOIN " & mstrBFDatabaseName & "..hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If xBFDateJoinQry.Trim.Length > 0 Then
                StrQ &= xBFDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xBFUACQry.Trim.Length > 0 Then
            '    StrQ &= xBFUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACQry.Trim.Length > 0 Then
                    StrQ &= xBFUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xBFAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xBFAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "                                              WHERE     ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                                            "AND prtnaleave_tran.balanceamount <> 0 " & _
                                                                            "AND prtnaleave_tran.payperiodunkid = @prevperiodid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmployee = False Then
            '    StrQ &= strQInactiveEmployee
            'End If
            'Sohail (21 Aug 2015) -- End

            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xBFUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xBFDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xBFDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Pinkal (24-May-2013) -- End

            StrQ &= "                   ) AS Table_BF ON Table_BF.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "LEFT JOIN " & _
                            "( " & _
                                 "SELECT " & _
                                       "RH.EmpId " & _
                                      ",RH.REHIRE " & _
                                 "FROM " & _
                                 "( " & _
                                      "SELECT " & _
                                            "employeeunkid AS EmpId " & _
                                           ",reinstatment_date AS REHIRE " & _
                                           ",effectivedate " & _
                                           ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                      "FROM hremployee_rehire_tran WHERE isvoid = 0 " & _
                                      " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                 ") AS RH WHERE RH.xNo = 1 " & _
                            ") AS HIRE ON HIRE.EmpId = hremployee_master.employeeunkid "
            'Sohail (21 Aug 2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'StrQ &= "                     WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) < @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.reinstatement_date, 112) >= @startdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.reinstatement_date, 112) <= @enddate " & _
            '                                       "AND reinstatement_date > appointeddate " & _
            '                                       "AND reinstatement_date IS NOT NULL " & _
            '                                       " AND isapproved = 1 " & _
            '                                       "AND Table_BF.employeeunkid IS NULL " & _
            '                             "UNION ALL " & _
            '                             "SELECT    ISNULL(TR.name, @retired) AS reason_action " & _
            '                                     ", 3 AS GrpId " & _
            '                                     ", @LessHeadCountPreviousPeriodPayrollIncluded AS Grp " & _
            '                                     ", COUNT(*) AS TotalCount " & _
            '                             "FROM      hremployee_master " & _
            '                                       " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
            '                             "WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_to_date, 112) >= @prevstartdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.termination_to_date, 112) <= @prevenddate " & _
            '                                       "AND empl_enddate IS NULL " & _
            '                                       "AND termination_from_date IS NULL " & _
            '                                       "AND isexclude_payroll = 0 " & _
            '                                       " AND isapproved = 1 " & _
            '                                       "AND ( ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) <= @prevenddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) <= @prevenddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) <= @prevenddate " & _
            '                                            ") "
            StrQ &= "                     WHERE     1 = 1 " & _
                                                   "AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) < @startdate " & _
                                                   "AND CONVERT(CHAR(8), HIRE.REHIRE, 112) >= @startdate " & _
                                                   "AND CONVERT(CHAR(8), HIRE.REHIRE, 112) <= @enddate " & _
                                                   "AND HIRE.REHIRE > appointeddate " & _
                                                   "AND HIRE.REHIRE IS NOT NULL " & _
                                                   " AND isapproved = 1 " & _
                                                   "AND Table_BF.employeeunkid IS NULL " & _
                                         "UNION ALL " & _
                                         "SELECT    ISNULL(TR.name, @retired) AS reason_action " & _
                                                 ", 3 AS GrpId " & _
                                                 ", @LessHeadCountPreviousPeriodPayrollIncluded AS Grp " & _
                                                 ", COUNT(*) AS TotalCount " & _
                                         "FROM      hremployee_master " & _
                                                    " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                                                      "LEFT JOIN " & _
                                                        "( " & _
                                                             "SELECT " & _
                                                                  " R.EmpId " & _
                                                                  ",R.RETIRE " & _
                                                             "FROM " & _
                                                             "( " & _
                                                                  "SELECT " & _
                                                                       " employeeunkid AS EmpId " & _
                                                                       ",date1 AS RETIRE " & _
                                                                       ",effectivedate " & _
                                                                       ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                                                  "FROM hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_RETIREMENT & "') AND isvoid = 0 " & _
                                                                  " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                                             ") AS R WHERE R.xNo = 1 " & _
                                                        ") AS RT ON RT.EmpId = hremployee_master.employeeunkid " & _
                                                        " LEFT JOIN " & _
                                                        "( " & _
                                                             "SELECT " & _
                                                                  " T.EmpId " & _
                                                                  ",T.EOC " & _
                                                                  ",T.LEAVING " & _
                                                                  ",T.isexclude_payroll AS IsExPayroll " & _
                                                             "FROM " & _
                                                             "( " & _
                                                                  "SELECT " & _
                                                                       "employeeunkid AS EmpId " & _
                                                                       ",date1 AS EOC " & _
                                                                       ",date2 AS LEAVING " & _
                                                                       ",effectivedate " & _
                                                                       ",isexclude_payroll " & _
                                                                       ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                                                  "FROM hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_TERMINATION & "') AND isvoid = 0 " & _
                                                                  " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                                             ") AS T WHERE T.xNo = 1 " & _
                                                        ") AS TM ON TM.EmpId = hremployee_master.employeeunkid "
            'Sohail (03 Dec 2016) - [AND reinstatement_date > appointeddate] = [AND HIRE.REHIRE > appointeddate]

            If xBFDateJoinQry.Trim.Length > 0 Then
                StrQ &= xBFDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xBFUACQry.Trim.Length > 0 Then
            '    StrQ &= xBFUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACQry.Trim.Length > 0 Then
                    StrQ &= xBFUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xBFAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xBFAdvanceJoinQry
            End If
            StrQ &= "                     WHERE     1 = 1 " & _
                                                   "AND CONVERT(CHAR(8), RT.RETIRE, 112) >= @prevstartdate " & _
                                                   "AND CONVERT(CHAR(8), RT.RETIRE, 112) <= @prevenddate " & _
                                                   "AND TM.EOC IS NULL " & _
                                                   "AND TM.LEAVING IS NULL " & _
                                                   "AND TM.IsExPayroll = 0 " & _
                                                   " AND isapproved = 1 " & _
                                                   "AND ( ISNULL(CONVERT(CHAR(8), TM.LEAVING, 112), @startdate) <= @prevenddate " & _
                                                                  "OR ISNULL(CONVERT(CHAR(8), RT.RETIRE, 112), @startdate) <= @prevenddate " & _
                                                                  "OR ISNULL(CONVERT(CHAR(8), TM.EOC, 112), @startdate) <= @prevenddate " & _
                                                        ") "
            'Sohail (21 Aug 2015) -- End
            'S.SANDEEP [09 APR 2015] -- START
            ' LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid ------ REMOVED
            ' LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
            'S.SANDEEP [09 APR 2015] -- END

            'Sohail (23 May 2014) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmployee = False Then
            '    StrQ &= strQInactiveEmployee
            'End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xBFUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xBFDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xBFDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (23 May 2014) -- End

            StrQ &= "                     GROUP BY  ISNULL(TR.name, @retired) " & _
                                         "UNION ALL " & _
                                         "SELECT    ISNULL(TR.name, @terminated) AS reason_action " & _
                                                 ", 3 AS GrpId " & _
                                                 ", @LessHeadCountPreviousPeriodPayrollIncluded AS Grp " & _
                                                 ", COUNT(*) AS TotalCount " & _
                                         "FROM      hremployee_master " & _
                                                   " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' " & _
                                                   "LEFT JOIN " & _
                                                   "( " & _
                                                         "SELECT " & _
                                                              " R.EmpId " & _
                                                              ",R.RETIRE " & _
                                                         "FROM " & _
                                                         "( " & _
                                                              "SELECT " & _
                                                                   " employeeunkid AS EmpId " & _
                                                                   ",date1 AS RETIRE " & _
                                                                   ",effectivedate " & _
                                                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                                              "FROM hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_RETIREMENT & "') AND isvoid = 0 " & _
                                                              " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                                         ") AS R WHERE R.xNo = 1 " & _
                                                    ") AS RT ON RT.EmpId = hremployee_master.employeeunkid " & _
                                                    " LEFT JOIN " & _
                                                    "( " & _
                                                         "SELECT " & _
                                                              " T.EmpId " & _
                                                              ",T.EOC " & _
                                                              ",T.LEAVING " & _
                                                              ",T.isexclude_payroll AS IsExPayroll " & _
                                                         "FROM " & _
                                                         "( " & _
                                                              "SELECT " & _
                                                                   "employeeunkid AS EmpId " & _
                                                                   ",date1 AS EOC " & _
                                                                   ",date2 AS LEAVING " & _
                                                                   ",effectivedate " & _
                                                                   ",isexclude_payroll " & _
                                                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                                              "FROM hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_TERMINATION & "') AND isvoid = 0 " & _
                                                              " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                                         ") AS T WHERE T.xNo = 1 " & _
                                                    ") AS TM ON TM.EmpId = hremployee_master.employeeunkid "

            If xBFDateJoinQry.Trim.Length > 0 Then
                StrQ &= xBFDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xBFUACQry.Trim.Length > 0 Then
            '    StrQ &= xBFUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACQry.Trim.Length > 0 Then
                    StrQ &= xBFUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xBFAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xBFAdvanceJoinQry
            End If
            StrQ &= "WHERE     1 = 1 " & _
                                                   "AND CONVERT(CHAR(8), TM.LEAVING, 112) >= @prevstartdate " & _
                                                   "AND CONVERT(CHAR(8), TM.LEAVING, 112) <= @prevenddate " & _
                                                   "AND TM.EOC IS NULL " & _
                                                   "AND TM.IsExPayroll = 0 " & _
                                                   " AND isapproved = 1 " & _
                                                   "AND ( ISNULL(CONVERT(CHAR(8), TM.LEAVING, 112), @startdate) <= @prevenddate " & _
                                                                  "OR ISNULL(CONVERT(CHAR(8), RT.RETIRE, 112), @startdate) <= @prevenddate " & _
                                                                  "OR ISNULL(CONVERT(CHAR(8), TM.EOC, 112), @startdate) <= @prevenddate " & _
                                                        ") "

            'S.SANDEEP [09 APR 2015] -- START
            ' LEFT JOIN hraction_reason_master ON hremployee_master.actionreasonunkid = hraction_reason_master.actionreasonunkid ------ REMOVED
            ' LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
            'S.SANDEEP [09 APR 2015] -- END

            'Sohail (23 May 2014) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmployee = False Then
            '    StrQ &= strQInactiveEmployee
            'End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xBFUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xBFDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xBFDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (23 May 2014) -- End

            StrQ &= "                     GROUP BY  ISNULL(TR.name, @terminated) " & _
                                         "UNION ALL " & _
                                         "SELECT    ISNULL(TR.name, '') AS reason_action " & _
                                                 ", 3 AS GrpId " & _
                                                 ", @LessHeadCountPreviousPeriodPayrollIncluded AS Grp " & _
                                                 ", COUNT(*) AS TotalCount " & _
                                         "FROM      hremployee_master " & _
                                                   " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "                               LEFT JOIN " & _
                                                   "( " & _
                                                         "SELECT " & _
                                                              " R.EmpId " & _
                                                              ",R.RETIRE " & _
                                                         "FROM " & _
                                                         "( " & _
                                                              "SELECT " & _
                                                                   " employeeunkid AS EmpId " & _
                                                                   ",date1 AS RETIRE " & _
                                                                   ",effectivedate " & _
                                                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                                              "FROM hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_RETIREMENT & "') AND isvoid = 0 " & _
                                                              " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                                         ") AS R WHERE R.xNo = 1 " & _
                                                    ") AS RT ON RT.EmpId = hremployee_master.employeeunkid " & _
                                                    " LEFT JOIN " & _
                                                    "( " & _
                                                         "SELECT " & _
                                                              " T.EmpId " & _
                                                              ",T.EOC " & _
                                                              ",T.LEAVING " & _
                                                              ",T.isexclude_payroll AS IsExPayroll " & _
                                                         "FROM " & _
                                                         "( " & _
                                                              "SELECT " & _
                                                                   "employeeunkid AS EmpId " & _
                                                                   ",date1 AS EOC " & _
                                                                   ",date2 AS LEAVING " & _
                                                                   ",effectivedate " & _
                                                                   ",isexclude_payroll " & _
                                                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                                              "FROM hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_TERMINATION & "') AND isvoid = 0 " & _
                                                              " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                                         ") AS T WHERE T.xNo = 1 " & _
                                                    ") AS TM ON TM.EmpId = hremployee_master.employeeunkid "

            If xBFDateJoinQry.Trim.Length > 0 Then
                StrQ &= xBFDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xBFUACQry.Trim.Length > 0 Then
            '    StrQ &= xBFUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACQry.Trim.Length > 0 Then
                    StrQ &= xBFUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xBFAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xBFAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'StrQ &= "                     WHERE     1 = 1 " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.empl_enddate, 112) >= @prevstartdate " & _
            '                                       "AND CONVERT(CHAR(8), hremployee_master.empl_enddate, 112) <= @prevenddate " & _
            '                                       "AND isexclude_payroll = 0 " & _
            '                                       " AND isapproved = 1 " & _
            '                                       "AND ( ISNULL(CONVERT(CHAR(8), termination_from_date, 112), @startdate) <= @prevenddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), termination_to_date, 112), @startdate) <= @prevenddate " & _
            '                                                      "OR ISNULL(CONVERT(CHAR(8), empl_enddate, 112), @startdate) <= @prevenddate " & _
            '                                            ") "
            StrQ &= "                     WHERE     1 = 1 " & _
                                                   "AND CONVERT(CHAR(8), TM.EOC, 112) >= @prevstartdate " & _
                                                   "AND CONVERT(CHAR(8), TM.EOC, 112) <= @prevenddate " & _
                                                   "AND TM.IsExPayroll = 0 " & _
                                                   " AND isapproved = 1 " & _
                                                   "AND ( ISNULL(CONVERT(CHAR(8), TM.LEAVING, 112), @startdate) <= @prevenddate " & _
                                                                  "OR ISNULL(CONVERT(CHAR(8), RT.RETIRE, 112), @startdate) <= @prevenddate " & _
                                                                  "OR ISNULL(CONVERT(CHAR(8), TM.EOC, 112), @startdate) <= @prevenddate " & _
                                                        ") "
            'Sohail (21 Aug 2015) -- End
            'S.SANDEEP [09 APR 2015] -- START
            ' LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hremployee_master.actionreasonunkid ------ REMOVED
            ' LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
            'S.SANDEEP [09 APR 2015] -- END

            'Sohail (23 May 2014) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmployee = False Then
            '    StrQ &= strQInactiveEmployee
            'End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xBFUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xBFUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xBFDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xBFDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (23 May 2014) -- End

            StrQ &= "                     GROUP BY  ISNULL(TR.name, '') " & _
                                         "UNION ALL " & _
                                         "SELECT    ISNULL(TR.name, @retired) AS reason_action " & _
                                                 ", 4 AS GrpId " & _
                                                 ", @LessHeadCountPayrollExcluded AS Grp " & _
                                                 ", COUNT(*) AS TotalCount " & _
                                         "FROM      hremployee_master " & _
                                                   "LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "                               LEFT JOIN " & _
                                                   "( " & _
                                                         "SELECT " & _
                                                              " R.EmpId " & _
                                                              ",R.RETIRE " & _
                                                         "FROM " & _
                                                         "( " & _
                                                              "SELECT " & _
                                                                   " employeeunkid AS EmpId " & _
                                                                   ",date1 AS RETIRE " & _
                                                                   ",effectivedate " & _
                                                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                                              "FROM hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_RETIREMENT & "') AND isvoid = 0 " & _
                                                              " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                                         ") AS R WHERE R.xNo = 1 " & _
                                                    ") AS RT ON RT.EmpId = hremployee_master.employeeunkid " & _
                                                    " LEFT JOIN " & _
                                                    "( " & _
                                                         "SELECT " & _
                                                              " T.EmpId " & _
                                                              ",T.EOC " & _
                                                              ",T.LEAVING " & _
                                                              ",T.isexclude_payroll AS IsExPayroll " & _
                                                         "FROM " & _
                                                         "( " & _
                                                              "SELECT " & _
                                                                   "employeeunkid AS EmpId " & _
                                                                   ",date1 AS EOC " & _
                                                                   ",date2 AS LEAVING " & _
                                                                   ",effectivedate " & _
                                                                   ",isexclude_payroll " & _
                                                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                                              "FROM hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_TERMINATION & "') AND isvoid = 0 " & _
                                                              " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                                         ") AS T WHERE T.xNo = 1 " & _
                                                    ") AS TM ON TM.EmpId = hremployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "                     WHERE     1 = 1 " & _
                                                   "AND CONVERT(CHAR(8), RT.RETIRE, 112) >= @startdate " & _
                                                   "AND CONVERT(CHAR(8), RT.RETIRE, 112) <= @enddate " & _
                                                   "AND TM.EOC IS NULL " & _
                                                   "AND TM.LEAVING IS NULL " & _
                                                   "AND TM.IsExPayroll = 1 " & _
                                                   " AND isapproved = 1 " & _
                                                   "AND ( ISNULL(CONVERT(CHAR(8), TM.LEAVING, 112), @nextstartdate) <= @enddate " & _
                                                                  "OR ISNULL(CONVERT(CHAR(8), RT.RETIRE, 112), @nextstartdate) <= @enddate " & _
                                                                  "OR ISNULL(CONVERT(CHAR(8), TM.EOC, 112), @nextstartdate) <= @enddate " & _
                                                        ") "
            'S.SANDEEP [09 APR 2015] -- START
            ' LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hremployee_master.actionreasonunkid ------ REMOVED
            ' LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
            'S.SANDEEP [09 APR 2015] -- END


            'Sohail (23 May 2014) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmployee = False Then
            '    StrQ &= strQInactiveEmployee
            'End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (23 May 2014) -- End

            StrQ &= "                     GROUP BY  ISNULL(TR.name, @retired) " & _
                                         "UNION ALL " & _
                                         "SELECT    ISNULL(TR.name, @terminated) AS reason_action " & _
                                                 ", 4 AS GrpId " & _
                                                 ", @LessHeadCountPayrollExcluded AS Grp " & _
                                                 ", COUNT(*) AS TotalCount " & _
                                         "FROM      hremployee_master " & _
                                                   " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "                               LEFT JOIN " & _
                                                   "( " & _
                                                         "SELECT " & _
                                                              " R.EmpId " & _
                                                              ",R.RETIRE " & _
                                                         "FROM " & _
                                                         "( " & _
                                                              "SELECT " & _
                                                                   " employeeunkid AS EmpId " & _
                                                                   ",date1 AS RETIRE " & _
                                                                   ",effectivedate " & _
                                                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                                              "FROM hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_RETIREMENT & "') AND isvoid = 0 " & _
                                                              " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                                         ") AS R WHERE R.xNo = 1 " & _
                                                    ") AS RT ON RT.EmpId = hremployee_master.employeeunkid " & _
                                                    " LEFT JOIN " & _
                                                    "( " & _
                                                         "SELECT " & _
                                                              " T.EmpId " & _
                                                              ",T.EOC " & _
                                                              ",T.LEAVING " & _
                                                              ",T.isexclude_payroll AS IsExPayroll " & _
                                                         "FROM " & _
                                                         "( " & _
                                                              "SELECT " & _
                                                                   "employeeunkid AS EmpId " & _
                                                                   ",date1 AS EOC " & _
                                                                   ",date2 AS LEAVING " & _
                                                                   ",effectivedate " & _
                                                                   ",isexclude_payroll " & _
                                                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                                              "FROM hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_TERMINATION & "') AND isvoid = 0 " & _
                                                              " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                                         ") AS T WHERE T.xNo = 1 " & _
                                                    ") AS TM ON TM.EmpId = hremployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "                     WHERE     1 = 1 " & _
                                                   "AND CONVERT(CHAR(8), TM.LEAVING, 112) >= @startdate " & _
                                                   "AND CONVERT(CHAR(8), TM.LEAVING, 112) <= @enddate " & _
                                                   "AND TM.EOC IS NULL " & _
                                                   "AND TM.IsExPayroll = 1 " & _
                                                   " AND isapproved = 1 " & _
                                                   "AND ( ISNULL(CONVERT(CHAR(8), TM.LEAVING, 112), @nextstartdate) <= @enddate " & _
                                                                  "OR ISNULL(CONVERT(CHAR(8), RT.RETIRE, 112), @nextstartdate) <= @enddate " & _
                                                                  "OR ISNULL(CONVERT(CHAR(8), TM.EOC, 112), @nextstartdate) <= @enddate " & _
                                                        ") "

            'S.SANDEEP [09 APR 2015] -- START
            ' LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hremployee_master.actionreasonunkid ------ REMOVED
            ' LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
            'S.SANDEEP [09 APR 2015] -- END

            'Sohail (23 May 2014) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmployee = False Then
            '    StrQ &= strQInactiveEmployee
            'End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (23 May 2014) -- End

            StrQ &= "                     GROUP BY  ISNULL(TR.name, @terminated) " & _
                                         "UNION ALL " & _
                                         "SELECT    ISNULL(TR.name, '') AS reason_action " & _
                                                 ", 4 AS GrpId " & _
                                                 ", @LessHeadCountPayrollExcluded AS Grp " & _
                                                 ", COUNT(*) AS TotalCount " & _
                                         "FROM      hremployee_master " & _
                                                   " LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' "

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            StrQ &= "                               LEFT JOIN " & _
                                                   "( " & _
                                                         "SELECT " & _
                                                              " R.EmpId " & _
                                                              ",R.RETIRE " & _
                                                         "FROM " & _
                                                         "( " & _
                                                              "SELECT " & _
                                                                   " employeeunkid AS EmpId " & _
                                                                   ",date1 AS RETIRE " & _
                                                                   ",effectivedate " & _
                                                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                                              "FROM hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_RETIREMENT & "') AND isvoid = 0 " & _
                                                              " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                                         ") AS R WHERE R.xNo = 1 " & _
                                                    ") AS RT ON RT.EmpId = hremployee_master.employeeunkid " & _
                                                    " LEFT JOIN " & _
                                                    "( " & _
                                                         "SELECT " & _
                                                              " T.EmpId " & _
                                                              ",T.EOC " & _
                                                              ",T.LEAVING " & _
                                                              ",T.isexclude_payroll AS IsExPayroll " & _
                                                         "FROM " & _
                                                         "( " & _
                                                              "SELECT " & _
                                                                   "employeeunkid AS EmpId " & _
                                                                   ",date1 AS EOC " & _
                                                                   ",date2 AS LEAVING " & _
                                                                   ",effectivedate " & _
                                                                   ",isexclude_payroll " & _
                                                                   ",ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                                              "FROM hremployee_dates_tran WHERE datetypeunkid IN('" & enEmp_Dates_Transaction.DT_TERMINATION & "') AND isvoid = 0 " & _
                                                              " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd).ToString & "' " & _
                                                         ") AS T WHERE T.xNo = 1 " & _
                                                    ") AS TM ON TM.EmpId = hremployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If
            'Sohail (21 Aug 2015) -- End

            StrQ &= "                     WHERE     1 = 1 " & _
                                                   "AND CONVERT(CHAR(8), TM.EOC, 112) >= @startdate " & _
                                                   "AND CONVERT(CHAR(8), TM.EOC, 112) <= @enddate " & _
                                                   "AND TM.IsExPayroll = 1 " & _
                                                   " AND isapproved = 1 " & _
                                                   "AND ( ISNULL(CONVERT(CHAR(8), TM.LEAVING, 112), @nextstartdate) <= @enddate " & _
                                                                  "OR ISNULL(CONVERT(CHAR(8), RT.RETIRE, 112), @nextstartdate) <= @enddate " & _
                                                                  "OR ISNULL(CONVERT(CHAR(8), TM.EOC, 112), @nextstartdate) <= @enddate " & _
                                                        ") "

            'S.SANDEEP [09 APR 2015] -- START
            ' LEFT JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hremployee_master.actionreasonunkid ------ REMOVED
            ' LEFT JOIN cfcommon_master AS TR ON TR.masterunkid = hremployee_master.actionreasonunkid AND TR.mastertype = '" & clsCommon_Master.enCommonMaster.TERMINATION & "' ----- ADDED
            'S.SANDEEP [09 APR 2015] -- END


            'Sohail (23 May 2014) -- Start
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If mblnIncludeInactiveEmployee = False Then
            '    StrQ &= strQInactiveEmployee
            'End If

            'If mstrUserAccessFilter = "" Then
            '    If UserAccessLevel._AccessLevel.Length > 0 Then
            '        StrQ &= UserAccessLevel._AccessLevelFilterString
            '    End If
            'Else
            '    StrQ &= mstrUserAccessFilter
            'End If
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If
            'Sohail (21 Aug 2015) -- End
            'Sohail (23 May 2014) -- End

            StrQ &= "                 GROUP BY  ISNULL(TR.name, '') " & _
                                       ") AS TableLessHeadCount "
            'Sohail (08 May 2013) -- End
            'Sohail (22 Nov 2012) -- End

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            'StrQ &= mstrOrderByQuery

            objDataOperation.AddParameter("@Head_Count_previous_period_BF", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Head Count of previous period B/F"))
            objDataOperation.AddParameter("@NewHiredEmployees", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "New Hired Employees"))
            objDataOperation.AddParameter("@AdditionalHeadCount", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Additional Head Count"))
            objDataOperation.AddParameter("@Reinstated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Reinstated"))
            'Sohail (22 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@LessHeadCount", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Less Head Count"))
            objDataOperation.AddParameter("@LessHeadCountPreviousPeriodPayrollIncluded", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Less Head Count with Payroll Included in Previous Period (B/F)"))
            objDataOperation.AddParameter("@LessHeadCountPayrollExcluded", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "Less Head Count with Payroll Excluded"))
            'Sohail (22 Nov 2012) -- End
            objDataOperation.AddParameter("@retired", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Retired without reason"))
            objDataOperation.AddParameter("@terminated", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Terminated without reason"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("Grp")
                rpt_Rows.Item("Column81") = dtRow.Item("GrpId")
                rpt_Rows.Item("Column2") = dtRow.Item("Reason")
                rpt_Rows.Item("Column82") = CLng(dtRow.Item("TotalCount"))

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next


            objRpt = New ArutiReport.Designer.rptPayrollHeadCount

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            'If ConfigParameter._Object._IsShowPreparedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 16, "Prepared By :"))
            '    Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "PageFooter1", True)
            'End If

            'If ConfigParameter._Object._IsShowCheckedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 26, "Checked By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection6", True)
            'End If

            'If ConfigParameter._Object._IsShowApprovedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 27, "Approved By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection7", True)
            'End If

            'If ConfigParameter._Object._IsShowReceivedBy = True Then
            '    Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 28, "Received By :"))
            'Else
            '    Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection8", True)
            'End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Call ReportFunction.TextChange(objRpt, "txtDescription", Language.getMessage(mstrModuleName, 9, "Description"))
            Call ReportFunction.TextChange(objRpt, "txtCount", Language.getMessage(mstrModuleName, 10, "Head Count"))

            Call ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 11, "Sub Total"))
            Call ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 12, "Head Count at the end of the period C/F"))

            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 13, "Print Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 14, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 15, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Head Count of previous period B/F")
            Language.setMessage(mstrModuleName, 2, "New Hired Employees")
            Language.setMessage(mstrModuleName, 3, "Additional Head Count")
            Language.setMessage(mstrModuleName, 4, "Reinstated")
            Language.setMessage(mstrModuleName, 5, "Less Head Count with Payroll Included in Previous Period (B/F)")
            Language.setMessage(mstrModuleName, 6, "Retired without reason")
            Language.setMessage(mstrModuleName, 7, "Terminated without reason")
            Language.setMessage(mstrModuleName, 8, "PAYROLL HEAD COUNT FOR THE PERIOD OF")
            Language.setMessage(mstrModuleName, 9, "Description")
            Language.setMessage(mstrModuleName, 10, "Head Count")
            Language.setMessage(mstrModuleName, 11, "Sub Total")
            Language.setMessage(mstrModuleName, 12, "Head Count at the end of the period C/F")
            Language.setMessage(mstrModuleName, 13, "Print Date :")
            Language.setMessage(mstrModuleName, 14, "Printed By :")
            Language.setMessage(mstrModuleName, 15, "Page :")
            Language.setMessage(mstrModuleName, 16, "Less Head Count with Payroll Excluded")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
