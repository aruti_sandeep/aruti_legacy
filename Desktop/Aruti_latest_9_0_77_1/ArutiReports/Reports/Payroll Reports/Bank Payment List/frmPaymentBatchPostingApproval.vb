﻿
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Security.Cryptography

Public Class frmPaymentBatchPostingApproval

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmPaymentBatchPostingApproval"
    Private objPaymentBatchPostingMaster As clspaymentbatchposting_master
    Private mblnCancel As Boolean = True
    Private mstrVoucherNo As String
    Private mintPeriodUnkid As Integer
    Private mdsBatchData As DataSet
    Private mdtPeriod_startdate As DateTime
    Private mdtPeriod_enddate As DateTime
    Private mintBankId As Integer
    Private mintBranchId As Integer
    Private mintCompanyBranchId As Integer
    Private mstrCompanyBankAccountNo As String
    Private mintCurrencyId As Integer = 0
    Private mintReportModeId As Integer
    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    Private mstrCurrency_Sign As String
    'Hemant (21 Jul 2023) -- End
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal intReportModeId As Integer, ByVal intBankId As Integer, ByVal intBranchId As Integer, ByVal intCompanyBranchId As Integer, ByVal strCompanyBankAccountNo As String, ByVal intCurrencyId As Integer, ByVal strCurrency_Sign As String) As Boolean
        'Hemant (21 Jul 2023) -- [strCurrency_Sign]
        Try
            mintReportModeId = intReportModeId
            mintBankId = intBankId
            mintBranchId = intBranchId
            mintCompanyBranchId = intCompanyBranchId
            mstrCompanyBankAccountNo = strCompanyBankAccountNo
            mintCurrencyId = intCurrencyId
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            mstrCurrency_Sign = strCurrency_Sign
            'Hemant (21 Jul 2023) -- End
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region


#Region " Private Function "

    Private Sub FillCombo()
        Dim objCountry As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Dim dsCombo As New DataSet

        Try
            Dim intFirstOpenPeriod As Integer = objCountry.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = intFirstOpenPeriod
            End With

            dsCombo = objPaymentBatchPostingMaster.getApprovalStatusComboList("List", True)
            With cboStatus
                .DisplayMember = "Name"
                .ValueMember = "Id"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = "-1"
                .SelectedIndex = 0
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objCountry = Nothing
            objPeriod = Nothing
            objPaymentBatchPostingMaster = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objPaymentBatchPostingTran As New clspaymentbatchposting_tran
        Dim dsList As DataSet
        'Dim drRow() As DataRow
        Try

            'If CInt(cboStatus.SelectedValue) <= 0 Then
            '    dtTable = mdsBatchData.Tables(0)
            'Else
            '    drRow = mdsBatchData.Tables(0).Select("status = '" & cboStatus.Text & "'")
            '    If drRow.Length > 0 Then
            '        dtTable = drRow.CopyToDataTable()

            '    End If
            'End If

            'Call SetGridDataSource(dtTable)
            dsList = objPaymentBatchPostingTran.GetList("List", CInt(cboBatch.SelectedValue))

            dgvDataList.AutoGenerateColumns = False
            dgcolhBatchId.DataPropertyName = "batchname"
            dgcolhEmployeeCode.DataPropertyName = "employeecode"
            dgcolhBankGroup.DataPropertyName = "bankname"
            dgcolhBranch.DataPropertyName = "branchname"
            dgcolhAccountNo.DataPropertyName = "accountno"
            dgvDataList.DataSource = dsList.Tables(0)
            If CInt(cboStatus.SelectedValue) = clspaymentbatchposting_master.enApprovalStatus.SubmitForApproval Then
                btnApproveReject.Visible = CBool(User._Object.Privilege._AllowToApproveRejectPaymentBatchPosting)
            Else
                btnApproveReject.Visible = False
            End If

            If CInt(cboStatus.SelectedValue) = clspaymentbatchposting_master.enApprovalStatus.Approved Then
                btnSendRequest.Enabled = True
            Else
                btnSendRequest.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
            objPaymentBatchPostingTran = Nothing
        End Try
    End Sub

    Private Sub SetGridDataSource(ByVal dtTable As DataTable)
        Try
            dgvDataList.AutoGenerateColumns = False
            dgcolhBatchId.DataPropertyName = "batchId"
            dgcolhEmployeeCode.DataPropertyName = "employeeCode"
            dgcolhAccountNo.DataPropertyName = "accountnumber"

            'Dim objBranch As New clsbankbranch_master
            'Dim dsBranch As DataSet = objBranch.getListForCombo("Branch", True)
            'With dgcolhBranch
            '    .ValueMember = "branchunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsBranch.Tables("Branch")
            '    .DataPropertyName = "branchunkid"
            '    .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            'End With
            'objdgcolhBranchUnkid.DataPropertyName = "branchunkid"
            'objBranch = Nothing

            dgvDataList.DataSource = dtTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridDataSource", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue(ByVal objPaymentBatchPostingMaster As clspaymentbatchposting_master)
        Try
            objPaymentBatchPostingMaster._Statusunkid = clspaymentbatchposting_master.enApprovalStatus.Approved
            'objPaymentBatchPostingMaster._BatchName = mstrVoucherNo & "-" & objPaymentBatchPostingMaster.getNextRefNo(mstrVoucherNo)
            'objPaymentBatchPostingMaster._Periodunkid = mintPeriodUnkid
            'objPaymentBatchPostingMaster._VoucherNo = mstrVoucherNo            '
            'objPaymentBatchPostingMaster._Userunkid = User._Object._Userunkid
            'objPaymentBatchPostingMaster._Refno = objPaymentBatchPostingMaster.getNextRefNo(mstrVoucherNo).ToString
            'objPaymentBatchPostingMaster._Isvoid = False
            'objPaymentBatchPostingMaster._Voiduserunkid = -1
            'objPaymentBatchPostingMaster._Voiddatetime = Nothing
            'objPaymentBatchPostingMaster._Voidreason = ""
            'objPaymentBatchPostingMaster._ClientIP = getIP()
            'objPaymentBatchPostingMaster._HostName = getHostName()
            'objPaymentBatchPostingMaster._FormName = mstrModuleName
            'objPaymentBatchPostingMaster._AuditUserId = User._Object._Userunkid
            'objPaymentBatchPostingMaster._Isweb = False
            'objPaymentBatchPostingMaster._BatchPostingEmpTran = CType(dgvDataList.DataSource, DataTable)
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            objPaymentBatchPostingMaster._CompanyUnkid = ConfigParameter._Object._Companyunkid
            'Hemant (21 Jul 2023) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If dgvDataList.Rows.Cast(Of DataGridViewRow).Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Atleast one Employee in the list is required for Approval."), enMsgBoxStyle.Information)
                dgvDataList.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Function GenarateJSONGenericEFT(ByVal strBatchId As String _
                                   , ByVal intBatchCount As Integer _
                                   , ByVal strOrganizationCode As String _
                                   , ByVal strbatchTimestamp As String _
                                   , ByVal drBatchData() As DataRow _
                                   , ByVal strDebitAccountNumber As String _
                                   , ByVal strDebitBankName As String _
                                   , ByVal strDebitBankCode As String _
                                   , ByVal strDebitBankBranchSortCode As String _
                                   , ByVal strDebitCurrency As String _
                                   , ByVal strbatchSignature As String _
                                   , ByVal strEFTCustomCSVColumnsIds As String _
                                   , ByVal strFmtCurrency As String _
                                   , ByVal intPeriodId As Integer _
                                   , Optional ByVal strDateFormat As String = "ddMMyyyy" _
                                   ) As String
        'Hemant (07 Jul 2023) -- Start
        'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
        Dim objCompany As New clsCompany_Master
        'Hemant (07 July 2023) -- End
        Dim strJSON As String = String.Empty

        Try

            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            Dim strPeriodName As String = String.Empty
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPeriodId
            Dim dtPeriodStartDate As Date = objPeriod._Start_Date
            strPeriodName = objPeriod._Period_Name
            objPeriod = Nothing
            Dim intProcessCount As Integer = strBatchId.ToString.Split("-1").Length
            Dim strProcessCode As String
            If intProcessCount.ToString.Length < 2 Then
                strProcessCode = "0" & intProcessCount.ToString
            Else
                strProcessCode = intProcessCount.ToString
            End If
            'Hemant (21 Jul 2023) -- End
            'Hemant (07 Jul 2023) -- Start
            'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
            objCompany._Companyunkid = Company._Object._Companyunkid
            'Dim decTotalAmount As Decimal = (From p In drBatchData.CopyToDataTable Select (CDec(p.Item("Amount")))).Sum()
            Dim decTotalAmount As Decimal = 0
            'Hemant (07 July 2023) -- End
            Dim arrList As New ArrayList(strEFTCustomCSVColumnsIds.Split(CChar(",")))

            strJSON = "{"
            strJSON &= """batchId"":""" & strBatchId & """"
            strJSON &= ",""batchCount"":""" & intBatchCount & """"
            strJSON &= ",""organizationCode"":""" & strOrganizationCode & """"
            strJSON &= ",""batchTimestamp"":""" & strbatchTimestamp & """"
            'Hemant (07 Jul 2023) -- Start
            'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
            'strJSON &= ",""batchAmount"":""" & Format(decTotalAmount, "#########################0.#0") & """"
            strJSON &= ",""batchNarration"":""" & strPeriodName & " " & Language.getMessage(mstrModuleName, 3, "Salary payment") & """"
            'Hemant (07 July 2023) -- End
            Dim ds As DataSet = (New clsMasterData).GetComboListForEFTCustomColumns("List", False)
            Dim dic As New Dictionary(Of Integer, String)
            If ds.Tables(0).Rows.Count > 0 Then
                dic = (From p In ds.Tables(0).AsEnumerable() Select New With {Key .id = CInt(p.Item("id")), .Name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(y) y.Name)
            End If

            strJSON &= ",""batchData"": ["
            Dim intCount As Integer = 1
            Dim strAllEmployeesJSON As String = String.Empty
            For Each drEmployeeBatch In drBatchData
                Dim stCurrentEmployeeJSON As String = String.Empty
                Dim stCurrentEmployeeData As String = String.Empty
                stCurrentEmployeeJSON &= "{"
                For Each id As String In arrList
                    Select Case CInt(id)
                        Case enEFT_EFT_Custom_Columns.COMP_BANK_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_BGCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_NAME
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_BankName").ToString & """"

                        Case enEFT_EFT_Custom_Columns.COMP_BRANCH_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_BRCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.COMP_BRANCH_NAME
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_BranchName").ToString & """"

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_SORT_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_SortCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_SWIFT_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_SwiftCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.COMP_BANK_ACCOUNT_NO
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("CMP_Account").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_BGCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_NAME
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_BankName").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BRANCH_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_BRCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BRANCH_NAME
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_BranchName").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_SORT_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_SortCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_SWIFT_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_SwiftCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_ACCOUNT_NO
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EMP_AccountNo").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMPLOYEE_CODE
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EmpCode").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMPLOYEE_NAME
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("EmpName").ToString & """"

                        Case enEFT_EFT_Custom_Columns.EMP_BANK_AMOUNT
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & Format(drEmployeeBatch.Item("Amount"), strFmtCurrency).Replace(",", "") & """"

                        Case enEFT_EFT_Custom_Columns.DESCRIPTION
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & strPeriodName & " " & Language.getMessage(mstrModuleName, 2, "Salary") & """"

                        Case enEFT_EFT_Custom_Columns.EMP_MEMBERSHIP_NO
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("Membershipno").ToString & """"

                        Case enEFT_EFT_Custom_Columns.PAYMENT_DATE
                            Dim dtDate As DateTime
                            If DateTime.TryParseExact(drEmployeeBatch.Item("paymentdate").ToString, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, dtDate) Then
                                stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & dtDate.ToString(strDateFormat) & """"
                            End If

                        Case enEFT_EFT_Custom_Columns.PAYMENT_CURRENCY
                            stCurrentEmployeeData &= ",""" & dic.Item(CInt(id)) & """:""" & drEmployeeBatch.Item("currency_sign").ToString & """"

                    End Select
                Next
                If stCurrentEmployeeData.Trim.Length > 0 Then
                    stCurrentEmployeeData = stCurrentEmployeeData.Substring(1)
                End If

                Dim strCount As String = String.Empty
                If intCount.ToString.Trim.Length = 1 Then
                    strCount = "00000" & intCount.ToString
                ElseIf intCount.ToString.Trim.Length = 2 Then
                    strCount = "0000" & intCount.ToString
                ElseIf intCount.ToString.Trim.Length = 3 Then
                    strCount = "000" & intCount.ToString
                ElseIf intCount.ToString.Trim.Length = 4 Then
                    strCount = "00" & intCount.ToString
                ElseIf intCount.ToString.Trim.Length = 5 Then
                    strCount = "0" & intCount.ToString
                Else
                    strCount = intCount.ToString
                End If

                'Hemant (07 Jul 2023) -- Start
                'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
                'Hemant (21 Jul 2023) -- Start
                'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
                If drEmployeeBatch.Item("EMP_SwiftCode").ToString.Trim.ToUpper.Contains("NLCBTZ") = True Then
                    stCurrentEmployeeData &= ",""transactionType"":""INTERNAL"""
                Else
                    'Hemant (21 Jul 2023) -- End
                stCurrentEmployeeData &= ",""transactionType"":""EFT"""
                End If 'Hemant (21 Jul 2023)
                stCurrentEmployeeData &= ",""chargeType"":""O"""
                stCurrentEmployeeData &= ",""postalAddress"":""" & objCompany._Address1 & """"
                stCurrentEmployeeData &= ",""physicalAddress"":""" & objCompany._Address2 & """"
                stCurrentEmployeeData &= ",""clientReference"":""" & dtPeriodStartDate.ToString("yyMM") & drEmployeeBatch.Item("voucherno").ToString & strProcessCode & drEmployeeBatch.Item("EmpCode").ToString & strCount.ToString & """"
                stCurrentEmployeeData &= ",""email"":""" & objCompany._Email & """"
                stCurrentEmployeeData &= ",""phoneNumber"":""" & objCompany._Phone1 & """"
                'Hemant (07 July 2023) -- End

                stCurrentEmployeeJSON &= stCurrentEmployeeData

                stCurrentEmployeeJSON &= "}"

                If strAllEmployeesJSON.Trim.Length > 0 Then
                    strAllEmployeesJSON = "," & stCurrentEmployeeJSON
                Else
                    strAllEmployeesJSON = stCurrentEmployeeJSON
                End If

                strJSON &= strAllEmployeesJSON

                'Hemant (21 Jul 2023) -- Start
                'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
                drEmployeeBatch.Item("clientreference") = dtPeriodStartDate.ToString("yyMM") & drEmployeeBatch.Item("voucherno").ToString & strProcessCode & drEmployeeBatch.Item("EmpCode").ToString & strCount.ToString
                drEmployeeBatch.Item("status") = ""
                'Hemant (21 Jul 2023) -- End

                'Hemant (07 Jul 2023) -- Start
                'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
                intCount = intCount + 1
                'Hemant (07 July 2023) -- End
                decTotalAmount = decTotalAmount + Format(drEmployeeBatch.Item("Amount"), strFmtCurrency)
            Next
            strJSON &= "]"
            strJSON &= ",""companyBankDetails"" : {"
            strJSON &= """debitAccount"":""" & strDebitAccountNumber & """"
            strJSON &= ",""bankName"":""" & strDebitBankName & """"
            strJSON &= ",""bankCode"":""" & strDebitBankCode & """"
            strJSON &= ",""bankBranchSortCode"":""" & strDebitBankBranchSortCode & """"
            strJSON &= ",""debitCurrency"":""" & strDebitCurrency & """"
            strJSON &= ",""debitAccountName"":""TRA Salary Account"""
            strJSON &= "}"
            strJSON &= ",""batchAmount"":""" & Format(decTotalAmount, "#########################0.#0") & """"
            strJSON &= ",""batchSignature"":""" & strbatchSignature & """"
            strJSON &= "}"

            Return strJSON
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GenarateJSONGenericEFT", mstrModuleName)
            'Hemant (07 Jul 2023) -- Start
            'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
        Finally
            objCompany = Nothing
            'Hemant (07 July 2023) -- End
        End Try
    End Function

    Public Function SignData(ByVal StringToSign As String) As String
        Try

            Dim rsa As RSACryptoServiceProvider = New RSACryptoServiceProvider()
            rsa.FromXmlString("<RSAKeyValue><Modulus>7W+lAXsQXX4bIITjBLeGGzKIaOIWKLyMItGNYs3ozTvemyBigsTRKJToBhwsx5p+fDsHOGmFPyL1z8P2k3lPUM6Yg28qKl1h9WxOz1W8t4/WMBTCEu9h04ldspJSgMOynovjeaerj7bpge/ffpS8/LRA0j0ZvRaMs7DkfvzCsGby7aaeXm3uglvGhA2p0Gl/TNxfFNo+9eWnjN3Dg/7qXNwYjlYbFVMA/SZ5xJDY+F5YzjVDpKq6XVpB+9H5iD5lv04VLveWk+EkJct8oq+WmgNEqZzIWzmadgv++B0VtxiPnkpzeqRmWoZAtAT1IeevyX0y/KKXKKJYam7CFwETCQ==</Modulus><Exponent>AQAB</Exponent><P>+ZpHDGtbuc03Nl5Dop96bi8M6VMiR4n7wye97cTn76tQ6iNbioUnlmmdUXllognllUmk6h+sD92wuQbMpbkgh5WZmodcrY/N2acafKBrrAS5JwgN2WioxoHyIqJa9Z9B7GfxTLNTXfoz7CEoxDUJD1QHsoWFDjDUqzyMFxdZ7e0=</P><Q>84WJ27U7gNLTpFFTLwMKidHuroWx7PkyWVwyx2qxki1mmGYHf+13jlSCdT7Jz7d8h3XAykrDsHElTus6jRV2PSFfs4g4u1rsrqmgHYa2jrAGvX8U9LfnjoYs9v6alu74Z90HCLTBdJ5Hea8yumEMMiKkDxp/prpiItBplMRANg0=</Q><DP>TLJ1ZoGOu/ctIg2xJsVub3ERvJiJDgZ+UCdkGy3IP0MbJ/cZZ+Umlvd5GdH9wt7bpxXsEO0OiAmNBi3qsHnEXyU+/9bcSZDIpjrMzsLUkxUYd7/n0YhxZB4F81KENLltHmGKKhFoapY5YjOGPVQ2pnkhrF+O1R94Ge4O9gF85rk=</DP><DQ>me8Y1LQ8F9OtCxqJPZdrivEUMme6r/RaGliIlLvh4WgniUA9j2U5hNPw31JAWbg/1JTfuEAIcTkkfz18doBRjJTTHPaH/g6cvE/nMaLdNVcZ+6EgSw0RJ2uzcrJAYBZRGb6C2sL/4srGnancpCoCfpKdKBr1BByfOiiKBQsFF+U=</DQ><InverseQ>ywk/7sgD8R/8QWqnUF2fmXLgEUnSWh2Y6bKFbiDffnH1+AZdp3x8s4tocyu+RXFUt+IXHWKP5sxTNyjYSJ0YGxGeUSwjhn87t0G6L2/KGOe6ihtfXEOgvlngVbUVTCtdJYrXD50+/pvBxOoWB9Yxsve1NYJfhr5V3YLZJg97jL4=</InverseQ><D>aV6Of6W5kYQRTdErXkCDxzYZy1HqO5HRLvKIKDzw/4N+OqGYlif6GmRaw7tlM/+f+knH3oUVmPtO0zFIEBJZ3KaSkGGY+MwQWPYD04ddBKlUiGnt5rFNXK8tYb4F1xcCAdJa1PZP8Ktf3UYyjN49MHhd++8ZqQyEzInIHYLWc6mhdcNN6QuLMPhwcA19yoAMeuP6Bu03XcmObhulrFaEl4AVA4VK8KmZKG80rl3bjE9L59a83T6nEiIkD4xpvKJjmGrnI0pSsSptJZUkrWgCTDAmTaaMIdRU93+WF6N3/ycAd5c1uU72Z7qhgT8HWO2+AArqoAuBB3jeIrVF5H69wQ==</D></RSAKeyValue>")
            Dim originalString As String = StringToSign
            Dim signedData As Byte() = rsa.SignData(System.Text.Encoding.UTF8.GetBytes(originalString), CryptoConfig.MapNameToOID("SHA256"))
            Return Convert.ToBase64String(signedData)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SignData", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmPaymentBatchPosting_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPaymentBatchPosting_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Button Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnApproveReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApproveReject.Click
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Dim blnFlag As Boolean = False
        Dim objEmpBankTran As New clsEmployeeBanks
        Dim objEmpSalaryTran As New clsEmpSalaryTran
        Dim objGarnisheesBankTran As New clsGarnishees_Bank_Tran
        Try
            If IsValid() = False Then Exit Sub

            Dim xPeriodStart As Date
            Dim xPeriodEnd As Date
            If CInt(cboPeriod.SelectedValue) > 0 Then
                xPeriodStart = mdtPeriod_startdate
                xPeriodEnd = mdtPeriod_enddate
            Else
                xPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                xPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If

            Dim dsEmpBankList As DataSet = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, xPeriodStart, xPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", , "", , mdtPeriod_enddate, "BankGrp, BranchName, EmpName, end_date DESC", "")
            Dim dsEmpSalaryList As DataSet = objEmpSalaryTran.GetSalaryPaymentData(CInt(cboPeriod.SelectedValue))
            Dim dsGarnisheeBankList As DataSet = objGarnisheesBankTran.GetList(xPeriodEnd, mdtPeriod_enddate)
            objPaymentBatchPostingMaster._PaymentBatchPostingunkid = CInt(cboBatch.SelectedValue)
            SetValue(objPaymentBatchPostingMaster)
            blnFlag = objPaymentBatchPostingMaster.UpdateApprovalStatus(CType(dgvDataList.DataSource, DataTable), dsEmpBankList, dsEmpSalaryList, dsGarnisheeBankList, CInt(cboPeriod.SelectedValue), ConfigParameter._Object._CurrentDateAndTime)

            If blnFlag = False And objPaymentBatchPostingMaster._Message <> "" Then
                eZeeMsgBox.Show(objPaymentBatchPostingMaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag = True Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnApproveReject_Click", mstrModuleName)
        Finally
            objPaymentBatchPostingMaster = Nothing
            objEmpBankTran = Nothing
            objEmpSalaryTran = Nothing
        End Try
    End Sub

    Private Sub btnSendRequest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendRequest.Click
        Dim objBankPaymentList As New clsBankPaymentList(User._Object._Languageunkid, Company._Object._Companyunkid)
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsCustomColumnsList As DataSet
        Dim strEFTCustomColumnsIds As String = String.Empty
        Dim strDateFormat As String = String.Empty
        Dim intEFTMembershipUnkId As Integer = 0
        Dim strVoucherNo As String = String.Empty
        Dim strError As String = ""
        Dim strPostedData As String = ""
        Dim strResponseData As String = ""
        Try
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            If (New clspaymentbatchreconciliation_tran).GetList("List", cboBatch.Text).Tables(0).Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Selected Batch is already Posted."), enMsgBoxStyle.Information)
                cboBatch.Focus()
                Exit Sub
            End If
            'Hemant (21 Jul 2023) -- End

            dsCustomColumnsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList, enEFT_Export_Mode.CSV, 2)
            Dim drCustomColumnsList() As DataRow = dsCustomColumnsList.Tables(0).Select("headtypeid = 0 ")
            If drCustomColumnsList.Length > 0 Then
                strEFTCustomColumnsIds = drCustomColumnsList(0).Item("transactionheadid").ToString()
            End If
            Dim drDateFormat() As DataRow = dsCustomColumnsList.Tables(0).Select("headtypeid = 5 ")
            If drDateFormat.Length > 0 Then
                strDateFormat = drDateFormat(0).Item("transactionheadid").ToString()
            End If

            Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
            If CInt(cboBatch.SelectedValue) > 0 Then
                objPaymentBatchPostingMaster._PaymentBatchPostingunkid = CInt(cboBatch.SelectedValue)
                strVoucherNo = objPaymentBatchPostingMaster._VoucherNo
            End If
            objPaymentBatchPostingMaster = Nothing

            Dim strBatchEmployeelist As String = String.Join(",", (From p In CType(dgvDataList.DataSource, DataTable) Where CInt(p.Item("dpndtbeneficetranunkid")) <= 0 Select (p.Item("employeeunkid").ToString)).ToArray())
            'Hemant (07 Jul 2023) -- Start
            'Enhancement(NMB) : A1X-1120 : Batch Payload changes to include additional parameters
            Dim strDpndtBeneficeTranUnkIDs As String = String.Join(",", (From p In CType(dgvDataList.DataSource, DataTable) Where CInt(p.Item("dpndtbeneficetranunkid")) > 0 Select (p.Item("dpndtbeneficetranunkid").ToString)).ToArray())

            If strBatchEmployeelist.Trim.Length <= 0 Then strBatchEmployeelist = "-1"
            If strDpndtBeneficeTranUnkIDs.Trim.Length <= 0 Then strDpndtBeneficeTranUnkIDs = "-1"
            'Hemant (07 July 2023) -- End
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            objBankPaymentList._PeriodId = CStr(cboPeriod.SelectedValue)
            objBankPaymentList._BankId = mintBankId
            objBankPaymentList._BranchId = mintBranchId
            objBankPaymentList._CompanyBranchId = mintCompanyBranchId
            objBankPaymentList._CompanyBankAccountNo = mstrCompanyBankAccountNo
            objBankPaymentList._CurrencyId = mintCurrencyId
            objBankPaymentList._ReportModeId = mintReportModeId
            Dim dsGenericCSVPostWeb As DataSet = objBankPaymentList.Get_EFT_Generic_CSV_Post_Data(FinancialYear._Object._DatabaseName, _
                                                                                                  User._Object._Userunkid, _
                                                                                                  FinancialYear._Object._YearUnkid, _
                                                                                                  Company._Object._Companyunkid, _
                                                                                                  objPeriod._Start_Date, _
                                                                                                  objPeriod._End_Date, _
                                                                                                  ConfigParameter._Object._UserAccessModeSetting, _
                                                                                                  True, True, _
                                                                                                  True, _
                                                                                                  ConfigParameter._Object._CurrencyFormat, _
                                                                                                  intEFTMembershipUnkId, _
                                                                                                  strBatchEmployeelist, _
                                                                                                  "", _
                                                                                                  strDpndtBeneficeTranUnkIDs _
                                                                                                  )

            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            For Each drMobileMoneyData As DataRow In CType(dgvDataList.DataSource, DataTable).Select("employeeunkid = -1")
                Dim dsRenciliationData As DataSet = (New clspaymentbatchreconciliation_tran).GetList("List", strVoucherNo)
                Dim decTotalAmount As Decimal = (From p In dsRenciliationData.Tables(0) Where CInt(p.Item("employeeunkid")) = -1 Select (CDec(p.Item("Amount")))).Sum()
                If decTotalAmount > 0 AndAlso CInt(ConfigParameter._Object._EFTMobileMoneyBank) > 0 AndAlso CInt(ConfigParameter._Object._EFTMobileMoneyBranch) > 0 AndAlso ConfigParameter._Object._EFTMobileMoneyAccountName.Trim.Length > 0 AndAlso ConfigParameter._Object._EFTMobileMoneyAccountNo.Trim.Length > 0 Then
                    Dim objPayrollGroupMaster As New clspayrollgroup_master
                    Dim objBankbBranchMaster As New clsbankbranch_master
                    Dim strEMP_BGCode As String = "", strEMP_BankName As String = "", strEMP_BRCode As String = "", strEMP_SwiftCode As String = "", strEMP_BranchName As String = "", strEMP_SortCode As String = ""
                    objPayrollGroupMaster._Groupmasterunkid = CInt(ConfigParameter._Object._EFTMobileMoneyBank)
                    strEMP_BGCode = objPayrollGroupMaster._Groupcode
                    strEMP_BankName = objPayrollGroupMaster._Groupname
                    strEMP_SwiftCode = objPayrollGroupMaster._Swiftcode
                    objBankbBranchMaster._Branchunkid = CInt(ConfigParameter._Object._EFTMobileMoneyBranch)
                    strEMP_BRCode = objBankbBranchMaster._Branchcode
                    strEMP_BranchName = objBankbBranchMaster._Branchname
                    strEMP_SortCode = objBankbBranchMaster._Sortcode
                    Dim strCMP_BGID As String = "", strCMP_BRID As String = "", strCMP_BGCODE As String = "", strCMP_BankName As String = "", strCMP_BRCode As String = "", strCMP_BranchName As String = "", strCMP_Account As String = "", strCMP_SortCode = "", strCMP_SwiftCode = ""
                    objBankbBranchMaster._Branchunkid = mintCompanyBranchId
                    strCMP_BRCode = objBankbBranchMaster._Branchcode
                    strCMP_BranchName = objBankbBranchMaster._Branchname
                    strCMP_SortCode = objBankbBranchMaster._Sortcode
                    objPayrollGroupMaster._Groupmasterunkid = CInt(objBankbBranchMaster._Bankgroupunkid)
                    strCMP_BGID = objPayrollGroupMaster._Groupmasterunkid
                    strCMP_BGCODE = objPayrollGroupMaster._Groupcode
                    strCMP_BankName = objPayrollGroupMaster._Groupname
                    strCMP_SwiftCode = objPayrollGroupMaster._Swiftcode
                    Dim drMobileMoney As DataRow = dsGenericCSVPostWeb.Tables(0).NewRow
                    drMobileMoney.Item("employeeunkid") = "-1"
                    drMobileMoney.Item("EmpCode") = "000000"
                    drMobileMoney.Item("EmpName") = ConfigParameter._Object._EFTMobileMoneyAccountName.ToString
                    drMobileMoney.Item("EMP_BGCode") = strEMP_BGCode
                    drMobileMoney.Item("EMP_BankName") = strEMP_BankName
                    drMobileMoney.Item("EMP_BRCode") = strEMP_BRCode
                    drMobileMoney.Item("EMP_SwiftCode") = strEMP_SwiftCode
                    drMobileMoney.Item("EMP_BranchName") = strEMP_BranchName
                    drMobileMoney.Item("EMP_SortCode") = strEMP_SortCode
                    drMobileMoney.Item("EMP_AccountNo") = ConfigParameter._Object._EFTMobileMoneyAccountNo.ToString
                    drMobileMoney.Item("EMP_BGID") = CInt(ConfigParameter._Object._EFTMobileMoneyBank)
                    drMobileMoney.Item("EMP_BRID") = CInt(ConfigParameter._Object._EFTMobileMoneyBranch)
                    drMobileMoney.Item("currency_sign") = mstrCurrency_Sign
                    drMobileMoney.Item("Amount") = Format(decTotalAmount, ConfigParameter._Object._CurrencyFormat).Replace(",", "")
                    drMobileMoney.Item("CMP_BGID") = strCMP_BGID
                    drMobileMoney.Item("CMP_BRID") = mintCompanyBranchId
                    drMobileMoney.Item("CMP_BGCODE") = strCMP_BGCODE
                    drMobileMoney.Item("CMP_BankName") = strCMP_BankName
                    drMobileMoney.Item("CMP_BRCode") = strCMP_BRCode
                    drMobileMoney.Item("CMP_BranchName") = strCMP_BranchName
                    drMobileMoney.Item("CMP_Account") = mstrCompanyBankAccountNo
                    drMobileMoney.Item("CMP_SortCode") = strCMP_SortCode
                    drMobileMoney.Item("CMP_SwiftCode") = strCMP_SwiftCode
                    drMobileMoney.Item("voucherno") = strVoucherNo
                    drMobileMoney.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                    drMobileMoney.Item("paidcurrencyid") = dsRenciliationData.Tables(0).Rows(0).Item("paidcurrencyid")
                    drMobileMoney.Item("payment_typeid") = CInt(enBankPaymentType.BankAccount)
                    drMobileMoney.Item("dpndtbeneficetranunkid") = 0
                    dsGenericCSVPostWeb.Tables(0).Rows.Add(drMobileMoney)
                    dsGenericCSVPostWeb.Tables(0).AcceptChanges()
                    objPayrollGroupMaster = Nothing
                    objBankbBranchMaster = Nothing
                End If
                Dim dtBatch As DataTable = dsGenericCSVPostWeb.Tables(0).Select("payment_typeid = " & CInt(enBankPaymentType.BankAccount) & " ").CopyToDataTable
                dsGenericCSVPostWeb.Tables.RemoveAt(0)
                dsGenericCSVPostWeb.Tables.Add(dtBatch)
                Exit For
            Next
            'Hemant (21 Jul 2023) -- End


            'Hemant (07 Jul 2023) -- [strDpndtBeneficeTranUnkIDs]
            Dim dtvoucherno As DataTable = dsGenericCSVPostWeb.Tables(0).DefaultView.ToTable(True, "voucherno")
            For Each drvoucher As DataRow In dtvoucherno.Rows
                Dim strbatchSignature As String = String.Empty

                Dim drVoucherData() As DataRow = dsGenericCSVPostWeb.Tables(0).Select("voucherno = '" & drvoucher.Item("voucherno").ToString() & "'")
                Dim drBatchData() As DataRow = CType(dgvDataList.DataSource, DataTable).Select("1=1")
                strbatchSignature = ConfigParameter._Object._EFTRequestClientID & drBatchData(0).Item("batchname").ToString() & drVoucherData.Length & dsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_Account").ToString() & dsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BGCode").ToString() & CDate(ConfigParameter._Object._CurrentDateAndTime).ToString("yyyyMMdd")
                Dim strJSON As String = GenarateJSONGenericEFT(drBatchData(0).Item("batchname").ToString(), drVoucherData.Length, Company._Object._Code, CDate(ConfigParameter._Object._CurrentDateAndTime).ToString("yyyyMMdd"), _
                                                              drVoucherData, dsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_Account").ToString(), dsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BankName").ToString(), dsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_BGCode").ToString(), _
                                                              dsGenericCSVPostWeb.Tables(0).Rows(0).Item("CMP_SortCode").ToString(), dsGenericCSVPostWeb.Tables(0).Rows(0).Item("currency_sign").ToString(), SignData(strbatchSignature), strEFTCustomColumnsIds, ConfigParameter._Object._CurrencyFormat, _
                                                              CInt(cboPeriod.SelectedValue), strDateFormat)
                'Hemant (07 Jul 2023) -- [objPeriod._Period_Code]
                Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(ConfigParameter._Object._EFTRequestClientID & ":" & ConfigParameter._Object._EFTRequestPassword)
                Dim strBase64Credential As String = Convert.ToBase64String(byt)

                Dim strMessage = objBankPaymentList.GetSetPaymentAPIRequest(ConfigParameter._Object._EFTRequestURL, strBase64Credential, strJSON, strError, strPostedData)

                If strMessage IsNot Nothing Then
                    eZeeMsgBox.Show(" Request Response : " & strMessage("statusCode").ToString() & " - " & strMessage("message").ToString(), enMsgBoxStyle.Information)

                    If strMessage("statusCode").ToString() = "4000" Then
                        Dim objPaymentBatchReconciliationTran As New clspaymentbatchreconciliation_tran
                        objPaymentBatchReconciliationTran._TranDataTable = drVoucherData.CopyToDataTable
                        objPaymentBatchReconciliationTran._BatchName = drBatchData(0).Item("batchname").ToString()
                        objPaymentBatchReconciliationTran._BatchPostedDateTime = ConfigParameter._Object._CurrentDateAndTime
                        objPaymentBatchReconciliationTran._Isvoid = False
                        objPaymentBatchReconciliationTran._Voiduserunkid = -1
                        objPaymentBatchReconciliationTran._Voiddatetime = Nothing
                        objPaymentBatchReconciliationTran._Voidreason = ""
                        If objPaymentBatchReconciliationTran.InsertAll() = False And objPaymentBatchReconciliationTran._Message <> "" Then
                            eZeeMsgBox.Show(objPaymentBatchReconciliationTran._Message, enMsgBoxStyle.Information)
                        End If
                        objPaymentBatchReconciliationTran = Nothing
                    End If
                End If
            Next

            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSendRequest_Click", mstrModuleName)
        Finally
            objBankPaymentList = Nothing
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, _
                                                                                                                   cboStatus.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objPaymentBatchPostingMaster As New clspaymentbatchposting_master
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

                mdtPeriod_startdate = objPeriod._Start_Date
                mdtPeriod_enddate = objPeriod._End_Date
            End If
            dsCombos = objPaymentBatchPostingMaster.GetBatchNameList("List", CInt(cboPeriod.SelectedValue), CInt(cboStatus.SelectedValue), False, True, , FinancialYear._Object._DatabaseName)
            With cboBatch
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dgvDataList.DataSource = Nothing
            btnApproveReject.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
            objPaymentBatchPostingMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub Combo_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cbo As ComboBox = TryCast(sender, ComboBox)
            If cbo IsNot Nothing Then
                If dgvDataList.CurrentCell.ColumnIndex = dgcolhBranch.Index Then
                    'dgvDataList.CurrentRow.Cells(objdgcolhBranchUnkid.Index).Value = 0
                    'Dim dsCombos As New DataSet
                    'Dim objBranch As New clsbankbranch_master

                    'dsCombos = objBranch.getListForCombo("Branch", True, CInt(cbo.SelectedValue))
                    'With dgcolhBranch
                    '    .ValueMember = "branchunkid"
                    '    .DisplayMember = "name"
                    '    .DataSource = dsCombos.Tables("Branch")
                    '    .DataPropertyName = "branchunkid"
                    '    .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox

                    'End With
                    'objdgcolhBranchUnkid.DataPropertyName = "branchunkid"

                    'objBranch = Nothing
                    Dim intBankGroupid As Integer = -1
                    Dim intBranchUnkid As Integer = CInt(cbo.SelectedValue)
                    Dim objBranch As New clsbankbranch_master
                    Dim objBankGroup As New clspayrollgroup_master
                    objBranch._Branchunkid = intBranchUnkid
                    intBankGroupid = CInt(objBranch._Bankgroupunkid)
                    objBankGroup._Groupmasterunkid = intBankGroupid
                    dgvDataList.CurrentRow.Cells(dgcolhBankGroup.Index).Value = objBankGroup._Groupname


                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Combo_SelectionChangeCommitted", mstrModuleName)
        End Try
    End Sub


#End Region

#Region " DataGrid Events "

    Private Sub dgvDataList_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDataList.EditingControlShowing
        Try
            Select Case dgvDataList.CurrentCell.ColumnIndex
                Case dgcolhBranch.Index
                    If e.Control IsNot Nothing Then
                        Dim cbo As ComboBox = TryCast(e.Control, ComboBox)
                        'RemoveHandler cbo.SelectionChangeCommitted, AddressOf Combo_SelectionChangeCommitted
                        AddHandler cbo.SelectionChangeCommitted, AddressOf Combo_SelectionChangeCommitted
                    End If

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDataList_EditingControlShowing", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgvDataList_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvDataList.DataError

    End Sub

    Private Sub dgvDataList_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataList.CellEnter

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnApproveReject.GradientBackColor = GUI._ButttonBackColor
			Me.btnApproveReject.GradientForeColor = GUI._ButttonFontColor

			Me.btnSendRequest.GradientBackColor = GUI._ButttonBackColor
			Me.btnSendRequest.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
			Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
			Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
			Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
			Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.DataGridViewTextBoxColumn18.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn18.Name, Me.DataGridViewTextBoxColumn18.HeaderText)
			Me.DataGridViewTextBoxColumn19.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn19.Name, Me.DataGridViewTextBoxColumn19.HeaderText)
			Me.DataGridViewTextBoxColumn20.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn20.Name, Me.DataGridViewTextBoxColumn20.HeaderText)
			Me.DataGridViewTextBoxColumn21.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn21.Name, Me.DataGridViewTextBoxColumn21.HeaderText)
			Me.DataGridViewTextBoxColumn17.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn17.Name, Me.DataGridViewTextBoxColumn17.HeaderText)
			Me.DataGridViewTextBoxColumn16.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn16.Name, Me.DataGridViewTextBoxColumn16.HeaderText)
			Me.DataGridViewTextBoxColumn12.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn12.Name, Me.DataGridViewTextBoxColumn12.HeaderText)
			Me.DataGridViewTextBoxColumn13.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn13.Name, Me.DataGridViewTextBoxColumn13.HeaderText)
			Me.DataGridViewTextBoxColumn14.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn14.Name, Me.DataGridViewTextBoxColumn14.HeaderText)
			Me.DataGridViewTextBoxColumn15.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn15.Name, Me.DataGridViewTextBoxColumn15.HeaderText)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnApproveReject.Text = Language._Object.getCaption(Me.btnApproveReject.Name, Me.btnApproveReject.Text)
			Me.DataGridViewTextBoxColumn22.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn22.Name, Me.DataGridViewTextBoxColumn22.HeaderText)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblBatch.Text = Language._Object.getCaption(Me.lblBatch.Name, Me.lblBatch.Text)
			Me.btnSendRequest.Text = Language._Object.getCaption(Me.btnSendRequest.Name, Me.btnSendRequest.Text)
			Me.dgcolhBatchId.HeaderText = Language._Object.getCaption(Me.dgcolhBatchId.Name, Me.dgcolhBatchId.HeaderText)
			Me.dgcolhEmployeeCode.HeaderText = Language._Object.getCaption(Me.dgcolhEmployeeCode.Name, Me.dgcolhEmployeeCode.HeaderText)
			Me.dgcolhBankGroup.HeaderText = Language._Object.getCaption(Me.dgcolhBankGroup.Name, Me.dgcolhBankGroup.HeaderText)
			Me.dgcolhBranch.HeaderText = Language._Object.getCaption(Me.dgcolhBranch.Name, Me.dgcolhBranch.HeaderText)
			Me.dgcolhAccountNo.HeaderText = Language._Object.getCaption(Me.dgcolhAccountNo.Name, Me.dgcolhAccountNo.HeaderText)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Atleast one Employee in the list is required for Approval.")
            Language.setMessage(mstrModuleName, 2, "Salary")
            Language.setMessage(mstrModuleName, 3, "Salary payment")
            Language.setMessage(mstrModuleName, 4, "Sorry, Selected Batch is already Posted.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class