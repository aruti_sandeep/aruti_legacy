﻿'************************************************************************************************************************************
'Class Name : clsFlexcubeLoanOfferLetter.vb
'Purpose    :
'Date       : 25/11/2022
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsFlexcubeLoanOfferLetter
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsFlexcubeLoanOfferLetter"
    Private mstrReportId As String = enArutiReport.Flexcube_Loan_Offer_Letter
    Dim objDataOperation As clsDataOperation

#Region "Enum"
    Enum enLoanType
        General_Loan = 1
        Car_Loan = 2
        Refinancing_Mortgage_Loan = 3
        Purchase_Mortgage_Loan = 4
        Construction_Mortgage_Loan = 5
    End Enum
#End Region

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintLoanTypeId As Integer = 0
    Private mstrLoanTypeName As String = ""

    Private mintEmployeeUnkId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintLoanSchemeUnkId As Integer = 0
    Private mstrLoanSchemeName As String = ""
    Private mintProcessPendingLoanUnkid As Integer = 0
    Private mstrApplicationNo As String = ""

    Private mstrOrderByQuery As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
#End Region

#Region " Properties "

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _LoanTypeId() As Integer
        Set(ByVal value As Integer)
            mintLoanTypeId = value
        End Set
    End Property

    Public WriteOnly Property _LoanTypeName() As String
        Set(ByVal value As String)
            mstrLoanTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeUnkId() As Integer
        Set(ByVal value As Integer)
            mintLoanSchemeUnkId = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeName() As String
        Set(ByVal value As String)
            mstrLoanSchemeName = value
        End Set
    End Property

    Public WriteOnly Property _ProcessPendingLoanUnkid() As Integer
        Set(ByVal value As Integer)
            mintProcessPendingLoanUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ApplicationNo() As String
        Set(ByVal value As String)
            mstrApplicationNo = value
        End Set
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintLoanTypeId = 0
            mstrLoanTypeName = ""

            mintEmployeeUnkId = 0
            mstrEmployeeName = ""
            mintLoanSchemeUnkId = 0
            mstrLoanSchemeName = ""
            mintProcessPendingLoanUnkid = 0
            mstrApplicationNo = ""

            mstrOrderByQuery = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        mstrOrderByQuery = ""

        Try

            If mintEmployeeUnkId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkId)
                Me._FilterQuery &= " AND lnloan_process_pending_loan.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintLoanSchemeUnkId > 0 Then
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeUnkId)
                Me._FilterQuery &= " AND  lnloan_process_pending_loan.loanschemeunkid = @loanschemeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Loan Scheme :") & " " & mstrLoanSchemeName & " "
            End If

            If mintProcessPendingLoanUnkid > 0 Then
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessPendingLoanUnkid)
                Me._FilterQuery &= " AND lnloan_process_pending_loan.processpendingloanunkid = @processpendingloanunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "Application No :") & " " & mstrApplicationNo & " "
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function NumberInWords(ByVal doubleNumber As Double) As String
        Dim strNumToWords As String
        Try
            Dim beforeFloatingPoint = CInt(Math.Floor(doubleNumber))
            Dim beforeFloatingPointWord As String = String.Empty
            If beforeFloatingPoint = 0 Then
                beforeFloatingPointWord = "zero"
            Else
                beforeFloatingPointWord = String.Format("{0}", NumberToString(beforeFloatingPoint.ToString))
            End If
            Dim afterFloatingPointWord As String = String.Empty
            If CInt(((doubleNumber - beforeFloatingPoint) * 100)) > 0 Then
                Dim strDecimalNumber As String = CInt(((doubleNumber - beforeFloatingPoint) * 100)).ToString
                Dim strFirstDecimal As String = String.Empty
                If strDecimalNumber.Length > 1 Then
                    strFirstDecimal = GroupToWords(CInt(strDecimalNumber.Substring(0, 1))) & " "
                Else
                    strFirstDecimal = "zero" & " "
                End If
                afterFloatingPointWord = strFirstDecimal
                Dim strSecondDecimal As String = String.Empty
                If strDecimalNumber.Length > 1 Then
                    strSecondDecimal = GroupToWords(CInt(strDecimalNumber.Substring(1, 1)))
                    afterFloatingPointWord &= strSecondDecimal
                Else
                    strSecondDecimal = GroupToWords(CInt(strDecimalNumber.Substring(0, 1)))
                    afterFloatingPointWord &= strSecondDecimal
                End If
            End If

            If CInt(((doubleNumber - beforeFloatingPoint) * 100)) > 0 Then
                Return String.Format("{0} POINT {1}", beforeFloatingPointWord, afterFloatingPointWord)
            Else
                Return String.Format("{0}", beforeFloatingPointWord)
            End If
        Catch ex As Exception
            strNumToWords = ""
        Finally

        End Try
        Return strNumToWords
    End Function

    Public Function NumberToWords(ByVal doubleNumber As Double) As String
        Dim beforeFloatingPoint = CInt(Math.Floor(doubleNumber))
        Dim beforeFloatingPointWord = String.Format("{0}", NumberToString(beforeFloatingPoint))
        Dim afterFloatingPointWord = String.Format("{0} Cents ", GroupToWords(CInt(((doubleNumber - beforeFloatingPoint) * 100))))

        If CInt(((doubleNumber - beforeFloatingPoint) * 100)) > 0 Then
            Return String.Format("{0} and {1}", beforeFloatingPointWord, afterFloatingPointWord)
        Else
            Return String.Format("{0}", beforeFloatingPointWord)
        End If
    End Function

    Public Function NumberToString(ByVal num_str As String) As String ', Optional ByVal use_us_group_names As Boolean = True
        ' Get the appropiate group names.
        Dim groups() As String
        'If use_us_group_names Then
        '    groups = New String() {"", "thousand", "million", "billion", "trillion", "quadrillion", "quintillion", "sextillion", "septillion", "octillion", "nonillion", "decillion", "undecillion", "duodecillion", "tredecillion", "quattuordecillion", "quindecillion", "sexdecillion", "septendecillion", "octodecillion", "novemdecillion", "vigintillion"}
        'Else
        groups = New String() {"", "Thousand", "Million", "Milliard", "Billion", "1000 Billion", "Trillion", "1000 Trillion", "Quadrillion", "1000 Quadrillion", "Quintillion", "1000 Quintillion", "Sextillion", "1000 Sextillion", "Septillion", "1000 Septillion", "Octillion", "1000 Octillion", "Nonillion", "1000 Nonillion", "Decillion", "1000 Decillion"}
        'End If

        ' Clean the string a bit.
        ' Remove "$", ",", leading zeros, and
        ' anything after a decimal point.
        Const CURRENCY As String = "$"
        Const SEPARATOR As String = ","
        Const DECIMAL_POINT As String = "."
        num_str = num_str.Replace(CURRENCY, "").Replace(SEPARATOR, "")
        num_str = num_str.TrimStart(New Char() {"0"c})
        Dim pos As Integer = num_str.IndexOf(DECIMAL_POINT)
        If pos = 0 Then
            Return "zero"
        ElseIf pos > 0 Then
            num_str = num_str.Substring(0, pos - 1)
        End If

        ' See how many groups there will be.
        Dim num_groups As Integer = (num_str.Length + 2) \ 3

        ' Pad so length is a multiple of 3.
        num_str = num_str.PadLeft(num_groups * 3, " "c)

        ' Process the groups, largest first.
        Dim result As String = ""
        Dim group_num As Integer
        For group_num = num_groups - 1 To 0 Step -1
            ' Get the next three digits.
            Dim group_str As String = num_str.Substring(0, 3)
            num_str = num_str.Substring(3)
            Dim group_value As Integer = CInt(group_str)

            ' Convert the group into words.
            If group_value > 0 Then
                If group_num >= groups.Length Then
                    result &= GroupToWords(group_value) & _
                        " ?, "
                Else
                    result &= GroupToWords(group_value) & _
                        " " & groups(group_num) & ", "
                End If
            End If
        Next group_num

        ' Remove the trailing ", ".
        If result.EndsWith(", ") Then
            result = result.Substring(0, result.Length - 2)
        End If

        Return result.Trim()
    End Function
    ' Convert a number between 0 and 999 into words.
    Private Function GroupToWords(ByVal num As Integer) As String
        Static one_to_nineteen() As String = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen"}
        Static multiples_of_ten() As String = {"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"}

        ' If the number is 0, return an empty string.
        If num = 0 Then Return ""

        ' Handle the hundreds digit.
        Dim digit As Integer
        Dim result As String = ""
        If num > 99 Then
            digit = num \ 100
            num = num Mod 100
            result = one_to_nineteen(digit) & " Hundred"
        End If

        ' If num = 0, we have hundreds only.
        If num = 0 Then Return result.Trim()

        ' See if the rest is less than 20.
        If num < 20 Then
            ' Look up the correct name.
            result &= " " & one_to_nineteen(num)
        Else
            ' Handle the tens digit.
            digit = num \ 10
            num = num Mod 10
            result &= " " & multiples_of_ten(digit - 2)

            ' Handle the final digit.
            If num > 0 Then
                result &= " " & one_to_nineteen(num)
            End If
        End If

        Return result.Trim()
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal xDatabaseName As String _
                                     , ByVal xUserUnkid As Integer _
                                     , ByVal xYearUnkid As Integer _
                                     , ByVal xCompanyUnkid As Integer _
                                     , ByVal xPeriodStart As Date _
                                     , ByVal xPeriodEnd As Date _
                                     , ByVal xUserModeSetting As String _
                                     , ByVal xOnlyApproved As Boolean _
                                     ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try


            Company._Object._Companyunkid = xCompanyUnkid
            ConfigParameter._Object._Companyunkid = xCompanyUnkid

            objDataOperation = New clsDataOperation

            objDataOperation.ClearParameters()


            StrQ = " SELECT " & _
                   "    application_no " & _
                   ",   lnloan_process_pending_loan.application_date " & _
                   ",   lnloan_process_pending_loan.employeeunkid " & _
                   ",   ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
                   ",   ISNULL(hremployee_master.firstname, '') AS EmpFirstName " & _
                   ",   ISNULL(hremployee_master.othername, '') AS EmpOtherName " & _
                   ",   ISNULL(hremployee_master.surname, '') AS EmpSurName " & _
                   ",   ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' +ISNULL(hremployee_master.surname, '') AS EmployeeFullName " & _
                   ",   hrclasses_master.name AS Class " & _
                   ",   hrsectiongroup_master.name AS SectionGroup " & _
                   ",   ISNULL(lnloan_scheme_master.name, '') AS LoanSchemeName " & _
                   ",   ISNULL(loan_amount, 0) AS loan_amount " & _
                   ",   ISNULL(lnloan_process_pending_loan.interest_rate, 0) AS interest_rate " & _
                   ",   ISNULL(lnloan_process_pending_loan.noofinstallment, 0) AS Tenure " & _
                   ",   ISNULL(lnloan_process_pending_loan.installmentamt, 0) AS InstallmentAmount " & _
                   ",   ISNULL(lnloan_scheme_master.name, '') AS LoanSchemeName " & _
                   ",   ISNULL(lnloan_process_pending_loan.model, '') AS model " & _
                   ",   ISNULL(lnloan_process_pending_loan.chassis_no, '') AS chassisno " & _
                   ",   ISNULL(lnloan_process_pending_loan.colour, '') AS colour " & _
                   ",   ISNULL(lnloan_process_pending_loan.yom, '') AS yom " & _
                   ",   ISNULL(lnloan_process_pending_loan.plot_no, '') AS plot_no " & _
                   ",   ISNULL(lnloan_process_pending_loan.block_no, '') AS block_no " & _
                   ",   ISNULL(lnloan_process_pending_loan.street, '') AS street " & _
                   ",   ISNULL(lnloan_process_pending_loan.town_city, '') AS town_city " & _
                   ",   ISNULL(lnloan_process_pending_loan.ct_no, '') AS ct_no " & _
                   ",   ISNULL(lnloan_process_pending_loan.lo_no, '') AS lo_no " & _
                   ",   ISNULL(lnloan_process_pending_loan.approverunkid, 0) AS approverunkid " & _
                   " FROM lnloan_process_pending_loan " & _
                   " JOIN hremployee_master on hremployee_master.employeeunkid = lnloan_process_pending_loan.employeeunkid " & _
                   " LEFT JOIN lnloan_scheme_master on lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid AND lnloan_scheme_master.isactive = 1 " & _
                   " JOIN " & _
                                " ( " & _
                                "    SELECT " & _
                                "         employeeunkid " & _
                                "        ,ISNULL(classunkid, 0) AS classunkid " & _
                                "        ,ISNULL(sectiongroupunkid, 0) AS sectiongroupunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_transfer_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodStart) & "'" & _
                                " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                   " LEFT JOIN hrclasses_master ON Alloc.classunkid = hrclasses_master.classesunkid " & _
                   " LEFT JOIN hrsectiongroup_master ON Alloc.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                   " WHERE lnloan_process_pending_loan.isvoid = 0 "


            'Call FilterTitleAndFilterQuery()

            'StrQ &= Me._FilterQuery

            'StrQ &= mstrOrderByQuery

            If mintEmployeeUnkId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkId)
                StrQ &= " AND lnloan_process_pending_loan.employeeunkid = @employeeunkid "
            End If

            If mintLoanSchemeUnkId > 0 Then
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeUnkId)
                StrQ &= " AND  lnloan_process_pending_loan.loanschemeunkid = @loanschemeunkid "
            End If

            If mintProcessPendingLoanUnkid > 0 Then
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessPendingLoanUnkid)
                StrQ &= " AND lnloan_process_pending_loan.processpendingloanunkid = @processpendingloanunkid "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim strTenureValue As String = Format(CDec(dtRow.Item("Tenure")), GUI.fmtCurrency)
                Dim strTenureInWords As String = NumberInWords(strTenureValue)

                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = "NMB/" & dtRow.Item("employeecode") & "/" & dtRow.Item("application_no")
                rpt_Row.Item("Column2") = CDate(dtRow.Item("application_date")).ToShortDateString()
                rpt_Row.Item("Column3") = dtRow.Item("EmployeeFullName")
                rpt_Row.Item("Column4") = dtRow.Item("Class")
                rpt_Row.Item("Column5") = dtRow.Item("SectionGroup")
                rpt_Row.Item("Column6") = dtRow.Item("EmpFirstName") & ","

                Dim strLoanAmount As String = Format(CDec(dtRow.Item("loan_amount")), GUI.fmtCurrency)
                Dim strLoanAmountInWords As String = String.Empty
                strLoanAmountInWords = NumberToWords(CDec(dtRow.Item("loan_amount")))
                '--------------------------Subject Description -- Start --------------------
                Dim strSubjectDescription As String
                If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                    strSubjectDescription = Language.getMessage(mstrModuleName, 2021, "Please refer to the captioned subject in respect of your request for a secured #LoanSchemName#. We are pleased to inform you #EmployeeFullName# (""the Borrower"") that NMB Bank Plc  (""the Bank"") has approved your request for a secured #LoanSchemName# with the below mentioned terms and subject to your acceptance of the following credit terms and conditions;")
                    strSubjectDescription = strSubjectDescription.Replace("#LoanSchemName#", dtRow.Item("LoanSchemeName"))
                    strSubjectDescription = strSubjectDescription.Replace("#EmployeeFullName#", dtRow.Item("EmployeeFullName"))
                Else
                    strSubjectDescription = Language.getMessage(mstrModuleName, 21, "Please refer to the captioned subject in respect of your request for #LoanSchemName#. We are pleased to inform you that the Bank has approved your request of TZS #PrincipalAmount#. (Tanzania Shillings #PrincipalAmountInWord# only)")
                    If mintLoanTypeId = enLoanType.Car_Loan Then
                        strSubjectDescription &= " " & Language.getMessage(mstrModuleName, 1021, "(being within 80% of the value of the car)")
                    End If
                    strSubjectDescription &= " " & Language.getMessage(mstrModuleName, 15, "subject to your compliance with the following credit terms and conditions.")

                    strSubjectDescription = strSubjectDescription.Replace("#LoanSchemName#", dtRow.Item("LoanSchemeName"))
                    strSubjectDescription = strSubjectDescription.Replace("#PrincipalAmount#", CDec(dtRow.Item("loan_amount")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US")))
                    If strLoanAmount.Length > 0 Then
                        strSubjectDescription = strSubjectDescription.Replace("#PrincipalAmountInWord#", strLoanAmountInWords)
                    End If
                End If
                rpt_Row.Item("Column7") = strSubjectDescription
                '--------------------------Subject Description -- End --------------------

                '-------------------------- 1.0 FACILITY TYPE -- Start --------------------

                If mintLoanTypeId = enLoanType.Car_Loan Then
                    rpt_Row.Item("Column8") = Language.getMessage(mstrModuleName, 1015, "Car Loan – (Under Secured Personal Loan).")
                ElseIf mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan Then
                    rpt_Row.Item("Column8") = Language.getMessage(mstrModuleName, 2015, "Secured Refinancing Mortgage Loan")
                ElseIf mintLoanTypeId = enLoanType.Construction_Mortgage_Loan Then
                    rpt_Row.Item("Column8") = Language.getMessage(mstrModuleName, 3015, "Secured Construction Mortgage Loan")
                ElseIf mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                    rpt_Row.Item("Column8") = Language.getMessage(mstrModuleName, 4015, "Secured Purchase Mortgage Loan")
                Else
                    rpt_Row.Item("Column8") = dtRow.Item("LoanSchemeName")
                End If
                '-------------------------- 1.0 FACILITY TYPE -- End --------------------

                '-------------------------- 2.0 AMOUNT -- Start --------------------
                Dim strAmountDesc As String = Language.getMessage(mstrModuleName, 20, "TZS #PrincipalAmount# /= (Tanzania Shillings #PrincipalAmountInWord# only).")
                strAmountDesc = strAmountDesc.Replace("#PrincipalAmount#", CDec(dtRow.Item("loan_amount")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US")))
                strAmountDesc = strAmountDesc.Replace("#PrincipalAmountInWord#", strLoanAmountInWords)
                rpt_Row.Item("Column9") = strAmountDesc
                '-------------------------- 2.0 AMOUNT -- END --------------------

                '--------------------------3.0 PURPOSE -- Start --------------------
                Dim strthreeDesc As String = ""
                If mintLoanTypeId = enLoanType.General_Loan Then
                    strthreeDesc = Language.getMessage(mstrModuleName, 1033, "For Personal use, however, part of the loan, shall be used to pay-off borrower’s current outstanding general staff loan.")
                ElseIf mintLoanTypeId = enLoanType.Car_Loan Then
                    strthreeDesc = Language.getMessage(mstrModuleName, 1034, "For purchasing a vehicle with registration No……………….,  Model #Model#, Chassis No #ChassisNo#, color #Color#, manufactured year #YOM#, in the name of ……..………………………………….;")
                    strthreeDesc = strthreeDesc.Replace("#Model#", dtRow.Item("Model"))
                    strthreeDesc = strthreeDesc.Replace("#ChassisNo#", dtRow.Item("chassisno"))
                    strthreeDesc = strthreeDesc.Replace("#Color#", dtRow.Item("colour"))
                    strthreeDesc = strthreeDesc.Replace("#YOM#", dtRow.Item("yom"))
                ElseIf mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                    strthreeDesc = Language.getMessage(mstrModuleName, 2034, "The purpose of this facility is #Purpose# on Plot No #PlotNo#, Block #Block#, situated at #Street# in #TownCity#, issued with C.T No.#CTNo#, L.O No. #LONo#, in the name of #EmployeeFullName#.")
                    If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan Then
                        strthreeDesc = strthreeDesc.Replace("#Purpose#", Language.getMessage(mstrModuleName, 3034, "to release equity on residential property located"))
                    ElseIf mintLoanTypeId = enLoanType.Construction_Mortgage_Loan Then
                        strthreeDesc = strthreeDesc.Replace("#Purpose#", Language.getMessage(mstrModuleName, 4034, "to finance construction of residential property"))
                    ElseIf mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                        strthreeDesc = strthreeDesc.Replace("#Purpose#", Language.getMessage(mstrModuleName, 5034, "to finance purchase of a residential property located"))
                        strthreeDesc = strthreeDesc & " " & Language.getMessage(mstrModuleName, 6034, "To be transferred to the name of …………………………………..")
                    End If
                    strthreeDesc = strthreeDesc.Replace("#PlotNo#", dtRow.Item("plot_no"))
                    strthreeDesc = strthreeDesc.Replace("#Block#", dtRow.Item("block_no"))
                    strthreeDesc = strthreeDesc.Replace("#Street#", dtRow.Item("street"))
                    strthreeDesc = strthreeDesc.Replace("#TownCity#", dtRow.Item("town_city"))
                    strthreeDesc = strthreeDesc.Replace("#CTNo#", dtRow.Item("ct_no"))
                    strthreeDesc = strthreeDesc.Replace("#LONo#", dtRow.Item("lo_no"))
                    strthreeDesc = strthreeDesc.Replace("#EmployeeFullName#", dtRow.Item("EmployeeFullName"))
                End If
                rpt_Row.Item("Column13") = strthreeDesc
                '--------------------------3.0 PURPOSE -- End --------------------

                '--------------------------4.0 INTEREST ON LOAN -- Start --------------------
                Dim strInterestValue As String = Format(CDec(dtRow.Item("interest_rate")), GUI.fmtCurrency)
                Dim strFourDesc As String = String.Empty
                If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                    strFourDesc = Language.getMessage(mstrModuleName, 1027, "The credit facility shall be charged interest rate of #PercentageInWords# (#Percentage#) percent per annum accruing daily on the outstanding balance and shall be charged monthly. The Bank reserves the right to change the rate of interest.  The above-stated interest rate shall apply to this loan as long as the borrower is a staff member of the Bank. Should the borrower’s employment with the Bank be terminated or cease for any reason(s) whatsoever, the Bank shall have the right to change the interest rate to a commercial rate that shall be applicable at the time.")

                Else
                    strFourDesc = Language.getMessage(mstrModuleName, 27, "The credit facility shall be charged interest rate of #PercentageInWords# (#Percentage#) percent per annum, accruing daily on outstanding balance and charged monthly. The Bank reserves the right to change the rate of interest, which will not be unreasonably changed, with prior consultation with the borrower. The above-stated interest rate shall apply to this loan as long as the borrower is a staff of the NMB Bank Plc. Should the borrower’s employment with the Bank be terminated or cease for any reason(s) whatsoever, the Bank shall have the right to change the interest rate to a commercial rate that shall be applicable at the time.")
                End If
                strFourDesc = strFourDesc.Replace("#PercentageInWords#", NumberInWords(strInterestValue))
                strFourDesc = strFourDesc.Replace("#Percentage#", Format(CDec(dtRow.Item("interest_rate")), GUI.fmtCurrency))
                rpt_Row.Item("Column10") = strFourDesc
                '--------------------------4.0 INTEREST ON LOAN -- End --------------------

                '--------------------------5.0 CHARGES - Start --------------------
                Dim strFiveDesc As String = ""
                If mintLoanTypeId = enLoanType.General_Loan Then
                    strFiveDesc = Language.getMessage(mstrModuleName, 29, "As per NMB tariff for staff loans. However, all costs of stamps, fees, seals, and other costs with this staff loan shall be borne by the Borrower. All fees shall be paid before disbursement.")
                ElseIf mintLoanTypeId = enLoanType.Car_Loan Then
                    strFiveDesc = Language.getMessage(mstrModuleName, 101, "As per NMB tariff for car loans. However, all costs of stamps, fees, seals, registration, and other costs with this car loan shall be borne by the Borrower. All fees shall be paid before disbursement. Cost for adding NMB as tittle holder and the cost of the discharge will be borne by the Borrower,")
                ElseIf mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                    strFiveDesc = Language.getMessage(mstrModuleName, 201, "This shall be charged as per the Bank’s tariff for staff loans. However, all costs of stamps, fees, relevant taxes, seals, registration and other costs for this loan shall be borne by the Borrower. All fees shall be paid before disbursement.")
                End If
                rpt_Row.Item("Column22") = strFiveDesc
                '--------------------------5.0 CHARGES - End --------------------


                '--------------------------6.0 TENURE - Start --------------------
                Dim strSixDesc As String = ""
                If mintLoanTypeId = enLoanType.Car_Loan Then
                    strSixDesc = Language.getMessage(mstrModuleName, 102, "The Facility shall be available to the borrower for") & " "
                End If
                If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                    strSixDesc &= Language.getMessage(mstrModuleName, 132, "#TenureInWords# (#Tenure#) months from the date of disbursement, (renewable upon request by the Borrower). There shall be no grace period.")
                Else
                    strSixDesc &= Language.getMessage(mstrModuleName, 32, "#TenureInWords# (#Tenure#) months from the date of acceptance of the Letter of Offer hereof, (renewable upon request by the borrower). There shall be no grace period.")
                End If
                strSixDesc = strSixDesc.Replace("#TenureInWords#", strTenureInWords)
                strSixDesc = strSixDesc.Replace("#Tenure#", CDec(dtRow.Item("Tenure")))
                rpt_Row.Item("Column11") = strSixDesc
                '--------------------------6.0 TENURE - End --------------------

                '--------------------------7.0 REPAYMENT OF THE LOAN - Start --------------------
                Dim strSevenDesc As String = String.Empty
                If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                    strSevenDesc = Language.getMessage(mstrModuleName, 1033, "The Borrower’s general credit facility amount shall be fully repaid through #TenureInWords# (#Tenure#) Monthly installments of TZS #InstallmentAmount# /= as shown in the attached Loan Repayment Schedule herein referred to as Annex 1. Furthermore, should the Bank decide to change the interest rate then the monthly installments shall change to reflect the changed interest rate.")
                Else
                    strSevenDesc = Language.getMessage(mstrModuleName, 33, "The staff general credit facility amount shall be fully repaid through #TenureInWords# (#Tenure#) months installments of Tshs #InstallmentAmount# /= as shown in the attached Loan Repayment Schedule here-with referred to as Annex 1, furthermore, should the Bank interest rate change then, the monthly installments shall change to reflect the change of interest rate.")
                End If
                strSevenDesc = strSevenDesc.Replace("#TenureInWords#", strTenureInWords)
                strSevenDesc = strSevenDesc.Replace("#Tenure#", CDec(dtRow.Item("Tenure")))
                strSevenDesc = strSevenDesc.Replace("#InstallmentAmount#", CDec(dtRow.Item("InstallmentAmount")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US")))
                rpt_Row.Item("Column12") = strSevenDesc
                '--------------------------7.0 REPAYMENT OF THE LOAN - End --------------------

                '--------------------------8.0 SECURITY OFFERED - Start --------------------
                Dim strEightDesc As String = ""
                If mintLoanTypeId = enLoanType.Car_Loan Then
                    strEightDesc = Language.getMessage(mstrModuleName, 1037, "First ranking Chattel Mortgage over vehicle to be purchased with registration No………………., Model #Model#, Chassis No #ChassisNo#,Body color  #Color# manufactured year #YOM#, in the name of ……..………………………………….;")
                    strEightDesc = strEightDesc.Replace("#Model#", dtRow.Item("Model"))
                    strEightDesc = strEightDesc.Replace("#ChassisNo#", dtRow.Item("chassisno"))
                    strEightDesc = strEightDesc.Replace("#Color#", dtRow.Item("colour"))
                    strEightDesc = strEightDesc.Replace("#YOM#", dtRow.Item("yom"))
                ElseIf mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                    strEightDesc = Language.getMessage(mstrModuleName, 2037, "First ranking legal mortgage over residential property to be constructed on Plot No #PlotNo#, Block #Block#, situated at #Street#,issued with CT No #CTNo#, L.O No. #LONo#, in the name of #EmployeeFullName#.")
                    strEightDesc = strEightDesc.Replace("#PlotNo#", dtRow.Item("plot_no"))
                    strEightDesc = strEightDesc.Replace("#Block#", dtRow.Item("block_no"))
                    strEightDesc = strEightDesc.Replace("#Street#", dtRow.Item("street"))
                    strEightDesc = strEightDesc.Replace("#CTNo#", dtRow.Item("ct_no"))
                    strEightDesc = strEightDesc.Replace("#LONo#", dtRow.Item("lo_no"))
                    strEightDesc = strEightDesc.Replace("#EmployeeFullName#", dtRow.Item("EmployeeFullName"))
                    If mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                        strEightDesc = strEightDesc & " " & Language.getMessage(mstrModuleName, 3037, "To be transferred to the name of …………………………………..")
                    End If
                Else
                    strEightDesc = Language.getMessage(mstrModuleName, 37, "N/A")
                End If
                rpt_Row.Item("Column23") = strEightDesc
                '--------------------------8.0 SECURITY OFFERED - End --------------------

                rpt_Row.Item("Column14") = dtRow.Item("employeecode")

                '---------------------- Reporting To Data -- Start--------------
                Dim dsReportingTo As New DataSet
                objDataOperation.ClearParameters()

                StrQ = "SELECT " & _
                        "    ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS ReportingToName " & _
                        ",   lnroleloanapproval_process_tran.approvaldate AS ReportingToApprovalDate " & _
                        "FROM lnroleloanapproval_process_tran " & _
                        "JOIN hremployee_master     ON hremployee_master.employeeunkid = lnroleloanapproval_process_tran.mapuserunkid " & _
                        "WHERE lnroleloanapproval_process_tran.isvoid = 0 " & _
                        "AND lnroleloanapproval_process_tran.levelunkid = -1 "

                If mintProcessPendingLoanUnkid > 0 Then
                    objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessPendingLoanUnkid)
                    StrQ &= " AND lnroleloanapproval_process_tran.processpendingloanunkid = @processpendingloanunkid "
                End If

                dsReportingTo = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                If dsReportingTo.Tables(0).Rows.Count > 0 Then
                    rpt_Row.Item("Column15") = dsReportingTo.Tables(0).Rows(0).Item("ReportingToName")
                    If IsDBNull(dsReportingTo.Tables(0).Rows(0).Item("ReportingToApprovalDate")) = False Then
                        rpt_Row.Item("Column16") = Format$(CDate(dsReportingTo.Tables(0).Rows(0).Item("ReportingToApprovalDate")), "dd")
                        rpt_Row.Item("Column17") = Format$(CDate(dsReportingTo.Tables(0).Rows(0).Item("ReportingToApprovalDate")), "MMM yyyy")
                    End If
                End If

                dsReportingTo = Nothing

                '---------------------- Reporting To Data -- End--------------

                '---------------------- Employee History Data -- Start--------------

                Dim dsEmployeeHistory As New DataSet
                objDataOperation.ClearParameters()

                StrQ = "SELECT " & _
                       "    athremployee_master.employeecode " & _
                       ",   ISNULL(athremployee_master.firstname, '') + ' ' + ISNULL(athremployee_master.othername, '') + ' ' + ISNULL(athremployee_master.surname, '') AS EmployeeName " & _
                       ",   isapplicant " & _
                       ",   iscentralized " & _
                       ",   isfinalapprover " & _
                       ",   empsignature " & _
                       ",   ISNULL(hrjob_master.job_name, '') as Job " & _
                        "FROM lnloan_history_tran " & _
                        "LEFT JOIN athremployee_master ON athremployee_master.atemployeeunkid = lnloan_history_tran.atemployeeunkid " & _
                        "LEFT JOIN hremployee_categorization_tran ON hremployee_categorization_tran.categorizationtranunkid = lnloan_history_tran.categorizationtranunkid AND hremployee_categorization_tran.isvoid = 0 " & _
                        "LEFT JOIN hrjob_master on hrjob_master.jobunkid = hremployee_categorization_tran.jobunkid " & _
                        "WHERE lnloan_history_tran.isvoid = 0 "

                If mintProcessPendingLoanUnkid > 0 Then
                    objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessPendingLoanUnkid)
                    StrQ &= " AND lnloan_history_tran.processpendingloanunkid = @processpendingloanunkid "
                End If

                dsEmployeeHistory = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                Dim drEmployeeHistory() As DataRow = dsEmployeeHistory.Tables(0).Select("isapplicant = 1")
                If drEmployeeHistory.Length > 0 Then
                    rpt_Row.Item("sign1") = drEmployeeHistory(0).Item("empsignature")
                End If

                Dim drFinalApproverHistory() As DataRow = dsEmployeeHistory.Tables(0).Select("isfinalapprover = 1")
                If drFinalApproverHistory.Length > 0 Then
                    rpt_Row.Item("Column18") = drFinalApproverHistory(0).Item("EmployeeName")
                    rpt_Row.Item("Column19") = drFinalApproverHistory(0).Item("Job")
                    rpt_Row.Item("sign2") = drFinalApproverHistory(0).Item("empsignature")
                End If

                Dim drCentralizedApproverHistory() As DataRow = dsEmployeeHistory.Tables(0).Select("iscentralized = 1")
                If drCentralizedApproverHistory.Length > 0 Then
                    rpt_Row.Item("Column20") = drCentralizedApproverHistory(0).Item("EmployeeName")
                    rpt_Row.Item("Column21") = drCentralizedApproverHistory(0).Item("Job")
                    rpt_Row.Item("sign3") = drCentralizedApproverHistory(0).Item("empsignature")
                End If

                dsEmployeeHistory = Nothing

                '---------------------- Employee History Data -- End--------------

                '---------------------- Final Approver Data -- Start--------------
                Dim strThirteenDesc1 As String = Language.getMessage(mstrModuleName, 3061, "First tranche; shall be limited to Tzs #FirstTranche# for ………….")
                Dim strThirteenDesc2 As String = Language.getMessage(mstrModuleName, 3062, "Second tranche; shall be limited to Tzs #SecondTranche# for ………….")
                Dim strThirteenDesc3 As String = Language.getMessage(mstrModuleName, 3063, "Third tranche; shall be limited to Tzs #ThirdTranche# for …………. ")
                Dim strThirteenDesc4 As String = Language.getMessage(mstrModuleName, 3064, "Fourth tranche; shall be limited to Tzs #ForthTranche# for …………. ")
                Dim decFirstTranche As Decimal = 0
                Dim decSecondTranche As Decimal = 0
                Dim decThirdTranche As Decimal = 0
                Dim decForthTranche As Decimal = 0
                Dim objRoleLoanApprovalProcessTran As New clsroleloanapproval_process_Tran
                Dim dsFinalApprover As New DataSet
                dsFinalApprover = objRoleLoanApprovalProcessTran.GetRoleBasedApprovalList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, False, "List", " lna.processpendingloanunkid = " & mintProcessPendingLoanUnkid & " AND lna.mapuserunkid = " & dtRow.Item("approverunkid") & " ", , False)
                If dsFinalApprover IsNot Nothing AndAlso dsFinalApprover.Tables(0).Rows.Count > 0 Then
                    decFirstTranche = CDec(dsFinalApprover.Tables(0).Rows(0).Item("first_tranche"))
                    decSecondTranche = CDec(dsFinalApprover.Tables(0).Rows(0).Item("second_tranche"))
                    decThirdTranche = CDec(dsFinalApprover.Tables(0).Rows(0).Item("third_tranche"))
                    decForthTranche = CDec(dsFinalApprover.Tables(0).Rows(0).Item("fourth_tranche"))
                End If
                strThirteenDesc1 = strThirteenDesc1.Replace("#FirstTranche#", decFirstTranche.ToString("#,##0.##", New System.Globalization.CultureInfo("en-US")))
                strThirteenDesc2 = strThirteenDesc2.Replace("#SecondTranche#", decSecondTranche.ToString("#,##0.##", New System.Globalization.CultureInfo("en-US")))
                strThirteenDesc3 = strThirteenDesc3.Replace("#ThirdTranche#", decThirdTranche.ToString("#,##0.##", New System.Globalization.CultureInfo("en-US")))
                strThirteenDesc4 = strThirteenDesc4.Replace("#ForthTranche#", decForthTranche.ToString("#,##0.##", New System.Globalization.CultureInfo("en-US")))

                rpt_Row.Item("Column24") = strThirteenDesc1
                rpt_Row.Item("Column25") = strThirteenDesc2
                rpt_Row.Item("Column26") = strThirteenDesc3
                rpt_Row.Item("Column27") = strThirteenDesc4
                objRoleLoanApprovalProcessTran = Nothing

                '---------------------- Final Approver Data -- Start--------------
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)


            Next

            Select Case mintLoanTypeId
                Case enLoanType.General_Loan
                    objRpt = New ArutiReport.Designer.rptFlexcubeGeneralLoan
                Case enLoanType.Car_Loan
                    objRpt = New ArutiReport.Designer.rptFlexcubeCarLoan
                Case enLoanType.Refinancing_Mortgage_Loan
                    objRpt = New ArutiReport.Designer.rptFlexcubeRefinanceMortgageLoan
                Case enLoanType.Construction_Mortgage_Loan
                    objRpt = New ArutiReport.Designer.rptFlexcubeConstructionMortgageLoan
                Case enLoanType.Purchase_Mortgage_Loan
                    objRpt = New ArutiReport.Designer.rptFlexcubePurchaseMortgageLoan

            End Select


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                       True, _
                                       False, _
                                       "arutiLogo1", _
                                       "arutiLogo2", _
                                       arrImageRow, _
                                       , _
                                       , _
                                       , _
                                       ConfigParameter._Object._GetLeftMargin, _
                                       ConfigParameter._Object._GetRightMargin, _
                                       ConfigParameter._Object._TOPPageMarging)




            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 11, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 12, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 13, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 14, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblTel", Language.getMessage(mstrModuleName, 1, "Tel :"))
            Call ReportFunction.TextChange(objRpt, "txtTel", Language.getMessage(mstrModuleName, 2, "+255(0)  2161000"))
            Call ReportFunction.TextChange(objRpt, "lblDir", Language.getMessage(mstrModuleName, 3, "Dir :"))
            Call ReportFunction.TextChange(objRpt, "txtDir", Language.getMessage(mstrModuleName, 4, "+255(0)  2161310"))
            Call ReportFunction.TextChange(objRpt, "lblFax", Language.getMessage(mstrModuleName, 5, "Fax :"))
            Call ReportFunction.TextChange(objRpt, "txtFax", Language.getMessage(mstrModuleName, 6, "+255(0)  2161312"))
            Call ReportFunction.TextChange(objRpt, "lblRefNo", Language.getMessage(mstrModuleName, 7, "Ref. No. :"))
            Call ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 8, "Date :"))
            Call ReportFunction.TextChange(objRpt, "lblName", Language.getMessage(mstrModuleName, 9, "Name :"))
            Call ReportFunction.TextChange(objRpt, "txtCompanyGroupName", Language.getMessage(mstrModuleName, 10, "NMB Bank Plc,"))
            Call ReportFunction.TextChange(objRpt, "lblBranch", Language.getMessage(mstrModuleName, 11, "Branch"))
            If mintLoanTypeId = enLoanType.General_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblDepartment", Language.getMessage(mstrModuleName, 12, "Department"))
            End If
            Call ReportFunction.TextChange(objRpt, "lblDear", Language.getMessage(mstrModuleName, 13, "Dear"))

            If mintLoanTypeId = enLoanType.General_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblSubject", Language.getMessage(mstrModuleName, 14, "Re: Un-Secured Staff General Loan"))
            ElseIf mintLoanTypeId = enLoanType.Car_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblSubject", Language.getMessage(mstrModuleName, 1014, "Re: Car Loan Offer Letter"))
            ElseIf mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblSubject", Language.getMessage(mstrModuleName, 2014, "Offer Letter for Secured Refinancing Mortgage Loan"))
            ElseIf mintLoanTypeId = enLoanType.Construction_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblSubject", Language.getMessage(mstrModuleName, 3014, "Offer Letter for Secured Construction Mortgage Loan"))
            ElseIf mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblSubject", Language.getMessage(mstrModuleName, 4014, "Offer Letter for Secured Purchase Mortgage Loan"))
            End If

            Call ReportFunction.TextChange(objRpt, "lblone", Language.getMessage(mstrModuleName, 16, "1.0"))
            Call ReportFunction.TextChange(objRpt, "txtone", Language.getMessage(mstrModuleName, 17, "FACILITY TYPE:-"))

            Call ReportFunction.TextChange(objRpt, "lbltwo", Language.getMessage(mstrModuleName, 18, "2.0"))
            Call ReportFunction.TextChange(objRpt, "txttwo", Language.getMessage(mstrModuleName, 19, "AMOUNT:-"))

            Call ReportFunction.TextChange(objRpt, "lblthree", Language.getMessage(mstrModuleName, 22, "3.0"))
            Call ReportFunction.TextChange(objRpt, "txtthree", Language.getMessage(mstrModuleName, 23, "PURPOSE:-"))


            Call ReportFunction.TextChange(objRpt, "lblfour", Language.getMessage(mstrModuleName, 25, "4.0"))
            Call ReportFunction.TextChange(objRpt, "txtfour", Language.getMessage(mstrModuleName, 26, "INTEREST ON LOAN:-"))

            Call ReportFunction.TextChange(objRpt, "lblfive", Language.getMessage(mstrModuleName, 27, "5.0"))
            Call ReportFunction.TextChange(objRpt, "txtfive", Language.getMessage(mstrModuleName, 28, "CHARGES:-"))

            Call ReportFunction.TextChange(objRpt, "lblsix", Language.getMessage(mstrModuleName, 30, "6.0"))
            Call ReportFunction.TextChange(objRpt, "txtsix", Language.getMessage(mstrModuleName, 31, "TENURE:-"))

            Call ReportFunction.TextChange(objRpt, "lblSeven", Language.getMessage(mstrModuleName, 33, "7.0"))
            Call ReportFunction.TextChange(objRpt, "txtSeven", Language.getMessage(mstrModuleName, 34, "REPAYMENT OF THE LOAN:-"))

            Call ReportFunction.TextChange(objRpt, "lblEight", Language.getMessage(mstrModuleName, 35, "8.0"))
            Call ReportFunction.TextChange(objRpt, "txtEight", Language.getMessage(mstrModuleName, 36, "SECURITY OFFERED:-"))
            If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblEightI", Language.getMessage(mstrModuleName, 1036, "i)"))
                Call ReportFunction.TextChange(objRpt, "txtEightDesc1", Language.getMessage(mstrModuleName, 1037, "The facility shall be secured by the following collaterals:"))
            End If

            Call ReportFunction.TextChange(objRpt, "lblNine", Language.getMessage(mstrModuleName, 38, "9.0"))
            Call ReportFunction.TextChange(objRpt, "txtNine", Language.getMessage(mstrModuleName, 39, "SPOUSE CONSENT:-"))
            If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtNineDesc", Language.getMessage(mstrModuleName, 140, "Submission of marriage certificate, spousal consent and ID of the spouse. If the Borrower does not have a spouse then an affidavit to that effect must be submitted."))
            Else
                Call ReportFunction.TextChange(objRpt, "txtNineDesc", Language.getMessage(mstrModuleName, 40, "N/A"))
            End If

            Call ReportFunction.TextChange(objRpt, "lblTen", Language.getMessage(mstrModuleName, 38, "10.0"))
            Call ReportFunction.TextChange(objRpt, "txtTen", Language.getMessage(mstrModuleName, 39, "CREDIT LIFE INSURANCE:-"))
            If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtTenDesc", Language.getMessage(mstrModuleName, 240, "The Borrower undertakes to procure Credit Life Insurance with an insurer acceptable to the Bank to cover the risk of death and/or permanent disability. The Bank shall have the right to debit the Borrower’s account, pay the premium to the insurer and charge the Borrower’s account."))
            Else
                Call ReportFunction.TextChange(objRpt, "txtTenDesc", Language.getMessage(mstrModuleName, 40, "The Borrower is to undertake to procure Credit Life Insurance with an insurer acceptable to the Bank to cover the risk of death and/or permanent disability. The Borrower will authorize the Bank to pay the premium to the insurer and charge his/her account. The Borrower will accept in writing by authorizing the Bank to debit his/her account with the premium to cover an amount equal to the outstanding Bank loan at any time."))
            End If

            Call ReportFunction.TextChange(objRpt, "lblEleven", Language.getMessage(mstrModuleName, 41, "11.0"))
            Call ReportFunction.TextChange(objRpt, "txtEleven", Language.getMessage(mstrModuleName, 42, "PROPERTY INSURANCE:-"))
            If mintLoanTypeId = enLoanType.General_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtElevenDesc", Language.getMessage(mstrModuleName, 43, "N/A"))
            ElseIf mintLoanTypeId = enLoanType.Car_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtElevenDesc", Language.getMessage(mstrModuleName, 1043, "Comprehensive Insurance."))
            ElseIf mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtElevenDesc", Language.getMessage(mstrModuleName, 2043, "Submission of insurance cover over the purchased property from the list of approved bank insurers with the interests of the Bank noted as 1st loss payee."))
            End If

            Call ReportFunction.TextChange(objRpt, "lblTwelve", Language.getMessage(mstrModuleName, 44, "12.0"))
            Call ReportFunction.TextChange(objRpt, "txtTwelve", Language.getMessage(mstrModuleName, 45, "ASSIGNING TERMINAL BENEFITS TO OFF-SET OUTSTANDING BANK DEBTS:-"))
            If mintLoanTypeId = enLoanType.General_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtTwelveDesc1", Language.getMessage(mstrModuleName, 46, "The borrower voluntarily assigns to NMB Bank Plc all his/her rights due with regard to current and future outstanding Bank loan(s) and accrued interest, the borrower’s terminal benefits or gratuity or scheme, for the purposes of satisfying any outstanding liability upon cessation of the borrower’s employment at NMB Bank Plc by whatever means, such to include but not to be limited to the borrower’s resignation, abscondment, dismissal, retrenchment or termination."))
                Call ReportFunction.TextChange(objRpt, "txtTwelveDesc2", Language.getMessage(mstrModuleName, 47, "The Borrower commits to ensure that their outstanding liability is settled regardless of how they leave employment."))
            ElseIf mintLoanTypeId = enLoanType.Car_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtTwelveDesc1", Language.getMessage(mstrModuleName, 1046, "The borrower to voluntarily assign to the NMB Bank Plc (NMB) all the rights due to NMB with regard to current and future outstanding Bank loan(s) and accrued interest, the borrower’s terminal benefits or gratuity from either the National Social Security Fund (NSSF), (PSSSF) or any other relevant pension fund or scheme,  for the purposes of satisfying any outstanding liability upon cessation of the borrower’s employment at NMB by whatever means, such to include but not to be limited to the borrower’s resignation, abscondment, dismissal, retrenchment or termination."))
            ElseIf mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtTwelveDesc1", Language.getMessage(mstrModuleName, 246, "The Borrower to voluntarily assign to the  Bank all the rights due to the Bank with regard to current and future outstanding Bank loan(s) and accrued interest, the Borrower’s terminal benefits or gratuity or scheme. This is for the purposes of satisfying any outstanding liability upon cessation of the Borrower’s employment at the Bank by retirement."))
                Call ReportFunction.TextChange(objRpt, "txtTwelveDesc2", Language.getMessage(mstrModuleName, 247, "For cessation of employment due to resignation, the Borrower shall have to settle the entire outstanding balance before leaving the bank or arrange for take over with the new employer."))
                Call ReportFunction.TextChange(objRpt, "txtTwelveDesc3", Language.getMessage(mstrModuleName, 248, "The Borrower commits to ensure that their outstanding liability is settled regardless of how they leave employment."))
            End If

            Call ReportFunction.TextChange(objRpt, "lblThirteen", Language.getMessage(mstrModuleName, 48, "13.0"))
            Call ReportFunction.TextChange(objRpt, "txtThirteen", Language.getMessage(mstrModuleName, 49, "DIBURSEMENT OF FUNDS:-"))
            If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtThirteenDesc", Language.getMessage(mstrModuleName, 354, "Disbursement will be made in lump sum into the Borrower`s account."))
            ElseIf mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtThirteenDesc", Language.getMessage(mstrModuleName, 354, "Disbursement of the facility shall be done in tranches as follows;-"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenI", Language.getMessage(mstrModuleName, 361, "(i)"))
                'Call ReportFunction.TextChange(objRpt, "txtThirteenDesc1", Language.getMessage(mstrModuleName, 3061, "First tranche; shall be limited to Tzs ……………………… for ………..."))
                Call ReportFunction.TextChange(objRpt, "lblThirteenII", Language.getMessage(mstrModuleName, 362, "(ii)"))
                'Call ReportFunction.TextChange(objRpt, "txtThirteenDesc2", Language.getMessage(mstrModuleName, 3062, "Second tranche; shall be limited to Tzs ……………….. for …………."))
                Call ReportFunction.TextChange(objRpt, "lblThirteenIII", Language.getMessage(mstrModuleName, 363, "(iii)"))
                'Call ReportFunction.TextChange(objRpt, "txtThirteenDesc3", Language.getMessage(mstrModuleName, 3063, "Third tranche; shall be limited to   Tzs …………………. for ……………. "))
                Call ReportFunction.TextChange(objRpt, "lblThirteenIV", Language.getMessage(mstrModuleName, 364, "(iv)"))
                'Call ReportFunction.TextChange(objRpt, "txtThirteenDesc4", Language.getMessage(mstrModuleName, 3064, "Fourth tranche; shall be limited to   Tzs …………………. for ……………. "))
                Call ReportFunction.TextChange(objRpt, "lblThirteenV", Language.getMessage(mstrModuleName, 365, "(v)"))
                Call ReportFunction.TextChange(objRpt, "txtThirteenDesc5", Language.getMessage(mstrModuleName, 3065, "Next tranche disbursement after the first tranche should be accompanied by a completion 	proof of the previous tranche with coloured photos of the building and a call report by a facilities team member confirming that the Borrower is ready for the next phase disbursement."))
            End If
            'Hemant (29 Mar 2023) -- Start
            'ISSUE/EHANCEMENT(NMB) : Remove this section 8 of general loan offer letter. After removing section 8, the number serquence will change
            If mintLoanTypeId = enLoanType.General_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblTwelveOne", Language.getMessage(mstrModuleName, 50, "12.1"))
            Else
                'Hemant (29 Mar 2023) -- End
            Call ReportFunction.TextChange(objRpt, "lblThirteenOne", Language.getMessage(mstrModuleName, 50, "13.1"))
            End If 'Hemant (29 Mar 2023)
            Call ReportFunction.TextChange(objRpt, "txtThirteenOneA", Language.getMessage(mstrModuleName, 51, "A: CONDITIONS PRECEDENT TO FACILITY UTILISATION"))
            Call ReportFunction.TextChange(objRpt, "lblThirteenOneAI", Language.getMessage(mstrModuleName, 52, "i."))
            Call ReportFunction.TextChange(objRpt, "lblThirteenOneAII", Language.getMessage(mstrModuleName, 53, "ii."))
            If mintLoanTypeId = enLoanType.General_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc1", Language.getMessage(mstrModuleName, 54, "After execution and submission of Offer Letter and after having met the precedent conditions."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc2", Language.getMessage(mstrModuleName, 55, "Full payment of the current serviced general staff loan (if any)"))
            ElseIf mintLoanTypeId = enLoanType.Car_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAIII", Language.getMessage(mstrModuleName, 153, "iii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc1", Language.getMessage(mstrModuleName, 1054, "Duly execution and submission of Offer Letter and after having met the precedent conditions;"))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc2", Language.getMessage(mstrModuleName, 1055, "Provide evidence of the borrower`s contribution equivalent to at least 20% of the vehicle selling price;"))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc3", Language.getMessage(mstrModuleName, 1055, "That the purchased vehicles shall not be tax exempted."))
            ElseIf mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAIII", Language.getMessage(mstrModuleName, 253, "iii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc1", Language.getMessage(mstrModuleName, 2054, "Submission of duly executed Offer letter;"))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc2", Language.getMessage(mstrModuleName, 2055, "Execution and submission of all legal documents;"))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc3", Language.getMessage(mstrModuleName, 2055, "Submission of marriage certificate. For staff who do not have a spouse, affidavit of that effect must be submitted;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAIV", Language.getMessage(mstrModuleName, 254, "iv."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc4", Language.getMessage(mstrModuleName, 2057, "Submission of current valuation report from approved bank’s valuer over pledged collateral. (Copy of the title deed must be attached in the valuation report.);"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAV", Language.getMessage(mstrModuleName, 255, "v."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc5", Language.getMessage(mstrModuleName, 2058, "Execution and perfection of all legal documents applicable;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAVI", Language.getMessage(mstrModuleName, 256, "vi."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc6", Language.getMessage(mstrModuleName, 2059, "Submission of copy of identity card (ID) for both Borrower and spouse i.e National ID, Voters registration ID, Driving Licence or Employee ID;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAVII", Language.getMessage(mstrModuleName, 257, "vii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc7", Language.getMessage(mstrModuleName, 2060, "Submission of insurance cover where the Bank is noted as first loss payee;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAVIII", Language.getMessage(mstrModuleName, 258, "viii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc8", Language.getMessage(mstrModuleName, 2061, "Submission of spouse consent over property to be mortgaged;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAIX", Language.getMessage(mstrModuleName, 259, "ix."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc9", Language.getMessage(mstrModuleName, 2062, "Submission of current land rent receipt;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAX", Language.getMessage(mstrModuleName, 260, "x."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc10", Language.getMessage(mstrModuleName, 2063, "Submission of the original certificate of title."))
            ElseIf mintLoanTypeId = enLoanType.Construction_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAIII", Language.getMessage(mstrModuleName, 253, "iii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc1", Language.getMessage(mstrModuleName, 2054, "Submission of duly executed Offer letter;"))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc2", Language.getMessage(mstrModuleName, 2055, "Execution and submission of all legal documents;"))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc3", Language.getMessage(mstrModuleName, 2055, "Submission of marriage certificate. For staff who do not have a spouse, affidavit of that effect must be submitted;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAIV", Language.getMessage(mstrModuleName, 254, "iv."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc4", Language.getMessage(mstrModuleName, 2057, "Submission of copy of identity card (ID) for both Borrower and spouse i.e National ID, Voters registration ID, Driving Licence or Employee ID;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAV", Language.getMessage(mstrModuleName, 255, "v."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc5", Language.getMessage(mstrModuleName, 2058, "Submission of spouse consent over property to be mortgaged;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAVI", Language.getMessage(mstrModuleName, 256, "vi."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc6", Language.getMessage(mstrModuleName, 2059, "Submission of current land rent receipt;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAVII", Language.getMessage(mstrModuleName, 257, "vii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc7", Language.getMessage(mstrModuleName, 2060, "Submission of the original certificate of title;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAVIII", Language.getMessage(mstrModuleName, 258, "viii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc8", Language.getMessage(mstrModuleName, 2061, "Submission of the Building Permit Document;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAIX", Language.getMessage(mstrModuleName, 259, "ix."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc9", Language.getMessage(mstrModuleName, 2062, "Submission of the Certified Building Drawings and Bills of Quantities."))
            ElseIf mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAIII", Language.getMessage(mstrModuleName, 253, "iii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc1", Language.getMessage(mstrModuleName, 2054, "Submission of duly executed Offer letter;"))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc2", Language.getMessage(mstrModuleName, 2055, "Execution and submission of all legal documents;"))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc3", Language.getMessage(mstrModuleName, 2055, "Submission of marriage certificate. For staff who do not have a spouse, affidavit of that effect must be submitted;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAIV", Language.getMessage(mstrModuleName, 254, "iv."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc4", Language.getMessage(mstrModuleName, 2057, "Submission of current valuation report from approved bank’s valuer over pledged collateral. (Copy of the title deed must be attached in the valuation report.);"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAV", Language.getMessage(mstrModuleName, 255, "v."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc5", Language.getMessage(mstrModuleName, 2058, "Execution and perfection of all legal documents applicable;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAVI", Language.getMessage(mstrModuleName, 256, "vi."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc6", Language.getMessage(mstrModuleName, 2059, "Submission of copy of identity card (ID) for both Borrower and spouse i.e National ID, Voters registration ID, Driving License;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAVII", Language.getMessage(mstrModuleName, 257, "vii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc7", Language.getMessage(mstrModuleName, 2060, "Submission of insurance cover where the Bank is noted as first loss payee;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAVIII", Language.getMessage(mstrModuleName, 258, "viii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc8", Language.getMessage(mstrModuleName, 2061, "Submission of spouse consent over property to be mortgaged;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAIX", Language.getMessage(mstrModuleName, 259, "ix."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc9", Language.getMessage(mstrModuleName, 2062, "Submission of current land rent receipt;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAX", Language.getMessage(mstrModuleName, 259, "x."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc10", Language.getMessage(mstrModuleName, 2062, "Submission of the original certificate of title;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAXI", Language.getMessage(mstrModuleName, 259, "xi."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc11", Language.getMessage(mstrModuleName, 2062, "Submission of sale agreement between the borrower and vendor;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAXII", Language.getMessage(mstrModuleName, 259, "xii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc12", Language.getMessage(mstrModuleName, 2062, "Submission of spouse consent of the vendor;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneAXIII", Language.getMessage(mstrModuleName, 259, "xiii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneADesc13", Language.getMessage(mstrModuleName, 2062, "Evidence of the borrower`s contribution amounting to TZS ……..."))
            End If
            
            Call ReportFunction.TextChange(objRpt, "txtThirteenOneB", Language.getMessage(mstrModuleName, 56, "B: SUBSEQUENT CONDITIONS: "))
            If mintLoanTypeId = enLoanType.General_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc", Language.getMessage(mstrModuleName, 57, "Nil"))
            ElseIf mintLoanTypeId = enLoanType.Car_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB1", Language.getMessage(mstrModuleName, 52, "i."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc1", Language.getMessage(mstrModuleName, 57, "Execution and submission of chattel mortgage documents;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB2", Language.getMessage(mstrModuleName, 52, "ii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc2", Language.getMessage(mstrModuleName, 57, "Submission of the Original Motor Vehicle Registration Card in the names of the Borrower;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB3", Language.getMessage(mstrModuleName, 52, "iii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc3", Language.getMessage(mstrModuleName, 57, "The Bank shall register Original Motor Vehicle Registration Card in Joint Names of the Bank and the Borrower at the Borrower`s cost;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB4", Language.getMessage(mstrModuleName, 52, "iv."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc4", Language.getMessage(mstrModuleName, 57, "Submission on yearly basis the original comprehensive insurance cover of the motor vehicle with interest of the Bank noted therein throughout the tenor of the facility;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB5", Language.getMessage(mstrModuleName, 52, "v."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc5", Language.getMessage(mstrModuleName, 57, "Disbursement shall be made to borrower`s account and immediately transferred to supplier`s account by the Bank;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB6", Language.getMessage(mstrModuleName, 52, "vi."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc6", Language.getMessage(mstrModuleName, 57, "For vehicles purchased within the country the original vehicle registration card shall be submitted to the Bank within 7 days from disbursement date;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB7", Language.getMessage(mstrModuleName, 52, "vii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc7", Language.getMessage(mstrModuleName, 57, "For vehicles purchased abroad the original vehicle registration card shall be submitted to the Bank within 60 days from disbursement date;"))
            ElseIf mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB1", Language.getMessage(mstrModuleName, 252, "i."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc1", Language.getMessage(mstrModuleName, 257, "Submission of Land rents, Insurance on annual basis and Valuation Report after every three years during the period of servicing the loan until expiry. The bank shall have the right to debit customer’s account for renewal of insurance, land rent and valuation report upon expiry of the same, the Bank will notify the Borrower 14 days prior its expiration for renewal."))
            ElseIf mintLoanTypeId = enLoanType.Construction_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB1", Language.getMessage(mstrModuleName, 252, "i."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc1", Language.getMessage(mstrModuleName, 257, "Submission of a valuation report prepared by approved bank valuer right after first tranche construction is completed on the financed property, (Copy of the title deed must be attached in the valuation report);"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB2", Language.getMessage(mstrModuleName, 252, "ii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc2", Language.getMessage(mstrModuleName, 257, "Submission of insurance cover where the Bank is noted as a first loss payee within 7 days after disbursement of final tranche;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB3", Language.getMessage(mstrModuleName, 252, "iii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc3", Language.getMessage(mstrModuleName, 257, "Perfection of pledged collaterals (Registration of Mortgage) shall be done after completion of first tranche. The Bank shall have the right to debit registration fees and all related fees from the Borrower’s account if the same is not submitted within 14 days;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB4", Language.getMessage(mstrModuleName, 252, "vi."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc4", Language.getMessage(mstrModuleName, 257, "Submission of valuation report after completion of the building which shall be within three months after completion of final tranche;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB5", Language.getMessage(mstrModuleName, 252, "v."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc5", Language.getMessage(mstrModuleName, 257, "The period from one tranche to another shall not exceed three months and construction period shall not exceed six months from first drawdown date; "))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB6", Language.getMessage(mstrModuleName, 252, "vi."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc6", Language.getMessage(mstrModuleName, 257, "Submission of Land rents, Insurance on annual basis and Valuation Report after every three years during the period of servicing the loan until expiry. The bank shall have the right to debit customer’s account for renewal of insurance, land rent and valuation report upon expiry of the same, the Bank will notify the Borrower 14 days prior its expiration for renewal."))
            ElseIf mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB1", Language.getMessage(mstrModuleName, 252, "i."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc1", Language.getMessage(mstrModuleName, 257, "The facility should be strictly used for  purpose as stipulated on this offer letter;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB2", Language.getMessage(mstrModuleName, 252, "ii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc2", Language.getMessage(mstrModuleName, 257, "No further borrowing from other financial institutions/ banks without NMB’s  prior written  consent;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB3", Language.getMessage(mstrModuleName, 252, "iii."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc3", Language.getMessage(mstrModuleName, 257, "There shall be no late payment for this loan;"))
                Call ReportFunction.TextChange(objRpt, "lblThirteenOneB4", Language.getMessage(mstrModuleName, 252, "vi."))
                Call ReportFunction.TextChange(objRpt, "txtThirteenOneBDesc4", Language.getMessage(mstrModuleName, 257, "Submission of Land rents, Insurance on annual basis and Valuation Report after every three years during the period of servicing the loan until expiry. The bank shall have the right to debit customer’s account for renewal of insurance, land rent and valuation report upon expiry of the same, the Bank will notify the Borrower 14 days prior its expiration for renewal."))
            End If

            Call ReportFunction.TextChange(objRpt, "lblC", Language.getMessage(mstrModuleName, 58, "C."))
            Call ReportFunction.TextChange(objRpt, "txtC", Language.getMessage(mstrModuleName, 59, "TERMS AND CONDITIONS:-"))
            Call ReportFunction.TextChange(objRpt, "lblForteen", Language.getMessage(mstrModuleName, 60, "14.0"))
            Call ReportFunction.TextChange(objRpt, "txtForteen", Language.getMessage(mstrModuleName, 61, "OPERATIONAL PRICING:-"))
            If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtForteenDesc", Language.getMessage(mstrModuleName, 262, "The Bank shall reserve the right on all transactions arising from the operation of the above-mentioned credit facility or the provision by the Bank of other services."))
            Else
                Call ReportFunction.TextChange(objRpt, "txtForteenDesc", Language.getMessage(mstrModuleName, 62, "All transactions arising from the operation of the above-mentioned credit facility or the provision by the Bank of other services should be agreed upon in writing with the Bank."))
            End If

            Call ReportFunction.TextChange(objRpt, "lblFifteen", Language.getMessage(mstrModuleName, 48, "15.0"))
            Call ReportFunction.TextChange(objRpt, "txtFifteen", Language.getMessage(mstrModuleName, 49, "COST EXPENSES AND FEES:-"))
            Call ReportFunction.TextChange(objRpt, "txtFifteenDesc1", Language.getMessage(mstrModuleName, 48, "The Borrower agrees that:"))
            Call ReportFunction.TextChange(objRpt, "lblFifteenI", Language.getMessage(mstrModuleName, 49, "i)"))
            If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtFifteenDescI1", Language.getMessage(mstrModuleName, 249, "All costs and expenses whatsoever including legal, registration of mortgage, discharge of mortgage and auctioneer costs related with the recovery or attempted recovery of moneys owing under the Facility as well as the contesting of any involvement in any legal proceedings of whatsoever nature by the Bank for the protection of or in connection with any account(s) or assets of the Borrower shall be payable by the Borrower on demand, on a full indemnity basis, together with interest from the date the costs and expenses are incurred to the date of full payment at such rate as the Bank may prescribe (both before and after judgment)."))
            Else
                Call ReportFunction.TextChange(objRpt, "txtFifteenDescI1", Language.getMessage(mstrModuleName, 49, "All costs and expenses whatsoever including legal and auctioneers costs connected with the recovery or attempted recovery of moneys owing under the Facility as well as the contesting of any involvement in any legal proceedings of whatsoever nature by the Bank for the protection of or in connection with any account(s) or assets of the Borrower shall be payable by the Borrower on demand, on a full indemnity basis, together with interest from the date the costs and expenses are incurred to the date of full payment at such rate as the Bank may prescribe (both before and after judgment)."))
            End If
            Call ReportFunction.TextChange(objRpt, "lblFifteenII", Language.getMessage(mstrModuleName, 49, "ii)"))
            Call ReportFunction.TextChange(objRpt, "txtFifteenDescII1", Language.getMessage(mstrModuleName, 49, "The Bank shall have the right at any time to debit the Borrower’s account with interest, commission, charges, fees and all monies arising from the Facilities as well as all amounts and sums of money mentioned in the preceding sub¬paragraph (i) payable by the Borrower. No such debiting shall be deemed to be a payment of the amount due (except to the extent of any amount in credit in the Borrower’s current account(s) or a waiver of any event of default under any agreement relating to the facilities. If such debiting causes the Borrower account(s) to be overdrawn beyond the permitted limit, interest and any other applicable charges shall be payable accordingly."))

            Call ReportFunction.TextChange(objRpt, "lblSixteen", Language.getMessage(mstrModuleName, 48, "16.0"))
            Call ReportFunction.TextChange(objRpt, "txtSixteen", Language.getMessage(mstrModuleName, 49, "RECONSTRUCTION, ASSIGNMENT AND ORGANISATION:-"))
            Call ReportFunction.TextChange(objRpt, "txtSixteenDesc", Language.getMessage(mstrModuleName, 49, "The Borrower shall not without the prior written consent of the Bank, such consent not to be unreasonably withheld, effect any material change in ownership/shareholding by new shareholders nor undertake or permit any arrangement or reconstruction of its present constitution. The Borrower shall obtain the written consent of the Bank before effecting any alteration of the Borrower’s Memorandum and Articles of Association relating to the borrowing power or the principal business of the Borrower."))
            Call ReportFunction.TextChange(objRpt, "lblSixteenA", Language.getMessage(mstrModuleName, 49, "(a)"))
            Call ReportFunction.TextChange(objRpt, "txtSixteenA1", Language.getMessage(mstrModuleName, 49, "The borrower may not assign or transfer all or any of its rights, benefits or obligations under or referred to in the Agreement without the Bank’s prior written consent; which consent may be given at the Bank’s sole discretion and on such conditions as the Bank deems fit."))
            Call ReportFunction.TextChange(objRpt, "txtSixteenA2", Language.getMessage(mstrModuleName, 49, "The Bank may at any time assign or transfer to any person all or any of its rights, benefits or obligations under or referred to in the Agreement or change its lending office, subject to providing 30 days’ notice."))

            Call ReportFunction.TextChange(objRpt, "lblSeventeen", Language.getMessage(mstrModuleName, 48, "17.0"))
            Call ReportFunction.TextChange(objRpt, "txtSeventeen", Language.getMessage(mstrModuleName, 49, "SET OFF, COMBINATION OR CONSOLIDATION OF ACCOUNTS:-"))
            Call ReportFunction.TextChange(objRpt, "txtSeventeenDesc", Language.getMessage(mstrModuleName, 49, "The Bank shall be entitled (but shall not be obliged) at any time and without notice to the Borrower to combine, consolidate or merge all or any of the Borrower’s accounts and liabilities with the Bank anywhere whether in or outside the Republic of Tanzania and may transfer or set off any sums in credit in such accounts in or towards satisfaction of any of the Borrower’s liabilities whether actual or contingent, primary or collateral notwithstanding that the credit balances on such accounts and the liabilities on any other accounts may not be expressed in the same currency and the Bank is hereby authorized to effect any necessary conversions at the Bank’s own rate of exchange then prevailing."))

            Call ReportFunction.TextChange(objRpt, "lblEighteen", Language.getMessage(mstrModuleName, 48, "18.0"))
            Call ReportFunction.TextChange(objRpt, "txtEighteen", Language.getMessage(mstrModuleName, 49, "CONTINGENT LIABILITIES:-"))
            Call ReportFunction.TextChange(objRpt, "txtEighteenDesc", Language.getMessage(mstrModuleName, 49, "In the event of the Bank issuing a demand for repayment, all contingent liabilities together with other indebtedness or liabilities shall become immediately due and payable whereupon the Bank may in addition to other rights herein call for cash cover and/or debit the Borrower’s account(s) for all such contingent liabilities and for all notes or bills accepted endorsed or discounted and all bonds guarantees indemnities documentary or other credits or any instruments whatsoever issued by the Bank on behalf of or at the request of the Borrower."))

            Call ReportFunction.TextChange(objRpt, "lblNineteen", Language.getMessage(mstrModuleName, 48, "19.0"))
            Call ReportFunction.TextChange(objRpt, "txtNineteen", Language.getMessage(mstrModuleName, 49, "PAYMENTS:-"))
            Call ReportFunction.TextChange(objRpt, "txtNineteenDesc1", Language.getMessage(mstrModuleName, 49, "All payments by the Borrower in respect of the facilities shall be made in full without set-off deductions or counterclaims and free of and without deduction for or on account of tax unless the Borrower is required by law in any jurisdiction to make any such payments subject to such withholdings or deductions, in which case the Borrower shall pay such additional amount to the Bank as may be necessary in order that the actual amount received after such withholding or deduction shall equal the amount that would have been received if such withholding or deduction were not required."))
            Call ReportFunction.TextChange(objRpt, "txtNineteenDesc2", Language.getMessage(mstrModuleName, 49, "The Borrower shall fully indemnify the Bank from any liability with respect to the delay or failure by the Borrower to pay any taxes or charges. Without prejudice to the foregoing, the Borrower shall complete such forms and documentation as may be required from time to time by the Bank for the purpose of conferring upon the Bank the benefit of any applicable tax treaties or provision under applicable law for any other purposes in connection therewith."))

            Call ReportFunction.TextChange(objRpt, "lblTwenty", Language.getMessage(mstrModuleName, 48, "20.0"))
            If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtTwenty", Language.getMessage(mstrModuleName, 1049, "DISCLOSURE AND TRASFER OF BANK’S RIGHTS:-"))
            Else
                Call ReportFunction.TextChange(objRpt, "txtTwenty", Language.getMessage(mstrModuleName, 49, "DISCLOSURE:-"))
            End If
            Call ReportFunction.TextChange(objRpt, "lblTwentyA", Language.getMessage(mstrModuleName, 48, "(a)"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyDesc1", Language.getMessage(mstrModuleName, 49, "To enable the Bank to comply with regulatory requirements, the Borrower, by its acceptance of the facilities, irrevocably consents to the disclosure by the Bank its officers and agents in any manner however of any information relating to the Borrower and its account relationship with the Bank including without limitation, details of the Borrower’s facilities, the securities taken, the Borrower’s credit balances and deposits with the Bank to (i) the Bank’s head office, any of its representative and branch offices in any jurisdiction, related corporations, ii) any regulatory or supervisory authority including fiscal authority in any jurisdiction, (iii) any potential assignee of the Bank or other participant in any of its rights and/or obligations in relation  to the Borrower’s facilities, (iv) any guarantors, third party pledge or security providers and the Bank’s agents and (v) any Tanzania Bankers Associations approved Credit Reference Bureau."))
            Call ReportFunction.TextChange(objRpt, "lblTwentyB", Language.getMessage(mstrModuleName, 48, "(b)"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyDesc2", Language.getMessage(mstrModuleName, 49, "To enable the Bank to make a comprehensive assessment of the Borrower, the Borrower, by its acceptance of this facility irrevocably consents to the Bank to make any enquiry’s with any Bank, Financial Institution, or Tanzania Bankers Associations approved Credit Reference Bureau in Tanzania to confirm any information provided by the Borrower or in seeking further information about the Borrower."))
            If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblTwentyC", Language.getMessage(mstrModuleName, 48, "(c)"))
                Call ReportFunction.TextChange(objRpt, "txtTwentyDesc3", Language.getMessage(mstrModuleName, 49, "The Borrower shall not be entitled to assign all or any part of its rights, obligations and/or benefits hereunder without the prior consent in writing of the Bank. This Letter shall be assignable by the Bank and remain valid and effective in all respects in favour of any assignee, transferee or other successor in title of the Bank in the same manner as if such assignee transferee or other successor in title has been named in this letter as a party instead of or in an addition to the Bank."))
            End If

            Call ReportFunction.TextChange(objRpt, "lblTwentyOne", Language.getMessage(mstrModuleName, 48, "21.0"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyOne", Language.getMessage(mstrModuleName, 49, "GOVERNING LAW AND JURISDICTION:-"))
            Call ReportFunction.TextChange(objRpt, "lblTwentyOneA", Language.getMessage(mstrModuleName, 48, "(a)"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyOneDescA", Language.getMessage(mstrModuleName, 48, "This Agreement is governed by and shall be construed in accordance with the law in Tanzania "))
            Call ReportFunction.TextChange(objRpt, "lblTwentyOneB", Language.getMessage(mstrModuleName, 48, "(b)"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyOneDescB", Language.getMessage(mstrModuleName, 48, "The Borrower irrevocably agrees for the exclusive benefit of the Bank that the High Court of Tanzania Commercial Court Division shall have jurisdiction to hear and determine any suit action or proceeding and to settle any dispute which may arise out of or in connection with this Agreement or any other document relating to or incidental to this Agreement and for such purposes irrevocably submits to the non-exclusive jurisdiction of such court."))
            Call ReportFunction.TextChange(objRpt, "lblTwentyOneC", Language.getMessage(mstrModuleName, 48, "(c)"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyOneDescC", Language.getMessage(mstrModuleName, 48, "Nothing contained in this Clause shall limit the right of the Bank to take proceedings against the Borrower in any other court of competent jurisdiction nor shall the taking of any such proceedings in one or more jurisdictions preclude the taking of proceedings in any other jurisdiction either concurrently or not (unless precluded by applicable law)."))
            Call ReportFunction.TextChange(objRpt, "lblTwentyOneD", Language.getMessage(mstrModuleName, 48, "(d)"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyOneDescD", Language.getMessage(mstrModuleName, 48, "The Borrower irrevocably waives any objection which it may have now or in the future to the High Court of Tanzania Commercial Court Division being nominated for the purpose of this Clause on the grounds of venue or otherwise and agrees not to claim that any such court is not a convenient or appropriate forum."))

            Call ReportFunction.TextChange(objRpt, "lblTwentyTwo", Language.getMessage(mstrModuleName, 48, "22.0"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyTwo", Language.getMessage(mstrModuleName, 49, "WAIVER NOT TO PREJUDICE BANK’S RIGHTS:-"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyTwoDesc", Language.getMessage(mstrModuleName, 49, "The Bank may as it deems fit neglect or forbear to enforce any of the terms of this Banking Facility Letter or waive such conditions of any breach of the Borrower or the same without prejudice to its right at any time afterwards to act strictly in accordance with the originally agreed terms in respect of the existing or subsequent breach."))

            Call ReportFunction.TextChange(objRpt, "lblTwentyThree", Language.getMessage(mstrModuleName, 48, "23.0"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyThree", Language.getMessage(mstrModuleName, 49, "AVAILABILITY:-"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyThreeDesc", Language.getMessage(mstrModuleName, 49, "The availability of this Facility is at all times subject to the Bank’s compliance in such manner as it thinks fit with any and all restrictions rules and regulations of the Central Bank of Tanzania, NMB Bank Plc Staff Manual or any other applicable regulatory authority from time to time in force and all terms and conditions hereof remain subject to any directions of the Central Bank of Tanzania as advised to the Bank from time to time."))

            Call ReportFunction.TextChange(objRpt, "lblTwentyFour", Language.getMessage(mstrModuleName, 48, "24.0"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyFour", Language.getMessage(mstrModuleName, 49, "PAYMENT ON DEMAND:-"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyFourDesc", Language.getMessage(mstrModuleName, 49, "In terms of normal banking practice, the Facility or part of them may be recalled by the Bank by written notice to that effect, payable either upon demand or within a period stated in the notice in which event the Facilities in question are canceled and any liability to the Bank becomes payable either forthwith or on the date stated in the demand, as the case may be:"))

            Call ReportFunction.TextChange(objRpt, "lblTwentyFive", Language.getMessage(mstrModuleName, 48, "25.0"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyFive", Language.getMessage(mstrModuleName, 49, "SECURITIES:-"))
            If mintLoanTypeId = enLoanType.General_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtTwentyFiveDesc", Language.getMessage(mstrModuleName, 49, "N/A"))
            ElseIf mintLoanTypeId = enLoanType.Car_Loan Then
                Call ReportFunction.TextChange(objRpt, "lblTwentyFiveA", Language.getMessage(mstrModuleName, 105, "(a)"))
                Call ReportFunction.TextChange(objRpt, "txtTwentyFiveADesc", Language.getMessage(mstrModuleName, 106, "All securities to be taken by the Bank:"))
                Call ReportFunction.TextChange(objRpt, "lblTwentyFiveAI", Language.getMessage(mstrModuleName, 107, "i)"))
                Call ReportFunction.TextChange(objRpt, "txtTwentyFiveAIDesc", Language.getMessage(mstrModuleName, 108, "shall be in a form agreed by the Bank."))
                Call ReportFunction.TextChange(objRpt, "lblTwentyFiveAII", Language.getMessage(mstrModuleName, 109, "ii)"))
                Call ReportFunction.TextChange(objRpt, "txtTwentyFiveAIIDesc", Language.getMessage(mstrModuleName, 108, "shall be prepared, executed and perfected at the borrower’s cost by the Bank’s choice."))
            End If

            Call ReportFunction.TextChange(objRpt, "lblTwentySix", Language.getMessage(mstrModuleName, 48, "26.0"))
            Call ReportFunction.TextChange(objRpt, "txtTwentySix", Language.getMessage(mstrModuleName, 49, "GOVERNMENT FEES, LEVIES, ETC:-"))
            Call ReportFunction.TextChange(objRpt, "txtTwentySixDesc", Language.getMessage(mstrModuleName, 49, "Any government, local authority or other fees levies taxes or other imposts in relation to these facilities the security documentation or the repayment of the facilities will be for the account of the Borrower."))

            Call ReportFunction.TextChange(objRpt, "lblTwentySeven", Language.getMessage(mstrModuleName, 48, "27.0"))
            Call ReportFunction.TextChange(objRpt, "txtTwentySeven", Language.getMessage(mstrModuleName, 49, "DISCOUNT (WHERE APPLICABLE):-"))
            Call ReportFunction.TextChange(objRpt, "txtTwentySevenDesc", Language.getMessage(mstrModuleName, 49, "The relevant discount charge(s) will be deducted from the proceeds of cash bill discounted and therefore only the balance will be credited to the Borrower’s account."))

            Call ReportFunction.TextChange(objRpt, "lblTwentyEight", Language.getMessage(mstrModuleName, 48, "28.0"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyEight", Language.getMessage(mstrModuleName, 49, "ACCEPTANCE:-"))
            Call ReportFunction.TextChange(objRpt, "txtTwentyEightDesc", Language.getMessage(mstrModuleName, 49, "This facility will be reviewed semi-annually except where otherwise expressly stated. All amendments shall be advised by a Banking Offer letter supplemental to this Banking Offer Letter (""Supplementary Banking Offer Letter""). This Banking Offer Letter once accepted otherwise remains in full force and effect, subject to termination as aforesaid, throughout the life of the facilities enumerated herein provided the facilities or any part thereof are being utilized."))

            If mintLoanTypeId = enLoanType.General_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtSignatureAcceptance", Language.getMessage(mstrModuleName, 49, "Please signify acceptance of the foregoing by signing and returning to us the copy of this letter."))
            ElseIf mintLoanTypeId = enLoanType.Car_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtSignatureAcceptance", Language.getMessage(mstrModuleName, 110, "Please signify acceptance of the foregoing by signing the acceptance form and initial each page then return to us the copy of this letter within 30 days."))
            ElseIf mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Construction_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage_Loan Then
                Call ReportFunction.TextChange(objRpt, "txtSignatureAcceptance", Language.getMessage(mstrModuleName, 210, "Please signify acceptance of the foregoing by signing the acceptance form and returning to the Bank the copy of this letter within 30 days."))
            End If

            Call ReportFunction.TextChange(objRpt, "lblBank", Language.getMessage(mstrModuleName, 102, "Bank"))
            Call ReportFunction.TextChange(objRpt, "lblBorrower2", Language.getMessage(mstrModuleName, 103, "Borrower"))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function



#End Region

End Class
