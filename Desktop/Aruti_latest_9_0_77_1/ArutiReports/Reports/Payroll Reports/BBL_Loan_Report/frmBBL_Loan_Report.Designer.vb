﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBBL_Loan_Report
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.EZeeCollapsibleContainer1 = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radGrossSalary = New System.Windows.Forms.RadioButton
        Me.gbMonthlySal = New eZee.Common.eZeeLine
        Me.radBasicSalary = New System.Windows.Forms.RadioButton
        Me.chkIncludeInactiveEmp = New System.Windows.Forms.CheckBox
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployeeName = New System.Windows.Forms.ComboBox
        Me.lblEmployeeName = New System.Windows.Forms.Label
        Me.objLoanSchemeSearch = New eZee.Common.eZeeGradientButton
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.lblLoanApplicationNo = New System.Windows.Forms.Label
        Me.cboLoanApplicationNo = New System.Windows.Forms.ComboBox
        Me.lblLoanVoucherNo = New System.Windows.Forms.Label
        Me.cboLoanVoucherNo = New System.Windows.Forms.ComboBox
        Me.EZeeCollapsibleContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 439)
        Me.NavPanel.Size = New System.Drawing.Size(700, 55)
        '
        'EZeeCollapsibleContainer1
        '
        Me.EZeeCollapsibleContainer1.BorderColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.Checked = False
        Me.EZeeCollapsibleContainer1.CollapseAllExceptThis = False
        Me.EZeeCollapsibleContainer1.CollapsedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapsedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.CollapseOnLoad = False
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblLoanVoucherNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboLoanVoucherNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblLoanApplicationNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboLoanApplicationNo)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objLoanSchemeSearch)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblLoanScheme)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboLoanScheme)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.radGrossSalary)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.gbMonthlySal)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.radBasicSalary)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.chkIncludeInactiveEmp)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.objbtnSearchEmployee)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.cboEmployeeName)
        Me.EZeeCollapsibleContainer1.Controls.Add(Me.lblEmployeeName)
        Me.EZeeCollapsibleContainer1.ExpandedHoverImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedNormalImage = Nothing
        Me.EZeeCollapsibleContainer1.ExpandedPressedImage = Nothing
        Me.EZeeCollapsibleContainer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeCollapsibleContainer1.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.EZeeCollapsibleContainer1.HeaderHeight = 25
        Me.EZeeCollapsibleContainer1.HeaderMessage = ""
        Me.EZeeCollapsibleContainer1.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.EZeeCollapsibleContainer1.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.EZeeCollapsibleContainer1.HeightOnCollapse = 0
        Me.EZeeCollapsibleContainer1.LeftTextSpace = 0
        Me.EZeeCollapsibleContainer1.Location = New System.Drawing.Point(12, 66)
        Me.EZeeCollapsibleContainer1.Name = "EZeeCollapsibleContainer1"
        Me.EZeeCollapsibleContainer1.OpenHeight = 300
        Me.EZeeCollapsibleContainer1.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.EZeeCollapsibleContainer1.ShowBorder = True
        Me.EZeeCollapsibleContainer1.ShowCheckBox = False
        Me.EZeeCollapsibleContainer1.ShowCollapseButton = False
        Me.EZeeCollapsibleContainer1.ShowDefaultBorderColor = True
        Me.EZeeCollapsibleContainer1.ShowDownButton = False
        Me.EZeeCollapsibleContainer1.ShowHeader = True
        Me.EZeeCollapsibleContainer1.Size = New System.Drawing.Size(442, 230)
        Me.EZeeCollapsibleContainer1.TabIndex = 2
        Me.EZeeCollapsibleContainer1.Temp = 0
        Me.EZeeCollapsibleContainer1.Text = "Filter Criteria"
        Me.EZeeCollapsibleContainer1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radGrossSalary
        '
        Me.radGrossSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radGrossSalary.Location = New System.Drawing.Point(143, 205)
        Me.radGrossSalary.Name = "radGrossSalary"
        Me.radGrossSalary.Size = New System.Drawing.Size(259, 17)
        Me.radGrossSalary.TabIndex = 11
        Me.radGrossSalary.Text = "Display Monthly Gross Amount"
        Me.radGrossSalary.UseVisualStyleBackColor = True
        '
        'gbMonthlySal
        '
        Me.gbMonthlySal.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.gbMonthlySal.Location = New System.Drawing.Point(117, 162)
        Me.gbMonthlySal.Name = "gbMonthlySal"
        Me.gbMonthlySal.Size = New System.Drawing.Size(285, 17)
        Me.gbMonthlySal.TabIndex = 11
        Me.gbMonthlySal.Text = "Monthly Salary Display Settings"
        Me.gbMonthlySal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radBasicSalary
        '
        Me.radBasicSalary.Checked = True
        Me.radBasicSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radBasicSalary.Location = New System.Drawing.Point(143, 182)
        Me.radBasicSalary.Name = "radBasicSalary"
        Me.radBasicSalary.Size = New System.Drawing.Size(259, 17)
        Me.radBasicSalary.TabIndex = 11
        Me.radBasicSalary.TabStop = True
        Me.radBasicSalary.Text = "Display Monthly Basic Salary"
        Me.radBasicSalary.UseVisualStyleBackColor = True
        '
        'chkIncludeInactiveEmp
        '
        Me.chkIncludeInactiveEmp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeInactiveEmp.Location = New System.Drawing.Point(117, 139)
        Me.chkIncludeInactiveEmp.Name = "chkIncludeInactiveEmp"
        Me.chkIncludeInactiveEmp.Size = New System.Drawing.Size(285, 16)
        Me.chkIncludeInactiveEmp.TabIndex = 9
        Me.chkIncludeInactiveEmp.Text = "Include inactive employee"
        Me.chkIncludeInactiveEmp.UseVisualStyleBackColor = True
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(408, 32)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 2
        '
        'cboEmployeeName
        '
        Me.cboEmployeeName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployeeName.DropDownWidth = 150
        Me.cboEmployeeName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployeeName.FormattingEnabled = True
        Me.cboEmployeeName.Location = New System.Drawing.Point(117, 32)
        Me.cboEmployeeName.Name = "cboEmployeeName"
        Me.cboEmployeeName.Size = New System.Drawing.Size(285, 21)
        Me.cboEmployeeName.TabIndex = 0
        '
        'lblEmployeeName
        '
        Me.lblEmployeeName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployeeName.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployeeName.Name = "lblEmployeeName"
        Me.lblEmployeeName.Size = New System.Drawing.Size(72, 13)
        Me.lblEmployeeName.TabIndex = 0
        Me.lblEmployeeName.Text = "Emp. Name"
        '
        'objLoanSchemeSearch
        '
        Me.objLoanSchemeSearch.BackColor = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objLoanSchemeSearch.BorderSelected = False
        Me.objLoanSchemeSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objLoanSchemeSearch.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objLoanSchemeSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objLoanSchemeSearch.Location = New System.Drawing.Point(408, 59)
        Me.objLoanSchemeSearch.Name = "objLoanSchemeSearch"
        Me.objLoanSchemeSearch.Size = New System.Drawing.Size(21, 21)
        Me.objLoanSchemeSearch.TabIndex = 234
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(8, 62)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(77, 15)
        Me.lblLoanScheme.TabIndex = 232
        Me.lblLoanScheme.Text = "Loan Scheme"
        Me.lblLoanScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Location = New System.Drawing.Point(117, 59)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(285, 21)
        Me.cboLoanScheme.TabIndex = 233
        '
        'lblLoanApplicationNo
        '
        Me.lblLoanApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanApplicationNo.Location = New System.Drawing.Point(8, 89)
        Me.lblLoanApplicationNo.Name = "lblLoanApplicationNo"
        Me.lblLoanApplicationNo.Size = New System.Drawing.Size(108, 15)
        Me.lblLoanApplicationNo.TabIndex = 235
        Me.lblLoanApplicationNo.Text = "Loan Application No"
        Me.lblLoanApplicationNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanApplicationNo
        '
        Me.cboLoanApplicationNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanApplicationNo.FormattingEnabled = True
        Me.cboLoanApplicationNo.Location = New System.Drawing.Point(117, 86)
        Me.cboLoanApplicationNo.Name = "cboLoanApplicationNo"
        Me.cboLoanApplicationNo.Size = New System.Drawing.Size(285, 21)
        Me.cboLoanApplicationNo.TabIndex = 236
        '
        'lblLoanVoucherNo
        '
        Me.lblLoanVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanVoucherNo.Location = New System.Drawing.Point(8, 116)
        Me.lblLoanVoucherNo.Name = "lblLoanVoucherNo"
        Me.lblLoanVoucherNo.Size = New System.Drawing.Size(89, 15)
        Me.lblLoanVoucherNo.TabIndex = 237
        Me.lblLoanVoucherNo.Text = "Loan Voucher No"
        Me.lblLoanVoucherNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanVoucherNo
        '
        Me.cboLoanVoucherNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanVoucherNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanVoucherNo.FormattingEnabled = True
        Me.cboLoanVoucherNo.Location = New System.Drawing.Point(117, 113)
        Me.cboLoanVoucherNo.Name = "cboLoanVoucherNo"
        Me.cboLoanVoucherNo.Size = New System.Drawing.Size(285, 21)
        Me.cboLoanVoucherNo.TabIndex = 238
        '
        'frmBBL_Loan_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(700, 494)
        Me.Controls.Add(Me.EZeeCollapsibleContainer1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBBL_Loan_Report"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.EZeeCollapsibleContainer1, 0)
        Me.EZeeCollapsibleContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeCollapsibleContainer1 As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents chkIncludeInactiveEmp As System.Windows.Forms.CheckBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployeeName As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployeeName As System.Windows.Forms.Label
    Friend WithEvents radGrossSalary As System.Windows.Forms.RadioButton
    Friend WithEvents gbMonthlySal As eZee.Common.eZeeLine
    Friend WithEvents radBasicSalary As System.Windows.Forms.RadioButton
    Friend WithEvents lblLoanVoucherNo As System.Windows.Forms.Label
    Friend WithEvents cboLoanVoucherNo As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanApplicationNo As System.Windows.Forms.Label
    Friend WithEvents cboLoanApplicationNo As System.Windows.Forms.ComboBox
    Friend WithEvents objLoanSchemeSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
End Class
