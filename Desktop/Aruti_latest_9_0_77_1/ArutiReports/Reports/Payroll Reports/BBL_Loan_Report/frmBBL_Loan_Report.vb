#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib

#End Region

Public Class frmBBL_Loan_Report

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmBBL_Loan_Report"
    Private objBBL As clsBBL_Loan_Report
    Private mintEmpId As Integer = 0
    'Sohail (29 Apr 2020) -- Start
    'Ifakara Enhancement # 0004668 : Advance Approve Form.
    Private mblnIsLoan As Boolean = True
    'Sohail (29 Apr 2020) -- End
    'Hemant (12 Nov 2021) -- Start
    'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
    Private mintProcessPendingLoanunkId As Integer = -1
    Private mintLoanSchemeId As Integer = -1
    Private mblnIsFromLoanAdvanceListScreen As Boolean = False
    'Hemant (12 Nov 2021) -- End
#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    'Hemant (12 Nov 2021) -- Start
    'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
    Public WriteOnly Property _ProcessPendingLoanunkId() As Integer
        Set(ByVal value As Integer)
            mintProcessPendingLoanunkId = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeId() As Integer
        Set(ByVal value As Integer)
            mintLoanSchemeId = value
        End Set
    End Property

    Public WriteOnly Property _IsFromLoanAdvanceListScreen() As Boolean
        Set(ByVal value As Boolean)
            mblnIsFromLoanAdvanceListScreen = value
        End Set
    End Property
    'Hemant (12 Nov 2021) -- End

    'Sohail (29 Apr 2020) -- Start
    'Ifakara Enhancement # 0004668 : Advance Approve Form.
    Public WriteOnly Property _IsLoan() As Boolean
        Set(ByVal value As Boolean)
            mblnIsLoan = value
        End Set
    End Property
    'Sohail (29 Apr 2020) -- End

#End Region

#Region " Constructor "

    Public Sub New()
        objBBL = New clsBBL_Loan_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objBBL.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Public Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        'Hemant (12 Nov 2021) -- Start
        'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
        Dim objLoanScheme As New clsLoan_Scheme
        'Hemant (12 Nov 2021) -- End
        Try

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))

            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'Anjan [10 June 2015] -- End

            With cboEmployeeName
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            'Hemant (12 Nov 2021) -- Start
            'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
            dsList = objLoanScheme.getComboList(True, "LoanScheme")
            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .SelectedValue = 0
            End With
            'Hemant (12 Nov 2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            dsList = Nothing
            objEmp = Nothing
            'Hemant (12 Nov 2021) -- Start
            'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
            objLoanScheme = Nothing
            'Hemant (12 Nov 2021) -- End
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            If mintEmpId > 0 Then
                Call Set_Logo(Me, gApplicationType)
                cboEmployeeName.SelectedValue = mintEmpId
                cboEmployeeName.Enabled = False
                objbtnSearchEmployee.Enabled = False
            Else
                cboEmployeeName.Enabled = True
                objbtnSearchEmployee.Enabled = True
                cboEmployeeName.SelectedValue = 0
            End If
            chkIncludeInactiveEmp.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            'Hemant (12 Nov 2021) -- Start
            'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
            If CInt(cboEmployeeName.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please Select Employee Name."), enMsgBoxStyle.Information)
                cboEmployeeName.Focus()
                Exit Function
            End If

            If CInt(cboLoanScheme.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Loan Scheme."), enMsgBoxStyle.Information)
                cboLoanScheme.Focus()
                Exit Function
            End If

            If CInt(cboLoanApplicationNo.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Loan Application No."), enMsgBoxStyle.Information)
                cboLoanApplicationNo.Focus()
                Exit Function
            End If

            If CInt(cboLoanVoucherNo.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select Loan Voucher No."), enMsgBoxStyle.Information)
                cboLoanVoucherNo.Focus()
                Exit Function
            End If
            'Hemant (12 Nov 2021) -- End


            objBBL.SetDefaultValue()

            objBBL._EmployeeId = cboEmployeeName.SelectedValue
            objBBL._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            'Hemant (12 Nov 2021) -- Start
            'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
            objBBL._LoanSchemeId = CInt(cboLoanScheme.SelectedValue)
            objBBL._ProcessPendingLoanunkId = CInt(cboLoanApplicationNo.SelectedValue)
            objBBL._LoanApplicationNo = CStr(cboLoanApplicationNo.Text)
            objBBL._LoanVoucherNo = CStr(cboLoanVoucherNo.Text)
            'Hemant (12 Nov 2021) -- End
            'Sohail (29 Apr 2020) -- Start
            'Ifakara Enhancement # 0004668 : Advance Approve Form.
            objBBL._IsLoan = mblnIsLoan
            'Sohail (29 Apr 2020) -- End

            If radBasicSalary.Checked = True Then
                objBBL._DisplayModeSetting = clsBBL_Loan_Report.enDisplayMode.BASIC_SALARY
            ElseIf radGrossSalary.Checked = True Then
                objBBL._DisplayModeSetting = clsBBL_Loan_Report.enDisplayMode.GROSS_SALARY
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmBBL_Loan_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBBL = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBBL_Loan_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBBL_Loan_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objBBL._ReportName
            Me._Message = objBBL._ReportDesc
            Me.Text = objBBL._ReportName
            Call FillCombo()
            Call ResetValue()
            'Hemant (12 Nov 2021) -- Start
            'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
            If mblnIsFromLoanAdvanceListScreen = True Then
                cboLoanScheme.SelectedValue = mintLoanSchemeId
                cboLoanApplicationNo.SelectedValue = mintProcessPendingLoanunkId
                cboLoanVoucherNo.SelectedValue = mintProcessPendingLoanunkId
                cboLoanScheme.Enabled = False
                cboLoanApplicationNo.Enabled = False
                cboLoanVoucherNo.Enabled = False
            End If
            'Hemant (12 Nov 2021) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBBL_Loan_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBBL_Loan_Report_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmBBL_Loan_Report_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBBL_Loan_Report_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBBL_Loan_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBBL_Loan_Report_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub frmBBL_Loan_Report_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objBBL._DbStartDate = FinancialYear._Object._Database_Start_Date
            'objBBL.generateReport(0, e.Type, enExportAction.None)
            objBBL.generateReportNew(FinancialYear._Object._DatabaseName, _
                                     User._Object._Userunkid, _
                                     FinancialYear._Object._YearUnkid, _
                                     Company._Object._Companyunkid, _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                     ConfigParameter._Object._UserAccessModeSetting, _
                                     True, _
                                     ConfigParameter._Object._ExportReportPath, _
                                     ConfigParameter._Object._OpenAfterExport, _
                                     0, e.Type, enExportAction.None)
            'Nilay (10-Oct-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBBL_Loan_Report_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBBL_Loan_Report_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objBBL._DbStartDate = FinancialYear._Object._Database_Start_Date
            'objBBL.generateReport(0, enPrintAction.None, e.Type)
            objBBL.generateReportNew(FinancialYear._Object._DatabaseName, _
                                  User._Object._Userunkid, _
                                  FinancialYear._Object._YearUnkid, _
                                  Company._Object._Companyunkid, _
                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                  eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                  ConfigParameter._Object._UserAccessModeSetting, _
                                  True, _
                                  ConfigParameter._Object._ExportReportPath, _
                                  ConfigParameter._Object._OpenAfterExport, _
                                  0, enPrintAction.None, e.Type)
            'Nilay (10-Oct-2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBBL_Loan_Report_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBBL_Loan_Report_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBBL_Loan_Report_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBBL_Loan_Report_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBBL_Loan_Report_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployeeName.DataSource
            frm.DisplayMember = cboEmployeeName.DisplayMember
            frm.ValueMember = cboEmployeeName.ValueMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployeeName.SelectedValue = frm.SelectedValue
                cboEmployeeName.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub frmBBL_Loan_Report_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBBL_Loan_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsBBL_Loan_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmBBL_Loan_Report_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End

    'Hemant (12 Nov 2021) -- Start
    'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
    Private Sub objLoanSchemeSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objLoanSchemeSearch.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = CType(cboLoanScheme.DataSource, DataTable)
            frm.DisplayMember = cboLoanScheme.DisplayMember
            frm.ValueMember = cboLoanScheme.ValueMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboLoanScheme.SelectedValue = frm.SelectedValue
                cboLoanScheme.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objLoanSchemeSearch_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Hemant (12 Nov 2021) -- End

#End Region

#Region " Combo Box's Events "
    'Hemant (12 Nov 2021) -- Start
    'ISSUE(REA) : Some assigned loans cannot be seen on the loan approve report.
    Private Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployeeName.SelectedIndexChanged, _
                                                                                                                       cboLoanScheme.SelectedIndexChanged
        Dim objProcessPendingLoan As New clsProcess_pending_loan
        Dim objLoanAdvance As New clsLoan_Advance
        Dim dsList As New DataSet
        Try
            If cboEmployeeName.SelectedValue > 0 AndAlso cboLoanScheme.SelectedValue > 0 Then
                dsList = objProcessPendingLoan.getLoanApplicationNumberList(True, "LoanApplication", CInt(cboEmployeeName.SelectedValue), CInt(cboLoanScheme.SelectedValue))
                With cboLoanApplicationNo
                    .ValueMember = "processpendingloanunkid"
                    .DisplayMember = "application_no"
                    .DataSource = dsList.Tables("LoanApplication")
                    .SelectedValue = 0
                End With

                dsList = objLoanAdvance.getLoanVoucherNumberList(True, "LoanVoucher", CInt(cboEmployeeName.SelectedValue), CInt(cboLoanScheme.SelectedValue))
                With cboLoanVoucherNo
                    .ValueMember = "processpendingloanunkid"
                    .DisplayMember = "loanvoucher_no"
                    .DataSource = dsList.Tables("LoanVoucher")
                    .SelectedValue = 0
                End With
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_SelectedIndexChanged", mstrModuleName)
        Finally
            dsList = Nothing
            objProcessPendingLoan = Nothing
            objLoanAdvance = Nothing
        End Try
    End Sub

    Private Sub cboLoanApplicationNo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoanApplicationNo.SelectedIndexChanged
        Try
            If cboLoanApplicationNo.SelectedValue > 0 Then
                RemoveHandler cboLoanVoucherNo.SelectedIndexChanged, AddressOf cboLoanVoucherNo_SelectedIndexChanged
                cboLoanVoucherNo.SelectedValue = CInt(cboLoanApplicationNo.SelectedValue)
                AddHandler cboLoanVoucherNo.SelectedIndexChanged, AddressOf cboLoanVoucherNo_SelectedIndexChanged
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanApplicationNo_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub


    Private Sub cboLoanVoucherNo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoanVoucherNo.SelectedIndexChanged
        Try
            If cboLoanVoucherNo.SelectedValue > 0 Then
                RemoveHandler cboLoanApplicationNo.SelectedIndexChanged, AddressOf cboLoanApplicationNo_SelectedIndexChanged
                cboLoanApplicationNo.SelectedValue = CInt(cboLoanVoucherNo.SelectedValue)
                AddHandler cboLoanApplicationNo.SelectedIndexChanged, AddressOf cboLoanApplicationNo_SelectedIndexChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanVoucherNo_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (12 Nov 2021) -- End
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
			Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.Name, Me.chkIncludeInactiveEmp.Text)
			Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
			Me.radGrossSalary.Text = Language._Object.getCaption(Me.radGrossSalary.Name, Me.radGrossSalary.Text)
			Me.gbMonthlySal.Text = Language._Object.getCaption(Me.gbMonthlySal.Name, Me.gbMonthlySal.Text)
			Me.radBasicSalary.Text = Language._Object.getCaption(Me.radBasicSalary.Name, Me.radBasicSalary.Text)
			Me.lblLoanVoucherNo.Text = Language._Object.getCaption(Me.lblLoanVoucherNo.Name, Me.lblLoanVoucherNo.Text)
			Me.lblLoanApplicationNo.Text = Language._Object.getCaption(Me.lblLoanApplicationNo.Name, Me.lblLoanApplicationNo.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select Employee Name.")
			Language.setMessage(mstrModuleName, 2, "Please Select Loan Scheme.")
			Language.setMessage(mstrModuleName, 3, "Please Select Loan Application No.")
			Language.setMessage(mstrModuleName, 4, "Please Select Loan Voucher No.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
