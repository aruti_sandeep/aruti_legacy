﻿'************************************************************************************************************************************
'Class Name : clsFlexcubeLoanFormReport.vb
'Purpose    :
'Date       : 25/11/2022
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

Public Class clsFlexcubeLoanFormReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsFlexcubeLoanFormReport"
    Private mstrReportId As String = enArutiReport.Flexcube_Loan_Form_Report
    Dim objDataOperation As clsDataOperation

#Region "Enum"
    Enum enLoanType
        General_Loan = 1
        Car_Loan = 2
        Refinancing_Mortgage_Loan = 3
        Purchase_Mortgage = 4
        Construction_Mortgage = 5
    End Enum
#End Region

#Region " Constructor "
    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private variables "

    Private mintLoanTypeId As Integer = 0
    Private mstrLoanTypeName As String = ""

    Private mintEmployeeUnkId As Integer = 0
    Private mstrEmployeeName As String = ""
    Private mintLoanSchemeUnkId As Integer = 0
    Private mstrLoanSchemeName As String = ""
    Private mintProcessPendingLoanUnkid As Integer = 0
    Private mstrApplicationNo As String = ""
    Private mintIdentityUnkId As Integer = 0
    Private mstrIdentityName As String = ""

    Private mstrOrderByQuery As String = ""
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
#End Region

#Region " Properties "

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _LoanTypeId() As Integer
        Set(ByVal value As Integer)
            mintLoanTypeId = value
        End Set
    End Property

    Public WriteOnly Property _LoanTypeName() As String
        Set(ByVal value As String)
            mstrLoanTypeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeUnkId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeUnkId = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeUnkId() As Integer
        Set(ByVal value As Integer)
            mintLoanSchemeUnkId = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeName() As String
        Set(ByVal value As String)
            mstrLoanSchemeName = value
        End Set
    End Property

    Public WriteOnly Property _ProcessPendingLoanUnkid() As Integer
        Set(ByVal value As Integer)
            mintProcessPendingLoanUnkid = value
        End Set
    End Property

    Public WriteOnly Property _ApplicationNo() As String
        Set(ByVal value As String)
            mstrApplicationNo = value
        End Set
    End Property

    Public WriteOnly Property _IdentityUnkId() As Integer
        Set(ByVal value As Integer)
            mintIdentityUnkId = value
        End Set
    End Property

    Public WriteOnly Property _IdentityName() As String
        Set(ByVal value As String)
            mstrIdentityName = value
        End Set
    End Property

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintLoanTypeId = 0
            mstrLoanTypeName = ""

            mintEmployeeUnkId = 0
            mstrEmployeeName = ""
            mintLoanSchemeUnkId = 0
            mstrLoanSchemeName = ""
            mintProcessPendingLoanUnkid = 0
            mstrApplicationNo = ""

            mintIdentityUnkId = 0
            mstrIdentityName = ""

            mstrOrderByQuery = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        mstrOrderByQuery = ""

        Try

            If mintEmployeeUnkId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkId)
                Me._FilterQuery &= " AND lnloan_process_pending_loan.employeeunkid = @employeeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintLoanSchemeUnkId > 0 Then
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeUnkId)
                Me._FilterQuery &= " AND  lnloan_process_pending_loan.loanschemeunkid = @loanschemeunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 32, "Loan Scheme :") & " " & mstrLoanSchemeName & " "
            End If

            If mintProcessPendingLoanUnkid > 0 Then
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessPendingLoanUnkid)
                Me._FilterQuery &= " AND lnloan_process_pending_loan.processpendingloanunkid = @processpendingloanunkid "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 31, "Application No :") & " " & mstrApplicationNo & " "
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, Optional ByVal intBaseCurrencyUnkid As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function NumberToWords(ByVal doubleNumber As Double) As String
        Dim beforeFloatingPoint = CInt(Math.Floor(doubleNumber))
        Dim beforeFloatingPointWord = String.Format("{0}", NumberToString(beforeFloatingPoint))
        Dim afterFloatingPointWord = String.Format("{0} cents ", GroupToWords(CInt(((doubleNumber - beforeFloatingPoint) * 100))))

        If CInt(((doubleNumber - beforeFloatingPoint) * 100)) > 0 Then
            Return String.Format("{0} and {1}", beforeFloatingPointWord, afterFloatingPointWord)
        Else
            Return String.Format("{0}", beforeFloatingPointWord)
        End If
    End Function

    Public Function NumberToString(ByVal num_str As String) As String ', Optional ByVal use_us_group_names As Boolean = True
        ' Get the appropiate group names.
        Dim groups() As String
        'If use_us_group_names Then
        '    groups = New String() {"", "thousand", "million", "billion", "trillion", "quadrillion", "quintillion", "sextillion", "septillion", "octillion", "nonillion", "decillion", "undecillion", "duodecillion", "tredecillion", "quattuordecillion", "quindecillion", "sexdecillion", "septendecillion", "octodecillion", "novemdecillion", "vigintillion"}
        'Else
        groups = New String() {"", "Thousand", "Million", "Milliard", "Billion", "1000 Billion", "Trillion", "1000 Trillion", "Quadrillion", "1000 Quadrillion", "Quintillion", "1000 Quintillion", "Sextillion", "1000 Sextillion", "Septillion", "1000 Septillion", "Octillion", "1000 Octillion", "Nonillion", "1000 Nonillion", "Decillion", "1000 Decillion"}
        'End If

        ' Clean the string a bit.
        ' Remove "$", ",", leading zeros, and
        ' anything after a decimal point.
        Const CURRENCY As String = "$"
        Const SEPARATOR As String = ","
        Const DECIMAL_POINT As String = "."
        num_str = num_str.Replace(CURRENCY, "").Replace(SEPARATOR, "")
        num_str = num_str.TrimStart(New Char() {"0"c})
        Dim pos As Integer = num_str.IndexOf(DECIMAL_POINT)
        If pos = 0 Then
            Return "zero"
        ElseIf pos > 0 Then
            num_str = num_str.Substring(0, pos - 1)
        End If

        ' See how many groups there will be.
        Dim num_groups As Integer = (num_str.Length + 2) \ 3

        ' Pad so length is a multiple of 3.
        num_str = num_str.PadLeft(num_groups * 3, " "c)

        ' Process the groups, largest first.
        Dim result As String = ""
        Dim group_num As Integer
        For group_num = num_groups - 1 To 0 Step -1
            ' Get the next three digits.
            Dim group_str As String = num_str.Substring(0, 3)
            num_str = num_str.Substring(3)
            Dim group_value As Integer = CInt(group_str)

            ' Convert the group into words.
            If group_value > 0 Then
                If group_num >= groups.Length Then
                    result &= GroupToWords(group_value) & _
                        " ?, "
                Else
                    result &= GroupToWords(group_value) & _
                        " " & groups(group_num) & ", "
                End If
            End If
        Next group_num

        ' Remove the trailing ", ".
        If result.EndsWith(", ") Then
            result = result.Substring(0, result.Length - 2)
        End If

        Return result.Trim()
    End Function

    Private Function GroupToWords(ByVal num As Integer) As String
        Static one_to_nineteen() As String = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen"}
        Static multiples_of_ten() As String = {"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"}

        ' If the number is 0, return an empty string.
        If num = 0 Then Return ""

        ' Handle the hundreds digit.
        Dim digit As Integer
        Dim result As String = ""
        If num > 99 Then
            digit = num \ 100
            num = num Mod 100
            result = one_to_nineteen(digit) & " hundred"
        End If

        ' If num = 0, we have hundreds only.
        If num = 0 Then Return result.Trim()

        ' See if the rest is less than 20.
        If num < 20 Then
            ' Look up the correct name.
            result &= " " & one_to_nineteen(num)
        Else
            ' Handle the tens digit.
            digit = num \ 10
            num = num Mod 10
            result &= " " & multiples_of_ten(digit - 2)

            ' Handle the final digit.
            If num > 0 Then
                result &= " " & one_to_nineteen(num)
            End If
        End If

        Return result.Trim()
    End Function

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Generate_DetailReport(ByVal xDatabaseName As String _
                                     , ByVal xUserUnkid As Integer _
                                     , ByVal xYearUnkid As Integer _
                                     , ByVal xCompanyUnkid As Integer _
                                     , ByVal xPeriodStart As Date _
                                     , ByVal xPeriodEnd As Date _
                                     , ByVal xUserModeSetting As String _
                                     , ByVal xOnlyApproved As Boolean _
                                     ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim StrQFilter As String = ""
        Dim dsList As New DataSet
        Dim rpt_LoanApprovers As ArutiReport.Designer.dsArutiReport
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try


            Company._Object._Companyunkid = xCompanyUnkid
            ConfigParameter._Object._Companyunkid = xCompanyUnkid

            User._Object._Userunkid = xUserUnkid

            Dim dsModule As DataSet = (New clsMasterData).GetModuleReference("List", Nothing, False)

            objDataOperation = New clsDataOperation

            objDataOperation.ClearParameters()


            StrQ = "SELECT " & _
                    "   ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' +ISNULL(hremployee_master.surname, '') AS EmployeeFullName " & _
                    "   ,hremployee_master.birthdate " & _
                    "   ,lnloan_process_pending_loan.loanschemeunkid " & _
                    "   ,lnloan_scheme_master.loanschemecategory_id " & _
                    "   ,hremployee_idinfo_tran.identity_no AS NationalID " & _
                    "   ,hremployee_master.appointeddate " & _
                    "   ,hremployee_master.employeecode " & _
                    "   ,CnfDt.date1 AS Confirmation_Date " & _
                    "   ,Bank.accountno AS accountno " & _
                    "   ,ISNULL(lnloan_process_pending_loan.loan_amount,0) AS loan_amount " & _
                    "   ,ISNULL(lnloan_process_pending_loan.istopup,0) AS istopup " & _
                    "   ,ISNULL(lnloan_process_pending_loan.emp_remark, 0) AS Purpose " & _
                    "   ,ISNULL(lnloan_scheme_master.loanschemecategory_id, 0) AS loanschemecategoryid " & _
                    "   ,ISNULL(lnloan_process_pending_loan.plot_no, '') AS plot_no " & _
                    "   ,ISNULL(lnloan_process_pending_loan.block_no, '') AS block_no " & _
                    "   ,ISNULL(lnloan_process_pending_loan.street, '') AS street " & _
                    "   ,ISNULL(lnloan_process_pending_loan.town_city, '') AS town_city " & _
                    "   ,ISNULL(lnloan_process_pending_loan.ct_no, '') AS ct_no " & _
                    "   ,ISNULL(lnloan_process_pending_loan.lo_no, '') AS lo_no " & _
                    "   ,ISNULL(lnloan_process_pending_loan.market_value, 0) AS market_value " & _
                    "   ,ISNULL(lnloan_process_pending_loan.fsv, 0) AS fsv " & _
                    "   ,ISNULL(lnloan_process_pending_loan.model, '') AS model " & _
                    "   ,ISNULL(lnloan_process_pending_loan.yom, '') AS yom " & _
                    "   ,ISNULL(lnloan_process_pending_loan.chassis_no, '') AS chassis_no " & _
                    "   ,ISNULL(lnloan_process_pending_loan.colour, '') AS colour " & _
                    "   ,ISNULL(lnloan_process_pending_loan.supplier, '') AS supplier " & _
                    " FROM lnloan_process_pending_loan " & _
                    " JOIN hremployee_master ON hremployee_master.employeeunkid = lnloan_process_pending_loan.employeeunkid " & _
                    " LEFT JOIN lnloan_scheme_master     ON lnloan_scheme_master.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid " & _
                    " LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = hremployee_master.employeeunkid	AND hremployee_idinfo_tran.idtypeunkid =  " & mintIdentityUnkId & " " & _
                    " LEFT JOIN " & _
                    " ( " & _
                    "    SELECT " & _
                    "         date1 " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_dates_tran " & _
                    "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodStart) & "' " & _
                    " )AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid AND CnfDt.rno = 1 " & _
                    " LEFT JOIN  " & _
                    " ( " & _
                    "       SELECT " & _
                    "            employeeunkid " & _
                    "           ,accountno " & _
                    "           ,DENSE_RANK() OVER (PARTITION BY premployee_bank_tran.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                    "       FROM premployee_bank_tran " & _
                    "       LEFT JOIN cfcommon_period_tran     ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "       WHERE ISNULL(isvoid, 0) = 0 " & _
                    "       AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    " ) AS Bank ON Bank.employeeunkid = hremployee_master.employeeunkid AND Bank.ROWNO = 1 " & _
                    " WHERE lnloan_process_pending_loan.isvoid = 0 "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_LoanApprovers = New ArutiReport.Designer.dsArutiReport
            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = dtRow.Item("EmployeeFullName")
                rpt_Row.Item("Column2") = CDate(dtRow.Item("birthdate")).ToShortDateString
                rpt_Row.Item("Column3") = dtRow.Item("NationalID")
                rpt_Row.Item("Column4") = CDate(dtRow.Item("appointeddate")).ToShortDateString()
                rpt_Row.Item("Column5") = dtRow.Item("employeecode")
                rpt_Row.Item("Column6") = CDate(dtRow.Item("Confirmation_Date")).ToShortDateString()
                rpt_Row.Item("Column11") = dtRow.Item("accountno")
                rpt_Row.Item("Column13") = CDec(dtRow.Item("loan_amount")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US")) & " /="
                rpt_Row.Item("Column14") = NumberToWords(CDec(dtRow.Item("loan_amount"))) & " Only"
                rpt_Row.Item("Column15") = CBool(dtRow.Item("istopup"))
                rpt_Row.Item("Column16") = dtRow.Item("purpose")

                If mintLoanTypeId = enLoanType.General_Loan Then
                    rpt_Row.Item("Column18") = Language.getMessage(mstrModuleName, 1, "Unsecured General Loan")
                ElseIf mintLoanTypeId = enLoanType.Car_Loan Then
                    rpt_Row.Item("Column18") = Language.getMessage(mstrModuleName, 2, "Car Loan")
                ElseIf mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan Then
                    rpt_Row.Item("Column18") = Language.getMessage(mstrModuleName, 3, "Refinancing Mortgage")
                ElseIf mintLoanTypeId = enLoanType.Purchase_Mortgage Then
                    rpt_Row.Item("Column18") = Language.getMessage(mstrModuleName, 4, "Purchase Mortgage")
                ElseIf mintLoanTypeId = enLoanType.Construction_Mortgage Then
                    rpt_Row.Item("Column18") = Language.getMessage(mstrModuleName, 5, "Construction Mortgage")
                End If

                If CInt(dtRow.Item("loanschemecategoryid")) = enLoanSchemeCategories.SECURED Then
                    rpt_Row.Item("Column17") = dtRow.Item("EmployeeFullName")
                Else
                    rpt_Row.Item("Column17") = ""
                End If

                rpt_Row.Item("Column19") = dtRow.Item("plot_no")
                rpt_Row.Item("Column20") = dtRow.Item("block_no")
                rpt_Row.Item("Column21") = dtRow.Item("street")
                rpt_Row.Item("Column22") = dtRow.Item("town_city")
                rpt_Row.Item("Column23") = dtRow.Item("ct_no")
                rpt_Row.Item("Column24") = dtRow.Item("lo_no")
                rpt_Row.Item("Column25") = CDec(dtRow.Item("market_value")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US"))
                rpt_Row.Item("Column26") = CDec(dtRow.Item("fsv")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US"))

                rpt_Row.Item("Column27") = dtRow.Item("model")
                rpt_Row.Item("Column28") = dtRow.Item("yom")
                rpt_Row.Item("Column29") = dtRow.Item("chassis_no")
                rpt_Row.Item("Column30") = dtRow.Item("colour")
                rpt_Row.Item("Column31") = dtRow.Item("supplier")
                rpt_Row.Item("Column32") = dtRow.Item("loanschemecategoryid")

                '---------------------- Employee History Data -- Start--------------

                Dim dsEmployeeHistory As New DataSet
                objDataOperation.ClearParameters()

                StrQ = "SELECT " & _
                       "    athremployee_master.employeecode " & _
                       ",   ISNULL(athremployee_master.firstname, '') + ' ' + ISNULL(athremployee_master.othername, '') + ' ' + ISNULL(athremployee_master.surname, '') AS EmployeeName " & _
                       ",   isapplicant " & _
                       ",   iscentralized " & _
                       ",   isfinalapprover " & _
                       ",   empsignature " & _
                       ",   ISNULL(hrjob_master.job_name, '') as Job " & _
                       ",   ISNULL(hrclasses_master.name, '') as Class " & _
                       ",   ISNULL(hrsectiongroup_master.name, '') as SectionGroup " & _
                       ",   ISNULL(athremployee_master.present_address1, '') AS present_address1 " & _
                       ",   ISNULL(athremployee_master.present_mobile, '') As present_mobile " & _
                        "FROM lnloan_history_tran " & _
                        "LEFT JOIN athremployee_master ON athremployee_master.atemployeeunkid = lnloan_history_tran.atemployeeunkid " & _
                        "LEFT JOIN hremployee_categorization_tran ON hremployee_categorization_tran.categorizationtranunkid = lnloan_history_tran.categorizationtranunkid AND hremployee_categorization_tran.isvoid = 0 " & _
                        "LEFT JOIN hrjob_master on hrjob_master.jobunkid = hremployee_categorization_tran.jobunkid " & _
                        "LEFT JOIN hremployee_transfer_tran ON hremployee_transfer_tran.transferunkid = lnloan_history_tran.transferunkid AND hremployee_transfer_tran.isvoid = 0 " & _
                        "LEFT JOIN hrclasses_master on hrclasses_master.classesunkid = hremployee_transfer_tran.classunkid AND hrclasses_master.isactive = 1 " & _
                        "LEFT JOIN hrsectiongroup_master on hrsectiongroup_master.sectiongroupunkid = hremployee_transfer_tran.sectiongroupunkid AND hrsectiongroup_master.isactive = 1 " & _
                        "WHERE lnloan_history_tran.isvoid = 0 "

                If mintProcessPendingLoanUnkid > 0 Then
                    objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessPendingLoanUnkid)
                    StrQ &= " AND lnloan_history_tran.processpendingloanunkid = @processpendingloanunkid "
                End If

                dsEmployeeHistory = objDataOperation.ExecQuery(StrQ, "DataTable")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                End If

                Dim drEmployeeHistory() As DataRow = dsEmployeeHistory.Tables(0).Select("isapplicant = 1")
                If drEmployeeHistory.Length > 0 Then
                    rpt_Row.Item("Column7") = drEmployeeHistory(0).Item("Class")
                    rpt_Row.Item("Column8") = drEmployeeHistory(0).Item("SectionGroup")
                    rpt_Row.Item("Column9") = drEmployeeHistory(0).Item("Job")
                    rpt_Row.Item("Column10") = drEmployeeHistory(0).Item("present_address1")
                    rpt_Row.Item("Column12") = drEmployeeHistory(0).Item("present_mobile")
                    rpt_Row.Item("sign1") = drEmployeeHistory(0).Item("empsignature")
                End If

                Dim drCentralizedApproverHistory() As DataRow = dsEmployeeHistory.Tables(0).Select("iscentralized = 1")
                If drCentralizedApproverHistory.Length > 0 Then
                    rpt_Row.Item("sign2") = drCentralizedApproverHistory(0).Item("empsignature")
                End If


                '---------------------- Employee History Data -- End--------------

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)



            Next

            Dim StrInnerQry, StrCommJoinQry, StrConditionQry As String

            StrInnerQry = "SELECT lnloan_process_pending_loan.processpendingloanunkid " & _
                         ",    #mappingunkid# AS mappingunkid  " & _
                         ",    #lnlevelunkid# AS lnlevelunkid " & _
                         ",    #lnlevelname# AS lnlevelname " & _
                         ",    #priority# AS priority " & _
                         "    ,#APPVR_NAME# approvername " & _
                         "    ,#APPR_JOB_NAME# AS approvertitle " & _
                         ",    lnroleloanapproval_process_tran.approvaldate " & _
                         ",    lnroleloanapproval_process_tran.statusunkid " & _
                         ",    CASE WHEN lnroleloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.PENDING & " THEN  @PENDING " & _
                         "          WHEN lnroleloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.APPROVED & " THEN  @APPROVED " & _
                         "          WHEN lnroleloanapproval_process_tran.statusunkid = " & enLoanApplicationStatus.REJECTED & " THEN  @REJECTED " & _
                         "     END status	 " & _
                         ",    ISNULL(lnroleloanapproval_process_tran.loan_amount, 0) AS approvedamount " & _
                         ",    lnroleloanapproval_process_tran.remark " & _
                         ",    athremployee_master.empsignature  " & _
                         " FROM lnroleloanapproval_process_tran " & _
                         " JOIN lnloan_process_pending_loan ON lnroleloanapproval_process_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid AND lnloan_process_pending_loan.isvoid = 0 " & _
                         " #ROLEMAPPING_JOIN# " & _
                             " #COMM_JOIN# "

            StrConditionQry = " WHERE lnroleloanapproval_process_tran.isvoid = 0 AND lnroleloanapproval_process_tran.priority #PriorityCond# " & _
                              " AND lnroleloanapproval_process_tran.processpendingloanunkid = @processpendingloanunkid "


            StrQ = StrInnerQry
            StrQ &= StrConditionQry

            StrQ = StrQ.Replace("#ROLEMAPPING_JOIN#", " JOIN lnloanscheme_role_mapping ON lnroleloanapproval_process_tran.mappingunkid = lnloanscheme_role_mapping.mappingunkid " & _
                                                    " JOIN lnapproverlevel_master ON lnloanscheme_role_mapping.levelunkid = lnapproverlevel_master.lnlevelunkid ")

            StrQ = StrQ.Replace("#COMM_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lnroleloanapproval_process_tran.mapuserunkid " & _
                                                       " JOIN #DB_Name#hremployee_master ON UEmp.employeeunkid = hremployee_master.employeeunkid " & _
                                               " LEFT JOIN lnloan_history_tran ON lnloan_history_tran.processpendingloanunkid = lnroleloanapproval_process_tran.processpendingloanunkid AND lnloan_history_tran.isvoid = 0 AND lnloan_history_tran.pendingloanaprovalunkid = lnroleloanapproval_process_tran.pendingloanaprovalunkid  " & _
                                               " LEFT JOIN hremployee_categorization_tran on hremployee_categorization_tran.categorizationtranunkid = lnloan_history_tran.categorizationtranunkid  " & _
                                               " LEFT JOIN hrjob_master on hrjob_master.jobunkid = hremployee_categorization_tran.jobunkid " & _
                                               " LEFT JOIN athremployee_master on athremployee_master.atemployeeunkid = lnloan_history_tran.atemployeeunkid ")



            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#mappingunkid#", "lnloanscheme_role_mapping.mappingunkid")
            StrQ = StrQ.Replace("#lnlevelunkid#", "lnapproverlevel_master.lnlevelunkid")
            StrQ = StrQ.Replace("#lnlevelname#", "lnapproverlevel_master.lnlevelname")
            StrQ = StrQ.Replace("#priority#", "lnapproverlevel_master.priority")
            StrQ = StrQ.Replace("#PriorityCond#", ">= 0")
            StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
            StrQ = StrQ.Replace("#APPVR_NAME#", " CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")

            StrQ &= " ORDER BY lnapproverlevel_master.priority "
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessPendingLoanUnkid)
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Rejected"))
            objDataOperation.AddParameter("@ReportingTo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Reporting To"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            Dim dsReportingToApproval As New DataSet
            StrQ = StrInnerQry
            StrQ &= StrConditionQry

           
            StrQ = StrQ.Replace("#APPVR_NAME#", " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') ")
            StrQ = StrQ.Replace("#COMM_JOIN#", " JOIN #DB_Name#hremployee_master ON lnroleloanapproval_process_tran.mapuserunkid = hremployee_master.employeeunkid  " & _
                                               " LEFT JOIN lnloan_history_tran ON lnloan_history_tran.processpendingloanunkid = lnroleloanapproval_process_tran.processpendingloanunkid AND lnloan_history_tran.isvoid = 0 AND lnloan_history_tran.pendingloanaprovalunkid = lnroleloanapproval_process_tran.pendingloanaprovalunkid  " & _
                                               " LEFT JOIN hremployee_categorization_tran on hremployee_categorization_tran.categorizationtranunkid = lnloan_history_tran.categorizationtranunkid  " & _
                                               " LEFT JOIN hrjob_master on hrjob_master.jobunkid = hremployee_categorization_tran.jobunkid " & _
                                               " LEFT JOIN athremployee_master on athremployee_master.atemployeeunkid = lnloan_history_tran.atemployeeunkid ")

            StrQ = StrQ.Replace("#DB_Name#", "")
            StrQ = StrQ.Replace("#ROLEMAPPING_JOIN#", "")
            StrQ = StrQ.Replace("#mappingunkid#", "-1")
            StrQ = StrQ.Replace("#lnlevelunkid#", "-1")
            StrQ = StrQ.Replace("#lnlevelname#", "@ReportingTo")
            StrQ = StrQ.Replace("#priority#", "-1")
            StrQ = StrQ.Replace("#PriorityCond#", "= -1")
            StrQ = StrQ.Replace("#APPR_JOB_NAME#", "hrjob_master.job_name ")
            dsReportingToApproval = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables.Count <= 0 Then
                dsList.Tables.Add(dsReportingToApproval.Tables(0).Copy)
            Else
                dsList.Tables(0).Merge(dsReportingToApproval.Tables(0), True)
            End If

            If dsList.Tables.Count > 0 Then
                dsList.Tables(0).DefaultView.Sort = "Priority ASC"
                Dim dtTempTable As DataTable = dsList.Tables(0).DefaultView.ToTable
                dsList.Tables.Remove(dsList.Tables(0))
                dsList.Tables.Add(dtTempTable)
            End If
            Dim lstProcessPendingLoanUnkIds As IEnumerable(Of Int32) = dsList.Tables(0).AsEnumerable(). _
                                                         Select(Function(row) row.Field(Of Int32)("processpendingloanunkid")). _
                                                         Distinct()

            Dim arrProcessPendingLoanIds As Integer() = lstProcessPendingLoanUnkIds.ToArray

            For Each intProcessPendingLoanID As Integer In arrProcessPendingLoanIds
                Dim drList() As DataRow = dsList.Tables(0).Select("processpendingloanunkid = " & intProcessPendingLoanID)
                If drList.Length > 0 Then
                    Dim dt As New DataTable
                    dt = drList.CopyToDataTable()

                    Dim mintPriorty As Integer = -1

                    If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
Recalculate:
                        Dim mintPrioriry As Integer = 0
                        For i As Integer = 0 To dt.Rows.Count - 1

                            If mintPrioriry <> CInt(dt.Rows(i)("priority")) Then
                                mintPrioriry = CInt(dt.Rows(i)("priority"))
                                Dim drRow() As DataRow = dt.Select("statusunkid <> 1 AND priority  = " & mintPrioriry)
                                If drRow.Length > 0 Then
                                    Dim drPending() As DataRow = dt.Select("statusunkid = 1 AND priority  = " & mintPrioriry)
                                    If drPending.Length > 0 Then
                                        Dim drPendingList() As DataRow = dsList.Tables(0).Select("processpendingloanunkid = " & intProcessPendingLoanID & " AND statusunkid = 1 AND priority  = " & mintPrioriry)
                                        If drPendingList.Length > 0 Then
                                            For Each dRowList As DataRow In drPendingList
                                                dsList.Tables(0).Rows.Remove(dRowList)
                                            Next
                                            dsList.Tables(0).AcceptChanges()
                                        End If
                                        For Each dRow As DataRow In drPending
                                            dt.Rows.Remove(dRow)
                                            GoTo Recalculate
                                        Next
                                        dt.AcceptChanges()
                                    End If
                                End If


                            End If

                        Next
                    End If
                End If
            Next

            Dim intPriority As Integer = -2
            For Each dtRow As DataRow In dsList.Tables(0).Rows
                If intPriority = CInt(dtRow.Item("priority")) Then Continue For
                Dim rpt_Row As DataRow
                rpt_Row = rpt_LoanApprovers.Tables("ArutiTable").NewRow
                rpt_Row.Item("Column1") = dtRow.Item("lnlevelname")
                If CInt(dtRow.Item("statusunkid")) <> enLoanApplicationStatus.PENDING Then
                    rpt_Row.Item("Column2") = dtRow.Item("approvername")
                    rpt_Row.Item("Column3") = dtRow.Item("approvertitle")
                    rpt_Row.Item("Column4") = dtRow.Item("status")
                    rpt_Row.Item("Column5") = CDec(dtRow.Item("approvedamount")).ToString("#,##0.##", New System.Globalization.CultureInfo("en-US"))
                    rpt_Row.Item("Column6") = CDate(dtRow.Item("approvaldate")).ToShortDateString
                    rpt_Row.Item("Column7") = dtRow.Item("remark")
                    rpt_Row.Item("sign1") = dtRow.Item("empsignature")
                End If
                rpt_LoanApprovers.Tables("ArutiTable").Rows.Add(rpt_Row)
                intPriority = CInt(dtRow.Item("priority"))
            Next

            objRpt = New ArutiReport.Designer.rptFlexcubeLoanForm

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                       True, _
                                       False, _
                                       "arutiLogo1", _
                                       "arutiLogo2", _
                                       arrImageRow, _
                                       , _
                                       , _
                                       , _
                                       ConfigParameter._Object._GetLeftMargin, _
                                       ConfigParameter._Object._GetRightMargin, _
                                       ConfigParameter._Object._TOPPageMarging)




            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 11, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 12, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 13, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 14, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection4", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "lblReportName", Language.getMessage(mstrModuleName, 1, "STANDARD STAFF LOAN APPLICATION FORM CR-HR-2022"))
            Call ReportFunction.TextChange(objRpt, "lblone", Language.getMessage(mstrModuleName, 2, "1.0"))
            Call ReportFunction.TextChange(objRpt, "txtone", Language.getMessage(mstrModuleName, 3, "PERSONAL BRIEF EMPLOYMENT PARTICULARS"))

            Call ReportFunction.TextChange(objRpt, "lblone_one", Language.getMessage(mstrModuleName, 2, "1.1"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 3, "Employee’s Full Name :"))

            Call ReportFunction.TextChange(objRpt, "lblone_two", Language.getMessage(mstrModuleName, 2, "1.2"))
            Call ReportFunction.TextChange(objRpt, "txtBirthDate", Language.getMessage(mstrModuleName, 3, "Date of Birth :"))

            Call ReportFunction.TextChange(objRpt, "lblone_three", Language.getMessage(mstrModuleName, 2, "1.3"))
            Call ReportFunction.TextChange(objRpt, "txtEmployementDate", Language.getMessage(mstrModuleName, 3, "Date of Employment :"))

            Call ReportFunction.TextChange(objRpt, "lblone_four", Language.getMessage(mstrModuleName, 2, "1.4"))
            Call ReportFunction.TextChange(objRpt, "txtConfirmationDate", Language.getMessage(mstrModuleName, 3, "Date of Confirmation :"))

            Call ReportFunction.TextChange(objRpt, "lblone_five", Language.getMessage(mstrModuleName, 2, "1.5"))
            Call ReportFunction.TextChange(objRpt, "txtStationOfDuty", Language.getMessage(mstrModuleName, 3, "Station of Duty :"))

            Call ReportFunction.TextChange(objRpt, "lblone_six", Language.getMessage(mstrModuleName, 2, "1.6"))
            Call ReportFunction.TextChange(objRpt, "txtCurrentDesignation", Language.getMessage(mstrModuleName, 3, "Current Designation :"))

            Call ReportFunction.TextChange(objRpt, "lblone_seven", Language.getMessage(mstrModuleName, 2, "1.7"))
            Call ReportFunction.TextChange(objRpt, "txtAccountNumber", Language.getMessage(mstrModuleName, 3, "Account Number :"))


            Call ReportFunction.TextChange(objRpt, "txtNIDAIdNo", Language.getMessage(mstrModuleName, 2, "NIDA ID No :"))
            Call ReportFunction.TextChange(objRpt, "txtStaffIdNo", Language.getMessage(mstrModuleName, 6, "Staff ID No :"))
            Call ReportFunction.TextChange(objRpt, "txtBranch", Language.getMessage(mstrModuleName, 8, "Branch"))
            Call ReportFunction.TextChange(objRpt, "txtDepartment", Language.getMessage(mstrModuleName, 9, "Department"))
            Call ReportFunction.TextChange(objRpt, "txtResidence", Language.getMessage(mstrModuleName, 15, "Residence :"))
            Call ReportFunction.TextChange(objRpt, "txtMobileNo", Language.getMessage(mstrModuleName, 16, "Mobile No :"))

            Call ReportFunction.TextChange(objRpt, "lbltwo", Language.getMessage(mstrModuleName, 2, "2.0"))
            Call ReportFunction.TextChange(objRpt, "txttwo", Language.getMessage(mstrModuleName, 2, "CREDIT PARTICULARS"))

            Call ReportFunction.TextChange(objRpt, "lbltwo_one", Language.getMessage(mstrModuleName, 2, "2.0.1"))
            Call ReportFunction.TextChange(objRpt, "txttwo_one", Language.getMessage(mstrModuleName, 2, "Credit Amount Requested (TZS) :"))

            Call ReportFunction.TextChange(objRpt, "lbltwo_two", Language.getMessage(mstrModuleName, 2, "2.0.2"))
            Call ReportFunction.TextChange(objRpt, "txttwo_two", Language.getMessage(mstrModuleName, 2, "Type of Credit Accommodation Applied for: (Please give ""X"" as appropriate) "))

            Call ReportFunction.TextChange(objRpt, "lbltwo_twoI", Language.getMessage(mstrModuleName, 2, "i)."))
            Call ReportFunction.TextChange(objRpt, "lblNew", Language.getMessage(mstrModuleName, 2, "New"))
            Call ReportFunction.TextChange(objRpt, "lblTopUp", Language.getMessage(mstrModuleName, 2, "Top-Up"))
            Call ReportFunction.TextChange(objRpt, "lblNewX", Language.getMessage(mstrModuleName, 2, "X"))
            Call ReportFunction.TextChange(objRpt, "lblTopUpX", Language.getMessage(mstrModuleName, 2, "X"))

            Call ReportFunction.TextChange(objRpt, "lbltwo_three", Language.getMessage(mstrModuleName, 2, "2.0.3"))
            Call ReportFunction.TextChange(objRpt, "txttwo_three", Language.getMessage(mstrModuleName, 2, "Purpose of the Credit Facility: "))

            Call ReportFunction.TextChange(objRpt, "lblthree", Language.getMessage(mstrModuleName, 2, "3.0"))
            Call ReportFunction.TextChange(objRpt, "txtthree", Language.getMessage(mstrModuleName, 2, "PROTECTION (FOR SECURED CREDIT REQUESTS ONLY)"))
            Call ReportFunction.TextChange(objRpt, "lblthree_one", Language.getMessage(mstrModuleName, 2, "3.1"))
            Call ReportFunction.TextChange(objRpt, "txtthree_one", Language.getMessage(mstrModuleName, 2, "Ownership:"))
            Call ReportFunction.TextChange(objRpt, "lblthree_oneA", Language.getMessage(mstrModuleName, 2, "(a)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_oneA", Language.getMessage(mstrModuleName, 2, "Own (Full Name) :"))
            Call ReportFunction.TextChange(objRpt, "lblthree_oneB", Language.getMessage(mstrModuleName, 2, "(b)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_oneB", Language.getMessage(mstrModuleName, 2, "Guarantor’s	Husband’s/Wife/Child’s (strike through not applicable)"))
            Call ReportFunction.TextChange(objRpt, "lblthree_oneC", Language.getMessage(mstrModuleName, 2, "(c)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_oneC", Language.getMessage(mstrModuleName, 2, "Third Party	Please indicate__________________________________"))
            Call ReportFunction.TextChange(objRpt, "lblthree_two", Language.getMessage(mstrModuleName, 2, "3.2"))
            Call ReportFunction.TextChange(objRpt, "txtthree_two", Language.getMessage(mstrModuleName, 2, "Security Description:"))


            Call ReportFunction.TextChange(objRpt, "txtSecurityType", Language.getMessage(mstrModuleName, 2, "Security Type"))
            Call ReportFunction.TextChange(objRpt, "txtLocation", Language.getMessage(mstrModuleName, 2, "Location"))
            Call ReportFunction.TextChange(objRpt, "txtRegistration", Language.getMessage(mstrModuleName, 2, "Registration"))
            Call ReportFunction.TextChange(objRpt, "txtValue", Language.getMessage(mstrModuleName, 2, "Value (TZS ""000"")"))
            Call ReportFunction.TextChange(objRpt, "txtMarket", Language.getMessage(mstrModuleName, 2, "Market"))
            Call ReportFunction.TextChange(objRpt, "txtFSV", Language.getMessage(mstrModuleName, 2, "FSV"))
            Call ReportFunction.TextChange(objRpt, "lblthree_twoA", Language.getMessage(mstrModuleName, 2, "(a)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_twoA", Language.getMessage(mstrModuleName, 2, "Residential House"))
            Call ReportFunction.TextChange(objRpt, "txtPlotNo", Language.getMessage(mstrModuleName, 2, "Plot No:"))
            Call ReportFunction.TextChange(objRpt, "txtBlockNo", Language.getMessage(mstrModuleName, 2, "Block No:"))
            Call ReportFunction.TextChange(objRpt, "txtStreet", Language.getMessage(mstrModuleName, 2, "Street :"))
            Call ReportFunction.TextChange(objRpt, "txtTown", Language.getMessage(mstrModuleName, 2, "Town/City :"))
            Call ReportFunction.TextChange(objRpt, "txtCTNo", Language.getMessage(mstrModuleName, 2, "CT No :"))
            Call ReportFunction.TextChange(objRpt, "txtLONo", Language.getMessage(mstrModuleName, 2, "LO No :"))

            Call ReportFunction.TextChange(objRpt, "txtModel", Language.getMessage(mstrModuleName, 2, "Model :"))
            Call ReportFunction.TextChange(objRpt, "txtYOM", Language.getMessage(mstrModuleName, 2, "YOM :"))
            Call ReportFunction.TextChange(objRpt, "txtChassisNo", Language.getMessage(mstrModuleName, 2, "Chassis No :"))
            Call ReportFunction.TextChange(objRpt, "txtColor", Language.getMessage(mstrModuleName, 2, "Color :"))
            Call ReportFunction.TextChange(objRpt, "txtSupplier", Language.getMessage(mstrModuleName, 2, "Supplier :"))


            Call ReportFunction.TextChange(objRpt, "lblthree_twoB", Language.getMessage(mstrModuleName, 2, "(b)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_twoB", Language.getMessage(mstrModuleName, 2, "Others (**indicate) "))

            Call ReportFunction.TextChange(objRpt, "txtSecurityType2", Language.getMessage(mstrModuleName, 2, "Security Type"))
            Call ReportFunction.TextChange(objRpt, "txtLocation2", Language.getMessage(mstrModuleName, 2, "Location"))
            Call ReportFunction.TextChange(objRpt, "txtRegistration2", Language.getMessage(mstrModuleName, 2, "Registration"))
            Call ReportFunction.TextChange(objRpt, "txtValue2", Language.getMessage(mstrModuleName, 2, "Value (TZS ""000"")"))
            Call ReportFunction.TextChange(objRpt, "txtMarket2", Language.getMessage(mstrModuleName, 2, "Market"))
            Call ReportFunction.TextChange(objRpt, "txtFSV2", Language.getMessage(mstrModuleName, 2, "FSV"))
            Call ReportFunction.TextChange(objRpt, "lblthree_twoA2", Language.getMessage(mstrModuleName, 2, "(a)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_twoA2", Language.getMessage(mstrModuleName, 2, "Motor Vehicle"))
            Call ReportFunction.TextChange(objRpt, "lblthree_twoB2", Language.getMessage(mstrModuleName, 2, "(b)"))
            Call ReportFunction.TextChange(objRpt, "txtthree_twoB2", Language.getMessage(mstrModuleName, 2, "Others (**indicate) "))

            Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", True)
            Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection3", True)
            If mintLoanTypeId = enLoanType.Refinancing_Mortgage_Loan OrElse mintLoanTypeId = enLoanType.Purchase_Mortgage OrElse mintLoanTypeId = enLoanType.Construction_Mortgage Then
                Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection2", False)
            End If
            If mintLoanTypeId = enLoanType.Car_Loan Then
                Call ReportFunction.EnableSuppressSection(objRpt, "DetailSection3", False)
            End If

            Call ReportFunction.TextChange(objRpt, "lblfour", Language.getMessage(mstrModuleName, 2, "4.0"))
            Call ReportFunction.TextChange(objRpt, "txtfour", Language.getMessage(mstrModuleName, 2, "ASSIGNMENT OF TERMINAL BENEFITS TO LIQUIDATE OUTSTANDING ASSET(S)"))

            Call ReportFunction.TextChange(objRpt, "lblfive", Language.getMessage(mstrModuleName, 2, "5.0"))
            Call ReportFunction.TextChange(objRpt, "txtfive", Language.getMessage(mstrModuleName, 2, "APPROVALS"))

            objRpt.Subreports("rptLoanApproverDetailReport").SetDataSource(rpt_LoanApprovers)
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtLevelName", Language.getMessage(mstrModuleName, 17, "Level Name"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtApprover", Language.getMessage(mstrModuleName, 17, "Approver"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtJobTitle", Language.getMessage(mstrModuleName, 17, "Job Title"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtStatus", Language.getMessage(mstrModuleName, 17, "Status"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtApporvedAmount", Language.getMessage(mstrModuleName, 17, "Approved Amount"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtApprovalDate", Language.getMessage(mstrModuleName, 17, "Approval Date"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtRemarks", Language.getMessage(mstrModuleName, 17, "Remarks"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptLoanApproverDetailReport"), "txtSignature", Language.getMessage(mstrModuleName, 17, "Signature"))

            Call ReportFunction.TextChange(objRpt, "lblBank", Language.getMessage(mstrModuleName, 102, "Bank"))
            Call ReportFunction.TextChange(objRpt, "lblBorrower2", Language.getMessage(mstrModuleName, 103, "Borrower"))


            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function



#End Region


End Class

