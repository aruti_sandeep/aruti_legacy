#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmEmployeeBankRegisterList

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmEmployeeBankRegisterListReport"
    Private objEmpBankRegisterList As clsEmployeeBankRegisterList
    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvanceFilter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End
#End Region

#Region "Constructor"
    Public Sub New()
        objEmpBankRegisterList = New clsEmployeeBankRegisterList(User._Object._Languageunkid,Company._Object._Companyunkid)
        objEmpBankRegisterList.SetDefaultValue()
        InitializeComponent()
        'Pinkal (27-Feb-2013) -- Start
        'Enhancement : TRA Changes
        _Show_AdvanceFilter = True
        'Pinkal (27-Feb-2013) -- End
    End Sub
#End Region

#Region "Private Function"
    Public Sub FillCombo()
        Try


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            'cboEmployee.DataSource = (New clsEmployee_Master).GetEmployeeList(True, True).Tables(0)

            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'cboEmployee.DataSource = (New clsEmployee_Master).GetEmployeeList(True, True, False).Tables(0)
            cboEmployee.DataSource = (New clsEmployee_Master).GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True).Tables(0)
            'Anjan [10 June 2015] -- End

            'Pinkal (24-Jun-2011) -- End

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"

            'Sohail (03 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'cboDepartment.DataSource = (New clsDepartment).getComboList(, True).Tables(0)
            'cboDepartment.ValueMember = "departmentunkid"
            'cboDepartment.DisplayMember = "name"
            'Sohail (03 Aug 2013) -- End

            'Vimal (27 Nov 2010) -- Start 
            'cboBank.DataSource = (New clspayrollgroup_master).getListForCombo(3, , True).Tables(0)
            'cboBank.DataSource = (New clspayrollgroup_master).getListForCombo(enPayrollGroupType.Bank, , True).Tables(0) 'Sohail (03 Aug 2013)
            'Vimal (27 Nov 2010) -- End
            cboBank.ValueMember = "groupmasterunkid"
            cboBank.DisplayMember = "name"
            'Sohail (03 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            cboBank.DataSource = (New clspayrollgroup_master).getListForCombo(enPayrollGroupType.Bank, , True).Tables(0)
            cboBank.SelectedValue = 0
            'Sohail (03 Aug 2013) -- End

            'Sohail (03 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'cboBranch.DataSource = (New clsbankbranch_master).getListForCombo(, True).Tables(0)
            'cboBranch.ValueMember = "branchunkid"
            'cboBranch.DisplayMember = "name"
            'Sohail (03 Aug 2013) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboBank.SelectedValue = 0
            cboBranch.SelectedValue = 0
            'cboDepartment.SelectedValue = 0 'Sohail (03 Aug 2013)

            txtAccountNo.Text = ""

            'Vimal (10 Dec 2010) -- Start 
            'objEmpBankRegisterList.setDefaultOrderBy(cboEmployee.SelectedIndex)
            objEmpBankRegisterList.setDefaultOrderBy(0)
            'Vimal (10 Dec 2010) -- End
            txtOrderBy.Text = objEmpBankRegisterList.OrderByDisplay


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            chkInactiveemp.Checked = False
            'Pinkal (24-Jun-2011) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvanceFilter = ""
            'Pinkal (27-Feb-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValues", mstrModuleName)
        End Try
    End Sub


    Private Function SetFilter() As Boolean
        Try
            objEmpBankRegisterList.SetDefaultValue()

            objEmpBankRegisterList._EmployeeUnkId = cboEmployee.SelectedValue
            objEmpBankRegisterList._EmployeeName = cboEmployee.Text

            'Sohail (03 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'objEmpBankRegisterList._DeptId = cboDepartment.SelectedValue
            'objEmpBankRegisterList._DeptName = cboDepartment.Text
            'Sohail (03 Aug 2013) -- End

            objEmpBankRegisterList._BankId = cboBank.SelectedValue
            objEmpBankRegisterList._BankName = cboBank.Text

            objEmpBankRegisterList._BranchId = cboBranch.SelectedValue
            objEmpBankRegisterList._BranchName = cboBranch.Text

            objEmpBankRegisterList._AccountNo = txtAccountNo.Text


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objEmpBankRegisterList._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objEmpBankRegisterList._Advance_Filter = mstrAdvanceFilter
            'Pinkal (27-Feb-2013) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function
#End Region

#Region "Form's Events"

    Private Sub frmEmployeeBankRegisterListReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objEmpBankRegisterList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeBankRegisterListReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeBankRegisterListReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Title = objEmpBankRegisterList._ReportName
            Me._Message = objEmpBankRegisterList._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeBankRegisterListReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeBankRegisterListReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmEmployeeBankRegisterListReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeBankRegisterListReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeBankRegisterListReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeBankRegisterListReport_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Buttons"

    Private Sub frmEmployeeBankRegisterListReport_Report_Click(ByVal sender As System.Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles MyBase.Report_Click
        Try
            If SetFilter() = False Then Exit Sub
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEmpBankRegisterList.generateReport(0, e.Type, enExportAction.None)
            objEmpBankRegisterList.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeBankRegisterListReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeBankRegisterListReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEmpBankRegisterList.generateReport(0, enPrintAction.None, e.Type)
            objEmpBankRegisterList.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeBankRegisterListReport_Export_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub frmEmployeeBankRegisterListReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeBankRegisterListReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEmployeeBankRegisterListReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEmployeeBankRegisterListReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub frmEmployeeBankRegisterListReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsEmployeeBankRegisterList.SetMessages()
            objfrm._Other_ModuleNames = "clsEmployeeBankRegisterListReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmEmployeeBankRegisterListReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Pinkal (03-Sep-2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Pinkal (27-Feb-2013) -- End


#End Region

    'Sohail (03 Aug 2013) -- Start
    'TRA - ENHANCEMENT
#Region " Combobox's Events "
    Private Sub cboBank_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBank.SelectedValueChanged
        Try
            With cboBranch
                .ValueMember = "branchunkid"
                .DisplayMember = "name"
                .DataSource = (New clsbankbranch_master).getListForCombo(, True, CInt(cboBank.SelectedValue)).Tables(0)
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBank_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (03 Aug 2013) -- End

#Region "Control"
    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            'Vimal (10 Dec 2010) -- Start 
            'objEmpBankRegisterList.setOrderBy(cboEmployee.SelectedIndex)
            objEmpBankRegisterList.setOrderBy(0)
            'Vimal (10 Dec 2010) -- End
            txtOrderBy.Text = objEmpBankRegisterList.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblAccountNo.Text = Language._Object.getCaption(Me.lblAccountNo.Name, Me.lblAccountNo.Text)
			Me.lblBankName.Text = Language._Object.getCaption(Me.lblBankName.Name, Me.lblBankName.Text)
			Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
