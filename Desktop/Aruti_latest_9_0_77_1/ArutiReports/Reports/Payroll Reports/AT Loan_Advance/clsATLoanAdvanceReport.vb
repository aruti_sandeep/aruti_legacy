'************************************************************************************************************************************
'Class Name : clsATLoanAdvanceReport.vb
'Purpose    :
'Date       :03/02/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsATLoanAdvanceReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsATLoanAdvanceReport"
    Private mstrReportId As String = enArutiReport.ATLoanAdvanceReport  '44
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Property Variables "

    Private mstrEmployeeName As String = String.Empty
    Private mintEmployeeId As Integer = 0
    Private mintAuditTypeId As Integer = 0
    Private mstrAuditTypeName As String = String.Empty
    Private mintLoanSchemeId As Integer = 0
    Private mstrLoanSchemeName As String = String.Empty
    Private mstrApproverId As Integer = 0
    Private mstrApproverName As String = String.Empty
    Private mintLAStatusId As Integer = 0
    Private mstrLoanStatusName As String = String.Empty
    Private mintATUserId As Integer = 0
    Private mstrATUserName As String = String.Empty
    Private mdtAuditDateFrom As Date = Nothing
    Private mdtAuditDateTo As Date = Nothing
    Private mdtAppliedDateFrom As Date = Nothing
    Private mdtAppliedDateTo As Date = Nothing

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIncludeInactiveEmp As Boolean = True
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Private mintBranchId As Integer = -1
    Private mstrBranchName As String = String.Empty
    'S.SANDEEP [ 17 AUG 2011 ] -- END 

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (17 Apr 2012) -- End

    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _EmployeeName() As String
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public WriteOnly Property _EmployeeId() As Integer
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public WriteOnly Property _AuditTypeId() As Integer
        Set(ByVal value As Integer)
            mintAuditTypeId = value
        End Set
    End Property

    Public WriteOnly Property _AuditTypeName() As String
        Set(ByVal value As String)
            mstrAuditTypeName = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeId() As Integer
        Set(ByVal value As Integer)
            mintLoanSchemeId = value
        End Set
    End Property

    Public WriteOnly Property _LoanSchemeName() As String
        Set(ByVal value As String)
            mstrLoanSchemeName = value
        End Set
    End Property

    Public WriteOnly Property _ApproverId() As Integer
        Set(ByVal value As Integer)
            mstrApproverId = value
        End Set
    End Property

    Public WriteOnly Property _ApproverName() As String
        Set(ByVal value As String)
            mstrApproverName = value
        End Set
    End Property

    Public WriteOnly Property _LAStatusId() As Integer
        Set(ByVal value As Integer)
            mintLAStatusId = value
        End Set
    End Property

    Public WriteOnly Property _LoanStatusName() As String
        Set(ByVal value As String)
            mstrLoanStatusName = value
        End Set
    End Property

    Public WriteOnly Property _ATUserId() As Integer
        Set(ByVal value As Integer)
            mintATUserId = value
        End Set
    End Property

    Public WriteOnly Property _ATUserName() As String
        Set(ByVal value As String)
            mstrATUserName = value
        End Set
    End Property

    Public WriteOnly Property _AuditDateFrom() As Date
        Set(ByVal value As Date)
            mdtAuditDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _AuditDateTo() As Date
        Set(ByVal value As Date)
            mdtAuditDateTo = value
        End Set
    End Property

    Public WriteOnly Property _AppliedDateFrom() As Date
        Set(ByVal value As Date)
            mdtAppliedDateFrom = value
        End Set
    End Property

    Public WriteOnly Property _AppliedDateTo() As Date
        Set(ByVal value As Date)
            mdtAppliedDateTo = value
        End Set
    End Property

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 17 AUG 2011 ] -- START
    'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    Public WriteOnly Property _BranchId() As Integer
        Set(ByVal value As Integer)
            mintBranchId = value
        End Set
    End Property

    Public WriteOnly Property _Branch_Name() As String
        Set(ByVal value As String)
            mstrBranchName = value
        End Set
    End Property
    'S.SANDEEP [ 17 AUG 2011 ] -- END

    'Sohail (17 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (17 Apr 2012) -- End


    'Pinkal (27-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property
    'Pinkal (27-Feb-2013) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrEmployeeName = ""
            mintEmployeeId = 0
            mintAuditTypeId = 0
            mstrAuditTypeName = ""
            mintLoanSchemeId = 0
            mstrLoanSchemeName = ""
            mstrApproverId = 0
            mstrApproverName = ""
            mintLAStatusId = 0
            mstrLoanStatusName = ""
            mintATUserId = 0
            mstrATUserName = ""
            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIncludeInactiveEmp = True
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            mintBranchId = -1
            mstrBranchName = ""
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Pinkal (27-Feb-2013) -- Start
            'Enhancement : TRA Changes
            mstrAdvance_Filter = ""
            'Pinkal (27-Feb-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

            objDataOperation.AddParameter("@Adva", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Advance"))
            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Loan"))

            objDataOperation.AddParameter("@Add", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Add"))
            objDataOperation.AddParameter("@Edit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Edit"))
            objDataOperation.AddParameter("@Delete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Delete"))

            objDataOperation.AddParameter("@InPrg", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 96, "In Progress"))
            objDataOperation.AddParameter("@OnHld", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 97, "On Hold"))
            objDataOperation.AddParameter("@WtOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 98, "Written Off"))
            objDataOperation.AddParameter("@Cmplt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 100, "Completed"))

            objDataOperation.AddParameter("@FDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditDateFrom))
            objDataOperation.AddParameter("@TDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAuditDateTo))
            Me._FilterTitle &= Language.getMessage(mstrModuleName, 6, "Audit Date From :") & "" & mdtAuditDateFrom.Date & " " & _
                               Language.getMessage(mstrModuleName, 7, "To") & " " & mdtAuditDateTo.Date & " "

            If mdtAppliedDateFrom <> Nothing AndAlso mdtAppliedDateTo <> Nothing Then
                objDataOperation.AddParameter("@AppliedDateFrom", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAppliedDateFrom))
                objDataOperation.AddParameter("@AppliedDateTo", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtAppliedDateTo))
                Me._FilterQuery &= " AND EffDate BETWEEN @AppliedDateFrom AND @AppliedDateTo "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 8, "Applied Date From :") & " " & mdtAppliedDateFrom.Date & " " & _
                                   Language.getMessage(mstrModuleName, 7, "To") & " " & mdtAppliedDateTo.Date & " "
            End If

            If mintEmployeeId > 0 Then
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                Me._FilterQuery &= " AND EmpId = @EmployeeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 9, "Employee :") & " " & mstrEmployeeName & " "
            End If

            If mintAuditTypeId > 0 Then
                objDataOperation.AddParameter("@AuditTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditTypeId)
                Me._FilterQuery &= " AND ATypeId = @AuditTypeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 10, "Audit Type :") & " " & mstrAuditTypeName & " "
            End If

            If mintLoanSchemeId > 0 Then
                objDataOperation.AddParameter("@LoanSchemeId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeId)
                Me._FilterQuery &= " AND ALSchemeId = @LoanSchemeId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 11, "Loan Scheme :") & " " & mstrLoanSchemeName & " "
            End If

            If mstrApproverId > 0 Then
                objDataOperation.AddParameter("@ApproverId", SqlDbType.Int, eZeeDataType.INT_SIZE, mstrApproverId)
                Me._FilterQuery &= " AND ApprId = @ApproverId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 12, "Approver :") & " " & mstrApproverName & " "
            End If

            'Nilay (28-Aug-2015) -- Start
            'Purpose : To display Future Loan
            objDataOperation.AddParameter("@FutureLoan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 42, "Future Loan"))
            'If mintLAStatusId > 0 Then
            '    objDataOperation.AddParameter("@LAStatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLAStatusId)
            '    Me._FilterQuery &= " AND LAStatusId = @LAStatusId "
            '    Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Status :") & " " & mstrLoanStatusName & " "
            'End If

            If mintLAStatusId = -999 OrElse mintLAStatusId > 0 Then
                objDataOperation.AddParameter("@LAStatusId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLAStatusId)
                Me._FilterQuery &= " AND LAStatusId = @LAStatusId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 13, "Status :") & " " & mstrLoanStatusName & " "
            End If
            'Nilay (28-Aug-2015) -- End



            If mintATUserId > 0 Then
                objDataOperation.AddParameter("@ATUserId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintATUserId)
                Me._FilterQuery &= " AND ATUserId = @ATUserId "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 14, "Audit User :") & " " & mstrATUserName & " "
            End If

            'S.SANDEEP [ 17 AUG 2011 ] -- START
            'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
            If mintBranchId > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 40, "Branch :") & " " & mstrBranchName & " "
            End If
            'S.SANDEEP [ 17 AUG 2011 ] -- END 

            'Sohail (17 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mblnIncludeInactiveEmp = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If
            'Sohail (17 Apr 2012) -- End

            If Me.OrderByQuery <> "" Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 15, "Order By :") & " " & Me.OrderByDisplay & " "
                Me._FilterQuery &= "ORDER BY EmpId,ATypeId, " & Me.OrderByQuery
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        Try
            'objRpt = Generate_DetailReport()
            'If Not IsNothing(objRpt) Then
            '    Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, False)
            'End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As Date, _
                                           ByVal xPeriodEnd As Date, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean, _
                                           ByVal xExportReportPath As String, _
                                           ByVal xOpenReportAfterExport As Boolean, _
                                           ByVal pintReportType As Integer, _
                                           Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, _
                                           Optional ByVal ExportAction As enExportAction = enExportAction.None, _
                                           Optional ByVal xBaseCurrencyId As Integer = 0)

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""

        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True)
            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, False)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Nilay (10-Oct-2015) -- End

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("AuditDate", Language.getMessage(mstrModuleName, 16, "Audit Date")))
            iColumn_DetailReport.Add(New IColumn("AuditTime", Language.getMessage(mstrModuleName, 17, "Audit Time")))
            iColumn_DetailReport.Add(New IColumn("EName", Language.getMessage(mstrModuleName, 18, "Employee Name")))
            iColumn_DetailReport.Add(New IColumn("AType", Language.getMessage(mstrModuleName, 19, "Audit Type")))
            iColumn_DetailReport.Add(New IColumn("ATUser", Language.getMessage(mstrModuleName, 20, "Audit User")))
            iColumn_DetailReport.Add(New IColumn("Amount", Language.getMessage(mstrModuleName, 21, "Loan Amount")))
            iColumn_DetailReport.Add(New IColumn("NetAmount", Language.getMessage(mstrModuleName, 22, "Net Amount")))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Private Function Generate_DetailReport(ByVal xDatabaseName As String, _
                                           ByVal xUserUnkid As Integer, _
                                           ByVal xYearUnkid As Integer, _
                                           ByVal xCompanyUnkid As Integer, _
                                           ByVal xPeriodStart As DateTime, _
                                           ByVal xPeriodEnd As DateTime, _
                                           ByVal xUserModeSetting As String, _
                                           ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            Dim StrQFinal As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQ1 As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, 1, mintFinYearId, , True)

            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, mintFinYearId, 1, , True)
            Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, 1, , True)
            'Nilay (10-Oct-2015) -- End


            'S.SANDEEP [04 JUN 2015] -- END

            Dim dtAsONDate As DateTime = Nothing
            If intFirstOpenPeriodID <= 0 Then
                dtAsONDate = Now
            Else
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(xDatabaseName) = intFirstOpenPeriodID
                dtAsONDate = objPrd._End_Date
                objPrd = Nothing
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            StrQ = "SELECT " & _
                      " VocNo as VocNo " & _
                      ",ECode AS ECode " & _
                      ",EName AS EName " & _
                      ",EffDate AS EffDate " & _
                      ",Amount AS Amount " & _
                      ",BalanceAmount AS BalanceAmount " & _
                      ",CASE WHEN ISNULL(ATLA.PeriodId,0)=0 THEN @FutureLoan ELSE ATLA.LStatus END AS LStatus " & _
                      ",(LAState+' --> '+LAScheme) AS LAScheme " & _
                      ",EMI_Duration AS EMI_Duration " & _
                      ",EMI_Amt AS EMI_Amt " & _
                      ",AName AS AName " & _
                      ",AType AS AType " & _
                      ",AuditDate AS AuditDate " & _
                      ",AuditTime AS AuditTime " & _
                      ",ATUser AS ATUser " & _
                      ",MachineIP AS MachineIP " & _
                      ",MName AS MName " & _
                      ",EmpId AS EmpId " & _
                      ",ATypeId AS ATypeId " & _
                    "FROM " & _
                    "( "

            StrQ1 &= "  SELECT " & _
                             " ISNULL(atlnloan_advance_tran.loanvoucher_no,'') AS VocNo " & _
                             ",ISNULL(hremployee_master.employeecode,'') AS ECode " & _
                             ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
                             ",CONVERT(CHAR(8),effective_date,112) AS EffDate " & _
                             ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(atlnloan_advance_tran.loan_amount,0) " & _
                                    "WHEN atlnloan_advance_tran.isloan = 0 THEN ISNULL(atlnloan_advance_tran.advance_amount,0) END AS Amount " & _
                             ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(atlnloan_advance_tran.balance_amountpaidcurrency,0) " & _
                                    "WHEN atlnloan_advance_tran.isloan = 0 THEN ISNULL(atlnloan_advance_tran.advance_amount,0) END AS BalanceAmount " & _
                             ",CASE WHEN atlnloan_advance_tran.loan_statusunkid = 1 THEN @InPrg " & _
                                    "WHEN atlnloan_advance_tran.loan_statusunkid = 2 THEN @OnHld " & _
                                    "WHEN atlnloan_advance_tran.loan_statusunkid = 3 THEN @WtOff " & _
                                    "WHEN atlnloan_advance_tran.loan_statusunkid = 4 THEN @Cmplt END AS LStatus " & _
                             ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN @Loan " & _
                                    "WHEN atlnloan_advance_tran.isloan = 0 THEN @Adva END AS LAState " & _
                             ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') " & _
                                    "WHEN atlnloan_advance_tran.isloan = 0 THEN @Adva END AS LAScheme " & _
                             ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN CAST (ISNULL(emi_tenure,'') AS NVARCHAR(50)) " & _
                                    "WHEN atlnloan_advance_tran.isloan = 0 THEN '' END AS EMI_Duration " & _
                             ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(emi_amount,0) " & _
                                    "WHEN atlnloan_advance_tran.isloan = 0 THEN 0 END AS EMI_Amt " & _
                             " ,#APPROVER_NAME# AS AName " & _
                             ",CASE WHEN audittype = 1 THEN @Add " & _
                                    "WHEN audittype = 2 THEN @Edit " & _
                                    "WHEN audittype = 3 THEN @Delete END AS AType " & _
                             ",CONVERT(CHAR(8),auditdatetime,112) AS AuditDate " & _
                             ",CONVERT(CHAR(8),auditdatetime,108) AS AuditTime " & _
                             ",ISNULL(hrmsConfiguration..cfuser_master.username,'') AS ATUser " & _
                             ",ISNULL(ip,'') AS MachineIP " & _
                             ",ISNULL(machine_name,'') AS MName " & _
                             ",hremployee_master.employeeunkid AS EmpId " & _
                             ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN 1 " & _
                    "			  WHEN atlnloan_advance_tran.isloan = 0 THEN 2 END AS ALStateId " & _
                             ",audittype AS ATypeId " & _
                             ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS ALSchemeId " & _
                             " ,#APPROVER_ID# AS ApprId " & _
                             ",CASE WHEN ISNULL(CP.periodunkid,0)=0 THEN -999 ELSE atlnloan_advance_tran.loan_statusunkid END AS LAStatusId " & _
                             ",hrmsConfiguration..cfuser_master.userunkid AS ATUserId " & _
                             ",CP.periodunkid as PeriodId " & _
                         "FROM atlnloan_advance_tran " & _
                              "JOIN hrmsConfiguration..cfuser_master ON atlnloan_advance_tran.audituserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                              "LEFT JOIN lnloan_scheme_master ON atlnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
                              "LEFT JOIN lnloan_process_pending_loan ON atlnloan_advance_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
                                    "AND atlnloan_advance_tran.approverunkid = lnloan_process_pending_loan.approverunkid " & _
                              "LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_process_pending_loan.approverunkid " & _
                              "LEFT JOIN hremployee_master ON atlnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
                             " #APPROVER_JOIN# " & _
                              "LEFT JOIN lnloan_status_tran on lnloan_status_tran.loanadvancetranunkid = atlnloan_advance_tran.loanadvancetranunkid " & _
                              "LEFT JOIN cfcommon_period_tran as CP on CP.periodunkid = lnloan_status_tran.periodunkid " & _
                                    " AND CP.modulerefid = 1 AND CONVERT(CHAR(8),CP.start_date,112) <= '" & eZeeDate.convertDate(dtAsONDate) & "' " & _
                              "LEFT JOIN " & _
                              "( " & _
                              "      SELECT " & _
                              "           stationunkid " & _
                              "          ,employeeunkid " & _
                              "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                              "      FROM hremployee_transfer_tran " & _
                              "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                              ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "

            StrQFinal = StrQ1

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ1 &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ1 &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ1 &= xAdvanceJoinQry
            End If

            StrQCondition &= " WHERE CONVERT(CHAR(8),auditdatetime,112) BETWEEN @FDate AND @TDate "

            StrQCondition &= " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQCondition &= " AND " & mstrAdvance_Filter
            End If

            If mintBranchId > 0 Then
                StrQCondition &= " AND Alloc.stationunkid = " & mintBranchId
            End If

            StrQ1 &= StrQCondition

            If mblnIncludeInactiveEmp = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ1 &= xDateFilterQry
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ1 &= " AND " & xUACFiltrQry
            End If

            StrQ1 = StrQ1.Replace("#APPROVER_ID#", "ISNULL(lnloanapprover_master.lnapproverunkid,0) ")
            StrQ1 = StrQ1.Replace("#APPROVER_NAME#", "ISNULL(Apprv.firstname, '') + ' ' + ISNULL(Apprv.othername, '') + ' ' + ISNULL(Apprv.surname, '') ")
            StrQ1 = StrQ1.Replace("#APPROVER_JOIN#", "LEFT JOIN hremployee_master AS Apprv ON Apprv.employeeunkid = lnloanapprover_master.approverempunkid AND Apprv.isapproved = 1 ")
            StrQ1 = StrQ1.Replace("#EXT_APPROVER#", "0")

            StrQ &= StrQ1

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows

                StrQ1 = StrQFinal

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    StrQ1 = StrQ1.Replace("#APPROVER_ID#", "ISNULL(lnloanapprover_master.lnapproverunkid,0) ")
                    StrQ1 = StrQ1.Replace("#APPROVER_NAME#", "ISNULL(UM.username,'') ")
                    StrQ1 = StrQ1.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid  = lnloanapprover_master.approverempunkid ")
                    StrQ1 &= StrQCondition
                Else
                    StrQ1 = StrQ1.Replace("#APPROVER_ID#", "CASE WHEN ISNULL(Apprv.firstname, '') + ' ' + ISNULL(Apprv.othername, '') + ' ' + ISNULL(Apprv.surname, '') = ' ' THEN ISNULL(UM.userunkid,0) " & _
                                                                "ELSE ISNULL(Apprv.employeeunkid, 0) END ")
                    StrQ1 = StrQ1.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(Apprv.firstname, '') + ' ' + ISNULL(Apprv.othername, '') + ' ' + ISNULL(Apprv.surname, '') = ' ' THEN ISNULL(UM.username,'') " & _
                                                                  "ELSE ISNULL(Apprv.firstname, '') + ' ' + ISNULL(Apprv.othername, '') + ' ' + ISNULL(Apprv.surname, '') END ")
                    StrQ1 = StrQ1.Replace("#APPROVER_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid = lnloanapprover_master.approverempunkid " & _
                                                             "LEFT JOIN #DB_NAME#hremployee_master AS Apprv ON Apprv.employeeunkid = UM.employeeunkid ")
                    StrQ1 = StrQ1.Replace("#DB_NAME#", dRow("dbname").ToString & "..")

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ1 &= xDateJoinQry
                    End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ1 &= xUACQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ1 &= xAdvanceJoinQry
                    End If

                    StrQ1 &= StrQCondition

                    If mblnIncludeInactiveEmp = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQ1 &= xDateFilterQry
                        End If
                    End If

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ1 &= " AND " & xUACFiltrQry
                    End If

                End If

                StrQ1 = StrQ1.Replace("#EXT_APPROVER#", "1")

                StrQ1 &= " AND UM.companyunkid = " & dRow("companyunkid")

                StrQ &= " UNION " & StrQ1
            Next

            StrQ &= ") AS ATLA " & _
                    "WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("ECode")
                rpt_Rows.Item("Column2") = dtRow.Item("EName")
                rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("EffDate").ToString).ToShortDateString
                rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                'Nilay (28-Aug-2015) -- Start
                'rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("NetAmount")), GUI.fmtCurrency)
                rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("BalanceAmount")), GUI.fmtCurrency)
                'Nilay (28-Aug-2015) -- End
                rpt_Rows.Item("Column6") = dtRow.Item("LStatus")
                rpt_Rows.Item("Column7") = dtRow.Item("LAScheme")
                rpt_Rows.Item("Column8") = dtRow.Item("EMI_Duration")
                rpt_Rows.Item("Column9") = Format(CDec(dtRow.Item("EMI_Amt")), GUI.fmtCurrency)
                rpt_Rows.Item("Column10") = dtRow.Item("AName")
                rpt_Rows.Item("Column11") = dtRow.Item("AType")
                'Nilay (28-Aug-2015) -- Start
                'rpt_Rows.Item("Column12") = eZeeDate.convertDate(dtRow.Item("AuditDate").ToString) & vbCrLf & dtRow.Item("AuditTime")
                rpt_Rows.Item("Column12") = eZeeDate.convertDate(dtRow.Item("AuditDate").ToString) & " " & dtRow.Item("AuditTime")
                'Nilay (28-Aug-2015) -- End
                rpt_Rows.Item("Column13") = dtRow.Item("ATUser")
                rpt_Rows.Item("Column14") = dtRow.Item("MachineIP")
                rpt_Rows.Item("Column15") = dtRow.Item("MName")
                rpt_Rows.Item("Column16") = dtRow.Item("EmpId")
                'Nilay (28-Aug-2015) -- Start
                rpt_Rows.Item("Column17") = dtRow.Item("VocNo")
                'Nilay (28-Aug-2015) -- End

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next

            objRpt = New ArutiReport.Designer.rptAT_LoanAdvanceReport

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 23, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 24, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 25, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 26, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 27, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 18, "Employee Name"))
            Call ReportFunction.TextChange(objRpt, "txtEDate", Language.getMessage(mstrModuleName, 38, "Effective Date"))
            Call ReportFunction.TextChange(objRpt, "txtLAmt", Language.getMessage(mstrModuleName, 21, "Loan Amount"))
            'Nilay (28-Aug-2015) -- Start
            'Call ReportFunction.TextChange(objRpt, "txtNAmt", Language.getMessage(mstrModuleName, 22, "Net Amount"))
            Call ReportFunction.TextChange(objRpt, "txtBAmt", Language.getMessage(mstrModuleName, 22, "Balance Amount"))
            'Nilay (28-Aug-2015) -- End
            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 28, "Status"))
            Call ReportFunction.TextChange(objRpt, "txtScheme", Language.getMessage(mstrModuleName, 29, "Scheme"))
            Call ReportFunction.TextChange(objRpt, "txtemiTanure", Language.getMessage(mstrModuleName, 30, "EMI Duration"))
            Call ReportFunction.TextChange(objRpt, "txtemiAmt", Language.getMessage(mstrModuleName, 31, "EMI Amount"))
            Call ReportFunction.TextChange(objRpt, "txtApprover", Language.getMessage(mstrModuleName, 39, "Approver"))
            Call ReportFunction.TextChange(objRpt, "txtATUser", Language.getMessage(mstrModuleName, 20, "Audit User"))
            Call ReportFunction.TextChange(objRpt, "txtAType", Language.getMessage(mstrModuleName, 19, "Audit Type"))
            Call ReportFunction.TextChange(objRpt, "txtADateTime", Language.getMessage(mstrModuleName, 32, "Audit Date & Time"))
            Call ReportFunction.TextChange(objRpt, "txtIPAddr", Language.getMessage(mstrModuleName, 33, "IP Address"))
            Call ReportFunction.TextChange(objRpt, "txtMName", Language.getMessage(mstrModuleName, 34, "Machine Name"))
            'Nilay (28-Aug-2015) -- Start
            Call ReportFunction.TextChange(objRpt, "txtVocNo", Language.getMessage(mstrModuleName, 41, "Voucher No."))
            'Nilay (28-Aug-2015) -- End
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 35, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 36, "Printed Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 37, "Page :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Private Function Generate_DetailReport(ByVal xDatabaseName As String, _
    '                                       ByVal xUserUnkid As Integer, _
    '                                       ByVal xYearUnkid As Integer, _
    '                                       ByVal xCompanyUnkid As Integer, _
    '                                       ByVal xPeriodStart As DateTime, _
    '                                       ByVal xPeriodEnd As DateTime, _
    '                                       ByVal xUserModeSetting As String, _
    '                                       ByVal xOnlyApproved As Boolean) As CrystalDecisions.CrystalReports.Engine.ReportClass

    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport

    '    Try

    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
    '        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

    '        objDataOperation = New clsDataOperation

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, 1, mintFinYearId, , True)

    '        'Nilay (10-Oct-2015) -- Start
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, mintFinYearId, 1, , True)
    '        Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, 1, , True)
    '        'Nilay (10-Oct-2015) -- End


    '        'S.SANDEEP [04 JUN 2015] -- END

    '        Dim dtAsONDate As DateTime = Nothing
    '        If intFirstOpenPeriodID <= 0 Then
    '            dtAsONDate = Now
    '        Else
    '            Dim objPrd As New clscommom_period_Tran
    '            objPrd._Periodunkid(xDatabaseName) = intFirstOpenPeriodID
    '            dtAsONDate = objPrd._End_Date
    '            objPrd = Nothing
    '        End If

    '        StrQ = "SELECT " & _
    '                  " VocNo as VocNo " & _
    '                  ",ECode AS ECode " & _
    '                  ",EName AS EName " & _
    '                  ",EffDate AS EffDate " & _
    '                  ",Amount AS Amount " & _
    '                  ",BalanceAmount AS BalanceAmount " & _
    '                  ",CASE WHEN ISNULL(ATLA.PeriodId,0)=0 THEN @FutureLoan ELSE ATLA.LStatus END AS LStatus " & _
    '                  ",(LAState+' --> '+LAScheme) AS LAScheme " & _
    '                  ",EMI_Duration AS EMI_Duration " & _
    '                  ",EMI_Amt AS EMI_Amt " & _
    '                  ",AName AS AName " & _
    '                  ",AType AS AType " & _
    '                  ",AuditDate AS AuditDate " & _
    '                  ",AuditTime AS AuditTime " & _
    '                  ",ATUser AS ATUser " & _
    '                  ",MachineIP AS MachineIP " & _
    '                  ",MName AS MName " & _
    '                  ",EmpId AS EmpId " & _
    '                  ",ATypeId AS ATypeId " & _
    '                "FROM " & _
    '                "( " & _
    '                     "SELECT " & _
    '                         " ISNULL(atlnloan_advance_tran.loanvoucher_no,'') AS VocNo " & _
    '                         ",ISNULL(hremployee_master.employeecode,'') AS ECode " & _
    '                         ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
    '                         ",CONVERT(CHAR(8),effective_date,112) AS EffDate " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(atlnloan_advance_tran.loan_amount,0) " & _
    '                                "WHEN atlnloan_advance_tran.isloan = 0 THEN ISNULL(atlnloan_advance_tran.advance_amount,0) END AS Amount " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(atlnloan_advance_tran.balance_amountpaidcurrency,0) " & _
    '                                "WHEN atlnloan_advance_tran.isloan = 0 THEN ISNULL(atlnloan_advance_tran.advance_amount,0) END AS BalanceAmount " & _
    '                         ",CASE WHEN atlnloan_advance_tran.loan_statusunkid = 1 THEN @InPrg " & _
    '                                "WHEN atlnloan_advance_tran.loan_statusunkid = 2 THEN @OnHld " & _
    '                                "WHEN atlnloan_advance_tran.loan_statusunkid = 3 THEN @WtOff " & _
    '                                "WHEN atlnloan_advance_tran.loan_statusunkid = 4 THEN @Cmplt END AS LStatus " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN @Loan " & _
    '                                "WHEN atlnloan_advance_tran.isloan = 0 THEN @Adva END AS LAState " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') " & _
    '                                "WHEN atlnloan_advance_tran.isloan = 0 THEN @Adva END AS LAScheme " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN CAST (ISNULL(emi_tenure,'') AS NVARCHAR(50)) " & _
    '                                "WHEN atlnloan_advance_tran.isloan = 0 THEN '' END AS EMI_Duration " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(emi_amount,0) " & _
    '                                "WHEN atlnloan_advance_tran.isloan = 0 THEN 0 END AS EMI_Amt " & _
    '                         ",ISNULL(Apprv.firstname,'')+' '+ISNULL(Apprv.othername,'')+' '+ISNULL(Apprv.surname,'') AS AName " & _
    '                         ",CASE WHEN audittype = 1 THEN @Add " & _
    '                                "WHEN audittype = 2 THEN @Edit " & _
    '                                "WHEN audittype = 3 THEN @Delete END AS AType " & _
    '                         ",CONVERT(CHAR(8),auditdatetime,112) AS AuditDate " & _
    '                         ",CONVERT(CHAR(8),auditdatetime,108) AS AuditTime " & _
    '                         ",ISNULL(hrmsConfiguration..cfuser_master.username,'') AS ATUser " & _
    '                         ",ISNULL(ip,'') AS MachineIP " & _
    '                         ",ISNULL(machine_name,'') AS MName " & _
    '                         ",hremployee_master.employeeunkid AS EmpId " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN 1 " & _
    '                "			  WHEN atlnloan_advance_tran.isloan = 0 THEN 2 END AS ALStateId " & _
    '                         ",audittype AS ATypeId " & _
    '                         ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS ALSchemeId " & _
    '                         ",ISNULL(Apprv.employeeunkid,0) AS ApprId " & _
    '                         ",CASE WHEN ISNULL(CP.periodunkid,0)=0 THEN -999 ELSE atlnloan_advance_tran.loan_statusunkid END AS LAStatusId " & _
    '                         ",hrmsConfiguration..cfuser_master.userunkid AS ATUserId " & _
    '                         ",CP.periodunkid as PeriodId " & _
    '                     "FROM atlnloan_advance_tran " & _
    '                          "JOIN hrmsConfiguration..cfuser_master ON atlnloan_advance_tran.audituserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
    '                          "LEFT JOIN lnloan_scheme_master ON atlnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
    '                          "LEFT JOIN lnloan_process_pending_loan ON atlnloan_advance_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
    '                                "AND atlnloan_advance_tran.approverunkid = lnloan_process_pending_loan.approverunkid " & _
    '                          "LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_process_pending_loan.approverunkid " & _
    '                          "LEFT JOIN hremployee_master ON atlnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                          "LEFT JOIN hremployee_master AS Apprv ON Apprv.employeeunkid = lnloanapprover_master.approverempunkid " & _
    '                          "LEFT JOIN lnloan_status_tran on lnloan_status_tran.loanadvancetranunkid = atlnloan_advance_tran.loanadvancetranunkid " & _
    '                          "LEFT JOIN cfcommon_period_tran as CP on CP.periodunkid = lnloan_status_tran.periodunkid " & _
    '                                " AND CP.modulerefid = 1 AND CONVERT(CHAR(8),CP.start_date,112) <= '" & eZeeDate.convertDate(dtAsONDate) & "' " & _
    '                          "LEFT JOIN " & _
    '                          "( " & _
    '                          "      SELECT " & _
    '                          "           stationunkid " & _
    '                          "          ,employeeunkid " & _
    '                          "          ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                          "      FROM hremployee_transfer_tran " & _
    '                          "      WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                          ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "

    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= xDateJoinQry
    '        End If

    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrQ &= xAdvanceJoinQry
    '        End If

    '        StrQ &= " WHERE CONVERT(CHAR(8),auditdatetime,112) BETWEEN @FDate AND @TDate "


    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If

    '        If mblnIncludeInactiveEmp = False Then
    '            If xDateFilterQry.Trim.Length > 0 Then
    '                StrQ &= xDateFilterQry
    '            End If
    '        End If

    '        If mintBranchId > 0 Then
    '            'StrQ &= " AND hremployee_master.stationunkid = " & mintBranchId
    '            StrQ &= " AND Alloc.stationunkid = " & mintBranchId
    '        End If

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        StrQ &= ") AS ATLA " & _
    '                "WHERE 1 = 1 "

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Rows.Item("Column1") = dtRow.Item("ECode")
    '            rpt_Rows.Item("Column2") = dtRow.Item("EName")
    '            rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("EffDate").ToString).ToShortDateString
    '            rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
    '            'Nilay (28-Aug-2015) -- Start
    '            'rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("NetAmount")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("BalanceAmount")), GUI.fmtCurrency)
    '            'Nilay (28-Aug-2015) -- End
    '            rpt_Rows.Item("Column6") = dtRow.Item("LStatus")
    '            rpt_Rows.Item("Column7") = dtRow.Item("LAScheme")
    '            rpt_Rows.Item("Column8") = dtRow.Item("EMI_Duration")
    '            rpt_Rows.Item("Column9") = Format(CDec(dtRow.Item("EMI_Amt")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column10") = dtRow.Item("AName")
    '            rpt_Rows.Item("Column11") = dtRow.Item("AType")
    '            'Nilay (28-Aug-2015) -- Start
    '            'rpt_Rows.Item("Column12") = eZeeDate.convertDate(dtRow.Item("AuditDate").ToString) & vbCrLf & dtRow.Item("AuditTime")
    '            rpt_Rows.Item("Column12") = eZeeDate.convertDate(dtRow.Item("AuditDate").ToString) & " " & dtRow.Item("AuditTime")
    '            'Nilay (28-Aug-2015) -- End
    '            rpt_Rows.Item("Column13") = dtRow.Item("ATUser")
    '            rpt_Rows.Item("Column14") = dtRow.Item("MachineIP")
    '            rpt_Rows.Item("Column15") = dtRow.Item("MName")
    '            rpt_Rows.Item("Column16") = dtRow.Item("EmpId")
    '            'Nilay (28-Aug-2015) -- Start
    '            rpt_Rows.Item("Column17") = dtRow.Item("VocNo")
    '            'Nilay (28-Aug-2015) -- End

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptAT_LoanAdvanceReport

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 23, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 24, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 25, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 26, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 27, "Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 18, "Employee Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtEDate", Language.getMessage(mstrModuleName, 38, "Effective Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtLAmt", Language.getMessage(mstrModuleName, 21, "Loan Amount"))
    '        'Nilay (28-Aug-2015) -- Start
    '        'Call ReportFunction.TextChange(objRpt, "txtNAmt", Language.getMessage(mstrModuleName, 22, "Net Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtBAmt", Language.getMessage(mstrModuleName, 22, "Balance Amount"))
    '        'Nilay (28-Aug-2015) -- End
    '        Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 28, "Status"))
    '        Call ReportFunction.TextChange(objRpt, "txtScheme", Language.getMessage(mstrModuleName, 29, "Scheme"))
    '        Call ReportFunction.TextChange(objRpt, "txtemiTanure", Language.getMessage(mstrModuleName, 30, "EMI Duration"))
    '        Call ReportFunction.TextChange(objRpt, "txtemiAmt", Language.getMessage(mstrModuleName, 31, "EMI Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtApprover", Language.getMessage(mstrModuleName, 39, "Approver"))
    '        Call ReportFunction.TextChange(objRpt, "txtATUser", Language.getMessage(mstrModuleName, 20, "Audit User"))
    '        Call ReportFunction.TextChange(objRpt, "txtAType", Language.getMessage(mstrModuleName, 19, "Audit Type"))
    '        Call ReportFunction.TextChange(objRpt, "txtADateTime", Language.getMessage(mstrModuleName, 32, "Audit Date & Time"))
    '        Call ReportFunction.TextChange(objRpt, "txtIPAddr", Language.getMessage(mstrModuleName, 33, "IP Address"))
    '        Call ReportFunction.TextChange(objRpt, "txtMName", Language.getMessage(mstrModuleName, 34, "Machine Name"))
    '        'Nilay (28-Aug-2015) -- Start
    '        Call ReportFunction.TextChange(objRpt, "txtVocNo", Language.getMessage(mstrModuleName, 41, "Voucher No."))
    '        'Nilay (28-Aug-2015) -- End
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 35, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 36, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 37, "Page :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
    'Nilay (01-Mar-2016) -- End



    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation


    '        'S.SANDEEP [ 24 JUNE 2011 ] -- START
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'StrQ = "SELECT " & _
    '        '          " ECode AS ECode " & _
    '        '          ",EName AS EName " & _
    '        '          ",EffDate AS EffDate " & _
    '        '          ",Amount AS Amount " & _
    '        '          ",NetAmount AS NetAmount " & _
    '        '          ",LStatus AS LStatus " & _
    '        '          ",(LAState+' --> '+LAScheme) AS LAScheme " & _
    '        '          ",EMI_Duration AS EMI_Duration " & _
    '        '          ",EMI_Amt AS EMI_Amt " & _
    '        '          ",AName AS AName " & _
    '        '          ",AType AS AType " & _
    '        '          ",AuditDate AS AuditDate " & _
    '        '          ",AuditTime AS AuditTime " & _
    '        '          ",ATUser AS ATUser " & _
    '        '          ",MachineIP AS MachineIP " & _
    '        '          ",MName AS MName " & _
    '        '          ",EmpId AS EmpId " & _
    '        '          ",ATypeId AS ATypeId " & _
    '        '        "FROM " & _
    '        '        "( " & _
    '        '             "SELECT " & _
    '        '                 " ISNULL(hremployee_master.employeecode,'') AS ECode " & _
    '        '                 ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
    '        '                 ",CONVERT(CHAR(8),effective_date,112) AS EffDate " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(atlnloan_advance_tran.loan_amount,0) " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN ISNULL(atlnloan_advance_tran.advance_amount,0) END AS Amount " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(atlnloan_advance_tran.net_amount,0) " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN ISNULL(atlnloan_advance_tran.advance_amount,0) END AS NetAmount " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.loan_statusunkid = 1 THEN @InPrg " & _
    '        '                        "WHEN atlnloan_advance_tran.loan_statusunkid = 2 THEN @OnHld " & _
    '        '                        "WHEN atlnloan_advance_tran.loan_statusunkid = 3 THEN @WtOff " & _
    '        '                        "WHEN atlnloan_advance_tran.loan_statusunkid = 4 THEN @Cmplt END AS LStatus " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN @Loan " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN @Adva END AS LAState " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN @Adva END AS LAScheme " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN CAST (ISNULL(emi_tenure,'') AS NVARCHAR(50)) " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN '' END AS EMI_Duration " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(emi_amount,0) " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN 0 END AS EMI_Amt " & _
    '        '                 ",ISNULL(Apprv.firstname,'')+' '+ISNULL(Apprv.othername,'')+' '+ISNULL(Apprv.surname,'') AS AName " & _
    '        '                 ",CASE WHEN audittype = 1 THEN @Add " & _
    '        '                        "WHEN audittype = 2 THEN @Edit " & _
    '        '                        "WHEN audittype = 3 THEN @Delete END AS AType " & _
    '        '                 ",CONVERT(CHAR(8),auditdatetime,112) AS AuditDate " & _
    '        '                 ",CONVERT(CHAR(8),auditdatetime,108) AS AuditTime " & _
    '        '                 ",ISNULL(hrmsConfiguration..cfuser_master.username,'') AS ATUser " & _
    '        '                 ",ISNULL(ip,'') AS MachineIP " & _
    '        '                 ",ISNULL(machine_name,'') AS MName " & _
    '        '                 ",hremployee_master.employeeunkid AS EmpId " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN 1 " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN 2 " & _
    '        '                  "END AS ALStateId " & _
    '        '                 ",audittype AS ATypeId " & _
    '        '                 ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS ALSchemeId " & _
    '        '                 ",ISNULL(Apprv.employeeunkid,0) AS ApprId " & _
    '        '                 ",atlnloan_advance_tran.loan_statusunkid AS LAStatusId " & _
    '        '                 ",hrmsConfiguration..cfuser_master.userunkid AS ATUserId " & _
    '        '             "FROM atlnloan_advance_tran " & _
    '        '                  "JOIN hrmsConfiguration..cfuser_master ON atlnloan_advance_tran.audituserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
    '        '                  "LEFT JOIN lnloan_scheme_master ON atlnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
    '        '                  "JOIN lnloan_process_pending_loan ON atlnloan_advance_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
    '        '                  "JOIN hremployee_master ON atlnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                  "JOIN dbo.hremployee_master AS Apprv ON atlnloan_advance_tran.approverunkid = Apprv.employeeunkid " & _
    '        '             "WHERE CONVERT(CHAR(8),auditdatetime,112) BETWEEN @FDate AND @TDate " & _
    '        '        ") AS ATLA " & _
    '        '        "WHERE 1 = 1 "


    '        'Nilay (28-Aug-2015) -- Start
    '        'StrQ = "SELECT " & _
    '        '          " VocNo as VocNo " & _
    '        '          ",ECode AS ECode " & _
    '        '          ",EName AS EName " & _
    '        '          ",EffDate AS EffDate " & _
    '        '          ",Amount AS Amount " & _
    '        '          ",BalanceAmount AS BalanceAmount " & _
    '        '          ",LStatus AS LStatus " & _
    '        '          ",(LAState+' --> '+LAScheme) AS LAScheme " & _
    '        '          ",EMI_Duration AS EMI_Duration " & _
    '        '          ",EMI_Amt AS EMI_Amt " & _
    '        '          ",AName AS AName " & _
    '        '          ",AType AS AType " & _
    '        '          ",AuditDate AS AuditDate " & _
    '        '          ",AuditTime AS AuditTime " & _
    '        '          ",ATUser AS ATUser " & _
    '        '          ",MachineIP AS MachineIP " & _
    '        '          ",MName AS MName " & _
    '        '          ",EmpId AS EmpId " & _
    '        '          ",ATypeId AS ATypeId " & _
    '        '        "FROM " & _
    '        '        "( " & _
    '        '             "SELECT " & _
    '        '                 " ISNULL(atlnloan_advance_tran.loanvoucher_no,'') AS VocNo " & _
    '        '                 ",ISNULL(hremployee_master.employeecode,'') AS ECode " & _
    '        '                 ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
    '        '                 ",CONVERT(CHAR(8),effective_date,112) AS EffDate " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(atlnloan_advance_tran.loan_amount,0) " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN ISNULL(atlnloan_advance_tran.advance_amount,0) END AS Amount " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(atlnloan_advance_tran.balance_amountpaidcurrency,0) " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN ISNULL(atlnloan_advance_tran.advance_amount,0) END AS BalanceAmount " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.loan_statusunkid = 1 THEN @InPrg " & _
    '        '                        "WHEN atlnloan_advance_tran.loan_statusunkid = 2 THEN @OnHld " & _
    '        '                        "WHEN atlnloan_advance_tran.loan_statusunkid = 3 THEN @WtOff " & _
    '        '                        "WHEN atlnloan_advance_tran.loan_statusunkid = 4 THEN @Cmplt END AS LStatus " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN @Loan " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN @Adva END AS LAState " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN @Adva END AS LAScheme " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN CAST (ISNULL(emi_tenure,'') AS NVARCHAR(50)) " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN '' END AS EMI_Duration " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(emi_amount,0) " & _
    '        '                        "WHEN atlnloan_advance_tran.isloan = 0 THEN 0 END AS EMI_Amt " & _
    '        '                 ",ISNULL(Apprv.firstname,'')+' '+ISNULL(Apprv.othername,'')+' '+ISNULL(Apprv.surname,'') AS AName " & _
    '        '                 ",CASE WHEN audittype = 1 THEN @Add " & _
    '        '                        "WHEN audittype = 2 THEN @Edit " & _
    '        '                        "WHEN audittype = 3 THEN @Delete END AS AType " & _
    '        '                 ",CONVERT(CHAR(8),auditdatetime,112) AS AuditDate " & _
    '        '                 ",CONVERT(CHAR(8),auditdatetime,108) AS AuditTime " & _
    '        '                 ",ISNULL(hrmsConfiguration..cfuser_master.username,'') AS ATUser " & _
    '        '                 ",ISNULL(ip,'') AS MachineIP " & _
    '        '                 ",ISNULL(machine_name,'') AS MName " & _
    '        '                 ",hremployee_master.employeeunkid AS EmpId " & _
    '        '                 ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN 1 " & _
    '        '                 "WHEN atlnloan_advance_tran.isloan = 0 THEN 2 END AS ALStateId " & _
    '        '                 ",audittype AS ATypeId " & _
    '        '                 ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS ALSchemeId " & _
    '        '                 ",ISNULL(Apprv.employeeunkid,0) AS ApprId " & _
    '        '                 ",atlnloan_advance_tran.loan_statusunkid AS LAStatusId " & _
    '        '                 ",hrmsConfiguration..cfuser_master.userunkid AS ATUserId " & _
    '        '             "FROM atlnloan_advance_tran " & _
    '        '                 "JOIN hrmsConfiguration..cfuser_master ON atlnloan_advance_tran.audituserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
    '        '                 "LEFT JOIN lnloan_scheme_master ON atlnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
    '        '                 "JOIN lnloan_process_pending_loan ON atlnloan_advance_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
    '        '                 "JOIN hremployee_master ON atlnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                 "JOIN hremployee_master AS Apprv ON atlnloan_advance_tran.approverunkid = Apprv.employeeunkid " & _
    '        '        "	WHERE CONVERT(CHAR(8),auditdatetime,112) BETWEEN @FDate AND @TDate "

    '        Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, 1, mintFinYearId, , True)
    '        Dim dtAsONDate As DateTime = Nothing
    '        If intFirstOpenPeriodID <= 0 Then
    '            dtAsONDate = Now
    '        Else
    '            Dim objPrd As New clscommom_period_Tran
    '            objPrd._Periodunkid = intFirstOpenPeriodID
    '            dtAsONDate = objPrd._End_Date
    '            objPrd = Nothing
    '        End If

    '        StrQ = "SELECT " & _
    '                  " VocNo as VocNo " & _
    '                  ",ECode AS ECode " & _
    '                  ",EName AS EName " & _
    '                  ",EffDate AS EffDate " & _
    '                  ",Amount AS Amount " & _
    '                  ",BalanceAmount AS BalanceAmount " & _
    '                  ",CASE WHEN ISNULL(ATLA.PeriodId,0)=0 THEN @FutureLoan ELSE ATLA.LStatus END AS LStatus " & _
    '                  ",(LAState+' --> '+LAScheme) AS LAScheme " & _
    '                  ",EMI_Duration AS EMI_Duration " & _
    '                  ",EMI_Amt AS EMI_Amt " & _
    '                  ",AName AS AName " & _
    '                  ",AType AS AType " & _
    '                  ",AuditDate AS AuditDate " & _
    '                  ",AuditTime AS AuditTime " & _
    '                  ",ATUser AS ATUser " & _
    '                  ",MachineIP AS MachineIP " & _
    '                  ",MName AS MName " & _
    '                  ",EmpId AS EmpId " & _
    '                  ",ATypeId AS ATypeId " & _
    '                "FROM " & _
    '                "( " & _
    '                     "SELECT " & _
    '                         " ISNULL(atlnloan_advance_tran.loanvoucher_no,'') AS VocNo " & _
    '                         ",ISNULL(hremployee_master.employeecode,'') AS ECode " & _
    '                         ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
    '                         ",CONVERT(CHAR(8),effective_date,112) AS EffDate " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(atlnloan_advance_tran.loan_amount,0) " & _
    '                                "WHEN atlnloan_advance_tran.isloan = 0 THEN ISNULL(atlnloan_advance_tran.advance_amount,0) END AS Amount " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(atlnloan_advance_tran.balance_amountpaidcurrency,0) " & _
    '                                "WHEN atlnloan_advance_tran.isloan = 0 THEN ISNULL(atlnloan_advance_tran.advance_amount,0) END AS BalanceAmount " & _
    '                         ",CASE WHEN atlnloan_advance_tran.loan_statusunkid = 1 THEN @InPrg " & _
    '                                "WHEN atlnloan_advance_tran.loan_statusunkid = 2 THEN @OnHld " & _
    '                                "WHEN atlnloan_advance_tran.loan_statusunkid = 3 THEN @WtOff " & _
    '                                "WHEN atlnloan_advance_tran.loan_statusunkid = 4 THEN @Cmplt END AS LStatus " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN @Loan " & _
    '                                "WHEN atlnloan_advance_tran.isloan = 0 THEN @Adva END AS LAState " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(lnloan_scheme_master.name,'') " & _
    '                                "WHEN atlnloan_advance_tran.isloan = 0 THEN @Adva END AS LAScheme " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN CAST (ISNULL(emi_tenure,'') AS NVARCHAR(50)) " & _
    '                                "WHEN atlnloan_advance_tran.isloan = 0 THEN '' END AS EMI_Duration " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN ISNULL(emi_amount,0) " & _
    '                                "WHEN atlnloan_advance_tran.isloan = 0 THEN 0 END AS EMI_Amt " & _
    '                         ",ISNULL(Apprv.firstname,'')+' '+ISNULL(Apprv.othername,'')+' '+ISNULL(Apprv.surname,'') AS AName " & _
    '                         ",CASE WHEN audittype = 1 THEN @Add " & _
    '                                "WHEN audittype = 2 THEN @Edit " & _
    '                                "WHEN audittype = 3 THEN @Delete END AS AType " & _
    '                         ",CONVERT(CHAR(8),auditdatetime,112) AS AuditDate " & _
    '                         ",CONVERT(CHAR(8),auditdatetime,108) AS AuditTime " & _
    '                         ",ISNULL(hrmsConfiguration..cfuser_master.username,'') AS ATUser " & _
    '                         ",ISNULL(ip,'') AS MachineIP " & _
    '                         ",ISNULL(machine_name,'') AS MName " & _
    '                         ",hremployee_master.employeeunkid AS EmpId " & _
    '                         ",CASE WHEN atlnloan_advance_tran.isloan = 1 THEN 1 " & _
    '                "			  WHEN atlnloan_advance_tran.isloan = 0 THEN 2 END AS ALStateId " & _
    '                         ",audittype AS ATypeId " & _
    '                         ",ISNULL(lnloan_scheme_master.loanschemeunkid,0) AS ALSchemeId " & _
    '                         ",ISNULL(Apprv.employeeunkid,0) AS ApprId " & _
    '                         ",CASE WHEN ISNULL(CP.periodunkid,0)=0 THEN -999 ELSE atlnloan_advance_tran.loan_statusunkid END AS LAStatusId " & _
    '                         ",hrmsConfiguration..cfuser_master.userunkid AS ATUserId " & _
    '                         ",CP.periodunkid as PeriodId " & _
    '                     "FROM atlnloan_advance_tran " & _
    '                          "JOIN hrmsConfiguration..cfuser_master ON atlnloan_advance_tran.audituserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
    '                          "LEFT JOIN lnloan_scheme_master ON atlnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
    '                          "LEFT JOIN lnloan_process_pending_loan ON atlnloan_advance_tran.processpendingloanunkid = lnloan_process_pending_loan.processpendingloanunkid " & _
    '                                "AND atlnloan_advance_tran.approverunkid = lnloan_process_pending_loan.approverunkid " & _
    '                          "LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_process_pending_loan.approverunkid " & _
    '                          "LEFT JOIN hremployee_master ON atlnloan_advance_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                          "LEFT JOIN hremployee_master AS Apprv ON Apprv.employeeunkid = lnloanapprover_master.approverempunkid " & _
    '                          "LEFT JOIN lnloan_status_tran on lnloan_status_tran.loanadvancetranunkid = atlnloan_advance_tran.loanadvancetranunkid " & _
    '                          "LEFT JOIN cfcommon_period_tran as CP on CP.periodunkid = lnloan_status_tran.periodunkid " & _
    '                                " AND CP.modulerefid = 1 AND CONVERT(CHAR(8),CP.start_date,112) <= '" & eZeeDate.convertDate(dtAsONDate) & "' " & _
    '                "	WHERE CONVERT(CHAR(8),auditdatetime,112) BETWEEN @FDate AND @TDate "

    '        'Nilay (28-Aug-2015) -- End

    '        'Pinkal (27-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If
    '        'Pinkal (27-Feb-2013) -- End

    '        If mblnIncludeInactiveEmp = False Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.isactive = 1 "
    '            StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '            'Sohail (17 Apr 2012) -- End
    '        End If

    '        'S.SANDEEP [ 17 AUG 2011 ] -- START
    '        'ENHANCEMENT : BRANCH ( STATION )INCLUSION IN FILTER
    '        If mintBranchId > 0 Then
    '            StrQ &= " AND hremployee_master.stationunkid = " & mintBranchId
    '        End If
    '        'S.SANDEEP [ 17 AUG 2011 ] -- END 

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            'Sohail (17 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'Sohail (17 Apr 2012) -- End
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= ") AS ATLA " & _
    '                "WHERE 1 = 1 "

    '        'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    '        Call FilterTitleAndFilterQuery()

    '        StrQ &= Me._FilterQuery

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Rows As DataRow = rpt_Data.Tables("ArutiTable").NewRow

    '            rpt_Rows.Item("Column1") = dtRow.Item("ECode")
    '            rpt_Rows.Item("Column2") = dtRow.Item("EName")
    '            rpt_Rows.Item("Column3") = eZeeDate.convertDate(dtRow.Item("EffDate").ToString).ToShortDateString
    '            rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
    '            'Nilay (28-Aug-2015) -- Start
    '            'rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("NetAmount")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column5") = Format(CDec(dtRow.Item("BalanceAmount")), GUI.fmtCurrency)
    '            'Nilay (28-Aug-2015) -- End
    '            rpt_Rows.Item("Column6") = dtRow.Item("LStatus")
    '            rpt_Rows.Item("Column7") = dtRow.Item("LAScheme")
    '            rpt_Rows.Item("Column8") = dtRow.Item("EMI_Duration")
    '            rpt_Rows.Item("Column9") = Format(CDec(dtRow.Item("EMI_Amt")), GUI.fmtCurrency)
    '            rpt_Rows.Item("Column10") = dtRow.Item("AName")
    '            rpt_Rows.Item("Column11") = dtRow.Item("AType")
    '            'Nilay (28-Aug-2015) -- Start
    '            'rpt_Rows.Item("Column12") = eZeeDate.convertDate(dtRow.Item("AuditDate").ToString) & vbCrLf & dtRow.Item("AuditTime")
    '            rpt_Rows.Item("Column12") = eZeeDate.convertDate(dtRow.Item("AuditDate").ToString) & " " & dtRow.Item("AuditTime")
    '            'Nilay (28-Aug-2015) -- End
    '            rpt_Rows.Item("Column13") = dtRow.Item("ATUser")
    '            rpt_Rows.Item("Column14") = dtRow.Item("MachineIP")
    '            rpt_Rows.Item("Column15") = dtRow.Item("MName")
    '            rpt_Rows.Item("Column16") = dtRow.Item("EmpId")
    '            'Nilay (28-Aug-2015) -- Start
    '            rpt_Rows.Item("Column17") = dtRow.Item("VocNo")
    '            'Nilay (28-Aug-2015) -- End

    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '        Next

    '        objRpt = New ArutiReport.Designer.rptAT_LoanAdvanceReport

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 23, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 24, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 25, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 26, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeCode", Language.getMessage(mstrModuleName, 27, "Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmployeeName", Language.getMessage(mstrModuleName, 18, "Employee Name"))
    '        Call ReportFunction.TextChange(objRpt, "txtEDate", Language.getMessage(mstrModuleName, 38, "Effective Date"))
    '        Call ReportFunction.TextChange(objRpt, "txtLAmt", Language.getMessage(mstrModuleName, 21, "Loan Amount"))
    '        'Nilay (28-Aug-2015) -- Start
    '        'Call ReportFunction.TextChange(objRpt, "txtNAmt", Language.getMessage(mstrModuleName, 22, "Net Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtBAmt", Language.getMessage(mstrModuleName, 22, "Balance Amount"))
    '        'Nilay (28-Aug-2015) -- End
    '        Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 28, "Status"))
    '        Call ReportFunction.TextChange(objRpt, "txtScheme", Language.getMessage(mstrModuleName, 29, "Scheme"))
    '        Call ReportFunction.TextChange(objRpt, "txtemiTanure", Language.getMessage(mstrModuleName, 30, "EMI Duration"))
    '        Call ReportFunction.TextChange(objRpt, "txtemiAmt", Language.getMessage(mstrModuleName, 31, "EMI Amount"))
    '        Call ReportFunction.TextChange(objRpt, "txtApprover", Language.getMessage(mstrModuleName, 39, "Approver"))
    '        Call ReportFunction.TextChange(objRpt, "txtATUser", Language.getMessage(mstrModuleName, 20, "Audit User"))
    '        Call ReportFunction.TextChange(objRpt, "txtAType", Language.getMessage(mstrModuleName, 19, "Audit Type"))
    '        Call ReportFunction.TextChange(objRpt, "txtADateTime", Language.getMessage(mstrModuleName, 32, "Audit Date & Time"))
    '        Call ReportFunction.TextChange(objRpt, "txtIPAddr", Language.getMessage(mstrModuleName, 33, "IP Address"))
    '        Call ReportFunction.TextChange(objRpt, "txtMName", Language.getMessage(mstrModuleName, 34, "Machine Name"))
    '        'Nilay (28-Aug-2015) -- Start
    '        Call ReportFunction.TextChange(objRpt, "txtVocNo", Language.getMessage(mstrModuleName, 41, "Voucher No."))
    '        'Nilay (28-Aug-2015) -- End
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 35, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 36, "Printed Date :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 37, "Page :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    'Nilay (10-Oct-2015) -- End


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Advance")
            Language.setMessage(mstrModuleName, 2, "Loan")
            Language.setMessage(mstrModuleName, 3, "Add")
            Language.setMessage(mstrModuleName, 4, "Edit")
            Language.setMessage(mstrModuleName, 5, "Delete")
            Language.setMessage(mstrModuleName, 6, "Audit Date From :")
            Language.setMessage(mstrModuleName, 7, "To")
            Language.setMessage(mstrModuleName, 8, "Applied Date From :")
            Language.setMessage(mstrModuleName, 9, "Employee :")
            Language.setMessage(mstrModuleName, 10, "Audit Type :")
            Language.setMessage(mstrModuleName, 11, "Loan Scheme :")
            Language.setMessage(mstrModuleName, 12, "Approver :")
            Language.setMessage(mstrModuleName, 13, "Status :")
            Language.setMessage(mstrModuleName, 14, "Audit User :")
            Language.setMessage(mstrModuleName, 15, "Order By :")
            Language.setMessage(mstrModuleName, 16, "Audit Date")
            Language.setMessage(mstrModuleName, 17, "Audit Time")
            Language.setMessage(mstrModuleName, 18, "Employee Name")
            Language.setMessage(mstrModuleName, 19, "Audit Type")
            Language.setMessage(mstrModuleName, 20, "Audit User")
            Language.setMessage(mstrModuleName, 21, "Loan Amount")
            Language.setMessage(mstrModuleName, 22, "Balance Amount")
            Language.setMessage(mstrModuleName, 23, "Prepared By :")
            Language.setMessage(mstrModuleName, 24, "Checked By :")
            Language.setMessage(mstrModuleName, 25, "Approved By :")
            Language.setMessage(mstrModuleName, 26, "Received By :")
            Language.setMessage(mstrModuleName, 27, "Code")
            Language.setMessage(mstrModuleName, 28, "Status")
            Language.setMessage(mstrModuleName, 29, "Scheme")
            Language.setMessage(mstrModuleName, 30, "EMI Duration")
            Language.setMessage(mstrModuleName, 31, "EMI Amount")
            Language.setMessage(mstrModuleName, 32, "Audit Date & Time")
            Language.setMessage(mstrModuleName, 33, "IP Address")
            Language.setMessage(mstrModuleName, 34, "Machine Name")
            Language.setMessage(mstrModuleName, 35, "Printed By :")
            Language.setMessage(mstrModuleName, 36, "Printed Date :")
            Language.setMessage(mstrModuleName, 37, "Page :")
            Language.setMessage(mstrModuleName, 38, "Effective Date")
            Language.setMessage(mstrModuleName, 39, "Approver")
            Language.setMessage(mstrModuleName, 40, "Branch :")
            Language.setMessage("clsMasterData", 96, "In Progress")
            Language.setMessage("clsMasterData", 97, "On Hold")
            Language.setMessage("clsMasterData", 98, "Written Off")
            Language.setMessage("clsMasterData", 100, "Completed")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
