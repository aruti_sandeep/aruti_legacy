'************************************************************************************************************************************
'Class Name : frmMAP_Malawi_Report.vb
'Purpose    : 
'Written By : Sandeep Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmMAP_Malawi_Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmMAP_Malawi_Report"
    Private objMAP As clsMAP_Malawi_Report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Constructor "

    Public Sub New()
        InitializeComponent()
        _Show_AdvanceFilter = True
        objMAP = New clsMAP_Malawi_Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objMAP.SetDefaultValue()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objPrd As New clscommom_period_Tran
        Dim objMCat As New clsmedical_master
        Dim objEmplType As New clsCommon_Master
        Dim objMem As New clsmembership_master
        Try
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPrd.getListForCombo(enModuleReference.Payroll, Company._Object._YearUnkid, "Period", True)
            dsCombo = objPrd.getListForCombo(enModuleReference.Payroll, Company._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("Period")
                .SelectedValue = 0
            End With

            dsCombo = objMem.getListForCombo("List", True, , 1)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objMCat.getListForCombo(enMedicalMasterType.Medical_Category, "List", True)
            With cboMedCategory
                .ValueMember = "mdmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date)
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                            ConfigParameter._Object._UserAccessModeSetting, _
                                            True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("Emp")
                .SelectedValue = 0
            End With

            dsCombo = objEmplType.getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, "List")
            With cboEmplType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing : objPrd = Nothing : objMCat = Nothing : objEmplType = Nothing : objMem = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objMAP.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            End If

            objMAP._MedicalCatId = cboMedCategory.SelectedValue
            objMAP._MedicalCatName = cboMedCategory.Text

            objMAP._MembershipId = cboMembership.SelectedValue
            objMAP._MembershipName = cboMembership.Text

            objMAP._PeriodId = cboPeriod.SelectedValue
            objMAP._PeriodName = cboPeriod.Text

            objMAP._EmployeeId = cboEmployee.SelectedValue
            objMAP._EmployeeName = cboEmployee.Text

            objMAP._EmplTypeId = cboEmplType.SelectedValue
            objMAP._EmplTypeName = cboEmplType.Text

            objMAP._ViewByIds = mstrStringIds
            objMAP._ViewIndex = mintViewIdx
            objMAP._ViewByName = mstrStringName
            objMAP._Analysis_Fields = mstrAnalysis_Fields
            objMAP._Analysis_Join = mstrAnalysis_Join
            objMAP._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMAP._Report_GroupName = mstrReport_GroupName
            objMAP._AdvanceFilter = mstrAdvanceFilter
            objMAP._IncludeInactiveEmp = chkInactiveemp.Checked
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            objMAP._AsOnDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString)
            'Sohail (18 May 2019) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboEmplType.SelectedValue = 0
            cboMedCategory.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            txtAmountFrom.Text = "" : txtAmountTo.Text = ""
            txtDepFrom.Text = "" : txtDepTo.Text = ""
            chkInactiveemp.Checked = False
            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = String.Empty
            objMAP.setDefaultOrderBy(0)
            txtOrderBy.Text = objMAP.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmMAP_Malawi_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objMAP = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMAP_Malawi_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMAP_Malawi_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Me._Title = objMAP._ReportName
            Me._Message = objMAP._ReportDesc
            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMAP_Malawi_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMAP_Malawi_Report_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmMAP_Malawi_Report_KeyPress", mstrModuleName)
        End Try
    End Sub
    Private Sub frmMAP_Malawi_Report_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsMAP_Malawi_Report.SetMessages()
            objfrm._Other_ModuleNames = "clsMAP_Malawi_Report"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmMAP_Malawi_Report_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try

    End Sub
#End Region

#Region " Buttons "

    Private Sub frmMAP_Malawi_Report_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objMAP.generateReport(0, e.Type, enExportAction.None)
            objMAP.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Anjan [08 September 2015] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMAP_Malawi_Report_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalBillsummary_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub


            'Anjan [08 September 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objMAP.generateReport(0, enPrintAction.None, e.Type)
            objMAP.generateReportNew(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Anjan [08 September 2015] -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalBillsummary_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMAP_Malawi_Report_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMAP_Malawi_Report_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMAP_Malawi_Report_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMAP_Malawi_Report_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboEmployee.DataSource, DataTable)
            With cboEmployee
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

    Private Sub Form_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Form_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Private Sub objbtnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            objMAP.setOrderBy(0)
            txtOrderBy.Text = objMAP.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblMedCategory.Text = Language._Object.getCaption(Me.lblMedCategory.Name, Me.lblMedCategory.Text)
			Me.lblEmplType.Text = Language._Object.getCaption(Me.lblEmplType.Name, Me.lblEmplType.Text)
			Me.lblDependantTo.Text = Language._Object.getCaption(Me.lblDependantTo.Name, Me.lblDependantTo.Text)
			Me.lblDependantFrom.Text = Language._Object.getCaption(Me.lblDependantFrom.Name, Me.lblDependantFrom.Text)
			Me.lblCatAmtTo.Text = Language._Object.getCaption(Me.lblCatAmtTo.Name, Me.lblCatAmtTo.Text)
			Me.lblCatAmtFrom.Text = Language._Object.getCaption(Me.lblCatAmtFrom.Name, Me.lblCatAmtFrom.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.Name, Me.chkInactiveemp.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Membership is mandatory information. Please select Membership to continue.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
