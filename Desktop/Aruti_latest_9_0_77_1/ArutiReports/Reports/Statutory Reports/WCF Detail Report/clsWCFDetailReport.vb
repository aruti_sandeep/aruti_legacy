﻿Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Text
Imports System.Text.RegularExpressions

Public Class clsWCFDetailReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsWCFDetailReport"
    Private mstrReportId As String = enArutiReport.WCF_Detail_Report
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "
    Private mintPeriodId As Integer = -1
    Private mstrPeriodCode As String = String.Empty
    Private mstrPeriodName As String = String.Empty
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mintIdentityId As Integer = 0
    Private mstrIdentityName As String = String.Empty
    Private mintWCFId As Integer = 0
    Private mstrWCFName As String = String.Empty
    Private mintBasicPayHeadId As Integer = 0
    Private mstrGrossPayFormula As String = String.Empty
    Private mstrfmtCurrency As String = GUI.fmtCurrency
    Private mdecConversionRate As Decimal = 0
#End Region

#Region " Properties "

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodCode() As String
        Set(ByVal value As String)
            mstrPeriodCode = value
        End Set
    End Property

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _WCFId() As Integer
        Set(ByVal value As Integer)
            mintWCFId = value
        End Set
    End Property

    Public WriteOnly Property _WCFName() As String
        Set(ByVal value As String)
            mstrWCFName = value
        End Set
    End Property

    Public WriteOnly Property _IdentityId() As Integer
        Set(ByVal value As Integer)
            mintIdentityId = value
        End Set
    End Property

    Public WriteOnly Property _IdentityName() As String
        Set(ByVal value As String)
            mstrIdentityName = value
        End Set
    End Property

    Public WriteOnly Property _GrossPayFormula() As String
        Set(ByVal value As String)
            mstrGrossPayFormula = value
        End Set
    End Property

    Public WriteOnly Property _BasicPayHeadId() As Integer
        Set(ByVal value As Integer)
            mintBasicPayHeadId = value
        End Set
    End Property

    Public WriteOnly Property _fmtCurrency() As String
        Set(ByVal value As String)
            mstrfmtCurrency = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
    End Sub

#End Region

#Region "Public Function & Procedures "
    Public Sub SetDefaultValue()
        Try
            mintPeriodId = 0
            mstrPeriodName = String.Empty
            mstrPeriodCode = String.Empty
            mintWCFId = 0
            mstrWCFName = String.Empty
            mintIdentityId = 0
            mstrIdentityName = String.Empty
            mintBasicPayHeadId = 0
            mstrGrossPayFormula = ""
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)

    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, _
                                          ByVal xUserUnkid As Integer, _
                                          ByVal xYearUnkid As Integer, _
                                          ByVal xCompanyUnkid As Integer, _
                                          ByVal xPeriodStart As Date, _
                                          ByVal xPeriodEnd As Date, _
                                          ByVal xUserModeSetting As String, _
                                          ByVal xOnlyApproved As Boolean, _
                                          ByVal xExportReportPath As String, _
                                          ByVal xOpenReportAfterExport As Boolean, _
                                          ByVal pintReportType As Integer, _
                                          Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                          Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None, _
                                          Optional ByVal intBaseCurrencyUnkid As Integer = 0)


    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            'OrderByDisplay = "startdate"
            'OrderByQuery = "trdepartmentaltrainingneed_master.startdate"
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function RemoveComma(ByVal dr As DataRow, ByVal xColName As String, ByVal nCol As String) As Boolean
        dr(xColName) = dr(xColName).ToString.Replace(",", "")
        dr(nCol) = CDec(dr(xColName))
        Return True
    End Function

#End Region

#Region " Report Generation "
    Public Sub Generate_DetailReport(ByVal xDatabaseName As String, _
                                          ByVal xUserUnkid As Integer, _
                                          ByVal xYearUnkid As Integer, _
                                          ByVal xCompanyUnkid As Integer, _
                                          ByVal xPeriodStart As Date, _
                                          ByVal xPeriodEnd As Date, _
                                          ByVal xUserModeSetting As String, _
                                          ByVal xOnlyApproved As Boolean, _
                                          ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                          ByVal xExportReportPath As String, _
                                          ByVal xOpenReportAfterExport As Boolean, _
                                          ByVal intBaseCurrencyId As Integer, _
                                          Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, _
                                          Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None _
                                          )
        Dim StrQ As String = ""
        Dim dtCol As DataColumn
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim mdtTableExcel As DataTable
        Try
            objDataOperation = New clsDataOperation

            Dim dtFinalTable As DataTable
            dtFinalTable = New DataTable("WCF")

            dtCol = New DataColumn("Emp_No", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 1, "Emp No")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("wcf_number", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 2, "wcf_number")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("firstname", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 3, "firstname")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("middlename", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 4, "middlename")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("lastname", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 5, "lastname")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("gender", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 6, "gender")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("dob", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 7, "dob")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("basicpay", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 8, "basicpay")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("grosspay", System.Type.GetType("System.Decimal"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 9, "grosspay")
            dtCol.DefaultValue = 0
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("job_title", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 10, "job_title")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("employment_category", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 11, "employment_category")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            dtCol = New DataColumn("national_id", System.Type.GetType("System.String"))
            dtCol.Caption = Language.getMessage(mstrModuleName, 12, "national_id")
            dtCol.DefaultValue = ""
            dtFinalTable.Columns.Add(dtCol)

            Dim objMaster As New clsMasterData
            Dim dsGender As DataSet = objMaster.getGenderList("List", False)
            Dim dicGender As Dictionary(Of Integer, String) = (From p In dsGender.Tables("List") Select New With {.Id = CInt(p.Item("id")), .Name = p.Item("name").ToString}).ToDictionary(Function(x) x.Id, Function(x) x.Name)
            objMaster = Nothing

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            Dim objDHead As New Dictionary(Of Integer, String)
            Dim strGrossPayFormulaID As String = mstrGrossPayFormula
            Dim strGrossPaySumQuery As String = String.Empty
            If mstrGrossPayFormula.Trim <> "" Then
                Dim objHead As New clsTransactionHead
                Dim dsHead As DataSet = objHead.getComboList(xDatabaseName, "List", False, , , , True, , , , True, , , True)
                Dim strPattern As String = "([^#]*#[^#]*)#"
                Dim strReplacement As String = "$1)"
                Dim strResult As String = Regex.Replace(mstrGrossPayFormula, strPattern, strReplacement)
                Dim arr As MatchCollection = Regex.Matches(strResult, "(?<=\#)[^\)]*(?=\))", RegexOptions.IgnoreCase)
                For Each s As RegularExpressions.Match In arr
                    Dim scode As String = s.Groups(0).ToString
                    If scode.Trim <> "" Then
                        Dim dr() As DataRow = dsHead.Tables(0).Select("code = '" & scode & "' ")
                        If dr.Length > 0 Then
                            If objDHead.ContainsKey(CInt(dr(0).Item("tranheadunkid"))) = False Then
                                objDHead.Add(CInt(dr(0).Item("tranheadunkid")), scode)
                                strGrossPayFormulaID = Microsoft.VisualBasic.Strings.Replace(strGrossPayFormulaID, "#" & scode & "#", "#" & CInt(dr(0).Item("tranheadunkid")) & "#", , , CompareMethod.Text)
                            End If
                        End If
                    End If
                Next

                strGrossPaySumQuery = strGrossPayFormulaID
                For Each itm In objDHead
                    strGrossPaySumQuery = strGrossPaySumQuery.Replace("#" & itm.Key & "#", "SUM(PAYE.HeadId" & itm.Key & "Amount)")
                Next
            End If




            If mintBaseCurrId = mintPaidCurrencyId Then
                StrQ = "SELECT   hremployee_master.employeecode AS employeecode " & _
                               ", ISNULL(BasicPay, 0) AS BasicPay " & _
                               ", ISNULL(GrossPay, 0) AS GrossPay " & _
                               ", '' AS Remark " & _
                               ", hremployee_master.employeeunkid AS EmpId " & _
                               ", ISNULL(hremployee_master.firstname, '') as  FirstName " & _
                               ", ISNULL(hremployee_master.othername, '') as Othername  " & _
                               ", ISNULL(hremployee_master.surname, '') AS Surname " & _
                               ", ISNULL(IdWCF.Id_NumWCF, '') AS Id_NumWCF " & _
                               ", ISNULL(Id.Id_Num, '') AS Id_Num "

                StrQ &= ", CASE hremployee_master.gender "
                For Each pair In dicGender
                    StrQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Next
                StrQ &= " ELSE '' END AS gendername "

                StrQ &= ",     ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName " & _
                        ", ISNULL(CONVERT(VARCHAR(8),  birthdate, 112), '') AS birthdate " & _
                        ", ISNULL(hrjob_master.job_name, '') AS jobname " & _
                        ", hremployee_master.employmenttypeunkid " & _
                        ", ISNULL(cfcommon_master.name, '') AS employmenttype "


                StrQ &= "FROM    ( SELECT    ISNULL(SUM(PAYE.BasicPay), 0) AS BasicPay " & _
                                             ", ISNULL( " & strGrossPaySumQuery & ", 0) AS GrossPay " & _
                                             ", PAYE.empid AS EmpId " & _
                                 "FROM      ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                   ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS BasicPay "

                For Each itm In objDHead
                    StrQ &= ", 0 AS HeadId" & itm.Key & "Amount "
                Next

                StrQ &= "FROM   prpayrollprocess_tran " & _
                                                   "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                   "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                   "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                           "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

                'StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @BasicPayHeadId "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                For Each itm In objDHead
                    StrQ &= "UNION ALL "

                    StrQ &= "SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                   ", 0 AS BasicPay "

                    For Each itm2 In objDHead
                        If itm2.Key = itm.Key Then
                            StrQ &= ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, 0)), 0) AS HeadId" & itm.Key & "Amount "
                        Else
                            StrQ &= ", 0 AS HeadId" & itm.Key & "Amount "
                        End If
                    Next

                    StrQ &= "FROM   prpayrollprocess_tran " & _
                                                  "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                  "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                  "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                          "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

                    'StrQ &= mstrAnalysis_Join

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If

                    StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                        "AND prpayrollprocess_tran.tranheadunkid = " & itm.Key & " "

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                Next


                StrQ &= "	        ) AS PAYE " & _
                                    "WHERE     1 = 1 " & _
                                      "GROUP BY  empid "

                StrQ &= ") AS A " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
                            "LEFT JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "  " & _
                            "	LEFT JOIN " & _
                            "	( " & _
                            "		SELECT " & _
                            "			 employeeunkid AS IdEmp " & _
                            "			,ISNULL(hremployee_idinfo_tran.identity_no,'') AS Id_NumWCF " & _
                            "		FROM hremployee_idinfo_tran " & _
                            "		JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid " & _
                            "		WHERE cfcommon_master.masterunkid = '" & mintWCFId & "' " & _
                            "	) AS IdWCF ON IdWCF.IdEmp = hremployee_master.employeeunkid " & _
                            "	LEFT JOIN " & _
                            "	( " & _
                            "		SELECT " & _
                            "			 employeeunkid AS IdEmp " & _
                            "			,ISNULL(hremployee_idinfo_tran.identity_no,'') AS Id_Num " & _
                            "		FROM hremployee_idinfo_tran " & _
                            "		JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid " & _
                            "		WHERE cfcommon_master.masterunkid = '" & mintIdentityId & "' " & _
                            "	) AS Id ON Id.IdEmp = hremployee_master.employeeunkid " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         jobgroupunkid " & _
                            "        ,jobunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_categorization_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                            "LEFT JOIN hrjob_master ON J.jobunkid = hrjob_master.jobunkid "
                StrQ &= "WHERE 1 = 1 " & _
                        " ORDER BY employeecode "

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then
                StrQ = "SELECT   hremployee_master.employeecode AS employeecode " & _
                               ", ISNULL(BasicPay, 0) AS BasicPay " & _
                               ", ISNULL(GrossPay, 0) AS GrossPay " & _
                               ", '' AS Remark " & _
                               ", hremployee_master.employeeunkid AS EmpId " & _
                               ", ISNULL(hremployee_master.firstname, '') as  FirstName " & _
                               ", ISNULL(hremployee_master.othername, '') as Othername  " & _
                               ", ISNULL(hremployee_master.surname, '') AS Surname " & _
                               ", ISNULL(IdWCF.Id_NumWCF, '') AS Id_NumWCF " & _
                               ", ISNULL(Id.Id_Num, '') AS Id_Num "

                StrQ &= ", CASE hremployee_master.gender "
                For Each pair In dicGender
                    StrQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
                Next
                StrQ &= " ELSE '' END AS gendername "

                StrQ &= ",     ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName " & _
                        ", ISNULL(CONVERT(VARCHAR(8),  birthdate, 112), '') AS birthdate " & _
                        ", ISNULL(hrjob_master.job_name, '') AS jobname " & _
                        ", hremployee_master.employmenttypeunkid " & _
                        ", ISNULL(cfcommon_master.name, '') AS employmenttype "


                StrQ &= "FROM    ( SELECT    ISNULL(SUM(PAYE.BasicPay), 0) AS BasicPay " & _
                                             ", ISNULL( " & strGrossPaySumQuery & ", 0) AS GrossPay " & _
                                             ", PAYE.empid AS EmpId " & _
                                 "FROM      ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                   ", " & mdecConversionRate & " * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS BasicPay "

                For Each itm In objDHead
                    StrQ &= ", 0 AS HeadId" & itm.Key & "Amount "
                Next

                StrQ &= "FROM   prpayrollprocess_tran " & _
                                                   "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                   "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                   "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                           "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

                'StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid = @BasicPayHeadId "

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                For Each itm In objDHead
                    StrQ &= "UNION ALL "

                    StrQ &= "SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                   ", 0 AS BasicPay "

                    For Each itm2 In objDHead
                        If itm2.Key = itm.Key Then
                            StrQ &= ", " & mdecConversionRate & " * ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, 0)), 0) AS HeadId" & itm.Key & "Amount "
                        Else
                            StrQ &= ", 0 AS HeadId" & itm.Key & "Amount "
                        End If
                    Next

                    StrQ &= "FROM   prpayrollprocess_tran " & _
                                                  "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                  "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                  "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                          "AND ISNULL(cfcommon_period_tran.isactive,0) = 1 "

                    'StrQ &= mstrAnalysis_Join

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If

                    StrQ &= "                   WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                                                        "AND prtnaleave_tran.isvoid = 0 " & _
                                                        "AND prtnaleave_tran.payperiodunkid IN (" & mintPeriodId & ") " & _
                                                        "AND prpayrollprocess_tran.tranheadunkid = " & itm.Key & " "

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                Next


                StrQ &= "	        ) AS PAYE " & _
                                    "WHERE     1 = 1 " & _
                                      "GROUP BY  empid "

                StrQ &= ") AS A " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
                            "LEFT JOIN cfcommon_master ON hremployee_master.employmenttypeunkid = cfcommon_master.masterunkid AND mastertype = " & clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE & "  " & _
                            "	LEFT JOIN " & _
                            "	( " & _
                            "		SELECT " & _
                            "			 employeeunkid AS IdEmp " & _
                            "			,ISNULL(hremployee_idinfo_tran.identity_no,'') AS Id_NumWCF " & _
                            "		FROM hremployee_idinfo_tran " & _
                            "		JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid " & _
                            "		WHERE cfcommon_master.masterunkid = '" & mintWCFId & "' " & _
                            "	) AS IdWCF ON IdWCF.IdEmp = hremployee_master.employeeunkid " & _
                            "	LEFT JOIN " & _
                            "	( " & _
                            "		SELECT " & _
                            "			 employeeunkid AS IdEmp " & _
                            "			,ISNULL(hremployee_idinfo_tran.identity_no,'') AS Id_Num " & _
                            "		FROM hremployee_idinfo_tran " & _
                            "		JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid " & _
                            "		WHERE cfcommon_master.masterunkid = '" & mintIdentityId & "' " & _
                            "	) AS Id ON Id.IdEmp = hremployee_master.employeeunkid " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         jobgroupunkid " & _
                            "        ,jobunkid " & _
                            "        ,employeeunkid " & _
                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                            "    FROM hremployee_categorization_tran " & _
                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                            "LEFT JOIN hrjob_master ON J.jobunkid = hrjob_master.jobunkid "
                StrQ &= "WHERE 1 = 1 " & _
                        " ORDER BY employeecode "

            End If

            objDataOperation.AddParameter("@BasicPayHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasicPayHeadId)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim rpt_Row As DataRow = Nothing
            Dim drRow As DataRow
            Dim intCount As Integer = 1
            Dim intRowCount As Integer = dsList.Tables(0).Rows.Count
            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                rpt_Row = dtFinalTable.NewRow
                rpt_Row.Item("Emp_No") = dtRow.Item("employeecode")
                rpt_Row.Item("wcf_number") = dtRow.Item("Id_NumWCF")
                rpt_Row.Item("firstname") = dtRow.Item("firstname").ToString.ToUpper
                rpt_Row.Item("middlename") = dtRow.Item("othername").ToString.ToUpper
                rpt_Row.Item("lastname") = dtRow.Item("surname").ToString.ToUpper
                rpt_Row.Item("gender") = dtRow.Item("gendername").ToString.ToUpper
                If dtRow.Item("birthdate").ToString.Trim <> "" Then
                    rpt_Row.Item("dob") = eZeeDate.convertDate(dtRow.Item("birthdate").ToString).ToString("yyyy-MM-dd")
                Else
                    rpt_Row.Item("dob") = dtRow.Item("birthdate")
                End If
                rpt_Row.Item("basicpay") = Format(CDec(dtRow.Item("BasicPay")), mstrfmtCurrency)
                rpt_Row.Item("grosspay") = Format(CDec(dtRow.Item("GrossPay")), mstrfmtCurrency)
                rpt_Row.Item("job_title") = dtRow.Item("jobname").ToString.ToUpper
                rpt_Row.Item("employment_category") = dtRow.Item("employmenttype").ToString.ToUpper
                rpt_Row.Item("national_id") = dtRow.Item("Id_Num")

                dtFinalTable.Rows.Add(rpt_Row)

                intCount = intCount + 1
            Next

            dtFinalTable.AcceptChanges()

            mdtTableExcel = dtFinalTable


            Using package As New OfficeOpenXml.ExcelPackage()
                Dim worksheet As OfficeOpenXml.ExcelWorksheet = package.Workbook.Worksheets.Add("Data")
                Dim xTable As DataTable = mdtTableExcel.Copy
                For Each iCol As DataColumn In xTable.Columns
                    If iCol.Ordinal <> xTable.Columns.Count - 1 Then
                        xTable.Columns(iCol.Ordinal).ColumnName = iCol.Caption.Trim()
                    End If
                Next
                'Dim xCol As New DataColumn
                'With xCol
                '    .DataType = GetType(System.Decimal)
                '    .ColumnName = xTable.Columns(xTable.Columns.Count - 1).Caption
                '    .DefaultValue = 0
                'End With
                'xTable.Columns.Add(xCol)
                'xTable.AsEnumerable().ToList().ForEach(Function(x) RemoveComma(x, xTable.Columns(xTable.Columns.Count - 2).ColumnName, xTable.Columns(xTable.Columns.Count - 1).ColumnName))
                'xTable.Columns.RemoveAt(xTable.Columns.Count - 2)
                worksheet.Cells("A1").LoadFromDataTable(xTable, True)
                worksheet.Cells("A1:XFD").AutoFilter = False
                worksheet.Cells(2, 1, xTable.Rows.Count + 1, xTable.Columns.Count - 2).Style.Numberformat.Format = "@"
                worksheet.Cells(2, xTable.Columns.Count - 1, xTable.Rows.Count + 1, xTable.Columns.Count - 1).Style.Numberformat.Format = "0"
                worksheet.Cells(2, xTable.Columns.Count, xTable.Rows.Count + 1, xTable.Columns.Count).Style.Numberformat.Format = "0"

                worksheet.Cells.AutoFitColumns(0)

                If Me._ReportName.Contains("/") = True Then
                    Me._ReportName = Me._ReportName.Replace("/", "_")
                End If

                If Me._ReportName.Contains("\") Then
                    Me._ReportName = Me._ReportName.Replace("\", "_")
                End If

                If Me._ReportName.Contains(":") Then
                    Me._ReportName = Me._ReportName.Replace(":", "_")
                End If
                Dim strExportFileName As String = ""
                strExportFileName = Me._ReportName.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss")

                xExportReportPath = xExportReportPath & "\" & strExportFileName & ".xlsx"

                Dim fi As IO.FileInfo = New IO.FileInfo(xExportReportPath)
                package.SaveAs(fi)


            End Using

            If IO.File.Exists(xExportReportPath) Then
                If xOpenReportAfterExport Then
                    Process.Start(xExportReportPath)
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

End Class
