'************************************************************************************************************************************
'Class Name : frmNSSFForm5Report.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmNSSFForm5Report

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmNSSFForm5Report"
    Private objNNSF5 As clsNSSFForm5Report
'Pinkal (24-Jul-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    'Pinkal (24-Jul-2012) -- End
    Private eReport As enArutiReport

    Private mintFirstOpenPeriod As Integer = 0 'Sohail (20 Mar 2013)


    'Pinkal (02-May-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mintPaidCurrencyId As Integer = 0
    'Pinkal (02-May-2013) -- End
    'Sohail (23 Dec 2019) -- Start
    'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
    Private mstrAdvanceFilter As String = ""
    'Sohail (23 Dec 2019) -- End

#End Region

#Region " Contructor "

    Public Sub New(ByVal menReport As enArutiReport, ByVal intLangId As Integer, ByVal intCompanyId As Integer) 'Anjan (25 Jul 2012)-Start

        eReport = menReport

        'Anjan (25 Jul 2012)-Start
        'ENHANCEMENT : TRA COMMENTS on Rutta's Request
        'objNNSF5 = New clsNSSFForm5Report(User._Object._Languageunkid,Company._Object._Companyunkid)
        objNNSF5 = New clsNSSFForm5Report(menReport, intLangId, intCompanyId)
        'Anjan (25 jul 2012)-End 

        'Sohail (17 Aug 2013) -- Start
        'TRA - ENHANCEMENT
        If Company._Object._Countryunkid = 112 Then 'Kenya
            _Show_ExcelExtra_Menu = True
            'Pinkal (29-May-2018) -- Start
            'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
            _Show_ExcelDataOnly_Menu = True
            'Pinkal (29-May-2018) -- End
        Else
            _Show_ExcelExtra_Menu = False
        End If
        'Sohail (17 Aug 2013) -- End


        objNNSF5.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

    'Sohail (17 Oct 2020) -- Start
    'FFK Enhancement # OLD-67 : - Give option to Save User-Selected Settings when Generating Statutory Reports NSSF and NHIF Reports for Kenyan Companies - FFK.
#Region " Private Enum "
    Private Enum enHeadTypeId
        Membership = 1
        Currency = 2
        Include_Membership = 3
        Identity = 4
        KRA_PIN = 5
        VOL_Head = 6
        Ignore_Zero = 7
        BSOtherEarning = 8
        OtherEarning = 9
    End Enum
#End Region
    'Sohail (17 Oct 2020) -- End

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMember As New clsmembership_master
        Dim dsCombos As New DataSet
        Dim objperiod As New clscommom_period_Tran
        Dim objEmpContribution As New clsTransactionHead
        Dim objMaster As New clsMasterData 'Sohail (20 Mar 2013)
        Dim objCMaster As New clsCommon_Master 'Hemant (16 Dec 2019)
        Try
            'Sohail (17 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objMember.getListForCombo("Membership", True)
            dsCombos = objMember.getListForCombo("Membership", True, , 1)
            'Sohail (17 Aug 2013) -- End
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With



            'Sohail (17 Aug 2013) -- Start
            'TRA - ENHANCEMENT

            'Pinkal (29-May-2018) -- Start
            'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.

            RemoveHandler cboIncludeMembership.SelectedValueChanged, AddressOf cboIncludeMembership_SelectedValueChanged
            dsCombos = objMember.getListForCombo("Membership", True, , 2)
            With cboIncludeMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Membership")
                .SelectedValue = 0
            End With
            AddHandler cboIncludeMembership.SelectedValueChanged, AddressOf cboIncludeMembership_SelectedValueChanged


            If Company._Object._Countryunkid = 112 Then   'Kenya
                RemoveHandler cboKRAPIN.SelectedValueChanged, AddressOf cboIncludeMembership_SelectedValueChanged
                With cboKRAPIN
                    .ValueMember = "membershipunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombos.Tables("Membership").Copy()
                    .SelectedValue = 0
                End With
                AddHandler cboKRAPIN.SelectedValueChanged, AddressOf cboIncludeMembership_SelectedValueChanged
            Else
                LblKRAPin.Visible = False
                cboKRAPIN.Visible = False
            End If

            'Pinkal (29-May-2018) -- End


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objEmpContribution.getComboList("Head", True, , , enTypeOf.Informational, , )
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "Head", True, , , enTypeOf.Informational, , )
            'Sohail (21 Aug 2015) -- End
            Dim strSystemGeneratedHead As String = " " & enCalcType.NET_PAY & ", " & enCalcType.TOTAL_EARNING & ", " & enCalcType.TOTAL_DEDUCTION & ", " & enCalcType.EMPLOYER_CONTRIBUTION_PAYABLE & ", " & enCalcType.TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_TAXABLE_EARNING_TOTAL & ", " & enCalcType.NON_CASH_BENEFIT_TOTAL & ""
            Dim dt As DataTable = New DataView(dsCombos.Tables("Head"), "calctype_id NOT IN (" & strSystemGeneratedHead & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboVOLHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dt
                .SelectedValue = 0
            End With
            'Sohail (17 Aug 2013) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid) 'Sohail (20 Mar 2013)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1) 'Sohail (20 Mar 2013)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            'Sohail (12 Sep 2017) -- Start
            'Enhancement - 69.1 - On NSSF Statutory Report  should be printed in multiple periods by allowing periods in a list with check box.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            ''Sohail (21 Aug 2015) -- End
            'With cboPeriod
            '    .ValueMember = "periodunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables(0)
            '    'Sohail (20 Mar 2013) -- Start
            '    'TRA - ENHANCEMENT
            '    '.SelectedValue = 0
            '    .SelectedValue = mintFirstOpenPeriod
            '    'Sohail (20 Mar 2013) -- End
            'End With
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", False)
            lvPeriodList.Items.Clear()
            If dsCombos.Tables(0).Rows.Count > 0 Then
                Dim lvItem As ListViewItem
                For Each dtRow As DataRow In dsCombos.Tables("List").Rows
                    lvItem = New ListViewItem

                    lvItem.Text = ""
                    lvItem.SubItems.Add(dtRow.Item("name").ToString)
                    lvItem.Tag = dtRow.Item("periodunkid")

                    'Sohail (26 Aug 2019) -- Start
                    'SportPesa issue # 0004104: 76.1 : Error when extracting NSSF report
                    lvItem.SubItems.Add(dtRow.Item("start_Date").ToString)
                    lvItem.SubItems(objcolhPeriodStart.Index).Tag = dtRow.Item("end_Date").ToString
                    'Sohail (26 Aug 2019) -- End

                    lvPeriodList.Items.Add(lvItem)

                    lvItem = Nothing
                Next
                If lvPeriodList.Items.Count > 8 Then
                    colhPeriodName.Width = colhPeriodName.Width - 20
                Else
                    colhPeriodName.Width = colhPeriodName.Width
                End If
            End If
            'Sohail (12 Sep 2017) -- End

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objEmpContribution.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)
            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Removing other earnings and including all earnings on all statutory reports as per Rutta's request.
            'dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "typeof_id = " & enTypeOf.Other_Earnings & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            'Anjan [26 September 2016] -- End

            'Sohail (16 Mar 2016) -- End
            'Sohail (21 Aug 2015) -- End
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("OtherEarning")
                .SelectedValue = 0
            End With
            'Sohail (28 Aug 2013) -- End

            'Sohail (17 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmpContribution.getComboList("EmpContribution", True, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Employee_Statutory_Contributions)
            'With cboEmpHeadType
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombos.Tables("EmpContribution")
            '    .SelectedValue = 0
            'End With

            'dsCombos = objEmpContribution.getComboList("EmployerContribution", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            'With cboCoHeadType
            '    .ValueMember = "tranheadunkid"
            '    .DisplayMember = "Name"
            '    .DataSource = dsCombos.Tables("EmployerContribution")
            '    .SelectedValue = 0
            'End With
            'Sohail (17 Aug 2013) -- End

            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes

            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency", True)
            cboCurrency.ValueMember = "countryunkid"
            cboCurrency.DisplayMember = "currency_sign"
            cboCurrency.DataSource = dsCombos.Tables("Currency")

            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If

            'Pinkal (02-May-2013) -- End

            'Hemant (16 Dec 2019) -- Start
            'Ref # 4327 : NSSF FORM NEW TEMPLATE
            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")
            With cboIdentity
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            'Hemant (16 Dec 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMember = Nothing
            objperiod = Nothing
            objEmpContribution = Nothing
            objMaster = Nothing 'Sohail (20 Mar 2013)
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            'Sohail (17 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'cboCoHeadType.SelectedValue = 0
            'cboEmpHeadType.SelectedValue = 0
            cboIncludeMembership.SelectedValue = 0
            cboVOLHead.SelectedValue = 0
            'Sohail (17 Aug 2013) -- End
            'Sohail (20 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'cboPeriod.SelectedValue = 0
            'cboPeriod.SelectedValue = mintFirstOpenPeriod 'Sohail (12 Sep 2017)
            'Sohail (20 Mar 2013) -- End
            cboOtherEarning.SelectedValue = 0 'Sohail (28 Aug 2013)
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport 'Sohail (07 Sep 2013)

            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            'Pinkal (24-Jul-2012) -- End
            chkShowBasicSal.Checked = True 'Sohail (22 Aug 2012)



            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes
            cboCurrency.SelectedValue = 0
            mintPaidCurrencyId = 0
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            'Pinkal (02-May-2013) -- End


            'Pinkal (29-May-2018) -- Start
            'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
            cboKRAPIN.SelectedValue = 0
            'Pinkal (29-May-2018) -- End

            'Hemant (16 Dec 2019) -- Start
            'Ref # 4327 : NSSF FORM NEW TEMPLATE
            cboIdentity.SelectedValue = 0
            'Hemant (16 Dec 2019) -- End
            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            mstrAdvanceFilter = ""
            'Sohail (23 Dec 2019) -- End

            Call GetValue() 'Sohail (17 Oct 2020)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    'Sohail (17 Oct 2020) -- Start
    'FFK Enhancement # OLD-67 : - Give option to Save User-Selected Settings when Generating Statutory Reports NSSF and NHIF Reports for Kenyan Companies - FFK.
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.NSSF_FORM_5)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Currency
                            cboCurrency.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Include_Membership
                            cboIncludeMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Identity
                            cboIdentity.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.KRA_PIN
                            cboKRAPIN.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.VOL_Head
                            cboVOLHead.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Ignore_Zero
                            chkIgnoreZero.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.BSOtherEarning
                            gbBasicSalaryOtherEarning.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.OtherEarning
                            cboOtherEarning.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Sohail (17 Oct 2020) -- End

    Private Function SetFilter() As Boolean
        Try
            Call objNNSF5.SetDefaultValue()

            If CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            End If

            'Sohail (12 Sep 2017) -- Start
            'Enhancement - 69.1 - On NSSF Statutory Report  should be printed in multiple periods by allowing periods in a list with check box.
            'If CInt(cboPeriod.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
            '    cboPeriod.Focus()
            '    Return False
            'End If
            If lvPeriodList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select atleast one period to view report."), enMsgBoxStyle.Information)
                lvPeriodList.Focus()
                Return False
            End If
            'Sohail (12 Sep 2017) -- End

            'Sohail (17 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'If CInt(cboEmpHeadType.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Employee Contribution is mandatory information. Please select Employee Contribution to continue."), enMsgBoxStyle.Information)
            '    cboEmpHeadType.Focus()
            '    Return False
            'End If

            'If CInt(cboCoHeadType.SelectedValue) <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employer Contribution is mandatory information. Please select Employer Contribution to continue."), enMsgBoxStyle.Information)
            '    cboCoHeadType.Focus()
            '    Return False
            'End If
            'Sohail (17 Aug 2013) -- End

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If gbBasicSalaryOtherEarning.Visible = True AndAlso gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If
            'Sohail (17 Dec 2014) - [gbBasicSalaryOtherEarning.Visible = True]
            'Sohail (28 Aug 2013) -- End

            If CInt(cboMembership.SelectedValue) > 0 Then
                objNNSF5._MembershipId = cboMembership.SelectedValue
                objNNSF5._MembershipName = cboMembership.Text
            End If

            'Sohail (12 Sep 2017) -- Start
            'Enhancement - 69.1 - On NSSF Statutory Report  should be printed in multiple periods by allowing periods in a list with check box.
            'If CInt(cboPeriod.SelectedValue) > 0 Then
            '    objNNSF5._PeriodId = cboPeriod.SelectedValue
            '    objNNSF5._PeriodName = cboPeriod.Text
            'End If
            Dim StrPeriodIds As String = String.Empty
            Dim StrPeriodName As String = String.Empty


            StrPeriodIds = String.Join(",", (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToArray)
            StrPeriodName = String.Join(",", (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(colhPeriodName.Index).Text.ToString)).ToArray)

            If StrPeriodName.Contains("(") = False Then
                StrPeriodName = StrPeriodName.Insert(0, "(")
            End If
            If StrPeriodName.Contains(")") = False Then
                StrPeriodName = StrPeriodName.Insert(StrPeriodName.Length, ")")
            End If
            objNNSF5._Period_Ids = StrPeriodIds
            objNNSF5._Period_Names = StrPeriodName
            'Sohail (12 Sep 2017) -- End

            'Sohail (17 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'If CInt(cboEmpHeadType.SelectedValue) > 0 Then
            '    objNNSF5._EmpHeadTypeId = cboEmpHeadType.SelectedValue
            'End If

            'If CInt(cboCoHeadType.SelectedValue) > 0 Then
            '    objNNSF5._EmprHeadTypeId = cboCoHeadType.SelectedValue
            'End If
            objNNSF5._NonPayrollMembershipId = CInt(cboIncludeMembership.SelectedValue)
            If CInt(cboIncludeMembership.SelectedValue) > 0 Then
                objNNSF5._NonPayrollMembershipName = cboIncludeMembership.Text
            Else
                objNNSF5._NonPayrollMembershipName = Language.getMessage(mstrModuleName, 5, "ID No.")
            End If

            objNNSF5._VOLHeadId = CInt(cboVOLHead.SelectedValue)
            'Sohail (17 Aug 2013) -- End

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If gbBasicSalaryOtherEarning.Checked = True Then
                objNNSF5._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objNNSF5._OtherEarningTranId = 0
            End If
            'Sohail (28 Aug 2013) -- End

            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes

            objNNSF5._ViewByIds = mstrStringIds
            objNNSF5._ViewIndex = mintViewIdx
            objNNSF5._ViewByName = mstrStringName
            objNNSF5._Analysis_Fields = mstrAnalysis_Fields
            objNNSF5._Analysis_Join = mstrAnalysis_Join
            objNNSF5._Report_GroupName = mstrReport_GroupName

            'Pinkal (24-Jul-2012) -- End

            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            objNNSF5._AdvanceFilter = mstrAdvanceFilter
            'Sohail (23 Dec 2019) -- End

            'S.SANDEEP [ 28 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objNNSF5._ShowBasicSalary = chkShowBasicSal.Checked 'Sohail (22 Aug 2012)

            'Nilay (13-Oct-2016) -- Start
            'Enhancement : Membership wise option - Show Basic Salary on Statutory Report and ignore that option on configuration
            'objNNSF5._ShowBasicSalary = ConfigParameter._Object._ShowBasicSalaryOnStatutoryReport
            Dim objMember As New clsmembership_master
            objMember._Membershipunkid = CInt(cboMembership.SelectedValue)
            objNNSF5._ShowBasicSalary = objMember._IsShowBasicSalary
            'Nilay (13-Oct-2016) -- End

            'S.SANDEEP [ 28 FEB 2013 ] -- END

            'Pinkal (02-May-2013) -- Start
            'Enhancement : TRA Changes

            If (mintBaseCurrId = mintPaidCurrencyId) Or mintPaidCurrencyId <= 0 Then
                mintPaidCurrencyId = mintBaseCurrId
            End If

            objNNSF5._CountryId = CInt(cboCurrency.SelectedValue)
            objNNSF5._BaseCurrencyId = mintBaseCurrId
            objNNSF5._PaidCurrencyId = mintPaidCurrencyId

            If mdecBaseExRate > 0 AndAlso mdecPaidExRate Then
                objNNSF5._ConversionRate = mdecPaidExRate / mdecBaseExRate
            End If

            'Sohail (12 Sep 2017) -- Start
            'Enhancement - 69.1 - On NSSF Statutory Report  should be printed in multiple periods by allowing periods in a list with check box.
            'objNNSF5._ExchangeRate = LblCurrencyRate.Text
            objNNSF5._ExchangeRate = objlblExRate.Text
            'Sohail (12 Sep 2017) -- End

            'Pinkal (02-May-2013) -- End

            'SHANI (02 JUL 2015) -- Start
            'AKFK Enhancement - Provide Ignore Zero option on NSSF Report and NHIF Report
            objNNSF5._IgnoreZero = chkIgnoreZero.Checked
            'SHANI (02 JUL 2015) -- End 

            'Sohail (28 Jan 2016) -- Start
            'Enhancement - Changes in NSSF Kenya Report (Email From : Anatory Rutta, Subject: ARUTI NSSF REPORT, Sent: Wednesday, January 27, 2016 11:08 AM.
            objNNSF5._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            'Sohail (28 Jan 2016) -- End


            'Pinkal (29-May-2018) -- Start
            'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.
            objNNSF5._KRAPINId = CInt(cboKRAPIN.SelectedValue)
            objNNSF5._KRAPIN = cboKRAPIN.Text.ToString()
            'Pinkal (29-May-2018) -- End

            'Hemant (16 Dec 2019) -- Start
            'Ref # 4327 : NSSF FORM NEW TEMPLATE
            objNNSF5._IdentityId = cboIdentity.SelectedValue
            objNNSF5._IdentityName = cboIdentity.Text
            'Hemant (16 Dec 2019) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter()", mstrModuleName)
        End Try
    End Function

    'Sohail (12 Sep 2017) -- Start
    'Enhancement - 69.1 - On NSSF Statutory Report  should be printed in multiple periods by allowing periods in a list with check box.
    Private Sub DoOperation(ByVal blnOpreration As Boolean)
        Try
            For Each lvItem As ListViewItem In lvPeriodList.Items
                lvItem.Checked = blnOpreration
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoOperation", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Sep 2017) -- End

#End Region

#Region " Forms "

    Private Sub frmPPFContributionReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objNNSF5 = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPPFContributionReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPPFContributionReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Me._Title = objNNSF5._ReportName
            Me._Message = objNNSF5._ReportDesc

            'S.SANDEEP [ 03 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call OtherSettings()
            'S.SANDEEP [ 03 SEP 2012 ] -- END

            'S.SANDEEP |18-JUN-2021| -- START
            'ISSUE/ENHANCEMENT : Kenya Reports Changes (Statutory)
            lnkExportDataxlsx.Visible = False
            'S.SANDEEP |18-JUN-2021| -- END

            'Sohail (17 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If Company._Object._Countryunkid = 112 Then 'Kenya
                lblIncludeMembership.Visible = True
                cboIncludeMembership.Visible = True
                lblVOLHead.Visible = True
                cboVOLHead.Visible = True

                gbBasicSalaryOtherEarning.Visible = False 'Sohail (28 Aug 2013)
                'Hemant (16 Dec 2019) -- Start
                'Ref # 4327 : NSSF FORM NEW TEMPLATE
                lblIdentity.Visible = False
                cboIdentity.Visible = False
                'Hemant (16 Dec 2019) -- End

                'S.SANDEEP |18-JUN-2021| -- START
                'ISSUE/ENHANCEMENT : Kenya Reports Changes (Statutory)
                lnkExportDataxlsx.Visible = True
                'S.SANDEEP |18-JUN-2021| -- END

            Else
                'Hemant (16 Dec 2019) -- Start
                'Ref # 4327 : NSSF FORM NEW TEMPLATE
                'If Company._Object._Countryunkid = 219 Then 'Uganda      'Hemant (09 Dec 2019)
                '    lblIncludeMembership.Visible = False
                '    cboIncludeMembership.Visible = False
                'End If 'Hemant (09 Dec 2019)
                'Hemant (22 Dec 2022) -- Start
                'ENHANCEMENT(Tanzania) : A1X-512 - New Xlsx Export file for NSSF
                If Company._Object._Countryunkid = 208 Then 'Tanzania
                    lnkExportDataxlsx.Visible = True
                End If
                'Hemant (22 Dec 2022) -- End
                lblIncludeMembership.Visible = False
                cboIncludeMembership.Visible = False
                lblIdentity.Visible = False
                cboIdentity.Visible = False
                If Company._Object._Countryunkid <> 219 Then
                    lblIdentity.Visible = True
                    cboIdentity.Visible = True
                    lblIdentity.Location = lblIncludeMembership.Location
                    cboIdentity.Location = cboIncludeMembership.Location
                End If
                'Hemant (16 Dec 2019) -- End

                lblVOLHead.Visible = False
                cboVOLHead.Visible = False

                gbBasicSalaryOtherEarning.Visible = True 'Sohail (28 Aug 2013)
            End If
            'Sohail (17 Aug 2013) -- End


            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPPFContributionReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try

            If Not SetFilter() Then Exit Sub

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objNNSF5._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(colhPeriodName.Index).Text.ToString)).Max)
            'Sohail (03 Aug 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objNNSF5.generateReport(0, e.Type, enExportAction.None)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objNNSF5.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                           User._Object._Userunkid, _
            '                           FinancialYear._Object._YearUnkid, _
            '                           Company._Object._Companyunkid, _
            '                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                           ConfigParameter._Object._UserAccessModeSetting, _
            '                           True, ConfigParameter._Object._ExportReportPath, _
            '                           ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            objNNSF5.generateReportNew(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       eZeeDate.convertDate((From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Text.ToString)).Min), _
                                       eZeeDate.convertDate((From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max), _
                                       ConfigParameter._Object._UserAccessModeSetting, _
                                       True, ConfigParameter._Object._ExportReportPath, _
                                       ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (07 Nov 2019) - [lv.SubItems(objcolhPeriodStart.Index).Text.ToString = lv.SubItems(objcolhPeriodStart.Index).Tag.ToString]
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try

            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objNNSF5.generateReport(0, enPrintAction.None, e.Type)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objNNSF5.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                           User._Object._Userunkid, _
            '                           FinancialYear._Object._YearUnkid, _
            '                           Company._Object._Companyunkid, _
            '                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                           ConfigParameter._Object._UserAccessModeSetting, _
            '                           True, ConfigParameter._Object._ExportReportPath, _
            '                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            objNNSF5.generateReportNew(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       eZeeDate.convertDate((From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Text.ToString)).Min), _
                                       eZeeDate.convertDate((From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max), _
                                       ConfigParameter._Object._UserAccessModeSetting, _
                                       True, ConfigParameter._Object._ExportReportPath, _
                                       ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (07 Nov 2019) - [lv.SubItems(objcolhPeriodStart.Index).Text.ToString = lv.SubItems(objcolhPeriodStart.Index).Tag.ToString]
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsNSSFForm5Report.SetMessages()
            Select Case eReport
                Case enArutiReport.NSSF_FORM_5
                    objfrm._Other_ModuleNames = "clsNSSFForm5Report"
                Case enArutiReport.ZSSF_FORM
                    objfrm._Other_ModuleNames = "clsZSSFContribution"
            End Select

            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 03 SEP 2012 ] -- END

    'Sohail (17 Oct 2020) -- Start
    'FFK Enhancement # OLD-67 : - Give option to Save User-Selected Settings when Generating Statutory Reports NSSF and NHIF Reports for Kenyan Companies - FFK.
    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.NSSF_FORM_5
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NSSF_FORM_5, 0, 0, intHeadType)

                    Case enHeadTypeId.Currency
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboCurrency.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NSSF_FORM_5, 0, 0, intHeadType)

                    Case enHeadTypeId.Include_Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboIncludeMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NSSF_FORM_5, 0, 0, intHeadType)

                    Case enHeadTypeId.Identity
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboIdentity.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NSSF_FORM_5, 0, 0, intHeadType)

                    Case enHeadTypeId.KRA_PIN
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboKRAPIN.SelectedValue).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NSSF_FORM_5, 0, 0, intHeadType)

                    Case enHeadTypeId.VOL_Head
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboVOLHead.SelectedValue).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NSSF_FORM_5, 0, 0, intHeadType)

                    Case enHeadTypeId.Ignore_Zero
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = chkIgnoreZero.Checked.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NSSF_FORM_5, 0, 0, intHeadType)

                    Case enHeadTypeId.BSOtherEarning
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = gbBasicSalaryOtherEarning.Checked.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NSSF_FORM_5, 0, 0, intHeadType)

                    Case enHeadTypeId.OtherEarning
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = CInt(cboOtherEarning.SelectedValue).ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NSSF_FORM_5, 0, 0, intHeadType)


                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Sohail (17 Oct 2020) -- End

#End Region


    'Pinkal (24-Jul-2012) -- Start
    'Enhancement : TRA Changes

#Region "LinkLabel Event"

    Private Sub lnkAdvanceFilter_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAdvanceFilter.LinkClicked
        'Sohail (23 Dec 2019) -- Start
        'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
        'Dim frm As New frmViewAnalysis
        Dim frm As New frmAdvanceSearch
        'Sohail (23 Dec 2019) -- End
        Try
            'Sohail (23 Dec 2019) -- Start
            'AKF Enhancement # 0004231: Advanced Filter is required for NSSF Form and Tax Report.
            'frm.displayDialog()
            'mstrStringIds = frm._ReportBy_Ids
            'mstrStringName = frm._ReportBy_Name
            'mintViewIdx = frm._ViewIndex
            'mstrAnalysis_Fields = frm._Analysis_Fields
            'mstrAnalysis_Join = frm._Analysis_Join
            'mstrReport_GroupName = frm._Report_GroupName
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
            'Sohail (23 Dec 2019) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvanceFilter_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'S.SANDEEP |18-JUN-2021| -- START
    'ISSUE/ENHANCEMENT : Kenya Reports Changes (Statutory)
    Private Sub lnkExportDataxlsx_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkExportDataxlsx.LinkClicked
        Try
            If Not SetFilter() Then Exit Sub

            objNNSF5._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(colhPeriodName.Index).Text.ToString)).Max)

            Dim strExportPath As String = ""
            If System.IO.Directory.Exists(ConfigParameter._Object._ExportReportPath) = False Then
                Dim dig As New System.Windows.Forms.FolderBrowserDialog
                dig.Description = "Select Folder Where to export report."

                If dig.ShowDialog = System.Windows.Forms.DialogResult.OK Then
                    strExportPath = dig.SelectedPath
                Else
                    Exit Sub
                End If
            Else
                strExportPath = ConfigParameter._Object._ExportReportPath
            End If


            objNNSF5.generateReportNew(FinancialYear._Object._DatabaseName, _
                                       User._Object._Userunkid, _
                                       FinancialYear._Object._YearUnkid, _
                                       Company._Object._Companyunkid, _
                                       eZeeDate.convertDate((From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Text.ToString)).Min), _
                                       eZeeDate.convertDate((From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objcolhPeriodStart.Index).Tag.ToString)).Max), _
                                       ConfigParameter._Object._UserAccessModeSetting, _
                                       True, strExportPath, _
                                       ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, enExportAction.ExcelXLSX, ConfigParameter._Object._Base_CurrencyId)

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Report successfully exported."), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkExportDataxlsx_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |18-JUN-2021| -- END

#End Region

    'Pinkal (24-Jul-2012) -- End


    'Pinkal (02-May-2013) -- Start
    'Enhancement : TRA Changes

#Region "ComboBox Event"

    'Sohail (12 Sep 2017) -- Start
    'Enhancement - 69.1 - On NSSF Statutory Report  should be printed in multiple periods by allowing periods in a list with check box.
    'Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboCurrency.SelectedIndexChanged
    '    Try
    '        If CInt(cboPeriod.SelectedValue) > 0 AndAlso CInt(cboCurrency.SelectedValue) > 0 Then
    '            Dim objExRate As New clsExchangeRate
    '            Dim objPeriod As New clscommom_period_Tran
    '            'Sohail (21 Aug 2015) -- Start
    '            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
    '            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
    '            'Sohail (21 Aug 2015) -- End
    '            Dim dsList As DataSet = Nothing
    '            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
    '            Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate")).ToTable

    '            If dtTable.Rows.Count > 0 Then
    '                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
    '                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
    '                mintPaidCurrencyId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
    '                LblCurrencyRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
    '            Else
    '                mdecBaseExRate = 0
    '                mdecPaidExRate = 0
    '                mintPaidCurrencyId = 0
    '                LblCurrencyRate.Text = ""
    '            End If
    '        Else
    '            mdecBaseExRate = 0
    '            mdecPaidExRate = 0
    '            mintPaidCurrencyId = 0
    '            LblCurrencyRate.Text = ""
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
    '    End Try
    'End Sub
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged, dtpDate.ValueChanged
        Try
            objlblExRate.Text = ""
                    mdecBaseExRate = 0
                    mdecPaidExRate = 0
            'mdec_ExRate = 1
            If CInt(cboCurrency.SelectedValue) > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim ds As DataSet = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpDate.Value.Date, True)
                If ds.Tables("ExRate").Rows.Count > 0 Then
                    mdecBaseExRate = CDec(ds.Tables("ExRate").Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(ds.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                    'mdec_ExRate = CDec(ds.Tables("ExRate").Rows(0).Item("exchange_rate"))
                    mintPaidCurrencyId = CInt(ds.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
                    objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & ds.Tables("ExRate").Rows(0).Item("currency_sign").ToString & " "
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Sep 2017) -- End


    'Pinkal (29-May-2018) -- Start
    'Enhancement - Ref # 209 NSSF report template has changed due to a change in the NSSF system.

    Private Sub cboIncludeMembership_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboIncludeMembership.SelectedValueChanged, cboKRAPIN.SelectedValueChanged
        Dim dtTable As DataTable = Nothing
        Dim mintMembershipOldValue As Integer = 0
        Dim mintKRAOldValue As Integer = 0

        Try
            Dim objMember As New clsmembership_master
            dtTable = objMember.getListForCombo("Membership", True, , 2).Tables(0)
            objMember = Nothing

            If CType(sender, ComboBox).Name.ToUpper() = cboIncludeMembership.Name.ToUpper() Then
                mintKRAOldValue = CInt(cboKRAPIN.SelectedValue)

                If CInt(cboIncludeMembership.SelectedValue) > 0 Then
                    dtTable = New DataView(dtTable, "membershipunkid <>" & CInt(cboIncludeMembership.SelectedValue), "", DataViewRowState.CurrentRows).ToTable()
                End If

                If cboKRAPIN.Visible Then

                    RemoveHandler cboKRAPIN.SelectedValueChanged, AddressOf cboIncludeMembership_SelectedValueChanged
                    With cboKRAPIN
                        .ValueMember = "membershipunkid"
                        .DisplayMember = "name"
                        .DataSource = dtTable
                        .SelectedValue = mintKRAOldValue
                    End With
                    AddHandler cboKRAPIN.SelectedValueChanged, AddressOf cboIncludeMembership_SelectedValueChanged

                End If

            ElseIf CType(sender, ComboBox).Name.ToUpper() = cboKRAPIN.Name.ToUpper() AndAlso cboKRAPIN.Visible Then

                mintMembershipOldValue = CInt(cboIncludeMembership.SelectedValue)

                If CInt(cboKRAPIN.SelectedValue) > 0 Then
                    dtTable = New DataView(dtTable, "membershipunkid <>" & CInt(cboKRAPIN.SelectedValue), "", DataViewRowState.CurrentRows).ToTable()
                End If

                RemoveHandler cboIncludeMembership.SelectedValueChanged, AddressOf cboIncludeMembership_SelectedValueChanged
                With cboIncludeMembership
                    .ValueMember = "membershipunkid"
                    .DisplayMember = "name"
                    .DataSource = dtTable
                    .SelectedValue = mintMembershipOldValue
                End With
                AddHandler cboIncludeMembership.SelectedValueChanged, AddressOf cboIncludeMembership_SelectedValueChanged
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboIncludeMembership_SelectedValueChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (29-May-2018) -- End

#End Region

    'Pinkal (02-May-2013) -- End

    'Sohail (12 Sep 2017) -- Start
    'Enhancement - 69.1 - On NSSF Statutory Report  should be printed in multiple periods by allowing periods in a list with check box.
#Region " Listview Events "

    Private Sub lvPeriodList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriodList.ItemChecked
        Try
            RemoveHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged
            If lvPeriodList.CheckedItems.Count <= 0 Then
                objCheckAll.CheckState = CheckState.Unchecked
            ElseIf lvPeriodList.CheckedItems.Count < lvPeriodList.Items.Count Then
                objCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvPeriodList.CheckedItems.Count = lvPeriodList.Items.Count Then
                objCheckAll.CheckState = CheckState.Checked
            End If
            AddHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriodList_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region
    'Sohail (12 Sep 2017) -- End

    'Sohail (28 Aug 2013) -- Start
    'TRA - ENHANCEMENT
#Region " Other Controls Events "

    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (12 Sep 2017) -- Start
    'Enhancement - 69.1 - On NSSF Statutory Report  should be printed in multiple periods by allowing periods in a list with check box.
    Private Sub objCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objCheckAll.CheckedChanged
        Try
            Call DoOperation(CBool(objCheckAll.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (12 Sep 2017) -- End

#End Region
    'Sohail (28 Aug 2013) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lblEmpContriHeadType.Text = Language._Object.getCaption(Me.lblEmpContriHeadType.Name, Me.lblEmpContriHeadType.Text)
			Me.lblCompanyHeadType.Text = Language._Object.getCaption(Me.lblCompanyHeadType.Name, Me.lblCompanyHeadType.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
            Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)
			Me.chkShowBasicSal.Text = Language._Object.getCaption(Me.chkShowBasicSal.Name, Me.chkShowBasicSal.Text)
			Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.Name, Me.LblCurrency.Text)
			Me.lblIncludeMembership.Text = Language._Object.getCaption(Me.lblIncludeMembership.Name, Me.lblIncludeMembership.Text)
			Me.lblVOLHead.Text = Language._Object.getCaption(Me.lblVOLHead.Name, Me.lblVOLHead.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
			Me.chkIgnoreZero.Text = Language._Object.getCaption(Me.chkIgnoreZero.Name, Me.chkIgnoreZero.Text)
			Me.lblPeriodList.Text = Language._Object.getCaption(Me.lblPeriodList.Name, Me.lblPeriodList.Text)
			Me.colhPeriodName.Text = Language._Object.getCaption(CStr(Me.colhPeriodName.Tag), Me.colhPeriodName.Text)
			Me.lblAsOnDate.Text = Language._Object.getCaption(Me.lblAsOnDate.Name, Me.lblAsOnDate.Text)
			Me.LblKRAPin.Text = Language._Object.getCaption(Me.LblKRAPin.Name, Me.LblKRAPin.Text)
            Me.lblIdentity.Text = Language._Object.getCaption(Me.lblIdentity.Name, Me.lblIdentity.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
            Me.lnkExportDataxlsx.Text = Language._Object.getCaption(Me.lnkExportDataxlsx.Name, Me.lnkExportDataxlsx.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Membership is mandatory information. Please select Membership to continue.")
			Language.setMessage(mstrModuleName, 2, "Please select atleast one period to view report.")
			Language.setMessage(mstrModuleName, 5, "ID No.")
			Language.setMessage(mstrModuleName, 6, "Please select transaction head.")
			Language.setMessage(mstrModuleName, 7, "Selection Saved Successfully")
            Language.setMessage(mstrModuleName, 8, "Report successfully exported.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
