#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmPE_FormsReports

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPE_FormsReports"
    Private objPE_Forms As clsPE_FormsReport
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrAdvanceFilter As String = String.Empty
    Private strSavedMembership As String = String.Empty
    Private strSavedAllocations As String = String.Empty
    Private mintSaveAllocTypeId As Integer = enAllocation.CLASS_GROUP
    Private mdtPeriodStartDate As Date = Nothing
    Private mdtPeriodEndDate As Date = Nothing

#End Region

#Region " Constructor "

    Public Sub New()
        objPE_Forms = New clsPE_FormsReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objPE_Forms.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objperiod As New clscommom_period_Tran
        Dim objTrans As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Dim objCMaste As New clsCommon_Master
        Dim dsList As New DataSet
        Try
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                          True, True, "Emp", True)
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            dsList = objTrans.getComboList(FinancialYear._Object._DatabaseName, "List", True)
            With cboHealthInsurance
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 100, "Select"))  '0
                .Items.Add(Language.getMessage(mstrModuleName, 101, "FORM 8(a) - SUMMARY OF PERSONAL EMOLUMENTS ESTIMATES")) '1
                .Items.Add(Language.getMessage(mstrModuleName, 102, "FORM 8(b) - SUMMARY OF PERSONAL EMOLUMENTS ESTIMATES")) '2
                .Items.Add(Language.getMessage(mstrModuleName, 103, "FORM 8(c) - ITEM I: SUMMARY OF EXISTING EMPLOYEE ON PAYROLL")) '3
                .Items.Add(Language.getMessage(mstrModuleName, 104, "FORM 8(d) - ITEM II: SUMMARY OF EXISTING EMPLOYEES NOT ON PAYROLL"))   '4
                .Items.Add(Language.getMessage(mstrModuleName, 105, "FORM 8(f) - LIST OF EMPLOYEES EXPECTED TO RETIRE")) '5
                .SelectedIndex = 0
            End With


            dsList = objMaster.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable
                .SelectedValue = enAllocation.CLASS_GROUP
            End With

            dsList = objCMaste.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "List")
            With cboIncrement
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0).Copy
                .SelectedValue = 0
            End With
            With cboPromotion
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0).Copy
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing : objperiod = Nothing : objTrans = Nothing
            objMaster = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case cboAllocations.SelectedValue
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            lvAllocation.Items.Clear()
            Select Case cboReportType.SelectedIndex
                Case 1  'FORM 8(a)
                    strSavedAllocations = ConfigParameter._Object._PEReport1_SelectedAllocationIds
                Case 2  'FORM 8(b)
                    strSavedAllocations = ConfigParameter._Object._PEReport2_SelectedAllocationIds
                Case 3  'FORM 8(c)
                    strSavedAllocations = ConfigParameter._Object._PEReport3_SelectedAllocationIds
                Case 4  'FORM 8(d)
                    strSavedAllocations = ConfigParameter._Object._PEReport4_SelectedAllocationIds
                Case 5  'FORM 8(f)
                    strSavedAllocations = ConfigParameter._Object._PEReport5_SelectedAllocationIds
            End Select
            For Each dtRow As DataRow In dTable.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item(StrDisColName).ToString
                lvItem.Tag = dtRow.Item(StrIdColName)

                If strSavedAllocations.Trim.Length > 0 Then
                    If Array.IndexOf(strSavedAllocations.Split(","), dtRow.Item(StrIdColName).ToString) <> -1 Then
                        lvItem.Checked = True
                    End If
                End If

                lvAllocation.Items.Add(lvItem)
            Next
            If lvAllocation.Items.Count > 8 Then
                colhAllocations.Width = 249 - 20
            Else
                colhAllocations.Width = 249
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_List", mstrModuleName)
        End Try
    End Sub

    Private Sub FillMembership()
        Dim dList As New DataSet
        Dim objMembership As New clsmembership_master
        Try
            'RemoveHandler objchkM_All.CheckedChanged, AddressOf objchkM_All_CheckedChanged
            'RemoveHandler lvMembership.ItemChecked, AddressOf lvMembership_ItemChecked

            dList = objMembership.getListForCombo("List", False, , 1)
            lvMembership.Items.Clear()
            Select Case cboReportType.SelectedIndex
                Case 1  'FORM 8(a)
                    strSavedMembership = ConfigParameter._Object._PEReport1_SelectedMembershipIds
                Case 2  'FORM 8(b)
                    strSavedMembership = ConfigParameter._Object._PEReport2_SelectedMembershipIds
                Case 3  'FORM 8(c)
                    strSavedMembership = ConfigParameter._Object._PEReport3_SelectedMembershipIds
                Case 4  'FORM 8(d)
                    strSavedMembership = ConfigParameter._Object._PEReport4_SelectedMembershipIds
                Case 5  'FORM 8(f)
                    strSavedMembership = ConfigParameter._Object._PEReport5_SelectedMembershipIds
            End Select
            For Each dtRow As DataRow In dList.Tables(0).Rows
                Dim lvItem As New ListViewItem
                lvItem.Text = dtRow.Item("name").ToString
                objMembership._Membershipunkid = CInt(dtRow.Item("membershipunkid"))
                lvItem.SubItems.Add(objMembership._ETransHead_Id.ToString)
                lvItem.SubItems.Add(objMembership._CTransHead_Id.ToString)
                lvItem.Tag = dtRow.Item("membershipunkid").ToString
                If strSavedMembership.Trim.Length > 0 Then
                    If Array.IndexOf(strSavedMembership.Split(","), dtRow.Item("membershipunkid").ToString) <> -1 Then
                        lvItem.Checked = True
                    End If
                End If
                lvMembership.Items.Add(lvItem)
            Next
            If lvAllocation.Items.Count > 6 Then
                colhAllocations.Width = 229 - 20
            Else
                colhAllocations.Width = 229
            End If
            'AddHandler objchkM_All.CheckedChanged, AddressOf objchkM_All_CheckedChanged
            'AddHandler lvMembership.ItemChecked, AddressOf lvMembership_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMembership", mstrModuleName)
        Finally
            dList.Dispose() : objMembership = Nothing
        End Try
    End Sub

    Private Sub Do_Operation(ByVal blnOpt As Boolean, ByVal lv As ListView)
        Try
            For Each LItem As ListViewItem In lv.Items
                LItem.Checked = blnOpt
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Do_Operation", mstrModuleName)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            cboEmployee.SelectedValue = 0
            cboHealthInsurance.SelectedValue = 0
            cboIncrement.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboPromotion.SelectedValue = 0
            objchkA_All.Checked = False
            objchkM_All.Checked = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objPE_Forms.SetDefaultValue()

            If CInt(cboReportType.SelectedIndex) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Sorry, Report Type is mandtory information. Please select report type to continue."), enMsgBoxStyle.Information)
                cboReportType.Focus()
                Return False
            End If

            If cboReportType.SelectedIndex <> 5 Then
                If CInt(cboPeriod.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 107, "Sorry, Period is mandtory information. Please select period to continue."), enMsgBoxStyle.Information)
                    cboPeriod.Focus()
                    Return False
                End If


                If CInt(cboHealthInsurance.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 108, "Sorry, Health Insurance is mandtory information. Please select health insurance to continue."), enMsgBoxStyle.Information)
                    cboHealthInsurance.Focus()
                    Return False
                End If

                If lvMembership.CheckedItems.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 109, "Sorry, Membership is mandtory information. Please check membership(s) to continue."), enMsgBoxStyle.Information)
                    lvMembership.Focus()
                    Return False
                End If
            End If

            Select Case cboReportType.SelectedIndex
                Case 2, 3, 4, 5
                    If lvAllocation.CheckedItems.Count <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 110, "Sorry, Allocation is mandtory information. Please check allocation(s) to continue."), enMsgBoxStyle.Information)
                        lvAllocation.Focus()
                        Return False
                    End If
            End Select

            If cboReportType.SelectedIndex <> 5 Then
                If CInt(cboIncrement.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 111, "Sorry, Increment Reason is mandtory information. Please select increment reason to continue."), enMsgBoxStyle.Information)
                    cboIncrement.Focus()
                    Return False
                End If

                If CInt(cboPromotion.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 112, "Sorry, Promotion Reason is mandtory information. Please select promotion reason to continue."), enMsgBoxStyle.Information)
                    cboPromotion.Focus()
                    Return False
                End If
            End If

            objPE_Forms._ViewByIds = mstrStringIds
            objPE_Forms._ViewIndex = mintViewIdx
            objPE_Forms._ViewByName = mstrStringName
            objPE_Forms._Analysis_Fields = mstrAnalysis_Fields
            objPE_Forms._Analysis_Join = mstrAnalysis_Join
            objPE_Forms._Report_GroupName = mstrReport_GroupName
            objPE_Forms._AdvanceFilter = mstrAdvanceFilter
            objPE_Forms._AllocationTypeId = CInt(cboAllocations.SelectedValue)
            objPE_Forms._AllocationTypeName = cboAllocations.Text
            Select Case cboReportType.SelectedIndex
                Case 2, 3, 4, 5
                    objPE_Forms._DicAllocations = lvAllocation.CheckedItems.Cast(Of ListViewItem).ToDictionary(Function(x) CInt(x.Tag), Function(y) y.Text)
            End Select
            objPE_Forms._DicMemberships = lvMembership.CheckedItems.Cast(Of ListViewItem).ToDictionary(Function(x) CInt(x.SubItems(objcolhCHeadId.Index).Text), Function(y) y.Text)
            objPE_Forms._EmployeeId = CInt(cboEmployee.SelectedValue)
            objPE_Forms._EmployeeName = cboEmployee.Text
            objPE_Forms._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            objPE_Forms._HealthInsuranceHeadId = CInt(cboHealthInsurance.SelectedValue)
            objPE_Forms._HealthInsuranceHeadName = cboHealthInsurance.Text
            objPE_Forms._IncludeAccessFilter = True
            objPE_Forms._IncrementReasonId = CInt(cboIncrement.SelectedValue)
            objPE_Forms._IncrementReasonName = cboIncrement.Text
            objPE_Forms._PeriodId = CInt(cboPeriod.SelectedValue)
            objPE_Forms._PeriodName = cboPeriod.Text
            objPE_Forms._PromotionReasonId = CInt(cboPromotion.SelectedValue)
            objPE_Forms._PromotionReasonName = cboPromotion.Text
            objPE_Forms._ReportTypeId = cboReportType.SelectedIndex
            objPE_Forms._ReportTypeName = cboReportType.Text

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub RaiseCommonSearch(ByVal ebtn As eZee.Common.eZeeGradientButton, ByVal cbo As ComboBox)
        Dim frm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            With frm
                If cbo.DataSource IsNot Nothing Then
                    .DataSource = cbo.DataSource
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    Select Case ebtn.Name.ToString.ToUpper
                        Case objbtnSearchEmployee.Name.ToUpper
                            .CodeMember = "employeecode"
                        Case objbtnSearchTransHead.Name.ToUpper
                            .CodeMember = "code"
                    End Select
                End If
                If .DisplayDialog Then
                    cbo.SelectedValue = .SelectedValue
                    cbo.Focus()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "RaiseCommonSearch", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Private Sub frmPE_FormsReports_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPE_Forms = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPE_FormsReports_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPE_FormsReports_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            eZeeHeader.Title = objPE_Forms._ReportName
            eZeeHeader.Message = objPE_Forms._ReportDesc

            OtherSettings()
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPE_FormsReports_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.E Then
                    Call btnExport_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub

            If cboReportType.SelectedIndex = 5 Then
                mdtPeriodStartDate = dtpFromDate.Value.Date
                mdtPeriodEndDate = dtpToDate.Value.Date
            End If

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objPE_Forms._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(mdtPeriodEndDate))
            'Sohail (03 Aug 2019) -- End

            objPE_Forms.Export_PE_Forms(FinancialYear._Object._DatabaseName, _
                                        User._Object._Userunkid, _
                                        FinancialYear._Object._YearUnkid, _
                                        Company._Object._Companyunkid, _
                                        mdtPeriodStartDate, _
                                        mdtPeriodEndDate, _
                                        ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._OpenAfterExport, _
                                        ConfigParameter._Object._ExportReportPath, GUI.fmtCurrency)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnAdvanceFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAdvanceFilter.Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPE_FormsReport.SetMessages()
            objfrm._Other_ModuleNames = "clsPE_FormsReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchTransHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTransHead.Click
        Try
            Call RaiseCommonSearch(objbtnSearchTransHead, cboHealthInsurance)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTransHead_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Call RaiseCommonSearch(objbtnSearchEmployee, cboPromotion)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchPromotion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPromotion.Click
        Try
            'S.SANDEEP [31-MAR-2017] -- START
            'ISSUE/ENHANCEMENT : Tanapa PE Forms Statutory Report
            'Call RaiseCommonSearch(objbtnSearchPromotion, cboEmployee)
            Call RaiseCommonSearch(objbtnSearchPromotion, cboPromotion)
            'S.SANDEEP [31-MAR-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPromotion_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objbtnSearchIncrement_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchIncrement.Click
        Try
            Call RaiseCommonSearch(objbtnSearchIncrement, cboIncrement)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchIncrement_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case cboReportType.SelectedIndex
                Case 0  'SELECT
                    gbAllocationInfo.Enabled = False
                    gbHeadSelection.Enabled = False
                    gbMembershipSelection.Enabled = False
                    gbIncPromotion.Enabled = False
                Case Else
                    Call FillMembership()
                    Select Case cboReportType.SelectedIndex
                        Case 1  'FORM 8(a)
                            cboPeriod.Visible = True
                            lblPeriod.Visible = True
                            lblDateAsOn.Visible = False : lbltoDate.Visible = False : dtpFromDate.Visible = False : dtpToDate.Visible = False
                            gbAllocationInfo.Enabled = False
                            gbHeadSelection.Enabled = True
                            gbMembershipSelection.Enabled = True
                            gbIncPromotion.Enabled = True
                            mintSaveAllocTypeId = CInt(ConfigParameter._Object._PEReport1_AllocationTypeId)
                            cboHealthInsurance.SelectedValue = CInt(ConfigParameter._Object._PEReport1_HealthInsuranceMappedHeadId)
                            cboIncrement.SelectedValue = CInt(ConfigParameter._Object._PEReport1_IncrementReasonMappedId)
                            cboPromotion.SelectedValue = CInt(ConfigParameter._Object._PEReport1_PromotionReasonMappedId)

                        Case 2  'FORM 8(b)
                            cboPeriod.Visible = True
                            lblPeriod.Visible = True
                            lblDateAsOn.Visible = False : lbltoDate.Visible = False : dtpFromDate.Visible = False : dtpToDate.Visible = False
                            gbAllocationInfo.Enabled = True
                            gbHeadSelection.Enabled = True
                            gbMembershipSelection.Enabled = True
                            gbIncPromotion.Enabled = True
                            mintSaveAllocTypeId = CInt(ConfigParameter._Object._PEReport2_AllocationTypeId)
                            cboHealthInsurance.SelectedValue = CInt(ConfigParameter._Object._PEReport2_HealthInsuranceMappedHeadId)
                            cboIncrement.SelectedValue = CInt(ConfigParameter._Object._PEReport2_IncrementReasonMappedId)
                            cboPromotion.SelectedValue = CInt(ConfigParameter._Object._PEReport2_PromotionReasonMappedId)

                        Case 3  'FORM 8(c)
                            cboPeriod.Visible = True
                            lblPeriod.Visible = True
                            gbAllocationInfo.Enabled = True
                            gbHeadSelection.Enabled = True
                            gbMembershipSelection.Enabled = True
                            gbIncPromotion.Enabled = True
                            lblDateAsOn.Visible = False : lbltoDate.Visible = False : dtpFromDate.Visible = False : dtpToDate.Visible = False
                            mintSaveAllocTypeId = CInt(ConfigParameter._Object._PEReport3_AllocationTypeId)
                            cboHealthInsurance.SelectedValue = CInt(ConfigParameter._Object._PEReport3_HealthInsuranceMappedHeadId)
                            cboIncrement.SelectedValue = CInt(ConfigParameter._Object._PEReport3_IncrementReasonMappedId)
                            cboPromotion.SelectedValue = CInt(ConfigParameter._Object._PEReport3_PromotionReasonMappedId)

                        Case 4  'FORM 8(d)
                            cboPeriod.Visible = True
                            lblPeriod.Visible = True
                            gbAllocationInfo.Enabled = True
                            gbHeadSelection.Enabled = True
                            gbMembershipSelection.Enabled = True
                            gbIncPromotion.Enabled = True
                            lblDateAsOn.Visible = False : lbltoDate.Visible = False : dtpFromDate.Visible = False : dtpToDate.Visible = False
                            mintSaveAllocTypeId = CInt(ConfigParameter._Object._PEReport4_AllocationTypeId)
                            cboHealthInsurance.SelectedValue = CInt(ConfigParameter._Object._PEReport4_HealthInsuranceMappedHeadId)
                            cboIncrement.SelectedValue = CInt(ConfigParameter._Object._PEReport4_IncrementReasonMappedId)
                            cboPromotion.SelectedValue = CInt(ConfigParameter._Object._PEReport4_PromotionReasonMappedId)

                        Case 5  'FORM 8(f)
                            cboPeriod.Visible = False
                            lblPeriod.Visible = False
                            lblDateAsOn.Visible = True : lbltoDate.Visible = True : dtpFromDate.Visible = True : dtpToDate.Visible = True
                            gbAllocationInfo.Enabled = True
                            gbHeadSelection.Enabled = False
                            gbMembershipSelection.Enabled = False
                            gbIncPromotion.Enabled = False
                            mintSaveAllocTypeId = CInt(ConfigParameter._Object._PEReport5_AllocationTypeId)
                            cboHealthInsurance.SelectedValue = CInt(ConfigParameter._Object._PEReport5_HealthInsuranceMappedHeadId)
                            cboIncrement.SelectedValue = CInt(ConfigParameter._Object._PEReport5_IncrementReasonMappedId)
                            cboPromotion.SelectedValue = CInt(ConfigParameter._Object._PEReport5_PromotionReasonMappedId)
                    End Select

                    If cboAllocations.SelectedValue = mintSaveAllocTypeId Then
                        Call Fill_Data()
                    Else
                        cboAllocations.SelectedValue = mintSaveAllocTypeId
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            If CInt(cboReportType.SelectedIndex) > 0 Then
                colhAllocations.Text = cboAllocations.Text
                Call Fill_Data()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAllocations_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStartDate = objPrd._Start_Date
                mdtPeriodEndDate = objPrd._End_Date
                objPrd = Nothing
            Else
                mdtPeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date
                mdtPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objchkA_All_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkA_All.CheckedChanged
        Try
            RemoveHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
            Call Do_Operation(CBool(objchkA_All.CheckState), lvAllocation)
            AddHandler lvAllocation.ItemChecked, AddressOf lvAllocation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkA_All_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkM_All_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkM_All.CheckedChanged
        Try
            RemoveHandler lvMembership.ItemChecked, AddressOf lvMembership_ItemChecked
            Call Do_Operation(CBool(objchkM_All.CheckState), lvMembership)
            AddHandler lvMembership.ItemChecked, AddressOf lvMembership_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkM_All_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Listview Event(s) "

    Private Sub lvAllocation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvAllocation.ItemChecked
        Try
            RemoveHandler objchkA_All.CheckedChanged, AddressOf objchkA_All_CheckedChanged
            If lvAllocation.CheckedItems.Count <= 0 Then
                objchkA_All.CheckState = CheckState.Unchecked
            ElseIf lvAllocation.CheckedItems.Count < lvAllocation.Items.Count Then
                objchkA_All.CheckState = CheckState.Indeterminate
            ElseIf lvAllocation.CheckedItems.Count = lvAllocation.Items.Count Then
                objchkA_All.CheckState = CheckState.Checked
            End If
            AddHandler objchkA_All.CheckedChanged, AddressOf objchkA_All_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvAllocation_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lvMembership_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvMembership.ItemChecked
        Try
            RemoveHandler objchkM_All.CheckedChanged, AddressOf objchkM_All_CheckedChanged
            If lvMembership.CheckedItems.Count <= 0 Then
                objchkM_All.CheckState = CheckState.Unchecked
            ElseIf lvMembership.CheckedItems.Count < lvMembership.Items.Count Then
                objchkM_All.CheckState = CheckState.Indeterminate
            ElseIf lvMembership.CheckedItems.Count = lvMembership.Items.Count Then
                objchkM_All.CheckState = CheckState.Checked
            End If
            AddHandler objchkM_All.CheckedChanged, AddressOf objchkM_All_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvMembership_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub lnkTransHeadSelection_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkTransHeadSelection.LinkClicked
        Try
            If CInt(cboHealthInsurance.SelectedValue) > 0 Then
                Dim objConfig As New clsConfigOptions
                objConfig._Companyunkid = Company._Object._Companyunkid
                Select Case cboReportType.SelectedIndex
                    Case 1  'FORM 8(a)
                        objConfig._PEReport1_HealthInsuranceMappedHeadId = CInt(cboHealthInsurance.SelectedValue)
                    Case 2  'FORM 8(b)
                        objConfig._PEReport2_HealthInsuranceMappedHeadId = CInt(cboHealthInsurance.SelectedValue)
                    Case 3  'FORM 8(c)
                        objConfig._PEReport3_HealthInsuranceMappedHeadId = CInt(cboHealthInsurance.SelectedValue)
                    Case 4  'FORM 8(d)
                        objConfig._PEReport4_HealthInsuranceMappedHeadId = CInt(cboHealthInsurance.SelectedValue)
                    Case 5  'FORM 8(f)
                        objConfig._PEReport5_HealthInsuranceMappedHeadId = CInt(cboHealthInsurance.SelectedValue)
                End Select
                objConfig.updateParam()
                ConfigParameter._Object.Refresh()
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Settings saved successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkTransHeadSelection_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkMembershipSelection_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkMembershipSelection.LinkClicked
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Company._Object._Companyunkid
            Dim xCheckedItems As String = String.Empty
            If lvMembership.CheckedItems.Count > 0 Then
                xCheckedItems = String.Join(",", (From p In lvMembership.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)
                Select Case cboReportType.SelectedIndex
                    Case 1  'FORM 8(a)
                        objConfig._PEReport1_SelectedMembershipIds = xCheckedItems
                    Case 2  'FORM 8(b)
                        objConfig._PEReport2_SelectedMembershipIds = xCheckedItems
                    Case 3  'FORM 8(c)
                        objConfig._PEReport3_SelectedMembershipIds = xCheckedItems
                    Case 4  'FORM 8(d)
                        objConfig._PEReport4_SelectedMembershipIds = xCheckedItems
                    Case 5  'FORM 8(f)
                        objConfig._PEReport5_SelectedMembershipIds = xCheckedItems
                End Select
                objConfig.updateParam()
                ConfigParameter._Object.Refresh()
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Settings saved successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkMembershipSelection_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkAllocSaveInfo_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocSaveInfo.LinkClicked
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Company._Object._Companyunkid
            Dim xCheckedItems As String = String.Empty
            If lvAllocation.CheckedItems.Count > 0 Then
                xCheckedItems = String.Join(",", (From p In lvAllocation.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)
                Select Case cboReportType.SelectedIndex
                    Case 1  'FORM 8(a)
                        objConfig._PEReport1_AllocationTypeId = CInt(cboAllocations.SelectedValue)
                        objConfig._PEReport1_SelectedAllocationIds = xCheckedItems
                    Case 2  'FORM 8(b)
                        objConfig._PEReport2_AllocationTypeId = CInt(cboAllocations.SelectedValue)
                        objConfig._PEReport2_SelectedAllocationIds = xCheckedItems
                    Case 3  'FORM 8(c)
                        objConfig._PEReport3_AllocationTypeId = CInt(cboAllocations.SelectedValue)
                        objConfig._PEReport3_SelectedAllocationIds = xCheckedItems
                    Case 4  'FORM 8(d)
                        objConfig._PEReport4_AllocationTypeId = CInt(cboAllocations.SelectedValue)
                        objConfig._PEReport4_SelectedAllocationIds = xCheckedItems
                    Case 5  'FORM 8(f)
                        objConfig._PEReport5_AllocationTypeId = CInt(cboAllocations.SelectedValue)
                        objConfig._PEReport5_SelectedAllocationIds = xCheckedItems
                End Select
                objConfig.updateParam()
                ConfigParameter._Object.Refresh()
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Settings saved successfully."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocSaveInfo_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lnkSalarySelection_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSalarySelection.LinkClicked
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Company._Object._Companyunkid
            If CInt(cboIncrement.SelectedValue) > 0 Then
                Select Case cboReportType.SelectedIndex
                    Case 1  'FORM 8(a)
                        objConfig._PEReport1_IncrementReasonMappedId = CInt(cboIncrement.SelectedValue)
                    Case 2  'FORM 8(b)
                        objConfig._PEReport2_IncrementReasonMappedId = CInt(cboIncrement.SelectedValue)
                    Case 3  'FORM 8(c)
                        objConfig._PEReport3_IncrementReasonMappedId = CInt(cboIncrement.SelectedValue)
                    Case 4  'FORM 8(d)
                        objConfig._PEReport4_IncrementReasonMappedId = CInt(cboIncrement.SelectedValue)
                    Case 5  'FORM 8(f)
                        objConfig._PEReport5_IncrementReasonMappedId = CInt(cboIncrement.SelectedValue)
                End Select
            End If
            If CInt(cboPromotion.SelectedValue) > 0 Then
                Select Case cboReportType.SelectedIndex
                    Case 1  'FORM 8(a)
                        objConfig._PEReport1_PromotionReasonMappedId = CInt(cboPromotion.SelectedValue)
                    Case 2  'FORM 8(b)
                        objConfig._PEReport2_PromotionReasonMappedId = CInt(cboPromotion.SelectedValue)
                    Case 3  'FORM 8(c)
                        objConfig._PEReport3_PromotionReasonMappedId = CInt(cboPromotion.SelectedValue)
                    Case 4  'FORM 8(d)
                        objConfig._PEReport4_PromotionReasonMappedId = CInt(cboPromotion.SelectedValue)
                    Case 5  'FORM 8(f)
                        objConfig._PEReport5_PromotionReasonMappedId = CInt(cboPromotion.SelectedValue)
                End Select
            End If
            objConfig.updateParam()
            ConfigParameter._Object.Refresh()
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 106, "Settings saved successfully."), enMsgBoxStyle.Information)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSalarySelection_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbAllocationInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbAllocationInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbHeadSelection.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbHeadSelection.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbMembershipSelection.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMembershipSelection.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.objbtnAdvanceFilter.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnAdvanceFilter.GradientForeColor = GUI._ButttonFontColor

            Me.btnReset.GradientBackColor = GUI._ButttonBackColor
            Me.btnReset.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor
            Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblRType.Text = Language._Object.getCaption(Me.lblRType.Name, Me.lblRType.Text)
            Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.Name, Me.LblEmployee.Text)
            Me.colhAllocations.Text = Language._Object.getCaption(CStr(Me.colhAllocations.Tag), Me.colhAllocations.Text)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.Name, Me.lblAllocation.Text)
            Me.gbAllocationInfo.Text = Language._Object.getCaption(Me.gbAllocationInfo.Name, Me.gbAllocationInfo.Text)
            Me.lnkAllocSaveInfo.Text = Language._Object.getCaption(Me.lnkAllocSaveInfo.Name, Me.lnkAllocSaveInfo.Text)
            Me.gbHeadSelection.Text = Language._Object.getCaption(Me.gbHeadSelection.Name, Me.gbHeadSelection.Text)
            Me.lnkTransHeadSelection.Text = Language._Object.getCaption(Me.lnkTransHeadSelection.Name, Me.lnkTransHeadSelection.Text)
            Me.lblHealthInsurance.Text = Language._Object.getCaption(Me.lblHealthInsurance.Name, Me.lblHealthInsurance.Text)
            Me.gbMembershipSelection.Text = Language._Object.getCaption(Me.gbMembershipSelection.Name, Me.gbMembershipSelection.Text)
            Me.lblMemberships.Text = Language._Object.getCaption(Me.lblMemberships.Name, Me.lblMemberships.Text)
            Me.lnkMembershipSelection.Text = Language._Object.getCaption(Me.lnkMembershipSelection.Name, Me.lnkMembershipSelection.Text)
            Me.colhMembership.Text = Language._Object.getCaption(CStr(Me.colhMembership.Tag), Me.colhMembership.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 100, "Select")
            Language.setMessage(mstrModuleName, 101, "Form 8(a) - SUMMARY OF PERSONAL EMOLUMENTS ESTIMATES")
            Language.setMessage(mstrModuleName, 102, "Form 8(b) - SUMMARY OF PERSONAL EMOLUMENTS ESTIMATES")
            Language.setMessage(mstrModuleName, 103, "Form 8(c) - ITEM I: SUMMARY OF EXISTING EMPLOYEE ON PAYROLL")
            Language.setMessage(mstrModuleName, 104, "Form 8(d) - ITEM II: SUMMARY OF EXISTING EMPLOYEES NOT ON PAYROLL")
            Language.setMessage(mstrModuleName, 105, "Form 8(f) - LIST OF EMPLOYEES EXPECTED TO RETIRE")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
