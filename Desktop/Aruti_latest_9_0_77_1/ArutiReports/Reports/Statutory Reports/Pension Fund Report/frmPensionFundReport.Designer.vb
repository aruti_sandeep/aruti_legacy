﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPensionFundReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPensionFundReport))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.chkShowBasicSal = New System.Windows.Forms.CheckBox
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.elLineSort = New eZee.Common.eZeeLine
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.lblSortBy = New System.Windows.Forms.Label
        Me.lblMembershipNo = New System.Windows.Forms.Label
        Me.txtMembershipNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtGrossPayTo = New eZee.TextBox.NumericTextBox
        Me.lblGrossPayTo = New System.Windows.Forms.Label
        Me.lblGrossPayFrom = New System.Windows.Forms.Label
        Me.txtGrossPayFrom = New eZee.TextBox.NumericTextBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblMembership = New System.Windows.Forms.Label
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkDoNotShowBasicSalary = New System.Windows.Forms.CheckBox
        Me.chkShowGroupByCCenter = New System.Windows.Forms.CheckBox
        Me.chkShowOtherMembershipNo = New System.Windows.Forms.CheckBox
        Me.chkShowEmployeeCode = New System.Windows.Forms.CheckBox
        Me.lblIncludeMembership = New System.Windows.Forms.Label
        Me.cboIncludeMembership = New System.Windows.Forms.ComboBox
        Me.objchkAllMembership = New System.Windows.Forms.CheckBox
        Me.lvMembership = New eZee.Common.eZeeListView(Me.components)
        Me.objColhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhMembershipName = New System.Windows.Forms.ColumnHeader
        Me.LblCurrencyRate = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.LblCurrency = New System.Windows.Forms.Label
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblReportType = New System.Windows.Forms.Label
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.txtNumber = New System.Windows.Forms.TextBox
        Me.cboCoNumberType = New System.Windows.Forms.ComboBox
        Me.lblCoNunbertype = New System.Windows.Forms.Label
        Me.gbBasicSalaryOtherEarning = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlAdvanceTaken = New System.Windows.Forms.Panel
        Me.objbtnSearchOtherEarning = New eZee.Common.eZeeGradientButton
        Me.cboOtherEarning = New System.Windows.Forms.ComboBox
        Me.lblOtherEarning = New System.Windows.Forms.Label
        Me.gbOtherEmplrContribution = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearch_Contrib2 = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch_Contrib1 = New eZee.Common.eZeeGradientButton
        Me.lblEmplrContrib2 = New System.Windows.Forms.Label
        Me.lblEmplrContrib1 = New System.Windows.Forms.Label
        Me.cboEmplrContrib2 = New System.Windows.Forms.ComboBox
        Me.cboEmplrContrib1 = New System.Windows.Forms.ComboBox
        Me.chkIgnoreZero = New System.Windows.Forms.CheckBox
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbMandatoryInfo.SuspendLayout()
        Me.gbBasicSalaryOtherEarning.SuspendLayout()
        Me.pnlAdvanceTaken.SuspendLayout()
        Me.gbOtherEmplrContribution.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 582)
        Me.NavPanel.Size = New System.Drawing.Size(926, 50)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSort)
        Me.gbFilterCriteria.Controls.Add(Me.elLineSort)
        Me.gbFilterCriteria.Controls.Add(Me.txtOrderBy)
        Me.gbFilterCriteria.Controls.Add(Me.lblSortBy)
        Me.gbFilterCriteria.Controls.Add(Me.lblMembershipNo)
        Me.gbFilterCriteria.Controls.Add(Me.txtMembershipNo)
        Me.gbFilterCriteria.Controls.Add(Me.txtGrossPayTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblGrossPayTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblGrossPayFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtGrossPayFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 415)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(467, 161)
        Me.gbFilterCriteria.TabIndex = 2
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(329, 87)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 74
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'chkShowBasicSal
        '
        Me.chkShowBasicSal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowBasicSal.Location = New System.Drawing.Point(278, 323)
        Me.chkShowBasicSal.Name = "chkShowBasicSal"
        Me.chkShowBasicSal.Size = New System.Drawing.Size(172, 17)
        Me.chkShowBasicSal.TabIndex = 72
        Me.chkShowBasicSal.Text = "Show Basic Salary"
        Me.chkShowBasicSal.UseVisualStyleBackColor = True
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(438, 130)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 55
        '
        'elLineSort
        '
        Me.elLineSort.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elLineSort.Location = New System.Drawing.Point(8, 111)
        Me.elLineSort.Name = "elLineSort"
        Me.elLineSort.Size = New System.Drawing.Size(449, 16)
        Me.elLineSort.TabIndex = 70
        Me.elLineSort.Text = "Sorting"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(112, 130)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.Size = New System.Drawing.Size(320, 21)
        Me.txtOrderBy.TabIndex = 3
        '
        'lblSortBy
        '
        Me.lblSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSortBy.Location = New System.Drawing.Point(12, 133)
        Me.lblSortBy.Name = "lblSortBy"
        Me.lblSortBy.Size = New System.Drawing.Size(94, 15)
        Me.lblSortBy.TabIndex = 2
        Me.lblSortBy.Text = "Sort By"
        Me.lblSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMembershipNo
        '
        Me.lblMembershipNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembershipNo.Location = New System.Drawing.Point(9, 90)
        Me.lblMembershipNo.Name = "lblMembershipNo"
        Me.lblMembershipNo.Size = New System.Drawing.Size(97, 15)
        Me.lblMembershipNo.TabIndex = 68
        Me.lblMembershipNo.Text = "Membership No."
        Me.lblMembershipNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMembershipNo
        '
        Me.txtMembershipNo.Flags = 0
        Me.txtMembershipNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMembershipNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtMembershipNo.Location = New System.Drawing.Point(112, 87)
        Me.txtMembershipNo.Name = "txtMembershipNo"
        Me.txtMembershipNo.Size = New System.Drawing.Size(126, 21)
        Me.txtMembershipNo.TabIndex = 67
        '
        'txtGrossPayTo
        '
        Me.txtGrossPayTo.AllowNegative = True
        Me.txtGrossPayTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtGrossPayTo.DigitsInGroup = 0
        Me.txtGrossPayTo.Flags = 0
        Me.txtGrossPayTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGrossPayTo.Location = New System.Drawing.Point(329, 60)
        Me.txtGrossPayTo.MaxDecimalPlaces = 4
        Me.txtGrossPayTo.MaxWholeDigits = 9
        Me.txtGrossPayTo.Name = "txtGrossPayTo"
        Me.txtGrossPayTo.Prefix = ""
        Me.txtGrossPayTo.RangeMax = 1.7976931348623157E+308
        Me.txtGrossPayTo.RangeMin = -1.7976931348623157E+308
        Me.txtGrossPayTo.Size = New System.Drawing.Size(126, 21)
        Me.txtGrossPayTo.TabIndex = 58
        Me.txtGrossPayTo.Text = "0"
        Me.txtGrossPayTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblGrossPayTo
        '
        Me.lblGrossPayTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrossPayTo.Location = New System.Drawing.Point(251, 63)
        Me.lblGrossPayTo.Name = "lblGrossPayTo"
        Me.lblGrossPayTo.Size = New System.Drawing.Size(72, 15)
        Me.lblGrossPayTo.TabIndex = 57
        Me.lblGrossPayTo.Text = "To"
        Me.lblGrossPayTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblGrossPayFrom
        '
        Me.lblGrossPayFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGrossPayFrom.Location = New System.Drawing.Point(8, 63)
        Me.lblGrossPayFrom.Name = "lblGrossPayFrom"
        Me.lblGrossPayFrom.Size = New System.Drawing.Size(97, 15)
        Me.lblGrossPayFrom.TabIndex = 56
        Me.lblGrossPayFrom.Text = "Gross Pay From"
        Me.lblGrossPayFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtGrossPayFrom
        '
        Me.txtGrossPayFrom.AllowNegative = True
        Me.txtGrossPayFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtGrossPayFrom.DigitsInGroup = 0
        Me.txtGrossPayFrom.Flags = 0
        Me.txtGrossPayFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGrossPayFrom.Location = New System.Drawing.Point(111, 60)
        Me.txtGrossPayFrom.MaxDecimalPlaces = 4
        Me.txtGrossPayFrom.MaxWholeDigits = 9
        Me.txtGrossPayFrom.Name = "txtGrossPayFrom"
        Me.txtGrossPayFrom.Prefix = ""
        Me.txtGrossPayFrom.RangeMax = 1.7976931348623157E+308
        Me.txtGrossPayFrom.RangeMin = -1.7976931348623157E+308
        Me.txtGrossPayFrom.Size = New System.Drawing.Size(126, 21)
        Me.txtGrossPayFrom.TabIndex = 55
        Me.txtGrossPayFrom.Text = "0"
        Me.txtGrossPayFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(97, 15)
        Me.lblEmployee.TabIndex = 1
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(437, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 54
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(111, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(320, 21)
        Me.cboEmployee.TabIndex = 2
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(8, 118)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(97, 15)
        Me.lblMembership.TabIndex = 55
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(251, 63)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(72, 15)
        Me.lblPeriod.TabIndex = 57
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 230
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(329, 60)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboPeriod.TabIndex = 58
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.chkIgnoreZero)
        Me.gbMandatoryInfo.Controls.Add(Me.chkShowBasicSal)
        Me.gbMandatoryInfo.Controls.Add(Me.chkDoNotShowBasicSalary)
        Me.gbMandatoryInfo.Controls.Add(Me.chkShowGroupByCCenter)
        Me.gbMandatoryInfo.Controls.Add(Me.chkShowOtherMembershipNo)
        Me.gbMandatoryInfo.Controls.Add(Me.chkShowEmployeeCode)
        Me.gbMandatoryInfo.Controls.Add(Me.lblIncludeMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.cboIncludeMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.objchkAllMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.lvMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrencyRate)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.lblReportType)
        Me.gbMandatoryInfo.Controls.Add(Me.cboReportType)
        Me.gbMandatoryInfo.Controls.Add(Me.txtNumber)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCoNumberType)
        Me.gbMandatoryInfo.Controls.Add(Me.lblCoNunbertype)
        Me.gbMandatoryInfo.Controls.Add(Me.lblMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPeriod)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(12, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(467, 343)
        Me.gbMandatoryInfo.TabIndex = 67
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkDoNotShowBasicSalary
        '
        Me.chkDoNotShowBasicSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDoNotShowBasicSalary.Location = New System.Drawing.Point(278, 300)
        Me.chkDoNotShowBasicSalary.Name = "chkDoNotShowBasicSalary"
        Me.chkDoNotShowBasicSalary.Size = New System.Drawing.Size(181, 17)
        Me.chkDoNotShowBasicSalary.TabIndex = 93
        Me.chkDoNotShowBasicSalary.Text = "Do Not Show Basic/Gross Salary"
        Me.chkDoNotShowBasicSalary.UseVisualStyleBackColor = True
        '
        'chkShowGroupByCCenter
        '
        Me.chkShowGroupByCCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowGroupByCCenter.Location = New System.Drawing.Point(278, 278)
        Me.chkShowGroupByCCenter.Name = "chkShowGroupByCCenter"
        Me.chkShowGroupByCCenter.Size = New System.Drawing.Size(177, 16)
        Me.chkShowGroupByCCenter.TabIndex = 91
        Me.chkShowGroupByCCenter.Text = "Show Group By Cost Center"
        Me.chkShowGroupByCCenter.UseVisualStyleBackColor = True
        '
        'chkShowOtherMembershipNo
        '
        Me.chkShowOtherMembershipNo.Checked = True
        Me.chkShowOtherMembershipNo.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowOtherMembershipNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowOtherMembershipNo.Location = New System.Drawing.Point(111, 300)
        Me.chkShowOtherMembershipNo.Name = "chkShowOtherMembershipNo"
        Me.chkShowOtherMembershipNo.Size = New System.Drawing.Size(160, 17)
        Me.chkShowOtherMembershipNo.TabIndex = 89
        Me.chkShowOtherMembershipNo.Text = "Other Membership No."
        Me.chkShowOtherMembershipNo.UseVisualStyleBackColor = True
        '
        'chkShowEmployeeCode
        '
        Me.chkShowEmployeeCode.Checked = True
        Me.chkShowEmployeeCode.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkShowEmployeeCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowEmployeeCode.Location = New System.Drawing.Point(111, 277)
        Me.chkShowEmployeeCode.Name = "chkShowEmployeeCode"
        Me.chkShowEmployeeCode.Size = New System.Drawing.Size(161, 17)
        Me.chkShowEmployeeCode.TabIndex = 88
        Me.chkShowEmployeeCode.Text = "Show Employee Code"
        Me.chkShowEmployeeCode.UseVisualStyleBackColor = True
        '
        'lblIncludeMembership
        '
        Me.lblIncludeMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncludeMembership.Location = New System.Drawing.Point(8, 243)
        Me.lblIncludeMembership.Name = "lblIncludeMembership"
        Me.lblIncludeMembership.Size = New System.Drawing.Size(97, 26)
        Me.lblIncludeMembership.TabIndex = 84
        Me.lblIncludeMembership.Text = "Include Membership"
        Me.lblIncludeMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboIncludeMembership
        '
        Me.cboIncludeMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIncludeMembership.DropDownWidth = 230
        Me.cboIncludeMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIncludeMembership.FormattingEnabled = True
        Me.cboIncludeMembership.Location = New System.Drawing.Point(112, 250)
        Me.cboIncludeMembership.Name = "cboIncludeMembership"
        Me.cboIncludeMembership.Size = New System.Drawing.Size(241, 21)
        Me.cboIncludeMembership.TabIndex = 83
        '
        'objchkAllMembership
        '
        Me.objchkAllMembership.AutoSize = True
        Me.objchkAllMembership.Location = New System.Drawing.Point(120, 118)
        Me.objchkAllMembership.Name = "objchkAllMembership"
        Me.objchkAllMembership.Size = New System.Drawing.Size(15, 14)
        Me.objchkAllMembership.TabIndex = 80
        Me.objchkAllMembership.UseVisualStyleBackColor = True
        '
        'lvMembership
        '
        Me.lvMembership.BackColorOnChecked = False
        Me.lvMembership.CheckBoxes = True
        Me.lvMembership.ColumnHeaders = Nothing
        Me.lvMembership.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.objColhCheck, Me.colhMembershipName})
        Me.lvMembership.CompulsoryColumns = ""
        Me.lvMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvMembership.FullRowSelect = True
        Me.lvMembership.GridLines = True
        Me.lvMembership.GroupingColumn = Nothing
        Me.lvMembership.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvMembership.HideSelection = False
        Me.lvMembership.Location = New System.Drawing.Point(112, 114)
        Me.lvMembership.MinColumnWidth = 50
        Me.lvMembership.MultiSelect = False
        Me.lvMembership.Name = "lvMembership"
        Me.lvMembership.OptionalColumns = ""
        Me.lvMembership.ShowMoreItem = False
        Me.lvMembership.ShowSaveItem = False
        Me.lvMembership.ShowSelectAll = True
        Me.lvMembership.ShowSizeAllColumnsToFit = True
        Me.lvMembership.Size = New System.Drawing.Size(240, 130)
        Me.lvMembership.Sortable = True
        Me.lvMembership.TabIndex = 81
        Me.lvMembership.UseCompatibleStateImageBehavior = False
        Me.lvMembership.View = System.Windows.Forms.View.Details
        '
        'objColhCheck
        '
        Me.objColhCheck.Tag = "objColhCheck"
        Me.objColhCheck.Text = ""
        Me.objColhCheck.Width = 25
        '
        'colhMembershipName
        '
        Me.colhMembershipName.Tag = "colhMembershipName"
        Me.colhMembershipName.Text = "Membership"
        Me.colhMembershipName.Width = 209
        '
        'LblCurrencyRate
        '
        Me.LblCurrencyRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrencyRate.Location = New System.Drawing.Point(250, 89)
        Me.LblCurrencyRate.Name = "LblCurrencyRate"
        Me.LblCurrencyRate.Size = New System.Drawing.Size(207, 15)
        Me.LblCurrencyRate.TabIndex = 78
        Me.LblCurrencyRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 230
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(111, 60)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(125, 21)
        Me.cboCurrency.TabIndex = 76
        '
        'LblCurrency
        '
        Me.LblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrency.Location = New System.Drawing.Point(9, 63)
        Me.LblCurrency.Name = "LblCurrency"
        Me.LblCurrency.Size = New System.Drawing.Size(97, 15)
        Me.LblCurrency.TabIndex = 77
        Me.LblCurrency.Text = "Currency"
        Me.LblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(356, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 74
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblReportType
        '
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(97, 15)
        Me.lblReportType.TabIndex = 72
        Me.lblReportType.Text = "Report Type"
        Me.lblReportType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(111, 33)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(345, 21)
        Me.cboReportType.TabIndex = 71
        '
        'txtNumber
        '
        Me.txtNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumber.Location = New System.Drawing.Point(358, 115)
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.ReadOnly = True
        Me.txtNumber.Size = New System.Drawing.Size(97, 21)
        Me.txtNumber.TabIndex = 69
        Me.txtNumber.Visible = False
        '
        'cboCoNumberType
        '
        Me.cboCoNumberType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCoNumberType.DropDownWidth = 230
        Me.cboCoNumberType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCoNumberType.FormattingEnabled = True
        Me.cboCoNumberType.Location = New System.Drawing.Point(112, 87)
        Me.cboCoNumberType.Name = "cboCoNumberType"
        Me.cboCoNumberType.Size = New System.Drawing.Size(125, 21)
        Me.cboCoNumberType.TabIndex = 67
        '
        'lblCoNunbertype
        '
        Me.lblCoNunbertype.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCoNunbertype.Location = New System.Drawing.Point(9, 90)
        Me.lblCoNunbertype.Name = "lblCoNunbertype"
        Me.lblCoNunbertype.Size = New System.Drawing.Size(97, 15)
        Me.lblCoNunbertype.TabIndex = 68
        Me.lblCoNunbertype.Text = "Co. No. Type"
        Me.lblCoNunbertype.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbBasicSalaryOtherEarning
        '
        Me.gbBasicSalaryOtherEarning.BorderColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.Checked = False
        Me.gbBasicSalaryOtherEarning.CollapseAllExceptThis = False
        Me.gbBasicSalaryOtherEarning.CollapsedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapsedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.CollapseOnLoad = False
        Me.gbBasicSalaryOtherEarning.Controls.Add(Me.pnlAdvanceTaken)
        Me.gbBasicSalaryOtherEarning.ExpandedHoverImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedNormalImage = Nothing
        Me.gbBasicSalaryOtherEarning.ExpandedPressedImage = Nothing
        Me.gbBasicSalaryOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbBasicSalaryOtherEarning.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbBasicSalaryOtherEarning.HeaderHeight = 25
        Me.gbBasicSalaryOtherEarning.HeaderMessage = ""
        Me.gbBasicSalaryOtherEarning.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbBasicSalaryOtherEarning.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbBasicSalaryOtherEarning.HeightOnCollapse = 0
        Me.gbBasicSalaryOtherEarning.LeftTextSpace = 0
        Me.gbBasicSalaryOtherEarning.Location = New System.Drawing.Point(486, 66)
        Me.gbBasicSalaryOtherEarning.Name = "gbBasicSalaryOtherEarning"
        Me.gbBasicSalaryOtherEarning.OpenHeight = 300
        Me.gbBasicSalaryOtherEarning.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbBasicSalaryOtherEarning.ShowBorder = True
        Me.gbBasicSalaryOtherEarning.ShowCheckBox = True
        Me.gbBasicSalaryOtherEarning.ShowCollapseButton = False
        Me.gbBasicSalaryOtherEarning.ShowDefaultBorderColor = True
        Me.gbBasicSalaryOtherEarning.ShowDownButton = False
        Me.gbBasicSalaryOtherEarning.ShowHeader = True
        Me.gbBasicSalaryOtherEarning.Size = New System.Drawing.Size(429, 61)
        Me.gbBasicSalaryOtherEarning.TabIndex = 85
        Me.gbBasicSalaryOtherEarning.Temp = 0
        Me.gbBasicSalaryOtherEarning.Text = "Basic Salary As Other Earning"
        Me.gbBasicSalaryOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlAdvanceTaken
        '
        Me.pnlAdvanceTaken.Controls.Add(Me.objbtnSearchOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.cboOtherEarning)
        Me.pnlAdvanceTaken.Controls.Add(Me.lblOtherEarning)
        Me.pnlAdvanceTaken.Location = New System.Drawing.Point(2, 26)
        Me.pnlAdvanceTaken.Name = "pnlAdvanceTaken"
        Me.pnlAdvanceTaken.Size = New System.Drawing.Size(424, 33)
        Me.pnlAdvanceTaken.TabIndex = 259
        '
        'objbtnSearchOtherEarning
        '
        Me.objbtnSearchOtherEarning.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchOtherEarning.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchOtherEarning.BorderSelected = False
        Me.objbtnSearchOtherEarning.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchOtherEarning.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchOtherEarning.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchOtherEarning.Location = New System.Drawing.Point(289, 3)
        Me.objbtnSearchOtherEarning.Name = "objbtnSearchOtherEarning"
        Me.objbtnSearchOtherEarning.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchOtherEarning.TabIndex = 57
        '
        'cboOtherEarning
        '
        Me.cboOtherEarning.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherEarning.FormattingEnabled = True
        Me.cboOtherEarning.Location = New System.Drawing.Point(97, 3)
        Me.cboOtherEarning.Name = "cboOtherEarning"
        Me.cboOtherEarning.Size = New System.Drawing.Size(186, 21)
        Me.cboOtherEarning.TabIndex = 56
        '
        'lblOtherEarning
        '
        Me.lblOtherEarning.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherEarning.Location = New System.Drawing.Point(8, 8)
        Me.lblOtherEarning.Name = "lblOtherEarning"
        Me.lblOtherEarning.Size = New System.Drawing.Size(83, 16)
        Me.lblOtherEarning.TabIndex = 0
        Me.lblOtherEarning.Text = "Other Earning"
        Me.lblOtherEarning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbOtherEmplrContribution
        '
        Me.gbOtherEmplrContribution.BorderColor = System.Drawing.Color.Black
        Me.gbOtherEmplrContribution.Checked = False
        Me.gbOtherEmplrContribution.CollapseAllExceptThis = False
        Me.gbOtherEmplrContribution.CollapsedHoverImage = Nothing
        Me.gbOtherEmplrContribution.CollapsedNormalImage = Nothing
        Me.gbOtherEmplrContribution.CollapsedPressedImage = Nothing
        Me.gbOtherEmplrContribution.CollapseOnLoad = False
        Me.gbOtherEmplrContribution.Controls.Add(Me.objbtnSearch_Contrib2)
        Me.gbOtherEmplrContribution.Controls.Add(Me.objbtnSearch_Contrib1)
        Me.gbOtherEmplrContribution.Controls.Add(Me.lblEmplrContrib2)
        Me.gbOtherEmplrContribution.Controls.Add(Me.lblEmplrContrib1)
        Me.gbOtherEmplrContribution.Controls.Add(Me.cboEmplrContrib2)
        Me.gbOtherEmplrContribution.Controls.Add(Me.cboEmplrContrib1)
        Me.gbOtherEmplrContribution.ExpandedHoverImage = Nothing
        Me.gbOtherEmplrContribution.ExpandedNormalImage = Nothing
        Me.gbOtherEmplrContribution.ExpandedPressedImage = Nothing
        Me.gbOtherEmplrContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOtherEmplrContribution.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbOtherEmplrContribution.HeaderHeight = 25
        Me.gbOtherEmplrContribution.HeaderMessage = ""
        Me.gbOtherEmplrContribution.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbOtherEmplrContribution.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbOtherEmplrContribution.HeightOnCollapse = 0
        Me.gbOtherEmplrContribution.LeftTextSpace = 0
        Me.gbOtherEmplrContribution.Location = New System.Drawing.Point(486, 133)
        Me.gbOtherEmplrContribution.Name = "gbOtherEmplrContribution"
        Me.gbOtherEmplrContribution.OpenHeight = 300
        Me.gbOtherEmplrContribution.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbOtherEmplrContribution.ShowBorder = True
        Me.gbOtherEmplrContribution.ShowCheckBox = False
        Me.gbOtherEmplrContribution.ShowCollapseButton = False
        Me.gbOtherEmplrContribution.ShowDefaultBorderColor = True
        Me.gbOtherEmplrContribution.ShowDownButton = False
        Me.gbOtherEmplrContribution.ShowHeader = True
        Me.gbOtherEmplrContribution.Size = New System.Drawing.Size(429, 91)
        Me.gbOtherEmplrContribution.TabIndex = 6
        Me.gbOtherEmplrContribution.Temp = 0
        Me.gbOtherEmplrContribution.Text = "Other Employer Contribution"
        Me.gbOtherEmplrContribution.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbOtherEmplrContribution.Visible = False
        '
        'objbtnSearch_Contrib2
        '
        Me.objbtnSearch_Contrib2.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch_Contrib2.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch_Contrib2.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch_Contrib2.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch_Contrib2.BorderSelected = False
        Me.objbtnSearch_Contrib2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch_Contrib2.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearch_Contrib2.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch_Contrib2.Location = New System.Drawing.Point(396, 60)
        Me.objbtnSearch_Contrib2.Name = "objbtnSearch_Contrib2"
        Me.objbtnSearch_Contrib2.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch_Contrib2.TabIndex = 90
        '
        'objbtnSearch_Contrib1
        '
        Me.objbtnSearch_Contrib1.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch_Contrib1.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch_Contrib1.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch_Contrib1.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch_Contrib1.BorderSelected = False
        Me.objbtnSearch_Contrib1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch_Contrib1.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearch_Contrib1.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch_Contrib1.Location = New System.Drawing.Point(396, 33)
        Me.objbtnSearch_Contrib1.Name = "objbtnSearch_Contrib1"
        Me.objbtnSearch_Contrib1.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch_Contrib1.TabIndex = 89
        '
        'lblEmplrContrib2
        '
        Me.lblEmplrContrib2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmplrContrib2.Location = New System.Drawing.Point(14, 63)
        Me.lblEmplrContrib2.Name = "lblEmplrContrib2"
        Me.lblEmplrContrib2.Size = New System.Drawing.Size(128, 15)
        Me.lblEmplrContrib2.TabIndex = 87
        Me.lblEmplrContrib2.Text = "Employer Contribution 2"
        Me.lblEmplrContrib2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEmplrContrib1
        '
        Me.lblEmplrContrib1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmplrContrib1.Location = New System.Drawing.Point(14, 36)
        Me.lblEmplrContrib1.Name = "lblEmplrContrib1"
        Me.lblEmplrContrib1.Size = New System.Drawing.Size(128, 15)
        Me.lblEmplrContrib1.TabIndex = 86
        Me.lblEmplrContrib1.Text = "Employer Contribution 1"
        Me.lblEmplrContrib1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboEmplrContrib2
        '
        Me.cboEmplrContrib2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmplrContrib2.DropDownWidth = 230
        Me.cboEmplrContrib2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmplrContrib2.FormattingEnabled = True
        Me.cboEmplrContrib2.Location = New System.Drawing.Point(148, 60)
        Me.cboEmplrContrib2.Name = "cboEmplrContrib2"
        Me.cboEmplrContrib2.Size = New System.Drawing.Size(242, 21)
        Me.cboEmplrContrib2.TabIndex = 85
        '
        'cboEmplrContrib1
        '
        Me.cboEmplrContrib1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmplrContrib1.DropDownWidth = 230
        Me.cboEmplrContrib1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmplrContrib1.FormattingEnabled = True
        Me.cboEmplrContrib1.Location = New System.Drawing.Point(148, 33)
        Me.cboEmplrContrib1.Name = "cboEmplrContrib1"
        Me.cboEmplrContrib1.Size = New System.Drawing.Size(242, 21)
        Me.cboEmplrContrib1.TabIndex = 84
        '
        'chkIgnoreZero
        '
        Me.chkIgnoreZero.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIgnoreZero.Location = New System.Drawing.Point(111, 323)
        Me.chkIgnoreZero.Name = "chkIgnoreZero"
        Me.chkIgnoreZero.Size = New System.Drawing.Size(161, 17)
        Me.chkIgnoreZero.TabIndex = 95
        Me.chkIgnoreZero.Text = "Ignore Zero Value"
        Me.chkIgnoreZero.UseVisualStyleBackColor = True
        '
        'frmPensionFundReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(926, 632)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Controls.Add(Me.gbBasicSalaryOtherEarning)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.gbOtherEmplrContribution)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPensionFundReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Controls.SetChildIndex(Me.gbOtherEmplrContribution, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbBasicSalaryOtherEarning, 0)
        Me.Controls.SetChildIndex(Me.gbMandatoryInfo, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.gbMandatoryInfo.PerformLayout()
        Me.gbBasicSalaryOtherEarning.ResumeLayout(False)
        Me.pnlAdvanceTaken.ResumeLayout(False)
        Me.gbOtherEmplrContribution.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboCoNumberType As System.Windows.Forms.ComboBox
    Friend WithEvents lblCoNunbertype As System.Windows.Forms.Label
    Friend WithEvents txtNumber As System.Windows.Forms.TextBox
    Friend WithEvents txtGrossPayFrom As eZee.TextBox.NumericTextBox
    Friend WithEvents lblGrossPayFrom As System.Windows.Forms.Label
    Friend WithEvents txtGrossPayTo As eZee.TextBox.NumericTextBox
    Friend WithEvents lblGrossPayTo As System.Windows.Forms.Label
    Friend WithEvents txtMembershipNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblMembershipNo As System.Windows.Forms.Label
    Friend WithEvents lblSortBy As System.Windows.Forms.Label
    Friend WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Friend WithEvents elLineSort As eZee.Common.eZeeLine
    Friend WithEvents cboReportType As System.Windows.Forms.ComboBox
    Friend WithEvents lblReportType As System.Windows.Forms.Label
    Friend WithEvents chkShowBasicSal As System.Windows.Forms.CheckBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents LblCurrency As System.Windows.Forms.Label
    Friend WithEvents LblCurrencyRate As System.Windows.Forms.Label
    Friend WithEvents objchkAllMembership As System.Windows.Forms.CheckBox
    Friend WithEvents lvMembership As eZee.Common.eZeeListView
    Friend WithEvents objColhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMembershipName As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblIncludeMembership As System.Windows.Forms.Label
    Friend WithEvents cboIncludeMembership As System.Windows.Forms.ComboBox
    Friend WithEvents gbBasicSalaryOtherEarning As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlAdvanceTaken As System.Windows.Forms.Panel
    Friend WithEvents objbtnSearchOtherEarning As eZee.Common.eZeeGradientButton
    Friend WithEvents cboOtherEarning As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherEarning As System.Windows.Forms.Label
    Friend WithEvents gbOtherEmplrContribution As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboEmplrContrib2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmplrContrib1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmplrContrib2 As System.Windows.Forms.Label
    Friend WithEvents lblEmplrContrib1 As System.Windows.Forms.Label
    Friend WithEvents objbtnSearch_Contrib2 As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearch_Contrib1 As eZee.Common.eZeeGradientButton
    Friend WithEvents chkShowOtherMembershipNo As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowEmployeeCode As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowGroupByCCenter As System.Windows.Forms.CheckBox
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents chkDoNotShowBasicSalary As System.Windows.Forms.CheckBox
    Friend WithEvents chkIgnoreZero As System.Windows.Forms.CheckBox
End Class
