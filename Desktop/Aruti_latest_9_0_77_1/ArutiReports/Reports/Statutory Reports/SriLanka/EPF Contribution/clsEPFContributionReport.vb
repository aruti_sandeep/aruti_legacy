'************************************************************************************************************************************
'Class Name : clsEPFContributionReport.vb
'Purpose    :
'Date       :03/03/2011
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>

Public Class clsEPFContributionReport
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsEPFContributionReport"
    Private mstrReportId As String = enArutiReport.EPF_Contribution_Report

    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mintPeriodId As Integer = -1
    Private mstrPeriodName As String = String.Empty
    Private mdtPeriod_Startdate As Date = Nothing
    Private mdtPeriod_Enddate As Date = Nothing
    Private mintMembershipId As Integer = 1
    Private mstrMembershipName As String = String.Empty

    Private mbIncludeInactiveEmp As Boolean = True

    Private mstrExchangeRate As String = ""
    Private mintCountryId As Integer = 0
    Private mintBaseCurrId As Integer = 0
    Private mintPaidCurrencyId As Integer = 0
    Private mdecConversionRate As Decimal = 0
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""

    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
#End Region

#Region " Properties "

    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _Period_Startdate()
        Set(ByVal value)
            mdtPeriod_Startdate = value
        End Set
    End Property

    Public WriteOnly Property _Period_Enddate()
        Set(ByVal value)
            mdtPeriod_Enddate = value
        End Set
    End Property

    Public WriteOnly Property _MembershipId() As Integer
        Set(ByVal value As Integer)
            mintMembershipId = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property

    Public WriteOnly Property _CountryId() As Integer
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrId = value
        End Set
    End Property

    Public WriteOnly Property _PaidCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintPaidCurrencyId = value
        End Set
    End Property

    Public WriteOnly Property _ConversionRate() As Decimal
        Set(ByVal value As Decimal)
            mdecConversionRate = value
        End Set
    End Property

    Public WriteOnly Property _ExchangeRate() As String
        Set(ByVal value As String)
            mstrExchangeRate = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mbIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintMembershipId = 0
            mstrMembershipName = ""
            mintPeriodId = 0
            mstrPeriodName = ""
            mdtPeriod_Startdate = Nothing
            mdtPeriod_Enddate = Nothing
            mbIncludeInactiveEmp = True

            mintReportId = -1
            mstrReportTypeName = ""

            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""

            mintCountryId = 0
            mintBaseCurrId = 0
            mintPaidCurrencyId = 0
            mdecConversionRate = 0
            mstrExchangeRate = ""


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overloads Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then

        '        Dim intArrayColumnWidth As Integer() = {130, 200, 100, 110, 110, 110, 120, 150}
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim row As ExcelWriter.WorksheetRow
        '        Dim wcell As WorksheetCell
        '        If mdtTableExcel IsNot Nothing Then



        '            '*******   REPORT HEADER   ******
        '            'row = New WorksheetRow()
        '            'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 39, "Employer Number"), "s9bw")
        '            'row.Cells.Add(wcell)

        '            'wcell = New WorksheetCell(Company._Object._Nssfno, "s9w")
        '            'row.Cells.Add(wcell)
        '            'wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '            'rowsArrayHeader.Add(row)
        '            ''--------------------

        '            'row = New WorksheetRow()
        '            'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 37, "Employer Name"), "s9bw")
        '            'row.Cells.Add(wcell)

        '            'wcell = New WorksheetCell(Company._Object._Name, "s9w") '& " " & mstrAddress
        '            'row.Cells.Add(wcell)
        '            'wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '            'rowsArrayHeader.Add(row)
        '            ''--------------------

        '            'row = New WorksheetRow()
        '            'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 40, "Contributions Period"), "s9bw")
        '            'row.Cells.Add(wcell)

        '            'wcell = New WorksheetCell(mstrPeriodName, "s9w")
        '            'row.Cells.Add(wcell)
        '            'wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '            'rowsArrayHeader.Add(row)
        '            ''--------------------

        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell("", "s9w")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayHeader.Add(row)
        '            '--------------------
        '        End If

        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, Nothing, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, Nothing)


        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then

                Dim intArrayColumnWidth As Integer() = {130, 200, 100, 110, 110, 110, 120, 150}
                Dim rowsArrayHeader As New ArrayList
                Dim row As ExcelWriter.WorksheetRow
                Dim wcell As WorksheetCell
                If mdtTableExcel IsNot Nothing Then



                    '*******   REPORT HEADER   ******
                    'row = New WorksheetRow()
                    'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 39, "Employer Number"), "s9bw")
                    'row.Cells.Add(wcell)

                    'wcell = New WorksheetCell(Company._Object._Nssfno, "s9w")
                    'row.Cells.Add(wcell)
                    'wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                    'rowsArrayHeader.Add(row)
                    ''--------------------

                    'row = New WorksheetRow()
                    'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 37, "Employer Name"), "s9bw")
                    'row.Cells.Add(wcell)

                    'wcell = New WorksheetCell(Company._Object._Name, "s9w") '& " " & mstrAddress
                    'row.Cells.Add(wcell)
                    'wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                    'rowsArrayHeader.Add(row)
                    ''--------------------

                    'row = New WorksheetRow()
                    'wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 40, "Contributions Period"), "s9bw")
                    'row.Cells.Add(wcell)

                    'wcell = New WorksheetCell(mstrPeriodName, "s9w")
                    'row.Cells.Add(wcell)
                    'wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

                    'rowsArrayHeader.Add(row)
                    ''--------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s9w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayHeader.Add(row)
                    '--------------------
                End If

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, True, Nothing, mstrReportTypeName, "", " ", Nothing, "", True, rowsArrayHeader, Nothing, Nothing, Nothing, Nothing)

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try

            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsBasGross As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If mdtPeriod_Startdate <> Nothing AndAlso mdtPeriod_Enddate <> Nothing Then
                dtPeriodStart = mdtPeriod_Startdate
                dtPeriodEnd = mdtPeriod_Enddate
            End If
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            If mintBaseCurrId = mintPaidCurrencyId Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'StrQ = "SELECT  hremployee_master.employeecode AS employeecode " & _
                '              ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
                '              ", ISNULL(hremployee_master.surname, '') AS surname " & _
                '              ", hremployee_master.employeeunkid AS EmpId " & _
                '              ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
                '              ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
                '              ", ISNULL(CAmount, 0) AS employercontribution " & _
                '              ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0) AS Amount " & _
                '              ", '' AS Remark " & _
                '              ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                '              ", GrossBasic.BasicSal " & _
                '              ", GrossBasic.GrossPay " & _
                '              ", CASE WHEN ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112), " & _
                '                                 "'19000101') BETWEEN @start_date " & _
                '                                             "AND     @end_date THEN 'V' " & _
                '                     "WHEN ISNULL(CONVERT(CHAR(8), hremployee_master.termination_to_date, 112), " & _
                '                                 "'19000101') BETWEEN @start_date " & _
                '                                             "AND     @end_date THEN 'V' " & _
                '                     "WHEN ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate, 112), " & _
                '                                 "'19000101') BETWEEN @start_date " & _
                '                                             "AND     @end_date THEN 'V' " & _
                '                     "WHEN CONVERT(CHAR(8), hremployee_master.appointeddate, 112) BETWEEN @start_date " & _
                '                                                                      "AND " & _
                '                                                                      "@end_date " & _
                '                     "THEN 'N' " & _
                '                     "ELSE 'E' " & _
                '                "END AS MemberStatus " & _
                '              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS DaysWorked " & _
                '              ", ISNULL(hremployee_master.jobunkid, 0) AS jobunkid " & _
                '              ", ISNULL(hrjob_master.job_code, '') AS job_code " & _
                '              ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
                '              ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS identity_no "


                StrQ = "SELECT  hremployee_master.employeecode AS employeecode " & _
                              ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
                              ", ISNULL(hremployee_master.surname, '') AS surname " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
                              ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
                              ", ISNULL(CAmount, 0) AS employercontribution " & _
                              ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0) AS Amount " & _
                              ", '' AS Remark " & _
                              ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                              ", GrossBasic.BasicSal " & _
                              ", GrossBasic.GrossPay " & _
                              ", CASE WHEN ISNULL(CONVERT(CHAR(8), EocDt.date2, 112),'19000101') BETWEEN @start_date AND @end_date THEN 'V' " & _
                              "       WHEN ISNULL(CONVERT(CHAR(8), RT.date1, 112),'19000101') BETWEEN @start_date AND @end_date THEN 'V' " & _
                              "       WHEN ISNULL(CONVERT(CHAR(8), EocDt.date1, 112),'19000101') BETWEEN @start_date AND @end_date THEN 'V' " & _
                              "       WHEN CONVERT(CHAR(8), hremployee_master.appointeddate, 112) BETWEEN @start_date AND @end_date THEN 'N' ELSE 'E' " & _
                              "  END AS MemberStatus " & _
                              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS DaysWorked " & _
                              ", ISNULL(hremployee_master.jobunkid, 0) AS jobunkid " & _
                              ", ISNULL(hrjob_master.job_code, '') AS job_code " & _
                              ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
                              ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS identity_no "
                'S.SANDEEP [04 JUN 2015] -- END

                If mblnFirstNamethenSurname = True Then
                    StrQ &= "  , ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
                Else
                    StrQ &= "  , ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'StrQ &= "FROM    prpayrollprocess_tran " & _
                '                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
                '                "LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                '                                                    "AND hremployee_idinfo_tran.isdefault = 1 " & _
                '                "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                '                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                '                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                '                                                 "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                '                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                '                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '                                                  "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 "

                StrQ &= "FROM    prpayrollprocess_tran " & _
                                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         jobunkid " & _
                                "        ,jobgroupunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_categorization_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                                "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         date1 " & _
                                "        ,date2 " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_dates_tran " & _
                                "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ")AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 " & _
                                "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         date1 " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_dates_tran " & _
                                "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ")AS RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 " & _
                                "LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                                    "AND hremployee_idinfo_tran.isdefault = 1 " & _
                                "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                 "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                  "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 "
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS CAmount " & _
                                            "FROM    prpayrollprocess_tran " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, " & _
                                                                                      "0) = 0 " & _
                                                    "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                                     "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                                      "AND ISNULL(cfcommon_period_tran.isactive, 0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                                    "AND hrmembership_master.membershipunkid = @MemId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= ") AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                                "LEFT JOIN ( SELECT  A.Empid " & _
                                                  ", ISNULL(BasicSal, 0) AS BasicSal " & _
                                                  ", ISNULL(GrossPay, 0) AS GrossPay " & _
                                            "FROM    ( SELECT    prpayrollprocess_tran.employeeunkid AS Empid " & _
                                                              ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
                                                      "FROM      prpayrollprocess_tran " & _
                                                                "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                              WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                                "AND cfcommon_period_tran.isactive = 1 " & _
                                                                "AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                                                "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & " "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid " & _
                                                    ") AS A " & _
                                                    "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid AS Empid " & _
                                                                      ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay " & _
                                                                "FROM    prpayrollprocess_tran " & _
                                                                        "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                                      "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                                        "LEFT  JOIN practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
                                                                                      "AND prpayrollprocess_tran.activityunkid > 0 " & _
                                                                        "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                        "JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                        "JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                                        WHERE   ISNULL(prtranhead_master.trnheadtype_id, 1) = 1 " & _
                                                                        "AND ISNULL(practivity_master.trnheadtype_id, " & enTranHeadType.EarningForEmployees & ") = 1 " & _
                                                                        "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                                        "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                                        "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                                                        "AND cfcommon_period_tran.periodunkid = @PeriodId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                                      GROUP BY prpayrollprocess_tran.employeeunkid " & _
                                                              ") AS G ON G.Empid = A.Empid " & _
                                          ") AS GrossBasic ON GrossBasic.Empid = hremployee_master.employeeunkid " & _
                                "LEFT JOIN ( SELECT  employeeunkid " & _
                                                  ", SUM(day_type) AS DaysWorked " & _
                                            "FROM    tnalogin_summary " & _
                                            "WHERE   login_date BETWEEN @start_date AND @end_date " & _
                                                    "AND total_hrs <> total_overtime " & _
                                            "GROUP BY employeeunkid " & _
                                          ") AS TableDaysWorked ON hremployee_master.employeeunkid = TableDaysWorked.employeeunkid "


                StrQ &= "WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                                " AND hrmembership_master.membershipunkid = @MemId "


                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END


                StrQ &= " ORDER BY employeecode "

            ElseIf mintBaseCurrId <> mintPaidCurrencyId Then

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'StrQ = "SELECT  hremployee_master.employeecode AS employeecode " & _
                '              ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
                '              ", ISNULL(hremployee_master.surname, '') AS surname " & _
                '              ", hremployee_master.employeeunkid AS EmpId " & _
                '              ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
                '              ", ( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
                '              ", ( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) *  ISNULL(CAmount, 0) AS employercontribution " & _
                '              ", ( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount,0)) AS Amount " & _
                '              ", '' AS Remark " & _
                '              ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                '              ", GrossBasic.BasicSal " & _
                '              ", GrossBasic.GrossPay " & _
                '              ", CASE WHEN ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112), " & _
                '                                 "'19000101') BETWEEN @start_date " & _
                '                                             "AND     @end_date THEN 'V' " & _
                '                     "WHEN ISNULL(CONVERT(CHAR(8), hremployee_master.termination_to_date, 112), " & _
                '                                 "'19000101') BETWEEN @start_date " & _
                '                                             "AND     @end_date THEN 'V' " & _
                '                     "WHEN ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate, 112), " & _
                '                                 "'19000101') BETWEEN @start_date " & _
                '                                             "AND     @end_date THEN 'V' " & _
                '                     "WHEN CONVERT(CHAR(8), hremployee_master.appointeddate, 112) BETWEEN @start_date " & _
                '                                                                      "AND " & _
                '                                                                      "@end_date " & _
                '                     "THEN 'N' " & _
                '                     "ELSE 'E' " & _
                '                "END AS MemberStatus " & _
                '              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS DaysWorked " & _
                '              ", ISNULL(hremployee_master.jobunkid, 0) AS jobunkid " & _
                '              ", ISNULL(hrjob_master.job_code, '') AS job_code " & _
                '              ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
                '              ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS identity_no "


                StrQ = "SELECT  hremployee_master.employeecode AS employeecode " & _
                              ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
                              ", ISNULL(hremployee_master.surname, '') AS surname " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
                              ", ( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
                              ", ( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) *  ISNULL(CAmount, 0) AS employercontribution " & _
                              ", ( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount,0)) AS Amount " & _
                              ", '' AS Remark " & _
                              ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                              ", GrossBasic.BasicSal " & _
                              ", GrossBasic.GrossPay " & _
                              ", CASE WHEN ISNULL(CONVERT(CHAR(8), EocDt.date2, 112),'19000101') BETWEEN @start_date AND @end_date THEN 'V' " & _
                              "       WHEN ISNULL(CONVERT(CHAR(8), RT.date1, 112),'19000101') BETWEEN @start_date AND @end_date THEN 'V' " & _
                              "       WHEN ISNULL(CONVERT(CHAR(8), EocDt.date1, 112),'19000101') BETWEEN @start_date AND @end_date THEN 'V' " & _
                              "       WHEN CONVERT(CHAR(8), hremployee_master.appointeddate, 112) BETWEEN @start_date AND @end_date THEN 'N' ELSE 'E' " & _
                              "  END AS MemberStatus " & _
                              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS DaysWorked " & _
                              ", ISNULL(hremployee_master.jobunkid, 0) AS jobunkid " & _
                              ", ISNULL(hrjob_master.job_code, '') AS job_code " & _
                              ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
                              ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS identity_no "
                'S.SANDEEP [04 JUN 2015] -- END

                If mblnFirstNamethenSurname = True Then
                    StrQ &= "  , ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
                Else
                    StrQ &= "  , ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'StrQ &= "FROM    prpayrollprocess_tran " & _
                '                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
                '                "LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                '                                                    "AND hremployee_idinfo_tran.isdefault = 1 " & _
                '                "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                '                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                '                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                '                                                 "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                '                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                '                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '                                                  "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 " & _
                '                "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                '                            "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                '                            "AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId

                StrQ &= "FROM    prpayrollprocess_tran " & _
                                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         jobunkid " & _
                                "        ,jobgroupunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_categorization_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                                "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         date1 " & _
                                "        ,date2 " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_dates_tran " & _
                                "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ")AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 " & _
                                "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         date1 " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_dates_tran " & _
                                "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ")AS RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 " & _
                                "LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                                    "AND hremployee_idinfo_tran.isdefault = 1 " & _
                                "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                 "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                  "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 " & _
                                "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                            "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                            "AND prpayment_tran.isvoid = 0 AND prpayment_tran.countryunkid = " & mintCountryId
                'S.SANDEEP [04 JUN 2015] -- END

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS CAmount " & _
                                            "FROM    prpayrollprocess_tran " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, " & _
                                                                                      "0) = 0 " & _
                                                    "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                                     "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                                      "AND ISNULL(cfcommon_period_tran.isactive, 0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                   "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                   "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                                   "AND hrmembership_master.membershipunkid = @MemId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= ") AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                               "LEFT JOIN ( SELECT  A.Empid " & _
                                                 ", ISNULL(BasicSal, 0) AS BasicSal " & _
                                                 ", ISNULL(GrossPay, 0) AS GrossPay " & _
                                           "FROM    ( SELECT    prpayrollprocess_tran.employeeunkid AS Empid " & _
                                                             ", (CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) *  SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
                                                     "FROM      prpayrollprocess_tran " & _
                                                               "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                               "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                               "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                               "JOIN prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                                                        "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid = " & mintCountryId & " "

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                              WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                                "AND cfcommon_period_tran.isactive = 1 " & _
                                                                "AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                                                "AND prpayment_tran.periodunkid = @PeriodId " & _
                                                                "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & " "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                              GROUP BY  prpayrollprocess_tran.employeeunkid, prpayment_tran.expaidrate,prpayment_tran.baseexchangerate " & _
                                                    ") AS A " & _
                                                    "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid AS Empid " & _
                                                                      ", ( CAST(prpayment_tran.expaidrate AS DECIMAL(36, 15)) / prpayment_tran.baseexchangerate ) * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay " & _
                                                                "FROM    prpayrollprocess_tran " & _
                                                                        "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                                      "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                                        "LEFT  JOIN practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
                                                                                      "AND prpayrollprocess_tran.activityunkid > 0 " & _
                                                                        "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                        "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                        "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                                        "LEFT JOIN prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                                                                    "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid = " & mintCountryId & " "

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	                WHERE ISNULL(prtranhead_master.trnheadtype_id, 1) = 1 " & _
                                               "AND ISNULL(practivity_master.trnheadtype_id, 1) = 1 " & _
                                               "AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                               "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                               "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                               "AND cfcommon_period_tran.isactive = 1 " & _
                                               "AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                                "AND prpayment_tran.periodunkid = @PeriodId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "            GROUP BY prpayrollprocess_tran.employeeunkid, prpayment_tran.expaidrate,prpayment_tran.baseexchangerate " & _
                                       ") AS G ON G.Empid = A.Empid " & _
                                 ") AS GrossBasic ON     GrossBasic.Empid    = hremployee_master.employeeunkid " & _
                                 "LEFT JOIN ( SELECT  employeeunkid " & _
                                                  ", SUM(day_type) AS DaysWorked " & _
                                            "FROM    tnalogin_summary " & _
                                            "WHERE   login_date BETWEEN @start_date AND @end_date " & _
                                                    "AND total_hrs <> total_overtime " & _
                                            "GROUP BY employeeunkid " & _
                                          ") AS TableDaysWorked ON hremployee_master.employeeunkid = TableDaysWorked.employeeunkid " & _
                           "WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                   "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                   "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                   "AND hrmembership_master.membershipunkid = @MemId " & _
                                   " AND prpayment_tran.periodunkid = @PeriodId "


                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'StrQ &= "UNION ALL " & _
                '        "SELECT  hremployee_master.employeecode AS employeecode " & _
                '              ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
                '              ", ISNULL(hremployee_master.surname, '') AS surname " & _
                '              ", hremployee_master.employeeunkid AS EmpId " & _
                '              ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
                '              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
                '              ", " & mdecConversionRate & " *  ISNULL(CAmount, 0) AS employercontribution " & _
                '              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0)) AS Amount " & _
                '              ", '' AS Remark " & _
                '              ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                '              ", GrossBasic.BasicSal " & _
                '              ", GrossBasic.GrossPay " & _
                '              ", CASE WHEN ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112), " & _
                '                                 "'19000101') BETWEEN @start_date " & _
                '                                             "AND     @end_date THEN 'V' " & _
                '                     "WHEN ISNULL(CONVERT(CHAR(8), hremployee_master.termination_to_date, 112), " & _
                '                                 "'19000101') BETWEEN @start_date " & _
                '                                             "AND     @end_date THEN 'V' " & _
                '                     "WHEN ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate, 112), " & _
                '                                 "'19000101') BETWEEN @start_date " & _
                '                                             "AND     @end_date THEN 'V' " & _
                '                     "WHEN CONVERT(CHAR(8), hremployee_master.appointeddate, 112) BETWEEN @start_date " & _
                '                                                                      "AND " & _
                '                                                                      "@end_date " & _
                '                     "THEN 'N' " & _
                '                     "ELSE 'E' " & _
                '                "END AS MemberStatus " & _
                '              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS DaysWorked " & _
                '              ", ISNULL(hremployee_master.jobunkid, 0) AS jobunkid " & _
                '              ", ISNULL(hrjob_master.job_code, '') AS job_code " & _
                '              ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
                '              ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS identity_no "

                StrQ &= "UNION ALL " & _
                        "SELECT  hremployee_master.employeecode AS employeecode " & _
                              ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
                              ", ISNULL(hremployee_master.surname, '') AS surname " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAmount, 0) AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0)) AS Amount " & _
                              ", '' AS Remark " & _
                              ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                              ", GrossBasic.BasicSal " & _
                              ", GrossBasic.GrossPay " & _
                              ", CASE WHEN ISNULL(CONVERT(CHAR(8), EocDt.date2, 112),'19000101') BETWEEN @start_date AND @end_date THEN 'V' " & _
                              "       WHEN ISNULL(CONVERT(CHAR(8), RT.date1, 112),'19000101') BETWEEN @start_date AND @end_date THEN 'V' " & _
                              "       WHEN ISNULL(CONVERT(CHAR(8), EocDt.date1, 112),'19000101') BETWEEN @start_date AND @end_date THEN 'V' " & _
                              "       WHEN CONVERT(CHAR(8), hremployee_master.appointeddate, 112) BETWEEN @start_date AND @end_date THEN 'N' ELSE 'E' " & _
                              "  END AS MemberStatus " & _
                              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS DaysWorked " & _
                              ", ISNULL(hremployee_master.jobunkid, 0) AS jobunkid " & _
                              ", ISNULL(hrjob_master.job_code, '') AS job_code " & _
                              ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
                              ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS identity_no "
                'S.SANDEEP [04 JUN 2015] -- END

                If mblnFirstNamethenSurname = True Then
                    StrQ &= "  ,  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
                Else
                    StrQ &= "  ,  ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'StrQ &= "FROM    prpayrollprocess_tran " & _
                '                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
                '                "LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                '                                                    "AND hremployee_idinfo_tran.isdefault = 1 " & _
                '                "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                '                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                '                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                '                                                 "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                '                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                '                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '                                                  "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 " & _
                '                "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                '                            "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                '                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " "


                StrQ &= "FROM    prpayrollprocess_tran " & _
                                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         jobunkid " & _
                                "        ,jobgroupunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_categorization_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                                "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         date1 " & _
                                "        ,date2 " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_dates_tran " & _
                                "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ")AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 " & _
                                "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         date1 " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_dates_tran " & _
                                "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ")AS RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 " & _
                                "LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                                    "AND hremployee_idinfo_tran.isdefault = 1 " & _
                                "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                 "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                  "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 " & _
                                "JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                            "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " "
                'S.SANDEEP [04 JUN 2015] -- END


                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS CAmount " & _
                                            "FROM    prpayrollprocess_tran " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, " & _
                                                                                      "0) = 0 " & _
                                                    "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                                     "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                                      "AND ISNULL(cfcommon_period_tran.isactive, 0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                   "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                   "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                                   "AND hrmembership_master.membershipunkid = @MemId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= ") AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                               "LEFT JOIN ( SELECT  A.Empid " & _
                                                 ", ISNULL(BasicSal, 0) AS BasicSal " & _
                                                 ", ISNULL(GrossPay, 0) AS GrossPay " & _
                                           "FROM    ( SELECT    prpayrollprocess_tran.employeeunkid AS Empid " & _
                                                             ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS BasicSal " & _
                                                     "FROM      prpayrollprocess_tran " & _
                                                               "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                               "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                               "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                               "JOIN prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                                                        "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " "

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                              WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                                "AND cfcommon_period_tran.isactive = 1 " & _
                                                                "AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                                                "AND prpayment_tran.periodunkid = @PeriodId " & _
                                                                "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & " "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                          GROUP BY  prpayrollprocess_tran.employeeunkid, prpayment_tran.expaidrate,prpayment_tran.baseexchangerate " & _
                                                    ") AS A " & _
                                                    "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid AS Empid " & _
                                                                      ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay   " & _
                                                                "FROM    prpayrollprocess_tran " & _
                                                                        "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                                      "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                                        "LEFT  JOIN practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
                                                                                      "AND prpayrollprocess_tran.activityunkid > 0 " & _
                                                                        "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                        "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                        "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                                        "LEFT JOIN prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                                                                    "AND ISNULL(prpayment_tran.isvoid, 0) = 0 AND prpayment_tran.countryunkid <> " & mintCountryId & " "

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	                WHERE ISNULL(prtranhead_master.trnheadtype_id, 1) = 1 " & _
                                               "AND ISNULL(practivity_master.trnheadtype_id, 1) = 1 " & _
                                               "AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                               "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                               "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                               "AND cfcommon_period_tran.isactive = 1 " & _
                                               "AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                                "AND prpayment_tran.periodunkid = @PeriodId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "GROUP BY prpayrollprocess_tran.employeeunkid, prpayment_tran.expaidrate,prpayment_tran.baseexchangerate " & _
                                       ") AS G ON G.Empid = A.Empid " & _
                                 ") AS GrossBasic ON     GrossBasic.Empid    = hremployee_master.employeeunkid " & _
                                 "LEFT JOIN ( SELECT  employeeunkid " & _
                                                  ", SUM(day_type) AS DaysWorked " & _
                                            "FROM    tnalogin_summary " & _
                                            "WHERE   login_date BETWEEN @start_date AND @end_date " & _
                                                    "AND total_hrs <> total_overtime " & _
                                            "GROUP BY employeeunkid " & _
                                          ") AS TableDaysWorked ON hremployee_master.employeeunkid = TableDaysWorked.employeeunkid " & _
                           "WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                   "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                   "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                   "AND hrmembership_master.membershipunkid = @MemId " & _
                                   " AND prpayment_tran.periodunkid = @PeriodId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'StrQ &= "UNION ALL " & _
                '        "SELECT  hremployee_master.employeecode AS employeecode " & _
                '              ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
                '              ", ISNULL(hremployee_master.surname, '') AS surname " & _
                '              ", hremployee_master.employeeunkid AS EmpId " & _
                '              ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
                '              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
                '              ", " & mdecConversionRate & " *  ISNULL(CAmount, 0) AS employercontribution " & _
                '              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0)) AS Amount " & _
                '              ", '' AS Remark " & _
                '              ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                '              ", GrossBasic.BasicSal " & _
                '              ", GrossBasic.GrossPay " & _
                '              ", CASE WHEN ISNULL(CONVERT(CHAR(8), hremployee_master.termination_from_date, 112), " & _
                '                                 "'19000101') BETWEEN @start_date " & _
                '                                             "AND     @end_date THEN 'V' " & _
                '                     "WHEN ISNULL(CONVERT(CHAR(8), hremployee_master.termination_to_date, 112), " & _
                '                                 "'19000101') BETWEEN @start_date " & _
                '                                             "AND     @end_date THEN 'V' " & _
                '                     "WHEN ISNULL(CONVERT(CHAR(8), hremployee_master.empl_enddate, 112), " & _
                '                                 "'19000101') BETWEEN @start_date " & _
                '                                             "AND     @end_date THEN 'V' " & _
                '                     "WHEN CONVERT(CHAR(8), hremployee_master.appointeddate, 112) BETWEEN @start_date " & _
                '                                                                      "AND " & _
                '                                                                      "@end_date " & _
                '                     "THEN 'N' " & _
                '                     "ELSE 'E' " & _
                '                "END AS MemberStatus " & _
                '              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS DaysWorked " & _
                '              ", ISNULL(hremployee_master.jobunkid, 0) AS jobunkid " & _
                '              ", ISNULL(hrjob_master.job_code, '') AS job_code " & _
                '              ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
                '              ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS identity_no "


                StrQ &= "UNION ALL " & _
                        "SELECT  hremployee_master.employeecode AS employeecode " & _
                              ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
                              ", ISNULL(hremployee_master.surname, '') AS surname " & _
                              ", hremployee_master.employeeunkid AS EmpId " & _
                              ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS MemshipNo " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS empcontribution " & _
                              ", " & mdecConversionRate & " *  ISNULL(CAmount, 0) AS employercontribution " & _
                              ", " & mdecConversionRate & " *  (ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) + ISNULL(CAmount, 0)) AS Amount " & _
                              ", '' AS Remark " & _
                              ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                              ", GrossBasic.BasicSal " & _
                              ", GrossBasic.GrossPay " & _
                              ", CASE WHEN ISNULL(CONVERT(CHAR(8), EocDt.date2, 112),'19000101') BETWEEN @start_date AND @end_date THEN 'V' " & _
                              "       WHEN ISNULL(CONVERT(CHAR(8), RT.date1, 112),'19000101') BETWEEN @start_date AND @end_date THEN 'V' " & _
                              "       WHEN ISNULL(CONVERT(CHAR(8), EocDt.date1, 112),'19000101') BETWEEN @start_date AND @end_date THEN 'V' " & _
                              "       WHEN CONVERT(CHAR(8), hremployee_master.appointeddate, 112) BETWEEN @start_date AND @end_date THEN 'N' ELSE 'E' " & _
                              "  END AS MemberStatus " & _
                              ", ISNULL(TableDaysWorked.DaysWorked, 0) AS DaysWorked " & _
                              ", ISNULL(hremployee_master.jobunkid, 0) AS jobunkid " & _
                              ", ISNULL(hrjob_master.job_code, '') AS job_code " & _
                              ", ISNULL(hrjob_master.job_name, '') AS job_name " & _
                              ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS identity_no "
                'S.SANDEEP [04 JUN 2015] -- END

                If mblnFirstNamethenSurname = True Then
                    StrQ &= "  ,  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName "
                Else
                    StrQ &= "  ,  ISNULL(hremployee_master.surname,'')+' '+ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'') AS EName "
                End If

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'StrQ &= "FROM    prpayrollprocess_tran " & _
                '                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
                '                "LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                '                                                    "AND hremployee_idinfo_tran.isdefault = 1 " & _
                '                "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                '                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                '                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                '                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                '                                                 "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                '                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                '                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                '                                                  "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 " & _
                '                "LEFT JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                '                            "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                '                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 "


                StrQ &= "FROM    prpayrollprocess_tran " & _
                                "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         jobunkid " & _
                                "        ,jobgroupunkid " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_categorization_tran " & _
                                "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                                "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = Jobs.jobunkid " & _
                                "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         date1 " & _
                                "        ,date2 " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_dates_tran " & _
                                "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ")AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 " & _
                                "LEFT JOIN " & _
                                "( " & _
                                "    SELECT " & _
                                "         date1 " & _
                                "        ,employeeunkid " & _
                                "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                "    FROM hremployee_dates_tran " & _
                                "    WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                                ")AS RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 " & _
                                "LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                                    "AND hremployee_idinfo_tran.isdefault = 1 " & _
                                "LEFT JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
                                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                 "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.emptranheadunkid " & _
                                "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                  "AND ISNULL(cfcommon_period_tran.isactive, 0) = 1 " & _
                                "LEFT JOIN prpayment_tran ON prpayment_tran.periodunkid = prtnaleave_tran.payperiodunkid " & _
                                            "AND prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid " & _
                                            "AND ISNULL(prpayment_tran.isvoid, 0) = 0 "
                'S.SANDEEP [04 JUN 2015] -- END


                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "        LEFT JOIN ( SELECT  hremployee_master.employeeunkid AS EmpId " & _
                                                  ", ISNULL(CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")), 0) AS CAmount " & _
                                            "FROM    prpayrollprocess_tran " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN hremployee_meminfo_tran ON prpayrollprocess_tran.membershiptranunkid = hremployee_meminfo_tran.membershiptranunkid " & _
                                                                                    "AND hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, " & _
                                                                                      "0) = 0 " & _
                                                    "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                                                                                     "AND prpayrollprocess_tran.tranheadunkid = hrmembership_master.cotranheadunkid " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                                      "AND ISNULL(cfcommon_period_tran.isactive, 0) = 0 "

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                    WHERE   ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                   "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                   "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                                   "AND hrmembership_master.membershipunkid = @MemId "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                  ) AS C ON C.EmpId = hremployee_master.employeeunkid " & _
                               "LEFT JOIN ( SELECT  A.Empid " & _
                                                 ", ISNULL(BasicSal, 0) AS BasicSal " & _
                                                 ", ISNULL(GrossPay, 0) AS GrossPay " & _
                                           "FROM    ( SELECT    prpayrollprocess_tran.employeeunkid AS Empid " & _
                                                             ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36,  " & decDecimalPlaces & "))) AS BasicSal " & _
                                                     "FROM      prpayrollprocess_tran " & _
                                                               "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                               "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                               "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                               "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                               "LEFT JOIN prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                                                        "AND ISNULL(prpayment_tran.isvoid, 0) = 0 "

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "                              WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                                                "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                                                "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                                "AND cfcommon_period_tran.isactive = 1 " & _
                                                                "AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                                                "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & " " & _
                                                                "AND prpayment_tran.periodunkid IS NULL "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "GROUP BY  prpayrollprocess_tran.employeeunkid, prpayment_tran.expaidrate,prpayment_tran.baseexchangerate " & _
                                                    ") AS A " & _
                                                    "LEFT JOIN ( SELECT  prpayrollprocess_tran.employeeunkid AS Empid " & _
                                                                      ", " & mdecConversionRate & " * SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS GrossPay   " & _
                                                                "FROM    prpayrollprocess_tran " & _
                                                                        "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                                                      "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                                        "LEFT  JOIN practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
                                                                                      "AND prpayrollprocess_tran.activityunkid > 0 " & _
                                                                        "LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                                        "LEFT JOIN cfcommon_period_tran ON prtnaleave_tran.payperiodunkid = cfcommon_period_tran.periodunkid " & _
                                                                        "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                                        "LEFT JOIN prpayment_tran ON prtnaleave_tran.employeeunkid = prpayment_tran.employeeunkid AND prtnaleave_tran.payperiodunkid = prpayment_tran.periodunkid " & _
                                                                                    "AND ISNULL(prpayment_tran.isvoid, 0) = 0  "

                'Pinkal (22-APR-2015) -- Start
                'Staturory Report Enhancement - CHANGES IN STATUTORY REPORTS PUT PAYMENT REFERENCE ID IN QUERY
                StrQ &= " AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                             " AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " "
                'Pinkal (22-APR-2015) -- End

                StrQ &= mstrAnalysis_Join

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "	                WHERE ISNULL(prtranhead_master.trnheadtype_id, 1) = 1 " & _
                                               "AND ISNULL(practivity_master.trnheadtype_id, 1) = 1 " & _
                                               "AND ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                               "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                                               "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                               "AND cfcommon_period_tran.isactive = 1 " & _
                                               "AND cfcommon_period_tran.periodunkid = @PeriodId " & _
                                               "AND prpayment_tran.periodunkid IS NULL "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= "              GROUP BY prpayrollprocess_tran.employeeunkid, prpayment_tran.expaidrate,prpayment_tran.baseexchangerate " & _
                                       ") AS G ON G.Empid = A.Empid " & _
                                 ") AS GrossBasic ON     GrossBasic.Empid    = hremployee_master.employeeunkid " & _
                                 "LEFT JOIN ( SELECT  employeeunkid " & _
                                                  ", SUM(day_type) AS DaysWorked " & _
                                            "FROM    tnalogin_summary " & _
                                            "WHERE   login_date BETWEEN @start_date AND @end_date " & _
                                                    "AND total_hrs <> total_overtime " & _
                                            "GROUP BY employeeunkid " & _
                                          ") AS TableDaysWorked ON hremployee_master.employeeunkid = TableDaysWorked.employeeunkid " & _
                           "WHERE ISNULL(prpayrollprocess_tran.isvoid,0) = 0 " & _
                                   "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                                   "AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                   "AND hrmembership_master.membershipunkid = @MemId " & _
                                   " AND prpayment_tran.periodunkid IS NULL "

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                ''Pinkal (15-Oct-2014) -- Start
                ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
                'If UserAccessLevel._AccessLevel.Length > 0 Then
                '    StrQ &= UserAccessLevel._AccessLevelFilterString
                'End If
                ''Pinkal (15-Oct-2014) -- End

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                'S.SANDEEP [04 JUN 2015] -- END

                StrQ &= " ORDER BY employeecode "

            End If


            objDataOperation.AddParameter("@MemId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@start_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriod_Startdate))
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriod_Enddate))


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim iCnt As Integer = 0

            Dim StrTitle As String = ""
            Dim StrAddress As String = ""

            Dim decColumn5Total As Decimal = 0
            Dim decColumn3Total As Decimal = 0
            Dim decColumn10Total As Decimal = 0
            Dim decColumn11Total As Decimal = 0



            'StrTitle = Language.getMessage(mstrModuleName, 1, "THE UNITED REPUBLIC OF TANZANIA") & vbCrLf & _
            '           Language.getMessage(mstrModuleName, 2, "NATIONAL SOCIAL SECURITY FUND") & vbCrLf & _
            '           Language.getMessage(mstrModuleName, 3, "MEMBER'S CONTRIBUTION RECORDS")



            'Dim StrNotice As String = Language.getMessage(mstrModuleName, 22, "Note :") & vbCrLf & _
            '                          Language.getMessage(mstrModuleName, 23, "* To be used for NSSF registerd members and full contribution 20 % should be shown.") & vbCrLf & _
            '                          Language.getMessage(mstrModuleName, 24, "* Each page total must be shown separately.") & vbCrLf & _
            '                          Language.getMessage(mstrModuleName, 25, "* Summary of all page totals must be shown on last page.") & vbCrLf




            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim strFiancialYearName As String = ""
            Dim strCompanyRegistrationValue As String = ""

            Dim objCompany As New clsCompany_Master
            Dim dsFinData As New DataSet
            dsFinData = objCompany.GetFinancialYearList(intCompanyUnkid, , "List", intYearUnkid)
            If dsFinData.Tables(0).Rows.Count > 0 Then
                strFiancialYearName = dsFinData.Tables(0).Rows(0)("financialyear_name")
            End If
            dsFinData.Dispose()
            objCompany._Companyunkid = intCompanyUnkid
            strCompanyRegistrationValue = objCompany._Reg2_Value
            objCompany = Nothing
            'S.SANDEEP [04 JUN 2015] -- END


            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                If dtRow.Item("PeriodName").ToString.Trim.Length <= 0 Then Continue For
                Dim rpt_Row As DataRow
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                iCnt += 1
                rpt_Row.Item("Column1") = iCnt.ToString
                rpt_Row.Item("Column2") = dtRow.Item("EName")
                'If mblnShowBasicSalary = True Then
                '    rpt_Row.Item("Column3") = Format(CDec(dtTemp(0).Item("BasicSal")), GUI.fmtCurrency)
                'Else

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("GrossPay")), GUI.fmtCurrency)
                If IsDBNull(dtRow.Item("GrossPay")) = False Then
                    rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("GrossPay")), GUI.fmtCurrency)
                Else
                    rpt_Row.Item("Column3") = Format(CDec(0), GUI.fmtCurrency)
                End If
                'S.SANDEEP [04 JUN 2015] -- END


                'End If
                rpt_Row.Item("Column4") = dtRow.Item("MemshipNo")
                rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                rpt_Row.Item("Column6") = dtRow.Item("Remark")
                rpt_Row.Item("Column7") = dtRow.Item("PeriodName")
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'rpt_Row.Item("Column8") = FinancialYear._Object._FinancialYear_Name
                rpt_Row.Item("Column8") = IIf(strFiancialYearName = "", FinancialYear._Object._FinancialYear_Name, strFiancialYearName)
                'S.SANDEEP [04 JUN 2015] -- END
                rpt_Row.Item("Column9") = dtRow.Item("employeecode")
                rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("empcontribution")), GUI.fmtCurrency)
                rpt_Row.Item("Column11") = Format(CDec(dtRow.Item("employercontribution")), GUI.fmtCurrency)
                rpt_Row.Item("Column12") = dtRow.Item("identity_no").ToString
                rpt_Row.Item("Column13") = dtRow.Item("surname").ToString
                rpt_Row.Item("Column14") = dtRow.Item("firstname").ToString
                rpt_Row.Item("Column15") = dtRow.Item("MemberStatus").ToString
                rpt_Row.Item("Column16") = "" '<TODO zone code>

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'rpt_Row.Item("Column17") = Company._Object._Reg2_Value 'employer no.
                rpt_Row.Item("Column17") = strCompanyRegistrationValue
                'S.SANDEEP [04 JUN 2015] -- END
                rpt_Row.Item("Column18") = Format(mdtPeriod_Startdate, "yyyyMM")
                rpt_Row.Item("Column19") = "1" 'Data Submission Number
                rpt_Row.Item("Column20") = dtRow.Item("DaysWorked").ToString
                rpt_Row.Item("Column21") = dtRow.Item("job_code").ToString

                decColumn5Total = decColumn5Total + CDec(dtRow.Item("Amount"))
                'If mblnShowBasicSalary = True Then
                '    decColumn3Total = decColumn3Total + CDec(dtTemp(0).Item("BasicSal"))
                'Else

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'decColumn3Total = decColumn3Total + CDec(dtRow.Item("GrossPay"))
                If IsDBNull(dtRow.Item("GrossPay")) = False Then
                    decColumn3Total = decColumn3Total + CDec(dtRow.Item("GrossPay"))
                End If
                'S.SANDEEP [04 JUN 2015] -- END
                'End If
                decColumn10Total = decColumn10Total + CDec(dtRow.Item("empcontribution"))
                decColumn11Total = decColumn11Total + CDec(dtRow.Item("employercontribution"))
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next


            objRpt = New ArutiReport.Designer.rptEPFContributionReport

            objRpt.SetDataSource(rpt_Data)



            Call ReportFunction.TextChange(objRpt, "txtFormNo", Language.getMessage(mstrModuleName, 1, "EPF Contribution"))
            'objRpt.ReportDefinition.ReportObjects("Picture2").ObjectFormat.EnableSuppress = False

            'Call ReportFunction.TextChange(objRpt, "lblEmplrName", Language.getMessage(mstrModuleName, 34, "Employer's Name"))
            'Call ReportFunction.TextChange(objRpt, "txtEmplrName", Company._Object._Name)

            'Dim objCMaster As New clsMasterData
            'Dim dsCountry As New DataSet
            'dsCountry = objCMaster.getCountryList("List", False, Company._Object._Countryunkid)
            'If dsCountry.Tables(0).Rows.Count > 0 Then
            '    StrAddress &= dsCountry.Tables(0).Rows(0)("country_name")
            'End If
            'dsCountry.Dispose()
            'objCMaster = Nothing

            'Dim objState As New clsstate_master
            'Dim objCity As New clscity_master
            'Dim objZipCode As New clszipcode_master

            'objState._Stateunkid = Company._Object._Stateunkid
            'objCity._Cityunkid = Company._Object._Cityunkid
            'objZipCode._Zipcodeunkid = Company._Object._Postalunkid

            'Call ReportFunction.TextChange(objRpt, "lblAddress", Language.getMessage(mstrModuleName, 26, "Address"))
            'Call ReportFunction.TextChange(objRpt, "txtAddress1", Company._Object._Address1 & " " & Company._Object._Address2)
            'Call ReportFunction.TextChange(objRpt, "txtAddress2", StrAddress)
            'Call ReportFunction.TextChange(objRpt, "txtAddress3", objState._Name & " " & objCity._Name & " " & objZipCode._Zipcode_No)
            'objState = Nothing
            'objCity = Nothing
            'objZipCode = Nothing

            Call ReportFunction.TextChange(objRpt, "lblReportTotal", Language.getMessage(mstrModuleName, 2, "Report Total"))
            Call ReportFunction.TextChange(objRpt, "lblMembers", Language.getMessage(mstrModuleName, 3, "Members"))
            Call ReportFunction.TextChange(objRpt, "lblContributions", Language.getMessage(mstrModuleName, 4, "Contributions"))


            Call ReportFunction.TextChange(objRpt, "txtIdentity", Language.getMessage(mstrModuleName, 5, "NIC/Passport Number"))
            Call ReportFunction.TextChange(objRpt, "txtLastName", Language.getMessage(mstrModuleName, 6, "Last Name"))
            Call ReportFunction.TextChange(objRpt, "txtInitials", Language.getMessage(mstrModuleName, 7, "Initials"))
            Call ReportFunction.TextChange(objRpt, "txtMembershipNo", Language.getMessage(mstrModuleName, 8, "Member AC Number"))
            Call ReportFunction.TextChange(objRpt, "txtContribution", Language.getMessage(mstrModuleName, 9, "Total Contribution"))
            Call ReportFunction.TextChange(objRpt, "txtCoContribution", Language.getMessage(mstrModuleName, 10, "Employer’s Contribution "))
            Call ReportFunction.TextChange(objRpt, "txtEmpContribution", Language.getMessage(mstrModuleName, 11, "Member’s Contribution"))

            'If mblnShowBasicSalary = True Then
            '    Call ReportFunction.TextChange(objRpt, "txtWages", Language.getMessage(mstrModuleName, 17, "WAGE"))
            'Else
            Call ReportFunction.TextChange(objRpt, "txtWages", Language.getMessage(mstrModuleName, 12, "Total Earnings "))
            'End If

            Call ReportFunction.TextChange(objRpt, "txtStatus", Language.getMessage(mstrModuleName, 13, "Member Status E=Extg.  N=New V=Vacated "))
            Call ReportFunction.TextChange(objRpt, "txtZoneCode", Language.getMessage(mstrModuleName, 14, "Zone Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmployerNo", Language.getMessage(mstrModuleName, 15, "Employer Number"))
            Call ReportFunction.TextChange(objRpt, "txtContribMonth", Language.getMessage(mstrModuleName, 16, "Contribution Year & Month"))
            Call ReportFunction.TextChange(objRpt, "txtDataNo", Language.getMessage(mstrModuleName, 17, "Data Submission Number"))
            Call ReportFunction.TextChange(objRpt, "txtDaysWorked", Language.getMessage(mstrModuleName, 18, "No of Days Worked"))
            Call ReportFunction.TextChange(objRpt, "txtClassGrade", Language.getMessage(mstrModuleName, 19, "Occupation Classification Grade"))

            Call ReportFunction.TextChange(objRpt, "txtMembers", iCnt.ToString)
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 20, "Total"))

            Call ReportFunction.TextChange(objRpt, "txtWagesTables", Format(decColumn3Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn5Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal2", Format(decColumn5Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal3", Format(decColumn10Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal4", Format(decColumn11Total, GUI.fmtCurrency))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "EPF Contribution")
            Language.setMessage(mstrModuleName, 2, "Report Total")
            Language.setMessage(mstrModuleName, 3, "Members")
            Language.setMessage(mstrModuleName, 4, "Contributions")
            Language.setMessage(mstrModuleName, 5, "NIC/Passport Number")
            Language.setMessage(mstrModuleName, 6, "Last Name")
            Language.setMessage(mstrModuleName, 7, "Initials")
            Language.setMessage(mstrModuleName, 8, "Member AC Number")
            Language.setMessage(mstrModuleName, 9, "Total Contribution")
            Language.setMessage(mstrModuleName, 10, "Employer’s Contribution")
            Language.setMessage(mstrModuleName, 11, "Member’s Contribution")
            Language.setMessage(mstrModuleName, 12, "Total Earnings")
            Language.setMessage(mstrModuleName, 13, "Member Status E=Extg.  N=New V=Vacated")
            Language.setMessage(mstrModuleName, 14, "Zone Code")
            Language.setMessage(mstrModuleName, 15, "Employer Number")
            Language.setMessage(mstrModuleName, 16, "Contribution Year & Month")
            Language.setMessage(mstrModuleName, 17, "Data Submission Number")
            Language.setMessage(mstrModuleName, 18, "No of Days Worked")
            Language.setMessage(mstrModuleName, 19, "Occupation Classification Grade")
            Language.setMessage(mstrModuleName, 20, "Total")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
