'************************************************************************************************************************************
'Class Name : frmWCFFormReport.vb
'Purpose    : 
'Written By : 
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmWCFFormReport
    Private ReadOnly mstrModuleName As String = "frmWCFFormReport"
    Private objWCFForm As clsWCFFormReport
    Private dsPeriods As New DataSet

#Region " Private Variables "

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mdec_ExRate As Decimal = 1
    Private mintPaidCurrencyId As Integer = 0
    'Sohail (02 Jun 2017) -- Start
    'Enhancement - 68.1 - Advance Filter on WCF Form Report.
    Private mstrAdvanceFilter As String = String.Empty
    'Sohail (02 Jun 2017) -- End
#End Region

#Region " Contructor "

    Public Sub New()
        objWCFForm = New clsWCFFormReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        'Sohail (02 Jun 2017) -- Start
        'Enhancement - 68.1 - Advance Filter on WCF Form Report.
        _Show_AdvanceFilter = True
        'Sohail (02 Jun 2017) -- End
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objperiod As New clscommom_period_Tran
        Dim objMember As New clsmembership_master
        Dim objEmpContribution As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Dim objCMaster As New clsCommon_Master 'Hemant (27 Aug 2019)
        Try

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objEmpContribution.getComboList("List", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "List", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            'Sohail (21 Aug 2015) -- End
            With cboTranHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
            End With


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", False)
            'Sohail (08 Jul 2022) -- Start
            'Enhancement : AC2-671 : Tanzania - WCF statutory report enhancement to include NIDA number. Give additional field mapping for NIDA - whose combo should also have Identities. Period selections should be possible for all previous year and current year periods.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", False)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", False)
            Dim mdtPeriod As DataTable = New DataView(dsCombos.Tables(0), "periodunkid = 0 OR (start_date >= '" & eZeeDate.convertDate(CDate("01/Jan/" & Year(CDate(FinancialYear._Object._Database_End_Date).AddYears(-1)))) & "' AND end_date <= '" & eZeeDate.convertDate(CDate("31/Dec/" & Year(CDate(FinancialYear._Object._Database_End_Date)))) & "') ", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (08 Jul 2022) -- End
            'Sohail (21 Aug 2015) -- End
            lvPeriodList.Items.Clear()
            'Sohail (08 Jul 2022) -- Start
            'Enhancement : AC2-671 : Tanzania - WCF statutory report enhancement to include NIDA number. Give additional field mapping for NIDA - whose combo should also have Identities. Period selections should be possible for all previous year and current year periods.
            'If dsCombos.Tables(0).Rows.Count > 0 Then
            If mdtPeriod.Rows.Count > 0 Then
                'Sohail (08 Jul 2022) -- End
                Dim lvItem As ListViewItem
                'Sohail (08 Jul 2022) -- Start
                'Enhancement : AC2-671 : Tanzania - WCF statutory report enhancement to include NIDA number. Give additional field mapping for NIDA - whose combo should also have Identities. Period selections should be possible for all previous year and current year periods.
                'For Each dtRow As DataRow In dsCombos.Tables("List").Rows
                For Each dtRow As DataRow In mdtPeriod.Rows
                    'Sohail (08 Jul 2022) -- End
                    lvItem = New ListViewItem

                    lvItem.Text = ""
                    lvItem.SubItems.Add(dtRow.Item("name").ToString)
                    lvItem.Tag = dtRow.Item("periodunkid")

                    'Sohail (18 Mar 2019) -- Start
                    'TWAWEZA Enhancement - Ref # 0003506 - 74.1 - Integration with WCF online portal on WCF Form statutory report.
                    lvItem.SubItems.Add(dtRow.Item("start_date").ToString)
                    lvItem.SubItems.Add(dtRow.Item("end_date").ToString)
                    'Sohail (18 Mar 2019) -- End

                    'Sohail (08 Jul 2022) -- Start
                    'Enhancement : AC2-671 : Tanzania - WCF statutory report enhancement to include NIDA number. Give additional field mapping for NIDA - whose combo should also have Identities. Period selections should be possible for all previous year and current year periods.
                    lvItem.SubItems(objColhEndDate.Index).Tag = CInt(dtRow.Item("yearunkid"))
                    'Sohail (08 Jul 2022) -- End

                    lvPeriodList.Items.Add(lvItem)

                    lvItem = Nothing
                Next
                If lvPeriodList.Items.Count > 5 Then
                    colhPeriodName.Width = colhPeriodName.Width - 20
                Else
                    colhPeriodName.Width = colhPeriodName.Width
                End If
            End If

            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency")
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombos.Tables("Currency")
            End With
            

            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("countryunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If
            If mintBaseCurrId > 0 Then
                cboCurrency.SelectedValue = mintBaseCurrId
            Else
                If cboCurrency.Items.Count > 0 Then cboCurrency.SelectedIndex = 0
            End If


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objEmpContribution.getComboList("OtherEarning", False, , , enTypeOf.Other_Earnings)
            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", False, , , enTypeOf.Other_Earnings)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Removing other earnings and including all earnings on all statutory reports as per Rutta's request.
            'dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", False, , , , , , "typeof_id = " & enTypeOf.Other_Earnings & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", False, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            'Anjan [26 September 2016] -- End

            'Sohail (16 Mar 2016) -- End
            'Sohail (21 Aug 2015) -- End
            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("OtherEarning")
                .SelectedValue = 0
            End With

            'Hemant (27 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT#4093(HYATT REGENCY DSM) : Mapping Wcf Number with employee identity on EFT WCF report
            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")
            With cboIdentity
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            'Hemant (27 Aug 2019) -- End

            'Sohail (08 Jul 2022) -- Start
            'Enhancement : AC2-671 : Tanzania - WCF statutory report enhancement to include NIDA number. Give additional field mapping for NIDA - whose combo should also have Identities. Period selections should be possible for all previous year and current year periods.
            With cboNIDA
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .SelectedValue = 0
            End With
            'Sohail (08 Jul 2022) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMember = Nothing
            objperiod = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            mintPaidCurrencyId = 0
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            mdec_ExRate = 1
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            cboOtherEarning.SelectedValue = 0
            'Sohail (02 Jun 2017) -- Start
            'Enhancement - 68.1 - Advance Filter on WCF Form Report.
            mstrAdvanceFilter = ""
            'Sohail (02 Jun 2017) -- End
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport
            'Hemant (27 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT#4093(HYATT REGENCY DSM) : Mapping Wcf Number with employee identity on EFT WCF report
            cboIdentity.SelectedValue = 0
            'Hemant (27 Aug 2019) -- End
            'Sohail (08 Jul 2022) -- Start
            'Enhancement : AC2-671 : Tanzania - WCF statutory report enhancement to include NIDA number. Give additional field mapping for NIDA - whose combo should also have Identities. Period selections should be possible for all previous year and current year periods.
            cboNIDA.SelectedValue = 0
            'Sohail (08 Jul 2022) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Dim objCompany As New clsCompany_Master
        Dim arrDatabaseName As New ArrayList
        Dim arrAllDatabaseName As New ArrayList
        Dim intPrevYearID As Integer = 0
        Dim STC_FYYears As New List(Of clsWCFFormReport.STC_FYYear) 'Sohail (08 Jul 2022)
        Try

            If CInt(cboTranHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Transaction Head to continue."), enMsgBoxStyle.Information)
                cboTranHead.Focus()
                Return False
            ElseIf gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select other earning transaction head for Basic Salary."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If

            If lvPeriodList.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select atleast one period to view report."), enMsgBoxStyle.Information)
                lvPeriodList.Focus()
                Return False
            End If

            'Sohail (08 Jul 2022) -- Start
            'Enhancement : AC2-671 : Tanzania - WCF statutory report enhancement to include NIDA number. Give additional field mapping for NIDA - whose combo should also have Identities. Period selections should be possible for all previous year and current year periods.
            If CInt(cboIdentity.SelectedValue) > 0 AndAlso CInt(cboNIDA.SelectedValue) > 0 Then
                If CInt(cboIdentity.SelectedValue) = CInt(cboNIDA.SelectedValue) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, WCF and NIDA membership cannot be same."), enMsgBoxStyle.Information)
                    cboNIDA.Focus()
                    Return False
                End If
            End If
            'Sohail (08 Jul 2022) -- End

            objWCFForm.SetDefaultValue()


            Dim StrPeriodIds As String = String.Empty
            Dim StrPeriodName As String = String.Empty

            
            StrPeriodIds = String.Join(",", (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.Tag.ToString)).ToArray)
            StrPeriodName = String.Join(",", (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(colhPeriodName.Index).Text.ToString)).ToArray)

            If StrPeriodName.Contains("(") = False Then
                StrPeriodName = StrPeriodName.Insert(0, "(")
            End If
            If StrPeriodName.Contains(")") = False Then
                StrPeriodName = StrPeriodName.Insert(StrPeriodName.Length, ")")
            End If

            objWCFForm._Period_Ids = StrPeriodIds
            objWCFForm._Comma_PeriodName = StrPeriodName
            objWCFForm._IngnoreZero = CBool(chkIngnoreZero.CheckState)
            objWCFForm._CurrencyId = CInt(cboCurrency.SelectedValue)
            objWCFForm._CurrencyName = cboCurrency.Text.Trim
            objWCFForm._Ex_Rate = mdec_ExRate
            objWCFForm._TranHeadId = CInt(cboTranHead.SelectedValue)
            objWCFForm._TranHeadName = cboTranHead.Text

            If gbBasicSalaryOtherEarning.Checked = True Then
                objWCFForm._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objWCFForm._OtherEarningTranId = 0
            End If

            'If arrDatabaseName.Count <= 0 Then Return False
            Dim dsDB As DataSet = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", 0)
            Dim a() As String = (From p In dsDB.Tables(0) Select (p.Item("database_name").ToString)).ToArray
            arrAllDatabaseName.AddRange(a)
            If (mintBaseCurrId = mintPaidCurrencyId) OrElse mintPaidCurrencyId <= 0 Then
                mintPaidCurrencyId = mintBaseCurrId
            End If

            objWCFForm._ViewByIds = mstrStringIds
            objWCFForm._ViewIndex = mintViewIdx
            objWCFForm._ViewByName = mstrStringName
            objWCFForm._Analysis_Fields = mstrAnalysis_Fields
            objWCFForm._Analysis_Join = mstrAnalysis_Join
            objWCFForm._Report_GroupName = mstrReport_GroupName
            'Sohail (02 Jun 2017) -- Start
            'Enhancement - 68.1 - Advance Filter on WCF Form Report.
            objWCFForm._Advance_Filter = mstrAdvanceFilter
            'Sohail (02 Jun 2017) -- End

            'Sohail (18 Mar 2019) -- Start
            'TWAWEZA Enhancement - Ref # 0003506 - 74.1 - Integration with WCF online portal on WCF Form statutory report.
            objWCFForm._fmtCurrency = GUI.fmtCurrency
            'Sohail (18 Mar 2019) -- End
            'Hemant (27 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT#4093(HYATT REGENCY DSM) : Mapping Wcf Number with employee identity on EFT WCF report
            objWCFForm._IdentityId = cboIdentity.SelectedValue
            objWCFForm._IdentityName = cboIdentity.Text
            'Hemant (27 Aug 2019) -- End

            'Sohail (08 Jul 2022) -- Start
            'Enhancement : AC2-671 : Tanzania - WCF statutory report enhancement to include NIDA number. Give additional field mapping for NIDA - whose combo should also have Identities. Period selections should be possible for all previous year and current year periods.
            For Each lvItem As ListViewItem In lvPeriodList.CheckedItems
                If STC_FYYears.Exists(Function(x) x.YearUnkid = CInt(lvItem.SubItems(objColhEndDate.Index).Tag)) = False Then
                    Dim dsList As DataSet = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(lvItem.SubItems(objColhEndDate.Index).Tag))
                    If dsList.Tables("Database").Rows.Count > 0 Then
                        STC_FYYears.Add(New clsWCFFormReport.STC_FYYear With {.YearUnkid = CInt(lvItem.SubItems(objColhEndDate.Index).Tag), .DatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString, .EndDate = eZeeDate.convertDate(dsList.Tables("Database").Rows(0).Item("end_date").ToString)})
                    End If
                End If
            Next
            objWCFForm._NIDAId = cboNIDA.SelectedValue
            objWCFForm._NIDAName = cboNIDA.Text
            objWCFForm._STC_FYYear = STC_FYYears
            'Sohail (08 Jul 2022) -- End

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub DoOperation(ByVal blnOpreration As Boolean)
        Try
            For Each lvItem As ListViewItem In lvPeriodList.Items
                lvItem.Checked = blnOpreration
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoOperation", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmWCFFormReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objWCFForm = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWCFFormReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmWCFFormReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)
            Me._Title = objWCFForm._ReportName
            Me._Message = objWCFForm._ReportDesc
            'Hemant (27 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT#4093(HYATT REGENCY DSM) : Mapping Wcf Number with employee identity on EFT WCF report
            Call OtherSettings()
            'Hemant (27 Aug 2019) -- End
            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWCFFormReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmWCFFormReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmWCFFormReport_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmWCFFormReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            clsWCFFormReport.SetMessages()
            objfrm._Other_ModuleNames = "clsWCFFormReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmWCFFormReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub frmWCFFormReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objWCFForm.generateReport(0, e.Type, enExportAction.None)
            'Sohail (18 Mar 2019) -- Start
            'TWAWEZA Enhancement - Ref # 0003506 - 74.1 - Integration with WCF online portal on WCF Form statutory report.
            'objWCFForm.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                             User._Object._Userunkid, _
            '                             FinancialYear._Object._YearUnkid, _
            '                             Company._Object._Companyunkid, _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             ConfigParameter._Object._UserAccessModeSetting, _
            '                             True, ConfigParameter._Object._ExportReportPath, _
            '                             ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, _
            '                             ConfigParameter._Object._Base_CurrencyId)
            Dim strStartDate As String = (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objColhStartDate.Index).Text)).Min()
            Dim strEndDate As String = (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objColhEndDate.Index).Text)).Max()

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objWCFForm._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, strEndDate)
            'Sohail (03 Aug 2019) -- End

            objWCFForm.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         eZeeDate.convertDate(strStartDate), _
                                         eZeeDate.convertDate(strEndDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, _
                                         ConfigParameter._Object._Base_CurrencyId)
            'Sohail (18 Mar 2019) -- End            
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWCFFormReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmWCFFormReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objWCFForm.generateReport(0, enPrintAction.None, e.Type)

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objWCFForm.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                            User._Object._Userunkid, _
            '                            FinancialYear._Object._YearUnkid, _
            '                            Company._Object._Companyunkid, _
            '                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                            ConfigParameter._Object._UserAccessModeSetting, _
            '                            True, ConfigParameter._Object._ExportReportPath, _
            '                            ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            Dim strStartDate As String = (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objColhStartDate.Index).Text)).Min()
            Dim strEndDate As String = (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objColhEndDate.Index).Text)).Max()

            objWCFForm._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, strEndDate)

            objWCFForm.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                        eZeeDate.convertDate(strStartDate), _
                                        eZeeDate.convertDate(strEndDate), _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWCFFormReport_Export_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmWCFFormReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWCFFormReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmWCFFormReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWCFFormReport_Cancel_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'Sohail (02 Jun 2017) -- Start
    'Enhancement - 68.1 - Advance Filter on WCF Form Report.
    Private Sub frmWCFFormReport_AdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.AdvanceFilter_Click
        Dim frm As New frmAdvanceSearch
        Try
            frm._Hr_EmployeeTable_Alias = "hremployee_master"
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmWCFFormReport_AdvanceFilter_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
    'Sohail (02 Jun 2017) -- End
#End Region

#Region " Linkbutton Events "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'Sohail (18 Mar 2019) -- Start
    'TWAWEZA Enhancement - Ref # 0003506 - 74.1 - Integration with WCF online portal on WCF Form statutory report.
    Private Sub lnkEFT_WCF_Export_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkEFT_WCF_Export.LinkClicked
        Try
            If SetFilter() = False Then Exit Try


            Dim strStartDate As String = (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objColhStartDate.Index).Text)).Min()
            Dim strEndDate As String = (From lv In lvPeriodList.CheckedItems.Cast(Of ListViewItem)() Select (lv.SubItems(objColhEndDate.Index).Text)).Max()

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objWCFForm._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, strEndDate)
            'Sohail (03 Aug 2019) -- End

            If objWCFForm.Generate_EFT_WCF_Report(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(strStartDate), eZeeDate.convertDate(strEndDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport) = True Then

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkEFT_WCF_Export_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (18 Mar 2019) -- End
#End Region

#Region " CheckBox Events "

    Private Sub objCheckAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objCheckAll.CheckedChanged
        Try
            Call DoOperation(CBool(objCheckAll.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objCheckAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Listview Events "

    Private Sub lvPeriodList_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriodList.ItemChecked
        Try
            RemoveHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged
            If lvPeriodList.CheckedItems.Count <= 0 Then
                objCheckAll.CheckState = CheckState.Unchecked
            ElseIf lvPeriodList.CheckedItems.Count < lvPeriodList.Items.Count Then
                objCheckAll.CheckState = CheckState.Indeterminate
            ElseIf lvPeriodList.CheckedItems.Count = lvPeriodList.Items.Count Then
                objCheckAll.CheckState = CheckState.Checked
            End If
            AddHandler objCheckAll.CheckedChanged, AddressOf objCheckAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriodList_ItemChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Events "
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged, dtpDate.ValueChanged
        Try
            objlblExRate.Text = ""
            mdecBaseExRate = 0
            mdecPaidExRate = 0
            mdec_ExRate = 1
            If CInt(cboCurrency.SelectedValue) > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim ds As DataSet = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpDate.Value.Date, True)
                If ds.Tables("ExRate").Rows.Count > 0 Then
                    mdecBaseExRate = CDec(ds.Tables("ExRate").Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(ds.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                    mdec_ExRate = CDec(ds.Tables("ExRate").Rows(0).Item("exchange_rate"))
                    objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & ds.Tables("ExRate").Rows(0).Item("currency_sign").ToString & " "
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.chkIngnoreZero.Text = Language._Object.getCaption(Me.chkIngnoreZero.Name, Me.chkIngnoreZero.Text)
			Me.lblPeriodList.Text = Language._Object.getCaption(Me.lblPeriodList.Name, Me.lblPeriodList.Text)
			Me.colhPeriodName.Text = Language._Object.getCaption(CStr(Me.colhPeriodName.Tag), Me.colhPeriodName.Text)
			Me.lblEmployerContribution.Text = Language._Object.getCaption(Me.lblEmployerContribution.Name, Me.lblEmployerContribution.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)
			Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
			Me.lblAsOnDate.Text = Language._Object.getCaption(Me.lblAsOnDate.Name, Me.lblAsOnDate.Text)
			Me.lnkEFT_WCF_Export.Text = Language._Object.getCaption(Me.lnkEFT_WCF_Export.Name, Me.lnkEFT_WCF_Export.Text)
			Me.lblWCF.Text = Language._Object.getCaption(Me.lblWCF.Name, Me.lblWCF.Text)
			Me.lblNIDA.Text = Language._Object.getCaption(Me.lblNIDA.Name, Me.lblNIDA.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Transaction Head to continue.")
			Language.setMessage(mstrModuleName, 2, "Please select other earning transaction head for Basic Salary.")
			Language.setMessage(mstrModuleName, 3, "Please select atleast one period to view report.")
			Language.setMessage(mstrModuleName, 4, "Sorry, WCF and NIDA membership cannot be same.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
