﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNHIFReportTZ
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmNHIFReportTZ))
        Me.gbMandatoryInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkShowReportNameOnReport = New System.Windows.Forms.CheckBox
        Me.chkIgnoreZero = New System.Windows.Forms.CheckBox
        Me.lblIncludeMembership = New System.Windows.Forms.Label
        Me.cboIncludeMembership = New System.Windows.Forms.ComboBox
        Me.LblCurrencyRate = New System.Windows.Forms.Label
        Me.LblCurrency = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.lblMembership = New System.Windows.Forms.Label
        Me.cboMembership = New System.Windows.Forms.ComboBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.chkShowRemark = New System.Windows.Forms.CheckBox
        Me.objbtnAdvanceFilter = New eZee.Common.eZeeLightButton(Me.components)
        Me.lnkEFT_NHIF_Export = New System.Windows.Forms.LinkLabel
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbMandatoryInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 528)
        Me.NavPanel.Size = New System.Drawing.Size(765, 55)
        '
        'gbMandatoryInfo
        '
        Me.gbMandatoryInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.Checked = False
        Me.gbMandatoryInfo.CollapseAllExceptThis = False
        Me.gbMandatoryInfo.CollapsedHoverImage = Nothing
        Me.gbMandatoryInfo.CollapsedNormalImage = Nothing
        Me.gbMandatoryInfo.CollapsedPressedImage = Nothing
        Me.gbMandatoryInfo.CollapseOnLoad = False
        Me.gbMandatoryInfo.Controls.Add(Me.btnSaveSelection)
        Me.gbMandatoryInfo.Controls.Add(Me.chkShowReportNameOnReport)
        Me.gbMandatoryInfo.Controls.Add(Me.chkIgnoreZero)
        Me.gbMandatoryInfo.Controls.Add(Me.lblIncludeMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.cboIncludeMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrencyRate)
        Me.gbMandatoryInfo.Controls.Add(Me.LblCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.cboCurrency)
        Me.gbMandatoryInfo.Controls.Add(Me.lnkSetAnalysis)
        Me.gbMandatoryInfo.Controls.Add(Me.lblMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.cboMembership)
        Me.gbMandatoryInfo.Controls.Add(Me.lblPeriod)
        Me.gbMandatoryInfo.Controls.Add(Me.cboPeriod)
        Me.gbMandatoryInfo.ExpandedHoverImage = Nothing
        Me.gbMandatoryInfo.ExpandedNormalImage = Nothing
        Me.gbMandatoryInfo.ExpandedPressedImage = Nothing
        Me.gbMandatoryInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMandatoryInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMandatoryInfo.HeaderHeight = 25
        Me.gbMandatoryInfo.HeaderMessage = ""
        Me.gbMandatoryInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMandatoryInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMandatoryInfo.HeightOnCollapse = 0
        Me.gbMandatoryInfo.LeftTextSpace = 0
        Me.gbMandatoryInfo.Location = New System.Drawing.Point(9, 66)
        Me.gbMandatoryInfo.Name = "gbMandatoryInfo"
        Me.gbMandatoryInfo.OpenHeight = 300
        Me.gbMandatoryInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMandatoryInfo.ShowBorder = True
        Me.gbMandatoryInfo.ShowCheckBox = False
        Me.gbMandatoryInfo.ShowCollapseButton = False
        Me.gbMandatoryInfo.ShowDefaultBorderColor = True
        Me.gbMandatoryInfo.ShowDownButton = False
        Me.gbMandatoryInfo.ShowHeader = True
        Me.gbMandatoryInfo.Size = New System.Drawing.Size(455, 188)
        Me.gbMandatoryInfo.TabIndex = 68
        Me.gbMandatoryInfo.Temp = 0
        Me.gbMandatoryInfo.Text = "Mandatory Information"
        Me.gbMandatoryInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkShowReportNameOnReport
        '
        Me.chkShowReportNameOnReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowReportNameOnReport.Location = New System.Drawing.Point(123, 165)
        Me.chkShowReportNameOnReport.Name = "chkShowReportNameOnReport"
        Me.chkShowReportNameOnReport.Size = New System.Drawing.Size(314, 17)
        Me.chkShowReportNameOnReport.TabIndex = 92
        Me.chkShowReportNameOnReport.Text = "Show Report Name On Excel Advance Report"
        Me.chkShowReportNameOnReport.UseVisualStyleBackColor = True
        Me.chkShowReportNameOnReport.Visible = False
        '
        'chkIgnoreZero
        '
        Me.chkIgnoreZero.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIgnoreZero.Location = New System.Drawing.Point(123, 119)
        Me.chkIgnoreZero.Name = "chkIgnoreZero"
        Me.chkIgnoreZero.Size = New System.Drawing.Size(193, 17)
        Me.chkIgnoreZero.TabIndex = 90
        Me.chkIgnoreZero.Text = "Ignore Zero"
        Me.chkIgnoreZero.UseVisualStyleBackColor = True
        '
        'lblIncludeMembership
        '
        Me.lblIncludeMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncludeMembership.Location = New System.Drawing.Point(9, 139)
        Me.lblIncludeMembership.Name = "lblIncludeMembership"
        Me.lblIncludeMembership.Size = New System.Drawing.Size(108, 17)
        Me.lblIncludeMembership.TabIndex = 86
        Me.lblIncludeMembership.Text = "Include Membership"
        Me.lblIncludeMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblIncludeMembership.Visible = False
        '
        'cboIncludeMembership
        '
        Me.cboIncludeMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIncludeMembership.DropDownWidth = 230
        Me.cboIncludeMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIncludeMembership.FormattingEnabled = True
        Me.cboIncludeMembership.Location = New System.Drawing.Point(123, 142)
        Me.cboIncludeMembership.Name = "cboIncludeMembership"
        Me.cboIncludeMembership.Size = New System.Drawing.Size(126, 21)
        Me.cboIncludeMembership.TabIndex = 85
        Me.cboIncludeMembership.Visible = False
        '
        'LblCurrencyRate
        '
        Me.LblCurrencyRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrencyRate.Location = New System.Drawing.Point(260, 64)
        Me.LblCurrencyRate.Name = "LblCurrencyRate"
        Me.LblCurrencyRate.Size = New System.Drawing.Size(188, 15)
        Me.LblCurrencyRate.TabIndex = 79
        Me.LblCurrencyRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblCurrency
        '
        Me.LblCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCurrency.Location = New System.Drawing.Point(9, 62)
        Me.LblCurrency.Name = "LblCurrency"
        Me.LblCurrency.Size = New System.Drawing.Size(94, 15)
        Me.LblCurrency.TabIndex = 75
        Me.LblCurrency.Text = "Currency"
        Me.LblCurrency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 180
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(123, 61)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(126, 21)
        Me.cboCurrency.TabIndex = 76
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(357, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 69
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMembership
        '
        Me.lblMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMembership.Location = New System.Drawing.Point(9, 90)
        Me.lblMembership.Name = "lblMembership"
        Me.lblMembership.Size = New System.Drawing.Size(87, 15)
        Me.lblMembership.TabIndex = 64
        Me.lblMembership.Text = "Membership"
        Me.lblMembership.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMembership
        '
        Me.cboMembership.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMembership.DropDownWidth = 180
        Me.cboMembership.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMembership.FormattingEnabled = True
        Me.cboMembership.Location = New System.Drawing.Point(123, 88)
        Me.cboMembership.Name = "cboMembership"
        Me.cboMembership.Size = New System.Drawing.Size(126, 21)
        Me.cboMembership.TabIndex = 65
        '
        'lblPeriod
        '
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(9, 35)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(94, 15)
        Me.lblPeriod.TabIndex = 57
        Me.lblPeriod.Text = "Period"
        Me.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 180
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(123, 33)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(126, 21)
        Me.cboPeriod.TabIndex = 58
        '
        'chkShowRemark
        '
        Me.chkShowRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowRemark.Location = New System.Drawing.Point(132, 295)
        Me.chkShowRemark.Name = "chkShowRemark"
        Me.chkShowRemark.Size = New System.Drawing.Size(193, 17)
        Me.chkShowRemark.TabIndex = 88
        Me.chkShowRemark.Text = "Show Remark"
        Me.chkShowRemark.UseVisualStyleBackColor = True
        Me.chkShowRemark.Visible = False
        '
        'objbtnAdvanceFilter
        '
        Me.objbtnAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnAdvanceFilter.BackColor = System.Drawing.Color.White
        Me.objbtnAdvanceFilter.BackgroundImage = CType(resources.GetObject("objbtnAdvanceFilter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnAdvanceFilter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnAdvanceFilter.BorderColor = System.Drawing.Color.Empty
        Me.objbtnAdvanceFilter.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.objbtnAdvanceFilter.FlatAppearance.BorderSize = 0
        Me.objbtnAdvanceFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnAdvanceFilter.ForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.objbtnAdvanceFilter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Location = New System.Drawing.Point(369, 13)
        Me.objbtnAdvanceFilter.Name = "objbtnAdvanceFilter"
        Me.objbtnAdvanceFilter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnAdvanceFilter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnAdvanceFilter.Size = New System.Drawing.Size(97, 30)
        Me.objbtnAdvanceFilter.TabIndex = 6
        Me.objbtnAdvanceFilter.Text = "Adv. Filter"
        Me.objbtnAdvanceFilter.UseVisualStyleBackColor = False
        '
        'lnkEFT_NHIF_Export
        '
        Me.lnkEFT_NHIF_Export.BackColor = System.Drawing.Color.Transparent
        Me.lnkEFT_NHIF_Export.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkEFT_NHIF_Export.Location = New System.Drawing.Point(129, 266)
        Me.lnkEFT_NHIF_Export.Name = "lnkEFT_NHIF_Export"
        Me.lnkEFT_NHIF_Export.Size = New System.Drawing.Size(314, 17)
        Me.lnkEFT_NHIF_Export.TabIndex = 89
        Me.lnkEFT_NHIF_Export.TabStop = True
        Me.lnkEFT_NHIF_Export.Text = "EFT NHIF Export..."
        Me.lnkEFT_NHIF_Export.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(123, 142)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 111
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'frmNHIFReportTZ
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(765, 583)
        Me.ControlBox = False
        Me.Controls.Add(Me.lnkEFT_NHIF_Export)
        Me.Controls.Add(Me.gbMandatoryInfo)
        Me.Controls.Add(Me.chkShowRemark)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNHIFReportTZ"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Controls.SetChildIndex(Me.chkShowRemark, 0)
        Me.Controls.SetChildIndex(Me.gbMandatoryInfo, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.lnkEFT_NHIF_Export, 0)
        Me.gbMandatoryInfo.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbMandatoryInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblPeriod As System.Windows.Forms.Label
    Friend WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblMembership As System.Windows.Forms.Label
    Friend WithEvents cboMembership As System.Windows.Forms.ComboBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents LblCurrency As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents LblCurrencyRate As System.Windows.Forms.Label
    Friend WithEvents lblIncludeMembership As System.Windows.Forms.Label
    Friend WithEvents cboIncludeMembership As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAdvanceFilter As eZee.Common.eZeeLightButton
    Friend WithEvents chkIgnoreZero As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowReportNameOnReport As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowRemark As System.Windows.Forms.CheckBox
    Friend WithEvents lnkEFT_NHIF_Export As System.Windows.Forms.LinkLabel
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
End Class
