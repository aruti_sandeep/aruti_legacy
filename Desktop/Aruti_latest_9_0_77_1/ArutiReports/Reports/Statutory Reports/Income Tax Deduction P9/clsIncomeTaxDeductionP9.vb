'************************************************************************************************************************************
'Class Name : clsIncomeTaxDeductionP9.vb
'Purpose    :
'Date        :03/02/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text
Imports System.IO

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsIncomeTaxDeductionP9
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsIncomeTaxDeductionP9"
    Private mstrReportId As String = enArutiReport.IncomeTaxDeductionP9  '13
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mintHousingTranId As Integer = 0
    Private mstrStatutoryIds As String = ""
    Private mintReportTypeId As Integer = 0
    Private mstrReportTypeName As String = ""
    Private mstrPeriodIds As String = ""
    Private mintPeriodCount As Integer = -1
    Private mintOtherEarningTranId As Integer = 0 'Sohail (28 Aug 2013)
    Private mblnShowGroupByCostCenter As Boolean = False 'Sohail (30 Nov 2013)
    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIncludeInactiveEmp As Boolean = True
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrPeriodNames As String = String.Empty
    'S.SANDEEP [ 12 MAY 2012 ] -- END

    'S.SANDEEP [ 02 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'S.SANDEEP [ 02 AUG 2012 ] -- END
    Private mstrAnalysis_OrderBy_GroupName As String = "" 'Sohail (30 Nov 2013)

    'S.SANDEEP [ 16 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdtSlabDate As Date
    'S.SANDEEP [ 16 JAN 2013 ] -- END

    Private mblnShowPublicAddressCity As Boolean = True 'Sohail (18 Apr 2014)
#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _HousingTranId() As Integer
        Set(ByVal value As Integer)
            mintHousingTranId = value
        End Set
    End Property

    Public WriteOnly Property _StatutoryIds() As String
        Set(ByVal value As String)
            mstrStatutoryIds = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeId() As Integer
        Set(ByVal value As Integer)
            mintReportTypeId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    'Sohail (28 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property
    'Sohail (28 Aug 2013) -- End

    'Sohail (30 Nov 2013) -- Start
    'Enhancement - Oman
    Public WriteOnly Property _ShowGroupByCostCenter() As Boolean
        Set(ByVal value As Boolean)
            mblnShowGroupByCostCenter = value
        End Set
    End Property
    'Sohail (30 Nov 2013) -- End

    'S.SANDEEP [ 24 JUNE 2011 ] -- START
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property
    'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    'S.SANDEEP [ 12 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property
    'S.SANDEEP [ 12 MAY 2012 ] -- END

    'S.SANDEEP [ 02 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'S.SANDEEP [ 02 AUG 2012 ] -- END

    'Sohail (30 Nov 2013) -- Start
    'Enhancement - Oman
    Public WriteOnly Property _Analysis_OrderBy_GName() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy_GroupName = value
        End Set
    End Property
    'Sohail (30 Nov 2013) -- End

    'S.SANDEEP [ 16 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _PeriodEndDate() As DateTime
        Set(ByVal value As DateTime)
            mdtSlabDate = value
        End Set
    End Property
    'S.SANDEEP [ 16 JAN 2013 ] -- END

    'Sohail (18 Apr 2014) -- Start
    'Enhancement - Show / Hide Address City option on PAYE Detail Report
    Public WriteOnly Property _ShowPublicAddressCity() As Boolean
        Set(ByVal value As Boolean)
            mblnShowPublicAddressCity = value
        End Set
    End Property
    'Sohail (18 Apr 2014) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintEmpId = 0
            mstrEmpName = ""
            mintHousingTranId = 0
            mintOtherEarningTranId = 0 'Sohail (28 Aug 2013)
            mstrStatutoryIds = ""
            mintReportTypeId = 0
            mstrReportTypeName = ""
            mstrPeriodIds = ""
            mstrAnalysis_OrderBy_GroupName = "" 'Sohail (30 Nov 2013)
            'S.SANDEEP [ 24 JUNE 2011 ] -- START
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIncludeInactiveEmp = True
            'S.SANDEEP [ 24 JUNE 2011 ] -- END 
            mblnShowPublicAddressCity = True 'Sohail (18 Apr 2014)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@HousingTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHousingTranId)
            'Me._FilterTitle &= " Employee :  " & mstr 'Sohail (07 Sep 2015)
            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@EmplId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                'Me._FilterQuery &= " AND empid = @EmpId " 'Sohail (23 Apr 2015)
                Me._FilterTitle &= " Employee :  " & mstrEmpName 'Sohail (07 Sep 2015)
            End If

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If
            'Sohail (28 Aug 2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    Select Case pintReportType
        '        Case 0
        '            objRpt = Generate_DetailReport()
        '        Case 1
        '            objRpt = Generate_PAYE_Details()
        '            'Sohail (07 Sep 2015) -- Start
        '            'Enhancement - New Report type P9 Annualization Report in P9 Report.
        '        Case 2
        '            objRpt = Generate_P9_Annualization_Report()
        '            'Sohail (07 Sep 2015) -- End
        '    End Select
        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            Select Case pintReportType
                Case 0
                    objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
                Case 1
                    objRpt = Generate_PAYE_Details(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
                Case 2
                    objRpt = Generate_P9_Annualization_Report(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
            End Select

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Sohail (17 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            'Sohail (17 Aug 2012) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            'ISSUE : INCLUSION OF EMPLOYEE IF HE IS ACTIVE FOR SOME PERIOD BUT NOW INACTIVE
            'E.G.  : EMPLOYEE IS ACTIVE FOR JAN - MAR & TERMINATED IN APR. SO ITS NOT COMING FOR JAN TO MAR IN REPORT.

            'S.SANDEEP [ 22 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ &= "SELECT " & _
            '        "	 Period.Pid " & _
            '        "	,SUM(ISNULL(Period.basicsalary, 0)) AS basicpay " & _
            '        "	,SUM(ISNULL(Period.housing, 0)) AS housing " & _
            '        "	,SUM(ISNULL(Period.allowance, 0)) AS allowancebenefit " & _
            '        "	,SUM(ISNULL(Period.basicsalary, 0)) + SUM(ISNULL(Period.allowance, 0)) + SUM(ISNULL(Period.housing, 0)) AS TotalPay " & _
            '        "	,(SUM(ISNULL(Period.basicsalary, 0)) + SUM(ISNULL(Period.allowance, 0)) + SUM(ISNULL(Period.housing, 0)) ) - SUM(ISNULL(Pension, 0)) AS TaxablePay " & _
            '        "	,SUM(ISNULL(Pension, 0)) AS Pension " & _
            '        "	,SUM(ISNULL(TaxPayable, 0)) AS TaxPayable " & _
            '        "	,SUM(ISNULL(LPR, 0)) AS LPR " & _
            '        "	,SUM(ISNULL(TaxPayable, 0)) - SUM(ISNULL(LPR, 0)) AS NTD " & _
            '        "	,ISNULL(empid, -1) AS empid " & _
            '        "FROM " & _
            '        "( " & _
            '        "	SELECT " & _
            '        "		 ISNULL(vwPayroll.amount,0) AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "		AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,ISNULL(vwPayroll.amount,0) AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	WHERE vwPayroll.tranheadunkid = @HousingTranId " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,SUM(ISNULL(vwPayroll.amount,0)) AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "		AND vwPayroll.typeof_id IN (" & enTypeOf.Allowance & "," & enTypeOf.BENEFIT & ") " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,SUM(ISNULL(vwPayroll.amount,0)) AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	WHERE vwPayroll.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & "  " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,SUM(ISNULL(vwPayroll.amount,0)) AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.typeof_id  = " & enTypeOf.Taxes & "  " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,SUM(ISNULL(vwPayroll.amount,0)) AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
            '        "	WHERE ISNULL(prtranhead_master.istaxrelief,0) = 1 " & _
            '        "		AND vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        ") AS Period " & _
            '        "WHERE 1 = 1 "

            'S.SANDEEP [ 19 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ &= "SELECT " & _
            '        "	 Period.Pid " & _
            '        "	,SUM(ISNULL(Period.basicsalary, 0)) AS basicpay " & _
            '        "	,SUM(ISNULL(Period.housing, 0)) AS housing " & _
            '        "	,SUM(ISNULL(Period.allowance, 0))-(SUM(ISNULL(Period.basicsalary, 0)) + SUM(ISNULL(Period.housing, 0))) AS allowancebenefit " & _
            '        "	,SUM(ISNULL(Period.basicsalary, 0)) + SUM(ISNULL(Period.allowance, 0))-(SUM(ISNULL(Period.basicsalary, 0))+ SUM(ISNULL(Period.housing, 0))) + SUM(ISNULL(Period.housing, 0)) AS TotalPay " & _
            '        "	,(SUM(ISNULL(Period.basicsalary, 0)) + SUM(ISNULL(Period.allowance, 0))-(SUM(ISNULL(Period.basicsalary, 0))+ SUM(ISNULL(Period.housing, 0))) + SUM(ISNULL(Period.housing, 0)) ) - SUM(ISNULL(Pension, 0)) AS TaxablePay " & _
            '        "	,SUM(ISNULL(Pension, 0)) AS Pension " & _
            '        "	,SUM(ISNULL(TaxPayable, 0)) AS TaxPayable " & _
            '        "	,SUM(ISNULL(LPR, 0)) AS LPR " & _
            '        "	,SUM(ISNULL(TaxPayable, 0)) - SUM(ISNULL(LPR, 0)) AS NTD " & _
            '        "	,ISNULL(empid, -1) AS empid " & _
            '        "FROM " & _
            '        "( " & _
            '        "	SELECT " & _
            '        "		 ISNULL(vwPayroll.amount,0) AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "		AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,ISNULL(vwPayroll.amount,0) AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
            '        "	WHERE vwPayroll.tranheadunkid = @HousingTranId " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "		AND prtranhead_master.istaxable = 1 " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,SUM(ISNULL(vwPayroll.amount,0)) AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
            '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "		AND prtranhead_master.istaxable = 1 " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,SUM(ISNULL(vwPayroll.amount,0)) AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	WHERE vwPayroll.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & "  " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,SUM(ISNULL(vwPayroll.amount,0)) AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.typeof_id  = " & enTypeOf.Taxes & "  " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,SUM(ISNULL(vwPayroll.amount,0)) AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
            '        "	WHERE ISNULL(prtranhead_master.istaxrelief,0) = 1 " & _
            '        "		AND vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        ") AS Period " & _
            '        "WHERE 1 = 1 "

            'S.SANDEEP [ 02 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ &= "SELECT " & _
            '        "	 Pid " & _
            '        "	,basicpay AS basicpay " & _
            '        "	,housing AS housing " & _
            '        "	,CASE WHEN allowancebenefit <=0 THEN 0 " & _
            '        "		  WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
            '        "	 ELSE allowancebenefit - (basicpay+housing) END AS allowancebenefit " & _
            '        "	,Pension " & _
            '        "	,TaxPayable " & _
            '        "	,basicpay+housing+(CASE WHEN allowancebenefit <=0 THEN 0 " & _
            '        "		  WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
            '        "	 ELSE allowancebenefit - (basicpay+housing) END) AS TotalPay " & _
            '        "	,(basicpay+housing+(CASE WHEN allowancebenefit <=0 THEN 0 " & _
            '        "		  WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
            '        "	 ELSE allowancebenefit - (basicpay+housing) END))- Pension AS TaxablePay " & _
            '        "	,LPR " & _
            '        "	,TaxPayable - LPR AS NTD " & _
            '        "   ,empid AS empid " & _
            '        "FROM " & _
            '        "( " & _
            '        "SELECT " & _
            '        "	 Period.Pid " & _
            '        "	,SUM(ISNULL(Period.basicsalary, 0)) AS basicpay " & _
            '        "	,SUM(ISNULL(Period.housing, 0)) AS housing " & _
            '        "	,SUM(ISNULL(Period.allowance, 0)) AS allowancebenefit " & _
            '        "	,SUM(ISNULL(Pension, 0)) AS Pension " & _
            '        "	,SUM(ISNULL(TaxPayable, 0)) AS TaxPayable " & _
            '        "	,SUM(ISNULL(LPR, 0)) AS LPR " & _
            '        "	,ISNULL(empid, -1) AS empid " & _
            '        "FROM " & _
            '        "( " & _
            '        "	SELECT " & _
            '        "		 ISNULL(vwPayroll.amount,0) AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "		AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,ISNULL(vwPayroll.amount,0) AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
            '        "	WHERE vwPayroll.tranheadunkid = @HousingTranId " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "		AND prtranhead_master.istaxable = 1 " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,SUM(ISNULL(vwPayroll.amount,0)) AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
            '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "		AND prtranhead_master.istaxable = 1 " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,SUM(ISNULL(vwPayroll.amount,0)) AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	WHERE vwPayroll.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & "  " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,SUM(ISNULL(vwPayroll.amount,0)) AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.typeof_id  = " & enTypeOf.Taxes & "  " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,SUM(ISNULL(vwPayroll.amount,0)) AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll " & _
            '        "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
            '        "	WHERE ISNULL(prtranhead_master.istaxrelief,0) = 1 " & _
            '        "		AND vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        ") AS Period " & _
            '        "WHERE 1 = 1 GROUP BY Period.Pid ,ISNULL(empid, -1) " & _
            '        ") AS A WHERE 1 = 1 "

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'StrQ &= "SELECT " & _
            '        "	 Pid " & _
            '        "	,basicpay AS basicpay " & _
            '        "	,housing AS housing " & _
            '        "	,CASE WHEN allowancebenefit <=0 THEN 0 " & _
            '        "		  WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
            '        "	 ELSE allowancebenefit - (basicpay+housing) END AS allowancebenefit " & _
            '        "	,Pension " & _
            '        "	,TaxPayable " & _
            '        "	,basicpay+housing+(CASE WHEN allowancebenefit <=0 THEN 0 " & _
            '        "		  WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
            '        "	 ELSE allowancebenefit - (basicpay+housing) END) AS TotalPay " & _
            '        "	,(basicpay+housing+(CASE WHEN allowancebenefit <=0 THEN 0 " & _
            '        "		  WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
            '        "	 ELSE allowancebenefit - (basicpay+housing) END))- Pension AS TaxablePay " & _
            '        "	,LPR " & _
            '        "	,TaxPayable - LPR AS NTD " & _
            '        "   ,empid AS empid " & _
            '        "FROM " & _
            '        "( " & _
            '        "SELECT " & _
            '        "	 Period.Pid " & _
            '        "	,SUM(ISNULL(Period.basicsalary, 0)) AS basicpay " & _
            '        "	,SUM(ISNULL(Period.housing, 0)) AS housing " & _
            '        "	,SUM(ISNULL(Period.allowance, 0)) AS allowancebenefit " & _
            '        "	,SUM(ISNULL(Pension, 0)) AS Pension " & _
            '        "	,SUM(ISNULL(TaxPayable, 0)) AS TaxPayable " & _
            '        "	,SUM(ISNULL(LPR, 0)) AS LPR " & _
            '        "	,ISNULL(empid, -1) AS empid " & _
            '        "FROM " & _
            '        "( " & _
            '        "	SELECT " & _
            '        "		 CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '       "	FROM vwPayroll "
            'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join

            'StrQ &= "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "		AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll "
            'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join

            'StrQ &= "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
            '        "	WHERE vwPayroll.tranheadunkid = @HousingTranId " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "		AND prtranhead_master.istaxable = 1 " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,SUM(CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '       "	FROM vwPayroll "
            'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join

            'StrQ &= "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
            '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "		AND prtranhead_master.istaxable = 1 " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,SUM(CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '       "	FROM vwPayroll "
            'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join

            'StrQ &= "	WHERE vwPayroll.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & "  " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,SUM(CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll "
            'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join

            'StrQ &= "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.typeof_id  = " & enTypeOf.Taxes & "  " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,SUM(CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll "
            'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join

            'StrQ &= "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
            '        "	WHERE ISNULL(prtranhead_master.istaxrelief,0) = 1 " & _
            '        "		AND vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        ") AS Period " & _
            '        "WHERE 1 = 1 GROUP BY Period.Pid ,ISNULL(empid, -1) " & _
            '        ") AS A WHERE 1 = 1 "
            'Sohail (28 Aug 2013) -- End

            'StrQ &= "SELECT " & _
            '        "	 Pid " & _
            '        "	,basicpay AS basicpay " & _
            '        "	,housing AS housing " & _
            '        "	,CASE WHEN allowancebenefit <=0 THEN 0 " & _
            '        "		  WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
            '        "	 ELSE allowancebenefit - (basicpay+housing) END AS allowancebenefit " & _
            '        "	,Pension " & _
            '        "	,TaxPayable " & _
            '        "	,basicpay+housing+(CASE WHEN allowancebenefit <=0 THEN 0 " & _
            '        "		  WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
            '        "	 ELSE allowancebenefit - (basicpay+housing) END) AS TotalPay " & _
            '        "	,(basicpay+housing+(CASE WHEN allowancebenefit <=0 THEN 0 " & _
            '        "		  WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
            '        "	 ELSE allowancebenefit - (basicpay+housing) END))- Pension AS TaxablePay " & _
            '        "	,LPR " & _
            '        "	,TaxPayable - LPR AS NTD " & _
            '        "   ,empid AS empid " & _
            '        "FROM " & _
            '        "( " & _
            '        "SELECT " & _
            '        "	 Period.Pid " & _
            '        "	,SUM(ISNULL(Period.basicsalary, 0)) AS basicpay " & _
            '        "	,SUM(ISNULL(Period.housing, 0)) AS housing " & _
            '        "	,SUM(ISNULL(Period.allowance, 0)) AS allowancebenefit " & _
            '        "	,SUM(ISNULL(Pension, 0)) AS Pension " & _
            '        "	,SUM(ISNULL(TaxPayable, 0)) AS TaxPayable " & _
            '        "	,SUM(ISNULL(LPR, 0)) AS LPR " & _
            '        "	,ISNULL(empid, -1) AS empid " & _
            '        "FROM " & _
            '        "( " & _
            '        "	SELECT " & _
            '        "		 ISNULL(vwPayroll.amount, 0) AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '       "	FROM vwPayroll "
            'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join

            'StrQ &= "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "		AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,ISNULL(vwPayroll.amount, 0) AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll "
            'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join

            'StrQ &= "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
            '        "	WHERE vwPayroll.tranheadunkid = @HousingTranId " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "		AND prtranhead_master.istaxable = 1 " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,SUM(ISNULL(vwPayroll.amount, 0)) AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '       "	FROM vwPayroll "
            'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join

            'StrQ &= "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
            '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '        "		AND prtranhead_master.istaxable = 1 " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,SUM(ISNULL(vwPayroll.amount, 0)) AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '       "	FROM vwPayroll "
            'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join

            'StrQ &= "	WHERE vwPayroll.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
            '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & "  " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,SUM(ISNULL(vwPayroll.amount, 0)) AS TaxPayable " & _
            '        "		,0 AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll "
            'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join

            'StrQ &= "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.typeof_id  = " & enTypeOf.Taxes & "  " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        "UNION ALL " & _
            '        "	SELECT " & _
            '        "		 0 AS basicsalary " & _
            '        "		,0 AS housing " & _
            '        "		,0 AS allowance " & _
            '        "		,0 AS Pension " & _
            '        "		,0 AS TaxPayable " & _
            '        "		,SUM(ISNULL(vwPayroll.amount, 0)) AS LPR " & _
            '        "		,vwPayroll.employeeunkid AS empid " & _
            '        "		,vwPayroll.payperiodunkid AS Pid " & _
            '        "	FROM vwPayroll "
            'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join

            'StrQ &= "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
            '        "	WHERE ISNULL(prtranhead_master.istaxrelief,0) = 1 " & _
            '        "		AND vwPayroll.tranheadunkid NOT IN (-1) " & _
            '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '        "	GROUP BY vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '        ") AS Period " & _
            '        "WHERE 1 = 1 GROUP BY Period.Pid ,ISNULL(empid, -1) " & _
            '        ") AS A WHERE 1 = 1 "

            'Sohail (28 Aug 2013) -- Start
            'TRA - ENHANCEMENT
            'Sohail (04 Sep 2013) -- Start
            'TRA - ENHANCEMENT - Changed the logic of Allowance as per Rutta's Comment discussed with Sandy. Don't calculate Basic and Housing in Allowance
            'StrQ &= "SELECT  Pid " & _
            '              ", basicpay AS basicpay " & _
            '              ", housing AS housing " & _
            '              ", CASE WHEN allowancebenefit <=0 THEN 0 " & _
            '                      "WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
            '                      "ELSE allowancebenefit - (basicpay+housing) END AS allowancebenefit " & _
            '              ", Pension " & _
            '              ", TaxPayable " & _
            '              ", basicpay + housing + (CASE WHEN allowancebenefit <=0 THEN 0 " & _
            '                                           "WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
            '                                           "ELSE allowancebenefit - (basicpay+housing) END) AS TotalPay " & _
            '              ", (basicpay + housing + (CASE WHEN allowancebenefit <=0 THEN 0 " & _
            '                                            "WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
            '              "ELSE allowancebenefit - (basicpay+housing) END))- Pension AS TaxablePay " & _
            '              ", LPR " & _
            '              ", TaxPayable - LPR AS NTD " & _
            '              ", empid AS empid " & _
            '        "FROM    ( SELECT    Period.Pid " & _
            '                          ", SUM(ISNULL(Period.basicsalary, 0)) AS basicpay " & _
            '                          ", SUM(ISNULL(Period.housing, 0)) AS housing " & _
            '                          ", SUM(ISNULL(Period.allowance, 0)) AS allowancebenefit " & _
            '                          ", SUM(ISNULL(Pension, 0)) AS Pension " & _
            '                          ", SUM(ISNULL(TaxPayable, 0)) AS TaxPayable " & _
            '                          ", SUM(ISNULL(LPR, 0)) AS LPR " & _
            '                          ", ISNULL(empid, -1) AS empid " & _
            '                  "FROM      ( SELECT    CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
            '                                      ", 0 AS housing " & _
            '                                      ", 0 AS allowance " & _
            '                                      ", 0 AS Pension " & _
            '                                      ", 0 AS TaxPayable " & _
            '                                      ", 0 AS LPR " & _
            '                                      ", vwPayroll.employeeunkid AS empid " & _
            '                                      ", vwPayroll.payperiodunkid AS Pid " & _
            '                              "FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            'mstrAnalysis_Join
            StrQ &= "SELECT  Pid " & _
                          ", basicpay AS basicpay " & _
                          ", housing AS housing " & _
                          ", allowancebenefit AS allowancebenefit " & _
                          ", Pension " & _
                          ", TaxPayable " & _
                          ", basicpay + housing + ( allowancebenefit ) AS TotalPay " & _
                          ", (basicpay + housing + ( allowancebenefit ))- Pension AS TaxablePay " & _
                          ", LPR " & _
                          ", TaxPayable - LPR AS NTD " & _
                          ", empid AS empid " & _
                    "FROM    ( SELECT    Period.Pid " & _
                                      ", SUM(ISNULL(Period.basicsalary, 0)) AS basicpay " & _
                                      ", SUM(ISNULL(Period.housing, 0)) AS housing " & _
                                      ", SUM(ISNULL(Period.allowance, 0)) AS allowancebenefit " & _
                                      ", SUM(ISNULL(Pension, 0)) AS Pension " & _
                                      ", SUM(ISNULL(TaxPayable, 0)) AS TaxPayable " & _
                                      ", SUM(ISNULL(LPR, 0)) AS LPR " & _
                                      ", ISNULL(empid, -1) AS empid " & _
                              "FROM      ( SELECT    CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
                                                  ", 0 AS housing " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxPayable " & _
                                                  ", 0 AS LPR " & _
                                                  ", vwPayroll.employeeunkid AS empid " & _
                                                  ", vwPayroll.payperiodunkid AS Pid " & _
                                          "FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            mstrAnalysis_Join
            'Sohail (04 Sep 2013) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	                   WHERE     vwPayroll.tranheadunkid NOT IN (-1) " & _
                                                    "AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
                                                    "AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") "
            'Sohail (01 Feb 2018) - [AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ")]

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND vwPayroll.tranheadunkid = @OtherEarningTranId  "
            Else
                StrQ &= "AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  "
            End If

            'Sohail (01 Feb 2018) -- Start
            'Aga Khan issue # 0001967: Some employees are missing on Income Tax Deduction card P9 Report in 70.1.
            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If
            'Sohail (01 Feb 2018) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS housing " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxPayable " & _
                                                  ", 0 AS LPR " & _
                                                  ", vwPayroll.employeeunkid AS empid " & _
                                                  ", vwPayroll.payperiodunkid AS Pid " & _
                                          "FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
             mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE     vwPayroll.tranheadunkid = @HousingTranId " & _
                                                    "AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
                                                    "AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
                                                    "AND prtranhead_master.istaxable = 1 " & _
                                                    " AND vwPayroll.GroupID IN (1, 2, 3, 4) " & _
                                                    "AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") "
            'Hemant (25 Feb 2019) -- Start
            'Issue LONAGRO#3511: Incorrect Tax Due calculated in P9 report.
            '" AND vwPayroll.GroupID IN (1, 2, 3, 4) " & _
            'Hemant (25 Feb 2019) -- End
            'Sohail (01 Feb 2018) - [AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ")]

            'Sohail (01 Feb 2018) -- Start
            'Aga Khan issue # 0001967: Some employees are missing on Income Tax Deduction card P9 Report in 70.1.
            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If
            'Sohail (01 Feb 2018) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= "                  UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS housing " & _
                                                  ", SUM(CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS allowance " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxPayable " & _
                                                  ", 0 AS LPR " & _
                                                  ", vwPayroll.employeeunkid AS empid " & _
                                                  ", vwPayroll.payperiodunkid AS Pid " & _
                                          "FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
             mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (04 Sep 2013) -- Start
            'TRA - ENHANCEMENT - Changed the logic of Allowance as per Rutta's Comment discussed with Sandy. Don't calculate Basic and Housing in Allowance
            'StrQ &= "	                   WHERE     vwPayroll.tranheadunkid NOT IN (-1) " & _
            '                                        "AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
            '                                        "AND prtranhead_master.istaxable = 1 " & _
            '                                        "AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '                              "GROUP BY  vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
            '                              "UNION ALL " & _
            '                              "SELECT    0 AS basicsalary " & _
            '                                      ", 0 AS housing " & _
            '                                      ", 0 AS allowance " & _
            '                                      ", SUM(CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Pension " & _
            '                                      ", 0 AS TaxPayable " & _
            '                                      ", 0 AS LPR " & _
            '                                      ", vwPayroll.employeeunkid AS empid " & _
            '                                      ", vwPayroll.payperiodunkid AS Pid " & _
            '                              "FROM      vwPayroll " & _
            '                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            ' mstrAnalysis_Join
            StrQ &= "	                   WHERE     vwPayroll.tranheadunkid NOT IN (-1) " & _
                                                    "AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
                                                    "AND prtranhead_master.istaxable = 1 " & _
                                                    " AND vwPayroll.GroupID IN (1, 2, 3, 4) " & _
                                                    "AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
                                                    "AND NOT (vwPayroll.tranheadunkid = @HousingTranId AND istaxable = 1) " & _
                                                    "AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") "
            'Hemant (25 Feb 2019) -- Start
            'Issue LONAGRO#3511: Incorrect Tax Due calculated in P9 report.
            '" AND vwPayroll.GroupID IN (1, 2, 3, 4) " & _
            'Hemant (25 Feb 2019) -- End
            'Sohail (01 Feb 2018) - [AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ")]

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND NOT ( vwPayroll.tranheadunkid = @OtherEarningTranId AND istaxable = 1 ) "
            Else
                StrQ &= "AND NOT ( vwPayroll.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) "
            End If

            'Sohail (01 Feb 2018) -- Start
            'Aga Khan issue # 0001967: Some employees are missing on Income Tax Deduction card P9 Report in 70.1.
            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If
            'Sohail (01 Feb 2018) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "GROUP BY  vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS housing " & _
                                                  ", 0 AS allowance " & _
                                                  ", SUM(CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Pension " & _
                                                  ", 0 AS TaxPayable " & _
                                                  ", 0 AS LPR " & _
                                                  ", vwPayroll.employeeunkid AS empid " & _
                                                  ", vwPayroll.payperiodunkid AS Pid " & _
                                          "FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
             mstrAnalysis_Join
            'Sohail (04 Sep 2013) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	                   WHERE     vwPayroll.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
                                                    "AND vwPayroll.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & "  " & _
                                                    "AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
                                                    "AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") "
            'Sohail (01 Feb 2018) - [AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ")]

            'Sohail (01 Feb 2018) -- Start
            'Aga Khan issue # 0001967: Some employees are missing on Income Tax Deduction card P9 Report in 70.1.
            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If
            'Sohail (01 Feb 2018) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                  GROUP BY  vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS housing " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS Pension " & _
                                                  ", SUM(CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS TaxPayable " & _
                                                  ", 0 AS LPR " & _
                                                  ", vwPayroll.employeeunkid AS empid " & _
                                                  ", vwPayroll.payperiodunkid AS Pid " & _
                                          "FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
             mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	                   WHERE     vwPayroll.tranheadunkid NOT IN (-1) " & _
                                                    "AND vwPayroll.typeof_id  = " & enTypeOf.Taxes & "  " & _
                                                    "AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") "
            'Sohail (01 Feb 2018) - [AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ")]

            'Sohail (01 Feb 2018) -- Start
            'Aga Khan issue # 0001967: Some employees are missing on Income Tax Deduction card P9 Report in 70.1.
            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If
            'Sohail (01 Feb 2018) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                  GROUP BY  vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS housing " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxPayable " & _
                                                  ", SUM(CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS LPR " & _
                                                  ", vwPayroll.employeeunkid AS empid " & _
                                                  ", vwPayroll.payperiodunkid AS Pid " & _
                                          "FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
             mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	                   WHERE     ISNULL(prtranhead_master.istaxrelief,0) = 1 " & _
                                                    "AND vwPayroll.tranheadunkid NOT IN (-1) " & _
                                                    " AND vwPayroll.GroupID IN (1, 2, 3, 4) " & _
                                                    "AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
                                                    "AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") "
            'Hemant (25 Feb 2019) -- Start
            'Issue LONAGRO#3511: Incorrect Tax Due calculated in P9 report.
            '" AND vwPayroll.GroupID IN (1, 2, 3, 4) " & _
            'Hemant (25 Feb 2019) -- End
            'Sohail (01 Feb 2018) - [AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ")]

            'Sohail (01 Feb 2018) -- Start
            'Aga Khan issue # 0001967: Some employees are missing on Income Tax Deduction card P9 Report in 70.1.
            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If
            'Sohail (01 Feb 2018) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                     GROUP BY  vwPayroll.employeeunkid,vwPayroll.payperiodunkid " & _
                                        ") AS Period " & _
                              "WHERE     1 = 1 GROUP BY Period.Pid ,ISNULL(empid, -1) " & _
                        ") AS A WHERE 1 = 1 "
            'Sohail (28 Aug 2013) -- End

            'S.SANDEEP [ 02 AUG 2012 ] -- END


            'S.SANDEEP [ 19 JUNE 2012 ] -- END


            'S.SANDEEP [ 22 JUNE 2012 ] -- END



            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            'S.SANDEEP [ 19 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ &= "GROUP BY Period.Pid ,ISNULL(empid, -1) "

            'S.SANDEEP [ 19 JUNE 2012 ] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtBlankRow() As DataRow = dsList.Tables(0).Select("basicpay = 0 AND housing = 0 AND allowancebenefit = 0 AND TotalPay = 0 AND Pension = 0 AND TaxablePay = 0 AND TaxPayable = 0 AND LPR = 0 AND NTD = 0")
            Dim StrKeyId As String = String.Empty
            If dtBlankRow.Length > 0 Then
                'Sohail (01 Feb 2018) -- Start
                'Aga Khan issue # 0001967: Some employees are missing on Income Tax Deduction card P9 Report in 70.1.
                Dim arr As List(Of String) = (From p In dtBlankRow Select (p.Field(Of Integer)("empid").ToString())).Distinct().ToList
                Dim arrFinal As New List(Of String)
                'Sohail (01 Feb 2018) -- End
                For i As Integer = 0 To dtBlankRow.Length - 1
                    'Sohail (01 Feb 2018) -- Start
                    'Aga Khan issue # 0001967: Some employees are missing on Income Tax Deduction card P9 Report in 70.1.
                    'StrKeyId &= "," & dtBlankRow(i).Item("empid")
                    'Sohail (01 Feb 2018) -- End
                    dsList.Tables(0).Rows.Remove(dtBlankRow(i))
                Next
                dsList.Tables(0).AcceptChanges()
                'Sohail (01 Feb 2018) -- Start
                'Aga Khan issue # 0001967: Some employees are missing on Income Tax Deduction card P9 Report in 70.1.
                For Each strID As String In arr
                    If dsList.Tables(0).Select("empid = " & CInt(strID) & " ").Length > 0 Then
                        'Do nothing
                    Else
                        arrFinal.Add(strID)
                    End If
                Next
                StrKeyId = String.Join(",", arrFinal.ToArray)
                'Sohail (01 Feb 2018) -- End
            End If

            'Sohail (01 Feb 2018) -- Start
            'Aga Khan issue # 0001967: Some employees are missing on Income Tax Deduction card P9 Report in 70.1.
            'If StrKeyId.Length > 0 Then
            '    StrKeyId = Mid(StrKeyId, 2)
            'End If
            'Sohail (01 Feb 2018) -- End

            Dim dsEmployee As DataSet = Nothing
            objDataOperation.ClearParameters() 'Sohail (31 Aug 2015) - [Issue : The variable name @EmplId has already been declared] 

            'S.SANDEEP [ 02 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT " & _
            '       "	 Emp.EId " & _
            '       "	,Emp.Code " & _
            '       "	,Emp.MainName " & _
            '       "	,Emp.OtherName " & _
            '       "FROM " & _
            '       "( " & _
            '       "	SELECT " & _
            '       "		 employeeunkid AS EId " & _
            '       "		,employeecode AS Code " & _
            '       "		,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS MainName " & _
            '       "		,ISNULL(othername,'') AS OtherName " & _
            '       "	FROM hremployee_master " & _
            '       "	WHERE 1 = 1 " & _
            '       ") AS Emp WHERE 1 = 1 "

            StrQ = "SELECT " & _
                    "	 Emp.EId " & _
                    "	,Emp.Code " & _
                    "	,Emp.MainName " & _
                    "	,Emp.OtherName " & _
                    "FROM " & _
                    "( " & _
                    "	SELECT " & _
                    "		 hremployee_master.employeeunkid AS EId " & _
                    "		,employeecode AS Code " & _
                    "		,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS MainName " & _
                    "		,ISNULL(othername,'') AS OtherName " & _
                   "	FROM hremployee_master "
            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	WHERE 1 = 1 "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= ") AS Emp WHERE 1 = 1 "
            'S.SANDEEP [ 02 AUG 2012 ] -- END

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@EmplId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                StrQ &= "AND EId = @EmplId "
            End If

            If StrKeyId.Length > 0 Then
                StrQ &= "AND EId NOT IN(" & StrKeyId & ")"
            End If

            dsEmployee = objDataOperation.ExecQuery(StrQ, "Emp")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsPeriod As DataSet = Nothing
            Dim objPeriod As New clscommom_period_Tran

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsPeriod = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            dsPeriod = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
            'Sohail (21 Aug 2015) -- End

            If mstrPeriodIds.Length > 0 Then
                Dim dtTemp() As DataRow = Nothing
                dtTemp = dsPeriod.Tables(0).Select("periodunkid NOT IN (" & mstrPeriodIds & ")")
                For i As Integer = 0 To dtTemp.Length - 1
                    dsPeriod.Tables(0).Rows.Remove(dtTemp(i))
                    dsPeriod.Tables(0).AcceptChanges()
                Next
            End If

            mintPeriodCount = dsPeriod.Tables(0).Rows.Count

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim intCnt As Integer = 1
            Dim rpt_Row As DataRow = Nothing

            Dim dicTaxableTotalFirst6Month As New Dictionary(Of Integer, Decimal)  'EmployeeId,Total For First Six Month
            Dim dicTaxableTotalNextt6Month As New Dictionary(Of Integer, Decimal)  'EmployeeId,Total For Next Six Month
            Dim dicDivideByFirst6Month As New Dictionary(Of Integer, Integer)   'EmployeeId, DivideValue For First Six Month
            Dim dicDivideByNext6Month As New Dictionary(Of Integer, Integer)   'EmployeeId, DivideValue For Next Six Month
            Dim intCntSixMonth As Integer = 1

            'S.SANDEEP [ 22 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim blnFlag As Boolean = False
            'S.SANDEEP [ 22 JUNE 2012 ] -- END


            Dim decColumn2GrpTotal, decColumn3GrpTotal, decColumn4GrpTotal, decColumn5GrpTotal, decColumn6GrpTotal, decColumn7GrpTotal, decColumn8GrpTotal, decColumn9GrpTotal, decColumn10GrpTotal As Decimal

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim intMonthId As Integer = 0
            'S.SANDEEP [ 16 JAN 2013 ] -- END

            For Each dtERow As DataRow In dsEmployee.Tables(0).Rows
                intCnt = 1
                intCntSixMonth = 1
                'S.SANDEEP [ 22 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                blnFlag = False
                'S.SANDEEP [ 22 JUNE 2012 ] -- END
                decColumn2GrpTotal = 0 : decColumn3GrpTotal = 0 : decColumn4GrpTotal = 0 : decColumn5GrpTotal = 0 : decColumn6GrpTotal = 0 : decColumn7GrpTotal = 0 : decColumn8GrpTotal = 0 : decColumn9GrpTotal = 0 : decColumn10GrpTotal = 0
                For Each dtPRow As DataRow In dsPeriod.Tables(0).Rows

                    'S.SANDEEP [ 16 JAN 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    If intMonthId <= 0 Then intMonthId = eZeeDate.convertDate(dtPRow.Item("start_date").ToString).Date.Month
                    'S.SANDEEP [ 16 JAN 2013 ] -- END

                    Dim dtTempRow() As DataRow
                    dtTempRow = dsList.Tables(0).Select("Empid = '" & dtERow.Item("EId") & "' AND Pid = '" & dtPRow.Item("periodunkid") & "'")
                    If dtTempRow.Length > 0 Then
                        rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                        rpt_Row.Item("Column1") = dtPRow.Item("name")
                        rpt_Row.Item("Column2") = Format(CDec(dtTempRow(0).Item("basicpay")), GUI.fmtCurrency)
                        rpt_Row.Item("Column3") = Format(CDec(dtTempRow(0).Item("housing")), GUI.fmtCurrency)
                        rpt_Row.Item("Column4") = Format(CDec(dtTempRow(0).Item("allowancebenefit")), GUI.fmtCurrency)
                        rpt_Row.Item("Column5") = Format(CDec(dtTempRow(0).Item("TotalPay")), GUI.fmtCurrency)
                        rpt_Row.Item("Column6") = Format(CDec(dtTempRow(0).Item("Pension")), GUI.fmtCurrency)
                        rpt_Row.Item("Column7") = Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
                        rpt_Row.Item("Column8") = Format(CDec(dtTempRow(0).Item("TaxPayable")), GUI.fmtCurrency)
                        rpt_Row.Item("Column9") = Format(CDec(dtTempRow(0).Item("LPR")), GUI.fmtCurrency)
                        rpt_Row.Item("Column10") = Format(CDec(dtTempRow(0).Item("NTD")), GUI.fmtCurrency)
                        rpt_Row.Item("Column11") = Company._Object._Name
                        rpt_Row.Item("Column12") = Company._Object._Address1
                        rpt_Row.Item("Column13") = Company._Object._Address2
                        rpt_Row.Item("Column14") = dtERow.Item("EId")
                        rpt_Row.Item("Column15") = dtERow.Item("OtherName")
                        rpt_Row.Item("Column16") = dtERow.Item("Code")
                        rpt_Row.Item("Column17") = Company._Object._Tinno
                        rpt_Row.Item("Column18") = "" 'Left Blank
                        rpt_Row.Item("Column19") = dtERow.Item("MainName")

                        'S.SANDEEP [ 05 JULY 2011 ] -- START
                        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
                        decColumn2GrpTotal = decColumn2GrpTotal + CDec(dtTempRow(0).Item("basicpay"))
                        decColumn3GrpTotal = decColumn3GrpTotal + CDec(dtTempRow(0).Item("housing"))
                        decColumn4GrpTotal = decColumn4GrpTotal + CDec(dtTempRow(0).Item("allowancebenefit"))
                        decColumn5GrpTotal = decColumn5GrpTotal + CDec(dtTempRow(0).Item("TotalPay"))
                        decColumn6GrpTotal = decColumn6GrpTotal + CDec(dtTempRow(0).Item("Pension"))
                        decColumn7GrpTotal = decColumn7GrpTotal + CDec(dtTempRow(0).Item("TaxablePay"))
                        decColumn8GrpTotal = decColumn8GrpTotal + CDec(dtTempRow(0).Item("TaxPayable"))
                        decColumn9GrpTotal = decColumn9GrpTotal + CDec(dtTempRow(0).Item("LPR"))
                        decColumn10GrpTotal = decColumn10GrpTotal + CDec(dtTempRow(0).Item("NTD"))
                        'S.SANDEEP [ 05 JULY 2011 ] -- END 



                        '/*************** SETTING DIVIDE EMPLOYEE WISE ***************/

                        'S.SANDEEP [ 16 JAN 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'If intCntSixMonth <= 6 Then
                        '    If dicDivideByFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                        '        dicDivideByFirst6Month(dtERow.Item("EId")) += 1
                        '    Else
                        '        dicDivideByFirst6Month.Add(dtERow.Item("EId"), 1)
                        '    End If

                        '    If dicTaxableTotalFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                        '        dicTaxableTotalFirst6Month(dtERow.Item("EId")) += Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
                        '    Else
                        '        dicTaxableTotalFirst6Month.Add(dtERow.Item("EId"), Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency))
                        '    End If
                        '    intCntSixMonth += 1
                        'ElseIf intCntSixMonth >= 7 Then
                        '    If dicDivideByNext6Month.ContainsKey(dtERow.Item("EId")) Then
                        '        dicDivideByNext6Month(dtERow.Item("EId")) += 1
                        '    Else
                        '        dicDivideByNext6Month.Add(dtERow.Item("EId"), 1)
                        '    End If

                        '    If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
                        '        dicTaxableTotalNextt6Month(dtERow.Item("EId")) += Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
                        '    Else
                        '        dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency))
                        '    End If
                        '    intCntSixMonth += 1
                        'End If
                        If mintPeriodCount = 12 Then
                            If intCntSixMonth <= 6 Then
                                If dicDivideByFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByFirst6Month(dtERow.Item("EId")) += 1
                                Else
                                    dicDivideByFirst6Month.Add(dtERow.Item("EId"), 1)
                                End If

                                If dicTaxableTotalFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalFirst6Month(dtERow.Item("EId")) += Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
                                Else
                                    dicTaxableTotalFirst6Month.Add(dtERow.Item("EId"), Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency))
                                End If
                                intCntSixMonth += 1
                            ElseIf intCntSixMonth >= 7 Then
                                If dicDivideByNext6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByNext6Month(dtERow.Item("EId")) += 1
                                Else
                                    dicDivideByNext6Month.Add(dtERow.Item("EId"), 1)
                                End If

                                If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalNextt6Month(dtERow.Item("EId")) += Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
                                Else
                                    dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency))
                                End If
                                intCntSixMonth += 1
                            End If
                        ElseIf mintPeriodCount < 12 Then
                            If intMonthId <= 6 Then
                                If dicDivideByFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByFirst6Month(dtERow.Item("EId")) += 1
                                Else
                                    dicDivideByFirst6Month.Add(dtERow.Item("EId"), 1)
                                End If

                                If dicTaxableTotalFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalFirst6Month(dtERow.Item("EId")) += Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
                                Else
                                    dicTaxableTotalFirst6Month.Add(dtERow.Item("EId"), Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency))
                                End If
                            ElseIf intMonthId >= 7 Then
                                If dicDivideByNext6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByNext6Month(dtERow.Item("EId")) += 1
                                Else
                                    dicDivideByNext6Month.Add(dtERow.Item("EId"), 1)
                                End If

                                'If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
                                'dicTaxableTotalNextt6Month(dtERow.Item("EId")) += Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
                                'Else
                                'dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency))
                                'End If

                                If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalNextt6Month(dtERow.Item("EId")) += CDec(dtTempRow(0).Item("TaxablePay"))
                                Else
                                    dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), CDec(dtTempRow(0).Item("TaxablePay")))
                                End If
                            End If
                        End If
                        'S.SANDEEP [ 16 JAN 2013 ] -- END

                        If blnFlag = False Then
                            rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 28, "Information required from the" & vbCrLf & "Employer at the end of the year") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 29, "1. Date commenced/left during the year:-") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 30, "2. Name and address of OLD/NEW employer:-") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 31, "3. If employee received benefits other than cash totalling to more than Shs. 1000/= in the year, please give particulars and ") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 32, "4. Where housing is provided show monthly rent charged to employee Shs .....................") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 33, "5. Where lumpsum payment is made state NATURE and AMOUNT ..............................") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 34, "6. where any of the Pay relates to a period other than this year e.g. give the following details:.")
                            blnFlag = True
                        End If
                        'S.SANDEEP [ 22 JUNE 2012 ] -- END


                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

                    Else
                        'rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                        'rpt_Row.Item("Column1") = dtPRow.Item("name")
                        'rpt_Row.Item("Column2") = Format(CDec(0), GUI.fmtCurrency)
                        'rpt_Row.Item("Column3") = Format(CDec(0), GUI.fmtCurrency)
                        'rpt_Row.Item("Column4") = Format(CDec(0), GUI.fmtCurrency)
                        'rpt_Row.Item("Column5") = Format(CDec(0), GUI.fmtCurrency)
                        'rpt_Row.Item("Column6") = Format(CDec(0), GUI.fmtCurrency)
                        'rpt_Row.Item("Column7") = Format(CDec(0), GUI.fmtCurrency)
                        'rpt_Row.Item("Column8") = Format(CDec(0), GUI.fmtCurrency)
                        'rpt_Row.Item("Column9") = Format(CDec(0), GUI.fmtCurrency)
                        'rpt_Row.Item("Column10") = Format(CDec(0), GUI.fmtCurrency)
                        'rpt_Row.Item("Column11") = Company._Object._Name
                        'rpt_Row.Item("Column12") = Company._Object._Address1
                        'rpt_Row.Item("Column13") = Company._Object._Address2
                        'rpt_Row.Item("Column14") = dtERow.Item("EId")
                        'rpt_Row.Item("Column15") = dtERow.Item("OtherName")
                        'rpt_Row.Item("Column16") = dtERow.Item("Code")
                        'rpt_Row.Item("Column17") = Company._Object._Tinno
                        'rpt_Row.Item("Column18") = "" 'Left Blank
                        'rpt_Row.Item("Column19") = dtERow.Item("MainName")

                        'S.SANDEEP [ 16 JAN 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'If intCntSixMonth <= 6 Then
                        '    If dicDivideByFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                        '        dicDivideByFirst6Month(dtERow.Item("EId")) += 0
                        '    Else
                        '        dicDivideByFirst6Month.Add(dtERow.Item("EId"), 0)
                        '    End If

                        '    If dicTaxableTotalFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                        '        dicTaxableTotalFirst6Month(dtERow.Item("EId")) += 0
                        '    Else
                        '        dicTaxableTotalFirst6Month.Add(dtERow.Item("EId"), 0)
                        '    End If
                        '    intCntSixMonth += 1
                        'ElseIf intCntSixMonth >= 7 Then
                        '    If dicDivideByNext6Month.ContainsKey(dtERow.Item("EId")) Then
                        '        dicDivideByNext6Month(dtERow.Item("EId")) += 0
                        '    Else
                        '        dicDivideByNext6Month.Add(dtERow.Item("EId"), 0)
                        '    End If

                        '    If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
                        '        dicTaxableTotalNextt6Month(dtERow.Item("EId")) += 0
                        '    Else
                        '        dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), 0)
                        '    End If
                        '    intCntSixMonth += 1
                        'End If
                        If mintPeriodCount = 12 Then
                            If intCntSixMonth <= 6 Then
                                If dicDivideByFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByFirst6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicDivideByFirst6Month.Add(dtERow.Item("EId"), 0)
                                End If

                                If dicTaxableTotalFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalFirst6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicTaxableTotalFirst6Month.Add(dtERow.Item("EId"), 0)
                                End If
                                intCntSixMonth += 1
                            ElseIf intCntSixMonth >= 7 Then
                                If dicDivideByNext6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByNext6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicDivideByNext6Month.Add(dtERow.Item("EId"), 0)
                                End If

                                If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalNextt6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), 0)
                                End If
                                intCntSixMonth += 1
                            End If
                        ElseIf mintPeriodCount < 12 Then
                            If intMonthId <= 6 Then
                                If dicDivideByFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByFirst6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicDivideByFirst6Month.Add(dtERow.Item("EId"), 0)
                                End If

                                If dicTaxableTotalFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalFirst6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicTaxableTotalFirst6Month.Add(dtERow.Item("EId"), 0)
                                End If
                            ElseIf intMonthId >= 7 Then
                                If dicDivideByNext6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByNext6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicDivideByNext6Month.Add(dtERow.Item("EId"), 0)
                                End If

                                If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalNextt6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), 0)
                                End If
                            End If
                        End If
                        'S.SANDEEP [ 16 JAN 2013 ] -- END
                    End If

                    'S.SANDEEP [ 22 JUNE 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Select Case intCnt
                    '    Case 1
                    '        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 28, "Information required from the" & vbCrLf & "Employer at the end of the year") & vbCrLf
                    '    Case 2
                    '        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 29, "1. Date commenced/left if during the year:-")
                    '    Case 3
                    '        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 30, "2. Name and address of OLD/NEW employer:-")
                    '    Case 4
                    '        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 31, "3. If employee received benefits other than cash totalling to more than Shs. 1000/= in the year, please give particulars and ")
                    '    Case 5
                    '        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 32, "4. Where housing is provided show monthly rent charged to employee Shs .....................")
                    '    Case 6
                    '        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 33, "5. Where lumpsum payment is made state NATURE and AMOUNT ..............................")
                    '    Case 7
                    '        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 34, "6. where any of the Pay relates to a period other than this year e.g. give the following details:.")
                    'End Select
                    'intCnt += 1
                Next

                'S.SANDEEP [ 05 JULY 2011 ] -- START
                'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
                Dim rpt_dRow() As DataRow = rpt_Data.Tables("ArutiTable").Select("Column14 = '" & dtERow.Item("EId") & "'")
                If rpt_dRow.Length > 0 Then
                    For i As Integer = 0 To rpt_dRow.Length - 1
                        rpt_dRow(i).Item("Column72") = Format(decColumn2GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column73") = Format(decColumn3GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column74") = Format(decColumn4GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column75") = Format(decColumn5GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column76") = Format(decColumn6GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column77") = Format(decColumn7GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column78") = Format(decColumn8GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column79") = Format(decColumn9GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column80") = Format(decColumn10GrpTotal, GUI.fmtCurrency)

                        rpt_Data.Tables("ArutiTable").AcceptChanges()
                    Next
                End If
                'S.SANDEEP [ 05 JULY 2011 ] -- END 
            Next

            '///////// Performing Calculation For Particular Employee ///////// -- Start
            Dim objTrnTaxSlab As New clsTranheadInexcessslabTran
            Dim dtExcess_Slab As DataTable = Nothing

            'S.SANDEEP [ 22 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT DISTINCT " & _
            '       "	 prpayrollprocess_tran.tranheadunkid AS HeadId " & _
            '       "	,prpayrollprocess_tran.employeeunkid AS EmpId " & _
            '       "FROM prpayrollprocess_tran,prtranhead_master, hremployee_master " & _
            '       "WHERE prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '       "AND hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid" & _
            '       "	AND prtranhead_master.typeof_id = 9 AND prtranhead_master.calctype_id = 2 "

            StrQ = "SELECT DISTINCT " & _
                   "	 prpayrollprocess_tran.tranheadunkid AS HeadId " & _
                   "	,prpayrollprocess_tran.employeeunkid AS EmpId " & _
                   "FROM prpayrollprocess_tran,prtranhead_master, hremployee_master " & _
                   "WHERE prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                   "AND hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid" & _
                   "	AND prtranhead_master.typeof_id = " & enTypeOf.Taxes & " AND prtranhead_master.calctype_id =" & enCalcType.AsComputedOnWithINEXCESSOFTaxSlab & " "
            'S.SANDEEP [ 22 JUNE 2012 ] -- END

            'Sohail (28 Jun 2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            If mblnIncludeInactiveEmp = False Then
                StrQ &= " AND hremployee_master.isactive = 1 "
            End If

            'Issue : According to prvilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 19 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            'End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 19 JUNE 2012 ] -- END


            'Sohail (28 Jun 2011) -- End

            'S.SANDEEP [ 22 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mintEmpId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmpId & "' "
            End If
            'S.SANDEEP [ 22 JUNE 2012 ] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "Heads")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dblAvgIncomeFirst6Months As Decimal = 0
            Dim dblAvgIncomeNext6Months As Decimal = 0
            Dim intRowIndex As Integer = -1
            Dim dblTaxPerForFirst6Month As Decimal = 0
            Dim dblTaxPerForNext6Month As Decimal = 0
            Dim intTranHeadId As Integer = 0
            Dim dblFirst6MonthPAYE As Decimal = 0
            Dim dblNext6MonthPAYE As Decimal = 0
            Dim dblTaxFirstYear As Decimal = 0
            Dim dblTaxNextYear As Decimal = 0
            Dim strSting As String = String.Empty
            Dim strBuilder As New StringBuilder
            Dim writer As New StringWriter()


            For k As Integer = 0 To dsEmployee.Tables(0).Rows.Count - 1
                Dim drRowIdx() As DataRow = rpt_Data.Tables(0).Select("Column14='" & dsEmployee.Tables(0).Rows(k).Item("EId") & "'")

                If drRowIdx.Length > 0 Then
                    intRowIndex = rpt_Data.Tables(0).Rows.IndexOf(drRowIdx(0))
                End If

                If intRowIndex < 0 Then Continue For

                Dim dtTemp() As DataRow = dsList.Tables(0).Select("EmpId = '" & dsEmployee.Tables(0).Rows(k).Item("EId") & "'")

                If dtTemp.Length > 0 Then
                    intTranHeadId = dtTemp(0)("HeadId")
                End If

                'S.SANDEEP [ 16 JAN 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If mdtSlabDate <> Nothing Then
                    dtExcess_Slab = objTrnTaxSlab.GetCurrentTaxSlab(intTranHeadId, mdtSlabDate)
                Else
                    objTrnTaxSlab._Tranheadunkid = intTranHeadId
                    dtExcess_Slab = objTrnTaxSlab._Datasource
                End If
                'S.SANDEEP [ 16 JAN 2013 ] -- END

                If dicDivideByFirst6Month.Keys.Count > 0 Then
                    If dicDivideByFirst6Month.ContainsKey(dsEmployee.Tables(0).Rows(k).Item("EId")) And dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) > 0 Then
                        dblAvgIncomeFirst6Months = Format(CDec(IIf(dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) = 0, 0, dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
                        'S.SANDEEP [ 22 JUNE 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES {ACCORDING TO MR. RUTTA'S COMMENTS}
                        'dblTaxPerForFirst6Month = Format(CDec((dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / 6)), GUI.fmtCurrency) '******* TAX PER MONTH For Paye Table 1
                        dblTaxPerForFirst6Month = Format(CDec((dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency) '******* TAX PER MONTH For Paye Table 1
                        'S.SANDEEP [ 22 JUNE 2012 ] -- END
                        If dtExcess_Slab.Rows.Count > 0 Then
                            Dim drTemp As DataRow() = dtExcess_Slab.Select("amountupto >= " & dblTaxPerForFirst6Month & "", "amountupto ASC")
                            If drTemp.Length > 0 Then
                                dblFirst6MonthPAYE = (dblTaxPerForFirst6Month - CDec(drTemp(0).Item("inexcessof"))) * CDec(drTemp(0).Item("ratepayable"))
                                If dblFirst6MonthPAYE <> 0 Then
                                    dblFirst6MonthPAYE = dblFirst6MonthPAYE / 100
                                Else
                                    dblFirst6MonthPAYE = 0
                                End If
                                dblFirst6MonthPAYE += CDec(drTemp(0).Item("fixedamount"))
                            Else
                                dblFirst6MonthPAYE = 0
                            End If
                        End If
                    End If
                End If

                If dicDivideByNext6Month.Keys.Count > 0 Then
                    If dicDivideByNext6Month.ContainsKey(dsEmployee.Tables(0).Rows(k).Item("EId")) And dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) > 0 Then
                        dblAvgIncomeNext6Months = Format(CDec(IIf(dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) = 0, 0, dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
                        'S.SANDEEP [ 22 JUNE 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'dblTaxPerForNext6Month = Format(CDec((dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / 6)), GUI.fmtCurrency) '*******  TAX PER MONTH  For Paye Table 2
                        dblTaxPerForNext6Month = Format(CDec((dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency) '*******  TAX PER MONTH  For Paye Table 2
                        'dblTaxPerForNext6Month = CDec((dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))) '*******  TAX PER MONTH  For Paye Table 2
                        'S.SANDEEP [ 22 JUNE 2012 ] -- END
                        If dtExcess_Slab.Rows.Count > 0 Then
                            Dim drTemp As DataRow() = dtExcess_Slab.Select("amountupto >= " & dblTaxPerForNext6Month & "", "amountupto ASC")
                            If drTemp.Length > 0 Then
                                dblNext6MonthPAYE = (dblTaxPerForNext6Month - CDec(drTemp(0).Item("inexcessof"))) * CDec(drTemp(0).Item("ratepayable"))
                                If dblNext6MonthPAYE <> 0 Then
                                    dblNext6MonthPAYE = dblNext6MonthPAYE / 100
                                Else
                                    dblNext6MonthPAYE = 0
                                End If
                                dblNext6MonthPAYE += CDec(drTemp(0).Item("fixedamount"))
                            Else
                                dblNext6MonthPAYE = 0
                            End If
                        End If
                    End If
                End If
                If dicDivideByFirst6Month.Keys.Count > 0 Then
                    dblTaxFirstYear = Format(CDec((dblFirst6MonthPAYE * dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
                Else
                    dblTaxFirstYear = Format(CDec((dblFirst6MonthPAYE * 0)), GUI.fmtCurrency)
                End If
                If dicDivideByNext6Month.Keys.Count > 0 Then
                    dblTaxNextYear = Format(CDec((dblNext6MonthPAYE * dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
                Else
                    dblTaxNextYear = Format(CDec((dblNext6MonthPAYE * 0)), GUI.fmtCurrency)
                End If

                If dicDivideByFirst6Month.Keys.Count > 0 Then
                    strSting = "Average Income per 1st 6 months (Jan-Jun) = ( " & Space(4) & Format(CDec(dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId"))), GUI.fmtCurrency) & Space(4) & " / " & Space(4) & dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & " ) = " & Space(4) & Format(CDec(dblAvgIncomeFirst6Months), GUI.fmtCurrency) & Space(4) & vbCrLf & _
                               "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxPerForFirst6Month), GUI.fmtCurrency) & " = " & Space(4) & Format(CDec(dblFirst6MonthPAYE), GUI.fmtCurrency) & Space(4) & " ( Checking using 1st PAYE table)" & Space(4) & vbCrLf & _
                               "Tax 1st 1/2 year ( " & Space(4) & Format(CDec(dblFirst6MonthPAYE), GUI.fmtCurrency) & Space(4) & " * " & Space(4) & dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & Space(4) & " month )  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxFirstYear), GUI.fmtCurrency) & Space(4) & vbCrLf
                Else
                    strSting = "Average Income per 1st 6 months (Jan-Jun) = ( " & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & " / " & Space(4) & "0" & " ) = " & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & vbCrLf & _
                               "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(0, GUI.fmtCurrency) & " = " & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & " ( Checking using 1st PAYE table)" & Space(4) & vbCrLf & _
                               "Tax 1st 1/2 year ( " & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & " * " & Space(4) & "0" & Space(4) & " month )  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & vbCrLf
                End If

                If dicTaxableTotalNextt6Month.Keys.Count > 0 Then
                    strSting &= "Average Income per 2nd 6 months (Jul-Dec) = ( " & Space(4) & Format(CDec(dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId"))), GUI.fmtCurrency) & Space(4) & " / " & Space(4) & dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & Space(4) & ") = " & Space(4) & Format(CDec(dblAvgIncomeNext6Months), GUI.fmtCurrency) & Space(4) & vbCrLf & _
                                "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxPerForNext6Month), GUI.fmtCurrency) & Space(4) & " = " & Space(4) & Format(CDec(dblNext6MonthPAYE), GUI.fmtCurrency) & Space(4) & " ( Checking using 2nd PAYE table)" & Space(4) & vbCrLf & _
                                "Tax 2nd 1/2 year ( " & Space(4) & Format(CDec(dblNext6MonthPAYE), GUI.fmtCurrency) & Space(4) & " * " & Space(4) & dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & " month)  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxNextYear), GUI.fmtCurrency) & Space(4)
                Else
                    strSting &= "Average Income per 2nd 6 months (Jul-Dec) = ( " & Space(4) & "0" & Space(4) & " / " & Space(4) & "0 ) = " & Space(4) & "0" & Space(4) & vbCrLf & _
                                "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & "0" & Space(4) & " = " & Space(4) & "0" & Space(4) & " ( Checking using 2nd PAYE table)" & Space(4) & vbCrLf & _
                                "Tax 2nd 1/2 year ( " & Space(4) & "0" & Space(4) & " * " & Space(4) & "0" & " month)  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & "0" & Space(4)
                End If

                For j As Integer = 0 To mintPeriodCount - 1
                    If (intRowIndex + j) >= rpt_Data.Tables(0).Rows.Count Then Continue For
                    rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column21") = strSting
                    rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column22") = Format(dblTaxFirstYear, GUI.fmtCurrency)
                    rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column23") = Format(dblTaxNextYear, GUI.fmtCurrency)
                    rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column71") = Format(CDec(rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column80")) - (dblTaxFirstYear + dblTaxNextYear), GUI.fmtCurrency)
                Next
                dblAvgIncomeFirst6Months = 0 : dblAvgIncomeNext6Months = 0 : intRowIndex = -1 : dblTaxPerForFirst6Month = 0 : dblTaxPerForNext6Month = 0
                intTranHeadId = 0 : dblFirst6MonthPAYE = 0 : dblNext6MonthPAYE = 0 : dblTaxFirstYear = 0 : dblTaxNextYear = 0 : strSting = String.Empty
            Next
            '///////// Performing Calculation For Particular Employee ///////// -- End

            objRpt = New ArutiReport.Designer.rptIncomeTaxDeductionP9

            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rptTaxSummary").SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 1, "TANZANIA   REVENUE   AUTHORITY"))
            ReportFunction.TextChange(objRpt, "txtHeading2", Language.getMessage(mstrModuleName, 2, "INCOME TAX DEPARTMENT "))
            ReportFunction.TextChange(objRpt, "txtHeading3", Language.getMessage(mstrModuleName, 3, "INCOME TAX DEDUCTION CARD YEAR") & FinancialYear._Object._FinancialYear_Name)

            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblEmployerName", Language.getMessage(mstrModuleName, 4, "Employer's Name"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblAddress", Language.getMessage(mstrModuleName, 5, "Address"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblEmployeeName", Language.getMessage(mstrModuleName, 6, "Employee's Main Name"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblEmployeeOtherName", Language.getMessage(mstrModuleName, 7, "Employee's Other Name"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblPWCNo", Language.getMessage(mstrModuleName, 8, "*Payroll/Works/Check No."))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblEmployerTINNo1", Language.getMessage(mstrModuleName, 9, "Employer's T.I.N. No."))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblEmployerTINNo2", Language.getMessage(mstrModuleName, 10, "Employee's T.I.N. No"))

            ReportFunction.TextChange(objRpt, "txtNotes", Language.getMessage(mstrModuleName, 11, "NOTE:"))
            ReportFunction.TextChange(objRpt, "txtNote1", Language.getMessage(mstrModuleName, 12, "1. indicate by asterisk the effective date(s) of adjustments, if any .............................................................."))
            ReportFunction.TextChange(objRpt, "txtNote2", Language.getMessage(mstrModuleName, 13, "2. Indicate by asterick against the appropriate months, showing errors and their correction and briefly " & vbCrLf & _
                                                                                                  "explain these here or at the reverse of the card .............................................................................."))
            ReportFunction.TextChange(objRpt, "txtNote3", Language.getMessage(mstrModuleName, 14, "3. *Delete as appropriate"))

            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtAgreeTotalIncome", Language.getMessage(mstrModuleName, 15, "Agreed Total Income"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTax", Language.getMessage(mstrModuleName, 16, "Tax"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtInitials", Language.getMessage(mstrModuleName, 17, "Initials"))

            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtMonths", Language.getMessage(mstrModuleName, 18, "Months"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtBasicPay", Language.getMessage(mstrModuleName, 19, "Basic Pay" & vbCrLf & "(a)"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtHousing", Language.getMessage(mstrModuleName, 20, "Housing" & vbCrLf & "(b)"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtAllowancesandBenefit", Language.getMessage(mstrModuleName, 21, "Allowances" & vbCrLf & "and" & vbCrLf & "Benefits" & vbCrLf & "(c)"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTotalPay", Language.getMessage(mstrModuleName, 22, "Total Pay" & vbCrLf & "(d)"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtPensionAndNSSF", Language.getMessage(mstrModuleName, 23, "Pension  & NSSF" & vbCrLf & "(e)"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTaxablePay", Language.getMessage(mstrModuleName, 24, "Taxable Pay" & vbCrLf & "(f)"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTaxPayable", Language.getMessage(mstrModuleName, 25, "Tax Payable" & vbCrLf & "(g)"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtLessPersonal", Language.getMessage(mstrModuleName, 26, "Less" & vbCrLf & "Personal Reliefs" & vbCrLf & "(h)"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtNetTaxDue", Language.getMessage(mstrModuleName, 27, "Net" & vbCrLf & "Due Tax" & vbCrLf & "(i)"))

            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtYear", Language.getMessage(mstrModuleName, 35, "Year"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtAmt", Language.getMessage(mstrModuleName, 36, "Amt"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTax", Language.getMessage(mstrModuleName, 16, "Tax"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtYear1", Language.getMessage(mstrModuleName, 35, "Year"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtAmt1", Language.getMessage(mstrModuleName, 36, "Amt"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTax1", Language.getMessage(mstrModuleName, 16, "Tax"))

            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTotal", Language.getMessage(mstrModuleName, 77, "Total"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtEmployerUse", Language.getMessage(mstrModuleName, 78, "FOR EMPOYER'S USE"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtLumsum", Language.getMessage(mstrModuleName, 79, "* Add / Deduct tax on previous year lumpsum payment"))
            ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtOverDeduction", Language.getMessage(mstrModuleName, 80, "* Overdeduction/underdeduction c/f"))

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_PAYE_Details(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_PAYE_Details() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ = "SELECT  basicpay AS basicpay " & _
                   "    ,housing AS housing " & _
                          ", allowancebenefit AS allowancebenefit " & _
                          ", basicpay + housing + ( allowancebenefit ) AS Grosspay " & _
                          ", ( basicpay + housing + noncashbenefit + ( allowancebenefit )) - deduction AS taxable " & _
                   "    ,deduction AS deduction " & _
                   "    ,taxpayable AS taxpayable " & _
                   "    ,relief AS relief " & _
                   "    ,noncashbenefit AS noncashbenefit " & _
                   "    ,taxpayable - relief AS taxdue " & _
                   "    ,empid AS empid " & _
                          ", A.employeecode " & _
                          ", employeename " & _
                          ", ISNULL(hremployee_master.present_address1, '') + ' ' + ISNULL(hremployee_master.present_address2, '') AS ADDRESS " & _
                          ", ISNULL(hrmsConfiguration..cfcity_master.name, '') AS city " & _
                          ", CC.costcentercode AS CCCode " & _
                          ", CC.costcentername AS CCName " & _
                          ", Id " & _
                          ", GName " & _
                    "FROM    ( SELECT    SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
                   "	    ,SUM(ISNULL(PAYE.allowance, 0)) AS allowancebenefit " & _
                    "	,SUM(ISNULL(PAYE.housing, 0)) AS housing " & _
                    "	,SUM(ISNULL(PAYE.deduction, 0)) AS deduction " & _
                   "	    ,SUM(ISNULL(PAYE.taxpayable, 0)) AS taxpayable " & _
                   "        ,SUM(ISNULL(PAYE.relief, 0)) AS relief " & _
                   "        ,SUM(ISNULL(PAYE.noncashbenefit, 0)) AS noncashbenefit " & _
                    "	,paye.empid AS empid " & _
                                      ", employeecode " & _
                                      ", employeename " & _
                                      ", Id " & _
                                      ", GName " & _
                              "FROM      ( SELECT    CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
                   "		,0 AS allowance " & _
                    "		,0 AS housing " & _
                    "		,0 AS deduction " & _
                    "		,0 AS taxpayable " & _
                    "		,0 AS relief " & _
                    "		,0 AS noncashbenefit " & _
                    "		,vwPayroll.employeeunkid AS empid " & _
                                                  ", vwPayroll.employeecode " & _
                                                  ", vwPayroll.employeename "
            'Sohail (08 Feb 2017) - [add noncashbenefit amount to taxable column as per louis requesr for Lonagro]
            'Sohail (06 Feb 2017) - [noncashbenefit]

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "
            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE     vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND vwPayroll.tranheadunkid = @OtherEarningTranId  "
            Else
                StrQ &= "AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  "
            End If

            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= "   UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                    "		,0 AS allowance " & _
                    "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS housing " & _
                    "		,0 AS deduction " & _
                    "		,0 AS taxpayable " & _
                    "		,0 AS relief " & _
                    "		,0 AS noncashbenefit " & _
                    "		,vwPayroll.employeeunkid AS empid " & _
                                                  ", vwPayroll.employeecode " & _
                                                  ", vwPayroll.employeename "
            'Sohail (06 Feb 2017) - [noncashbenefit]

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	WHERE vwPayroll.tranheadunkid = @HousingTranId " & _
                    "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
                                                    "AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                    "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS allowance " & _
                    "		,0 AS housing " & _
                    "		,0 AS deduction " & _
                    "		,0 AS taxpayable " & _
                    "		,0 AS relief " & _
                    "		,0 AS noncashbenefit " & _
                    "		,vwPayroll.employeeunkid AS empid " & _
                                                  ", vwPayroll.employeecode " & _
                                                  ", vwPayroll.employeename "
            'Sohail (06 Feb 2017) - [noncashbenefit]

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE     vwPayroll.tranheadunkid NOT IN ( -1 ) " & _
                    "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
                    "		AND prtranhead_master.istaxable = 1 " & _
                    " AND vwPayroll.GroupID IN (1, 2, 3, 4) " & _
                                                    "AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                    "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
                                         "AND NOT (vwPayroll.tranheadunkid = @HousingTranId AND istaxable = 1) "
            'Hemant (25 Feb 2019) -- Start
            'Issue LONAGRO#3511: Incorrect Tax Due calculated in P9 report.
            '" AND vwPayroll.GroupID IN (1, 2, 3, 4) " & _
            'Hemant (25 Feb 2019) -- End

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND NOT ( vwPayroll.tranheadunkid = @OtherEarningTranId AND istaxable = 1 ) "
            Else
                StrQ &= "AND NOT ( vwPayroll.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) "
            End If

            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                    "		,0 AS allowance " & _
                    "		,0 AS housing " & _
                    "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS deduction " & _
                    "		,0 AS taxpayable " & _
                    "		,0 AS relief " & _
                    "		,0 AS noncashbenefit " & _
                    "		,vwPayroll.employeeunkid AS empid " & _
                                                  ", vwPayroll.employeecode " & _
                                                  ", vwPayroll.employeename "
            'Sohail (06 Feb 2017) - [noncashbenefit]

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	WHERE vwPayroll.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
                    "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & "  " & _
                                                    "AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                    "		,0 AS allowance " & _
                    "		,0 AS housing " & _
                    "		,0 AS deduction " & _
                    "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxpayable " & _
                    "		,0 AS relief " & _
                    "		,0 AS noncashbenefit " & _
                    "		,vwPayroll.employeeunkid AS empid " & _
                                                  ", vwPayroll.employeecode " & _
                                                  ", vwPayroll.employeename "
            'Sohail (06 Feb 2017) - [noncashbenefit]

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	WHERE vwPayroll.typeof_id = " & enTypeOf.Taxes & "  " & _
                                                    "AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") "

            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                    "		,0 AS allowance " & _
                    "		,0 AS housing " & _
                    "		,0 AS deduction " & _
                    "		,0 AS taxpayable " & _
                    "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS relief " & _
                    "		,0 AS noncashbenefit " & _
                    "		,vwPayroll.employeeunkid AS empid " & _
                                                  ", vwPayroll.employeecode " & _
                                                  ", vwPayroll.employeename "
            'Sohail (06 Feb 2017) - [noncashbenefit]

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE     vwPayroll.tranheadunkid NOT IN ( -1 ) " & _
                                                    "AND prtranhead_master.istaxrelief = 1 " & _
                                                    " AND vwPayroll.GroupID IN (1, 2, 3, 4) " & _
                                                    "AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") "
            'Hemant (25 Feb 2019) -- Start
            'Issue LONAGRO#3511: Incorrect Tax Due calculated in P9 report.
            '" AND vwPayroll.GroupID IN (1, 2, 3, 4) " & _
            'Hemant (25 Feb 2019) -- End

            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Feb 2017) -- Start
            'TRA Enhancement - 64.1 - Add column to show Non-Cash Benefit.
            StrQ &= "                     UNION ALL " & _
                                         "SELECT    0 AS basicsalary " & _
                                           "		,0 AS allowance " & _
                                           "		,0 AS housing " & _
                                           "		,0 AS deduction " & _
                                           "		,0 AS taxpayable " & _
                                           "		,0 AS relief " & _
                                           "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS noncashbenefit " & _
                                           "		,vwPayroll.employeeunkid AS empid " & _
                                             ", vwPayroll.employeecode " & _
                                             ", vwPayroll.employeename "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If

            StrQ &= "                      FROM      vwPayroll " & _
                                                    "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     vwPayroll.tranheadunkid NOT IN ( -1 ) " & _
                                                    "AND prtranhead_master.isnoncashbenefit = 1 " & _
                                                    " AND vwPayroll.GroupID IN (1, 2, 3, 4) " & _
                                                    "AND vwPayroll.payperiodunkid IN (" & mstrPeriodIds & ") "
            'Hemant (25 Feb 2019) -- Start
            'Issue LONAGRO#3511: Incorrect Tax Due calculated in P9 report.
            '" AND vwPayroll.GroupID IN (1, 2, 3, 4) " & _
            'Hemant (25 Feb 2019) -- End

            If mintEmpId > 0 Then
                StrQ &= " AND vwPayroll.employeeunkid = @EmplId "
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If
            'Sohail (06 Feb 2017) -- End

            StrQ &= ") AS PAYE " & _
                              "WHERE     1 = 1 " & _
                              "GROUP BY  empid " & _
                                      ", employeecode " & _
                                      ", employeename " & _
                                      ", Id " & _
                                      ", GName " & _
                            ") AS A " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
                            "LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = hremployee_master.present_post_townunkid " & _
                            "LEFT JOIN prcostcenter_master AS CC ON CC.costcenterunkid = hremployee_master.costcenterunkid " & _
                    "WHERE   1 = 1 "


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            If mblnShowGroupByCostCenter = True Then
                If mintViewIndex > 0 Then
                    StrQ &= " ORDER BY CCName, GName "
                Else
                    StrQ &= " ORDER BY CCName "
                End If
            Else
                If mintViewIndex > 0 Then
                    StrQ &= " ORDER BY GNAME"
                Else
                    StrQ &= " ORDER BY employeename "
                End If
            End If

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (06 Feb 2017) -- Start
            'TRA Enhancement - 57.4 and 64.1 - Add column to show Non-Cash Benefit.
            'Dim dtTemp() As DataRow = dsList.Tables(0).Select("basicpay = 0 AND housing = 0 AND allowancebenefit = 0 AND Grosspay = 0 AND deduction = 0 AND taxable = 0 AND taxdue = 0")
            Dim dtTemp() As DataRow = dsList.Tables(0).Select("basicpay = 0 AND housing = 0 AND allowancebenefit = 0 AND Grosspay = 0 AND deduction = 0 AND taxable = 0 AND taxdue = 0 AND noncashbenefit = 0")
            'Sohail (06 Feb 2017) -- End

            If dtTemp.Length > 0 Then
                For i As Integer = 0 To dtTemp.Length - 1
                    dsList.Tables(0).Rows.Remove(dtTemp(i))
                Next
                dsList.Tables(0).AcceptChanges()
            End If

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@EmplId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing
            Dim blnIsAdded As Boolean = False
            Dim intCnt As Integer = 1
            Dim strPrevCCCode As String = ""

            Dim decColumn6Total, decColumn7Total, decColumn8Total, decColumn9Total, decColumn10Total, decColumn11Total, decColumn12Total, decColumn22Total As Decimal 'Sohail (06 Feb 2017) - [decColumn22Total]
            decColumn6Total = 0 : decColumn7Total = 0 : decColumn8Total = 0 : decColumn9Total = 0 : decColumn10Total = 0 : decColumn11Total = 0 : decColumn12Total = 0 : decColumn22Total = 0 'Sohail (06 Feb 2017) - [decColumn22Total]
            Dim decColumn6SubTotal, decColumn7SubTotal, decColumn8SubTotal, decColumn9SubTotal, decColumn10SubTotal, decColumn11SubTotal, decColumn12SubTotal, decColumn22SubTotal As Decimal 'Sohail (06 Feb 2017) - [decColumn22Total]

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = intCnt
                rpt_Row.Item("Column2") = dtRow.Item("employeename")
                rpt_Row.Item("Column3") = dtRow.Item("employeecode")
                rpt_Row.Item("Column4") = dtRow.Item("Address")
                rpt_Row.Item("Column5") = dtRow.Item("city")

                rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("basicpay")), GUI.fmtCurrency)
                rpt_Row.Item("Column7") = Format(CDec(dtRow.Item("housing")), GUI.fmtCurrency)
                rpt_Row.Item("Column8") = Format(CDec(dtRow.Item("allowancebenefit")), GUI.fmtCurrency)
                rpt_Row.Item("Column9") = Format(CDec(dtRow.Item("Grosspay")), GUI.fmtCurrency)
                rpt_Row.Item("Column10") = Format(CDec(dtRow.Item("deduction")), GUI.fmtCurrency)
                rpt_Row.Item("Column11") = Format(CDec(dtRow.Item("taxable")), GUI.fmtCurrency)
                rpt_Row.Item("Column12") = Format(CDec(dtRow.Item("taxdue")), GUI.fmtCurrency)
                'Sohail (06 Feb 2017) -- Start
                'TRA Enhancement - 64.1 - Add column to show Non-Cash Benefit.
                rpt_Row.Item("Column22") = Format(CDec(dtRow.Item("noncashbenefit")), GUI.fmtCurrency)
                'Sohail (06 Feb 2017) -- End

                If mblnShowGroupByCostCenter = True Then
                    rpt_Row.Item("Column13") = dtRow.Item("CCCode")
                    rpt_Row.Item("Column14") = dtRow.Item("CCName")
                Else
                    rpt_Row.Item("Column13") = ""
                    rpt_Row.Item("Column14") = ""
                End If

                If strPrevCCCode <> dtRow.Item("CCCode").ToString Then
                    decColumn6SubTotal = 0 : decColumn7SubTotal = 0 : decColumn8SubTotal = 0 : decColumn9SubTotal = 0 : decColumn10SubTotal = 0 : decColumn11SubTotal = 0 : decColumn12SubTotal = 0 : decColumn22SubTotal = 0 'Sohail (06 Feb 2017) - [decColumn22SubTotal]
                End If

                decColumn6SubTotal = decColumn6SubTotal + CDec(dtRow.Item("basicpay"))
                decColumn7SubTotal = decColumn7SubTotal + CDec(dtRow.Item("housing"))
                decColumn8SubTotal = decColumn8SubTotal + CDec(dtRow.Item("allowancebenefit"))
                decColumn9SubTotal = decColumn9SubTotal + CDec(dtRow.Item("Grosspay"))
                decColumn10SubTotal = decColumn10SubTotal + CDec(dtRow.Item("deduction"))
                decColumn11SubTotal = decColumn11SubTotal + CDec(dtRow.Item("taxable"))
                decColumn12SubTotal = decColumn12SubTotal + CDec(dtRow.Item("taxdue"))
                'Sohail (06 Feb 2017) -- Start
                'TRA Enhancement - 57.4 and 64.1 - Add column to show Non-Cash Benefit.
                decColumn22SubTotal = decColumn22SubTotal + CDec(dtRow.Item("noncashbenefit"))
                'Sohail (06 Feb 2017) -- End

                decColumn6Total = decColumn6Total + CDec(dtRow.Item("basicpay"))
                decColumn7Total = decColumn7Total + CDec(dtRow.Item("housing"))
                decColumn8Total = decColumn8Total + CDec(dtRow.Item("allowancebenefit"))
                decColumn9Total = decColumn9Total + CDec(dtRow.Item("Grosspay"))
                decColumn10Total = decColumn10Total + CDec(dtRow.Item("deduction"))
                decColumn11Total = decColumn11Total + CDec(dtRow.Item("taxable"))
                decColumn12Total = decColumn12Total + CDec(dtRow.Item("taxdue"))
                'Sohail (06 Feb 2017) -- Start
                'TRA Enhancement - 57.4 and 64.1 - Add column to show Non-Cash Benefit.
                decColumn22Total = decColumn22Total + CDec(dtRow.Item("noncashbenefit"))
                'Sohail (06 Feb 2017) -- End

                rpt_Row.Item("Column15") = Format(decColumn6SubTotal, GUI.fmtCurrency)
                rpt_Row.Item("Column16") = Format(decColumn7SubTotal, GUI.fmtCurrency)
                rpt_Row.Item("Column17") = Format(decColumn8SubTotal, GUI.fmtCurrency)
                rpt_Row.Item("Column18") = Format(decColumn9SubTotal, GUI.fmtCurrency)
                rpt_Row.Item("Column19") = Format(decColumn10SubTotal, GUI.fmtCurrency)
                rpt_Row.Item("Column20") = Format(decColumn11SubTotal, GUI.fmtCurrency)
                rpt_Row.Item("Column21") = Format(decColumn12SubTotal, GUI.fmtCurrency)
                'Sohail (06 Feb 2017) -- Start
                'TRA Enhancement - 57.4 and 64.1 - Add column to show Non-Cash Benefit.
                rpt_Row.Item("Column23") = Format(decColumn22SubTotal, GUI.fmtCurrency)
                'Sohail (06 Feb 2017) -- End

                intCnt += 1
                strPrevCCCode = dtRow.Item("CCCode").ToString
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

            Next

            objRpt = New ArutiReport.Designer.rptPAYEPaymentTax

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblTRA", Language.getMessage(mstrModuleName, 37, "TANZANIA REVENUE AUTHORITY")) 'Start From 37
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPAYE", Language.getMessage(mstrModuleName, 38, "P.A.Y.E."))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblReportName", Language.getMessage(mstrModuleName, 39, "STATEMENT AND PAYMENT OF TAX WITHHELD"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblYear", Language.getMessage(mstrModuleName, 40, "YEAR:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtYear", FinancialYear._Object._FinancialYear_Name)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblTIN", Language.getMessage(mstrModuleName, 41, "TIN:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtTIN", Company._Object._Tinno)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod", Language.getMessage(mstrModuleName, 42, "Period: (Please tick the appropriate box) "))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod1", Language.getMessage(mstrModuleName, 43, "From 1 January to 30 June "))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod2", Language.getMessage(mstrModuleName, 44, "From 1 January to 31 December "))

            If mstrPeriodNames.Trim.Length > 0 Then
                ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPeriodNames", Language.getMessage(mstrModuleName, 81, "Periods : (") & mstrPeriodNames & Language.getMessage(mstrModuleName, 82, ")"))
            Else
                ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPeriodNames", "")
            End If


            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblEmployer", Language.getMessage(mstrModuleName, 45, "Name of Employer:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtEmployer", Company._Object._Name)

            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPostalAddress", Language.getMessage(mstrModuleName, 46, "Postal Address:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPoBox", Language.getMessage(mstrModuleName, 47, "P. O. Box"))
            Dim objZip As New clszipcode_master
            objZip._Zipcodeunkid = Company._Object._Postalunkid
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPoBox", objZip._Zipcode_No)
            objZip = Nothing
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPostalCity", Language.getMessage(mstrModuleName, 48, "Postal City"))
            Dim objCity As New clscity_master
            objCity._Cityunkid = Company._Object._Cityunkid
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPostalCity", objCity._Name)
            objCity = Nothing
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblContactNumbers", Language.getMessage(mstrModuleName, 49, "Contact Numbers:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPhone", Language.getMessage(mstrModuleName, 50, "Phone number"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPhone1", Company._Object._Phone1)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblSecondPhone", Language.getMessage(mstrModuleName, 51, "Second Phone"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtSecondPhone", Company._Object._Phone2)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblThirdPhone", Language.getMessage(mstrModuleName, 52, "Third Phone"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtThirdphone", Company._Object._Phone3)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblFax", Language.getMessage(mstrModuleName, 53, " Fax number"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtFax", Company._Object._Fax)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblEmail", Language.getMessage(mstrModuleName, 54, "E-mail address:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtEmail", Company._Object._Email)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPhysical", Language.getMessage(mstrModuleName, 55, "Physical Address:"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPlot", Language.getMessage(mstrModuleName, 56, "Plot Number"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPlot", Company._Object._Reg1_Value)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblBlock", Language.getMessage(mstrModuleName, 57, "Block Number"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtBlock", Company._Object._Reg2_Value)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblStreet", Language.getMessage(mstrModuleName, 58, "Street/Location"))
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtStreet", Company._Object._Address1 & " " & Company._Object._Address2)
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblBranch", Language.getMessage(mstrModuleName, 59, "Name of Branch"))
            Dim objBranch As New clsbankbranch_master
            objBranch._Branchunkid = Company._Object._Branchunkid
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtBranch", objBranch._Branchname)
            objBranch = Nothing
            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPaymentStatement", Language.getMessage(mstrModuleName, 60, "ITX215.01.E -  P.A.Y.E.-Statement"))


            ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 61, "P.A.Y.E. - DETAILS OF PAYMENT OF TAX WITHHELD"))
            ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 62, "Name of Employer :"))
            ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "lblTIN", Language.getMessage(mstrModuleName, 63, "TIN :"))
            ReportFunction.TextChange(objRpt, "txtTIN", Company._Object._Tinno)
            ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 64, "S/No."))
            ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 65, "Name Of Employee"))
            ReportFunction.TextChange(objRpt, "txtPayroll", Language.getMessage(mstrModuleName, 66, "Payroll No."))
            ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 67, "Postal Address"))
            ReportFunction.TextChange(objRpt, "txtCity", Language.getMessage(mstrModuleName, 68, "Postal City"))
            ReportFunction.TextChange(objRpt, "txtPay", Language.getMessage(mstrModuleName, 69, "Basic Pay"))
            ReportFunction.TextChange(objRpt, "txtHousing", Language.getMessage(mstrModuleName, 70, "Housing"))
            ReportFunction.TextChange(objRpt, "txtAllBenefit", Language.getMessage(mstrModuleName, 71, "Allowances And Benefits"))
            ReportFunction.TextChange(objRpt, "txtGrossPay", Language.getMessage(mstrModuleName, 72, "Grosspay"))
            ReportFunction.TextChange(objRpt, "txtDeduction", Language.getMessage(mstrModuleName, 73, "Deductions"))
            ReportFunction.TextChange(objRpt, "txtTaxable", Language.getMessage(mstrModuleName, 74, "Taxable"))
            ReportFunction.TextChange(objRpt, "txtTaxDue", Language.getMessage(mstrModuleName, 75, "Tax Due"))
            ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 76, "Total"))
            ReportFunction.TextChange(objRpt, "txtCostCenter", Language.getMessage(mstrModuleName, 83, "Cost Center : "))
            ReportFunction.TextChange(objRpt, "txtSubTotal", Language.getMessage(mstrModuleName, 84, "Sub Total"))
            'Sohail (06 Feb 2017) -- Start
            'TRA Enhancement - 64.1 - Add column to show Non-Cash Benefit.
            ReportFunction.TextChange(objRpt, "txtNonCashBenefit", Language.getMessage(mstrModuleName, 93, "Non-Cash Benefit"))
            'Sohail (06 Feb 2017) -- End

            Call ReportFunction.TextChange(objRpt, "txtTotal6", Format(decColumn6Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal7", Format(decColumn7Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal8", Format(decColumn8Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal9", Format(decColumn9Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal10", Format(decColumn10Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal11", Format(decColumn11Total, GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtTotal12", Format(decColumn12Total, GUI.fmtCurrency))
            'Sohail (06 Feb 2017) -- Start
            'TRA Enhancement - 57.4 and 64.1 - Add column to show Non-Cash Benefit.
            Call ReportFunction.TextChange(objRpt, "txtTotal22", Format(decColumn22Total, GUI.fmtCurrency))
            'Sohail (06 Feb 2017) -- End

            If mblnShowGroupByCostCenter = True Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GHCostCenter", False)
                Call ReportFunction.EnableSuppressSection(objRpt, "GFCostCenter", False)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "GHCostCenter", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GFCostCenter", True)
            End If

            'Sohail (18 Apr 2014) -- Start
            'Enhancement - Show / Hide Address City option on PAYE Detail Report
            If mblnShowPublicAddressCity = False Then
                Call ReportFunction.EnableSuppress(objRpt, "txtAddress", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column41", True)
                Call ReportFunction.EnableSuppress(objRpt, "txtCity", True)
                Call ReportFunction.EnableSuppress(objRpt, "Column51", True)

                objRpt.ReportDefinition.ReportObjects("txtEmpName").Width = objRpt.ReportDefinition.ReportObjects("txtEmpName").Width + objRpt.ReportDefinition.ReportObjects("txtAddress").Width + objRpt.ReportDefinition.ReportObjects("txtCity").Width
                objRpt.ReportDefinition.ReportObjects("Column21").Width = objRpt.ReportDefinition.ReportObjects("Column21").Width + objRpt.ReportDefinition.ReportObjects("Column41").Width + objRpt.ReportDefinition.ReportObjects("Column51").Width
            End If
            'Sohail (18 Apr 2014) -- End

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_PAYE_Details; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Sohail (07 Sep 2015) -- Start
    'Enhancement - New Report type P9 Annualization Report in P9 Report.

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_P9_Annualization_Report(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_P9_Annualization_Report() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= "SELECT  Pid " & _
                          ", basicpay AS basicpay " & _
                          ", housing AS housing " & _
                          ", allowancebenefit AS allowancebenefit " & _
                          ", Pension " & _
                          ", TaxPayable " & _
                          ", basicpay + housing + ( allowancebenefit ) AS TotalPay " & _
                          ", (basicpay + housing + ( allowancebenefit ))- Pension AS TaxablePay " & _
                          ", LPR " & _
                          ", TaxPayable - LPR AS NTD " & _
                          ", empid AS empid " & _
                    "FROM    ( SELECT    Period.Pid " & _
                                      ", SUM(ISNULL(Period.basicsalary, 0)) AS basicpay " & _
                                      ", SUM(ISNULL(Period.housing, 0)) AS housing " & _
                                      ", SUM(ISNULL(Period.allowance, 0)) AS allowancebenefit " & _
                                      ", SUM(ISNULL(Pension, 0)) AS Pension " & _
                                      ", SUM(ISNULL(TaxPayable, 0)) AS TaxPayable " & _
                                      ", SUM(ISNULL(LPR, 0)) AS LPR " & _
                                      ", ISNULL(empid, -1) AS empid " & _
                              "FROM      ( SELECT    CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
                                                  ", 0 AS housing " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxPayable " & _
                                                  ", 0 AS LPR " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                                  ", prtnaleave_tran.payperiodunkid AS Pid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
            mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	                   WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid > 0 "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND prtranhead_master.tranheadunkid = @OtherEarningTranId  "
            Else
                StrQ &= "AND prtranhead_master.typeof_id = " & enTypeOf.Salary & "  "
            End If

            If mintEmpId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmplId "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS housing " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxPayable " & _
                                                  ", 0 AS LPR " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                                  ", prtnaleave_tran.payperiodunkid AS Pid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
            mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      WHERE      prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid > 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @HousingTranId " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
                                                    "AND prtranhead_master.istaxable = 1 "

            If mintEmpId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmplId "
            End If


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '   StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                  UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS housing " & _
                                                  ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS allowance " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxPayable " & _
                                                  ", 0 AS LPR " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                                  ", prtnaleave_tran.payperiodunkid AS Pid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
             mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	                   WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid > 0 " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
                                                    "AND prtranhead_master.istaxable = 1 " & _
                                                    "AND NOT (prtranhead_master.tranheadunkid = @HousingTranId AND prtranhead_master.istaxable = 1) "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND NOT ( prtranhead_master.tranheadunkid = @OtherEarningTranId AND prtranhead_master.istaxable = 1 ) "
            Else
                StrQ &= "AND NOT ( prtranhead_master.typeof_id = " & enTypeOf.Salary & " AND prtranhead_master.istaxable = 1 ) "
            End If

            If mintEmpId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmplId "
            End If


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      GROUP BY  prpayrollprocess_tran.employeeunkid " & _
                                                  ", prtnaleave_tran.payperiodunkid " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS housing " & _
                                                  ", 0 AS allowance " & _
                                                  ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Pension " & _
                                                  ", 0 AS TaxPayable " & _
                                                  ", 0 AS LPR " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                                  ", prtnaleave_tran.payperiodunkid AS Pid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
             mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	                   WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid > 0 " & _
                                                    "AND prtranhead_master.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
                                                    "AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & "  "

            If mintEmpId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmplId "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                      GROUP BY  prpayrollprocess_tran.employeeunkid " & _
                                                  ", prtnaleave_tran.payperiodunkid " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS housing " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS Pension " & _
                                                  ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS TaxPayable " & _
                                                  ", 0 AS LPR " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                                  ", prtnaleave_tran.payperiodunkid AS Pid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
             mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	                   WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid > 0 " & _
                                                    "AND prtranhead_master.typeof_id  = " & enTypeOf.Taxes & "  "

            If mintEmpId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmplId "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                   GROUP BY  prpayrollprocess_tran.employeeunkid " & _
                                               ", prtnaleave_tran.payperiodunkid " & _
                                          "UNION ALL " & _
                                          "SELECT    0 AS basicsalary " & _
                                                  ", 0 AS housing " & _
                                                  ", 0 AS allowance " & _
                                                  ", 0 AS Pension " & _
                                                  ", 0 AS TaxPayable " & _
                                                  ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS LPR " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                                  ", prtnaleave_tran.payperiodunkid AS Pid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
             mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	                   WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid > 0 " & _
                                                    "AND ISNULL(prtranhead_master.istaxrelief, 0) = 1 "

            If mintEmpId > 0 Then
                StrQ &= " AND prpayrollprocess_tran.employeeunkid = @EmplId "
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "                     GROUP BY  prpayrollprocess_tran.employeeunkid " & _
                                                 ", prtnaleave_tran.payperiodunkid " & _
                                        ") AS Period " & _
                              "WHERE     1 = 1 GROUP BY Period.Pid, ISNULL(empid, -1) " & _
                        ") AS A WHERE 1 = 1 "

            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtBlankRow() As DataRow = dsList.Tables(0).Select("basicpay = 0 AND housing = 0 AND allowancebenefit = 0 AND TotalPay = 0 AND Pension = 0 AND TaxablePay = 0 AND TaxPayable = 0 AND LPR = 0 AND NTD = 0")
            Dim StrKeyId As String = String.Empty
            If dtBlankRow.Length > 0 Then
                'Sohail (22 Jul 2020) -- Start
                'HYATT Issue # 0004812 : PAYE :Income Tax P9 Report type P9 Annualization amount differ from payroll summary report.
                Dim arr As List(Of String) = (From p In dtBlankRow Select (p.Field(Of Integer)("empid").ToString())).Distinct().ToList
                Dim arrFinal As New List(Of String)
                'Sohail (22 Jul 2020) -- End
                For i As Integer = 0 To dtBlankRow.Length - 1
                    'Sohail (22 Jul 2020) -- Start
                    'HYATT Issue # 0004812 : PAYE :Income Tax P9 Report type P9 Annualization amount differ from payroll summary report.
                    'StrKeyId &= "," & dtBlankRow(i).Item("empid")
                    'Sohail (22 Jul 2020) -- End
                    dsList.Tables(0).Rows.Remove(dtBlankRow(i))
                Next
                dsList.Tables(0).AcceptChanges()
                'Sohail (22 Jul 2020) -- Start
                'HYATT Issue # 0004812 : PAYE :Income Tax P9 Report type P9 Annualization amount differ from payroll summary report.
                For Each strID As String In arr
                    If dsList.Tables(0).Select("empid = " & CInt(strID) & " ").Length > 0 Then
                        'Do nothing
                    Else
                        arrFinal.Add(strID)
                    End If
                Next
                StrKeyId = String.Join(",", arrFinal.ToArray)
                'Sohail (22 Jul 2020) -- End
            End If

            'Sohail (22 Jul 2020) -- Start
            'HYATT Issue # 0004812 : PAYE :Income Tax P9 Report type P9 Annualization amount differ from payroll summary report.
            'If StrKeyId.Length > 0 Then
            '    StrKeyId = Mid(StrKeyId, 2)
            'End If
            'Sohail (22 Jul 2020) -- End

            Dim dsEmployee As DataSet = Nothing
            objDataOperation.ClearParameters()


            StrQ = "SELECT " & _
                    "	 Emp.EId " & _
                    "	,Emp.Code " & _
                    "	,Emp.MainName " & _
                    "	,Emp.OtherName " & _
                    "FROM " & _
                    "( " & _
                    "	SELECT " & _
                    "		 hremployee_master.employeeunkid AS EId " & _
                    "		,employeecode AS Code " & _
                    "		,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS MainName " & _
                    "		,ISNULL(othername,'') AS OtherName " & _
                   "	FROM hremployee_master "
            StrQ &= mstrAnalysis_Join

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "	WHERE 1 = 1 "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= ") AS Emp WHERE 1 = 1 "

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@EmplId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
                StrQ &= "AND EId = @EmplId "
            End If

            If StrKeyId.Length > 0 Then
                StrQ &= "AND EId NOT IN(" & StrKeyId & ")"
            End If

            dsEmployee = objDataOperation.ExecQuery(StrQ, "Emp")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsPeriod As DataSet = Nothing
            Dim objPeriod As New clscommom_period_Tran

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsPeriod = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            dsPeriod = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
            'Sohail (21 Aug 2015) -- End

            If mstrPeriodIds.Length > 0 Then
                Dim dtTemp() As DataRow = Nothing
                dtTemp = dsPeriod.Tables(0).Select("periodunkid NOT IN (" & mstrPeriodIds & ")")
                For i As Integer = 0 To dtTemp.Length - 1
                    dsPeriod.Tables(0).Rows.Remove(dtTemp(i))
                    dsPeriod.Tables(0).AcceptChanges()
                Next
            End If

            mintPeriodCount = dsPeriod.Tables(0).Rows.Count

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim intCnt As Integer = 1
            Dim rpt_Row As DataRow = Nothing

            Dim dicTaxableTotalFirst6Month As New Dictionary(Of Integer, Decimal)  'EmployeeId,Total For First Six Month
            Dim dicTaxableTotalNextt6Month As New Dictionary(Of Integer, Decimal)  'EmployeeId,Total For Next Six Month
            Dim dicDivideByFirst6Month As New Dictionary(Of Integer, Integer)   'EmployeeId, DivideValue For First Six Month
            Dim dicDivideByNext6Month As New Dictionary(Of Integer, Integer)   'EmployeeId, DivideValue For Next Six Month
            Dim intCntSixMonth As Integer = 1

            Dim blnFlag As Boolean = False


            Dim decColumn2GrpTotal, decColumn3GrpTotal, decColumn4GrpTotal, decColumn5GrpTotal, decColumn6GrpTotal, decColumn7GrpTotal, decColumn8GrpTotal, decColumn9GrpTotal, decColumn10GrpTotal, decColumn11GrpTotal As Decimal
            Dim decColumn2GTotal, decColumn3GTotal, decColumn4GTotal, decColumn5GTotal, decColumn6GTotal, decColumn7GTotal, decColumn8GTotal, decColumn9GTotal, decColumn10GTotal, decColumn11GTotal As Decimal

            Dim intMonthId As Integer = 0

            For Each dtERow As DataRow In dsEmployee.Tables(0).Rows
                intCnt = 1
                intCntSixMonth = 1
                blnFlag = False
                decColumn2GrpTotal = 0 : decColumn3GrpTotal = 0 : decColumn4GrpTotal = 0 : decColumn5GrpTotal = 0 : decColumn6GrpTotal = 0 : decColumn7GrpTotal = 0 : decColumn8GrpTotal = 0 : decColumn9GrpTotal = 0 : decColumn10GrpTotal = 0
                For Each dtPRow As DataRow In dsPeriod.Tables(0).Rows

                    If intMonthId <= 0 Then intMonthId = eZeeDate.convertDate(dtPRow.Item("start_date").ToString).Date.Month

                    Dim dtTempRow() As DataRow
                    dtTempRow = dsList.Tables(0).Select("Empid = '" & dtERow.Item("EId") & "' AND Pid = '" & dtPRow.Item("periodunkid") & "'")
                    If dtTempRow.Length > 0 Then
                        rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
                        rpt_Row.Item("Column1") = dtPRow.Item("name")
                        rpt_Row.Item("Column2") = Format(CDec(dtTempRow(0).Item("basicpay")), GUI.fmtCurrency)
                        rpt_Row.Item("Column3") = Format(CDec(dtTempRow(0).Item("housing")), GUI.fmtCurrency)
                        rpt_Row.Item("Column4") = Format(CDec(dtTempRow(0).Item("allowancebenefit")), GUI.fmtCurrency)
                        rpt_Row.Item("Column5") = Format(CDec(dtTempRow(0).Item("TotalPay")), GUI.fmtCurrency)
                        rpt_Row.Item("Column6") = Format(CDec(dtTempRow(0).Item("Pension")), GUI.fmtCurrency)
                        rpt_Row.Item("Column7") = Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
                        rpt_Row.Item("Column8") = Format(CDec(dtTempRow(0).Item("TaxPayable")), GUI.fmtCurrency)
                        rpt_Row.Item("Column9") = Format(CDec(dtTempRow(0).Item("LPR")), GUI.fmtCurrency)
                        rpt_Row.Item("Column10") = Format(CDec(dtTempRow(0).Item("NTD")), GUI.fmtCurrency)
                        rpt_Row.Item("Column11") = Company._Object._Name
                        rpt_Row.Item("Column12") = Company._Object._Address1
                        rpt_Row.Item("Column13") = Company._Object._Address2
                        rpt_Row.Item("Column14") = dtERow.Item("EId")
                        rpt_Row.Item("Column15") = dtERow.Item("OtherName")
                        rpt_Row.Item("Column16") = dtERow.Item("Code")
                        rpt_Row.Item("Column17") = Company._Object._Tinno
                        rpt_Row.Item("Column18") = "" 'Left Blank
                        rpt_Row.Item("Column19") = dtERow.Item("MainName")

                        decColumn2GrpTotal = decColumn2GrpTotal + CDec(dtTempRow(0).Item("basicpay"))
                        decColumn3GrpTotal = decColumn3GrpTotal + CDec(dtTempRow(0).Item("housing"))
                        decColumn4GrpTotal = decColumn4GrpTotal + CDec(dtTempRow(0).Item("allowancebenefit"))
                        decColumn5GrpTotal = decColumn5GrpTotal + CDec(dtTempRow(0).Item("TotalPay"))
                        decColumn6GrpTotal = decColumn6GrpTotal + CDec(dtTempRow(0).Item("Pension"))
                        decColumn7GrpTotal = decColumn7GrpTotal + CDec(dtTempRow(0).Item("TaxablePay"))
                        decColumn8GrpTotal = decColumn8GrpTotal + CDec(dtTempRow(0).Item("TaxPayable"))
                        decColumn9GrpTotal = decColumn9GrpTotal + CDec(dtTempRow(0).Item("LPR"))
                        decColumn10GrpTotal = decColumn10GrpTotal + CDec(dtTempRow(0).Item("NTD"))


                        If mintPeriodCount = 12 Then
                            If intCntSixMonth <= 6 Then
                                If dicDivideByFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByFirst6Month(dtERow.Item("EId")) += 1
                                Else
                                    dicDivideByFirst6Month.Add(dtERow.Item("EId"), 1)
                                End If

                                If dicTaxableTotalFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalFirst6Month(dtERow.Item("EId")) += Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
                                Else
                                    dicTaxableTotalFirst6Month.Add(dtERow.Item("EId"), Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency))
                                End If
                                intCntSixMonth += 1
                            ElseIf intCntSixMonth >= 7 Then
                                If dicDivideByNext6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByNext6Month(dtERow.Item("EId")) += 1
                                Else
                                    dicDivideByNext6Month.Add(dtERow.Item("EId"), 1)
                                End If

                                If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalNextt6Month(dtERow.Item("EId")) += Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
                                Else
                                    dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency))
                                End If
                                intCntSixMonth += 1
                            End If
                        ElseIf mintPeriodCount < 12 Then
                            If intMonthId <= 6 Then
                                If dicDivideByFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByFirst6Month(dtERow.Item("EId")) += 1
                                Else
                                    dicDivideByFirst6Month.Add(dtERow.Item("EId"), 1)
                                End If

                                If dicTaxableTotalFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalFirst6Month(dtERow.Item("EId")) += Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
                                Else
                                    dicTaxableTotalFirst6Month.Add(dtERow.Item("EId"), Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency))
                                End If
                            ElseIf intMonthId >= 7 Then
                                If dicDivideByNext6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByNext6Month(dtERow.Item("EId")) += 1
                                Else
                                    dicDivideByNext6Month.Add(dtERow.Item("EId"), 1)
                                End If

                                If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalNextt6Month(dtERow.Item("EId")) += CDec(dtTempRow(0).Item("TaxablePay"))
                                Else
                                    dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), CDec(dtTempRow(0).Item("TaxablePay")))
                                End If
                            End If
                        End If

                        If blnFlag = False Then
                            rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 28, "Information required from the" & vbCrLf & "Employer at the end of the year") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 29, "1. Date commenced/left during the year:-") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 30, "2. Name and address of OLD/NEW employer:-") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 31, "3. If employee received benefits other than cash totalling to more than Shs. 1000/= in the year, please give particulars and ") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 32, "4. Where housing is provided show monthly rent charged to employee Shs .....................") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 33, "5. Where lumpsum payment is made state NATURE and AMOUNT ..............................") & vbCrLf & _
                                                       Language.getMessage(mstrModuleName, 34, "6. where any of the Pay relates to a period other than this year e.g. give the following details:.")
                            blnFlag = True
                        End If


                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

                    Else

                        If mintPeriodCount = 12 Then
                            If intCntSixMonth <= 6 Then
                                If dicDivideByFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByFirst6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicDivideByFirst6Month.Add(dtERow.Item("EId"), 0)
                                End If

                                If dicTaxableTotalFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalFirst6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicTaxableTotalFirst6Month.Add(dtERow.Item("EId"), 0)
                                End If
                                intCntSixMonth += 1
                            ElseIf intCntSixMonth >= 7 Then
                                If dicDivideByNext6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByNext6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicDivideByNext6Month.Add(dtERow.Item("EId"), 0)
                                End If

                                If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalNextt6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), 0)
                                End If
                                intCntSixMonth += 1
                            End If
                        ElseIf mintPeriodCount < 12 Then
                            If intMonthId <= 6 Then
                                If dicDivideByFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByFirst6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicDivideByFirst6Month.Add(dtERow.Item("EId"), 0)
                                End If

                                If dicTaxableTotalFirst6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalFirst6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicTaxableTotalFirst6Month.Add(dtERow.Item("EId"), 0)
                                End If
                            ElseIf intMonthId >= 7 Then
                                If dicDivideByNext6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicDivideByNext6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicDivideByNext6Month.Add(dtERow.Item("EId"), 0)
                                End If

                                If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
                                    dicTaxableTotalNextt6Month(dtERow.Item("EId")) += 0
                                Else
                                    dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), 0)
                                End If
                            End If
                        End If

                    End If


                Next

                Dim rpt_dRow() As DataRow = rpt_Data.Tables("ArutiTable").Select("Column14 = '" & dtERow.Item("EId") & "'")
                If rpt_dRow.Length > 0 Then
                    For i As Integer = 0 To rpt_dRow.Length - 1
                        rpt_dRow(i).Item("Column72") = Format(decColumn2GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column73") = Format(decColumn3GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column74") = Format(decColumn4GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column75") = Format(decColumn5GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column76") = Format(decColumn6GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column77") = Format(decColumn7GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column78") = Format(decColumn8GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column79") = Format(decColumn9GrpTotal, GUI.fmtCurrency)
                        rpt_dRow(i).Item("Column80") = Format(decColumn10GrpTotal, GUI.fmtCurrency)



                        rpt_Data.Tables("ArutiTable").AcceptChanges()
                    Next

                    decColumn2GTotal += CDec(Format(decColumn2GrpTotal, GUI.fmtCurrency))
                    decColumn3GTotal += CDec(Format(decColumn3GrpTotal, GUI.fmtCurrency))
                    decColumn4GTotal += CDec(Format(decColumn4GrpTotal, GUI.fmtCurrency))
                    decColumn5GTotal += CDec(Format(decColumn5GrpTotal, GUI.fmtCurrency))
                    decColumn6GTotal += CDec(Format(decColumn6GrpTotal, GUI.fmtCurrency))
                    decColumn7GTotal += CDec(Format(decColumn7GrpTotal, GUI.fmtCurrency))
                    decColumn8GTotal += CDec(Format(decColumn8GrpTotal, GUI.fmtCurrency))
                    decColumn9GTotal += CDec(Format(decColumn9GrpTotal, GUI.fmtCurrency))
                    decColumn10GTotal += CDec(Format(decColumn10GrpTotal, GUI.fmtCurrency))

                End If
            Next

            '///////// Performing Calculation For Particular Employee ///////// -- Start
            Dim objTrnTaxSlab As New clsTranheadInexcessslabTran
            Dim dtExcess_Slab As DataTable = Nothing


            StrQ = "SELECT DISTINCT " & _
                   "	 prpayrollprocess_tran.tranheadunkid AS HeadId " & _
                   "	,prpayrollprocess_tran.employeeunkid AS EmpId " & _
                   "FROM prpayrollprocess_tran,prtranhead_master, hremployee_master " & _
                   "WHERE prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                   "AND hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid" & _
                   "	AND prtranhead_master.typeof_id = " & enTypeOf.Taxes & " AND prtranhead_master.calctype_id =" & enCalcType.AsComputedOnWithINEXCESSOFTaxSlab & " "

            If mblnIncludeInactiveEmp = False Then
                StrQ &= " AND hremployee_master.isactive = 1 "
            End If


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            If mintEmpId > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = '" & mintEmpId & "' "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "Heads")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dblAvgIncomeFirst6Months As Decimal = 0
            Dim dblAvgIncomeNext6Months As Decimal = 0
            Dim intRowIndex As Integer = -1
            Dim dblTaxPerForFirst6Month As Decimal = 0
            Dim dblTaxPerForNext6Month As Decimal = 0
            Dim intTranHeadId As Integer = 0
            Dim dblFirst6MonthPAYE As Decimal = 0
            Dim dblNext6MonthPAYE As Decimal = 0
            Dim dblTaxFirstYear As Decimal = 0
            Dim dblTaxNextYear As Decimal = 0
            Dim strSting As String = String.Empty
            Dim strBuilder As New StringBuilder
            Dim writer As New StringWriter()


            For k As Integer = 0 To dsEmployee.Tables(0).Rows.Count - 1
                Dim drRowIdx() As DataRow = rpt_Data.Tables(0).Select("Column14='" & dsEmployee.Tables(0).Rows(k).Item("EId") & "'")

                If drRowIdx.Length > 0 Then
                    intRowIndex = rpt_Data.Tables(0).Rows.IndexOf(drRowIdx(0))
                End If

                If intRowIndex < 0 Then Continue For

                Dim dtTemp() As DataRow = dsList.Tables(0).Select("EmpId = '" & dsEmployee.Tables(0).Rows(k).Item("EId") & "'")

                If dtTemp.Length > 0 Then
                    intTranHeadId = dtTemp(0)("HeadId")
                End If

                If mdtSlabDate <> Nothing Then
                    dtExcess_Slab = objTrnTaxSlab.GetCurrentTaxSlab(intTranHeadId, mdtSlabDate)
                Else
                    objTrnTaxSlab._Tranheadunkid = intTranHeadId
                    dtExcess_Slab = objTrnTaxSlab._Datasource
                End If

                If dicDivideByFirst6Month.Keys.Count > 0 Then
                    If dicDivideByFirst6Month.ContainsKey(dsEmployee.Tables(0).Rows(k).Item("EId")) And dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) > 0 Then
                        dblAvgIncomeFirst6Months = Format(CDec(IIf(dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) = 0, 0, dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
                        dblTaxPerForFirst6Month = Format(CDec((dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency) '******* TAX PER MONTH For Paye Table 1
                        If dtExcess_Slab.Rows.Count > 0 Then
                            Dim drTemp As DataRow() = dtExcess_Slab.Select("amountupto >= " & dblTaxPerForFirst6Month & "", "amountupto ASC")
                            If drTemp.Length > 0 Then
                                dblFirst6MonthPAYE = (dblTaxPerForFirst6Month - CDec(drTemp(0).Item("inexcessof"))) * CDec(drTemp(0).Item("ratepayable"))
                                If dblFirst6MonthPAYE <> 0 Then
                                    dblFirst6MonthPAYE = dblFirst6MonthPAYE / 100
                                Else
                                    dblFirst6MonthPAYE = 0
                                End If
                                dblFirst6MonthPAYE += CDec(drTemp(0).Item("fixedamount"))
                            Else
                                dblFirst6MonthPAYE = 0
                            End If
                        End If
                    End If
                End If

                If dicDivideByNext6Month.Keys.Count > 0 Then
                    If dicDivideByNext6Month.ContainsKey(dsEmployee.Tables(0).Rows(k).Item("EId")) And dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) > 0 Then
                        dblAvgIncomeNext6Months = Format(CDec(IIf(dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) = 0, 0, dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
                        dblTaxPerForNext6Month = Format(CDec((dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency) '*******  TAX PER MONTH  For Paye Table 2
                        If dtExcess_Slab.Rows.Count > 0 Then
                            Dim drTemp As DataRow() = dtExcess_Slab.Select("amountupto >= " & dblTaxPerForNext6Month & "", "amountupto ASC")
                            If drTemp.Length > 0 Then
                                dblNext6MonthPAYE = (dblTaxPerForNext6Month - CDec(drTemp(0).Item("inexcessof"))) * CDec(drTemp(0).Item("ratepayable"))
                                If dblNext6MonthPAYE <> 0 Then
                                    dblNext6MonthPAYE = dblNext6MonthPAYE / 100
                                Else
                                    dblNext6MonthPAYE = 0
                                End If
                                dblNext6MonthPAYE += CDec(drTemp(0).Item("fixedamount"))
                            Else
                                dblNext6MonthPAYE = 0
                            End If
                        End If
                    End If
                End If
                If dicDivideByFirst6Month.Keys.Count > 0 Then
                    dblTaxFirstYear = Format(CDec((dblFirst6MonthPAYE * dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
                Else
                    dblTaxFirstYear = Format(CDec((dblFirst6MonthPAYE * 0)), GUI.fmtCurrency)
                End If
                If dicDivideByNext6Month.Keys.Count > 0 Then
                    dblTaxNextYear = Format(CDec((dblNext6MonthPAYE * dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
                Else
                    dblTaxNextYear = Format(CDec((dblNext6MonthPAYE * 0)), GUI.fmtCurrency)
                End If

                If dicDivideByFirst6Month.Keys.Count > 0 Then
                    strSting = "Average Income per 1st 6 months (Jan-Jun) = ( " & Space(4) & Format(CDec(dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId"))), GUI.fmtCurrency) & Space(4) & " / " & Space(4) & dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & " ) = " & Space(4) & Format(CDec(dblAvgIncomeFirst6Months), GUI.fmtCurrency) & Space(4) & vbCrLf & _
                               "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxPerForFirst6Month), GUI.fmtCurrency) & " = " & Space(4) & Format(CDec(dblFirst6MonthPAYE), GUI.fmtCurrency) & Space(4) & " ( Checking using 1st PAYE table)" & Space(4) & vbCrLf & _
                               "Tax 1st 1/2 year ( " & Space(4) & Format(CDec(dblFirst6MonthPAYE), GUI.fmtCurrency) & Space(4) & " * " & Space(4) & dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & Space(4) & " month )  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxFirstYear), GUI.fmtCurrency) & Space(4) & vbCrLf
                Else
                    strSting = "Average Income per 1st 6 months (Jan-Jun) = ( " & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & " / " & Space(4) & "0" & " ) = " & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & vbCrLf & _
                               "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(0, GUI.fmtCurrency) & " = " & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & " ( Checking using 1st PAYE table)" & Space(4) & vbCrLf & _
                               "Tax 1st 1/2 year ( " & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & " * " & Space(4) & "0" & Space(4) & " month )  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & vbCrLf
                End If

                If dicTaxableTotalNextt6Month.Keys.Count > 0 Then
                    strSting &= "Average Income per 2nd 6 months (Jul-Dec) = ( " & Space(4) & Format(CDec(dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId"))), GUI.fmtCurrency) & Space(4) & " / " & Space(4) & dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & Space(4) & ") = " & Space(4) & Format(CDec(dblAvgIncomeNext6Months), GUI.fmtCurrency) & Space(4) & vbCrLf & _
                                "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxPerForNext6Month), GUI.fmtCurrency) & Space(4) & " = " & Space(4) & Format(CDec(dblNext6MonthPAYE), GUI.fmtCurrency) & Space(4) & " ( Checking using 2nd PAYE table)" & Space(4) & vbCrLf & _
                                "Tax 2nd 1/2 year ( " & Space(4) & Format(CDec(dblNext6MonthPAYE), GUI.fmtCurrency) & Space(4) & " * " & Space(4) & dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & " month)  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxNextYear), GUI.fmtCurrency) & Space(4)
                Else
                    strSting &= "Average Income per 2nd 6 months (Jul-Dec) = ( " & Space(4) & "0" & Space(4) & " / " & Space(4) & "0 ) = " & Space(4) & "0" & Space(4) & vbCrLf & _
                                "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & "0" & Space(4) & " = " & Space(4) & "0" & Space(4) & " ( Checking using 2nd PAYE table)" & Space(4) & vbCrLf & _
                                "Tax 2nd 1/2 year ( " & Space(4) & "0" & Space(4) & " * " & Space(4) & "0" & " month)  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & "0" & Space(4)
                End If

                For j As Integer = 0 To mintPeriodCount - 1
                    If (intRowIndex + j) >= rpt_Data.Tables(0).Rows.Count Then Continue For
                    'Hemant (18 Jul 2019) -- Start
                    'ISSUE/ENHANCEMENT#3980(MWAUWASA) - Discrepancy in p9 annualization report.
                    If CInt(dsEmployee.Tables(0).Rows(k).Item("EId")) <> CInt(rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column14")) Then Continue For
                    'Hemant (18 Jul 2019) -- End
                    rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column21") = strSting
                    rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column22") = Format(dblTaxFirstYear, GUI.fmtCurrency)
                    rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column23") = Format(dblTaxNextYear, GUI.fmtCurrency)
                    rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column71") = Format(CDec(rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column80")) - (dblTaxFirstYear + dblTaxNextYear), GUI.fmtCurrency)

                    decColumn11GrpTotal = CDec(Format(CDec(rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column80")) - (dblTaxFirstYear + dblTaxNextYear), GUI.fmtCurrency))
                Next
                decColumn11GTotal += decColumn11GrpTotal
                dblAvgIncomeFirst6Months = 0 : dblAvgIncomeNext6Months = 0 : intRowIndex = -1 : dblTaxPerForFirst6Month = 0 : dblTaxPerForNext6Month = 0
                intTranHeadId = 0 : dblFirst6MonthPAYE = 0 : dblNext6MonthPAYE = 0 : dblTaxFirstYear = 0 : dblTaxNextYear = 0 : strSting = String.Empty
            Next
            '///////// Performing Calculation For Particular Employee ///////// -- End

            objRpt = New ArutiReport.Designer.rptP9Annualization


            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If


            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "lblEmpCode", Language.getMessage(mstrModuleName, 85, "Emp. code"))
            ReportFunction.TextChange(objRpt, "lblEmployerTINNo1", Language.getMessage(mstrModuleName, 9, "Employer's T.I.N. No."))
            ReportFunction.TextChange(objRpt, "lblPeriodList", Language.getMessage(mstrModuleName, 86, "Periods : "))
            ReportFunction.TextChange(objRpt, "txtPeriodList", mstrPeriodNames)

            ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 87, "Emp. Name"))
            ReportFunction.TextChange(objRpt, "txtBasicPay", Language.getMessage(mstrModuleName, 19, "Basic Pay" & vbCrLf & "(a)"))
            ReportFunction.TextChange(objRpt, "txtHousing", Language.getMessage(mstrModuleName, 20, "Housing" & vbCrLf & "(b)"))
            ReportFunction.TextChange(objRpt, "txtAllowancesandBenefit", Language.getMessage(mstrModuleName, 21, "Allowances" & vbCrLf & "and" & vbCrLf & "Benefits" & vbCrLf & "(c)"))
            ReportFunction.TextChange(objRpt, "txtTotalPay", Language.getMessage(mstrModuleName, 22, "Total Pay" & vbCrLf & "(d)"))
            ReportFunction.TextChange(objRpt, "txtPensionAndNSSF", Language.getMessage(mstrModuleName, 23, "Pension  & NSSF" & vbCrLf & "(e)"))
            ReportFunction.TextChange(objRpt, "txtTaxablePay", Language.getMessage(mstrModuleName, 24, "Taxable Pay" & vbCrLf & "(f)"))
            ReportFunction.TextChange(objRpt, "txtTaxPayable", Language.getMessage(mstrModuleName, 25, "Tax Payable" & vbCrLf & "(g)"))
            ReportFunction.TextChange(objRpt, "txtLessPersonal", Language.getMessage(mstrModuleName, 26, "Less" & vbCrLf & "Personal Reliefs" & vbCrLf & "(h)"))
            ReportFunction.TextChange(objRpt, "txtNetTaxDue", Language.getMessage(mstrModuleName, 27, "Net" & vbCrLf & "Due Tax" & vbCrLf & "(i)"))
            ReportFunction.TextChange(objRpt, "txtOverDeduction", Language.getMessage(mstrModuleName, 88, "*Over / underductions"))
            ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 77, "Total"))

            ReportFunction.TextChange(objRpt, "txtLegend1", Language.getMessage(mstrModuleName, 91, "* Negative (-ve) Amount indicates Under deductions"))
            ReportFunction.TextChange(objRpt, "txtLegend2", Language.getMessage(mstrModuleName, 92, "* Positive (+ve) Amount indicates Over deductions"))

            ReportFunction.TextChange(objRpt, "txtTotBasicPay", Format(decColumn2GTotal, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtTotHousing", Format(decColumn3GTotal, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtTotAllowance", Format(decColumn4GTotal, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtTotTotalPay", Format(decColumn5GTotal, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtTotPension", Format(decColumn6GTotal, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtTotTaxable", Format(decColumn7GTotal, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtTotTaxPayable", Format(decColumn8GTotal, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtTotLess", Format(decColumn9GTotal, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtTotNetTax", Format(decColumn10GTotal, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtTotOverDed", Format(decColumn11GTotal, GUI.fmtCurrency))

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 89, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 90, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyname", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_P9_Annualization_Report; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sohail (07 Sep 2015) -- End

#Region " PAYE DETAIL before Group By Cost Center on 30-NOV-2013 "
    'Private Function Generate_PAYE_Details() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Dim dtFilterTable As DataTable
    '    Try
    '        objDataOperation = New clsDataOperation

    '        'Sohail (17 Aug 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0
    '        objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        'Sohail (17 Aug 2012) -- End


    '        'ISSUE : INCLUSION OF EMPLOYEE IF HE IS ACTIVE FOR SOME PERIOD BUT NOW INACTIVE
    '        'E.G.  : EMPLOYEE IS ACTIVE FOR JAN - MAR & TERMINATED IN APR. SO ITS NOT COMING FOR JAN TO MAR IN REPORT.

    '        'S.SANDEEP [ 19 JUNE 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES

    '        'S.SANDEEP [ 02 AUG 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'StrQ = "SELECT " & _
    '        '        "	 SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
    '        '        "	,SUM(ISNULL(PAYE.allowancebenefit, 0)) AS allowancebenefit " & _
    '        '        "	,SUM(ISNULL(PAYE.housing, 0)) AS housing " & _
    '        '        "	,SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.allowancebenefit, 0)) + SUM(ISNULL(PAYE.housing, 0)) AS Grosspay " & _
    '        '        "	,SUM(ISNULL(PAYE.deduction, 0)) AS deduction " & _
    '        '        "	,(SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.allowancebenefit,0)) + SUM(ISNULL(PAYE.housing, 0))) - SUM(ISNULL(PAYE.deduction, 0)) AS taxable " & _
    '        '        "	,SUM(ISNULL(PAYE.taxpayable, 0)) - SUM(ISNULL(PAYE.relief, 0)) AS taxdue " & _
    '        '        "	,paye.pid AS pid " & _
    '        '        "	,paye.empid AS empid " & _
    '        '        "FROM " & _
    '        '        "( " & _
    '        '        "	SELECT " & _
    '        '        "		 ISNULL(vwPayroll.amount, 0) AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll " & _
    '        '        "	WHERE vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '        '        "		AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,ISNULL(vwPayroll.amount, 0) AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll " & _
    '        '        "	WHERE vwPayroll.tranheadunkid = @HousingTranId " & _
    '        '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,ISNULL(vwPayroll.amount, 0) AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll " & _
    '        '        "	WHERE vwPayroll.typeof_id IN (" & enTypeOf.Allowance & "," & enTypeOf.BENEFIT & ") " & _
    '        '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,ISNULL(vwPayroll.amount, 0) AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll " & _
    '        '        "	WHERE vwPayroll.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,ISNULL(vwPayroll.amount, 0) AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll " & _
    '        '        "	WHERE vwPayroll.typeof_id = " & enTypeOf.Taxes & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,ISNULL(vwPayroll.amount, 0) AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll " & _
    '        '        "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
    '        '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
    '        '        "		AND prtranhead_master.istaxrelief = 1 " & _
    '        '        ") AS PAYE " & _
    '        '        "WHERE   pid IN (" & mstrPeriodIds & ") "

    '        'S.SANDEEP [ 13 DEC 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'StrQ = "SELECT " & _
    '        '        "	 SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
    '        '        "	,SUM(ISNULL(PAYE.allowancebenefit, 0)) AS allowancebenefit " & _
    '        '        "	,SUM(ISNULL(PAYE.housing, 0)) AS housing " & _
    '        '        "	,SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.allowancebenefit, 0)) + SUM(ISNULL(PAYE.housing, 0)) AS Grosspay " & _
    '        '        "	,SUM(ISNULL(PAYE.deduction, 0)) AS deduction " & _
    '        '        "	,(SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.allowancebenefit,0)) + SUM(ISNULL(PAYE.housing, 0))) - SUM(ISNULL(PAYE.deduction, 0)) AS taxable " & _
    '        '        "	,SUM(ISNULL(PAYE.taxpayable, 0)) - SUM(ISNULL(PAYE.relief, 0)) AS taxdue " & _
    '        '        "	,paye.pid AS pid " & _
    '        '        "	,paye.empid AS empid " & _
    '        '        "FROM " & _
    '        '        "( " & _
    '        '        "	SELECT " & _
    '        '        "		 CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll "
    '        'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '         mstrAnalysis_Join

    '        'StrQ &= "	WHERE vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '        '        "		AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll "
    '        'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        mstrAnalysis_Join

    '        'StrQ &= "	WHERE vwPayroll.tranheadunkid = @HousingTranId " & _
    '        '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll "
    '        'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        mstrAnalysis_Join

    '        'StrQ &= "	WHERE vwPayroll.typeof_id IN (" & enTypeOf.Allowance & "," & enTypeOf.BENEFIT & ") " & _
    '        '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll "
    '        'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        mstrAnalysis_Join

    '        'StrQ &= "	WHERE vwPayroll.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
    '        '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll "
    '        'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        mstrAnalysis_Join

    '        'StrQ &= "	WHERE vwPayroll.typeof_id = " & enTypeOf.Taxes & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll "
    '        'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        mstrAnalysis_Join

    '        'StrQ &= "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
    '        '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
    '        '        "		AND prtranhead_master.istaxrelief = 1 " & _
    '        '        ") AS PAYE " & _
    '        '        "WHERE   pid IN (" & mstrPeriodIds & ") "

    '        'Sohail (04 Sep 2013) -- Start
    '        'TRA - ENHANCEMENT - Changed the logic of Allowance as per Rutta's Comment discussed with Sandy. Don't calculate Basic and Housing in Allowance
    '        'StrQ = "SELECT " & _
    '        '       "     pid AS pid " & _
    '        '       "    ,basicpay AS basicpay " & _
    '        '       "    ,housing AS housing " & _
    '        '       "	,CASE WHEN allowancebenefit <=0 THEN 0 " & _
    '        '       "		  WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
    '        '       "	 ELSE allowancebenefit - (basicpay+housing) END AS allowancebenefit " & _
    '        '       "	,basicpay+housing+(CASE WHEN allowancebenefit <=0 THEN 0 " & _
    '        '       "		  WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
    '        '       "	 ELSE allowancebenefit - (basicpay+housing) END) AS Grosspay " & _
    '        '       "	,(basicpay+housing+(CASE WHEN allowancebenefit <=0 THEN 0 " & _
    '        '       "		  WHEN (basicpay+housing) > allowancebenefit THEN (basicpay+housing) - allowancebenefit " & _
    '        '       "	 ELSE allowancebenefit - (basicpay+housing) END))- deduction AS taxable " & _
    '        '       "    ,deduction AS deduction " & _
    '        '       "    ,taxpayable AS taxpayable " & _
    '        '       "    ,relief AS relief " & _
    '        '       "    ,taxpayable - relief AS taxdue " & _
    '        '       "    ,empid AS empid " & _
    '        '       "FROM ( " & _
    '        '       "    SELECT " & _
    '        '        "	 SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
    '        '       "	    ,SUM(ISNULL(PAYE.allowance, 0)) AS allowancebenefit " & _
    '        '        "	,SUM(ISNULL(PAYE.housing, 0)) AS housing " & _
    '        '        "	,SUM(ISNULL(PAYE.deduction, 0)) AS deduction " & _
    '        '       "	    ,SUM(ISNULL(PAYE.taxpayable, 0)) AS taxpayable " & _
    '        '       "        ,SUM(ISNULL(PAYE.relief, 0)) AS relief " & _
    '        '        "	,paye.pid AS pid " & _
    '        '        "	,paye.empid AS empid " & _
    '        '        "FROM " & _
    '        '        "( " & _
    '        '        "	SELECT " & _
    '        '        "		 CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
    '        '       "		,0 AS allowance " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll "
    '        'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '         mstrAnalysis_Join
    '        StrQ = "SELECT  pid AS pid " & _
    '               "    ,basicpay AS basicpay " & _
    '               "    ,housing AS housing " & _
    '                      ", allowancebenefit AS allowancebenefit " & _
    '                      ", basicpay + housing + ( allowancebenefit ) AS Grosspay " & _
    '                      ", ( basicpay + housing + ( allowancebenefit )) - deduction AS taxable " & _
    '               "    ,deduction AS deduction " & _
    '               "    ,taxpayable AS taxpayable " & _
    '               "    ,relief AS relief " & _
    '               "    ,taxpayable - relief AS taxdue " & _
    '               "    ,empid AS empid " & _
    '                "FROM    ( SELECT    SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
    '               "	    ,SUM(ISNULL(PAYE.allowance, 0)) AS allowancebenefit " & _
    '                "	,SUM(ISNULL(PAYE.housing, 0)) AS housing " & _
    '                "	,SUM(ISNULL(PAYE.deduction, 0)) AS deduction " & _
    '               "	    ,SUM(ISNULL(PAYE.taxpayable, 0)) AS taxpayable " & _
    '               "        ,SUM(ISNULL(PAYE.relief, 0)) AS relief " & _
    '                "	,paye.pid AS pid " & _
    '                "	,paye.empid AS empid " & _
    '                          "FROM      ( SELECT    CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS basicsalary " & _
    '               "		,0 AS allowance " & _
    '                "		,0 AS housing " & _
    '                "		,0 AS deduction " & _
    '                "		,0 AS taxpayable " & _
    '                "		,0 AS relief " & _
    '                "		,vwPayroll.employeeunkid AS empid " & _
    '                "		,vwPayroll.payperiodunkid AS pid " & _
    '                                      "FROM      vwPayroll " & _
    '                                                "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '                 mstrAnalysis_Join
    '        'Sohail (04 Sep 2013) -- End

    '        'Sohail (28 Aug 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ &= "	WHERE vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '        '        "		AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowance " & _
    '        '        "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll "
    '        'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        mstrAnalysis_Join
    '        StrQ &= "	WHERE 1 = 1 "

    '        If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
    '            StrQ &= "AND vwPayroll.tranheadunkid = @OtherEarningTranId  "
    '        Else
    '            StrQ &= "AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  "
    '        End If

    '        StrQ &= "   UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 AS basicsalary " & _
    '                "		,0 AS allowance " & _
    '                "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS housing " & _
    '                "		,0 AS deduction " & _
    '                "		,0 AS taxpayable " & _
    '                "		,0 AS relief " & _
    '                "		,vwPayroll.employeeunkid AS empid " & _
    '                "		,vwPayroll.payperiodunkid AS pid " & _
    '                "	FROM vwPayroll "
    '        StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '                mstrAnalysis_Join
    '        'Sohail (28 Aug 2013) -- End

    '        StrQ &= "	WHERE vwPayroll.tranheadunkid = @HousingTranId " & _
    '                "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '                "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 AS basicsalary " & _
    '                "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS allowance " & _
    '                "		,0 AS housing " & _
    '                "		,0 AS deduction " & _
    '                "		,0 AS taxpayable " & _
    '                "		,0 AS relief " & _
    '                "		,vwPayroll.employeeunkid AS empid " & _
    '                "		,vwPayroll.payperiodunkid AS pid " & _
    '                "	FROM vwPayroll "
    '        StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '                mstrAnalysis_Join

    '        'Sohail (04 Sep 2013) -- Start
    '        'TRA - ENHANCEMENT - Changed the logic of Allowance as per Rutta's Comment discussed with Sandy. Don't calculate Basic and Housing in Allowance
    '        'StrQ &= "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
    '        '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
    '        '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '        '        "		AND prtranhead_master.istaxable = 1 " & _
    '        '        "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowance " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll "
    '        'StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '        '        mstrAnalysis_Join
    '        StrQ &= "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
    '                "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
    '                "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '                "		AND prtranhead_master.istaxable = 1 " & _
    '                "		AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
    '                                     "AND NOT (vwPayroll.tranheadunkid = @HousingTranId AND istaxable = 1) "

    '        If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
    '            StrQ &= "AND NOT ( vwPayroll.tranheadunkid = @OtherEarningTranId AND istaxable = 1 ) "
    '        Else
    '            StrQ &= "AND NOT ( vwPayroll.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) "
    '        End If

    '        StrQ &= "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 AS basicsalary " & _
    '                "		,0 AS allowance " & _
    '                "		,0 AS housing " & _
    '                "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS deduction " & _
    '                "		,0 AS taxpayable " & _
    '                "		,0 AS relief " & _
    '                "		,vwPayroll.employeeunkid AS empid " & _
    '                "		,vwPayroll.payperiodunkid AS pid " & _
    '                "	FROM vwPayroll "
    '        StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '                mstrAnalysis_Join
    '        'Sohail (04 Sep 2013) -- End

    '        StrQ &= "	WHERE vwPayroll.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
    '                "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & "  " & _
    '                "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 AS basicsalary " & _
    '                "		,0 AS allowance " & _
    '                "		,0 AS housing " & _
    '                "		,0 AS deduction " & _
    '                "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxpayable " & _
    '                "		,0 AS relief " & _
    '                "		,vwPayroll.employeeunkid AS empid " & _
    '                "		,vwPayroll.payperiodunkid AS pid " & _
    '                "	FROM vwPayroll "
    '        StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '                mstrAnalysis_Join

    '        StrQ &= "	WHERE vwPayroll.typeof_id = " & enTypeOf.Taxes & "  " & _
    '                "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 AS basicsalary " & _
    '                "		,0 AS allowance " & _
    '                "		,0 AS housing " & _
    '                "		,0 AS deduction " & _
    '                "		,0 AS taxpayable " & _
    '                "		,CAST(ISNULL(vwPayroll.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS relief " & _
    '                "		,vwPayroll.employeeunkid AS empid " & _
    '                "		,vwPayroll.payperiodunkid AS pid " & _
    '                "	FROM vwPayroll "
    '        StrQ &= " JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '                mstrAnalysis_Join

    '        StrQ &= "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
    '                "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
    '                "		AND prtranhead_master.istaxrelief = 1 " & _
    '                ") AS PAYE " & _
    '                "WHERE   pid IN (" & mstrPeriodIds & ") " & _
    '                "GROUP BY pid,empid " & _
    '                ", costcentercode " & _
    '                ", costcentername " & _
    '                ") AS A WHERE 1 = 1 "
    '        'Sohail (30 Nov 2013) - [costcentercode, costcentername]
    '        'S.SANDEEP [ 13 DEC 2012 ] -- END


    '        'S.SANDEEP [ 02 AUG 2012 ] -- END



    '        'StrQ = "SELECT " & _
    '        '        "	 SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
    '        '        "	,SUM(ISNULL(PAYE.allowance, 0))-(SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.housing, 0))) AS allowancebenefit " & _
    '        '        "	,SUM(ISNULL(PAYE.housing, 0)) AS housing " & _
    '        '        "	,SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.allowance, 0))-(SUM(ISNULL(PAYE.basicsalary, 0))+ SUM(ISNULL(PAYE.housing, 0))) + SUM(ISNULL(Period.housing, 0)) AS Grosspay " & _
    '        '        "	,SUM(ISNULL(PAYE.deduction, 0)) AS deduction " & _
    '        '        "	,(SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.allowance, 0))-(SUM(ISNULL(PAYE.basicsalary, 0))+ SUM(ISNULL(PAYE.housing, 0))) + SUM(ISNULL(PAYE.housing, 0)) ) - SUM(ISNULL(PAYE.deduction, 0)) AS taxable " & _
    '        '        "	,SUM(ISNULL(PAYE.taxpayable, 0)) - SUM(ISNULL(PAYE.relief, 0)) AS taxdue " & _
    '        '        "	,paye.pid AS pid " & _
    '        '        "	,paye.empid AS empid " & _
    '        '        "FROM " & _
    '        '        "( " & _
    '        '        "	SELECT " & _
    '        '        "		 ISNULL(vwPayroll.amount, 0) AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll " & _
    '        '        "	WHERE vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '        '        "		AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,ISNULL(vwPayroll.amount, 0) AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll " & _
    '        '        "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
    '        '        "	WHERE vwPayroll.tranheadunkid = @HousingTranId " & _
    '        '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '        '        "		AND prtranhead_master.istaxable = 1 " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,ISNULL(vwPayroll.amount, 0) AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll " & _
    '        '        "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
    '        '        "	WHERE prtranhead_master.istaxable = 1 " & _
    '        '        "		AND vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,ISNULL(vwPayroll.amount, 0) AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll " & _
    '        '        "	WHERE vwPayroll.tranheadunkid IN (" & mstrStatutoryIds & ") " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,ISNULL(vwPayroll.amount, 0) AS taxpayable " & _
    '        '        "		,0 AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll " & _
    '        '        "	WHERE vwPayroll.typeof_id = " & enTypeOf.Taxes & "  " & _
    '        '        "UNION ALL " & _
    '        '        "	SELECT " & _
    '        '        "		 0 AS basicsalary " & _
    '        '        "		,0 AS allowancebenefit " & _
    '        '        "		,0 AS housing " & _
    '        '        "		,0 AS deduction " & _
    '        '        "		,0 AS taxpayable " & _
    '        '        "		,ISNULL(vwPayroll.amount, 0) AS relief " & _
    '        '        "		,vwPayroll.employeeunkid AS empid " & _
    '        '        "		,vwPayroll.payperiodunkid AS pid " & _
    '        '        "	FROM vwPayroll " & _
    '        '        "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
    '        '        "	WHERE vwPayroll.tranheadunkid NOT IN (-1) " & _
    '        '        "		AND prtranhead_master.istaxrelief = 1 " & _
    '        '        ") AS PAYE " & _
    '        '        "WHERE   pid IN (" & mstrPeriodIds & ") "
    '        'S.SANDEEP [ 19 JUNE 2012 ] -- END


    '        Call FilterTitleAndFilterQuery()
    '        StrQ &= Me._FilterQuery

    '        'S.SANDEEP [ 13 DEC 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        '   StrQ &= "GROUP BY empid,pid "
    '        'S.SANDEEP [ 13 DEC 2012 ] -- END



    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dtTemp() As DataRow = dsList.Tables(0).Select("basicpay = 0 AND housing = 0 AND allowancebenefit = 0 AND Grosspay = 0 AND deduction = 0 AND taxable = 0 AND taxdue = 0")

    '        If dtTemp.Length > 0 Then
    '            For i As Integer = 0 To dtTemp.Length - 1
    '                dsList.Tables(0).Rows.Remove(dtTemp(i))
    '            Next
    '            dsList.Tables(0).AcceptChanges()
    '        End If

    '        Dim dsEmployee As DataSet = Nothing

    '        StrQ = "SELECT " & _
    '               "	 ISNULL(firstname,'') +' '+ ISNULL(othername,'') +' '+ ISNULL(surname,'') AS empname " & _
    '               "	,hremployee_master.employeecode AS empcode " & _
    '               "	,ISNULL(hremployee_master.present_address1,'') +' '+ ISNULL(hremployee_master.present_address2,'') AS Address " & _
    '               "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS city " & _
    '               " 	,hremployee_master.employeeunkid " & _
    '               "FROM hremployee_master " & _
    '               "	LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = hremployee_master.present_post_townunkid " & _
    '                " WHERE 1 = 1 "

    '        If mintEmpId > 0 Then
    '            objDataOperation.AddParameter("@EmplId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
    '            StrQ &= "AND employeeunkid = @EmplId "
    '        End If




    '        dsEmployee = objDataOperation.ExecQuery(StrQ, "Emp")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim rpt_Row As DataRow = Nothing
    '        Dim blnIsAdded As Boolean = False
    '        Dim intCnt As Integer = 1

    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        Dim decColumn6Total, decColumn7Total, decColumn8Total, decColumn9Total, decColumn10Total, decColumn11Total, decColumn12Total As Decimal
    '        decColumn6Total = 0 : decColumn7Total = 0 : decColumn8Total = 0 : decColumn9Total = 0 : decColumn10Total = 0 : decColumn11Total = 0 : decColumn12Total = 0
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 

    '        For Each dtRow As DataRow In dsEmployee.Tables(0).Rows
    '            dtFilterTable = New DataView(dsList.Tables(0), "empid = '" & dtRow.Item("employeeunkid") & "'", "", DataViewRowState.CurrentRows).ToTable
    '            blnIsAdded = False
    '            If dtFilterTable.Rows.Count > 0 Then
    '                For Each drRow As DataRow In dtFilterTable.Rows
    '                    If blnIsAdded = False Then
    '                        rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '                        rpt_Row.Item("Column1") = intCnt
    '                        rpt_Row.Item("Column2") = dtRow.Item("empname")
    '                        rpt_Row.Item("Column3") = dtRow.Item("empcode")
    '                        rpt_Row.Item("Column4") = dtRow.Item("Address")
    '                        rpt_Row.Item("Column5") = dtRow.Item("city")

    '                        rpt_Row.Item("Column6") = Format(CDec(drRow.Item("basicpay")), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column7") = Format(CDec(drRow.Item("housing")), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column8") = Format(CDec(drRow.Item("allowancebenefit")), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column9") = Format(CDec(drRow.Item("Grosspay")), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column10") = Format(CDec(drRow.Item("deduction")), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column11") = Format(CDec(drRow.Item("taxable")), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column12") = Format(CDec(drRow.Item("taxdue")), GUI.fmtCurrency)
    '                        blnIsAdded = True
    '                    Else
    '                        rpt_Row.Item("Column6") = Format(CDec(CDec(rpt_Row.Item("Column6")) + CDec(drRow.Item("basicpay"))), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column7") = Format(CDec(CDec(rpt_Row.Item("Column7")) + CDec(drRow.Item("housing"))), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column8") = Format(CDec(CDec(rpt_Row.Item("Column8")) + CDec(drRow.Item("allowancebenefit"))), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column9") = Format(CDec(CDec(rpt_Row.Item("Column9")) + CDec(drRow.Item("Grosspay"))), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column10") = Format(CDec(CDec(rpt_Row.Item("Column10")) + CDec(drRow.Item("deduction"))), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column11") = Format(CDec(CDec(rpt_Row.Item("Column11")) + CDec(drRow.Item("taxable"))), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column12") = Format(CDec(CDec(rpt_Row.Item("Column12")) + CDec(drRow.Item("taxdue"))), GUI.fmtCurrency)
    '                    End If

    '                    'S.SANDEEP [ 05 JULY 2011 ] -- START
    '                    'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '                    decColumn6Total = decColumn6Total + CDec(drRow.Item("basicpay"))
    '                    decColumn7Total = decColumn7Total + CDec(drRow.Item("housing"))
    '                    decColumn8Total = decColumn8Total + CDec(drRow.Item("allowancebenefit"))
    '                    decColumn9Total = decColumn9Total + CDec(drRow.Item("Grosspay"))
    '                    decColumn10Total = decColumn10Total + CDec(drRow.Item("deduction"))
    '                    decColumn11Total = decColumn11Total + CDec(drRow.Item("taxable"))
    '                    decColumn12Total = decColumn12Total + CDec(drRow.Item("taxdue"))
    '                    'S.SANDEEP [ 05 JULY 2011 ] -- END 
    '                Next
    '                intCnt += 1
    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            End If
    '        Next

    '        objRpt = New ArutiReport.Designer.rptPAYEPaymentTax

    '        objRpt.SetDataSource(rpt_Data)

    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblTRA", Language.getMessage(mstrModuleName, 37, "TANZANIA REVENUE AUTHORITY")) 'Start From 37
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPAYE", Language.getMessage(mstrModuleName, 38, "P.A.Y.E."))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblReportName", Language.getMessage(mstrModuleName, 39, "STATEMENT AND PAYMENT OF TAX WITHHELD"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblYear", Language.getMessage(mstrModuleName, 40, "YEAR:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtYear", FinancialYear._Object._FinancialYear_Name)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblTIN", Language.getMessage(mstrModuleName, 41, "TIN:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtTIN", Company._Object._Tinno)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod", Language.getMessage(mstrModuleName, 42, "Period: (Please tick the appropriate box) "))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod1", Language.getMessage(mstrModuleName, 43, "From 1 January to 30 June "))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod2", Language.getMessage(mstrModuleName, 44, "From 1 January to 31 December "))

    '        'S.SANDEEP [ 12 MAY 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        If mstrPeriodNames.Trim.Length > 0 Then
    '            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPeriodNames", Language.getMessage(mstrModuleName, 81, "Periods : (") & mstrPeriodNames & Language.getMessage(mstrModuleName, 82, ")"))
    '        Else
    '            ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPeriodNames", "")
    '        End If
    '        'S.SANDEEP [ 12 MAY 2012 ] -- END


    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblEmployer", Language.getMessage(mstrModuleName, 45, "Name of Employer:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtEmployer", Company._Object._Name)

    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPostalAddress", Language.getMessage(mstrModuleName, 46, "Postal Address:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPoBox", Language.getMessage(mstrModuleName, 47, "P. O. Box"))
    '        Dim objZip As New clszipcode_master
    '        objZip._Zipcodeunkid = Company._Object._Postalunkid
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPoBox", objZip._Zipcode_No)
    '        objZip = Nothing
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPostalCity", Language.getMessage(mstrModuleName, 48, "Postal City"))
    '        Dim objCity As New clscity_master
    '        objCity._Cityunkid = Company._Object._Cityunkid
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPostalCity", objCity._Name)
    '        objCity = Nothing
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblContactNumbers", Language.getMessage(mstrModuleName, 49, "Contact Numbers:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPhone", Language.getMessage(mstrModuleName, 50, "Phone number"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPhone1", Company._Object._Phone1)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblSecondPhone", Language.getMessage(mstrModuleName, 51, "Second Phone"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtSecondPhone", Company._Object._Phone2)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblThirdPhone", Language.getMessage(mstrModuleName, 52, "Third Phone"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtThirdphone", Company._Object._Phone3)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblFax", Language.getMessage(mstrModuleName, 53, " Fax number"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtFax", Company._Object._Fax)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblEmail", Language.getMessage(mstrModuleName, 54, "E-mail address:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtEmail", Company._Object._Email)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPhysical", Language.getMessage(mstrModuleName, 55, "Physical Address:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPlot", Language.getMessage(mstrModuleName, 56, "Plot Number"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPlot", Company._Object._Reg1_Value)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblBlock", Language.getMessage(mstrModuleName, 57, "Block Number"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtBlock", Company._Object._Reg2_Value)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblStreet", Language.getMessage(mstrModuleName, 58, "Street/Location"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtStreet", Company._Object._Address1 & " " & Company._Object._Address2)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblBranch", Language.getMessage(mstrModuleName, 59, "Name of Branch"))
    '        Dim objBranch As New clsbankbranch_master
    '        objBranch._Branchunkid = Company._Object._Branchunkid
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtBranch", objBranch._Branchname)
    '        objBranch = Nothing
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPaymentStatement", Language.getMessage(mstrModuleName, 60, "ITX215.01.E -  P.A.Y.E.-Statement"))


    '        ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 61, "P.A.Y.E. - DETAILS OF PAYMENT OF TAX WITHHELD"))
    '        ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 62, "Name of Employer :"))
    '        ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
    '        ReportFunction.TextChange(objRpt, "lblTIN", Language.getMessage(mstrModuleName, 63, "TIN :"))
    '        ReportFunction.TextChange(objRpt, "txtTIN", Company._Object._Tinno)
    '        ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 64, "S/No."))
    '        ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 65, "Name Of Employee"))
    '        ReportFunction.TextChange(objRpt, "txtPayroll", Language.getMessage(mstrModuleName, 66, "Payroll No."))
    '        ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 67, "Postal Address"))
    '        ReportFunction.TextChange(objRpt, "txtCity", Language.getMessage(mstrModuleName, 68, "Postal City"))
    '        ReportFunction.TextChange(objRpt, "txtPay", Language.getMessage(mstrModuleName, 69, "Basic Pay"))
    '        ReportFunction.TextChange(objRpt, "txtHousing", Language.getMessage(mstrModuleName, 70, "Housing"))
    '        ReportFunction.TextChange(objRpt, "txtAllBenefit", Language.getMessage(mstrModuleName, 71, "Allowances And Benefits"))
    '        ReportFunction.TextChange(objRpt, "txtGrossPay", Language.getMessage(mstrModuleName, 72, "Grosspay"))
    '        ReportFunction.TextChange(objRpt, "txtDeduction", Language.getMessage(mstrModuleName, 73, "Deductions"))
    '        ReportFunction.TextChange(objRpt, "txtTaxable", Language.getMessage(mstrModuleName, 74, "Taxable"))
    '        ReportFunction.TextChange(objRpt, "txtTaxDue", Language.getMessage(mstrModuleName, 75, "Tax Due"))
    '        ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 76, "Total"))


    '        Call ReportFunction.TextChange(objRpt, "txtTotal6", Format(decColumn6Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal7", Format(decColumn7Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal8", Format(decColumn8Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal9", Format(decColumn9Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal10", Format(decColumn10Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal11", Format(decColumn11Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal12", Format(decColumn12Total, GUI.fmtCurrency))

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_PAYE_Details; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function
#End Region
    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        StrQ = "SELECT " & _
    '                "	 Period.Pid " & _
    '                "	,SUM(ISNULL(Period.basicsalary,0)) AS basicpay " & _
    '                "	,SUM(ISNULL(Period.housing,0))  AS housing " & _
    '                "	,SUM(ISNULL(Period.allowance,0)) AS allowancebenefit " & _
    '                "	,SUM(ISNULL(Period.basicsalary,0))+SUM(ISNULL(Period.allowance,0))+SUM(ISNULL(Period.housing,0)) AS TotalPay " & _
    '                "   ,(SUM(ISNULL(Period.basicsalary,0))+SUM(ISNULL(Period.allowance,0))+SUM(ISNULL(Period.housing,0)))-SUM(ISNULL(Pension,0)) As TaxablePay " & _
    '                "	,SUM(ISNULL(Pension,0)) AS Pension " & _
    '                "	,SUM(ISNULL(TaxPayable,0)) AS TaxPayable " & _
    '                "	,SUM(ISNULL(LPR,0)) AS LPR " & _
    '                "	,SUM(ISNULL(TaxPayable,0)) -SUM(ISNULL(LPR,0)) AS NTD " & _
    '                "	,ISNULL(empid,-1) AS empid " & _
    '                "FROM " & _
    '                "( " & _
    '                "	SELECT " & _
    '                "		 ISNULL(prpayrollprocess_tran.amount,0) as basicsalary " & _
    '                "		,0 AS housing " & _
    '                "		,0 AS allowance " & _
    '                "		,0 AS Pension " & _
    '                "		,0 AS TaxPayable " & _
    '                "		,0 AS LPR " & _
    '                "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                "		,prtnaleave_tran.payperiodunkid AS Pid " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "		AND prtranhead_master.trnheadtype_id =1 " & _
    '                "		AND prtranhead_master.typeof_id=1 " & _
    '                "		AND prpayrollprocess_tran.tranheadunkid NOT IN(-1) "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 AS basicsalary " & _
    '                "		,ISNULL(prpayrollprocess_tran.amount,0) as housing " & _
    '                "		,0 AS allowance " & _
    '                "		,0 AS Pension " & _
    '                "		,0 AS TaxPayable " & _
    '                "		,0 AS LPR " & _
    '                "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                "		,prtnaleave_tran.payperiodunkid AS Pid " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "		AND prtranhead_master.tranheadunkid=@HousingTranId "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 as basicsalary " & _
    '                "		,0 AS housing " & _
    '                "		,SUM(ISNULL(prpayrollprocess_tran.amount,0)) as allowance " & _
    '                "		,0 AS Pension " & _
    '                "		,0 AS TaxPayable " & _
    '                "		,0 AS LPR " & _
    '                "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                "		,prtnaleave_tran.payperiodunkid AS Pid " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "		AND prtranhead_master.trnheadtype_id =1 " & _
    '                "		AND prtranhead_master.typeof_id IN (2,5) " & _
    '                "		AND prpayrollprocess_tran.tranheadunkid NOT IN(-1) "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "	GROUP BY prtnaleave_tran.employeeunkid,prtnaleave_tran.payperiodunkid " & _
    '                "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 as basicsalary " & _
    '                "		,0 AS housing " & _
    '                "		,0 as allowance " & _
    '                "		,SUM(ISNULL(prpayrollprocess_tran.amount,0)) AS Pension " & _
    '                "		,0 AS TaxPayable " & _
    '                "		,0 AS LPR " & _
    '                "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                "		,prtnaleave_tran.payperiodunkid AS Pid " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "		AND prtranhead_master.trnheadtype_id =3 " & _
    '                "		AND prtranhead_master.tranheadunkid IN (" & mstrStatutoryIds & ") "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "	GROUP BY prtnaleave_tran.employeeunkid,prtnaleave_tran.payperiodunkid " & _
    '                "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 as basicsalary " & _
    '                "		,0 AS housing " & _
    '                "		,0 as allowance " & _
    '                "		,0 AS Pension " & _
    '                "		,SUM(ISNULL(prpayrollprocess_tran.amount,0)) AS TaxPayable " & _
    '                "		,0 AS LPR " & _
    '                "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                "		,prtnaleave_tran.payperiodunkid AS Pid " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "		AND prtranhead_master.typeof_id = 9 " & _
    '                "		AND prpayrollprocess_tran.tranheadunkid NOT IN(-1) "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "	GROUP BY prtnaleave_tran.employeeunkid,prtnaleave_tran.payperiodunkid " & _
    '                "UNION  ALL " & _
    '                "	SELECT " & _
    '                "		 0 as basicsalary " & _
    '                "		,0 AS housing " & _
    '                "		,0 as allowance " & _
    '                "		,0 AS Pension " & _
    '                "		,0 AS TaxPayable " & _
    '                "		,SUM(ISNULL(prpayrollprocess_tran.amount,0)) AS LPR " & _
    '                "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                "		,prtnaleave_tran.payperiodunkid AS Pid " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.istaxrelief,0)=1 " & _
    '                "		AND prpayrollprocess_tran.tranheadunkid NOT IN(-1) "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "	GROUP BY prtnaleave_tran.employeeunkid,prtnaleave_tran.payperiodunkid " & _
    '                "       ,prpayrollprocess_tran.tranheadunkid " & _
    '                ") AS Period WHERE 1 = 1 "
    '        Call FilterTitleAndFilterQuery()
    '        StrQ &= Me._FilterQuery
    '        StrQ &= "GROUP BY Period.Pid ,ISNULL(empid,-1) "

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dsEmployee As DataSet = Nothing
    '        'S.SANDEEP [ 24 JUNE 2011 ] -- START
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'StrQ = "SELECT " & _
    '        '                   "	 employeeunkid AS EId " & _
    '        '                   "	,employeecode AS Code " & _
    '        '                   "	,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS MainName " & _
    '        '                   "	,ISNULL(othername,'') AS OtherName " & _
    '        '                   "FROM hremployee_master " & _
    '        '                   "WHERE isactive = 1 "

    '        StrQ = "SELECT " & _
    '               "	 employeeunkid AS EId " & _
    '               "	,employeecode AS Code " & _
    '               "	,ISNULL(firstname,'')+' '+ISNULL(surname,'') AS MainName " & _
    '               "	,ISNULL(othername,'') AS OtherName " & _
    '               "FROM hremployee_master WHERE 1 = 1  "

    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND isactive = 1 "
    '        End If
    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    '        If mintEmpId > 0 Then
    '            objDataOperation.AddParameter("@EmplId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
    '            StrQ &= "AND employeeunkid = @EmplId "
    '        End If

    '        dsEmployee = objDataOperation.ExecQuery(StrQ, "Emp")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dsPeriod As DataSet = Nothing
    '        Dim objPeriod As New clscommom_period_Tran


    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        'dsPeriod = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False, 1)
    '        dsPeriod = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 

    '        mintPeriodCount = dsPeriod.Tables(0).Rows.Count

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport
    '        Dim intCnt As Integer = 1
    '        Dim rpt_Row As DataRow = Nothing

    '        Dim dicTaxableTotalFirst6Month As New Dictionary(Of Integer, Decimal)  'EmployeeId,Total For First Six Month
    '        Dim dicTaxableTotalNextt6Month As New Dictionary(Of Integer, Decimal)  'EmployeeId,Total For Next Six Month
    '        Dim dicDivideByFirst6Month As New Dictionary(Of Integer, Integer)   'EmployeeId, DivideValue For First Six Month
    '        Dim dicDivideByNext6Month As New Dictionary(Of Integer, Integer)   'EmployeeId, DivideValue For Next Six Month
    '        Dim intCntSixMonth As Integer = 1

    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        Dim decColumn2GrpTotal, decColumn3GrpTotal, decColumn4GrpTotal, decColumn5GrpTotal, decColumn6GrpTotal, decColumn7GrpTotal, decColumn8GrpTotal, decColumn9GrpTotal, decColumn10GrpTotal As Decimal
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 


    '        For Each dtERow As DataRow In dsEmployee.Tables(0).Rows
    '            intCnt = 1
    '            intCntSixMonth = 1
    '            decColumn2GrpTotal = 0 : decColumn3GrpTotal = 0 : decColumn4GrpTotal = 0 : decColumn5GrpTotal = 0 : decColumn6GrpTotal = 0 : decColumn7GrpTotal = 0 : decColumn8GrpTotal = 0 : decColumn9GrpTotal = 0 : decColumn10GrpTotal = 0
    '            For Each dtPRow As DataRow In dsPeriod.Tables(0).Rows
    '                Dim dtTempRow() As DataRow
    '                dtTempRow = dsList.Tables(0).Select("Empid = '" & dtERow.Item("EId") & "' AND Pid = '" & dtPRow.Item("periodunkid") & "'")
    '                If dtTempRow.Length > 0 Then
    '                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
    '                    rpt_Row.Item("Column1") = dtPRow.Item("name")
    '                    rpt_Row.Item("Column2") = Format(CDec(dtTempRow(0).Item("basicpay")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column3") = Format(CDec(dtTempRow(0).Item("housing")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column4") = Format(CDec(dtTempRow(0).Item("allowancebenefit")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column5") = Format(CDec(dtTempRow(0).Item("TotalPay")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column6") = Format(CDec(dtTempRow(0).Item("Pension")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column7") = Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column8") = Format(CDec(dtTempRow(0).Item("TaxPayable")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column9") = Format(CDec(dtTempRow(0).Item("LPR")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column10") = Format(CDec(dtTempRow(0).Item("NTD")), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column11") = Company._Object._Name
    '                    rpt_Row.Item("Column12") = Company._Object._Address1
    '                    rpt_Row.Item("Column13") = Company._Object._Address2
    '                    rpt_Row.Item("Column14") = dtERow.Item("EId")
    '                    rpt_Row.Item("Column15") = dtERow.Item("OtherName")
    '                    rpt_Row.Item("Column16") = dtERow.Item("Code")
    '                    rpt_Row.Item("Column17") = Company._Object._Tinno
    '                    rpt_Row.Item("Column18") = "" 'Left Blank
    '                    rpt_Row.Item("Column19") = dtERow.Item("MainName")

    '                    'S.SANDEEP [ 05 JULY 2011 ] -- START
    '                    'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '                    decColumn2GrpTotal = decColumn2GrpTotal + CDec(dtTempRow(0).Item("basicpay"))
    '                    decColumn3GrpTotal = decColumn3GrpTotal + CDec(dtTempRow(0).Item("housing"))
    '                    decColumn4GrpTotal = decColumn4GrpTotal + CDec(dtTempRow(0).Item("allowancebenefit"))
    '                    decColumn5GrpTotal = decColumn5GrpTotal + CDec(dtTempRow(0).Item("TotalPay"))
    '                    decColumn6GrpTotal = decColumn6GrpTotal + CDec(dtTempRow(0).Item("Pension"))
    '                    decColumn7GrpTotal = decColumn7GrpTotal + CDec(dtTempRow(0).Item("TaxablePay"))
    '                    decColumn8GrpTotal = decColumn8GrpTotal + CDec(dtTempRow(0).Item("TaxPayable"))
    '                    decColumn9GrpTotal = decColumn9GrpTotal + CDec(dtTempRow(0).Item("LPR"))
    '                    decColumn10GrpTotal = decColumn10GrpTotal + CDec(dtTempRow(0).Item("NTD"))
    '                    'S.SANDEEP [ 05 JULY 2011 ] -- END 



    '                    '/*************** SETTING DIVIDE EMPLOYEE WISE ***************/
    '                    If intCntSixMonth <= 6 Then
    '                        If dicDivideByFirst6Month.ContainsKey(dtERow.Item("EId")) Then
    '                            dicDivideByFirst6Month(dtERow.Item("EId")) += 1
    '                        Else
    '                            dicDivideByFirst6Month.Add(dtERow.Item("EId"), 1)
    '                        End If

    '                        If dicTaxableTotalFirst6Month.ContainsKey(dtERow.Item("EId")) Then
    '                            dicTaxableTotalFirst6Month(dtERow.Item("EId")) += Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
    '                        Else
    '                            dicTaxableTotalFirst6Month.Add(dtERow.Item("EId"), Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency))
    '                        End If
    '                        intCntSixMonth += 1
    '                    ElseIf intCntSixMonth >= 7 Then
    '                        If dicDivideByNext6Month.ContainsKey(dtERow.Item("EId")) Then
    '                            dicDivideByNext6Month(dtERow.Item("EId")) += 1
    '                        Else
    '                            dicDivideByNext6Month.Add(dtERow.Item("EId"), 1)
    '                        End If

    '                        If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
    '                            dicTaxableTotalNextt6Month(dtERow.Item("EId")) += Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency)
    '                        Else
    '                            dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), Format(CDec(dtTempRow(0).Item("TaxablePay")), GUI.fmtCurrency))
    '                        End If
    '                        intCntSixMonth += 1
    '                    End If
    '                Else
    '                    rpt_Row = rpt_Data.Tables("ArutiTable").NewRow
    '                    rpt_Row.Item("Column1") = dtPRow.Item("name")
    '                    rpt_Row.Item("Column2") = Format(CDec(0), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column3") = Format(CDec(0), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column4") = Format(CDec(0), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column5") = Format(CDec(0), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column6") = Format(CDec(0), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column7") = Format(CDec(0), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column8") = Format(CDec(0), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column9") = Format(CDec(0), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column10") = Format(CDec(0), GUI.fmtCurrency)
    '                    rpt_Row.Item("Column11") = Company._Object._Name
    '                    rpt_Row.Item("Column12") = Company._Object._Address1
    '                    rpt_Row.Item("Column13") = Company._Object._Address2
    '                    rpt_Row.Item("Column14") = dtERow.Item("EId")
    '                    rpt_Row.Item("Column15") = dtERow.Item("OtherName")
    '                    rpt_Row.Item("Column16") = dtERow.Item("Code")
    '                    rpt_Row.Item("Column17") = Company._Object._Tinno
    '                    rpt_Row.Item("Column18") = "" 'Left Blank
    '                    rpt_Row.Item("Column19") = dtERow.Item("MainName")

    '                    If intCntSixMonth <= 6 Then
    '                        If dicDivideByFirst6Month.ContainsKey(dtERow.Item("EId")) Then
    '                            dicDivideByFirst6Month(dtERow.Item("EId")) += 0
    '                        Else
    '                            dicDivideByFirst6Month.Add(dtERow.Item("EId"), 0)
    '                        End If

    '                        If dicTaxableTotalFirst6Month.ContainsKey(dtERow.Item("EId")) Then
    '                            dicTaxableTotalFirst6Month(dtERow.Item("EId")) += 0
    '                        Else
    '                            dicTaxableTotalFirst6Month.Add(dtERow.Item("EId"), 0)
    '                        End If
    '                        intCntSixMonth += 1
    '                    ElseIf intCntSixMonth >= 7 Then
    '                        If dicDivideByNext6Month.ContainsKey(dtERow.Item("EId")) Then
    '                            dicDivideByNext6Month(dtERow.Item("EId")) += 0
    '                        Else
    '                            dicDivideByNext6Month.Add(dtERow.Item("EId"), 0)
    '                        End If

    '                        If dicTaxableTotalNextt6Month.ContainsKey(dtERow.Item("EId")) Then
    '                            dicTaxableTotalNextt6Month(dtERow.Item("EId")) += 0
    '                        Else
    '                            dicTaxableTotalNextt6Month.Add(dtERow.Item("EId"), 0)
    '                        End If
    '                        intCntSixMonth += 1
    '                    End If
    '                End If
    '                Select Case intCnt
    '                    Case 1
    '                        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 28, "Information required from the" & vbCrLf & "Employer at the end of the year") & vbCrLf
    '                    Case 2
    '                        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 29, "1. Date commenced/left if during the year:-")
    '                    Case 3
    '                        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 30, "2. Name and address of OLD/NEW employer:-")
    '                    Case 4
    '                        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 31, "3. If employee received benefits other than cash totalling to more than Shs. 1000/= in the year, please give particulars and ")
    '                    Case 5
    '                        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 32, "4. Where housing is provided show monthly rent charged to employee Shs .....................")
    '                    Case 6
    '                        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 33, "5. Where lumpsum payment is made state NATURE and AMOUNT ..............................")
    '                    Case 7
    '                        rpt_Row.Item("Column20") = Language.getMessage(mstrModuleName, 34, "6. where any of the Pay relates to a period other than this year e.g. give the following details:.")
    '                End Select
    '                intCnt += 1
    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            Next

    '            'S.SANDEEP [ 05 JULY 2011 ] -- START
    '            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '            Dim rpt_dRow() As DataRow = rpt_Data.Tables("ArutiTable").Select("Column14 = '" & dtERow.Item("EId") & "'")
    '            If rpt_dRow.Length > 0 Then
    '                For i As Integer = 0 To rpt_dRow.Length - 1
    '                    rpt_dRow(i).Item("Column72") = Format(decColumn2GrpTotal, GUI.fmtCurrency)
    '                    rpt_dRow(i).Item("Column73") = Format(decColumn3GrpTotal, GUI.fmtCurrency)
    '                    rpt_dRow(i).Item("Column74") = Format(decColumn4GrpTotal, GUI.fmtCurrency)
    '                    rpt_dRow(i).Item("Column75") = Format(decColumn5GrpTotal, GUI.fmtCurrency)
    '                    rpt_dRow(i).Item("Column76") = Format(decColumn6GrpTotal, GUI.fmtCurrency)
    '                    rpt_dRow(i).Item("Column77") = Format(decColumn7GrpTotal, GUI.fmtCurrency)
    '                    rpt_dRow(i).Item("Column78") = Format(decColumn8GrpTotal, GUI.fmtCurrency)
    '                    rpt_dRow(i).Item("Column79") = Format(decColumn9GrpTotal, GUI.fmtCurrency)
    '                    rpt_dRow(i).Item("Column80") = Format(decColumn10GrpTotal, GUI.fmtCurrency)

    '                    rpt_Data.Tables("ArutiTable").AcceptChanges()
    '                Next
    '            End If
    '            'S.SANDEEP [ 05 JULY 2011 ] -- END 
    '        Next

    '        '///////// Performing Calculation For Particular Employee ///////// -- Start
    '        Dim objTrnTaxSlab As New clsTranheadInexcessslabTran
    '        Dim dtExcess_Slab As DataTable = Nothing

    '        StrQ = "SELECT DISTINCT " & _
    '               "	 prpayrollprocess_tran.tranheadunkid AS HeadId " & _
    '               "	,prpayrollprocess_tran.employeeunkid AS EmpId " & _
    '               "FROM prpayrollprocess_tran,prtranhead_master, hremployee_master " & _
    '               "WHERE prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '               "AND hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid" & _
    '               "	AND prtranhead_master.typeof_id = 9 AND prtranhead_master.calctype_id = 2 "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        dsList = objDataOperation.ExecQuery(StrQ, "Heads")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dblAvgIncomeFirst6Months As Decimal = 0
    '        Dim dblAvgIncomeNext6Months As Decimal = 0
    '        Dim intRowIndex As Integer = -1
    '        Dim dblTaxPerForFirst6Month As Decimal = 0
    '        Dim dblTaxPerForNext6Month As Decimal = 0
    '        Dim intTranHeadId As Integer = 0
    '        Dim dblFirst6MonthPAYE As Decimal = 0
    '        Dim dblNext6MonthPAYE As Decimal = 0
    '        Dim dblTaxFirstYear As Decimal = 0
    '        Dim dblTaxNextYear As Decimal = 0
    '        Dim strSting As String = String.Empty
    '        Dim strBuilder As New StringBuilder
    '        Dim writer As New StringWriter()


    '        For k As Integer = 0 To dsEmployee.Tables(0).Rows.Count - 1
    '            Dim drRowIdx() As DataRow = rpt_Data.Tables(0).Select("Column14='" & dsEmployee.Tables(0).Rows(k).Item("EId") & "'")

    '            If drRowIdx.Length > 0 Then
    '                intRowIndex = rpt_Data.Tables(0).Rows.IndexOf(drRowIdx(0))
    '            End If

    '            Dim dtTemp() As DataRow = dsList.Tables(0).Select("EmpId = '" & dsEmployee.Tables(0).Rows(k).Item("EId") & "'")

    '            If dtTemp.Length > 0 Then
    '                intTranHeadId = dtTemp(0)("HeadId")
    '            End If

    '            objTrnTaxSlab._Tranheadunkid = intTranHeadId
    '            dtExcess_Slab = objTrnTaxSlab._Datasource
    '            'Sandeep [ 01 MARCH 2011 ] -- Start
    '            'If dicDivideByFirst6Month.ContainsKey(dsEmployee.Tables(0).Rows(k).Item("EId")) And dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) > 0 Then
    '            '    dblAvgIncomeFirst6Months = Format(cdec(IIf(dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) = 0, 0, dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
    '            '    dblTaxPerForFirst6Month = Format(cdec((dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / 6)), GUI.fmtCurrency) '******* TAX PER MONTH For Paye Table 1
    '            '    If dtExcess_Slab.Rows.Count > 0 Then
    '            '        Dim drTemp As DataRow() = dtExcess_Slab.Select("amountupto >= " & dblTaxPerForFirst6Month & "", "amountupto ASC")
    '            '        If drTemp.Length > 0 Then
    '            '            dblFirst6MonthPAYE = (dblTaxPerForFirst6Month - cdec(drTemp(0).Item("inexcessof"))) * cdec(drTemp(0).Item("ratepayable"))
    '            '            If dblFirst6MonthPAYE <> 0 Then
    '            '                dblFirst6MonthPAYE = dblFirst6MonthPAYE / 100
    '            '            Else
    '            '                dblFirst6MonthPAYE = 0
    '            '            End If
    '            '            dblFirst6MonthPAYE += cdec(drTemp(0).Item("fixedamount"))
    '            '        Else
    '            '            dblFirst6MonthPAYE = 0
    '            '        End If
    '            '    End If
    '            'End If
    '            'If dicDivideByNext6Month.ContainsKey(dsEmployee.Tables(0).Rows(k).Item("EId")) And dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) > 0 Then
    '            '    dblAvgIncomeFirst6Months = Format(cdec(IIf(dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) = 0, 0, dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
    '            '    dblTaxPerForNext6Month = Format(cdec((dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / 6)), GUI.fmtCurrency) '*******  TAX PER MONTH  For Paye Table 2
    '            '    If dtExcess_Slab.Rows.Count > 0 Then
    '            '        Dim drTemp As DataRow() = dtExcess_Slab.Select("amountupto >= " & dblTaxPerForFirst6Month & "", "amountupto ASC")
    '            '        If drTemp.Length > 0 Then
    '            '            dblNext6MonthPAYE = (dblTaxPerForNext6Month - cdec(drTemp(0).Item("inexcessof"))) * cdec(drTemp(0).Item("ratepayable"))
    '            '            If dblNext6MonthPAYE <> 0 Then
    '            '                dblNext6MonthPAYE = dblNext6MonthPAYE / 100
    '            '            Else
    '            '                dblNext6MonthPAYE = 0
    '            '            End If
    '            '            dblNext6MonthPAYE += cdec(drTemp(0).Item("fixedamount"))
    '            '        Else
    '            '            dblNext6MonthPAYE = 0
    '            '        End If
    '            '    End If
    '            'End If
    '            'dblTaxFirstYear = Format(cdec((dblFirst6MonthPAYE * dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
    '            'dblTaxNextYear = Format(cdec((dblNext6MonthPAYE * dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
    '            If dicDivideByFirst6Month.Keys.Count > 0 Then
    '                If dicDivideByFirst6Month.ContainsKey(dsEmployee.Tables(0).Rows(k).Item("EId")) And dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) > 0 Then
    '                    dblAvgIncomeFirst6Months = Format(CDec(IIf(dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) = 0, 0, dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
    '                    dblTaxPerForFirst6Month = Format(CDec((dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / 6)), GUI.fmtCurrency) '******* TAX PER MONTH For Paye Table 1
    '                    If dtExcess_Slab.Rows.Count > 0 Then
    '                        Dim drTemp As DataRow() = dtExcess_Slab.Select("amountupto >= " & dblTaxPerForFirst6Month & "", "amountupto ASC")
    '                        If drTemp.Length > 0 Then
    '                            dblFirst6MonthPAYE = (dblTaxPerForFirst6Month - CDec(drTemp(0).Item("inexcessof"))) * CDec(drTemp(0).Item("ratepayable"))
    '                            If dblFirst6MonthPAYE <> 0 Then
    '                                dblFirst6MonthPAYE = dblFirst6MonthPAYE / 100
    '                            Else
    '                                dblFirst6MonthPAYE = 0
    '                            End If
    '                            dblFirst6MonthPAYE += CDec(drTemp(0).Item("fixedamount"))
    '                        Else
    '                            dblFirst6MonthPAYE = 0
    '                        End If
    '                    End If
    '                End If
    '            End If

    '            If dicDivideByNext6Month.Keys.Count > 0 Then
    '                If dicDivideByNext6Month.ContainsKey(dsEmployee.Tables(0).Rows(k).Item("EId")) And dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) > 0 Then
    '                    'S.SANDEEP [ 05 JULY 2011 ] -- START
    '                    'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '                    'dblAvgIncomeFirst6Months = Format(CDec(IIf(dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) = 0, 0, dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
    '                    dblAvgIncomeNext6Months = Format(CDec(IIf(dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) = 0, 0, dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
    '                    'S.SANDEEP [ 05 JULY 2011 ] -- END 
    '                    dblTaxPerForNext6Month = Format(CDec((dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) / 6)), GUI.fmtCurrency) '*******  TAX PER MONTH  For Paye Table 2
    '                    If dtExcess_Slab.Rows.Count > 0 Then
    '                        Dim drTemp As DataRow() = dtExcess_Slab.Select("amountupto >= " & dblTaxPerForFirst6Month & "", "amountupto ASC")
    '                        If drTemp.Length > 0 Then
    '                            dblNext6MonthPAYE = (dblTaxPerForNext6Month - CDec(drTemp(0).Item("inexcessof"))) * CDec(drTemp(0).Item("ratepayable"))
    '                            If dblNext6MonthPAYE <> 0 Then
    '                                dblNext6MonthPAYE = dblNext6MonthPAYE / 100
    '                            Else
    '                                dblNext6MonthPAYE = 0
    '                            End If
    '                            dblNext6MonthPAYE += CDec(drTemp(0).Item("fixedamount"))
    '                        Else
    '                            dblNext6MonthPAYE = 0
    '                        End If
    '                    End If
    '                End If
    '            End If
    '            If dicDivideByFirst6Month.Keys.Count > 0 Then
    '                dblTaxFirstYear = Format(CDec((dblFirst6MonthPAYE * dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
    '            Else
    '                dblTaxFirstYear = Format(CDec((dblFirst6MonthPAYE * 0)), GUI.fmtCurrency)
    '            End If
    '            If dicDivideByNext6Month.Keys.Count > 0 Then
    '                dblTaxNextYear = Format(CDec((dblNext6MonthPAYE * dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")))), GUI.fmtCurrency)
    '            Else
    '                dblTaxNextYear = Format(CDec((dblNext6MonthPAYE * 0)), GUI.fmtCurrency)
    '            End If
    '            'Sandeep [ 01 MARCH 2011 ] -- End 

    '            'Sandeep [ 01 MARCH 2011 ] -- Sart

    '            'S.SANDEEP [ 24 JUNE 2011 ] -- START
    '            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '            'strSting = "Average Income per 1st 6 months (Jan-Jun) = ( " & Space(4) & Format(CDec(dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId"))), GUI.fmtCurrency) & Space(4) & " / " & Space(4) & dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & " ) = " & Space(4) & Format(CDec(dblAvgIncomeFirst6Months), GUI.fmtCurrency) & Space(4) & vbCrLf & _
    '            '                "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxPerForFirst6Month), GUI.fmtCurrency) & " = " & Space(4) & Format(CDec(dblFirst6MonthPAYE), GUI.fmtCurrency) & Space(4) & " ( Checking using 1st PAYE table)" & Space(4) & vbCrLf & _
    '            '                "Tax 1st 1/2 year ( " & Space(4) & Format(CDec(dblFirst6MonthPAYE), GUI.fmtCurrency) & Space(4) & " * " & Space(4) & dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & Space(4) & " month )  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxFirstYear), GUI.fmtCurrency) & Space(4) & vbCrLf

    '            If dicDivideByFirst6Month.Keys.Count > 0 Then
    '                strSting = "Average Income per 1st 6 months (Jan-Jun) = ( " & Space(4) & Format(CDec(dicTaxableTotalFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId"))), GUI.fmtCurrency) & Space(4) & " / " & Space(4) & dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & " ) = " & Space(4) & Format(CDec(dblAvgIncomeFirst6Months), GUI.fmtCurrency) & Space(4) & vbCrLf & _
    '                           "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxPerForFirst6Month), GUI.fmtCurrency) & " = " & Space(4) & Format(CDec(dblFirst6MonthPAYE), GUI.fmtCurrency) & Space(4) & " ( Checking using 1st PAYE table)" & Space(4) & vbCrLf & _
    '                           "Tax 1st 1/2 year ( " & Space(4) & Format(CDec(dblFirst6MonthPAYE), GUI.fmtCurrency) & Space(4) & " * " & Space(4) & dicDivideByFirst6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & Space(4) & " month )  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxFirstYear), GUI.fmtCurrency) & Space(4) & vbCrLf
    '            Else
    '                strSting = "Average Income per 1st 6 months (Jan-Jun) = ( " & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & " / " & Space(4) & "0" & " ) = " & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & vbCrLf & _
    '                           "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(0, GUI.fmtCurrency) & " = " & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & " ( Checking using 1st PAYE table)" & Space(4) & vbCrLf & _
    '                           "Tax 1st 1/2 year ( " & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & " * " & Space(4) & "0" & Space(4) & " month )  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(0, GUI.fmtCurrency) & Space(4) & vbCrLf
    '            End If
    '            'S.SANDEEP [ 24 JUNE 2011 ] -- END 

    '            If dicTaxableTotalNextt6Month.Keys.Count > 0 Then
    '                strSting &= "Average Income per 2nd 6 months (Jul-Dec) = ( " & Space(4) & Format(CDec(dicTaxableTotalNextt6Month(dsEmployee.Tables(0).Rows(k).Item("EId"))), GUI.fmtCurrency) & Space(4) & " / " & Space(4) & dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & Space(4) & ") = " & Space(4) & Format(CDec(dblAvgIncomeNext6Months), GUI.fmtCurrency) & Space(4) & vbCrLf & _
    '                            "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxPerForNext6Month), GUI.fmtCurrency) & Space(4) & " = " & Space(4) & Format(CDec(dblNext6MonthPAYE), GUI.fmtCurrency) & Space(4) & " ( Checking using 2nd PAYE table)" & Space(4) & vbCrLf & _
    '                            "Tax 2nd 1/2 year ( " & Space(4) & Format(CDec(dblNext6MonthPAYE), GUI.fmtCurrency) & Space(4) & " * " & Space(4) & dicDivideByNext6Month(dsEmployee.Tables(0).Rows(k).Item("EId")) & " month)  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & Format(CDec(dblTaxNextYear), GUI.fmtCurrency) & Space(4)
    '            Else
    '                strSting &= "Average Income per 2nd 6 months (Jul-Dec) = ( " & Space(4) & "0" & Space(4) & " / " & Space(4) & "0 ) = " & Space(4) & "0" & Space(4) & vbCrLf & _
    '                            "Tax per month based on " & Space(4) & ConfigParameter._Object._Currency & Space(4) & "0" & Space(4) & " = " & Space(4) & "0" & Space(4) & " ( Checking using 2nd PAYE table)" & Space(4) & vbCrLf & _
    '                            "Tax 2nd 1/2 year ( " & Space(4) & "0" & Space(4) & " * " & Space(4) & "0" & " month)  = " & Space(4) & ConfigParameter._Object._Currency & Space(4) & "0" & Space(4)
    '            End If
    '            'Sandeep [ 01 MARCH 2011 ] -- End

    '            For j As Integer = 0 To mintPeriodCount - 1
    '                rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column21") = strSting
    '                rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column22") = Format(dblTaxFirstYear, GUI.fmtCurrency)
    '                rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column23") = Format(dblTaxNextYear, GUI.fmtCurrency)
    '                'S.SANDEEP [ 05 JULY 2011 ] -- START
    '                'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '                rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column71") = Format(CDec(rpt_Data.Tables(0).Rows(intRowIndex + j).Item("Column80")) - (dblTaxFirstYear + dblTaxNextYear), GUI.fmtCurrency)
    '                'S.SANDEEP [ 05 JULY 2011 ] -- END 
    '            Next
    '            dblAvgIncomeFirst6Months = 0 : dblAvgIncomeNext6Months = 0 : intRowIndex = -1 : dblTaxPerForFirst6Month = 0 : dblTaxPerForNext6Month = 0
    '            intTranHeadId = 0 : dblFirst6MonthPAYE = 0 : dblNext6MonthPAYE = 0 : dblTaxFirstYear = 0 : dblTaxNextYear = 0 : strSting = String.Empty
    '        Next
    '        '///////// Performing Calculation For Particular Employee ///////// -- End

    '        objRpt = New ArutiReport.Designer.rptIncomeTaxDeductionP9

    '        objRpt.SetDataSource(rpt_Data)
    '        objRpt.Subreports("rptTaxSummary").SetDataSource(rpt_Data)

    '        ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 1, "TANZANIA   REVENUE   AUTHORITY"))
    '        ReportFunction.TextChange(objRpt, "txtHeading2", Language.getMessage(mstrModuleName, 2, "INCOME TAX DEPARTMENT "))
    '        ReportFunction.TextChange(objRpt, "txtHeading3", Language.getMessage(mstrModuleName, 3, "INCOME TAX DEDUCTION CARD YEAR") & FinancialYear._Object._FinancialYear_Name)

    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblEmployerName", Language.getMessage(mstrModuleName, 4, "Employer's Name"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblAddress", Language.getMessage(mstrModuleName, 5, "Address"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblEmployeeName", Language.getMessage(mstrModuleName, 6, "Employee's Main Name"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblEmployeeOtherName", Language.getMessage(mstrModuleName, 7, "Employee's Other Name"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblPWCNo", Language.getMessage(mstrModuleName, 8, "*Payroll/Works/Check No."))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblEmployerTINNo1", Language.getMessage(mstrModuleName, 9, "Employer's T.I.N. No."))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "lblEmployerTINNo2", Language.getMessage(mstrModuleName, 10, "Employee's T.I.N. No"))

    '        ReportFunction.TextChange(objRpt, "txtNotes", Language.getMessage(mstrModuleName, 11, "NOTE:"))
    '        ReportFunction.TextChange(objRpt, "txtNote1", Language.getMessage(mstrModuleName, 12, "1. indicate by asterisk the effective date(s) of adjustments, if any .............................................................."))
    '        ReportFunction.TextChange(objRpt, "txtNote2", Language.getMessage(mstrModuleName, 13, "2. Indicate by asterick against the appropriate months, showing errors and their correction and briefly " & vbCrLf & _
    '                                                                                              "explain these here or at the reverse of the card .............................................................................."))
    '        ReportFunction.TextChange(objRpt, "txtNote3", Language.getMessage(mstrModuleName, 14, "3. *Delete as appropriate"))

    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtAgreeTotalIncome", Language.getMessage(mstrModuleName, 15, "Agreed Total Income"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTax", Language.getMessage(mstrModuleName, 16, "Tax"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtInitials", Language.getMessage(mstrModuleName, 17, "Initials"))

    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtMonths", Language.getMessage(mstrModuleName, 18, "Months"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtBasicPay", Language.getMessage(mstrModuleName, 19, "Basic Pay" & vbCrLf & "(a)"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtHousing", Language.getMessage(mstrModuleName, 20, "Housing" & vbCrLf & "(b)"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtAllowancesandBenefit", Language.getMessage(mstrModuleName, 21, "Allowances" & vbCrLf & "and" & vbCrLf & "Benefits" & vbCrLf & "(c)"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTotalPay", Language.getMessage(mstrModuleName, 22, "Total Pay" & vbCrLf & "a+b+c (d)"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtPensionAndNSSF", Language.getMessage(mstrModuleName, 23, "Pension  & NSSF" & vbCrLf & "(e)"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTaxablePay", Language.getMessage(mstrModuleName, 24, "Taxable Pay" & vbCrLf & "(f)"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTaxPayable", Language.getMessage(mstrModuleName, 25, "Tax Payable" & vbCrLf & "(g)"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtLessPersonal", Language.getMessage(mstrModuleName, 26, "Less" & vbCrLf & "Personal Reliefs" & vbCrLf & "(h)"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtNetTaxDue", Language.getMessage(mstrModuleName, 27, "Net" & vbCrLf & "Due Tax" & vbCrLf & "(i)"))

    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtYear", Language.getMessage(mstrModuleName, 35, "Year"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtAmt", Language.getMessage(mstrModuleName, 36, "Amt"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTax", Language.getMessage(mstrModuleName, 16, "Tax"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtYear1", Language.getMessage(mstrModuleName, 35, "Year"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtAmt1", Language.getMessage(mstrModuleName, 36, "Amt"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTax1", Language.getMessage(mstrModuleName, 16, "Tax"))

    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtTotal", Language.getMessage(mstrModuleName, 77, "Total"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtEmployerUse", Language.getMessage(mstrModuleName, 78, "FOR EMPOYER'S USE"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtLumsum", Language.getMessage(mstrModuleName, 79, "* Add / Deduct tax on previous year lumpsum payment"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptTaxSummary"), "txtOverDeduction", Language.getMessage(mstrModuleName, 80, "* Overdeduction/underdeduction c/f"))


    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        'ReportFunction.SetRptDecimal(objRpt.Subreports("rptTaxSummary"), "frmlGrandTot21")
    '        'ReportFunction.SetRptDecimal(objRpt.Subreports("rptTaxSummary"), "frmlGrandTot31")
    '        'ReportFunction.SetRptDecimal(objRpt.Subreports("rptTaxSummary"), "frmlGrandTot41")
    '        'ReportFunction.SetRptDecimal(objRpt.Subreports("rptTaxSummary"), "frmlGrandTot51")
    '        'ReportFunction.SetRptDecimal(objRpt.Subreports("rptTaxSummary"), "frmlGrandTot61")
    '        'ReportFunction.SetRptDecimal(objRpt.Subreports("rptTaxSummary"), "frmlGrandTot71")
    '        'ReportFunction.SetRptDecimal(objRpt.Subreports("rptTaxSummary"), "frmlGrandTot81")
    '        'ReportFunction.SetRptDecimal(objRpt.Subreports("rptTaxSummary"), "frmlGrandTot91")
    '        'ReportFunction.SetRptDecimal(objRpt.Subreports("rptTaxSummary"), "frmlGrandTot101")
    '        'ReportFunction.SetRptDecimal(objRpt.Subreports("rptTaxSummary"), "frmlTotal1")
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 


    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    'Private Function Generate_PAYE_Details() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Dim dtFilterTable As DataTable
    '    Try
    '        objDataOperation = New clsDataOperation

    '        StrQ = "SELECT " & _
    '                "	 SUM(ISNULL(PAYE.basicsalary,0)) AS basicpay " & _
    '                "	,SUM(ISNULL(PAYE.allowancebenefit,0)) AS allowancebenefit " & _
    '                "	,SUM(ISNULL(PAYE.housing,0))  AS housing " & _
    '                "	,SUM(ISNULL(PAYE.basicsalary,0))+ SUM(ISNULL(PAYE.allowancebenefit,0))+ SUM(ISNULL(PAYE.housing,0)) AS Grosspay " & _
    '                "	,SUM(ISNULL(PAYE.deduction,0)) AS deduction " & _
    '                "	,(SUM(ISNULL(PAYE.basicsalary,0))+ SUM(ISNULL(PAYE.allowancebenefit,0))+ SUM(ISNULL(PAYE.housing,0))) - SUM(ISNULL(PAYE.deduction,0)) AS taxable " & _
    '                "	,SUM(ISNULL(PAYE.taxpayable,0)) - SUM(ISNULL(PAYE.relief,0)) AS taxdue " & _
    '                "	,paye.pid AS pid " & _
    '                "	,paye.empid AS empid " & _
    '                "FROM " & _
    '                "( " & _
    '                "	SELECT " & _
    '                "		 ISNULL(prpayrollprocess_tran.amount,0) as basicsalary " & _
    '                "		,0 AS allowancebenefit " & _
    '                "		,0 AS housing " & _
    '                "		,0 AS deduction " & _
    '                "		,0 as taxpayable " & _
    '                "		,0 as relief " & _
    '                "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                "		,prtnaleave_tran.payperiodunkid AS pid " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "		AND prtranhead_master.trnheadtype_id =1 " & _
    '                "		AND prtranhead_master.typeof_id=1 "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 as basicsalary " & _
    '                "		,0 AS allowancebenefit " & _
    '                "		,ISNULL(prpayrollprocess_tran.amount,0) as housing " & _
    '                "		,0 AS deduction " & _
    '                "		,0 as taxpayable " & _
    '                "		,0 as relief " & _
    '                "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                "		,prtnaleave_tran.payperiodunkid AS pid " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "		AND prtranhead_master.trnheadtype_id = 1 " & _
    '                "		AND prtranhead_master.tranheadunkid=@HousingTranId "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 as basicsalary " & _
    '                "		,ISNULL(prpayrollprocess_tran.amount,0) as allowancebenefit " & _
    '                "		,0 AS housing " & _
    '                "		,0 AS deduction " & _
    '                "		,0 as taxpayable " & _
    '                "		,0 as relief " & _
    '                "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                "		,prtnaleave_tran.payperiodunkid AS pid " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "		AND prtranhead_master.trnheadtype_id =1 " & _
    '                "		AND prtranhead_master.typeof_id IN (2,5) "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 as basicsalary " & _
    '                "		,0 AS allowancebenefit " & _
    '                "		,0 AS housing " & _
    '                "		,ISNULL(prpayrollprocess_tran.amount,0) as deduction " & _
    '                "		,0 as taxpayable " & _
    '                "		,0 as relief " & _
    '                "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                "		,prtnaleave_tran.payperiodunkid AS pid " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0  " & _
    '                "		AND prtranhead_master.tranheadunkid IN (" & mstrStatutoryIds & ") "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 as basicsalary " & _
    '                "		,0 AS allowancebenefit " & _
    '                "		,0 AS housing " & _
    '                "		,0 AS deduction " & _
    '                "		,ISNULL(prpayrollprocess_tran.amount,0) as taxpayable " & _
    '                "		,0  as relief " & _
    '                "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                "		,prtnaleave_tran.payperiodunkid AS pid " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "		AND prtranhead_master.typeof_id=9 "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "UNION ALL " & _
    '                "	SELECT " & _
    '                "		 0 as basicsalary " & _
    '                "		,0 AS allowancebenefit " & _
    '                "		,0 AS housing " & _
    '                "		,0 AS deduction " & _
    '                "		,0 as taxpayable " & _
    '                "		,ISNULL(prpayrollprocess_tran.amount,0) as relief " & _
    '                "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                "		,prtnaleave_tran.payperiodunkid AS pid " & _
    '                "	FROM prpayrollprocess_tran " & _
    '                "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                "		AND prtranhead_master.istaxrelief=1 "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= ") AS PAYE " & _
    '                "WHERE pid IN (" & mstrPeriodIds & ") "
    '        Call FilterTitleAndFilterQuery()
    '        StrQ &= Me._FilterQuery
    '        StrQ &= "GROUP BY empid,pid "

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dsEmployee As DataSet = Nothing

    '        'S.SANDEEP [ 24 JUNE 2011 ] -- START
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'StrQ = "SELECT " & _
    '        '       "	 ISNULL(firstname,'') +''+ ISNULL(othername,'') +''+ ISNULL(surname,'') AS empname " & _
    '        '       "	,hremployee_master.employeecode AS empcode " & _
    '        '       "	,ISNULL(hremployee_master.present_address1,'') +' '+ ISNULL(hremployee_master.present_address2,'') AS Address " & _
    '        '       "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS city " & _
    '        '       " 	,hremployee_master.employeeunkid " & _
    '        '       "FROM hremployee_master " & _
    '        '       "	LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = hremployee_master.present_post_townunkid " & _
    '        '       "WHERE hremployee_master.isactive = 1 "

    '        StrQ = "SELECT " & _
    '               "	 ISNULL(firstname,'') +''+ ISNULL(othername,'') +''+ ISNULL(surname,'') AS empname " & _
    '               "	,hremployee_master.employeecode AS empcode " & _
    '               "	,ISNULL(hremployee_master.present_address1,'') +' '+ ISNULL(hremployee_master.present_address2,'') AS Address " & _
    '               "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS city " & _
    '               " 	,hremployee_master.employeeunkid " & _
    '               "FROM hremployee_master " & _
    '               "	LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = hremployee_master.present_post_townunkid " & _
    '                " WHERE 1 = 1 "

    '        If mblnIncludeInactiveEmp = False Then
    '            StrQ &= " AND hremployee_master.isactive = 1 "
    '        End If

    '        'Sohail (28 Jun 2011) -- Start
    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        If mintEmpId > 0 Then
    '            objDataOperation.AddParameter("@EmplId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
    '            StrQ &= "AND employeeunkid = @EmplId "
    '        End If

    '        'S.SANDEEP [ 24 JUNE 2011 ] -- END 



    '        dsEmployee = objDataOperation.ExecQuery(StrQ, "Emp")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        Dim rpt_Row As DataRow = Nothing
    '        Dim blnIsAdded As Boolean = False
    '        Dim intCnt As Integer = 1

    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        Dim decColumn6Total, decColumn7Total, decColumn8Total, decColumn9Total, decColumn10Total, decColumn11Total, decColumn12Total As Decimal
    '        decColumn6Total = 0 : decColumn7Total = 0 : decColumn8Total = 0 : decColumn9Total = 0 : decColumn10Total = 0 : decColumn11Total = 0 : decColumn12Total = 0
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 

    '        For Each dtRow As DataRow In dsEmployee.Tables(0).Rows
    '            dtFilterTable = New DataView(dsList.Tables(0), "empid = '" & dtRow.Item("employeeunkid") & "'", "", DataViewRowState.CurrentRows).ToTable
    '            blnIsAdded = False
    '            If dtFilterTable.Rows.Count > 0 Then
    '                For Each drRow As DataRow In dtFilterTable.Rows
    '                    If blnIsAdded = False Then
    '                        rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '                        rpt_Row.Item("Column1") = intCnt
    '                        rpt_Row.Item("Column2") = dtRow.Item("empname")
    '                        rpt_Row.Item("Column3") = dtRow.Item("empcode")
    '                        rpt_Row.Item("Column4") = dtRow.Item("Address")
    '                        rpt_Row.Item("Column5") = dtRow.Item("city")

    '                        rpt_Row.Item("Column6") = Format(CDec(drRow.Item("basicpay")), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column7") = Format(CDec(drRow.Item("housing")), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column8") = Format(CDec(drRow.Item("allowancebenefit")), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column9") = Format(CDec(drRow.Item("Grosspay")), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column10") = Format(CDec(drRow.Item("deduction")), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column11") = Format(CDec(drRow.Item("taxable")), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column12") = Format(CDec(drRow.Item("taxdue")), GUI.fmtCurrency)
    '                        blnIsAdded = True
    '                    Else
    '                        rpt_Row.Item("Column6") = Format(CDec(CDec(rpt_Row.Item("Column6")) + CDec(drRow.Item("basicpay"))), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column7") = Format(CDec(CDec(rpt_Row.Item("Column7")) + CDec(drRow.Item("housing"))), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column8") = Format(CDec(CDec(rpt_Row.Item("Column8")) + CDec(drRow.Item("allowancebenefit"))), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column9") = Format(CDec(CDec(rpt_Row.Item("Column9")) + CDec(drRow.Item("Grosspay"))), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column10") = Format(CDec(CDec(rpt_Row.Item("Column10")) + CDec(drRow.Item("deduction"))), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column11") = Format(CDec(CDec(rpt_Row.Item("Column11")) + CDec(drRow.Item("taxable"))), GUI.fmtCurrency)
    '                        rpt_Row.Item("Column12") = Format(CDec(CDec(rpt_Row.Item("Column12")) + CDec(drRow.Item("taxdue"))), GUI.fmtCurrency)
    '                    End If

    '                    'S.SANDEEP [ 05 JULY 2011 ] -- START
    '                    'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '                    decColumn6Total = decColumn6Total + CDec(drRow.Item("basicpay"))
    '                    decColumn7Total = decColumn7Total + CDec(drRow.Item("housing"))
    '                    decColumn8Total = decColumn8Total + CDec(drRow.Item("allowancebenefit"))
    '                    decColumn9Total = decColumn9Total + CDec(drRow.Item("Grosspay"))
    '                    decColumn10Total = decColumn10Total + CDec(drRow.Item("deduction"))
    '                    decColumn11Total = decColumn11Total + CDec(drRow.Item("taxable"))
    '                    decColumn12Total = decColumn12Total + CDec(drRow.Item("taxdue"))
    '                    'S.SANDEEP [ 05 JULY 2011 ] -- END 
    '                Next
    '                intCnt += 1
    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
    '            Else
    '                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

    '                rpt_Row.Item("Column1") = intCnt
    '                rpt_Row.Item("Column2") = dtRow.Item("empname")
    '                rpt_Row.Item("Column3") = dtRow.Item("empcode")
    '                rpt_Row.Item("Column4") = dtRow.Item("Address")
    '                rpt_Row.Item("Column5") = dtRow.Item("city")

    '                rpt_Row.Item("Column6") = Format(CDec(0), GUI.fmtCurrency)
    '                rpt_Row.Item("Column7") = Format(CDec(0), GUI.fmtCurrency)
    '                rpt_Row.Item("Column8") = Format(CDec(0), GUI.fmtCurrency)
    '                rpt_Row.Item("Column9") = Format(CDec(0), GUI.fmtCurrency)
    '                rpt_Row.Item("Column10") = Format(CDec(0), GUI.fmtCurrency)
    '                rpt_Row.Item("Column11") = Format(CDec(0), GUI.fmtCurrency)
    '                rpt_Row.Item("Column12") = Format(CDec(0), GUI.fmtCurrency)

    '                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)

    '                intCnt += 1
    '            End If
    '        Next

    '        objRpt = New ArutiReport.Designer.rptPAYEPaymentTax

    '        objRpt.SetDataSource(rpt_Data)

    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblTRA", Language.getMessage(mstrModuleName, 37, "TANZANIA REVENUE AUTHORITY")) 'Start From 37
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPAYE", Language.getMessage(mstrModuleName, 38, "P.A.Y.E."))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblReportName", Language.getMessage(mstrModuleName, 39, "STATEMENT AND PAYMENT OF TAX WITHHELD"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblYear", Language.getMessage(mstrModuleName, 40, "YEAR:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtYear", FinancialYear._Object._FinancialYear_Name)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblTIN", Language.getMessage(mstrModuleName, 41, "TIN:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtTIN", Company._Object._Tinno)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod", Language.getMessage(mstrModuleName, 42, "Period: (Please tick the appropriate box) "))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod1", Language.getMessage(mstrModuleName, 43, "From 1 January to 30 June "))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPeriod2", Language.getMessage(mstrModuleName, 44, "From 1 January to 31 December "))

    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblEmployer", Language.getMessage(mstrModuleName, 45, "Name of Employer:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtEmployer", Company._Object._Name)

    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPostalAddress", Language.getMessage(mstrModuleName, 46, "Postal Address:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPoBox", Language.getMessage(mstrModuleName, 47, "P. O. Box"))
    '        Dim objZip As New clszipcode_master
    '        objZip._Zipcodeunkid = Company._Object._Postalunkid
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPoBox", objZip._Zipcode_No)
    '        objZip = Nothing
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPostalCity", Language.getMessage(mstrModuleName, 48, "Postal City"))
    '        Dim objCity As New clscity_master
    '        objCity._Cityunkid = Company._Object._Cityunkid
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPostalCity", objCity._Name)
    '        objCity = Nothing
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblContactNumbers", Language.getMessage(mstrModuleName, 49, "Contact Numbers:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPhone", Language.getMessage(mstrModuleName, 50, "Phone number"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPhone1", Company._Object._Phone1)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblSecondPhone", Language.getMessage(mstrModuleName, 51, "Second Phone"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtSecondPhone", Company._Object._Phone2)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblThirdPhone", Language.getMessage(mstrModuleName, 52, "Third Phone"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtThirdphone", Company._Object._Phone3)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblFax", Language.getMessage(mstrModuleName, 53, " Fax number"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtFax", Company._Object._Fax)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblEmail", Language.getMessage(mstrModuleName, 54, "E-mail address:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtEmail", Company._Object._Email)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPhysical", Language.getMessage(mstrModuleName, 55, "Physical Address:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPlot", Language.getMessage(mstrModuleName, 56, "Plot Number"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtPlot", Company._Object._Reg1_Value)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblBlock", Language.getMessage(mstrModuleName, 57, "Block Number"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtBlock", Company._Object._Reg2_Value)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblStreet", Language.getMessage(mstrModuleName, 58, "Street/Location"))
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtStreet", Company._Object._Address1 & " " & Company._Object._Address2)
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblBranch", Language.getMessage(mstrModuleName, 59, "Name of Branch"))
    '        Dim objBranch As New clsbankbranch_master
    '        objBranch._Branchunkid = Company._Object._Branchunkid
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "txtBranch", objBranch._Branchname)
    '        objBranch = Nothing
    '        ReportFunction.TextChange(objRpt.Subreports("rptEmployerDetail"), "lblPaymentStatement", Language.getMessage(mstrModuleName, 60, "ITX215.01.E -  P.A.Y.E.-Statement"))


    '        ReportFunction.TextChange(objRpt, "txtReportName", Language.getMessage(mstrModuleName, 61, "P.A.Y.E. - DETAILS OF PAYMENT OF TAX WITHHELD"))
    '        ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 62, "Name of Employer :"))
    '        ReportFunction.TextChange(objRpt, "txtEmployerName", Company._Object._Name)
    '        ReportFunction.TextChange(objRpt, "lblTIN", Language.getMessage(mstrModuleName, 63, "TIN :"))
    '        ReportFunction.TextChange(objRpt, "txtTIN", Company._Object._Tinno)
    '        ReportFunction.TextChange(objRpt, "txtSrNo", Language.getMessage(mstrModuleName, 64, "S/No."))
    '        ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 65, "Name Of Employee"))
    '        ReportFunction.TextChange(objRpt, "txtPayroll", Language.getMessage(mstrModuleName, 66, "Payroll No."))
    '        ReportFunction.TextChange(objRpt, "txtAddress", Language.getMessage(mstrModuleName, 67, "Postal Address"))
    '        ReportFunction.TextChange(objRpt, "txtCity", Language.getMessage(mstrModuleName, 68, "Postal City"))
    '        ReportFunction.TextChange(objRpt, "txtPay", Language.getMessage(mstrModuleName, 69, "Basic Pay"))
    '        ReportFunction.TextChange(objRpt, "txtHousing", Language.getMessage(mstrModuleName, 70, "Housing"))
    '        ReportFunction.TextChange(objRpt, "txtAllBenefit", Language.getMessage(mstrModuleName, 71, "Allowances And Benefits"))
    '        ReportFunction.TextChange(objRpt, "txtGrossPay", Language.getMessage(mstrModuleName, 72, "Grosspay"))
    '        ReportFunction.TextChange(objRpt, "txtDeduction", Language.getMessage(mstrModuleName, 73, "Deductions"))
    '        ReportFunction.TextChange(objRpt, "txtTaxable", Language.getMessage(mstrModuleName, 74, "Taxable"))
    '        ReportFunction.TextChange(objRpt, "txtTaxDue", Language.getMessage(mstrModuleName, 75, "Tax Due"))
    '        ReportFunction.TextChange(objRpt, "txtGrandTotal", Language.getMessage(mstrModuleName, 76, "Total"))


    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        'ReportFunction.SetRptDecimal(objRpt, "frmlTotCol61")
    '        'ReportFunction.SetRptDecimal(objRpt, "frmlTotCol71")
    '        'ReportFunction.SetRptDecimal(objRpt, "frmlTotCol81")
    '        'ReportFunction.SetRptDecimal(objRpt, "frmlTotCol91")
    '        'ReportFunction.SetRptDecimal(objRpt, "frmlTotCol101")
    '        'ReportFunction.SetRptDecimal(objRpt, "frmlTotCol111")
    '        'ReportFunction.SetRptDecimal(objRpt, "frmlTotCol121")

    '        Call ReportFunction.TextChange(objRpt, "txtTotal6", Format(decColumn6Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal7", Format(decColumn7Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal8", Format(decColumn8Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal9", Format(decColumn9Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal10", Format(decColumn10Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal11", Format(decColumn11Total, GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal12", Format(decColumn12Total, GUI.fmtCurrency))
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 


    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_PAYE_Details; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "TANZANIA   REVENUE   AUTHORITY")
            Language.setMessage(mstrModuleName, 2, "INCOME TAX DEPARTMENT")
            Language.setMessage(mstrModuleName, 3, "INCOME TAX DEDUCTION CARD YEAR")
            Language.setMessage(mstrModuleName, 4, "Employer's Name")
            Language.setMessage(mstrModuleName, 5, "Address")
            Language.setMessage(mstrModuleName, 6, "Employee's Main Name")
            Language.setMessage(mstrModuleName, 7, "Employee's Other Name")
            Language.setMessage(mstrModuleName, 8, "*Payroll/Works/Check No.")
            Language.setMessage(mstrModuleName, 9, "Employer's T.I.N. No.")
            Language.setMessage(mstrModuleName, 10, "Employee's T.I.N. No")
            Language.setMessage(mstrModuleName, 11, "NOTE:")
            Language.setMessage(mstrModuleName, 12, "1. indicate by asterisk the effective date(s) of adjustments, if any ..............................................................")
            Language.setMessage(mstrModuleName, 13, "2. Indicate by asterick against the appropriate months, showing errors and their correction and briefly " & vbCrLf & _
                                                                                                           "explain these here or at the reverse of the card ..............................................................................")
            Language.setMessage(mstrModuleName, 14, "3. *Delete as appropriate")
            Language.setMessage(mstrModuleName, 15, "Agreed Total Income")
            Language.setMessage(mstrModuleName, 16, "Tax")
            Language.setMessage(mstrModuleName, 17, "Initials")
            Language.setMessage(mstrModuleName, 18, "Months")
            Language.setMessage(mstrModuleName, 19, "Basic Pay" & vbCrLf & "(a)")
            Language.setMessage(mstrModuleName, 20, "Housing" & vbCrLf & "(b)")
            Language.setMessage(mstrModuleName, 21, "Allowances" & vbCrLf & "and" & vbCrLf & "Benefits" & vbCrLf & "(c)")
            Language.setMessage(mstrModuleName, 22, "Total Pay" & vbCrLf & "(d)")
            Language.setMessage(mstrModuleName, 23, "Pension  & NSSF" & vbCrLf & "(e)")
            Language.setMessage(mstrModuleName, 24, "Taxable Pay" & vbCrLf & "(f)")
            Language.setMessage(mstrModuleName, 25, "Tax Payable" & vbCrLf & "(g)")
            Language.setMessage(mstrModuleName, 26, "Less" & vbCrLf & "Personal Reliefs" & vbCrLf & "(h)")
            Language.setMessage(mstrModuleName, 27, "Net" & vbCrLf & "Due Tax" & vbCrLf & "(i)")
            Language.setMessage(mstrModuleName, 28, "Information required from the" & vbCrLf & "Employer at the end of the year")
            Language.setMessage(mstrModuleName, 29, "1. Date commenced/left during the year:-")
            Language.setMessage(mstrModuleName, 30, "2. Name and address of OLD/NEW employer:-")
            Language.setMessage(mstrModuleName, 31, "3. If employee received benefits other than cash totalling to more than Shs. 1000/= in the year, please give particulars and")
            Language.setMessage(mstrModuleName, 32, "4. Where housing is provided show monthly rent charged to employee Shs .....................")
            Language.setMessage(mstrModuleName, 33, "5. Where lumpsum payment is made state NATURE and AMOUNT ..............................")
            Language.setMessage(mstrModuleName, 34, "6. where any of the Pay relates to a period other than this year e.g. give the following details:.")
            Language.setMessage(mstrModuleName, 35, "Year")
            Language.setMessage(mstrModuleName, 36, "Amt")
            Language.setMessage(mstrModuleName, 37, "TANZANIA REVENUE AUTHORITY")
            Language.setMessage(mstrModuleName, 38, "P.A.Y.E.")
            Language.setMessage(mstrModuleName, 39, "STATEMENT AND PAYMENT OF TAX WITHHELD")
            Language.setMessage(mstrModuleName, 40, "YEAR:")
            Language.setMessage(mstrModuleName, 41, "TIN:")
            Language.setMessage(mstrModuleName, 42, "Period: (Please tick the appropriate box)")
            Language.setMessage(mstrModuleName, 43, "From 1 January to 30 June")
            Language.setMessage(mstrModuleName, 44, "From 1 January to 31 December")
            Language.setMessage(mstrModuleName, 45, "Name of Employer:")
            Language.setMessage(mstrModuleName, 46, "Postal Address:")
            Language.setMessage(mstrModuleName, 47, "P. O. Box")
            Language.setMessage(mstrModuleName, 48, "Postal City")
            Language.setMessage(mstrModuleName, 49, "Contact Numbers:")
            Language.setMessage(mstrModuleName, 50, "Phone number")
            Language.setMessage(mstrModuleName, 51, "Second Phone")
            Language.setMessage(mstrModuleName, 52, "Third Phone")
            Language.setMessage(mstrModuleName, 53, " Fax number")
            Language.setMessage(mstrModuleName, 54, "E-mail address:")
            Language.setMessage(mstrModuleName, 55, "Physical Address:")
            Language.setMessage(mstrModuleName, 56, "Plot Number")
            Language.setMessage(mstrModuleName, 57, "Block Number")
            Language.setMessage(mstrModuleName, 58, "Street/Location")
            Language.setMessage(mstrModuleName, 59, "Name of Branch")
            Language.setMessage(mstrModuleName, 60, "ITX215.01.E -  P.A.Y.E.-Statement")
            Language.setMessage(mstrModuleName, 61, "P.A.Y.E. - DETAILS OF PAYMENT OF TAX WITHHELD")
            Language.setMessage(mstrModuleName, 62, "Name of Employer :")
            Language.setMessage(mstrModuleName, 63, "TIN :")
            Language.setMessage(mstrModuleName, 64, "S/No.")
            Language.setMessage(mstrModuleName, 65, "Name Of Employee")
            Language.setMessage(mstrModuleName, 66, "Payroll No.")
            Language.setMessage(mstrModuleName, 67, "Postal Address")
            Language.setMessage(mstrModuleName, 68, "Postal City")
            Language.setMessage(mstrModuleName, 69, "Basic Pay")
            Language.setMessage(mstrModuleName, 70, "Housing")
            Language.setMessage(mstrModuleName, 71, "Allowances And Benefits")
            Language.setMessage(mstrModuleName, 72, "Grosspay")
            Language.setMessage(mstrModuleName, 73, "Deductions")
            Language.setMessage(mstrModuleName, 74, "Taxable")
            Language.setMessage(mstrModuleName, 75, "Tax Due")
            Language.setMessage(mstrModuleName, 76, "Total")
            Language.setMessage(mstrModuleName, 77, "Total")
            Language.setMessage(mstrModuleName, 78, "FOR EMPOYER'S USE")
            Language.setMessage(mstrModuleName, 79, "* Add / Deduct tax on previous year lumpsum payment")
            Language.setMessage(mstrModuleName, 80, "* Overdeduction/underdeduction c/f")
            Language.setMessage(mstrModuleName, 81, "Periods : (")
            Language.setMessage(mstrModuleName, 82, "")
            Language.setMessage(mstrModuleName, 83, "Cost Center :")
            Language.setMessage(mstrModuleName, 84, "Sub Total")
            Language.setMessage(mstrModuleName, 85, "Emp. code")
            Language.setMessage(mstrModuleName, 86, "Periods :")
            Language.setMessage(mstrModuleName, 87, "Emp. Name")
            Language.setMessage(mstrModuleName, 88, "*Over / underductions")
            Language.setMessage(mstrModuleName, 89, "Printed By :")
            Language.setMessage(mstrModuleName, 90, "Printed Date :")
            Language.setMessage(mstrModuleName, 91, "* Negative (-ve) Amount indicates Under deductions")
            Language.setMessage(mstrModuleName, 92, "* Positive (+ve) Amount indicates Over deductions")
            Language.setMessage(mstrModuleName, 93, "Non-Cash Benefit")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
