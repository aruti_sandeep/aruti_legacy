'************************************************************************************************************************************
'Class Name : frmP9AReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmP9AReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmP9AReport"
    Private objP9AReport As clsP9AReport
    Private mintFirstPeriodId As Integer = 0
    Private mintLastPeriodId As Integer = 0 'Sohail (29 Mar 2017)

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mdtPeriodEndDate As DateTime

#End Region

#Region " Contructor "

    Public Sub New()
        objP9AReport = New clsP9AReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objP9AReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Quarter_Value = 1
        E1 = 2
        E2 = 3
        E3 = 4
        Amount_Of_Interest = 5
        Tax_Charged = 6
        Personal_Relief = 7

        'Anjan [03 December 2015] -- Start
        'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
        membership = 8
        'Anjan [03 December 2015] -- End
        'Sohail (18 Jan 2017) -- Start
        'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
        Insurance_Relief = 9
        'Sohail (18 Jan 2017) -- End
        'Sohail (08 May 2020) -- Start
        'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
        Taxable_Income = 10
        'Sohail (08 May 2020) -- End
        'Sohail (20 Mar 2021) -- Start
        'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
        BasicSalaryFormula = 11
        TaxChargedFormula = 12
        'Sohail (20 Mar 2021) -- End
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objMaster As New clsMasterData

        'Anjan [03 December 2015] -- Start
        'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
        Dim objMember As New clsmembership_master
        'Anjan [03 December 2015] -- End


        Dim dsCombos As DataSet

        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objEmp.GetEmployeeList("Emp", True, False)
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, True, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to view previous closed year report on P9A Report.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (18 Jan 2017) -- End
            'Sohail (21 Aug 2015) -- End
            'Sohail (29 Mar 2017) -- Start
            'CCK Enhancement - 65.2 - 2. End of year P9A – Currently system open to February 2017, therefore employees can generate reports from Jan 16 to Feb 2017. In fact, this report should not have period option. Should be specific from January to December 2016. (by default first period in From period and last period in to period) 
            Dim dsYear As DataSet = objMaster.Get_Database_Year_List("Year", True, Company._Object._Companyunkid)
            Dim dt As DataTable = New DataView(dsYear.Tables(0), "start_date >= '" & eZeeDate.convertDate(FinancialYear._Object._Database_Start_Date.AddYears(-1)) & "' AND end_date <= '" & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date.AddYears(-1)) & "' ", "", DataViewRowState.CurrentRows).ToTable
            Dim strYear As String = String.Join(",", (From p In dt Select (p.Item("yearunkid").ToString)).ToArray)
            Dim mdtPeriod As DataTable = New DataView(dsCombos.Tables("Period"), "yearunkid IN (0, " & strYear & ") ", "", DataViewRowState.CurrentRows).ToTable
            If mdtPeriod.Rows.Count > 1 Then
                If CInt(mdtPeriod.Rows(1).Item("periodunkid")) > 0 Then mintFirstPeriodId = CInt(mdtPeriod.Rows(1).Item("periodunkid"))
                mintLastPeriodId = CInt(mdtPeriod.Rows(mdtPeriod.Rows.Count - 1).Item("periodunkid"))
            End If
            If mintLastPeriodId <= 0 Then mintLastPeriodId = mintFirstPeriodId
            'Sohail (29 Mar 2017) -- End

            With cboFromPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstPeriodId
            End With

            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                'Sohail (29 Mar 2017) -- Start
                'CCK Enhancement - 65.2 - 2. End of year P9A – Currently system open to February 2017, therefore employees can generate reports from Jan 16 to Feb 2017. In fact, this report should not have period option. Should be specific from January to December 2016. (by default first period in From period and last period in to period) 
                '.SelectedValue = mintFirstPeriodId
                .SelectedValue = mintLastPeriodId
                'Sohail (29 Mar 2017) -- End
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("Heads", True, , , enTypeOf.Informational)
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , enTypeOf.Informational)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " ")
            'Sohail (18 Jan 2017) -- End
            'Sohail (21 Aug 2015) -- End
            With cboQuarterValue
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            With cboE1
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboE2
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboE3
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboAmountOfInterest
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("Heads", True, , enCalcType.AsComputedOnWithINEXCESSOFTaxSlab)
            'Hemant (04 Jul 2020) -- Start
            'ENHANCEMENT #0004771: P9 Statutory Report Changes - Tax Charged Column to Allow Mapping to All Informational Heads
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , enCalcType.AsComputedOnWithINEXCESSOFTaxSlab)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , " ( calctype_id = " & CInt(enCalcType.AsComputedOnWithINEXCESSOFTaxSlab) & " OR trnheadtype_id = " & CInt(enTranHeadType.Informational) & "  )")
            'Hemant (04 Jul 2020) -- End

            'Sohail (21 Aug 2015) -- End
            With cboTaxCharged
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objTranHead.getComboList("Heads", True, , , , , , "istaxrelief = 1")
            'Sohail (11 Apr 2016) -- Start
            'Enhancement - 58.1 - Allow to map all types of heads with Personal Relief on P9A Report.
            'Sohail (11 Apr 2016) -- End
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , "istaxrelief = 1")
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " ")
            'Sohail (21 Aug 2015) -- End
            With cboPersonalRelief
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
            With cboInsuranceRelief
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With
            'Sohail (18 Jan 2017) -- End

            'Sohail (08 May 2020) -- Start
            'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, enTranHeadType.Informational, , , , , "typeof_id <> " & enTypeOf.Salary & " ")
            With cboTaxableIcome
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            'Sohail (08 May 2020) -- End

            'Anjan [03 December 2015] -- Start
            'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
            dsCombos = objMember.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With
            'Anjan [03 December 2015] -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objperiod = Nothing
            objTranHead = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboQuarterValue.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboFromPeriod.SelectedValue = mintFirstPeriodId
            'Sohail (29 Mar 2017) -- Start
            'CCK Enhancement - 65.2 - 2. End of year P9A – Currently system open to February 2017, therefore employees can generate reports from Jan 16 to Feb 2017. In fact, this report should not have period option. Should be specific from January to December 2016. (by default first period in From period and last period in to period) 
            'cboToPeriod.SelectedValue = mintFirstPeriodId
            cboToPeriod.SelectedValue = mintLastPeriodId
            'Sohail (29 Mar 2017) -- End
            cboE1.SelectedValue = 0
            cboE2.SelectedValue = 0
            cboE3.SelectedValue = 0
            cboAmountOfInterest.SelectedValue = 0
            cboTaxCharged.SelectedValue = 0
            cboPersonalRelief.SelectedValue = 0
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
            cboInsuranceRelief.SelectedValue = 0
            'Sohail (18 Jan 2017) -- End
            'Sohail (08 May 2020) -- Start
            'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
            cboTaxableIcome.SelectedValue = 0
            'Sohail (08 May 2020) -- End
            'Sohail (20 Mar 2021) -- Start
            'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
            txtBasicSalaryFormula.Text = ""
            txtTaxChargedFormula.Text = ""
            'Sohail (20 Mar 2021) -- End

            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            'Anjan [03 December 2015] -- Start
            'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
            cboMembership.SelectedValue = 0
            'Anjan [03 December 2015] -- End

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.P9A_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Quarter_Value
                            cboQuarterValue.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                        Case enHeadTypeId.E1
                            cboE1.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.E2
                            cboE2.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.E3
                            cboE3.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Amount_Of_Interest
                            cboAmountOfInterest.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Tax_Charged
                            cboTaxCharged.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Personal_Relief
                            cboPersonalRelief.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                            'Anjan [03 December 2015] -- Start
                            'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
                        Case enHeadTypeId.membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Anjan [03 December 2015] -- End

                            'Sohail (18 Jan 2017) -- Start
                            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
                        Case enHeadTypeId.Insurance_Relief
                            cboInsuranceRelief.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (18 Jan 2017) -- End

                            'Sohail (08 May 2020) -- Start
                            'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
                        Case enHeadTypeId.Taxable_Income
                            cboTaxableIcome.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Sohail (08 May 2020) -- End

                            'Sohail (20 Mar 2021) -- Start
                            'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                        Case enHeadTypeId.BasicSalaryFormula
                            txtBasicSalaryFormula.Text = dsRow.Item("transactionheadid").ToString

                        Case enHeadTypeId.TaxChargedFormula
                            txtTaxChargedFormula.Text = dsRow.Item("transactionheadid").ToString
                            'Sohail (20 Mar 2021) -- End

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        'Sohail (18 Jan 2017) -- Start
        'CCK Enhancement - 64.1 - Allow to view previous closed year report on P9A Report.
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim intPrevYearID As Integer = 0
        Dim mdicYearDBName As New Dictionary(Of Integer, String)
        'Sohail (18 Jan 2017) -- End
        Try
            objP9AReport.SetDefaultValue()

            
            If CInt(cboFromPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "From Period is mandatory information. Please select From Period."), enMsgBoxStyle.Information)
                cboFromPeriod.Focus()
                Return False
            ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "To Period is mandatory information. Please select To Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            ElseIf CInt(cboFromPeriod.SelectedIndex) > CInt(cboToPeriod.SelectedIndex) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            ElseIf CInt(cboQuarterValue.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select head for Value of Quarters."), enMsgBoxStyle.Information)
                cboQuarterValue.Focus()
                Return False
            ElseIf CInt(cboE1.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select head for E1."), enMsgBoxStyle.Information)
                cboE1.Focus()
                Return False
            ElseIf CInt(cboE2.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select head for E2."), enMsgBoxStyle.Information)
                cboE2.Focus()
                Return False
            ElseIf CInt(cboE3.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select head for E3."), enMsgBoxStyle.Information)
                cboE3.Focus()
                Return False
            ElseIf CInt(cboAmountOfInterest.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select head for Amount Of Interest."), enMsgBoxStyle.Information)
                cboAmountOfInterest.Focus()
                Return False
            ElseIf CInt(cboTaxCharged.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Please select head for Tax Charged."), enMsgBoxStyle.Information)
                cboTaxCharged.Focus()
                Return False
            ElseIf CInt(cboPersonalRelief.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select head for Personal Relief."), enMsgBoxStyle.Information)
                cboPersonalRelief.Focus()
                Return False
                'Sohail (18 Jan 2017) -- Start
                'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
            ElseIf CInt(cboInsuranceRelief.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please select head for Insurance Relief."), enMsgBoxStyle.Information)
                cboInsuranceRelief.Focus()
                Return False
                'Sohail (18 Jan 2017) -- End
                'Sohail (08 May 2020) -- Start
                'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
            ElseIf CInt(cboTaxableIcome.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select head for Taxable Income."), enMsgBoxStyle.Information)
                cboTaxableIcome.Focus()
                Return False
                'Sohail (08 May 2020) -- End
            End If


            objP9AReport._EmpId = cboEmployee.SelectedValue
            objP9AReport._EmpName = cboEmployee.Text


            Dim i As Integer = 0
            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to view previous closed year report on P9A Report.
            'Dim strPreriodIds As String = ""
            'Dim mstrPeriodsName As String = ""
            'For Each dsRow As DataRow In CType(cboFromPeriod.DataSource, DataTable).Rows
            '    If i >= cboFromPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
            '        strPreriodIds &= ", " & dsRow.Item("periodunkid").ToString
            '        mstrPeriodsName &= ", " & dsRow.Item("name").ToString
            '    End If
            '    i += 1
            'Next

            'If strPreriodIds.Trim <> "" Then
            '    strPreriodIds = strPreriodIds.Substring(2)
            'End If
            'If mstrPeriodsName.Trim <> "" Then
            '    mstrPeriodsName = mstrPeriodsName.Substring(2)
            'End If
            Dim strPreriodIds As String = cboFromPeriod.SelectedValue.ToString
            Dim mstrPeriodsName As String = cboFromPeriod.Text
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid")))
            If dsList.Tables("Database").Rows.Count > 0 Then
                intPrevYearID = CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid"))
                If mdicYearDBName.ContainsKey(intPrevYearID) = False Then
                    mdicYearDBName.Add(intPrevYearID, dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                End If
            End If

            For Each dsRow As DataRow In CType(cboFromPeriod.DataSource, DataTable).Rows
                If i > cboFromPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                    strPreriodIds &= ", " & dsRow.Item("periodunkid").ToString
                    mstrPeriodsName &= ", " & dsRow.Item("name").ToString
                    If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                        dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(dsRow.Item("yearunkid")))
                        If dsList.Tables("Database").Rows.Count > 0 Then
                            If mdicYearDBName.ContainsKey(CInt(dsRow.Item("yearunkid"))) = False Then
                                mdicYearDBName.Add(CInt(dsRow.Item("yearunkid")), dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                            End If
                End If
            End If
                    intPrevYearID = CInt(dsRow.Item("yearunkid"))
            End If
                i += 1
            Next
            objP9AReport._dic_YearDBName = mdicYearDBName
            'Sohail (18 Jan 2017) -- End
            objP9AReport._PeriodIds = strPreriodIds
            objP9AReport._PeriodNames = mstrPeriodsName

            'Sohail (19 Nov 2015) - Start
            'Sohail (23 Jan 2021) -- Start
            'FFK Issue : OLD-281 - P9 report reads wrong year when FY is not Jan-Dec. Pick year of the last period selected
            'Dim objYear As New clsCompany_Master
            'objYear._YearUnkid = CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid"))
            'objP9AReport._FirstPeriodYearName = objYear._FinancialYear_Name.Substring(0, objYear._FinancialYear_Name.IndexOf("-"))
            'objYear = Nothing
            objP9AReport._FirstPeriodYearName = eZeeDate.convertDate(CType(cboToPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Year.ToString
            'Sohail (23 Jan 2021) -- End
            'Sohail (19 Nov 2015) - End

            objP9AReport._PayeHeadId = CInt(cboQuarterValue.SelectedValue)
            objP9AReport._PayeHeadName = cboQuarterValue.Text

            objP9AReport._E1HeadId = CInt(cboE1.SelectedValue)
            objP9AReport._E1HeadName = cboE1.Text

            objP9AReport._E2HeadId = CInt(cboE2.SelectedValue)
            objP9AReport._E2HeadName = cboE2.Text

            objP9AReport._E3HeadId = CInt(cboE3.SelectedValue)
            objP9AReport._E3HeadName = cboE3.Text

            objP9AReport._AmountOfInterestHeadId = CInt(cboAmountOfInterest.SelectedValue)
            objP9AReport._AmountOfInterestHeadName = cboAmountOfInterest.Text

            objP9AReport._TaxChargedHeadId = CInt(cboTaxCharged.SelectedValue)
            objP9AReport._TaxChargedHeadName = cboTaxCharged.Text

            objP9AReport._PersonalReliefHeadId = CInt(cboPersonalRelief.SelectedValue)
            objP9AReport._PersonalReliefHeadName = cboPersonalRelief.Text

            'Sohail (18 Jan 2017) -- Start
            'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
            objP9AReport._InsuranceReliefHeadId = CInt(cboInsuranceRelief.SelectedValue)
            objP9AReport._InsuranceReliefHeadName = cboInsuranceRelief.Text
            'Sohail (18 Jan 2017) -- End

            'Sohail (08 May 2020) -- Start
            'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
            objP9AReport._TaxbleIncomeHeadId = CInt(cboTaxableIcome.SelectedValue)
            objP9AReport._TaxbleIncomeHeadName = cboTaxableIcome.Text
            'Sohail (08 May 2020) -- End

            'Sohail (20 Mar 2021) -- Start
            'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
            objP9AReport._BasicSalaryFormula = txtBasicSalaryFormula.Text
            objP9AReport._TaxChargedFormula = txtTaxChargedFormula.Text
            'Sohail (20 Mar 2021) -- End

            objP9AReport._ViewByIds = mstrStringIds
            objP9AReport._ViewIndex = mintViewIdx
            objP9AReport._ViewByName = mstrStringName
            objP9AReport._Analysis_Fields = mstrAnalysis_Fields
            objP9AReport._Analysis_Join = mstrAnalysis_Join
            objP9AReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objP9AReport._Report_GroupName = mstrReport_GroupName


            'Anjan [03 December 2015] -- Start
            'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
            objP9AReport._MembershipId = CInt(cboMembership.SelectedValue)
            'Anjan [03 December 2015] -- End



            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function


#End Region

#Region " Forms "

    

    Private Sub frmP9AReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objP9AReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmP9AReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmP9AReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            ElseIf e.Control AndAlso e.KeyCode = Keys.R Then
                Call frmP9AReport_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmP9AReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmP9AReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objP9AReport._ReportName
            Me._Message = objP9AReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmP9AReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmP9AReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsP9AReport.SetMessages()
            objfrm._Other_ModuleNames = "clsP9AReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmP9AReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Buttons "

    Private Sub frmP9AReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)

            objP9AReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objP9AReport.generateReport(0, e.Type, enExportAction.None)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objP9AReport.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, ConfigParameter._Object._ExportReportPath, _
            '                               ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.Preview, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            objP9AReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                          objPeriod._Start_Date, _
                                          objPeriod._End_Date, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.Preview, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmP9AReport_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmP9AReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)

            objP9AReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objP9AReport.generateReport(0, e.Type, enExportAction.None)
            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objP9AReport.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                               User._Object._Userunkid, _
            '                               FinancialYear._Object._YearUnkid, _
            '                               Company._Object._Companyunkid, _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                               ConfigParameter._Object._UserAccessModeSetting, _
            '                               True, ConfigParameter._Object._ExportReportPath, _
            '                               ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            objP9AReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           objPeriod._Start_Date, _
                                           objPeriod._End_Date, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (03 Aug 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmP9AReport_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            'Sohail (01 Feb 2019) -- Start
            'HJFMRI Enhancement Support Issue Id # 3457 - 74.1 - Assistance to reset the saved selection in P9 mapped report.
            'If SetFilter() = False Then Exit Try
            'Sohail (01 Feb 2019) -- End

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.P9A_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Quarter_Value
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboQuarterValue.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.P9A_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.E1
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboE1.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.P9A_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.E2
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboE2.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.P9A_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.E3
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboE3.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.P9A_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Amount_Of_Interest
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboAmountOfInterest.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.P9A_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Tax_Charged
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTaxCharged.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.P9A_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Personal_Relief
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboPersonalRelief.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.P9A_Report, 0, 0, intHeadType)


                        'Anjan [03 December 2015] -- Start
                        'ENHANCEMENT : Included Membership No in Employee's PIN textbox on Rutta's request
                    Case enHeadTypeId.membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.P9A_Report, 0, 0, intHeadType)

                        'Anjan [03 December 2015] -- End

                        'Sohail (18 Jan 2017) -- Start
                        'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
                    Case enHeadTypeId.Insurance_Relief
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboInsuranceRelief.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.P9A_Report, 0, 0, intHeadType)
                        'Sohail (18 Jan 2017) -- End

                        'Sohail (08 May 2020) -- Start
                        'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
                    Case enHeadTypeId.Taxable_Income
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTaxableIcome.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.P9A_Report, 0, 0, intHeadType)
                        'Sohail (08 May 2020) -- End

                        'Sohail (20 Mar 2021) -- Start
                        'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
                    Case enHeadTypeId.BasicSalaryFormula
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtBasicSalaryFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.P9A_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.TaxChargedFormula
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = txtTaxChargedFormula.Text.Trim

                        intUnkid = objUserDefRMode.isExist(enArutiReport.P9A_Report, 0, 0, intHeadType)
                        'Sohail (20 Mar 2021) -- End

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    'Nilay (21-Oct-2016) -- Start
    'Enhancement : Kenya P9A statutory report on ESS for CCK
    Private Sub frmP9AReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmP9AReport_Reset_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (21-Oct-2016) -- End

#End Region

#Region " Combobox' Events "
    Private Sub cboQuarterValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQuarterValue.SelectedIndexChanged, _
                                                                                                                        cboE1.SelectedIndexChanged, _
                                                                                                                        cboE2.SelectedIndexChanged, _
                                                                                                                        cboE3.SelectedIndexChanged, _
                                                                                                                        cboAmountOfInterest.SelectedIndexChanged, _
                                                                                                                        cboTaxCharged.SelectedIndexChanged, _
                                                                                                                        cboPersonalRelief.SelectedIndexChanged, _
                                                                                                                        cboQuarterValue.SelectedIndexChanged, _
                                                                                                                        cboInsuranceRelief.SelectedIndexChanged
        'Sohail (18 Jan 2017) - [cboQuarterValue.SelectedIndexChanged, cboInsuranceRelief.SelectedIndexChanged]
        Try
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub cboAmountOfInterest_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboAmountOfInterest.Validating, _
                                                                                                                        cboE1.Validating, _
                                                                                                                        cboE2.Validating, _
                                                                                                                        cboE3.Validating, _
                                                                                                                        cboAmountOfInterest.Validating, _
                                                                                                                        cboTaxCharged.Validating, _
                                                                                                                        cboPersonalRelief.Validating, _
                                                                                                                        cboQuarterValue.Validating, _
                                                                                                                        cboInsuranceRelief.Validating
        'Sohail (18 Jan 2017) - [cboQuarterValue.Validating, cboInsuranceRelief.Validating]
        Try
            Dim cbo As ComboBox = CType(sender, ComboBox)

            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFilterCriteria.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboEmployee.Name AndAlso t.Name <> cboFromPeriod.Name AndAlso t.Name <> cboToPeriod.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Sorry, This transaction head is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboAmountOfInterest_Validating", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    Private Sub objbtnSearchE1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchE1.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboE1.DataSource
            frm.ValueMember = cboE1.ValueMember
            frm.DisplayMember = cboE1.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboE1.SelectedValue = frm.SelectedValue
                cboE1.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchE1_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchE2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchE2.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboE2.DataSource
            frm.ValueMember = cboE2.ValueMember
            frm.DisplayMember = cboE2.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboE2.SelectedValue = frm.SelectedValue
                cboE2.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchE2_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchE3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchE3.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboE3.DataSource
            frm.ValueMember = cboE3.ValueMember
            frm.DisplayMember = cboE3.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboE3.SelectedValue = frm.SelectedValue
                cboE3.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchE3_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchAmountOfInterest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchAmountOfInterest.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboAmountOfInterest.DataSource
            frm.ValueMember = cboAmountOfInterest.ValueMember
            frm.DisplayMember = cboAmountOfInterest.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboAmountOfInterest.SelectedValue = frm.SelectedValue
                cboAmountOfInterest.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchAmountOfInterest_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchTaxCharged_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTaxCharged.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboTaxCharged.DataSource
            frm.ValueMember = cboTaxCharged.ValueMember
            frm.DisplayMember = cboTaxCharged.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboTaxCharged.SelectedValue = frm.SelectedValue
                cboTaxCharged.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTaxCharged_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchPersonalRelief_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchPersonalRelief.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboPersonalRelief.DataSource
            frm.ValueMember = cboPersonalRelief.ValueMember
            frm.DisplayMember = cboPersonalRelief.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboPersonalRelief.SelectedValue = frm.SelectedValue
                cboPersonalRelief.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchPersonalRelief_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    'Sohail (18 Jan 2017) -- Start
    'CCK Enhancement - 64.1 - Allow to map Insurance Relief head on P9/a Report.
    Private Sub objbtnSearchInsuranceRelief_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchInsuranceRelief.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboInsuranceRelief.DataSource
            frm.ValueMember = cboInsuranceRelief.ValueMember
            frm.DisplayMember = cboInsuranceRelief.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboInsuranceRelief.SelectedValue = frm.SelectedValue
                cboInsuranceRelief.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchInsuranceRelief_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sohail (18 Jan 2017) -- End

    'Sohail (21 Nov 2014) -- Start
    'Enhancement - Search option for Quarter Value.
    Private Sub objbtnSearchQuarterValue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchQuarterValue.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboQuarterValue.DataSource
            frm.ValueMember = cboQuarterValue.ValueMember
            frm.DisplayMember = cboQuarterValue.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboQuarterValue.SelectedValue = frm.SelectedValue
                cboQuarterValue.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchQuarterValue_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sohail (21 Nov 2014) -- End

    'Sohail (08 May 2020) -- Start
    'Kenya Enhancement # : Provide mapping for column H in P9A report and caption it Taxable Income.
    Private Sub objbtnSearchTaxableIcome_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTaxableIcome.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboTaxableIcome.DataSource
            frm.ValueMember = cboTaxableIcome.ValueMember
            frm.DisplayMember = cboTaxableIcome.DisplayMember
            frm.CodeMember = "code"
            If frm.DisplayDialog Then
                cboTaxableIcome.SelectedValue = frm.SelectedValue
                cboTaxableIcome.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTaxableIcome_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sohail (08 May 2020) -- End

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim frm As New frmCommonSearch
            Try
                frm.DataSource = cboEmployee.DataSource
                frm.ValueMember = cboEmployee.ValueMember
                frm.DisplayMember = cboEmployee.DisplayMember
                frm.CodeMember = "employeecode"
                If frm.DisplayDialog Then
                    cboEmployee.SelectedValue = frm.SelectedValue
                    cboEmployee.Focus()
                End If
            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
            Finally
                frm = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    'Sohail (01 Feb 2019) -- Start
    'SLMC Enhancement Support Issue Id # 3453 - 74.1 - Include Analysis by in P9 report.
    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub
    'Sohail (01 Feb 2019) -- End

    'Sohail (20 Mar 2021) -- Start
    'FFK Kenya Enhancement : OLD-333 : Give free text mapping for column A and Tax Charged column on P9A statutory Report.
    Private Sub objbtnKeywords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnKeywordsBS.Click, objbtnKeywordsTC.Click
        Dim frm As New frmRemark
        Dim strB As New StringBuilder
        Try
            frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 15, "Awailable Keywords")
            frm.Text = frm.objgbRemarks.Text
            Dim strRemarks As String = ""
            strB.Length = 0

            strB.AppendLine("#headcode#      (e.g. #010#)")

            strRemarks = strB.ToString

            frm.displayDialog(strRemarks, enArutiApplicatinType.Aruti_Payroll)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnKeywords_Click", mstrModuleName)
        End Try
    End Sub
    'Sohail (20 Mar 2021) -- End

#End Region
    

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.Name, Me.lblToPeriod.Text)
			Me.lblFromPeriod.Text = Language._Object.getCaption(Me.lblFromPeriod.Name, Me.lblFromPeriod.Text)
			Me.lblQuarterValue.Text = Language._Object.getCaption(Me.lblQuarterValue.Name, Me.lblQuarterValue.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblE3.Text = Language._Object.getCaption(Me.lblE3.Name, Me.lblE3.Text)
			Me.lblE2.Text = Language._Object.getCaption(Me.lblE2.Name, Me.lblE2.Text)
			Me.lblE1.Text = Language._Object.getCaption(Me.lblE1.Name, Me.lblE1.Text)
			Me.lblAmountOfInterest.Text = Language._Object.getCaption(Me.lblAmountOfInterest.Name, Me.lblAmountOfInterest.Text)
			Me.lblTaxCharged.Text = Language._Object.getCaption(Me.lblTaxCharged.Name, Me.lblTaxCharged.Text)
			Me.lblPersonalRelief.Text = Language._Object.getCaption(Me.lblPersonalRelief.Name, Me.lblPersonalRelief.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblInsuranceRelief.Text = Language._Object.getCaption(Me.lblInsuranceRelief.Name, Me.lblInsuranceRelief.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblTaxableIcome.Text = Language._Object.getCaption(Me.lblTaxableIcome.Name, Me.lblTaxableIcome.Text)
			Me.lblBasicSalaryFormula.Text = Language._Object.getCaption(Me.lblBasicSalaryFormula.Name, Me.lblBasicSalaryFormula.Text)
			Me.lblTaxChargedFormula.Text = Language._Object.getCaption(Me.lblTaxChargedFormula.Name, Me.lblTaxChargedFormula.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "From Period is mandatory information. Please select From Period.")
			Language.setMessage(mstrModuleName, 2, "To Period is mandatory information. Please select To Period.")
			Language.setMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period.")
			Language.setMessage(mstrModuleName, 4, "Please select head for Value of Quarters.")
			Language.setMessage(mstrModuleName, 5, "Please select head for E1.")
			Language.setMessage(mstrModuleName, 6, "Please select head for E2.")
			Language.setMessage(mstrModuleName, 7, "Please select head for E3.")
			Language.setMessage(mstrModuleName, 8, "Please select head for Amount Of Interest.")
			Language.setMessage(mstrModuleName, 9, "Please select head for Tax Charged.")
			Language.setMessage(mstrModuleName, 10, "Please select head for Personal Relief.")
			Language.setMessage(mstrModuleName, 11, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 12, "Sorry, This transaction head is already mapped.")
			Language.setMessage(mstrModuleName, 13, "Please select head for Insurance Relief.")
			Language.setMessage(mstrModuleName, 14, "Please select head for Taxable Income.")
			Language.setMessage(mstrModuleName, 15, "Awailable Keywords")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
