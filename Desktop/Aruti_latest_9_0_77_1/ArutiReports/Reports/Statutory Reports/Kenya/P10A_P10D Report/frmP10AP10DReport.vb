'************************************************************************************************************************************
'Class Name : frmP10DReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmP10AP10DReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmP10DReport"
    Private objP10DReport As clsP10AP10DReport

    Private mstrPeriodsName As String = ""


    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    'Nilay (15-Dec-2015) -- Start
    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
    Private mdtPeriodStartDate As DateTime
    'Nilay (15-Dec-2015) -- End
    Private mdtPeriodEndDate As DateTime

#End Region

#Region " Contructor "

    Public Sub New()
        'Nilay (15-Dec-2015) -- Start
        'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
        _Show_ExcelExtra_Menu = True
        'Nilay (15-Dec-2015) -- End
        objP10DReport = New clsP10AP10DReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objP10DReport.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region "Private enum"
    Private Enum enHeadTypeId
        PAYE = 1
        EMPLOYEE = 2
        MEMBERSHIP = 3
        FRING_BENEFIT = 4
        TAXON = 5
        OTHER_EARNING = 6
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objperiod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objMembership As New clsmembership_master

        Dim dsCombos As DataSet

        Try
            With cboReportType
                .Items.Clear()
                .Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 11, "P10 A Report"), 1))
                .Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 12, "P10 D Report"), 2))
                'Nilay (15-Dec-2015) -- Start
                'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
                '.SelectedValue = 1
                .SelectedIndex = 0
                'Nilay (15-Dec-2015) -- End

            End With

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'dsCombos = objEmp.GetEmployeeList("Emp", True, False)
            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, _
                                              ConfigParameter._Object._IsIncludeInactiveEmp, _
                                              "Emp", True)
            'Nilay (15-Dec-2015) -- End

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'dsCombos = objTranHead.getComboList("Heads", False, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Taxes)
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", False, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Taxes)
            'Nilay (15-Dec-2015) -- End

            With cboPAYE
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                If dsCombos.Tables(0).Rows.Count > 0 Then .SelectedIndex = 0
            End With

            dsCombos = objMembership.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'dsCombos = objTranHead.getComboList("OtherEarning", True, , , enTypeOf.Other_Earnings)
            'Sohail (16 Mar 2016) -- Start
            'Enhancement - Include Informational Heads in Basic Salary As Other Earning option in all Statutory Reports.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , enTypeOf.Other_Earnings)

            'Anjan [26 September 2016] -- Start
            'ENHANCEMENT : Removing other earnings and including all earnings on all statutory reports as per Rutta's request.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "typeof_id = " & enTypeOf.Other_Earnings & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "OtherEarning", True, , , , , , "trnheadtype_id = " & enTranHeadType.EarningForEmployees & " OR trnheadtype_id = " & enTranHeadType.Informational & " ")
            'Anjan [26 September 2016] -- End

            'Sohail (16 Mar 2016) -- End
            'Nilay (15-Dec-2015) -- End

            With cboOtherEarning
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("OtherEarning")
                .SelectedValue = 0
            End With

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'dsCombos = objTranHead.getComboList("Heads", True, , , , , , "isnoncashbenefit = 1")

            'Anjan [07 February 2016] -- Start
            'ENHANCEMENT : On Rutta's request for KBC Kenya.
            'dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , "isnoncashbenefit = 1")
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, enTranHeadType.Informational, , , , , )
            'Anjan [07 February 2016] -- End


            'Nilay (15-Dec-2015) -- End

            With cboFringeBenefit
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'dsCombos = objTranHead.getComboList("Heads", True, enTranHeadType.Informational, , , , , "typeof_id <> " & enTypeOf.Salary & " ")
            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, enTranHeadType.Informational, , , , , "typeof_id <> " & enTypeOf.Salary & " ")
            'Nilay (15-Dec-2015) -- End

            With cboTaxOn
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objperiod = Nothing
            objTranHead = Nothing
            objMembership = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboOtherEarning.SelectedValue = 0
            cboFringeBenefit.SelectedValue = 0
            cboTaxOn.SelectedValue = 0
            gbBasicSalaryOtherEarning.Checked = ConfigParameter._Object._SetBasicSalaryAsOtherEarningOnStatutoryReport

            Call DoOperation(lvPeriod, False)

            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            Call GetValue()
            'Nilay (15-Dec-2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objP10DReport.SetDefaultValue()

            mstrPeriodsName = String.Empty

            If cboPAYE.Items.Count <= 0 OrElse CInt(cboPAYE.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select PAYE head."), enMsgBoxStyle.Information)
                cboPAYE.Focus()
                Return False
            ElseIf CInt(cboSlabEffectivePeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Effective Period is mandatory information. Please select Effective Period."), enMsgBoxStyle.Information)
                cboSlabEffectivePeriod.Focus()
                Return False
            ElseIf lvPeriod.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please check Periods to view report."), enMsgBoxStyle.Information)
                lvPeriod.Focus()
                Return False
            End If

            If gbBasicSalaryOtherEarning.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select transaction head."), enMsgBoxStyle.Information)
                cboOtherEarning.Focus()
                Return False
            End If

            objP10DReport._EmpId = cboEmployee.SelectedValue
            objP10DReport._EmpName = cboEmployee.Text


            Dim strSatutaryIds As String = ""
            objP10DReport._StatutoryIds = strSatutaryIds

            Dim strPreriodIds As String = String.Join(",", (From p In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToArray)
            objP10DReport._PeriodIds = strPreriodIds

            mstrPeriodsName = String.Join(", ", (From p In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (p.SubItems(colhPeriodName.Index).Text)).ToArray)
            objP10DReport._PeriodNames = mstrPeriodsName

            Dim objMaster As New clsMasterData

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            Dim lvFirstSelected = (From p In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (p)).First()
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(lvFirstSelected.Tag.ToString)
            mdtPeriodStartDate = objPeriod._Start_Date.Date
            'Nilay (15-Dec-2015) -- End

            Dim lvLastSelected = (From p In lvPeriod.CheckedItems.Cast(Of ListViewItem)() Select (p)).Last()
            mdtPeriodEndDate = eZeeDate.convertDate(lvLastSelected.SubItems(colhPeriodName.Index).Tag.ToString)
            objP10DReport._PeriodEndDate = mdtPeriodEndDate
            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'objP10DReport._LastPeriodYearName = objMaster.GetFinancialYear_Name(CInt(lvLastSelected.SubItems(colhYear.Index).Text))
            objP10DReport._LastPeriodYearName = objMaster.GetFinancialYear_Name(CInt(lvLastSelected.SubItems(colhYear.Index).Text), CInt(Company._Object._Companyunkid))
            'Nilay (15-Dec-2015) -- End

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            If CType(cboReportType.SelectedItem, ComboBoxValue).Value = 2 Then
                If lvPeriod.CheckedItems.Count <> 4 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "P10D is Quarterly report. So please check four periods."), enMsgBoxStyle.Information)
                    lvPeriod.Focus()
                    Return False
                Else
                    If DateDiff(DateInterval.Month, mdtPeriodStartDate, mdtPeriodEndDate.AddDays(1)) <> 4 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please check four consecutive periods only."), enMsgBoxStyle.Information)
                        lvPeriod.Focus()
                        Return False
                    End If
                End If
            End If
            'Nilay (15-Dec-2015) -- End
            objMaster = Nothing

            objP10DReport._PayeHeadId = CInt(cboPAYE.SelectedValue)
            objP10DReport._PayeHeadName = cboPAYE.Text

            objP10DReport._MembershipId = CInt(cboMembership.SelectedValue)
            objP10DReport._MembershipName = cboMembership.Text

            objP10DReport._FringeBenefitHeadId = CInt(cboFringeBenefit.SelectedValue)
            objP10DReport._FringeBenefitHeadName = cboFringeBenefit.Text

            objP10DReport._TaxOnHeadId = CInt(cboTaxOn.SelectedValue)
            objP10DReport._TaxOnHeadName = cboTaxOn.Text

            If gbBasicSalaryOtherEarning.Checked = True Then
                objP10DReport._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objP10DReport._OtherEarningTranId = 0
            End If


            objP10DReport._ViewByIds = mstrStringIds
            objP10DReport._ViewIndex = mintViewIdx
            objP10DReport._ViewByName = mstrStringName
            objP10DReport._Analysis_Fields = mstrAnalysis_Fields
            objP10DReport._Analysis_Join = mstrAnalysis_Join
            objP10DReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objP10DReport._Report_GroupName = mstrReport_GroupName

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objP10DReport._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(mdtPeriodEndDate))
            'Sohail (03 Aug 2019) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

    Private Sub DoOperation(ByVal LV As ListView, ByVal blnOperation As Boolean)
        Try
            For Each LvItem As ListViewItem In LV.Items
                LvItem.Checked = blnOperation
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DoOperation", mstrModuleName)
        End Try
    End Sub

    'Nilay (15-Dec-2015) -- Start
    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet = Nothing
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.P10A_P10D_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dRow.Item("headtypeid"))
                        Case enHeadTypeId.PAYE
                            cboPAYE.SelectedValue = CInt(dRow.Item("transactionheadid"))

                        Case enHeadTypeId.EMPLOYEE
                            cboEmployee.SelectedValue = CInt(dRow.Item("transactionheadid"))

                        Case enHeadTypeId.MEMBERSHIP
                            cboMembership.SelectedValue = CInt(dRow.Item("transactionheadid"))

                        Case enHeadTypeId.FRING_BENEFIT
                            cboFringeBenefit.SelectedValue = CInt(dRow.Item("transactionheadid"))

                        Case enHeadTypeId.TAXON
                            cboTaxOn.SelectedValue = CInt(dRow.Item("transactionheadid"))

                        Case enHeadTypeId.OTHER_EARNING
                            cboOtherEarning.SelectedValue = CInt(dRow.Item("transactionheadid"))
                            If CInt(dRow.Item("transactionheadid")) > 0 Then
                                gbBasicSalaryOtherEarning.Checked = True
                            Else
                                gbBasicSalaryOtherEarning.Checked = False
                            End If
                    End Select
                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub
    'Nilay (15-Dec-2015) -- End


#End Region

#Region " Forms "

    Private Sub frmP10DReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objP10DReport = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmP10DReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmP10DReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            ElseIf e.Control AndAlso e.KeyCode = Keys.R Then
                Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmP10DReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objP10DReport._ReportName
            Me._Message = objP10DReport._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmP10DReport_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'If Not SetFilter() Then Exit Sub
            'objP10DReport.generateReport(0, e.Type, enExportAction.None)
            If Not SetFilter() Then Exit Sub

            objP10DReport._ReportTypeId = CType(cboReportType.SelectedItem, ComboBoxValue).Value

            objP10DReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            mdtPeriodStartDate, _
                                            mdtPeriodEndDate, _
                                            ConfigParameter._Object._UserAccessModeSetting, True, _
                                            ConfigParameter._Object._ExportReportPath, _
                                            ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None)
            'Nilay (15-Dec-2015) -- End

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'If Not SetFilter() Then Exit Sub
            'objP10DReport.generateReport(0, enPrintAction.None, e.Type)

            If Not SetFilter() Then Exit Sub

            objP10DReport._ReportTypeId = CType(cboReportType.SelectedItem, ComboBoxValue).Value

            objP10DReport.generateReportNew(FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, _
                                            FinancialYear._Object._YearUnkid, _
                                            Company._Object._Companyunkid, _
                                            mdtPeriodStartDate, _
                                            mdtPeriodEndDate, _
                                            ConfigParameter._Object._UserAccessModeSetting, True, _
                                            ConfigParameter._Object._ExportReportPath, _
                                            ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type)
            'Nilay (15-Dec-2015) -- End

            
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployee.DataSource
            frm.ValueMember = cboEmployee.ValueMember
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployee.SelectedValue = frm.SelectedValue
                cboEmployee.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsP10AP10DReport.SetMessages()
            objfrm._Other_ModuleNames = "clsP10DReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    'Nilay (15-Dec-2015) -- Start
    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Sub

            For intHeadType As Integer = 1 To 6
                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.P10A_P10D_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType
                    Case enHeadTypeId.PAYE
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboPAYE.SelectedValue.ToString
                        intUnkid = objUserDefRMode.isExist(enArutiReport.P10A_P10D_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.EMPLOYEE
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboEmployee.SelectedValue.ToString
                        intUnkid = objUserDefRMode.isExist(enArutiReport.P10A_P10D_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.MEMBERSHIP
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString
                        intUnkid = objUserDefRMode.isExist(enArutiReport.P10A_P10D_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.FRING_BENEFIT
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboFringeBenefit.SelectedValue.ToString
                        intUnkid = objUserDefRMode.isExist(enArutiReport.P10A_P10D_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.TAXON
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTaxOn.SelectedValue.ToString
                        intUnkid = objUserDefRMode.isExist(enArutiReport.P10A_P10D_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.OTHER_EARNING
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboOtherEarning.SelectedValue.ToString
                        intUnkid = objUserDefRMode.isExist(enArutiReport.P10A_P10D_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "btnSaveSelection_Click", mstrModuleName)
        End Try
    End Sub
    'Nilay (15-Dec-2015) -- End


    
#End Region

#Region " Controls "

    Private Sub objchkAllPeriod_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkAllPeriod.CheckedChanged
        Try
            Call DoOperation(lvPeriod, CBool(objchkAllPeriod.CheckState))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkAllPeriod_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub lvPeriod_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPeriod.ItemChecked
        Try
            RemoveHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
            If lvPeriod.CheckedItems.Count <= 0 Then
                objchkAllPeriod.CheckState = CheckState.Unchecked
            ElseIf lvPeriod.CheckedItems.Count < lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Indeterminate
            ElseIf lvPeriod.CheckedItems.Count = lvPeriod.Items.Count Then
                objchkAllPeriod.CheckState = CheckState.Checked
            End If
            AddHandler objchkAllPeriod.CheckedChanged, AddressOf objchkAllPeriod_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvPeriod_ItemChecked", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex

            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy
            mstrReport_GroupName = frm._Report_GroupName

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub cboPAYE_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPAYE.SelectedIndexChanged
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As DataSet
        Try
            If CInt(cboPAYE.SelectedValue) <= 0 Then Exit Try

            Dim objHead As New clsTransactionHead
            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'objHead._Tranheadunkid = CInt(cboPAYE.SelectedValue)
            objHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboPAYE.SelectedValue)
            'Nilay (15-Dec-2015) -- End

            If objHead._Calctype_Id = enCalcType.AsComputedValue Then
                Dim objSimpleSlab As New clsTranheadSlabTran
                dsCombos = objSimpleSlab.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0, CInt(cboPAYE.SelectedValue))
                objSimpleSlab = Nothing
            ElseIf objHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then
                dsCombos = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0, CInt(cboPAYE.SelectedValue))
            Else
                Dim objMaster As New clsMasterData
                Dim objPeriod As New clscommom_period_Tran
                'Nilay (15-Dec-2015) -- Start
                'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
                'Dim intFirstPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, , , True)
                Dim intFirstPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(FinancialYear._Object._YearUnkid), , True)

                'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Slab", True, 0)
                dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, _
                                                     FinancialYear._Object._Database_Start_Date, "Slab", True, 0, )
                'Nilay (15-Dec-2015) -- End


                Dim dtTable As DataTable = New DataView(dsCombos.Tables("Slab"), "periodunkid IN (0, " & intFirstPeriod & ")", "", DataViewRowState.CurrentRows).ToTable
                dsCombos.Tables.Clear()
                dsCombos.Tables.Add(dtTable)
            End If

            With cboSlabEffectivePeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Slab")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPAYE_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboSlabEffectivePeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSlabEffectivePeriod.SelectedIndexChanged
        Dim objperiod As New clscommom_period_Tran
        Dim objExcessSlan As New clsTranheadInexcessslabTran
        Dim dsCombos As DataSet
        Dim dtTable As DataTable
        Dim strNextSlabFilter As String = ""

        Try
            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, _
                                                 FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False)
            'Nilay (15-Dec-2015) -- End


            If CInt(cboSlabEffectivePeriod.SelectedValue) > 0 Then
                'Nilay (15-Dec-2015) -- Start
                'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
                'objperiod._Periodunkid = CInt(cboSlabEffectivePeriod.SelectedValue)
                objperiod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboSlabEffectivePeriod.SelectedValue)
                'Nilay (15-Dec-2015) -- End

                Dim objHead As New clsTransactionHead
                Dim ds As DataSet
                'Nilay (15-Dec-2015) -- Start
                'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
                'objHead._Tranheadunkid = CInt(cboPAYE.SelectedValue)
                objHead._Tranheadunkid(FinancialYear._Object._DatabaseName) = CInt(cboPAYE.SelectedValue)
                'Nilay (15-Dec-2015) -- End

                If objHead._Calctype_Id = enCalcType.AsComputedValue Then
                    Dim objSimpleSlab As New clsTranheadSlabTran
                    ds = objSimpleSlab.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0, CInt(cboPAYE.SelectedValue))
                    objSimpleSlab = Nothing
                ElseIf objHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab Then
                    ds = objExcessSlan.GetSlabPeriodList(enModuleReference.Payroll, 0, "Slab", True, 0, CInt(cboPAYE.SelectedValue))
                Else
                    Dim objPeriodTmp As New clscommom_period_Tran
                    'Nilay (15-Dec-2015) -- Start
                    'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
                    'ds = objPeriodTmp.getListForCombo(enModuleReference.Payroll, 0, "Slab", False)
                    ds = objPeriodTmp.getListForCombo(enModuleReference.Payroll, CInt(FinancialYear._Object._YearUnkid), _
                                                      FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Slab", False)
                    'Nilay (15-Dec-2015) -- End

                End If

                Dim dt As DataTable = New DataView(ds.Tables("Slab"), "end_date > '" & eZeeDate.convertDate(objperiod._End_Date) & "' ", "", DataViewRowState.CurrentRows).ToTable
                If dt.Rows.Count > 0 And objHead._Calctype_Id <> enCalcType.FlatRate_Others Then
                    strNextSlabFilter = " AND end_date < '" & dt.Rows(0).Item("end_date").ToString & "' "
                End If

                dtTable = New DataView(dsCombos.Tables("Period"), "end_date >= '" & eZeeDate.convertDate(objperiod._End_Date) & "' " & strNextSlabFilter & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsCombos.Tables("Period")).ToTable
                dtTable.Rows.Clear()
            End If

            lvPeriod.Items.Clear()
            Dim lvItem As ListViewItem
            For Each dtRow As DataRow In dtTable.Rows
                lvItem = New ListViewItem
                lvItem.Text = ""
                lvItem.SubItems.Add(dtRow.Item("name").ToString)
                lvItem.Tag = dtRow.Item("periodunkid")
                lvItem.SubItems(colhPeriodName.Index).Tag = dtRow.Item("end_date").ToString
                lvItem.SubItems.Add(dtRow.Item("yearunkid").ToString)
                'Nilay (15-Dec-2015) -- Start
                'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
                lvItem.SubItems.Add(dtRow.Item("start_date").ToString)
                'Nilay (15-Dec-2015) -- End

                lvPeriod.Items.Add(lvItem)
                lvItem = Nothing
            Next
            lvPeriod.GridLines = False

            If lvPeriod.Items.Count > 12 Then
                colhPeriodName.Width = 205 - 18
            Else
                colhPeriodName.Width = 205
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSlabEffectivePeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchOtherEarning_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchOtherEarning.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboOtherEarning.DataSource
            frm.ValueMember = cboOtherEarning.ValueMember
            frm.DisplayMember = cboOtherEarning.DisplayMember
            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'frm.CodeMember = "trnheadcode"
            frm.CodeMember = "code"
            'Nilay (15-Dec-2015) -- End

            If frm.DisplayDialog Then
                cboOtherEarning.SelectedValue = frm.SelectedValue
                cboOtherEarning.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchOtherEarning_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchFringeBenefit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFringeBenefit.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboFringeBenefit.DataSource
            frm.ValueMember = cboFringeBenefit.ValueMember
            frm.DisplayMember = cboFringeBenefit.DisplayMember
            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'frm.CodeMember = "trnheadcode"
            frm.CodeMember = "code"
            'Nilay (15-Dec-2015) -- End

            If frm.DisplayDialog Then
                cboFringeBenefit.SelectedValue = frm.SelectedValue
                cboFringeBenefit.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFringeBenefit_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub objbtnSearchTaxOn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchTaxOn.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboTaxOn.DataSource
            frm.ValueMember = cboTaxOn.ValueMember
            frm.DisplayMember = cboTaxOn.DisplayMember
            'Nilay (15-Dec-2015) -- Start
            'Enhancement - NEW Statutory Reports P10A, P10B and P10D Reports for Kenya.
            'frm.CodeMember = "trnheadcode"
            frm.CodeMember = "code"
            'Nilay (15-Dec-2015) -- End

            If frm.DisplayDialog Then
                cboTaxOn.SelectedValue = frm.SelectedValue
                cboTaxOn.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchTaxOn_Click", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

    Private Sub gbBasicSalaryOtherEarning_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gbBasicSalaryOtherEarning.CheckedChanged
        Try
            If gbBasicSalaryOtherEarning.Checked = False Then
                cboOtherEarning.SelectedValue = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gbBasicSalaryOtherEarning_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbBasicSalaryOtherEarning.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBasicSalaryOtherEarning.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
            Me.colhPeriodName.Text = Language._Object.getCaption(CStr(Me.colhPeriodName.Tag), Me.colhPeriodName.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblPAYE.Text = Language._Object.getCaption(Me.lblPAYE.Name, Me.lblPAYE.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblOtherEarning.Text = Language._Object.getCaption(Me.lblOtherEarning.Name, Me.lblOtherEarning.Text)
            Me.gbBasicSalaryOtherEarning.Text = Language._Object.getCaption(Me.gbBasicSalaryOtherEarning.Name, Me.gbBasicSalaryOtherEarning.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select PAYE head.")
            Language.setMessage(mstrModuleName, 2, "Effective Period is mandatory information. Please select Effective Period.")
            Language.setMessage(mstrModuleName, 3, "Please check atleast one Employee statutary deduction head.")
            Language.setMessage(mstrModuleName, 4, "Please check Periods to view report.")
            Language.setMessage(mstrModuleName, 5, "Please select transaction head.")
            Language.setMessage(mstrModuleName, 6, "P10D is Quarterly report. So please check four periods.")
            Language.setMessage(mstrModuleName, 7, "Please check four consecutive periods only.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
