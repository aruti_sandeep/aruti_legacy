﻿'************************************************************************************************************************************
'Class Name : frmNITAReport.vb
'Purpose    : 
'Written By : Sohail.
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmNITAReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmNITAReport"
    Private objNITA As clsNITAReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private eReport As enArutiReport
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Contructor "

    Public Sub New()
        objNITA = New clsNITAReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objNITA.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        Membership = 1
        Include_Membership = 2
        NITA_Deduction = 3
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMember As New clsmembership_master
        Dim dsCombos As New DataSet
        Dim objperiod As New clscommom_period_Tran
        Dim objHead As New clsTransactionHead
        Dim objMaster As New clsMasterData
        Try
            'Sohail (08 Jul 2022) -- Start
            'Enhancement : AC2-670 : Kenya - As a user, I want to be able to map any membership on the NITA report - Remove restriction which only allows to map payroll memberships.
            'dsCombos = objMember.getListForCombo("Membership", True, , 1)
            dsCombos = objMember.getListForCombo("Membership", True, , 0)
            'Sohail (08 Jul 2022) -- End
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            dsCombos = objMember.getListForCombo("Membership", True, , 2)
            With cboIncludeMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Membership")
                .SelectedValue = 0
            End With

            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)

            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
            End With

            dsCombos = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, strFilter:=" prtranhead_master.trnheadtype_id <> " & enTranHeadType.EarningForEmployees & " ")
            With cboNITADeduction
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objMember = Nothing
            objperiod = Nothing
            objHead = Nothing
            objMaster = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboIncludeMembership.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            cboMembership.SelectedValue = 0
            cboNITADeduction.SelectedValue = 0

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""


            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.NITA_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Membership
                            cboMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Include_Membership
                            cboIncludeMembership.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.NITA_Deduction
                            cboNITADeduction.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            Call objNITA.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
            ElseIf CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Exit Function
            ElseIf CInt(cboIncludeMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "KRA PIN Membership is mandatory information. Please select KRA PIN Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Exit Function
            ElseIf CInt(cboNITADeduction.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "NITA Deduction head is mandatory information. Please select NITA Deduction head to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Exit Function
            End If

            objNITA._PeriodId = CInt(cboPeriod.SelectedValue)
            objNITA._PeriodName = cboPeriod.Text

            objNITA._MembershipId = CInt(cboMembership.SelectedValue)
            objNITA._MembershipName = cboMembership.Text

            objNITA._NonPayrollMembershipId = CInt(cboIncludeMembership.SelectedValue)
            objNITA._NonPayrollMembershipName = cboIncludeMembership.Text

            objNITA._NITADeductionHeadId = CInt(cboNITADeduction.SelectedValue)
            objNITA._NITADeductionHeadName = cboNITADeduction.Text

            objNITA._ViewByIds = mstrStringIds
            objNITA._ViewIndex = mintViewIdx
            objNITA._ViewByName = mstrStringName
            objNITA._Analysis_Fields = mstrAnalysis_Fields
            objNITA._Analysis_Join = mstrAnalysis_Join
            objNITA._Report_GroupName = mstrReport_GroupName


            objNITA._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmNITAReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objNITA = Nothing
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmNITAReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmNITAReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Me.eZeeHeader.Title = objNITA._ReportName
            Me.eZeeHeader.Message = objNITA._ReportDesc

            Call OtherSettings()

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmNITAReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then

                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub

            SaveDialog.FileName = ""
            SaveDialog.Filter = "CSV File|*.csv"
            SaveDialog.FilterIndex = 0
            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If


            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)

            objNITA._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, objPeriod._End_Date)


            If objNITA.Generate_DetailReport(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             objPeriod._Start_Date, _
                                             objPeriod._End_Date, _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, _
                                             ConfigParameter._Object._Base_CurrencyId, _
                                             GUI.fmtCurrency, _
                                             SaveDialog.FileName _
                                             ) = True Then

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Data Exported Successfuly."), enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsNITAReport.SetMessages()
            objfrm._Other_ModuleNames = "clsNITAReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "objbtnLanguage_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.NITA_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NITA_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.Include_Membership
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboIncludeMembership.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NITA_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.NITA_Deduction
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboNITADeduction.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.NITA_Report, 0, 0, intHeadType)

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub


#End Region

#Region "ComboBox Event"

    Private Sub cboNITADeduction_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboNITADeduction.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    If cbo.Name = cboNITADeduction.Name Then
                        .CodeMember = "code"
                    End If
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor

			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblIncludeMembership.Text = Language._Object.getCaption(Me.lblIncludeMembership.Name, Me.lblIncludeMembership.Text)
			Me.LblCurrencyRate.Text = Language._Object.getCaption(Me.LblCurrencyRate.Name, Me.LblCurrencyRate.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblNITADeduction.Text = Language._Object.getCaption(Me.lblNITADeduction.Name, Me.lblNITADeduction.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 2, "Membership is mandatory information. Please select Membership to continue.")
			Language.setMessage(mstrModuleName, 3, "KRA PIN Membership is mandatory information. Please select KRA PIN Membership to continue.")
			Language.setMessage(mstrModuleName, 4, "NITA Deduction head is mandatory information. Please select NITA Deduction head to continue.")
			Language.setMessage(mstrModuleName, 5, "Data Exported Successfuly.")
			Language.setMessage(mstrModuleName, 6, "Selection Saved Successfully")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class