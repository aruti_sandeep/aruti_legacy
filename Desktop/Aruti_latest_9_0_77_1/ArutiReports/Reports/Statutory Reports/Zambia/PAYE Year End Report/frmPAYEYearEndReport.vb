﻿'************************************************************************************************************************************
'Class Name : frmPAYEYearEndReport.vb
'Purpose    : 
'Written By : Sohail
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmPAYEYearEndReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPAYEYearEndReport"
    Private objPAYE As clsPAYEYearEndReport
    Private mintFirstPeriodId As Integer = 0
    Private mintLastPeriodId As Integer = 0

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mdtPeriodEndDate As DateTime

#End Region

#Region " Contructor "

    Public Sub New()
        objPAYE = New clsPAYEYearEndReport(User._Object._Languageunkid, Company._Object._Companyunkid)
        objPAYE.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        PayInMonth = 1
        FreePay = 2
        TaxDue = 3
        PAYE = 4
        TaxRefund = 5
        'Hemant (22 Dec 2022) -- Start
        'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
        TPIN = 6
        'Hemant (22 Dec 2022) -- End
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objMaster As New clsMasterData
        'Hemant (22 Dec 2022) -- Start
        'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
        Dim objCMaster As New clsCommon_Master
        'Hemant (22 Dec 2022) -- End	
        Dim dsCombos As DataSet

        Try

            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, True, "Emp", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            mintFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)


            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)

            Dim dsYear As DataSet = objMaster.Get_Database_Year_List("Year", True, Company._Object._Companyunkid)
            Dim dt As DataTable = New DataView(dsYear.Tables(0), "start_date >= '" & eZeeDate.convertDate(FinancialYear._Object._Database_Start_Date.AddYears(-1)) & "' AND end_date <= '" & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date.AddYears(-1)) & "' ", "", DataViewRowState.CurrentRows).ToTable
            Dim strYear As String = String.Join(",", (From p In dt Select (p.Item("yearunkid").ToString)).ToArray)
            Dim mdtPeriod As DataTable = New DataView(dsCombos.Tables("Period"), "yearunkid IN (0, " & strYear & ") ", "", DataViewRowState.CurrentRows).ToTable
            If mdtPeriod.Rows.Count > 1 Then
                If CInt(mdtPeriod.Rows(1).Item("periodunkid")) > 0 Then mintFirstPeriodId = CInt(mdtPeriod.Rows(1).Item("periodunkid"))
                mintLastPeriodId = CInt(mdtPeriod.Rows(mdtPeriod.Rows.Count - 1).Item("periodunkid"))
            End If
            If mintLastPeriodId <= 0 Then mintLastPeriodId = mintFirstPeriodId

            With cboFromPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstPeriodId
            End With

            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = mintLastPeriodId
            End With


            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " ")
            With cboPayInMonth
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            With cboFreePay
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboTaxDue
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboPAYE
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboTaxRefund
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")
            With cboTPIN
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            'Hemant (22 Dec 2022) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objPeriod = Nothing
            objTranHead = Nothing
            objMaster = Nothing
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
            objCMaster = Nothing
            'Hemant (22 Dec 2022) -- End   
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPayInMonth.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboFromPeriod.SelectedValue = mintFirstPeriodId
            cboToPeriod.SelectedValue = mintLastPeriodId
            cboFreePay.SelectedValue = 0
            cboTaxDue.SelectedValue = 0
            cboPAYE.SelectedValue = 0
            cboTaxRefund.SelectedValue = 0
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
            cboTPIN.SelectedValue = 0
            'Hemant (22 Dec 2022) -- End

            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.PAYE_Year_End_Report)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.PayInMonth
                            cboPayInMonth.SelectedValue = CInt(dsRow.Item("transactionheadid"))


                        Case enHeadTypeId.FreePay
                            cboFreePay.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.TaxDue
                            cboTaxDue.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.PAYE
                            cboPAYE.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.TaxRefund
                            cboTaxRefund.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                            'Hemant (22 Dec 2022) -- Start
                            'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
                        Case enHeadTypeId.TPIN
                            cboTPIN.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Hemant (22 Dec 2022) -- End

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim intPrevYearID As Integer = 0
        Dim STC_FYYears As New List(Of clsPAYEYearEndReport.STC_FYYear)
        Try
            objPAYE.SetDefaultValue()


            If CInt(cboFromPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "From Period is mandatory information. Please select From Period."), enMsgBoxStyle.Information)
                cboFromPeriod.Focus()
                Return False
            ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "To Period is mandatory information. Please select To Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            ElseIf CInt(cboFromPeriod.SelectedIndex) > CInt(cboToPeriod.SelectedIndex) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            ElseIf CInt(cboPayInMonth.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select head for Pay In Month."), enMsgBoxStyle.Information)
                cboPayInMonth.Focus()
                Return False
            ElseIf CInt(cboFreePay.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select head for Free Pay."), enMsgBoxStyle.Information)
                cboFreePay.Focus()
                Return False
            ElseIf CInt(cboTaxDue.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select head for Tax Due."), enMsgBoxStyle.Information)
                cboTaxDue.Focus()
                Return False
            ElseIf CInt(cboPAYE.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select head for PAYE."), enMsgBoxStyle.Information)
                cboPAYE.Focus()
                Return False
            ElseIf CInt(cboTaxRefund.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select head for Tax Refund."), enMsgBoxStyle.Information)
                cboTaxRefund.Focus()
                Return False
                'Hemant (22 Dec 2022) -- Start
                'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
            ElseIf CInt(cboTPIN.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Please select TPIN."), enMsgBoxStyle.Information)
                cboTPIN.Focus()
                Return False
                'Hemant (22 Dec 2022) -- End
            End If


            objPAYE._EmpId = cboEmployee.SelectedValue
            objPAYE._EmpName = cboEmployee.Text

            Dim i As Integer = 0

            Dim strPreriodIds As String = cboFromPeriod.SelectedValue.ToString
            Dim mstrPeriodsName As String = cboFromPeriod.Text
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid")))
            If dsList.Tables("Database").Rows.Count > 0 Then
                intPrevYearID = CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid"))

                If STC_FYYears.Exists(Function(x) x.YearUnkid = intPrevYearID) = False Then
                    STC_FYYears.Add(New clsPAYEYearEndReport.STC_FYYear With {.YearUnkid = intPrevYearID, .DatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString, .EndDate = eZeeDate.convertDate(dsList.Tables("Database").Rows(0).Item("end_date").ToString)})
                End If
            End If

            For Each dsRow As DataRow In CType(cboFromPeriod.DataSource, DataTable).Rows
                If i > cboFromPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                    strPreriodIds &= ", " & dsRow.Item("periodunkid").ToString
                    mstrPeriodsName &= ", " & dsRow.Item("name").ToString
                    If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                        dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(dsRow.Item("yearunkid")))
                        If dsList.Tables("Database").Rows.Count > 0 Then

                            If STC_FYYears.Exists(Function(x) x.YearUnkid = CInt(dsRow.Item("yearunkid"))) = False Then
                                STC_FYYears.Add(New clsPAYEYearEndReport.STC_FYYear With {.YearUnkid = CInt(dsRow.Item("yearunkid")), .DatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString, .EndDate = eZeeDate.convertDate(dsList.Tables("Database").Rows(0).Item("end_date").ToString)})
                            End If
                        End If
                    End If
                    intPrevYearID = CInt(dsRow.Item("yearunkid"))
                End If
                i += 1
            Next
            objPAYE._STC_FYYear = STC_FYYears
            objPAYE._PeriodIds = strPreriodIds
            objPAYE._PeriodNames = mstrPeriodsName

            objPAYE._FirstPeriodYearName = eZeeDate.convertDate(CType(cboToPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Year.ToString


            objPAYE._PayInMonthHeadId = CInt(cboPayInMonth.SelectedValue)
            objPAYE._PayInMonthHeadName = cboPayInMonth.Text

            objPAYE._FreePayHeadId = CInt(cboFreePay.SelectedValue)
            objPAYE._FreePayHeadName = cboFreePay.Text

            objPAYE._TaxDueHeadId = CInt(cboTaxDue.SelectedValue)
            objPAYE._TaxDueHeadName = cboTaxDue.Text

            objPAYE._PAYEHeadId = CInt(cboPAYE.SelectedValue)
            objPAYE._PAYEHeadName = cboPAYE.Text

            objPAYE._TaxRefundHeadId = CInt(cboTaxRefund.SelectedValue)
            objPAYE._TaxRefundHeadName = cboTaxRefund.Text

            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
            objPAYE._TPINId = CInt(cboTPIN.SelectedValue)
            objPAYE._TPINName = cboTPIN.Text
            'Hemant (22 Dec 2022) -- End

            objPAYE._ViewByIds = mstrStringIds
            objPAYE._ViewIndex = mintViewIdx
            objPAYE._ViewByName = mstrStringName
            objPAYE._Analysis_Fields = mstrAnalysis_Fields
            objPAYE._Analysis_Join = mstrAnalysis_Join
            objPAYE._Analysis_OrderBy = mstrAnalysis_OrderBy
            objPAYE._Report_GroupName = mstrReport_GroupName



            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function


#End Region

#Region " Forms Events "

    Private Sub frmPAYEYearEndReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPAYE = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPAYEYearEndReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPAYEYearEndReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            ElseIf e.Control AndAlso e.KeyCode = Keys.R Then
                Call frmPAYEYearEndReport_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPAYEYearEndReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPAYEYearEndReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objPAYE._ReportName
            Me._Message = objPAYE._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPAYEYearEndReport_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPAYEYearEndReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPAYEYearEndReport.SetMessages()
            objfrm._Other_ModuleNames = "clsPAYEYearEndReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmPAYEYearEndReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Buttons Events "

    Private Sub frmPAYEYearEndReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)

            objPAYE._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objPAYE.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                          objPeriod._Start_Date, _
                                          objPeriod._End_Date, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.Preview, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPAYEYearEndReport_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmPAYEYearEndReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)

            objPAYE._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objPAYE.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           objPeriod._Start_Date, _
                                           objPeriod._End_Date, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPAYEYearEndReport_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.PAYE_Year_End_Report
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.PayInMonth
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboPayInMonth.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_Year_End_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.FreePay
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboFreePay.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_Year_End_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.TaxDue
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTaxDue.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_Year_End_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.PAYE
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboPAYE.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_Year_End_Report, 0, 0, intHeadType)

                    Case enHeadTypeId.TaxRefund
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTaxRefund.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_Year_End_Report, 0, 0, intHeadType)

                        'Hemant (22 Dec 2022) -- Start
                        'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
                    Case enHeadTypeId.TPIN
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTPIN.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_Year_End_Report, 0, 0, intHeadType)
                        'Hemant (22 Dec 2022) -- End
                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub frmPAYEYearEndReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPAYEYearEndReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox' Events "

    Private Sub cboPayInMonth_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboPayInMonth.Validating, _
                                                                                                                        cboFreePay.Validating, _
                                                                                                                        cboTaxDue.Validating, _
                                                                                                                        cboPAYE.Validating, _
                                                                                                                        cboTaxRefund.Validating
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFilterCriteria.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboEmployee.Name AndAlso t.Name <> cboFromPeriod.Name AndAlso t.Name <> cboToPeriod.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, This transaction head is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Validating", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPayInMonth_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPayInMonth.KeyPress, cboFreePay.KeyPress, cboTaxDue.KeyPress, cboPAYE.KeyPress, cboTaxRefund.KeyPress _
                                                                                                                        , cboEmployee.KeyPress, cboFromPeriod.KeyPress, cboToPeriod.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)

                    If cbo.Name = cboEmployee.Name Then
                        .CodeMember = "employeecode"
                    Else
                        .CodeMember = "code"
                    End If

                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    cbo.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                    cbo.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub


#End Region



    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			 
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblTaxDue.Text = Language._Object.getCaption(Me.lblTaxDue.Name, Me.lblTaxDue.Text)
			Me.lblFreePay.Text = Language._Object.getCaption(Me.lblFreePay.Name, Me.lblFreePay.Text)
			Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.Name, Me.lblToPeriod.Text)
			Me.lblFromPeriod.Text = Language._Object.getCaption(Me.lblFromPeriod.Name, Me.lblFromPeriod.Text)
			Me.lblPayInMonth.Text = Language._Object.getCaption(Me.lblPayInMonth.Name, Me.lblPayInMonth.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblPAYE.Text = Language._Object.getCaption(Me.lblPAYE.Name, Me.lblPAYE.Text)
			Me.lblTaxRefund.Text = Language._Object.getCaption(Me.lblTaxRefund.Name, Me.lblTaxRefund.Text)
            Me.lblTPIN.Text = Language._Object.getCaption(Me.lblTPIN.Name, Me.lblTPIN.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "From Period is mandatory information. Please select From Period.")
			Language.setMessage(mstrModuleName, 2, "To Period is mandatory information. Please select To Period.")
			Language.setMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period.")
			Language.setMessage(mstrModuleName, 4, "Please select head for Pay In Month.")
			Language.setMessage(mstrModuleName, 5, "Please select head for Free Pay.")
			Language.setMessage(mstrModuleName, 6, "Please select head for Tax Due.")
			Language.setMessage(mstrModuleName, 7, "Please select head for PAYE.")
			Language.setMessage(mstrModuleName, 8, "Please select head for Tax Refund.")
			Language.setMessage(mstrModuleName, 9, "Selection Saved Successfully")
			Language.setMessage(mstrModuleName, 10, "Sorry, This transaction head is already mapped.")
            Language.setMessage(mstrModuleName, 11, "Please select TPIN.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class