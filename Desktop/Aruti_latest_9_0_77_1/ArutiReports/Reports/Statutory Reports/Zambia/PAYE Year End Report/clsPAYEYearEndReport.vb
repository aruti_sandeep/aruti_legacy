﻿'************************************************************************************************************************************
'Class Name : clsPAYEYearEndReport.vb
'Purpose    :
'Date       :28/03/2022
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System
Imports System.Text.RegularExpressions
Imports System.Text

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sohail
''' </summary>
Public Class clsPAYEYearEndReport
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPAYEYearEndReport"
    Private mstrReportId As String = enArutiReport.PAYE_Year_End_Report

    Dim objDataOperation As clsDataOperation

    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass

#Region " Constructor "

    Public Sub New(ByVal intLangId As Integer, ByVal intCompanyId As Integer)
        Me.setReportData(CInt(mstrReportId), intLangId, intCompanyId)
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Structure "

    Public Structure STC_FYYear
        Dim YearUnkid As Integer
        Dim DatabaseName As String
        Dim EndDate As Date
    End Structure

#End Region

#Region " Private Variables "

    'For Report
    Private mintEmpId As Integer = 0
    Private mstrEmpName As String = ""
    Private mstrPeriodIds As String = ""
    Private mstrPeriodNames As String = String.Empty
    Private mintPayInMonthHeadId As Integer = 0
    Private mstrPayInMonthHeadName As String = ""
    Private mintFreePayHeadId As Integer = 0
    Private mstrFreePayHeadName As String = ""
    Private mintTaxDueHeadId As Integer = 0
    Private mstrTaxDueHeadName As String = ""
    Private mintPAYEHeadId As Integer = 0
    Private mstrPAYEHeadName As String = ""
    Private mintTaxRefundHeadId As Integer = 0
    Private mstrTaxRefundHeadName As String = ""

    Private mblnIncludeInactiveEmp As Boolean = True
    Private mSTCFYYear As New List(Of STC_FYYear)
    Private mstrFirstPeriodYearName As String = ""

    'For Analysis By
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = ""

    Private mblnApplyUserAccessFilter As Boolean = True
    'Hemant (22 Dec 2022) -- Start
    'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
    Private mintTPINId As Integer = 0
    Private mstrTPINName As String = ""
    'Hemant (22 Dec 2022) -- End  
#End Region

#Region " Properties "

    Public WriteOnly Property _EmpId() As Integer
        Set(ByVal value As Integer)
            mintEmpId = value
        End Set
    End Property

    Public WriteOnly Property _EmpName() As String
        Set(ByVal value As String)
            mstrEmpName = value
        End Set
    End Property

    Public WriteOnly Property _PayInMonthHeadId() As Integer
        Set(ByVal value As Integer)
            mintPayInMonthHeadId = value
        End Set
    End Property

    Public WriteOnly Property _PayInMonthHeadName() As String
        Set(ByVal value As String)
            mstrPayInMonthHeadName = value
        End Set
    End Property

    Public WriteOnly Property _FreePayHeadId() As Integer
        Set(ByVal value As Integer)
            mintFreePayHeadId = value
        End Set
    End Property

    Public WriteOnly Property _FreePayHeadName() As String
        Set(ByVal value As String)
            mstrFreePayHeadName = value
        End Set
    End Property

    Public WriteOnly Property _TaxDueHeadId() As Integer
        Set(ByVal value As Integer)
            mintTaxDueHeadId = value
        End Set
    End Property

    Public WriteOnly Property _TaxDueHeadName() As String
        Set(ByVal value As String)
            mstrTaxDueHeadName = value
        End Set
    End Property

    Public WriteOnly Property _PAYEHeadId() As Integer
        Set(ByVal value As Integer)
            mintPAYEHeadId = value
        End Set
    End Property

    Public WriteOnly Property _PAYEHeadName() As String
        Set(ByVal value As String)
            mstrPAYEHeadName = value
        End Set
    End Property

    Public WriteOnly Property _TaxRefundHeadId() As Integer
        Set(ByVal value As Integer)
            mintTaxRefundHeadId = value
        End Set
    End Property

    Public WriteOnly Property _TaxRefundHeadName() As String
        Set(ByVal value As String)
            mstrTaxRefundHeadName = value
        End Set
    End Property

    'Hemant (22 Dec 2022) -- Start
    'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
    Public WriteOnly Property _TPINId() As Integer
        Set(ByVal value As Integer)
            mintTPINId = value
        End Set
    End Property

    Public WriteOnly Property _TPINName() As String
        Set(ByVal value As String)
            mstrTPINName = value
        End Set
    End Property
    'Hemant (22 Dec 2022) -- End

    Public WriteOnly Property _STC_FYYear() As List(Of STC_FYYear)
        Set(ByVal value As List(Of STC_FYYear))
            mSTCFYYear = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _PeriodNames() As String
        Set(ByVal value As String)
            mstrPeriodNames = value
        End Set
    End Property

    Public WriteOnly Property _IncludeInactiveEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeInactiveEmp = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _FirstPeriodYearName() As String
        Set(ByVal value As String)
            mstrFirstPeriodYearName = value
        End Set
    End Property


    Public WriteOnly Property _ApplyUserAccessFilter() As Boolean
        Set(ByVal value As Boolean)
            mblnApplyUserAccessFilter = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property


#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try

            mintEmpId = 0
            mstrEmpName = ""
            mstrPeriodIds = ""
            mintPayInMonthHeadId = 0
            mstrPayInMonthHeadName = ""
            mintFreePayHeadId = 0
            mstrFreePayHeadName = ""
            mintTaxDueHeadId = 0
            mstrTaxDueHeadName = ""
            mintPAYEHeadId = 0
            mstrPAYEHeadName = ""
            mintTaxRefundHeadId = 0
            mstrTaxRefundHeadName = ""
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
            mintTPINId = 0
            mstrTPINName = ""
            'Hemant (22 Dec 2022) -- End	

            mSTCFYYear.Clear()

            mblnIncludeInactiveEmp = True

            mstrFirstPeriodYearName = ""


            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""

        'Try

        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If
        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Dim objCompany As New clsCompany_Master
        Dim objConfig As New clsConfigOptions
        Try
            objCompany._Companyunkid = xCompanyUnkid
            objConfig._Companyunkid = xCompanyUnkid
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId, objCompany._Name, objCompany._Tinno, objConfig._CurrencyFormat)

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try

            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub


    Private Function Generate_DetailReport(ByVal strDatabaseName As String _
                                           , ByVal intUserUnkid As Integer _
                                           , ByVal intYearUnkid As Integer _
                                           , ByVal intCompanyUnkid As Integer _
                                           , ByVal dtPeriodEnd As Date _
                                           , ByVal strUserModeSetting As String _
                                           , ByVal blnOnlyApproved As Boolean _
                                           , ByVal intBaseCurrencyId As Integer _
                                           , ByVal strCompanyName As String _
                                           , ByVal strCompanyTinNo As String _
                                           , ByVal strFmtCurrency As String _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim strBaseCurrencySign As String
        Dim decTotalPaye As Decimal = 0

        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId

            decDecimalPlaces = objExchangeRate._Digits_After_Decimal

            strBaseCurrencySign = objExchangeRate._Currency_Sign

            Dim xUACQry, xUACFiltrQry As String


            For Each key In mSTCFYYear

                xUACQry = "" : xUACFiltrQry = ""

                If mblnApplyUserAccessFilter = True Then
                    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, key.EndDate, blnOnlyApproved, key.DatabaseName, intUserUnkid, intCompanyUnkid, key.YearUnkid, strUserModeSetting)
                End If

                StrQ &= "SELECT hremployee_master.employeeunkid " & _
                                ", Transfer.DeptName " & _
                                ", Recate.JobName " & _
                        "INTO #" & key.DatabaseName & " " & _
                        "FROM hremployee_master "

                StrQ &= "LEFT JOIN " & _
                            "( " & _
                                "SELECT hremployee_transfer_tran.employeeunkid " & _
                                     ", hremployee_transfer_tran.departmentunkid AS allocationtranunkid " & _
                                     ", ISNULL(hrdepartment_master.code, '') AS DeptCode " & _
                                     ", ISNULL(hrdepartment_master.name, '') AS DeptName " & _
                                     ", ROW_NUMBER() OVER (PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS ROWNO " & _
                                "FROM hremployee_transfer_tran " & _
                                    "JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_transfer_tran.employeeunkid " & _
                                    "LEFT JOIN hrdepartment_master ON hremployee_transfer_tran.departmentunkid = hrdepartment_master.departmentunkid " & _
                                "WHERE hremployee_transfer_tran.isvoid = 0 " & _
                                      "AND CONVERT(CHAR(8), hremployee_transfer_tran.effectivedate, 112) <= '" & eZeeDate.convertDate(key.EndDate) & "' " & _
                            ") AS Transfer ON Transfer.employeeunkid = hremployee_master.employeeunkid "

                StrQ &= "LEFT JOIN " & _
                            "( " & _
                                "SELECT hremployee_categorization_tran.employeeunkid " & _
                                     ", hremployee_categorization_tran.jobunkid AS allocationtranunkid " & _
                                     ", ISNULL(hrjob_master.job_code, '') AS JobCode " & _
                                     ", ISNULL(hrjob_master.job_name, '') AS JobName " & _
                                     ", ROW_NUMBER() OVER (PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS ROWNO " & _
                                "FROM hremployee_categorization_tran " & _
                                    "JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_categorization_tran.employeeunkid " & _
                                    "LEFT JOIN hrjob_master ON hremployee_categorization_tran.jobunkid = hrjob_master.jobunkid " & _
                                "WHERE hremployee_categorization_tran.isvoid = 0 " & _
                                      "AND CONVERT(CHAR(8), hremployee_categorization_tran.effectivedate, 112) <= '" & eZeeDate.convertDate(key.EndDate) & "' " & _
                            ") AS Recate ON Recate.employeeunkid = hremployee_master.employeeunkid "

                StrQ &= mstrAnalysis_Join

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= " WHERE 1 = 1 " & _
                        " AND Transfer.ROWNO = 1 " & _
                        " AND Recate.ROWNO = 1 "

                If mintEmpId > 0 Then
                    StrQ &= "                  AND hremployee_master.employeeunkid = @employeeunkid "
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If

                StrQ &= " "
            Next

            StrQ &= "SELECT  A.periodunkid " & _
                          ", period_name " & _
                          ", period_enddate " & _
                          ", A.employeeunkid " & _
                          ", employeecode " & _
                          ", firstname " & _
                          ", othername " & _
                          ", surname " & _
                          ", employeename " & _
                          ", DeptName " & _
                          ", JobName " & _
                          ", PayInMonth " & _
                          ", FreePay " & _
                          ", TaxDue " & _
                          ", PAYE " & _
                          ", TaxRefund " & _
                          ", identity_no " & _
                          ", TPIN "
            'Hemant (22 Dec 2022) -- [TPIN]



            StrQ &= "FROM    ( "

            Dim i As Integer = -1
            Dim strCurrDatabaseName As String = ""
            Dim StrInnerQ As String = ""
            For Each key In mSTCFYYear
                strCurrDatabaseName = key.DatabaseName
                i += 1

                If i > 0 Then
                    StrInnerQ &= " UNION ALL "
                End If

                StrInnerQ &= " " & _
                             "SELECT    ISNULL(Period.payperiodunkid, 0) AS periodunkid " & _
                                      ", cfcommon_period_tran.period_name " & _
                                      ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS period_enddate " & _
                                      ", ISNULL(Period.employeeunkid, 0) AS employeeunkid " & _
                                      ", hremployee_master.employeecode " & _
                                      ", ISNULL(hremployee_master.firstname, '') AS firstname " & _
                                      ", ISNULL(hremployee_master.othername, '') AS othername " & _
                                      ", ISNULL(hremployee_master.surname, '') AS surname " & _
                                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                                      ", Period.DeptName " & _
                                      ", Period.JobName " & _
                                      ", SUM(ISNULL(Period.PayInMonth, 0)) AS PayInMonth " & _
                                      ", SUM(ISNULL(Period.FreePay, 0)) AS FreePay " & _
                                      ", SUM(ISNULL(Period.TaxDue, 0)) AS TaxDue " & _
                                      ", SUM(ISNULL(Period.PAYE, 0)) AS PAYE " & _
                                      ", SUM(ISNULL(Period.TaxRefund, 0)) AS TaxRefund " & _
                                      ", ISNULL(C.identity_no, '') AS identity_no " & _
                                      ", ISNULL(D.TPIN, '') AS TPIN "
                'Hemant (22 Dec 2022) -- [TPIN]


                StrInnerQ &= "FROM      ( SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS PayInMonth " & _
                                                  ", 0 AS FreePay " & _
                                                  ", 0 AS TaxDue " & _
                                                  ", 0 AS PAYE " & _
                                                  ", 0 AS TaxRefund " & _
                                                  ", #" & strCurrDatabaseName & ".DeptName " & _
                                                  ", #" & strCurrDatabaseName & ".JobName "


                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                   "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                   "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                   "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @payinmonthheadid "

                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS PayInMonth " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS FreePay " & _
                                                  ", 0 AS TaxDue " & _
                                                  ", 0 AS PAYE " & _
                                                  ", 0 AS TaxRefund " & _
                                                  ", #" & strCurrDatabaseName & ".DeptName " & _
                                                  ", #" & strCurrDatabaseName & ".JobName "


                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @freepayheadid "

                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS PayInMonth " & _
                                                  ", 0 AS FreePay " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS TaxDue " & _
                                                  ", 0 AS PAYE " & _
                                                  ", 0 AS TaxRefund " & _
                                                  ", #" & strCurrDatabaseName & ".DeptName " & _
                                                  ", #" & strCurrDatabaseName & ".JobName "


                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @taxdueheadid "

                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS PayInMonth " & _
                                                  ", 0 AS FreePay " & _
                                                  ", 0 AS TaxDue " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS PAYE " & _
                                                  ", 0 AS TaxRefund " & _
                                                  ", #" & strCurrDatabaseName & ".DeptName " & _
                                                  ", #" & strCurrDatabaseName & ".JobName "


                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @payeheadid "

                StrInnerQ &= "                      UNION ALL " & _
                                          "SELECT    payperiodunkid " & _
                                                  ", prpayrollprocess_tran.employeeunkid " & _
                                                  ", 0 AS PayInMonth " & _
                                                  ", 0 AS FreePay " & _
                                                  ", 0 AS TaxDue " & _
                                                  ", 0 AS PAYE " & _
                                                  ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS TaxRefund " & _
                                                  ", #" & strCurrDatabaseName & ".DeptName " & _
                                                  ", #" & strCurrDatabaseName & ".JobName "


                StrInnerQ &= "FROM      #" & strCurrDatabaseName & " " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prpayrollprocess_tran ON #" & strCurrDatabaseName & ".employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                        "LEFT JOIN " & strCurrDatabaseName & "..prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid " & _
                                                                                  "AND prpayrollprocess_tran.tranheadunkid > 0 "

                StrInnerQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "AND prtranhead_master.tranheadunkid = @taxrefundheadid "



                StrInnerQ &= "                    ) AS Period " & _
                                    "LEFT JOIN " & strCurrDatabaseName & "..hremployee_master ON Period.employeeunkid = hremployee_master.employeeunkid " & _
                                    "LEFT JOIN " & strCurrDatabaseName & "..cfcommon_period_tran ON Period.Payperiodunkid = cfcommon_period_tran.periodunkid "

                StrInnerQ &= "       LEFT JOIN ( SELECT  DISTINCT #" & strCurrDatabaseName & ".employeeunkid AS EmpId " & _
                                                ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS identity_no " & _
                                                  "FROM    #" & strCurrDatabaseName & " " & _
                                                          "LEFT JOIN " & strCurrDatabaseName & "..hremployee_idinfo_tran ON #" & strCurrDatabaseName & ".employeeunkid = hremployee_idinfo_tran.employeeunkid " & _
                                                          "/*JOIN " & strCurrDatabaseName & "..hremployee_master ON hremployee_idinfo_tran.employeeunkid = hremployee_master.employeeunkid*/ " & _
                                                          "LEFT JOIN " & strCurrDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = hremployee_idinfo_tran.idtypeunkid "


                StrInnerQ &= "                   WHERE hremployee_idinfo_tran.isdefault = 1 "


                StrInnerQ &= "           ) AS C ON C.EmpId = hremployee_master.employeeunkid "
                'Hemant (22 Dec 2022) -- Start
                'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
                StrInnerQ &= "       LEFT JOIN ( SELECT  DISTINCT #" & strCurrDatabaseName & ".employeeunkid AS EmpId " & _
                                               ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS TPIN " & _
                                                 "FROM    #" & strCurrDatabaseName & " " & _
                                                         "LEFT JOIN " & strCurrDatabaseName & "..hremployee_idinfo_tran ON #" & strCurrDatabaseName & ".employeeunkid = hremployee_idinfo_tran.employeeunkid "

                StrInnerQ &= "                   WHERE hremployee_idinfo_tran.idtypeunkid = @tpinid "


                StrInnerQ &= "           ) AS D ON D.EmpId = hremployee_master.employeeunkid "
                'Hemant (22 Dec 2022) -- End
                StrInnerQ &= "GROUP BY  Period.payperiodunkid " & _
                                            ", cfcommon_period_tran.period_name " & _
                                          ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) " & _
                                          ", Period.employeeunkid " & _
                                          ", hremployee_master.employeecode " & _
                                          ", ISNULL(hremployee_master.firstname, '') " & _
                                          ", ISNULL(hremployee_master.othername, '') " & _
                                          ", ISNULL(hremployee_master.surname, '') " & _
                                          ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
                                          ", Period.DeptName " & _
                                          ", Period.JobName " & _
                                          ", ISNULL(C.identity_no, '') " & _
                                          ", ISNULL(D.TPIN, '') "
                'Hemant (22 Dec 2022) -- [D.TPIN]


            Next

            Call FilterTitleAndFilterQuery()

            StrQ &= StrInnerQ
            StrQ &= ") AS A "

            StrQ &= " ORDER BY employeename, period_enddate "


            For Each key In mSTCFYYear
                StrQ &= " DROP TABLE #" & key.DatabaseName & " "
            Next

            If mintEmpId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpId)
            End If

            objDataOperation.AddParameter("@payinmonthheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayInMonthHeadId)
            objDataOperation.AddParameter("@freepayheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFreePayHeadId)
            objDataOperation.AddParameter("@taxdueheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTaxDueHeadId)
            objDataOperation.AddParameter("@payeheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPAYEHeadId)
            objDataOperation.AddParameter("@taxrefundheadid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTaxRefundHeadId)
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
            objDataOperation.AddParameter("@tpinid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTPINId)
            'Hemant (22 Dec 2022) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing
            Dim intPrevEmpId As Integer = 0
            Dim intCurrEmpId As Integer = 0
            Dim decTotPayInMonth As Decimal = 0
            Dim decTotFreePay As Decimal = 0
            Dim decTotTaxDue As Decimal = 0
            Dim decTotPaye As Decimal = 0
            Dim decTotTaxRefund As Decimal = 0

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                intCurrEmpId = CInt(dtRow.Item("employeeunkid"))

                If intPrevEmpId <> intCurrEmpId Then
                    decTotPayInMonth = 0
                    decTotFreePay = 0
                    decTotTaxDue = 0
                    decTotPaye = 0
                    decTotTaxRefund = 0
                End If

                rpt_Row = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Row.Item("Column1") = Month(eZeeDate.convertDate(dtRow.Item("period_enddate").ToString))
                rpt_Row.Item("Column2") = Format(CDec(dtRow.Item("PayInMonth")), strFmtCurrency)

                rpt_Row.Item("Column3") = Format(CDec(dtRow.Item("FreePay")), strFmtCurrency)
                rpt_Row.Item("Column4") = Format(CDec(dtRow.Item("TaxDue")), strFmtCurrency)
                rpt_Row.Item("Column5") = Format(CDec(dtRow.Item("Paye")), strFmtCurrency)
                rpt_Row.Item("Column6") = Format(CDec(dtRow.Item("TaxRefund")), strFmtCurrency)

                rpt_Row.Item("Column12") = dtRow.Item("DeptName").ToString
                rpt_Row.Item("Column13") = dtRow.Item("JobName").ToString
                rpt_Row.Item("Column14") = dtRow.Item("period_name").ToString
                rpt_Row.Item("Column15") = dtRow.Item("employeeunkid").ToString
                rpt_Row.Item("Column16") = dtRow.Item("employeecode").ToString
                rpt_Row.Item("Column17") = dtRow.Item("employeename").ToString
                rpt_Row.Item("Column18") = dtRow.Item("firstname").ToString & " " & dtRow.Item("othername").ToString
                rpt_Row.Item("Column19") = dtRow.Item("surname").ToString
                rpt_Row.Item("Column20") = dtRow.Item("identity_no").ToString


                decTotPayInMonth += CDec(rpt_Row.Item("Column2"))
                decTotFreePay += CDec(rpt_Row.Item("Column3"))
                decTotTaxDue += CDec(rpt_Row.Item("Column4"))
                decTotPaye += CDec(rpt_Row.Item("Column5"))
                decTotTaxRefund += CDec(rpt_Row.Item("Column6"))

                rpt_Row.Item("Column21") = Format(decTotPayInMonth, strFmtCurrency)
                rpt_Row.Item("Column22") = Format(decTotFreePay, strFmtCurrency)
                rpt_Row.Item("Column23") = Format(decTotTaxDue, strFmtCurrency)
                rpt_Row.Item("Column24") = Format(decTotPaye, strFmtCurrency)
                rpt_Row.Item("Column25") = Format(decTotTaxRefund, strFmtCurrency)
                'Hemant (22 Dec 2022) -- Start
                'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
                rpt_Row.Item("Column26") = dtRow.Item("TPIN").ToString
                'Hemant (22 Dec 2022) -- End


                intPrevEmpId = intCurrEmpId

                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Row)
            Next

            objRpt = New ArutiReport.Designer.rptPAYEYearEnd

            objRpt.SetDataSource(rpt_Data)

            ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 1, "RECORD OF P.A.Y.E. AND INCOME TAX DEDUCTIONS"))
            ReportFunction.TextChange(objRpt, "lblCaption2", Language.getMessage(mstrModuleName, 2, "THIS CARD IS USED FOR MONTHLY PAID EMPLOYEES AND FOR THOSE PAID AT INTERVALS OF MORE  THAN A MONTH."))
            ReportFunction.TextChange(objRpt, "lblNRCNo", Language.getMessage(mstrModuleName, 3, "NRC NO:"))
            ReportFunction.TextChange(objRpt, "lblWorkNo", Language.getMessage(mstrModuleName, 4, "WORK NO."))
            ReportFunction.TextChange(objRpt, "lblEmployeeSurname", Language.getMessage(mstrModuleName, 5, "EMPLOYEE'S SURNAME:"))
            ReportFunction.TextChange(objRpt, "lblEmployeeForename", Language.getMessage(mstrModuleName, 6, "EMPLOYEE'S FORENAMES:"))
            ReportFunction.TextChange(objRpt, "lblDepartment", Language.getMessage(mstrModuleName, 7, "DEPARTMENT:"))
            ReportFunction.TextChange(objRpt, "lblJob", Language.getMessage(mstrModuleName, 8, "NATURE OF WORK:"))
            ReportFunction.TextChange(objRpt, "lblEmplrPIN", Language.getMessage(mstrModuleName, 9, "EMPLOYER'S PAYE ACCOUNT:"))
            ReportFunction.TextChange(objRpt, "txtEmplrPIN", strCompanyTinNo)
            ReportFunction.TextChange(objRpt, "lblEmployerName", Language.getMessage(mstrModuleName, 10, "EMPLOYER:"))
            ReportFunction.TextChange(objRpt, "txtEmployerName", strCompanyName)


            ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 11, "MONTH NO."))
            ReportFunction.TextChange(objRpt, "lblPayInMonth", Language.getMessage(mstrModuleName, 12, "PAY IN THE MONTH"))
            ReportFunction.TextChange(objRpt, "lblPayToDate", Language.getMessage(mstrModuleName, 13, "TOTAL PAY TO DATE"))
            ReportFunction.TextChange(objRpt, "lblFreePay", Language.getMessage(mstrModuleName, 14, "TOTAL FREE PAY TO DATE"))
            ReportFunction.TextChange(objRpt, "lblTaxDue", Language.getMessage(mstrModuleName, 15, "TOTAL TAX DUE TO DATE"))
            ReportFunction.TextChange(objRpt, "lblPAYE", Language.getMessage(mstrModuleName, 16, "TAX DEDUCTED IN THE MONTH"))
            ReportFunction.TextChange(objRpt, "lblTaxRefund", Language.getMessage(mstrModuleName, 17, "TAX REFUNDED IN THE MONTH"))
			
            ReportFunction.TextChange(objRpt, "lblEngaged", Language.getMessage(mstrModuleName, 18, "If engaged during the Year Deduct Pay in"))
            ReportFunction.TextChange(objRpt, "lblDeductTax", Language.getMessage(mstrModuleName, 19, "Deduct Tax in Respect of Previous"))
            ReportFunction.TextChange(objRpt, "lblPreviousEmployment", Language.getMessage(mstrModuleName, 20, "Previous Employments"))
            ReportFunction.TextChange(objRpt, "txtPreviousEmployment", Language.getMessage(mstrModuleName, 21, "0.00"))
            ReportFunction.TextChange(objRpt, "lblEmployment", Language.getMessage(mstrModuleName, 22, "Employment(s)"))
            ReportFunction.TextChange(objRpt, "txtEmployment", Language.getMessage(mstrModuleName, 23, "0.00"))
            ReportFunction.TextChange(objRpt, "lblPayThis", Language.getMessage(mstrModuleName, 24, "Pay in this Employment"))
            ReportFunction.TextChange(objRpt, "lblTaxDeductedThis", Language.getMessage(mstrModuleName, 25, "Tax Deducted or (Refunded) in this Employment"))
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-442 - PAYE Year End Report enhancement to include TPIN
            ReportFunction.TextChange(objRpt, "lblTPIN", Language.getMessage(mstrModuleName, 26, "TPIN:"))
            'Hemant (22 Dec 2022) -- End
            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

#End Region


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "RECORD OF P.A.Y.E. AND INCOME TAX DEDUCTIONS")
			Language.setMessage(mstrModuleName, 2, "THIS CARD IS USED FOR MONTHLY PAID EMPLOYEES AND FOR THOSE PAID AT INTERVALS OF MORE  THAN A MONTH.")
			Language.setMessage(mstrModuleName, 3, "NRC NO:")
			Language.setMessage(mstrModuleName, 4, "WORK NO.")
			Language.setMessage(mstrModuleName, 5, "EMPLOYEE'S SURNAME:")
			Language.setMessage(mstrModuleName, 6, "EMPLOYEE'S FORENAMES:")
			Language.setMessage(mstrModuleName, 7, "DEPARTMENT:")
			Language.setMessage(mstrModuleName, 8, "NATURE OF WORK:")
			Language.setMessage(mstrModuleName, 9, "EMPLOYER'S PAYE ACCOUNT:")
			Language.setMessage(mstrModuleName, 10, "EMPLOYER:")
			Language.setMessage(mstrModuleName, 11, "MONTH NO.")
			Language.setMessage(mstrModuleName, 12, "PAY IN THE MONTH")
			Language.setMessage(mstrModuleName, 13, "TOTAL PAY TO DATE")
			Language.setMessage(mstrModuleName, 14, "TOTAL FREE PAY TO DATE")
			Language.setMessage(mstrModuleName, 15, "TOTAL TAX DUE TO DATE")
			Language.setMessage(mstrModuleName, 16, "TAX DEDUCTED IN THE MONTH")
			Language.setMessage(mstrModuleName, 17, "TAX REFUNDED IN THE MONTH")
			Language.setMessage(mstrModuleName, 18, "If engaged during the Year Deduct Pay in")
			Language.setMessage(mstrModuleName, 19, "Deduct Tax in Respect of Previous")
			Language.setMessage(mstrModuleName, 20, "Previous Employments")
			Language.setMessage(mstrModuleName, 21, "0.00")
			Language.setMessage(mstrModuleName, 22, "Employment(s)")
			Language.setMessage(mstrModuleName, 23, "0.00")
			Language.setMessage(mstrModuleName, 24, "Pay in this Employment")
			Language.setMessage(mstrModuleName, 25, "Tax Deducted or (Refunded) in this Employment")
            Language.setMessage(mstrModuleName, 26, "TPIN:")


		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
