﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPAYEYearEndReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPAYEYearEndReport))
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboTaxRefund = New System.Windows.Forms.ComboBox
        Me.lblTaxRefund = New System.Windows.Forms.Label
        Me.cboPAYE = New System.Windows.Forms.ComboBox
        Me.lblPAYE = New System.Windows.Forms.Label
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.cboTaxDue = New System.Windows.Forms.ComboBox
        Me.lblTaxDue = New System.Windows.Forms.Label
        Me.btnSaveSelection = New eZee.Common.eZeeLightButton(Me.components)
        Me.cboFreePay = New System.Windows.Forms.ComboBox
        Me.lblFreePay = New System.Windows.Forms.Label
        Me.cboToPeriod = New System.Windows.Forms.ComboBox
        Me.lblToPeriod = New System.Windows.Forms.Label
        Me.cboFromPeriod = New System.Windows.Forms.ComboBox
        Me.lblFromPeriod = New System.Windows.Forms.Label
        Me.lblPayInMonth = New System.Windows.Forms.Label
        Me.cboPayInMonth = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.cboTPIN = New System.Windows.Forms.ComboBox
        Me.lblTPIN = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 434)
        Me.NavPanel.Size = New System.Drawing.Size(711, 55)
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboTPIN)
        Me.gbFilterCriteria.Controls.Add(Me.lblTPIN)
        Me.gbFilterCriteria.Controls.Add(Me.cboTaxRefund)
        Me.gbFilterCriteria.Controls.Add(Me.lblTaxRefund)
        Me.gbFilterCriteria.Controls.Add(Me.cboPAYE)
        Me.gbFilterCriteria.Controls.Add(Me.lblPAYE)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.cboTaxDue)
        Me.gbFilterCriteria.Controls.Add(Me.lblTaxDue)
        Me.gbFilterCriteria.Controls.Add(Me.btnSaveSelection)
        Me.gbFilterCriteria.Controls.Add(Me.cboFreePay)
        Me.gbFilterCriteria.Controls.Add(Me.lblFreePay)
        Me.gbFilterCriteria.Controls.Add(Me.cboToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblToPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboFromPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblFromPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayInMonth)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayInMonth)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(643, 245)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTaxRefund
        '
        Me.cboTaxRefund.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTaxRefund.FormattingEnabled = True
        Me.cboTaxRefund.Location = New System.Drawing.Point(131, 114)
        Me.cboTaxRefund.Name = "cboTaxRefund"
        Me.cboTaxRefund.Size = New System.Drawing.Size(167, 21)
        Me.cboTaxRefund.TabIndex = 6
        '
        'lblTaxRefund
        '
        Me.lblTaxRefund.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxRefund.Location = New System.Drawing.Point(8, 116)
        Me.lblTaxRefund.Name = "lblTaxRefund"
        Me.lblTaxRefund.Size = New System.Drawing.Size(117, 15)
        Me.lblTaxRefund.TabIndex = 78
        Me.lblTaxRefund.Text = "Tax Refund"
        Me.lblTaxRefund.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPAYE
        '
        Me.cboPAYE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPAYE.FormattingEnabled = True
        Me.cboPAYE.Location = New System.Drawing.Point(447, 87)
        Me.cboPAYE.Name = "cboPAYE"
        Me.cboPAYE.Size = New System.Drawing.Size(167, 21)
        Me.cboPAYE.TabIndex = 5
        '
        'lblPAYE
        '
        Me.lblPAYE.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPAYE.Location = New System.Drawing.Point(319, 89)
        Me.lblPAYE.Name = "lblPAYE"
        Me.lblPAYE.Size = New System.Drawing.Size(122, 15)
        Me.lblPAYE.TabIndex = 75
        Me.lblPAYE.Text = "PAYE"
        Me.lblPAYE.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(535, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 72
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lnkSetAnalysis.Visible = False
        '
        'cboTaxDue
        '
        Me.cboTaxDue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTaxDue.FormattingEnabled = True
        Me.cboTaxDue.Location = New System.Drawing.Point(131, 87)
        Me.cboTaxDue.Name = "cboTaxDue"
        Me.cboTaxDue.Size = New System.Drawing.Size(167, 21)
        Me.cboTaxDue.TabIndex = 4
        '
        'lblTaxDue
        '
        Me.lblTaxDue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxDue.Location = New System.Drawing.Point(8, 89)
        Me.lblTaxDue.Name = "lblTaxDue"
        Me.lblTaxDue.Size = New System.Drawing.Size(117, 15)
        Me.lblTaxDue.TabIndex = 10
        Me.lblTaxDue.Text = "Tax Due"
        Me.lblTaxDue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSaveSelection
        '
        Me.btnSaveSelection.BackColor = System.Drawing.Color.White
        Me.btnSaveSelection.BackgroundImage = CType(resources.GetObject("btnSaveSelection.BackgroundImage"), System.Drawing.Image)
        Me.btnSaveSelection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSaveSelection.BorderColor = System.Drawing.Color.Empty
        Me.btnSaveSelection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSaveSelection.FlatAppearance.BorderSize = 0
        Me.btnSaveSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSaveSelection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveSelection.ForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSaveSelection.GradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Location = New System.Drawing.Point(131, 186)
        Me.btnSaveSelection.Name = "btnSaveSelection"
        Me.btnSaveSelection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSaveSelection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSaveSelection.Size = New System.Drawing.Size(111, 30)
        Me.btnSaveSelection.TabIndex = 8
        Me.btnSaveSelection.Text = "&Save Selection"
        Me.btnSaveSelection.UseVisualStyleBackColor = True
        '
        'cboFreePay
        '
        Me.cboFreePay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFreePay.FormattingEnabled = True
        Me.cboFreePay.Location = New System.Drawing.Point(447, 60)
        Me.cboFreePay.Name = "cboFreePay"
        Me.cboFreePay.Size = New System.Drawing.Size(167, 21)
        Me.cboFreePay.TabIndex = 3
        '
        'lblFreePay
        '
        Me.lblFreePay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFreePay.Location = New System.Drawing.Point(319, 62)
        Me.lblFreePay.Name = "lblFreePay"
        Me.lblFreePay.Size = New System.Drawing.Size(122, 15)
        Me.lblFreePay.TabIndex = 7
        Me.lblFreePay.Text = "Free Pay"
        Me.lblFreePay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboToPeriod
        '
        Me.cboToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboToPeriod.FormattingEnabled = True
        Me.cboToPeriod.Location = New System.Drawing.Point(447, 33)
        Me.cboToPeriod.Name = "cboToPeriod"
        Me.cboToPeriod.Size = New System.Drawing.Size(167, 21)
        Me.cboToPeriod.TabIndex = 1
        '
        'lblToPeriod
        '
        Me.lblToPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToPeriod.Location = New System.Drawing.Point(319, 36)
        Me.lblToPeriod.Name = "lblToPeriod"
        Me.lblToPeriod.Size = New System.Drawing.Size(122, 15)
        Me.lblToPeriod.TabIndex = 2
        Me.lblToPeriod.Text = "To Period"
        Me.lblToPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboFromPeriod
        '
        Me.cboFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromPeriod.FormattingEnabled = True
        Me.cboFromPeriod.Location = New System.Drawing.Point(131, 33)
        Me.cboFromPeriod.Name = "cboFromPeriod"
        Me.cboFromPeriod.Size = New System.Drawing.Size(167, 21)
        Me.cboFromPeriod.TabIndex = 0
        '
        'lblFromPeriod
        '
        Me.lblFromPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromPeriod.Location = New System.Drawing.Point(8, 36)
        Me.lblFromPeriod.Name = "lblFromPeriod"
        Me.lblFromPeriod.Size = New System.Drawing.Size(117, 15)
        Me.lblFromPeriod.TabIndex = 0
        Me.lblFromPeriod.Text = "From Period"
        Me.lblFromPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPayInMonth
        '
        Me.lblPayInMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayInMonth.Location = New System.Drawing.Point(8, 62)
        Me.lblPayInMonth.Name = "lblPayInMonth"
        Me.lblPayInMonth.Size = New System.Drawing.Size(117, 15)
        Me.lblPayInMonth.TabIndex = 4
        Me.lblPayInMonth.Text = "Pay in the Month"
        Me.lblPayInMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayInMonth
        '
        Me.cboPayInMonth.DropDownWidth = 180
        Me.cboPayInMonth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayInMonth.FormattingEnabled = True
        Me.cboPayInMonth.Location = New System.Drawing.Point(131, 60)
        Me.cboPayInMonth.Name = "cboPayInMonth"
        Me.cboPayInMonth.Size = New System.Drawing.Size(167, 21)
        Me.cboPayInMonth.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(447, 114)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(167, 21)
        Me.cboEmployee.TabIndex = 7
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(319, 118)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(122, 15)
        Me.lblEmployee.TabIndex = 29
        Me.lblEmployee.Text = "Employee"
        Me.lblEmployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTPIN
        '
        Me.cboTPIN.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTPIN.FormattingEnabled = True
        Me.cboTPIN.Location = New System.Drawing.Point(131, 141)
        Me.cboTPIN.Name = "cboTPIN"
        Me.cboTPIN.Size = New System.Drawing.Size(167, 21)
        Me.cboTPIN.TabIndex = 249
        '
        'lblTPIN
        '
        Me.lblTPIN.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTPIN.Location = New System.Drawing.Point(8, 141)
        Me.lblTPIN.Name = "lblTPIN"
        Me.lblTPIN.Size = New System.Drawing.Size(104, 15)
        Me.lblTPIN.TabIndex = 248
        Me.lblTPIN.Text = "TPIN"
        Me.lblTPIN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmPAYEYearEndReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(711, 489)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmPAYEYearEndReport"
        Me.Text = "frmPAYEYearEndReport"
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
    Friend WithEvents btnSaveSelection As eZee.Common.eZeeLightButton
    Friend WithEvents cboTaxDue As System.Windows.Forms.ComboBox
    Friend WithEvents lblTaxDue As System.Windows.Forms.Label
    Friend WithEvents cboFreePay As System.Windows.Forms.ComboBox
    Friend WithEvents lblFreePay As System.Windows.Forms.Label
    Friend WithEvents cboToPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblToPeriod As System.Windows.Forms.Label
    Friend WithEvents cboFromPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblFromPeriod As System.Windows.Forms.Label
    Friend WithEvents lblPayInMonth As System.Windows.Forms.Label
    Friend WithEvents cboPayInMonth As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents cboPAYE As System.Windows.Forms.ComboBox
    Friend WithEvents lblPAYE As System.Windows.Forms.Label
    Friend WithEvents cboTaxRefund As System.Windows.Forms.ComboBox
    Friend WithEvents lblTaxRefund As System.Windows.Forms.Label
    Friend WithEvents cboTPIN As System.Windows.Forms.ComboBox
    Friend WithEvents lblTPIN As System.Windows.Forms.Label
End Class
