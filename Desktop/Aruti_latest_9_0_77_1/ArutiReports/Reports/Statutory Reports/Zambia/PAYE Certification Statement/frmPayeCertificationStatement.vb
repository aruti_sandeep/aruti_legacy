﻿'************************************************************************************************************************************
'Class Name : frmPayeCertificationStatement.vb
'Purpose    : 
'Written By : Hemant
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

#End Region

Public Class frmPayeCertificationStatement

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmPayeCertificationStatement"
    Private objPayeCertificationStatement As clsPayeCertificationStatement
    Private mintFirstPeriodId As Integer = 0
    Private mintLastPeriodId As Integer = 0

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mdtPeriodEndDate As DateTime

#End Region

#Region " Contructor "

    Public Sub New()
        objPayeCertificationStatement = New clsPayeCertificationStatement(User._Object._Languageunkid, Company._Object._Companyunkid)
        objPayeCertificationStatement.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Enum "
    Private Enum enHeadTypeId
        GrossTaxable = 1
        FreePay = 2
        TotalTaxPaid = 3
        NAPSA = 4
        'Hemant (22 Dec 2022) -- Start
        'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
        TPIN = 5
        'Hemant (22 Dec 2022) -- End
    End Enum
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim objTranHead As New clsTransactionHead
        Dim objMaster As New clsMasterData
        'Hemant (22 Dec 2022) -- Start
        'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN		
        Dim objCMaster As New clsCommon_Master
        'Hemant (22 Dec 2022) -- End	
        Dim dsCombos As DataSet

        Try

            dsCombos = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                              User._Object._Userunkid, _
                                              FinancialYear._Object._YearUnkid, _
                                              Company._Object._Companyunkid, _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                              ConfigParameter._Object._UserAccessModeSetting, _
                                              True, True, "Emp", True)

            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            mintFirstPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)


            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, 0, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)

            Dim dsYear As DataSet = objMaster.Get_Database_Year_List("Year", True, Company._Object._Companyunkid)
            Dim dt As DataTable = New DataView(dsYear.Tables(0), "start_date >= '" & eZeeDate.convertDate(FinancialYear._Object._Database_Start_Date.AddYears(-1)) & "' AND end_date <= '" & eZeeDate.convertDate(FinancialYear._Object._Database_End_Date.AddYears(-1)) & "' ", "", DataViewRowState.CurrentRows).ToTable
            Dim strYear As String = String.Join(",", (From p In dt Select (p.Item("yearunkid").ToString)).ToArray)
            Dim mdtPeriod As DataTable = New DataView(dsCombos.Tables("Period"), "yearunkid IN (0, " & strYear & ") ", "", DataViewRowState.CurrentRows).ToTable
            If mdtPeriod.Rows.Count > 1 Then
                If CInt(mdtPeriod.Rows(1).Item("periodunkid")) > 0 Then mintFirstPeriodId = CInt(mdtPeriod.Rows(1).Item("periodunkid"))
                mintLastPeriodId = CInt(mdtPeriod.Rows(mdtPeriod.Rows.Count - 1).Item("periodunkid"))
            End If
            If mintLastPeriodId <= 0 Then mintLastPeriodId = mintFirstPeriodId

            With cboFromPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = mintFirstPeriodId
            End With

            With cboToPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = mintLastPeriodId
            End With


            dsCombos = objTranHead.getComboList(FinancialYear._Object._DatabaseName, "Heads", True, , , , , , "typeof_id <> " & enTypeOf.Salary & " ")
            With cboGrossTaxable
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            With cboFreePay
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboTotalTaxPaid
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            With cboNAPSA
                .ValueMember = "tranheadunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
            End With

            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")
            With cboTPIN
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With
            'Hemant (22 Dec 2022) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objPeriod = Nothing
            objTranHead = Nothing
            objMaster = Nothing
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN		
            objCMaster = Nothing
            'Hemant (22 Dec 2022) -- End            
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboGrossTaxable.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboFromPeriod.SelectedValue = mintFirstPeriodId
            cboToPeriod.SelectedValue = mintLastPeriodId
            cboFreePay.SelectedValue = 0
            cboTotalTaxPaid.SelectedValue = 0
            cboNAPSA.SelectedValue = 0
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
            cboTPIN.SelectedValue = 0
            'Hemant (22 Dec 2022) -- End

            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

            Call GetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.PAYE_Certification_Statement)

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.GrossTaxable
                            cboGrossTaxable.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.FreePay
                            cboFreePay.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.TotalTaxPaid
                            cboTotalTaxPaid.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.NAPSA
                            cboNAPSA.SelectedValue = CInt(dsRow.Item("transactionheadid"))

                            'Hemant (22 Dec 2022) -- Start
                            'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
                        Case enHeadTypeId.TPIN
                            cboTPIN.SelectedValue = CInt(dsRow.Item("transactionheadid"))
                            'Hemant (22 Dec 2022) -- End

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim intPrevYearID As Integer = 0
        Dim STC_FYYears As New List(Of clsPayeCertificationStatement.STC_FYYear)
        Try
            objPayeCertificationStatement.SetDefaultValue()


            If CInt(cboFromPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "From Period is mandatory information. Please select From Period."), enMsgBoxStyle.Information)
                cboFromPeriod.Focus()
                Return False
            ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "To Period is mandatory information. Please select To Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            ElseIf CInt(cboFromPeriod.SelectedIndex) > CInt(cboToPeriod.SelectedIndex) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period."), enMsgBoxStyle.Information)
                cboToPeriod.Focus()
                Return False
            ElseIf CInt(cboGrossTaxable.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select head for Gross Taxable."), enMsgBoxStyle.Information)
                cboGrossTaxable.Focus()
                Return False
            ElseIf CInt(cboFreePay.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Please select head for Free Pay."), enMsgBoxStyle.Information)
                cboFreePay.Focus()
                Return False
            ElseIf CInt(cboTotalTaxPaid.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Please select head for Total Tax Paid."), enMsgBoxStyle.Information)
                cboTotalTaxPaid.Focus()
                Return False
            ElseIf CInt(cboNAPSA.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please select head for NAPSA."), enMsgBoxStyle.Information)
                cboNAPSA.Focus()
                Return False
                'Hemant (22 Dec 2022) -- Start
                'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
            ElseIf CInt(cboTPIN.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Please select TPIN."), enMsgBoxStyle.Information)
                cboTPIN.Focus()
                Return False
                'Hemant (22 Dec 2022) -- End
            End If


            objPayeCertificationStatement._EmpId = cboEmployee.SelectedValue
            objPayeCertificationStatement._EmpName = cboEmployee.Text

            Dim i As Integer = 0

            Dim strPreriodIds As String = cboFromPeriod.SelectedValue.ToString
            Dim mstrPeriodsName As String = cboFromPeriod.Text
            dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid")))
            If dsList.Tables("Database").Rows.Count > 0 Then
                intPrevYearID = CInt(CType(cboFromPeriod.SelectedItem, DataRowView).Item("yearunkid"))

                If STC_FYYears.Exists(Function(x) x.YearUnkid = intPrevYearID) = False Then
                    STC_FYYears.Add(New clsPayeCertificationStatement.STC_FYYear With {.YearUnkid = intPrevYearID, .DatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString, .EndDate = eZeeDate.convertDate(dsList.Tables("Database").Rows(0).Item("end_date").ToString)})
                End If
            End If

            For Each dsRow As DataRow In CType(cboFromPeriod.DataSource, DataTable).Rows
                If i > cboFromPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                    strPreriodIds &= ", " & dsRow.Item("periodunkid").ToString
                    mstrPeriodsName &= ", " & dsRow.Item("name").ToString
                    If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                        dsList = objCompany.GetFinancialYearList(Company._Object._Companyunkid, , "Database", CInt(dsRow.Item("yearunkid")))
                        If dsList.Tables("Database").Rows.Count > 0 Then

                            If STC_FYYears.Exists(Function(x) x.YearUnkid = CInt(dsRow.Item("yearunkid"))) = False Then
                                STC_FYYears.Add(New clsPayeCertificationStatement.STC_FYYear With {.YearUnkid = CInt(dsRow.Item("yearunkid")), .DatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString, .EndDate = eZeeDate.convertDate(dsList.Tables("Database").Rows(0).Item("end_date").ToString)})
                            End If
                        End If
                    End If
                    intPrevYearID = CInt(dsRow.Item("yearunkid"))
                End If
                i += 1
            Next
            objPayeCertificationStatement._STC_FYYear = STC_FYYears
            objPayeCertificationStatement._PeriodIds = strPreriodIds
            objPayeCertificationStatement._PeriodNames = mstrPeriodsName
            objPayeCertificationStatement._ToPeriodId = CInt(cboToPeriod.SelectedValue)
            objPayeCertificationStatement._ToPeriodName = cboToPeriod.Text

            objPayeCertificationStatement._FirstPeriodYearName = eZeeDate.convertDate(CType(cboToPeriod.SelectedItem, DataRowView).Item("end_date").ToString).Year.ToString


            objPayeCertificationStatement._GrossTaxableHeadId = CInt(cboGrossTaxable.SelectedValue)
            objPayeCertificationStatement._GrossTaxableHeadName = cboGrossTaxable.Text

            objPayeCertificationStatement._FreePayHeadId = CInt(cboFreePay.SelectedValue)
            objPayeCertificationStatement._FreePayHeadName = cboFreePay.Text

            objPayeCertificationStatement._TotalTaxPaidHeadId = CInt(cboTotalTaxPaid.SelectedValue)
            objPayeCertificationStatement._TotalTaxPaidHeadName = cboTotalTaxPaid.Text

            objPayeCertificationStatement._NAPSAHeadId = CInt(cboNAPSA.SelectedValue)
            objPayeCertificationStatement._NAPSAHeadName = cboNAPSA.Text

            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
            objPayeCertificationStatement._TPINId = CInt(cboTPIN.SelectedValue)
            objPayeCertificationStatement._TPINName = cboTPIN.Text
            'Hemant (22 Dec 2022) -- End

            objPayeCertificationStatement._ViewByIds = mstrStringIds
            objPayeCertificationStatement._ViewIndex = mintViewIdx
            objPayeCertificationStatement._ViewByName = mstrStringName
            objPayeCertificationStatement._Analysis_Fields = mstrAnalysis_Fields
            objPayeCertificationStatement._Analysis_Join = mstrAnalysis_Join
            objPayeCertificationStatement._Analysis_OrderBy = mstrAnalysis_OrderBy
            objPayeCertificationStatement._Report_GroupName = mstrReport_GroupName



            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function


#End Region

#Region " Forms Events "

    Private Sub frmPayeCertificationStatement_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPayeCertificationStatement = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayeCertificationStatement_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayeCertificationStatement_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
                e.Handled = True
            ElseIf e.Control AndAlso e.KeyCode = Keys.R Then
                Call frmPayeCertificationStatement_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPayeCertificationStatement_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayeCertificationStatement_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objPayeCertificationStatement._ReportName
            Me._Message = objPayeCertificationStatement._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayeCertificationStatement_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayeCertificationStatement_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayeCertificationStatement.SetMessages()
            objfrm._Other_ModuleNames = "clsPayeCertificationStatement"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmPayeCertificationStatement_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Buttons Events "

    Private Sub frmPayeCertificationStatement_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)

            objPayeCertificationStatement._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objPayeCertificationStatement.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                          objPeriod._Start_Date, _
                                          objPeriod._End_Date, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.Preview, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPayeCertificationStatement_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmPayeCertificationStatement_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)

            objPayeCertificationStatement._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))

            objPayeCertificationStatement.generateReportNew(FinancialYear._Object._DatabaseName, _
                                           User._Object._Userunkid, _
                                           FinancialYear._Object._YearUnkid, _
                                           Company._Object._Companyunkid, _
                                           objPeriod._Start_Date, _
                                           objPeriod._End_Date, _
                                           ConfigParameter._Object._UserAccessModeSetting, _
                                           True, ConfigParameter._Object._ExportReportPath, _
                                           ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmPayeCertificationStatement_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSaveSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSelection.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            If SetFilter() = False Then Exit Try

            For intHeadType As Integer = 1 To GetEnumHighestValue(Of enHeadTypeId)()
                objUserDefRMode = New clsUserDef_ReportMode()

                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.PAYE_Certification_Statement
                objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reportmodeid = 0

                Select Case intHeadType

                    Case enHeadTypeId.GrossTaxable
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboGrossTaxable.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_Certification_Statement, 0, 0, intHeadType)

                    Case enHeadTypeId.FreePay
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboFreePay.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_Certification_Statement, 0, 0, intHeadType)

                    Case enHeadTypeId.TotalTaxPaid
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTotalTaxPaid.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_Certification_Statement, 0, 0, intHeadType)

                    Case enHeadTypeId.NAPSA
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboNAPSA.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_Certification_Statement, 0, 0, intHeadType)

                        'Hemant (22 Dec 2022) -- Start
                        'ENHANCEMENT(Zambia) : A1X-441 - PAYE Certification Statement to include employee TPIN
                    Case enHeadTypeId.TPIN
                        objUserDefRMode._Headtypeid = intHeadType
                        objUserDefRMode._EarningTranHeadIds = cboTPIN.SelectedValue.ToString

                        intUnkid = objUserDefRMode.isExist(enArutiReport.PAYE_Certification_Statement, 0, 0, intHeadType)
                        'Hemant (22 Dec 2022) -- End

                End Select

                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If

            Next

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Selection Saved Successfully"), enMsgBoxStyle.Information)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSaveSelection_Click", mstrModuleName)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    Private Sub frmPayeCertificationStatement_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayeCertificationStatement_Reset_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox' Events "

    Private Sub cboGrossTaxable_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboGrossTaxable.Validating, _
                                                                                                                             cboFreePay.Validating, _
                                                                                                                             cboTotalTaxPaid.Validating, _
                                                                                                                             cboNAPSA.Validating
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            If CInt(cbo.SelectedValue) > 0 Then
                Dim lst As IEnumerable(Of ComboBox) = gbFilterCriteria.Controls.OfType(Of ComboBox)().Where(Function(t) t.Name <> cbo.Name AndAlso t.Name <> cboEmployee.Name AndAlso t.Name <> cboFromPeriod.Name AndAlso t.Name <> cboToPeriod.Name AndAlso CInt(t.SelectedValue) = CInt(cbo.SelectedValue))
                If lst.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, This transaction head is already mapped."))
                    cbo.SelectedValue = 0
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Validating", mstrModuleName)
        End Try
    End Sub

    Private Sub cboGrossTaxable_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboGrossTaxable.KeyPress, cboFreePay.KeyPress, cboTotalTaxPaid.KeyPress _
                                                                                                                            , cboNAPSA.KeyPress, cboEmployee.KeyPress, cboFromPeriod.KeyPress _
                                                                                                                            , cboToPeriod.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)

                    If cbo.Name = cboEmployee.Name Then
                        .CodeMember = "employeecode"
                    Else
                        .CodeMember = "code"
                    End If

                End With

                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString

                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    cbo.Tag = frm.SelectedAlias
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                    cbo.Tag = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
            mstrAnalysis_OrderBy = frm._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub


#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

			Me.btnSaveSelection.GradientBackColor = GUI._ButttonBackColor
			Me.btnSaveSelection.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.btnSaveSelection.Text = Language._Object.getCaption(Me.btnSaveSelection.Name, Me.btnSaveSelection.Text)
			Me.lblTotalTaxPaid.Text = Language._Object.getCaption(Me.lblTotalTaxPaid.Name, Me.lblTotalTaxPaid.Text)
			Me.lblFreePay.Text = Language._Object.getCaption(Me.lblFreePay.Name, Me.lblFreePay.Text)
			Me.lblToPeriod.Text = Language._Object.getCaption(Me.lblToPeriod.Name, Me.lblToPeriod.Text)
			Me.lblFromPeriod.Text = Language._Object.getCaption(Me.lblFromPeriod.Name, Me.lblFromPeriod.Text)
			Me.lblGrossTaxable.Text = Language._Object.getCaption(Me.lblGrossTaxable.Name, Me.lblGrossTaxable.Text)
			Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
			Me.lblNAPSA.Text = Language._Object.getCaption(Me.lblNAPSA.Name, Me.lblNAPSA.Text)
            Me.lblTPIN.Text = Language._Object.getCaption(Me.lblTPIN.Name, Me.lblTPIN.Text)
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "From Period is mandatory information. Please select From Period.")
            Language.setMessage(mstrModuleName, 2, "To Period is mandatory information. Please select To Period.")
            Language.setMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period.")
            Language.setMessage(mstrModuleName, 4, "Please select head for Gross Taxable.")
            Language.setMessage(mstrModuleName, 5, "Please select head for Free Pay.")
            Language.setMessage(mstrModuleName, 6, "Please select head for Total Tax Paid.")
            Language.setMessage(mstrModuleName, 7, "Please select head for NAPSA.")
            Language.setMessage(mstrModuleName, 8, "Sorry, This transaction head is already mapped.")
            Language.setMessage(mstrModuleName, 9, "Selection Saved Successfully")
            Language.setMessage(mstrModuleName, 10, "Please select TPIN.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class