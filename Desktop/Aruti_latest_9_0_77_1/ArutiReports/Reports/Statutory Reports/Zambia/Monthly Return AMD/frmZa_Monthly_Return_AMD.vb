'************************************************************************************************************************************
'Class Name : frmZa_Monthly_Return_AMD.vb
'Purpose    : 
'Written By : Sandeep J. Sharma
'Modified   : 
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmZa_Monthly_Return_AMD

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmZa_Monthly_Return_AMD"
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrReport_GroupName As String = ""
    Private objMPR_AMD As clsZa_Monthly_Return_AMD

#End Region

#Region " Contructor "

    Public Sub New()
        objMPR_AMD = New clsZa_Monthly_Return_AMD(User._Object._Languageunkid,Company._Object._Companyunkid)
        objMPR_AMD.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMember As New clsmembership_master
        Dim objperiod As New clscommom_period_Tran
        Dim objEmpContribution As New clsTransactionHead
        Dim objCMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try
            dsCombos = objMember.getListForCombo("Membership", True)
            With cboMembership
                .ValueMember = "membershipunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True)
            dsCombos = objperiod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objEmpContribution.getComboList("EmpContribution", True, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Employee_Statutory_Contributions)
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "EmpContribution", True, enTranHeadType.EmployeesStatutoryDeductions, , enTypeOf.Employee_Statutory_Contributions)
            'Sohail (21 Aug 2015) -- End
            With cboEmpHeadType
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("EmpContribution")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objEmpContribution.getComboList("EmployerContribution", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            dsCombos = objEmpContribution.getComboList(FinancialYear._Object._DatabaseName, "EmployerContribution", True, enTranHeadType.EmployersStatutoryContributions, , enTypeOf.Employers_Statutory_Contribution)
            'Sohail (21 Aug 2015) -- End
            With cboCoHeadType
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("EmployerContribution")
                .SelectedValue = 0
            End With

            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")
            With cboIdentity
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboCoHeadType.SelectedValue = 0
            cboEmpHeadType.SelectedValue = 0
            cboIdentity.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrReport_GroupName = ""            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objMPR_AMD.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboMembership.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Membership is mandatory information. Please select Membership to continue."), enMsgBoxStyle.Information)
                cboMembership.Focus()
                Return False
            End If

            If CInt(cboIdentity.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Identity is mandatory information. Please select Identity to continue."), enMsgBoxStyle.Information)
                cboIdentity.Focus()
                Return False
            End If

            If CInt(cboEmpHeadType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Employee Contribution is mandatory information. Please select Employee Contribution to continue."), enMsgBoxStyle.Information)
                cboEmpHeadType.Focus()
                Return False
            End If

            If CInt(cboCoHeadType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Employer Contribution is mandatory information. Please select Employer Contribution to continue."), enMsgBoxStyle.Information)
                cboCoHeadType.Focus()
                Return False
            End If

            objMPR_AMD._MembershipId = cboMembership.SelectedValue
            objMPR_AMD._MembershipName = cboMembership.Text
            objMPR_AMD._PeriodId = cboPeriod.SelectedValue
            objMPR_AMD._PeriodName = cboPeriod.Text
            objMPR_AMD._EmpHeadTypeId = cboEmpHeadType.SelectedValue
            objMPR_AMD._EmprHeadTypeId = cboCoHeadType.SelectedValue
            objMPR_AMD._IdentityId = cboIdentity.SelectedValue
            objMPR_AMD._IdentityName = cboIdentity.Text
            objMPR_AMD._ViewByIds = mstrStringIds
            objMPR_AMD._ViewIndex = mintViewIdx
            objMPR_AMD._ViewByName = mstrStringName
            objMPR_AMD._Analysis_Fields = mstrAnalysis_Fields
            objMPR_AMD._Analysis_Join = mstrAnalysis_Join

            Dim oPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'oPeriod._Periodunkid = cboPeriod.SelectedValue
            oPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = cboPeriod.SelectedValue
            'Sohail (21 Aug 2015) -- End
            objMPR_AMD._PeriodEndDate = eZeeDate.convertDate(oPeriod._End_Date)
            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
            objMPR_AMD._PeriodCode = oPeriod._Period_Code
            'Sohail (14 May 2019) -- End
            oPeriod = Nothing


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Forms "

    Private Sub frmZa_IFT_P16_Report_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objMPR_AMD = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmZa_IFT_P16_Report_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmZa_IFT_P16_Report_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            Call OtherSettings()

            Me._Title = objMPR_AMD._ReportName
            Me._Message = objMPR_AMD._ReportDesc

            Call FillCombo()
            Call ResetValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmZa_IFT_P16_Report_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call Form_Report_Click(Me, New Aruti.Data.PrintButtonEventArgs(enPrintAction.Preview))
                End If
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
                    Exit Select
            End Select
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_KeyPress", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub Form_Report_Click(ByVal sender As Object, ByVal e As PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objMPR_AMD.generateReport(0, e.Type, enExportAction.None)
            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
            'objMPR_AMD.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                             User._Object._Userunkid, _
            '                             FinancialYear._Object._YearUnkid, _
            '                             Company._Object._Companyunkid, _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             ConfigParameter._Object._UserAccessModeSetting, _
            '                             True, ConfigParameter._Object._ExportReportPath, _
            '                             ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = cboPeriod.SelectedValue

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objMPR_AMD._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            objMPR_AMD.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         objPeriod._Start_Date, _
                                         objPeriod._End_Date, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (14 May 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Report_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_Export_Click(ByVal sender As System.Object, ByVal e As PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If Not SetFilter() Then Exit Sub
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objMPR_AMD.generateReport(0, enPrintAction.None, e.Type)
            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
            'objMPR_AMD.generateReportNew(FinancialYear._Object._DatabaseName, _
            '                             User._Object._Userunkid, _
            '                             FinancialYear._Object._YearUnkid, _
            '                             Company._Object._Companyunkid, _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
            '                             ConfigParameter._Object._UserAccessModeSetting, _
            '                             True, ConfigParameter._Object._ExportReportPath, _
            '                             ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = cboPeriod.SelectedValue

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objMPR_AMD._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            objMPR_AMD.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         objPeriod._Start_Date, _
                                         objPeriod._End_Date, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, ConfigParameter._Object._ExportReportPath, _
                                         ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, e.Type, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (14 May 2019) -- End
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Export_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (14 May 2019) -- Start
    'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
    Private Sub lnkENapsaExport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkENapsaExport.LinkClicked
        Try
            If Not SetFilter() Then Exit Sub

            objMPR_AMD._IsExportToCSV = True

            SaveDialog.FileName = ""
            SaveDialog.Filter = "CSV File|*.csv"
            SaveDialog.FilterIndex = 0

            SaveDialog.ShowDialog()
            If SaveDialog.FileName = "" Then
                Exit Try
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = cboPeriod.SelectedValue

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            objMPR_AMD._Analysis_Join = mstrAnalysis_Join.Replace(ConfigParameter._Object._EmployeeAsOnDate, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            objMPR_AMD.generateReportNew(FinancialYear._Object._DatabaseName, _
                                         User._Object._Userunkid, _
                                         FinancialYear._Object._YearUnkid, _
                                         Company._Object._Companyunkid, _
                                         objPeriod._Start_Date, _
                                         objPeriod._End_Date, _
                                         ConfigParameter._Object._UserAccessModeSetting, _
                                         True, SaveDialog.FileName, _
                                         ConfigParameter._Object._OpenAfterExport, 0, enPrintAction.None, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)

            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Data exported successfully!"), enMsgBoxStyle.Information)

            If ConfigParameter._Object._OpenAfterExport = True Then
                Process.Start(SaveDialog.FileName)
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "lnkENapsaExport_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub
    'Sohail (14 May 2019) -- End

    Private Sub Form_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Form_Cancel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_Language_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsZa_Monthly_Return_AMD.SetMessages()
            objfrm._Other_ModuleNames = "clsZa_Monthly_Return_AMD"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub lnkSetAnalysis_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkSetAnalysis.LinkClicked
        Dim frm As New frmViewAnalysis
        Try
            frm.displayDialog()
            mstrStringIds = frm._ReportBy_Ids
            mstrStringName = frm._ReportBy_Name
            mintViewIdx = frm._ViewIndex
            mstrAnalysis_Fields = frm._Analysis_Fields
            mstrAnalysis_Join = frm._Analysis_Join
            mstrReport_GroupName = frm._Report_GroupName
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkSetAnalysis_LinkClicked", mstrModuleName)
        Finally
            frm = Nothing
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbMandatoryInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbMandatoryInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbMandatoryInfo.Text = Language._Object.getCaption(Me.gbMandatoryInfo.Name, Me.gbMandatoryInfo.Text)
			Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.Name, Me.lnkSetAnalysis.Text)
			Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.Name, Me.lblMembership.Text)
			Me.lblEmpContriHeadType.Text = Language._Object.getCaption(Me.lblEmpContriHeadType.Name, Me.lblEmpContriHeadType.Text)
			Me.lblCompanyHeadType.Text = Language._Object.getCaption(Me.lblCompanyHeadType.Name, Me.lblCompanyHeadType.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblIdentity.Text = Language._Object.getCaption(Me.lblIdentity.Name, Me.lblIdentity.Text)
			Me.lnkENapsaExport.Text = Language._Object.getCaption(Me.lnkENapsaExport.Name, Me.lnkENapsaExport.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue.")
			Language.setMessage(mstrModuleName, 2, "Membership is mandatory information. Please select Membership to continue.")
			Language.setMessage(mstrModuleName, 3, "Identity is mandatory information. Please select Identity to continue.")
			Language.setMessage(mstrModuleName, 4, "Employee Contribution is mandatory information. Please select Employee Contribution to continue.")
			Language.setMessage(mstrModuleName, 5, "Employer Contribution is mandatory information. Please select Employer Contribution to continue.")
			Language.setMessage(mstrModuleName, 6, "Data exported successfully!")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
