'************************************************************************************************************************************
'Class Name : clsZa_ITF_P16_Report.vb
'Purpose    :
'Date        :11/07/2013
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsZa_ITF_P16_Report
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsZa_ITF_P16_Report"
    Private mstrReportId As String = enArutiReport.Za_ITF_P16_Report  '120
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty
    Private minSlabTranId As Integer = 0
    'Sohail (14 May 2019) -- Start
    'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
    Private mblnIsExportToCSV As Boolean = False
    Private mdtStartPeriodDate As DateTime = Nothing
    'Sohail (14 May 2019) -- End
    Private mdtEndPeriodDate As DateTime
    Private mstrPeriodName As String = String.Empty
    Private mintPeriodUnkid As Integer = 0
    'Sohail (19 Sep 2017) -- Start
    'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
    Private mintTaxAdjustedHeadUnkid As Integer = 0
    Private mstrTaxAdjustedHeadName As String = String.Empty
    Private mintMembershipUnkid As Integer = 0
    Private mstrMembershipName As String = String.Empty
    'Sohail (19 Sep 2017) -- End
    'Sohail (09 Nov 2017) -- Start
    'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
    Private mintIdentityUnkid As Integer = 0
    Private mstrIdentityName As String = String.Empty
    Private mblnShowIdentity As Boolean = False
    'Sohail (09 Nov 2017) -- End
    'Sohail (14 Apr 2018) -- Start
    'FINCA Zambia Enhancement - Ref # 226 : Statutory P16 Report to show gross Earnings in 5th column in 72.1.
    Private mstrDeductionHeadIDs As String = String.Empty
    Private mstrDeductionHeadNames As String = String.Empty
    'Sohail (14 Apr 2018) -- End
    'Sohail (27 Jul 2019) -- Start
    'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
    Private mintSkillDevHeadUnkid As Integer = 0
    Private mstrSkillDevHeadName As String = String.Empty
    'Sohail (27 Jul 2019) -- End
    'Sohail (17 Dec 2021) -- Start
    'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
    Private mintGrossEmolumentHeadId As Integer = 0
    Private mstrGrossEmolumentHeadName As String = String.Empty
    Private mintChargeableEmolumentHeadId As Integer = 0
    Private mstrChargeableEmolumentHeadName As String = String.Empty
    'Sohail (17 Dec 2021) -- End
    'Hemant (22 Dec 2022) -- Start
    'ENHANCEMENT(Zambia) : A1X-443 - PAYE MONTHLY RETURN - ITF P16 report enhancement to include TPIN
    Private mintTPINId As Integer = 0
    Private mstrTPINName As String = ""
    'Hemant (22 Dec 2022) -- End    
#End Region

#Region " Properties "

    Public WriteOnly Property _SlabTranId() As Integer
        Set(ByVal value As Integer)
            minSlabTranId = value
        End Set
    End Property

    'Sohail (14 May 2019) -- Start
    'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
    Public WriteOnly Property _IsExportToCSV() As Boolean
        Set(ByVal value As Boolean)
            mblnIsExportToCSV = value
        End Set
    End Property

    Public WriteOnly Property _StartPeriodDate() As DateTime
        Set(ByVal value As DateTime)
            mdtStartPeriodDate = value
        End Set
    End Property
    'Sohail (14 May 2019) -- End

    Public WriteOnly Property _EndPeriodDate() As DateTime
        Set(ByVal value As DateTime)
            mdtEndPeriodDate = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodUnkid() As Integer
        Set(ByVal value As Integer)
            mintPeriodUnkid = value
        End Set
    End Property

    'Sohail (19 Sep 2017) -- Start
    'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
    Public WriteOnly Property _TaxAdjustedHeadUnkid() As Integer
        Set(ByVal value As Integer)
            mintTaxAdjustedHeadUnkid = value
        End Set
    End Property

    Public WriteOnly Property _TaxAdjustedHeadName() As String
        Set(ByVal value As String)
            mstrTaxAdjustedHeadName = value
        End Set
    End Property

    Public WriteOnly Property _MembershipUnkid() As Integer
        Set(ByVal value As Integer)
            mintMembershipUnkid = value
        End Set
    End Property

    Public WriteOnly Property _MembershipName() As String
        Set(ByVal value As String)
            mstrMembershipName = value
        End Set
    End Property
    'Sohail (19 Sep 2017) -- End

    'Sohail (09 Nov 2017) -- Start
    'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
    Public WriteOnly Property _IdentityUnkid() As Integer
        Set(ByVal value As Integer)
            mintIdentityUnkid = value
        End Set
    End Property

    Public WriteOnly Property _IdentityName() As String
        Set(ByVal value As String)
            mstrIdentityName = value
        End Set
    End Property

    Public WriteOnly Property _ShowIdentity() As Boolean
        Set(ByVal value As Boolean)
            mblnShowIdentity = value
        End Set
    End Property
    'Sohail (09 Nov 2017) -- End

    'Sohail (14 Apr 2018) -- Start
    'FINCA Zambia Enhancement - Ref # 226 : Statutory P16 Report to show gross Earnings in 5th column in 72.1.
    Public WriteOnly Property _DeductionHeadIDs() As String
        Set(ByVal value As String)
            mstrDeductionHeadIDs = value
        End Set
    End Property

    Public WriteOnly Property _DeductionHeadNames() As String
        Set(ByVal value As String)
            mstrDeductionHeadNames = value
        End Set
    End Property
    'Sohail (14 Apr 2018) -- End

    'Sohail (27 Jul 2019) -- Start
    'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
    Public WriteOnly Property _SkillDevHeadUnkid() As Integer
        Set(ByVal value As Integer)
            mintSkillDevHeadUnkid = value
        End Set
    End Property

    Public WriteOnly Property _SkillDevHeadName() As String
        Set(ByVal value As String)
            mstrSkillDevHeadName = value
        End Set
    End Property
    'Sohail (27 Jul 2019) -- End

    'Sohail (17 Dec 2021) -- Start
    'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
    Public WriteOnly Property _GrossEmolumentHeadId() As Integer
        Set(ByVal value As Integer)
            mintGrossEmolumentHeadId = value
        End Set
    End Property

    Public WriteOnly Property _GrossEmolumentHeadName() As String
        Set(ByVal value As String)
            mstrGrossEmolumentHeadName = value
        End Set
    End Property

    Public WriteOnly Property _ChargeableEmolumentHeadId() As Integer
        Set(ByVal value As Integer)
            mintChargeableEmolumentHeadId = value
        End Set
    End Property

    Public WriteOnly Property _ChargeableEmolumentHeadName() As String
        Set(ByVal value As String)
            mstrChargeableEmolumentHeadName = value
        End Set
    End Property
    'Sohail (17 Dec 2021) -- End

    'Hemant (22 Dec 2022) -- Start
    'ENHANCEMENT(Zambia) : A1X-443 - PAYE MONTHLY RETURN - ITF P16 report enhancement to include TPIN
    Public WriteOnly Property _TPINId() As Integer
        Set(ByVal value As Integer)
            mintTPINId = value
        End Set
    End Property

    Public WriteOnly Property _TPINName() As String
        Set(ByVal value As String)
            mstrTPINName = value
        End Set
    End Property
    'Hemant (22 Dec 2022) -- End

#End Region

#Region " Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = String.Empty
            mstrViewByName = String.Empty
            mstrReport_GroupName = String.Empty
            minSlabTranId = 0
            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
            mblnIsExportToCSV = False
            mdtStartPeriodDate = Nothing
            'Sohail (14 May 2019) -- End
            mdtEndPeriodDate = Nothing
            mstrPeriodName = String.Empty
            mintPeriodUnkid = 0
            'Sohail (19 Sep 2017) -- Start
            'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
            mintTaxAdjustedHeadUnkid = 0
            mstrTaxAdjustedHeadName = ""
            mintMembershipUnkid = 0
            mstrMembershipName = ""
            'Sohail (19 Sep 2017) -- End
            'Sohail (09 Nov 2017) -- Start
            'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
            mintIdentityUnkid = 0
            mstrIdentityName = ""
            mblnShowIdentity = False
            'Sohail (09 Nov 2017) -- End
            'Sohail (27 Jul 2019) -- Start
            'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
            mintSkillDevHeadUnkid = 0
            mstrSkillDevHeadName = String.Empty
            'Sohail (27 Jul 2019) -- End
            'Sohail (17 Dec 2021) -- Start
            'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
            mintGrossEmolumentHeadId = 0
            mstrGrossEmolumentHeadName = String.Empty
            mintChargeableEmolumentHeadId = 0
            mstrChargeableEmolumentHeadName = String.Empty
            'Sohail (17 Dec 2021) -- End
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-443 - PAYE MONTHLY RETURN - ITF P16 report enhancement to include TPIN
            mintTPINId = 0
            mstrTPINName = String.Empty
            'Hemant (22 Dec 2022) -- End	
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            'Sohail (19 Sep 2017) -- Start
            'Enhancement - 69.1 - The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report.
            'objDataOperation.AddParameter("@TranHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, minSlabTranId)
            objDataOperation.AddParameter("@payeheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minSlabTranId)
            objDataOperation.AddParameter("@taxadjustedheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTaxAdjustedHeadUnkid)
            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkid)
            objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipUnkid)
            'Sohail (19 Sep 2017) -- End
            'Sohail (09 Nov 2017) -- Start
            'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
            objDataOperation.AddParameter("@IdentityId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIdentityUnkid)
            'Sohail (09 Nov 2017) -- End

            'Sohail (27 Jul 2019) -- Start
            'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
            objDataOperation.AddParameter("@SkillDevHeadUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillDevHeadUnkid)
            'Sohail (27 Jul 2019) -- End
            'Sohail (17 Dec 2021) -- Start
            'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
            objDataOperation.AddParameter("@GrossEmolumentHeadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGrossEmolumentHeadId)
            objDataOperation.AddParameter("@ChargeableEmolumentHeadUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChargeableEmolumentHeadId)
            'Sohail (17 Dec 2021) -- End
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-443 - PAYE MONTHLY RETURN - ITF P16 report enhancement to include TPIN
            objDataOperation.AddParameter("@tpinid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTPINId)
            'Hemant (22 Dec 2022) -- End	

            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEndPeriodDate))

            objDataOperation.AddParameter("@Upto", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Upto"))
            objDataOperation.AddParameter("@Above", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Above"))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId, xExportReportPath)
            'Sohail (14 May 2019) -- End

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "
    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer, _
                                           ByVal strExportPath As String _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (14 May 2019) - [strExportPath]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId

            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End

            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEndPeriodDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
            Dim xAdvanceJoinQry As String
            xAdvanceJoinQry = ""
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEndPeriodDate, strDatabaseName)

            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtStartPeriodDate, mdtEndPeriodDate, , , strDatabaseName)

            Dim xUACQryBF, xUACFiltrQryBF, xDateJoinQryBF, xDateFilterQryBF As String
            xUACQryBF = "" : xUACFiltrQryBF = "" : xDateJoinQryBF = "" : xDateFilterQryBF = ""
            Call NewAccessLevelFilterString(xUACQryBF, xUACFiltrQryBF, mdtStartPeriodDate.AddDays(-1), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetDatesFilterString(xDateJoinQryBF, xDateFilterQryBF, mdtStartPeriodDate.AddDays(-1), mdtStartPeriodDate.AddDays(-1), , , strDatabaseName)

            Dim xDateJoinQryCF, xDateFilterQryCF As String
            xDateJoinQryCF = "" : xDateFilterQryCF = ""
            Call GetDatesFilterString(xDateJoinQryCF, xDateFilterQryCF, mdtEndPeriodDate, mdtEndPeriodDate, , , strDatabaseName)
            'Sohail (14 May 2019) -- End

            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes. (For column E Chargeable Emolument, Deduct PAYE head first slab amount upto value which has rate payable as Zero from Taxable Earning)
            Dim decFirstSlabAmt As Decimal = 0

            StrQ = "SELECT A.amountupto " & _
                         ", A.ratepayable " & _
                         ", A.ROWNO " & _
                    "FROM " & _
                    "( " & _
                        "SELECT amountupto " & _
                             ", ratepayable " & _
                             ", DENSE_RANK() OVER (PARTITION BY prtranhead_inexcessslab_tran.tranheadunkid ORDER BY end_date DESC ) AS ROWNO " & _
                        "FROM prtranhead_inexcessslab_tran " & _
                            "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_inexcessslab_tran.periodunkid " & _
                        "WHERE prtranhead_inexcessslab_tran.isvoid = 0 " & _
                              "AND cfcommon_period_tran.isactive = 1 " & _
                              "AND prtranhead_inexcessslab_tran.tranheadunkid = @tranheadunkid " & _
                              "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                    ") AS A " & _
                    "WHERE A.ROWNO = 1 " & _
                          "AND A.ratepayable = 0 "

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minSlabTranId)
            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndPeriodDate))

            Dim dsSlab As DataSet = objDataOperation.ExecQuery(StrQ, "Slab")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsSlab.Tables(0).Rows.Count > 0 Then
                decFirstSlabAmt = CDec(dsSlab.Tables(0).Rows(0).Item("amountupto"))
            End If

            objDataOperation.ClearParameters()
            'Sohail (14 May 2019) -- End

            StrQ = "SELECT   TaxableEarning AS TaxableEarning " & _
                          ", TaxableEarning - " & decFirstSlabAmt & " AS ChargeableEarning " & _
                          ", paye AS paye " & _
                          ", taxadjusted AS taxadjusted " & _
                          ", grossemolument " & _
                          ", chargeableemolument " & _
                          ", empid AS empid " & _
                          ", hremployee_master.employeecode AS empcode " & _
                          ", ISNULL(hremployee_meminfo_tran.membershipno, '') AS membershipno " & _
                          ", ISNULL(cfcommon_master.name, '') AS employmenttype " & _
                          ", ISNULL(hremployee_idinfo_tran.identity_no, '') AS IdentityName " & _
                          ", ISNULL(TPIN.identity_no, '') AS TPIN "
            'Hemant (22 Dec 2022) -- [TPIN]
            'Sohail (17 Dec 2021) - [grossemolument, chargeableemolument]
            'Sohail (14 May 2019) - [TaxableEarning - employeecontribution AS ChargeableEarning] = [TaxableEarning - " & decFirstSlabAmt & " AS ChargeableEarning]
            'Sohail (03 Apr 2018) - [ISNULL(IDnt.name, '') AS IdentityName = ISNULL(hremployee_idinfo_tran.identity_no, '') AS IdentityName]
            'Sohail (09 Nov 2017) - [IdentityName]

            If ConfigParameter._Object._FirstNamethenSurname = True Then
                StrQ &= ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS empname "
            Else
                StrQ &= ", ISNULL(surname, '') + ' ' + ISNULL(firstname, '') + ' ' + ISNULL(othername, '') AS empname "
            End If


            StrQ &= "FROM    ( SELECT    SUM(ISNULL(PAYE.TaxableEarning, 0)) AS TaxableEarning " & _
                                      ", SUM(ISNULL(PAYE.employeecontribution, 0)) AS employeecontribution " & _
                                      ", SUM(ISNULL(PAYE.paye, 0)) AS paye " & _
                                      ", SUM(ISNULL(PAYE.taxadjusted, 0)) AS taxadjusted " & _
                                      ", SUM(ISNULL(PAYE.grossemolument, 0)) AS grossemolument " & _
                                      ", SUM(ISNULL(PAYE.chargeableemolument, 0)) AS chargeableemolument " & _
                                      ", paye.empid AS empid " & _
                              "FROM      ( SELECT    CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS TaxableEarning " & _
                                                  ", 0 AS employeecontribution " & _
                                                  ", 0 AS paye " & _
                                                  ", 0 AS taxadjusted " & _
                                                  ", 0 AS grossemolument " & _
                                                  ", 0 AS chargeableemolument " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            'Sohail (17 Dec 2021) - [grossemolument, chargeableemolument]

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                                                    "AND prtranhead_master.istaxable = 1 " & _
                                                    "AND prtnaleave_tran.payperiodunkid = @payperiodunkid "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS TaxableEarning " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS employeecontribution " & _
                                                  ", 0 AS paye " & _
                                                  ", 0 AS taxadjusted " & _
                                                  ", 0 AS grossemolument " & _
                                                  ", 0 AS chargeableemolument " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prpayrollprocess_tran.tranheadunkid IN (" & mstrDeductionHeadIDs & ") " & _
                                                    "AND prtnaleave_tran.payperiodunkid = @payperiodunkid "
            'Sohail (14 Apr 2018) -- Start
            'FINCA Zambia Enhancement - Ref # 226 : Statutory P16 Report to show gross Earnings in 5th column in 72.1.
            '[AND prtranhead_master.trnheadtype_id = " & CInt(enTranHeadType.EmployeesStatutoryDeductions) & "] = []
            'Sohail (14 Apr 2018) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS TaxableEarning " & _
                                                  ", 0 AS employeecontribution " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS paye " & _
                                                  ", 0 AS taxadjusted " & _
                                                  ", 0 AS grossemolument " & _
                                                  ", 0 AS chargeableemolument " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @payeheadunkid " & _
                                                    "AND prtnaleave_tran.payperiodunkid = @payperiodunkid "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS TaxableEarning " & _
                                                  ", 0 AS employeecontribution " & _
                                                  ", 0 AS paye " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS taxadjusted " & _
                                                  ", 0 AS grossemolument " & _
                                                  ", 0 AS chargeableemolument " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @taxadjustedheadunkid " & _
                                                    "AND prtnaleave_tran.payperiodunkid = @payperiodunkid "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'Sohail (17 Dec 2021) -- Start
            'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
            StrQ &= "                      UNION ALL " & _
                                          "SELECT    0 AS TaxableEarning " & _
                                                  ", 0 AS employeecontribution " & _
                                                  ", 0 AS paye " & _
                                                  ", 0 AS taxadjusted " & _
                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS grossemolument " & _
                                                  ", 0 AS chargeableemolument " & _
                                                  ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                          "FROM      prpayrollprocess_tran " & _
                                                    "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @grossemolumentheadunkid " & _
                                                    "AND prtnaleave_tran.payperiodunkid = @payperiodunkid "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= "                      UNION ALL " & _
                                         "SELECT    0 AS TaxableEarning " & _
                                                 ", 0 AS employeecontribution " & _
                                                 ", 0 AS paye " & _
                                                 ", 0 AS taxadjusted " & _
                                                 ", 0 AS grossemolument " & _
                                                 ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS chargeableemolument " & _
                                                 ", prpayrollprocess_tran.employeeunkid AS empid " & _
                                         "FROM      prpayrollprocess_tran " & _
                                                   "JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                                                   "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                                   "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            StrQ &= "                      WHERE     prpayrollprocess_tran.isvoid = 0 " & _
                                                    "AND prtnaleave_tran.isvoid = 0 " & _
                                                    "AND prtranhead_master.isvoid = 0 " & _
                                                    "AND prtranhead_master.tranheadunkid = @chargeableemolumentheadunkid " & _
                                                    "AND prtnaleave_tran.payperiodunkid = @payperiodunkid "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If
            'Sohail (17 Dec 2021) -- End

            'Sohail (09 Nov 2017) -- Start
            'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
            'StrQ &= "                      ) AS PAYE " & _
            '                  "WHERE     1 = 1 " & _
            '                  "GROUP BY  empid " & _
            '                  "/*HAVING    SUM(ISNULL(PAYE.paye, 0)) <> 0*/ " & _
            '                ") AS A " & _
            '                "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
            '                "LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
            '                        "AND ISNULL(hremployee_meminfo_tran.isactive, 1) = 1 " & _
            '                        "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
            '                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
            '                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = employmenttypeunkid " & _
            '                        "AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE) & " " & _
            '        "WHERE   hrmembership_master.membershipunkid = @membershipunkid "

            'Sohail (12 Dec 2017) -- Start
            'SUPPORT 1468 - Silverlands | Some employees not showing in NSSF report. Removing hremployee_meminfo_tran.isactive filter from all statutory reports in 70.1.
            'StrQ &= "                      ) AS PAYE " & _
            '                  "WHERE     1 = 1 " & _
            '                  "GROUP BY  empid " & _
            '                  "/*HAVING    SUM(ISNULL(PAYE.paye, 0)) <> 0*/ " & _
            '                ") AS A " & _
            '                "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
            '                "LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
            '                        "AND ISNULL(hremployee_meminfo_tran.isactive, 1) = 1 " & _
            '                        "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 " & _
            '                "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
            '                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = employmenttypeunkid " & _
            '                        "AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE) & " " & _
            '                "LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = A.empid " & _
            '                "LEFT JOIN cfcommon_master AS IDnt ON IDnt.masterunkid = hremployee_idinfo_tran.idtypeunkid " & _
            '                        "AND IDnt.masterunkid = @IdentityId " & _
            '        "WHERE   hrmembership_master.membershipunkid = @membershipunkid "
            StrQ &= "                      ) AS PAYE " & _
                              "WHERE     1 = 1 " & _
                              "GROUP BY  empid " & _
                              "/*HAVING    SUM(ISNULL(PAYE.paye, 0)) <> 0*/ " & _
                            ") AS A " & _
                            "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = A.empid " & _
                            "LEFT JOIN hremployee_meminfo_tran ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                                    "AND ISNULL(hremployee_meminfo_tran.isdeleted, 0) = 0 "

            'Sohail (06 Oct 2018) -- Start
            'Enhancement - PACRA - Ref. No.  226 : Make Membership selection optional on PAYE Monthly Return ITF P16 Report in 75.1.
            If mintMembershipUnkid <= 0 Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (06 Oct 2018) -- End

            StrQ &= "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                            "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = employmenttypeunkid " & _
                                    "AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE) & " " & _
                            "LEFT JOIN hremployee_idinfo_tran ON hremployee_idinfo_tran.employeeunkid = A.empid " & _
                                    "AND hremployee_idinfo_tran.idtypeunkid = @IdentityId " & _
                            "/*LEFT JOIN cfcommon_master AS IDnt ON IDnt.masterunkid = hremployee_idinfo_tran.idtypeunkid " & _
                                    "AND IDnt.masterunkid = @IdentityId*/ " & _
                            "LEFT JOIN hremployee_idinfo_tran AS TPIN ON TPIN.employeeunkid = A.empid " & _
                                    "AND TPIN.idtypeunkid = @tpinid " & _
                    "WHERE   1 = 1 "
            'Sohail (06 Oct 2018) - [WHERE   hrmembership_master.membershipunkid = @membershipunkid] = [WHERE   1 = 1], AND hrmembership_master.membershipunkid = @membershipunkid
            'Sohail (03 Apr 2018) - [Removed LEFT JOIN IDnt], [Added = AND hremployee_idinfo_tran.idtypeunkid = @IdentityId]
            'Sohail (12 Dec 2017) -- End
            'Sohail (09 Nov 2017) -- End

            'Sohail (06 Oct 2018) -- Start
            'Enhancement - PACRA - Ref. No.  226 : Make Membership selection optional on PAYE Monthly Return ITF P16 Report in 75.1.
            If mintMembershipUnkid > 0 Then
                StrQ &= " AND hrmembership_master.membershipunkid = @membershipunkid "
            End If
            'Sohail (06 Oct 2018) -- End


            Call FilterTitleAndFilterQuery()
            StrQ &= Me._FilterQuery

            If ConfigParameter._Object._FirstNamethenSurname = True Then
                StrQ &= " ORDER BY  ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') "
            Else
                StrQ &= " ORDER BY  ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '')  "
            End If


            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim rpt_Row As DataRow = Nothing

            Dim decTaxableEarning As Decimal = 0
            Dim decChargeableEarning As Decimal = 0
            Dim decPaye As Decimal = 0
            Dim decTaxadjusted As Decimal = 0

            For Each dRow As DataRow In dsList.Tables(0).Rows
                rpt_Row = rpt_Data.Tables(0).NewRow

                rpt_Row.Item("Column1") = FinancialYear._Object._FinancialYear_Name
                rpt_Row.Item("Column2") = dRow.Item("empcode")
                rpt_Row.Item("Column3") = dRow.Item("empname")
                rpt_Row.Item("Column4") = dRow.Item("employmenttype")
                'Sohail (09 Nov 2017) -- Start
                'Enhancement - 70.1 - Option to show Employee Identity on NAPSA Report for Zambia.
                'rpt_Row.Item("Column5") = dRow.Item("membershipno")
                If mblnShowIdentity = True Then
                    rpt_Row.Item("Column5") = dRow.Item("IdentityName")
                    'Sohail (20 Mar 2020) -- Start
                    'ZRA Enhancement # 0004548 : Need one more column to show selected identity name in the e-Tax Remittance CSV (PAYE) statutory report for 2020.
                    rpt_Row.Item("Column11") = mstrIdentityName
                    'Sohail (20 Mar 2020) -- End
                Else
                    rpt_Row.Item("Column5") = dRow.Item("membershipno")
                    'Sohail (20 Mar 2020) -- Start
                    'ZRA Enhancement # 0004548 : Need one more column to show selected identity name in the e-Tax Remittance CSV (PAYE) statutory report for 2020.
                    rpt_Row.Item("Column11") = ""
                    'Sohail (20 Mar 2020) -- End
                End If
                'Sohail (09 Nov 2017) -- End
                rpt_Row.Item("Column10") = dRow.Item("EmpId")

                'Sohail (17 Dec 2021) -- Start
                'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
                'rpt_Row.Item("Column6") = Format(CDec(dRow.Item("TaxableEarning")), GUI.fmtCurrency)
                rpt_Row.Item("Column6") = Format(CDec(dRow.Item("grossemolument")), GUI.fmtCurrency)
                'Sohail (17 Dec 2021) -- End
                'Sohail (14 May 2019) -- Start
                'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes. (For column E, If it is negative then put Zero)
                'rpt_Row.Item("Column7") = Format(CDec(dRow.Item("ChargeableEarning")), GUI.fmtCurrency)
                'Sohail (17 Dec 2021) -- Start
                'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
                'If CDec(dRow.Item("ChargeableEarning")) < 0 Then
                If CDec(dRow.Item("chargeableemolument")) < 0 Then
                    'Sohail (17 Dec 2021) -- End
                    rpt_Row.Item("Column7") = Format(0, GUI.fmtCurrency)
                Else
                    'Sohail (17 Dec 2021) -- Start
                    'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
                    'rpt_Row.Item("Column7") = Format(CDec(dRow.Item("ChargeableEarning")), GUI.fmtCurrency)
                    rpt_Row.Item("Column7") = Format(CDec(dRow.Item("chargeableemolument")), GUI.fmtCurrency)
                    'Sohail (17 Dec 2021) -- End
                End If
                'Sohail (14 May 2019) -- End
                rpt_Row.Item("Column8") = Format(CDec(dRow.Item("paye")), GUI.fmtCurrency)
                rpt_Row.Item("Column9") = Format(CDec(dRow.Item("taxadjusted")), GUI.fmtCurrency)
                'Hemant (22 Dec 2022) -- Start
                'ENHANCEMENT(Zambia) : A1X-443 - PAYE MONTHLY RETURN - ITF P16 report enhancement to include TPIN
                rpt_Row.Item("Column12") = dRow.Item("tpin")
                'Hemant (22 Dec 2022) -- End	

                'Sohail (17 Dec 2021) -- Start
                'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
                'decTaxableEarning += CDec(Format(CDec(dRow.Item("TaxableEarning")), GUI.fmtCurrency))
                'decChargeableEarning += CDec(Format(CDec(dRow.Item("ChargeableEarning")), GUI.fmtCurrency))
                decTaxableEarning += CDec(Format(CDec(rpt_Row.Item("Column6")), GUI.fmtCurrency))
                decChargeableEarning += CDec(Format(CDec(rpt_Row.Item("Column7")), GUI.fmtCurrency))
                'Sohail (17 Dec 2021) -- End
                decPaye += CDec(Format(CDec(dRow.Item("paye")), GUI.fmtCurrency))
                decTaxadjusted += CDec(Format(CDec(dRow.Item("taxadjusted")), GUI.fmtCurrency))

                rpt_Data.Tables(0).Rows.Add(rpt_Row)
            Next

            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes. (Show staff turnover of the month)
            If mblnIsExportToCSV = True Then
                Dim mdtTableExcel As DataTable = rpt_Data.Tables(0)
                Dim intColIdx As Integer = -1

                'Sohail (20 Mar 2020) -- Start
                'ZRA Enhancement # 0004548 : Need one more column to show selected identity name in the e-Tax Remittance CSV (PAYE) statutory report for 2020.
                intColIdx += 1
                mdtTableExcel.Columns("Column11").Caption = Language.getMessage(mstrModuleName, 72, "Identity Type")
                mdtTableExcel.Columns("Column11").SetOrdinal(intColIdx)
                'Sohail (20 Mar 2020) -- End

                intColIdx += 1
                mdtTableExcel.Columns("Column5").Caption = Language.getMessage(mstrModuleName, 36, "NRC of Employee")
                mdtTableExcel.Columns("Column5").SetOrdinal(intColIdx)

                'Hemant (22 Dec 2022) -- Start
                'ENHANCEMENT(Zambia) : A1X-443 - PAYE MONTHLY RETURN - ITF P16 report enhancement to include TPIN
                intColIdx += 1
                mdtTableExcel.Columns("Column12").Caption = Language.getMessage(mstrModuleName, 73, "TPIN")
                mdtTableExcel.Columns("Column12").SetOrdinal(intColIdx)
                'Hemant (22 Dec 2022) -- End	

                intColIdx += 1
                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 37, "Name of Employee")
                mdtTableExcel.Columns("Column3").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column4").Caption = Language.getMessage(mstrModuleName, 38, "Nature of Employment Temporal/Permanent")
                mdtTableExcel.Columns("Column4").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column6").Caption = Language.getMessage(mstrModuleName, 39, "Gross emoluments for the tax period")
                mdtTableExcel.Columns("Column6").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column7").Caption = Language.getMessage(mstrModuleName, 40, "Chargeable emoluments for the tax period")
                mdtTableExcel.Columns("Column7").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column8").Caption = Language.getMessage(mstrModuleName, 41, "Total tax credit for the tax period")
                mdtTableExcel.Columns("Column8").SetOrdinal(intColIdx)

                intColIdx += 1
                mdtTableExcel.Columns("Column70").Expression = "Column8"
                mdtTableExcel.Columns("Column70").Caption = Language.getMessage(mstrModuleName, 42, "Tax Deducted as  shown on the TRD for the year")
                mdtTableExcel.Columns("Column70").SetOrdinal(intColIdx)

                If mintTaxAdjustedHeadUnkid > 0 Then
                    intColIdx += 1
                    mdtTableExcel.Columns("Column9").Caption = Language.getMessage(mstrModuleName, 43, "Tax Adjusted")
                    mdtTableExcel.Columns("Column9").SetOrdinal(intColIdx)
                End If

                For i = intColIdx + 1 To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(intColIdx + 1)
                Next

                Dim strSB As System.Text.StringBuilder = DataTableToCSV(mdtTableExcel, CChar(","), False)

                If SaveTextFile(strExportPath, strSB) = False Then
                    Return Nothing
                    Exit Function
                Else
                    Return Nothing
                    Exit Function
                End If
            End If


            StrQ = "SELECT employeeunkid " & _
                         ", CONVERT(CHAR(8), MAX(mindate), 112) AS terminationdate " & _
                    "INTO #Terminated " & _
                    "FROM " & _
                    "( " & _
                        "SELECT hremployee_dates_tran.employeeunkid " & _
                             ", date1 " & _
                             ", date2 " & _
                        "FROM hremployee_dates_tran " & _
                            "LEFT JOIN hremployee_master ON hremployee_dates_tran.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            StrQ &= "WHERE isvoid = 0 " & _
                              "AND hremployee_dates_tran.datetypeunkid IN ( " & enEmp_Dates_Transaction.DT_TERMINATION & ", " & enEmp_Dates_Transaction.DT_RETIREMENT & " ) " & _
                              "AND ( (datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT & " AND date1 BETWEEN @startdate AND @enddate ) OR ( datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " AND (date1 BETWEEN @startdate AND @enddate OR date2 BETWEEN @startdate AND @enddate )) ) "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If blnOnlyApproved = False Then
            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If
            'End If

            StrQ &= ") AS A UNPIVOT(mindate FOR d IN(date1, date2))unpvt " & _
                    "GROUP BY employeeunkid " & _
                    "HAVING CONVERT(CHAR(8), MAX(mindate), 112) " & _
                    "BETWEEN @startdate AND @enddate "

            StrQ &= " " & _
                "SELECT SUM(TotalEmployee) AS TotalEmployee " & _
                     ", SUM(TotalEmployeeBF) AS TotalEmployeeBF " & _
                     ", SUM(TotalAppointed) AS TotalAppointed " & _
                     ", SUM(TotalRehired) AS TotalRehired " & _
                     ", SUM(TotalTerminated) AS TotalTerminated " & _
                     ", SUM(TotalEmployeeCF) AS TotalEmployeeCF " & _
                     ", SUM(TotalEarning) AS TotalEarning " & _
                "FROM ( " & _
                    "SELECT COUNT(hremployee_master.employeeunkid) AS TotalEmployee " & _
                         ", 0 AS TotalEmployeeBF " & _
                         ", 0 AS TotalAppointed " & _
                         ", 0 AS TotalRehired " & _
                         ", 0 AS TotalTerminated " & _
                         ", 0 AS TotalEmployeeCF " & _
                         ", 0 AS TotalEarning " & _
                    "FROM hremployee_master "

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            StrQ &= "WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If blnOnlyApproved = False Then
            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If
            'End If

            StrQ &= "UNION ALL " & _
                    "SELECT 0 AS TotalEmployee " & _
                         ", COUNT(hremployee_master.employeeunkid) AS TotalEmployeeBF " & _
                         ", 0 AS TotalAppointed " & _
                         ", 0 AS TotalRehired " & _
                         ", 0 AS TotalTerminated " & _
                         ", 0 AS TotalEmployeeCF " & _
                         ", 0 AS TotalEarning " & _
                    "FROM hremployee_master "

            StrQ &= mstrAnalysis_Join

            If xDateJoinQryBF.Trim.Length > 0 Then
                StrQ &= xDateJoinQryBF
            End If

            If xUACQryBF.Trim.Length > 0 Then
                StrQ &= xUACQryBF
            End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            StrQ &= "WHERE 1 = 1 "

            If xUACFiltrQryBF.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQryBF
            End If

            'If blnOnlyApproved = False Then
            If xDateFilterQryBF.Trim.Length > 0 Then
                StrQ &= xDateFilterQryBF
            End If
            'End If

            StrQ &= "UNION ALL " & _
                    "SELECT 0 AS TotalEmployee " & _
                         ", 0 AS TotalEmployeeBF " & _
                         ", COUNT(hremployee_master.employeeunkid) AS TotalAppointed " & _
                         ", 0 AS TotalRehired " & _
                         ", 0 AS TotalTerminated " & _
                         ", 0 AS TotalEmployeeCF " & _
                         ", 0 AS TotalEarning " & _
                    "FROM hremployee_master "

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            StrQ &= "WHERE 1 = 1 " & _
                          "AND CONVERT(CHAR(8), appointeddate, 112) " & _
                          "BETWEEN @startdate AND @enddate "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If blnOnlyApproved = False Then
            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If
            'End If

            StrQ &= "UNION ALL " & _
                    "SELECT 0 AS TotalEmployee " & _
                         ", 0 AS TotalEmployeeBF " & _
                         ", 0 AS TotalAppointed " & _
                         ", COUNT(A.employeeunkid) AS TotalRehired " & _
                         ", 0 AS TotalTerminated " & _
                         ", 0 AS TotalEmployeeCF " & _
                         ", 0 AS TotalEarning " & _
                    "FROM " & _
                    "( " & _
                        "SELECT hremployee_rehire_tran.employeeunkid " & _
                             ", CONVERT(CHAR(8), reinstatment_date, 112) AS reinstatment_date " & _
                             ", DENSE_RANK() OVER (PARTITION BY hremployee_rehire_tran.employeeunkid ORDER BY effectivedate DESC, rehiretranunkid DESC ) AS ROWNO " & _
                        "FROM hremployee_rehire_tran " & _
                             "LEFT JOIN hremployee_master ON hremployee_rehire_tran.employeeunkid = hremployee_master.employeeunkid "

            StrQ &= mstrAnalysis_Join

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            StrQ &= "WHERE isvoid = 0 " & _
                              "AND CONVERT(CHAR(8), hremployee_rehire_tran.reinstatment_date, 112) " & _
                              "BETWEEN @startdate AND @enddate "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If blnOnlyApproved = False Then
            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If
            'End If

            StrQ &= ") AS A " & _
                        "LEFT JOIN #Terminated ON #Terminated.employeeunkid = A.employeeunkid " & _
                    "WHERE A.ROWNO = 1 " & _
                          "AND ( " & _
                                  "A.reinstatment_date > #Terminated.terminationdate " & _
                                  "OR #Terminated.terminationdate IS NULL " & _
                              ") "

            StrQ &= "UNION ALL " & _
                    "SELECT 0 AS TotalEmployee " & _
                         ", 0 AS TotalEmployeeBF " & _
                         ", 0 AS TotalAppointed " & _
                         ", 0 AS TotalRehired " & _
                         ", COUNT(employeeunkid) AS TotalTerminated " & _
                         ", 0 AS TotalEmployeeCF " & _
                         ", 0 AS TotalEarning " & _
                    "FROM #Terminated " & _
                    "WHERE 1 = 1 "

            StrQ &= "UNION ALL " & _
                   "SELECT 0 AS TotalEmployee " & _
                        ", 0 AS TotalEmployeeBF " & _
                        ", 0 AS TotalAppointed " & _
                        ", 0 AS TotalRehired " & _
                        ", 0 AS TotalTerminated " & _
                        ", COUNT(hremployee_master.employeeunkid) AS TotalEmployeeCF " & _
                        ", 0 AS TotalEarning " & _
                   "FROM hremployee_master "

            StrQ &= mstrAnalysis_Join

            If xDateJoinQryCF.Trim.Length > 0 Then
                StrQ &= xDateJoinQryCF
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            StrQ &= "WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If blnOnlyApproved = False Then
            If xDateFilterQryCF.Trim.Length > 0 Then
                StrQ &= xDateFilterQryCF
            End If
            'End If

            'Sohail (10 Jul 2019) -- Start
            'EFC ZAMBIA - Support Issue Id # 3964 - 76.1 - Claim Request Earning was not included in Total Earning.
            'StrQ &= "UNION ALL " & _
            '       "SELECT 0 AS TotalEmployee " & _
            '            ", 0 AS TotalEmployeeBF " & _
            '            ", 0 AS TotalAppointed " & _
            '            ", 0 AS TotalRehired " & _
            '            ", 0 AS TotalTerminated " & _
            '            ", 0 AS TotalEmployeeCF " & _
            '            ", SUM(prpayrollprocess_tran.amount) AS TotalEarning " & _
            '       "FROM prpayrollprocess_tran " & _
            '       "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
            '       "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid AND prpayrollprocess_tran.tranheadunkid > 0 " & _
            '       "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "
            StrQ &= "UNION ALL " & _
                  "SELECT 0 AS TotalEmployee " & _
                       ", 0 AS TotalEmployeeBF " & _
                       ", 0 AS TotalAppointed " & _
                       ", 0 AS TotalRehired " & _
                       ", 0 AS TotalTerminated " & _
                       ", 0 AS TotalEmployeeCF " & _
                       ", SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))) AS TotalEarning " & _
                  "FROM prpayrollprocess_tran " & _
                  "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                  "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid " & _
                  "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prpayrollprocess_tran.tranheadunkid "
            'Sohail (29 Dec 2021) - [LEFT JOIN prtranhead_master]
            'Sohail (10 Jul 2019) -- End

            StrQ &= mstrAnalysis_Join

            If xDateJoinQryCF.Trim.Length > 0 Then
                'Sohail (10 Jul 2019) -- Start
                'EFC ZAMBIA - Support Issue Id # 3964 - 76.1 - Claim Request Earning was not included in Total Earning.
                'StrQ &= xDateJoinQryCF
                StrQ &= xDateJoinQry
                'Sohail (10 Jul 2019) -- End
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            'Sohail (10 Jul 2019) -- Start
            'EFC ZAMBIA - Support Issue Id # 3964 - 76.1 - Claim Request Earning was not included in Total Earning.
            'StrQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
            '              "AND prtnaleave_tran.isvoid = 0 " & _
            '              "AND prtranhead_master.isvoid = 0 " & _
            '              "AND trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '              "AND payperiodunkid = @payperiodunkid "
            StrQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                         "AND prtnaleave_tran.isvoid = 0 " & _
                         "AND prtranhead_master.isvoid = 0 " & _
                         "AND prpayrollprocess_tran.tranheadunkid > 0 " & _
                         "AND prtranhead_master.istaxable = 1 " & _
                         "/*AND prpayrollprocess_tran.add_deduct = 1*/ " & _
                         "AND payperiodunkid = @payperiodunkid "
            'Sohail (17 Dec 2021) - [AND prtranhead_master.*, /*AND prpayrollprocess_tran.add_deduct = 1*/]
            'Sohail (10 Jul 2019) -- End

            'Sohail (17 Dec 2021) -- Start
            'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
            StrQ &= "AND prpayrollprocess_tran.employeeunkid IN ( " & _
                    "SELECT DISTINCT prpayrollprocess_tran.employeeunkid " & _
                          "FROM prpayrollprocess_tran " & _
                          "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                         "AND prtnaleave_tran.isvoid = 0 " & _
                         "AND prpayrollprocess_tran.tranheadunkid = @SkillDevHeadUnkid " & _
                         "AND payperiodunkid = @payperiodunkid " & _
                         "AND prpayrollprocess_tran.amount > 0 " & _
                    " ) "
            'Sohail (17 Jan 2022) - [AND prpayrollprocess_tran.amount > 0] (pick the taxable earning total only if the Skills Dev. Levy head assigned to that employee has amount >0 not when the Skills Levy head has zero amount.)
            'Sohail (17 Dec 2021) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'If blnOnlyApproved = False Then
            If xDateFilterQryCF.Trim.Length > 0 Then
                'Sohail (10 Jul 2019) -- Start
                'EFC ZAMBIA - Support Issue Id # 3964 - 76.1 - Claim Request Earning was not included in Total Earning.
                'StrQ &= xDateFilterQryCF
                StrQ &= xDateFilterQry
                'Sohail (10 Jul 2019) -- End
            End If
            'End If

            StrQ &= ") AS B "

            StrQ &= "DROP TABLE #Terminated "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartPeriodDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndPeriodDate))
            objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkid)
            objDataOperation.AddParameter("@SkillDevHeadUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillDevHeadUnkid) 'Sohail (17 Dec 2021)

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sohail (14 May 2019) -- End

            objRpt = New ArutiReport.Designer.rpt_ITF_P16

            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtGross", Format(decTaxableEarning, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtChargeable", Format(decChargeableEarning, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtTaxCredit", Format(decPaye, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtTaxDeducted", Format(decPaye, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtTaxAdjusted", Format(decTaxadjusted, GUI.fmtCurrency))

            'Sohail (17 Dec 2021) -- Start
            'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
            ReportFunction.TextChange(objRpt, "txtPoint11", Format(decPaye, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtPoint1_13", Format(decTaxadjusted, GUI.fmtCurrency))
            ReportFunction.TextChange(objRpt, "txtPoint12", Format(decPaye - decTaxadjusted, GUI.fmtCurrency))
            'Sohail (17 Dec 2021) -- End
            ReportFunction.TextChange(objRpt, "txtPoint10", Format(decChargeableEarning, GUI.fmtCurrency)) 'Sohail (29 Dec 2021)

            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
            If dsList.Tables(0).Rows.Count > 0 Then
                ReportFunction.TextChange(objRpt, "txtPoint1_15", Format(CDec(dsList.Tables(0).Rows(0).Item("TotalEarning")), GUI.fmtCurrency))
                If mintSkillDevHeadUnkid <= 0 Then 'Sohail (27 Jul 2019)
                    'Sohail (17 Dec 2021) -- Start
                    'Enhancement : OLD-532 : Zambia - Changes to PAYE Monthly Return Export and E-Tax Remittance.
                    'ReportFunction.TextChange(objRpt, "txtPoint1_16", Format(CDec(dsList.Tables(0).Rows(0).Item("TotalEarning")) * 0.005, GUI.fmtCurrency))
                    ReportFunction.TextChange(objRpt, "txtPoint1_16", Format(0, GUI.fmtCurrency))
                    'Sohail (17 Dec 2021) -- End
                    'Sohail (27 Jul 2019) -- Start
                    'EFC ZAMBIA Issue # 0004010 - 76.1 - Skills Development amounts not tallying on various payroll Reports. (Provide optional mapping for skill development levy.)
                Else
                    StrQ = "SELECT ISNULL(SUM(CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))), 0) AS SkillDevLevy " & _
                          "FROM prpayrollprocess_tran " & _
                          "LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                          "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayrollprocess_tran.employeeunkid "
                    'Sohail (27 Jun 2022) - [SUM(CAST = ISNULL(SUM(CAST]

                    StrQ &= mstrAnalysis_Join

                    If xDateJoinQryCF.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If

                    StrQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                         "AND prtnaleave_tran.isvoid = 0 " & _
                         "AND prpayrollprocess_tran.tranheadunkid = @SkillDevHeadUnkid " & _
                         "AND payperiodunkid = @payperiodunkid "

                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If

                    If xDateFilterQryCF.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@payperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkid)
                    objDataOperation.AddParameter("@SkillDevHeadUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSkillDevHeadUnkid)

                    Dim dsSkill As DataSet = objDataOperation.ExecQuery(StrQ, "DataTable")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsSkill.Tables(0).Rows.Count > 0 Then
                        ReportFunction.TextChange(objRpt, "txtPoint1_16", Format(CDec(dsSkill.Tables(0).Rows(0).Item("SkillDevLevy")), GUI.fmtCurrency))
                    Else
                        ReportFunction.TextChange(objRpt, "txtPoint1_16", Format(0, GUI.fmtCurrency))
                    End If

                End If
                'Sohail (27 Jul 2019) -- End

                ReportFunction.TextChange(objRpt, "txtPoint13_1", Format(CInt(dsList.Tables(0).Rows(0).Item("TotalEmployeeBF")), "###,###,##0"))
                ReportFunction.TextChange(objRpt, "txtPoint13_2", Format(CInt(dsList.Tables(0).Rows(0).Item("TotalAppointed")) + CInt(dsList.Tables(0).Rows(0).Item("TotalRehired")), "###,###,##0"))
                ReportFunction.TextChange(objRpt, "txtPoint13_3", Format(CInt(dsList.Tables(0).Rows(0).Item("TotalTerminated")), "###,###,##0"))
                ReportFunction.TextChange(objRpt, "txtPoint13_4", Format(CInt(dsList.Tables(0).Rows(0).Item("TotalEmployeeCF")), "###,###,##0"))
            End If
            'Sohail (14 May 2019) -- End

            'If dsList.Tables(0).Rows.Count > 0 Then
            '    ReportFunction.TextChange(objRpt, "txtPoint10", Format(CDec(dsList.Tables(0).Compute("SUM(TotalPay)", "")), GUI.fmtCurrency))
            '    ReportFunction.TextChange(objRpt, "txtPoint11", Format(CDec(dsList.Tables(0).Compute("SUM(taxdue)", "")), GUI.fmtCurrency))
            '    ReportFunction.TextChange(objRpt, "txtPoint12", Format(CDec(dsList.Tables(0).Compute("SUM(taxdue)", "")), GUI.fmtCurrency))
            'End If

            'Dim iBeginingMonth, iEmployedInMonth, iTerminateInMonth, iTotalActive As Integer
            'iBeginingMonth = 0 : iEmployedInMonth = 0 : iTerminateInMonth = 0 : iTotalActive = 0

            'StrQ = "SELECT TOP 1 CONVERT(CHAR(8),start_date,112) AS sDate ,CONVERT(CHAR(8),end_date,112) AS eDate " & _
            '       " FROM cfcommon_period_tran WHERE CONVERT(CHAR(8),end_date,112) < @end_date AND isactive = 1 ORDER BY periodunkid DESC"

            'dsList = objDataOperation.ExecQuery(StrQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If dsList.Tables(0).Rows.Count > 0 Then
            '    objDataOperation.AddParameter("@sDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, dsList.Tables(0).Rows(0).Item("sDate"))
            '    objDataOperation.AddParameter("@eDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, dsList.Tables(0).Rows(0).Item("eDate"))

            '    Dim xDateJoinQry, xDateFilterQry As String
            '    xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
            '    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("sDate")), eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("eDate")), , , strDatabaseName)
            '    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("eDate")), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            '    StrQ = "SELECT " & _
            '           "	COUNT(hremployee_master.employeeunkid) AS B_Month " & _
            '           "FROM hremployee_master "
            '    StrQ &= mstrAnalysis_Join

            '    If xDateJoinQry.Trim.Length > 0 Then
            '        StrQ &= xDateJoinQry
            '    End If

            '    If xUACQry.Trim.Length > 0 Then
            '        StrQ &= xUACQry
            '    End If

            '    StrQ &= " WHERE 1 = 1 "

            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        StrQ &= " AND " & xUACFiltrQry & " "
            '    End If

            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry & " "
            '    End If



            '    dsList = objDataOperation.ExecQuery(StrQ, "List")

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            '    If dsList.Tables(0).Rows.Count > 0 Then
            '        iBeginingMonth = dsList.Tables(0).Rows(0).Item("B_Month")
            '    End If

            '    xUACQry = "" : xUACFiltrQry = ""
            '    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEndPeriodDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)

            '    StrQ = "SELECT " & _
            '           "   COUNT(hremployee_master.employeeunkid) AS E_Month " & _
            '           "FROM hremployee_master "
            '    StrQ &= mstrAnalysis_Join

            '    'S.SANDEEP [04 JUN 2015] -- START
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    If xUACQry.Trim.Length > 0 Then
            '        StrQ &= xUACQry
            '    End If
            '    'S.SANDEEP [04 JUN 2015] -- END

            '    StrQ &= "WHERE MONTH(appointeddate) = MONTH(@end_date) " & _
            '            "AND YEAR(appointeddate) = YEAR(@end_date) "


            '    'S.SANDEEP [04 JUN 2015] -- START
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            '    ''Pinkal (15-Oct-2014) -- Start
            '    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

            '    'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    '    StrQ &= UserAccessLevel._AccessLevelFilterString
            '    'End If
            '    ''Pinkal (15-Oct-2014) -- End

            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        StrQ &= " AND " & xUACFiltrQry & " "
            '    End If

            '    'S.SANDEEP [04 JUN 2015] -- END

            '    dsList = objDataOperation.ExecQuery(StrQ, "List")

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            '    If dsList.Tables(0).Rows.Count > 0 Then
            '        iEmployedInMonth = dsList.Tables(0).Rows(0).Item("E_Month")
            '    End If

            '    'S.SANDEEP [04 JUN 2015] -- START
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'StrQ = "SELECT " & _
            '    '       "    SUM(T_Month) AS T_Month " & _
            '    '       "FROM " & _
            '    '       "( " & _
            '    '       "    SELECT " & _
            '    '       "        COUNT(employeeunkid) AS T_Month " & _
            '    '       "    FROM hremployee_master "
            '    'StrQ &= mstrAnalysis_Join
            '    'StrQ &= "    WHERE MONTH(termination_from_date) = MONTH(@end_date) " & _
            '    '        "        AND YEAR(termination_from_date) = YEAR(@end_date) "

            '    ''Pinkal (15-Oct-2014) -- Start
            '    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

            '    'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    '    StrQ &= UserAccessLevel._AccessLevelFilterString
            '    'End If
            '    ''Pinkal (15-Oct-2014) -- End

            '    StrQ = "SELECT " & _
            '           "    SUM(T_Month) AS T_Month " & _
            '           "FROM " & _
            '           "( " & _
            '           "    SELECT " & _
            '           "        COUNT(hremployee_master.employeeunkid) AS T_Month " & _
            '           "    FROM hremployee_master " & _
            '           "    JOIN " & _
            '           "    ( " & _
            '           "        SELECT " & _
            '           "             employeeunkid " & _
            '           "            ,date2 AS termination_from_date " & _
            '           "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '           "        FROM hremployee_dates_tran " & _
            '           "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndPeriodDate) & "' AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' " & _
            '           "    ) AS TFD ON TFD.employeeunkid = hremployee_master.employeeunkid AND TFD.rno = 1 "

            '    StrQ &= mstrAnalysis_Join

            '    If xUACQry.Trim.Length > 0 Then
            '        StrQ &= xUACQry
            '    End If

            '    StrQ &= "    WHERE MONTH(TFD.termination_from_date) = MONTH(@end_date) " & _
            '            "        AND YEAR(TFD.termination_from_date) = YEAR(@end_date) "

            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        StrQ &= " AND " & xUACFiltrQry & " "
            '    End If
            '    'S.SANDEEP [04 JUN 2015] -- END



            '    'S.SANDEEP [04 JUN 2015] -- START
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'StrQ &= "UNION ALL " & _
            '    '        "    SELECT " & _
            '    '        "        COUNT(employeeunkid) AS T_Month " & _
            '    '        "    FROM hremployee_master "
            '    'StrQ &= mstrAnalysis_Join
            '    'StrQ &= "    WHERE MONTH(empl_enddate) = MONTH(@end_date) " & _
            '    '        "        AND YEAR(empl_enddate) = YEAR(@end_date) "

            '    ''Pinkal (15-Oct-2014) -- Start
            '    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

            '    'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    '    StrQ &= UserAccessLevel._AccessLevelFilterString
            '    'End If
            '    ''Pinkal (15-Oct-2014) -- End

            '    StrQ &= "UNION ALL " & _
            '           "    SELECT " & _
            '           "        COUNT(hremployee_master.employeeunkid) AS T_Month " & _
            '           "    FROM hremployee_master " & _
            '           "    JOIN " & _
            '           "    ( " & _
            '           "        SELECT " & _
            '           "             employeeunkid " & _
            '           "            ,date1 AS empl_enddate " & _
            '           "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '           "        FROM hremployee_dates_tran " & _
            '           "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & mdtEndPeriodDate & "' AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' " & _
            '           "    ) AS EED ON EED.employeeunkid = hremployee_master.employeeunkid AND EED.rno = 1 "

            '    StrQ &= mstrAnalysis_Join

            '    If xUACQry.Trim.Length > 0 Then
            '        StrQ &= xUACQry
            '    End If

            '    StrQ &= "    WHERE MONTH(EED.empl_enddate) = MONTH(@end_date) " & _
            '            "        AND YEAR(EED.empl_enddate) = YEAR(@end_date) "

            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        StrQ &= " AND " & xUACFiltrQry & " "
            '    End If

            '    'S.SANDEEP [04 JUN 2015] -- END



            '    'S.SANDEEP [04 JUN 2015] -- START
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'StrQ &= "UNION ALL " & _
            '    '        "    SELECT " & _
            '    '        "        COUNT(employeeunkid) AS T_Month " & _
            '    '        "    FROM hremployee_master "
            '    'StrQ &= mstrAnalysis_Join
            '    'StrQ &= "    WHERE MONTH(termination_to_date) = MONTH(@end_date) " & _
            '    '        "        AND YEAR(termination_to_date) = YEAR(@end_date) "

            '    ''Pinkal (15-Oct-2014) -- Start
            '    ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

            '    'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    '    StrQ &= UserAccessLevel._AccessLevelFilterString
            '    'End If
            '    ''Pinkal (15-Oct-2014) -- End


            '    StrQ &= "UNION ALL " & _
            '            "    SELECT " & _
            '            "        COUNT(hremployee_master.employeeunkid) AS T_Month " & _
            '            "    FROM hremployee_master " & _
            '            "    JOIN " & _
            '            "    ( " & _
            '            "        SELECT " & _
            '            "             employeeunkid " & _
            '            "            ,date1 AS termination_to_date " & _
            '            "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '            "        FROM hremployee_dates_tran " & _
            '            "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & mdtEndPeriodDate & "' AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' " & _
            '            "    ) AS TTD ON TTD.employeeunkid = hremployee_master.employeeunkid AND TTD.rno = 1 "
            '    StrQ &= mstrAnalysis_Join

            '    If xUACQry.Trim.Length > 0 Then
            '        StrQ &= xUACQry
            '    End If

            '    StrQ &= "    WHERE MONTH(TTD.termination_to_date) = MONTH(@end_date) " & _
            '            "        AND YEAR(TTD.termination_to_date) = YEAR(@end_date) "

            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        StrQ &= " AND " & xUACFiltrQry & " "
            '    End If

            '    'S.SANDEEP [04 JUN 2015] -- END


            '    StrQ &= ") AS SAP "

            '    dsList = objDataOperation.ExecQuery(StrQ, "List")

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            '    If dsList.Tables(0).Rows.Count > 0 Then
            '        iTerminateInMonth = dsList.Tables(0).Rows(0).Item("T_Month")
            '    End If

            '    iTotalActive = (iBeginingMonth + iEmployedInMonth) - iTerminateInMonth

            'ReportFunction.TextChange(objRpt, "txtPoint13_1", iBeginingMonth)
            'ReportFunction.TextChange(objRpt, "txtPoint13_2", iEmployedInMonth)
            'ReportFunction.TextChange(objRpt, "txtPoint13_3", iTerminateInMonth)
            'ReportFunction.TextChange(objRpt, "txtPoint13_4", iTotalActive)
            'End If

            Dim dsPerm As New DataSet : Dim dsTemp As New DataSet



            'StrQ = "SELECT " & _
            '       "	 SUM(ISNULL(TotalPay,0)) AS TotalPay " & _
            '       "	,empid,pid " & _
            '       "FROM " & _
            '       "( " & _
            '       "	SELECT DISTINCT " & _
            '       "		 CAST(ISNULL(amount,0) AS DECIMAL(36, 2 )) AS TotalPay " & _
            '       "		,vwPayroll.employeeunkid AS empid " & _
            '       "		,payperiodunkid AS pid " & _
            '       "	FROM vwPayroll " & _
            '       "        LEFT JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '       "		LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'StrQ &= mstrAnalysis_Join


            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'StrQ &= "	WHERE vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " AND prtranhead_master.istaxable = 1 AND payrollprocesstranunkid IS NOT NULL " & _
            '        "	AND vwPayroll.employeeunkid NOT IN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "           A.employeeunkid " & _
            '        "       FROM " & _
            '        "       ( " & _
            '        "           SELECT " & _
            '        "                employeeunkid " & _
            '        "               ,date1 " & _
            '        "               ,date2 " & _
            '        "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '        "           FROM hremployee_dates_tran " & _
            '        "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndPeriodDate) & "' " & _
            '        "           AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' " & _
            '        "       ) AS A WHERE A.rno = 1 AND CONVERT(CHAR(8),A.date1)<= @end_date AND CONVERT(CHAR(8),A.date2) >= @end_date " & _
            '        "   ) " & _
            '        "	AND payperiodunkid = '" & mintPeriodUnkid & "' "

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry & " "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- END



            'StrQ &= ") AS tmp WHERE 1 = 1 " & _
            '        "GROUP BY empid,pid "

            'dsPerm = objDataOperation.ExecQuery(StrQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'Dim dRtmp() As DataRow = Nothing

            'For Each dRow As DataRow In dsPerm.Tables("List").Rows
            '    dRtmp = rpt_Data.Tables(0).Select(dRow.Item("TotalPay") & ">= Column70 AND " & dRow.Item("TotalPay") & "<= Column71")
            '    If dRtmp.Length > 0 Then
            '        If dRtmp(0).Item("Column4").ToString = "" Then
            '            dRtmp(0).Item("Column4") = 1
            '        Else
            '            dRtmp(0).Item("Column4") = CInt(dRtmp(0).Item("Column4")) + 1
            '        End If
            '        dRtmp(0).Item("Column81") = dRtmp(0).Item("Column4")
            '        If dRtmp(0).Item("Column5").ToString = "" Then
            '            dRtmp(0).Item("Column5") = Format(CDec(dRow.Item("TotalPay")), GUI.fmtCurrency)
            '        Else
            '            dRtmp(0).Item("Column5") = Format(CDec(CDec(dRtmp(0).Item("Column5")) + CDec(dRow.Item("TotalPay"))), GUI.fmtCurrency)
            '        End If
            '        dRtmp(0).Item("Column82") = dRtmp(0).Item("Column5")
            '    End If
            'Next

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''StrQ = "SELECT " & _
            ''       "	 SUM(ISNULL(TotalPay,0)) AS TotalPay " & _
            ''       "	,empid,pid " & _
            ''       "FROM " & _
            ''       "( " & _
            ''       "    SELECT DISTINCT " & _
            ''       "         CAST(ISNULL(amount,0) AS DECIMAL(36," & decDecimalPlaces & ")) AS TotalPay " & _
            ''       "        ,vwPayroll.employeeunkid AS empid " & _
            ''       "        ,payperiodunkid AS pid " & _
            ''       "    FROM vwPayroll " & _
            ''       "        LEFT JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
            ''       "        LEFT JOIN athremployee_master ON vwPayroll.employeeunkid = athremployee_master.employeeunkid "
            ''StrQ &= mstrAnalysis_Join.Replace("hremployee_master", "athremployee_master")
            ''StrQ &= "    WHERE vwPayroll.trnheadtype_id =  1 AND prtranhead_master.istaxable = 1 AND payrollprocesstranunkid IS NOT NULL " & _
            ''        "        AND CONVERT(CHAR(8),probation_from_date,112)<= @end_date " & _
            ''        "        AND CONVERT(CHAR(8),probation_to_date,112)>=@end_date " & _
            ''        "        AND payperiodunkid = '" & mintPeriodUnkid & "' "

            ' ''Pinkal (15-Oct-2014) -- Start
            ' ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "athremployee_master")
            ''End If
            ' ''Pinkal (15-Oct-2014) -- End

            'StrQ = "SELECT " & _
            '       "	 SUM(ISNULL(TotalPay,0)) AS TotalPay " & _
            '       "	,empid,pid " & _
            '       "FROM " & _
            '       "( " & _
            '       "    SELECT DISTINCT " & _
            '       "         CAST(ISNULL(amount,0) AS DECIMAL(36," & decDecimalPlaces & ")) AS TotalPay " & _
            '       "        ,vwPayroll.employeeunkid AS empid " & _
            '       "        ,payperiodunkid AS pid " & _
            '       "    FROM vwPayroll " & _
            '       "        LEFT JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '       "        LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
            '       "        LEFT JOIN " & _
            '       "        ( " & _
            '       "            SELECT " & _
            '       "                 employeeunkid " & _
            '       "                ,date1 " & _
            '       "                ,date2 " & _
            '       "                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '       "            FROM hremployee_dates_tran " & _
            '       "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndPeriodDate) & "' " & _
            '       "            AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' " & _
            '       "        ) AS PR ON PR.employeeunkid = hremployee_master.employeeunkid AND PR.rno = 1 "

            'StrQ &= mstrAnalysis_Join

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'StrQ &= "    WHERE vwPayroll.trnheadtype_id =  1 AND prtranhead_master.istaxable = 1 AND payrollprocesstranunkid IS NOT NULL " & _
            '        "        AND CONVERT(CHAR(8),PR.date1,112)<= @end_date " & _
            '        "        AND CONVERT(CHAR(8),PR.date2,112)>=@end_date " & _
            '        "        AND payperiodunkid = '" & mintPeriodUnkid & "' "

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry & " "
            'End If

            ''S.SANDEEP [04 JUN 2015] -- END



            'StrQ &= ")AS tmp WHERE 1 = 1 " & _
            '        "GROUP BY empid,pid "

            'dsTemp = objDataOperation.ExecQuery(StrQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'For Each dRow As DataRow In dsTemp.Tables("List").Rows
            '    dRtmp = rpt_Data.Tables(0).Select(dRow.Item("TotalPay") & ">= Column70 AND " & dRow.Item("TotalPay") & "<= Column71")
            '    If dRtmp.Length > 0 Then
            '        If dRtmp(0).Item("Column6").ToString = "" Then
            '            dRtmp(0).Item("Column6") = 1
            '        Else
            '            dRtmp(0).Item("Column6") = CInt(dRtmp(0).Item("Column6")) + 1
            '        End If
            '        dRtmp(0).Item("Column83") = dRtmp(0).Item("Column6")
            '        If dRtmp(0).Item("Column7").ToString = "" Then
            '            dRtmp(0).Item("Column7") = Format(CDec(dRow.Item("TotalPay")), GUI.fmtCurrency)
            '        Else
            '            dRtmp(0).Item("Column7") = Format(CDec(CDec(dRtmp(0).Item("Column7")) + CDec(dRow.Item("TotalPay"))), GUI.fmtCurrency)
            '        End If
            '        dRtmp(0).Item("Column84") = dRtmp(0).Item("Column7")
            '    End If
            'Next

            ReportFunction.TextChange(objRpt, "lblTopCaption", Language.getMessage(mstrModuleName, 3, "CF/P16 V.1"))
            ReportFunction.TextChange(objRpt, "lblTitle1", Language.getMessage(mstrModuleName, 4, "PAYE RETURN FORM"))
            ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 5, "Month/Quarter/Half Year Ended on"))
            ReportFunction.TextChange(objRpt, "lblChargeYr", Language.getMessage(mstrModuleName, 6, "CHARGE YEAR"))
            ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 7, "TAXPAYER DETAILS (Please notify the Tax Office if there has been any change in details under 4 to 7)"))
            ReportFunction.TextChange(objRpt, "lblEmplrName", Language.getMessage(mstrModuleName, 8, "NAME OF EMPLOYER*"))
            ReportFunction.TextChange(objRpt, "lblTINNo", Language.getMessage(mstrModuleName, 9, "TAXPAYER  IDENTIFICATION NUMBER (TPIN)"))
            ReportFunction.TextChange(objRpt, "lblAccountNo", Language.getMessage(mstrModuleName, 10, "PAYE  ACCOUNT NUMBER"))
            ReportFunction.TextChange(objRpt, "lblPostalAddress", Language.getMessage(mstrModuleName, 11, "POSTAL ADDRESS"))
            ReportFunction.TextChange(objRpt, "lblPhysicalAddress", Language.getMessage(mstrModuleName, 12, "PHYSICAL ADDRESS"))
            ReportFunction.TextChange(objRpt, "lblEmail", Language.getMessage(mstrModuleName, 13, "EMAIL ADDRESS"))
            ReportFunction.TextChange(objRpt, "lblTelephone", Language.getMessage(mstrModuleName, 14, "TELEPHONE NUMBER"))

            ReportFunction.TextChange(objRpt, "lblCaption3", Language.getMessage(mstrModuleName, 18, "Note*: Row 1 should indicate name of payer where the payment is made by a person whose employer is not obligated by law to deduct PAYE (Direct Payment Cases)."))
            ReportFunction.TextChange(objRpt, "lblPart1", Language.getMessage(mstrModuleName, 19, "PART I"))
            ReportFunction.TextChange(objRpt, "lblPoint10", Language.getMessage(mstrModuleName, 20, "Chargeable Emoluments (including salaries, wages, fees, commissions, bonuses, overtime, gratuity, etc.) (Sum of column E to part II)")) 'Anjan (20 Sep 2017)
            ReportFunction.TextChange(objRpt, "lblPoint11", Language.getMessage(mstrModuleName, 21, "Tax deducted (total tax deducted from the emoluments) (Sum of column G to part II)")) 'Anjan (20 Sep 2017)
            ReportFunction.TextChange(objRpt, "lblPoint12", Language.getMessage(mstrModuleName, 22, "Tax Payable/(Repayable) (2-3)")) 'Anjan (20 Sep 2017)
            ReportFunction.TextChange(objRpt, "lblPoint13", Language.getMessage(mstrModuleName, 23, "OTHER INFORMATION"))
            ReportFunction.TextChange(objRpt, "lblPoint13_1", Language.getMessage(mstrModuleName, 24, "Number of employees at the beginning of the month"))
            ReportFunction.TextChange(objRpt, "lblPoint13_2", Language.getMessage(mstrModuleName, 25, "Number of new employees employed in the month"))
            ReportFunction.TextChange(objRpt, "lblPoint13_3", Language.getMessage(mstrModuleName, 26, "Number of employees that have separated in the month"))
            ReportFunction.TextChange(objRpt, "lblPoint13_4", Language.getMessage(mstrModuleName, 27, "Number of employees at the end of the month"))
            ReportFunction.TextChange(objRpt, "lblPoint14", Language.getMessage(mstrModuleName, 28, "ZAMBIA DEVELOPMENT AGENCY APPROVED INVESTMENTS (ONLY APPLICABLE TO THOSE WITH APPROVED ZDA INCENTIVES):"))
            ReportFunction.TextChange(objRpt, "lblLicenseNo", Language.getMessage(mstrModuleName, 29, "Licence Number:"))
            ReportFunction.TextChange(objRpt, "lblPoint14_1", Language.getMessage(mstrModuleName, 30, "Total number of employees pledged to be employed in the year"))
            ReportFunction.TextChange(objRpt, "lblPoint14_2", Language.getMessage(mstrModuleName, 31, "Number of employees employed prior to this month"))
            ReportFunction.TextChange(objRpt, "lblPoint14_3", Language.getMessage(mstrModuleName, 32, "Number of new employees employed in the month"))
            ReportFunction.TextChange(objRpt, "lblPoint14_4", Language.getMessage(mstrModuleName, 33, "Number of employees that have separated in the month")) 'Anjan (20 Sep 2017)
            ReportFunction.TextChange(objRpt, "lblPoint14_5", Language.getMessage(mstrModuleName, 34, "Number of employees at the end of the month"))
            'Anjan (20 Sep 2017) --Start
            ReportFunction.TextChange(objRpt, "lblOriginal", Language.getMessage(mstrModuleName, 63, "Original"))
            ReportFunction.TextChange(objRpt, "lblAmended", Language.getMessage(mstrModuleName, 64, "Amended"))
            ReportFunction.TextChange(objRpt, "lblSupplementary", Language.getMessage(mstrModuleName, 65, "Supplementary"))
            ReportFunction.TextChange(objRpt, "lblNote", Language.getMessage(mstrModuleName, 66, "If Amended,Amendment Approval Number"))
            ReportFunction.TextChange(objRpt, "lblPoint1_13", Language.getMessage(mstrModuleName, 67, "Tax adjusted  (total tax adjusted from the emoluments) (Sum of column H to part II)"))
            ReportFunction.TextChange(objRpt, "lblPoint1_15", Language.getMessage(mstrModuleName, 68, "Skills Development Levy (Total Gross Emoluments for the period) (Does not allow to exempt Institutions)"))
            ReportFunction.TextChange(objRpt, "lblPoint1_16", Language.getMessage(mstrModuleName, 69, "Skills Development Levy (0.5% of Total Gross Emoluments for the period)"))

            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCaption2", Language.getMessage(mstrModuleName, 15, "FOR OFFICIAL USE ONLY"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblOffSign", Language.getMessage(mstrModuleName, 16, "(OFFICER'S NAME)")) 'Anjan (20 Sep 2017)
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblDateReceived", Language.getMessage(mstrModuleName, 17, "(DATE RECEIVED)"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblOffSign", Language.getMessage(mstrModuleName, 70, "Officer's Signature"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblStamp", Language.getMessage(mstrModuleName, 71, "Receiving Office Date Stamp"))

            'Anjan (20 Sep 2017) --End

            'Sohail (14 May 2019) -- Start
            'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes. (Hide Tax Adjusted column if Tax Adjusted is not selected)
            If mintTaxAdjustedHeadUnkid <= 0 Then
                ReportFunction.EnableSuppress(objRpt.Subreports("rpt_ITF_Summary"), "lblTaxAdjusted", True)
                ReportFunction.EnableSuppress(objRpt.Subreports("rpt_ITF_Summary"), "Column91", True)
                ReportFunction.EnableSuppress(objRpt.Subreports("rpt_ITF_Summary"), "txtTaxAdjusted", True)
                ReportFunction.EnableSuppress(objRpt.Subreports("rpt_ITF_Summary"), "Line13", True)
                ReportFunction.EnableSuppress(objRpt.Subreports("rpt_ITF_Summary"), "Line2", True)
                ReportFunction.EnableSuppress(objRpt.Subreports("rpt_ITF_Summary"), "Line30", True)
                ReportFunction.SetColumnOn(objRpt.Subreports("rpt_ITF_Summary"), "lblTaxDeducted", "lblTaxDeducted", 2422)
                ReportFunction.SetColumnOn(objRpt.Subreports("rpt_ITF_Summary"), "Column82", "Column82", 2422)
                ReportFunction.SetColumnOn(objRpt.Subreports("rpt_ITF_Summary"), "txtTaxDeducted", "txtTaxDeducted", 2422)
            End If
            'Sohail (14 May 2019) -- End

            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblPart2", Language.getMessage(mstrModuleName, 35, "PART II"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblNRCofEmployee", Language.getMessage(mstrModuleName, 36, "NRC of Employee"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblNameofEmployee", Language.getMessage(mstrModuleName, 37, "Name of Employee"))
            'Hemant (22 Dec 2022) -- Start
            'ENHANCEMENT(Zambia) : A1X-443 - PAYE MONTHLY RETURN - ITF P16 report enhancement to include TPIN
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblTPIN", Language.getMessage(mstrModuleName, 73, "TPIN"))
            'Hemant (22 Dec 2022) -- End	
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblEmployment", Language.getMessage(mstrModuleName, 38, "Nature of Employment Temporal/Permanent"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblGross", Language.getMessage(mstrModuleName, 39, "Gross emoluments for the tax period"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblChargeable", Language.getMessage(mstrModuleName, 40, "Chargeable emoluments for the tax period"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblTaxCredit", Language.getMessage(mstrModuleName, 41, "Total tax credit for the tax period"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblTaxDeducted", Language.getMessage(mstrModuleName, 42, "Tax Deducted as  shown on the TRD for the year"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblTaxAdjusted", Language.getMessage(mstrModuleName, 43, "Tax Adjusted"))
            'ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblLocalGov", Language.getMessage(mstrModuleName, 44, "Local Government"))
            'ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblPubLC", Language.getMessage(mstrModuleName, 45, "Public Limited Body"))
            'ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblPubBO", Language.getMessage(mstrModuleName, 46, "Church/Public Benefit Organisation"))
            'ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblIndividual", Language.getMessage(mstrModuleName, 47, "Individual"))
            'ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblParastatal", Language.getMessage(mstrModuleName, 48, "Parastatal"))
            'ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblOther", Language.getMessage(mstrModuleName, 49, "Other (specify)"))
            'ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCaption3", Language.getMessage(mstrModuleName, 50, "3. Distribution of Employees by Income Bracket"))
            'ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblGrossIncome", Language.getMessage(mstrModuleName, 51, "Gross Income per Month"))
            'ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblTotPrmEmp", Language.getMessage(mstrModuleName, 52, "Number of Permanent Employees in the bracket"))
            'ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblPrmGrossIncome", Language.getMessage(mstrModuleName, 53, "Gross Income within bracket"))
            'ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblTotTepEmp", Language.getMessage(mstrModuleName, 54, "Number of Temporary Employees in the bracket"))
            'ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblTotTmpGrossIncome", Language.getMessage(mstrModuleName, 53, "Gross Income within bracket"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblGrandTotal", Language.getMessage(mstrModuleName, 55, "Total"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCaption3_txt", Language.getMessage(mstrModuleName, 56, "Note: Gross income (in Part II, 3.0)  must include ALL TAXABLE INCOME including allowances such as housing allowance, transport allowance, overtime pay, etc."))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCaption4", Language.getMessage(mstrModuleName, 57, "4. DECLARATION"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCaption4_txt", Language.getMessage(mstrModuleName, 58, "I declare that the details in this Return are true and correct."))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblName", Language.getMessage(mstrModuleName, 59, "Name:"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblSign", Language.getMessage(mstrModuleName, 60, "Signature:"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCapacity", Language.getMessage(mstrModuleName, 61, "Capacity:"))
            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblDate", Language.getMessage(mstrModuleName, 62, "Date:"))

            ReportFunction.TextChange(objRpt, "txtMonthName", mstrPeriodName)
            ReportFunction.TextChange(objRpt, "txtEmplrName", Company._Object._Name)
            ReportFunction.TextChange(objRpt, "txtTINNo", Company._Object._Tinno)
            ReportFunction.TextChange(objRpt, "txtAccountNo", Company._Object._Company_Reg_No)
            ReportFunction.TextChange(objRpt, "txtPostalAddress", Company._Object._Address1 & " " & Company._Object._Address2)
            ReportFunction.TextChange(objRpt, "txtPhysicalAddress", Company._Object._Address1 & " " & Company._Object._Address2)
            ReportFunction.TextChange(objRpt, "txtEmail", Company._Object._Email)
            ReportFunction.TextChange(objRpt, "txtTelehone", Company._Object._Phone1)
            'If rpt_Data.Tables(0).Rows.Count > 0 Then
            '    Dim decCol81, decCol82, decCol83, decCol84 As Decimal : decCol81 = 0 : decCol82 = 0 : decCol83 = 0 : decCol84 = 0
            '    For i As Integer = 0 To rpt_Data.Tables(0).Rows.Count - 1
            '        If IsDBNull(rpt_Data.Tables(0).Rows(i).Item("Column81")) = False Then
            '            decCol81 += CInt(rpt_Data.Tables(0).Rows(i).Item("Column81"))
            '        End If

            '        If IsDBNull(rpt_Data.Tables(0).Rows(i).Item("Column82")) = False Then
            '            decCol82 += CDec(rpt_Data.Tables(0).Rows(i).Item("Column82"))
            '        End If

            '        If IsDBNull(rpt_Data.Tables(0).Rows(i).Item("Column83")) = False Then
            '            decCol83 += CInt(rpt_Data.Tables(0).Rows(i).Item("Column83"))
            '        End If

            '        If IsDBNull(rpt_Data.Tables(0).Rows(i).Item("Column84")) = False Then
            '            decCol84 += CDec(rpt_Data.Tables(0).Rows(i).Item("Column84"))
            '        End If
            '    Next
            '    ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtPEmpTot", decCol81)
            '    ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtTEmpTot", decCol83)
            '    ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtPGross", Format(CDec(decCol82), GUI.fmtCurrency))
            '    ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtTGross", Format(CDec(decCol84), GUI.fmtCurrency))
            'End If
            objRpt.SetDataSource(rpt_Data)
            objRpt.Subreports("rpt_ITF_Summary").SetDataSource(rpt_Data)

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Sohail (14 May 2019) -- Start
    'JSI ZAMBIA - REF # 2647 - 74.1 - NAPSA & PAYE Template changes.
    Private Function SaveTextFile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try

            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb.ToString.Trim)
                .Close()
            End With
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SaveExcelfile; Module Name: " & mstrModuleName)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function
    'Sohail (14 May 2019) -- End

#Region " Report before changes on 19 Sep 2017 (The NAPSA report Need to include Social Security Number report (Membership contribution Number) and the Tax Report to include NRC number,Nature of Employment Temporal/Permanent,NAPSA Compliance,Total Tax Credit for  Tax period, Tax Deducted as shown on TRD for Tax Period and also include all employees in the report) "
    'Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
    '                                       ByVal intUserUnkid As Integer, _
    '                                       ByVal intYearUnkid As Integer, _
    '                                       ByVal intCompanyUnkid As Integer, _
    '                                       ByVal dtPeriodStart As Date, _
    '                                       ByVal dtPeriodEnd As Date, _
    '                                       ByVal strUserModeSetting As String, _
    '                                       ByVal blnOnlyApproved As Boolean, _
    '                                       ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    'S.SANDEEP [04 JUN 2015] -- END
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        Dim objExchangeRate As New clsExchangeRate
    '        Dim decDecimalPlaces As Decimal = 0

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
    '        objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        'Sohail (29 Mar 2017) -- Start
    '        'Issue - 65.2 - Amount not matching with Payroll Report.
    '        'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '        decDecimalPlaces = 6
    '        'Sohail (29 Mar 2017) -- End

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
    '        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEndPeriodDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ = "SELECT " & _
    '               "	 CASE WHEN SrNo < Cnt AND SrNo = 1 THEN CONVERT(NVARCHAR(50), CASE WHEN inexcessof = 0 THEN @Upto ELSE CONVERT(NVARCHAR(50),inexcessof)END) " & _
    '               "		  WHEN SrNo < Cnt THEN CONVERT(NVARCHAR(50), inexcessof + 1) " & _
    '               "		  WHEN SrNo = Cnt THEN @Above END AS Col2 " & _
    '               " 	,CASE WHEN SrNo = Cnt THEN inexcessof + 1 ELSE amountupto END AS Col3 " & _
    '               "	,CASE WHEN SrNo = 1 THEN CASE WHEN inexcessof = 0 THEN '1.00' ELSE inexcessof END ELSE inexcessof + 1 END AS inexcessof " & _
    '               "	,amountupto " & _
    '               "	,SrNo AS RangeNo " & _
    '               "FROM " & _
    '               "( " & _
    '               "	SELECT " & _
    '               "	   ROW_NUMBER() OVER(ORDER BY inexcessof ASC) AS SrNo " & _
    '               "	  ,CONVERT(DECIMAL(36," & decDecimalPlaces & "), inexcessof) AS inexcessof " & _
    '               "	  ,CONVERT(DECIMAL(36," & decDecimalPlaces & "), amountupto) AS amountupto " & _
    '               "	  ,(SELECT " & _
    '               "			COUNT(prtranhead_inexcessslab_tran.tranheadtaxslabtranunkid) " & _
    '               "		FROM prtranhead_inexcessslab_tran " & _
    '               "			LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '               "		WHERE tranheadunkid = @TranHeadId AND isvoid = 0 " & _
    '               "			AND cfcommon_period_tran.end_date = (SELECT MAX(end_date) " & _
    '               "												 FROM prtranhead_inexcessslab_tran " & _
    '               "													LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '               "												 WHERE ISNULL(prtranhead_inexcessslab_tran.isvoid, 0) = 0 " & _
    '               "													AND prtranhead_inexcessslab_tran.tranheadunkid = @TranHeadId " & _
    '               "													AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date) " & _
    '               "		) AS Cnt " & _
    '               "	FROM prtranhead_inexcessslab_tran " & _
    '               "		LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '               "	WHERE tranheadunkid = @TranHeadId AND isvoid = 0 " & _
    '               "	AND cfcommon_period_tran.end_date = (SELECT MAX(end_date) " & _
    '               "										 FROM prtranhead_inexcessslab_tran " & _
    '               "											LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '               "										 WHERE ISNULL(prtranhead_inexcessslab_tran.isvoid, 0) = 0 " & _
    '               "											AND prtranhead_inexcessslab_tran.tranheadunkid = @TranHeadId " & _
    '               "											AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date) " & _
    '               ")AS SLB "

    '        Call FilterTitleAndFilterQuery()

    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        For Each dRow As DataRow In dsList.Tables(0).Rows
    '            Dim rpt_Row As DataRow = rpt_Data.Tables(0).NewRow

    '            rpt_Row.Item("Column1") = FinancialYear._Object._FinancialYear_Name
    '            If Val(dRow.Item("Col2")) > 0 Then
    '                rpt_Row.Item("Column2") = Format(CDec(dRow.Item("Col2")), GUI.fmtCurrency)
    '            Else
    '                rpt_Row.Item("Column2") = dRow.Item("Col2")
    '            End If
    '            rpt_Row.Item("Column3") = Format(CDec(dRow.Item("Col3")), GUI.fmtCurrency)


    '            rpt_Row.Item("Column70") = dRow.Item("inexcessof")
    '            rpt_Row.Item("Column71") = dRow.Item("amountupto")
    '            rpt_Row.Item("Column72") = dRow.Item("RangeNo")

    '            rpt_Data.Tables(0).Rows.Add(rpt_Row)
    '        Next

    '        objRpt = New ArutiReport.Designer.rpt_ITF_P16

    '        StrQ = "SELECT " & _
    '                "	 SUM(ISNULL(PAYE.TotalPay,0)) AS TotalPay " & _
    '                "	,SUM(ISNULL(PAYE.taxpayable, 0)) - SUM(ISNULL(PAYE.relief, 0)) AS TaxDue " & _
    '                "	,SUM(ISNULL(TotalPay,0)) - (SUM(ISNULL(PAYE.taxpayable, 0)) - SUM(ISNULL(PAYE.relief, 0))) AS Total " & _
    '                "	,paye.empid AS empid " & _
    '                "FROM " & _
    '                "( " & _
    '                "		SELECT " & _
    '                "			 CAST(amount AS DECIMAL(36," & decDecimalPlaces & ")) AS TotalPay " & _
    '                "			,0 AS taxpayable " & _
    '                "			,0 AS relief " & _
    '                "			,vwPayroll.employeeunkid AS empid " & _
    '                "			,payperiodunkid AS pid "
    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If
    '        StrQ &= "		FROM vwPayroll " & _
    '                "			LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "
    '        StrQ &= mstrAnalysis_Join

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "           JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
    '                "		WHERE vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
    '                "          AND prtranhead_master.istaxable = 1 "

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "	UNION ALL " & _
    '                "		SELECT " & _
    '                "			 0 AS basicsalary " & _
    '                "			,CAST(amount AS DECIMAL(36," & decDecimalPlaces & ")) AS taxpayable " & _
    '                "			,0 AS relief " & _
    '                "			,vwPayroll.employeeunkid AS empid " & _
    '                "		,payperiodunkid AS pid "
    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If
    '        StrQ &= "		FROM vwPayroll " & _
    '                "			LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "
    '        StrQ &= mstrAnalysis_Join

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "		WHERE typeof_id = " & enTypeOf.Taxes & " "

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "	UNION ALL " & _
    '                "		SELECT " & _
    '                "			 0 AS basicsalary " & _
    '                "			,0 AS taxpayable " & _
    '                "			,CAST(amount AS DECIMAL(36," & decDecimalPlaces & ")) AS relief " & _
    '                "			,vwPayroll.employeeunkid AS empid " & _
    '                "			,payperiodunkid AS pid "
    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If
    '        StrQ &= "		FROM vwPayroll " & _
    '                "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
    '                "		LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "
    '        StrQ &= mstrAnalysis_Join

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If
    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= "		WHERE prtranhead_master.istaxrelief = 1 "

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END

    '        StrQ &= ") AS PAYE " & _
    '                "WHERE   pid IN (" & mintPeriodUnkid & ") " & _
    '                "GROUP BY empid "

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList.Tables(0).Rows.Count > 0 Then
    '            ReportFunction.TextChange(objRpt, "txtPoint10", Format(CDec(dsList.Tables(0).Compute("SUM(TotalPay)", "")), GUI.fmtCurrency))
    '            ReportFunction.TextChange(objRpt, "txtPoint11", Format(CDec(dsList.Tables(0).Compute("SUM(taxdue)", "")), GUI.fmtCurrency))
    '            ReportFunction.TextChange(objRpt, "txtPoint12", Format(CDec(dsList.Tables(0).Compute("SUM(taxdue)", "")), GUI.fmtCurrency))
    '            'ReportFunction.TextChange(objRpt, "txtPoint12", Format(CDec(dsList.Tables(0).Compute("SUM(Total)", "")), GUI.fmtCurrency))
    '        End If

    '        Dim iBeginingMonth, iEmployedInMonth, iTerminateInMonth, iTotalActive As Integer
    '        iBeginingMonth = 0 : iEmployedInMonth = 0 : iTerminateInMonth = 0 : iTotalActive = 0

    '        StrQ = "SELECT TOP 1 CONVERT(CHAR(8),start_date,112) AS sDate ,CONVERT(CHAR(8),end_date,112) AS eDate " & _
    '               " FROM cfcommon_period_tran WHERE CONVERT(CHAR(8),end_date,112) < @end_date AND isactive = 1 ORDER BY periodunkid DESC"

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList.Tables(0).Rows.Count > 0 Then
    '            objDataOperation.AddParameter("@sDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, dsList.Tables(0).Rows(0).Item("sDate"))
    '            objDataOperation.AddParameter("@eDate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, dsList.Tables(0).Rows(0).Item("eDate"))

    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            Dim xDateJoinQry, xDateFilterQry As String
    '            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
    '            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("sDate")), eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("eDate")), , , strDatabaseName)
    '            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("eDate")), blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
    '            'S.SANDEEP [04 JUN 2015] -- END

    '            StrQ = "SELECT " & _
    '                   "	COUNT(hremployee_master.employeeunkid) AS B_Month " & _
    '                   "FROM hremployee_master "
    '            StrQ &= mstrAnalysis_Join

    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '            'StrQ &= "WHERE CONVERT(CHAR(8),appointeddate,112) <= @eDate " & _
    '            '        "    AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@sDate) >= @sDate " & _
    '            '        "    AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@sDate) >= @sDate " & _
    '            '        "    AND ISNULL(CONVERT(CHAR(8),empl_enddate,112), @sDate) >= @sDate "

    '            ''Pinkal (15-Oct-2014) -- Start
    '            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'End If
    '            ''Pinkal (15-Oct-2014) -- End

    '            If xDateJoinQry.Trim.Length > 0 Then
    '                StrQ &= xDateJoinQry
    '            End If

    '            If xUACQry.Trim.Length > 0 Then
    '                StrQ &= xUACQry
    '            End If

    '            StrQ &= " WHERE 1 = 1 "

    '            If xUACFiltrQry.Trim.Length > 0 Then
    '                StrQ &= " AND " & xUACFiltrQry & " "
    '            End If

    '            If xDateFilterQry.Trim.Length > 0 Then
    '                StrQ &= xDateFilterQry & " "
    '            End If
    '            'S.SANDEEP [04 JUN 2015] -- END



    '            dsList = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList.Tables(0).Rows.Count > 0 Then
    '                iBeginingMonth = dsList.Tables(0).Rows(0).Item("B_Month")
    '            End If

    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            xUACQry = "" : xUACFiltrQry = ""
    '            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEndPeriodDate, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
    '            'S.SANDEEP [04 JUN 2015] -- END

    '            StrQ = "SELECT " & _
    '                   "   COUNT(hremployee_master.employeeunkid) AS E_Month " & _
    '                   "FROM hremployee_master "
    '            StrQ &= mstrAnalysis_Join

    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            If xUACQry.Trim.Length > 0 Then
    '                StrQ &= xUACQry
    '            End If
    '            'S.SANDEEP [04 JUN 2015] -- END

    '            StrQ &= "WHERE MONTH(appointeddate) = MONTH(@end_date) " & _
    '                    "AND YEAR(appointeddate) = YEAR(@end_date) "


    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '            ''Pinkal (15-Oct-2014) -- Start
    '            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'End If
    '            ''Pinkal (15-Oct-2014) -- End

    '            If xUACFiltrQry.Trim.Length > 0 Then
    '                StrQ &= " AND " & xUACFiltrQry & " "
    '            End If

    '            'S.SANDEEP [04 JUN 2015] -- END

    '            dsList = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList.Tables(0).Rows.Count > 0 Then
    '                iEmployedInMonth = dsList.Tables(0).Rows(0).Item("E_Month")
    '            End If

    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'StrQ = "SELECT " & _
    '            '       "    SUM(T_Month) AS T_Month " & _
    '            '       "FROM " & _
    '            '       "( " & _
    '            '       "    SELECT " & _
    '            '       "        COUNT(employeeunkid) AS T_Month " & _
    '            '       "    FROM hremployee_master "
    '            'StrQ &= mstrAnalysis_Join
    '            'StrQ &= "    WHERE MONTH(termination_from_date) = MONTH(@end_date) " & _
    '            '        "        AND YEAR(termination_from_date) = YEAR(@end_date) "

    '            ''Pinkal (15-Oct-2014) -- Start
    '            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'End If
    '            ''Pinkal (15-Oct-2014) -- End

    '            StrQ = "SELECT " & _
    '                   "    SUM(T_Month) AS T_Month " & _
    '                   "FROM " & _
    '                   "( " & _
    '                   "    SELECT " & _
    '                   "        COUNT(hremployee_master.employeeunkid) AS T_Month " & _
    '                   "    FROM hremployee_master " & _
    '                   "    JOIN " & _
    '                   "    ( " & _
    '                   "        SELECT " & _
    '                   "             employeeunkid " & _
    '                   "            ,date2 AS termination_from_date " & _
    '                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                   "        FROM hremployee_dates_tran " & _
    '                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndPeriodDate) & "' AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' " & _
    '                   "    ) AS TFD ON TFD.employeeunkid = hremployee_master.employeeunkid AND TFD.rno = 1 "

    '            StrQ &= mstrAnalysis_Join

    '            If xUACQry.Trim.Length > 0 Then
    '                StrQ &= xUACQry
    '            End If

    '            StrQ &= "    WHERE MONTH(TFD.termination_from_date) = MONTH(@end_date) " & _
    '                    "        AND YEAR(TFD.termination_from_date) = YEAR(@end_date) "

    '            If xUACFiltrQry.Trim.Length > 0 Then
    '                StrQ &= " AND " & xUACFiltrQry & " "
    '            End If
    '            'S.SANDEEP [04 JUN 2015] -- END



    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'StrQ &= "UNION ALL " & _
    '            '        "    SELECT " & _
    '            '        "        COUNT(employeeunkid) AS T_Month " & _
    '            '        "    FROM hremployee_master "
    '            'StrQ &= mstrAnalysis_Join
    '            'StrQ &= "    WHERE MONTH(empl_enddate) = MONTH(@end_date) " & _
    '            '        "        AND YEAR(empl_enddate) = YEAR(@end_date) "

    '            ''Pinkal (15-Oct-2014) -- Start
    '            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'End If
    '            ''Pinkal (15-Oct-2014) -- End

    '            StrQ &= "UNION ALL " & _
    '                   "    SELECT " & _
    '                   "        COUNT(hremployee_master.employeeunkid) AS T_Month " & _
    '                   "    FROM hremployee_master " & _
    '                   "    JOIN " & _
    '                   "    ( " & _
    '                   "        SELECT " & _
    '                   "             employeeunkid " & _
    '                   "            ,date1 AS empl_enddate " & _
    '                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                   "        FROM hremployee_dates_tran " & _
    '                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & mdtEndPeriodDate & "' AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' " & _
    '                   "    ) AS EED ON EED.employeeunkid = hremployee_master.employeeunkid AND EED.rno = 1 "

    '            StrQ &= mstrAnalysis_Join

    '            If xUACQry.Trim.Length > 0 Then
    '                StrQ &= xUACQry
    '            End If

    '            StrQ &= "    WHERE MONTH(EED.empl_enddate) = MONTH(@end_date) " & _
    '                    "        AND YEAR(EED.empl_enddate) = YEAR(@end_date) "

    '            If xUACFiltrQry.Trim.Length > 0 Then
    '                StrQ &= " AND " & xUACFiltrQry & " "
    '            End If

    '            'S.SANDEEP [04 JUN 2015] -- END



    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'StrQ &= "UNION ALL " & _
    '            '        "    SELECT " & _
    '            '        "        COUNT(employeeunkid) AS T_Month " & _
    '            '        "    FROM hremployee_master "
    '            'StrQ &= mstrAnalysis_Join
    '            'StrQ &= "    WHERE MONTH(termination_to_date) = MONTH(@end_date) " & _
    '            '        "        AND YEAR(termination_to_date) = YEAR(@end_date) "

    '            ''Pinkal (15-Oct-2014) -- Start
    '            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'End If
    '            ''Pinkal (15-Oct-2014) -- End


    '            StrQ &= "UNION ALL " & _
    '                    "    SELECT " & _
    '                    "        COUNT(hremployee_master.employeeunkid) AS T_Month " & _
    '                    "    FROM hremployee_master " & _
    '                    "    JOIN " & _
    '                    "    ( " & _
    '                    "        SELECT " & _
    '                    "             employeeunkid " & _
    '                    "            ,date1 AS termination_to_date " & _
    '                    "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                    "        FROM hremployee_dates_tran " & _
    '                    "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & mdtEndPeriodDate & "' AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' " & _
    '                    "    ) AS TTD ON TTD.employeeunkid = hremployee_master.employeeunkid AND TTD.rno = 1 "
    '            StrQ &= mstrAnalysis_Join

    '            If xUACQry.Trim.Length > 0 Then
    '                StrQ &= xUACQry
    '            End If

    '            StrQ &= "    WHERE MONTH(TTD.termination_to_date) = MONTH(@end_date) " & _
    '                    "        AND YEAR(TTD.termination_to_date) = YEAR(@end_date) "

    '            If xUACFiltrQry.Trim.Length > 0 Then
    '                StrQ &= " AND " & xUACFiltrQry & " "
    '            End If

    '            'S.SANDEEP [04 JUN 2015] -- END


    '            StrQ &= ") AS SAP "

    '            dsList = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList.Tables(0).Rows.Count > 0 Then
    '                iTerminateInMonth = dsList.Tables(0).Rows(0).Item("T_Month")
    '            End If

    '            iTotalActive = (iBeginingMonth + iEmployedInMonth) - iTerminateInMonth

    '            ReportFunction.TextChange(objRpt, "txtPoint13_1", iBeginingMonth)
    '            ReportFunction.TextChange(objRpt, "txtPoint13_2", iEmployedInMonth)
    '            ReportFunction.TextChange(objRpt, "txtPoint13_3", iTerminateInMonth)
    '            ReportFunction.TextChange(objRpt, "txtPoint13_4", iTotalActive)
    '        End If

    '        Dim dsPerm As New DataSet : Dim dsTemp As New DataSet

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'StrQ = "SELECT " & _
    '        '       "	 SUM(ISNULL(TotalPay,0)) AS TotalPay " & _
    '        '       "	,empid,pid " & _
    '        '       "FROM " & _
    '        '       "( " & _
    '        '       "	SELECT DISTINCT " & _
    '        '       "		 CAST(ISNULL(amount,0) AS DECIMAL(36, 2 )) AS TotalPay " & _
    '        '       "		,vwPayroll.employeeunkid AS empid " & _
    '        '       "		,payperiodunkid AS pid " & _
    '        '       "	FROM vwPayroll " & _
    '        '       "        LEFT JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '       "		LEFT JOIN athremployee_master ON vwPayroll.employeeunkid = athremployee_master.employeeunkid "
    '        'StrQ &= mstrAnalysis_Join.Replace("hremployee_master", "athremployee_master")
    '        'StrQ &= "	WHERE vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " AND prtranhead_master.istaxable = 1 AND payrollprocesstranunkid IS NOT NULL " & _
    '        '        "		AND vwPayroll.employeeunkid NOT IN (SELECT DISTINCT employeeunkid FROM athremployee_master " & _
    '        '        "		WHERE CONVERT(CHAR(8),probation_from_date,112)<= @end_date AND CONVERT(CHAR(8),probation_to_date,112)>= @end_date) " & _
    '        '        "		AND payperiodunkid = '" & mintPeriodUnkid & "' "

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "athremployee_master")
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        StrQ = "SELECT " & _
    '               "	 SUM(ISNULL(TotalPay,0)) AS TotalPay " & _
    '               "	,empid,pid " & _
    '               "FROM " & _
    '               "( " & _
    '               "	SELECT DISTINCT " & _
    '               "		 CAST(ISNULL(amount,0) AS DECIMAL(36, 2 )) AS TotalPay " & _
    '               "		,vwPayroll.employeeunkid AS empid " & _
    '               "		,payperiodunkid AS pid " & _
    '               "	FROM vwPayroll " & _
    '               "        LEFT JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '               "		LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

    '        StrQ &= mstrAnalysis_Join


    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If

    '        StrQ &= "	WHERE vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " AND prtranhead_master.istaxable = 1 AND payrollprocesstranunkid IS NOT NULL " & _
    '                "	AND vwPayroll.employeeunkid NOT IN " & _
    '                "   ( " & _
    '                "       SELECT " & _
    '                "           A.employeeunkid " & _
    '                "       FROM " & _
    '                "       ( " & _
    '                "           SELECT " & _
    '                "                employeeunkid " & _
    '                "               ,date1 " & _
    '                "               ,date2 " & _
    '                "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                "           FROM hremployee_dates_tran " & _
    '                "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndPeriodDate) & "' " & _
    '                "           AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' " & _
    '                "       ) AS A WHERE A.rno = 1 AND CONVERT(CHAR(8),A.date1)<= @end_date AND CONVERT(CHAR(8),A.date2) >= @end_date " & _
    '                "   ) " & _
    '                "	AND payperiodunkid = '" & mintPeriodUnkid & "' "

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry & " "
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END



    '        StrQ &= ") AS tmp WHERE 1 = 1 " & _
    '                "GROUP BY empid,pid "

    '        dsPerm = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dRtmp() As DataRow = Nothing

    '        For Each dRow As DataRow In dsPerm.Tables("List").Rows
    '            dRtmp = rpt_Data.Tables(0).Select(dRow.Item("TotalPay") & ">= Column70 AND " & dRow.Item("TotalPay") & "<= Column71")
    '            If dRtmp.Length > 0 Then
    '                If dRtmp(0).Item("Column4").ToString = "" Then
    '                    dRtmp(0).Item("Column4") = 1
    '                Else
    '                    dRtmp(0).Item("Column4") = CInt(dRtmp(0).Item("Column4")) + 1
    '                End If
    '                dRtmp(0).Item("Column81") = dRtmp(0).Item("Column4")
    '                If dRtmp(0).Item("Column5").ToString = "" Then
    '                    dRtmp(0).Item("Column5") = Format(CDec(dRow.Item("TotalPay")), GUI.fmtCurrency)
    '                Else
    '                    dRtmp(0).Item("Column5") = Format(CDec(CDec(dRtmp(0).Item("Column5")) + CDec(dRow.Item("TotalPay"))), GUI.fmtCurrency)
    '                End If
    '                dRtmp(0).Item("Column82") = dRtmp(0).Item("Column5")
    '            End If
    '        Next

    '        'S.SANDEEP [04 JUN 2015] -- START
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'StrQ = "SELECT " & _
    '        '       "	 SUM(ISNULL(TotalPay,0)) AS TotalPay " & _
    '        '       "	,empid,pid " & _
    '        '       "FROM " & _
    '        '       "( " & _
    '        '       "    SELECT DISTINCT " & _
    '        '       "         CAST(ISNULL(amount,0) AS DECIMAL(36," & decDecimalPlaces & ")) AS TotalPay " & _
    '        '       "        ,vwPayroll.employeeunkid AS empid " & _
    '        '       "        ,payperiodunkid AS pid " & _
    '        '       "    FROM vwPayroll " & _
    '        '       "        LEFT JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '        '       "        LEFT JOIN athremployee_master ON vwPayroll.employeeunkid = athremployee_master.employeeunkid "
    '        'StrQ &= mstrAnalysis_Join.Replace("hremployee_master", "athremployee_master")
    '        'StrQ &= "    WHERE vwPayroll.trnheadtype_id =  1 AND prtranhead_master.istaxable = 1 AND payrollprocesstranunkid IS NOT NULL " & _
    '        '        "        AND CONVERT(CHAR(8),probation_from_date,112)<= @end_date " & _
    '        '        "        AND CONVERT(CHAR(8),probation_to_date,112)>=@end_date " & _
    '        '        "        AND payperiodunkid = '" & mintPeriodUnkid & "' "

    '        ''Pinkal (15-Oct-2014) -- Start
    '        ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT

    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "athremployee_master")
    '        'End If
    '        ''Pinkal (15-Oct-2014) -- End

    '        StrQ = "SELECT " & _
    '               "	 SUM(ISNULL(TotalPay,0)) AS TotalPay " & _
    '               "	,empid,pid " & _
    '               "FROM " & _
    '               "( " & _
    '               "    SELECT DISTINCT " & _
    '               "         CAST(ISNULL(amount,0) AS DECIMAL(36," & decDecimalPlaces & ")) AS TotalPay " & _
    '               "        ,vwPayroll.employeeunkid AS empid " & _
    '               "        ,payperiodunkid AS pid " & _
    '               "    FROM vwPayroll " & _
    '               "        LEFT JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '               "        LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid " & _
    '               "        LEFT JOIN " & _
    '               "        ( " & _
    '               "            SELECT " & _
    '               "                 employeeunkid " & _
    '               "                ,date1 " & _
    '               "                ,date2 " & _
    '               "                ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '               "            FROM hremployee_dates_tran " & _
    '               "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEndPeriodDate) & "' " & _
    '               "            AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' " & _
    '               "        ) AS PR ON PR.employeeunkid = hremployee_master.employeeunkid AND PR.rno = 1 "

    '        StrQ &= mstrAnalysis_Join

    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= xUACQry
    '        End If

    '        StrQ &= "    WHERE vwPayroll.trnheadtype_id =  1 AND prtranhead_master.istaxable = 1 AND payrollprocesstranunkid IS NOT NULL " & _
    '                "        AND CONVERT(CHAR(8),PR.date1,112)<= @end_date " & _
    '                "        AND CONVERT(CHAR(8),PR.date2,112)>=@end_date " & _
    '                "        AND payperiodunkid = '" & mintPeriodUnkid & "' "

    '        If xUACFiltrQry.Trim.Length > 0 Then
    '            StrQ &= " AND " & xUACFiltrQry & " "
    '        End If

    '        'S.SANDEEP [04 JUN 2015] -- END



    '        StrQ &= ")AS tmp WHERE 1 = 1 " & _
    '                "GROUP BY empid,pid "

    '        dsTemp = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dRow As DataRow In dsTemp.Tables("List").Rows
    '            dRtmp = rpt_Data.Tables(0).Select(dRow.Item("TotalPay") & ">= Column70 AND " & dRow.Item("TotalPay") & "<= Column71")
    '            If dRtmp.Length > 0 Then
    '                If dRtmp(0).Item("Column6").ToString = "" Then
    '                    dRtmp(0).Item("Column6") = 1
    '                Else
    '                    dRtmp(0).Item("Column6") = CInt(dRtmp(0).Item("Column6")) + 1
    '                End If
    '                dRtmp(0).Item("Column83") = dRtmp(0).Item("Column6")
    '                If dRtmp(0).Item("Column7").ToString = "" Then
    '                    dRtmp(0).Item("Column7") = Format(CDec(dRow.Item("TotalPay")), GUI.fmtCurrency)
    '                Else
    '                    dRtmp(0).Item("Column7") = Format(CDec(CDec(dRtmp(0).Item("Column7")) + CDec(dRow.Item("TotalPay"))), GUI.fmtCurrency)
    '                End If
    '                dRtmp(0).Item("Column84") = dRtmp(0).Item("Column7")
    '            End If
    '        Next

    '        ReportFunction.TextChange(objRpt, "lblTopCaption", Language.getMessage(mstrModuleName, 3, "CF/P16 V.1"))
    '        ReportFunction.TextChange(objRpt, "lblTitle1", Language.getMessage(mstrModuleName, 4, "MONTHLY PAYE RETURN FORM"))
    '        ReportFunction.TextChange(objRpt, "lblMonth", Language.getMessage(mstrModuleName, 5, "MONTH"))
    '        ReportFunction.TextChange(objRpt, "lblChargeYr", Language.getMessage(mstrModuleName, 6, "CHARGE YEAR"))
    '        ReportFunction.TextChange(objRpt, "lblCaption1", Language.getMessage(mstrModuleName, 7, "TAXPAYER DETAILS (Please notify the Tax Office if there has been any change in details under 4 to 7)"))
    '        ReportFunction.TextChange(objRpt, "lblEmplrName", Language.getMessage(mstrModuleName, 8, "NAME OF EMPLOYER*"))
    '        ReportFunction.TextChange(objRpt, "lblTINNo", Language.getMessage(mstrModuleName, 9, "TAXPAYER  IDENTIFICATION NUMBER (TPIN)"))
    '        ReportFunction.TextChange(objRpt, "lblAccountNo", Language.getMessage(mstrModuleName, 10, "PAYE  ACCOUNT NUMBER"))
    '        ReportFunction.TextChange(objRpt, "lblPostalAddress", Language.getMessage(mstrModuleName, 11, "POSTAL ADDRESS"))
    '        ReportFunction.TextChange(objRpt, "lblPhysicalAddress", Language.getMessage(mstrModuleName, 12, "PHYSICAL ADDRESS"))
    '        ReportFunction.TextChange(objRpt, "lblEmail", Language.getMessage(mstrModuleName, 13, "EMAIL ADDRESS"))
    '        ReportFunction.TextChange(objRpt, "lblTelephone", Language.getMessage(mstrModuleName, 14, "TELEPHONE NUMBER"))
    '        ReportFunction.TextChange(objRpt, "lblCaption2", Language.getMessage(mstrModuleName, 15, "FOR OFFICIAL USE ONLY"))
    '        ReportFunction.TextChange(objRpt, "lblOffSign", Language.getMessage(mstrModuleName, 16, "(OFFICER'S NAME & SIGNATURE)"))
    '        ReportFunction.TextChange(objRpt, "lblDateReceived", Language.getMessage(mstrModuleName, 17, "(DATE RECEIVED)"))
    '        ReportFunction.TextChange(objRpt, "lblCaption3", Language.getMessage(mstrModuleName, 18, "Note*: Row 1 should indicate name of payer where the payment is made by a person whose employer is not obligated by law to deduct PAYE (Direct Payment Cases)."))
    '        ReportFunction.TextChange(objRpt, "lblPart1", Language.getMessage(mstrModuleName, 19, "PART I"))
    '        ReportFunction.TextChange(objRpt, "lblPoint10", Language.getMessage(mstrModuleName, 20, "Chargeable Emoluments (including salaries,wages, fees, commissions, bonuses, overtime,gratuity, etc.)"))
    '        ReportFunction.TextChange(objRpt, "lblPoint11", Language.getMessage(mstrModuleName, 21, "Tax deducted  (total tax deducted from the emoluments)"))
    '        ReportFunction.TextChange(objRpt, "lblPoint12", Language.getMessage(mstrModuleName, 22, "Tax Payable/Repayable"))
    '        ReportFunction.TextChange(objRpt, "lblPoint13", Language.getMessage(mstrModuleName, 23, "OTHER INFORMATION"))
    '        ReportFunction.TextChange(objRpt, "lblPoint13_1", Language.getMessage(mstrModuleName, 24, "Number of employees at the beginning of the month"))
    '        ReportFunction.TextChange(objRpt, "lblPoint13_2", Language.getMessage(mstrModuleName, 25, "Number of new employees employed in the month"))
    '        ReportFunction.TextChange(objRpt, "lblPoint13_3", Language.getMessage(mstrModuleName, 26, "Number of employees  that have separated in the month"))
    '        ReportFunction.TextChange(objRpt, "lblPoint13_4", Language.getMessage(mstrModuleName, 27, "Number of employees at the end of the month"))
    '        ReportFunction.TextChange(objRpt, "lblPoint14", Language.getMessage(mstrModuleName, 28, "ZAMBIA DEVELOPMENT AGENCY APPROVED INVESTMENTS (ONLY APPLICABLE TO THOSE WITH APPROVED ZDA INCENTIVES):"))
    '        ReportFunction.TextChange(objRpt, "lblLicenseNo", Language.getMessage(mstrModuleName, 29, "Licence Number:"))
    '        ReportFunction.TextChange(objRpt, "lblPoint14_1", Language.getMessage(mstrModuleName, 30, "Total number of employees pledged to be employed in the year"))
    '        ReportFunction.TextChange(objRpt, "lblPoint14_2", Language.getMessage(mstrModuleName, 31, "Number of employees employed prior to this month"))
    '        ReportFunction.TextChange(objRpt, "lblPoint14_3", Language.getMessage(mstrModuleName, 32, "Number of new  employees employed in the month"))
    '        ReportFunction.TextChange(objRpt, "lblPoint14_4", Language.getMessage(mstrModuleName, 33, "Number of employees that have left/died in the month"))
    '        ReportFunction.TextChange(objRpt, "lblPoint14_5", Language.getMessage(mstrModuleName, 34, "Number of employees at the end of the month"))

    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblPart2", Language.getMessage(mstrModuleName, 35, "PART II"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCaption1", Language.getMessage(mstrModuleName, 36, "1. Main economic activity"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblOffUse", Language.getMessage(mstrModuleName, 37, "For official use"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCaption1_txt", Language.getMessage(mstrModuleName, 38, "The main economic activity is the final product or service that the firm/establishment renders. Please be very specific, e.g. copper production, manufacturing of cement, Dairy farming, mixed farming, accounting and management consultancy, retailing of mixed groceries, sale of used cars, etc."))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCaption2", Language.getMessage(mstrModuleName, 39, "2. Type of firm/establishment: "))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCentralGov", Language.getMessage(mstrModuleName, 40, "Central Government"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblStatutoryBody", Language.getMessage(mstrModuleName, 41, "Statutory Body"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblPriLC", Language.getMessage(mstrModuleName, 42, "Private Limited Company"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCooperativeSct", Language.getMessage(mstrModuleName, 43, "Cooperative Society"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblLocalGov", Language.getMessage(mstrModuleName, 44, "Local Government"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblPubLC", Language.getMessage(mstrModuleName, 45, "Public Limited Body"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblPubBO", Language.getMessage(mstrModuleName, 46, "Church/Public Benefit Organisation"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblIndividual", Language.getMessage(mstrModuleName, 47, "Individual"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblParastatal", Language.getMessage(mstrModuleName, 48, "Parastatal"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblOther", Language.getMessage(mstrModuleName, 49, "Other (specify)"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCaption3", Language.getMessage(mstrModuleName, 50, "3. Distribution of Employees by Income Bracket"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblGrossIncome", Language.getMessage(mstrModuleName, 51, "Gross Income per Month"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblTotPrmEmp", Language.getMessage(mstrModuleName, 52, "Number of Permanent Employees in the bracket"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblPrmGrossIncome", Language.getMessage(mstrModuleName, 53, "Gross Income within bracket"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblTotTepEmp", Language.getMessage(mstrModuleName, 54, "Number of Temporary Employees in the bracket"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblTotTmpGrossIncome", Language.getMessage(mstrModuleName, 53, "Gross Income within bracket"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblGrandTotal", Language.getMessage(mstrModuleName, 55, "Total"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCaption3_txt", Language.getMessage(mstrModuleName, 56, "Note: Gross income (in Part II, 3.0)  must include ALL TAXABLE INCOME including allowances such as housing allowance, transport allowance, overtime pay, etc."))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCaption4", Language.getMessage(mstrModuleName, 57, "4. DECLARATION"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCaption4_txt", Language.getMessage(mstrModuleName, 58, "I declare that the details in this Return are true and correct."))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblName", Language.getMessage(mstrModuleName, 59, "Name:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblSign", Language.getMessage(mstrModuleName, 60, "Signature:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblCapacity", Language.getMessage(mstrModuleName, 61, "Capacity:"))
    '        ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "lblDate", Language.getMessage(mstrModuleName, 62, "Date:"))

    '        ReportFunction.TextChange(objRpt, "txtMonthName", mstrPeriodName)
    '        ReportFunction.TextChange(objRpt, "txtEmplrName", Company._Object._Name)
    '        ReportFunction.TextChange(objRpt, "txtTINNo", Company._Object._Tinno)
    '        ReportFunction.TextChange(objRpt, "txtAccountNo", Company._Object._Company_Reg_No)
    '        ReportFunction.TextChange(objRpt, "txtPostalAddress", Company._Object._Address1 & " " & Company._Object._Address2)
    '        ReportFunction.TextChange(objRpt, "txtPhysicalAddress", Company._Object._Address1 & " " & Company._Object._Address2)
    '        ReportFunction.TextChange(objRpt, "txtEmail", Company._Object._Email)
    '        ReportFunction.TextChange(objRpt, "txtTelehone", Company._Object._Phone1)
    '        If rpt_Data.Tables(0).Rows.Count > 0 Then
    '            Dim decCol81, decCol82, decCol83, decCol84 As Decimal : decCol81 = 0 : decCol82 = 0 : decCol83 = 0 : decCol84 = 0
    '            For i As Integer = 0 To rpt_Data.Tables(0).Rows.Count - 1
    '                If IsDBNull(rpt_Data.Tables(0).Rows(i).Item("Column81")) = False Then
    '                    decCol81 += CInt(rpt_Data.Tables(0).Rows(i).Item("Column81"))
    '                End If

    '                If IsDBNull(rpt_Data.Tables(0).Rows(i).Item("Column82")) = False Then
    '                    decCol82 += CDec(rpt_Data.Tables(0).Rows(i).Item("Column82"))
    '                End If

    '                If IsDBNull(rpt_Data.Tables(0).Rows(i).Item("Column83")) = False Then
    '                    decCol83 += CInt(rpt_Data.Tables(0).Rows(i).Item("Column83"))
    '                End If

    '                If IsDBNull(rpt_Data.Tables(0).Rows(i).Item("Column84")) = False Then
    '                    decCol84 += CDec(rpt_Data.Tables(0).Rows(i).Item("Column84"))
    '                End If
    '            Next
    '            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtPEmpTot", decCol81)
    '            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtTEmpTot", decCol83)
    '            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtPGross", Format(CDec(decCol82), GUI.fmtCurrency))
    '            ReportFunction.TextChange(objRpt.Subreports("rpt_ITF_Summary"), "txtTGross", Format(CDec(decCol84), GUI.fmtCurrency))
    '        End If
    '        objRpt.SetDataSource(rpt_Data)
    '        objRpt.Subreports("rpt_ITF_Summary").SetDataSource(rpt_Data)

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function
#End Region

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Upto")
            Language.setMessage(mstrModuleName, 2, "Above")
            Language.setMessage(mstrModuleName, 3, "CF/P16 V.1")
            Language.setMessage(mstrModuleName, 4, "PAYE RETURN FORM")
            Language.setMessage(mstrModuleName, 5, "Month/Quarter/Half Year Ended on")
            Language.setMessage(mstrModuleName, 6, "CHARGE YEAR")
            Language.setMessage(mstrModuleName, 7, "TAXPAYER DETAILS (Please notify the Tax Office if there has been any change in details under 4 to 7)")
            Language.setMessage(mstrModuleName, 8, "NAME OF EMPLOYER*")
            Language.setMessage(mstrModuleName, 9, "TAXPAYER  IDENTIFICATION NUMBER (TPIN)")
            Language.setMessage(mstrModuleName, 10, "PAYE  ACCOUNT NUMBER")
            Language.setMessage(mstrModuleName, 11, "POSTAL ADDRESS")
            Language.setMessage(mstrModuleName, 12, "PHYSICAL ADDRESS")
            Language.setMessage(mstrModuleName, 13, "EMAIL ADDRESS")
            Language.setMessage(mstrModuleName, 14, "TELEPHONE NUMBER")
            Language.setMessage(mstrModuleName, 15, "FOR OFFICIAL USE ONLY")
            Language.setMessage(mstrModuleName, 16, "(OFFICER'S NAME)")
            Language.setMessage(mstrModuleName, 17, "(DATE RECEIVED)")
            Language.setMessage(mstrModuleName, 18, "Note*: Row 1 should indicate name of payer where the payment is made by a person whose employer is not obligated by law to deduct PAYE (Direct Payment Cases).")
            Language.setMessage(mstrModuleName, 19, "PART I")
            Language.setMessage(mstrModuleName, 20, "Chargeable Emoluments (including salaries, wages, fees, commissions, bonuses, overtime, gratuity, etc.) (Sum of column E to part II)")
            Language.setMessage(mstrModuleName, 21, "Tax deducted (total tax deducted from the emoluments) (Sum of column G to part II)")
            Language.setMessage(mstrModuleName, 22, "Tax Payable/(Repayable) (2-3)")
            Language.setMessage(mstrModuleName, 23, "OTHER INFORMATION")
            Language.setMessage(mstrModuleName, 24, "Number of employees at the beginning of the month")
            Language.setMessage(mstrModuleName, 25, "Number of new employees employed in the month")
            Language.setMessage(mstrModuleName, 26, "Number of employees that have separated in the month")
            Language.setMessage(mstrModuleName, 27, "Number of employees at the end of the month")
            Language.setMessage(mstrModuleName, 28, "ZAMBIA DEVELOPMENT AGENCY APPROVED INVESTMENTS (ONLY APPLICABLE TO THOSE WITH APPROVED ZDA INCENTIVES):")
            Language.setMessage(mstrModuleName, 29, "Licence Number:")
            Language.setMessage(mstrModuleName, 30, "Total number of employees pledged to be employed in the year")
            Language.setMessage(mstrModuleName, 31, "Number of employees employed prior to this month")
            Language.setMessage(mstrModuleName, 32, "Number of new employees employed in the month")
            Language.setMessage(mstrModuleName, 33, "Number of employees that have separated in the month")
            Language.setMessage(mstrModuleName, 34, "Number of employees at the end of the month")
            Language.setMessage(mstrModuleName, 35, "PART II")
            Language.setMessage(mstrModuleName, 36, "NRC of Employee")
            Language.setMessage(mstrModuleName, 37, "Name of Employee")
            Language.setMessage(mstrModuleName, 38, "Nature of Employment Temporal/Permanent")
            Language.setMessage(mstrModuleName, 39, "Gross emoluments for the tax period")
            Language.setMessage(mstrModuleName, 40, "Chargeable emoluments for the tax period")
            Language.setMessage(mstrModuleName, 41, "Total tax credit for the tax period")
            Language.setMessage(mstrModuleName, 42, "Tax Deducted as  shown on the TRD for the year")
            Language.setMessage(mstrModuleName, 43, "Tax Adjusted")
            Language.setMessage(mstrModuleName, 55, "Total")
            Language.setMessage(mstrModuleName, 56, "Note: Gross income (in Part II, 3.0)  must include ALL TAXABLE INCOME including allowances such as housing allowance, transport allowance, overtime pay, etc.")
            Language.setMessage(mstrModuleName, 57, "4. DECLARATION")
            Language.setMessage(mstrModuleName, 58, "I declare that the details in this Return are true and correct.")
            Language.setMessage(mstrModuleName, 59, "Name:")
            Language.setMessage(mstrModuleName, 60, "Signature:")
            Language.setMessage(mstrModuleName, 61, "Capacity:")
            Language.setMessage(mstrModuleName, 62, "Date:")
            Language.setMessage(mstrModuleName, 63, "Original")
            Language.setMessage(mstrModuleName, 64, "Amended")
            Language.setMessage(mstrModuleName, 65, "Supplementary")
            Language.setMessage(mstrModuleName, 66, "If Amended,Amendment Approval Number")
            Language.setMessage(mstrModuleName, 67, "Tax adjusted  (total tax adjusted from the emoluments) (Sum of column H to part II)")
            Language.setMessage(mstrModuleName, 68, "Skills Development Levy (Total Gross Emoluments for the period) (Does not allow to exempt Institutions)")
            Language.setMessage(mstrModuleName, 69, "Skills Development Levy (0.5% of Total Gross Emoluments for the period)")
            Language.setMessage(mstrModuleName, 70, "Officer's Signature")
            Language.setMessage(mstrModuleName, 71, "Receiving Office Date Stamp")
            Language.setMessage(mstrModuleName, 72, "Identity Type")
            Language.setMessage(mstrModuleName, 73, "TPIN")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
