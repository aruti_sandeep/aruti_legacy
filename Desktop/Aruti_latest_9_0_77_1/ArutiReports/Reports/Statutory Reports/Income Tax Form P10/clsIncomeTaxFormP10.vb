'************************************************************************************************************************************
'Class Name : clsIncomeTaxFormP10.vb
'Purpose    :
'Date        :09/12/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsIncomeTaxFormP10
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsIncomeTaxFormP10"
    Private mstrReportId As String = enArutiReport.IncomeTaxFormP10  '15
    Dim objDataOperation As clsDataOperation

#Region " Constructor "

    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub

#End Region

#Region " Private variables "

    Private mintHousingTranId As Integer = 0
    Private minSlabTranId As Integer = 0
    Private mstrPeriodIds As String = String.Empty
    Private mstrEndPeriodName As String = String.Empty
    Private mdtEndPeriodDate As DateTime 'Sohail (21 Nov 2011)
    'S.SANDEEP [ 04 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintPeriodCount As Integer = 0
    'S.SANDEEP [ 04 JULY 2012 ] -- END

    'Sohail (07 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Private mintOtherEarningTranId As Integer = 0
    Private mblnIncludeNonTaxableEarningInGrossPay As Boolean = False
    'Sohail (07 Sep 2013) -- End

    'Sohail (18 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mstrReport_GroupName As String = String.Empty
    'Sohail (18 Mar 2013) -- End

#End Region

#Region " Properties "

    Public WriteOnly Property _HousingTranId() As Integer
        Set(ByVal value As Integer)
            mintHousingTranId = value
        End Set
    End Property

    Public WriteOnly Property _SlabTranId() As Integer
        Set(ByVal value As Integer)
            minSlabTranId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodIds() As String
        Set(ByVal value As String)
            mstrPeriodIds = value
        End Set
    End Property

    Public WriteOnly Property _EndPeriodName() As String
        Set(ByVal value As String)
            mstrEndPeriodName = value
        End Set
    End Property

    'Sohail (21 Nov 2011) -- Start
    Public WriteOnly Property _EndPeriodDate() As DateTime
        Set(ByVal value As DateTime)
            mdtEndPeriodDate = value
        End Set
    End Property
    'Sohail (21 Nov 2011) -- End

    'S.SANDEEP [ 04 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _PeriodCount() As Integer
        Set(ByVal value As Integer)
            mintPeriodCount = value
        End Set
    End Property
    'S.SANDEEP [ 04 JULY 2012 ] -- END

    'Sohail (07 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _OtherEarningTranId() As Integer
        Set(ByVal value As Integer)
            mintOtherEarningTranId = value
        End Set
    End Property

    Public WriteOnly Property _IncludeNonTaxableEarningInGrossPay() As Boolean
        Set(ByVal value As Boolean)
            mblnIncludeNonTaxableEarningInGrossPay = value
        End Set
    End Property
    'Sohail (07 Sep 2013) -- End

    'Sohail (18 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property
    'Sohail (18 Mar 2013) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintHousingTranId = 0
            minSlabTranId = 0
            mstrPeriodIds = String.Empty
            mstrEndPeriodName = String.Empty
            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mintPeriodCount = 0
            'S.SANDEEP [ 04 JULY 2012 ] -- END

            'Sohail (07 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            mintOtherEarningTranId = 0
            mblnIncludeNonTaxableEarningInGrossPay = False
            'Sohail (07 Sep 2013) -- End

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrReport_GroupName = ""
            'Sohail (18 Mar 2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Private Sub FilterTitleAndFilterQuery()
        Me._FilterQuery = ""
        Me._FilterTitle = ""
        Try
            objDataOperation.AddParameter("@HousingTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHousingTranId)

            'Sohail (07 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            If mintOtherEarningTranId > 0 Then
                objDataOperation.AddParameter("@OtherEarningTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherEarningTranId)
            End If
            'Sohail (07 Sep 2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try
        '    objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
        'S.SANDEEP [04 JUN 2015] -- END
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim dsPeriodicData As New DataSet
        Dim exForce As Exception
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try
            objDataOperation = New clsDataOperation

            'Sohail (17 Aug 2012) -- Start
            'TRA - ENHANCEMENT
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            'S.SANDEEP [04 JUN 2015] -- END
            'Sohail (29 Mar 2017) -- Start
            'Issue - 65.2 - Amount not matching with Payroll Report.
            'decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (29 Nov 2017) -- End
            'Sohail (29 Mar 2017) -- End
            'Sohail (17 Aug 2012) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'S.SANDEEP [04 JUN 2015] -- END


            'ISSUE : INCLUSION OF EMPLOYEE IF HE IS ACTIVE FOR SOME PERIOD BUT NOW INACTIVE
            'E.G.  : EMPLOYEE IS ACTIVE FOR JAN - MAR & TERMINATED IN APR. SO ITS NOT COMING FOR JAN TO MAR IN REPORT.

            'Sohail (21 Nov 2011) -- Start
            'Query moved on View Payroll
            'StrQ = "SELECT " & _
            '            "	 PeriodId AS PeriodId " & _
            '            "	,PeriodName AS PeriodName " & _
            '            "	,TaxPayable AS TaxPayable " & _
            '            "	,LPR AS Less_Personal_Relief " & _
            '            "	,NTD AS Net_Tax_Due " & _
            '            "FROM " & _
            '            "	( " & _
            '            "		SELECT " & _
            '            "			 Period.PeriodId " & _
            '            "			,Period.PeriodName " & _
            '            "			,StDate AS StDate " & _
            '            "			,SUM(ISNULL(TaxPayable,0)) AS TaxPayable " & _
            '            "			,SUM(ISNULL(LPR,0)) AS LPR " & _
            '            "			,SUM(ISNULL(TaxPayable,0)) -SUM(ISNULL(LPR,0)) AS NTD " & _
            '            "		FROM " & _
            '            "			( " & _
            '            "				SELECT " & _
            '            "					 periodunkid AS PeriodId " & _
            '            "					,period_name AS PeriodName " & _
            '            "					,start_date AS StDate " & _
            '            "				FROM cfcommon_period_tran " & _
            '            "				WHERE isactive = 1 AND modulerefid = 1 " & _
            '            "					AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ")  " & _
            '            "			) AS Period " & _
            '            "		LEFT JOIN " & _
            '            "			( " & _
            '            "				SELECT " & _
            '            "					 SUM(ISNULL(prpayrollprocess_tran.amount,0)) AS TaxPayable " & _
            '            "					,0 AS LPR " & _
            '            "					,prtnaleave_tran.payperiodunkid AS Pid " & _
            '            "				FROM prpayrollprocess_tran " & _
            '            "					JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
            '            "					JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
            '            "                   JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
            '            "				WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '            "					AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '            "					AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '            "					AND prtranhead_master.typeof_id = 9 " & _
            '            "					AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ")  " & _
            '            "				GROUP BY prtnaleave_tran.payperiodunkid " & _
            '            "		UNION  ALL " & _
            '            "				SELECT " & _
            '            "					 0 AS TaxPayable " & _
            '            "					,SUM(ISNULL(prpayrollprocess_tran.amount,0)) AS LPR " & _
            '            "					,prtnaleave_tran.payperiodunkid AS Pid " & _
            '            "				FROM prpayrollprocess_tran " & _
            '            "					JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
            '            "					JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
            '            "                   JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
            '            "				WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '            "					AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '            "					AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '            "					AND ISNULL(prtranhead_master.istaxrelief,0)=1 " & _
            '            "					AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ")  " & _
            '            "				GROUP BY prtnaleave_tran.payperiodunkid " & _
            '            "			) AS basicpay ON basicpay.Pid=Period.PeriodId " & _
            '            "			GROUP BY " & _
            '            "				 Period.PeriodId " & _
            '            "				,Period.PeriodName " & _
            '            "				,StDate " & _
            '            "	) AS ITD "

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT  PeriodId AS PeriodId " & _
            '            "	,PeriodName AS PeriodName " & _
            '            "	,TaxPayable AS TaxPayable " & _
            '            "	,LPR AS Less_Personal_Relief " & _
            '            "	,NTD AS Net_Tax_Due " & _
            '        "FROM    ( SELECT    Period.PeriodId " & _
            '            "			,Period.PeriodName " & _
            '            "			,StDate AS StDate " & _
            '                          ", EndDate " & _
            '            "			,SUM(ISNULL(TaxPayable,0)) AS TaxPayable " & _
            '            "			,SUM(ISNULL(LPR,0)) AS LPR " & _
            '            "			,SUM(ISNULL(TaxPayable,0)) -SUM(ISNULL(LPR,0)) AS NTD " & _
            '                  "FROM      ( SELECT    periodunkid AS PeriodId " & _
            '            "					,period_name AS PeriodName " & _
            '            "					,start_date AS StDate " & _
            '                                      ", end_date AS EndDate " & _
            '            "				FROM cfcommon_period_tran " & _
            '                              "WHERE     isactive = 1 " & _
            '                                        "AND modulerefid = 1 " & _
            '            "					AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ")  " & _
            '            "			) AS Period " & _
            '                            "LEFT JOIN ( SELECT  SUM(amount) AS TaxPayable " & _
            '            "					,0 AS LPR " & _
            '                                              ", payperiodunkid AS Pid " & _
            '                                        "FROM    vwPayroll " & _
            '                                        "WHERE   typeof_id = " & enTypeOf.Taxes & " " & _
            '                                                "AND payperiodunkid IN (" & mstrPeriodIds & ") " & _
            '                                        "GROUP BY payperiodunkid " & _
            '            "		UNION  ALL " & _
            '                                        "SELECT  0 AS TaxPayable " & _
            '                                              ", SUM(amount) AS LPR " & _
            '                                              ", payperiodunkid AS Pid " & _
            '                                        "FROM    vwPayroll " & _
            '                                        "LEFT JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                                        "WHERE   ISNULL(prtranhead_master.istaxrelief,0) = 1 " & _
            '                                                "AND payperiodunkid IN (" & mstrPeriodIds & ") " & _
            '                                        "GROUP BY payperiodunkid " & _
            '            "			) AS basicpay ON basicpay.Pid=Period.PeriodId " & _
            '                  "GROUP BY  Period.PeriodId " & _
            '            "				,Period.PeriodName " & _
            '            "				,StDate " & _
            '                          ", EndDate " & _
            '                ") AS ITD  ORDER BY ITD.EndDate "

            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT  PeriodId AS PeriodId " & _
            '            "	,PeriodName AS PeriodName " & _
            '            "	,TaxPayable AS TaxPayable " & _
            '            "	,LPR AS Less_Personal_Relief " & _
            '            "	,NTD AS Net_Tax_Due " & _
            '        "FROM    ( SELECT    Period.PeriodId " & _
            '            "			,Period.PeriodName " & _
            '            "			,StDate AS StDate " & _
            '                          ", EndDate " & _
            '            "			,SUM(ISNULL(TaxPayable,0)) AS TaxPayable " & _
            '            "			,SUM(ISNULL(LPR,0)) AS LPR " & _
            '            "			,SUM(ISNULL(TaxPayable,0)) -SUM(ISNULL(LPR,0)) AS NTD " & _
            '                  "FROM      ( SELECT    periodunkid AS PeriodId " & _
            '            "					,period_name AS PeriodName " & _
            '            "					,start_date AS StDate " & _
            '                                      ", end_date AS EndDate " & _
            '            "				FROM cfcommon_period_tran " & _
            '                              "WHERE     isactive = 1 " & _
            '                                        "AND modulerefid = 1 " & _
            '            "					AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ")  " & _
            '            "			) AS Period " & _
            '                            "LEFT JOIN ( SELECT  (SUM(amount)/" & mintPeriodCount & ")" & " AS TaxPayable " & _
            '            "					,0 AS LPR " & _
            '                                              ", payperiodunkid AS Pid " & _
            '                                        "FROM    vwPayroll " & _
            '                                        "WHERE   typeof_id = " & enTypeOf.Taxes & " " & _
            '                                                "AND payperiodunkid IN (" & mstrPeriodIds & ") " & _
            '                                        "GROUP BY payperiodunkid " & _
            '            "		UNION  ALL " & _
            '                                        "SELECT  0 AS TaxPayable " & _
            '                                              ", (SUM(amount)/" & mintPeriodCount & ")" & " AS LPR " & _
            '                                              ", payperiodunkid AS Pid " & _
            '                                        "FROM    vwPayroll " & _
            '                                        "LEFT JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                                        "WHERE   ISNULL(prtranhead_master.istaxrelief,0) = 1 " & _
            '                                                "AND payperiodunkid IN (" & mstrPeriodIds & ") " & _
            '                                        "GROUP BY payperiodunkid " & _
            '            "			) AS basicpay ON basicpay.Pid=Period.PeriodId " & _
            '                  "GROUP BY  Period.PeriodId " & _
            '            "				,Period.PeriodName " & _
            '            "				,StDate " & _
            '                          ", EndDate " & _
            '                ") AS ITD  ORDER BY ITD.EndDate "

            'Hemant (11 Jul 2019) -- Start
            'ISSUE/ENHANCEMENT #0003967 - Unable to generate p10 report Jan 19 - June 19
            'StrQ = "SELECT  PeriodId AS PeriodId " & _
            '            "	,PeriodName AS PeriodName " & _
            '            "	,TaxPayable AS TaxPayable " & _
            '            "	,LPR AS Less_Personal_Relief " & _
            '            "	,NTD AS Net_Tax_Due " & _
            '        "FROM    ( SELECT    Period.PeriodId " & _
            '            "			,Period.PeriodName " & _
            '            "			,StDate AS StDate " & _
            '                          ", EndDate " & _
            '            "			,SUM(ISNULL(TaxPayable,0)) AS TaxPayable " & _
            '            "			,SUM(ISNULL(LPR,0)) AS LPR " & _
            '            "			,SUM(ISNULL(TaxPayable,0)) -SUM(ISNULL(LPR,0)) AS NTD " & _
            '                  "FROM      ( SELECT    periodunkid AS PeriodId " & _
            '            "					,period_name AS PeriodName " & _
            '            "					,start_date AS StDate " & _
            '                                      ", end_date AS EndDate " & _
            '            "				FROM cfcommon_period_tran " & _
            '                              "WHERE     isactive = 1 " & _
            '                                        "AND modulerefid = 1 " & _
            '            "					AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ")  " & _
            '            "			) AS Period " & _
            '                            "LEFT JOIN ( SELECT  (SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))))" & " AS TaxPayable " & _
            '            "					,0 AS LPR " & _
            '                                              ", payperiodunkid AS Pid "

            ''Sohail (18 Mar 2013) -- Start
            ''TRA - ENHANCEMENT
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            ''Sohail (18 Mar 2013) -- End

            'StrQ &= "FROM    vwPayroll " & _
            '                                        "LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            ''Sohail (18 Mar 2013) -- Start
            ''TRA - ENHANCEMENT
            'StrQ &= mstrAnalysis_Join
            ''Sohail (18 Mar 2013) -- End

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END

            'StrQ &= "WHERE   typeof_id = " & enTypeOf.Taxes & " " & _
            '                                                "AND payperiodunkid IN (" & mstrPeriodIds & ") " & _
            '                                        "GROUP BY payperiodunkid "

            ''Sohail (18 Mar 2013) -- Start
            ''TRA - ENHANCEMENT
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            'End If
            ''Sohail (18 Mar 2013) -- End

            'StrQ &= "		UNION  ALL " & _
            '                                        "SELECT  0 AS TaxPayable " & _
            '                                              ", (SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))))" & " AS LPR " & _
            '                                              ", payperiodunkid AS Pid "

            ''Sohail (18 Mar 2013) -- Start
            ''TRA - ENHANCEMENT
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ", 0 AS Id, '' AS GName "
            'End If
            ''Sohail (18 Mar 2013) -- End

            'StrQ &= "FROM    vwPayroll " & _
            '                                        "LEFT JOIN prtranhead_master ON vwPayroll.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                                        "LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            ''Sohail (18 Mar 2013) -- Start
            ''TRA - ENHANCEMENT
            'StrQ &= mstrAnalysis_Join
            ''Sohail (18 Mar 2013) -- End

            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            ''S.SANDEEP [04 JUN 2015] -- END

            'StrQ &= "WHERE   ISNULL(prtranhead_master.istaxrelief,0) = 1 " & _
            '                                                "AND payperiodunkid IN (" & mstrPeriodIds & ") "


            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ' ''Pinkal (15-Oct-2014) -- Start
            ' ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            ''If UserAccessLevel._AccessLevel.Length > 0 Then
            ''    StrQ &= UserAccessLevel._AccessLevelFilterString
            ''End If
            ' ''Pinkal (15-Oct-2014) -- End

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If

            ''S.SANDEEP [04 JUN 2015] -- END



            'StrQ &= "GROUP BY payperiodunkid "

            ''Sohail (18 Mar 2013) -- Start
            ''TRA - ENHANCEMENT
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            'End If
            ''Sohail (18 Mar 2013) -- End

            'StrQ &= "			) AS basicpay ON basicpay.Pid=Period.PeriodId " & _
            '                  "GROUP BY  Period.PeriodId " & _
            '            "				,Period.PeriodName " & _
            '            "				,StDate " & _
            '                          ", EndDate " & _
            '                ") AS ITD  ORDER BY ITD.EndDate "
            StrQ = " SELECT    periodunkid AS PeriodId " & _
                        "			,period_name AS PeriodName " & _
                        "			,start_date AS StDate " & _
                        ",          end_date AS EndDate " & _
                        " INTO #TablePeriod " & _
                        "				FROM cfcommon_period_tran " & _
                        "                  WHERE     isactive = 1 " & _
                                                    "AND modulerefid = 1 " & _
                        "					AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ")  "

            StrQ &= "SELECT hremployee_master.employeeunkid "

            StrQ &= "INTO #TableEmp " & _
                        "FROM hremployee_master "

            StrQ &= mstrAnalysis_Join

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If


            StrQ &= "SELECT  PeriodId AS PeriodId " & _
                        "	,PeriodName AS PeriodName " & _
                        "	,TaxPayable AS TaxPayable " & _
                        "	,LPR AS Less_Personal_Relief " & _
                        "	,NTD AS Net_Tax_Due " & _
                    "FROM    ( SELECT    Period.PeriodId " & _
                        "			,Period.PeriodName " & _
                        "			,StDate AS StDate " & _
                                      ", EndDate " & _
                        "			,SUM(ISNULL(TaxPayable,0)) AS TaxPayable " & _
                        "			,SUM(ISNULL(LPR,0)) AS LPR " & _
                        "			,SUM(ISNULL(TaxPayable,0)) -SUM(ISNULL(LPR,0)) AS NTD " & _
                              "FROM      #TablePeriod AS Period " & _
                        "           LEFT JOIN ( SELECT  (SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))))" & " AS TaxPayable " & _
                        "					,0 AS LPR " & _
                                                          ", payperiodunkid AS Pid "


            StrQ &= "FROM    prpayrollprocess_tran " & _
                             "LEFT join prtnaleave_tran on prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid  " & _
                             "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                             "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid "


            StrQ &= "WHERE  prpayrollprocess_tran.isvoid = 0 and prtnaleave_tran.isvoid = 0  " & _
                            "and  typeof_id = " & enTypeOf.Taxes & " " & _
                                                            "AND payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                    "GROUP BY payperiodunkid "


            StrQ &= "		UNION  ALL " & _
                                                    "SELECT  0 AS TaxPayable " & _
                                                          ", (SUM(CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))))" & " AS LPR " & _
                                                          ", payperiodunkid AS Pid "


            StrQ &= "FROM    prpayrollprocess_tran LEFT join prtnaleave_tran on prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                                                    "LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                                                    " JOIN #TableEmp ON #TableEmp.employeeunkid = prpayrollprocess_tran.employeeunkid "


            StrQ &= "WHERE   ISNULL(prtranhead_master.istaxrelief,0) = 1 " & _
                                                            "AND payperiodunkid IN (" & mstrPeriodIds & ") " & _
                                                            "AND prpayrollprocess_tran.isvoid = 0 and prtnaleave_tran.isvoid = 0 "

            StrQ &= "GROUP BY payperiodunkid "


            StrQ &= "			) AS basicpay ON basicpay.Pid=Period.PeriodId " & _
                              "GROUP BY  Period.PeriodId " & _
                        "				,Period.PeriodName " & _
                        "				,StDate " & _
                                      ", EndDate " & _
                            ") AS ITD  ORDER BY ITD.EndDate "

            StrQ &= "DROP TABLE #TableEmp " & _
                    "DROP TABLE #TablePeriod "
            'Hemant (11 Jul 2019) -- End
            'S.SANDEEP [ 19 JULY 2012 ] -- END


            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'Sohail (21 Nov 2011) -- End
            Call FilterTitleAndFilterQuery()

            dsPeriodicData = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsSlab As New DataSet

            'Dim StrDecPoint() As String = GUI.fmtCurrency.ToString.Split(".")
            'Dim iCnt As Integer = StrDecPoint(1).Length

            'Sohail (21 Nov 2011) -- Start
            'Changes : pick only one slab as Now slab comes for multi periods.
            'StrQ = "SELECT " & _
            '            "	CASE WHEN	SrNo < Cnt AND  SrNo = 1 THEN CONVERT(NVARCHAR(50), inexcessof )+' - ' + CONVERT(NVARCHAR(50),amountupto) " & _
            '            "			 WHEN SrNo < Cnt THEN CONVERT(NVARCHAR(50),inexcessof +1 )+' - ' + CONVERT(NVARCHAR(50), amountupto) " & _
            '            "			 WHEN SrNo = Cnt THEN @Over + CONVERT(NVARCHAR(50),inexcessof ) " & _
            '            "	 END AS SLAB " & _
            '            "	,CASE WHEN SrNo = 1 THEN inexcessof ELSE inexcessof+1 END AS inexcessof " & _
            '            "	,amountupto " & _
            '            "   ,SrNo As RangeNo " & _
            '            "FROM " & _
            '            "( " & _
            '            "	SELECT " & _
            '            "		 ROW_NUMBER() OVER (ORDER BY inexcessof ASC)AS SrNo " & _
            '            "		, convert(Decimal(30," & iCnt & "),inexcessof) as inexcessof " & _
            '            "		, convert(Decimal(30," & iCnt & "),amountupto) as amountupto " & _
            '            "		,(SELECT COUNT(prtranhead_inexcessslab_tran.tranheadtaxslabtranunkid) FROM prtranhead_inexcessslab_tran WHERE tranheadunkid = @TranHeadId AND isvoid = 0) AS Cnt " & _
            '            "	FROM prtranhead_inexcessslab_tran WHERE tranheadunkid = @TranHeadId AND isvoid = 0 " & _
            '            ") AS SLB "

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT  CASE WHEN SrNo < Cnt " & _
            '                          "AND SrNo = 1 " & _
            '                     "THEN CONVERT(NVARCHAR(50), inexcessof) + ' - ' " & _
            '                          "+ CONVERT(NVARCHAR(50), amountupto) " & _
            '                     "WHEN SrNo < Cnt " & _
            '                     "THEN CONVERT(NVARCHAR(50), inexcessof + 1) + ' - ' " & _
            '                          "+ CONVERT(NVARCHAR(50), amountupto) " & _
            '            "			 WHEN SrNo = Cnt THEN @Over + CONVERT(NVARCHAR(50),inexcessof ) " & _
            '            "	 END AS SLAB " & _
            '              ", CASE WHEN SrNo = 1 THEN inexcessof " & _
            '                     "ELSE inexcessof + 1 " & _
            '                "END AS inexcessof " & _
            '            "	,amountupto " & _
            '              ", SrNo AS RangeNo " & _
            '        "FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY inexcessof ASC ) AS SrNo " & _
            '                          ", CONVERT(DECIMAL(30, 2), inexcessof) AS inexcessof " & _
            '                          ", CONVERT(DECIMAL(30, 2), amountupto) AS amountupto " & _
            '                          ", ( SELECT    COUNT(prtranhead_inexcessslab_tran.tranheadtaxslabtranunkid) " & _
            '                              "FROM      prtranhead_inexcessslab_tran " & _
            '                              "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                              "WHERE     tranheadunkid = @TranHeadId " & _
            '                                        "AND isvoid = 0 " & _
            '                                        "AND cfcommon_period_tran.end_date = ( SELECT    MAX(end_date) " & _
            '                                                                              "FROM      prtranhead_inexcessslab_tran " & _
            '                                                                              "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                                                                              "WHERE     ISNULL(prtranhead_inexcessslab_tran.isvoid, 0) = 0 " & _
            '                                                                              "AND prtranhead_inexcessslab_tran.tranheadunkid = @TranHeadId " & _
            '                                                                              "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date ) " & _
            '                            ") AS Cnt " & _
            '                  "FROM      prtranhead_inexcessslab_tran " & _
            '                  "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                  "WHERE     tranheadunkid = @TranHeadId " & _
            '                            "AND isvoid = 0 " & _
            '                            "AND cfcommon_period_tran.end_date = ( SELECT    MAX(end_date) " & _
            '                                           "FROM      prtranhead_inexcessslab_tran " & _
            '                                           "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                                           "WHERE     ISNULL(prtranhead_inexcessslab_tran.isvoid, 0) = 0 " & _
            '                                           "AND prtranhead_inexcessslab_tran.tranheadunkid = @TranHeadId " & _
            '                                           "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date ) " & _
            '            ") AS SLB "
            StrQ = "SELECT  CASE WHEN SrNo < Cnt " & _
                                      "AND SrNo = 1 " & _
                                 "THEN CONVERT(NVARCHAR(50), CASE WHEN inexcessof = 0 THEN '1.00' ELSE inexcessof END) + ' - ' " & _
                                      "+ CONVERT(NVARCHAR(50), amountupto) " & _
                                 "WHEN SrNo < Cnt " & _
                                 "THEN CONVERT(NVARCHAR(50), inexcessof + 1) + ' - ' " & _
                                      "+ CONVERT(NVARCHAR(50), amountupto) " & _
                        "			 WHEN SrNo = Cnt THEN @Over + CONVERT(NVARCHAR(50),inexcessof ) " & _
                        "	 END AS SLAB " & _
                          ", CASE WHEN SrNo = 1 THEN CASE WHEN inexcessof = 0 THEN '1.00' ELSE inexcessof END " & _
                                 "ELSE inexcessof + 1 " & _
                            "END AS inexcessof " & _
                        "	,amountupto " & _
                          ", SrNo AS RangeNo " & _
                    "FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY inexcessof ASC ) AS SrNo " & _
                                      ", CONVERT(DECIMAL(30, 2), inexcessof) AS inexcessof " & _
                                      ", CONVERT(DECIMAL(30, 2), amountupto) AS amountupto " & _
                                      ", ( SELECT    COUNT(prtranhead_inexcessslab_tran.tranheadtaxslabtranunkid) " & _
                                          "FROM      prtranhead_inexcessslab_tran " & _
                                          "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                          "WHERE     tranheadunkid = @TranHeadId " & _
                                                    "AND isvoid = 0 " & _
                                                    "AND cfcommon_period_tran.end_date = ( SELECT    MAX(end_date) " & _
                                                                                          "FROM      prtranhead_inexcessslab_tran " & _
                                                                                          "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                                                                          "WHERE     ISNULL(prtranhead_inexcessslab_tran.isvoid, 0) = 0 " & _
                                                                                          "AND prtranhead_inexcessslab_tran.tranheadunkid = @TranHeadId " & _
                                                                                          "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date ) " & _
                                        ") AS Cnt " & _
                              "FROM      prtranhead_inexcessslab_tran " & _
                              "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                              "WHERE     tranheadunkid = @TranHeadId " & _
                                        "AND isvoid = 0 " & _
                                        "AND cfcommon_period_tran.end_date = ( SELECT    MAX(end_date) " & _
                                                       "FROM      prtranhead_inexcessslab_tran " & _
                                                       "LEFT JOIN cfcommon_period_tran ON prtranhead_inexcessslab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                                       "WHERE     ISNULL(prtranhead_inexcessslab_tran.isvoid, 0) = 0 " & _
                                                       "AND prtranhead_inexcessslab_tran.tranheadunkid = @TranHeadId " & _
                                                       "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date ) " & _
                        ") AS SLB "
            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'Sohail (21 Nov 2011) -- End

            objDataOperation.AddParameter("@TranHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, minSlabTranId)
            objDataOperation.AddParameter("@Over", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Over "))
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndPeriodDate)) 'Sohail (21 Nov 2011)

            dsSlab = objDataOperation.ExecQuery(StrQ, "Slab")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            'ISSUE : INCLUSION OF EMPLOYEE IF HE IS ACTIVE FOR SOME PERIOD BUT NOW INACTIVE
            'E.G.  : EMPLOYEE IS ACTIVE FOR JAN - MAR & TERMINATED IN APR. SO ITS NOT COMING FOR JAN TO MAR IN REPORT.

            'Sohail (21 Nov 2011) -- Start
            'Query moved on View Payroll
            'StrQ = "SELECT " & _
            '            "	 SUM(ISNULL(PAYE.basicsalary,0)) AS basicpay " & _
            '            "	,SUM(ISNULL(PAYE.allowancebenefit,0)) AS allowancebenefit " & _
            '            "	,SUM(ISNULL(PAYE.housing,0))  AS housing " & _
            '            "	,SUM(ISNULL(PAYE.basicsalary,0))+ SUM(ISNULL(PAYE.allowancebenefit,0))+ SUM(ISNULL(PAYE.housing,0)) AS Grosspay " & _
            '            "	,SUM(ISNULL(PAYE.taxpayable,0)) - SUM(ISNULL(PAYE.relief,0)) AS taxdue " & _
            '            "	,paye.empid AS empid " & _
            '            "FROM " & _
            '            "( " & _
            '            "	SELECT " & _
            '            "		 ISNULL(prpayrollprocess_tran.amount,0) as basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '            "		,0 AS housing " & _
            '            "		,0 AS deduction " & _
            '            "		,0 as taxpayable " & _
            '            "		,0 as relief " & _
            '            "		,prtnaleave_tran.employeeunkid AS empid " & _
            '            "		,prtnaleave_tran.payperiodunkid AS pid " & _
            '            "	FROM prpayrollprocess_tran " & _
            '            "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
            '            "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
            '            "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
            '            "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '            "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '            "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '            "		AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '            "		AND prtranhead_master.typeof_id= " & enTypeOf.Salary & " " & _
            '            "UNION ALL " & _
            '            "	SELECT " & _
            '            "		 0 as basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '            "		,ISNULL(prpayrollprocess_tran.amount,0) as housing " & _
            '            "		,0 AS deduction " & _
            '            "		,0 as taxpayable " & _
            '            "		,0 as relief " & _
            '            "		,prtnaleave_tran.employeeunkid AS empid " & _
            '            "		,prtnaleave_tran.payperiodunkid AS pid " & _
            '            "	FROM prpayrollprocess_tran " & _
            '            "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
            '            "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
            '            "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
            '            "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '            "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '            "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '            "		AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '            "		AND prtranhead_master.tranheadunkid=@HousingTranId " & _
            '            "UNION ALL " & _
            '            "	SELECT " & _
            '            "		 0 as basicsalary " & _
            '            "		,ISNULL(prpayrollprocess_tran.amount,0) as allowancebenefit " & _
            '            "		,0 AS housing " & _
            '            "		,0 AS deduction " & _
            '            "		,0 as taxpayable " & _
            '            "		,0 as relief " & _
            '            "		,prtnaleave_tran.employeeunkid AS empid " & _
            '            "		,prtnaleave_tran.payperiodunkid AS pid " & _
            '            "	FROM prpayrollprocess_tran " & _
            '            "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
            '            "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
            '            "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
            '            "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '            "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '            "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '            "		AND prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '            "		AND prtranhead_master.typeof_id IN (" & enTypeOf.Allowance & "," & enTypeOf.BENEFIT & ") " & _
            '            "UNION ALL " & _
            '            "	SELECT " & _
            '            "		 0 as basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '            "		,0 AS housing " & _
            '            "		,0 AS deduction " & _
            '            "		,ISNULL(prpayrollprocess_tran.amount,0) as taxpayable " & _
            '            "		,0  as relief " & _
            '            "		,prtnaleave_tran.employeeunkid AS empid " & _
            '            "		,prtnaleave_tran.payperiodunkid AS pid " & _
            '            "	FROM prpayrollprocess_tran " & _
            '            "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
            '            "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
            '            "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
            '            "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '            "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '            "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '            "		AND prtranhead_master.typeof_id = " & enTypeOf.Taxes & " " & _
            '            "UNION ALL " & _
            '            "	SELECT " & _
            '            "		 0 as basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '            "		,0 AS housing " & _
            '            "		,0 AS deduction " & _
            '            "		,0 as taxpayable " & _
            '            "		,ISNULL(prpayrollprocess_tran.amount,0) as relief " & _
            '            "		,prtnaleave_tran.employeeunkid AS empid " & _
            '            "		,prtnaleave_tran.payperiodunkid AS pid " & _
            '            "	FROM prpayrollprocess_tran " & _
            '            "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
            '            "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
            '            "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
            '            "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
            '            "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
            '            "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
            '            "		AND prtranhead_master.istaxrelief = 1 " & _
            '            ") AS PAYE " & _
            '            "WHERE pid IN (" & mstrPeriodIds & " )  " & _
            '            "GROUP BY empid "

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT  SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
            '            "	,SUM(ISNULL(PAYE.allowancebenefit,0)) AS allowancebenefit " & _
            '            "	,SUM(ISNULL(PAYE.housing,0))  AS housing " & _
            '              ", SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.allowancebenefit, 0)) " & _
            '                "+ SUM(ISNULL(PAYE.housing, 0)) AS Grosspay " & _
            '            "	,SUM(ISNULL(PAYE.taxpayable,0)) - SUM(ISNULL(PAYE.relief,0)) AS taxdue " & _
            '            "	,paye.empid AS empid " & _
            '        "FROM    ( SELECT    amount AS basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '            "		,0 AS housing " & _
            '            "		,0 AS deduction " & _
            '                          ", 0 AS taxpayable " & _
            '                          ", 0 AS relief " & _
            '                          ", employeeunkid AS empid " & _
            '                          ", payperiodunkid AS pid " & _
            '                  "FROM      vwPayroll " & _
            '                  "WHERE     trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                            "AND typeof_id = " & enTypeOf.Salary & " " & _
            '            "UNION ALL " & _
            '                  "SELECT    0 AS basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '                          ", amount AS housing " & _
            '            "		,0 AS deduction " & _
            '                          ", 0 AS taxpayable " & _
            '                          ", 0 AS relief " & _
            '                          ", employeeunkid AS empid " & _
            '                          ", payperiodunkid AS pid " & _
            '                  "FROM      vwPayroll " & _
            '                  "WHERE     trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                            "AND tranheadunkid = @HousingTranId " & _
            '            "UNION ALL " & _
            '                  "SELECT    0 AS basicsalary " & _
            '                          ", amount AS allowancebenefit " & _
            '            "		,0 AS housing " & _
            '            "		,0 AS deduction " & _
            '                          ", 0 AS taxpayable " & _
            '                          ", 0 AS relief " & _
            '                          ", employeeunkid AS empid " & _
            '                          ", payperiodunkid AS pid " & _
            '                  "FROM      vwPayroll " & _
            '                  "WHERE     trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                            "AND typeof_id IN (" & enTypeOf.Allowance & "," & enTypeOf.BENEFIT & ") " & _
            '            "UNION ALL " & _
            '                  "SELECT    0 AS basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '            "		,0 AS housing " & _
            '            "		,0 AS deduction " & _
            '                          ", amount AS taxpayable " & _
            '                          ", 0 AS relief " & _
            '                          ", employeeunkid AS empid " & _
            '                          ", payperiodunkid AS pid " & _
            '                  "FROM      vwPayroll " & _
            '                  "WHERE     typeof_id = " & enTypeOf.Taxes & " " & _
            '            "UNION ALL " & _
            '                  "SELECT    0 AS basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '            "		,0 AS housing " & _
            '            "		,0 AS deduction " & _
            '                          ", 0 AS taxpayable " & _
            '                          ", amount AS relief " & _
            '                          ", employeeunkid AS empid " & _
            '                          ", payperiodunkid AS pid " & _
            '                  "FROM      vwPayroll " & _
            '                  "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
            '                  "WHERE     prtranhead_master.istaxrelief = 1 " & _
            '            ") AS PAYE " & _
            '            "WHERE pid IN (" & mstrPeriodIds & " )  " & _
            '            "GROUP BY empid "

            'S.SANDEEP [ 09 JULY 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            'StrQ = "SELECT  SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
            '       "	,SUM(ISNULL(PAYE.allowancebenefit,0)) AS allowancebenefit " & _
            '       "	,SUM(ISNULL(PAYE.housing,0))  AS housing " & _
            '       "   ,SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.allowancebenefit, 0)) + SUM(ISNULL(PAYE.housing, 0)) AS Grosspay " & _
            '       "	,SUM(ISNULL(PAYE.taxpayable,0)) - SUM(ISNULL(PAYE.relief,0)) AS taxdue " & _
            '       "	,paye.empid AS empid " & _
            '       "FROM ( SELECT    ((CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))/" & mintPeriodCount & ")" & " AS basicsalary " & _
            '       "		,0 AS allowancebenefit " & _
            '       "		,0 AS housing " & _
            '       "		,0 AS deduction " & _
            '       "        ,0 AS taxpayable " & _
            '       "        ,0 AS relief " & _
            '       "        ,vwPayroll.employeeunkid AS empid " & _
            '       "        ,payperiodunkid AS pid "

            'Sohail (04 Sep 2013) -- Start
            'TRA - ENHANCEMENT - Changed the logic of Allowance as per Rutta's Comment discussed with Sandy. Don't calculate Basic and Housing in Allowance
            'StrQ = "SELECT  SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
            '            "	,SUM(ISNULL(PAYE.allowancebenefit,0)) AS allowancebenefit " & _
            '            "	,SUM(ISNULL(PAYE.housing,0))  AS housing " & _
            '       ", SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.housing, 0)) " & _
            '       "+ (CASE WHEN SUM(ISNULL(PAYE.allowancebenefit, 0)) <= 0 THEN 0 " & _
            '       "        WHEN ( SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.housing, 0)) ) > SUM(ISNULL(PAYE.allowancebenefit, 0)) THEN ( SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.housing, 0)) ) - SUM(ISNULL(PAYE.allowancebenefit, 0)) " & _
            '       "   ELSE SUM(ISNULL(PAYE.allowancebenefit, 0)) - ( SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.housing, 0)) ) END " & _
            '       "  ) AS Grosspay " & _
            '            "	,SUM(ISNULL(PAYE.taxpayable,0)) - SUM(ISNULL(PAYE.relief,0)) AS taxdue " & _
            '            "	,paye.empid AS empid " & _
            '       "FROM ( SELECT    ((CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))))" & " AS basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '            "		,0 AS housing " & _
            '            "		,0 AS deduction " & _
            '                          ", 0 AS taxpayable " & _
            '                          ", 0 AS relief " & _
            '                          ", vwPayroll.employeeunkid AS empid " & _
            '                          ", payperiodunkid AS pid "
            StrQ = "SELECT  SUM(ISNULL(PAYE.basicsalary, 0)) AS basicpay " & _
                        "	,SUM(ISNULL(PAYE.allowancebenefit,0)) AS allowancebenefit " & _
                        "	,SUM(ISNULL(PAYE.housing,0))  AS housing " & _
                          ", SUM(ISNULL(PAYE.basicsalary, 0)) + SUM(ISNULL(PAYE.housing, 0)) + ( SUM(ISNULL(PAYE.allowancebenefit, 0)) ) AS Grosspay " & _
                        "	,SUM(ISNULL(PAYE.taxpayable,0)) - SUM(ISNULL(PAYE.relief,0)) AS taxdue " & _
                        "	,paye.empid AS empid " & _
                   "FROM ( SELECT    ((CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))))" & " AS basicsalary " & _
                        "		,0 AS allowancebenefit " & _
                        "		,0 AS housing " & _
                        "		,0 AS deduction " & _
                                      ", 0 AS taxpayable " & _
                                      ", 0 AS relief " & _
                                      ", vwPayroll.employeeunkid AS empid " & _
                                      ", payperiodunkid AS pid "
            'Sohail (04 Sep 2013) -- End
            'S.SANDEEP [ 09 JULY 2013 ] -- END


            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            'Sohail (18 Mar 2013) -- End

            StrQ &= "FROM      vwPayroll " & _
                              "LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            StrQ &= mstrAnalysis_Join
            'Sohail (18 Mar 2013) -- End


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (04 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            'StrQ &= "WHERE     trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                            "AND typeof_id = " & enTypeOf.Salary & " " & _
            '            "UNION ALL " & _
            '                  "SELECT    0 AS basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '                          ", ((CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))))" & " AS housing " & _
            '            "		,0 AS deduction " & _
            '                          ", 0 AS taxpayable " & _
            '                          ", 0 AS relief " & _
            '                          ", vwPayroll.employeeunkid AS empid " & _
            '                          ", payperiodunkid AS pid "
            StrQ &= "WHERE     1 = 1 "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND vwPayroll.tranheadunkid = @OtherEarningTranId  "
            Else
                StrQ &= "AND vwPayroll.typeof_id = " & enTypeOf.Salary & "  "
            End If


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= "UNION ALL " & _
                              "SELECT    0 AS basicsalary " & _
                        "		,0 AS allowancebenefit " & _
                                      ", ((CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))))" & " AS housing " & _
                        "		,0 AS deduction " & _
                                      ", 0 AS taxpayable " & _
                                      ", 0 AS relief " & _
                                      ", vwPayroll.employeeunkid AS empid " & _
                                      ", payperiodunkid AS pid "
            'Sohail (04 Sep 2013) -- End

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            'Sohail (18 Mar 2013) -- End

            StrQ &= "FROM      vwPayroll " & _
                              "LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            StrQ &= mstrAnalysis_Join
            'Sohail (18 Mar 2013) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            StrQ &= "WHERE     trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                                        "AND tranheadunkid = @HousingTranId "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= "UNION ALL " & _
                              "SELECT    0 AS basicsalary " & _
                                      ", ((CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))))" & " AS allowancebenefit " & _
                        "		,0 AS housing " & _
                        "		,0 AS deduction " & _
                                      ", 0 AS taxpayable " & _
                                      ", 0 AS relief " & _
                                      ", vwPayroll.employeeunkid AS empid " & _
                                      ", payperiodunkid AS pid "

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            'Sohail (18 Mar 2013) -- End

            StrQ &= "FROM      vwPayroll " & _
                              "LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            StrQ &= mstrAnalysis_Join
            'Sohail (18 Mar 2013) -- End


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 09 JULY 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            'StrQ &= "WHERE     trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '                            "AND typeof_id IN (" & enTypeOf.Allowance & "," & enTypeOf.BENEFIT & ") " & _
            '            "UNION ALL " & _
            '                  "SELECT    0 AS basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '            "		,0 AS housing " & _
            '            "		,0 AS deduction " & _
            '                          ", ((CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))/" & mintPeriodCount & ")" & " AS taxpayable " & _
            '                          ", 0 AS relief " & _
            '                          ", vwPayroll.employeeunkid AS empid " & _
            '                          ", payperiodunkid AS pid "

            'Sohail (04 Sep 2013) -- Start
            'TRA - ENHANCEMENT - Changed the logic of Allowance as per Rutta's Comment discussed with Sandy. Don't calculate Basic and Housing in Allowance
            'StrQ &= "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
            '        "WHERE vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
            '        " AND vwPayroll.tranheadunkid NOT IN (-1) AND prtranhead_master.istaxable = 1 AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
            '            "UNION ALL " & _
            '                  "SELECT    0 AS basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '            "		,0 AS housing " & _
            '            "		,0 AS deduction " & _
            '                          ", ((CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))))" & " AS taxpayable " & _
            '                          ", 0 AS relief " & _
            '                          ", vwPayroll.employeeunkid AS empid " & _
            '                          ", payperiodunkid AS pid "
            StrQ &= "	JOIN prtranhead_master ON prtranhead_master.tranheadunkid =  vwPayroll.tranheadunkid " & _
                    "WHERE vwPayroll.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " " & _
                             "AND vwPayroll.tranheadunkid NOT IN (-1) " & _
                             "AND vwPayroll.payrollprocesstranunkid IS NOT NULL " & _
                             "AND NOT (vwPayroll.tranheadunkid = @HousingTranId AND istaxable = 1) "

            If mintOtherEarningTranId > 0 Then 'Basic Salary as Other Earning
                StrQ &= "AND NOT ( vwPayroll.tranheadunkid = @OtherEarningTranId AND istaxable = 1 ) "
            Else
                StrQ &= "AND NOT ( vwPayroll.typeof_id = " & enTypeOf.Salary & " AND istaxable = 1 ) "
            End If

            If mblnIncludeNonTaxableEarningInGrossPay = False Then
                StrQ &= "AND prtranhead_master.istaxable = 1 "
            End If


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= "UNION ALL " & _
                              "SELECT    0 AS basicsalary " & _
                        "		,0 AS allowancebenefit " & _
                        "		,0 AS housing " & _
                        "		,0 AS deduction " & _
                                      ", ((CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))))" & " AS taxpayable " & _
                                      ", 0 AS relief " & _
                                      ", vwPayroll.employeeunkid AS empid " & _
                                      ", payperiodunkid AS pid "
            'Sohail (04 Sep 2013) -- End
            'S.SANDEEP [ 09 JULY 2013 ] -- END


            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            'Sohail (18 Mar 2013) -- End

            StrQ &= "FROM      vwPayroll " & _
                              "LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            StrQ &= mstrAnalysis_Join
            'Sohail (18 Mar 2013) -- End


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [ 09 JULY 2013 ] -- START
            'ENHANCEMENT : OTHER CHANGES
            'StrQ &= "WHERE     typeof_id = " & enTypeOf.Taxes & " " & _
            '            "UNION ALL " & _
            '                  "SELECT    0 AS basicsalary " & _
            '            "		,0 AS allowancebenefit " & _
            '            "		,0 AS housing " & _
            '            "		,0 AS deduction " & _
            '                          ", 0 AS taxpayable " & _
            '                          ", ((CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")))/" & mintPeriodCount & ")" & " AS relief " & _
            '                          ", vwPayroll.employeeunkid AS empid " & _
            '                          ", payperiodunkid AS pid "

            StrQ &= "WHERE     typeof_id = " & enTypeOf.Taxes & " "


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''Pinkal (15-Oct-2014) -- Start
            ''Enhancement -  CHANGES FOR AKFTZ - PUTTING USER ACCESS LEVEL IN ALL STATUTORY REPORTS AS PER MR.RUTTA' S COMMENT
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= UserAccessLevel._AccessLevelFilterString
            'End If
            ''Pinkal (15-Oct-2014) -- End

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= "UNION ALL " & _
                              "SELECT    0 AS basicsalary " & _
                        "		,0 AS allowancebenefit " & _
                        "		,0 AS housing " & _
                        "		,0 AS deduction " & _
                                      ", 0 AS taxpayable " & _
                                      ", ((CAST(amount AS DECIMAL(36, " & decDecimalPlaces & "))))" & " AS relief " & _
                                      ", vwPayroll.employeeunkid AS empid " & _
                                      ", payperiodunkid AS pid "
            'S.SANDEEP [ 09 JULY 2013 ] -- END




            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If
            'Sohail (18 Mar 2013) -- End

            StrQ &= "FROM      vwPayroll " & _
                              "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = vwPayroll.tranheadunkid " & _
                              "LEFT JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

            'Sohail (18 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            StrQ &= mstrAnalysis_Join
            'Sohail (18 Mar 2013) -- End

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [04 JUN 2015] -- END


            StrQ &= "WHERE     prtranhead_master.istaxrelief = 1 " & _
                        ") AS PAYE " & _
                        "WHERE pid IN (" & mstrPeriodIds & " )  " & _
                     "GROUP BY empid "
            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'Sohail (21 Nov 2011) -- End
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport
            Dim rpt_Row As DataRow = Nothing
            Dim intCnt As Integer = 0
            Dim intCntTotalSlab As Integer = 0
            Dim intTotalEmployee As Integer = 0
            Dim intEndLoop As Integer = 0

            If dsPeriodicData.Tables(0).Rows.Count > 9 Then
                intEndLoop = dsPeriodicData.Tables(0).Rows.Count
            Else
                intEndLoop = 11
            End If

            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            Dim decColumn3Total As Decimal = 0
            'S.SANDEEP [ 05 JULY 2011 ] -- END 

            For i As Integer = 0 To intEndLoop
                rpt_Row = rpt_Data.Tables(0).NewRow

                If dsPeriodicData.Tables(0).Rows.Count > i Then
                    rpt_Row.Item("Column1") = dsPeriodicData.Tables(0).Rows(i).Item("PeriodName")
                    rpt_Row.Item("Column2") = ConfigParameter._Object._Currency
                    rpt_Row.Item("Column3") = Format(CDec(dsPeriodicData.Tables(0).Rows(i).Item("Net_Tax_Due")), GUI.fmtCurrency)
                    decColumn3Total = decColumn3Total + CDec(dsPeriodicData.Tables(0).Rows(i).Item("Net_Tax_Due"))
                End If

                Select Case intCnt
                    Case 0
                        rpt_Row.Item("Column4") = ""
                        rpt_Row.Item("Column5") = ""
                        rpt_Row.Item("Column6") = ""
                        rpt_Row.Item("Column7") = ""
                        rpt_Row.Item("Column8") = ""
                    Case 1
                        rpt_Row.Item("Column4") = ""
                        rpt_Row.Item("Column5") = Language.getMessage(mstrModuleName, 37, "Additional Information") & vbCrLf & "______________"
                        rpt_Row.Item("Column6") = ""
                        rpt_Row.Item("Column7") = ""
                        rpt_Row.Item("Column8") = ""
                    Case 2
                        rpt_Row.Item("Column4") = Language.getMessage(mstrModuleName, 38, "Income Range") & vbCrLf & "_____________" & vbCrLf & ConfigParameter._Object._Currency
                        rpt_Row.Item("Column5") = Language.getMessage(mstrModuleName, 39, "Number Of" & vbCrLf & "Employees")
                        rpt_Row.Item("Column6") = Language.getMessage(mstrModuleName, 40, "Wages" & vbCrLf & "Bill")
                        rpt_Row.Item("Column7") = Language.getMessage(mstrModuleName, 41, "Total Tax" & vbCrLf & "Paid Per Pg")
                        rpt_Row.Item("Column8") = Language.getMessage(mstrModuleName, 42, "Percentage Of" & vbCrLf & "Total Tax")
                End Select
                rpt_Data.Tables(0).Rows.Add(rpt_Row)
                intCnt += 1

            Next

            intCnt = 3
            intCntTotalSlab = dsSlab.Tables(0).Rows.Count
            For Each dtSRow As DataRow In dsSlab.Tables(0).Rows
                rpt_Data.Tables(0).Rows(intCnt)("Column4") = dtSRow.Item("SLAB")
                intCnt += 1
            Next

            Dim mdicEmployeeCounter As New Dictionary(Of String, Integer)
            Dim mdicEmployeeWages As New Dictionary(Of String, Decimal)
            Dim mdicEmployeeTax As New Dictionary(Of String, Decimal)

            For Each dsRow As DataRow In dsList.Tables(0).Rows
                Dim dtTemp() As DataRow = Nothing
                dtTemp = dsSlab.Tables(0).Select(dsRow.Item("Grosspay") & " >= inexcessof AND " & dsRow.Item("Grosspay") / mintPeriodCount & "<= amountupto")
                If dtTemp.Length > 0 Then
                    If mdicEmployeeCounter.ContainsKey(dtTemp(0)("SLAB")) Then
                        mdicEmployeeCounter(dtTemp(0)("SLAB")) += 1
                    Else
                        mdicEmployeeCounter.Add(dtTemp(0)("SLAB"), 1)
                    End If
                End If
            Next

            Dim drTemp() As DataRow = dsList.Tables(0).Select("basicpay > 0")
            intTotalEmployee = drTemp.Length
            Dim intRowIdx As Integer = -1
            Dim dblPercentage As Decimal = 0
            Dim dblWagesTotal As Decimal = 0
            Dim dblTaxTotal As Decimal = 0
            Dim rptDRow() As DataRow = Nothing

            For Each dsRow As DataRow In dsList.Tables(0).Rows
                Dim dtTemp() As DataRow = Nothing
                dtTemp = dsSlab.Tables(0).Select(dsRow.Item("Grosspay") & " >= inexcessof AND " & dsRow.Item("Grosspay") / mintPeriodCount & "<= amountupto")
                If dtTemp.Length > 0 Then
                    rptDRow = rpt_Data.Tables(0).Select("Column4= '" & dtTemp(0)("SLAB") & "'")
                    If rptDRow.Length > 0 Then
                        intRowIdx = rpt_Data.Tables(0).Rows.IndexOf(rptDRow(0))
                        If IsDBNull(rpt_Data.Tables(0).Rows(intRowIdx)("Column5")) Then
                            rpt_Data.Tables(0).Rows(intRowIdx)("Column5") = 1
                        Else
                            rpt_Data.Tables(0).Rows(intRowIdx)("Column5") += 1
                        End If
                        If IsDBNull(rpt_Data.Tables(0).Rows(intRowIdx)("Column6")) Then
                            rpt_Data.Tables(0).Rows(intRowIdx)("Column6") = Format(CDec(dsRow.Item("Grosspay")), GUI.fmtCurrency)
                        Else
                            rpt_Data.Tables(0).Rows(intRowIdx)("Column6") = Format(CDec(rpt_Data.Tables(0).Rows(intRowIdx)("Column6")) + CDec(dsRow.Item("Grosspay")), GUI.fmtCurrency)
                        End If
                        dblWagesTotal = Format(CDec(dblWagesTotal) + CDec(dsRow.Item("Grosspay")), GUI.fmtCurrency)

                        If IsDBNull(rpt_Data.Tables(0).Rows(intRowIdx)("Column7")) Then
                            rpt_Data.Tables(0).Rows(intRowIdx)("Column7") = Format(CDec(dsRow.Item("taxdue")), GUI.fmtCurrency)
                        Else
                            rpt_Data.Tables(0).Rows(intRowIdx)("Column7") = Format(CDec(rpt_Data.Tables(0).Rows(intRowIdx)("Column7")) + CDec(dsRow.Item("taxdue")), GUI.fmtCurrency)
                        End If
                        dblTaxTotal = CDec(dblTaxTotal) + CDec(Format(CDec(dsRow.Item("taxdue")), GUI.fmtCurrency))

                        If rpt_Data.Tables(0).Rows(intRowIdx)("Column8").ToString = "" Then
                            If mdicEmployeeCounter.ContainsKey(dtTemp(0)("SLAB")) Then
                                'S.SANDEEP [04 JUN 2015] -- START
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'ISSUE FOUND : BASIC PAY WAS COMING ZERO AND THROWING ERROR "DIVISION BY ZERO". THIS MAY HAPPEN DUE TO "SALARY HEAD IS INFORMATIONAL".
                                'rpt_Data.Tables(0).Rows(intRowIdx)("Column8") = Format(CDec(mdicEmployeeCounter(dtTemp(0)("SLAB")) * 100 / intTotalEmployee), "###.##")
                                If intTotalEmployee > 0 Then
                                    rpt_Data.Tables(0).Rows(intRowIdx)("Column8") = Format(CDec(mdicEmployeeCounter(dtTemp(0)("SLAB")) * 100 / intTotalEmployee), "###.##")
                                Else
                                    rpt_Data.Tables(0).Rows(intRowIdx)("Column8") = "0.00"
                                End If
                                'S.SANDEEP [04 JUN 2015] -- END
                            Else
                                rpt_Data.Tables(0).Rows(intRowIdx)("Column8") = "0.00"
                            End If
                            dblPercentage = dblPercentage + CDec(rpt_Data.Tables(0).Rows(intRowIdx)("Column8"))
                        End If
                    End If
                End If
            Next

            objRpt = New ArutiReport.Designer.rptPayeEmployerCertificate_P10

            objRpt.SetDataSource(rpt_Data)


            Call ReportFunction.TextChange(objRpt, "txtFormNo", Language.getMessage(mstrModuleName, 2, "FORM     P. 10"))
            Call ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 3, "TANZANIA   REVENUE   AUTHORITY"))
            Call ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 4, "P.A.Y.E - EMPLOYER'S END OF YEAR CERTIFICATE"))
            Call ReportFunction.TextChange(objRpt, "txtHeading3", Language.getMessage(mstrModuleName, 5, "(Section 84(2) of the Income Tax at 2004)"))
            Call ReportFunction.TextChange(objRpt, "lblEmployer", Language.getMessage(mstrModuleName, 6, "Employer's"))
            Call ReportFunction.TextChange(objRpt, "lblTinNo", Language.getMessage(mstrModuleName, 7, "TIN  No."))
            Call ReportFunction.TextChange(objRpt, "txtTinNo", Company._Object._Tinno)
            Call ReportFunction.TextChange(objRpt, "lblSalutation", Language.getMessage(mstrModuleName, 8, "To Regional/ District Revenue Officer"))
            Call ReportFunction.TextChange(objRpt, "lblYear", Language.getMessage(mstrModuleName, 9, "Year"))
            Call ReportFunction.TextChange(objRpt, "txtYear", FinancialYear._Object._FinancialYear_Name)
            Call ReportFunction.TextChange(objRpt, "lblOfficalUse", Language.getMessage(mstrModuleName, 10, "For Official Use"))
            Call ReportFunction.TextChange(objRpt, "lblStatisticalCode", Language.getMessage(mstrModuleName, 11, "Statistical Coding"))
            Call ReportFunction.TextChange(objRpt, "lblDepartment", Language.getMessage(mstrModuleName, 12, "Income Tax Department"))
            Call ReportFunction.TextChange(objRpt, "lblPOBox", Language.getMessage(mstrModuleName, 13, "P.O. Box"))
            Call ReportFunction.TextChange(objRpt, "txtPostBox", "")
            Call ReportFunction.TextChange(objRpt, "lblOrganization", Language.getMessage(mstrModuleName, 14, "Name of Organization/Institution"))
            Call ReportFunction.TextChange(objRpt, "txtOrganization", Company._Object._Name)
            Call ReportFunction.TextChange(objRpt, "lblNatureOfBusiness", Language.getMessage(mstrModuleName, 15, "Nature of Business "))
            Call ReportFunction.TextChange(objRpt, "txtNatureOfBusiness", "")
            Call ReportFunction.TextChange(objRpt, "lblPartnership", Language.getMessage(mstrModuleName, 16, "(State whether Parastatal/Other Compnay/Corporation or Partnership)"))
            Call ReportFunction.TextChange(objRpt, "txtPartnership", "")
            Call ReportFunction.TextChange(objRpt, "lblDeductionStatement", Language.getMessage(mstrModuleName, 17, "I/We forward herewith a statement of payments made and tax deducted during the six months"))
            Call ReportFunction.TextChange(objRpt, "lblforward", Language.getMessage(mstrModuleName, 18, "I/We forward herewith"))
            Call ReportFunction.TextChange(objRpt, "txtNoOfEmployee", intTotalEmployee)
            Call ReportFunction.TextChange(objRpt, "lblTaxDeductionCard", Language.getMessage(mstrModuleName, 19, "Tax Deduction Cards *Form P9:  (GE) *P9 (ZE) "))
            Call ReportFunction.TextChange(objRpt, "lblPeriodEnded", Language.getMessage(mstrModuleName, 20, "under period ended 31"))
            Call ReportFunction.TextChange(objRpt, "txtPeriodEnded", mstrEndPeriodName)
            Call ReportFunction.TextChange(objRpt, "lblTaxCalenderYear", Language.getMessage(mstrModuleName, 21, "Total tax for the calendar year (as listed) amount to ") & " " & ConfigParameter._Object._Currency)
            Call ReportFunction.TextChange(objRpt, "txtTaxCalenderYear", mstrEndPeriodName)
            Call ReportFunction.TextChange(objRpt, "lblAnnualWagesRounded", Language.getMessage(mstrModuleName, 22, "Annual Wages rounded up to nearest shilling paid per payroll including wages not"))
            Call ReportFunction.TextChange(objRpt, "lblSubject", Language.getMessage(mstrModuleName, 23, "Subject to P.A.Y.E. ") & " " & ConfigParameter._Object._Currency)
            Call ReportFunction.TextChange(objRpt, "txtSubject", dblWagesTotal.ToString(GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "lblCertified", Language.getMessage(mstrModuleName, 24, "I/We certify that the particulars entered by us/me on these forms are correct"))
            Call ReportFunction.TextChange(objRpt, "lblEmployerSignature", Language.getMessage(mstrModuleName, 25, "Signature of Employer/Paying Office"))
            Call ReportFunction.TextChange(objRpt, "lblBranch", Language.getMessage(mstrModuleName, 26, "Branch"))
            Call ReportFunction.TextChange(objRpt, "lblPostalAddress", Language.getMessage(mstrModuleName, 27, "Postal Address"))
            Call ReportFunction.TextChange(objRpt, "lblPhysicalAddress", Language.getMessage(mstrModuleName, 28, "Physical Address"))
            Call ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 29, "Date"))
            Call ReportFunction.TextChange(objRpt, "lblNotes", Language.getMessage(mstrModuleName, 30, "NOTE:"))
            Call ReportFunction.TextChange(objRpt, "lblIncomeRange", Language.getMessage(mstrModuleName, 31, "1)  For Income Range Groupings take the final month pay figure."))
            Call ReportFunction.TextChange(objRpt, "lblTaxPerAnnum", Language.getMessage(mstrModuleName, 32, "2)  For Tax paid per annum, take actaul Tax deducted and not the hypothetical tax that would have been deducted."))
            Call ReportFunction.TextChange(objRpt, "lblDuplicate", Language.getMessage(mstrModuleName, 33, "3)  To be submitted in DUPLICATE to Income Tax Office with the required particulars on or before 31 January of the year following."))
            Call ReportFunction.TextChange(objRpt, "lblOriginalWhite", Language.getMessage(mstrModuleName, 34, "Original (white)"))
            Call ReportFunction.TextChange(objRpt, "lblDuplicateYellow", Language.getMessage(mstrModuleName, 35, "Duplicate (Yellow). To Income Tax Office"))
            Call ReportFunction.TextChange(objRpt, "lblTriplicatePink", Language.getMessage(mstrModuleName, 43, "Triplicate (Pink)   . To be retained by the employer"))
            Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 36, "Total"))

            Call ReportFunction.TextChange(objRpt, "txtColumn5", intTotalEmployee)
            Call ReportFunction.TextChange(objRpt, "txtColumn6", Format(CDec(dblWagesTotal), GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtColumn7", Format(CDec(dblTaxTotal), GUI.fmtCurrency))
            Call ReportFunction.TextChange(objRpt, "txtColumn8", CDbl(System.Math.Round(dblPercentage)))

            'S.SANDEEP [ 05 JULY 2011 ] -- START
            'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
            'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot31")
            Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn3Total, GUI.fmtCurrency))
            'S.SANDEEP [ 05 JULY 2011 ] -- END 

            Return objRpt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim dsPeriodicData As New DataSet
    '    Dim exForce As Exception
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try
    '        objDataOperation = New clsDataOperation

    '        StrQ = "SELECT " & _
    '                    "	 PeriodId AS PeriodId " & _
    '                    "	,PeriodName AS PeriodName " & _
    '                    "	,TaxPayable AS TaxPayable " & _
    '                    "	,LPR AS Less_Personal_Relief " & _
    '                    "	,NTD AS Net_Tax_Due " & _
    '                    "FROM " & _
    '                    "	( " & _
    '                    "		SELECT " & _
    '                    "			 Period.PeriodId " & _
    '                    "			,Period.PeriodName " & _
    '                    "			,StDate AS StDate " & _
    '                    "			,SUM(ISNULL(TaxPayable,0)) AS TaxPayable " & _
    '                    "			,SUM(ISNULL(LPR,0)) AS LPR " & _
    '                    "			,SUM(ISNULL(TaxPayable,0)) -SUM(ISNULL(LPR,0)) AS NTD " & _
    '                    "		FROM " & _
    '                    "			( " & _
    '                    "				SELECT " & _
    '                    "					 periodunkid AS PeriodId " & _
    '                    "					,period_name AS PeriodName " & _
    '                    "					,start_date AS StDate " & _
    '                    "				FROM cfcommon_period_tran " & _
    '                    "				WHERE isactive = 1 AND modulerefid = 1 " & _
    '                    "					AND cfcommon_period_tran.periodunkid IN (" & mstrPeriodIds & ")  " & _
    '                    "			) AS Period " & _
    '                    "		LEFT JOIN " & _
    '                    "			( " & _
    '                    "				SELECT " & _
    '                    "					 SUM(ISNULL(prpayrollprocess_tran.amount,0)) AS TaxPayable " & _
    '                    "					,0 AS LPR " & _
    '                    "					,prtnaleave_tran.payperiodunkid AS Pid " & _
    '                    "				FROM prpayrollprocess_tran " & _
    '                    "					JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "					JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                    "                   JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                    "				WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "					AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "					AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                    "					AND prtranhead_master.typeof_id = 9 " & _
    '                    "					AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ")  "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'If mblnIncludeInactiveEmp = False Then
    '        '    StrQ &= " AND hremployee_master.isactive = 1 "
    '        'End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End
    '        StrQ &= "				GROUP BY prtnaleave_tran.payperiodunkid " & _
    '                    "		UNION  ALL " & _
    '                    "				SELECT " & _
    '                    "					 0 AS TaxPayable " & _
    '                    "					,SUM(ISNULL(prpayrollprocess_tran.amount,0)) AS LPR " & _
    '                    "					,prtnaleave_tran.payperiodunkid AS Pid " & _
    '                    "				FROM prpayrollprocess_tran " & _
    '                    "					JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "					JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                    "                   JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                    "				WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "					AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "					AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                    "					AND ISNULL(prtranhead_master.istaxrelief,0)=1 " & _
    '                    "					AND prtnaleave_tran.payperiodunkid IN (" & mstrPeriodIds & ")  "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'If mblnIncludeInactiveEmp = False Then
    '        '    StrQ &= " AND hremployee_master.isactive = 1 "
    '        'End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "				GROUP BY prtnaleave_tran.payperiodunkid " & _
    '                    "			) AS basicpay ON basicpay.Pid=Period.PeriodId " & _
    '                    "			GROUP BY " & _
    '                    "				 Period.PeriodId " & _
    '                    "				,Period.PeriodName " & _
    '                    "				,StDate " & _
    '                    "	) AS ITD "

    '        Call FilterTitleAndFilterQuery()

    '        dsPeriodicData = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dsSlab As New DataSet

    '        Dim StrDecPoint() As String = GUI.fmtCurrency.ToString.Split(".")

    '        Dim iCnt As Integer = StrDecPoint(1).Length

    '        StrQ = "SELECT " & _
    '                    "	CASE WHEN	SrNo < Cnt AND  SrNo = 1 THEN CONVERT(NVARCHAR(50), inexcessof )+' - ' + CONVERT(NVARCHAR(50),amountupto) " & _
    '                    "			 WHEN SrNo < Cnt THEN CONVERT(NVARCHAR(50),inexcessof +1 )+' - ' + CONVERT(NVARCHAR(50), amountupto) " & _
    '                    "			 WHEN SrNo = Cnt THEN @Over + CONVERT(NVARCHAR(50),inexcessof ) " & _
    '                    "	 END AS SLAB " & _
    '                    "	,CASE WHEN SrNo = 1 THEN inexcessof ELSE inexcessof+1 END AS inexcessof " & _
    '                    "	,amountupto " & _
    '                    "   ,SrNo As RangeNo " & _
    '                    "FROM " & _
    '                    "( " & _
    '                    "	SELECT " & _
    '                    "		 ROW_NUMBER() OVER (ORDER BY inexcessof ASC)AS SrNo " & _
    '                    "		, convert(Decimal(30," & iCnt & "),inexcessof) as inexcessof " & _
    '                    "		, convert(Decimal(30," & iCnt & "),amountupto) as amountupto " & _
    '                    "		,(SELECT COUNT(prtranhead_inexcessslab_tran.tranheadtaxslabtranunkid) FROM prtranhead_inexcessslab_tran WHERE tranheadunkid = @TranHeadId AND isvoid = 0) AS Cnt " & _
    '                    "	FROM prtranhead_inexcessslab_tran WHERE tranheadunkid = @TranHeadId AND isvoid = 0 " & _
    '                    ") AS SLB "

    '        objDataOperation.AddParameter("@TranHeadId", SqlDbType.Int, eZeeDataType.INT_SIZE, minSlabTranId)
    '        objDataOperation.AddParameter("@Over", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Over "))

    '        dsSlab = objDataOperation.ExecQuery(StrQ, "Slab")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If

    '        StrQ = "SELECT " & _
    '                    "	 SUM(ISNULL(PAYE.basicsalary,0)) AS basicpay " & _
    '                    "	,SUM(ISNULL(PAYE.allowancebenefit,0)) AS allowancebenefit " & _
    '                    "	,SUM(ISNULL(PAYE.housing,0))  AS housing " & _
    '                    "	,SUM(ISNULL(PAYE.basicsalary,0))+ SUM(ISNULL(PAYE.allowancebenefit,0))+ SUM(ISNULL(PAYE.housing,0)) AS Grosspay " & _
    '                    "	,SUM(ISNULL(PAYE.taxpayable,0)) - SUM(ISNULL(PAYE.relief,0)) AS taxdue " & _
    '                    "	,paye.empid AS empid " & _
    '                    "FROM " & _
    '                    "( " & _
    '                    "	SELECT " & _
    '                    "		 ISNULL(prpayrollprocess_tran.amount,0) as basicsalary " & _
    '                    "		,0 AS allowancebenefit " & _
    '                    "		,0 AS housing " & _
    '                    "		,0 AS deduction " & _
    '                    "		,0 as taxpayable " & _
    '                    "		,0 as relief " & _
    '                    "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                    "		,prtnaleave_tran.payperiodunkid AS pid " & _
    '                    "	FROM prpayrollprocess_tran " & _
    '                    "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                    "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                    "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                    "		AND prtranhead_master.trnheadtype_id =1 " & _
    '                    "		AND prtranhead_master.typeof_id=1 "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'If mblnIncludeInactiveEmp = False Then
    '        '    StrQ &= " AND hremployee_master.isactive = 1 "
    '        'End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "UNION ALL " & _
    '                    "	SELECT " & _
    '                    "		 0 as basicsalary " & _
    '                    "		,0 AS allowancebenefit " & _
    '                    "		,ISNULL(prpayrollprocess_tran.amount,0) as housing " & _
    '                    "		,0 AS deduction " & _
    '                    "		,0 as taxpayable " & _
    '                    "		,0 as relief " & _
    '                    "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                    "		,prtnaleave_tran.payperiodunkid AS pid " & _
    '                    "	FROM prpayrollprocess_tran " & _
    '                    "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                    "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                    "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                    "		AND prtranhead_master.trnheadtype_id = 1 " & _
    '                    "		AND prtranhead_master.tranheadunkid=@HousingTranId "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'If mblnIncludeInactiveEmp = False Then
    '        '    StrQ &= " AND hremployee_master.isactive = 1 "
    '        'End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If

    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "UNION ALL " & _
    '                    "	SELECT " & _
    '                    "		 0 as basicsalary " & _
    '                    "		,ISNULL(prpayrollprocess_tran.amount,0) as allowancebenefit " & _
    '                    "		,0 AS housing " & _
    '                    "		,0 AS deduction " & _
    '                    "		,0 as taxpayable " & _
    '                    "		,0 as relief " & _
    '                    "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                    "		,prtnaleave_tran.payperiodunkid AS pid " & _
    '                    "	FROM prpayrollprocess_tran " & _
    '                    "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                    "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                    "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                    "		AND prtranhead_master.trnheadtype_id =1 " & _
    '                    "		AND prtranhead_master.typeof_id IN (2,5) "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'If mblnIncludeInactiveEmp = False Then
    '        '    StrQ &= " AND hremployee_master.isactive = 1 "
    '        'End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "UNION ALL " & _
    '                    "	SELECT " & _
    '                    "		 0 as basicsalary " & _
    '                    "		,0 AS allowancebenefit " & _
    '                    "		,0 AS housing " & _
    '                    "		,0 AS deduction " & _
    '                    "		,ISNULL(prpayrollprocess_tran.amount,0) as taxpayable " & _
    '                    "		,0  as relief " & _
    '                    "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                    "		,prtnaleave_tran.payperiodunkid AS pid " & _
    '                    "	FROM prpayrollprocess_tran " & _
    '                    "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                    "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                    "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                    "		AND prtranhead_master.typeof_id=9 "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'If mblnIncludeInactiveEmp = False Then
    '        '    StrQ &= " AND hremployee_master.isactive = 1 "
    '        'End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= "UNION ALL " & _
    '                    "	SELECT " & _
    '                    "		 0 as basicsalary " & _
    '                    "		,0 AS allowancebenefit " & _
    '                    "		,0 AS housing " & _
    '                    "		,0 AS deduction " & _
    '                    "		,0 as taxpayable " & _
    '                    "		,ISNULL(prpayrollprocess_tran.amount,0) as relief " & _
    '                    "		,prtnaleave_tran.employeeunkid AS empid " & _
    '                    "		,prtnaleave_tran.payperiodunkid AS pid " & _
    '                    "	FROM prpayrollprocess_tran " & _
    '                    "		JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid=prpayrollprocess_tran.tnaleavetranunkid " & _
    '                    "		JOIN prtranhead_master ON prtranhead_master.tranheadunkid=prpayrollprocess_tran.tranheadunkid " & _
    '                    "       JOIN hremployee_master ON hremployee_master.employeeunkid=prpayrollprocess_tran.employeeunkid " & _
    '                    "	WHERE ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                    "		AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                    "		AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                    "		AND prtranhead_master.istaxrelief=1 "

    '        'Sohail (28 Jun 2011) -- Start
    '        'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '        'If mblnIncludeInactiveEmp = False Then
    '        '    StrQ &= " AND hremployee_master.isactive = 1 "
    '        'End If

    '        'Issue : According to prvilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        End If
    '        'Sohail (28 Jun 2011) -- End

    '        StrQ &= ") AS PAYE " & _
    '                    "WHERE pid IN (" & mstrPeriodIds & " )  " & _
    '                    "GROUP BY empid "

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        rpt_Data = New ArutiReport.Designer.dsArutiReport
    '        Dim rpt_Row As DataRow = Nothing
    '        Dim intCnt As Integer = 0
    '        Dim intCntTotalSlab As Integer = 0
    '        Dim intTotalEmployee As Integer = 0
    '        Dim intEndLoop As Integer = 0

    '        If dsPeriodicData.Tables(0).Rows.Count > 9 Then
    '            intEndLoop = dsPeriodicData.Tables(0).Rows.Count
    '        Else
    '            intEndLoop = 11
    '        End If

    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        Dim decColumn3Total As Decimal = 0
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 

    '        For i As Integer = 0 To intEndLoop
    '            rpt_Row = rpt_Data.Tables(0).NewRow

    '            If dsPeriodicData.Tables(0).Rows.Count > i Then
    '                rpt_Row.Item("Column1") = dsPeriodicData.Tables(0).Rows(i).Item("PeriodName")
    '                rpt_Row.Item("Column2") = ConfigParameter._Object._Currency
    '                rpt_Row.Item("Column3") = Format(CDec(dsPeriodicData.Tables(0).Rows(i).Item("Net_Tax_Due")), GUI.fmtCurrency)
    '                decColumn3Total = decColumn3Total + CDec(dsPeriodicData.Tables(0).Rows(i).Item("Net_Tax_Due"))
    '            End If

    '            Select Case intCnt
    '                Case 0
    '                    rpt_Row.Item("Column4") = ""
    '                    rpt_Row.Item("Column5") = ""
    '                    rpt_Row.Item("Column6") = ""
    '                    rpt_Row.Item("Column7") = ""
    '                    rpt_Row.Item("Column8") = ""
    '                Case 1
    '                    rpt_Row.Item("Column4") = ""
    '                    rpt_Row.Item("Column5") = Language.getMessage(mstrModuleName, 37, "Additional Information") & vbCrLf & "______________"
    '                    rpt_Row.Item("Column6") = ""
    '                    rpt_Row.Item("Column7") = ""
    '                    rpt_Row.Item("Column8") = ""
    '                Case 2
    '                    rpt_Row.Item("Column4") = Language.getMessage(mstrModuleName, 38, "Income Range") & vbCrLf & "_____________" & vbCrLf & ConfigParameter._Object._Currency
    '                    rpt_Row.Item("Column5") = Language.getMessage(mstrModuleName, 39, "Number Of" & vbCrLf & "Employee")
    '                    rpt_Row.Item("Column6") = Language.getMessage(mstrModuleName, 40, "Wages" & vbCrLf & "Bill")
    '                    rpt_Row.Item("Column7") = Language.getMessage(mstrModuleName, 41, "Total Tax" & vbCrLf & "Paid Per Pg")
    '                    rpt_Row.Item("Column8") = Language.getMessage(mstrModuleName, 42, "Percentage Of" & vbCrLf & "Total Tax")
    '            End Select
    '            rpt_Data.Tables(0).Rows.Add(rpt_Row)
    '            intCnt += 1

    '        Next

    '        intCnt = 3
    '        intCntTotalSlab = dsSlab.Tables(0).Rows.Count
    '        For Each dtSRow As DataRow In dsSlab.Tables(0).Rows
    '            rpt_Data.Tables(0).Rows(intCnt)("Column4") = dtSRow.Item("SLAB")
    '            intCnt += 1
    '        Next

    '        Dim mdicEmployeeCounter As New Dictionary(Of String, Integer)
    '        Dim mdicEmployeeWages As New Dictionary(Of String, Decimal)
    '        Dim mdicEmployeeTax As New Dictionary(Of String, Decimal)

    '        For Each dsRow As DataRow In dsList.Tables(0).Rows
    '            Dim dtTemp() As DataRow = Nothing
    '            dtTemp = dsSlab.Tables(0).Select(dsRow.Item("Grosspay") & " >= inexcessof AND " & dsRow.Item("Grosspay") & "<= amountupto")
    '            If dtTemp.Length > 0 Then
    '                If mdicEmployeeCounter.ContainsKey(dtTemp(0)("SLAB")) Then
    '                    mdicEmployeeCounter(dtTemp(0)("SLAB")) += 1
    '                Else
    '                    mdicEmployeeCounter.Add(dtTemp(0)("SLAB"), 1)
    '                End If
    '            End If
    '        Next

    '        intTotalEmployee = dsList.Tables(0).Rows.Count
    '        Dim intRowIdx As Integer = -1
    '        Dim dblPercentage As Decimal = 0
    '        Dim dblWagesTotal As Decimal = 0
    '        Dim dblTaxTotal As Decimal = 0
    '        Dim rptDRow() As DataRow = Nothing

    '        For Each dsRow As DataRow In dsList.Tables(0).Rows
    '            Dim dtTemp() As DataRow = Nothing
    '            dtTemp = dsSlab.Tables(0).Select(dsRow.Item("Grosspay") & " >= inexcessof AND " & dsRow.Item("Grosspay") & "<= amountupto")
    '            If dtTemp.Length > 0 Then
    '                rptDRow = rpt_Data.Tables(0).Select("Column4= '" & dtTemp(0)("SLAB") & "'")
    '                If rptDRow.Length > 0 Then
    '                    intRowIdx = rpt_Data.Tables(0).Rows.IndexOf(rptDRow(0))
    '                    If IsDBNull(rpt_Data.Tables(0).Rows(intRowIdx)("Column5")) Then
    '                        rpt_Data.Tables(0).Rows(intRowIdx)("Column5") = 1
    '                    Else
    '                        rpt_Data.Tables(0).Rows(intRowIdx)("Column5") += 1
    '                    End If
    '                    If IsDBNull(rpt_Data.Tables(0).Rows(intRowIdx)("Column6")) Then
    '                        rpt_Data.Tables(0).Rows(intRowIdx)("Column6") = Format(CDec(dsRow.Item("Grosspay")), GUI.fmtCurrency)
    '                    Else
    '                        rpt_Data.Tables(0).Rows(intRowIdx)("Column6") = Format(CDec(rpt_Data.Tables(0).Rows(intRowIdx)("Column6")) + CDec(dsRow.Item("Grosspay")), GUI.fmtCurrency)
    '                    End If
    '                    dblWagesTotal = Format(CDec(dblWagesTotal) + CDec(dsRow.Item("Grosspay")), GUI.fmtCurrency)

    '                    If IsDBNull(rpt_Data.Tables(0).Rows(intRowIdx)("Column7")) Then
    '                        rpt_Data.Tables(0).Rows(intRowIdx)("Column7") = Format(CDec(dsRow.Item("taxdue")), GUI.fmtCurrency)
    '                    Else
    '                        rpt_Data.Tables(0).Rows(intRowIdx)("Column7") = Format(CDec(rpt_Data.Tables(0).Rows(intRowIdx)("Column7")) + CDec(dsRow.Item("taxdue")), GUI.fmtCurrency)
    '                    End If
    '                    dblTaxTotal = CDec(dblTaxTotal) + CDec(Format(CDec(dsRow.Item("taxdue")), GUI.fmtCurrency))

    '                    If rpt_Data.Tables(0).Rows(intRowIdx)("Column8").ToString = "" Then
    '                        If mdicEmployeeCounter.ContainsKey(dtTemp(0)("SLAB")) Then
    '                            rpt_Data.Tables(0).Rows(intRowIdx)("Column8") = Format(CDec(mdicEmployeeCounter(dtTemp(0)("SLAB")) * 100 / intTotalEmployee), "###.##")
    '                        Else
    '                            rpt_Data.Tables(0).Rows(intRowIdx)("Column8") = "0.00"
    '                        End If
    '                        dblPercentage = dblPercentage + CDec(rpt_Data.Tables(0).Rows(intRowIdx)("Column8"))
    '                    End If
    '                End If
    '            End If
    '        Next

    '        objRpt = New ArutiReport.Designer.rptPayeEmployerCertificate_P10

    '        objRpt.SetDataSource(rpt_Data)


    '        Call ReportFunction.TextChange(objRpt, "txtFormNo", Language.getMessage(mstrModuleName, 2, "FORM     P. 10"))
    '        Call ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 3, "TANZANIA   REVENUE   AUTHORITY"))
    '        Call ReportFunction.TextChange(objRpt, "txtHeading1", Language.getMessage(mstrModuleName, 4, "P.A.Y.E - EMPLOYER'S END OF YEAR CERTIFICATE"))
    '        Call ReportFunction.TextChange(objRpt, "txtHeading3", Language.getMessage(mstrModuleName, 5, "(Section 84(2) of the Income Tax at 2004)"))
    '        Call ReportFunction.TextChange(objRpt, "lblEmployer", Language.getMessage(mstrModuleName, 6, "Employer's"))
    '        Call ReportFunction.TextChange(objRpt, "lblTinNo", Language.getMessage(mstrModuleName, 7, "TIN  No."))
    '        Call ReportFunction.TextChange(objRpt, "txtTinNo", Company._Object._Tinno)
    '        Call ReportFunction.TextChange(objRpt, "lblSalutation", Language.getMessage(mstrModuleName, 8, "To Regional/ District Revenue Officer"))
    '        Call ReportFunction.TextChange(objRpt, "lblYear", Language.getMessage(mstrModuleName, 9, "Year"))
    '        Call ReportFunction.TextChange(objRpt, "txtYear", FinancialYear._Object._FinancialYear_Name)
    '        Call ReportFunction.TextChange(objRpt, "lblOfficalUse", Language.getMessage(mstrModuleName, 10, "For Official Use"))
    '        Call ReportFunction.TextChange(objRpt, "lblStatisticalCode", Language.getMessage(mstrModuleName, 11, "Statistical Coding"))
    '        Call ReportFunction.TextChange(objRpt, "lblDepartment", Language.getMessage(mstrModuleName, 12, "Income Tax Department"))
    '        Call ReportFunction.TextChange(objRpt, "lblPOBox", Language.getMessage(mstrModuleName, 13, "P.O. Box"))
    '        Call ReportFunction.TextChange(objRpt, "txtPostBox", "")
    '        Call ReportFunction.TextChange(objRpt, "lblOrganization", Language.getMessage(mstrModuleName, 14, "Name of Organization/Institution"))
    '        Call ReportFunction.TextChange(objRpt, "txtOrganization", Company._Object._Name)
    '        Call ReportFunction.TextChange(objRpt, "lblNatureOfBusiness", Language.getMessage(mstrModuleName, 15, "Nature of Business "))
    '        Call ReportFunction.TextChange(objRpt, "txtNatureOfBusiness", "")
    '        Call ReportFunction.TextChange(objRpt, "lblPartnership", Language.getMessage(mstrModuleName, 16, "(State whether Parastatal/Other Compnay/Corporation or Partnership)"))
    '        Call ReportFunction.TextChange(objRpt, "txtPartnership", "")
    '        Call ReportFunction.TextChange(objRpt, "lblDeductionStatement", Language.getMessage(mstrModuleName, 17, "I/We forward herewith a statement of payments made and tax deducted during the six months"))
    '        Call ReportFunction.TextChange(objRpt, "lblforward", Language.getMessage(mstrModuleName, 18, "I/We forwarsd herewith"))
    '        Call ReportFunction.TextChange(objRpt, "txtNoOfEmployee", intTotalEmployee)
    '        Call ReportFunction.TextChange(objRpt, "lblTaxDeductionCard", Language.getMessage(mstrModuleName, 19, "Tax Deduction Cards *Form P9:  (GE) *P9 (ZE) "))
    '        Call ReportFunction.TextChange(objRpt, "lblPeriodEnded", Language.getMessage(mstrModuleName, 20, "under period ended 31"))
    '        Call ReportFunction.TextChange(objRpt, "txtPeriodEnded", mstrEndPeriodName)
    '        Call ReportFunction.TextChange(objRpt, "lblTaxCalenderYear", Language.getMessage(mstrModuleName, 21, "Total tax for the calendar year (as listed) amount to ") & " " & ConfigParameter._Object._Currency)
    '        Call ReportFunction.TextChange(objRpt, "txtTaxCalenderYear", mstrEndPeriodName)
    '        Call ReportFunction.TextChange(objRpt, "lblAnnualWagesRounded", Language.getMessage(mstrModuleName, 22, "Annual Wages rounded up to nearest shilling paid per payroll including wages not"))
    '        Call ReportFunction.TextChange(objRpt, "lblSubject", Language.getMessage(mstrModuleName, 23, "Subject to P.A.Y.E. ") & " " & ConfigParameter._Object._Currency)
    '        Call ReportFunction.TextChange(objRpt, "txtSubject", dblWagesTotal.ToString(GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "lblCertified", Language.getMessage(mstrModuleName, 24, "I/We certify that the particulars entered by us/me on these forms are correct"))
    '        Call ReportFunction.TextChange(objRpt, "lblEmployerSignature", Language.getMessage(mstrModuleName, 25, "Signature of Employer/Paying Office"))
    '        Call ReportFunction.TextChange(objRpt, "lblBranch", Language.getMessage(mstrModuleName, 26, "Branch"))
    '        Call ReportFunction.TextChange(objRpt, "lblPostalAddress", Language.getMessage(mstrModuleName, 27, "Postal Address"))
    '        Call ReportFunction.TextChange(objRpt, "lblPhysicalAddress", Language.getMessage(mstrModuleName, 28, "Physical Address"))
    '        Call ReportFunction.TextChange(objRpt, "lblDate", Language.getMessage(mstrModuleName, 29, "Date"))
    '        Call ReportFunction.TextChange(objRpt, "lblNotes", Language.getMessage(mstrModuleName, 30, "NOTE:"))
    '        Call ReportFunction.TextChange(objRpt, "lblIncomeRange", Language.getMessage(mstrModuleName, 31, "1)  For Income Range Groupings take the final month pay figure."))
    '        Call ReportFunction.TextChange(objRpt, "lblTaxPerAnnum", Language.getMessage(mstrModuleName, 32, "2)  For Tax paid per annum, take actaul Tax deducted and not the hypothetical tax that would have deducted."))
    '        Call ReportFunction.TextChange(objRpt, "lblDuplicate", Language.getMessage(mstrModuleName, 33, "3)  To be submitted in DUPLICATE to Income Tax Office with the required particulars on or before 31 January of the year following."))
    '        Call ReportFunction.TextChange(objRpt, "lblOriginalWhite", Language.getMessage(mstrModuleName, 34, "Origional (white)"))
    '        Call ReportFunction.TextChange(objRpt, "lblDuplicateYellow", Language.getMessage(mstrModuleName, 35, "Duplicate (Yellow). To Income Tax Office"))
    '        Call ReportFunction.TextChange(objRpt, "lblTriplicatePink", Language.getMessage(mstrModuleName, 35, "Triplicate (Pink)   . To be retained by the employer"))
    '        Call ReportFunction.TextChange(objRpt, "txtTotal", Language.getMessage(mstrModuleName, 36, "Total"))

    '        Call ReportFunction.TextChange(objRpt, "txtColumn5", intTotalEmployee)
    '        Call ReportFunction.TextChange(objRpt, "txtColumn6", Format(CDec(dblWagesTotal), GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtColumn7", Format(CDec(dblTaxTotal), GUI.fmtCurrency))
    '        Call ReportFunction.TextChange(objRpt, "txtColumn8", dblPercentage.ToString("###.##"))

    '        'S.SANDEEP [ 05 JULY 2011 ] -- START
    '        'ISSUE : LARGE NUMBER DISPLAY ON CRYSTAL REPORT
    '        'Call ReportFunction.SetRptDecimal(objRpt, "frmlGrandTot31")
    '        Call ReportFunction.TextChange(objRpt, "txtTotal1", Format(decColumn3Total, GUI.fmtCurrency))
    '        'S.SANDEEP [ 05 JULY 2011 ] -- END 

    '        Return objRpt

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Over")
            Language.setMessage(mstrModuleName, 2, "FORM     P. 10")
            Language.setMessage(mstrModuleName, 3, "TANZANIA   REVENUE   AUTHORITY")
            Language.setMessage(mstrModuleName, 4, "P.A.Y.E - EMPLOYER'S END OF YEAR CERTIFICATE")
            Language.setMessage(mstrModuleName, 5, "(Section 84(2) of the Income Tax at 2004)")
            Language.setMessage(mstrModuleName, 6, "Employer's")
            Language.setMessage(mstrModuleName, 7, "TIN  No.")
            Language.setMessage(mstrModuleName, 8, "To Regional/ District Revenue Officer")
            Language.setMessage(mstrModuleName, 9, "Year")
            Language.setMessage(mstrModuleName, 10, "For Official Use")
            Language.setMessage(mstrModuleName, 11, "Statistical Coding")
            Language.setMessage(mstrModuleName, 12, "Income Tax Department")
            Language.setMessage(mstrModuleName, 13, "P.O. Box")
            Language.setMessage(mstrModuleName, 14, "Name of Organization/Institution")
            Language.setMessage(mstrModuleName, 15, "Nature of Business")
            Language.setMessage(mstrModuleName, 16, "(State whether Parastatal/Other Compnay/Corporation or Partnership)")
            Language.setMessage(mstrModuleName, 17, "I/We forward herewith a statement of payments made and tax deducted during the six months")
            Language.setMessage(mstrModuleName, 18, "I/We forward herewith")
            Language.setMessage(mstrModuleName, 19, "Tax Deduction Cards *Form P9:  (GE) *P9 (ZE)")
            Language.setMessage(mstrModuleName, 20, "under period ended 31")
            Language.setMessage(mstrModuleName, 21, "Total tax for the calendar year (as listed) amount to")
            Language.setMessage(mstrModuleName, 22, "Annual Wages rounded up to nearest shilling paid per payroll including wages not")
            Language.setMessage(mstrModuleName, 23, "Subject to P.A.Y.E.")
            Language.setMessage(mstrModuleName, 24, "I/We certify that the particulars entered by us/me on these forms are correct")
            Language.setMessage(mstrModuleName, 25, "Signature of Employer/Paying Office")
            Language.setMessage(mstrModuleName, 26, "Branch")
            Language.setMessage(mstrModuleName, 27, "Postal Address")
            Language.setMessage(mstrModuleName, 28, "Physical Address")
            Language.setMessage(mstrModuleName, 29, "Date")
            Language.setMessage(mstrModuleName, 30, "NOTE:")
            Language.setMessage(mstrModuleName, 31, "1)  For Income Range Groupings take the final month pay figure.")
            Language.setMessage(mstrModuleName, 32, "2)  For Tax paid per annum, take actaul Tax deducted and not the hypothetical tax that would have been deducted.")
            Language.setMessage(mstrModuleName, 33, "3)  To be submitted in DUPLICATE to Income Tax Office with the required particulars on or before 31 January of the year following.")
            Language.setMessage(mstrModuleName, 34, "Original (white)")
            Language.setMessage(mstrModuleName, 35, "Duplicate (Yellow). To Income Tax Office")
            Language.setMessage(mstrModuleName, 36, "Total")
            Language.setMessage(mstrModuleName, 37, "Additional Information")
            Language.setMessage(mstrModuleName, 38, "Income Range")
            Language.setMessage(mstrModuleName, 39, "Number Of" & vbCrLf & "Employees")
            Language.setMessage(mstrModuleName, 40, "Wages" & vbCrLf & "Bill")
            Language.setMessage(mstrModuleName, 41, "Total Tax" & vbCrLf & "Paid Per Pg")
            Language.setMessage(mstrModuleName, 42, "Percentage Of" & vbCrLf & "Total Tax")
            Language.setMessage(mstrModuleName, 43, "Triplicate (Pink)   . To be retained by the employer")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
